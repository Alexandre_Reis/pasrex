﻿BEGIN TRANSACTION
SET XACT_ABORT ON

if(exists(select * from sys.all_objects where name = 'Movimentacao' and type = 'V'))
	drop view Movimentacao

GO

CREATE view dbo.Movimentacao as(
select distinct
	o.IdOperacao as ID,
	o.idcliente as CCI,
	p.cpfcnpj as CPF,
	a.codigo as Agencia,
	RTRIM(LTRIM(c.numero))as Conta,
	'?' as DAC,
	o.dataoperacao as DataOperacao,
	t.descricao as Mercadoria,
	o.taxaoperacao as Taxa,
	i.descricao as Indice,	
	t.datavencimento as Vencimento,
	e.nome as Emissor,
	case 
		when o.tipooperacao=1	then 'C'
		when o.tipooperacao=2	then 'V'
		when o.tipooperacao=3	then 'C'
		when o.tipooperacao=4	then 'V'
		when o.tipooperacao=6	then 'V'
		when o.tipooperacao=10	then 'C'
		when o.tipooperacao=11	then 'V'
		when o.tipooperacao=12	then 'V'
		when o.tipooperacao=13	then 'C'
		when o.tipooperacao=20	then 'E' 
		when o.tipooperacao=21	then 'S' 
	end as TipoOperacao,
	o.quantidade as Quantidade,
	o.puoperacao as PU,
	o.valor as Valor,
	o.rendimento as Rendimento,
	o.valorir as IR,
	o.valoriof as IOF,
	opEsp.Rendimento as Spread,
	case 
		when o.idcustodia= 2 then 'CETIP' 
		when o.idcustodia= 3 then 'CBLC' 
		else 'CBLC'
	end depositaria,
	s.IdAssessor assessor	
from 
operacaorendafixa o (NOLOCK)
join titulorendafixa t (NOLOCK)on o.idtitulo= t.idtitulo
left join emissor e (NOLOCK)on t.idemissor= e.idemissor
left join pessoa p (NOLOCK)on o.idcliente= p.idpessoa
left join contacorrente c (NOLOCK)on p.idpessoa= c.idpessoa
left join agencia a (NOLOCK)on c.idagencia= a.idagencia
left join indice i (NOLOCK)on t.idindice= i.idindice
left join clienteRendaFixa b (NOLOCK)on o.idcliente= b.idcliente
left join assessor s (NOLOCK)on b.idassessor= s.idassessor
left join Carteira ca (NOLOCK)on ca.IdCarteira = o.IdCliente
left join OperacaoRendaFixa opEsp (NOLOCK)on o.IdOperacaoEspelho = opEsp.IdOperacao
left join cliente cliEsp (NOLOCK)on cliEsp.IdCliente = opEsp.IdCliente
left join PosicaoRendaFixa posEsp (NOLOCK)on posEsp.IdCliente = opEsp.IdCliente and posEsp.IdTitulo = opEsp.IdTitulo and opEsp.DataOperacao = cliEsp.DataDia
left join PosicaoRendaFixaHistorico posEspH (NOLOCK)on posEspH.IdCliente = opEsp.IdCliente and posEspH.IdTitulo = opEsp.IdTitulo and posEspH.DataHistorico = opEsp.DataOperacao
where 
	c.contadefault='S' and 
	(ca.Corretora is null or ca.Corretora = 'N')

)

go

if(exists(select * from sys.all_objects where name = 'Posicao' and type = 'V'))
	drop view Posicao

GO

CREATE VIEW dbo.Posicao as(
select
	O.IdPosicao as ID,
	o.idcliente as CCI,
	p.cpfcnpj as CPF,
	a.codigo as Agencia,
	c.numero as Conta,
	'?' AS DAC,
	o.dataoperacao as DataOperacao,
	t.descricao as Mercadoria,
	o.taxaoperacao as Taxa,
	i.descricao as Indice,
	t.dataemissao as Emissao,
	t.datavencimento as Vencimento,
	e.nome as Emissor,	
	o.quantidade as Quantidade,
	o.PuMercado as PUMercado,	
	o.valormercado as Saldo,
	o.CustoCustodia as Custodia,
	o.valorir as IR,
	o.valoriof as IOF,
	case
		when o.idcustodia= 2 then 'CETIP' 
		when o.idcustodia= 3 then 'CBLC' 
	else 'CBLC'end depositaria,
	s.IdAssessor assessor
from 
	posicaorendafixahistorico o (NOLOCK)
	join titulorendafixa t (NOLOCK)on o.idtitulo= t.idtitulo
	join emissor e (NOLOCK)on t.idemissor= e.idemissor
	join pessoa p (NOLOCK)on o.idcliente= p.idpessoa
	left join contacorrente c (NOLOCK)on p.idpessoa= c.idpessoa
	left join agencia a (NOLOCK)on c.idagencia= a.idagencia
	left join indice i (NOLOCK)on t.idindice= i.idindice
	left join clienteRendaFixa b (NOLOCK)on o.idcliente= b.idcliente
	left join assessor s (NOLOCK)on b.idassessor= s.idassessor
	left join PapelRendaFixa pp (NOLOCK)on pp.IdPapel = t.IdPapel
	left join Carteira ca (NOLOCK) ON ca.IdCarteira = o.IdCliente
where 
	c.contadefault='S' and 
	(ca.Corretora is null or ca.Corretora = 'N')
)

GO

if(exists(select * from sys.all_objects where name = 'PB' and type = 'V'))
	drop view PB

go

CREATE view dbo.PB as(
select distinct	
	case 
		when posH.IdPosicao is not null then posH.IdPosicao
		else pos.IdPosicao
	end as ID,
	o.IdCliente as CCI,
	p.cpfcnpj as CPF,
	a.codigo as Agencia,
	RTRIM(LTRIM(c.numero))as Conta,
	'?' as DAC,
	o.dataoperacao as DataOperacao,
	t.descricao as Mercadoria,
	o.taxaoperacao as Taxa,
	i.descricao as Indice,
	t.dataemissao as Emissao,
	t.datavencimento as Vencimento,
	e.nome as Emissor,
	o.quantidade as Quantidade,	
	o.ValorIR + o.valorIOF as Tributos,
	'Operação' as Descricao,
	o.valorir as IR,
	o.valoriof as IOF,
	operacaoEspelho.Rendimento as Spread,
	case 
		when o.idcustodia= 2 then 'CETIP' 
		else 'CBLC'
	end depositaria,
	s.IdAssessor assessor
from 
operacaorendafixa o (NOLOCK)
join titulorendafixa t (NOLOCK)on o.idtitulo= t.idtitulo
left join emissor e (NOLOCK)on t.idemissor= e.idemissor
left join pessoa p (NOLOCK)on o.idcliente= p.idpessoa
left join contacorrente c (NOLOCK)on p.idpessoa= c.idpessoa
left join agencia a (NOLOCK)on c.idagencia= a.idagencia
left join indice i (NOLOCK)on t.idindice= i.idindice
left join clienteRendaFixa b (NOLOCK)on o.idcliente= b.idcliente
left join assessor s (NOLOCK)on b.idassessor= s.idassessor
left join Carteira ca (NOLOCK)on ca.IdCarteira = o.IdCliente
left join PapelRendaFixa pp (NOLOCK)on pp.IdPapel = t.IdPapel
left join Carteira carteiraContraparte (NOLOCK) on carteiraContraparte.IdCarteira = o.IdCarteiraContraparte
left join PosicaoRendaFixa pos (NOLOCK) ON pos.IdPosicao = o.IdOperacao
left join PosicaoRendaFixaHistorico posH (NOLOCK) on posH.IdOperacao = o.IdOperacao and posH.DataHistorico = o.DataOperacao
left join OperacaoRendaFixa operacaoEspelho (NOLOCK) on o.IdOperacaoEspelho = operacaoEspelho.IdOperacao
where 
	c.contadefault='S'	and
	o.TipoOperacao not in(2,4,6,11,21) and 
	(ca.Corretora is null or ca.Corretora = 'N') and
	carteiraContraparte.Corretora = 'S' 
union
select distinct	
	case 
		when posH.IdPosicao is not null then posH.IdPosicao
		else pos.IdPosicao
	end as ID,
	l.IdCliente as CCI,
	p.cpfcnpj as CPF,
	a.codigo as Agencia,
	RTRIM(LTRIM(c.numero))as Conta,
	'?' as DAC,
	o.dataoperacao as DataOperacao,
	t.descricao as Mercadoria,
	o.taxaoperacao as Taxa,
	i.descricao as Indice,
	t.dataemissao as Emissao,
	t.datavencimento as Vencimento,
	e.nome as Emissor,
	o.quantidade as Quantidade,	
	o.ValorIR + o.valorIOF as Tributos,
	'Custodia' as Descricao,
	o.valorir as IR,
	o.valoriof as IOF,
	l.CustoCustodia as Spread,
	case 
		when o.idcustodia= 2 then 'CETIP' 
		else 'CBLC'
	end depositaria,
	s.IdAssessor assessor	
from 
LiquidacaoTaxaCustodia l (NOLOCK)
left join OperacaoRendaFixa o (NOLOCK) ON o.IdOperacao = l.IdOperacao
left join titulorendafixa t (NOLOCK)on o.idtitulo= t.idtitulo
left join emissor e (NOLOCK)on t.idemissor= e.idemissor
left join pessoa p (NOLOCK)on o.idcliente= p.idpessoa
left join contacorrente c (NOLOCK)on p.idpessoa= c.idpessoa
left join agencia a (NOLOCK)on c.idagencia= a.idagencia
left join indice i (NOLOCK)on t.idindice= i.idindice
left join clienteRendaFixa b (NOLOCK)on o.idcliente= b.idcliente
left join assessor s (NOLOCK)on b.idassessor= s.idassessor
left join Carteira ca (NOLOCK)on ca.IdCarteira = o.IdCliente
left join PapelRendaFixa pp (NOLOCK)on pp.IdPapel = t.IdPapel
left join PosicaoRendaFixa pos (NOLOCK) ON pos.IdPosicao = o.IdOperacao
left join PosicaoRendaFixaHistorico posH (NOLOCK) on posH.IdOperacao = o.IdOperacao and posH.DataHistorico = l.DataHistorico
where 
	c.contadefault='S'	and	
	(ca.Corretora is null or ca.Corretora = 'N')
	)

GO

COMMIT TRANSACTION

