declare @data_versao char(10)
set @data_versao = '2016-04-19'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  n�o permite que seja executado novamente, avisa o usuario e 
*  n�o exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Aten��o este script ja foi executado anteriormente.',16,1)

	
	-- n�o mostra a execu��o do script
	set nocount on 
	
	-- n�o roda o script
	set noexec on 
	

end

begin transaction

	-- n�o permite que a transaction seja commitada em caso de erros
	set xact_abort on	
	
	insert into VersaoSchema values(@data_versao)

	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'CotaAtual' AND ID = OBJECT_ID('CalculoPerformance'))
	BEGIN
		alter table CalculoPerformance add CotaAtual decimal(28,12) NULL
	END
	
	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'CotaBase' AND ID = OBJECT_ID('CalculoPerformance'))
	BEGIN
		alter table CalculoPerformance add CotaBase decimal(28,12) NULL
	END
	
	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'CotaCorrigida' AND ID = OBJECT_ID('CalculoPerformance'))
	BEGIN
		alter table CalculoPerformance add CotaCorrigida decimal(28,12) NULL
	END
	
	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'FatorIndice' AND ID = OBJECT_ID('CalculoPerformance'))
	BEGIN
		alter table CalculoPerformance add FatorIndice decimal(28,12) NULL
	END

	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'FatorJuros' AND ID = OBJECT_ID('CalculoPerformance'))
	BEGIN
		alter table CalculoPerformance add FatorJuros decimal(28,12) NULL
	END	
	
	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'QuantidadeCotas' AND ID = OBJECT_ID('CalculoPerformance'))
	BEGIN
		alter table CalculoPerformance add QuantidadeCotas decimal(28,12) NULL
	END	
	
	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Rendimento' AND ID = OBJECT_ID('CalculoPerformance'))
	BEGIN
		alter table CalculoPerformance add Rendimento decimal(28,12) NULL
	END
	
	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'CotaAtual' AND ID = OBJECT_ID('CalculoPerformanceHistorico'))
	BEGIN
		alter table CalculoPerformanceHistorico add CotaAtual decimal(28,12) NULL
	END
	
	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'CotaBase' AND ID = OBJECT_ID('CalculoPerformanceHistorico'))
	BEGIN
		alter table CalculoPerformanceHistorico add CotaBase decimal(28,12) NULL
	END
	
	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'CotaCorrigida' AND ID = OBJECT_ID('CalculoPerformanceHistorico'))
	BEGIN
		alter table CalculoPerformanceHistorico add CotaCorrigida decimal(28,12) NULL
	END
	
	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'FatorIndice' AND ID = OBJECT_ID('CalculoPerformanceHistorico'))
	BEGIN
		alter table CalculoPerformanceHistorico add FatorIndice decimal(28,12) NULL
	END

	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'FatorJuros' AND ID = OBJECT_ID('CalculoPerformanceHistorico'))
	BEGIN
		alter table CalculoPerformanceHistorico add FatorJuros decimal(28,12) NULL
	END	
	
	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'QuantidadeCotas' AND ID = OBJECT_ID('CalculoPerformanceHistorico'))
	BEGIN
		alter table CalculoPerformanceHistorico add QuantidadeCotas decimal(28,12) NULL
	END	
	
	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'Rendimento' AND ID = OBJECT_ID('CalculoPerformanceHistorico'))
	BEGIN
		alter table CalculoPerformanceHistorico add Rendimento decimal(28,12) NULL
	END

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off