﻿USE [FIN_PORTOPAR]
GO
/****** Object:  Table [dbo].[CadastroComplementarTipoLista]    Script Date: 10/02/2015 17:36:36 ******/
SET IDENTITY_INSERT [dbo].[CadastroComplementarTipoLista] ON
INSERT [dbo].[CadastroComplementarTipoLista] ([IdTipoLista], [NomeLista], [TipoLista]) VALUES (5, N'AbertoFechado', -2)
INSERT [dbo].[CadastroComplementarTipoLista] ([IdTipoLista], [NomeLista], [TipoLista]) VALUES (6, N'AltaMediaBaixa', -2)
INSERT [dbo].[CadastroComplementarTipoLista] ([IdTipoLista], [NomeLista], [TipoLista]) VALUES (1, N'Cia Aberta ou não', -2)
INSERT [dbo].[CadastroComplementarTipoLista] ([IdTipoLista], [NomeLista], [TipoLista]) VALUES (13, N'Intrag', -2)
INSERT [dbo].[CadastroComplementarTipoLista] ([IdTipoLista], [NomeLista], [TipoLista]) VALUES (9, N'NivelAção', -2)
INSERT [dbo].[CadastroComplementarTipoLista] ([IdTipoLista], [NomeLista], [TipoLista]) VALUES (2, N'Publica ou Restrita', -2)
INSERT [dbo].[CadastroComplementarTipoLista] ([IdTipoLista], [NomeLista], [TipoLista]) VALUES (3, N'Qualificado ou Profissional', -2)
INSERT [dbo].[CadastroComplementarTipoLista] ([IdTipoLista], [NomeLista], [TipoLista]) VALUES (4, N'Sim ou Não', -2)
INSERT [dbo].[CadastroComplementarTipoLista] ([IdTipoLista], [NomeLista], [TipoLista]) VALUES (11, N'TipoClassificacaoCliente', -2)
INSERT [dbo].[CadastroComplementarTipoLista] ([IdTipoLista], [NomeLista], [TipoLista]) VALUES (7, N'TipoFundoComplementar', -2)
INSERT [dbo].[CadastroComplementarTipoLista] ([IdTipoLista], [NomeLista], [TipoLista]) VALUES (8, N'TipoInstFin', -2)
SET IDENTITY_INSERT [dbo].[CadastroComplementarTipoLista] OFF
