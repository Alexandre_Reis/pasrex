declare @data_versao char(10)
set @data_versao = '2014-10-31'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


ALTER TABLE PermissaoPortalCliente ADD AcessoPGBL char(1)
update PermissaoPortalCliente set AcessoPGBL = 'N'
alter table PermissaoPortalCliente alter column AcessoPGBL char(1) not null
ALTER TABLE PermissaoPortalCliente ADD CONSTRAINT [DF_PermissaoPortalCliente_AcessoPGBL]  DEFAULT (('N')) FOR AcessoPGBL