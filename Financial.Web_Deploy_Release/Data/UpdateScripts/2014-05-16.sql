declare @data_versao char(10)
set @data_versao = '2014-05-16'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

ALTER TABLE PosicaoRendaFixa ADD IdOperacao int NULL
ALTER TABLE PosicaoRendaFixaAbertura ADD IdOperacao int NULL
ALTER TABLE PosicaoRendaFixaHistorico ADD IdOperacao int NULL


INSERT INTO [dbo].[indice] ([IdIndice],[Descricao],[Tipo],[TipoDivulgacao])VALUES(94,'IFIX',2,1)