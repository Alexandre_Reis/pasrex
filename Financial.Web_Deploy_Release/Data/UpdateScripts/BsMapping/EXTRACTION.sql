﻿
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'fn_DiasUteis') 
    AND xtype IN (N'FN', N'IF', N'TF')
)
DROP FUNCTION fn_DiasUteis
GO

CREATE FUNCTION [dbo].[fn_DiasUteis] (@StartDate datetime,	@EndDate datetime, @IdLocal int) 
RETURNS @DateValues table(DateValue datetime)
AS
BEGIN	
	--se o horario da data de inicio for maior que 17h nÆo considera o dia atual como dia util
	IF(DATENAME(hh,DATEADD (hh , 7 , @StartDate )) < 7)
		SET @StartDate = @StartDate + 1;

	--se o horario da data de fim for maior que 17 horas adiciona um dia a mais e conta o ultimo dia como util
	IF(DATENAME(hh,DATEADD (hh , 7 , @EndDate )) < 7)
		SET @EndDate = @EndDate + 1;

	--trunca o horario das datas
	SET @StartDate = CAST( @StartDate AS DATE);
	SET @EndDate = CAST( @EndDate AS DATE);

	--rotina discursiva que traz todas as datas entre o periodo selecionado
	WITH mycte AS
	(
	  SELECT @StartDate DateValue
	  UNION ALL
	  SELECT  DateValue +1
	  FROM    mycte   
	  WHERE   DateValue +1 < @EndDate
	)

	--seleciona as datas da cte acima excluindo os sabados domingos e feriados
	INSERT INTO @DateValues
	SELECT  
		DateValue
	FROM
		mycte
	WHERE 
		DATENAME(dw,DateValue) <> 'Sunday' AND
		DATENAME(dw,DateValue) <> 'Saturday' AND
		NOT EXISTS (SELECT 1 FROM Feriado f WHERE f.Data = DateValue and IdLocal = @IdLocal) 
	OPTION (MAXRECURSION 0)


	RETURN
END

GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BSC_MOVIMENTO') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BSC_MOVIMENTO

GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BSC_MOVIMENTO]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT 
		CONVERT(int				,	opCot.IdOperacao			)	as	BSC_ID_NOTA,
		CONVERT(datetime		,	opCot.DataOperacao			)	as	BSC_DT_MOVIMENTO,/*dataregistro fin_gps*/
		CONVERT(char(2)			,	tpOperacao.CodigoExterno	)	as	BSC_CD_TIPO,
		CONVERT(char(1)			,	null						)	as	BSC_CD_PADRAO,
		CONVERT(datetime		,	null						)	as	BSC_DT_RETROATIVO_ESTORNO,
		CONVERT(char(15)		,	cot.IdCotista				)	as	BSC_CD_COTISTA,
		CONVERT(char(15)		,	cat.IdCarteira				)	as	BSC_CD_FUNDO,
		CONVERT(char(2)			,	'CC'						)	as	BSC_CD_LIQUIDACAO,
		CONVERT(float		,	null						)	as	BSC_VL_DIGITADO,
		CONVERT(float		,	null						)	as	BSC_QT_COTAS_DIGITADA,
		CONVERT(int				,	null						)	as	BSC_ID_NOTA_DIGITADA,
		CONVERT(char(1)			,	critResgate.CodigoExterno	)	as	BSC_CD_CRITERIO_RESGATE,
		CONVERT(datetime		,	opCot.DataLiquidacao		)	as	BSC_DT_LIQUIDACAO_FINANCEIRA,
		CONVERT(datetime		,	opCot.DataConversao			)	as	BSC_DT_LIQUIDACAO_FISICA,
		CONVERT(float		,	opcot.ValorBruto			)	as	BSC_VL_BRUTO,
		CONVERT(float		,	opCot.ValorIR				)	as	BSC_VL_IR,
		CONVERT(float		,	opcot.ValorIOF				)	as	BSC_VL_IOF,
		CONVERT(float		,	null						)	as	BSC_VL_PERFORMANCE,
		CONVERT(float		,	opCot.Quantidade			)	as	BSC_QT_COTAS,
		CONVERT(float		,	opCot.ValorLiquido			)	as	BSC_VL_LIQUIDO,
		CONVERT(int				,	null						)	as	BSC_ID_BANCO,
		CONVERT(char(5)			,	null						)	as	BSC_CD_AGENCIA_EXTERNA,
		CONVERT(char(12)		,	null						)	as	BSC_CD_CONTA_EXTERNA,
		CONVERT(datetime		,	opCot.DataOperacao			)	as	BSC_DT_DIGITACAO,
		CONVERT(char(1)			,	null						)	as	BSC_IC_SN_NOTA_EMITIDA,
		CONVERT(char(18)		,	null						)	as	BSC_CD_USUARIO_DIGITACAO,
		CONVERT(char(18)		,	null						)	as	BSC_CD_USUARIO_CONFERENTE,
		CONVERT(char(18)		,	null						)	as	BSC_CD_USUARIO_AUTORIZANTE,
		CONVERT(int				,	null						)	as	BSC_ID_NOTA_ORIGEM,
		CONVERT(datetime		,	null						)	as	BSC_DT_TRANSFERENCIA,
		CONVERT(int				,	null						)	as	BSC_ID_NOTA_BALANCEAMENTO,
		CONVERT(varchar(100)	,	null						)	as	BSC_DS_HISTORICO,
		CONVERT(char(1)			,	null						)	as	BSC_IC_SN_EXPORTADA,
		CONVERT(char(1)			,	null						)	as	BSC_IC_SN_NETBOL,
		CONVERT(char(15)		,	null						)	as	BSC_CD_CLEARING,
		CONVERT(char(1)			,	null						)	as	BSC_IC_AR_LIQUIDACAO,
		CONVERT(char(1)			,	null						)	as	BSC_IC_SN_CPMF,
		CONVERT(float		,	null						)	as	BSC_VL_CPMF,
		CONVERT(char(1)			,	null						)	as	BSC_IC_SN_CONFERIDO,
		CONVERT(datetime		,	null						)	as	BSC_DT_CONFERENCIA,
		CONVERT(float		,	null						)	as	BSC_VL_PENALTY,
		CONVERT(decimal(9)		,	null						)	as	BSC_ID_CONTA,
		CONVERT(float		,	null						)	as	BSC_VL_COTA_MOVIMENTACAO,
		CONVERT(char(1)			,	null						)	as	BSC_IC_SN_FEITO,
		CONVERT(char(15)		,	null						)	as	BSC_CD_CUSTODIA,
		CONVERT(char(15)		,	null						)	as	BSC_CD_INTERFACE_1,
		CONVERT(datetime		,	null						)	as	BSC_DT_POSICAO_FUNDO,
		CONVERT(char(1)			,	null						)	as	BSC_IC_AF_POSICAO,
		CONVERT(char(1)			,	null						)	as	BSC_IC_SN_LIBERADO_PENALTY,
		CONVERT(char(18)		,	null						)	as	BSC_CD_USUARIO_PENALTY,
		CONVERT(int				,	null						)	as	BSC_ID_BANCO_TERCEIROS,
		CONVERT(char(5)			,	null						)	as	BSC_CD_AGENCIA_TERCEIROS,
		CONVERT(char(12)		,	null						)	as	BSC_CD_CONTA_TERCEIROS,
		CONVERT(int				,	null						)	as	BSC_ID_SAC_DIVIDENDO,
		CONVERT(char(1)			,	null						)	as	BSC_IC_SN_ENVIADO,
		CONVERT(char(15)		,	null						)	as	BSC_CD_DIAS_LIQ_RESGATE,
		CONVERT(char(2)			,	null						)	as	BSC_CD_CONTA_ORDEM,
		CONVERT(char(20)		,	null						)	as	BSC_CD_IF,
		CONVERT(char(5)			,	null						)	as	BSC_CD_TIPO_COLAGEM,
		CONVERT(char(2)			,	null						)	as	BSC_CD_TIPO_TRANSFERENCIA,
		CONVERT(char(1)			,	null						)	as	BSC_IC_SN_MOV_RESTAURADO,
		CONVERT(datetime		,	null						)	as	BSC_DT_LIQ_FINAN_ANTECIPADA,
		CONVERT(char(1)			,	null						)	as	BSC_IC_SN_NEGOCIACAO,
		CONVERT(char(1)			,	null						)	as	BSC_IC_SN_ZERAGEM,
		CONVERT(char(1)			,	null						)	as	BSC_IC_SN_ENVIADO_EMAIL
	FROM 
		OperacaoCotista opCot
	LEFT JOIN DePara tpOperacao ON
		tpOperacao.CodigoInterno = opCot.TipoOperacao AND
		tpOperacao.IdTipoDePara = ( 
			SELECT TOP(1)
				IdTipoDePara 
			FROM 
				TipoDePara 
			WHERE 
				Descricao = (
					CASE 
						WHEN (opCot.TipoOperacao <> 1 AND opCot.TipoResgate =1)
							THEN 'TipoOperacaoCotistaNota' 
						ELSE 'TipoOperacaoCotista' 			
					END
				)
		)
	LEFT JOIN Cotista cot ON
		cot.IdCotista = opCot.IdCotista	
	LEFT JOIN Carteira cat on
		cat.IdCarteira = opCot.IdCarteira
	LEFT JOIN DePara critResgate ON 
		critResgate.CodigoInterno = opCot.TipoOperacao AND
		critResgate.IdTipoDePara = -19
	WHERE 
		opCot.DataOperacao = @DataRef
END



GO


















































IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BSC_RESGATE') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BSC_RESGATE
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BSC_RESGATE]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(int			,	detResgCot.IdOperacao							)	as	BSC_ID_NOTA,
		CONVERT(int			,	detResgCot.IdPosicaoResgatada					)	as	BSC_ID_NOTA_APLICACAO,
		CONVERT(datetime	,	null											)	as	BSC_DT_RESGATE,
		CONVERT(datetime	,	posCotH.DataAplicacao							)	as	BSC_DT_APLICACAO,
		CONVERT(float	,	detResgCot.Quantidade							)	as	BSC_QT_COTAS,
		CONVERT(float	,	posCotH.CotaAplicacao							)	as	BSC_VL_COTA_APLICACAO,
		CONVERT(float	,	detResgCot.Quantidade*posCotH.CotaAplicacao		)	as	BSC_VL_APLICACAO,
		CONVERT(float	,	null											)	as	BSC_VL_CUSTO_MEDIO,
		CONVERT(float	,	null											)	as	BSC_VL_CORRIGIDO,
		CONVERT(float	,	null											)	as	BSC_VL_RECEITA_SAQUE_CARENCIA,
		CONVERT(float	,	detResgCot.ValorIOF								)	as	BSC_VL_IOF,
		CONVERT(float	,	null											)	as	BSC_VL_IOF_APLICACAO,
		CONVERT(float	,	null											)	as	BSC_VL_IOF_VIRTUAL,
		CONVERT(float	,	detResgCot.ValorBruto							)	as	BSC_VL_BRUTO_RESGATE,
		CONVERT(float	,	detResgCot.ValorIR								)	as	BSC_VL_IR,
		CONVERT(float	,	null											)	as	BSC_VL_RENDIMENTO_RESGATE_IR,
		CONVERT(float	,	null											)	as	BSC_VL_PENALTY_FEE,
		CONVERT(float	,	null											)	as	BSC_PC_ALIQUOTA_IR,
		CONVERT(float	,	0												)	as	BSC_VL_RENDIMENTO_COMPENSADO,
		CONVERT(float	,	detResgCot.ValorLiquido							)	as	BSC_VL_LIQUIDO_RESGATE,
		CONVERT(float	,	null											)	as	BSC_VL_PERFORMANCE,
		CONVERT(float	,	null											)	as	BSC_VL_PERFORMANCE_ATIVO,
		CONVERT(float	,	null											)	as	BSC_VL_PIP,
		CONVERT(float	,	null											)	as	BSC_VL_IR_PIP,
		CONVERT(float	,	null											)	as	BSC_VL_DEVOLUCAO_TAXA_ADMIN,
		CONVERT(float	,	null											)	as	BSC_VL_TAXA_ADMINISTRACAO,
		CONVERT(float	,	null											)	as	BSC_VL_DEVOLUCAO_PERFORMANCE,
		CONVERT(datetime	,	null											)	as	BSC_DT_COTA_UTILIZADA,
		CONVERT(float	,	null											)	as	BSC_VL_COTA_UTILIZADA,
		CONVERT(float	,	null											)	as	BSC_VL_RENDIMENTO_TRIBUTADO,
		CONVERT(float	,	null											)	as	BSC_VL_RENDIMENTO_ISENTO,
		CONVERT(float	,	null											)	as	BSC_PC_ALIQ_1,
		CONVERT(float	,	null											)	as	BSC_VL_REND_1,
		CONVERT(float	,	null											)	as	BSC_PC_ALIQ_2,
		CONVERT(float	,	null											)	as	BSC_VL_REND_2,
		CONVERT(float	,	null											)	as	BSC_VL_REND_1_COMPENSADO,
		CONVERT(float	,	null											)	as	BSC_VL_REND_2_COMPENSADO,
		CONVERT(float	,	null											)	as	BSC_VL_REND_ULTIMO_COME_COTAS,
		CONVERT(float	,	null											)	as	BSC_VL_REND_PRIM_COME_COTAS,
		CONVERT(float	,	null											)	as	BSC_VL_REND_SEGU_COME_COTAS,
		CONVERT(float	,	null											)	as	BSC_VL_REND_TERC_COME_COTAS,
		CONVERT(decimal(9)	,	null											)	as	BSC_VL_DESENQ_RENDIMENTO,
		CONVERT(decimal(9)	,	null											)	as	BSC_VL_DESENQ_RENDIMENTO_COMPL,
		CONVERT(char(2)		,	null											)	as	BSC_CD_DESENQ,
		CONVERT(decimal(9)	,	null											)	as	BSC_VL_DESENQ_RENDIMENTO_1,
		CONVERT(decimal(9)	,	null											)	as	BSC_VL_DESENQ_RENDIMENTO_COMPL_1,
		CONVERT(char(2)		,	null											)	as	BSC_CD_DESENQ_1,
		CONVERT(float	,	null											)	as	BSC_VL_IR_CONTA_ORDEM,
		CONVERT(float	,	null											)	as	BSC_VL_IOF_CONTA_ORDEM,
		CONVERT(decimal(9)	,	null											)	as	BSC_VL_CUSTO_CONTABIL
	FROM DetalheResgateCotista detResgCot
	LEFT JOIN OperacaoCotista opCotista ON detResgCot.IdOperacao = opCotista.IdOperacao
	LEFT JOIN PosicaoCotistaHistorico posCotH on detResgCot.IdPosicaoResgatada =  posCotH.IdPosicao 
		AND posCotH.DataHistorico = @DataRef
	WHERE opCotista.DataOperacao = @DataRef
END

GO

IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_BAS_CAD_IF') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_BAS_CAD_IF
GO

CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_BAS_CAD_IF]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(char(8),'AM' + CAST(am.IdAgente AS VARCHAR(MAX))) as BS_CODINST,
		CONVERT(char(15), am.Nome) as BS_NOMEINST,
		CONVERT(char(4), 'AM'+CAST(am.IdAgente AS VARCHAR(MAX))) as BS_CODEMPMAE,
		CONVERT(float, null) as BS_LIMCRED,
		CONVERT(char(1), am.FuncaoCustodiante) as BS_IC_CUSTODIANTE,
		CONVERT(varchar(35), null) as BS_DS_RZSOCIAL,
		CONVERT(varchar(35), am.Endereco) as BS_DS_ENDERECO,
		CONVERT(char(12), am.Bairro) as BS_NM_BAIRRO,
		CONVERT(char(12), am.Cidade) as BS_NM_CIDADE,
		CONVERT(char(2), am.UF) as BS_CD_UF,
		CONVERT(char(8), am.CEP) as BS_NO_CEP,
		CONVERT(varchar(18), am.CNPJ) as BS_NO_CGC,
		CONVERT(float, 0) as BS_NO_DD_PRAZO,
		CONVERT(char(1), 'S') as BS_IC_LIMITE_CREDITO,
		CONVERT(float, 0) as BS_VL_LIMCRED_ESPECIAL,
		CONVERT(datetime, null) as BS_DT_EXPIRACAO_LIMCRED,
		CONVERT(char(1), 'N') as BS_IC_NOTA_NEG,
		CONVERT(char(3), null) as BS_CD_BANCO,
		CONVERT(char(9), null) as BS_CD_SELIC,
		CONVERT(char(8), am.CodigoCetip) as BS_CD_CETIP,
		CONVERT(char(20), null) as BS_NM_EMISSOR_CETIP,
		CONVERT(char(1), 'S') as BS_IC_FINANCEIRA,
		CONVERT(char(15), am.CartaPatente) as BS_CD_CARTA_PATENTE,
		CONVERT(char(3), null) as BS_CD_BANCO_CC,
		CONVERT(char(4), null) as BS_CD_AGENCIA_CC,
		CONVERT(char(10), null) as BS_CD_CONTA_CC,
		CONVERT(char(2), null) as BS_CD_DIGITO_CC,
		CONVERT(char(1), null) as BS_IC_REESTRUTURACAO,
		CONVERT(char(9), null) as BS_CD_SELIC_CLI1,
		CONVERT(char(1), null) as BS_IC_EXPORTA_SELIC,
		CONVERT(char(1), null) as BS_IC_EXPORTA_CETIP,
		CONVERT(char(6), null) as BS_CD_CONTROLE_FINANC,
		CONVERT(char(9), null) as BS_CD_CETIP_CLI1,
		CONVERT(char(8), null) as BS_CD_GRUPO_ECONOMICO,
		CONVERT(char(6), null) as BS_CD_ISIN,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO,
		CONVERT(float, null) as BS_PAT_LIQ,
		CONVERT(char(10), null) as BS_CD_SPC,
		CONVERT(char(9), null) as BS_CD_ISPB,
		CONVERT(char(15), null) as BS_CD_CODCLI,
		CONVERT(char(1), 'J') as BS_IC_PJ_PF,
		CONVERT(char(1), 'N') as BS_IC_INATIVO,
		CONVERT(char(8), ratingCc.ValorCampo) as BS_CD_RATING,
		CONVERT(char(1), speCc.ValorCampo) as BS_SPE,
		CONVERT(char(40),tipoInstCc.ValorCampo) as BS_TIPO_INST_FIN,
		@DataRef AS DataRef
	FROM AgenteMercado am 
	LEFT JOIN CadastroComplementar ratingCc ON ratingCC.IdMercadoTipoPessoa = am.IdAgente
		AND ratingCc.IdCamposComplementares = (
				SELECT IdCamposComplementares FROM CadastroComplementarCampos ratingCcc 
					WHERE ratingCcc.NomeCampo = 'CD RATING'
						AND ratingCcc.TipoCadastro = -1
			)
	LEFT JOIN CadastroComplementar speCc ON speCc.IdMercadoTipoPessoa = am.IdAgente
		AND speCc.IdCamposComplementares = (
				SELECT IdCamposComplementares FROM CadastroComplementarCampos ratingCcc 
					WHERE ratingCcc.NomeCampo = 'SPE'
						AND ratingCcc.TipoCadastro = -1
			)
	LEFT JOIN CadastroComplementar tipoInstCc ON
		tipoInstCc.IdMercadoTipoPessoa = am.IdAgente AND
		tipoInstCc.IdCamposComplementares = (
			SELECT 
				IdCamposComplementares
			FROM 
				CadastroComplementarCampos  ccc
			WHERE
				ccc.NomeCampo = 'TIPO INST FIN' AND
				ccc.TipoCadastro = -1
		)
	WHERE am.FuncaoCorretora = 'S' 
		OR am.FuncaoGestor = 'S' 
		OR am.FuncaoLiquidante = 'S'
		OR am.FuncaoCustodiante = 'S' 
		OR am.FuncaoDistribuidor = 'S'	
	UNION
		SELECT
		CONVERT(char(8),'EM' + CAST(em.IdEmissor AS VARCHAR(MAX))) as BS_CODINST,
		CONVERT(char(15), em.Nome) as BS_NOMEINST,
		CONVERT(char(4),'EM'+CAST(em.IdEmissor AS VARCHAR(MAX))) as BS_CODEMPMAE,
		CONVERT(float, null) as BS_LIMCRED,
		CONVERT(char(1), 'N') as BS_IC_CUSTODIANTE,
		CONVERT(varchar(35), null) as BS_DS_RZSOCIAL,
		CONVERT(varchar(35), null) as BS_DS_ENDERECO,
		CONVERT(char(12), null) as BS_NM_BAIRRO,
		CONVERT(char(12), null) as BS_NM_CIDADE,
		CONVERT(char(2), null) as BS_CD_UF,
		CONVERT(char(8), null) as BS_NO_CEP,
		CONVERT(varchar(18), null) as BS_NO_CGC,
		CONVERT(float, 0) as BS_NO_DD_PRAZO,
		CONVERT(char(1), 'S') as BS_IC_LIMITE_CREDITO,
		CONVERT(float, 0) as BS_VL_LIMCRED_ESPECIAL,
		CONVERT(datetime, null) as BS_DT_EXPIRACAO_LIMCRED,
		CONVERT(char(1), 'N') as BS_IC_NOTA_NEG,
		CONVERT(char(3), null) as BS_CD_BANCO,
		CONVERT(char(9), null) as BS_CD_SELIC,
		CONVERT(char(8), null) as BS_CD_CETIP,
		CONVERT(char(20), null) as BS_NM_EMISSOR_CETIP,
		CONVERT(char(1), 'N') as BS_IC_FINANCEIRA,
		CONVERT(char(15), null) as BS_CD_CARTA_PATENTE,
		CONVERT(char(3), null) as BS_CD_BANCO_CC,
		CONVERT(char(4), null) as BS_CD_AGENCIA_CC,
		CONVERT(char(10), null) as BS_CD_CONTA_CC,
		CONVERT(char(2), null) as BS_CD_DIGITO_CC,
		CONVERT(char(1), null) as BS_IC_REESTRUTURACAO,
		CONVERT(char(9), null) as BS_CD_SELIC_CLI1,
		CONVERT(char(1), null) as BS_IC_EXPORTA_SELIC,
		CONVERT(char(1), null) as BS_IC_EXPORTA_CETIP,
		CONVERT(char(6), null) as BS_CD_CONTROLE_FINANC,
		CONVERT(char(9), null) as BS_CD_CETIP_CLI1,
		CONVERT(char(8), null) as BS_CD_GRUPO_ECONOMICO,
		CONVERT(char(6), null) as BS_CD_ISIN,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO,
		CONVERT(float, null) as BS_PAT_LIQ,
		CONVERT(char(10), null) as BS_CD_SPC,
		CONVERT(char(9), null) as BS_CD_ISPB,
		CONVERT(char(15), null) as BS_CD_CODCLI,
		CONVERT(char(1), 'J') as BS_IC_PJ_PF,
		CONVERT(char(1), 'N') as BS_IC_INATIVO,
		CONVERT(char(8), null) as BS_CD_RATING,
		CONVERT(char(1), null) as BS_SPE,
		CONVERT(char(40),null) as BS_TIPO_INST_FIN,
		@DataRef AS DataRef
	FROM
		Emissor em
	

END


GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_BA_ADMINISTRACAO') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_BA_ADMINISTRACAO
GO

CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_BA_ADMINISTRACAO]  
	@DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT 
		CONVERT(char(15)	,	am.IdAgente	)	as	BS_CD_ADMINISTRACAO,	-- int not null				->	char(15) not null		ok
		CONVERT(varchar(30)	,	am.Nome		)	as	BS_NM_ADMINISTRACAO,	-- varchar(100) not null	->	varchar(30) not null	trunca a partir de 30 caracteres
		CONVERT(varchar(15)	,	am.Apelido	)	as	BS_NM_ABREVIADO,		-- varchar(20)				->	varchar(15) 			trunca a partir de 15 caracteres
		CONVERT(char(1)		,	''			)	as	BS_CD_LOGOTIPO,			-- null						->	char(1) not null		?
		CONVERT(char(10)	,	null		)	as	BS_CD_EXTERNO,			-- null						->	char(10)				ok
		CONVERT(varchar(18)	,	am.CNPJ		)	as	BS_CNPJ,				-- varchar(20)				->	varchar(18) 			trunca a partir de 18 caracteres
		CONVERT(char(1)		,	null		)	as	BS_IC_LOG_LIBERADO,		-- null						->	char(1)					ok 
		CONVERT(int			,	null		)	as	BS_ID_LOG_LIBERADO,		-- null						->	int						ok 
		CONVERT(char(60)	,	null		)	as	BS_DS_LOGOTIPO,			-- null						->	char(60)				ok 
		CONVERT(varchar(35)	,	am.Endereco	)	as	BS_DS_RUA,				-- varchar(255)				->	varchar(35)				trunca a partir de 35 caracteres
		CONVERT(char(6)		,	null		)	as	BS_NO_NUMERO,			-- null						->	char(6)					ok 
		CONVERT(char(20)	,	am.Cidade	)	as	BS_DS_CIDADE,			-- varchar(100)				->	char(20)				trunca a partir de 20 caracteres
		CONVERT(char(2)		,	am.UF		)	as	BS_SG_UF,				-- varchar(2)				->	char(2)					ok 
		CONVERT(char(8)		,	am.CEP		)	as	BS_NO_CEP,				-- varchar(10)				->	char(8)					trunca a partir de 8 caracteres 
		CONVERT(char(12)	,	'BRASIL'	)	as	BS_DS_PAIS,				-- 'BRASIL'(fixo)			->	char(12)				ok 
		CONVERT(char(8)		,	null		)	as	BS_CD_GRUPO_ECONOMICO,	-- null						->	char(8)					ok 
		@DataRef as DataRef
	FROM AgenteMercado am
	WHERE am.FuncaoAdministrador = 'S'

END

GO



	IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_BA_CLASSIFICACAO_FUNDOS') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_BA_CLASSIFICACAO_FUNDOS
GO

CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_BA_CLASSIFICACAO_FUNDOS]  
	@DataRef date --Data em que a procedure vai rodar    
AS
BEGIN
	SELECT 
		CONVERT(char(3), null) as BS_ID_TIPO_CLASSIFICACAO,
		CONVERT(char(5), null) as BS_SG_ORGAO_CLASSIFICADOR,
		CONVERT(varchar(150), null) as BS_DS_CLASSIFICACAO,
		CONVERT(char(1), null) as BS_CDA_IN409,
		CONVERT(char(4), null) as BS_CD_TIPO_FUNDO,
		@DataRef as DataRef
	FROM CategoriaFundo
END 

GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_BOLSA') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_BOLSA
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_BOLSA]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT TOP(1)
	CONVERT(char(8),		'BOVESPA')	as	BS_CD_BOLSA,
	CONVERT(varchar(50),	'BM&FBOVESPA')	as	BS_DS_BOLSA,
	CONVERT(int,			null)	as	BS_NO_DIAS_LIQ_FUTUROS,
	CONVERT(char(2),		null)	as	BS_CD_PAIS,
	CONVERT(int,			null)	as	BS_ID_LOCAL,
	CONVERT(char(8),		null)	as	BS_CD_MOEDA,
	CONVERT(char(1),		null)	as	BS_IC_BOLSA_BRASIL,
	CONVERT(float,		null)	as	BS_ID_TIPO_LOCAL,
	CONVERT(varchar(120),	null)	as	BS_DS_PAIS,
	CONVERT(char(14),		null)	as	BS_CD_CNPJ,
	CONVERT(char(1),		null)	as	BS_IC_LOG_LIBERADO,
	CONVERT(char(2),		null)	as	BS_SG_TIPO_LOCAL,
	CONVERT(char(1),		null)	as	BS_IC_PARAISO_FISCAL
	FROM SYS.all_columns
END

GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_CAIXA') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_CAIXA
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_CAIXA]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(int, li.IdLiquidacao) as BS_ORDEM,
		CONVERT(datetime, li.DataVencimento) as BS_DATALIQ,
		CONVERT(char(15), li.IdCliente) as BS_CODCLI,
		CONVERT(char(2),origem.CodigoExterno ) as BS_ORIGEM,
		CONVERT(char(3), tipo.CodigoExterno) as BS_TIPO,
		CONVERT(varchar(75), li.Descricao) as BS_HISTORICO,
		CONVERT(float, li.Valor) as BS_VALOR,
		CONVERT(float, li.Valor) as BS_VALORAUX,
		CONVERT(char(8), null) as BS_INDEXLANC,
		CONVERT(char(5), null) as BS_NUMDOC,
		CONVERT(char(1), null) as BS_FLAGLIQ,
		CONVERT(datetime, li.DataLancamento) as BS_DATALANC,
		CONVERT(char(1), 'N') as BS_FLAGALT,
		CONVERT(char(1), 'N') as BS_FLAGDEL,
		CONVERT(char(8), 'Real') as BS_CODMOEDA,
		CONVERT(float, null) as BS_VALORCALC,
		CONVERT(char(5), null) as BS_CODATIVO,
		CONVERT(char(1), null) as BS_IC_SFB,
		CONVERT(char(2), null) as BS_CD_PRACA,
		CONVERT(char(1), null) as BS_IC_CETIP,
		CONVERT(char(20), null) as BS_LOCAL_CETIP,
		CONVERT(char(20), null) as BS_DS_TIPO_LIQ_FIN,
		CONVERT(char(11), null) as BS_CD_USUARIO,
		CONVERT(datetime, null) as BS_DT_ATUALIZACAO,
		CONVERT(char(1), null) as BS_IC_INCIDE_CPMF,
		CONVERT(char(1), null) as BS_IC_REC_DESP,
		CONVERT(int, null) as BS_ID_PENDENCIA,
		CONVERT(datetime, null) as BS_DT_PENDENCIA,
		CONVERT(char(1), null) as BS_FLAG_AJUSTE,
		CONVERT(datetime, null) as BS_DT_LANC_LIQPARCIAL,
		CONVERT(char(1), null) as BS_IC_FLAG_RETRO,
		CONVERT(char(1), null) as BS_IC_FLAG_PROC,
		CONVERT(char(8), null) as BS_RVCOR_CD,
		CONVERT(char(12), null) as BS_CD_ATIVO,
		CONVERT(char(1), null) as BS_IC_TAXA_CDI,
		CONVERT(float, null) as BS_IDDIR_PC,
		CONVERT(int, null) as BS_ID_REFERENCIA,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO,
		CONVERT(char(10), null) as BS_COD_DESP_REC,
		CONVERT(char(5), null) as BS_SG_LOG_LIBERADO,
		CONVERT(char(4), null) as BS_CD_SUBSEGTO,
		CONVERT(varchar(80), null) as BS_HIST_TRADUZIDO,
		CONVERT(int, null) as BS_CODTLF,
		CONVERT(char(8), null) as BS_CODCLR,
		CONVERT(datetime, null) as BS_HORA_PARTIDA,
		CONVERT(int, null) as BS_PRIORIDADE,
		CONVERT(char(1), null) as BS_CD_PRIORIDADE,
		CONVERT(float, 0) as BS_QT,
		CONVERT(char(4), null) as BS_CD_CONTRAPARTE,
		CONVERT(char(8), null) as BS_CD_ATIVO_CC,
		CONVERT(numeric(5), li.IdConta) as BS_ID_CONTA,
		CONVERT(float, null) as BS_VL_MOEDA_CARTEIRA,
		CONVERT(char(5), null) as BS_CD_DISTRIBUICAO,
		CONVERT(numeric(9), null) as BS_NO_CONTRATO,
		CONVERT(char(5), null) as BS_CD_TIPO_DESP_REC,
		CONVERT(char(20), null) as BS_CD_EXT_LANC,
		CONVERT(char(15), null) as BS_CD_ESTRATEGIA_MOV,
		CONVERT(datetime, null) as BS_DT_EX_PROVENTO
	FROM Liquidacao li 
	LEFT JOIN Cliente cli ON 
		cli.IdCliente = li.IdCliente
	LEFT JOIN DePara origem ON 
		origem.IdTipoDePara = -4 AND	
		origem.CodigoInterno = li.Origem
	LEFT JOIN DePara tipo ON
		tipo.IdTipoDePara = -5 AND 
		tipo.CodigoInterno = li.Origem
	WHERE li.DataVencimento = @DataRef
END

GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_CLIENTE') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_CLIENTE
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_CLIENTE]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(char(15),ca.IdCarteira) as BS_CODCLI,
		CONVERT(char(8), agAdm.Nome) as BS_CODADM,
		CONVERT(varchar(60), ca.Nome) as BS_NOME,
		CONVERT(char(8), mo.Nome) as BS_MOEDA,
		CONVERT(varchar(18), pes.Cpfcnpj) as BS_CGC,
		CONVERT(char(1), 1) as BS_NIVEL,
		CONVERT(char(8), null) as BS_CODBVSP,
		CONVERT(char(8), null) as BS_CODBMF,
		CONVERT(char(1), 'M') as BS_TIPO,
		CONVERT(datetime, ca.DataInicioCota) as BS_DATAINIC,
		CONVERT(float, ca.CotaInicial) as BS_COTAINIC,
		CONVERT(char(8), null) as BS_INDEXRV1,
		CONVERT(char(8), null) as BS_INDEX1,
		CONVERT(char(8), null) as BS_INDEX2,
		CONVERT(char(8), null) as BS_INDEX3,
		CONVERT(float, null) as BS_ALIQDIV,
		CONVERT(float, null) as BS_PERCDEVRV,
		CONVERT(float, null) as BS_PERCDEVFUT,
		CONVERT(char(1), null) as BS_LINEXP,
		CONVERT(char(1), null) as BS_UTCOR,
		CONVERT(char(1), null) as BS_TIPORV,
		CONVERT(float, 0) as BS_FLAGDEL,
		CONVERT(char(1), cli.IsentoIR) as BS_PAGAIRRF,
		CONVERT(char(1), cli.IsentoIOF) as BS_PAGAIOF,
		CONVERT(char(1), 'N') as BS_PAGAIPMF,
		CONVERT(char(1), null) as BS_AJUSTEFUT,
		CONVERT(char(1), null) as BS_CLIRV,
		CONVERT(char(1), null) as BS_SOCIOBMF,
		CONVERT(char(1), null) as BS_FLAGWSAC,
		CONVERT(char(8), null) as BS_INDEXRV2,
		CONVERT(char(1), null) as BS_REFATURAMENTO,
		CONVERT(varchar(20), null) as BS_CC,
		CONVERT(char(7), null) as BS_NO_CC_2,
		CONVERT(char(7), null) as BS_NO_CC_3,
		CONVERT(char(7), null) as BS_NO_CC_4,
		CONVERT(char(12), null) as BS_SWIFT,
		CONVERT(char(6), null) as BS_TELEX,
		CONVERT(varchar(20), null) as BS_FAX,
		CONVERT(varchar(25), null) as BS_CVM,
		CONVERT(char(12), null) as BS_CERTIFICADO_REG,
		CONVERT(char(6), null) as BS_BVRJ,
		CONVERT(char(6), null) as BS_CETIP,
		CONVERT(char(15), null) as BS_ICATUFININVEST,
		CONVERT(char(1), null) as BS_TIPOCOTA,
		CONVERT(char(1), null) as BS_CALCPATUDC,
		CONVERT(float, null) as BS_PC_ADIC_INDEX1,
		CONVERT(float, null) as BS_PC_ADIC_INDEX2,
		CONVERT(float, null) as BS_PC_ADIC_INDEX3,
		CONVERT(char(1), null) as BS_IC_PATR_DIV,
		CONVERT(char(1), null) as BS_IC_BANCO_CENTRAL,
		CONVERT(char(5), null) as BS_CLGIR_CD,
		CONVERT(char(8), null) as BS_CLENQ_CD,
		CONVERT(char(2), null) as BS_IC_PLENQ,
		CONVERT(char(8), null) as BS_CD_INTERFACE,
		CONVERT(datetime, null) as BS_DT_WCRC_LASTPOS,
		CONVERT(char(1), null) as BS_SG_WCRC_FREQUENCIA,
		CONVERT(char(4), null) as BS_CD_WCRC_SENHA,
		CONVERT(char(1), null) as BS_IC_MTM,
		CONVERT(char(1), null) as BS_IC_PROCESSA_CTB,
		CONVERT(char(3), null) as BS_DS_EMPRESA,
		CONVERT(char(10), null) as BS_CD_ANBID,
		CONVERT(char(10), null) as BS_CD_GAZETA,
		CONVERT(datetime, null) as BS_DT_DATA_MINIMA,
		CONVERT(char(8), null) as BS_IDDIR_CD_FUNDO,
		CONVERT(char(3), null) as BS_CD_LIVRO,
		CONVERT(char(3), null) as BS_CD_ENTIDADE,
		CONVERT(char(3), null) as BS_CD_CARTEIRA,
		CONVERT(char(3), null) as BS_CD_ESTRATEGIA,
		CONVERT(char(5), null) as BS_CD_PACOTE,
		CONVERT(char(2), null) as BS_CD_FILIAL,
		CONVERT(char(12), null) as BS_CD_USER_TRADER,
		CONVERT(int, 1) as BS_ID_LOCAL,
		CONVERT(char(10), null) as BS_COD_PERFIL_ONL,
		CONVERT(char(1), null) as BS_IC_RETENCAO,
		CONVERT(char(8), null) as BS_CD_INST,
		CONVERT(char(8), null) as BS_CD_CUSTODIANTE_RV,
		CONVERT(char(8), null) as BS_CD_CUSTODIANTE_FU,
		CONVERT(char(1), null) as BS_IC_LIMITE_CREDITO,
		CONVERT(char(1), null) as BS_IC_DESCONTO_EMOLUMENTO,
		CONVERT(float, null) as BS_IC_DIACOT,
		CONVERT(char(7), null) as BS_NO_CC_1,
		CONVERT(char(1), null) as BS_CD_DV_1,
		CONVERT(char(4), null) as BS_CD_AGENCIA_1,
		CONVERT(char(4), null) as BS_CD_AGENCIA_2,
		CONVERT(char(4), null) as BS_CD_AGENCIA_3,
		CONVERT(char(4), null) as BS_CD_AGENCIA_4,
		CONVERT(char(1), null) as BS_CD_DV_2,
		CONVERT(char(1), null) as BS_CD_DV_3,
		CONVERT(char(1), null) as BS_CD_DV_4,
		CONVERT(char(8), null) as BS_CD_CETIP,
		CONVERT(char(9), null) as BS_CD_SELIC,
		CONVERT(char(1), null) as BS_IC_TIPO_MTM,
		CONVERT(char(1), null) as BS_IC_RV_CUSTO,
		CONVERT(char(1), null) as BS_IC_PAGA_IOF,
		CONVERT(int, null) as BS_IC_RETENCAO_IOF,
		CONVERT(char(1), null) as BS_IC_RV_COTACAO,
		CONVERT(char(1), null) as BS_IC_DEFASAGEM,
		CONVERT(char(8), null) as BS_IDDIR_CD_COMP4,
		CONVERT(float, null) as BS_PC_ADIC_INDEX4,
		CONVERT(int, null) as BS_MM_RV_CUSTO_1,
		CONVERT(int, null) as BS_MM_RV_CUSTO_2,
		CONVERT(char(1), null) as BS_IC_PAGA_IOF_AF,
		CONVERT(char(1), null) as BS_IC_TIPO_FUNDO,
		CONVERT(char(1), null) as BS_IC_PAGA_IOF_RESG,
		CONVERT(int, null) as BS_IC_RETENCAO_IOF_RESG,
		CONVERT(char(1), null) as BS_SG_IOF_RESGATE,
		CONVERT(char(1), null) as BS_IC_PROVISIONA_IOF_RESGATE,
		CONVERT(char(10), null) as BS_CD_PERFIL_CPMF,
		CONVERT(char(1), null) as BS_SG_REFATURAMENTO,
		CONVERT(float, null) as BS_NO_DIAS_CORTE_MTM,
		CONVERT(char(1), null) as BS_IC_CONTA_PROPRIA_SELIC,
		CONVERT(char(12), null) as BS_CD_SWIFT,
		CONVERT(char(6), null) as BS_CD_INF_BOVESPA,
		CONVERT(char(1), null) as BS_IC_RETENCAO_CPMF,
		CONVERT(char(15), null) as BS_CD_COTISTA_YMF,
		CONVERT(char(1), null) as BS_SG_INTERFACE_PASSIVO,
		CONVERT(char(1), null) as BS_IC_ZERA_SALDO_TSR,
		CONVERT(char(35), null) as BS_NM_BCO_EXTERIOR,
		CONVERT(char(35), null) as BS_NO_CC_EXTERIOR,
		CONVERT(char(1), null) as BS_SG_IR_DAYTRADE,
		CONVERT(char(1), null) as BS_IC_PAGA_IRRF_TXADM,
		CONVERT(char(1), null) as BS_IC_ATUALIZA_NETASSET,
		CONVERT(datetime, null) as BS_DT_ENCERRAMENTO,
		CONVERT(char(3), null) as BS_SG_CVM306,
		CONVERT(char(1), 'N') as BS_IC_MAEMOVIMENTO,
		CONVERT(char(1), 'N') as BS_SG_RESIDENTE,
		CONVERT(char(1), null) as BS_IC_TAXA_CDI,
		CONVERT(char(1), null) as BS_IC_TAXA_CDI2,
		CONVERT(char(1), null) as BS_IC_TAXA_CDI3,
		CONVERT(char(1), null) as BS_IC_TAXA_CDI4,
		CONVERT(char(4), null) as BS_CDPATROC,
		CONVERT(char(2), null) as BS_PLAN_BENEF,
		CONVERT(int, null) as BS_COD_EMP,
		CONVERT(char(4), null) as BS_CD_CENTROCUSTO,
		CONVERT(char(60), null) as BS_NM_DIR_EXP_BOOK_XLS,
		CONVERT(char(10), null) as BS_CD_EXTERNO,
		CONVERT(char(1), null) as BS_IC_EFPP,
		CONVERT(char(6), null) as BS_CD_EFPP,
		CONVERT(char(6), null) as BS_CD_PLANO_EFPP,
		CONVERT(char(1), null) as BS_IC_SPC,
		CONVERT(datetime, null) as BS_DT_ENTIDADE_JUR,
		CONVERT(char(4), null) as BS_CD_MICROSEGMTO,
		CONVERT(char(8), null) as BS_CD_IDX_MERCADO,
		CONVERT(char(1), 'F') as BS_IC_CARTEIRA_PROPRIA,
		CONVERT(char(4), null) as BS_CD_BACEN_PESP500,
		CONVERT(char(1), null) as BS_IC_IRRF_IOF_CONTINGENCIA,
		CONVERT(char(8), null) as BS_CD_CONTROL,
		CONVERT(char(1), null) as BS_IC_FLUXO_CAIXA,
		CONVERT(char(10), null) as BS_COD_PERFIL_FLUXO_CX,
		CONVERT(char(1), null) as BS_IC_COLAGEM_PROC,
		CONVERT(char(2), null) as BS_SG_DT_AUTOMATICO,
		CONVERT(char(15), null) as BS_CD_SUSEP,
		CONVERT(char(1), null) as BS_CD_TIPO,
		CONVERT(char(1), null) as BS_IC_DEDUZ_CUSTO_DIVIDENDO,
		CONVERT(datetime, null) as BS_DT_INIC_PREST_SERV,
		CONVERT(char(30), null) as BS_DIR_SFR,
		CONVERT(char(15), null) as BS_CLCLI_CD_ZERA_TSR,
		CONVERT(char(1), null) as BS_IC_CGI,
		CONVERT(char(1), null) as BS_IC_CUSTO_MEDIO_RF,
		CONVERT(char(1), null) as BS_IC_IRRF_IOF_CONTINGENCIAL,
		CONVERT(char(1), null) as BS_IC_SN_INTEGRA_MOV_FUNDO,
		CONVERT(char(8), gE.Nome) as BS_CD_GRUPO_ECONOMICO,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO,
		CONVERT(char(1), null) as BS_CD_TIPO_CONTA,
		CONVERT(float, null) as BS_CX_MINIMO,
		CONVERT(char(5), null) as BS_CD_PONTO_CONTROLE,
		CONVERT(char(1), null) as BS_IC_MGCONCOB,
		CONVERT(char(1), null) as BS_IC_MGGACOB,
		CONVERT(char(10), null) as BS_CD_PERFIL_INVEST,
		CONVERT(char(1), null) as BS_IC_FIFO,
		CONVERT(char(5), null) as BS_CD_GRADE_CONTABIL,
		CONVERT(char(1), null) as BS_IC_EXP_CVM,
		CONVERT(char(1), null) as BS_SG_FATURA_RV,
		CONVERT(char(1), null) as BS_IC_MTM_SCC,
		CONVERT(char(1), null) as BS_IC_CARTEIRA_TVM,
		CONVERT(char(1), null) as BS_IC_NEG_VENC,
		CONVERT(char(8), null) as BS_RVCOR_CD_AGENTE,
		CONVERT(char(1), null) as BS_SG_NEG_VENC,
		CONVERT(char(1), null) as BS_IC_ABERT_INDEX,
		CONVERT(char(1), null) as BS_IC_PAGA_IRRF_TXCUST,
		CONVERT(char(1), null) as BS_SG_LEGAIS,
		CONVERT(char(10), null) as BS_CD_CUSTODIANTE_RF,
		CONVERT(char(1), null) as BS_IC_PERMITE_DAY_TRADE,
		CONVERT(char(1), null) as BS_IC_SN_PERMITIR_SHORT_RV,
		CONVERT(varchar(4), null) as BS_SG_ALTERACAO_NEG_VENC,
		CONVERT(char(1), null) as BS_IC_PAGA_IRRF_TXPFEE,
		CONVERT(char(8), null) as BS_RVCOR_CD_AGENTE_RV,
		CONVERT(char(9), null) as BS_CD_SELIC_BMF,
		CONVERT(char(9), null) as BS_CD_SELIC_BVSP,
		CONVERT(char(9), null) as BS_CD_SELIC_RT,
		CONVERT(char(8), null) as BS_CD_CETIP_RT,
		CONVERT(char(1), null) as BS_IC_MTM_EMPR_ACOES,
		CONVERT(char(15), null) as BS_SG_CURVA_MTM_EMPR_ACOES,
		CONVERT(char(1), null) as BS_SG_PARTICIPANTE_EMPR_ACOES,
		CONVERT(char(1), null) as BS_IC_ZERA_SALDO_TSR_CC,
		CONVERT(char(1), null) as BS_IC_APROP_EXP_AJ_NEG_VCTO,
		CONVERT(char(1), null) as BS_IC_CORRETAGEM_SINACOR_RV,
		CONVERT(char(2), null) as BS_SG_RECOL_IR_RESG_COTAS_CLI,
		CONVERT(char(10), null) as BS_SG_TP_FUNDO_DAIEA,
		CONVERT(char(12), null) as BS_ISIN,
		CONVERT(char(8), null) as BS_CD_BCO_LIQUIDANTE,
		CONVERT(float, null) as BS_CD_CTA_DEP_BMA,
		CONVERT(float, null) as BS_CD_CTA_GARANTIA_BMA,
		CONVERT(char(8), null) as BS_CD_COR_PNA,
		CONVERT(int, null) as BS_CD_TITULAR_BMA,
		CONVERT(char(1), null) as BS_IC_PLC,
		CONVERT(char(25), null) as BS_IC_OPERA_BMA,
		CONVERT(char(1), null) as BS_IC_UTILIZA_CCI,
		CONVERT(char(1), null) as BS_SG_DT_LIQUIDA_TRIBUTO,
		CONVERT(char(5), null) as BS_CD_TIPO_CLASSIFICACAO,
		CONVERT(char(1), null) as BS_SG_CRITERIO_CALCULO_IR,
		CONVERT(char(1), null) as BS_SG_TIPO_RECURSOS_ZERAGEM,
		CONVERT(char(1), null) as BS_IC_LIQ_VIA_INTERF,
		CONVERT(char(1), null) as BS_IC_IMP_RV_TX_SERV,
		CONVERT(char(1), null) as BS_IC_NOTA_APLICACAO,
		CONVERT(char(10), null) as BS_CD_RESPONSAVEL_RF,
		CONVERT(char(10), null) as BS_CD_RESPONSAVEL_RV,
		CONVERT(char(2), null) as BS_CD_RISCO,
		CONVERT(char(1), null) as BS_IC_LANC_IR_RESG,
		CONVERT(char(1), null) as BS_SG_CRITERIO_ZERAGEM_B0,
		CONVERT(char(1), null) as BS_SG_COBRANCA_IR_GANHO_CAP,
		CONVERT(char(1), null) as BS_IC_SN_ULTIMA_COTA_FI,
		CONVERT(char(1), null) as BS_IC_REGIME_CTB,
		CONVERT(char(2), null) as BS_SG_SEGMENTO_CTB,
		CONVERT(char(1), null) as BS_IC_REGISTRO_OFERTA,
		CONVERT(char(1), null) as BS_IC_PROVENTOS_EA,
		CONVERT(int, null) as BS_QT_DIA_CONF_CDA,
		CONVERT(char(1), null) as BS_IC_REPASSE_DIV,
		CONVERT(int, null) as BS_NO_DIAS_REPASSE_DIV,
		CONVERT(char(1), null) as BS_IC_REPASSE_JUROS,
		CONVERT(int, null) as BS_NO_DIAS_REPASSE_JUROS,
		CONVERT(char(1), null) as BS_IC_REPASSE_REND,
		CONVERT(int, null) as BS_NO_DIAS_REPASSE_REND,
		CONVERT(char(1), null) as BS_IC_TES_PROV_EA,
		CONVERT(char(1), null) as BS_IC_PRE_MATCHING_RV,
		CONVERT(char(1), null) as BS_IC_PRE_MATCHING_FU,
		CONVERT(char(1), null) as BS_IC_COL_FC_FI_DN,
		CONVERT(char(1), null) as BS_IC_FIXA_VL_COTA,
		CONVERT(char(1), null) as BS_IC_NEG_FDO_MERC,
		CONVERT(char(1), null) as BS_IC_MSG_SPB,
		CONVERT(char(1), null) as BS_SG_TIPO_PRIORIZ_NOTAS,
		CONVERT(char(1), null) as BS_IC_TRADUZ_SN,
		CONVERT(char(14), null) as BS_CD_MNEMONICO_CETIP,
		CONVERT(char(5), null) as BS_CD_CUSTODIANTE_EUROCLEAR,
		CONVERT(char(1), null) as BS_IC_CONVERTE_POS_REC_SUB,
		CONVERT(char(9), null) as BS_CD_CUSTODIANTE_1,
		CONVERT(char(9), null) as BS_CD_CUSTODIANTE_2,
		CONVERT(char(1), null) as BS_SG_CALC_IRRF_FUTUROS,
		CONVERT(char(1), null) as BS_SG_COBRANCA_IR_GL_FUTUROS,
		CONVERT(char(1), null) as BS_IC_PERMITE_DIVERG_IR,
		CONVERT(char(10), tpFundoCc.ValorCampo)	as	BS_TIPO_FUNDO_COMPL,
		CONVERT(char(1), tpFundoQpCc.ValorCampo) as BS_TIPO_FUNDO_QUAL_PROF,
		CONVERT(varchar(255),cf.Descricao) as BS_CATEGORIA_FUNDO,
		CONVERT(char(8), ind.Descricao) as BS_CD_BENCHMARK,
		CONVERT(char(1), mercCc.ValorCampo) as BS_IC_MERCOSUL,  
		CONVERT(char(1),CASE WHEN EXISTS(SELECT 1 FROM TabelaTaxaPerformance WHERE IdCarteira = CA.IdCarteira) THEN 'S' ELSE 'N' END) as BS_IC_PAGA_PFEE,
		CONVERT(char(15),agGestor.Nome) as BS_CD_GESTOR,
		@DataRef as DataRef
	FROM Carteira ca
	LEFT JOIN Cliente cli on cli.IdCliente = ca.IdCarteira
	LEFT JOIN CategoriaFundo cf on ca.IdCategoria = cf.IdCategoria
	LEFT JOIN Moeda mo on mo.IdMoeda = cli.IdMoeda
	LEFT JOIN AgenteMercado agAdm on agAdm.IdAgente = ca.IdAgenteAdministrador
	LEFT JOIN AgenteMercado agGestor ON ca.IdAgenteGestor = agGestor.IdAgente
	/*MASTER -> FIN_GPS ALTERAR QUANDO FOR TROCAR*/
	LEFT JOIN Pessoa pes on pes.IdPessoa = cli.IdPessoa	
	LEFT JOIN CadastroComplementar tpFundoCc ON tpFundoCc.IdMercadoTipoPessoa = cli.IdCliente
		AND tpFundoCc.IdCamposComplementares = (
				SELECT IdCamposComplementares FROM CadastroComplementarCampos ccc 
					WHERE ccc.NomeCampo = 'TIPO FUNDO COMPL'
						AND ccc.TipoCadastro = -8
			)	
	LEFT JOIN CadastroComplementar tpFundoQpCc ON tpFundoQpCc.IdMercadoTipoPessoa = cli.IdCliente
		AND tpFundoQpCc.IdCamposComplementares = (
				SELECT IdCamposComplementares FROM CadastroComplementarCampos ccc 
					WHERE ccc.NomeCampo = 'TIPO FUNDO QUAL PROF'
						AND ccc.TipoCadastro = -8
			)	
	LEFT JOIN GrupoEconomico gE on 
		ge.IdGrupo = ca.IdGrupoEconomico
	LEFT JOIN Indice ind on 
		ind.IdIndice = ca.IdIndiceBenchmark
	LEFT JOIN CadastroComplementar mercCc ON 
		mercCc.IdMercadoTipoPessoa = ca.IdCarteira AND
		mercCc.IdCamposComplementares = (
			SELECT 
				IdCamposComplementares 
			FROM 
				CadastroComplementarCampos ccc
			WHERE
				ccc.NomeCampo = 'IC MERCOSUL' AND
				ccc.TipoCadastro = -8
		)
	WHERE 
		cli.TipoControle <> 1

		


END
GO

IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_CL_CPR') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_CL_CPR
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_CL_CPR]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(numeric(9), li.IdLiquidacao) as BS_ID,
		CONVERT(datetime, li.DataLancamento) as BS_DT,
		CONVERT(char(15), li.IdCliente) as BS_CLCLI_CD,
		CONVERT(float, li.Valor) as BS_VL,
		CONVERT(varchar(80), li.Descricao) as BS_DS,
		CONVERT(char(3), origem.CodigoExterno) as BS_MTTP_CD,
		CONVERT(char(1), null) as BS_IC_REC_DESP,
		CONVERT(datetime, li.DataVencimento) as BS_DT_LIQUIDACAO,
		CONVERT(varchar(80), null) as BS_HIST_TRADUZIDO,
		CONVERT(char(1), null) as BS_IC_CAIXA,
		CONVERT(char(15), null) as BS_CD_ATIVO,
		CONVERT(char(5), null) as BS_CD_TIPO_DESP_REC,
		CONVERT(int, 0) as BS_ID_PAGAMENTO,
		CONVERT(char(15), null) as BS_CD_ESTRATEGIA_MOV,
		CONVERT(char(8), null) as BS_CD_MOEDA_ORIGEM,
		CONVERT(float, null) as BS_VL_MOEDA_ORIGEM,
		@DataRef as DataRef	
	FROM LiquidacaoHistorico li
	LEFT JOIN cliente cli ON 
		cli.IdCliente = li.IdCliente
	LEFT JOIN DePara origem ON
		Origem.IdTipoDePara = -4 AND 
		origem.CodigoInterno = li.Origem
	WHERE 
		li.DataLancamento <= @DataRef AND 
		li.DataVencimento > @DataRef AND
		li.DataHistorico = @DataRef
END
GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_CL_PATR') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_CL_PATR

GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_CL_PATR]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(datetime,	hc.Data							)	as	BS_DT,
		CONVERT(char(15),	ca.IdCarteira					)	as	BS_CLCLI_CD,
		CONVERT(float,	null							)	as	BS_VL_TOTFUT,
		CONVERT(float,	null							)	as	BS_VL_TOTOE,
		CONVERT(float,	null							)	as	BS_VL_TOTFC,
		CONVERT(float,	null							)	as	BS_VL_TOTFD,
		CONVERT(float,	null							)	as	BS_VL_TOTFI,
		CONVERT(float,	null							)	as	BS_VL_TOTFAF,
		CONVERT(float,	null							)	as	BS_VL_TOTFAFLIQ,
		CONVERT(float,	null							)	as	BS_VL_TOTEM,
		CONVERT(float,	null							)	as	BS_VL_TOTCCTE,
		CONVERT(float,	null							)	as	BS_VL_TOTSW,
		CONVERT(float,	null							)	as	BS_VL_TOTPROVFUT,
		CONVERT(float,	null							)	as	BS_VL_TOTOUTROS,
		CONVERT(float,	null							)	as	BS_VL_TOTDESPESAS,
		CONVERT(float,	null							)	as	BS_VL_COTASEMIT,
		CONVERT(float,	null							)	as	BS_VL_COTASRESG,
		CONVERT(float,	null							)	as	BS_VL_LANCFUTUROS,
		CONVERT(float,	null							)	as	BS_VL_TOTRV,
		CONVERT(float,	hc.PLFechamento					)	as	BS_VL_PATRLIQTOT,
		CONVERT(float,	null							)	as	BS_VL_PATRLIQTOT2,
		CONVERT(float,	null							)	as	BS_VL_TOTRF1,
		CONVERT(float,	null							)	as	BS_VL_TOTRF2,
		CONVERT(float,	null							)	as	BS_VL_TOTOA,
		CONVERT(float,	null							)	as	BS_VL_TOTFUTMERC,
		CONVERT(float,	null							)	as	BS_VL_PATRLIQABERT,
		CONVERT(float,	null							)	as	BS_VL_TOTFUNDOS,
		CONVERT(float,	null							)	as	BS_VL_IRRF_A_COMPENSAR_RV,
		CONVERT(float,	null							)	as	BS_VL_CCTE_ADM,
		CONVERT(float,	null							)	as	BS_VL_CCTE_RES,
		CONVERT(float,	(
			SELECT 
				sum(SaldoFechamento) BS_VL_CCTE_CONSOLIDADO
			FROM SaldoCaixa
			where IdCliente = hc.IdCarteira 
				and Data = @DataRef
			group by IdCliente, Data)						)	as	BS_VL_CCTE_CONSOLIDADO,
		CONVERT(float,	null							)	as	BS_VL_TXADM_ACUM,
		CONVERT(float,	null							)	as	BS_VL_IRRF_A_COMPENSAR_DT,
		CONVERT(float,	null							)	as	BS_VL_TOTRV_RC,
		CONVERT(float,	null							)	as	BS_VL_PATRLIQTOT_RC,
		CONVERT(float,	null							)	as	BS_VL_TXGES_ACUM,
		CONVERT(float,	mm.MediaMovel					)	as	BS_VL_PATRLIQMED
	FROM 
		HistoricoCota hc	
	LEFT JOIN Carteira ca ON 
		ca.IdCarteira = hc.IdCarteira
	LEFT JOIN MediaMovel mm ON
		mm.IdCliente = hc.IdCarteira AND
		mm.DataAtual = hc.Data AND
		mm.TipoMediaMovel = 0
	LEFT JOIN Cliente cli ON
		cli.IdCliente = hc.IdCarteira
	WHERE 		
		hc.Data = @DataRef AND
		cli.TipoControle <>1
END
GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_CL_PATRFUT') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_CL_PATRFUT
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_CL_PATRFUT]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(char(7), mer.CodigoExterno + ' ' + at.CdAtivoBMF) as BS_FUATV_CD,
		CONVERT(datetime, posBmfH.DataHistorico) as BS_DT,
		CONVERT(char(15), cli.IdCliente) as BS_CL_CLI_CD,
		CONVERT(char(6), posBmfH.Serie) as BS_FUVCT_CD,
		CONVERT(char(8), case when ag.Apelido is not null then ag.Apelido else ag.Nome end) as BS_RVCOR_CD,
		CONVERT(float, posBmfH.Quantidade) as BS_QT,
		CONVERT(float, null) as BS_VL_EQUALIZACAO,
		CONVERT(float, null) as BS_VL_VALORIZACAO,
		CONVERT(float, null) as BS_VL_PRECOMERC,
		CONVERT(char(1), 'S') as BS_IC_ATIVO,
		CONVERT(char(1), null) as SG_ALOCACAO,
		CONVERT(float, null) as BS_VL_AJUSTE_TMO,
		CONVERT(float, null) as BS_VL_AJUSTE_SWP,
		CONVERT(float, null) as BS_VL_PROVISAO_SWP,
		CONVERT(float, at.PrecoExercicio) as BS_VL_PRECO_EXERC,
		CONVERT(float, null) as BS_VL_CUSTO_MEDIO,
		CONVERT(float, null) as BS_VL_COTACAO,
		CONVERT(float, null) as BS_VL_PRECO_CORTG,
		CONVERT(float, null) as BS_VL_IRRF
	FROM PosicaoBMFHistorico posBmfH
	LEFT JOIN Cliente cli ON 
		cli.IdCliente = posBmfH.IdCliente
	LEFT JOIN AgenteMercado ag ON 
		ag.IdAgente = posBmfH.IdAgente
	LEFT JOIN AtivoBMF at ON 
		at.CdAtivoBMF = posBmfH.CdAtivoBMF AND 
		at.Serie = posBmfH.Serie
	LEFT JOIN OperacaoBMF op ON	
		op.Origem IN(2,3) AND 
		op.IdCliente = posBmfH.IdCliente AND
		op.CdAtivoBMF = posBmfH.CdAtivoBMF
	LEFT JOIN DePara mer ON
		mer.IdTipoDePara = -20 AND 
		mer.CodigoInterno = at.TipoMercado
	WHERE 
		posBmfH.DataHistorico = @DataRef AND
		cli.TipoControle <> 1
END
GO


IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_CL_PATRRF') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_CL_PATRRF
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_CL_PATRRF]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(char(10),	posRfH.IdOperacao)	as	BS_RFOP_CD,		
		CONVERT(datetime,	posRfH.DataHistorico)	as	BS_DT,
		CONVERT(char(15),	cli.IdCliente)	as	BS_CL_CLI_CD,
		CONVERT(float,	posRfH.TaxaOperacao)	as	BS_VL_TAXA_ANO,
		CONVERT(float,	null)	as	BS_VL_PRAZO,
		CONVERT(char(8),	null)	as	BS_IDDIR_CD,
		CONVERT(char(8),	null)	as	BS_IDDIR_CD_OVER,
		CONVERT(datetime,	null)	as	BS_DT_VENCTO,
		CONVERT(float,	null)	as	BS_VL_PU_PARTIDA,
		CONVERT(float,	posRfH.Quantidade)	as	BS_QT,
		CONVERT(float,	null)	as	BS_VL_FIN_LIQUIDO,
		CONVERT(float,	null)	as	BS_VL_FIN_BRUTO1,
		CONVERT(float,	null)	as	BS_VL_FIN_BRUTO2,
		CONVERT(float,	null)	as	BS_VL_FIN_LIQUIDO2,
		CONVERT(float,	null)	as	BS_VL_IRRF1,
		CONVERT(float,	null)	as	BS_VL_IRRF2,
		CONVERT(float,	null)	as	BS_VL_IOF1,
		CONVERT(float,	null)	as	BS_VL_IOF2
	FROM
	PosicaoRendaFixaHistorico posRfH
	LEFT JOIN Cliente cli on cli.IdCliente = posRfH.IdCliente
	WHERE 
		posRfH.DataHistorico = @DataRef AND
		cli.TipoControle <> 1
END


GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_CL_PATRRV') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_CL_PATRRV
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_CL_PATRRV]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT
		CONVERT(char(12), posBolsaHis.CdAtivoBolsa) as BS_RVPAP_CD,
		CONVERT(datetime, posBolsaHis.DataHistorico) as BS_DT,
		CONVERT(char(15), posBolsaHis.IdCliente) as BS_CL_CLI_CD,
		CONVERT(char(8),ag.Nome ) as BS_RVCOR_CD,
		CONVERT(char(2), 'SP') as BS_CD_PRACA,
		CONVERT(float, posBolsaHis.Quantidade - posBolsaHis.QuantidadeBloqueada) as BS_QT_DISPONIVEL,
		CONVERT(float, posBolsaHis.Quantidade) as BS_QT_TOTAL,
		CONVERT(float, posBolsaHis.ValorMercado) as BS_VL_MERC,
		CONVERT(float, posBolsaHis.ValorCustoLiquido) as BS_VL_CUSTO,
		CONVERT(float, null) as BS_VL_CUSTOCOR,
		CONVERT(float, cot.PUFechamento) as BS_VL_COTACAO,
		CONVERT(float, posBolsaHis.PUCusto) as BS_VL_CUSTO_FISC,
		CONVERT(float, posBolsaHis.PUCusto) as BS_VL_CUSTO_FISCCOR,
		CONVERT(float, null) as BS_VL_IRRF,
		CONVERT(float, null) as BS_VL_MERC_LIQUIDO,
		CONVERT(float, null) as BS_VL_EQUALIZACAO,
		CONVERT(float, null) as BS_VL_VALORIZACAO,
		CONVERT(char(1), null) as SG_ALOCACAO
	FROM PosicaoBolsaHistorico posBolsaHis
		LEFT JOIN AgenteMercado ag on ag.IdAgente = posBolsaHis.IdAgente
		LEFT JOIN CotacaoBolsa cot on cot.CdAtivoBolsa = posBolsaHis.CdAtivoBolsa 
			AND cot.Data = posBolsaHis.DataHistorico
		LEFT JOIN Cliente cli on cli.IdCliente = posBolsaHis.IdCliente
	WHERE 
		posBolsaHis.DataHistorico = @DataRef AND
		cli.TipoControle <> 1

END

GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_CL_PATRSINT') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_CL_PATRSINT
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_CL_PATRSINT]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	select
		CONVERT(char(15)	,	hc.IdCarteira				) as BS_CL_CLI_CD,
		CONVERT(datetime	,	hc.Data						) as BS_DT,
		CONVERT(float	,	null						) as BS_VL_COTAS_EMIT,
		CONVERT(float	,	null						) as BS_VL_COTAS_RESG,
		CONVERT(float	,	null						) as BS_VL_OUTROS,
		CONVERT(float	,	null						) as BS_VL_DESPESAS,
		CONVERT(float	,	null						) as BS_VL_SW,
		CONVERT(float	,	null						) as BS_VL_PROVFUT,
		CONVERT(float	,	hc.PLFechamento				) as BS_VL_PATRLIQTOT1,
		CONVERT(float	,	hc.QuantidadeFechamento		) as BS_QT_COTAS1,
		CONVERT(float	,	hc.CotaFechamento			) as BS_VL_COTA1,
		CONVERT(float	,	null						) as BS_VL_TXADMPR,
		CONVERT(float	,	null						) as BS_VL_PATRLIQTOT2,
		CONVERT(float	,	null						) as BS_QT_COTAS2,
		CONVERT(float	,	null						) as BS_VL_COTA2,
		CONVERT(float	,	null						) as BS_VL_COTAPFEE1,
		CONVERT(float	,	null						) as BS_VL_COTAPFEE2,
		CONVERT(float	,	null						) as BS_VL_LANC_FUTUROS,
		CONVERT(float	,	null						) as BS_QT_COTAS_PENAL,
		CONVERT(float	,	null						) as BS_VL_ENTRADAS,
		CONVERT(float	,	null						) as BS_VL_SAIDAS,
		CONVERT(float	,	null						) as BS_VL_APORTE,
		CONVERT(float	,	null						) as BS_VL_RETIRADA,
		CONVERT(float	,	null						) as BS_VL_APORTE_RF,
		CONVERT(float	,	null						) as BS_VL_RETIRADA_RF,
		CONVERT(float	,	null						) as BS_VL_CUSTODIAPR,
		CONVERT(float	,	null						) as BS_VL_COTA_TX2,
		CONVERT(char(1)		,	null						) as BS_IC_LOG_LIBERADO,
		CONVERT(int			,	null						) as BS_ID_LOG_LIBERADO,
		CONVERT(float		,	mm.MediaMovel				) as BS_MEDIA_MOVEL,
		CONVERT(float		,	pm.PrazoMedio				) as BS_PRZ_MEDIO_PORTFOLIO 
	FROM HistoricoCota hc
	LEFT JOIN MediaMovel mm ON mm.IdCliente = hc.IdCarteira
		AND mm.DataAtual = hc.Data
		AND mm.TipoMediaMovel = 1
	LEFT JOIN PrazoMedio pm ON pm.IdCliente = hc.IdCarteira
		AND pm.DataAtual = hc.Data
		AND pm.MercadoAtivo = 7
	LEFT JOIN Cliente cli ON
		cli.IdCliente = hc.IdCarteira
	WHERE 
		hc.Data = @DataRef AND
		cli.TipoControle <> 1
	    
END
GO




IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_CONTA_CORRENTE') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_CONTA_CORRENTE
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_CONTA_CORRENTE]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN


	SELECT 
		CONVERT(numeric(5), cc.IdConta) AS BS_ID_CONTA,
		CONVERT(char(15), pes.Nome) AS BS_CLCLI_CD,
		CONVERT(char(3), bc.CodigoCompensacao) AS BS_CD_BANCO,
		CONVERT(char(4), ag.Nome) AS BS_CD_AGENCIA,
		CONVERT(char(15), cc.Numero) AS BS_CD_CONTA_CORRENTE,
		CONVERT(char(2), null) AS BS_CD_DAC,
		CONVERT(char(1), null) AS BS_SG_TIPO_CONTA,
		CONVERT(char(1), null) AS BS_IC_LOG_LIBERADO,
		CONVERT(int, null) AS BS_ID_LOG_LIBERADO,
		CONVERT(char(1), null) AS BS_IC_CONTA_INVESTIMENTO,
		CONVERT(char(2), null) AS BS_IC_ID_INT_EXT,
		@DataRef as DataRef
	FROM ContaCorrente cc
	LEFT JOIN pessoa pes ON pes.IdPessoa = cc.IdPessoa
	LEFT JOIN banco bc ON bc.IdBanco = cc.IdBanco
	LEFT JOIN Agencia ag ON ag.IdAgencia = cc.IdAgencia
	WHERE cc.IdBanco IS NOT NULL
END

GO

IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_FI_CAD') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_FI_CAD
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_FI_CAD]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(char(8), cli.IdCliente) as BS_CD,
		CONVERT(char(10), 'OUTROS') as BS_TPFDO_CD,
		CONVERT(varchar(20), ca.Nome) as BS_NM,
		CONVERT(char(8), ca.Apelido) as BS_IDDIR_CD,
		CONVERT(char(8), 'AM' + CAST(ag.IdAgente AS VARCHAR(MAX))) as BS_BAINS_CD,
		CONVERT(int, null) as BS_DD_LIQUID_FIN_ENTRADA,
		CONVERT(int, null) as BS_DD_LIQUID_FIS_ENTRADA,
		CONVERT(int, null) as BS_DD_LIQUID_FIN_SAIDA,
		CONVERT(int, null) as BS_DD_LIQUID_FIS_SAIDA,
		CONVERT(char(8), mo.Nome) as BS_BAMDA_CD,
		CONVERT(int, null) as BS_NO_DECIMAIS_QT,
		CONVERT(int, null) as BS_NO_DECIMAIS_VL,
		CONVERT(float, null) as BS_PC_JUROS,
		CONVERT(char(2), null) as BS_BAGRP_CD,
		CONVERT(char(1), null) as BS_SG_PRODUTO,
		CONVERT(char(2), dp.CodigoExterno) as BS_SG_SEGMENTO,
		CONVERT(char(8), null) as BS_CD_CETIP,
		CONVERT(char(15), null) as BS_CD_CLIENTE_SAC,
		CONVERT(char(1), null) as BS_IC_TIPO_SEGMENTO,
		CONVERT(char(20), null) as BS_SG_LOCAL_CUSTODIA,
		CONVERT(char(1), null) as BS_IC_COME_COTAS,
		CONVERT(float, null) as BS_PC_IRRF,
		CONVERT(char(1), null) as BS_IC_INTERLIGACAO,
		CONVERT(char(5), null) as BS_CD_FM,
		CONVERT(char(1), null) as BS_IC_NOTA_APLICACAO,
		CONVERT(char(1), null) as BS_IC_LIQUIDEZ_DIARIA,
		CONVERT(datetime, null) as BS_DT_CORTE,
		CONVERT(char(3), null) as BS_CD_SFR,
		CONVERT(char(1), null) as BS_IC_ARREDONDA_TRUNCA,
		CONVERT(char(1), null) as BS_IC_IMPORTA_COME_COTAS_COTISTA,
		CONVERT(char(15), null) as BS_CD_FUNDO_COTISTA,
		CONVERT(char(14), null) as BS_CD_MNEMONICO_CETIP,
		CONVERT(char(1), null) as BS_IC_ARREDONDA_TRUNCA_IRRF,
		CONVERT(char(1), null) as BS_IC_FLUXO_DISPONIBILIDADE,
		CONVERT(char(5), null) as BS_CD_TIPO_CLASSIFICACAO,
		CONVERT(char(6), null) as BS_CD_SUSEP,
		CONVERT(char(18), null) as BS_NO_CNPJ,
		CONVERT(char(1), null) as BS_IC_POSICAO_NEGATIVA,
		CONVERT(char(10), null) as BS_CD_ANBID,
		CONVERT(smallint, null) as BS_DD_PRAZO,
		CONVERT(char(2), null) as BS_CD_TIPO_ATIVO,
		CONVERT(char(12), null) as BS_CD_ISIN,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO,
		CONVERT(char(3), null) as BS_CD_SUBSEG_SPC,
		CONVERT(char(8), null) as BS_CD_FUNDO_ANTIGO,
		CONVERT(float, null) as BS_VL_TX_ADM_INF,
		CONVERT(char(1), null) as BS_IC_SN_CORTE_31122001,
		CONVERT(float, null) as BS_PC_IRRF_31122001,
		CONVERT(char(10), null) as BS_CD_CLASS_ANBID,
		CONVERT(char(30), null) as BS_DS_CLASSIF_ANBID,
		CONVERT(char(30), null) as BS_DS_OBJETIVO,
		CONVERT(varchar(250), null) as BS_DS_POL_INVEST,
		CONVERT(char(15), null) as BS_CD_ADMINISTRADOR,
		CONVERT(char(2), null) as BS_SG_SEG_TIR,
		CONVERT(datetime, null) as BS_DT_VIGENCIA,
		CONVERT(char(15),agGestor.Nome) as BS_CD_GESTOR,
		CONVERT(char(15), null) as BS_CD_CUSTODIANTE,
		CONVERT(char(15), null) as BS_CD_PERFIL,
		CONVERT(char(30), null) as BS_NO_CONTA_CORRENTE,
		CONVERT(char(8), null) as BS_CD_MOEDA_VENDIDA,
		CONVERT(char(15), null) as BS_CD_PARIDADE,
		CONVERT(char(1), null) as BS_SG_CONVERTE_COTA_PROC,
		CONVERT(char(15), null) as BS_CD_PARIDADE_AVL_PL,
		CONVERT(char(1), null) as BS_SG_STATUS_FECHAMENTO,
		CONVERT(char(8), null) as BS_CD_LIQUIDANTE,
		CONVERT(datetime, null) as BS_HR_INICIAL_APLICACAO,
		CONVERT(datetime, null) as BS_HR_FINAL_APLICACAO,
		CONVERT(datetime, null) as BS_HR_INICIAL_RESGATE,
		CONVERT(datetime, null) as BS_HR_FINAL_RESGATE,
		CONVERT(float, null) as BS_VL_TX_ADM_ANO,
		CONVERT(datetime, null) as BS_DT_CORTE_MP281,
		CONVERT(char(8), null) as BS_CD_MOEDA_LIQUIDACAO,
		CONVERT(char(2), null) as BS_SG_CONTA_DATA_IOF,
		CONVERT(char(8), null) as BS_CD_BOLSA_NEG,
		CONVERT(char(8), null) as BS_CD_PRACA_NEG,
		CONVERT(char(8), null) as BS_CD_BOLSA,
		CONVERT(int, null) as BS_NO_DIAS_AGENDAMENTO,
		CONVERT(char(1), null) as BS_IC_TIPO_CONTAGEM_DIAS,
		CONVERT(char(1), null) as BS_SG_BASE_DIAS_MTM,
		CONVERT(float, null) as BS_IC_DT_INDEXADOR,
		CONVERT(char(1), null) as BS_IC_NEG_FDO_MERC,
		CONVERT(char(1), null) as BS_IC_CALC_IOF_RESG_CARENCIA,
		CONVERT(char(1), null) as BS_IC_CCI,
		CONVERT(char(3), null) as BS_CD_BANCO,
		CONVERT(char(4), null) as BS_CD_AGENCIA,
		CONVERT(char(15), null) as BS_CD_CONTA,
		CONVERT(char(2), null) as BS_CD_DV,
		CONVERT(char(1), null) as BS_SG_TIPO_PRIORIZ_NOTAS,
		CONVERT(decimal(9), null) as BS_NO_LIQFIS_DEPOSITO,
		CONVERT(decimal(9), null) as BS_NO_LIQFIS_RETIRADA,
		CONVERT(int, null) as BS_NO_DIAS_LIQ_FIN_EVENTO,
		CONVERT(char(1), null) as BS_IC_EVENTO_SN,
		CONVERT(char(1), null) as BS_IC_IMPACTA_POS_FIN_DN,
		CONVERT(datetime, null) as BS_DT_VENCTO,
		CONVERT(char(1), null) as BS_IC_DATA_FIXA,
		CONVERT(char(1), null) as BS_SG_TIPO_DIAS,
		CONVERT(char(2), null) as BS_ID_DATA_FIXA,
		CONVERT(char(1), null) as BS_ID_DIA_SEMANA,
		CONVERT(int, null) as BS_NO_DIAS_ANTEC,
		CONVERT(char(1), null) as BS_SG_TIPO_DIAS_ANTEC,
		CONVERT(float, null) as BS_VL_FINANC_TOTAL_SERIE,
		CONVERT(float, null) as BS_QT_COTAS_TOTAL_SERIE,
		CONVERT(char(1), null) as BS_CD_FORMA_CONDOMINIO,
		CONVERT(char(1), txPerformanceCc.ValorCampo) as BS_APLICA_TX_PERFORMANCE,
		CONVERT(char(8), ratingCc.ValorCampo) as BS_CD_RATING, 
		CONVERT(char(8), grupoEconomicoCc.ValorCampo) as BS_CD_GRUPO_ECONOMICO,
		CONVERT(char(30), tipoFundoCc.ValorCampo) as BS_TIPO_FUNDO_COMPL,	
		CONVERT(char(1),CASE WHEN EXISTS(SELECT 1 FROM TabelaTaxaPerformance WHERE IdCarteira = CA.IdCarteira) THEN 'S' ELSE 'N' END) as BS_IC_PAGA_PFEE,	
		@DataRef as DataRef
	FROM Carteira ca
	LEFT JOIN Cliente cli ON cli.IdCliente = ca.IdCarteira
	LEFT JOIN AgenteMercado agGestor ON ca.IdAgenteGestor = agGestor.IdAgente
	LEFT JOIN Moeda mo ON mo.IdMoeda = cli.IdMoeda
	LEFT JOIN DePara dp ON IdTipoDePara = -8 and dp.CodigoInterno = ca.TipoCarteira
	/*gps -> master --left join Pessoa pe on pe.IdPessoa = cli.IdPessoa */
	LEFT JOIN Pessoa pe ON pe.IdPessoa = cli.IdPessoa
	LEFT JOIN AgenteMercado ag ON ag.IdAgente = pe.IdAgenteDistribuidor
	LEFT JOIN CadastroComplementar txPerformanceCc ON 
		txPerformanceCc.IdMercadoTipoPessoa = cli.IdCliente AND
		txPerformanceCc.IdCamposComplementares = (
				SELECT 
					IdCamposComplementares 
				FROM 
					CadastroComplementarCampos ccc 
				WHERE 
					ccc.NomeCampo = 'APLICA TX PERFORMANCE' AND
					ccc.TipoCadastro = -8
			)	
	LEFT JOIN CadastroComplementar ratingCc ON 
		ratingCc.IdMercadoTipoPessoa = cli.IdCliente AND
		ratingCc.IdCamposComplementares = (
				SELECT 
					IdCamposComplementares 
				FROM 
					CadastroComplementarCampos ccc 
				WHERE 
					ccc.NomeCampo = 'CD RATING' AND
					ccc.TipoCadastro = -8
		)
	LEFT JOIN CadastroComplementar grupoEconomicoCc ON 
		grupoEconomicoCc.IdMercadoTipoPessoa = cli.IdCliente  AND
		grupoEconomicoCc.IdCamposComplementares = (
			SELECT 
				IdCamposComplementares 
			FROM 
				CadastroComplementarCampos ccc 
			WHERE 
				ccc.NomeCampo = 'CD GRUPO ECONOMICO' AND
				ccc.TipoCadastro = -8
		)
	LEFT JOIN CadastroComplementar tipoFundoCc ON 
		tipoFundoCc.IdMercadoTipoPessoa = cli.IdCliente  AND
		tipoFundoCc.IdCamposComplementares = (
			SELECT 
				IdCamposComplementares 
			FROM 
				CadastroComplementarCampos ccc 
			WHERE 
				ccc.NomeCampo = 'TIPO FUNDO COMPL' AND
				ccc.TipoCadastro = -8
		)

	

END


GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_FI_MOV') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_FI_MOV
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_FI_MOV]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT
		CONVERT(int, op.IdOperacao) as BS_ID,
		CONVERT(datetime, op.DataOperacao) as BS_DT,
		CONVERT(char(8), ca.IdCarteira) as BS_FICAD_CD,
		CONVERT(char(15), cli.IdCliente) as BS_CLCLI_CD,
		CONVERT(datetime, op.DataLiquidacao) as BS_DT_LIQUID_FIN,
		CONVERT(datetime, op.DataConversao) as BS_DT_LIQUID_FIS,
		CONVERT(float, op.ValorLiquido) as BS_VL,
		CONVERT(float, op.Quantidade) as BS_QT_COTAS,
		CONVERT(char(1), dp.CodigoInterno) as BS_IC_APLICACAO_RESGATE,
		CONVERT(float, null) as BS_NO_COMANDO,
		CONVERT(char(10), null) as BS_CD_LOCAL_LIQ,
		CONVERT(char(8), null) as BS_CD_LIQUIDANTE,
		CONVERT(char(1), null) as BS_IC_EXPORTADA_IT,
		CONVERT(float, op.ValorIR) as BS_VL_IRRF,
		CONVERT(float, null) as BS_VL_MOVIMENTADO,
		CONVERT(float, null) as BS_VL_IOF_APLICACAO,
		CONVERT(char(20), null) as BS_DS_TIPO_LIQ_FIN,
		CONVERT(varchar(75), null) as BS_DS_HISTORICO,
		CONVERT(float, null) as BS_VL_IOF_COMPENSADO,
		CONVERT(datetime, null) as BS_DT_APLICACAO,
		CONVERT(float, op.ValorIOF) as BS_VL_IOF,
		CONVERT(char(1), null) as BS_IC_ALTERADO,
		CONVERT(char(1), null) as BS_IC_IMPORTADA_IT,
		CONVERT(int, null) as BS_ID_MOVIMENTO_COTISTA,
		CONVERT(char(1), null) as BS_IC_IRRF_VIRTUAL,
		CONVERT(char(1), null) as BS_IC_IOF_RESGATE_VIRTUAL,
		CONVERT(float, op.RendimentoResgate) as BS_VL_RENDIMENTO,
		CONVERT(float, null) as BS_VL_CUSTO,
		CONVERT(datetime, null) as BS_DT_IMPORTACAO,
		CONVERT(char(14), null) as BS_CD_OPER_INTERNO,
		CONVERT(char(9), null) as BS_CD_CONTRAPARTE_CETIP_SELIC,
		CONVERT(char(10), null) as BS_CD_CONTRAPARTE,
		CONVERT(char(15), null) as BS_CD_LOCAL_GARANTIA,
		CONVERT(char(1), null) as BS_IC_NETBOL,
		CONVERT(char(1), null) as BS_SG_ALOCACAO,
		CONVERT(int, null) as BS_ID_MOV_PLANO,
		CONVERT(char(4), null) as BS_CD_SUBSEGTO,
		CONVERT(char(1), null) as BS_IC_TRANSFERENCIA,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO,
		CONVERT(char(1), null) as BS_IC_FQ_RESGATE,
		CONVERT(int, null) as BS_CODTLF,
		CONVERT(char(8), null) as BS_CODCLR,
		CONVERT(datetime, null) as BS_HR_PARTIDA,
		CONVERT(int, null) as BS_NO_PRIORIDADE,
		CONVERT(char(1), null) as BS_CD_PRIORIDADE,
		CONVERT(char(10), null) as BS_CD_MERCADO_EXT,
		CONVERT(char(1), null) as BS_IC_IMPFY,
		CONVERT(char(1), null) as BS_SG_MOD_LIQUID,
		CONVERT(numeric(5), null) as BS_ID_CONTA,
		CONVERT(numeric(5), null) as BS_ID_CONTA_ORIGEM,
		CONVERT(float, op.CotaOperacao) as BS_VL_COTA_CONV,
		CONVERT(datetime, null) as BS_DT_COTA_CONV,
		CONVERT(datetime, null) as BS_DT_INTEGRACAO_NETBOL,
		CONVERT(char(8), null) as BS_CD_BOLSA,
		CONVERT(float, null) as BS_VL_RENDI_TRIBU,
		CONVERT(float, null) as BS_VL_REND_IOF_VIRTUAL,
		CONVERT(float, null) as BS_VL_PREJUIZO_COMPENSADO,
		CONVERT(char(15), null) as BS_CD_ESTRATEGIA_MOV,
		CONVERT(char(1), null) as BS_IC_NEG_FDO_MERC,
		CONVERT(char(1), null) as BS_SG_TIPO_NEG,
		CONVERT(datetime, null) as BS_HR_CONFIRM,
		CONVERT(char(8), null) as BS_CODCOR,
		CONVERT(char(1), null) as BS_IC_CALC_COR,
		CONVERT(char(1), null) as BS_SG_TIPO_COR,
		CONVERT(float, null) as BS_VL_FATOR_COR,
		CONVERT(float, null) as BS_VL_COR,
		CONVERT(char(1), null) as BS_IC_AR_TRUNC_COR_MERC,
		CONVERT(char(1), null) as BS_IC_CALC_EMOL,
		CONVERT(char(1), null) as BS_SG_TIPO_EMOL,
		CONVERT(float, null) as BS_VL_FATOR_EMOL,
		CONVERT(float, null) as BS_VL_EMOL,
		CONVERT(char(1), null) as BS_IC_AR_TRUNC_EMOL_MERC,
		CONVERT(float, null) as BS_VL_COTA_MERC,
		CONVERT(float, null) as BS_VL_COTA_CUSTO,
		CONVERT(int, null) as BS_NO_NOTA,
		CONVERT(float, null) as BS_VL_IOF_CARTEIRA,
		CONVERT(char(1), null) as BS_SG_TIPO_RESGATE,
		CONVERT(decimal(9), null) as BS_ID_SWIFT,
		CONVERT(char(1), null) as BS_IC_EVENTO_AUTOM,
		CONVERT(float, null) as BS_VL_EVENTO_REND,
		CONVERT(float, null) as BS_VL_EVENTO_PRINC,
		CONVERT(datetime, null) as BS_DT_ORIG_DF
	FROM OperacaoFundo op
	LEFT JOIN carteira ca ON 
		ca.IdCarteira = op.IdCarteira
	LEFT JOIN cliente cli ON 
		cli.IdCliente = op.IdCliente
	LEFT JOIN DePara dp ON 
		dp.IdTipoDePara = -6 AND 
		dp.CodigoInterno = op.TipoOperacao
	WHERE 
		op.DataOperacao = @DataRef

END


GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_FI_POS') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_FI_POS
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_FI_POS]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	
	DECLARE @FatorMoeda TABLE
	(
		Moeda float,
		IdCliente int,
		IdCarteira Int
	)
	
	
	INSERT INTO @FatorMoeda	
	SELECT 
		CASE
			WHEN car.IdMoeda = cli.IdMoeda THEN 1
			WHEN cm.Tipo = 1 THEN ci.Valor 
			WHEN ci.Valor <> 0 THEN 1/ci.Valor
			ELSE 0
		END Moeda,
		cli.IdCliente IdCliente,
		car.IdCliente IdCarteira
	FROM			
		Cliente car 
	CROSS JOIN Cliente cli			
	LEFT JOIN ConversaoMoeda cm ON
		cm.IdMoedaDe = car.IdMoeda AND
		cm.IdMoedaPara = cli.IdMoeda
	LEFT JOIN CotacaoIndice ci ON
		ci.IdIndice = cm.IdIndice AND
		ci.Data = @DataRef	
	
	--variaveis utilizadas para verificar se existem problemas
	declare @result varchar(max) = ''	
	declare @cabecalho varchar(max)= ' IdCarteira      IdCliente      ' + CHAR(10)
	
	SELECT
		@result = @result + ' ' + cast(fator.IdCarteira as char(15)) + ' ' + cast(fator.IdCliente as char(15)) + ' ' + CHAR(10)
	FROM 
		@FatorMoeda fator
	INNER JOIN PosicaoFundoHistorico posFundH ON
		posFundH.IdCliente = fator.IdCliente AND
		posFundH.IdCarteira = fator.IdCarteira
	WHERE 		
		Moeda = 0 AND
		posFundH.DataHistorico = @DataRef		
	
	IF(@result <> '')
	BEGIN
		SET @RESULT = @cabecalho + @result
		RAISERROR(@result,1,1)
	END

	

	SELECT
		CONVERT(datetime,posFundH.DataHistorico) as BS_DT,
		CONVERT(char(15), posFundH.IdCliente) as BS_CLCLI_CD,
		CONVERT(char(8), posFundH.IdCarteira) as BS_FICAD_CD,
		CONVERT(datetime, posFundH.DataAplicacao) as BS_DT_APLICACAO,
		CONVERT(float, posFundH.Quantidade) as BS_QT_COTAS,
		CONVERT(float, posFundH.CotaDia) as BS_VL_COTA, --TODO: registros estão vindo com cotadia diferente(importação) rever isto
		CONVERT(float, op.ValorLiquido) as BS_VL_RESG_EMIT,	
		CONVERT(float, posFundH.ValorAplicacao) as BS_VL_PRINCIPAL,
		CONVERT(float, 0) as BS_VL_JUROS,
		CONVERT(float, posFundH.ValorAplicacao*fator.Moeda) as BS_VL_TOTAL,
		CONVERT(float, posFundH.ValorIR*fator.Moeda) as BS_VL_IRRF,
		CONVERT(float,posFundH.ValorLiquido*fator.Moeda) as BS_VL_LIQ,
		CONVERT(float, prej.prejuizoSomado*fator.Moeda) as BS_VL_PREJUIZO_A_COMPENSAR,
		CONVERT(float, op.ValorLiquido*fator.Moeda)  as BS_VL_SALDO_MOVIMENTADO,
		CONVERT(float, posFundH.ValorIOFVirtual*fator.Moeda) as BS_VL_IOF_A_COMPENSAR,
		CONVERT(float, posFundH.ValorIOF*fator.Moeda) as BS_VL_IOF,
		CONVERT(float, posFundH.QuantidadeBloqueada) as BS_QT_BLOQUEADA,
		CONVERT(float, 0) as BS_VL_RENDIMENTO,
		CONVERT(float, 0) as BS_VL_CUSTO,
		CONVERT(char(14), 0) as BS_CD_OPER_INTERNO,
		CONVERT(char(3), 0) as BS_CD_SUBSEG_SPC,
		CONVERT(char(1), 'S') as BS_IC_LOG_LIBERADO,
		CONVERT(int, 0) as BS_ID_LOG_LIBERADO,
		CONVERT(float, posFundH.CotaAplicacao) as BS_VL_COTA_CUSTO,
		CONVERT(float, null) as BS_VL_COTA_CORTE,
		CONVERT(numeric(5), null) as BS_ID_CONTA,
		CONVERT(float, null) as BS_PC_ALIQUOTA_IR,
		CONVERT(float, null) as BS_VL_COTA_ULT_ANIV,
		CONVERT(float, null) as BS_VL_RENDIMENTO_CC,
		CONVERT(datetime, posFundH.DataConversao) as BS_DT_COTA_CONV,
		CONVERT(float, null) as BS_PC_ALIQUOTA_IOF,
		CONVERT(float, 0) as BS_VL_PREJUIZO_COMPENSADO,
		CONVERT(float, 0) as BS_QT_BLOQ_01,
		CONVERT(float, 0) as BS_QT_BLOQ_03,
		CONVERT(float, 0) as BS_QT_BLOQ_05,
		CONVERT(float, 0) as BS_QT_BLOQ_14,
		CONVERT(float, 0) as BS_VL_AJUSTE_PAR_ATUAL_VP,
		CONVERT(float, 0) as BS_VL_PARIDADE_VP,
		CONVERT(float, 0) as BS_VL_AJUSTE_PAR_AVL_PL_VP,
		CONVERT(datetime, null) as BS_DT_ULTIMO_ANIV,
		CONVERT(datetime, null) as BS_DT_PROX_ANIV,
		CONVERT(datetime, null) as BS_DT_ULTIMO_ANIV_COR,
		CONVERT(datetime, null) as BS_DT_PROX_ANIV_COR,
		CONVERT(datetime, null) as BS_DT_ULTIMO_RESGATE_IR,
		CONVERT(datetime, null) as BS_DT_PROX_RESGATE_IR,
		CONVERT(float, null) as BS_VL_COTA_ULTIMO_RESGATE_IR,
		CONVERT(float, pm.PrazoMedio) as BS_PRZ_MEDIO_POSICAO_ATIVO,
		CONVERT(float, mm.MediaMovel) as BS_VL_PATRLIQMED90,		
		CONVERT(int,posFundH.IdPosicao) as BS_CAUTELA
	FROM 
		PosicaoFundoHistorico posFundH
	LEFT JOIN (
		SELECT
			IdCarteira,
			Data,
			sum(ValorPrejuizo) as prejuizoSomado
		FROM 
			PrejuizoCotista
		GROUP BY 
			IdCarteira,
			Data
	) prej ON
		prej.Data = posFundH.DataHistorico AND
		prej.IdCarteira = posFundH.IdCarteira
	LEFT JOIN OperacaoFundo op ON	
		op.IdOperacao = posFundH.IdOperacao	
	LEFT JOIN Cliente cli ON
		cli.IdCliente = posFundH.IdCliente
	LEFT JOIN PrazoMedio pm ON 
		pm.IdCliente = posFundH.IdCliente AND
		pm.DataAtual = posFundH.DataHistorico AND
		pm.MercadoAtivo = 3	AND
		pm.IdPosicao = posFundH.IdPosicao
	LEFT JOIN MediaMovel mm ON
		mm.IdCliente = posFundH.IdCarteira AND
		mm.DataAtual = posFundH.DataHistorico AND
		mm.TipoMediaMovel = 5 AND
		mm.idPosicao = posFundH.IdPosicao	
	 LEFT JOIN @FatorMoeda fator ON
		fator.IdCliente = posFundH.IdCliente AND
		fator.IdCarteira = posFundH.IdCarteira
	WHERE 	
		posFundH.DataHistorico = @DataRef
		
END

GO
--select  12345678.9123456789
--exec usp_EXTRACT_BS_FI_POS '20141212'

--DECLARE @DataRef DATETIME = '20141212'
--select * from PosicaoFundoHistorico posfundh
--LEFT JOIN (
--		SELECT 
--			CASE
--				WHEN car.IdMoeda = cli.IdMoeda THEN 1
--				WHEN cm.Tipo = 1 THEN ci.Valor 
--				WHEN ci.Valor <> 0 THEN 1/ci.Valor
--				ELSE 0
--			END Moeda,
--			cli.IdCliente IdCliente,
--			car.IdCliente IdCarteira
--		FROM			
--			Cliente car 
--		CROSS JOIN Cliente cli			
--		LEFT JOIN ConversaoMoeda cm ON
--			cm.IdMoedaDe = car.IdMoeda AND
--			cm.IdMoedaPara = cli.IdMoeda
--		LEFT JOIN CotacaoIndice ci ON
--			ci.IdIndice = cm.IdIndice AND
--			ci.Data = @DataRef
--	) fator ON
--		fator.IdCliente = posFundH.IdCliente AND
--		fator.IdCarteira = posFundH.IdCarteira
-- where  POSFUNDH.DATAHISTORICO = @DataRef
-- AND MOEDA <> 1 AND ValorIOF <> 0


IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_FU_CAD') 
		AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_FU_CAD
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_FU_CAD]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	DECLARE @result varchar(max)
	SET @result = ''

	--IF EXISTS(
	--	select CdAtivoBMF from (
	--		select CdAtivoBMF from AtivoBMF group by CdAtivoBMF,IdMoeda having count(*) > 1
	--	) abc
	--	group by CdAtivoBMF having count(*) > 1
	--)
	--BEGIN
	--	select @result = CdAtivoBMF +',' from (select CdAtivoBMF from AtivoBMF where TipoMercado = 2 group by CdAtivoBMF,IdMoeda having count(*) > 1) abc 
	--	group by CdAtivoBMF having count(*) > 1
	--	set @result = REVERSE(substring(reverse(@result),charindex(',',reverse(@result))+1,LEN(@result)))  --tira virgula
	--	set @result = 'Atenção erro no cadastro de AtivoBMF existe mais de uma moeda para um mesmo ativo (' + @result + ')'
	--	RAISERROR(@result,15,1)
	--END


	--IF EXISTS(
	--	select CdAtivoBMF from (
	--		select CdAtivoBMF from AtivoBMF group by CdAtivoBMF,Peso having count(*) > 1
	--	) abc 
	--	group by CdAtivoBMF having count(*) > 1
	--)
	--BEGIN
	--	select @result = CdAtivoBMF + ',' from (select CdAtivoBMF from AtivoBMF where TipoMercado= 2 group by CdAtivoBMF,Peso having count(*) > 1) abc 
	--	group by CdAtivoBMF having count(*) > 1
	--	set @result = REVERSE(substring(reverse(@result),charindex(',',reverse(@result))+1,LEN(@result)))  --tira virgula
	--	set @result = 'Atenção erro no cadastro de AtivoBMF existe mais de um peso para um mesmo ativo (' + @result + ')'
	--	RAISERROR(@result,15,1)
	--END



	SELECT DISTINCT
		CONVERT(char(10), mer.CodigoExterno + ' ' + at.CdAtivoBMF) as BS_CD,
		CONVERT(varchar(30), at.CdAtivoBMF) as BS_NM,
		CONVERT(char(3), null) as BS_IC,
		CONVERT(char(8), null) as BS_IDDIR_CD_AJUSTE,
		CONVERT(float, (select top(1) at2.peso from ativobmf at2 where at2.CdAtivoBMF = at.cdAtivobmf)) as BS_VL_VARPONTO,
		CONVERT(float, null) as BS_VL_PU,
		CONVERT(char(8), mo.Nome) as BS_BAMDA_CD,
		CONVERT(char(2), null) as BS_SG_VCTO_EMOL1,
		CONVERT(float, null) as BS_PC_EMOL1,
		CONVERT(char(2), null) as BS_SG_VCTO_EMOL2,
		CONVERT(float, null) as BS_PC_EMOL2,
		CONVERT(char(2), null) as BS_SG_VCTO_EMOL3,
		CONVERT(float, null) as BS_PC_EMOL3,
		CONVERT(float, null) as BS_VL_VCTO_ABERTO,
		CONVERT(float, null) as BS_VL_VCTO_NEGOCIADO,
		CONVERT(char(7), null) as BS_CD_BMF,
		CONVERT(float, null) as BS_NO_DIAS_EMOL,
		CONVERT(char(2), null) as BS_SG_SEGMENTO,
		CONVERT(char(8), null) as BS_IDDIR_CD_CAMBIO,
		CONVERT(float, null) as BS_IC_BASE_CAMBIO,
		CONVERT(char(8), null) as BS_IDDIR_CD_ALAV,
		CONVERT(char(7), null) as BS_CD_OBJETO_OPCAO,
		CONVERT(char(3), null) as BS_CD_SFR,
		CONVERT(char(8), null) as BS_IDDIR_CD_ALAV2,
		CONVERT(char(1), null) as BS_IC_TAXA_DDI,
		CONVERT(float, null) as BS_VL_MULT_RISK,
		CONVERT(char(1), null) as BS_IC_FORWARD,
		CONVERT(char(1), null) as BS_SG_CALCULA_MTM,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO,
		CONVERT(char(3), null) as BS_CD_SUBSEG_SPC,
		CONVERT(char(8), null) as BS_IDDIR_CD_CAMBIO_COR,
		CONVERT(float, null) as BS_IC_BASE_CAMBIO_COR,
		CONVERT(char(1), null) as BS_SG_TIPOOPC,
		CONVERT(char(8), null) as BS_IDDIR_CD_AJUSTE2,
		CONVERT(char(3), null) as BS_SG_TIPO_ATIVO,
		CONVERT(float, null) as BS_VL_EMOL_MIN,
		CONVERT(char(8), null) as BS_CD_MOEDA_EMOL_MIN,
		CONVERT(float, null) as BS_VL_EMOL_MAX,
		CONVERT(char(8), null) as BS_CD_MOEDA_EMOL_MAX,
		CONVERT(float, null) as BS_VL_EMOL_MIN_DT,
		CONVERT(char(8), null) as BS_CD_MOEDA_EMOL_MIN_DT,
		CONVERT(float, null) as BS_VL_EMOL_MAX_DT,
		CONVERT(char(8), null) as BS_CD_MOEDA_EMOL_MAX_DT,
		CONVERT(float, null) as BS_VL_LOTECOT,
		CONVERT(char(8), null) as BS_CD_BOLSA,
		CONVERT(int, null) as BS_NO_DIAS_LIQ_AJUSTE,
		CONVERT(char(1), null) as BS_SG_PERIODO_AJUSTE,
		CONVERT(char(1), null) as BS_SG_BASE_DIAS_MATURITY,
		CONVERT(char(1), null) as BS_SG_DURATION,
		CONVERT(char(1), null) as BS_IC_CALC_EMOLUMENTO,
		CONVERT(float, null) as BS_VL_PRECO_EXERC,
		CONVERT(char(1), null) as BS_IC_DESDOBRA_OPER_MOV,
		CONVERT(char(7), null) as BS_CD_OBJETO_OPCAO2,
		CONVERT(varchar(80), null) as BS_NM_ATIVO_INGLES,
		CONVERT(char(12), null) as BS_CD_CUSIP,
		CONVERT(char(1), null) as BS_IC_PERIOD_CORR_IDX_DESC,
		CONVERT(char(1), null) as BS_IC_MINICONTRATO,
		CONVERT(char(7), null) as BS_CD_ATIVO_RELACIONADO,
		CONVERT(char(21), null) as BS_CD_ATIVO_EXTERNO,
		CONVERT(char(1), garantiaCc.ValorCampo) as BS_IC_GARANTIA,
		CONVERT(char(20), localCutodiaCc.ValorCampo) as BS_SG_LOCAL_CUSTODIA,
		CONVERT(char(8), grupoEconomicoCc.ValorCampo) as BS_CD_GRUPO_ECONOMICO,
		CONVERT(char(1), hedge.ValorCampo) as BS_IC_HEDGE,
		@DataRef as DataRef
	FROM AtivoBMF at	
	LEFT JOIN Moeda mo ON mo.IdMoeda = at.IdMoeda
	LEFT JOIN CadastroComplementar garantiaCc ON garantiaCc.IdMercadoTipoPessoa = (at.CdAtivoBMF +'-' +at.Serie)
		AND garantiaCc.IdCamposComplementares = (
				SELECT IdCamposComplementares FROM CadastroComplementarCampos ccc 
					WHERE ccc.NomeCampo = 'IC GARANTIA'
						AND ccc.TipoCadastro = -11
			)
	LEFT JOIN CadastroComplementar localCutodiaCc ON localCutodiaCc.IdMercadoTipoPessoa = (at.CdAtivoBMF+'-'+at.Serie)
		AND localCutodiaCc.IdCamposComplementares = (
				SELECT IdCamposComplementares FROM CadastroComplementarCampos ccc 
					WHERE ccc.NomeCampo = 'LOCAL CUSTODIA'
						AND ccc.TipoCadastro = -11
			)	
	LEFT JOIN CadastroComplementar grupoEconomicoCc ON grupoEconomicoCc.IdMercadoTipoPessoa = (at.CdAtivoBMF+'-'+at.Serie)
		AND grupoEconomicoCc.IdCamposComplementares = (
				SELECT IdCamposComplementares FROM CadastroComplementarCampos ccc 
					WHERE ccc.NomeCampo = 'CD GRUPO ECONOMICO'
						AND ccc.TipoCadastro = -11
			)
	LEFT JOIN CadastroComplementar hedge ON
		hedge.IdCamposComplementares = ( 
			SELECT 
				IdCamposComplementares
			FROM
				CadastroComplementarCampos ccc
			WHERE
			    ccc.NomeCampo = 'IC_HEDGE' AND
				ccc.TipoCadastro = -11
		)
	LEFT JOIN DePara mer ON
		mer.IdTipoDePara = -20 AND 
		mer.CodigoInterno = at.TipoMercado
	WHERE at.TipoMercado = 2
	
	

 --FUT AUD, Dec 12 2014 12:00AM).

END

GO
--EXEC [usp_EXTRACT_BS_FU_CAD] '20141212'

IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_FU_COT') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_FU_COT
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_FU_COT]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(char(7),	mer.CodigoExterno +' '+cotBmf.CdAtivoBMF)	as	BS_FUVCT_FUATV_CD,
		CONVERT(char(6),	cotBmf.Serie)	as	BS_FUVCT_CD,
		CONVERT(datetime,	cotBmf.Data)	as	BS_DT,
		CONVERT(float,	cotBmf.PUFechamento)	as	BS_VL_PRECO,
		CONVERT(float,	null)	as	BS_VL_AJUSTE_DIA_ANTERIOR,
		CONVERT(char(1),	null)	as	BS_SG_ORIGEM,
		CONVERT(char(1),	null)	as	BS_IC_LOG_LIBERADO,
		CONVERT(int,		null)	as	BS_ID_LOG_LIBERADO
	FROM
		CotacaoBMF cotBmf
	LEFT JOIN AtivoBMF at ON
		at.CdAtivoBMF = cotBmf.CdAtivoBMF AND
		at.Serie = cotBmf.Serie	
	LEFT JOIN DePara mer ON
		mer.IdTipoDePara = -20 AND 
		mer.CodigoInterno = at.TipoMercado	
	WHERE
		cotBmf.Data = @DataRef
END

GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_FU_MOV') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_FU_MOV
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_FU_MOV]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT 
		CONVERT(int, op.IdOperacao) as BS_ID_ORDEM,
		CONVERT(datetime, op.Data) as BS_DT,
		CONVERT(char(15), cli.IdCliente) as BS_CLCLI_CD,
		CONVERT(char(8), ' ') as BS_OECAD_CD,
		CONVERT(char(7), mer.CodigoExterno+' '+op.CdAtivoBMF) as BS_FUATV_CD,
		CONVERT(char(6), op.Serie) as BS_FUVCT_CD,
		CONVERT(char(8), null) as BS_RVCOR_CD,
		CONVERT(datetime, op.Data) as BS_DT_DIGITACAO,
		CONVERT(float, op.Quantidade) as BS_QT,
		CONVERT(char(1), op.TipoOperacao) as BS_IC_CV,
		CONVERT(float, op.PU) as BS_VL_PRECO,
		CONVERT(float, op.Ajuste) as BS_VL_EQUALIZACAO,
		CONVERT(char(1), null) as BS_IC_CORTG,
		CONVERT(float, null) as BS_VL_CORTG_INT_CORR,
		CONVERT(float, null) as BS_VL_DEV_CORR_BANCO,
		CONVERT(float, null) as BS_VL_CORTG_INT_BANCO,
		CONVERT(float, null) as BS_VL_DEV_CORTG_CLI,
		CONVERT(float, op.Corretagem) as BS_VL_CORTG_CLI,
		CONVERT(float, null) as BS_PC_DEV_CORTG,
		CONVERT(char(7), null) as BS_FUATV_CD_ORIG,
		CONVERT(char(6), null) as BS_FUVCT_CD_ORIG,
		CONVERT(char(1), 'N') as BS_IC_DAYTRADE,
		CONVERT(char(1), null) as BS_IC_IMPORTADA,
		CONVERT(float, op.Emolumento) as BS_VL_EMOLUMENTO,
		CONVERT(float, op.Registro) as BS_VL_TAXA_REGISTRO,
		CONVERT(char(6), null) as BS_CD_ORDEM,
		CONVERT(float, null) as BS_VL_SEQUENCIA,
		CONVERT(char(7), null) as BS_CD_NEGOC,
		CONVERT(char(2), null) as BS_CD_FILIAL,
		CONVERT(char(3), null) as BS_CD_PRODUTO,
		CONVERT(char(5), null) as BS_CD_CT_RESULTADO,
		CONVERT(char(12), null) as BS_CD_USER_TRADER,
		CONVERT(char(3), null) as BS_CD_LIVRO,
		CONVERT(char(3), null) as BS_CD_CARTEIRA,
		CONVERT(char(3), null) as BS_CD_ENTIDADE,
		CONVERT(char(3), null) as BS_CD_MERCADO,
		CONVERT(char(3), null) as BS_CD_ATIVO,
		CONVERT(char(4), null) as BS_CD_VENC_SERIE,
		CONVERT(char(5), null) as BS_CD_PACOTE,
		CONVERT(char(3), null) as BS_CD_ESTRATEGIA,
		CONVERT(char(8), null) as BS_CD_CLIENTE_ING,
		CONVERT(char(8), null) as BS_CD_CORR_ING,
		CONVERT(float, null) as BS_VL_PERCENT_RISCO,
		CONVERT(char(1), null) as BS_ST_OPER_ESPECIAL,
		CONVERT(datetime, null) as BS_DT_NEGOC,
		CONVERT(char(3), null) as BS_CD_ENTIDADE_RISCO,
		CONVERT(char(1), null) as BS_ST_RISCO_PAIS,
		CONVERT(char(1), null) as BS_ST_RISCO_TRANSFER,
		CONVERT(char(1), null) as BS_TP_OPERACAO,
		CONVERT(varchar(16), null) as BS_CD_MOEDA,
		CONVERT(char(5), null) as BS_CD_CORR,
		CONVERT(float, null) as BS_VL_COTACAO,
		CONVERT(float, null) as BS_VL_TAMANHO,
		CONVERT(char(1), null) as BS_IC_TRANSF_CUSTODIA,
		CONVERT(float, null) as BS_QT_SALDO,
		CONVERT(float, null) as BS_PC_DEVOL_CORRET_IMP,
		CONVERT(char(1), null) as BS_IC_EXPORTADA_IT,
		CONVERT(char(1), null) as BS_IC_MOV_EXERCICIO,
		CONVERT(float, op.ValorLiquido) as BS_VL_FINANCEIRO,
		CONVERT(float, null) as BS_VL_IR_DAYTRADE,
		CONVERT(char(1), null) as BS_IC_IRRF_VIRTUAL,
		CONVERT(char(1), null) as BS_IC_MOV_LIQ_DIL,
		CONVERT(char(14), null) as BS_CD_OPER_INTERNO,
		CONVERT(float, null) as BS_VL_TAXA,
		CONVERT(char(1), null) as BS_IC_CV_FORWARD,
		CONVERT(char(1), null) as BS_IC_MOV_AUTOMATICO,
		CONVERT(char(10), null) as BS_NO_NOTA,
		CONVERT(char(1), null) as BS_SG_ALOCACAO,
		CONVERT(int, null) as BS_ID_MOV_PLANO,
		CONVERT(char(4), null) as BS_CD_SUBSEGMENTO,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO,
		CONVERT(char(3), null) as BS_CD_SUBSEG_SPC,
		CONVERT(char(1), null) as BS_IC_NETBOL,
		CONVERT(char(1), null) as BS_IC_STRADLE,
		CONVERT(char(8), null) as BS_BALOCUST_CD_CODLCC,
		CONVERT(char(1), null) as BS_IC_PONTA_FRC,
		CONVERT(char(8), null) as BS_BACLR_CD_CODCLR,
		CONVERT(char(8), null) as BS_CD_AG_COMP,
		CONVERT(datetime, null) as BS_DT_DIV,
		CONVERT(char(8), null) as BS_RVCOR_CD_AGENTE,
		CONVERT(char(10), null) as BS_CD_MERCADO_EXT,
		CONVERT(int, null) as BS_ID_ORDEM_ORIGEM,
		CONVERT(char(7), null) as BS_CD_NEGOCIO_BMF,
		CONVERT(float, null) as BS_VL_COR_LANC,
		CONVERT(char(1), null) as BS_SG_TIPO_COR_LANC,
		CONVERT(char(1), null) as BS_IC_TRUNCA_ARREDONDA_COR_LANC,
		CONVERT(char(1), null) as BS_IC_EMOL,
		CONVERT(float, null) as BS_VL_EMOL_LANC,
		CONVERT(char(1), null) as BS_SG_TIPO_EMOL_LANC,
		CONVERT(char(1), null) as BS_IC_TRUNCA_ARREDONDA_EMOL_LANC,
		CONVERT(float, null) as BS_VL_DELTA,
		CONVERT(char(1), null) as BS_IC_ALTERADA,
		CONVERT(char(1), null) as BS_SG_TIPO_DEV_CORTG_LANC,
		CONVERT(char(1), null) as BS_IC_MOV_DESDOBRADO,
		CONVERT(float, null) as BS_VL_PRECO_PONTA,
		CONVERT(float, null) as BS_VL_PRECO_PONTA_LONGA,
		CONVERT(datetime, null) as BS_DT_ALTERACAO_VL_OPER_ESTRUT,
		CONVERT(char(1), null) as BS_SG_ORIGEM_VL_OPER_ESTRUT,
		CONVERT(char(8), null) as BS_RVCOR_CD_CUSTODIANTE,
		CONVERT(char(1), null) as BS_IC_MOP,
		CONVERT(char(15), null) as BS_CD_ESTRATEGIA_MOV,
		CONVERT(char(1), null) as BS_IC_AFTER_HOURS,
		CONVERT(decimal(9), null) as BS_ID_SWIFT,
		CONVERT(char(1), 'N') as BS_IC_DAYTRADE_GL,
		CONVERT(float, null) as BS_VL_IRRF_ON,
		CONVERT(char(1), null) as BS_IC_DMA
	FROM OperacaoBMF op
	LEFT JOIN cliente cli ON 
		cli.IdCliente = op.IdCliente
	LEFT JOIN DePara mer ON
		mer.IdTipoDePara = -20 AND 
		mer.CodigoInterno = op.TipoMercado	
	WHERE 
		op.Data = @DataRef
END


GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_FU_POS') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_FU_POS
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_FU_POS]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(char(15), posBmfH.IdCliente) as BS_CLCLI_CD,
		CONVERT(datetime, posBmfH.DataHistorico) as BS_DT,
		CONVERT(char(6), posBmfH.Serie) as BS_FUVCT_CD,
		CONVERT(char(7), mer.CodigoExterno+' '+posBmfH.CdAtivoBMF) as BS_FUVCT_FUATV_CD,
		CONVERT(char(8), ' ') as BS_OECAD_CD,
		CONVERT(char(8), ag.Nome) as BS_RVCOR_CD,
		CONVERT(float, posBmfH.Quantidade) as BS_QT,
		CONVERT(int, null) as BS_SEQ_EXP,
		CONVERT(float, null) as BS_VL_EQUALIZACAO,
		CONVERT(float, posBmfH.PUCusto) as BS_VL_PRECO,
		CONVERT(float, posBmfH.PUCusto) as BS_VL_PRECO_CLI,
		CONVERT(float, posBmfH.PUCusto) as BS_VL_PRECO_CORTG,
		CONVERT(float, posBmfH.PUCusto) as BS_VL_PRECO_CORTG_CLI,
		CONVERT(float, posBmfH.PUCusto) as BS_VL_PRECO_CORTG_INT,
		CONVERT(float, posBmfH.PUCusto) as BS_VL_PRECO_INT,
		CONVERT(float, posBmfH.PUMercado) as BS_VL_PRECOMERC,
		CONVERT(float, posBmfH.ValorMercado - posBmfH.ValorCustoLiquido) as BS_VL_VALORIZACAO,
		CONVERT(char(1), 'S') as BS_IC_TRANSF_CUSTODIA,
		CONVERT(numeric(9), 0) as BS_VL_PONTA_PRE,
		CONVERT(numeric(9), 0) as BS_VL_PONTA_DI,
		CONVERT(numeric(9), 0) as BS_VL_PONTA_PRE_DESC,
		CONVERT(numeric(9), 0) as BS_VL_AJUSTE_A_LIQUIDAR,
		CONVERT(numeric(9), 0) as BS_VL_PONTA_DI_CORRIGIDA,
		CONVERT(numeric(9), 0) as BS_VL_APROP_IRRF,
		CONVERT(numeric(9), 0) as BS_VL_AJUSTE_LIQUIDO,
		CONVERT(int, null) as BS_DD_UTIL,
		CONVERT(char(1), null) as BS_SG_ALOCACAO,
		CONVERT(float, null) as BS_VL_PONTA_FINAL,
		CONVERT(float, null) as BS_VL_PONTA_CUPOM,
		CONVERT(float, null) as BS_VL_PONTA_FINAL_CONV,
		CONVERT(float, null) as BS_VL_PONTA_CUPOM_CONV,
		CONVERT(float, null) as BS_VL_IRRF,
		CONVERT(float, 1) as BS_PRZ_MEDIO_POSICAO_ATIVO,
		CONVERT(float ,null) as BS_VL_PATRLIQMED90
	FROM PosicaoBMFHistorico posBmfH
	LEFT JOIN AgenteMercado ag on ag.IdAgente = posBmfH.IdAgente
	LEFT JOIN Cliente cli on cli.IdCliente = posBmfH.IdCliente
	LEFT JOIN MediaMovel mm ON mm.IdCliente = posBmfH.IdCliente	AND
		mm.DataAtual = posBmfH.DataHistorico AND 
		mm.TipoMediaMovel = 3 AND 
		mm.ChaveComposta = posBmfH.CdAtivoBMF+ posBmfH.Serie + CAST(posBmfH.IdAgente AS VARCHAR(MAX))
	LEFT JOIN DePara mer ON
		mer.IdTipoDePara = -20 AND 
		mer.CodigoInterno = posBmfH.TipoMercado	
	--	left join OperacaoBMF op on 
	--	op.IdCliente = posBmfH.IdCliente and
	--	op.IdAgenteLiquidacao
	WHERE posBmfH.DataHistorico = @DataRef
END


GO

IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_FU_VCTO') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_FU_VCTO
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_FU_VCTO]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT
		CONVERT(char(10),	mer.CodigoExterno+' '+at.CdAtivoBMF)		as	BS_FUATV_CD,
		CONVERT(char(6),	at.Serie)											as	BS_CD,
		CONVERT(datetime,	at.DataVencimento)									as	BS_DT,
		CONVERT(float,	null)												as	BS_PC_CORNORMAL,
		CONVERT(float,	null)												as	BS_PC_DAYTRADE,
		CONVERT(float,	null)												as	BS_PC_EMOL,
		CONVERT(char(1),	null)												as	BS_IC_COR_FDA,
		CONVERT(char(1),	null)												as	BS_IC_COR_1VA,
		CONVERT(char(1),	null)												as	BS_IC_TX_REG,
		CONVERT(float,	null)												as	BS_VL_CORFIXA_NORMAL,
		CONVERT(float,	null)												as	BS_VL_CORFIXA_DAYTRADE,
		CONVERT(float,	null)												as	BS_PC_EMOL_ADIC,
		CONVERT(float,	null)												as	BS_VL_TX_REG,
		CONVERT(float,	null)												as	BS_PC_EMOL_DAYTRADE,
		CONVERT(char(8),	null)												as	BS_CD_BROADCAST,
		CONVERT(float,	null)												as	BS_PC_CORVCTO,
		CONVERT(char(1),	null)												as	BS_IC_CORVCTO_FDA,
		CONVERT(char(1),	null)												as	BS_IC_CORVCTO_1VA,
		CONVERT(char(1),	null)												as	BS_IC_BASEEMOL,
		CONVERT(float,	null)												as	BS_PC_CORNORMAL_EXERC,
		CONVERT(float,	null)												as	BS_PC_CORCASADO_EXERC,
		CONVERT(char(1),	null)												as	BS_IC_COR_FDA_EXERC,
		CONVERT(char(1),	null)												as	BS_IC_COR_1VA_EXERC,
		CONVERT(float,	at.PrecoExercicio)									as	BS_VL_PRECO_EXERC,
		CONVERT(char(1),	at.TipoSerie)										as	BS_SG_TIPOOPC,
		CONVERT(char(1),	null)												as	BS_IC_MODELO_OPCAO,
		CONVERT(float,	null)												as	BS_VL_DELTA,
		CONVERT(char(8),	null)												as	BS_BAMDA_CD_TXREG,
		CONVERT(char(12),	null)												as	BS_CD_ISIN,
		CONVERT(char(1),	null)												as	BS_IC_LOG_LIBERADO,
		CONVERT(int,		null)												as	BS_ID_LOG_LIBERADO,
		CONVERT(float,	null)												as	BS_VL_EMOL_MIN,
		CONVERT(char(8),	null)												as	BS_CD_MOEDA_EMOL_MIN,
		CONVERT(float,	null)												as	BS_VL_EMOL_MAX,
		CONVERT(char(8),	null)												as	BS_CD_MOEDA_EMOL_MAX,
		CONVERT(float,	null)												as	BS_VL_EMOL_MIN_DT,
		CONVERT(char(8),	null)												as	BS_CD_MOEDA_EMOL_MIN_DT,
		CONVERT(float,	null)												as	BS_VL_EMOL_MAX_DT,
		CONVERT(char(8),	null)												as	BS_CD_MOEDA_EMOL_MAX_DT,
		CONVERT(datetime,	null)												as	BS_DT_VIGENCIA,
		CONVERT(float,	null)												as	BS_VL_TX_REG_PRIMARIO_SWAP,
		CONVERT(float,	null)												as	BS_VL_TX_LIQ_SWAP,
		CONVERT(float,	null)												as	BS_VL_TX_LEILAO_SWAP,
		CONVERT(char(8),	null)												as	BS_CD_MOEDA_TAXAS_SWAP,
		CONVERT(char(7),	null)												as	BS_CD_ATIVO_TOB_MIN_SWAP,
		CONVERT(char(7),	null)												as	BS_CD_ATIVO_TOB_MAX_SWAP,
		CONVERT(datetime,	null)												as	BS_DT_LIMITE_NEGOCIACAO,
		CONVERT(char(6),	null)												as	BS_FUVCT_CD_ANT,
		CONVERT(char(6),	null)												as	BS_FUVCT_CD_OBJETO_OPCAO,
		@DataRef as DataRef
	FROM
		AtivoBMF at
	LEFT JOIN DePara mer ON
		mer.IdTipoDePara = -20 AND 
		mer.CodigoInterno = at.TipoMercado

END

GO

IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_INDEX_COT') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_INDEX_COT
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_INDEX_COT]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT 
		CONVERT(char(8)		,	ind.IdIndice			)	as	BS_CODINDEX,
		CONVERT(datetime	,	cotInd.Data				)	as	BS_DATA,
		CONVERT(float	,	cotInd.Valor			)	as	BS_VALOR,
		CONVERT(float	,	cotInd.FatorAcumulado	)	as	BS_FATORACU,
		CONVERT(float	,	null					)	as	BS_VALORACU,
		CONVERT(int			,	null					)	as	BS_DD_PRAZO,
		CONVERT(int			,	null					)	as	BS_BASE,
		CONVERT(char(1)		,	null					)	as	BS_IC_LOG_LIBERADO,
		CONVERT(int			,	null					)	as	BS_ID_LOG_LIBERADO,
		CONVERT(float	,	null					)	as	BS_VL_PL,
		CONVERT(datetime	,	null					)	as	BS_DT_DIVULGACAO
	FROM
		CotacaoIndice cotInd
	LEFT JOIN Indice ind ON 
		cotInd.IdIndice	= ind.IdIndice
	WHERE 
		cotInd.Data = @DataRef
END

GO

IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_MOEDA') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_MOEDA
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_MOEDA]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
select
CONVERT(char(8), mo.IdMoeda) as BS_CODMOEDA,
CONVERT(char(15), mo.Nome) as BS_NOMEMOEDA,
CONVERT(char(5), null) as BS_SIMBOLO,
CONVERT(char(5), mo.Nome) as BS_CD_INTERNACIONAL,
CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
CONVERT(int, null) as BS_ID_LOG_LIBERADO,
@DataRef as DataRef
from Moeda mo
END

GO

IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_PARAM') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_PARAM
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_PARAM]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT TOP(1) @DataRef as BS_DATAHOJE
END

GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RF_LASTRO') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RF_LASTRO
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RF_LASTRO]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(char(10), titRf.IdTitulo) as BS_CD,
		CONVERT(char(2), null) as BS_BAGRP_CD,
		CONVERT(char(10), 'EM'+CAST(emi.IdEmissor AS VARCHAR(MAX))) as BS_BAINS_CD,
		CONVERT(char(5), pprf.IdPapel) as BS_RFTP_CD,
		CONVERT(char(8), CASE WHEN ind.Descricao IS NOT NULL THEN ind.Descricao ELSE 'PRE' END) as BS_IDDIR_CD,
		CONVERT(char(8), mo.Nome) as BS_BAMDA_CD,
		CONVERT(datetime, titRf.DataVencimento) as BS_DT_VENCIMENTO,
		CONVERT(datetime, titRf.DataEmissao) as BS_DT_EMISSAO,
		CONVERT(float, titRf.ValorNominal) as BS_VL_PU_EMISSAO,
		CONVERT(float, titRf.Taxa) as BS_VL_JUROS_EMISSAO,
		CONVERT(char(10), null) as BS_CD_SELIC,
		CONVERT(char(1), 'N') as BS_IC_ADLIC,
		CONVERT(char(1), null) as BS_IC_ATIVO_PASSIVO,
		CONVERT(char(1), null) as BS_SG_MTM,
		CONVERT(varchar(20), titRf.Descricao) as BS_DS_DESCRICAO,
		CONVERT(char(1), null) as BS_SG_TIPO_CAIXA,
		CONVERT(float, null) as BS_PC_MTM,
		CONVERT(float, null) as BS_VL_PU_FIR,
		CONVERT(char(10), null) as BS_CD_CUST_LIQ_FIN,
		CONVERT(char(8), null) as BS_IDDIR_CD_2,
		CONVERT(char(4), null) as BS_CD_CART_RURAL,
		CONVERT(char(20), null) as BS_CD_CETIP_SELIC,
		CONVERT(float, null) as BS_PU_VENCIMENTO,
		CONVERT(char(1), 'D') as BS_IC_TAXA_CDI,
		CONVERT(float,( 
							case 
								when titRf.IdIndice =1 
								then titRf.Percentual 
								else 100 
							end
						 ) ) as BS_IDDIR_PC,
		CONVERT(char(1), null) as BS_IC_TAXA_CDI2,
		CONVERT(float, null) as BS_IDDIR_PC2,
		CONVERT(char(1), null) as BS_IC_FORCA_CALCULO_PADRONIZADO,
		CONVERT(char(1), null) as BS_IC_RECEBE_PROXIMO_MES,
		CONVERT(float, titrf.PUNominal) as BS_VL_PU_BASE,
		CONVERT(datetime, null) as BS_DT_DATA_BASE,
		CONVERT(char(1), null) as BS_IC_EVENTO_AJUSTADO,
		CONVERT(char(1), null) as BS_IC_MOVER_DATA_ANIV_ANBID,
		CONVERT(char(15), null) as BS_CD_SUSEP,
		CONVERT(float, null) as BS_VL_TOTAL_EMITIDO,
		CONVERT(char(12), titrf.CodigoIsin) as BS_CD_ISIN,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO,
		CONVERT(char(1), null) as BS_UTCOR,
		CONVERT(int, null) as BS_CODTLF,
		CONVERT(char(1), null) as BS_IC_INATIVO,
		CONVERT(char(1), null) as BS_IC_PROVISAO,
		CONVERT(datetime, null) as BS_DT_EMISSAO_DAIEA,
		CONVERT(datetime, null) as BS_DT_COMPRA_DAIEA,
		CONVERT(datetime, null) as BS_DT_VENCTO_DAIEA,
		CONVERT(char(1), null) as BS_IC_ABERT_INDEX,
		CONVERT(char(1), null) as BS_IC_ORIGEM,
		CONVERT(char(3), null) as BS_SG_IDX_ABERT_INDEX,
		CONVERT(char(15), null) as BS_CD_PERFIL,
		CONVERT(int, null) as BS_DD_LIQ_VENC,
		CONVERT(char(8), null) as BS_CD_BOLSA_NEG,
		CONVERT(char(8), 'BRAZIL') as BS_CD_BOLSA_ORG,
		CONVERT(char(8), null) as BS_CD_BOLSA_LIQ,
		CONVERT(char(3), null) as BS_CD_SUBSEG_SPC,
		CONVERT(char(1), null) as BS_SG_COND_RESGATE,
		CONVERT(char(1), null) as BS_IC_UTILIZA_RESG_ANTEC,
		CONVERT(datetime, null) as BS_DT_INI_RESG_ANTEC,
		CONVERT(char(8), null) as BS_CD_BOLSA,
		CONVERT(char(1), null) as BS_IC_GARANTIA,
		CONVERT(char(14), null) as BS_CD_CNPJ_GARANTIDOR,
		CONVERT(char(1), null) as BS_IC_UTILIZA_TIR_COMPRA,
		CONVERT(char(1), null) as BS_IC_PARTICIP_LUCRO,
		CONVERT(char(12), null) as BS_CD_CUSIP,
		CONVERT(char(1), null) as BS_SG_ORIGEM_CAD,
		CONVERT(char(20), null) as BS_CD_CBLC,
		CONVERT(char(8), null) as BS_BAINS_CD_COOBRIGADO,
		CONVERT(float, null) as BS_PC_COOBRIGADO,
		CONVERT(char(8), ratingCc.ValorCampo) AS BS_CD_RATING,
		@DataRef as DataRef
	FROM TituloRendaFixa titRf
	LEFT JOIN Emissor emi ON 
		emi.IdEmissor = titRf.IdEmissor
	LEFT JOIN PapelRendaFixa pprf ON 
		pprf.IdPapel = titRf.IdPapel
	LEFT JOIN Indice ind ON 
		ind.IdIndice = titRf.IdIndice
	LEFT JOIN Moeda mo ON 
		mo.IdMoeda = titRf.IdMoeda
	LEFT JOIN CadastroComplementar ratingCc ON 
		ratingCC.IdMercadoTipoPessoa = titRf.IdTitulo AND
		ratingCc.IdCamposComplementares = (
			SELECT IdCamposComplementares FROM CadastroComplementarCampos ratingCcc 
				WHERE ratingCcc.NomeCampo = 'CD RATING'
					AND ratingCcc.TipoCadastro = -12
		)
END

GO
--select * from TituloRendaFixa
--select * from CadastroComplementar
--select * from CadastroComplementarCampos where NomeCampo = 'CD RATING' AND TIPOCADASTRO = -12


--BS_RF_LASTRO	BS_CD_RATING 	YES	CHAR(8) 		CadastroComplementar.ValorCampo		

--"CadastroComplementar.CadastroComplementarCampo.NomeCampo = 'RATING' and 
--CadastroComplementar.CadastroComplementarCampo.TipoCadastro = -12 and 
--CadastroComplementar.idMercadoTipoPessoa = TituloRendaFixa.idTitulo"
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RF_MEMORIA_IRRF') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RF_MEMORIA_IRRF
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RF_MEMORIA_IRRF]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
select 
CONVERT(datetime, op.DataLiquidacao) as BS_DT,
CONVERT(char(10), op.IdOperacao) as BS_RFOP_CD,
CONVERT(float, null) as BS_VL_IRRF1,
CONVERT(float, null) as BS_VL_IRRF2,
CONVERT(float, null) as BS_VL_IRRF3,
CONVERT(float, null) as BS_PC_ALIQ1,
CONVERT(float, null) as BS_PC_ALIQ2,
CONVERT(float, null) as BS_PC_ALIQ3,
CONVERT(float, null) as BS_PU_31_12_97,
CONVERT(float, null) as BS_PU_29_12_95,
CONVERT(float, op.PUOperacao) as BS_PU_AQ,
CONVERT(float, null) as BS_PU_EM_COR,
CONVERT(float, null) as BS_VL_IRRF4,
CONVERT(float, null) as BS_PC_ALIQ4,
CONVERT(float, null) as BS_PU_01_01_05,
CONVERT(datetime, null) as BS_DT_ALIQUOTA1,
CONVERT(datetime, null) as BS_DT_ALIQUOTA2,
CONVERT(datetime, null) as BS_DT_ALIQUOTA3,
CONVERT(datetime, null) as BS_DT_ALIQUOTA4,
CONVERT(float, null) as BS_VL_RENDIMENTO1,
CONVERT(float, null) as BS_VL_RENDIMENTO2,
CONVERT(float, null) as BS_VL_RENDIMENTO3,
CONVERT(float, null) as BS_VL_RENDIMENTO4,
CONVERT(float, null) as BS_PU_ATUAL
from OperacaoRendaFixa op
WHERE op.DataLiquidacao = @DataRef
END

GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RF_MOV') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RF_MOV
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RF_MOV]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT
		CONVERT(int, query.BS_ID) as BS_ID,														
		CONVERT(datetime, query.BS_DT) as BS_DT,
		CONVERT(char(10), query.RFOP_CD) as BS_RFOP_CD,
		CONVERT(datetime, query.BS_DT_DIGITACAO) as BS_DT_DIGITACAO,				
		CONVERT(char(1), query.BS_SG_OPERACAO) as BS_SG_OPERACAO,
		CONVERT(float, query.BS_QT) as BS_QT,
		CONVERT(float, query.BS_VL_PU_OPERACAO) as BS_VL_PU_OPERACAO,
		CONVERT(float, query.BS_VL_BRUTO) as BS_VL_BRUTO,
		CONVERT(float, query.BS_VL_IRRF) as BS_VL_IRRF,
		CONVERT(float, query.BS_VL_IOF) as BS_VL_IOF,
		CONVERT(float, query.BS_VL_LIQ) as BS_VL_LIQ,
		CONVERT(char(12), null) as BS_CD_REFSWIFT,
		CONVERT(varchar(18), null) as BS_BACMB_CD,
		CONVERT(char(1), null) as BS_IC_ALTERADA,
		CONVERT(char(8), null) as BS_CD_INST,
		CONVERT(char(8), null) as BS_CD_LIQUIDANTE,
		CONVERT(float, null) as BS_NO_COMANDO,
		CONVERT(char(1), null) as BS_IC_EXPORTADA_IT,
		CONVERT(char(1), null) as BS_IC_TRANSFERENCIA,
		CONVERT(char(20), null) as BS_DS_TIPO_LIQ_FIN,
		CONVERT(varchar(255), null) as BS_DS_OBS,
		CONVERT(char(1), null) as BS_IC_IRRF_VIRTUAL,
		CONVERT(char(1), null) as BS_IC_IOF_RESGATE_VIRTUAL,
		CONVERT(datetime, null) as BS_DT_REGISTRO,
		CONVERT(char(14), null) as BS_CD_OPER_INTERNO,
		CONVERT(varchar(20), null) as BS_OPERADOR,
		CONVERT(char(9), null) as BS_CD_CONTRAPARTE_CETIP_SELIC,
		CONVERT(char(1), null) as BS_SG_ALOCACAO,
		CONVERT(int, null) as BS_ID_MOV_PLANO,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO,
		CONVERT(char(3), null) as BS_CD_SUBSEG_SPC,
		CONVERT(float, null) as BS_NO_COMANDO_ORIGINAL,
		CONVERT(char(1), null) as BS_IC_NETBOL,
		CONVERT(int, null) as BS_CODTLF,
		CONVERT(char(8), null) as BS_CODCLR,
		CONVERT(char(8), null) as BS_CODLCC,
		CONVERT(datetime, null) as BS_HR_PARTIDA,
		CONVERT(numeric(9), null) as BS_NO_PRIORIDADE,
		CONVERT(char(1), null) as BS_CD_PRIORIDADE,
		CONVERT(char(10), null) as BS_CD_MERCADO_EXT,
		CONVERT(char(1), null) as BS_IC_MOV_AUTOMATICO,
		CONVERT(char(1), null) as BS_IC_PERC_VL_FACE,
		CONVERT(decimal(5), null) as BS_PC_VL_FACE,
		CONVERT(char(1), null) as BS_SG_ORIGEM,
		CONVERT(char(1), null) as BS_SG_MOD_LIQUID,
		CONVERT(char(1), null) as BS_SG_TIPO_OPERACAO,
		CONVERT(numeric(5), null) as BS_ID_CONTA,
		CONVERT(char(8), null) as BS_CD_BOLSA_NEG,
		CONVERT(char(10), null) as BS_RFOP_CD_LIQ_TMO,
		CONVERT(datetime, null) as BS_DT_INTEGRACAO_NETBOL,
		CONVERT(datetime, null) as BS_DT_LIQUIDACAO,
		CONVERT(char(8), null) as BS_CD_BOLSA,
		CONVERT(char(15), null) as BS_CD_ESTRATEGIA_MOV,
		CONVERT(decimal(9), null) as BS_ID_SWIFT,
		CONVERT(char(1), null) as BS_IC_VENDA_TERMO,
		CONVERT(datetime, null) as BS_DT_LIQ_VENDA_TERMO,
		CONVERT(char(1), null) as BS_IC_REG_VENDA_TERMO,
		CONVERT(float, null) as BS_ID_REG_VENDA_TERMO
	FROM (
		Select  
			OperacaoRendaFixa.IdOperacao as BS_ID,
			OperacaoRendaFixa.DataLiquidacao as BS_DT,
			OperacaoRendaFixa.IdOperacao as RFOP_CD,
			OperacaoRendaFixa.DataRegistro as BS_DT_DIGITACAO,
			CASE 
				WHEN OperacaoRendaFixa.TipoOperacao IN (1,3,10,13,22,23,20)
					THEN 'C'
				WHEN OperacaoRendaFixa.TipoOperacao IN (2,4,6,11,12,21,24,25)
					THEN 'V'
				ELSE null
			END	as BS_SG_OPERACAO,
			OperacaoRendaFixa.Quantidade as BS_QT,
			OperacaoRendaFixa.PUOperacao as BS_VL_PU_OPERACAO,
			OperacaoRendaFixa.Valor as BS_VL_BRUTO,
			OperacaoRendaFixa.ValorIR as BS_VL_IRRF,
			OperacaoRendaFixa.ValorIOF as BS_VL_IOF,
			OperacaoRendaFixa.ValorLiquido as BS_VL_LIQ
		from OperacaoRendaFixa
		where 
			OperacaoRendaFixa.DataLiquidacao = @DataRef AND
			OperacaoRendaFixa.TipoOperacao in (1,3,10,13,22,23,20)
	) query
END

GO


/*gps -> master*/
--(Select  OperacaoRendaFixa.IdOperacao as BS_ID,
--  OperacaoRendaFixa.DataLiquidacao as BS_DT,
--  OperacaoRendaFixa.IdOperacao as RFOP_CD,
--  OperacaoRendaFixa.DataRegistro as BS_DT_DIGITACAO,
--  'C' as BS_SG_OPERACAO,
--  OperacaoRendaFixa.Quantidade as BS_QT,
--  OperacaoRendaFixa.PUOperacao as BS_VL_PU_OPERACAO,
--  OperacaoRendaFixa.Valor as BS_VL_BRUTO,
--  OperacaoRendaFixa.ValorIR as BS_VL_IRRF,
--  OperacaoRendaFixa.ValorIOF as BS_VL_IOF,
--  OperacaoRendaFixa.ValorLiquido as BS_VL_LIQ
--From OperacaoRendaFixa
--where OperacaoRendaFixa.DataLiquidacao = @DataRef and
--      OperacaoRendaFixa.TipoOperacao in (1,3,10,13,22,23,20)
--UNION
--Select  OperacaoRendaFixa.IdOperacao*100000 + PosicaoRendaFixaHistorico.IdOperacao as BS_ID,
--  DetalhePosicaoAfetadaRF.DataOperacao as BS_DT,
--  PosicaoRendaFixaHistorico.IdOperacao as RFOP_CD,
--  DetalhePosicaoAfetadaRF.DataOperacao as BS_DT_DIGITACAO,
--  'V' as BS_SG_OPERACAO,
--  DetalhePosicaoAfetadaRF.QtdeMovimentada as BS_QT,
--  DetalhePosicaoAfetadaRF.PUOperacao as BS_VL_PU_OPERACAO,  
--  OperacaoRendaFixa.Valor/OperacaoRendaFixa.Quantidade * DetalhePosicaoAfetadaRF.QtdeMovimentada  as BS_VL_BRUTO,
--  OperacaoRendaFixa.ValorIR/OperacaoRendaFixa.Quantidade * DetalhePosicaoAfetadaRF.QtdeMovimentada   as BS_VL_IRRF,
--  OperacaoRendaFixa.ValorIOF/OperacaoRendaFixa.Quantidade * DetalhePosicaoAfetadaRF.QtdeMovimentada   as BS_VL_IOF,
--  OperacaoRendaFixa.ValorLiquido/OperacaoRendaFixa.Quantidade * DetalhePosicaoAfetadaRF.QtdeMovimentada   as BS_VL_LIQ
--From OperacaoRendaFixa, DetalhePosicaoAfetadaRF, PosicaoRendaFixaHistorico
--where OperacaoRendaFixa.IdOperacao = DetalhePosicaoAfetadaRF.IdOperacao and
--      PosicaoRendaFixaHistorico.DataHistorico = DetalhePosicaoAfetadaRF.DataOperacao and
--      PosicaoRendaFixaHistorico.IdPosicao = DetalhePosicaoAfetadaRF.IdPosicaoAfetada and 
--      OperacaoRendaFixa.TipoOperacao in (2,4,6,11,12,21,24,25) and 
--      OperacaoRendaFixa.DataLiquidacao = @DataRef) query

IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RF_OPER') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RF_OPER
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RF_OPER]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT DISTINCT
		CONVERT(char(10), op.IdOperacao) as BS_CD,
		CONVERT(char(15), cli.IdCliente) as BS_CLCLI_CD,
		CONVERT(char(10), op.IdTitulo) as BS_RFLAS_CD,
		CONVERT(float, null) as BS_VL_PU_MIN,
		CONVERT(float, null) as BS_VL_PU_MAX,
		CONVERT(char(8), null) as BS_IDDIR_CD_GER,
		CONVERT(float, null) as BS_VL_JUROSGER,
		CONVERT(float, null) as BS_VL_FEEGER,
		CONVERT(float, null) as BS_VL_JUROSOPER,
		CONVERT(float, null) as BS_VL_TAXA_CAMBIO,
		CONVERT(char(8), '') as BS_OECAD_CD,
		CONVERT(char(8), null) as BS_BAINS_CD_CUST,
		CONVERT(char(1), null) as BS_IC_COMPULSORIO,
		CONVERT(char(5), null) as BS_RFTP_CD_ADELIC,
		CONVERT(datetime, titRf.DataEmissao) as BS_DT_EMISSAO_ADELIC,
		CONVERT(datetime, titrf.DataVencimento) as BS_DT_VENCIMENTO_ADELIC,
		CONVERT(float, null) as BS_VL_VOLTA_ADELIC,
		CONVERT(float, null) as BS_VL_PU_VOLTA_ADELIC,
		CONVERT(char(15), null) as BS_CD_PRICE,
		CONVERT(char(1), null) as BS_IC_IMPORTADA,
		CONVERT(char(14), null) as BS_CD_OPER_INTERNO,
		CONVERT(char(1), null) as BS_IC_TX_ADM,
		CONVERT(char(4), null) as BS_CD_SUBSEGTO,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO,
		CONVERT(char(1), null) as BS_IC_NETBOL,
		CONVERT(int, null) as BS_SG_TIPO_OPERACAO,
		CONVERT(datetime, null) as BS_DT_LIQUIDACAO,
		CONVERT(char(1), null) as BS_IC_NEG_VENC,
		CONVERT(char(10), null) as BS_RFLAS_CD_ADELIC,
		CONVERT(float, null) as BS_VL_PU_IDA_REAL,
		CONVERT(float, null) as BS_QT_IDA_REAL,
		CONVERT(char(8), null) as BS_CD_EMITENTE_REAL,
		CONVERT(char(1), null) as BS_IC_OPER_TMO,
		CONVERT(datetime, null) as BS_DT_LIQUIDACAO_TMO,
		CONVERT(char(1), null) as BS_SG_TIPO_LEILAO,
		CONVERT(char(1), 'A') as BS_IC_ATIVO_PASSIVO,
		CONVERT(numeric(5), null) as BS_ID_CONTA,
		CONVERT(char(8), null) as BS_CD_BOLSA_ORG,
		CONVERT(char(8), null) as BS_CD_BOLSA_LIQ,
		CONVERT(datetime, null) as BS_DT_PAGAMENTO_IR_MP281,
		CONVERT(char(30), null) as BS_CD_EXT_OPER,
		CONVERT(char(8), null) as BS_CD_BOLSA,
		CONVERT(char(1), null) as BS_SG_ORIGEM_CAD,
		CONVERT(char(20), null) as BS_CD_CBLC,
		CONVERT(char(20), null) as BS_CD_CETIP_SELIC,
		CONVERT(char(20), null) as BS_CD_CUST_LIQ_FIN,
		@DataRef as DataRef
	FROM PosicaoRendaFixaHistorico pos
	LEFT JOIN OperacaoRendaFixa op ON
		op.IdOperacao = pos.IdOperacao 
	LEFT JOIN Cliente cli ON 
		cli.IdCliente = op.IdCliente
	LEFT JOIN TituloRendaFixa titRf ON 
		titRf.IdTitulo = op.IdTitulo
	WHERE pos.DataHistorico = @DataRef
END

GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RF_POS') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RF_POS
GO

CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RF_POS]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT 
		CONVERT(DATETIME,posRF.DataHistorico)								AS BS_DT,
		CONVERT(CHAR(10),posRF.IdOperacao)									AS BS_RFOP_CD,
		CONVERT(DATETIME,posRF.dataOperacao)								AS BS_DT_AQUISICAO,
		CONVERT(FLOAT,posRF.PuOperacao)										AS BS_VL_PU_AQUISICAO,
		CONVERT(FLOAT,posRF.Quantidade - posRF.QuantidadeBloqueada)			AS BS_QT_DISPONIVEL,
		CONVERT(FLOAT,posRF.QuantidadeBloqueada)							AS BS_QT_BLOQUEADA,
		CONVERT(FLOAT,posRF.PuMercado)										AS BS_VL_PU,
		CONVERT(FLOAT,TitRF.ValorNominal * posRF.Quantidade)				AS BS_VL_PRINCIPAL,
		CONVERT(FLOAT,posRF.ValorCorrecao)									AS BS_VL_CM,
		CONVERT(FLOAT,posRF.ValorJuros)										AS BS_VL_JUROS,
		CONVERT(FLOAT,0)													AS BS_VL_PREMIO,
		CONVERT(FLOAT,0)													AS BS_VL_AGIO,
		CONVERT(FLOAT,posRF.ValorMercado)									AS BS_VL_BRUTOPOS,
		CONVERT(FLOAT,posRF.ValorIR)										AS BS_VL_PROV_IRRF,
		CONVERT(FLOAT,posRF.ValorIOF)										AS BS_VL_PROV_IOF,
		CONVERT(FLOAT,posRF.ValorMercado - posRF.ValorIR - posRF.ValorIOF)	AS BS_VL_LIQ_POS,
		CONVERT(FLOAT,posRF.ValorMercado)									AS BS_VL_BRUTO_GER,
		CONVERT(FLOAT,posRF.ValorIR)										AS BS_VL_PROV_IRRFGER,
		CONVERT(FLOAT,posRF.ValorIOF)										AS BS_VL_PROV_IOFGER,
		CONVERT(FLOAT,posRF.ValorMercado - posRF.ValorIR - posRF.ValorIOF)	AS BS_VL_LIQUID_GER,
		CONVERT(FLOAT,1)													AS BS_VL_FATOR_CM,
		CONVERT(FLOAT,1)													AS BS_VL_FATOR_JUROS,
		CONVERT(FLOAT,1)													AS BS_VL_FATOR_PREMIO,
		CONVERT(FLOAT,1)													AS BS_VL_FATOR_AGIO,
		CONVERT(FLOAT,1)													AS BS_VL_FATOR_AJUSTE,
		CONVERT(FLOAT,0)													AS BS_VL_PUEM_COR,
		CONVERT(FLOAT,0)													AS BS_VL_PUEM_PRINC,
		CONVERT(FLOAT,0)													AS BS_VL_PUEM_CM,
		CONVERT(FLOAT,0)													AS BS_VL_PUEM_JUROS,
		CONVERT(FLOAT,0)													AS BS_VL_PUEM_PREMIO,
		CONVERT(FLOAT,0)													AS BS_VL_PROV_IRRF_UNIT,
		CONVERT(FLOAT,1)													AS BS_VL_AP,
		/*master - > gps trocar por posrf.AjusteMTM*/
		CONVERT(FLOAT,ValorMercado - ValorCurva)							AS BS_VL_AJUSTE,
	
		--((ValorMercado /  ValorMercado(D-1))^(252)-1)*100
		CONVERT(FLOAT,														--
		(																	--
			(																--
				POWER(														--
				(															--
					ValorMercado											--
					/														--
					(														--
						SELECT TOP(1) ValorMercado 							--
						FROM  PosicaoRendaFixaHistorico posRFint 			--
						WHERE 												--
						posRFint.IdPosicao = posRF.IdPosicao AND			--
						posRFint.DataHistorico < posRF.DataHistorico		--
						ORDER BY posRFint.DataHistorico DESC				--
					)														--
				),252) - 1													--
			) * 100															--
		))																	AS BS_PC_TAXA_OVER, 
	
		--'nrDiasUteis(@DataRef, TituloRendaFixa.DataVencimento))',			--
		CONVERT(FLOAT,														--
		(SELECT COUNT(*) FROM dbo.fn_DiasUteis(									--
			@DataRef,														--
			TitRF.DataVencimento,
			1
		)))																	AS BS_DD_UTEIS_ATE_VCTO,
	
		/*master->gps*/
		CONVERT(FLOAT,null)													AS BS_VL_TIR,
		--CONVERT(FLOAT,														--
		--(																	--
		--	select top(1) TaxaDesconto from MemoriaCalculoRendaFixa where 	--
		--	MemoriaCalculoRendaFixa.DataAtual = posRF.DataHistorico and 	--
		--	MemoriaCalculoRendaFixa.IdOperacao = posRF.IdOperacao and		--
		--	MemoriaCalculoRendaFixa.TipoPreco in(2,3)						--
		--	order by TipoPreco desc											--
		--))																	AS BS_VL_TIR,
	
		CONVERT(FLOAT,0)													AS BS_VL_DURATION,
		CONVERT(FLOAT,0)													AS BS_VL_COUPON,
		CONVERT(FLOAT,0)													AS BS_VL_M_DURATION,
		CONVERT(FLOAT,0)													AS BS_VL_PU_AGIO,
	
		/*master->gps*/
		CONVERT(FLOAT,null)													AS BS_VL_PU_AJUSTE_MTM,
		--CONVERT(FLOAT,posRF.AjusteMTM /  posRF.Quantidade)					AS BS_VL_PU_AJUSTE_MTM,
	
		--nrDiasUteis(BS_DT_AQUISICAO, TituloRendaFixa.DataVencimento)),
		CONVERT(FLOAT,														--
		(SELECT COUNT(*) FROM dbo.fn_DiasUteis(									--
			posRF.dataOperacao,												--
			TitRF.DataVencimento,
			1
		)))																	AS BS_DD_UTEIS_AQUISICAO_VENCTO, 
	
		CONVERT(FLOAT,0)													AS BS_VL_PU_NAO_DESCONTADO,
		CONVERT(FLOAT,0)													AS BS_VL_PU_AO_PAR,
		CONVERT(FLOAT,PUCurva)												AS BS_VL_PU_CURVA,
	
		/*master->gps*/
		CONVERT(FLOAT,null)													AS BS_VL_TIR_CURVA,
		--CONVERT(FLOAT,													--
		--(																	--
		--	select top(1) TaxaDesconto from MemoriaCalculoRendaFixa where	--
		--	MemoriaCalculoRendaFixa.DataAtual = posRF.DataHistorico and 	--
		--	MemoriaCalculoRendaFixa.IdOperacao = posRF.IdOperacao and 		--
		--	MemoriaCalculoRendaFixa.TipoPreco =2							--
		--))																	AS BS_VL_TIR_CURVA,
	
		/*master->gps*/
		CONVERT(FLOAT,null)													AS BS_VL_TIR_MERCADO,
		--CONVERT(FLOAT,TaxaMTM)												AS BS_VL_TIR_MERCADO,
		CONVERT(FLOAT,PUMercado)											AS BS_VL_PU_MERCADO,
		CONVERT(FLOAT,TitRF.ValorNominal)									AS BS_VL_PU_NOMINAL,
		CONVERT(CHAR(1),'S')												AS BS_IC_LOG_LIBERADO,
		CONVERT(INT,0)														AS BS_ID_LOG_LIBERADO,
		/*master->gps*/
		CONVERT(FLOAT,null)													AS BS_VL_AJUSTE_NEG_VENC,
		--CONVERT(FLOAT,posRF.AjusteVencimento)								AS BS_VL_AJUSTE_NEG_VENC,
		CONVERT(CHAR(1),'N')												AS BS_SG_TIPO_MTM,
		CONVERT(CHAR(1),'')													AS BS_CD_PROVEDOR_MTM,
		CONVERT(FLOAT,0)													AS BS_QT_BLOQ_01,
		CONVERT(FLOAT,0)													AS BS_QT_BLOQ_03,
		CONVERT(FLOAT,0)													AS BS_QT_BLOQ_05,
		CONVERT(FLOAT,0)													AS BS_QT_BLOQ_14,
		CONVERT(FLOAT,0)													AS BS_QT_VENDA_TERMO,
		CONVERT(CHAR(1),0)													AS BS_SG_ORIGEM_CAD
		--CONVERT(float,pm.PrazoMedio)										AS BS_PRZ_MEDIO_POSICAO_ATIVO
	FROM 
		PosicaoRendaFixaHistorico posRF
	LEFT JOIN TituloRendaFixa TitRF on 
		TitRF.IdTitulo = posRF.IdTitulo
	LEFT JOIN Indice ind ON
		ind.IdIndice = TitRF.IdIndice
	LEFT JOIN PrazoMedio pm ON 
		pm.IdCliente = posrf.IdCliente AND
		pm.DataAtual = posRF.DataHistorico AND
		pm.MercadoAtivo = 2	AND
		pm.Descricao = 
			'Compra Final ' + 
			TitRF.Descricao + 
			' - Vcto: ' + 
			cast(cast(TitRF.DataVencimento as date) as varchar(max)) +
			' -> ' + 
			cast(cast(TitRF.Percentual as numeric(38,4)) as varchar(max)) + 
			'% - ' +  
			ind.Descricao + 
			' - ' + 
			cast(cast(TitRF.Taxa as numeric(38,4)) as varchar(max)) +
			'%'
	INNER JOIN OperacaoRendaFixa op ON
		op.IdOperacao = posRF.IdOperacao AND
		op.IdCliente = posRF.IdCliente
	WHERE 
		CONVERT(DATE,posRF.DataHistorico) = @DataRef

END




GO

IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RF_TIPO') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RF_TIPO
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RF_TIPO]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN


	SELECT
		CONVERT(char(5), ppRf.IdPapel) as BS_CD,
		CONVERT(varchar(20), pprf.Descricao) as BS_NM,
		CONVERT(char(1), null) as BS_IC_PRE_POS,
		CONVERT(char(1), null) as BS_IC_PUB_PRIV,
		CONVERT(char(1), null) as BS_IC_JUROS,
		CONVERT(char(1), null) as BS_SG_APROP_LED,
		CONVERT(char(1), null) as BS_SG_APROP_UCD,
		CONVERT(char(1), null) as BS_IC_ANIVER,
		CONVERT(float, ppRf.BaseAno) as BS_VL_BASE_JUROS,
		CONVERT(float, null) as BS_VL_PU_EMISSAO,
		CONVERT(float, null) as BS_VL_PU_VENC,
		CONVERT(char(1), null) as BS_IC_FIXA_VAR,
		CONVERT(char(1), null) as BS_SG_PAPDIV,
		CONVERT(char(8), null) as BS_IDDIR_CD,
		CONVERT(char(1), null) as BS_SG_BACEN,
		CONVERT(int, null) as BS_DD_LIQ_FIN,
		CONVERT(char(1), null) as BS_IC_TX_PERIODO,
		/*master - > gps --(case pprf.IsentoIR when 1 then 'N' else 'S' end) não existe o campo isento ir na master*/
		CONVERT(char(1),null) as BS_IC_PAGA_IRRF,
		CONVERT(int, null) as BS_DD_DEFASAGEM,
		CONVERT(char(1), null) as BS_IC_JUROS_LIQ_FIN,
		CONVERT(char(1), null) as BS_SG_BACEN2,
		CONVERT(char(2), null) as BS_SG_BACEN3,
		CONVERT(char(4), null) as BS_BACEN_CD,
		CONVERT(char(1), null) as BS_IC_LIMITE_CREDITO,
		CONVERT(float, null) as BS_VL_LIMITE_CREDITO,
		CONVERT(char(10), null) as BS_CD_EMISSOR_PADRAO,
		CONVERT(char(1), null) as BS_SG_CAIXA_PADRAO,
		CONVERT(int, null) as BS_NO_DECIMAIS_QT,
		CONVERT(char(1), null) as BS_IC_MTM_LE,
		CONVERT(int, null) as BS_ID_TIPO_FERIADO,
		CONVERT(char(20), null) as BS_SG_LOCAL_CUSTODIA,
		CONVERT(int, null) as BS_DD_DEFASAGEM_CM,
		CONVERT(char(1), null) as BS_SG_MTM_DEFAULT,
		CONVERT(char(1), null) as BS_SG_APROP_UCD_MTM,
		CONVERT(char(1), null) as BS_IC_DISPONIBILIDADE,
		CONVERT(char(3), null) as BS_CD_SFR,
		CONVERT(char(1), null) as BS_IC_ENQUADRAMENTO,
		CONVERT(char(1), null) as BS_IC_TX_APURACAO,
		CONVERT(float, null) as BS_VL_BASE_APURACAO,
		CONVERT(char(1), null) as BS_IC_DIA_APURACAO,
		CONVERT(char(1), null) as BS_IC_PU_APURACAO,
		CONVERT(char(6), null) as BS_CD_SUSEP,
		CONVERT(char(1), null) as BS_IC_OBRIGA_CADASTRO_EVENTOS,
		CONVERT(char(1), null) as BS_IC_CALCULO_PADRONIZADO_MTM,
		CONVERT(char(1), null) as BS_IC_CALCULO_PADRONIZADO,
		CONVERT(float, null) as BS_VL_BASE_JUROS_MTM,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO,
		CONVERT(char(5), null) as BS_CD_ENQUADRA_SPC,
		CONVERT(char(3), null) as BS_CD_SUBSEG_SPC,
		CONVERT(datetime, null) as BS_DT_REGISTRO,
		CONVERT(char(1), null) as BS_IC_TRUNCA_PU,
		CONVERT(int, null) as BS_NO_DECIMAIS_PU,
		CONVERT(char(20), null) as BS_NM_TRADUZIDO,
		CONVERT(varchar(15), null) as BS_CD_PAPEL_SND,
		CONVERT(int, null) as BS_CODTLF,
		CONVERT(char(2), null) as BS_SG_CLASSIF_ATIVO,
		CONVERT(char(1), null) as BS_IC_EST_OPER_TMO,
		CONVERT(char(1), null) as BS_IC_ATIVO_INATIVO,
		CONVERT(char(1), null) as BS_IC_ORIGEM,
		CONVERT(char(8), null) as BS_IDDIR_CD_CM,
		CONVERT(int, null) as BS_DD_LIQ_VENC,
		CONVERT(char(8), null) as BS_CD_BOLSA_NEG,
		/*master - > gps -- --CONVERT(char(8), ln.Codigo) as BS_CD_BOLSA_ORG,*/
		CONVERT(char(8), null) as BS_CD_BOLSA_ORG,
		CONVERT(char(8), null) as BS_CD_BOLSA_LIQ,
		CONVERT(char(3), null) as BS_SG_TIPO_DC,
		CONVERT(char(1), null) as BS_SG_VF_TRUNCATE_ROUND,
		CONVERT(char(1), null) as BS_SG_PU_TRUNCATE_ROUND,
		CONVERT(char(1), null) as BS_SG_QT_TRUNCATE_ROUND,
		CONVERT(char(1), null) as BS_SG_IDX_TRUNCATE_ROUND,
		CONVERT(char(1), null) as BS_SG_JRS_TRUNCATE_ROUND,
		CONVERT(decimal(9), null) as BS_NO_DECIMAIS_INDEX,
		CONVERT(decimal(9), null) as BS_NO_DECIMAIS_JUROS,	
		/*master - > gps -- --CONVERT(char(8), ln.Codigo) as BS_CD_BOLSA,*/
		CONVERT(char(8), CASE WHEN ppRf.TipoPapel = 1 THEN 'SELIC' ELSE 'CETIP' END) as BS_CD_BOLSA,
		CONVERT(char(1), null) as BS_IC_PROC_SFLUXO,
		CONVERT(int, null) as BS_NO_DECIMAIS_VNA,
		CONVERT(char(1), null) as BS_IC_TRUNCA_VNA,
		CONVERT(char(1),secCc.ValorCampo) as BS_IC_SECURITIZADA,
		@DataRef as DataRef
	FROM 
		PapelRendaFixa ppRf
	LEFT JOIN CadastroComplementar secCc ON secCc.IdMercadoTipoPessoa = ppRf.IdPapel
		AND secCc.IdCamposComplementares = (
				SELECT IdCamposComplementares FROM CadastroComplementarCampos ccc 
					WHERE ccc.NomeCampo = 'IC SECURITIZADA'
						AND ccc.TipoCadastro = -13
			)
		
	
	
	/*master - > gps -- na master não existe essa tabela*/
	--left join LocalNegociacao ln on ln.IdLocalNegociacao = ppRf.IdLocalNegociacao
END


GO

  IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RV_CAD') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RV_CAD
GO



CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RV_CAD]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	declare @IdCcCapitalVolante int
	set @IdCcCapitalVolante = (
		SELECT TOP(1)IdCamposComplementares FROM CadastroComplementarCampos ccc
			WHERE ccc.NomeCampo = 'CAPITAL VOTANTE'
				AND ccc.TipoCadastro = -10
	)
	declare @IdCcCapitalTotal int 
	set @IdCcCapitalTotal =(	SELECT TOP(1)IdCamposComplementares FROM CadastroComplementarCampos ccc
			WHERE ccc.NomeCampo = 'CAPITAL TOTAL'
				AND ccc.TipoCadastro = -10
	)
	declare @IdCcSocControlada int 
	set @IdCcSocControlada =	(SELECT TOP(1)IdCamposComplementares FROM CadastroComplementarCampos ccc
			WHERE ccc.NomeCampo = 'SOC CONTROLADA'
				AND ccc.TipoCadastro = -10
	)
	declare @IdCcCiaNovoMercado int 
	set @IdCcCiaNovoMercado =	(SELECT TOP(1)IdCamposComplementares FROM CadastroComplementarCampos ccc
			WHERE ccc.NomeCampo = 'CIA NOVO MERCADO'
				AND ccc.TipoCadastro = -10
	)
	declare @IdCcIcCompanhia int 
	set @IdCcIcCompanhia =	(SELECT TOP(1)IdCamposComplementares FROM CadastroComplementarCampos ccc
			WHERE ccc.NomeCampo = 'IC COMPANHIA'
				AND ccc.TipoCadastro = -10
	)
	
	declare @IdCcNivel int 
	set @IdCcNivel =	(SELECT TOP(1)IdCamposComplementares FROM CadastroComplementarCampos ccc
			WHERE ccc.NomeCampo = 'NIVEL'
				AND ccc.TipoCadastro = -10
	)
	declare @IdCcLocalCustodia int 
	set @IdCcLocalCustodia =	(SELECT TOP(1)IdCamposComplementares FROM CadastroComplementarCampos ccc
			WHERE ccc.NomeCampo = 'LOCAL CUSTODIA'
				AND ccc.TipoCadastro = -10
	)
	declare @IdCcDtTipo int 
	set @IdCcDtTipo =	(SELECT TOP(1)IdCamposComplementares FROM CadastroComplementarCampos ccc
			WHERE ccc.NomeCampo = 'DT IPO'
				AND ccc.TipoCadastro = -10
	)

	
	declare @IdCcIcOferta int
	set @IdCcIcOferta =	(SELECT TOP(1)IdCamposComplementares FROM CadastroComplementarCampos ccc
			WHERE ccc.NomeCampo = 'IC OFERTA'
				AND ccc.TipoCadastro = -10
	)
	
	

	SELECT 
		CONVERT(char(12), at.CdAtivoBolsa) as BS_CODPAP,
		CONVERT(float, null) as BS_BETA,
		CONVERT(char(1), null) as BS_CD_ADR,
		CONVERT(char(9), null) as BS_CODBVRJ,
		CONVERT(char(8),'EM'+CAST(at.IdEmissor AS VARCHAR(MAX))) as BS_CODEMP,
		CONVERT(char(8), mo.Nome) as BS_CODMOEDA,
		CONVERT(datetime, at.DataVencimento) as BS_DATAVCTO,
		CONVERT(float, at.PUExercicio) as BS_EXERC,
		CONVERT(char(12), at.CodigoIsin) as BS_ISIN,
		CONVERT(float, null) as BS_LOTE_PADRAO_CT,
		CONVERT(float, 1) as BS_LOTECOT,
		CONVERT(varchar(30), at.Descricao) as BS_NOME,
		CONVERT(char(1), tipo.CodigoExterno) as BS_TIPO,
		CONVERT(char(12), null) as BS_RVPAP_CD_OPCAO,
		CONVERT(char(2), null) as BS_IC_OPCAO,
		CONVERT(float, null) as BS_TAMANHO_CONTRATO,
		CONVERT(char(6), null) as BS_FUVCT_CD,
		CONVERT(float, null) as BS_PC_CORNORMAL,
		CONVERT(float, null) as BS_PC_DAYTRADE,
		CONVERT(float, null) as BS_PC_EMOL,
		CONVERT(char(1), null) as BS_IC_COR_FDA,
		CONVERT(char(1), null) as BS_IC_COR_1VA,
		CONVERT(float, null) as BS_VL_CORFIXA_NORMAL,
		CONVERT(float, null) as BS_VL_CORFIXA_DAYTRADE,
		CONVERT(float, null) as BS_VL_TX_REG,
		CONVERT(float, null) as BS_PC_EMOL_DAYTRADE,
		CONVERT(float, null) as BS_PC_TOB_FU,
		CONVERT(char(12), null) as BS_CD_BROADCAST,
		CONVERT(char(1), null) as BS_IC_OP,
		CONVERT(char(1), null) as BS_IC_CLASSE,
		CONVERT(float, null) as BS_QT_TIPO,
		CONVERT(datetime, at.DataInicioVigencia) as BS_DT_ININEG,
		CONVERT(datetime, null) as BS_DT_LIMNEG,
		CONVERT(char(1), null) as BS_IC_INDBOV,
		CONVERT(datetime, null) as BS_DT_INI_PGT_EX,
		CONVERT(char(50), tipoPapelAtivo.CodigoExterno) as BS_IC_TIPO_PAPEL,
		CONVERT(char(1), null) as BS_IC_CLASSE_FUND,
		CONVERT(float, null) as BS_VL_THETA,
		CONVERT(float, null) as BS_PC_IBOVESPA,
		CONVERT(float, null) as BS_VL_PREMIO,
		CONVERT(float, null) as BS_VL_DELTA,
		CONVERT(char(30), null) as BS_NOME_INTL,
		CONVERT(char(1), 'N') as BS_CD_BDR,
		CONVERT(char(5), null) as BS_CD_SUSEP,
		CONVERT(char(12), null) as BS_RVPAP_CD_VINC,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO,
		CONVERT(char(3), null) as BS_CD_SUBSEG_SPC,
		CONVERT(char(1), null) as BS_IC_INT_CVM_999,
		CONVERT(char(1), null) as BS_IC_ORIGEM,
		CONVERT(char(15), null) as BS_CD_PERFIL,
		/*master - > gps -- ln.Codigo não existe na master*/
		CONVERT(char(8),  'BOVESPA') as BS_CD_BOLSA,
		CONVERT(char(1), null) as BS_IC_CALC_EMOLUMENTO,
		CONVERT(char(1), null) as BS_SG_TIPO_ATIVO_OPCAO,
		CONVERT(datetime, null) as BS_DT_INI_VIGENCIA,
		CONVERT(char(12), null) as BS_CD_CUSIP,
		CONVERT(numeric(9), null) as BS_CD_REGRA_NEG_ATIVO,
		CONVERT(char(21), null) as BS_CD_ATIVO_EXTERNO,
		CONVERT(float, capVotCc.ValorCampo) as BS_CAPITAL_VOTANTE,
		CONVERT(float, capTotCc.ValorCampo) as BS_CAPITAL_TOTAL,
		CONVERT(char(15), socContrCc.ValorCampo) as BS_SOC_CONTROLADA,
		CONVERT(char(1), ciaNovMercCc.ValorCampo) as BS_CIA_NOVO_MERCADO,
		CONVERT(char(1), icCompCc.ValorCampo) as BS_IC_COMPANHIA,
		CONVERT(char(10), nivCc.ValorCampo) as BS_NIVEL,
		CONVERT(char(20), locCust.ValorCampo ) as BS_SG_LOCAL_CUSTODIA,
		CONVERT(datetime, dtIpoCc.ValorCampo) as BS_DT_IPO,
		CONVERT(char(1), icOfCc.ValorCampo) as BS_IC_OFERTA,
		@DataRef as DataRef
	FROM 
		AtivoBolsa at	
	LEFT JOIN Moeda mo ON 
		mo.IdMoeda = at.IdMoeda	
	LEFT JOIN CadastroComplementar capVotCc ON 
		capVotCc.IdMercadoTipoPessoa = at.CdAtivoBolsa AND
		capVotCc.IdCamposComplementares = @IdCcCapitalVolante 
	LEFT JOIN CadastroComplementar capTotCc ON 
		capTotCc.IdMercadoTipoPessoa = at.CdAtivoBolsa AND
		capTotCc.IdCamposComplementares = @IdCcCapitalTotal
	LEFT JOIN CadastroComplementar socContrCc ON 
		socContrCc.IdMercadoTipoPessoa = at.CdAtivoBolsa AND
		socContrCc.IdCamposComplementares = @IdCcSocControlada
	LEFT JOIN CadastroComplementar ciaNovMercCc ON 
		ciaNovMercCc.IdMercadoTipoPessoa = at.CdAtivoBolsa AND
		ciaNovMercCc.IdCamposComplementares = @IdCcCiaNovoMercado
	LEFT JOIN CadastroComplementar icCompCc ON 
		icCompCc.IdMercadoTipoPessoa = at.CdAtivoBolsa	AND
		icCompCc.IdCamposComplementares = @IdCcIcCompanhia
	LEFT JOIN CadastroComplementar nivCc ON 
		nivCc.IdMercadoTipoPessoa = at.CdAtivoBolsa	AND
		nivCc.IdCamposComplementares = @IdCcNivel 
	LEFT JOIN CadastroComplementar locCust ON 
		locCust.IdMercadoTipoPessoa = at.CdAtivoBolsa AND
		locCust.IdCamposComplementares = @IdCcLocalCustodia
	LEFT JOIN CadastroComplementar dtIpoCc ON 
		dtIpoCc.IdMercadoTipoPessoa = at.CdAtivoBolsa AND
		dtIpoCc.IdCamposComplementares = @IdCcDtTipo
	LEFT JOIN CadastroComplementar icOfCc ON 
		icOfCc.IdMercadoTipoPessoa = at.CdAtivoBolsa AND
		icOfCc.IdCamposComplementares = @IdCcIcOferta
	LEFT JOIN DePara tipo ON 
		tipo.CodigoInterno = CONVERT(varchar(max),at.TipoMercado) AND 
		tipo.IdTipoDePara = -11
	LEFT JOIN DePara tipoPapelAtivo ON 
		tipoPapelAtivo.CodigoInterno = CONVERT(varchar(max),at.TipoPapel) AND 
		tipoPapelAtivo.IdTipoDePara = -24
	INNER JOIN Emissor e ON
		e.IdEmissor = at.IdEmissor


	
	/*master - > gps -- na master não existe essa tabela*/
	--left join LocalNegociacao ln on ln.IdLocalNegociacao = at.IdLocalNegociacao




END

GO



 
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RV_COT') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RV_COT
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RV_COT]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(datetime, cot.Data) as BS_DATA,
		CONVERT(char(12), cot.CdAtivoBolsa) as BS_CODPAP,
		CONVERT(float, cot.PUMedio) as BS_PRECO,
		CONVERT(float, cot.PUFechamento) as BS_FECH,
		CONVERT(float, null) as BS_VL_PRECO_MIN,
		CONVERT(float, null) as BS_VL_PRECO_MAX,
		CONVERT(float, null) as BS_VL_FECH_RJ,
		CONVERT(float, null) as BS_VL_PRECO_MED_RJ,
		CONVERT(float, null) as BS_VL_PRECO_MIN_RJ,
		CONVERT(float, null) as BS_VL_PRECO_MAX_RJ,
		CONVERT(float, null) as BS_QT_RJ,
		CONVERT(float, null) as BS_QT_SP,
		CONVERT(datetime, null) as BS_DT_ULT_COT,
		CONVERT(char(1), null) as BS_SG_ORIGEM,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO
	FROM 
		CotacaoBolsa cot
	WHERE 
		cot.Data = @DataRef
END


GO

  IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RV_DIVIDENDO') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RV_DIVIDENDO
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RV_DIVIDENDO]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT
		CONVERT(numeric(9), prov.IdProvento) as BS_ID,
		CONVERT(datetime, prov.DataEx) as BS_DT_EX,
		CONVERT(char(12), prov.CdAtivoBolsa) as BS_RVPAP_CD,
		CONVERT(datetime, null) as BS_DT_POSICAO,
		CONVERT(datetime, null) as BS_DT_ASSEMBLEIA,
		CONVERT(datetime, null) as BS_DT_VALORIZACAO,
		CONVERT(char(1), null) as BS_IC_TIPO,
		CONVERT(float, null) as BS_VL,
		CONVERT(char(5), null) as BS_CLGIR_CD,
		CONVERT(char(5), null) as BS_CLGIR_CD_RENDIMENTO,
		CONVERT(char(8), null) as BS_IDDIR_CD,
		CONVERT(char(18), null) as BS_CD_DIVIDENDO,
		CONVERT(datetime, null) as BS_DT_EXERCICIO,
		CONVERT(char(5), null) as BS_CLGIR_CD_DIVIDENDO,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(numeric(9), null) as BS_ID_LOG_LIBERADO,
		CONVERT(numeric(9), null) as BS_ID_EVENTO,
		CONVERT(char(1), null) as BS_IC_TRUNCA,
		CONVERT(char(1), null) as BS_SG_RECAL_PARCE_DIVID,
		CONVERT(char(8), null) as BS_CD_LOCAL_CUSTODIA,
		CONVERT(char(25), null) as BS_CD_DISTRIBUICAO,
		CONVERT(datetime, null) as BS_DT_ANUN,
		CONVERT(varchar(20), null) as BS_DS_INF_ADICIONAL,
		CONVERT(varchar(5), null) as BS_SG_INDICADOR,
		CONVERT(char(1), null) as BS_IC_BONIF_DINHEIRO_SWIFT,
		CONVERT(char(1), null) as BS_IC_PROVISAO_NEG,
		CONVERT(datetime, null) as BS_DT_INI_RENDIMENTO,
		CONVERT(float, null) as BS_VL_RENDIMENTO_UNIT,
		CONVERT(datetime, null) as BS_DT_EX_ADIANTADO,
		CONVERT(datetime, null) as BS_DT_CONVOCACAO,
		CONVERT(char(1), null) as BS_SG_ASSEMBLEIA,
		CONVERT(char(1), null) as BS_SG_PAGTO,
		CONVERT(char(1), null) as BS_SG_DISTRIBUICAO,
		CONVERT(varchar(250), null) as BS_CD_DESCRICAO,
		CONVERT(char(3), null) as BS_CD_DISTRIBUICAO_CBLC,
		CONVERT(int, null) as BS_NR_SEQUENCIA_CBLC,
		CONVERT(numeric(9), null) as BS_NR_PROCESSO,
		@DataRef as DataRef
	FROM
		ProventoBolsa prov

END

GO


IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RV_DIVIDENDO_PAG_POS') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RV_DIVIDENDO_PAG_POS
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RV_DIVIDENDO_PAG_POS]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT
		CONVERT(datetime,@DataRef) as BS_DT,
		CONVERT(numeric(9), li.IdLiquidacao) as BS_ID,
		CONVERT(datetime, li.DataLancamento) as BS_DT_EX,
		CONVERT(char(15), cli.IdCliente) as BS_CLCLI_CD,
		CONVERT(datetime, li.DataVencimento) as BS_DT_PAGAMENTO,
		CONVERT(char(8), null) as BS_IDDIR_CD,
		CONVERT(float, 100) as BS_PC,
		CONVERT(float, 0) as BS_VL_IRRF_DIVIDENDO,
		CONVERT(float, null) as BS_VL_CORRECAO,
		CONVERT(float, null) as BS_VL_IRRF_CORRECAO,
		CONVERT(float, li.Valor) as BS_VL_BRUTO,
		CONVERT(float, 0) as BS_VL_RENDIMENTO,
		CONVERT(float, null) as BS_VL_IRRF_RENDIMENTO,
		CONVERT(float, 0) as BS_VL_IRRF,
		CONVERT(char(1), null) as BS_IC_ALTERADO,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(numeric(9), null) as BS_ID_LOG_LIBERADO,
		CONVERT(char(12), null) as BS_CD_CONTRAPARTE,
		CONVERT(float, null) as BS_VL_LIQUIDO,
		CONVERT(float, null) as BS_VL_LIQUIDO_ORIG,
		CONVERT(float, null) as BS_VL_BRUTO_ORIG,
		CONVERT(int, null) as BS_ID_PAGAMENTO,
		CONVERT(float, null) as BS_VL_IRRF_DIVIDENDO_ORIG,
		CONVERT(float, null) as BS_VL_CORRECAO_ORIG,
		CONVERT(float, null) as BS_VL_IRRF_CORRECAO_ORIG,
		CONVERT(float, null) as BS_VL_RENDIMENTO_ORIG,
		CONVERT(float, null) as BS_VL_IRRF_RENDIMENTO_ORIG,
		CONVERT(decimal(9), null) as BS_BATPLQFN_CD_CODTLF,
		CONVERT(char(1), null) as BS_IC_DIVERG_IR,
		CONVERT(float, null) as BS_PC_IR_CBLC,
		CONVERT(float, null) as BS_VL_DIFERENCA_IR,
		CONVERT(float, null) as BS_VL_IRRF_PROV_TAX_CLAIM
	FROM 
		Liquidacao li
	LEFT JOIN Cliente cli ON 
		cli.IdCliente = li.IdCliente
	WHERE 
		li.DataLancamento <= @DataRef AND
		li.DataVencimento > @DataRef AND 
		li.IdentificadorOrigem = 18

END

GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RV_DIVIDENDO_QTD_POS') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RV_DIVIDENDO_QTD_POS
GO

CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RV_DIVIDENDO_QTD_POS]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(datetime,	pBC.DataEx)	as	BS_DT,
		CONVERT(numeric(9),	pBC.IdProvento)	as	BS_ID,
		CONVERT(datetime,	pBC.DataEx)	as	BS_DT_EX,
		CONVERT(char(15),	cli.IdCliente)	as	BS_CLCLI_CD,
		CONVERT(char(12),	pBC.CdAtivoBolsa)	as	BS_RVPAP_CD,
		CONVERT(float,	pBC.Quantidade)	as	BS_QT,
		CONVERT(float,	null)	as	BS_QT_BLOQ,
		CONVERT(float,	null)	as	BS_QT_DISP,
		CONVERT(char(1),	null)	as	BS_IC_ALTERADO,
		CONVERT(char(1),	'S')	as	BS_IC_CALCULADO,
		CONVERT(float,	null)	as	BS_QT_ANTERIOR,
		CONVERT(float,	null)	as	BS_QT_ESTORNO,
		CONVERT(float,	null)	as	BS_QT_ACRESCIMO
	FROM 
		ProventoBolsaCliente pBC
	LEFT JOIN Cliente cli ON 
		cli.IdCliente = pBC.IdCliente
	WHERE 
		pbc.DataEx = @DataRef
END


GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RV_EMPRESTIMO_ACOES') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RV_EMPRESTIMO_ACOES
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RV_EMPRESTIMO_ACOES]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT
		CONVERT(int,		posEmpBol.IdOperacao)	as	BS_ID_MOVIMENTO,
		CONVERT(datetime,	posEmpBol.DataRegistro)	as	BS_DT_MOVIMENTO,
		CONVERT(int,		null)	as	BS_ID_OPERACAO_EMPR_ACOES,
		CONVERT(char(1),	null)	as	BS_SG_OPERACAO,
		CONVERT(char(1),	posEmpBol.PontaEmprestimo)	as	BS_IC_TIPO_MOVIMENTO,
		CONVERT(char(15),	posEmpBol.IdCliente)	as	BS_CLCLI_CD,
		CONVERT(char(12),	posEmpBol.CdAtivoBolsa)	as	BS_RVPAP_CD,
		CONVERT(float,	posEmpBol.Quantidade)	as	BS_QT_MOVIMENTADA,
		CONVERT(char(1),	null)	as	BS_IC_COTACAO_LIQUIDACAO,
		CONVERT(char(1),	null)	as	BS_IC_RECEBIMENTO_PERIODICO,
		CONVERT(char(1),	null)	as	BS_SG_PERIODICIDADE,
		CONVERT(int,		null)	as	BS_DD_DIAS_RECEBIMENTO,
		CONVERT(char(1),	null)	as	BS_IC_DEVOLUCAO_ANTECIPADA,
		CONVERT(datetime,	null)	as	BS_DT_DEVOLUCAO_ANTECIPADA,
		CONVERT(char(8),	posEmpBol.IdAgente)	as	BS_RVCOR_CD_AGENTE,
		CONVERT(char(8),	null)	as	BS_RVCOR_CD,
		CONVERT(float,	posEmpBol.TaxaComissao)	as	BS_VL_TX_COMISSAO,
		CONVERT(float,	posEmpBol.TaxaOperacao)	as	BS_VL_TX_REMUNERACAO,
		CONVERT(int,		null)	as	BS_DD_DIAS_PRAZO,
		CONVERT(datetime,	posEmpBol.DataVencimento)	as	BS_DT_VENCIMENTO,
		CONVERT(char(1),	null)	as	BS_IC_MTM,
		CONVERT(char(15),	null)	as	BS_SG_CURVA_MTM,
		CONVERT(int,		null)	as	BS_ID_OFERTA,
		CONVERT(datetime,	null)	as	BS_DT_OFERTA,
		CONVERT(int,		null)	as	BS_ID_BLOQUEIO,
		CONVERT(numeric(5),	null)	as	BS_ID_CONTA,
		CONVERT(int,		null)	as	BS_ID_MOV_ORIGINAL,
		CONVERT(char(2),	null)	as	BS_SG_STATUS_PROVENTO,
		CONVERT(int,		null)	as	BS_ID_MOV_ORIG_PROV,
		CONVERT(datetime,	null)	as	BS_DT_DEPOSITO,
		CONVERT(datetime,	null)	as	BS_DT_EX_PROVENTO,
		CONVERT(numeric(9),	posEmpBol.NumeroContrato)	as	BS_NO_CONTRATO,
		CONVERT(datetime,	null)	as	BS_DT_CRED_RECIBO,
		CONVERT(char(15),	null)	as	BS_CD_ESTRATEGIA_MOV,
		CONVERT(datetime,	null)	as	BS_DT_BASE_PROVENTO,
		CONVERT(decimal(9),	null)	as	BS_ID_MT,
		CONVERT(decimal(9),	null)	as	BS_ID_MT_SEQ_B,
		CONVERT(char(1),	null)	as	BS_IC_MOV_VINCULADO,
		CONVERT(int,		null)	as	BS_ID_MOV_ORIG_VINC,
		CONVERT(char(1),	posEmpBol.TipoEmprestimo)	as	BS_IC_EMPR_COMPULSORIO,
		CONVERT(char(1),	null)	as	BS_IC_EMPR_AUTOMATICO,
		CONVERT(char(1),	null)	as	BS_IC_EMPR_INADIMPLENTE,
		CONVERT(char(1),	null)	as	BS_IC_EMPR_ORIG_INADIMP,
		CONVERT(int,		null)	as	BS_ID_MOV_ORIG_INADIMP,
		CONVERT(char(1),	null)	as	BS_SG_CONVERSAO,
		CONVERT(float,	null)	as	BS_VL_PGTO_FRACAO,
		CONVERT(float,	null)	as	BS_QT_FRACAO,
		CONVERT(char(2),	null)	as	BS_ID_MOTIVO,
		CONVERT(char(8),	null)	as	BS_RVCOR_CD_CUST_FISICA,
		CONVERT(float,	null)	as	BS_VL_APR_POS_DTLIQ,
		CONVERT(float,	null)	as	BS_VL_APR_NEG_DTLIQ,
		CONVERT(float,	null)	as	BS_VL_APR_POS_ACUM_DTLIQ,
		CONVERT(float,	null)	as	BS_VL_APR_NEG_ACUM_DTLIQ,
		CONVERT(float,	null)	as	BS_VL_VAR_APROP_DTLIQ,
		@DataRef as DataRef
	FROM
		PosicaoEmprestimoBolsaHistorico posEmpBol	
	WHERE
		posEmpBol.DataHistorico = @DataRef
END


GO

  IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RV_EM_POS') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RV_EM_POS
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RV_EM_POS]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT
		CONVERT(datetime, posEmpH.DataHistorico) as BS_DT_POSICAO,
		CONVERT(int, posEmpH.IdOperacao) as BS_ID_MOVIMENTO,
		CONVERT(char(1), dp.CodigoExterno) as BS_SG_DOADOR_TOMADOR,
		CONVERT(int, null) as BS_QT_EMPRESTIMO,
		CONVERT(float, posEmpH.ValorBase/posEmpH.ValorBase) as BS_VL_PRECO_CONTRATADO,
		CONVERT(float, null) as BS_VL_PRECO_MEDIO,
		CONVERT(float, posEmpH.PUMercado) as BS_VL_PRECO_FECHAMENTO,
		CONVERT(float, null) as BS_VL_PRECO_COM_CORRETAGEM,
		CONVERT(float, posEmpH.ValorBase) as BS_VL_CONTRATADO,
		CONVERT(float, null) as BS_VL_MEDIO,
		CONVERT(float, posEmpH.ValorMercado) as BS_VL_FECHAMENTO,
		CONVERT(int, null) as BS_DD_PRAZO_A_DECORRER,
		CONVERT(char(2), null) as BS_SG_PROVENTO,
		CONVERT(int, null) as BS_ID_PROVENTO,
		CONVERT(datetime, null) as BS_DT_GERACAO_PROV,
		CONVERT(float, null) as BS_VL_APR_POS_ACUM,
		CONVERT(float, null) as BS_VL_APR_NEG_ACUM
	FROM
		PosicaoEmprestimoBolsaHistorico posEmpH
	LEFT JOIN DePara dp ON 
		dp.IdTipoDePara = -12 AND 
		dp.CodigoInterno = posEmpH.PontaEmprestimo
	WHERE
		posEmpH.DataHistorico = @DataRef

END


GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RV_EM_POS_COMISSAO') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RV_EM_POS_COMISSAO
GO

CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RV_EM_POS_COMISSAO]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(datetime,	posEmpBolH.DataHistorico)	as	BS_DT_POSICAO,
		CONVERT(int,	posEmpBolH.IdOperacao)	as	BS_ID_MOVIMENTO,
		CONVERT(float,	null)	as	BS_VL_TAXA_A_APROPRIAR,
		CONVERT(float,	posEmpBolH.ValorCorrigidoComissao - posEmpBolH.ValorBase)	as	BS_VL_TAXA_APROPRIADA,
		CONVERT(float,	null)	as	BS_VL_TOTAL
	FROM
		PosicaoEmprestimoBolsaHistorico posEmpBolH
	WHERE
		posEmpBolH.DataHistorico = @DataRef
END

GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RV_EM_POS_REMUNERACAO') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RV_EM_POS_REMUNERACAO
GO

CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RV_EM_POS_REMUNERACAO]  
	@DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT
		CONVERT(datetime,	posEmpBolH.DataHistorico)	as	BS_DT_POSICAO,
		CONVERT(int,	posEmpBolH.IdOperacao)	as	BS_ID_MOVIMENTO,
		CONVERT(float,	posEmpBolH.TaxaOperacao)	as	BS_PC_TAXA_BASE_CARTEIRA,
		CONVERT(float,	posEmpBolH.ValorCorrigidoJuros - posEmpBolH.ValorBase)	as	BS_VL_TAXA_APROP_NAO_LIQUIDADA,
		CONVERT(float,	null)	as	BS_VL_TAXA_A_APROPRIAR,
		CONVERT(float,	null)	as	BS_VL_TAXA_TOTAL,
		CONVERT(float,	posEmpBolH.TaxaOperacao)	as	BS_PC_TAXA_NEGOCIADA,
		CONVERT(float,	null)	as	BS_VL_ULTIMA_LIQUIDACAO,
		CONVERT(float,	null)	as	BS_VL_TOTAL_LIQUIDACOES,
		CONVERT(float,	null)	as	BS_VL_TAXA_APROP_TOTAL,
		CONVERT(float,	null)	as	BS_VL_AJUSTADA_MTM_BRUTA,
		CONVERT(float,	null)	as	BS_VL_IMPOSTO_RENDA,
		CONVERT(float,	null)	as	BS_VL_IOF,
		CONVERT(float,	null)	as	BS_VL_AJUSTADA_MTM_LIQUIDA
	FROM 
		PosicaoEmprestimoBolsaHistorico posEmpBolH
	WHERE
		posEmpBolH.DataHistorico = @DataRef

END

GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RV_EM_POS_TAXA_REGISTRO') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RV_EM_POS_TAXA_REGISTRO
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RV_EM_POS_TAXA_REGISTRO]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	
	SELECT
		CONVERT(datetime,	posEmpBolH.DataHistorico)	as	BS_DT_POSICAO,
		CONVERT(int,	posEmpBolH.IdOperacao)	as	BS_ID_MOVIMENTO,
		CONVERT(float,	null)	as	BS_VL_TAXA_A_APROPRIAR,
		CONVERT(float,	posEmpBolH.ValorCorrigidoCBLC - posEmpBolH.ValorBase)	as	BS_VL_TAXA_APROPRIADA,
		CONVERT(float,	null)	as	BS_VL_TOTAL
	FROM
		PosicaoEmprestimoBolsaHistorico posEmpBolH
	WHERE
		posEmpBolH.DataHistorico = @DataRef

END

GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RV_GANHO_LIQ_MOV') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RV_GANHO_LIQ_MOV
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RV_GANHO_LIQ_MOV]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT
		CONVERT(int,	opBolsa.IdOperacao)	as	BS_ID_ORDEM,
		CONVERT(datetime,	opBolsa.Data)	as	BS_DT,
		CONVERT(char(15),	opBolsa.IdCliente)	as	BS_CLCLI_CD,
		CONVERT(char(12),	opBolsa.CdAtivoBolsa)	as	BS_RVPAP_CD,
		CONVERT(char(1),	opBolsa.TipoOperacao)	as	BS_SG_TIPO_MOV,
		CONVERT(float,	opBolsa.Quantidade)	as	BS_QT,
		CONVERT(float,	opBolsa.Valor)	as	BS_VL_VENDA,
		CONVERT(float,	opBolsa.Corretagem+opBolsa.Emolumento+opBolsa.LiquidacaoCBLC+opBolsa.RegistroBolsa+opBolsa.RegistroCBLC)	as	BS_VL_CUSTO,
		CONVERT(float,	opBolsa.ResultadoRealizado)	as	BS_VL_GANHO_LIQ
	FROM OperacaoBolsa opBolsa
		LEFT JOIN Cliente cli ON 
			cli.IdCliente = opBolsa.IdCliente
	WHERE 
		opBolsa.Data = @DataRef

END

GO
--BS_RV_GANHO_LIQ_MOV	BS_ID_ORDEM	NO	int		OperacaoBolsa.IdOperacao		
--BS_RV_GANHO_LIQ_MOV	BS_DT	NO	datetime		OperacaoBolsa.Data		
--BS_RV_GANHO_LIQ_MOV	BS_CLCLI_CD	NO	char(15)		OperacaoBolsa.Cliente.Apelido + OperacaoBolsa.Cliente.idCliente		
--BS_RV_GANHO_LIQ_MOV	BS_RVPAP_CD	NO	char(12)		OperacaoBolsa.CdAtivoBolsa		
--BS_RV_GANHO_LIQ_MOV	BS_SG_TIPO_MOV	NO	char(1)		OperacaoBolsa.TipoOperacao		OperacaoBolsa.TipoOperacao = V
--BS_RV_GANHO_LIQ_MOV	BS_QT	NO	float		OperacaoBolsa.Quantidade		
--BS_RV_GANHO_LIQ_MOV	BS_VL_VENDA	NO	float		OperacaoBolsa.Valor		
--BS_RV_GANHO_LIQ_MOV	BS_VL_CUSTO	NO	float		OperacaoBolsa.Corretagem + .Emolumento + .LiquidacaoCBLC + .RegistroBolsa + .RegistroCBLC		
--BS_RV_GANHO_LIQ_MOV	BS_VL_GANHO_LIQ	NO	float		OperacaoBolsa.ResultadoRealizado		


  IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RV_MOV') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RV_MOV
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RV_MOV]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(int, op.IdOperacao) as BS_ORDEM,
		CONVERT(datetime, op.Data) as BS_DATAMOV,
		CONVERT(char(15), cli.IdCliente) as BS_CODCLI,
		CONVERT(char(8), ag.Nome) as BS_CODCOR,
		CONVERT(char(8), null) as BS_CODOPER,
		CONVERT(char(12), op.CdAtivoBolsa) as BS_CODPAP,
		CONVERT(char(2), 'SP') as BS_PRACA,
		CONVERT(char(1), op.TipoOperacao) as BS_COMPRAVENDA,
		CONVERT(float, op.Corretagem) as BS_CORCLI,
		CONVERT(float, 0) as BS_CORINTEGRALBANCO,
		CONVERT(float, op.Corretagem) as BS_CORRINTEGRALCOR,
		CONVERT(datetime, op.DataLiquidacao) as BS_DATALIB,
		CONVERT(float, 0) as BS_DEVCORBANCO,
		CONVERT(float, 0) as BS_DEVCORCLI,
		CONVERT(char(12), null) as BS_EVE_CODPAP,
		CONVERT(datetime, null) as BS_EVE_DATAEX,
		CONVERT(char(1), null) as BS_FLAGCOR,
		CONVERT(char(1), null) as BS_FLAGLIB,
		CONVERT(char(1), null) as BS_FLAGLIBFIN,
		CONVERT(char(1), 'N') as BS_IC_DAYTRADE,
		CONVERT(float, null) as BS_P_CUSTO_BANCO,
		CONVERT(float, null) as BS_P_CUSTO_CLI,
		CONVERT(float, null) as BS_P_CUSTO_FISC,
		CONVERT(float, null) as BS_P_CUSTO_HIST,
		CONVERT(float, null) as BS_PERCDEVBANCOCLI,
		CONVERT(float, null) as BS_PERCDEVCORBANCO,
		CONVERT(float, null) as BS_PRECO,
		CONVERT(float, op.Quantidade) as BS_QUANTIDADE,
		CONVERT(char(1), 0) as BS_TIPOMOV,
		CONVERT(float, op.Valor) as BS_VALOR,
		CONVERT(float, op.Emolumento) as BS_VL_EMOLUMENTO,
		CONVERT(float, 0) as BS_VL_TX_ANA,
		CONVERT(float, 0) as BS_VL_TX_ANA_CORRETORA,
		CONVERT(float, 0) as BS_VL_TX_OPCOES,
		CONVERT(float, null) as BS_VL_QTD_BLOQ,
		CONVERT(char(1), null) as BS_IC_IMPORTADA,
		CONVERT(char(6), null) as BS_CD_ORDEM,
		CONVERT(float, null) as BS_VL_SEQUENCIA,
		CONVERT(char(6), null) as BS_CD_NEGOC,
		CONVERT(char(2), null) as BS_CD_FILIAL,
		CONVERT(char(3), null) as BS_CD_PRODUTO,
		CONVERT(char(5), null) as BS_CD_CT_RESULTADO,
		CONVERT(char(12), null) as BS_CD_USER_TRADER,
		CONVERT(char(3), null) as BS_CD_LIVRO,
		CONVERT(char(3), null) as BS_CD_CARTEIRA,
		CONVERT(char(3), null) as BS_CD_ENTIDADE,
		CONVERT(char(12), null) as BS_CD_PAPEL,
		CONVERT(char(5), null) as BS_CD_PACOTE,
		CONVERT(char(3), null) as BS_CD_ESTRATEGIA,
		CONVERT(char(8), null) as BS_CD_CLIENTE_ING,
		CONVERT(char(8), null) as BS_CD_CORR_ING,
		CONVERT(float, null) as BS_VL_PERCENT_RISCO,
		CONVERT(char(1), null) as BS_ST_OPER_ESPECIAL,
		CONVERT(datetime, null) as BS_DT_NEGOC,
		CONVERT(char(3), null) as BS_CD_ENTIDADE_RISCO,
		CONVERT(char(1), null) as BS_ST_RISCO_PAIS,
		CONVERT(char(1), null) as BS_ST_RISCO_TRANSFER,
		CONVERT(char(1), null) as BS_TP_OPERACAO,
		CONVERT(varchar(16), null) as BS_CD_MOEDA,
		CONVERT(char(5), null) as BS_CD_CORR,
		CONVERT(float, null) as BS_VL_PRECO_LOTE,
		CONVERT(float, null) as BS_VL_QTD_LOTE,
		CONVERT(char(1), null) as BS_IC_EXPORTADA_IT,
		CONVERT(char(10), null) as BS_NO_NOTA,
		CONVERT(char(8), null) as BS_RVCOR_CD_AGENTE,
		CONVERT(float, 0) as BS_VL_EMOLUMENTO_AGENTE,
		CONVERT(float, 0) as BS_VL_TX_OPCOES_AGENTE,
		CONVERT(float, null) as BS_VL_EMOLUMENTO_BANCO,
		CONVERT(float, null) as BS_VL_TX_OPCOES_BANCO,
		CONVERT(float, null) as BS_VL_COR_FIXA,
		CONVERT(numeric(9), null) as BS_ID_MT,
		CONVERT(numeric(9), null) as BS_ID_MT_SEQ_B,
		CONVERT(float, null) as BS_VL_IR_DAYTRADE,
		CONVERT(char(1), null) as BS_IC_IRRF_VIRTUAL,
		CONVERT(numeric(9), null) as BS_ID_SCPRIV,
		CONVERT(char(8), null) as BS_RVCOR_CD_FISICA,
		CONVERT(char(14), null) as BS_CD_OPER_INTERNO,
		CONVERT(char(1), null) as BS_IC_LIQ_FISICA,
		CONVERT(datetime, op.Data) as BS_DT_INPUT,
		CONVERT(datetime, null) as BS_DT_PREVLIQ,
		CONVERT(char(7), null) as BS_NO_OPBOLSA,
		CONVERT(char(3), null) as BS_CD_SUBSEG_SPC,
		CONVERT(char(1), null) as BS_SG_ALOCACAO,
		CONVERT(int, null) as BS_ID_MOV_PLANO,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO,
		CONVERT(char(10), null) as BS_CD_HOTKEY_DC,
		CONVERT(char(1), null) as BS_IC_TIPO_SEG,
		CONVERT(char(1), null) as BS_IC_NETBOL,
		CONVERT(char(1), null) as BS_IC_MOV_AUTOMATICO,
		CONVERT(float, null) as BS_VL_EQUALIZACAO,
		CONVERT(float, null) as BS_VL_EMOLUMENTO_FA,
		CONVERT(float, null) as BS_VL_TX_REG_FA,
		CONVERT(float, null) as BS_VL_TX_REG_FA_AGENTE,
		CONVERT(float, null) as BS_VL_TX_LIQ_FA_AGENTE,
		CONVERT(char(8), null) as BS_BALOCUST_CD_CODLCC,
		CONVERT(char(1), null) as BS_TP_LIQUI,
		CONVERT(char(8), null) as BS_BACLR_CD_CODCLR,
		CONVERT(char(10), null) as BS_CD_MERCADO_EXT,
		CONVERT(float, null) as BS_PC_DEVOLCORRBCO,
		CONVERT(float, null) as BS_PC_DEVOLCORRCLI,
		CONVERT(datetime, null) as BS_DT_PAG_JUROS_3,
		CONVERT(datetime, null) as BS_DT_PAG_JUROS_4,
		CONVERT(datetime, null) as BS_DT_PAG_JUROS_5,
		CONVERT(datetime, null) as BS_DT_PAG_JUROS_6,
		CONVERT(datetime, null) as BS_DT_PAG_JUROS_7,
		CONVERT(datetime, null) as BS_DT_PAG_JUROS_8,
		CONVERT(datetime, null) as BS_DT_PAG_JUROS_9,
		CONVERT(datetime, null) as BS_DT_PAG_JUROS_10,
		CONVERT(datetime, null) as BS_DT_PAG_JUROS_11,
		CONVERT(datetime, null) as BS_DT_PAG_JUROS_12,
		CONVERT(float, null) as BS_QT_EMPRESTIMO,
		CONVERT(float, null) as BS_VL_RESULTADO,
		CONVERT(float, null) as BS_VL_COR_LANC,
		CONVERT(char(1), null) as BS_SG_TIPO_COR_LANC,
		CONVERT(float, null) as BS_VL_EMOL_LANC,
		CONVERT(char(1), null) as BS_SG_TIPO_EMOL_LANC,
		CONVERT(char(8), null) as BS_CD_BOLSA,
		CONVERT(char(1), null) as BS_IC_TRUNCA_ARREDONDA_COR_RV,
		CONVERT(char(1), null) as BS_IC_TRUNCA_ARREDONDA_EMOL_LANC,
		CONVERT(numeric(5), null) as BS_ID_CONTA,
		CONVERT(char(1), null) as BS_IC_CONVERTE_COTAS,
		CONVERT(float, null) as BS_VL_IRRF_ON,
		CONVERT(float, null) as BS_VL_TX_SERVICO,
		CONVERT(datetime, null) as BS_DT_LIQ_FIS_SWIFT,
		CONVERT(int, null) as BS_ID_ORDEM_ORIGEM,
		CONVERT(float, null) as BS_QT_AUTO_EA,
		CONVERT(char(1), null) as BS_IC_DAYTRADE_GANHO_LIQ,
		CONVERT(datetime, null) as BS_EVE_DT_PAGAMENTO,
		CONVERT(datetime, null) as BS_EVE_DT_CRED_RECIBO,
		CONVERT(datetime, null) as BS_EVE_DT_LIM_EMP,
		CONVERT(char(15), null) as BS_CD_ESTRATEGIA_MOV,
		CONVERT(char(1), null) as BS_IC_BAIXA_DIRSUB,
		CONVERT(char(1), null) as BS_IC_AFTER_MARKET,
		CONVERT(int, null) as BS_ID_ORDEM_FILHA,
		CONVERT(float, null) as BS_ID_ORDEM_REV_MOV,
		CONVERT(float, null) as BS_QT_ORIGINAL,
		CONVERT(float, null) as BS_VL_ORIGINAL,
		CONVERT(char(1), null) as BS_IC_MOV_DAYTRADE_AUT,
		CONVERT(char(7), null) as BS_CD_NEGOCIO_BVP,
		CONVERT(float, null) as BS_VL_PGTO_FRACAO,
		CONVERT(float, null) as BS_VL_FRACAO,
		CONVERT(char(3), null) as BS_CD_DISTRIBUICAO_CBLC,
		CONVERT(int, null) as BS_NR_SEQUENCIA_CBLC,
		CONVERT(char(1), null) as BS_SG_CONVERSAO,
		CONVERT(char(2), null) as BS_CD_MOTIVO_TRANSF,
		CONVERT(int, null) as BS_ID_PROV_TRANSF,
		CONVERT(char(2), null) as BS_ID_MOTIVO,
		CONVERT(char(1), null) as BS_IC_PROV_POS_VENDIDA
	FROM 
		OperacaoBolsa op
	LEFT JOIN Cliente cli ON	
		cli.IdCliente = op.IdCliente
	LEFT JOIN AgenteMercado ag ON
		ag.IdAgente = op.IdAgenteCorretora
	WHERE
		op.Data = @DataRef
END

GO

  IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_RV_POS') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_RV_POS
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_RV_POS]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT
		CONVERT(datetime, posBoH.DataHistorico) as BS_DATAPOS,
		CONVERT(char(15), posBoH.IdCliente) as BS_CODCLI,
		CONVERT(char(8), ' ') as BS_CODOPER,
		CONVERT(char(12), posBoH.CdAtivoBolsa) as BS_CODPAP,
		CONVERT(char(8), ag.Nome) as BS_CODCOR,
		CONVERT(char(2), 'SP') as BS_PRACA,
		CONVERT(float, posBoH.QuantidadeBloqueada) as BS_QTDBLOQ,
		CONVERT(float, posBoH.Quantidade) as BS_QTDDISP,
		CONVERT(int, null) as BS_SEQ_EXP,
		CONVERT(float, posBoH.ValorMercado) as BS_VL_MERC,
		CONVERT(float, null) as BS_VL_IRRF,
		CONVERT(float, posBoH.ValorMercado) as BS_VL_MERC_LIQUIDO,
		CONVERT(char(1), null) as BS_IC_TIPO_SEG,
		CONVERT(float, 0) as BS_VL_EQUALIZACAO,
		CONVERT(float, 0) as BS_VL_VALORIZACAO,
		CONVERT(char(1), null) as BS_SG_ALOCACAO,
		CONVERT(float, 0) as BS_QT_BLOQ_01,
		CONVERT(float, 0) as BS_QT_BLOQ_03,
		CONVERT(float, 0) as BS_QT_BLOQ_05,
		CONVERT(float, 0) as BS_QT_BLOQ_14,
		CONVERT(float, null) as BS_PRZ_MEDIO_POSICAO_ATIVO,
		CONVERT(float, null) as BS_VL_PATRLIQMED90 
	FROM PosicaoBolsaHistorico posBoH
	LEFT JOIN AgenteMercado ag ON
		ag.IdAgente = posBoH.IdAgente
	LEFT JOIN Cliente cli ON 
		cli.IdCliente = posBoH.IdCliente
	LEFT JOIN MediaMovel mm ON 
		mm.IdCliente = posBoH.IdCliente AND
		mm.DataAtual = posBoH.DataHistorico	AND
		mm.TipoMediaMovel = 1 AND
		mm.ChaveComposta = posBoH.CdAtivoBolsa + CAST(posBoH.IdAgente AS VARCHAR(MAX))
	WHERE posBoH.DataHistorico = @DataRef
END

GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_SW_CAD') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_SW_CAD

GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_SW_CAD]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT
		CONVERT(char(5),	opSw.IdOperacao)	as	BS_CD,
		CONVERT(datetime,	opSw.DataVencimento)	as	BS_DT_VENCIMENTO,
		CONVERT(char(8),	ag.IdAgente)	as	BS_RVCOR_CD,
		CONVERT(datetime,	opsw.DataEmissao)	as	BS_DT_PARTIDA,
		CONVERT(float,	case opSw.TipoPonta when 1 then opSw.TaxaJurosContraParte else opSw.TaxaJuros end )	as	BS_PC_TAXA_PASSIVO,
		CONVERT(char(8),	case opSw.TipoPonta when 1 then opSw.IdIndice else opSw.IdIndiceContraParte end)	as	BS_IDDIR_CD_PASSIVO,
		CONVERT(char(1),	case opSw.TipoPonta when 1 then opSw.TipoApropriacao else opSw.TipoApropriacaoContraParte end)	as	BS_IC_JUROS_PASSIVO,
		CONVERT(char(1),	case opSw.TipoPonta when 1 then opSw.TipoApropriacao else opSw.TipoApropriacaoContraParte end)	as	BS_IC_APROP_PASSIVO,
		CONVERT(char(1),	case opSw.TipoPonta when 1 then opSw.ContagemDias else opSw.ContagemDiasContraParte end)	as	BS_IC_DIAS_PASSIVO,
		CONVERT(char(1),	null)	as	BS_IC_INDICE_PASSIVO,
		CONVERT(float,	case opSw.TipoPonta when 1 then opSw.TaxaJurosContraParte else opSw.TaxaJuros end )		as	BS_PC_TAXA_ATIVO,
		CONVERT(char(8),	case opSw.TipoPonta when 1 then opSw.IdIndice else opSw.IdIndiceContraParte end)		as	BS_IDDIR_CD_ATIVO,
		CONVERT(char(1),	case opSw.TipoPonta when 1 then opSw.TipoApropriacao else opSw.TipoApropriacaoContraParte end)	as	BS_IC_JUROS_ATIVO,
		CONVERT(char(1),	case opSw.TipoPonta when 1 then opSw.TipoApropriacao else opSw.TipoApropriacaoContraParte end)	as	BS_IC_APROP_ATIVO,
		CONVERT(char(1),	case opSw.TipoPonta when 1 then opSw.ContagemDias else opSw.ContagemDiasContraParte end)		as	BS_IC_DIAS_ATIVO,
		CONVERT(char(1),	null)	as	BS_IC_INDICE_ATIVO,
		CONVERT(char(1),	'N')	as	BS_IC_GARANTIA,
		CONVERT(char(8),	null)	as	BS_RVCOR_CD_CONTRA,
		CONVERT(float,	null)	as	BS_PC_FLOOR,
		CONVERT(char(8),	null)	as	BS_IDDIR_CD_FLOOR,
		CONVERT(char(1),	null)	as	BS_IC_JUROS_FLOOR,
		CONVERT(char(1),	null)	as	BS_IC_APROP_FLOOR,
		CONVERT(char(1),	null)	as	BS_IC_DIAS_FLOOR,
		CONVERT(char(1),	null)	as	BS_IC_INDICE_FLOOR,
		CONVERT(char(1),	null)	as	BS_SG_MTM_PASSIVO,
		CONVERT(char(1),	null)	as	BS_SG_MTM_ATIVO,
		CONVERT(char(1),	null)	as	BS_SG_MTM_FLOOR,
		CONVERT(float,	null)	as	BS_PC_MTM_PASSIVO,
		CONVERT(float,	null)	as	BS_PC_MTM_ATIVO,
		CONVERT(float,	null)	as	BS_PC_MTM_FLOOR,
		CONVERT(char(1),	null)	as	BS_IC_LIQ_FIN_D0_D1,
		CONVERT(char(1),	null)	as	BS_SWTP_CD,
		CONVERT(char(8),	null)	as	BS_CD_CETIP,
		CONVERT(char(5),	null)	as	BS_CD_LOCAL_REGISTRO,
		CONVERT(char(8),	'Real')	as	BS_BAMDA_CD,
		CONVERT(int,	null)	as	BS_DD_DEFASAGEM_CM_ATIVO,
		CONVERT(int,	null)	as	BS_DD_DEFASAGEM_CM_PASSIVO,
		CONVERT(int,	null)	as	BS_DD_DEFASAGEM_CM_FLOOR,
		CONVERT(int,	null)	as	BS_DD_UTEIS_PARTIDA_VENCTO,
		CONVERT(char(1),	'N')	as	BS_IC_HEDGE,
		CONVERT(datetime,	null)	as	BS_DT_INI_VALORIZACAO,
		CONVERT(char(15),	null)	as	BS_CD_MTM_PRICE_A,
		CONVERT(char(15),	null)	as	BS_CD_MTM_PRICE_P,
		CONVERT(char(15),	null)	as	BS_CD_MTM_PRICE_F,
		CONVERT(int,	null)	as	BS_DD_BASE_JUROS_ATIVO,
		CONVERT(int,	null)	as	BS_DD_BASE_JUROS_PASSIVO,
		CONVERT(int,	null)	as	BS_DD_BASE_JUROS_FLOOR,
		CONVERT(char(1),	null)	as	BS_IC_TAXA_CDI_ATIVO,
		CONVERT(float,	null)	as	BS_IDDIR_PC_ATIVO,
		CONVERT(char(1),	null)	as	BS_IC_TAXA_CDI_PASSIVO,
		CONVERT(float,	null)	as	BS_IDDIR_PC_PASSIVO,
		CONVERT(char(1),	null)	as	BS_IC_TAXA_CDI_FLOOR,
		CONVERT(float,	null)	as	BS_IDDIR_PC_FLOOR,
		CONVERT(char(1),	null)	as	BS_IC_LOG_LIBERADO,
		CONVERT(int,	null)	as	BS_ID_LOG_LIBERADO,
		CONVERT(int,	null)	as	BS_CODTLF,
		CONVERT(char(1),	null)	as	BS_IC_ABERT_INDEX,
		CONVERT(char(8),	null)	as	BS_IDDIR_CD_PASSIVO_ABERT_INDEX,
		CONVERT(char(8),	null)	as	BS_IDDIR_CD_ATIVO_ABERT_INDEX,
		CONVERT(char(12),	null)	as	BS_CD_ISIN,
		CONVERT(char(1),	null)	as	BS_IC_COTACAO_REFERENCIAL,
		CONVERT(float,	null)	as	BS_VL_COTACAO_INICIAL_A,
		CONVERT(float,	null)	as	BS_VL_COTACAO_INICIAL_P,
		CONVERT(float,	null)	as	BS_VL_COTACAO_INICIAL_F,
		CONVERT(char(1),	null)	as	BS_SG_IMPORTADO,
		CONVERT(char(3),	null)	as	BS_CD_SUBSEG_SPC,
		CONVERT(char(8),	null)	as	BS_CD_PRACA_NEG,
		CONVERT(char(1),	null)	as	BS_IC_NOVO_CALC_TX_PERM,
		CONVERT(CHAR(20), 'S') AS BS_SG_LOCAL_CUSTODIA,
		CONVERT(CHAR(8),   'S')	as	BS_CD_GRUPO_ECONOMICO,		
		@DataRef as DataRef
	FROM
		OperacaoSwap opSw
	LEFT JOIN AgenteMercado ag ON
		ag.IdAgente = opSw.IdAgente

END


GO

  IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_SW_MOV') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_SW_MOV
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_SW_MOV]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT
		CONVERT(int, op.IdOperacao) as BS_ID,
		CONVERT(datetime, op.DataEmissao) as BS_DT,
		CONVERT(char(5), op.IdOperacao) as BS_SWCAD_CD,
		CONVERT(char(10), null) as BS_RFOP_CD,
		CONVERT(char(15), cli.IdCliente) as BS_CLCLI_CD,
		CONVERT(float, op.ValorBase) as BS_QT,
		CONVERT(float, 0) as BS_VL_APROP_BRUTO,
		CONVERT(float, 0) as BS_VL_APROP_IRRF,
		CONVERT(float, 0) as BS_VL_APROP_LIQ,
		CONVERT(float, null) as BS_VL_APROP_LIQ_CALC,
		CONVERT(float, op.Emolumento) as BS_VL_EMOL,
		CONVERT(char(8), null) as BS_NO_COMANDO,
		CONVERT(char(10), null) as BS_CD_LOCAL_LIQ,
		CONVERT(char(8), null) as BS_CD_LIQUIDANTE,
		CONVERT(char(1), null) as BS_IC_EXPORTADA_IT,
		CONVERT(float, op.ValorBase) as BS_VL_PASSIVO,
		CONVERT(float, op.ValorBase) as BS_VL_PASSIVO_CALC,
		CONVERT(float, op.ValorBase) as BS_VL_ATIVO,
		CONVERT(float, op.ValorBase) as BS_VL_ATIVO_CALC,
		CONVERT(float, 0) as BS_VL_APROP_IRRF_CALC,
		CONVERT(float, op.ValorBase) as BS_VL_APROP_BRUTO_CALC,
		CONVERT(char(1), null) as BS_IC_IRRF_VIRTUAL,
		CONVERT(char(14), null) as BS_CD_OPERACAO_INTERNO,
		CONVERT(char(14), null) as BS_CD_OPER_INTERNO,
		CONVERT(char(11), null) as BS_CD_CONTRATO,
		CONVERT(char(1), null) as BS_IC_EXPORTADA_SPR,
		CONVERT(char(9), null) as BS_CD_CONTRAPARTE_CETIP_SELIC,
		CONVERT(char(1), null) as BS_SG_ALOCACAO,
		CONVERT(int, null) as BS_ID_MOV_PLANO,
		CONVERT(char(4), null) as BS_CD_SUBSEGTO,
		CONVERT(datetime, null) as BS_DT_TRANSFER,
		CONVERT(char(1), dp.CodigoExterno) as BS_IC_TIPO_MOV,
		CONVERT(char(1), null) as BS_IC_LOG_LIBERADO,
		CONVERT(int, null) as BS_ID_LOG_LIBERADO,
		CONVERT(char(3), null) as BS_CD_SUBSEG_SPC,
		CONVERT(char(1), null) as BS_IC_NETBOL,
		CONVERT(char(8), null) as BS_CODCLR,
		CONVERT(datetime, null) as BS_HR_PARTIDA,
		CONVERT(int, null) as BS_NO_PRIORIDADE,
		CONVERT(char(1), null) as BS_CD_PRIORIDADE,
		CONVERT(char(1), null) as BS_IC_NEG_VENC,
		CONVERT(char(1), null) as BS_IC_MTM_RF,
		CONVERT(char(10), null) as BS_CD_MERCADO_EXT,
		CONVERT(float, op.Registro) as BS_VL_TXREG,
		CONVERT(float, null) as BS_VL_COTACAO_LIQUIDACAO_A,
		CONVERT(float, null) as BS_VL_COTACAO_LIQUIDACAO_P,
		CONVERT(float, null) as BS_VL_COTACAO_LIQUIDACAO_F,
		CONVERT(char(1), null) as BS_IC_ABERT_INDEX,
		CONVERT(numeric(5), null) as BS_ID_CONTA,
		CONVERT(char(1), null) as BS_SG_IMPORTADO,
		CONVERT(char(1), null) as BS_IC_MOV_AUTOMATICO,
		CONVERT(datetime, null) as BS_DT_INTEGRACAO_NETBOL,
		CONVERT(char(8), null) as BS_CD_BOLSA,
		CONVERT(char(15), null) as BS_CD_ESTRATEGIA_MOV
	FROM 
		OperacaoSwap op
	LEFT JOIN cliente cli ON 
		cli.IdCliente = op.IdCliente
	LEFT JOIN DePara dp ON 
		dp.IdTipoDePara = -10 AND 
		dp.CodigoInterno = op.TipoPonta
	WHERE
		op.DataEmissao = @DataRef

END


GO


IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_SW_POS') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_SW_POS

GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_SW_POS]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT 
		CONVERT(datetime,	posSw.DataHistorico)	as	BS_DT,
		CONVERT(char(15),	cli.IdCliente)	as	BS_CLCLI_CD,
		CONVERT(char(5),	posSw.IdOperacao)	as	BS_SWCAD_CD,
		CONVERT(float,	posSw.ValorBase)	as	BS_QT,
		CONVERT(float,	0)	as	BS_PU_PASSIVO,
		CONVERT(float,	0)	as	BS_PU_ATIVO,
		CONVERT(float,	0)	as	BS_PU_FLOOR,
		CONVERT(float,   posSw.ValorContraParte)	as	BS_VL_PASSIVO,
		CONVERT(float,	posSw.ValorParte)	as	BS_VL_ATIVO,
		CONVERT(float,	posSw.ValorContraParte)	as	BS_VL_FLOOR,
		CONVERT(float,	posSw.ValorContraParte)	as	BS_VL_PASSIVO_CURVA,
		CONVERT(float,	posSw.ValorParte)	as	BS_VL_ATIVO_CURVA,
		CONVERT(float,	posSw.ValorContraParte)	as	BS_VL_FLOOR_CURVA,
		CONVERT(float,	posSw.Saldo)	as	BS_VL_APROP_BRUTO,
		CONVERT(float,	posSw.ValorIR)	as	BS_VL_APROP_IRRF,
		CONVERT(float,	posSw.Saldo - posSw.ValorIR)	as	BS_VL_APROP_LIQ,
		CONVERT(float,	0)	as	BS_VL_COR_CAMBIO,
		CONVERT(int,(
			SELECT 
				COUNT(*) 
			FROM 
				fn_DiasUteis(@DataRef,posSw.DataVencimento,1)
		))	as	BS_DD_UTEIS_ATE_VENCTO,
		CONVERT(float,	null)	as	BS_VL_RCDI,
		CONVERT(float,	0)	as	BS_VL_PASSIVO_VENCIMENTO,
		CONVERT(float,	0)	as	BS_VL_ATIVO_VENCIMENTO,
		CONVERT(float,	0)	as	BS_VL_FLOOR_VENCIMENTO,
		CONVERT(float,	posSw.ValorParte)	as	BS_VL_ATIVO_BMF,
		CONVERT(float,	posSw.ValorContraParte)	as	BS_VL_PASSIVO_BMF,
		CONVERT(float,	posSw.ValorContraParte)	as	BS_VL_FLOOR_BMF,
		CONVERT(char(1),	0)	as	BS_SG_ALOCACAO,
		CONVERT(float,	posSw.ValorParte)	as	BS_VL_ATIVO_NEG_VENC,
		CONVERT(float,	posSw.ValorContraParte)	as	BS_VL_PASSIVO_NEG_VENC,
		CONVERT(float,	posSw.ValorContraParte)	as	BS_VL_FLOOR_MTM,
		CONVERT(float,	posSw.ValorParte)	as	BS_VL_ATIVO_MTM,
		CONVERT(float,	posSw.ValorContraParte)	as	BS_VL_PASSIVO_MTM,
		CONVERT(float,	posSw.ValorContraParte)	as	BS_VL_FLOOR_NEG_VENC,
		CONVERT(float,	0)	as	BS_PC_TIR_ATIVO,
		CONVERT(float,	0)	as	BS_PC_TIR_PASSIVO,
		CONVERT(numeric(5),	0)	as	BS_ID_CONTA,
		CONVERT(float,	0)	as	BS_PC_IRRF1,
		CONVERT(float,	0)	as	BS_PC_IRRF2,
		CONVERT(float,	0)	as	BS_VL_BASE_IRRF1,
		CONVERT(float,	0)	as	BS_VL_BASE_IRRF2,
		CONVERT(float,	0)	as	BS_VL_IRRF1,
		CONVERT(float,	0)	as	BS_VL_IRRF2,
		CONVERT(char(1),	'N')	as	BS_SG_IMPORTADO,
		CONVERT(float,	0)	as	BS_VL_OUTROS_CUSTOS
	FROM 
		PosicaoSwapHistorico posSw
	LEFT JOIN Cliente cli ON 
		cli.IdCliente = posSw.IdCliente
	LEFT JOIN PrazoMedio pm ON 
		pm.IdCliente = posSw.IdCliente AND 
		pm.DataAtual = posSw.DataHistorico AND
		pm.MercadoAtivo = 6 AND
		pm.IdPosicao = posSw.IdPosicao
	LEFT JOIN MediaMovel mm ON 
		mm.IdCliente = posSw.IdCliente AND
		mm.DataAtual = posSw.DataHistorico AND
		mm.TipoMediaMovel = 5 AND
		mm.idPosicao = posSw.IdPosicao
	WHERE 
		posSw.DataHistorico = @DataRef
END


GO
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_XRATE') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_XRATE
GO

CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_XRATE]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT
		CONVERT(char(8),	moedaDe.Nome)	as	BS_CODMOEDA1,
		CONVERT(char(8),	moedaPara.Nome)	as	BS_CODMOEDA2,
		CONVERT(char(8),	ind.Descricao)	as	BS_INDEXADOR,
		CONVERT(char(1),	null)	as	BS_IC_LOG_LIBERADO,
		CONVERT(int,	null)	as	BS_ID_LOG_LIBERADO,
		@DataRef as DataRef
	FROM 
		ConversaoMoeda cm
	LEFT JOIN Moeda moedaDe ON
		moedaDe.IdMoeda = cm.IdMoedaDe
	LEFT JOIN Moeda moedaPara ON 
		moedaPara.IdMoeda = cm.IdMoedaPara
	LEFT JOIN Indice ind ON 
		ind.IdIndice = cm.IdIndice

END

GO

IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_INDEX_CAD') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_INDEX_CAD
GO

CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_INDEX_CAD]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN
	SELECT
		CONVERT(char(8),	ind.IdIndice)			as	BS_CODINDEX,
		CONVERT(varchar(20),	ind.Descricao)		as	BS_NOME,
		CONVERT(char(1),	tipo.CodigoExterno)		as	BS_TIPODM,
		CONVERT(char(1),	null)					as	BS_TIPOUC,
		CONVERT(char(1),	'N')					as	BS_TIPONCPD,
		CONVERT(char(1),	tp.CodigoExterno)		as	BS_TIPOINC,
		CONVERT(float,	null)						as	BS_BASE,
		CONVERT(varchar(255),	' ')				as	BS_FORMULA,
		CONVERT(char(10),	null)					as	BS_CD_ATIVO_RISCO,
		CONVERT(char(1),	null)					as	BS_IC_COTA_FUNDO,
		CONVERT(varchar(1),	null)					as	BS_IC_IDX_INDEXDIR,
		CONVERT(varchar(1),	null)					as	BS_IC_IDX_MERCADO,
		CONVERT(char(8),	null)					as	BS_IDDIR_CD_OBJETO,
		CONVERT(float,	null)						as	BS_PC_DIARIA,
		CONVERT(float,	null)						as	BS_PC_PERIODO,
		CONVERT(char(3),	null)					as	BS_CD_INDEX_BMF,
		CONVERT(char(3),	null)					as	BS_CD_INDEX_CETIP,
		CONVERT(char(1),	null)					as	BS_IC_OPERACAO_GARANTIA,
		CONVERT(char(1),	null)					as	BS_IC_LOG_LIBERADO,
		CONVERT(int,	null)						as	BS_ID_LOG_LIBERADO,
		CONVERT(char(8),	null)					as	BS_CD_INDEX_LH,
		CONVERT(int,	null)						as	BS_ID_LOCAL,
		CONVERT(char(2),	null)					as	BS_SG_TIPO_APROP,
		@DataRef as DataRef
	FROM
		Indice ind
	LEFT JOIN DePara tipo ON
		tipo.IdTipoDePara = -22 AND 
		tipo.CodigoInterno = ind.TipoDivulgacao
	LEFT JOIN DePara tp ON
		tipo.IdTipoDePara = -23 AND 
		tipo.CodigoInterno = ind.Tipo
END
	

	

GO



IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_BA_EQUIVALENCIAS') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_BA_EQUIVALENCIAS
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_BA_EQUIVALENCIAS]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

	SELECT
		CONVERT(char(20),	'_CLASSIF_ANBIMA')	as	BS_CD_EQUIVALENCIA,
		CONVERT(char(10),	'LINK')				as	BS_CD_CONTRAPARTE,
		CONVERT(char(40),	cf.Descricao)		as	BS_CD_SISTEMA_DESTINO,
		CONVERT(char(40),	cf.Descricao)		as	BS_CD_SISTEMA_ORIGEM,
		CONVERT(char(40),	cf.Descricao)		as	BS_DS_SISTEMA_ORIGEM,
		CONVERT(char(1),	'S')				as	BS_IC_LOG_LIBERADO,
		@DataRef as DataRef
	FROM
		CategoriaFundo cf
	UNION
	SELECT
		CONVERT(char(20),	cct.NomeLista)	as	BS_CD_EQUIVALENCIA,
		CONVERT(char(10),	'LINK')				as	BS_CD_CONTRAPARTE,
		CONVERT(char(40),	cci.ItemLista)		as	BS_CD_SISTEMA_DESTINO,
		CONVERT(char(40),	cci.ItemLista)		as	BS_CD_SISTEMA_ORIGEM,
		CONVERT(char(40),	cci.ItemLista)		as	BS_DS_SISTEMA_ORIGEM,
		CONVERT(char(1),	'S')				as	BS_IC_LOG_LIBERADO,
		@DataRef as DataRef
	FROM
		CadastroComplementarItemLista cci
	LEFT JOIN CadastroComplementarTipoLista cct on 
		cct.IdTipoLista = cci.IdTipoLista
	UNION
	SELECT
		CONVERT(char(20),	'_TIPO_PAPEL_ATIVO')	as	BS_CD_EQUIVALENCIA,
		CONVERT(char(10),	'LINK')				as	BS_CD_CONTRAPARTE,
		CONVERT(char(40),	dp.CodigoExterno)		as	BS_CD_SISTEMA_DESTINO,
		CONVERT(char(40),	dp.CodigoExterno)		as	BS_CD_SISTEMA_ORIGEM,
		CONVERT(char(40),	dp.CodigoExterno)		as	BS_DS_SISTEMA_ORIGEM,
		CONVERT(char(1),	'S')				as	BS_IC_LOG_LIBERADO,
		@DataRef as DataRef
	FROM
		DePara dp
	WHERE
		dp.IdTipoDepara = -24
	
		


	
END

GO

IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'usp_EXTRACT_BS_ENQUADRAMENTO') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE usp_EXTRACT_BS_ENQUADRAMENTO
GO


CREATE PROCEDURE[dbo].[usp_EXTRACT_BS_ENQUADRAMENTO]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

Select 
	R.IdResultado AS BS_ID,
	e.IdCarteira as BS_CODCLI,	
	R.ValorCriterio as BS_CRITERIO, 
	R.ValorBase AS BS_BASE,
	R.Resultado AS BS_RESULTADO, 
	CASE WHEN R.Enquadrado = 'S' then 'ENQUADRADO' ELSE 'DESENQUADRADO' END as BS_Enquadramento,
	E.Descricao AS BS_DESCRICAO,
	TipoEnquadramentoDePara.CodigoExterno as BS_TipoEnquadramento, 	
	TipoRegraDePara.CodigoExterno as BS_TipoRegra,
	E.Minimo AS BS_MINIMO, 
	E.Maximo AS BS_MAXIMO,
	@DataRef AS DataRef	
From   
	EnquadraResultado R
LEFT JOIN EnquadraRegra E ON 
	E.IdRegra = R.IdRegra
LEFT JOIN DePara TipoEnquadramentoDePara ON
	TipoEnquadramentoDePara.IdTipoDePara = (select IdTipoDePara from TipoDePara where Descricao = 'TipoEnquadramentoEnquadra') AND
	TipoEnquadramentoDePara.CodigoInterno = E.TipoEnquadramento
LEFT JOIN DePara TipoRegraDePara ON
	TipoRegraDePara.IdTipoDePara = (select IdTipoDePara from TipoDePara where Descricao = 'TipoRegraEnquadra') AND
	TipoRegraDePara.CodigoInterno = E.TipoRegra

END

GO


