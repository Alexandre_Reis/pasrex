declare @data_versao char(10)
set @data_versao = '2014-03-17'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table enquadraitemopcaobolsa add Posicao char(1) null
go
alter table enquadraitemtermobolsa add Posicao char(1) null
go
alter table enquadraitemfuturoBMF add Posicao char(1) null
go