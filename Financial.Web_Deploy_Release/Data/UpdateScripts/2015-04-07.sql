declare @data_versao char(10)
set @data_versao = '2015-04-07'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'Recon') = 0
BEGIN
CREATE TABLE [dbo].[Recon](
	[IdRecon] [int] IDENTITY(1,1) NOT NULL,
	[DataRegistro] [datetime] NOT NULL,
	[DataInicioProcesso] [datetime] NULL,
	[DataFimProcesso] [datetime] NULL,
	[IdUsuario] [int] NOT NULL,
	[Observacao] [varchar](200) NULL,
	[DataCancelamento] [datetime] NULL,
	[Status] [varchar](8000) NULL,
	[MessageId] [varchar](50) NULL,
 CONSTRAINT [PK_Recon] PRIMARY KEY CLUSTERED 
(
	[IdRecon] ASC
))
END
GO




