﻿declare @data_versao char(10)
set @data_versao = '2013-05-14'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Acesso à nova entrada para Ajuste Origem Liquidacao (contabil)
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,13150,'N','S','S','S' from grupousuario where idgrupo <> 0
go
