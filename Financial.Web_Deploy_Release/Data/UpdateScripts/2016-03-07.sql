﻿declare @data_versao char(10)
set @data_versao = '2016-03-07'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Atenção este script ja foi executado anteriormente.',16,1)

	
	-- não mostra a execução do script
	set nocount on 
	
	-- não roda o script
	set noexec on 
	

end

begin transaction

	if (not exists (select * from sys.all_columns where name = 'ValidaDigitoConta' and OBJECT_NAME(object_id) = 'ContabPlano'))
	begin
		ALTER TABLE ContabPlano ADD  ValidaDigitoConta char(1) not null default 'N'
	end

	if (not exists (select * from sys.all_columns where name = 'Digito' and OBJECT_NAME(object_id) = 'ContabConta'))
	begin
		ALTER TABLE ContabConta ADD  Digito char(1) null
	end


-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off



