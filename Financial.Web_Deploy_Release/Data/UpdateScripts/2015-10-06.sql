﻿declare @data_versao char(10)
set @data_versao = '2015-10-06'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
BEGIN TRANSACTION


	--garante que só sera commitado caso não aconteça nenhum erro
	set xact_abort on
	insert into VersaoSchema values(@data_versao);

	if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'DsLocalDivulg')
	BEGIN
		ALTER TABLE dbo.InformacoesComplementaresFundo ADD DsLocalDivulg varchar(300) NOT NULL;
	END	
	GO 
	
	if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'DsResp')
	BEGIN
		ALTER TABLE dbo.InformacoesComplementaresFundo ADD DsResp varchar(250) NOT NULL;
	END	
	GO 
	
	if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'CodVotoGestAssemb')
	BEGIN
		ALTER TABLE dbo.InformacoesComplementaresFundo ADD CodVotoGestAssemb varchar(1) NOT NULL;
	END	
	GO 
	
	if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'AgencClassifRatin')
	BEGIN
		ALTER TABLE dbo.InformacoesComplementaresFundo ADD AgencClassifRatin varchar(1) NOT NULL;
	END	
	GO 
	
	if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'ApresDetalheAdm')
	BEGIN
		ALTER TABLE dbo.InformacoesComplementaresFundo ADD ApresDetalheAdm varchar(2500) NOT NULL;
	END	
	GO 
	
	if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'DsServicoPrestado')
	BEGIN
		ALTER TABLE dbo.InformacoesComplementaresFundo ADD DsServicoPrestado varchar(100) NOT NULL;
	END	
	GO 
	
	if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'CodDistrOfertaPub')
	BEGIN
		ALTER TABLE dbo.InformacoesComplementaresFundo ADD CodDistrOfertaPub varchar(1) NOT NULL;
	END	
	GO 
	
	if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'InformAutoregulAnbima')
	BEGIN
		ALTER TABLE dbo.InformacoesComplementaresFundo ADD InformAutoregulAnbima varchar(2500) NOT NULL;
	END	
	GO 
	
	if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'CodMeioDivulg')
	BEGIN
		ALTER TABLE dbo.InformacoesComplementaresFundo ADD CodMeioDivulg varchar(1) NOT NULL;
	END	
	GO 
	
	if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'CodMeio')
	BEGIN
		ALTER TABLE dbo.InformacoesComplementaresFundo ADD CodMeio varchar(1) NOT NULL;
	END	
	GO 
	
	if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'DsLocal')
	BEGIN
		ALTER TABLE dbo.InformacoesComplementaresFundo ADD DsLocal varchar(100) NOT NULL;
	END	
	GO 
	
	if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'NrCnpj')
	BEGIN
		ALTER TABLE dbo.InformacoesComplementaresFundo ADD NrCnpj varchar(14) NOT NULL;
	END	
	GO 
	
	if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'NmPrest')
	BEGIN
		ALTER TABLE dbo.InformacoesComplementaresFundo ADD NmPrest varchar(100) NOT NULL;
	END	
	GO 
	
	if not exists(select 1 from syscolumns where id = object_id('InformacoesComplementaresFundo') and name = 'DisclAdvert')
	BEGIN
		ALTER TABLE dbo.InformacoesComplementaresFundo ADD DisclAdvert varchar(100) NOT NULL;
	END	
	GO 
	
COMMIT TRANSACTION

GO	

