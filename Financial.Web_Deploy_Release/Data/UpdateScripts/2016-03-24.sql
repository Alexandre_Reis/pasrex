declare @data_versao char(10)
set @data_versao = '2016-03-24'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  n�o permite que seja executado novamente, avisa o usuario e 
*  n�o exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Aten��o este script ja foi executado anteriormente.',16,1)

	
	-- n�o mostra a execu��o do script
	set nocount on 
	
	-- n�o roda o script
	set noexec on 
	

end

begin transaction

	-- n�o permite que a transaction seja commitada em caso de erros
	set xact_abort on

	insert into VersaoSchema values(@data_versao)
	
	if( exists(select * from sysobjects where id = object_id('ST_RELATORIO')))
	begin
	
		
		UPDATE	ST_RELATORIO
		SET		DS_OBJETO_CONTEUDO = '(11)3366-3370'
		where	CD_RELATORIO = 'CENTRAL_ATEND'
		and		CD_OBJETO = 'CENTRAL'
		
	end	

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off