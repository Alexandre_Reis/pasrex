declare @data_versao char(10)
set @data_versao = '2014-04-17'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

ALTER TABLE Cotista ADD DataExpiracao datetime NULL, PendenciaCadastral varchar(200) NULL

alter table permissaooperacaofundo add Email varchar(200) null

CREATE TABLE [dbo].[LimiteOperacaoUsuario](
	[IdUsuario] [int] NOT NULL,
	[MaximoAplicacao] [decimal](16, 2) NULL,
	[MaximoResgate] [decimal](16, 2) NULL,
 CONSTRAINT [PK_LimiteOperacaoUsuario] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LimiteOperacaoUsuario]  WITH CHECK ADD  CONSTRAINT [FK_LimiteOperacaoUsuario_Usuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuario] ([IdUsuario])
ON DELETE CASCADE
GO


