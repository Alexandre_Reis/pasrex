declare @data_versao char(10)
set @data_versao = '2015-06-26'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
BEGIN TRANSACTION
insert into VersaoSchema VALUES(@data_versao)
CREATE TABLE QueryCustom
(
	IdQuery int PRIMARY KEY IDENTITY,
	Nome Varchar(40) NOT NULL,
	Descricao varchar(255),
	QueryCustom varchar(8000) NOT NULL
)



COMMIT TRANSACTION

