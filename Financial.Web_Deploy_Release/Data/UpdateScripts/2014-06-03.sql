declare @data_versao char(10)
set @data_versao = '2014-06-03'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


alter table EvolucaoCapitalSocial add Tipo tinyint null
go
update EvolucaoCapitalSocial set Tipo = 3 --Outros
go
alter table EvolucaoCapitalSocial alter column Tipo tinyint not null
go