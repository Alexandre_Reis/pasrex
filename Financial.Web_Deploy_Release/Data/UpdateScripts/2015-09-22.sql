﻿declare @data_versao char(10)
set @data_versao = '2015-09-22'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityValidacao') = 0
begin
	CREATE TABLE SuitabilityValidacao
	(
		[IdValidacao] int IDENTITY(1,1) NOT NULL,
		[Descricao] varchar(100) NOT NULL
		CONSTRAINT PK_SuitabilityValidacao primary key(IdValidacao)
	)
end
GO

if(select count(1) from SuitabilityValidacao) = 0
begin
	insert into SuitabilityValidacao values ('Pessoa Física');
	insert into SuitabilityValidacao values ('Pessoa Jurídica');
	insert into SuitabilityValidacao values ('Fundos');
	insert into SuitabilityValidacao values ('Clubes de Investimento');
end

delete from PermissaoMenu where idMEnu = 1280
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1280
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1230;
GO

--Adicionar atributo
if not exists(select 1 from syscolumns where id = object_id('SuitabilityQuestao') and name = 'IdValidacao')
BEGIN
	ALTER TABLE SuitabilityQuestao ADD IdValidacao int null
END
GO

--Criar tabelas de historico
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityQuestaoHistorico') = 0
begin
	CREATE TABLE SuitabilityQuestaoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdQuestao] [int] NOT NULL,
		[Descricao] [varchar](2000) NOT NULL,
		[Fator] [int] NOT NULL,
		[IdValidacao] [int] NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityQuestaoHistorico primary key(DataHistorico, IdQuestao)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityOpcaoHistorico') = 0
begin
	CREATE TABLE SuitabilityOpcaoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdOpcao] [int] IDENTITY(1,1) NOT NULL,
		[IdQuestao] [int] NOT NULL,
		[Descricao] [varchar](2000) NOT NULL,
		[Pontos] [int] NOT NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityOpcaoHistorico primary key(DataHistorico, IdOpcao)
	)
end
GO

--Acesso a nova tela de tipo de dispensa
delete from PermissaoMenu where idMEnu = 1230
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1230
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1205;
GO

--Criar tabelas Tipo de Dispensa
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityTipoDispensa') = 0
begin
	CREATE TABLE SuitabilityTipoDispensa
	(
		[IdDispensa] int IDENTITY(1,1) NOT NULL,
		[Descricao] [varchar](100) NOT NULL,
		CONSTRAINT PK_SuitabilityTipoDispensa primary key(IdDispensa)
	)
end
GO

if(select count(1) from SuitabilityTipoDispensa) = 0
begin
	insert into SuitabilityTipoDispensa (Descricao) values ('Pessoas habilitadas a atuar como integrantes do sistema de distribuição');
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityTipoDispensaHistorico') = 0
begin
	CREATE TABLE SuitabilityTipoDispensaHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdDispensa] int NOT NULL,
		[Descricao] [varchar](100) NOT NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityTipoDispensaHistorico primary key(DataHistorico, IdDispensa)
	)
end
GO

--Acesso a nova tela de tipo de dispensa
delete from PermissaoMenu where idMEnu = 1235
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1235
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1205;
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityPerfilInvestidor') = 0
begin
	CREATE TABLE SuitabilityPerfilInvestidor
	(
		[IdPerfilInvestidor] int IDENTITY(1,1) NOT NULL,
		[Nivel] int NOT NULL default 0,
		[Perfil] [varchar](20) NOT NULL,
		CONSTRAINT PK_SuitabilityPerfilInvestidor primary key(IdPerfilInvestidor)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityPerfilInvestidorHistorico') = 0
begin
	CREATE TABLE SuitabilityPerfilInvestidorHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdPerfilInvestidor] int NOT NULL,
		[Nivel] int NOT NULL default 0,
		[Perfil] [varchar](20) NOT NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityPerfilInvestidorHistorico primary key(DataHistorico, IdPerfilInvestidor)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametroPerfil') = 0
begin
	CREATE TABLE SuitabilityParametroPerfil
	(
		IdParametroPerfil int IDENTITY(1,1) NOT NULL,
		IdValidacao int NOT NULL,
		PerfilInvestidor int NOT NULL,
		Definicao text NULL,
		FaixaPontuacao int NULL,
		CONSTRAINT PK_SuitabilityParametroPerfil primary key(IdParametroPerfil)
	)
	alter table SuitabilityParametroPerfil add constraint SuitabilityParametroPerfil_SuitabilityValidacao_FK FOREIGN KEY ( IdValidacao ) references SuitabilityValidacao(IdValidacao) ON DELETE CASCADE; 
	alter table SuitabilityParametroPerfil add constraint SuitabilityParametroPerfil_SuitabilityPerfilInvestidor_FK FOREIGN KEY ( PerfilInvestidor ) references SuitabilityPerfilInvestidor(IdPerfilInvestidor) ON DELETE CASCADE; 
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametroPerfilHistorico') = 0
begin
	CREATE TABLE SuitabilityParametroPerfilHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		IdParametroPerfil int NOT NULL,
		IdValidacao int NOT NULL,
		PerfilInvestidor int NOT NULL,
		Definicao text NULL,
		FaixaPontuacao int NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametroPerfilHistorico primary key(DataHistorico, IdParametroPerfil)
	)
end
GO

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'PerfilRisco')
BEGIN
	Alter Table Carteira add PerfilRisco int NULL;
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CarteiraSuitabilityHistorico') = 0
begin
	CREATE TABLE CarteiraSuitabilityHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdCarteira] int NOT NULL,
		[PerfilRisco] int NOT NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_CarteiraSuitabilityHistorico primary key(DataHistorico, IdCarteira)
	)	
end
GO


delete from PermissaoMenu where idMEnu = 4880
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,4880
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 4870;
GO


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityApuracaoRisco') = 0
begin
	CREATE TABLE SuitabilityApuracaoRisco
	(
		[IdApuracaoRisco] int IDENTITY(1,1) NOT NULL,
		[IdValidacao] int NOT NULL,
		[PerfilCarteira] int NOT NULL,
		[PerfilRiscoFundo] varchar(100) NOT NULL,
		[Criterio] int NOT NULL,
		[Percentual] int NOT NULL default 0,
		CONSTRAINT PK_SuitabilityApuracaoRisco primary key(IdApuracaoRisco)
	)
	
	alter table SuitabilityApuracaoRisco add constraint SuitabilityApuracaoRisco_SuitabilityPerfilInvestidor_FK FOREIGN KEY ( PerfilCarteira ) references SuitabilityPerfilInvestidor(IdPerfilInvestidor); 
	alter table SuitabilityApuracaoRisco add constraint SuitabilityApuracaoRisco_SuitabilityValidacao_FK FOREIGN KEY ( IdValidacao ) references SuitabilityValidacao(IdValidacao); 
end
GO



IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityApuracaoRiscoHistorico') = 0
begin
	CREATE TABLE SuitabilityApuracaoRiscoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdApuracaoRisco] int NOT NULL,
		[IdValidacao] int NOT NULL,
		[PerfilCarteira] int NOT NULL,
		[PerfilRiscoFundo] varchar(100) NOT NULL,
		[Criterio] int NOT NULL,
		[Percentual] int NOT NULL default 0,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityApuracaoRiscoHistorico primary key(DataHistorico, IdApuracaoRisco)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityMensagens') = 0
begin
	CREATE TABLE SuitabilityMensagens
	(
		[IdMensagem] int IDENTITY(1,1) NOT NULL,
		[Assunto] varchar(100) NOT NULL,
		[Descricao] text NOT NULL,
		[ControlaConcordancia] char(1) NOT NULL default 'N',
		CONSTRAINT PK_SuitabilityMensagens primary key(IdMensagem)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityMensagensHistorico') = 0
begin
	CREATE TABLE SuitabilityMensagensHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdMensagem] int NOT NULL,
		[Assunto] varchar(100) NOT NULL,
		[Descricao] text NOT NULL,
		[ControlaConcordancia] char(1) NOT NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityMensagensHistorico primary key(DataHistorico, IdMensagem)
	)
end
GO

delete from PermissaoMenu where idMEnu = 1240
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1240
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1215;
GO


declare @data_versao char(10)
set @data_versao = '2015-09-04'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

delete from PermissaoMenu where idMEnu = 1250
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1250
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1240;
GO


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametrosWorkflow') = 0
begin
	CREATE TABLE SuitabilityParametrosWorkflow
	(
		[IdParametrosWorkflow] int NOT NULL default 1,
		[AtivaWorkFlow] char(1) NOT NULL default 'N',
		[VerificaEnquadramento] int NULL,
		[ResponsavelNome] varchar(50) NULL,
		[ResponsavelEmail] varchar(100) NULL,
		[ValorPeriodo] int NULL,
		[GrandezaPeriodo] int NULL,
		CONSTRAINT PK_SuitabilityParametrosWorkflow primary key(IdParametrosWorkflow)
	)
	
end
GO



IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametrosWorkflowHistorico') = 0
begin
	CREATE TABLE SuitabilityParametrosWorkflowHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdParametrosWorkflow] int NOT NULL default 1,
		[AtivaWorkFlow] char(1) NOT NULL default 'N',
		[VerificaEnquadramento] int NULL,
		[ResponsavelNome] varchar(50) NULL,
		[ResponsavelEmail] varchar(100) NULL,
		[ValorPeriodo] int NULL,
		[GrandezaPeriodo] int NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametrosWorkflowHistorico primary key(DataHistorico, IdParametrosWorkflow)
	)
	
end
GO

if(select count(1) from SuitabilityParametrosWorkflow) = 0
begin
	insert into SuitabilityParametrosWorkflow (AtivaWorkFlow) Values('N')
end
go

if(select count(1) from SuitabilityParametrosWorkflowHistorico) = 0
begin
	insert into SuitabilityParametrosWorkflowHistorico (DataHistorico, AtivaWorkFlow, Tipo) Values (getdate(), 'N', 'Insert')
end
go

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityEventos') = 0
begin
	CREATE TABLE SuitabilityEventos(
		[IdSuitabilityEventos] [int] NOT NULL,
		[Descricao] [varchar](100) NOT NULL,
		[IdMensagem] [int] NULL,
	 CONSTRAINT [PK_SuitabilityEventos] PRIMARY KEY (IdSuitabilityEventos))
	 
	 alter table SuitabilityEventos add constraint SuitabilityEventos_SuitabilityMensagens_FK FOREIGN KEY ( IdMensagem ) references SuitabilityMensagens(IdMensagem); 
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityEventosHistorico') = 0
begin
	CREATE TABLE SuitabilityEventosHistorico(
		[DataHistorico] [datetime] NOT NULL,
		[IdSuitabilityEventos] [int] NOT NULL,
		[Descricao] [varchar](100) NOT NULL,
		[IdMensagem] [int] NULL,
		[Tipo] [varchar](10) NOT NULL,
	 CONSTRAINT [PK_SuitabilityEventosHistorico] PRIMARY KEY (DataHistorico, IdSuitabilityEventos))
end

if (select count(1) from SuitabilityEventos) = 0
begin
	insert into SuitabilityEventos values (1, 'Apuração de novo perfil de Investidor (Portal)', null);
	insert into SuitabilityEventos values (2, 'Declaração sobre as informações prestadas (Portal)', null);
	insert into SuitabilityEventos values (3, 'Aceite do perfil apurado', null);
	insert into SuitabilityEventos values (4, 'Aceite do perfil apurado (Portal)', null);
	insert into SuitabilityEventos values (5, 'Aceite do tipo de dispensa', null);
	insert into SuitabilityEventos values (6, 'Aceite do tipo de dispensa (Portal)', null);
	insert into SuitabilityEventos values (7, 'Recusa do preenchimento do formulário', null);
	insert into SuitabilityEventos values (8, 'Recusa do preenchimento do formulário (Portal)', null);
	insert into SuitabilityEventos values (9, 'Questionário não preenchido (Portal)', null);
	insert into SuitabilityEventos values (10, 'Atualizar perfil (Portal)', null);
	insert into SuitabilityEventos values (11, 'Inexistência de Perfil (Portal)', null);
	insert into SuitabilityEventos values (12, 'Perfil desatualizado (Portal)', null);
	insert into SuitabilityEventos values (13, 'Desenquadramento - Alteração de perfil', null);
	insert into SuitabilityEventos values (14, 'Desenquadramento - Alteração de perfil (Portal)', null);
	insert into SuitabilityEventos values (15, 'Termo de Ciência (Portal)', null);
	insert into SuitabilityEventos values (16, 'Termo de Ciência de Risco e Adesão ao Fundo', null);
	insert into SuitabilityEventos values (17, 'Termo de Ciência de Risco e Adesão ao Fundo (Portal)', null);
	insert into SuitabilityEventos values (18, 'Termo de Inadequação', null);
	insert into SuitabilityEventos values (19, 'Termo de Inadequação (Portal)', null);
end

if (select count(1) from SuitabilityEventosHistorico) = 0
begin
	insert into SuitabilityEventosHistorico values (getdate(),1, 'Apuração de novo perfil de Investidor (Portal)', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),2, 'Declaração sobre as informações prestadas (Portal)', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),3, 'Aceite do perfil apurado', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),4, 'Aceite do perfil apurado (Portal)', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),5, 'Aceite do tipo de dispensa', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),6, 'Aceite do tipo de dispensa (Portal)', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),7, 'Recusa do preenchimento do formulário”', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),8, 'Recusa do preenchimento do formulário (Portal)', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),9, 'Questionário não preenchido (Portal)', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),10, 'Atualizar perfil (Portal)', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),11, 'Inexistência de Perfil (Portal)', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),12, 'Perfil desatualizado (Portal)', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),13, 'Desenquadramento - Alteração de perfil', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),14, 'Desenquadramento - Alteração de perfil (Portal)', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),15, 'Termo de Ciência (Portal)', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),16, 'Termo de Ciência de Risco e Adesão ao Fundo', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),17, 'Termo de Ciência de Risco e Adesão ao Fundo (Portal)', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),18, 'Termo de Inadequação', null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),19, 'Termo de Inadequação (Portal)', null, 'Insert');
end


delete from PermissaoMenu where idMEnu = 1260
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1260
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1250;
GO

delete from PermissaoMenu where idMEnu = 1270
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1270
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1250;
GO


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityRespostaCotista') = 0
begin
	CREATE TABLE SuitabilityRespostaCotista(
		[Data] datetime NOT NULL,
		[IdCotista] int NOT NULL,
		[Status] int NOT NULL,
	 CONSTRAINT [PK_SuitabilityRespostaCotista] PRIMARY KEY (Data, IdCotista))
	 
	 alter table SuitabilityRespostaCotista add constraint SuitabilityRespostaCotista_Cotista_FK FOREIGN KEY ( IdCotista ) references Cotista(IdCotista) on delete cascade; 
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityRespostaCotistaDetalhe') = 0
begin
	CREATE TABLE SuitabilityRespostaCotistaDetalhe(
		[Data] datetime NOT NULL,
		[IdCotista] int NOT NULL,
		[IdQuestao] int NOT NULL,
		[IdOpcao] int NOT NULL,
	 CONSTRAINT [PK_SuitabilityRespostaCotistaDetalhe] PRIMARY KEY (Data, IdCotista, IdQuestao))
	 
	 alter table SuitabilityRespostaCotistaDetalhe add constraint SuitabilityRespostaCotistaDetalhe_SuitabilityRespostaCotista_FK FOREIGN KEY ( Data, IdCotista ) references SuitabilityRespostaCotista(Data, IdCotista) on delete cascade; 
	 alter table SuitabilityRespostaCotistaDetalhe add constraint SuitabilityRespostaCotistaDetalhe_Cotista_FK FOREIGN KEY ( IdCotista ) references Cotista(IdCotista); 
	 alter table SuitabilityRespostaCotistaDetalhe add constraint SuitabilityRespostaCotistaDetalhe_SuitabilityQuestao_FK FOREIGN KEY ( IdQuestao ) references SuitabilityQuestao(IdQuestao) on delete cascade; 
	 alter table SuitabilityRespostaCotistaDetalhe add constraint SuitabilityRespostaCotistaDetalhe_SuitabilityOpcao_FK FOREIGN KEY ( IdOpcao ) references SuitabilityOpcao(IdOpcao); 
end


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityLogMensagens') = 0
begin
	CREATE TABLE SuitabilityLogMensagens(
		[IdLogMensagem] int IDENTITY(1,1) NOT NULL,
		[Data] datetime NOT NULL,
		[Usuario] varchar(100) NOT NULL,
		[IdCotista] int NOT NULL,
		[IdMensagem] int NOT NULL,
		[Mensagem] text NOT NULL,
		[Resposta] varchar(10) NOT NULL
	 CONSTRAINT [PK_SuitabilityLogMensagens] PRIMARY KEY (IdLogMensagem))
	 
	 alter table SuitabilityLogMensagens add constraint SuitabilityLogMensagens_Cotista_FK FOREIGN KEY ( IdCotista ) references Cotista(IdCotista) on delete cascade; 
	 alter table SuitabilityLogMensagens add constraint SuitabilityLogMensagens_SuitabilityMensagens_FK FOREIGN KEY ( IdMensagem ) references SuitabilityMensagens(IdMensagem) on delete cascade; 
end


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PessoaSuitability') = 0
begin
	CREATE TABLE PessoaSuitability
	(
		[IdPessoa] int NOT NULL,
		[IdValidacao] int NULL,
		[Perfil] int NULL,
		[Recusa] char(1) NOT NULL default 'N',
		[Dispensado] char(1) NOT NULL default 'N',
		[Dispensa] int NULL,
		[UltimaAlteracao] datetime NULL,
		CONSTRAINT PK_PessoaSuitability primary key(IdPessoa)
	)
	
	alter table PessoaSuitability add constraint PessoaSuitability_Pessoa_FK FOREIGN KEY ( IdPessoa ) references Pessoa(IdPessoa) on delete cascade; 
	alter table PessoaSuitability add constraint PessoaSuitability_TipoDispensa_FK FOREIGN KEY ( Dispensa ) references SuitabilityTipoDispensa(IdDispensa); 
	alter table PessoaSuitability add constraint PessoaSuitability_PerfilInvestidor_FK FOREIGN KEY ( Perfil ) references SuitabilityPerfilInvestidor(IdPerfilInvestidor); 
	alter table PessoaSuitability add constraint PessoaSuitability_Validacao_FK FOREIGN KEY ( IdValidacao ) references SuitabilityValidacao(IdValidacao); 
	
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PessoaSuitabilityHistorico') = 0
begin
	CREATE TABLE PessoaSuitabilityHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdPessoa] int NOT NULL,
		[IdValidacao] int NULL,
		[Perfil] int NULL,
		[Recusa] char(1) NOT NULL default 'N',
		[Dispensado] char(1) NOT NULL default 'N',
		[Dispensa] int NULL,
		[UltimaAlteracao] datetime NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_PessoaSuitabilityHistorico primary key(DataHistorico, IdPessoa)
	)	
end
GO

delete from PermissaoMenu where idMEnu = 1290
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1290
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1250;
GO