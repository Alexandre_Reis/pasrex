declare @data_versao char(10)
set @data_versao = '2016-03-18'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  n�o permite que seja executado novamente, avisa o usuario e 
*  n�o exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Aten��o este script ja foi executado anteriormente.',16,1)

	
	-- n�o mostra a execu��o do script
	set nocount on 
	
	-- n�o roda o script
	set noexec on 
	

end

begin transaction

	-- n�o permite que a transaction seja commitada em caso de erros
	set xact_abort on

	insert into VersaoSchema values(@data_versao)

	--DML
	
	IF EXISTS(select 1 from syscolumns where id = object_id('ST_RELATORIO'))
	BEGIN
		update st_relatorio set DS_OBJETO_CONTEUDO = 'Rentabilidade Hist�rica' where Cd_Relatorio = '3.3.0.0'
		--update ST_RELATORIO set DS_OBJETO_CONTEUDO = '0800-727-1184 | (11) 3366-3184' where DS_OBJETO_CONTEUDO = '0800-727-1184 | 33663184'
	END

	--

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off