declare @data_versao char(10)
set @data_versao = '2013-12-30'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table papelrendafixa add CodigoInterface varchar(6) null
go
alter table titulorendafixa add CodigoInterface varchar(6) null
go
alter table emissor add CodigoInterface varchar(11) null
go

alter table posicaorendafixa add IdIndiceVolta smallint null
go
alter table posicaorendafixahistorico add IdIndiceVolta smallint null
go
alter table posicaorendafixaabertura add IdIndiceVolta smallint null
go


