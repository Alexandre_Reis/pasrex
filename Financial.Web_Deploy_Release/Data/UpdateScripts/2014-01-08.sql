declare @data_versao char(10)
set @data_versao = '2014-01-08'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

drop table PosicaoBolsaDetalheHistorico
go
drop table PosicaoBolsaDetalhe
go


CREATE TABLE PosicaoBolsaDetalhe(
	DataHistorico datetime NOT NULL,
	IdCliente int NOT NULL,
	IdAgente int NOT NULL,	
	CdAtivoBolsa varchar(20) NOT NULL,
	TipoCarteira int NOT NULL,
	Quantidade decimal(16, 2) NOT NULL	
 CONSTRAINT PosicaoBolsaDetalhe_PK PRIMARY KEY CLUSTERED 
(
	DataHistorico ASC,
	IdCliente ASC,
	IdAgente ASC,
	CdAtivoBolsa ASC,
	TipoCarteira ASC
)
)

ALTER TABLE PosicaoBolsaDetalhe ADD CONSTRAINT AgenteMercado_PosicaoBolsaDetalhe_FK1 FOREIGN KEY(IdAgente)
REFERENCES AgenteMercado (IdAgente)
ON DELETE CASCADE
GO

ALTER TABLE PosicaoBolsaDetalhe  WITH CHECK ADD  CONSTRAINT AtivoBolsa_PosicaoBolsaDetalhe_FK1 FOREIGN KEY(CdAtivoBolsa)
REFERENCES AtivoBolsa (CdAtivoBolsa)
ON DELETE CASCADE
GO

ALTER TABLE PosicaoBolsaDetalhe  WITH CHECK ADD  CONSTRAINT Cliente_PosicaoBolsaDetalhe_FK1 FOREIGN KEY(IdCliente)
REFERENCES Cliente (IdCliente)
ON DELETE CASCADE
GO


alter table bloqueiobolsa add TipoCarteiraDestino int null
go

