declare @data_versao char(10)
set @data_versao = '2014-07-23'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

ALTER TABLE ContaCorrente ALTER COLUMN Numero VARCHAR(12)