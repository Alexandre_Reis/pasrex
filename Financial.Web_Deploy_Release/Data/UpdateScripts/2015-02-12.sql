declare @data_versao char(10)
set @data_versao = '2015-02-12'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table permissaooperacaofundo add SituacaoEnvio tinyint null
go
update permissaooperacaofundo set SituacaoEnvio = 1
go
alter table permissaooperacaofundo alter column SituacaoEnvio tinyint not null
go

ALTER TABLE PermissaoOperacaoFundo ADD CONSTRAINT DF_SituacaoEnvio DEFAULT 1 FOR SituacaoEnvio