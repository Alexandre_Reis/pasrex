﻿declare @data_versao char(10)
set @data_versao = '2015-12-02'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Atenção este script ja foi executado anteriormente.',16,1)

	
	-- não mostra a execução do script
	set nocount on 
	
	-- não roda o script
	set noexec on 
	

end

begin transaction

	-- não permite que a transaction seja commitada em caso de erros
	set xact_abort on

	insert into VersaoSchema values(@data_versao)

	IF EXISTS (SELECT NAME FROM SYSOBJECTS WHERE NAME = 'trg_cliente_dataatualizacao' AND TYPE = 'TR')
	BEGIN 
		DROP TRIGGER trg_cliente_dataatualizacao
	END
	
	
	IF NOT EXISTS(SELECT * FROM SYSCOLUMNS WHERE NAME = 'DataEnvioCetip' AND ID = OBJECT_ID('Cliente'))
	BEGIN
		alter table Cliente add DataEnvioCetip Datetime NULL
	END


-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off

