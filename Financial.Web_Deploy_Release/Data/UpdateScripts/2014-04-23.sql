declare @data_versao char(10)
set @data_versao = '2014-04-23'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Menu Tabela Posicao Renda Fixa Detalhe Carteira
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,3850,'S','S','S','S' from grupousuario where idgrupo <> 0

--Menu Tabela Rebate por Distribuidor
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,9035,'S','S','S','S' from grupousuario where idgrupo <> 0


