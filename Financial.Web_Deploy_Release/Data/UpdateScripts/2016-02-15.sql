declare @data_versao char(10)
set @data_versao = '2016-01-18'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


BEGIN TRANSACTION
	IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoRendaFixa') and name = 'IdConta')
	BEGIN
		alter table OperacaoRendaFixa add IdConta int null;

		ALTER TABLE OperacaoRendaFixa ADD CONSTRAINT FK_OperacaoRendaFixa_ContaCorrente FOREIGN KEY(IdConta) 
		REFERENCES ContaCorrente(IdConta) ;
	END


	IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoBolsa') and name = 'IdConta')
	BEGIN
		alter table OperacaoBolsa add IdConta int null;

		ALTER TABLE OperacaoBolsa ADD CONSTRAINT FK_OperacaoBolsa_ContaCorrente FOREIGN KEY(IdConta) 
		REFERENCES ContaCorrente(IdConta) ;
	END

	IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoBmf') and name = 'IdConta')
	BEGIN
		alter table OperacaoBmf add IdConta int null;

		ALTER TABLE OperacaoBmf ADD CONSTRAINT FK_OperacaoBmf_ContaCorrente FOREIGN KEY(IdConta) 
		REFERENCES ContaCorrente(IdConta) ;
	END

	IF NOT EXISTS(select 1 from syscolumns where id = object_id('OperacaoSwap') and name = 'IdConta')
	BEGIN
		alter table OperacaoSwap add IdConta int null;

		ALTER TABLE OperacaoSwap ADD CONSTRAINT FK_OperacaoSwap_ContaCorrente FOREIGN KEY(IdConta) 
		REFERENCES ContaCorrente(IdConta) ;
	END
	
	
IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoBMF') and name = 'IdConta')
	BEGIN
		alter table PosicaoBMF add IdConta int null;

		ALTER TABLE PosicaoBMF ADD CONSTRAINT FK_PosicaoBMF_ContaCorrente FOREIGN KEY(IdConta) 
		REFERENCES ContaCorrente(IdConta) ;
	END

	IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoBMFAbertura') and name = 'IdConta')
	BEGIN
		alter table PosicaoBMFAbertura add IdConta int null;

		ALTER TABLE PosicaoBMFAbertura ADD CONSTRAINT FK_PosicaoBMFAbertura_ContaCorrente FOREIGN KEY(IdConta) 
		REFERENCES ContaCorrente(IdConta) ;
	END

	IF NOT EXISTS(select 1 from syscolumns where id = object_id('PosicaoBMFHistorico') and name = 'IdConta')
	BEGIN
		alter table PosicaoBMFHistorico add IdConta int null;

		ALTER TABLE PosicaoBMFHistorico ADD CONSTRAINT FK_PosicaoBMFHistoricop_ContaCorrente FOREIGN KEY(IdConta) 
		REFERENCES ContaCorrente(IdConta) ;
	END
	
	IF NOT EXISTS(select 1 from syscolumns where id = object_id('CustodiaBMF') and name = 'IdConta')
	BEGIN
		alter table CustodiaBMF add IdConta int null;

		ALTER TABLE CustodiaBMF ADD CONSTRAINT FK_CustodiaBMF_ContaCorrente FOREIGN KEY(IdConta) 
		REFERENCES ContaCorrente(IdConta) ;
	END

COMMIT TRANSACTION