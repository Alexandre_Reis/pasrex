﻿declare @data_versao char(10)
set @data_versao = '2015-10-01'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
BEGIN TRANSACTION


	--garante que só sera commitado caso não aconteça nenhum erro
	set xact_abort on
	insert into VersaoSchema values(@data_versao);


	ALTER TABLE Carteira
	ADD ExplodeCotasDeFundos char(1) NOT NULL DEFAULT 'N';

	UPDATE InformacoesComplementaresFundo SET Periodicidade = SUBSTRING(Periodicidade,1, 250);
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN Periodicidade varchar(250) null;

	UPDATE InformacoesComplementaresFundo SET LocalFormaDivulgacao = SUBSTRING(LocalFormaDivulgacao,1, 300);
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN LocalFormaDivulgacao varchar(300) null;

	UPDATE InformacoesComplementaresFundo SET LocalFormaSolicitacaoCotista = SUBSTRING(LocalFormaSolicitacaoCotista,1, 250);
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN LocalFormaSolicitacaoCotista varchar(250) null;


	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN FatoresRisco varchar(2000) null;
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN PoliticaExercicioVoto varchar(1000) null;
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN TributacaoAplicavel varchar(2500) null;
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN PoliticaAdministracaoRisco varchar(2500) null;

	UPDATE InformacoesComplementaresFundo SET AgenciaClassificacaoRisco = SUBSTRING(AgenciaClassificacaoRisco,1, 50);
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN AgenciaClassificacaoRisco varchar(50) null;

	UPDATE InformacoesComplementaresFundo SET RecursosServicosGestor = SUBSTRING(RecursosServicosGestor,1, 100);
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN RecursosServicosGestor varchar(100) null;

	UPDATE InformacoesComplementaresFundo SET PrestadoresServicos = SUBSTRING(PrestadoresServicos,1, 100);
	ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN PrestadoresServicos varchar(100) null;	
	
COMMIT TRANSACTION

GO	

