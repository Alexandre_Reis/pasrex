declare @data_versao char(10)
set @data_versao = '2015-01-14'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table carteira add LiberaAplicacao char(1) null
go

update carteira set LiberaAplicacao = 'S'
go

alter table carteira alter column LiberaAplicacao char(1) not null
go

ALTER TABLE carteira ADD  CONSTRAINT DF_Carteira_LiberaAplicacao  DEFAULT (('S')) FOR LiberaAplicacao
GO


alter table carteira add LiberaResgate char(1) null
go

update carteira set LiberaResgate = 'S'
go

alter table carteira alter column LiberaResgate char(1) not null
go

ALTER TABLE carteira ADD  CONSTRAINT DF_Carteira_LiberaResgate  DEFAULT (('S')) FOR LiberaResgate
GO

