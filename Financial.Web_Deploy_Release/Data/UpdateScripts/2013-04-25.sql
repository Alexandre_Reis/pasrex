declare @data_versao char(10)
set @data_versao = '2013-04-25'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

CREATE TABLE EspecificacaoBolsa(
	Especificacao [varchar](3) NOT NULL,
	Tipo tinyint NOT NULL,
 CONSTRAINT PK_EspecificacaoBolsa PRIMARY KEY CLUSTERED 
(
	Especificacao ASC
)
)
GO

CREATE TABLE GrupoEconomico(
	IdGrupo int NOT NULL,
	Nome varchar(200) NOT NULL,
 CONSTRAINT PK_GrupoEconomico PRIMARY KEY CLUSTERED 
(
	IdGrupo ASC
)
)
GO

alter table AtivoBolsa Add IdGrupo int null
go

ALTER TABLE AtivoBolsa  WITH CHECK ADD  CONSTRAINT AtivoBolsa_GrupoEconomico_FK1 FOREIGN KEY(IdGrupo)
REFERENCES GrupoEconomico (IdGrupo)
GO

drop table EvolucaoCapitalSocial
go

CREATE TABLE EvolucaoCapitalSocial(
	CdAtivoBolsa varchar(20) NOT NULL,
	Data datetime NOT NULL,
	Quantidade decimal(18, 2) NOT NULL,
	Valor decimal(18, 2) NOT NULL,
	Descricao varchar(200) NULL,
	QuantidadeFinal decimal(18, 2) NOT NULL,
	ValorFinal decimal(18, 2) NOT NULL
 CONSTRAINT PK_EvolucaoCapitalSocial PRIMARY KEY CLUSTERED 
(
	CdAtivoBolsa ASC,
	Data ASC
)
)
GO

ALTER TABLE EvolucaoCapitalSocial  WITH CHECK ADD  CONSTRAINT FK_EvolucaoCapitalSocial_AtivoBolsa FOREIGN KEY(CdAtivoBolsa)
REFERENCES AtivoBolsa (CdAtivoBolsa)
GO


