declare @data_versao char(10)
set @data_versao = '2014-05-21'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

if not exists(select valortexto from configuracao where id = 5013)
begin
insert into configuracao (id, descricao, valornumerico, valortexto) 
values(5013, 'Processa Indicador Carteira', null, 'N')
end