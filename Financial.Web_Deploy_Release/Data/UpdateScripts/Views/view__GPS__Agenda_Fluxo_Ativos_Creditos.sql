IF EXISTS( select OBJECT_ID('view__GPS__Agenda_Fluxo_Ativos_Creditos', 'V') )
BEGIN
	DROP VIEW view__GPS__Agenda_Fluxo_Ativos_Creditos
END
go 

CREATE VIEW [dbo].[view__GPS__Agenda_Fluxo_Ativos_Creditos] AS
SELECT Cl.Apelido, Cl.DataDia, HC.Data, HC.PLFechamento, TRF.Descricao, E.Nome, 0 RiscoFinal, PRF.Quantidade, PRF.ValorMercado, ARF.DataEvento, 
	CASE WHEN ARF.TipoEvento = 1 THEN 'Juros'
		 WHEN ARF.TipoEvento = 2 THEN 'Juros Correção'
		 WHEN ARF.TipoEvento = 3 THEN 'Amortização'
		 WHEN ARF.TipoEvento = 4 THEN 'Pagamento Principal'
		 WHEN ARF.TipoEvento = 8 THEN 'Amortização Corrigida'
		 ELSE 'Outros Eventos'
	END AS TipoEvento,
	ARF.Taxa, LRF.ValorBruto, '' Comentario, LRF.ValorIR, LRF.ValorIOF, LRF.ValorLiquido, cl.IdCliente
FROM Cliente Cl
	INNER JOIN HistoricoCota HC ON HC.IdCarteira = Cl.IdCliente
	INNER JOIN PosicaoRendaFixaHistorico PRF ON PRF.IdCliente = HC.IdCarteira AND PRF.DataHistorico = HC.Data
	INNER JOIN AgendaRendaFixa ARF ON ARF.IdTitulo = PRF.IdTitulo AND ARF.DataEvento >= PRF.DataHistorico
	INNER JOIN LiquidacaoRendaFixa LRF ON LRF.IdCliente = PRF.IdCliente AND LRF.IdTitulo = PRF.IdTitulo AND LRF.DataLiquidacao >= PRF.DataHistorico AND LRF.TipoLancamento = ARF.TipoEvento AND LRF.DataLiquidacao = ARF.DataPagamento
	INNER JOIN TituloRendaFixa TRF ON TRF.IdTitulo = LRF.IdTitulo
	INNER JOIN Emissor E ON E.IdEmissor = TRF.IdEmissor
UNION
SELECT Cl.Apelido, Cl.DataDia, HC.Data, HC.PLFechamento, PBC.CdAtivoBolsa, E.Nome, 0 RiscoFinal, PBC.Quantidade, PB.ValorMercado, PBC.DataLancamento, 
	CASE WHEN PBC.TipoProvento = 10 THEN 'Dividendo'
		 WHEN PBC.TipoProvento = 11 THEN 'Restituição Capital'
		 WHEN PBC.TipoProvento = 12 THEN 'Bonificação Financeira'
		 WHEN PBC.TipoProvento = 13 THEN 'Juros sobre Capital'
		 WHEN PBC.TipoProvento = 14 THEN 'Rendimento'
		 WHEN PBC.TipoProvento = 16 THEN 'Juros'
		 ELSE 'Outros Eventos'
	END AS TipoEvento,
	0 Percentual, PBC.Valor, '' Comentario, (PBC.Valor * PBC.PercentualIR) ValorIR, 0 OutrasTaxas, (PBC.Valor * (1 - PBC.PercentualIR)) ValorLiquido, Cl.IdCliente
FROM Cliente Cl
	INNER JOIN HistoricoCota HC ON HC.IdCarteira = Cl.IdCliente
	INNER JOIN PosicaoBolsaHistorico PB ON PB.IdCliente = HC.IdCarteira AND PB.DataHistorico = HC.Data
	INNER JOIN ProventoBolsaCliente PBC ON PBC.IdCliente = PB.IdCliente AND PBC.DataPagamento >= PB.DataHistorico
	INNER JOIN AtivoBolsa AB ON AB.CdAtivoBolsa = PBC.CdAtivoBolsa
	INNER JOIN Emissor E ON E.IdEmissor = AB.IdEmissor
UNION
SELECT Cl.Apelido, Cl.DataDia, Cl.DataDia, ps.Saldo as PLFechamento, 'Swap' as Descricao, '' as Nome, 0 RiscoFinal, 0 as Quantidade, 0 as ValorMercado, es.DataEvento, 
	CASE WHEN ES.TipoEvento = 1 THEN 'Reset de Swap'
	     WHEN ES.TipoEvento = 2 and ES.PontaSwap = 1 THEN 'Ajuste - Parte'
		 WHEN ES.TipoEvento = 2 and ES.PontaSwap = 2 THEN 'Ajuste - Contra Parte'
		 ELSE 'Outros Eventos'
	END AS TipoEvento,
	0 Percentual, es.valor AS ValorBruto, '' as Comentario, 0 as ValorIR, 0 as OutrasTaxas, es.valor as ValorLiquido, Cl.IdCliente
FROM Cliente Cl	
	INNER JOIN OperacaoSwap OS ON OS.IdCliente = Cl.IdCliente 	
	INNER JOIN PosicaoSwap PS ON OS.IdOperacao = PS.IdOperacao
	INNER JOIN EventoSwap ES ON ES.IdOperacao = OS.IdOperacao 
								and ES.DataVigencia in (select max(esAux.DataVigencia) 
													from EventoSwap esAux
													where esAux.IdOperacao = ES.IdOperacao 
													and esAux.DataEvento = ES.DataEvento
													and esAux.TipoEvento = ES.TipoEvento
													and esAux.PontaSwap = ES.PontaSwap )		
GO

