﻿declare @data_versao char(10)
set @data_versao = '2014-10-22'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


--Criar processo de cadastro de exceção de ativos
delete from PermissaoMenu where idMenu = 7440;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7440
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 7430
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TabelaExcecaoAdm') = 0
BEGIN
CREATE TABLE TabelaExcecaoAdm(
	IdTabelaExcecao int IDENTITY(1,1) NOT NULL,
	IdTabela int NOT NULL,
	TipoGrupo int NOT NULL,
	CodigoAtivo varchar(100),
 CONSTRAINT TabelaExcecaoAdm_PK PRIMARY KEY CLUSTERED 
(
	IdTabelaExcecao ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'TabelaExcecaoAdm' and object_name(constid) = 'TabelaExcecaoAdm_TabelaTaxaAdministracao_FK1')
BEGIN
	ALTER TABLE TabelaExcecaoAdm  WITH CHECK ADD  CONSTRAINT TabelaExcecaoAdm_TabelaTaxaAdministracao_FK1 FOREIGN KEY(IdTabela)
	REFERENCES TabelaTaxaAdministracao (IdTabela)
END	

IF NOT EXISTS(select 1 from syscolumns where id = object_id('TabelaTaxaAdministracao') and name = 'EscalonaProporcional')
BEGIN
	--Adiciona atributo na TabelaTaxaAdministracao para efetuar escalonamento proporcional 
	alter table TabelaTaxaAdministracao add EscalonaProporcional char(1) null
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('TabelaTaxaAdministracao') and name = 'Consolidadora')
BEGIN
	--Adiciona atributo na TabelaTaxaAdministracao indicar se tabela será consolidadora de taxas
	alter table TabelaTaxaAdministracao add Consolidadora char(1) null
END


--Cria processo para cadastro de Taxa consolidadora e suas respectivas tabelas de calculo
delete from PermissaoMenu where idMenu = 7450;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7450
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 7430
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TaxaGlobal') = 0
BEGIN
CREATE TABLE TaxaGlobal(
	IdTaxaGlobal int IDENTITY(1,1) NOT NULL,
	IdTabelaGlobal int NOT NULL,
	IdTabelaAssociada int NOT NULL,
	Prioridade int NOT NULL,
 CONSTRAINT TaxaGlobal_PK PRIMARY KEY CLUSTERED 
(
	IdTaxaGlobal ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'TaxaGlobal' and object_name(constid) = 'TaxaGlobal_TabelaTaxaAdministracao_FK1')
BEGIN
	ALTER TABLE TaxaGlobal  WITH CHECK ADD  CONSTRAINT TaxaGlobal_TabelaTaxaAdministracao_FK1 FOREIGN KEY(IdTabelaGlobal)
	REFERENCES TabelaTaxaAdministracao (IdTabela)
END

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'TaxaGlobal' and object_name(constid) = 'TaxaGlobal_TabelaTaxaAdministracao_FK2')
BEGIN
	ALTER TABLE TaxaGlobal  WITH CHECK ADD  CONSTRAINT TaxaGlobal_TabelaTaxaAdministracao_FK2 FOREIGN KEY(IdTabelaAssociada)
	REFERENCES TabelaTaxaAdministracao (IdTabela)
END

/****** Tabela de Calculo de Taxa Administracao para tabelas associadas ******/
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CalculoAdministracaoAssociada') = 0
BEGIN
CREATE TABLE [dbo].[CalculoAdministracaoAssociada](
	[IdTabela] [int] NOT NULL,
	[IdCarteira] [int] NOT NULL,
	[ValorDia] [decimal](16, 2) NOT NULL,
	[ValorAcumulado] [decimal](16, 2) NOT NULL,
	[DataFimApropriacao] [datetime] NOT NULL,
	[DataPagamento] [datetime] NOT NULL,
	[ValorCPMFDia] [decimal](8, 2) NOT NULL,
	[ValorCPMFAcumulado] [decimal](8, 2) NOT NULL,
 CONSTRAINT [CalculoAdministracaoAssociada_PK] PRIMARY KEY CLUSTERED 
(
	[IdTabela] ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoAdministracaoAssociada' and object_name(constid) = 'Carteira_CalculoAdministracaoAssociada_FK1')
BEGIN
	ALTER TABLE [dbo].[CalculoAdministracaoAssociada]  WITH CHECK ADD  CONSTRAINT [Carteira_CalculoAdministracaoAssociada_FK1] FOREIGN KEY([IdCarteira])
	REFERENCES [dbo].[Carteira] ([IdCarteira])
END

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoAdministracaoAssociada' and object_name(constid) = 'TabelaTaxaAdministracao_CalculoAdministracaoAssociada_FK1')
BEGIN
	ALTER TABLE [dbo].[CalculoAdministracaoAssociada]  WITH CHECK ADD  CONSTRAINT [TabelaTaxaAdministracao_CalculoAdministracaoAssociada_FK1] FOREIGN KEY([IdTabela])
	REFERENCES [dbo].[TabelaTaxaAdministracao] ([IdTabela])
END

/****** Tabela de Calculo de Taxa Administracao Historico para tabelas associadas ******/
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CalculoAdministracaoAssociadaHistorico') = 0
BEGIN
CREATE TABLE [dbo].[CalculoAdministracaoAssociadaHistorico](
	[DataHistorico] [datetime] NOT NULL,
	[IdTabela] [int] NOT NULL,
	[IdCarteira] [int] NOT NULL,
	[ValorDia] [decimal](16, 2) NOT NULL,
	[ValorAcumulado] [decimal](16, 2) NOT NULL,
	[DataFimApropriacao] [datetime] NOT NULL,
	[DataPagamento] [datetime] NOT NULL,
	[ValorCPMFDia] [decimal](8, 2) NOT NULL,
	[ValorCPMFAcumulado] [decimal](8, 2) NOT NULL,
 CONSTRAINT [CalculoAdministracaoAssociadaHistorico_PK] PRIMARY KEY CLUSTERED 
(
	[DataHistorico] ASC,
	[IdTabela] ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoAdministracaoAssociadaHistorico' and object_name(constid) = 'Carteira_CalculoAdministracaoAssociadaHistorico_FK1')
BEGIN
	ALTER TABLE [dbo].[CalculoAdministracaoAssociadaHistorico]  WITH CHECK ADD  CONSTRAINT [Carteira_CalculoAdministracaoAssociadaHistorico_FK1] FOREIGN KEY([IdCarteira])
	REFERENCES [dbo].[Carteira] ([IdCarteira])
END

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoAdministracaoAssociadaHistorico' and object_name(constid) = 'TabelaTaxaAdministracao_CalculoAdministracaoAssociadaHistorico_FK1')
BEGIN
	ALTER TABLE [dbo].[CalculoAdministracaoAssociadaHistorico]  WITH CHECK ADD  CONSTRAINT [TabelaTaxaAdministracao_CalculoAdministracaoAssociadaHistorico_FK1] FOREIGN KEY([IdTabela])
	REFERENCES [dbo].[TabelaTaxaAdministracao] ([IdTabela])
END


--Cria processo de cadastro de perfil de processamento
delete from PermissaoMenu where idMenu = 14590;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14590
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14520
GO

delete from PermissaoMenu where idMenu = 14591;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14591
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14520
GO

delete from PermissaoMenu where idMenu = 14592;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14592
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14520
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PerfilProcessamento') = 0
BEGIN
CREATE TABLE PerfilProcessamento(
	IdPerfil int IDENTITY(1,1) NOT NULL,
	Descricao varchar(40) NOT NULL,
	Tipo int NOT NULL,
	DiasFechamento int NULL,
	DiasRetroagir int NULL,
	AberturaIndexada char(1) NULL,
 CONSTRAINT PerfilProcessamento_PK PRIMARY KEY CLUSTERED 
(
	IdPerfil ASC
)
)
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ClientePerfil') = 0
BEGIN
CREATE TABLE ClientePerfil(
	IdPerfil int NOT NULL,
	IdCliente int NOT NULL,
	Sequencia int NOT NULL,
 CONSTRAINT CarteiraPerfil_PK PRIMARY KEY CLUSTERED 
(
	IdPerfil ASC, IdCliente ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'ClientePerfil' and object_name(constid) = 'Perfil_ClientePerfil_FK1')
BEGIN
	ALTER TABLE ClientePerfil  WITH CHECK ADD  CONSTRAINT [Perfil_ClientePerfil_FK1] FOREIGN KEY(IdPerfil)
	REFERENCES PerfilProcessamento (IdPerfil)
END

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'ClientePerfil' and object_name(constid) = 'Cliente_ClientePerfil_FK1')
BEGIN
	ALTER TABLE ClientePerfil  WITH CHECK ADD  CONSTRAINT [Cliente_ClientePerfil_FK1] FOREIGN KEY(IdCliente)
	REFERENCES Cliente (IdCliente)
END

--Cria tabelas para controle de boletos retroativos e Log de Processamento
delete from PermissaoMenu where idMenu = 14610;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14610
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14600
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'LogProcessamento') = 0
BEGIN
CREATE TABLE LogProcessamento(
	IdCliente int NOT NULL,
	Data datetime NOt NULL,
	Login varchar(50) NOT NULL,
	Tipo int NOT NULL,
	DataInicio datetime NOT NULL,
	DataFim datetime NOT NULL,
	DataInicialPeriodo datetime NOT NULL,
	DataFinalPeriodo datetime NOT NULL,
	Erro int NOT NULL,
	Mensagem varchar(8000) NOT NULL,
 CONSTRAINT LogProcessamento_PK PRIMARY KEY CLUSTERED 
(
	IdCliente ASC, Data ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'LogProcessamento' and object_name(constid) = 'Cliente_LogProcessamento_FK1')
BEGIN
	ALTER TABLE LogProcessamento  WITH CHECK ADD  CONSTRAINT [Cliente_LogProcessamento_FK1] FOREIGN KEY(IdCliente)
	REFERENCES Cliente (IdCliente)
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'BoletoRetroativo') = 0
BEGIN
CREATE TABLE BoletoRetroativo(
	IdCliente int NOT NULL,
	TipoMercado int NOT NULL,
	DataBoleto datetime NOT NULL,
 CONSTRAINT BoletoRetroativo_PK PRIMARY KEY CLUSTERED 
(
	IdCliente ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'BoletoRetroativo' and object_name(constid) = 'Cliente_BoletoRetroativo_FK1')
BEGIN
	ALTER TABLE BoletoRetroativo  WITH CHECK ADD  CONSTRAINT [Cliente_BoletoRetroativo_FK1] FOREIGN KEY(IdCliente)
	REFERENCES Cliente (IdCliente)
END

--Cria tabelas para agendamento de processos e log de processamento automático
delete from PermissaoMenu where idMenu = 14630;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14630
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14600
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AgendaProcessos') = 0
BEGIN
CREATE TABLE AgendaProcessos(
	IdAgendaProcesso int  IDENTITY(1,1) NOT NULL,
	IdPerfil int NOt NULL,
	HoraExecucao datetime NOT NULL,
	DataFinal datetime NULL,
	Ativo char(1) NOT NULL,
 CONSTRAINT AgendaProcessos_PK PRIMARY KEY CLUSTERED 
(
	IdAgendaProcesso ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'AgendaProcessos' and object_name(constid) = 'PerfilProcessamento_AgendaProcessos_FK1')
BEGIN
	ALTER TABLE AgendaProcessos  WITH CHECK ADD  CONSTRAINT [PerfilProcessamento_AgendaProcessos_FK1] FOREIGN KEY(IdPerfil)
	REFERENCES PerfilProcessamento (IdPerfil)
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'LogProcessamentoAutomatico') = 0
BEGIN
CREATE TABLE LogProcessamentoAutomatico(
	idLog int  IDENTITY(1,1) NOT NULL,
	idAgendaProcesso int NOT NULL,
	DataInicio datetime NOT NULL,
	DataFim datetime NOT NULL,
	Erro char(1) NOT NULL,
	Mensagem varchar(8000) NULL,
 CONSTRAINT LogProcessamentoAutomatico_PK PRIMARY KEY CLUSTERED 
(
	idLog ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'LogProcessamentoAutomatico' and object_name(constid) = 'AgendaProcessos_LogProcessamentoAutomatico_FK1')
BEGIN
	ALTER TABLE LogProcessamentoAutomatico  WITH CHECK ADD  CONSTRAINT [AgendaProcessos_LogProcessamentoAutomatico_FK1] FOREIGN KEY(IdAgendaProcesso)
	REFERENCES AgendaProcessos (IdAgendaProcesso)
END

--Cria parametros de configuração geral: 
--1) Retroagir carteiras dependentes: Verificar se deve marcar carteiras para reprocessamento que tenham fundos com cotas alteradas
--2) Permitir operações retroativas: indica que será possível cadastrar operações com data anterior a data da carteira
IF NOT EXISTS (SELECT * FROM configuracao WHERE ID = 5014)
BEGIN
	insert into configuracao (ID, Descricao, ValorTexto) Values(5014, 'Retroagir Carteiras Dependentes', 'N');
END

IF NOT EXISTS (SELECT * FROM configuracao WHERE ID = 5015)
BEGIN
	insert into configuracao (ID, Descricao, ValorTexto) Values(5015, 'Permitir Operações Retroativas', 'N');
END

IF NOT EXISTS (SELECT * FROM configuracao WHERE ID = 5016)
BEGIN
	insert into configuracao (ID, Descricao, ValorTexto) Values(5016, 'Data Base', CONVERT(VARCHAR(10), GETDATE(), 103));
END

--Alterar tabela de carteira para possibilitar buscar cota de fundo até N dias antes da data de processamento
IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'BuscaCotaAnterior')
BEGIN
	ALTER TABLE dbo.Carteira ADD
		BuscaCotaAnterior char(1) NOT NULL DEFAULT 'N'
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'NumeroDiasBuscaCota')
BEGIN
	ALTER TABLE dbo.Carteira ADD
		NumeroDiasBuscaCota int
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'BandaVariacao')
BEGIN
	ALTER TABLE dbo.Carteira ADD
		BandaVariacao decimal(16, 2) NULL DEFAULT 0.00
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'ExcecaoRegraTxAdm')
BEGIN
	ALTER TABLE dbo.Carteira ADD
		ExcecaoRegraTxAdm char(1) NOT NULL DEFAULT 'N'
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('Carteira') and name = 'RebateImpactaPL')
BEGIN
	ALTER TABLE dbo.Carteira ADD
		RebateImpactaPL char(1) NOT NULL DEFAULT 'N'
END

IF NOT EXISTS(select 1 from syscolumns where id = object_id('HistoricoCota') and name = 'CotaImportada')
BEGIN
	ALTER TABLE dbo.HistoricoCota ADD CotaImportada CHAR(1)
	CONSTRAINT DF_HistoricoCota_CotaImportada DEFAULT 'N' NOT NULL
END

--Criacao de tabela para controle de calculo de rebate que impacta PL
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CalculoRebateImpactaPL') = 0
BEGIN
create TABLE CalculoRebateImpactaPL
(
	IdCliente int NOT NULL,
	IdCarteira int NOT NULL,
	DataLancamento datetime NOT NULL,
	DataPagamento datetime NOT NULL,
	ValorDia decimal(16,2) NOT NUll,
	ValorAcumulado decimal(16,2) NOT NULL,
 CONSTRAINT CalculoRebateImpactaPL_PK PRIMARY KEY CLUSTERED 
(
	IdCliente ASC, IdCarteira ASC, DataLancamento ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoRebateImpactaPL' and object_name(constid) = 'CalculoRebateImpactaPL_Carteira_FK1')
BEGIN
	ALTER TABLE CalculoRebateImpactaPL  WITH CHECK ADD  CONSTRAINT CalculoRebateImpactaPL_Carteira_FK1 FOREIGN KEY(IdCarteira)
	REFERENCES Carteira (IdCarteira)
END

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'CalculoRebateImpactaPL' and object_name(constid) = 'CalculoRebateImpactaPL_Cliente_FK1')
BEGIN
	ALTER TABLE CalculoRebateImpactaPL  WITH CHECK ADD  CONSTRAINT CalculoRebateImpactaPL_Cliente_FK1 FOREIGN KEY(IdCliente)
	REFERENCES Cliente (IdCliente)
END

--Criacao de tabelas de Evento de Renda Variável
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'IncorporacaoCisaoFusaoBolsa') = 0
BEGIN
CREATE TABLE IncorporacaoCisaoFusaoBolsa(
	IdIncorporacaoCisaoFusaoBolsa int IDENTITY(1,1) NOT NULL,
	Tipo int NOT NULL,
	CdAtivoBolsa varchar(20) NOT NULL,
	DataPosicao datetime NOT NULL,
	DataEx datetime NOT NULL,
 CONSTRAINT IncorporacaoCisaoFusaoBolsa_PK PRIMARY KEY CLUSTERED 
(
	IdIncorporacaoCisaoFusaoBolsa ASC
)
)
END
GO


IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'IncorporacaoCisaoFusaoBolsa' and object_name(constid) = 'IncorporacaoCisaoFusaoBolsa_AtivoBolsa_FK1')
BEGIN
	ALTER TABLE IncorporacaoCisaoFusaoBolsa  WITH CHECK ADD  CONSTRAINT IncorporacaoCisaoFusaoBolsa_AtivoBolsa_FK1 FOREIGN KEY(CdAtivoBolsa)
	REFERENCES AtivoBolsa (CdAtivoBolsa)
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'IncorporacaoCisaoFusaoBolsaDetalhe') = 0
BEGIN
CREATE TABLE IncorporacaoCisaoFusaoBolsaDetalhe(
	IdIncorporacaoCisaoFusaoBolsa int NOT NULL,
	CdAtivoBolsa varchar(20) NOT NULL,
	Percentual decimal(5,2) NULL,
	Fator decimal(5,2) NULL,
	Custo decimal(8,2) NOT NULL,
 CONSTRAINT IncorporacaoCisaoFusaoBolsaDetalhe_PK PRIMARY KEY CLUSTERED 
(
	IdIncorporacaoCisaoFusaoBolsa ASC, CdAtivoBolsa ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'IncorporacaoCisaoFusaoBolsaDetalhe' and object_name(constid) = 'IncorporacaoCisaoFusaoBolsaDetalhe_AtivoBolsa_FK1')
BEGIN
	ALTER TABLE IncorporacaoCisaoFusaoBolsaDetalhe  WITH CHECK ADD  CONSTRAINT IncorporacaoCisaoFusaoBolsaDetalhe_AtivoBolsa_FK1 FOREIGN KEY(CdAtivoBolsa)
	REFERENCES AtivoBolsa (CdAtivoBolsa)
END



--Criacao de tabelas para controle de Incorporacao/Fusão/Cisão de fundos
delete from PermissaoMenu where idMenu = 4880;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,4880
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 4850
GO

delete from PermissaoMenu where idMenu = 4881;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,4881
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 4850
GO

delete from PermissaoMenu where idMenu = 4882;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,4882
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 4850
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundo') = 0
BEGIN
CREATE TABLE EventoFundo(
	IdEventoFundo int IDENTITY(1,1) NOT NULL,
	IdCarteiraOrigem int NOT NULL,
	IdCarteiraDestino int NOT NULL,
	TipoEvento int NOT NULL,
	Percentual decimal(5,2) NULL,
	DataPosicao datetime NOT NULL,
	Status varchar(10) NOT NULL,
 CONSTRAINT EventoFundo_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundo' and object_name(constid) = 'EventoFundo_Carteira_FK1')
BEGIN
	ALTER TABLE EventoFundo  WITH CHECK ADD  CONSTRAINT EventoFundo_Carteira_FK1 FOREIGN KEY(IdCarteiraOrigem)
	REFERENCES Carteira (IdCarteira)	
END

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundo' and object_name(constid) = 'EventoFundo_Carteira_FK2')
BEGIN
	ALTER TABLE EventoFundo  WITH CHECK ADD  CONSTRAINT EventoFundo_Carteira_FK2 FOREIGN KEY(IdCarteiraDestino)
	REFERENCES Carteira (IdCarteira)
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoCautela') > 0
Begin
	drop table EventoFundoCautela
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoCautela') = 0
BEGIN
CREATE TABLE EventoFundoCautela(
	IdEventoFundo int NOT NULL,
	IdCotista int NOT NULL,
	DataAplicacao datetime NOT NULL,
	IdOperacao int NOT NULL,
	Quantidade decimal(28,12) NOT NULL,
 CONSTRAINT EventoFundoCautela_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, IdCotista ASC, DataAplicacao ASC, IdOperacao ASc
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoCautela' and object_name(constid) = 'EventoFundoCautela_EventoFundo_FK1')
BEGIN
	ALTER TABLE EventoFundoCautela  WITH CHECK ADD  CONSTRAINT EventoFundoCautela_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)	
	REFERENCES EventoFundo (IdEventoFundo)
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoBolsa') > 0
Begin
	drop table EventoFundoBolsa
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoBolsa') = 0
BEGIN
CREATE TABLE EventoFundoBolsa(
	IdEventoFundo int NOT NULL,
	TipoMercado int NOT NULL,
	CdAtivoBolsa varchar(20) NOT NULL,
	Quantidade decimal(28,12) NOT NULL,
 CONSTRAINT EventoFundoBolsa_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado Asc, CdAtivoBolsa ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoBolsa' and object_name(constid) = 'EventoFundoBolsa_EventoFundo_FK1')
BEGIN
	ALTER TABLE EventoFundoBolsa  WITH CHECK ADD  CONSTRAINT EventoFundoBolsa_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoBMF') > 0
Begin
	drop table EventoFundoBMF
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoBMF') = 0
BEGIN
CREATE TABLE EventoFundoBMF(
	IdEventoFundo int NOT NULL,
	TipoMercado int NOT NULL,
	CdAtivoBMF varchar(20) NOT NULL,
	Serie varchar(5) NOT NULL,
	Quantidade decimal(28,12) NOT NULL,
	ValorLiquido decimal(16,2) NOT NULL,
 CONSTRAINT EventoFundoBMF_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado Asc, CdAtivoBMF ASC, Serie ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoBMF' and object_name(constid) = 'EventoFundoBMF_EventoFundo_FK1')
BEGIN
	ALTER TABLE EventoFundoBMF  WITH CHECK ADD  CONSTRAINT EventoFundoBMF_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoSwap') > 0
Begin
	drop table EventoFundoSwap
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoSwap') = 0
BEGIN
CREATE TABLE EventoFundoSwap(
	IdEventoFundo int NOT NULL,
	TipoMercado int NOT NULL,
	NumeroContrato varchar(20) NOT NULL,
	DataEmissao datetime NOT NULL,
	DataVencimento datetime NOT NULL,
	TipoPonta tinyint NOT NULL,
	IdIndice smallint NULL,
	TipoPontaContraParte tinyint NOT NULL,
	IdIndiceContraParte smallint NULL,
	Quantidade decimal(28,12) NOT NULL,
	QuantidadeOriginal decimal(28,12) NOT NULL,
 CONSTRAINT EventoFundoSwap_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado Asc, NumeroContrato ASC, DataEmissao ASC, DataVencimento ASC, TipoPonta ASC, TipoPontaContraParte ASC
)
)
END
GO


IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoSwap' and object_name(constid) = 'EventoFundoSwap_EventoFundo_FK1')
BEGIN
	ALTER TABLE EventoFundoSwap  WITH CHECK ADD  CONSTRAINT EventoFundoSwap_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoRendaFixa') > 0
Begin
	drop table 	EventoFundoRendaFixa
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoRendaFixa') = 0
BEGIN
CREATE TABLE EventoFundoRendaFixa(
	IdEventoFundo int NOT NULL,
	TipoMercado int NOT NULL,
	IdTitulo int NOT NULL,
	TipoOperacao tinyint NOT NULL,
	DataOperacao datetime NOT NULL,
	DataLiquidacao datetime NOT NULL,
	Quantidade decimal(28,12) NOT NULL,
 CONSTRAINT EventoFundoRendaFixa_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado Asc, IdTitulo ASC, TipoOperacao ASC, DataOperacao ASC, DataLiquidacao ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoRendaFixa' and object_name(constid) = 'EventoFundoRendaFixa_EventoFundo_FK1')
BEGIN
	ALTER TABLE EventoFundoRendaFixa  WITH CHECK ADD  CONSTRAINT EventoFundoRendaFixa_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoFundo') > 0
Begin
	drop table EventoFundoFundo
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoFundo') = 0
BEGIN
CREATE TABLE EventoFundoFundo(
	IdEventoFundo int NOT NULL,
	TipoMercado int NOT NULL,
	IdCarteira int NOT NULL,
	DataAplicacao datetime NOT NULL,
	DataConversao datetime NOT NULL,
	Quantidade decimal(28,12) NOT NULL,
 CONSTRAINT EventoFundoFundo_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado Asc, IdCarteira ASC, DataAplicacao ASC, DataConversao ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoFundo' and object_name(constid) = 'EventoFundoFundo_EventoFundo_FK1')
BEGIN
	ALTER TABLE EventoFundoFundo  WITH CHECK ADD  CONSTRAINT EventoFundoFundo_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoLiquidacao') > 0
Begin
	drop table EventoFundoLiquidacao
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoLiquidacao') = 0
BEGIN
CREATE TABLE EventoFundoLiquidacao(
	IdEventoFundo int Not NULL,
	TipoMercado int NOT NULL,
	DataLancamento Datetime NOT NULL,
	DataVencimento Datetime NOT NULL,
	Descricao varchar(600) NOT NULL,
	Valor  decimal(16,2) NOT NULL,
	Quantidade decimal(28,12) NOT NULL,
	QuantidadeConstante int NOT NULL,
	Contador int NOT NULL,
 CONSTRAINT EventoFundoLiquidacao_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado ASC, DataLancamento ASC, DataVencimento ASC, Descricao ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoLiquidacao' and object_name(constid) = 'EventoFundoLiquidacao_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoLiquidacao  WITH CHECK ADD  CONSTRAINT EventoFundoLiquidacao_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoCaixa') > 0
Begin
	drop table EventoFundoCaixa
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoCaixa') = 0
BEGIN
CREATE TABLE EventoFundoCaixa(
	IdEventoFundo int Not NULL,
	TipoMercado int NOT NULL,
	Data Datetime NOT NULL,
	IdConta int NOT NULL,
	SaldoAbertura decimal(16,2) NOT NULL,
	Valor  decimal(16,2) NOT NULL,
	QuantidadeConstante int NOT NULL,
	Descricao varchar(20) NOT NULL,
 CONSTRAINT EventoFundoCaixa_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado ASC, Data ASC, IdConta ASC, SaldoAbertura ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoCaixa' and object_name(constid) = 'EventoFundoCaixa_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoCaixa  WITH CHECK ADD  CONSTRAINT EventoFundoCaixa_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoMovimentoCautelas') > 0
Begin
	drop table EventoFundoMovimentoCautelas
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoMovimentoCautelas') = 0
BEGIN
CREATE TABLE EventoFundoMovimentoCautelas(
	IdEventoFundo int Not NULL,
	IdOperacao int NOT NULL,
 CONSTRAINT EventoFundoMovimentoCautelas_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, IdOperacao ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoMovimentoCautelas' and object_name(constid) = 'EventoFundoMovimentoCautelas_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoMovimentoCautelas  WITH CHECK ADD  CONSTRAINT EventoFundoMovimentoCautelas_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoMovimentoAtivos') > 0
Begin
	drop table EventoFundoMovimentoAtivos
end

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoMovimentoAtivos') = 0
BEGIN
CREATE TABLE EventoFundoMovimentoAtivos(
	IdEventoFundo int Not NULL,
	TipoMercado int NOT NULL,
	IdOperacao int NOT NULL,
 CONSTRAINT EventoFundoMovimentoAtivos_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado ASC, IdOperacao ASC
)
)
END
GO

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'EventoFundoMovimentoAtivos' and object_name(constid) = 'EventoFundoMovimentoAtivos_EventoFundo_FK1')
BEGIN 
	ALTER TABLE EventoFundoMovimentoAtivos  WITH CHECK ADD  CONSTRAINT EventoFundoMovimentoAtivos_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
	REFERENCES EventoFundo (IdEventoFundo)
END


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoPosicaoCautelas') > 0
Begin
	drop table EventoFundoPosicaoCautelas
end

CREATE TABLE EventoFundoPosicaoCautelas(
	IdEventoFundo int Not NULL,
	IdPosicao int NOT NULL,
 CONSTRAINT EventoFundoPosicaoCautelas_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, IdPosicao ASC
)
)
GO

ALTER TABLE EventoFundoPosicaoCautelas  WITH CHECK ADD  CONSTRAINT EventoFundoPosicaoCautelas_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
REFERENCES EventoFundo (IdEventoFundo)
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EventoFundoPosicaoAtivos') > 0
Begin
	drop table EventoFundoPosicaoAtivos
end

CREATE TABLE EventoFundoPosicaoAtivos(
	IdEventoFundo int Not NULL,
	TipoMercado int NOT NULL,
	IdPosicao int NOT NULL,
 CONSTRAINT EventoFundoPosicaoAtivos_PK PRIMARY KEY CLUSTERED 
(
	IdEventoFundo ASC, TipoMercado ASC, IdPosicao ASC
)
)
GO

ALTER TABLE EventoFundoPosicaoAtivos  WITH CHECK ADD  CONSTRAINT EventoFundoPosicaoAtivos_EventoFundo_FK1 FOREIGN KEY(IdEventoFundo)
REFERENCES EventoFundo (IdEventoFundo)
GO

--Criar Cadastros complementares
delete from permissaoMenu where idMenu in (480, 490, 500, 510, 520)

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,480
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,490
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,500
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,510
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,520
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementarTipoLista') = 0
Begin
 CREATE TABLE CadastroComplementarTipoLista
 (  
   IdTipoLista INT PRIMARY KEY identity NOT NULL, 
   NomeLista varchar(50),
   TipoLista int NOT NULL,   
   CONSTRAINT TipoUnico UNIQUE (NomeLista,TipoLista)
 ) 
End

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementarItemLista') = 0
Begin
 CREATE TABLE CadastroComplementarItemLista
 (  
   IdLista INT PRIMARY KEY identity NOT NULL, 
   IdTipoLista int FOREIGN KEY REFERENCES CadastroComplementarTipoLista(IdTipoLista) NOT NULL,
   ItemLista varchar(50) NOT NULL,   
 ) 
End

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementarCampos') = 0
Begin
 CREATE TABLE CadastroComplementarCampos
 (  
   IdCamposComplementares INT  PRIMARY KEY identity NOT NULL, 
   TipoCadastro int           NOT NULL, 
   NomeCampo varchar(50)      NOT NULL, 
   DescricaoCampo varchar(50) NULL, 
   ValorDefault varchar(20)   NULL,
   TipoCampo int              NOT NULL,
   CampoObrigatorio Char      NOT NULL,
   Tamanho int                NULL,
   CasasDecimais int          NULL
 ) 
End

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementar') = 0
Begin
 CREATE TABLE CadastroComplementar
 (  
   IdCadastroComplementares INT PRIMARY KEY identity NOT NULL, 
   IdCamposComplementares int FOREIGN KEY REFERENCES CadastroComplementarCampos(IdCamposComplementares),
   IdMercadoTipoPessoa int NOT NULL,
   DescricaoMercadoTipoPessoa varchar(50)    NOT NULL, 
   ValorCampo varchar(50)            NOT NULL
 ) 
End

--TAREFA 30278 - ISIN Cusip
--INICIO
--ADD COLUMN TO "TituloRendaFixa"
IF NOT EXISTS(SELECT * FROM sys.columns 
	          WHERE NAME = N'CodigoCusip' and Object_ID = Object_ID(N'TituloRendaFixa'))
BEGIN
    ALTER TABLE dbo.TituloRendaFixa
	ADD CodigoCusip VARCHAR(9)
END

--ADD COLUMN TO "AtivoBMF" 
IF NOT EXISTS(SELECT * FROM sys.columns 
	          WHERE NAME = N'CodigoCusip' and Object_ID = Object_ID(N'AtivoBMF'))
BEGIN
    ALTER TABLE dbo.AtivoBMF
	ADD CodigoCusip varchar(9)
END

--ADD COLUMN TO "AtivoBolsa"
IF NOT EXISTS(SELECT * FROM sys.columns 
	          WHERE NAME = N'CodigoCusip' and Object_ID = Object_ID(N'AtivoBolsa'))
BEGIN
    ALTER TABLE dbo.AtivoBolsa
	ADD CodigoCusip VARCHAR(9)	
END

--ADD COLUMN TO "ClienteInterface"
IF NOT EXISTS(SELECT * FROM sys.columns 
	          WHERE NAME = N'CodigoCusip' and Object_ID = Object_ID(N'ClienteInterface'))
BEGIN
    ALTER TABLE dbo.ClienteInterface
	ADD CodigoIsin varchar(12)

	ALTER TABLE dbo.ClienteInterface
	ADD CodigoCusip varchar(9)
END
--FIM

--TAREFA 30697 - REGISTRO DE EMPRESAS SECURITIZADAS
--INICIO
--CREATE TABLE "EmpresaSecuritizada"
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'EmpresaSecuritizada') = 0
Begin
	CREATE TABLE EmpresaSecuritizada
	(  
		 IdEmpresaSecuritizada INT PRIMARY KEY identity NOT NULL, 
		 IdAgenteMercado int FOREIGN KEY REFERENCES AgenteMercado(IdAgente) NOT NULL,
		 IdTitulo int FOREIGN KEY REFERENCES TituloRendaFixa(IdTitulo) NOT NULL
	) 
End

--ADD COLUMN TO "AgenteMercado"
IF NOT EXISTS(SELECT * FROM sys.columns 
	          WHERE NAME = N'EmpSecuritizada' and Object_ID = Object_ID(N'AgenteMercado'))
BEGIN
    ALTER TABLE AgenteMercado
	ADD EmpSecuritizada CHAR
END
--UPDATE: ATUALIZA OS REGISTROS PARA QUE ELAS ENTREM NA CATEGORIA DE EMPSECURITIZADA
Update AgenteMercado set EmpSecuritizada = 'S'
--FIM

--TAREFA 30699 - TRATAMENTO CADASTRAL PARA TRIBUTAÇÃO DE ATIVOS
--INICIO
--ADD COLUMN TO "PapelRendaFixa"
IF NOT EXISTS(SELECT * FROM sys.columns 
	          WHERE NAME = N'IsentoIR' and Object_ID = Object_ID(N'PapelRendaFixa'))
BEGIN
    ALTER TABLE PapelRendaFixa
	ADD IsentoIR int
END

--ADD MENU TO "PermissaoMenu"
DELETE FROM PermissaoMenu WHERE IdMenu = 530
    INSERT INTO [dbo].[PermissaoMenu]
	SELECT [IdGrupo]
      ,530
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
	FROM [dbo].[PermissaoMenu]
	WHERE IdMenu = 100

--ADD TABLE "ExcecoesTributacaoIR"
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ExcecoesTributacaoIR') = 0
Begin
	CREATE TABLE ExcecoesTributacaoIR
	(
		 IdExcecoesTributacaoIR INT PRIMARY KEY identity NOT NULL, 
		 Mercado int NOT NULL,
		 TipoClasseAtivo int NULL,
	     Ativo varchar(50) NULL,
		 TipoInvestidor int NOT NULL,	
		 IsencaoIR int NOT NULL,
		 AliquotaIR varchar(20) NULL,
		 IsencaoGanhoCapital int NOT NULL,
		 AliquotaIRGanhoCapital varchar(20) NULL,
		 CdAtivoBMF varchar(20)  NULL,
		 SerieBMF varchar(5) NULL,
		 IdTituloRendaFixa int FOREIGN KEY REFERENCES TituloRendaFixa(IdTitulo) NULL,
		 IdOperacaoSwap int FOREIGN KEY REFERENCES OperacaoSwap(IdOperacao) NULL,
		 IdCarteira int FOREIGN KEY REFERENCES Carteira(IdCarteira) NULL,
		 CdAtivoBolsa varchar(20) FOREIGN KEY REFERENCES AtivoBolsa(CdAtivoBolsa) NULL,
		 CONSTRAINT FK_AtivoBMF FOREIGN KEY (CdAtivoBMF, SerieBMF) REFERENCES AtivoBMF(CdAtivoBMF, Serie)
	) 
End
--FIM

--TAREFA PAS 291 - Fazer com que toda posição de RF referencie sua operação correspondente
--INICIO
--TRIGGER RENDA FIXA
drop trigger trgPosicaoRendaFixa
IF NOT EXISTS (select * from sys.objects where [type] = 'TR' and [name] = 'trgPosicaoRendaFixa')
EXEC('
	CREATE TRIGGER trgPosicaoRendaFixa ON PosicaoRendaFixa 
	AFTER INSERT AS 
	BEGIN 
		DECLARE @IdCliente	INT, 
				@IdTitulo	INT,
				@TipoOperacao INT,
				@Quantidade DECIMAL(25,12),
				@QuantidadeBloqueada DECIMAL(16,2),
				@DataOperacao	DATETIME,
				@PUOperacao	DECIMAL(25,12),
				@TaxaOperacao	DECIMAL(25,16),
				@PUMercado	DECIMAL(25,12),
				@DataVolta	DATETIME,
				@TaxaVolta	DECIMAL(8,4),
				@PUVolta	DECIMAL(25,12),
				@IdCustodia tinyint,
				@TipoNegociacao int,
				@IdPosicao int,
				@IdOperacao int,
				@DataDia DATETIME
		
		SELECT @IdCliente = IdCliente from inserted; 
		SELECT @IdTitulo =	IdTitulo from inserted;
		SELECT @TipoOperacao = TipoOperacao from inserted;
		SELECT @Quantidade = Quantidade from inserted;
		SELECT @DataOperacao = DataOperacao from inserted;
		SELECT @PUOperacao = PUOperacao from inserted;
		SELECT @TaxaOperacao = TaxaOperacao from inserted;
		SELECT @DataVolta = DataVolta from inserted;
		SELECT @TaxaVolta = TaxaVolta from inserted;
		SELECT @PUVolta = PUVolta from inserted;
		SELECT @IdCustodia = IdCustodia from inserted; 
		SELECT @TipoNegociacao = TipoNegociacao from inserted; 
		SELECT @IdPosicao = IdPosicao from inserted; 
		SELECT @IdOperacao = IdOperacao from inserted; 
		SELECT @DataDia = DataDia from Cliente where IdCliente = @IdCliente;

		IF(@IdOperacao IS NULL)
		BEGIN	
			INSERT INTO OperacaoRendaFixa(IdCliente, IdTitulo, TipoOperacao, Quantidade, DataOperacao, PUOperacao, TaxaOperacao, DataVolta, TaxaVolta, PUVolta, IdCustodia, DataLiquidacao, Valor,Fonte, TipoNegociacao, IdLiquidacao, DataRegistro) 
			VALUES(@IdCliente, @IdTitulo, @TipoOperacao, @Quantidade, @DataDia, @PUOperacao,@TaxaOperacao,@DataVolta,@TaxaVolta,@PUVolta,@IdCustodia, @DataOperacao, (@Quantidade*@PUOperacao), 0, @TipoNegociacao, 0, getDate());  

			UPDATE PosicaoRendaFixa 
			SET IdOperacao = (select max(OperacaoRendaFixa.IdOperacao) FROM OperacaoRendaFixa)
			WHERE IdPosicao = @IdPosicao;
		END
	END 
');

--TRIGGER FUNDO
drop trigger trgPosicaoFundo
IF NOT EXISTS (select * from sys.objects where [type] = 'TR' and [name] = 'trgPosicaoFundo')
EXEC('
CREATE TRIGGER trgPosicaoFundo ON PosicaoFundo 
	AFTER INSERT AS 
	BEGIN 
		DECLARE @IdCliente	INT, 
				@IdCarteira	INT,
				@ValorAplicacao DECIMAL(16,2),
				@DataAplicacao DATETIME,
				@DataConversao DATETIME,
				@Quantidade	DECIMAL(28,12),
				@CotaAplicacao DECIMAL(28,12),
				@IdOperacao INT,
				@IdPosicao INT,
				@DataDia DATETIME
	
		SELECT @IdCliente = IdCliente from inserted; 
		SELECT @IdCarteira = IdCarteira from inserted;
		SELECT @ValorAplicacao = ValorAplicacao from inserted;
		SELECT @Quantidade = Quantidade from inserted;
		SELECT @DataAplicacao = DataAplicacao from inserted;
		SELECT @DataConversao = DataConversao from inserted;
		SELECT @CotaAplicacao = CotaAplicacao from inserted;
		SELECT @IdOperacao = IdOperacao from inserted;
		SELECT @IdPosicao = IdPosicao from inserted;
		SELECT @DataDia = DataDia from Cliente where IdCliente = @idCarteira;

		IF(@IdOperacao IS NULL)
		BEGIN	
			INSERT INTO OperacaoFundo(IdCliente, IdCarteira, ValorBruto, Quantidade, DataOperacao, DataConversao, CotaOperacao, DataLiquidacao, DataAgendamento, TipoOperacao, IdFormaLiquidacao, Fonte) 
			VALUES(@IdCliente, @IdCarteira, @ValorAplicacao, @Quantidade, @DataDia, @DataConversao,@CotaAplicacao, GetDate(),@DataAplicacao, 10, 1, 2);  

			UPDATE PosicaoFundo 
			SET IdOperacao = (select max(OperacaoFundo.IdOperacao) FROM OperacaoFundo)
			WHERE IdPosicao = @IdPosicao;
		END
	END 
');
	
--TRIGGER SWAP	
IF NOT EXISTS (select * from sys.objects where [type] = 'TR' and [name] = 'trgPosicaoSwap')
EXEC('
	CREATE TRIGGER trgPosicaoSwap ON PosicaoSwap
	AFTER INSERT AS 
	BEGIN 
		DECLARE @IdCliente INT,
				@TipoRegistro TINYINT,
				@NumeroContrato VARCHAR(20),
				@DataEmissao DATETIME,
				@DataVencimento DATETIME, 
				@ValorBase DECIMAL(16,2),
				@TipoPonta TINYINT,                                                                                                                       
				@IdIndice SMALLINT,                                                                                                                        
				@TaxaJuros DECIMAL(10,4),                                                                                                                       
				@Percentual DECIMAL(10,4),                                                                                                                      
				@TipoApropriacao TINYINT,                                                                                                                  
				@ContagemDias TINYINT,                                                                                                                     
				@BaseAno SMALLINT,                                                                                                                         
				@PontaContraParte TINYINT,                                                                                                             
				@IdIndiceContraParte SMALLINT,                                                                                                             
				@TaxaJurosContraParte DECIMAL(10,4), 
				@PercentualContraParte DECIMAL(10,4),                                                                                                           
				@ApropriacaoContraParte TINYINT,                                                                                                      
				@ContagemDiasContraParte TINYINT,
				@BaseAnoContraParte SMALLINT,
				@ValorParte DECIMAL(16,2),
				@ValorContraParte DECIMAL(16,2),
				@Saldo DECIMAL(16,2),
				@IdOperacao INT,
				@IdPosicao INT,
				@ComGarantia CHAR,
				@DiasUteis INT,
				@DiasCorridos INT,
				@IdAgente INT

		SELECT	@IdCliente = IdCliente from inserted;
		SELECT	@TipoRegistro = TipoRegistro from inserted;
		SELECT	@NumeroContrato = NumeroContrato from inserted;
		SELECT	@DataEmissao = DataEmissao from inserted;
		SELECT	@DataVencimento = DataVencimento from inserted;
		SELECT	@ValorBase = ValorBase from inserted;
		SELECT	@TipoPonta = TipoPonta from inserted;
		SELECT	@IdIndice = IdIndice from inserted;
		SELECT	@TaxaJuros = TaxaJuros from inserted;
		SELECT	@Percentual = Percentual from inserted;                                                                                                        
		SELECT	@TipoApropriacao = TipoApropriacao from inserted;                                                                                                   
		SELECT	@ContagemDias = ContagemDias from inserted;                                                                                              
		SELECT	@BaseAno = BaseAno from inserted;                                                                                                 
		SELECT	@PontaContraParte = TipoPontaContraParte from inserted;                                                                                                  
		SELECT	@IdIndiceContraParte = IdIndiceContraParte from inserted;                                                                                            
		SELECT	@TaxaJurosContraParte = TaxaJurosContraParte from inserted;
		SELECT	@PercentualContraParte = PercentualContraParte from inserted;                                                                                           
		SELECT	@ApropriacaoContraParte = TipoApropriacaoContraParte from inserted;                                                                                
		SELECT	@ContagemDiasContraParte = ContagemDiasContraParte from inserted;
		SELECT	@BaseAnoContraParte = BaseAnoContraParte  from inserted;
		SELECT	@ValorParte = ValorParte from inserted;
		SELECT	@ValorContraParte = ValorContraParte from inserted;
		SELECT	@Saldo = Saldo from inserted;
		SELECT	@IdOperacao = IdOperacao from inserted;
		SELECT	@IdPosicao = IdPosicao from inserted;
		SELECT	@ComGarantia = ComGarantia from inserted;
		SELECT	@DiasUteis = DiasUteis from inserted;
		SELECT	@DiasCorridos = DiasCorridos from inserted;
		SELECT  @IdAgente = IdAgente from inserted
		
		IF(@IdOperacao IS NULL)
		BEGIN	
			INSERT INTO OperacaoSwap(IdCliente, TipoRegistro, NumeroContrato, DataEmissao, DataVencimento, ValorBase, TipoPonta, IdIndice, TaxaJuros, Percentual, TipoApropriacao, ContagemDias,BaseAno, TipoPontaContraParte, IdIndiceContraParte,TaxaJurosContraParte,PercentualContraParte,TipoApropriacaoContraParte, ContagemDiasContraParte,BaseAnoContraParte, ValorIndicePartida, ValorIndicePartidaContraParte, DataRegistro, ComGarantia, DiasUteis, DiasCorridos, IdAgente) 
			VALUES(@IdCliente, @TipoRegistro, @NumeroContrato, @DataEmissao, @DataVencimento, @ValorBase, @TipoPonta,@IdIndice,@TaxaJuros,@Percentual,@TipoApropriacao,@ContagemDias, @BaseAno, @PontaContraParte, @IdIndiceContraParte, @TaxaJurosContraParte, @PercentualContraParte, @ApropriacaoContraParte, @ContagemDiasContraParte, @BaseAnoContraParte, @ValorParte, @ValorContraParte, GetDate(), @ComGarantia, @DiasUteis, @DiasCorridos, @idAgente);  

			UPDATE PosicaoSwap 
			SET IdOperacao = (select max(OperacaoSwap.IdOperacao) FROM OperacaoSwap)
			WHERE IdPosicao = @IdPosicao;
		END
	END 
');

--TRIGGER COTISTA
drop trigger trgPosicaoCotista
IF NOT EXISTS (select * from sys.objects where [type] = 'TR' and [name] = 'trgPosicaoCotista')
EXEC('
CREATE TRIGGER trgPosicaoCotista ON PosicaoCotista 
	AFTER INSERT AS 
	BEGIN 
		DECLARE @IdCotista	INT, 
				@IdCarteira	INT,
				@ValorAplicacao DECIMAL(16,2),
				@DataAplicacao DATETIME,
				@DataConversao DATETIME,
				@Quantidade	DECIMAL(28,12),
				@CotaAplicacao DECIMAL(28,12),
				@IdOperacao INT,
				@IdPosicao INT,
				@DataDia DATETIME
				
	
		SELECT @IdCotista = IdCotista from inserted; 
		SELECT @IdCarteira = IdCarteira from inserted;
		SELECT @ValorAplicacao = ValorAplicacao from inserted;
		SELECT @Quantidade = Quantidade from inserted;
		SELECT @DataAplicacao = DataAplicacao from inserted;
		SELECT @DataConversao = DataConversao from inserted;
		SELECT @CotaAplicacao = CotaAplicacao from inserted;
		SELECT @IdOperacao = IdOperacao from inserted;
		SELECT @IdPosicao = IdPosicao from inserted;
		SELECT @DataDia = DataDia from Cliente where IdCliente = @idCarteira;
		
	
		IF(@IdOperacao IS NULL)
		BEGIN	
			INSERT INTO OperacaoCotista(IdCotista, IdCarteira, ValorBruto, Quantidade, DataOperacao, DataConversao, CotaOperacao, DataLiquidacao, DataAgendamento, TipoOperacao, IdFormaLiquidacao, Fonte, DataRegistro) 
			VALUES(@IdCotista, @IdCarteira, @ValorAplicacao, @Quantidade, @DataDia, @DataConversao,@CotaAplicacao, GetDate(),@DataAplicacao, 1, 1, 2, @DataDia);  

			UPDATE PosicaoCotista
			SET IdOperacao = (select max(OperacaoCotista.IdOperacao) FROM OperacaoCotista)
			WHERE IdPosicao = @IdPosicao;
		END
	END 
');

--CURSOR RENDA FIXA
--Declara as variáveis para armazenar os valores do fetch
DECLARE @IdCliente	INT, 
		@IdTitulo	INT,
		@TipoOperacao INT,
		@Quantidade DECIMAL(25,12),
		@QuantidadeBloqueada DECIMAL(16,2),
		@DataOperacao	DATETIME,
		@PUOperacao	DECIMAL(25,12),
		@TaxaOperacao	DECIMAL(25,16),
		@PUMercado	DECIMAL(25,12),
		@DataVolta	DATETIME,
		@TaxaVolta	DECIMAL(8,4),
		@PUVolta	DECIMAL(25,12),
		@IdCustodia TINYINT,
		@TipoNegociacao INT,
		@IdPosicao INT,
		@IdOperacao INT

DECLARE RendaFixaCursor CURSOR FOR
SELECT IdCliente,
	   IdTitulo, 
	   TipoOperacao,
	   Quantidade,
	   QuantidadeBloqueada,
	   DataOperacao,
	   PUOperacao,
	   TaxaOperacao,
	   PUMercado,
	   DataVolta,
	   TaxaVolta,
	   PUVolta,
	   IdCustodia,
	   TipoNegociacao,
	   IdPosicao,
	   IdOperacao   
FROM PosicaoRendaFixa
WHERE IdOperacao is null;

OPEN RendaFixaCursor;
-- Armazena os valores nas variáveis
FETCH NEXT FROM RendaFixaCursor
INTO @IdCliente, 
	 @IdTitulo,
	 @TipoOperacao,
	 @Quantidade ,
	 @QuantidadeBloqueada,
	 @DataOperacao,
	 @PUOperacao,
	 @TaxaOperacao,
	 @PUMercado,
	 @DataVolta,
	 @TaxaVolta,
	 @PUVolta,
	 @IdCustodia,
	 @TipoNegociacao,
	 @IdPosicao,
	 @IdOperacao;

--Verifica se há mais linhas a serem percorridas
WHILE @@FETCH_STATUS = 0
BEGIN
	INSERT INTO OperacaoRendaFixa(IdCliente, IdTitulo, TipoOperacao, Quantidade, DataOperacao, PUOperacao, TaxaOperacao, DataVolta, TaxaVolta, PUVolta, IdCustodia, DataLiquidacao, Valor,Fonte, TipoNegociacao, IdLiquidacao, DataRegistro) 
	VALUES(@IdCliente, @IdTitulo, @TipoOperacao, @Quantidade, @DataOperacao, @PUOperacao,@TaxaOperacao,@DataVolta,@TaxaVolta,@PUVolta,@IdCustodia, @DataOperacao, (@Quantidade*@PUOperacao), 0, @TipoNegociacao, 0, getDate());  

	UPDATE PosicaoRendaFixa 
	SET IdOperacao = (select max(OperacaoRendaFixa.IdOperacao) FROM OperacaoRendaFixa)
	WHERE IdPosicao = @IdPosicao; 

   FETCH NEXT FROM RendaFixaCursor
   INTO @IdCliente, 
		@IdTitulo,
		@TipoOperacao,
		@Quantidade ,
		@QuantidadeBloqueada,
		@DataOperacao,
		@PUOperacao,
		@TaxaOperacao,
		@PUMercado,
		@DataVolta,
		@TaxaVolta,
		@PUVolta,
		@IdCustodia,
		@TipoNegociacao,
		@IdPosicao,
		@IdOperacao;
END
CLOSE RendaFixaCursor;
DEALLOCATE RendaFixaCursor;
GO

--CURSOR SWAP
--Declara as variáveis para armazenar os valores do fetch
DECLARE @IdCliente INT,
		@TipoRegistro TINYINT,
		@NumeroContrato VARCHAR(20),
		@DataEmissao DATETIME,
		@DataVencimento DATETIME, 
		@ValorBase DECIMAL(16,2),
		@TipoPonta TINYINT,                                                                                                                       
		@IdIndice SMALLINT,                                                                                                                        
		@TaxaJuros DECIMAL(10,4),                                                                                                                       
		@Percentual DECIMAL(10,4),                                                                                                                      
		@TipoApropriacao TINYINT,                                                                                                                  
		@ContagemDias TINYINT,                                                                                                                     
		@BaseAno SMALLINT,                                                                                                                         
		@PontaContraParte TINYINT,                                                                                                             
		@IdIndiceContraParte SMALLINT,                                                                                                             
		@TaxaJurosContraParte DECIMAL(10,4), 
		@PercentualContraParte DECIMAL(10,4),                                                                                                           
		@ApropriacaoContraParte TINYINT,                                                                                                      
		@ContagemDiasContraParte TINYINT,
		@BaseAnoContraParte SMALLINT,
		@ValorParte DECIMAL(16,2),
		@ValorContraParte DECIMAL(16,2),
		@Saldo DECIMAL(16,2),
		@IdOperacao INT,
		@IdPosicao INT,
		@ComGarantia CHAR,
		@DiasUteis INT,
		@DiasCorridos INT

DECLARE SwapCursor CURSOR FOR
SELECT  IdCliente,
		TipoRegistro,
		NumeroContrato,
		DataEmissao,
		DataVencimento, 
		ValorBase,
		TipoPonta,
		IdIndice,
		TaxaJuros,
		Percentual,
		TipoApropriacao,
		ContagemDias,
		BaseAno,
		TipoPontaContraParte,
		IdIndiceContraParte,
		TaxaJurosContraParte,
		PercentualContraParte,
		TipoApropriacaoContraParte, 
		ContagemDiasContraParte,
		BaseAnoContraParte,
		ValorParte,
		ValorContraParte,
		Saldo,
		IdOperacao,
		IdPosicao,
		ComGarantia,
		DiasUteis,
		DiasCorridos
FROM PosicaoSwap
WHERE IdOperacao is null;

OPEN SwapCursor;
-- Armazena os valores nas variáveis
FETCH NEXT FROM SwapCursor
INTO  @IdCliente,
	  @TipoRegistro,
	  @NumeroContrato,
	  @DataEmissao,
	  @DataVencimento,
	  @ValorBase,
	  @TipoPonta,
	  @IdIndice,
	  @TaxaJuros,
	  @Percentual,
	  @TipoApropriacao,
	  @ContagemDias,
	  @BaseAno,
	  @PontaContraParte,
	  @IdIndiceContraParte,
	  @TaxaJurosContraParte,
	  @PercentualContraParte, 
	  @ApropriacaoContraParte, 
	  @ContagemDiasContraParte,
	  @BaseAnoContraParte,
	  @ValorParte,
	  @ValorContraParte,
	  @Saldo,
	  @IdOperacao,
	  @IdPosicao,
	  @ComGarantia,
	  @DiasUteis,
	  @DiasCorridos;

--Verifica se há mais linhas a serem percorridas
WHILE @@FETCH_STATUS = 0
BEGIN
	INSERT INTO OperacaoSwap(IdCliente, TipoRegistro, NumeroContrato, DataEmissao, DataVencimento, ValorBase, TipoPonta, IdIndice, TaxaJuros, Percentual, TipoApropriacao, ContagemDias,BaseAno, TipoPontaContraParte, IdIndiceContraParte,TaxaJurosContraParte,PercentualContraParte,TipoApropriacaoContraParte, ContagemDiasContraParte,BaseAnoContraParte, ValorIndicePartida, ValorIndicePartidaContraParte, DataRegistro, ComGarantia, DiasUteis, DiasCorridos) 
	VALUES(@IdCliente, @TipoRegistro, @NumeroContrato, @DataEmissao, @DataVencimento, @ValorBase, @TipoPonta,@IdIndice,@TaxaJuros,@Percentual,@TipoApropriacao,@ContagemDias, @BaseAno, @PontaContraParte, @IdIndiceContraParte, @TaxaJurosContraParte, @PercentualContraParte, @ApropriacaoContraParte, @ContagemDiasContraParte, @BaseAnoContraParte, @ValorParte, @ValorContraParte, GetDate(), @ComGarantia, @DiasUteis, @DiasCorridos);  

	UPDATE PosicaoSwap 
	SET IdOperacao = (select max(OperacaoSwap.IdOperacao) FROM OperacaoSwap)
	WHERE IdPosicao = @IdPosicao;

   FETCH NEXT FROM SwapCursor
   INTO @IdCliente,
	    @TipoRegistro,
	    @NumeroContrato,
	    @DataEmissao,
	    @DataVencimento,
	    @ValorBase,
	    @TipoPonta,
	    @IdIndice,
	    @TaxaJuros,
	    @Percentual,
	    @TipoApropriacao,
	    @ContagemDias,
	    @BaseAno,
	    @PontaContraParte,
	    @IdIndiceContraParte,
	    @TaxaJurosContraParte,
	    @PercentualContraParte, 
	    @ApropriacaoContraParte, 
	    @ContagemDiasContraParte,
	    @BaseAnoContraParte,
	    @ValorParte,
	    @ValorContraParte,
	    @Saldo,
	    @IdOperacao,
	    @IdPosicao,
	    @ComGarantia,
	    @DiasUteis,
	    @DiasCorridos;
END
CLOSE SwapCursor;
DEALLOCATE SwapCursor;
GO

--CURSOR FUNDO
--Declara as variáveis para armazenar os valores do fetch
DECLARE @IdCliente	INT, 
		@IdCarteira	INT,
		@ValorAplicacao DECIMAL(16,2),
		@DataAplicacao DATETIME,
		@DataConversao DATETIME,
		@Quantidade	DECIMAL(28,12),
		@CotaAplicacao DECIMAL(28,12),
		@IdOperacao INT,
		@IdPosicao INT

DECLARE FundoCursor CURSOR FOR
SELECT  IdCliente,
		IdCarteira,
		ValorAplicacao,
		DataAplicacao, 
		DataConversao, 
		Quantidade,
		CotaAplicacao, 
		IdOperacao,
		IdPosicao
FROM PosicaoFundo
WHERE IdOperacao is null;

OPEN FundoCursor;
-- Armazena os valores nas variáveis
FETCH NEXT FROM FundoCursor
INTO  @IdCliente, 
	  @IdCarteira,
	  @ValorAplicacao,
	  @DataAplicacao,
	  @DataConversao,
	  @Quantidade,
	  @CotaAplicacao,
	  @IdOperacao,
	  @IdPosicao;

--Verifica se há mais linhas a serem percorridas
WHILE @@FETCH_STATUS = 0
BEGIN
	INSERT INTO OperacaoFundo(IdCliente, IdCarteira, ValorBruto, Quantidade, DataOperacao, DataConversao, CotaOperacao, DataLiquidacao, DataAgendamento, TipoOperacao, IdFormaLiquidacao, Fonte) 
	VALUES(@IdCliente, @IdCarteira, @ValorAplicacao, @Quantidade, @DataAplicacao, @DataConversao,@CotaAplicacao, GetDate(),@DataAplicacao, 1, 1, 2);  

	UPDATE PosicaoFundo 
	SET IdOperacao = (select max(OperacaoFundo.IdOperacao) FROM OperacaoFundo)
	WHERE IdPosicao = @IdPosicao;

   FETCH NEXT FROM FundoCursor
   INTO @IdCliente, 
		@IdCarteira,
		@ValorAplicacao,
		@DataAplicacao,
		@DataConversao,
		@Quantidade,
		@CotaAplicacao,
		@IdOperacao,
		@IdPosicao;
END
CLOSE FundoCursor;
DEALLOCATE FundoCursor;
GO
--FIM	
	
--TAREFA CADASTRO COMPLEMENTAR
--INICIO
--ADD MENU
delete from PermissaoMenu where idMenu = 480;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,480
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 490;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,490
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 500;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,500
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 510;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,510
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 520;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,520
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

--CREATE TABLE
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementarTipoLista') = 0
Begin
 CREATE TABLE CadastroComplementarTipoLista
 (  
   IdTipoLista INT PRIMARY KEY identity NOT NULL, 
   NomeLista varchar(50),
   TipoLista int NOT NULL,   
   CONSTRAINT TipoUnico UNIQUE (NomeLista,TipoLista)
 ) 
End

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementarItemLista') = 0
Begin
 CREATE TABLE CadastroComplementarItemLista
 (  
   IdLista INT PRIMARY KEY identity NOT NULL, 
   IdTipoLista int FOREIGN KEY REFERENCES CadastroComplementarTipoLista(IdTipoLista) NOT NULL,
   ItemLista varchar(50) NOT NULL,   
 ) 
End

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementarCampos') = 0
Begin
 CREATE TABLE CadastroComplementarCampos
 (  
   IdCamposComplementares INT  PRIMARY KEY identity NOT NULL, 
   TipoCadastro int           NOT NULL, 
   NomeCampo varchar(50)      NOT NULL, 
   DescricaoCampo varchar(50) NULL, 
   ValorDefault varchar(20)   NULL,
   TipoCampo int              NOT NULL,
   CampoObrigatorio Char      NOT NULL,
   Tamanho int                NULL,
   CasasDecimais int          NULL
 ) 
End

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CadastroComplementar') = 0
Begin
 CREATE TABLE CadastroComplementar
 (  
   IdCadastroComplementares INT PRIMARY KEY identity NOT NULL, 
   IdCamposComplementares int FOREIGN KEY REFERENCES CadastroComplementarCampos(IdCamposComplementares),
   IdMercadoTipoPessoa int NOT NULL,
   DescricaoMercadoTipoPessoa varchar(50)    NOT NULL, 
   ValorCampo varchar(50)            NOT NULL
 ) 
End

ALTER TABLE CadastroComplementar
ALTER COLUMN IdMercadoTipoPessoa varchar(20)

--ADD COLUMN TO "Cliente"
IF NOT EXISTS(SELECT * FROM sys.columns 
	          WHERE NAME = N'RegimeEspecialTributacao' and Object_ID = Object_ID(N'Cliente'))
BEGIN
    ALTER TABLE Cliente
	ADD RegimeEspecialTributacao CHAR
END

--TAREFA 30698 - IMPORTAÇÃO DE ARQUIVOS DE ATIVOS DO MERCADO
--CREATE TABLE "Cotação ASEL007 - Cadastro Titulo Publico"
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CotacaoASEL007') = 0
BEGIN
	CREATE TABLE CotacaoASEL007
	(
		 Data DATETIME NULL,
		 CodigoTitulo VARCHAR(6) NULL,
	     DataVencimento DATETIME NULL,
		 PU DECIMAL(16,8) NULL	
	) 
END

--Inclusão de colunas para controle de cliente na geração de ativos e passivos.
if not exists(select 1 from syscolumns where id = object_id('EventoFundoMovimentoAtivos') and name = 'IdCliente')
BEGIN
	ALTER TABLE EventoFundoMovimentoAtivos ADD IdCliente INT NOT NULL;
END 

if not exists(select 1 from syscolumns where id = object_id('EventoFundoMovimentoCautelas') and name = 'IdCliente')
BEGIN
	ALTER TABLE EventoFundoMovimentoCautelas ADD IdCliente INT NOT NULL;
END 

if not exists(select 1 from syscolumns where id = object_id('EventoFundoPosicaoAtivos') and name = 'IdCliente')
BEGIN
	ALTER TABLE EventoFundoPosicaoAtivos ADD IdCliente INT NOT NULL;
END 

if not exists(select 1 from syscolumns where id = object_id('EventoFundoPosicaoCautelas') and name = 'IdCliente')
BEGIN
	ALTER TABLE EventoFundoPosicaoCautelas ADD IdCliente INT NOT NULL;
END 

--TAREFA 303 - CRIAR CADASTRO 7.1, 7.2 - PARAMETRIZAÇÃO PARA RECOLHIMENTO DE IR
--INICIO
--ADD MENU ITEM
DELETE FROM PermissaoMenu WHERE idMenu = 7720;
INSERT INTO [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7720
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 7430
--CREATE TABLE 
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ParametroAdministradorFundoInvestimento') = 0
BEGIN
	CREATE TABLE ParametroAdministradorFundoInvestimento
	(
		 IdParametro INT PRIMARY KEY identity NOT NULL, 
		 ExecucaoRecolhimento INT NOT NULL,
		 AliquotaIR INT NOT NULL,
		 DataRecolhimento DATETIME NULL,
		 Administrador INT FOREIGN KEY REFERENCES AgenteMercado(IdAgente) NOT NULL
	) 
END

--DROP COLUMN "Data Recolhimento"
IF EXISTS(select 1 from syscolumns where id = object_id('ParametroAdministradorFundoInvestimento') and name = 'DataRecolhimento')
BEGIN
    ALTER TABLE ParametroAdministradorFundoInvestimento
	DROP COLUMN DataRecolhimento 
END

--ADD COLUMN
IF NOT EXISTS(select 1 from syscolumns where id = object_id('EventoFundo') and name = 'ExecucaoRecolhimento')
BEGIN
	alter table EventoFundo add ExecucaoRecolhimento int null
END
IF NOT EXISTS(select 1 from syscolumns where id = object_id('EventoFundo') and name = 'AliquotaIR')
BEGIN
	alter table EventoFundo add AliquotaIR int null
END
IF NOT EXISTS(select 1 from syscolumns where id = object_id('EventoFundo') and name = 'DataRecolhimento')
BEGIN
	alter table EventoFundo add DataRecolhimento DateTime null
END

--ALTER COLUMN TYPE
ALTER TABLE ParametroAdministradorFundoInvestimento
ALTER COLUMN AliquotaIR int NULL
--FIM


--TAREFA 302 - CRIAR CADASTRO 7.3 - FORMA CONDOMÍNIO DE FUNDOS
--INICIO
--ADD NEW COLUMN
IF NOT EXISTS(SELECT * FROM sys.columns 
	          WHERE NAME = N'TipoFundo' and Object_ID = Object_ID(N'Carteira'))
BEGIN
	ALTER TABLE Carteira 
	ADD TipoFundo int not null DEFAULT 1
END

--ADD NEW TABLE
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'FundoInvestimentoFormaCondominio') = 0
BEGIN
	CREATE TABLE FundoInvestimentoFormaCondominio
	(
		 IdFormaCondominio INT PRIMARY KEY identity NOT NULL, 
		 DataInicioVigencia DATETIME NOT NULL,
		 FormaCondominio INT NOT NULL,
		 ExecucaoRecolhimento INT NOT NULL,
		 AliquotaIR INT NOT NULL,
		 DataRecolhimento DATETIME NULL,
		 FundoInvestimento INT FOREIGN KEY REFERENCES Carteira(IdCarteira) NOT NULL,
	) 
END
--ALTER NAME
IF EXISTS(SELECT * FROM sys.columns 
	          WHERE NAME = N'FundoInvestimento' and Object_ID = Object_ID(N'FundoInvestimentoFormaCondominio'))
BEGIN
	exec sp_rename 'FundoInvestimentoFormaCondominio.FundoInvestimento', 'IdCarteira'
END

--ADD NEW MENU	
delete from PermissaoMenu where idMenu = 4890;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,4890
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 4880
--FIM

--TAREFA PAS 304
--INICIO
--CRIA TABELA
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DesenquadramentoTributario') = 0
BEGIN
	CREATE TABLE DesenquadramentoTributario
	(
		 IdDesenquadramentoTributario INT PRIMARY KEY identity NOT NULL, 
		 TipoOcorrencia INT NOT NULL,
		 Data DATETIME NOT NULL,
		 ClassificacaoTributariaAtual INT NULL,
		 MotivoMudancaOcorrencia VARCHAR(200),
		 ExecucaoRecolhimento INT NULL,
		 AliquotaIR INT NULL,
		 DataRecolhimento DATETIME NULL,
		 FundoInvestimento INT FOREIGN KEY REFERENCES Carteira(IdCarteira) NOT NULL,
	) 
END

--ALTER NAME
IF EXISTS(SELECT * FROM sys.columns 
	          WHERE NAME = N'FundoInvestimento' and Object_ID = Object_ID(N'DesenquadramentoTributario'))
BEGIN
	exec sp_rename 'DesenquadramentoTributario.FundoInvestimento', 'IdCarteira'	
END

--ADD NEW MENU
delete from PermissaoMenu where idMenu = 4900;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,4900
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 4880
--FIM

--TAREFA PAS 305
--INICIO
--ADD NEW MENU
delete from PermissaoMenu where idMenu = 4910;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,4910
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 4880
--FIM

--TAREFA PAS 93
--INICIO
--ADD NEW MENU
delete from PermissaoMenu where idMenu = 540;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,540
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
--FIM

--ADD NEW MENU
delete from PermissaoMenu where idMenu = 550;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,550
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
--FIM

--ADD NEW MENU
delete from PermissaoMenu where idMenu = 560;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,560
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
  
--ADD NEW MENU
delete from PermissaoMenu where idMenu = 570;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,570
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
  
--ADD NEW MENU
delete from PermissaoMenu where idMenu = 580;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,580
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
  
--ADD NEW MENU
delete from PermissaoMenu where idMenu = 590;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,590
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
  
--ADD NEW MENU
delete from PermissaoMenu where idMenu = 600;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,600
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
  
--ADD NEW MENU
delete from PermissaoMenu where idMenu = 610;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,610
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
--FIM

--CREATE TABLE "AgenciaClassificadora"
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AgenciaClassificadora') = 0
Begin
	CREATE TABLE AgenciaClassificadora
	(  
		 IdAgenciaClassificadora INT PRIMARY KEY identity NOT NULL, 
		 CodigoAgencia int NOT NULL,
		 DescricaoAgencia varchar(255) NOT NULL,
		 CNPJ varchar(30) NULL,
		 CodigoExterno int NULL
	) 
End

--CREATE TABLE "ExcecoesTributacaoIR"
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PadraoNotas') = 0
Begin
	CREATE TABLE PadraoNotas
	(
		 IdPadraoNotas INT PRIMARY KEY identity NOT NULL, 
		 ItemAvaliado int NULL,
	     DataInicial DateTime NOT NULL,
		 DataFinal DateTime NOT NULL,	
		 CodigoNota Varchar(50) NOT NULL,
		 DescricaoNota varchar(200) NULL,
		 Sequencia int NOT NULL,
		 IdAgencia int FOREIGN KEY REFERENCES AgenciaClassificadora(IdAgenciaClassificadora) NULL
	) 
End

--CREATE TABLE "PerfilFaixaRating"
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PerfilFaixaRating') = 0
Begin
	CREATE TABLE PerfilFaixaRating
	(
		 IdPerfilFaixaRating INT PRIMARY KEY identity NOT NULL, 
		 CodigoPerfil varchar(50) NOT NULL,
	     DescricaoPerfil varchar(200) NULL
	) 
End

--CREATE TABLE "PerfilClienteFaixaRating"
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PerfilClienteFaixaRating') = 0
Begin
	CREATE TABLE PerfilClienteFaixaRating
	(
		 IdPerfilClienteFaixaRating INT PRIMARY KEY identity NOT NULL, 
		 Descricao varchar(200) NULL,
		 IdPerfilFaixaRating int FOREIGN KEY REFERENCES PerfilFaixaRating(IdPerfilFaixaRating) NULL
	) 
End

--CREATE TABLE "PerfilCliente"
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PerfilCliente') = 0
Begin
	CREATE TABLE PerfilCliente
	(
		 IdPerfilCliente INT PRIMARY KEY identity NOT NULL, 
		 IdPerfilClienteFaixaRating int FOREIGN KEY REFERENCES PerfilClienteFaixaRating(IdPerfilClienteFaixaRating) NOT NULL,
		 IdCarteira int FOREIGN KEY REFERENCES Carteira(IdCarteira) NOT NULL
	) 
End

--CREATE TABLE "ClassificacaoFaixaRating"
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ClassificacaoFaixaRating') = 0
Begin
	CREATE TABLE ClassificacaoFaixaRating
	(
		 IdClassificacaoFaixaRating INT PRIMARY KEY identity NOT NULL, 
		 SequenciaAgencia int NOT NULL,
		 DataVigencia DateTime NOT NULL,
		 Classificacao int NULL,
		 IdPerfilFaixaRating int FOREIGN KEY REFERENCES PerfilFaixaRating(IdPerfilFaixaRating) NOT NULL,
		 IdAgenciaClassificadora int FOREIGN KEY REFERENCES AgenciaClassificadora(IdAgenciaClassificadora) NOT NULL,
		 IdPadraoNotas int FOREIGN KEY REFERENCES PadraoNotas(IdPadraoNotas) NULL
	) 
End

--CREATE TABLE "ClassificacaoAtivo"
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ClassificacaoAtivo') = 0
Begin
	CREATE TABLE ClassificacaoAtivo
	(
		 IdClassificacaoAtivo INT PRIMARY KEY identity NOT NULL, 
		 ItemAvaliado int NOT NULL,
		 Ativo int NOT NULL,
		 IdAgenciaClassificadora int FOREIGN KEY REFERENCES AgenciaClassificadora(IdAgenciaClassificadora) NOT NULL,
		 IdPadraoNotas int FOREIGN KEY REFERENCES PadraoNotas(IdPadraoNotas) NULL
	) 
End

--DROP COLUMN "DataFinal"
IF EXISTS(select 1 from syscolumns where id = object_id('PadraoNotas') and name = 'DataFinal')
BEGIN
	ALTER TABLE PadraoNotas
	DROP COLUMN DataFinal
END

--RENAME COLUMN
IF EXISTS(SELECT * FROM sys.columns 
	          WHERE NAME = N'PadraoNotas' and Object_ID = Object_ID(N'PadraoNotas.DataInicial'))
BEGIN
	exec sp_rename 'PadraoNotas.DataInicial', 'DataVigencia'
END
--FIM

--TAREFA PAS 161
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AgendaComeCotas') = 0
BEGIN
	CREATE TABLE AgendaComeCotas
	(
		 IdAgendamentoComeCotas INT PRIMARY KEY identity NOT NULL,
		 IdCarteira int FOREIGN KEY REFERENCES Carteira(IdCarteira) NOT NULL,
		 DataLancamento datetime NOT NULL,
		 DataVencimento datetime NOT NULL,
		 TipoEvento int NOT NULL,
		 IdPosicao int NOT NULL,
		 QuantidadeCotas decimal(28,12) NOT NULL,
		 ValorIR decimal(16,2) NOT NULL,
		 ValorIOF decimal(16,2) NOT NULL,
		 ValorIOFVirtual decimal(16,2) NOT NULL,
		 Rendimento decimal(16,2) NULL,
		 RendimentoCompensado decimal(16,2) NULL,
		 PrejuizoUsado decimal(16,2) NULL
	) 
END

--ALTER COLUMN Posicao RendaFixa, Cotista, SWAP, Fundo
alter table PosicaoRendaFixa alter column IdOperacao int NULL;
alter table PosicaoSwap alter column IdOperacao int NULL;
alter table PosicaoFundo alter column IdOperacao int NULL;
alter table PosicaoCotista alter column IdOperacao int NULL;

--Alter AgendaComeCotas
IF NOT EXISTS(SELECT * FROM sys.columns 
	          WHERE NAME = N'AliquotaIR' and Object_ID = Object_ID(N'AgendaComeCotas'))
BEGIN
	ALTER TABLE AgendaComeCotas 
	ADD AliquotaIR int NOT NULL default 0
END

--Dropa e cria novamente a tabela de AgendamentoComeCotas
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AgendaComeCotas') > 0
BEGIN

	drop table AgendaComeCotas;

	CREATE TABLE AgendaComeCotas
	(
		 IdAgendamentoComeCotas INT PRIMARY KEY identity NOT NULL,
		 IdCarteira int FOREIGN KEY REFERENCES Carteira(IdCarteira) NOT NULL,
		 DataLancamento datetime NOT NULL,
		 DataVencimento datetime NOT NULL,
		 TipoEvento int NOT NULL,
		 IdPosicao int NOT NULL,
		 DataUltimoIR datetime NOT NULL,
		 Quantidade decimal(28,12) NOT NULL,
		 QuantidadeAntesCortes decimal(28,12) NOT NULL,
		 ValorCotaAplic decimal(28,12) NOT NULL,
		 ValorCotaUltimoPagamentoIR decimal(28,12) NOT NULL,
		 ValorCota decimal(28,12) NOT NULL,
		 AliquotaCC decimal(5,2) NOT NULL,
		 RendimentoBrutoDesdeAplicacao decimal(16,2) NOT NULL,
 		 RendimentoDesdeUltimoPagamentoIR decimal(16,2) NOT NULL,
		 NumDiasCorridosDesdeAquisicao int NOT NULL,
		 PrazoIOF int NOT NULL,
		 AliquotaIOF decimal(5,2) NOT NULL,
		 ValorIOF  decimal(28,12) NOT NULL,
		 ValorIOFVirtual decimal(28,12) NOT NULL,
		 PrejuizoUsado decimal(16,2) NOT NULL,
		 RendimentoCompensado decimal(16,2) NOT NULL,
		 ValorIRAgendado decimal(28,12) NOT NULL,
		 ValorIRPago decimal(16,2) NOT NULL,
		 Residuo15 decimal(28,12) NOT NULL,
		 Residuo175 decimal(28,12) NOT NULL,
		 Residuo20 decimal(28,12) NOT NULL,
		 Residuo225 decimal(28,12) NOT NULL,
		 QuantidadeComida decimal(28,12) NOT NULL,
		 QuantidadeFinal decimal(28,12) NOT NULL,
		 ExecucaoRecolhimento int NOT NULL,
		 TipoAliquotaIR int NOT NULL
	) 
END

--Alter AgendaComeCotas
IF NOT EXISTS(SELECT * FROM sys.columns 
	          WHERE NAME = N'TipoPosicao' and Object_ID = Object_ID(N'AgendaComeCotas'))
BEGIN
	ALTER TABLE AgendaComeCotas 
	ADD TipoPosicao int
END

--PAS ALTERAÇÃO POSIÇÃO DOS MENUS
--DE: Mercado > Fundos > Cadastro de Carteiras, Tabelas de Categoria, Forma de Condominio, Desenquadramento Tributário
--PARA: Carteira Cota > Carteira > Cadastro de Carteiras, Tabelas de Categoria, Forma de Condominio, Desenquadramento Tributário
delete from PermissaoMenu where idMenu = 4890;
delete from PermissaoMenu where idMenu = 4900;
delete from PermissaoMenu where idMenu = 4870;
--FIM

--PAS-250
--Tabela Cotista
IF EXISTS (select 1 from sysconstraints where object_name(id) = 'Cotista' and object_name(constid) = 'Pessoa_Cotista_FK1')
BEGIN
	ALTER TABLE Cotista DROP CONSTRAINT Pessoa_Cotista_FK1
END	

if not exists(select 1 from syscolumns where id = object_id('Cotista') and name = 'IdPessoa')
BEGIN
	ALTER TABLE Cotista ADD IdPessoa int;
	EXEC('UPDATE Cotista SET IdPessoa = IdCotista')
END

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'Cotista' and object_name(constid) = 'Cotista_IdPessoa_FK1')
BEGIN
	ALTER TABLE Cotista ADD CONSTRAINT Cotista_IdPessoa_FK1 FOREIGN KEY (IdPessoa) REFERENCES Pessoa(IdPessoa) on delete cascade
END	

--Tabela Cliente
IF EXISTS (select 1 from sysconstraints where object_name(id) = 'Cliente' and object_name(constid) = 'Pessoa_Cliente_FK1')
BEGIN
	ALTER TABLE Cliente DROP CONSTRAINT Pessoa_Cliente_FK1
END	

if not exists(select 1 from syscolumns where id = object_id('Cliente') and name = 'IdPessoa')
BEGIN
	ALTER TABLE Cliente ADD IdPessoa int;
	EXEC('UPDATE Cliente SET IdPessoa = IdCliente')
END	

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'Cotista' and object_name(constid) = 'Cotista_IdPessoa_FK1')
BEGIN
	ALTER TABLE Cliente ADD CONSTRAINT Cliente_IdPessoa_FK1 FOREIGN KEY (IdPessoa) REFERENCES Pessoa(IdPessoa) on delete cascade
END	

--CREATE COLUMN
if not exists(select 1 from syscolumns where id = object_id('Cotista') and name = 'IdCarteira')
BEGIN
	ALTER TABLE Cotista ADD IdCarteira INT NULL 
END	

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'Cotista' and object_name(constid) = 'Cotista_IdCarteira_FK1')
BEGIN
	ALTER TABLE Cotista ADD CONSTRAINT Cotista_IdCarteira_FK1 FOREIGN KEY (IdCarteira) REFERENCES Carteira(IdCarteira) on delete set null
END	

--UPDATE COTISTA
EXEC('BEGIN TRANSACTION T1; update Cotista set Cotista.IdCarteira = OperacaoCotista.IdCarteira from OperacaoCotista inner join Cotista on (OperacaoCotista.IdCotista = Cotista.IdCotista) where OperacaoCotista.IdCotista = OperacaoCotista.IdCarteira and OperacaoCotista.IdCarteira in(select IdCarteira from Carteira left join Cliente on(Carteira.IdCarteira = Cliente.IdCliente) where Cliente.TipoControle != 1 and Cliente.StatusAtivo = 1) COMMIT TRANSACTION T1;');

--PAS ALTERAÇÃO POSIÇÃO DOS MENUS
delete from PermissaoMenu where idMenu = 7800;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7800
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 7810;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7810
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 7820;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7820
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 7830;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7830
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 7840;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7840
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

--PAS-448 - Correcao de Posicao sem operacao.
BEGIN 
	DECLARE @IdCotista	INT, 
			@IdCarteira	INT,
			@ValorAplicacao DECIMAL(16,2),
			@DataAplicacao DATETIME,
			@DataConversao DATETIME,
			@Quantidade	DECIMAL(28,12),
			@CotaAplicacao DECIMAL(28,12),
			@IdOperacao INT,
			@IdPosicao INT,
			@DataDia DATETIME
	
	DECLARE CotistaCursor CURSOR FOR
	SELECT PosicaoCotista.IdCotista,
			PosicaoCotista.IdCarteira,
			PosicaoCotista.ValorAplicacao,
			PosicaoCotista.DataAplicacao,
			PosicaoCotista.DataConversao,
			PosicaoCotista.Quantidade,
			PosicaoCotista.CotaAplicacao,
			PosicaoCotista.IdOperacao,
			PosicaoCotista.IdPosicao,
			Cliente.DataDia
	FROM PosicaoCotista, Cliente
	WHERE PosicaoCotista.IdCarteira = Cliente.IdCliente
	and  PosicaoCotista.IdOperacao not in (select OperacaoCotista.IdOperacao from OperacaoCotista);

	OPEN CotistaCursor;
	-- Armazena os valores nas variáveis
	FETCH NEXT FROM CotistaCursor
	INTO @IdCotista, 
		@IdCarteira,
		@ValorAplicacao,
		@DataAplicacao,
		@DataConversao,
		@Quantidade,
		@CotaAplicacao,
		@IdOperacao,
		@IdPosicao,
		@DataDia 
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO OperacaoCotista(IdCotista, IdCarteira, ValorBruto, Quantidade, DataOperacao, DataConversao, CotaOperacao, DataLiquidacao, DataAgendamento, TipoOperacao, IdFormaLiquidacao, Fonte) 
		VALUES(@IdCotista, @IdCarteira, @ValorAplicacao, @Quantidade, @DataDia, @DataConversao,@CotaAplicacao, GetDate(),@DataAplicacao, 1, 1, 2);  

		UPDATE PosicaoCotista
		SET IdOperacao = (select max(OperacaoCotista.IdOperacao) FROM OperacaoCotista)
		WHERE IdPosicao = @IdPosicao;

		FETCH NEXT FROM CotistaCursor
		INTO @IdCotista, 
			@IdCarteira,
			@ValorAplicacao,
			@DataAplicacao,
			@DataConversao,
			@Quantidade,
			@CotaAplicacao,
			@IdOperacao,
			@IdPosicao,
			@DataDia 
	END

	CLOSE CotistaCursor;
	DEALLOCATE CotistaCursor;
END 
GO


if not EXISTS(select 1 from sysconstraints where object_name(id) = 'PosicaoCotista' and object_name(constid) = 'Operacao_PosicaoCotista_FK1')
Begin
	ALTER TABLE PosicaoCotista
	ADD CONSTRAINT Operacao_PosicaoCotista_FK1
	FOREIGN KEY (IdOperacao)
	REFERENCES OperacaoCotista(IdOPeracao);
End
--PAS-448 - Fim


--PAS-403 Alteração no cadastro de carteira e criacao de novas telas para fundos off shore
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ClassesOffShore') = 0
BEGIN

	CREATE TABLE ClassesOffShore
	(
		 IdClassesOffShore INT PRIMARY KEY NOT NULL,
		 Nome varchar(100) NOT NULL,
		 DataInicio datetime NOT NULL,
		 SerieUnica char(1) NOT NULL,
		 FrequenciaSerie int NOT NULL,
		 PoliticaInvestimentos varchar(200) NULL,
		 Moeda smallint FOREIGN KEY REFERENCES Moeda(IdMoeda) NULL,
		 Domicilio int FOREIGN KEY REFERENCES LocalNegociacao(IdLocalNegociacao) NULL,
		 TaxaAdm decimal(16,2) NULL,
		 TaxaCustodia decimal(16,2) NULL,
		 TaxaPerformance decimal(16,2) NULL,
		 IndexadorPerf smallint FOREIGN KEY REFERENCES Indice(IdIndice) NULL,
		 FreqCalcPerformance int NULL,
		 LockUp int NULL,
		 HardSoft int NULL,
		 PenalidadeResgate decimal(5,2) NULL,
		 VlMinimoAplicacao decimal(16,2) NULL,
		 VlMinimoResgate decimal(16,2) NULL,
		 VlMinimoPermanencia decimal(16,2) NULL,
		 QtdDiasConvResgate int NULL,
		 QtdDiasLiqFin int NULL,
		 HoldBack decimal(5,2) NULL,
		 AnivHoldBack datetime NULL,
		 SidePocket char(1) NOT NULL default 'N'
	) 
END

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'ClassesOffShore' and object_name(constid) = 'Classes_Carteira_FK1')
BEGIN
	ALTER TABLE [dbo].ClassesOffShore  WITH CHECK ADD  CONSTRAINT [Classes_Carteira_FK1] FOREIGN KEY([IdClassesOffShore])
	REFERENCES [dbo].[Carteira] ([IdCarteira])
END

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SeriesOffShore') = 0
BEGIN

	CREATE TABLE SeriesOffShore
	(
		 IdSeriesOffShore INT PRIMARY KEY identity NOT NULL,
		 IdClassesOffShore int  NOT NULL,
		 Mnemonico varchar(100) NOT NULL,
		 Descricao varchar(100) NULL,
		 VlCotaInicial decimal(28,12) NULL,
		 ISIN varchar(12) NULL,
		 DataRollUp datetime NULL,
		 Inativa char(1) NOT NULL DEFAULT 'N'
	)
END

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'SeriesOffShore' and object_name(constid) = 'Serie_Classes_FK1')
BEGIN
	ALTER TABLE [dbo].SeriesOffShore  WITH CHECK ADD  CONSTRAINT [Serie_Classes_FK1] FOREIGN KEY([IdClassesOffShore])
	REFERENCES [dbo].[ClassesOffShore] ([IdClassesOffShore])
END

IF NOT EXISTS (select 1 from TipoCliente where IdTipo = 801)
BEGIN
	insert into TipoCliente (IdTipo, Descricao) Values (801, 'Off Shore');
END

--PAS-408
if not exists(select 1 from syscolumns where id = object_id('SeriesOffShore') and name = 'DataInicioAplicacao')
BEGIN
	ALTER TABLE SeriesOffShore ADD DataInicioAplicacao datetime NULL 
END	

if not exists(select 1 from syscolumns where id = object_id('SeriesOffShore') and name = 'DataFimAplicacao')
BEGIN
	ALTER TABLE SeriesOffShore ADD DataFimAplicacao datetime NULL 
END	

if not exists(select 1 from syscolumns where id = object_id('SeriesOffShore') and name = 'SerieBase')
BEGIN
	ALTER TABLE SeriesOffShore ADD SerieBase char(1) NULL
END	

ALTER TABLE Pessoa ALTER COLUMN Tipo tinyint NULL;

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdContaCorrenteOrigem')
BEGIN
	ALTER TABLE OperacaoCotista ADD IdContaCorrenteOrigem int NULL
END

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCotista' and object_name(constid) = 'Operacao_ContaCorrenteOrigem_FK1')
BEGIN
	ALTER TABLE [dbo].OperacaoCotista  WITH CHECK ADD  CONSTRAINT [Operacao_ContaCorrenteOrigem_FK1] FOREIGN KEY([IdContaCorrenteOrigem])
	REFERENCES [dbo].[ContaCorrente] ([IdConta])
END

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdContaCorrenteDestino')
BEGIN
	ALTER TABLE OperacaoCotista ADD IdContaCorrenteDestino int NULL
END	

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCotista' and object_name(constid) = 'Operacao_ContaCorrenteDestino_FK1')
BEGIN
	ALTER TABLE [dbo].OperacaoCotista  WITH CHECK ADD  CONSTRAINT [Operacao_ContaCorrenteDestino_FK1] FOREIGN KEY([IdContaCorrenteDestino])
	REFERENCES [dbo].[ContaCorrente] ([IdConta])
END

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'ValorDespesas')
BEGIN
	ALTER TABLE OperacaoCotista ADD ValorDespesas decimal(16,2) NULL
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'ValorTaxas')
BEGIN
	ALTER TABLE OperacaoCotista ADD ValorTaxas decimal(16,2) NULL
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'ValorTributos')
BEGIN
	ALTER TABLE OperacaoCotista ADD ValorTributos decimal(16,2) NULL
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'PenalidadeLockUp')
BEGIN
	ALTER TABLE OperacaoCotista ADD PenalidadeLockUp decimal(16,2) NULL
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'ValorHoldBack')
BEGIN
	ALTER TABLE OperacaoCotista ADD ValorHoldBack decimal(16,2) NULL
END	

update OperacaoCotista set ValorDespesas = 0, ValorTaxas = 0, ValorTributos = 0, PenalidadeLockUp = 0, ValorHoldBack = 0
where valorTaxas is null;

if not exists(select 1 from sysobjects where xtype='D' and name = 'DF_ValorDespesas')
BEGIN
	ALTER TABLE OperacaoCotista ADD CONSTRAINT DF_ValorDespesas DEFAULT 0 FOR ValorDespesas;
END

if not exists(select 1 from sysobjects where xtype='D' and name = 'DF_ValorTaxas')
BEGIN
	ALTER TABLE OperacaoCotista ADD CONSTRAINT DF_ValorTaxas DEFAULT 0 FOR ValorTaxas;
END

if not exists(select 1 from sysobjects where xtype='D' and name = 'DF_ValorTributos')
BEGIN
	ALTER TABLE OperacaoCotista ADD CONSTRAINT DF_ValorTributos DEFAULT 0 FOR ValorTributos;
END

if not exists(select 1 from sysobjects where xtype='D' and name = 'DF_PenalidadeLockUp')
BEGIN
	ALTER TABLE OperacaoCotista ADD CONSTRAINT DF_PenalidadeLockUp DEFAULT 0 FOR PenalidadeLockUp;
END

if not exists(select 1 from sysobjects where xtype='D' and name = 'DF_ValorHoldBack')
BEGIN
	ALTER TABLE OperacaoCotista ADD CONSTRAINT DF_ValorHoldBack DEFAULT 0 FOR ValorHoldBack;
END

--
if not exists(select 1 from syscolumns where id = object_id('ClassesOffShore') and name = 'QtdDiasConvAplicacao')
BEGIN
	ALTER TABLE ClassesOffShore ADD QtdDiasConvAplicacao int NULL
END	

if not exists(select 1 from syscolumns where id = object_id('ClassesOffShore') and name = 'QtdDiasLiqFinAplic')
BEGIN
	ALTER TABLE ClassesOffShore ADD QtdDiasLiqFinAplic int NULL
END	

--Ajusta campos de conta corrente da operação cotista
if exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdContaCorrenteDestino')
BEGIN
	ALTER TABLE [dbo].[OperacaoCotista] DROP CONSTRAINT [Operacao_ContaCorrenteDestino_FK1]
	ALTER TABLE OperacaoCotista DROP COLUMN IdContaCorrenteDestino;
END	

if exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdContaCorrenteOrigem')
BEGIN
	ALTER TABLE [dbo].[OperacaoCotista] DROP CONSTRAINT [Operacao_ContaCorrenteOrigem_FK1]
	ALTER TABLE OperacaoCotista DROP COLUMN IdContaCorrenteOrigem;
END	


if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'IdContaCorrente')
BEGIN
	ALTER TABLE OperacaoCotista ADD IdContaCorrente int NULL;
END	

IF NOT EXISTS (select 1 from sysconstraints where object_name(id) = 'OperacaoCotista' and object_name(constid) = 'Operacao_ContaCorrente_FK1')
BEGIN
	ALTER TABLE [dbo].OperacaoCotista  WITH CHECK ADD  CONSTRAINT [Operacao_ContaCorrente_FK1] FOREIGN KEY([IdContaCorrente])
	REFERENCES [dbo].[ContaCorrente] ([IdConta])
END
