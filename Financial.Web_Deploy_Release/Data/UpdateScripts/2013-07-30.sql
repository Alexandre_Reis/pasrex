﻿declare @data_versao char(10)
set @data_versao = '2013-07-30'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Menu de Implantação de Ajuste em Conta Corrente em Lote
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,6130,'S','S','S','S' from grupousuario where idgrupo <> 0
go
