declare @data_versao char(10)
set @data_versao = '2013-11-11'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table usuario add Logotipo varchar(100) null
alter table cliente add Logotipo varchar(100) null
go

