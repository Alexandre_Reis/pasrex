declare @data_versao char(10)
set @data_versao = '2014-01-28'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table titulorendafixa alter column codigointerface varchar(20)
go