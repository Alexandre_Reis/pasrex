declare @data_versao char(10)
set @data_versao = '2013-04-05'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

DROP TABLE ContabSaldo
GO

ALTER TABLE ContabLancamento DROP CONSTRAINT ContabLancamento_ContabPlanoContas_FK1
GO
ALTER TABLE ContabLancamento DROP CONSTRAINT ContabLancamento_ContabPlanoContas_FK2
GO

DROP TABLE ContabPlanoContas
go

CREATE TABLE ContabConta(
	IdConta int IDENTITY(1,1) NOT NULL,
	IdPlano int NOT NULL,
	TipoConta char(1) NOT NULL,
	Descricao varchar(max) NOT NULL,
	IdContaMae int NULL,
	Codigo varchar(50) NULL,
	CodigoReduzida varchar(8) NULL,
 CONSTRAINT PK_ContabConta PRIMARY KEY CLUSTERED 
(
	IdConta ASC
)
)
GO

ALTER TABLE ContabConta WITH CHECK ADD  CONSTRAINT ContabConta_ContabConta_FK1 FOREIGN KEY(IdContaMae)
REFERENCES ContabConta (IdConta)
GO

ALTER TABLE ContabConta WITH CHECK ADD  CONSTRAINT ContabConta_ContabPlano_FK1 FOREIGN KEY(IdPlano)
REFERENCES ContabPlano (IdPlano)
GO

INSERT INTO ContabConta
(IdPlano
,TipoConta
,Descricao
,IdContaMae
,Codigo
,CodigoReduzida)
SELECT IdPlano, 
CASE SUBSTRING(ContaDebito, 1, 1) WHEN '1' THEN 'A' WHEN '2' THEN 'A' WHEN 3 THEN 'A' ELSE 'P' END, '', null, ContaDebito, ContaDebitoReduzida FROM ContabRoteiro
GROUP BY IdPlano, ContaDebito, ContaDebitoReduzida

INSERT INTO ContabConta
(IdPlano
,TipoConta
,Descricao
,IdContaMae
,Codigo
,CodigoReduzida)
SELECT IdPlano,
CASE SUBSTRING(ContaCredito, 1, 1) WHEN '1' THEN 'A' WHEN '2' THEN 'A' WHEN 3 THEN 'A' ELSE 'P' END, '', null, ContaCredito, ContaCreditoReduzida FROM ContabRoteiro
WHERE ContaCredito NOT IN (SELECT Codigo FROM ContabConta)
GROUP BY IdPlano, ContaCredito, ContaCreditoReduzida
GO

CREATE TABLE ContabLancamentoAux(
	IdLancamento int IDENTITY(1,1) NOT NULL,
	IdCliente int NOT NULL,
	DataLancamento datetime NOT NULL,
	DataRegistro datetime NOT NULL,	
	IdContaDebito int NOT NULL,
	IdContaCredito int NOT NULL,
	Valor decimal(16, 2) NOT NULL,
	ContaDebito varchar(20) NOT NULL,
	ContaCredito varchar(20) NOT NULL,
	Origem int NOT NULL,
	Identificador varchar(20) NULL,
	Descricao varchar(200) NOT NULL,
	IdPlano int NOT NULL,
	Fonte tinyint NOT NULL,
	IdCentroCusto int NULL
 CONSTRAINT ContabLancamento_PK2 PRIMARY KEY NONCLUSTERED 
(
	IdLancamento ASC
)
) 
GO

INSERT INTO ContabLancamentoAux
(IdCliente
,DataLancamento
,DataRegistro
,IdContaDebito
,IdContaCredito
,Valor
,ContaDebito
,ContaCredito
,Origem
,Identificador
,Descricao
,IdPlano
,Fonte
,IdCentroCusto)
SELECT IdCliente
,DataLancamento
,DataRegistro
,0
,0
,Valor
,ContaDebito
,ContaCredito
,Origem
,Identificador
,Descricao
,IdPlano
,Fonte
,IdCentroCusto 
FROM ContabLancamento
GO

UPDATE ContabLancamentoAux SET IdContaDebito = c.IdConta
FROM ContabLancamentoAux l, ContabConta c
WHERE l.ContaDebito = c.Codigo
GO

UPDATE ContabLancamentoAux SET IdContaCredito = c.IdConta
FROM ContabLancamentoAux l, ContabConta c
WHERE l.ContaCredito = c.Codigo
GO

DROP TABLE ContabLancamento
GO

CREATE TABLE ContabLancamento(
	IdLancamento int IDENTITY(1,1) NOT NULL,
	IdCliente int NOT NULL,
	DataLancamento datetime NOT NULL,
	DataRegistro datetime NOT NULL,	
	IdContaDebito int NOT NULL,
	IdContaCredito int NOT NULL,
	Valor decimal(16, 2) NOT NULL,
	ContaDebito varchar(20) NOT NULL,
	ContaCredito varchar(20) NOT NULL,
	Origem int NOT NULL,
	Identificador varchar(20) NULL,
	Descricao varchar(200) NOT NULL,
	IdPlano int NOT NULL,
	Fonte tinyint NOT NULL,
	IdCentroCusto int NULL
 CONSTRAINT ContabLancamento_PK PRIMARY KEY NONCLUSTERED 
(
	IdLancamento ASC
)
) 
GO

ALTER TABLE ContabLancamento WITH CHECK ADD  CONSTRAINT Cliente_ContabLancamento_FK1 FOREIGN KEY(IdCliente)
REFERENCES Cliente (IdCliente)
ON DELETE CASCADE
GO

ALTER TABLE ContabLancamento WITH CHECK ADD  CONSTRAINT ContabCentroCusto_ContabLancamento_FK1 FOREIGN KEY(IdCentroCusto)
REFERENCES ContabCentroCusto (IdCentroCusto)
ON DELETE CASCADE
GO

ALTER TABLE ContabLancamento WITH CHECK ADD  CONSTRAINT ContabLancamento_ContabConta_FK1 FOREIGN KEY(IdContaCredito)
REFERENCES ContabConta (IdConta)
GO

ALTER TABLE ContabLancamento WITH CHECK ADD  CONSTRAINT ContabLancamento_ContabConta_FK2 FOREIGN KEY(IdContaDebito)
REFERENCES ContabConta (IdConta)
GO

ALTER TABLE ContabLancamento  WITH CHECK ADD  CONSTRAINT ContabLancamento_ContabPlano_FK1 FOREIGN KEY(IdPlano)
REFERENCES ContabPlano (IdPlano)
ON DELETE CASCADE
GO


INSERT INTO ContabLancamento
(IdCliente
,DataLancamento
,DataRegistro
,IdContaDebito
,IdContaCredito
,Valor
,ContaDebito
,ContaCredito
,Origem
,Identificador
,Descricao
,IdPlano
,Fonte
,IdCentroCusto)
SELECT IdCliente
,DataLancamento
,DataRegistro
,IdContaDebito
,IdContaCredito
,Valor
,ContaDebito
,ContaCredito
,Origem
,Identificador
,Descricao
,IdPlano
,Fonte
,IdCentroCusto 
FROM ContabLancamentoAux

DROP TABLE ContabLancamentoAux
GO

CREATE TABLE ContabSaldo(
	IdCliente int NOT NULL,
	IdConta int NOT NULL,
	IdPlano int NOT NULL,
	Data datetime NOT NULL,
	SaldoAnterior decimal(16, 2) NOT NULL,
	TotalDebito decimal(16, 2) NOT NULL,
	TotalCredito decimal(16, 2) NOT NULL,
	SaldoFinal decimal(16, 2) NOT NULL,
 CONSTRAINT PK_ContabSaldo PRIMARY KEY CLUSTERED 
(
	IdCliente ASC,
	IdConta ASC,
	Data ASC
)
)
GO

ALTER TABLE ContabSaldo  WITH CHECK ADD  CONSTRAINT ContabSaldo_ContabConta_FK1 FOREIGN KEY(IdConta)
REFERENCES ContabConta (IdConta)
ON DELETE CASCADE
GO

ALTER TABLE ContabSaldo  WITH CHECK ADD  CONSTRAINT ContabSaldo_ContabPlano_FK1 FOREIGN KEY(IdPlano)
REFERENCES ContabPlano (IdPlano)
ON DELETE CASCADE
GO

ALTER TABLE ContabSaldo WITH CHECK ADD  CONSTRAINT ContabSaldo_ContabLancamento_FK1 FOREIGN KEY(IdCliente)
REFERENCES Cliente (IdCliente)
ON DELETE CASCADE
GO

ALTER TABLE ContabRoteiro ADD IdContaCredito int NULL
GO
UPDATE ContabRoteiro SET IdContaCredito = C.IdConta
FROM ContabConta C, ContabRoteiro R
WHERE C.Codigo = R.ContaCredito
GO
ALTER TABLE ContabRoteiro ALTER COLUMN IdContaCredito int NOT NULL
GO

ALTER TABLE ContabRoteiro ADD IdContaDebito int NULL
GO
UPDATE ContabRoteiro SET IdContaDebito = C.IdConta
FROM ContabConta C, ContabRoteiro R
WHERE C.Codigo = R.ContaDebito
GO
ALTER TABLE ContabRoteiro ALTER COLUMN IdContaDebito int NOT NULL
GO

ALTER TABLE ContabRoteiro WITH CHECK ADD  CONSTRAINT ContabRoteiro_ContabConta_FK1 FOREIGN KEY(IdContaCredito)
REFERENCES ContabConta (IdConta)
GO
ALTER TABLE ContabRoteiro WITH CHECK ADD  CONSTRAINT ContabRoteiro_ContabConta_FK2 FOREIGN KEY(IdContaDebito)
REFERENCES ContabConta (IdConta)
GO