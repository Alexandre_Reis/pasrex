declare @data_versao char(10)
set @data_versao = '2013-12-03'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

insert into dbo.configuracao values (1031, 'SchemaBTC', null, '')
go

CREATE NONCLUSTERED INDEX IDX_IdCotista ON PosicaoCotistaHistorico 
(
	IdCotista ASC
)
GO
