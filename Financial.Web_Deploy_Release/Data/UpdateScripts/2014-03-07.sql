declare @data_versao char(10)
set @data_versao = '2014-03-07'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table carteira add CodigoCetip varchar(14) null
go
alter table carteira add CodigoBloomberg varchar(25) null
go

alter table posicaoswap add IdEstrategia int null
go
alter table posicaoswaphistorico add IdEstrategia int null
go
alter table posicaoswapabertura add IdEstrategia int null
go

update posicaoswap set IdEstrategia = o.IdEstrategia
from posicaoswap p, operacaoswap o
where p.idoperacao = o.idoperacao

update posicaoswaphistorico set IdEstrategia = o.IdEstrategia
from posicaoswaphistorico p, operacaoswap o
where p.idoperacao = o.idoperacao

update posicaoswapabertura set IdEstrategia = o.IdEstrategia
from posicaoswapabertura p, operacaoswap o
where p.idoperacao = o.idoperacao
