declare @data_versao char(10)
set @data_versao = '2016-03-31'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  n�o permite que seja executado novamente, avisa o usuario e 
*  n�o exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Aten��o este script ja foi executado anteriormente.',16,1)

	
	-- n�o mostra a execu��o do script
	set nocount on 
	
	-- n�o roda o script
	set noexec on 
	

end

begin transaction

	-- n�o permite que a transaction seja commitada em caso de erros
	set xact_abort on

	insert into VersaoSchema values(@data_versao)
	
	if( exists(select * from sysobjects where id = object_id('ST_RELATORIO')))
	begin	
		
		if not exists (select ds_objeto_conteudo from st_relatorio where cd_objeto = '1.17.0.0') 
		begin
			insert into st_relatorio ( CD_RELATORIO,CD_OBJETO,CD_OBJETO_TIPO,DS_OBJETO_TAMANHO,DS_OBJETO_FORMATO,DS_OBJETO_CONTEUDO) values ( '1.17.0.0', '1.17.0.0', 'imagem' ,'255', 'byte','F:\inetpub\wwwroot\portopar\relatorioscomerciais\CustomReport\PortoPar\View\Reports\img\indice_1_17_0_0.jpg') 
		end
		 
		if not exists (select ds_objeto_conteudo from st_relatorio where cd_objeto = '1.3.0.0') 
		begin
			insert into st_relatorio ( CD_RELATORIO,CD_OBJETO,CD_OBJETO_TIPO,DS_OBJETO_TAMANHO,DS_OBJETO_FORMATO,DS_OBJETO_CONTEUDO) values ( '1.3.0.0', '1.3.0.0', 'imagem' ,'255', 'byte','F:\inetpub\wwwroot\portopar\relatorioscomerciais\CustomReport\PortoPar\View\Reports\img\imgCapa.jpg') 
		end

		if not exists (select ds_objeto_conteudo from st_relatorio where cd_objeto = '1.4.0.0') 
		begin
			insert into st_relatorio ( CD_RELATORIO,CD_OBJETO,CD_OBJETO_TIPO,DS_OBJETO_TAMANHO,DS_OBJETO_FORMATO,DS_OBJETO_CONTEUDO) values ( '1.4.0.0', '1.4.0.0', 'imagem' ,'255', 'byte','F:\inetpub\wwwroot\portopar\relatorioscomerciais\CustomReport\PortoPar\View\Reports\img\imgAnbimaCapa.jpg') 
		end

		if not exists (select ds_objeto_conteudo from st_relatorio where cd_objeto = '1.8.0.0') 
		begin
			insert into st_relatorio ( CD_RELATORIO,CD_OBJETO,CD_OBJETO_TIPO,DS_OBJETO_TAMANHO,DS_OBJETO_FORMATO,DS_OBJETO_CONTEUDO) values ( '1.8.0.0', '1.8.0.0', 'imagem' ,'255', 'byte','F:\inetpub\wwwroot\portopar\relatorioscomerciais\CustomReport\PortoPar\View\Reports\img\imgIndice.jpg') 
		end

		if not exists (select ds_objeto_conteudo from st_relatorio where cd_objeto = '1.41.2.0') 
		begin
			insert into st_relatorio ( CD_RELATORIO,CD_OBJETO,CD_OBJETO_TIPO,DS_OBJETO_TAMANHO,DS_OBJETO_FORMATO,DS_OBJETO_CONTEUDO) values ( '1.41.2.0', '1.41.2.0', 'imagem' ,'255', 'byte','F:\inetpub\wwwroot\portopar\relatorioscomerciais\CustomReport\PortoPar\View\Reports\img\imgAnbimaRodape.jpg') 
		end

		if not exists (select ds_objeto_conteudo from st_relatorio where cd_objeto = '1.10.0.0') 
		begin
			insert into st_relatorio ( CD_RELATORIO,CD_OBJETO,CD_OBJETO_TIPO,DS_OBJETO_TAMANHO,DS_OBJETO_FORMATO,DS_OBJETO_CONTEUDO) values ( '1.10.0.0', '1.10.0.0', 'imagem' ,'255', 'byte','F:\inetpub\wwwroot\portopar\relatorioscomerciais\CustomReport\PortoPar\View\Reports\img\indicePosConsol_1_10_0_0.jpg') 
		end

		if not exists (select ds_objeto_conteudo from st_relatorio where cd_objeto = '1.11.0.0') 
		begin
			insert into st_relatorio ( CD_RELATORIO,CD_OBJETO,CD_OBJETO_TIPO,DS_OBJETO_TAMANHO,DS_OBJETO_FORMATO,DS_OBJETO_CONTEUDO) values ( '1.11.0.0', '1.11.0.0', 'imagem' ,'255', 'byte','F:\inetpub\wwwroot\portopar\relatorioscomerciais\CustomReport\PortoPar\View\Reports\img\indicePosDetal_1_11_0_0.jpg') 
		end

		if not exists (select ds_objeto_conteudo from st_relatorio where cd_objeto = '1.12.0.0') 
		begin
			insert into st_relatorio ( CD_RELATORIO,CD_OBJETO,CD_OBJETO_TIPO,DS_OBJETO_TAMANHO,DS_OBJETO_FORMATO,DS_OBJETO_CONTEUDO) values ( '1.12.0.0', '1.12.0.0', 'imagem' ,'255', 'byte','F:\inetpub\wwwroot\portopar\relatorioscomerciais\CustomReport\PortoPar\View\Reports\img\indiceHistorico_1_12_0_0.jpg') 
		end

		if not exists (select ds_objeto_conteudo from st_relatorio where cd_objeto = '1.13.0.0') 
		begin
			insert into st_relatorio ( CD_RELATORIO,CD_OBJETO,CD_OBJETO_TIPO,DS_OBJETO_TAMANHO,DS_OBJETO_FORMATO,DS_OBJETO_CONTEUDO) values ( '1.13.0.0', '1.13.0.0', 'imagem' ,'255', 'byte','F:\inetpub\wwwroot\portopar\relatorioscomerciais\CustomReport\PortoPar\View\Reports\img\indiceRentab_1_13_0_0.jpg') 
		end  

		if not exists (select ds_objeto_conteudo from st_relatorio where cd_objeto = '2.1.0.0') 
		begin
			insert into st_relatorio ( CD_RELATORIO,CD_OBJETO,CD_OBJETO_TIPO,DS_OBJETO_TAMANHO,DS_OBJETO_FORMATO,DS_OBJETO_CONTEUDO) values ( '2.1.0.0', '2.1.0.0', 'imagem' ,'255', 'byte','F:\inetpub\wwwroot\portopar\relatorioscomerciais\CustomReport\PortoPar\View\Reports\img\imgLogoPSI_Vert.jpg') 
		end   

		if not exists (select ds_objeto_conteudo from st_relatorio where cd_objeto = '2.11.0.0') 
		begin
			insert into st_relatorio ( CD_RELATORIO,CD_OBJETO,CD_OBJETO_TIPO,DS_OBJETO_TAMANHO,DS_OBJETO_FORMATO,DS_OBJETO_CONTEUDO) values ( '2.11.0.0', '2.11.0.0', 'imagem' ,'255', 'byte','F:\inetpub\wwwroot\portopar\relatorioscomerciais\CustomReport\PortoPar\View\Reports\img\imgAnbimaRodape.jpg') 
		end   

		if not exists (select ds_objeto_conteudo from st_relatorio where cd_objeto = '3.2.0.0') 
		begin
			insert into st_relatorio ( CD_RELATORIO,CD_OBJETO,CD_OBJETO_TIPO,DS_OBJETO_TAMANHO,DS_OBJETO_FORMATO,DS_OBJETO_CONTEUDO) values ( '3.2.0.0', '3.2.0.0', 'imagem' ,'255', 'byte','F:\inetpub\wwwroot\portopar\relatorioscomerciais\CustomReport\PortoPar\View\Reports\img\imgLogoPSI_Hor.jpg') 
		end   

		if not exists (select ds_objeto_conteudo from st_relatorio where cd_objeto = '3.17.0.0') 
		begin
			insert into st_relatorio ( CD_RELATORIO,CD_OBJETO,CD_OBJETO_TIPO,DS_OBJETO_TAMANHO,DS_OBJETO_FORMATO,DS_OBJETO_CONTEUDO) values ( '3.17.0.0', '3.17.0.0', 'imagem' ,'255', 'byte','F:\inetpub\wwwroot\portopar\relatorioscomerciais\CustomReport\PortoPar\View\Reports\img\imgAnbimaRodape.jpg') 
		end    

		if not exists (select ds_objeto_conteudo from st_relatorio where cd_objeto = '4.2.2.0') 
		begin
			insert into st_relatorio ( CD_RELATORIO,CD_OBJETO,CD_OBJETO_TIPO,DS_OBJETO_TAMANHO,DS_OBJETO_FORMATO,DS_OBJETO_CONTEUDO) values ( '4.2.2.0', '4.2.2.0', 'imagem' ,'255', 'byte','F:\inetpub\wwwroot\portopar\relatorioscomerciais\CustomReport\PortoPar\View\Reports\img\imgLogoPSI_Ver.jpg') 
		end    

		if not exists (select ds_objeto_conteudo from st_relatorio where cd_objeto = '4.9.0.0') 
		begin
			insert into st_relatorio ( CD_RELATORIO,CD_OBJETO,CD_OBJETO_TIPO,DS_OBJETO_TAMANHO,DS_OBJETO_FORMATO,DS_OBJETO_CONTEUDO) values ( '4.9.0.0', '4.9.0.0', 'imagem' ,'255', 'byte','F:\inetpub\wwwroot\portopar\relatorioscomerciais\CustomReport\PortoPar\View\Reports\img\imgAnbimaRodape.jpg') 
		end 
		
	end	

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off