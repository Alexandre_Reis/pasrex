declare @data_versao char(10)
set @data_versao = '2016-03-22'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Atenção este script ja foi executado anteriormente.',16,1)

	
	-- não mostra a execução do script
	set nocount on 
	
	-- não roda o script
	set noexec on 
	

end

/****** Alteração do campo CdAtivoBolsa da Rentabilidade de varchar(20) para varchar(25)  ******/

begin transaction 

	-- não permite que a transaction seja commitada em caso de erros
	set xact_abort on

	insert into VersaoSchema values(@data_versao)

	if exists(select 1 from syscolumns where id = object_id('Rentabilidade') and name = 'CdAtivoBolsa')
	begin
		ALTER TABLE Rentabilidade ALTER COLUMN CdAtivoBolsa VARCHAR(25) NULL
	END

commit transaction
go
