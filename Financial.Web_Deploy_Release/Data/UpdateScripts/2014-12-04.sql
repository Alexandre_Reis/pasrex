declare @data_versao char(10)
set @data_versao = '2014-12-04'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table liquidacaorendafixa add Status tinyint null
go
ALTER TABLE liquidacaorendafixa ADD  CONSTRAINT DF_LiquidacaoRendaFixa_Status  DEFAULT ((1)) FOR Status
go
update liquidacaorendafixa set Status = 1
go