declare @data_versao char(10)
set @data_versao = '2013-10-07'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table operacaobolsa add ValorISS decimal(16, 2) null
go
update operacaobolsa set ValorISS = 0
go
alter table operacaobolsa alter column ValorISS decimal(16, 2) not null
go

alter table ordembolsa add ValorISS decimal(16, 2) null
go
update ordembolsa set ValorISS = 0
go
alter table ordembolsa alter column ValorISS decimal(16, 2) not null
go

ALTER TABLE OperacaoBolsa ADD  CONSTRAINT DF_OperacaoBolsa_ValorISS  DEFAULT ((0)) FOR ValorISS
GO
ALTER TABLE OrdemBolsa ADD  CONSTRAINT DF_OrdemBolsa_ValorISS  DEFAULT ((0)) FOR ValorISS
GO
