declare @data_versao char(10)
set @data_versao = '2014-07-01'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


ALTER TABLE IndicadoresCarteira ALTER COLUMN RetornoDia decimal(20,12) NULL
ALTER TABLE IndicadoresCarteira ALTER COLUMN RetornoMes decimal(20,12) NULL
ALTER TABLE IndicadoresCarteira ALTER COLUMN RetornoAno decimal(20,12) NULL
ALTER TABLE IndicadoresCarteira ALTER COLUMN Volatilidade decimal(20,12) NULL