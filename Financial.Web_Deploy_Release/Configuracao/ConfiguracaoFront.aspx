﻿<%@ page language="C#" autoeventwireup="true" inherits="ConfiguracaoFront, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
</head>

<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />

        <div class="divPanel divPanelNew">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Configuração de FrontOffice" />
                            </div>
                            <div id="mainContentSpace">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <br />
                                        <br />
                                        <table border="0" align="center">
                                            <tr>
                                                <td>
                                                    <dxrp:ASPxRoundPanel ID="pnlFront" runat="server" HeaderText=" " HeaderStyle-Font-Size="11px" Width="459px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <br />
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label11" runat="server" CssClass="labelNormal" Text="Texto Boleto Fundo:" />
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="textFundo" runat="server" TextMode="MultiLine" Rows="4" CssClass="textLongo5"/>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Texto Boleto Cotista:" />
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="textCotista" runat="server" TextMode="MultiLine" Rows="4" CssClass="textLongo5"/>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Texto Boleto Renda Fixa:" />
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="textRendaFixa" runat="server" TextMode="MultiLine" Rows="6" CssClass="textLongo5"/>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label13" runat="server" CssClass="labelNormal" Text="Email Fundo:" />
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="textEmailFundo" runat="server" CssClass="textLongo5"/>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label14" runat="server" CssClass="labelNormal" Text="Email Cotista:" />
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="textEmailCotista" runat="server" CssClass="textLongo5"/>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label15" runat="server" CssClass="labelNormal" Text="Email Renda Fixa:" />
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:TextBox ID="textEmailRendaFixa" runat="server" CssClass="textLongo5"/>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Horário Limite Fundo:" />
                                                                        </td>
                                                                        <td width="30px">
                                                                            <asp:Label ID="label4" runat="server" Text="Hora" />
                                                                            <dxe:ASPxSpinEdit ID="textHoraFundo" runat="server" CssClass="textCurto_5" 
                                                                                    ClientInstanceName="textHoraFundo" MaxLength="2" NumberType="Integer">
                                                                            </dxe:ASPxSpinEdit>
                                                                        </td>
                                                                        
                                                                        <td>
                                                                            <asp:Label ID="label6" runat="server" Text="Minuto"/>
                                                                            <dxe:ASPxSpinEdit ID="textMinutoFundo" runat="server" CssClass="textCurto_5" 
                                                                                    ClientInstanceName="textMinutoFundo" MaxLength="2" NumberType="Integer">
                                                                            </dxe:ASPxSpinEdit>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label5" runat="server" CssClass="labelNormal" Text="Horário Limite Cotista:" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label7" runat="server" Text="Hora" />
                                                                            <dxe:ASPxSpinEdit ID="textHoraCotista" runat="server" CssClass="textCurto_5" 
                                                                                    ClientInstanceName="textHoraCotista" MaxLength="2" NumberType="Integer">
                                                                            </dxe:ASPxSpinEdit>
                                                                        </td>
                                                                        
                                                                        <td>
                                                                            <asp:Label ID="label8" runat="server" Text="Minuto"/>
                                                                            <dxe:ASPxSpinEdit ID="textMinutoCotista" runat="server" CssClass="textCurto_5" 
                                                                                    ClientInstanceName="textMinutoCotista" MaxLength="2" NumberType="Integer">
                                                                            </dxe:ASPxSpinEdit>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label9" runat="server" CssClass="labelNormal" Text="Horário Limite Renda Fixa:" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label10" runat="server" Text="Hora" />
                                                                            <dxe:ASPxSpinEdit ID="textHoraRendaFixa" runat="server" CssClass="textCurto_5" 
                                                                                    ClientInstanceName="textHoraRendaFixa" MaxLength="2" NumberType="Integer">
                                                                            </dxe:ASPxSpinEdit>
                                                                        </td>
                                                                        
                                                                        <td>
                                                                            <asp:Label ID="label12" runat="server" Text="Minuto"/>
                                                                            <dxe:ASPxSpinEdit ID="textMinutoRendaFixa" runat="server" CssClass="textCurto_5" 
                                                                                    ClientInstanceName="textMinutoRendaFixa" MaxLength="2" NumberType="Integer">
                                                                            </dxe:ASPxSpinEdit>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                </table>
                                                                <div class="linkButton linkButtonTbar" style="padding-left: 50pt">
                                                                    <div class="linkButtonWrapper">
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                            OnClick="btnSalvar_Click">
                                                                            <asp:Literal ID="Literal1" runat="server" Text="Salvar" /><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                </td>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>