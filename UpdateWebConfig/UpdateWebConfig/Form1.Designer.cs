﻿namespace UpdateWebConfig
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dialogSetPathWebConfig = new System.Windows.Forms.OpenFileDialog();
            this.buttonSetPathWebConfig = new System.Windows.Forms.Button();
            this.pathWebConfig = new System.Windows.Forms.TextBox();
            this.labelPathWebConfig = new System.Windows.Forms.Label();
            this.buttonUpdateWebConfig = new System.Windows.Forms.Button();
            this.passwordFinancial = new System.Windows.Forms.TextBox();
            this.labelPasswordFinancial = new System.Windows.Forms.Label();
            this.labelPasswordSinacor = new System.Windows.Forms.Label();
            this.passwordSinacor = new System.Windows.Forms.TextBox();
            this.labelIntro = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dialogSetPathWebConfig
            // 
            this.dialogSetPathWebConfig.FileName = "openFileDialog1";
            // 
            // buttonSetPathWebConfig
            // 
            this.buttonSetPathWebConfig.Location = new System.Drawing.Point(318, 265);
            this.buttonSetPathWebConfig.Name = "buttonSetPathWebConfig";
            this.buttonSetPathWebConfig.Size = new System.Drawing.Size(30, 23);
            this.buttonSetPathWebConfig.TabIndex = 5;
            this.buttonSetPathWebConfig.Text = "...";
            this.buttonSetPathWebConfig.UseVisualStyleBackColor = true;
            this.buttonSetPathWebConfig.Click += new System.EventHandler(this.buttonSetPathWebConfig_Click);
            // 
            // pathWebConfig
            // 
            this.pathWebConfig.Location = new System.Drawing.Point(15, 267);
            this.pathWebConfig.Name = "pathWebConfig";
            this.pathWebConfig.Size = new System.Drawing.Size(299, 20);
            this.pathWebConfig.TabIndex = 3;
            // 
            // labelPathWebConfig
            // 
            this.labelPathWebConfig.AutoSize = true;
            this.labelPathWebConfig.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPathWebConfig.Location = new System.Drawing.Point(12, 251);
            this.labelPathWebConfig.Name = "labelPathWebConfig";
            this.labelPathWebConfig.Size = new System.Drawing.Size(170, 13);
            this.labelPathWebConfig.TabIndex = 3;
            this.labelPathWebConfig.Text = "Caminho Web.Config Financial:";
            // 
            // buttonUpdateWebConfig
            // 
            this.buttonUpdateWebConfig.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdateWebConfig.Location = new System.Drawing.Point(228, 324);
            this.buttonUpdateWebConfig.Name = "buttonUpdateWebConfig";
            this.buttonUpdateWebConfig.Size = new System.Drawing.Size(120, 38);
            this.buttonUpdateWebConfig.TabIndex = 6;
            this.buttonUpdateWebConfig.Text = "Atualizar Config";
            this.buttonUpdateWebConfig.UseVisualStyleBackColor = true;
            this.buttonUpdateWebConfig.Click += new System.EventHandler(this.buttonUpdateWebConfig_Click);
            // 
            // passwordFinancial
            // 
            this.passwordFinancial.Location = new System.Drawing.Point(15, 122);
            this.passwordFinancial.Name = "passwordFinancial";
            this.passwordFinancial.Size = new System.Drawing.Size(205, 20);
            this.passwordFinancial.TabIndex = 1;
            this.passwordFinancial.UseSystemPasswordChar = true;
            // 
            // labelPasswordFinancial
            // 
            this.labelPasswordFinancial.AutoSize = true;
            this.labelPasswordFinancial.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPasswordFinancial.Location = new System.Drawing.Point(12, 106);
            this.labelPasswordFinancial.Name = "labelPasswordFinancial";
            this.labelPasswordFinancial.Size = new System.Drawing.Size(91, 13);
            this.labelPasswordFinancial.TabIndex = 8;
            this.labelPasswordFinancial.Text = "Senha Financial:";
            this.labelPasswordFinancial.Click += new System.EventHandler(this.label1_Click);
            // 
            // labelPasswordSinacor
            // 
            this.labelPasswordSinacor.AutoSize = true;
            this.labelPasswordSinacor.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPasswordSinacor.Location = new System.Drawing.Point(12, 168);
            this.labelPasswordSinacor.Name = "labelPasswordSinacor";
            this.labelPasswordSinacor.Size = new System.Drawing.Size(83, 13);
            this.labelPasswordSinacor.TabIndex = 10;
            this.labelPasswordSinacor.Text = "Senha Sinacor:";
            this.labelPasswordSinacor.Click += new System.EventHandler(this.label3_Click);
            // 
            // passwordSinacor
            // 
            this.passwordSinacor.Location = new System.Drawing.Point(15, 184);
            this.passwordSinacor.Name = "passwordSinacor";
            this.passwordSinacor.Size = new System.Drawing.Size(205, 20);
            this.passwordSinacor.TabIndex = 2;
            this.passwordSinacor.UseSystemPasswordChar = true;
            this.passwordSinacor.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // labelIntro
            // 
            this.labelIntro.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIntro.Location = new System.Drawing.Point(12, 26);
            this.labelIntro.Name = "labelIntro";
            this.labelIntro.Size = new System.Drawing.Size(336, 59);
            this.labelIntro.TabIndex = 11;
            this.labelIntro.Text = "Digite nos campos abaixo as senhas que deverão ser criptografadas e armazenadas n" +
                "o arquivo de configuração do Financial:";
            this.labelIntro.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(248, 88);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 155);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 374);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelIntro);
            this.Controls.Add(this.labelPasswordSinacor);
            this.Controls.Add(this.passwordSinacor);
            this.Controls.Add(this.labelPasswordFinancial);
            this.Controls.Add(this.passwordFinancial);
            this.Controls.Add(this.buttonUpdateWebConfig);
            this.Controls.Add(this.buttonSetPathWebConfig);
            this.Controls.Add(this.pathWebConfig);
            this.Controls.Add(this.labelPathWebConfig);
            this.Name = "Form1";
            this.Text = "Plataforma Financial";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.OpenFileDialog dialogSetPathWebConfig;
        public System.Windows.Forms.Button buttonSetPathWebConfig;
        public System.Windows.Forms.TextBox pathWebConfig;
        public System.Windows.Forms.Label labelPathWebConfig;
        private System.Windows.Forms.Button buttonUpdateWebConfig;
        private System.Windows.Forms.TextBox passwordFinancial;
        public System.Windows.Forms.Label labelPasswordFinancial;
        public System.Windows.Forms.Label labelPasswordSinacor;
        private System.Windows.Forms.TextBox passwordSinacor;
        public System.Windows.Forms.Label labelIntro;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

