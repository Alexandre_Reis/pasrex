﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Security.Cryptography;

namespace UpdateWebConfig
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonSetPathWebConfig_Click(object sender, EventArgs e)
        {
            DialogResult result = dialogSetPathWebConfig.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK)
            {
                string webConfigFileName = dialogSetPathWebConfig.FileName;
                Form form = this.FindForm();
                TextBox textBoxToUpdate = (TextBox)form.Controls.Find("pathWebConfig", true)[0];
                textBoxToUpdate.Text = webConfigFileName;
            }
        }

        private static XmlDocument loadConfigDocument(string webConfigFileName)
        {
            XmlDocument doc = null;
            try
            {
                doc = new XmlDocument();
                doc.Load(webConfigFileName);
                return doc;
            }
            catch (System.IO.FileNotFoundException e)
            {
                throw new Exception("Arquivo de configuração não encontrado.", e);
            }
        }

        private void EncryptConnection(XmlNode node, string connectionName, string password)
        {
            XmlElement elem = (XmlElement)node.SelectSingleNode(string.Format("//add[@name='{0}']", connectionName));

            if (elem != null)
            {
                string connectionString = elem.GetAttribute("connectionString");
                string encryptedConnectionString = EncryptConnectionString(connectionString, password);
                elem.SetAttribute("connectionString", encryptedConnectionString);
            }
            else
            {
                throw new Exception("Não foi possivel encontrar a conexao " + connectionName + ".");
                // key was not found so create the 'add' element 
                // and set it's key/value attributes 
                /*elem = doc.CreateElement("add");
                elem.SetAttribute("key", key);
                elem.SetAttribute("value", value);
                node.AppendChild(elem);*/
            }
        }

        private string EncryptConnectionString(string decryptedConnectionString, string password)
        {
            const string SHARED_SECRET = "financial@$";
            string[] connectionStringTokens = decryptedConnectionString.Split(';');
            List<string> encryptedConnectionStringList = new List<string>();

            foreach (string connectionStringToken in connectionStringTokens)
            {
                string token = connectionStringToken.Trim();
                if (token.ToLower().StartsWith("password"))
                {
                    string encryptedPassword = Crypto.EncryptStringAES(password, SHARED_SECRET);
                    token = "Password=" + encryptedPassword;
                }

                encryptedConnectionStringList.Add(token);
            }

            string encryptedConnectionString = String.Join(";", encryptedConnectionStringList);
            return encryptedConnectionString;
        }

        private void buttonUpdateWebConfig_Click(object sender, EventArgs e)
        {
            Form form = this.FindForm();

            string pathWebConfig = ((TextBox)form.Controls.Find("pathWebConfig", true)[0]).Text;
            string passwordFinancial = ((TextBox)form.Controls.Find("passwordFinancial", true)[0]).Text;
            string passwordSinacor = ((TextBox)form.Controls.Find("passwordSinacor", true)[0]).Text;


            if (String.IsNullOrEmpty(pathWebConfig))
            {
                MessageBox.Show("Selecione o arquivo Web.Config no diretório de instalação do Financial", "Aviso");
                return;
            }
            else if (!pathWebConfig.ToLower().Contains("web.config"))
            {
                MessageBox.Show("Arquivo Web.Config inválido", "Aviso");
                return;
            }
            else if (String.IsNullOrEmpty(passwordFinancial) && String.IsNullOrEmpty(passwordSinacor))
            {
                MessageBox.Show("É necessário digitar pelo menos uma das senhas para continuar", "Aviso");
                return;
            }


            XmlDocument doc = loadConfigDocument(pathWebConfig);
            XmlNode node = doc.SelectSingleNode("//connectionStrings");

            if (node == null)
            {
                throw new InvalidOperationException("A seção \"ConnectionStrings\" do Web.Config não foi encontrada.");
            }

            try
            {
                if (!string.IsNullOrEmpty(passwordFinancial))
                {
                    EncryptConnection(node, "Financial", passwordFinancial);
                }

                if (!string.IsNullOrEmpty(passwordSinacor))
                {
                    EncryptConnection(node, "Sinacor", passwordSinacor);
                }
            }
            catch (Exception connectionException)
            {
                MessageBox.Show(connectionException.Message, "Erro");
                return;
            }
            doc.Save(pathWebConfig);

            MessageBox.Show("Configuração atualizada com sucesso!", "Sucesso");
            form.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

    }
}

public class Crypto
{
    private static byte[] _salt = Encoding.ASCII.GetBytes("o6806642kbM7c5");

    /// <summary>
    /// Encrypt the given string using AES.  The string can be decrypted using 
    /// DecryptStringAES().  The sharedSecret parameters must match.
    /// </summary>
    /// <param name="plainText">The text to encrypt.</param>
    /// <param name="sharedSecret">A password used to generate a key for encryption.</param>
    public static string EncryptStringAES(string plainText, string sharedSecret)
    {
        if (string.IsNullOrEmpty(plainText))
            throw new ArgumentNullException("plainText");
        if (string.IsNullOrEmpty(sharedSecret))
            throw new ArgumentNullException("sharedSecret");

        string outStr = null;                       // Encrypted string to return
        RijndaelManaged aesAlg = null;              // RijndaelManaged object used to encrypt the data.

        try
        {
            // generate the key from the shared secret and the salt
            Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, _salt);

            // Create a RijndaelManaged object
            aesAlg = new RijndaelManaged();
            aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

            // Create a decryptor to perform the stream transform.
            ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

            // Create the streams used for encryption.
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                // prepend the IV
                msEncrypt.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(int));
                msEncrypt.Write(aesAlg.IV, 0, aesAlg.IV.Length);
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        //Write all data to the stream.
                        swEncrypt.Write(plainText);
                    }
                }
                outStr = Convert.ToBase64String(msEncrypt.ToArray());
            }
        }
        finally
        {
            // Clear the RijndaelManaged object.
            if (aesAlg != null)
                aesAlg.Clear();
        }

        // Return the encrypted bytes from the memory stream.
        return outStr;
    }

    /// <summary>
    /// Decrypt the given string.  Assumes the string was encrypted using 
    /// EncryptStringAES(), using an identical sharedSecret.
    /// </summary>
    /// <param name="cipherText">The text to decrypt.</param>
    /// <param name="sharedSecret">A password used to generate a key for decryption.</param>
    public static string DecryptStringAES(string cipherText, string sharedSecret)
    {
        if (string.IsNullOrEmpty(cipherText))
            throw new ArgumentNullException("cipherText");
        if (string.IsNullOrEmpty(sharedSecret))
            throw new ArgumentNullException("sharedSecret");

        // Declare the RijndaelManaged object
        // used to decrypt the data.
        RijndaelManaged aesAlg = null;

        // Declare the string used to hold
        // the decrypted text.
        string plaintext = null;

        try
        {
            // generate the key from the shared secret and the salt
            Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, _salt);

            // Create the streams used for decryption.                
            byte[] bytes = Convert.FromBase64String(cipherText);
            using (MemoryStream msDecrypt = new MemoryStream(bytes))
            {
                // Create a RijndaelManaged object
                // with the specified key and IV.
                aesAlg = new RijndaelManaged();
                aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                // Get the initialization vector from the encrypted stream
                aesAlg.IV = ReadByteArray(msDecrypt);
                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))

                        // Read the decrypted bytes from the decrypting stream
                        // and place them in a string.
                        plaintext = srDecrypt.ReadToEnd();
                }
            }
        }
        finally
        {
            // Clear the RijndaelManaged object.
            if (aesAlg != null)
                aesAlg.Clear();
        }

        return plaintext;
    }

    private static byte[] ReadByteArray(Stream s)
    {
        byte[] rawLength = new byte[sizeof(int)];
        if (s.Read(rawLength, 0, rawLength.Length) != rawLength.Length)
        {
            throw new SystemException("Stream did not contain properly formatted byte array");
        }

        byte[] buffer = new byte[BitConverter.ToInt32(rawLength, 0)];
        if (s.Read(buffer, 0, buffer.Length) != buffer.Length)
        {
            throw new SystemException("Did not read byte array properly");
        }

        return buffer;
    }
}