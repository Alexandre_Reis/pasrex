﻿// The following code was generated by Microsoft Visual Studio 2005.
// The test owner should check each test for validity.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using System.Collections.Generic;
using Financial.BMF;
using Test.Financial.BMF.Properties;
namespace Test.Financial.BMF
{
    /// <summary>
    ///This is a test class for Financial.BMF.AtivoBMF and is intended
    ///to contain all Financial.BMF.AtivoBMF Unit Tests
    ///</summary>
    [TestClass()]
    public class AtivoBMFTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        }
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ConverteTaxaPU (string, decimal, int)
        ///</summary>
        [TestMethod()]
        public void ConverteTaxaPUTest()
        {
            AtivoBMF target = new AtivoBMF();

            string cdAtivoBMF = "DI1";

            decimal taxa = 10.5M;

            int prazo = 30;

            decimal expected = 98818.40M;
            decimal actual;

            actual = target.ConverteTaxaPU(cdAtivoBMF, taxa, prazo);

            Assert.AreEqual(expected, actual, "Financial.BMF.AtivoBMF.ConverteTaxaPU did not return the expected value.");

            cdAtivoBMF = "DIL";

            taxa = 10.5M;

            prazo = 70;

            expected = 98077.29M;

            actual = target.ConverteTaxaPU(cdAtivoBMF, taxa, prazo);

            Assert.AreEqual(expected, actual, "Financial.BMF.AtivoBMF.ConverteTaxaPU did not return the expected value.");

        }


        /// <summary>
        ///A test for IsAtivoBradie (string)
        ///</summary>
        [TestMethod()]
        public void IsAtivoBradieTest()
        {
            //AtivoBMF target = new AtivoBMF();

            string cdAtivoBMF = "B09";

            bool expected = true;
            bool actual;

            actual = AtivoBMF.IsAtivoBond(cdAtivoBMF);

            Assert.AreEqual(expected, actual, "Financial.BMF.AtivoBMF.IsAtivoBradie did not return the expected value.");
        }

        /// <summary>
        ///A test for CarregaAtivoBdPregao (DateTime)
        ///</summary>
        [TestMethod()]
        public void CarregaAtivoBdPregaoTest()
        {
            AtivoBMF target = new AtivoBMF();
            
            string path = Settings.Default.DiretorioDownloads;
            DateTime data = new DateTime(2007, 03, 21);

            target.CarregaAtivoBdPregao(data, path);

            //Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for RetornaPrazoDiasUteis (string, string, DateTime)
        ///</summary>
        [TestMethod()]
        public void RetornaPrazoDiasUteisTest()
        {
            AtivoBMF target = new AtivoBMF();

            string cdAtivoBMF = "DI1";

            string serie = "F08";

            DateTime data = new DateTime(2007, 1, 24);

            int expected = 30;
            int actual;

            actual = target.RetornaPrazoDiasUteis(cdAtivoBMF, serie, data);

            Assert.AreEqual(expected, actual, "Financial.BMF.AtivoBMF.RetornaPrazoDiasUteis did not return the expected value.");
        }

        /// <summary>
        ///A test for RetornaNumeroVencimentos (string, string, int, DateTime)
        ///</summary>
        [TestMethod()]
        public void RetornaNumeroVencimentosTest()
        {
            AtivoBMF target = new AtivoBMF();

            string cdAtivoBMF = "DI1";

            string serie = "K07";

            int tipoMercado = 2;

            DateTime dataVencimento = new DateTime(2007, 02, 09);

            int expected = 3;
            int actual;

            actual = target.RetornaNumeroVencimentos(cdAtivoBMF, serie, tipoMercado, dataVencimento);

            Assert.AreEqual(expected, actual, "Financial.BMF.AtivoBMF.RetornaNumeroVencimentos did not return the expected value" +
                    ".");            
        }
    }


}
