﻿// The following code was generated by Microsoft Visual Studio 2005.
// The test owner should check each test for validity.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using System.Collections.Generic;
using Financial.BMF;
using EntitySpaces.Interfaces;
namespace Test.Financial.BMF
{
    /// <summary>
    ///This is a test class for Financial.BMF.PosicaoBMF and is intended
    ///to contain all Financial.BMF.PosicaoBMF Unit Tests
    ///</summary>
    [TestClass()]
    public class PosicaoBMFTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        }

        //
        //Use ClassCleanup to run code after all tests in a class have run
        //
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for CalculaAjustePosicao (int, DateTime)
        ///</summary>
        [TestMethod()]
        public void CalculaAjustePosicaoTest()
        {
            PosicaoBMF target = new PosicaoBMF();

            int idCliente = 1;

            DateTime data = new DateTime(2006, 09, 29);

            target.CalculaAjustePosicao(idCliente, data);

            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }


        /// <summary>
        ///A test for CalculaTaxaPermanencia (int, DateTime)
        ///</summary>
        [TestMethod()]
        public void CalculaTaxaPermanenciaTest()
        {
            PosicaoBMF target = new PosicaoBMF();

            int idCliente = 2;

            DateTime data = new DateTime(2006, 04, 27);

            target.CalculaTaxaPermanencia(idCliente, data);

            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        //
        // Cria uma posição zerada
        ///        
        private void AtualizaValoresInitialize()
        {
            // insere uma posicao zerada (quantidade = 0)            
            PosicaoBMF posicaoBMFInsereDados = new PosicaoBMF();
            posicaoBMFInsereDados.AddNew();
            posicaoBMFInsereDados.IdCliente = 4;
            posicaoBMFInsereDados.IdAgente = 1;
            posicaoBMFInsereDados.CdAtivoBMF = "AL2";
            posicaoBMFInsereDados.Serie = "G07";
            posicaoBMFInsereDados.TipoMercado = 2;
            posicaoBMFInsereDados.PUCusto = 0;
            posicaoBMFInsereDados.PUCustoLiquido = 1;
            posicaoBMFInsereDados.Quantidade = 0;// Posição zerada            
            posicaoBMFInsereDados.QuantidadeInicial = 0;
            posicaoBMFInsereDados.ValorCustoLiquido = 0;
            posicaoBMFInsereDados.Save();
        }

        /// <summary>
        ///  Entradas: idCliente = 4
        ///  Data da cotação: 01/01/2003
        ///  Usa PosicaoBMF id = 5
        ///  Saida:   idPosicao = 5
        ///           valorMercado = 100
        ///           valorCustoLiquido = 100
        ///           resultadoRealizar = 0
        /// 
        ///</summary>
        [TestMethod()]
        public void AtualizaValoresTest()
        {
            using (esTransactionScope scope = new esTransactionScope())
            {
                // Cria uma posicao zerada (quantidade = 0)
                AtualizaValoresInitialize();
                PosicaoBMF target = new PosicaoBMF();
                //
                int idCliente = 4;
                DateTime data = new DateTime(2006, 9, 28);
                //

                // Conferencias
                PosicaoBMF posicaoBMF = new PosicaoBMF();
                decimal puMercado = 1025M;
                decimal valorMercado = 100M * 1025M * 30M;
                decimal valorCustoLiquido = 100M * 1M * 30M; ;
                decimal resultadoRealizar = 100M * (1025M - 1M) * 30M;

                //
                // seleciona a key que foi criada - maxID
                PosicaoBMF posicaoBMFAux = new PosicaoBMF();
                posicaoBMFAux.Query.Select(posicaoBMFAux.Query.IdPosicao.Max());
                posicaoBMFAux.Query.Load();
                int idPosicao = posicaoBMFAux.IdPosicao.Value;
                //
                target.AtualizaValores(idCliente, data);
                //
                posicaoBMF.LoadByPrimaryKey(5);
                Assert.AreEqual(puMercado, posicaoBMF.PUMercado, "posicaoBMF.PUMercado deveria ser: " + puMercado);
                Assert.AreEqual(valorMercado, posicaoBMF.ValorMercado, "posicaoBMF.ValorMercado deveria ser: " + valorMercado);
                Assert.AreEqual(valorCustoLiquido, posicaoBMF.ValorCustoLiquido, "posicaoBMF.ValorCustoLiquido deveria ser: " + valorCustoLiquido);
                Assert.AreEqual(resultadoRealizar, posicaoBMF.ResultadoRealizar, "posicaoBMF.ResultadoRealizar deveria ser: " + resultadoRealizar);
                // verifica se excluiu posicao
                posicaoBMFAux.QueryReset();
                posicaoBMFAux.LoadByPrimaryKey(idPosicao);
                Assert.IsFalse(posicaoBMFAux.Query.Load());
            }
        }

        /// <summary>
        /// Entrada: idCliente = 2
        /// Saida: posicoes 6,7 do cliente 2 com quantidadeAbertura = quantidade
        ///</summary>
        [TestMethod()]
        public void AtualizaQuantidadeAberturaTest()
        {
            using (esTransactionScope scope = new esTransactionScope())
            {
                PosicaoBMF target = new PosicaoBMF();
                int idCliente = 2;
                //
                //target.AtualizaQuantidadeAbertura(idCliente);
                //
                PosicaoBMF posicaoBMF = new PosicaoBMF();
                posicaoBMF.LoadByPrimaryKey(6);
                int quantidadeEsperada = 300;
                Assert.AreEqual(quantidadeEsperada, posicaoBMF.Quantidade, "A quantidade deveria ser: " + quantidadeEsperada);
                //
                posicaoBMF.QueryReset();
                posicaoBMF.LoadByPrimaryKey(7);
                quantidadeEsperada = 500;
                Assert.AreEqual(quantidadeEsperada, posicaoBMF.Quantidade, "A quantidade deveria ser: " + quantidadeEsperada);
            }
        }

        /// <summary>
        ///A test for ProcessaVencimento (int, DateTime)
        ///</summary>
        [TestMethod()]
        public void ProcessaVencimentoTest()
        {
            PosicaoBMF target = new PosicaoBMF();

            int idCliente = 7;

            DateTime data = new DateTime(2008, 01, 05); // TODO: Initialize to an appropriate value

            target.ProcessaVencimento(idCliente, data);

            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GeraExercicioAutomatico (int, DateTime)
        ///</summary>
        [TestMethod()]
        public void GeraExercicioAutomaticoTest()
        {
            PosicaoBMF target = new PosicaoBMF();

            int idCliente = 40;

            DateTime data = new DateTime(2007, 3, 21);

            target.GeraExercicioAutomatico(idCliente, data);

            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
