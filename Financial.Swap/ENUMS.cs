﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.Util;
using System.Reflection;

namespace Financial.Swap.Enums
{
    public enum TipoRegistroSwap
    {
        BMF = 1,
        CETIP = 2
    }

    public enum PontaSwap
    {
        [StringValue("Parte")]
        Parte = 1,

        [StringValue("Contra Parte")]
        ContraParte = 2,

        [StringValue("Ambas")]
        Ambas = 3,
    }

    public static class PontaSwapDescricao
    {
        public static string RetornaDescricao(int ponta)
        {
            return Enum.GetName(typeof(PontaSwap), ponta);
        }

        public static string RetornaStringValue(int ponta)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(PontaSwap), RetornaDescricao(ponta)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoEventoSwap
    {
        [StringValue("Reset")]
        Reset = 1,

        [StringValue("Ajuste")]
        Ajuste = 2
    }

    public static class TipoEventoSwapDescricao
    {
        public static string RetornaDescricao(int evento)
        {
            return Enum.GetName(typeof(TipoEventoSwap), evento);
        }

        public static string RetornaStringValue(int evento)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoEventoSwap), RetornaDescricao(evento)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoPontaSwap
    {
        [StringValue("Pré Fixado")]
        PreFixado = 1,

        [StringValue("Indexado")]
        Indexado = 2,

        [StringValue("Indexado-D0")]
        IndexadoD0 = 3,

        [StringValue("Ativo Bolsa")]
        AtivoBolsa = 4,

        [StringValue("Fundo de Investimento")]
        FundoInvestimento = 5
    }

    /// <summary>
    /// Tipo de curva usada em apropriações de juros (exponencial vs linear)
    /// </summary>
    public enum TipoApropriacaoSwap
    {
        Exponencial = 1,
        Linear = 2
    }

    /// <summary>
    /// Convenção da base de cálculo para ano a ser usada.
    /// </summary>
    public enum BaseCalculoSwap
    {
        Base252 = 252,
        Base360 = 360,
        Base365 = 365
    }

    /// <summary>
    /// Dias úteis ou corridos.
    /// </summary>
    public enum ContagemDiasSwap
    {
        Uteis = 1,
        Corridos = 2
    }

    public enum TipoLiquidacaoSwap
    {
        Antecipacao = 1,
        Vencimento = 2
    }

    public enum TipoPeriodicidadeReset
    {
        Dias = 1,
        Meses = 2,
        Anos = 3
    }
}
