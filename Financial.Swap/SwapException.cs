﻿using System;

namespace Financial.Swap.Exceptions
{
    /// <summary>
    /// Classe base de Exceção do componente de Swap
    /// </summary>
    public class SwapException : Exception
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public SwapException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public SwapException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public SwapException(string mensagem, Exception inner) : base(mensagem, inner) { }        
    }

    /// <summary>
    /// Exceção de SwapErroConfiguracaoException
    /// </summary>
    public class SwapErroConfiguracaoException : SwapException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public SwapErroConfiguracaoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public SwapErroConfiguracaoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de PosicaoSwapInexistente
    /// </summary>
    public class PosicaoSwapInexistenteException : SwapException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public PosicaoSwapInexistenteException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public PosicaoSwapInexistenteException(string mensagem) : base(mensagem) { }
    }
}


