/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 09/11/2015 14:52:12
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	








		
using Financial.Common;
using Financial.Investidor;


		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Swap
{

	[Serializable]
	abstract public class esPosicaoSwapCollection : esEntityCollection
	{
		public esPosicaoSwapCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoSwapCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoSwapQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoSwapQuery);
		}
		#endregion
		
		virtual public PosicaoSwap DetachEntity(PosicaoSwap entity)
		{
			return base.DetachEntity(entity) as PosicaoSwap;
		}
		
		virtual public PosicaoSwap AttachEntity(PosicaoSwap entity)
		{
			return base.AttachEntity(entity) as PosicaoSwap;
		}
		
		virtual public void Combine(PosicaoSwapCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoSwap this[int index]
		{
			get
			{
				return base[index] as PosicaoSwap;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoSwap);
		}
	}



	[Serializable]
	abstract public class esPosicaoSwap : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoSwapQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoSwap()
		{

		}

		public esPosicaoSwap(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPosicao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPosicaoSwapQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPosicao == idPosicao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao)
		{
			esPosicaoSwapQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAssessor": this.str.IdAssessor = (string)value; break;							
						case "TipoRegistro": this.str.TipoRegistro = (string)value; break;							
						case "NumeroContrato": this.str.NumeroContrato = (string)value; break;							
						case "DataEmissao": this.str.DataEmissao = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "ValorBase": this.str.ValorBase = (string)value; break;							
						case "TipoPonta": this.str.TipoPonta = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;							
						case "TaxaJuros": this.str.TaxaJuros = (string)value; break;							
						case "Percentual": this.str.Percentual = (string)value; break;							
						case "TipoApropriacao": this.str.TipoApropriacao = (string)value; break;							
						case "ContagemDias": this.str.ContagemDias = (string)value; break;							
						case "BaseAno": this.str.BaseAno = (string)value; break;							
						case "TipoPontaContraParte": this.str.TipoPontaContraParte = (string)value; break;							
						case "IdIndiceContraParte": this.str.IdIndiceContraParte = (string)value; break;							
						case "TaxaJurosContraParte": this.str.TaxaJurosContraParte = (string)value; break;							
						case "PercentualContraParte": this.str.PercentualContraParte = (string)value; break;							
						case "TipoApropriacaoContraParte": this.str.TipoApropriacaoContraParte = (string)value; break;							
						case "ContagemDiasContraParte": this.str.ContagemDiasContraParte = (string)value; break;							
						case "BaseAnoContraParte": this.str.BaseAnoContraParte = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "ComGarantia": this.str.ComGarantia = (string)value; break;							
						case "DiasUteis": this.str.DiasUteis = (string)value; break;							
						case "DiasCorridos": this.str.DiasCorridos = (string)value; break;							
						case "ValorParte": this.str.ValorParte = (string)value; break;							
						case "ValorContraParte": this.str.ValorContraParte = (string)value; break;							
						case "Saldo": this.str.Saldo = (string)value; break;							
						case "ValorIndicePartida": this.str.ValorIndicePartida = (string)value; break;							
						case "ValorIndicePartidaContraParte": this.str.ValorIndicePartidaContraParte = (string)value; break;							
						case "IdEstrategia": this.str.IdEstrategia = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "CdAtivoBolsaContraParte": this.str.CdAtivoBolsaContraParte = (string)value; break;							
						case "IdAtivoCarteira": this.str.IdAtivoCarteira = (string)value; break;							
						case "IdAtivoCarteiraContraParte": this.str.IdAtivoCarteiraContraParte = (string)value; break;							
						case "DataUltimoEvento": this.str.DataUltimoEvento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAssessor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAssessor = (System.Int32?)value;
							break;
						
						case "TipoRegistro":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoRegistro = (System.Byte?)value;
							break;
						
						case "DataEmissao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEmissao = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "ValorBase":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBase = (System.Decimal?)value;
							break;
						
						case "TipoPonta":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoPonta = (System.Byte?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
						
						case "TaxaJuros":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaJuros = (System.Decimal?)value;
							break;
						
						case "Percentual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Percentual = (System.Decimal?)value;
							break;
						
						case "TipoApropriacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoApropriacao = (System.Byte?)value;
							break;
						
						case "ContagemDias":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContagemDias = (System.Byte?)value;
							break;
						
						case "BaseAno":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.BaseAno = (System.Int16?)value;
							break;
						
						case "TipoPontaContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoPontaContraParte = (System.Byte?)value;
							break;
						
						case "IdIndiceContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndiceContraParte = (System.Int16?)value;
							break;
						
						case "TaxaJurosContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaJurosContraParte = (System.Decimal?)value;
							break;
						
						case "PercentualContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualContraParte = (System.Decimal?)value;
							break;
						
						case "TipoApropriacaoContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoApropriacaoContraParte = (System.Byte?)value;
							break;
						
						case "ContagemDiasContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContagemDiasContraParte = (System.Byte?)value;
							break;
						
						case "BaseAnoContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.BaseAnoContraParte = (System.Int16?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "DiasUteis":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DiasUteis = (System.Int32?)value;
							break;
						
						case "DiasCorridos":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DiasCorridos = (System.Int32?)value;
							break;
						
						case "ValorParte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorParte = (System.Decimal?)value;
							break;
						
						case "ValorContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorContraParte = (System.Decimal?)value;
							break;
						
						case "Saldo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Saldo = (System.Decimal?)value;
							break;
						
						case "ValorIndicePartida":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIndicePartida = (System.Decimal?)value;
							break;
						
						case "ValorIndicePartidaContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIndicePartidaContraParte = (System.Decimal?)value;
							break;
						
						case "IdEstrategia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEstrategia = (System.Int32?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "IdAtivoCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAtivoCarteira = (System.Int32?)value;
							break;
						
						case "IdAtivoCarteiraContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAtivoCarteiraContraParte = (System.Int32?)value;
							break;
						
						case "DataUltimoEvento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimoEvento = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoSwap.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdOperacao, value))
				{
					this._UpToOperacaoSwapByIdOperacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.IdAssessor
		/// </summary>
		virtual public System.Int32? IdAssessor
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdAssessor);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdAssessor, value))
				{
					this._UpToAssessorByIdAssessor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.TipoRegistro
		/// </summary>
		virtual public System.Byte? TipoRegistro
		{
			get
			{
				return base.GetSystemByte(PosicaoSwapMetadata.ColumnNames.TipoRegistro);
			}
			
			set
			{
				base.SetSystemByte(PosicaoSwapMetadata.ColumnNames.TipoRegistro, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.NumeroContrato
		/// </summary>
		virtual public System.String NumeroContrato
		{
			get
			{
				return base.GetSystemString(PosicaoSwapMetadata.ColumnNames.NumeroContrato);
			}
			
			set
			{
				base.SetSystemString(PosicaoSwapMetadata.ColumnNames.NumeroContrato, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.DataEmissao
		/// </summary>
		virtual public System.DateTime? DataEmissao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoSwapMetadata.ColumnNames.DataEmissao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoSwapMetadata.ColumnNames.DataEmissao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(PosicaoSwapMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoSwapMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.ValorBase
		/// </summary>
		virtual public System.Decimal? ValorBase
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapMetadata.ColumnNames.ValorBase);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapMetadata.ColumnNames.ValorBase, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.TipoPonta
		/// </summary>
		virtual public System.Byte? TipoPonta
		{
			get
			{
				return base.GetSystemByte(PosicaoSwapMetadata.ColumnNames.TipoPonta);
			}
			
			set
			{
				base.SetSystemByte(PosicaoSwapMetadata.ColumnNames.TipoPonta, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(PosicaoSwapMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				base.SetSystemInt16(PosicaoSwapMetadata.ColumnNames.IdIndice, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.TaxaJuros
		/// </summary>
		virtual public System.Decimal? TaxaJuros
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapMetadata.ColumnNames.TaxaJuros);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapMetadata.ColumnNames.TaxaJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.Percentual
		/// </summary>
		virtual public System.Decimal? Percentual
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapMetadata.ColumnNames.Percentual);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapMetadata.ColumnNames.Percentual, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.TipoApropriacao
		/// </summary>
		virtual public System.Byte? TipoApropriacao
		{
			get
			{
				return base.GetSystemByte(PosicaoSwapMetadata.ColumnNames.TipoApropriacao);
			}
			
			set
			{
				base.SetSystemByte(PosicaoSwapMetadata.ColumnNames.TipoApropriacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.ContagemDias
		/// </summary>
		virtual public System.Byte? ContagemDias
		{
			get
			{
				return base.GetSystemByte(PosicaoSwapMetadata.ColumnNames.ContagemDias);
			}
			
			set
			{
				base.SetSystemByte(PosicaoSwapMetadata.ColumnNames.ContagemDias, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.BaseAno
		/// </summary>
		virtual public System.Int16? BaseAno
		{
			get
			{
				return base.GetSystemInt16(PosicaoSwapMetadata.ColumnNames.BaseAno);
			}
			
			set
			{
				base.SetSystemInt16(PosicaoSwapMetadata.ColumnNames.BaseAno, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.TipoPontaContraParte
		/// </summary>
		virtual public System.Byte? TipoPontaContraParte
		{
			get
			{
				return base.GetSystemByte(PosicaoSwapMetadata.ColumnNames.TipoPontaContraParte);
			}
			
			set
			{
				base.SetSystemByte(PosicaoSwapMetadata.ColumnNames.TipoPontaContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.IdIndiceContraParte
		/// </summary>
		virtual public System.Int16? IdIndiceContraParte
		{
			get
			{
				return base.GetSystemInt16(PosicaoSwapMetadata.ColumnNames.IdIndiceContraParte);
			}
			
			set
			{
				if(base.SetSystemInt16(PosicaoSwapMetadata.ColumnNames.IdIndiceContraParte, value))
				{
					this._UpToIndiceByIdIndiceContraParte = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.TaxaJurosContraParte
		/// </summary>
		virtual public System.Decimal? TaxaJurosContraParte
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapMetadata.ColumnNames.TaxaJurosContraParte);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapMetadata.ColumnNames.TaxaJurosContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.PercentualContraParte
		/// </summary>
		virtual public System.Decimal? PercentualContraParte
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapMetadata.ColumnNames.PercentualContraParte);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapMetadata.ColumnNames.PercentualContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.TipoApropriacaoContraParte
		/// </summary>
		virtual public System.Byte? TipoApropriacaoContraParte
		{
			get
			{
				return base.GetSystemByte(PosicaoSwapMetadata.ColumnNames.TipoApropriacaoContraParte);
			}
			
			set
			{
				base.SetSystemByte(PosicaoSwapMetadata.ColumnNames.TipoApropriacaoContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.ContagemDiasContraParte
		/// </summary>
		virtual public System.Byte? ContagemDiasContraParte
		{
			get
			{
				return base.GetSystemByte(PosicaoSwapMetadata.ColumnNames.ContagemDiasContraParte);
			}
			
			set
			{
				base.SetSystemByte(PosicaoSwapMetadata.ColumnNames.ContagemDiasContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.BaseAnoContraParte
		/// </summary>
		virtual public System.Int16? BaseAnoContraParte
		{
			get
			{
				return base.GetSystemInt16(PosicaoSwapMetadata.ColumnNames.BaseAnoContraParte);
			}
			
			set
			{
				base.SetSystemInt16(PosicaoSwapMetadata.ColumnNames.BaseAnoContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.ComGarantia
		/// </summary>
		virtual public System.String ComGarantia
		{
			get
			{
				return base.GetSystemString(PosicaoSwapMetadata.ColumnNames.ComGarantia);
			}
			
			set
			{
				base.SetSystemString(PosicaoSwapMetadata.ColumnNames.ComGarantia, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.DiasUteis
		/// </summary>
		virtual public System.Int32? DiasUteis
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapMetadata.ColumnNames.DiasUteis);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoSwapMetadata.ColumnNames.DiasUteis, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.DiasCorridos
		/// </summary>
		virtual public System.Int32? DiasCorridos
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapMetadata.ColumnNames.DiasCorridos);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoSwapMetadata.ColumnNames.DiasCorridos, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.ValorParte
		/// </summary>
		virtual public System.Decimal? ValorParte
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapMetadata.ColumnNames.ValorParte);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapMetadata.ColumnNames.ValorParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.ValorContraParte
		/// </summary>
		virtual public System.Decimal? ValorContraParte
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapMetadata.ColumnNames.ValorContraParte);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapMetadata.ColumnNames.ValorContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.Saldo
		/// </summary>
		virtual public System.Decimal? Saldo
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapMetadata.ColumnNames.Saldo);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapMetadata.ColumnNames.Saldo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.ValorIndicePartida
		/// </summary>
		virtual public System.Decimal? ValorIndicePartida
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapMetadata.ColumnNames.ValorIndicePartida);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapMetadata.ColumnNames.ValorIndicePartida, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.ValorIndicePartidaContraParte
		/// </summary>
		virtual public System.Decimal? ValorIndicePartidaContraParte
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapMetadata.ColumnNames.ValorIndicePartidaContraParte);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapMetadata.ColumnNames.ValorIndicePartidaContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.IdEstrategia
		/// </summary>
		virtual public System.Int32? IdEstrategia
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdEstrategia);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdEstrategia, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(PosicaoSwapMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				base.SetSystemString(PosicaoSwapMetadata.ColumnNames.CdAtivoBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.CdAtivoBolsaContraParte
		/// </summary>
		virtual public System.String CdAtivoBolsaContraParte
		{
			get
			{
				return base.GetSystemString(PosicaoSwapMetadata.ColumnNames.CdAtivoBolsaContraParte);
			}
			
			set
			{
				base.SetSystemString(PosicaoSwapMetadata.ColumnNames.CdAtivoBolsaContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.IdAtivoCarteira
		/// </summary>
		virtual public System.Int32? IdAtivoCarteira
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdAtivoCarteira);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdAtivoCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.IdAtivoCarteiraContraParte
		/// </summary>
		virtual public System.Int32? IdAtivoCarteiraContraParte
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdAtivoCarteiraContraParte);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoSwapMetadata.ColumnNames.IdAtivoCarteiraContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwap.DataUltimoEvento
		/// </summary>
		virtual public System.DateTime? DataUltimoEvento
		{
			get
			{
				return base.GetSystemDateTime(PosicaoSwapMetadata.ColumnNames.DataUltimoEvento);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoSwapMetadata.ColumnNames.DataUltimoEvento, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected Assessor _UpToAssessorByIdAssessor;
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndiceContraParte;
		[CLSCompliant(false)]
		internal protected OperacaoSwap _UpToOperacaoSwapByIdOperacao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoSwap entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAssessor
			{
				get
				{
					System.Int32? data = entity.IdAssessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAssessor = null;
					else entity.IdAssessor = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoRegistro
			{
				get
				{
					System.Byte? data = entity.TipoRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoRegistro = null;
					else entity.TipoRegistro = Convert.ToByte(value);
				}
			}
				
			public System.String NumeroContrato
			{
				get
				{
					System.String data = entity.NumeroContrato;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroContrato = null;
					else entity.NumeroContrato = Convert.ToString(value);
				}
			}
				
			public System.String DataEmissao
			{
				get
				{
					System.DateTime? data = entity.DataEmissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEmissao = null;
					else entity.DataEmissao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorBase
			{
				get
				{
					System.Decimal? data = entity.ValorBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBase = null;
					else entity.ValorBase = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoPonta
			{
				get
				{
					System.Byte? data = entity.TipoPonta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPonta = null;
					else entity.TipoPonta = Convert.ToByte(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String TaxaJuros
			{
				get
				{
					System.Decimal? data = entity.TaxaJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaJuros = null;
					else entity.TaxaJuros = Convert.ToDecimal(value);
				}
			}
				
			public System.String Percentual
			{
				get
				{
					System.Decimal? data = entity.Percentual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Percentual = null;
					else entity.Percentual = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoApropriacao
			{
				get
				{
					System.Byte? data = entity.TipoApropriacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoApropriacao = null;
					else entity.TipoApropriacao = Convert.ToByte(value);
				}
			}
				
			public System.String ContagemDias
			{
				get
				{
					System.Byte? data = entity.ContagemDias;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContagemDias = null;
					else entity.ContagemDias = Convert.ToByte(value);
				}
			}
				
			public System.String BaseAno
			{
				get
				{
					System.Int16? data = entity.BaseAno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseAno = null;
					else entity.BaseAno = Convert.ToInt16(value);
				}
			}
				
			public System.String TipoPontaContraParte
			{
				get
				{
					System.Byte? data = entity.TipoPontaContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPontaContraParte = null;
					else entity.TipoPontaContraParte = Convert.ToByte(value);
				}
			}
				
			public System.String IdIndiceContraParte
			{
				get
				{
					System.Int16? data = entity.IdIndiceContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndiceContraParte = null;
					else entity.IdIndiceContraParte = Convert.ToInt16(value);
				}
			}
				
			public System.String TaxaJurosContraParte
			{
				get
				{
					System.Decimal? data = entity.TaxaJurosContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaJurosContraParte = null;
					else entity.TaxaJurosContraParte = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercentualContraParte
			{
				get
				{
					System.Decimal? data = entity.PercentualContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualContraParte = null;
					else entity.PercentualContraParte = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoApropriacaoContraParte
			{
				get
				{
					System.Byte? data = entity.TipoApropriacaoContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoApropriacaoContraParte = null;
					else entity.TipoApropriacaoContraParte = Convert.ToByte(value);
				}
			}
				
			public System.String ContagemDiasContraParte
			{
				get
				{
					System.Byte? data = entity.ContagemDiasContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContagemDiasContraParte = null;
					else entity.ContagemDiasContraParte = Convert.ToByte(value);
				}
			}
				
			public System.String BaseAnoContraParte
			{
				get
				{
					System.Int16? data = entity.BaseAnoContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseAnoContraParte = null;
					else entity.BaseAnoContraParte = Convert.ToInt16(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String ComGarantia
			{
				get
				{
					System.String data = entity.ComGarantia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ComGarantia = null;
					else entity.ComGarantia = Convert.ToString(value);
				}
			}
				
			public System.String DiasUteis
			{
				get
				{
					System.Int32? data = entity.DiasUteis;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasUteis = null;
					else entity.DiasUteis = Convert.ToInt32(value);
				}
			}
				
			public System.String DiasCorridos
			{
				get
				{
					System.Int32? data = entity.DiasCorridos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasCorridos = null;
					else entity.DiasCorridos = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorParte
			{
				get
				{
					System.Decimal? data = entity.ValorParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorParte = null;
					else entity.ValorParte = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorContraParte
			{
				get
				{
					System.Decimal? data = entity.ValorContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorContraParte = null;
					else entity.ValorContraParte = Convert.ToDecimal(value);
				}
			}
				
			public System.String Saldo
			{
				get
				{
					System.Decimal? data = entity.Saldo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Saldo = null;
					else entity.Saldo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIndicePartida
			{
				get
				{
					System.Decimal? data = entity.ValorIndicePartida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIndicePartida = null;
					else entity.ValorIndicePartida = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIndicePartidaContraParte
			{
				get
				{
					System.Decimal? data = entity.ValorIndicePartidaContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIndicePartidaContraParte = null;
					else entity.ValorIndicePartidaContraParte = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdEstrategia
			{
				get
				{
					System.Int32? data = entity.IdEstrategia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEstrategia = null;
					else entity.IdEstrategia = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String CdAtivoBolsaContraParte
			{
				get
				{
					System.String data = entity.CdAtivoBolsaContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsaContraParte = null;
					else entity.CdAtivoBolsaContraParte = Convert.ToString(value);
				}
			}
				
			public System.String IdAtivoCarteira
			{
				get
				{
					System.Int32? data = entity.IdAtivoCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAtivoCarteira = null;
					else entity.IdAtivoCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAtivoCarteiraContraParte
			{
				get
				{
					System.Int32? data = entity.IdAtivoCarteiraContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAtivoCarteiraContraParte = null;
					else entity.IdAtivoCarteiraContraParte = Convert.ToInt32(value);
				}
			}
				
			public System.String DataUltimoEvento
			{
				get
				{
					System.DateTime? data = entity.DataUltimoEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimoEvento = null;
					else entity.DataUltimoEvento = Convert.ToDateTime(value);
				}
			}
			

			private esPosicaoSwap entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoSwapQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoSwap can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoSwap : esPosicaoSwap
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_PosicaoSwap_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAssessorByIdAssessor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Assessor_PosicaoSwap_FK1
		/// </summary>

		[XmlIgnore]
		public Assessor UpToAssessorByIdAssessor
		{
			get
			{
				if(this._UpToAssessorByIdAssessor == null
					&& IdAssessor != null					)
				{
					this._UpToAssessorByIdAssessor = new Assessor();
					this._UpToAssessorByIdAssessor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAssessorByIdAssessor", this._UpToAssessorByIdAssessor);
					this._UpToAssessorByIdAssessor.Query.Where(this._UpToAssessorByIdAssessor.Query.IdAssessor == this.IdAssessor);
					this._UpToAssessorByIdAssessor.Query.Load();
				}

				return this._UpToAssessorByIdAssessor;
			}
			
			set
			{
				this.RemovePreSave("UpToAssessorByIdAssessor");
				

				if(value == null)
				{
					this.IdAssessor = null;
					this._UpToAssessorByIdAssessor = null;
				}
				else
				{
					this.IdAssessor = value.IdAssessor;
					this._UpToAssessorByIdAssessor = value;
					this.SetPreSave("UpToAssessorByIdAssessor", this._UpToAssessorByIdAssessor);
				}
				
			}
		}
		#endregion
		

				
		#region UpToIndiceByIdIndiceContraParte - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Indice_PosicaoSwap_FK2
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndiceContraParte
		{
			get
			{
				if(this._UpToIndiceByIdIndiceContraParte == null
					&& IdIndiceContraParte != null					)
				{
					this._UpToIndiceByIdIndiceContraParte = new Indice();
					this._UpToIndiceByIdIndiceContraParte.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndiceContraParte", this._UpToIndiceByIdIndiceContraParte);
					this._UpToIndiceByIdIndiceContraParte.Query.Where(this._UpToIndiceByIdIndiceContraParte.Query.IdIndice == this.IdIndiceContraParte);
					this._UpToIndiceByIdIndiceContraParte.Query.Load();
				}

				return this._UpToIndiceByIdIndiceContraParte;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndiceContraParte");
				

				if(value == null)
				{
					this.IdIndiceContraParte = null;
					this._UpToIndiceByIdIndiceContraParte = null;
				}
				else
				{
					this.IdIndiceContraParte = value.IdIndice;
					this._UpToIndiceByIdIndiceContraParte = value;
					this.SetPreSave("UpToIndiceByIdIndiceContraParte", this._UpToIndiceByIdIndiceContraParte);
				}
				
			}
		}
		#endregion
		

				
		#region UpToOperacaoSwapByIdOperacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Operacao_PosicaoSwap_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoSwap UpToOperacaoSwapByIdOperacao
		{
			get
			{
				if(this._UpToOperacaoSwapByIdOperacao == null
					&& IdOperacao != null					)
				{
					this._UpToOperacaoSwapByIdOperacao = new OperacaoSwap();
					this._UpToOperacaoSwapByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOperacaoSwapByIdOperacao", this._UpToOperacaoSwapByIdOperacao);
					this._UpToOperacaoSwapByIdOperacao.Query.Where(this._UpToOperacaoSwapByIdOperacao.Query.IdOperacao == this.IdOperacao);
					this._UpToOperacaoSwapByIdOperacao.Query.Load();
				}

				return this._UpToOperacaoSwapByIdOperacao;
			}
			
			set
			{
				this.RemovePreSave("UpToOperacaoSwapByIdOperacao");
				

				if(value == null)
				{
					this.IdOperacao = null;
					this._UpToOperacaoSwapByIdOperacao = null;
				}
				else
				{
					this.IdOperacao = value.IdOperacao;
					this._UpToOperacaoSwapByIdOperacao = value;
					this.SetPreSave("UpToOperacaoSwapByIdOperacao", this._UpToOperacaoSwapByIdOperacao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToAssessorByIdAssessor != null)
			{
				this.IdAssessor = this._UpToAssessorByIdAssessor.IdAssessor;
			}
			if(!this.es.IsDeleted && this._UpToOperacaoSwapByIdOperacao != null)
			{
				this.IdOperacao = this._UpToOperacaoSwapByIdOperacao.IdOperacao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoSwapQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoSwapMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAssessor
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.IdAssessor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoRegistro
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.TipoRegistro, esSystemType.Byte);
			}
		} 
		
		public esQueryItem NumeroContrato
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.NumeroContrato, esSystemType.String);
			}
		} 
		
		public esQueryItem DataEmissao
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.DataEmissao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorBase
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.ValorBase, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoPonta
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.TipoPonta, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TaxaJuros
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.TaxaJuros, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Percentual
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.Percentual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoApropriacao
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.TipoApropriacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ContagemDias
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.ContagemDias, esSystemType.Byte);
			}
		} 
		
		public esQueryItem BaseAno
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.BaseAno, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TipoPontaContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.TipoPontaContraParte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdIndiceContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.IdIndiceContraParte, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TaxaJurosContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.TaxaJurosContraParte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercentualContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.PercentualContraParte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoApropriacaoContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.TipoApropriacaoContraParte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ContagemDiasContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.ContagemDiasContraParte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem BaseAnoContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.BaseAnoContraParte, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ComGarantia
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.ComGarantia, esSystemType.String);
			}
		} 
		
		public esQueryItem DiasUteis
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.DiasUteis, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DiasCorridos
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.DiasCorridos, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.ValorParte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.ValorContraParte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Saldo
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.Saldo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIndicePartida
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.ValorIndicePartida, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIndicePartidaContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.ValorIndicePartidaContraParte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdEstrategia
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.IdEstrategia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem CdAtivoBolsaContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.CdAtivoBolsaContraParte, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAtivoCarteira
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.IdAtivoCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAtivoCarteiraContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.IdAtivoCarteiraContraParte, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataUltimoEvento
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapMetadata.ColumnNames.DataUltimoEvento, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PosicaoSwapCollection")]
	public partial class PosicaoSwapCollection : esPosicaoSwapCollection, IEnumerable<PosicaoSwap>
	{
		public PosicaoSwapCollection()
		{

		}
		
		public static implicit operator List<PosicaoSwap>(PosicaoSwapCollection coll)
		{
			List<PosicaoSwap> list = new List<PosicaoSwap>();
			
			foreach (PosicaoSwap emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoSwapMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoSwapQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoSwap(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoSwap();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoSwapQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoSwapQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoSwapQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoSwap AddNew()
		{
			PosicaoSwap entity = base.AddNewEntity() as PosicaoSwap;
			
			return entity;
		}

		public PosicaoSwap FindByPrimaryKey(System.Int32 idPosicao)
		{
			return base.FindByPrimaryKey(idPosicao) as PosicaoSwap;
		}


		#region IEnumerable<PosicaoSwap> Members

		IEnumerator<PosicaoSwap> IEnumerable<PosicaoSwap>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoSwap;
			}
		}

		#endregion
		
		private PosicaoSwapQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoSwap' table
	/// </summary>

	[Serializable]
	public partial class PosicaoSwap : esPosicaoSwap
	{
		public PosicaoSwap()
		{

		}
	
		public PosicaoSwap(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoSwapMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoSwapQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoSwapQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoSwapQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoSwapQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoSwapQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoSwapQuery query;
	}



	[Serializable]
	public partial class PosicaoSwapQuery : esPosicaoSwapQuery
	{
		public PosicaoSwapQuery()
		{

		}		
		
		public PosicaoSwapQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoSwapMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoSwapMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.IdOperacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.IdAssessor, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.IdAssessor;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.TipoRegistro, 4, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.TipoRegistro;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.NumeroContrato, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.NumeroContrato;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.DataEmissao, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.DataEmissao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.DataVencimento, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.ValorBase, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.ValorBase;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.TipoPonta, 9, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.TipoPonta;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.IdIndice, 10, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.TaxaJuros, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.TaxaJuros;	
			c.NumericPrecision = 10;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.Percentual, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.Percentual;	
			c.NumericPrecision = 10;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.TipoApropriacao, 13, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.TipoApropriacao;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.ContagemDias, 14, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.ContagemDias;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.BaseAno, 15, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.BaseAno;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.TipoPontaContraParte, 16, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.TipoPontaContraParte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.IdIndiceContraParte, 17, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.IdIndiceContraParte;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.TaxaJurosContraParte, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.TaxaJurosContraParte;	
			c.NumericPrecision = 10;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.PercentualContraParte, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.PercentualContraParte;	
			c.NumericPrecision = 10;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.TipoApropriacaoContraParte, 20, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.TipoApropriacaoContraParte;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.ContagemDiasContraParte, 21, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.ContagemDiasContraParte;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.BaseAnoContraParte, 22, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.BaseAnoContraParte;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.IdAgente, 23, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.ComGarantia, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.ComGarantia;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.DiasUteis, 25, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.DiasUteis;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.DiasCorridos, 26, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.DiasCorridos;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.ValorParte, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.ValorParte;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.ValorContraParte, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.ValorContraParte;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.Saldo, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.Saldo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.ValorIndicePartida, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.ValorIndicePartida;	
			c.NumericPrecision = 20;
			c.NumericScale = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.ValorIndicePartidaContraParte, 31, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.ValorIndicePartidaContraParte;	
			c.NumericPrecision = 20;
			c.NumericScale = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.IdEstrategia, 32, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.IdEstrategia;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.ValorIR, 33, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.CdAtivoBolsa, 34, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.CdAtivoBolsaContraParte, 35, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.CdAtivoBolsaContraParte;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.IdAtivoCarteira, 36, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.IdAtivoCarteira;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.IdAtivoCarteiraContraParte, 37, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.IdAtivoCarteiraContraParte;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapMetadata.ColumnNames.DataUltimoEvento, 38, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoSwapMetadata.PropertyNames.DataUltimoEvento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PosicaoSwapMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdAssessor = "IdAssessor";
			 public const string TipoRegistro = "TipoRegistro";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string DataEmissao = "DataEmissao";
			 public const string DataVencimento = "DataVencimento";
			 public const string ValorBase = "ValorBase";
			 public const string TipoPonta = "TipoPonta";
			 public const string IdIndice = "IdIndice";
			 public const string TaxaJuros = "TaxaJuros";
			 public const string Percentual = "Percentual";
			 public const string TipoApropriacao = "TipoApropriacao";
			 public const string ContagemDias = "ContagemDias";
			 public const string BaseAno = "BaseAno";
			 public const string TipoPontaContraParte = "TipoPontaContraParte";
			 public const string IdIndiceContraParte = "IdIndiceContraParte";
			 public const string TaxaJurosContraParte = "TaxaJurosContraParte";
			 public const string PercentualContraParte = "PercentualContraParte";
			 public const string TipoApropriacaoContraParte = "TipoApropriacaoContraParte";
			 public const string ContagemDiasContraParte = "ContagemDiasContraParte";
			 public const string BaseAnoContraParte = "BaseAnoContraParte";
			 public const string IdAgente = "IdAgente";
			 public const string ComGarantia = "ComGarantia";
			 public const string DiasUteis = "DiasUteis";
			 public const string DiasCorridos = "DiasCorridos";
			 public const string ValorParte = "ValorParte";
			 public const string ValorContraParte = "ValorContraParte";
			 public const string Saldo = "Saldo";
			 public const string ValorIndicePartida = "ValorIndicePartida";
			 public const string ValorIndicePartidaContraParte = "ValorIndicePartidaContraParte";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string ValorIR = "ValorIR";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string CdAtivoBolsaContraParte = "CdAtivoBolsaContraParte";
			 public const string IdAtivoCarteira = "IdAtivoCarteira";
			 public const string IdAtivoCarteiraContraParte = "IdAtivoCarteiraContraParte";
			 public const string DataUltimoEvento = "DataUltimoEvento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdAssessor = "IdAssessor";
			 public const string TipoRegistro = "TipoRegistro";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string DataEmissao = "DataEmissao";
			 public const string DataVencimento = "DataVencimento";
			 public const string ValorBase = "ValorBase";
			 public const string TipoPonta = "TipoPonta";
			 public const string IdIndice = "IdIndice";
			 public const string TaxaJuros = "TaxaJuros";
			 public const string Percentual = "Percentual";
			 public const string TipoApropriacao = "TipoApropriacao";
			 public const string ContagemDias = "ContagemDias";
			 public const string BaseAno = "BaseAno";
			 public const string TipoPontaContraParte = "TipoPontaContraParte";
			 public const string IdIndiceContraParte = "IdIndiceContraParte";
			 public const string TaxaJurosContraParte = "TaxaJurosContraParte";
			 public const string PercentualContraParte = "PercentualContraParte";
			 public const string TipoApropriacaoContraParte = "TipoApropriacaoContraParte";
			 public const string ContagemDiasContraParte = "ContagemDiasContraParte";
			 public const string BaseAnoContraParte = "BaseAnoContraParte";
			 public const string IdAgente = "IdAgente";
			 public const string ComGarantia = "ComGarantia";
			 public const string DiasUteis = "DiasUteis";
			 public const string DiasCorridos = "DiasCorridos";
			 public const string ValorParte = "ValorParte";
			 public const string ValorContraParte = "ValorContraParte";
			 public const string Saldo = "Saldo";
			 public const string ValorIndicePartida = "ValorIndicePartida";
			 public const string ValorIndicePartidaContraParte = "ValorIndicePartidaContraParte";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string ValorIR = "ValorIR";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string CdAtivoBolsaContraParte = "CdAtivoBolsaContraParte";
			 public const string IdAtivoCarteira = "IdAtivoCarteira";
			 public const string IdAtivoCarteiraContraParte = "IdAtivoCarteiraContraParte";
			 public const string DataUltimoEvento = "DataUltimoEvento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoSwapMetadata))
			{
				if(PosicaoSwapMetadata.mapDelegates == null)
				{
					PosicaoSwapMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoSwapMetadata.meta == null)
				{
					PosicaoSwapMetadata.meta = new PosicaoSwapMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAssessor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoRegistro", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("NumeroContrato", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataEmissao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorBase", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoPonta", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TaxaJuros", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Percentual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoApropriacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ContagemDias", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("BaseAno", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TipoPontaContraParte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdIndiceContraParte", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TaxaJurosContraParte", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PercentualContraParte", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoApropriacaoContraParte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ContagemDiasContraParte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("BaseAnoContraParte", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ComGarantia", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DiasUteis", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DiasCorridos", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorParte", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorContraParte", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Saldo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIndicePartida", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIndicePartidaContraParte", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdEstrategia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CdAtivoBolsaContraParte", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAtivoCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAtivoCarteiraContraParte", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataUltimoEvento", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "PosicaoSwap";
				meta.Destination = "PosicaoSwap";
				
				meta.spInsert = "proc_PosicaoSwapInsert";				
				meta.spUpdate = "proc_PosicaoSwapUpdate";		
				meta.spDelete = "proc_PosicaoSwapDelete";
				meta.spLoadAll = "proc_PosicaoSwapLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoSwapLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoSwapMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
