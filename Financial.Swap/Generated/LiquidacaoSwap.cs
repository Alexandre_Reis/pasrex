/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 31/03/2014 21:49:05
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		
		
using Financial.Investidor;





		




				
				








				




		

		
		
		
		
		





namespace Financial.Swap
{

	[Serializable]
	abstract public class esLiquidacaoSwapCollection : esEntityCollection
	{
		public esLiquidacaoSwapCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "LiquidacaoSwapCollection";
		}

		#region Query Logic
		protected void InitQuery(esLiquidacaoSwapQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esLiquidacaoSwapQuery);
		}
		#endregion
		
		virtual public LiquidacaoSwap DetachEntity(LiquidacaoSwap entity)
		{
			return base.DetachEntity(entity) as LiquidacaoSwap;
		}
		
		virtual public LiquidacaoSwap AttachEntity(LiquidacaoSwap entity)
		{
			return base.AttachEntity(entity) as LiquidacaoSwap;
		}
		
		virtual public void Combine(LiquidacaoSwapCollection collection)
		{
			base.Combine(collection);
		}
		
		new public LiquidacaoSwap this[int index]
		{
			get
			{
				return base[index] as LiquidacaoSwap;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(LiquidacaoSwap);
		}
	}



	[Serializable]
	abstract public class esLiquidacaoSwap : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esLiquidacaoSwapQuery GetDynamicQuery()
		{
			return null;
		}

		public esLiquidacaoSwap()
		{

		}

		public esLiquidacaoSwap(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idLiquidacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idLiquidacao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esLiquidacaoSwapQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdLiquidacao == idLiquidacao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLiquidacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idLiquidacao)
		{
			esLiquidacaoSwapQuery query = this.GetDynamicQuery();
			query.Where(query.IdLiquidacao == idLiquidacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLiquidacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdLiquidacao",idLiquidacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLiquidacao": this.str.IdLiquidacao = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "ValorAntecipacao": this.str.ValorAntecipacao = (string)value; break;							
						case "TipoLiquidacao": this.str.TipoLiquidacao = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLiquidacao = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "ValorAntecipacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAntecipacao = (System.Decimal?)value;
							break;
						
						case "TipoLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoLiquidacao = (System.Byte?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to LiquidacaoSwap.IdLiquidacao
		/// </summary>
		virtual public System.Int32? IdLiquidacao
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoSwapMetadata.ColumnNames.IdLiquidacao);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoSwapMetadata.ColumnNames.IdLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoSwap.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(LiquidacaoSwapMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(LiquidacaoSwapMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoSwap.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoSwapMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(LiquidacaoSwapMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoSwap.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoSwapMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoSwapMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoSwap.ValorAntecipacao
		/// </summary>
		virtual public System.Decimal? ValorAntecipacao
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoSwapMetadata.ColumnNames.ValorAntecipacao);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoSwapMetadata.ColumnNames.ValorAntecipacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoSwap.TipoLiquidacao
		/// </summary>
		virtual public System.Byte? TipoLiquidacao
		{
			get
			{
				return base.GetSystemByte(LiquidacaoSwapMetadata.ColumnNames.TipoLiquidacao);
			}
			
			set
			{
				base.SetSystemByte(LiquidacaoSwapMetadata.ColumnNames.TipoLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoSwap.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoSwapMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoSwapMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esLiquidacaoSwap entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLiquidacao
			{
				get
				{
					System.Int32? data = entity.IdLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLiquidacao = null;
					else entity.IdLiquidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorAntecipacao
			{
				get
				{
					System.Decimal? data = entity.ValorAntecipacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAntecipacao = null;
					else entity.ValorAntecipacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoLiquidacao
			{
				get
				{
					System.Byte? data = entity.TipoLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoLiquidacao = null;
					else entity.TipoLiquidacao = Convert.ToByte(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
			

			private esLiquidacaoSwap entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esLiquidacaoSwapQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esLiquidacaoSwap can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class LiquidacaoSwap : esLiquidacaoSwap
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_LiquidacaoSwap_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esLiquidacaoSwapQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoSwapMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLiquidacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoSwapMetadata.ColumnNames.IdLiquidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, LiquidacaoSwapMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, LiquidacaoSwapMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoSwapMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorAntecipacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoSwapMetadata.ColumnNames.ValorAntecipacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoLiquidacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoSwapMetadata.ColumnNames.TipoLiquidacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, LiquidacaoSwapMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("LiquidacaoSwapCollection")]
	public partial class LiquidacaoSwapCollection : esLiquidacaoSwapCollection, IEnumerable<LiquidacaoSwap>
	{
		public LiquidacaoSwapCollection()
		{

		}
		
		public static implicit operator List<LiquidacaoSwap>(LiquidacaoSwapCollection coll)
		{
			List<LiquidacaoSwap> list = new List<LiquidacaoSwap>();
			
			foreach (LiquidacaoSwap emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  LiquidacaoSwapMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoSwapQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new LiquidacaoSwap(row);
		}

		override protected esEntity CreateEntity()
		{
			return new LiquidacaoSwap();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public LiquidacaoSwapQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoSwapQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(LiquidacaoSwapQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public LiquidacaoSwap AddNew()
		{
			LiquidacaoSwap entity = base.AddNewEntity() as LiquidacaoSwap;
			
			return entity;
		}

		public LiquidacaoSwap FindByPrimaryKey(System.Int32 idLiquidacao)
		{
			return base.FindByPrimaryKey(idLiquidacao) as LiquidacaoSwap;
		}


		#region IEnumerable<LiquidacaoSwap> Members

		IEnumerator<LiquidacaoSwap> IEnumerable<LiquidacaoSwap>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as LiquidacaoSwap;
			}
		}

		#endregion
		
		private LiquidacaoSwapQuery query;
	}


	/// <summary>
	/// Encapsulates the 'LiquidacaoSwap' table
	/// </summary>

	[Serializable]
	public partial class LiquidacaoSwap : esLiquidacaoSwap
	{
		public LiquidacaoSwap()
		{

		}
	
		public LiquidacaoSwap(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoSwapMetadata.Meta();
			}
		}
		
		
		
		override protected esLiquidacaoSwapQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoSwapQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public LiquidacaoSwapQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoSwapQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(LiquidacaoSwapQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private LiquidacaoSwapQuery query;
	}



	[Serializable]
	public partial class LiquidacaoSwapQuery : esLiquidacaoSwapQuery
	{
		public LiquidacaoSwapQuery()
		{

		}		
		
		public LiquidacaoSwapQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class LiquidacaoSwapMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected LiquidacaoSwapMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(LiquidacaoSwapMetadata.ColumnNames.IdLiquidacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoSwapMetadata.PropertyNames.IdLiquidacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoSwapMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LiquidacaoSwapMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoSwapMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoSwapMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoSwapMetadata.ColumnNames.IdPosicao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoSwapMetadata.PropertyNames.IdPosicao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoSwapMetadata.ColumnNames.ValorAntecipacao, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoSwapMetadata.PropertyNames.ValorAntecipacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoSwapMetadata.ColumnNames.TipoLiquidacao, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = LiquidacaoSwapMetadata.PropertyNames.TipoLiquidacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoSwapMetadata.ColumnNames.ValorIR, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoSwapMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public LiquidacaoSwapMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string Data = "Data";
			 public const string IdCliente = "IdCliente";
			 public const string IdPosicao = "IdPosicao";
			 public const string ValorAntecipacao = "ValorAntecipacao";
			 public const string TipoLiquidacao = "TipoLiquidacao";
			 public const string ValorIR = "ValorIR";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string Data = "Data";
			 public const string IdCliente = "IdCliente";
			 public const string IdPosicao = "IdPosicao";
			 public const string ValorAntecipacao = "ValorAntecipacao";
			 public const string TipoLiquidacao = "TipoLiquidacao";
			 public const string ValorIR = "ValorIR";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(LiquidacaoSwapMetadata))
			{
				if(LiquidacaoSwapMetadata.mapDelegates == null)
				{
					LiquidacaoSwapMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (LiquidacaoSwapMetadata.meta == null)
				{
					LiquidacaoSwapMetadata.meta = new LiquidacaoSwapMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdLiquidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorAntecipacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoLiquidacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "LiquidacaoSwap";
				meta.Destination = "LiquidacaoSwap";
				
				meta.spInsert = "proc_LiquidacaoSwapInsert";				
				meta.spUpdate = "proc_LiquidacaoSwapUpdate";		
				meta.spDelete = "proc_LiquidacaoSwapDelete";
				meta.spLoadAll = "proc_LiquidacaoSwapLoadAll";
				meta.spLoadByPrimaryKey = "proc_LiquidacaoSwapLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private LiquidacaoSwapMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
