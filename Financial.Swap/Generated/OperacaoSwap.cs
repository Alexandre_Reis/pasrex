/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/03/2016 18:55:47
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Bolsa;
using Financial.Fundo;
using Financial.Common;
using Financial.Investidor;

namespace Financial.Swap
{

	[Serializable]
	abstract public class esOperacaoSwapCollection : esEntityCollection
	{
		public esOperacaoSwapCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OperacaoSwapCollection";
		}

		#region Query Logic
		protected void InitQuery(esOperacaoSwapQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOperacaoSwapQuery);
		}
		#endregion
		
		virtual public OperacaoSwap DetachEntity(OperacaoSwap entity)
		{
			return base.DetachEntity(entity) as OperacaoSwap;
		}
		
		virtual public OperacaoSwap AttachEntity(OperacaoSwap entity)
		{
			return base.AttachEntity(entity) as OperacaoSwap;
		}
		
		virtual public void Combine(OperacaoSwapCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OperacaoSwap this[int index]
		{
			get
			{
				return base[index] as OperacaoSwap;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OperacaoSwap);
		}
	}



	[Serializable]
	abstract public class esOperacaoSwap : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOperacaoSwapQuery GetDynamicQuery()
		{
			return null;
		}

		public esOperacaoSwap()
		{

		}

		public esOperacaoSwap(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao)
		{
			esOperacaoSwapQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAssessor": this.str.IdAssessor = (string)value; break;							
						case "TipoRegistro": this.str.TipoRegistro = (string)value; break;							
						case "NumeroContrato": this.str.NumeroContrato = (string)value; break;							
						case "DataEmissao": this.str.DataEmissao = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "ValorBase": this.str.ValorBase = (string)value; break;							
						case "TipoPonta": this.str.TipoPonta = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;							
						case "TaxaJuros": this.str.TaxaJuros = (string)value; break;							
						case "Percentual": this.str.Percentual = (string)value; break;							
						case "TipoApropriacao": this.str.TipoApropriacao = (string)value; break;							
						case "ContagemDias": this.str.ContagemDias = (string)value; break;							
						case "BaseAno": this.str.BaseAno = (string)value; break;							
						case "TipoPontaContraParte": this.str.TipoPontaContraParte = (string)value; break;							
						case "IdIndiceContraParte": this.str.IdIndiceContraParte = (string)value; break;							
						case "TaxaJurosContraParte": this.str.TaxaJurosContraParte = (string)value; break;							
						case "PercentualContraParte": this.str.PercentualContraParte = (string)value; break;							
						case "TipoApropriacaoContraParte": this.str.TipoApropriacaoContraParte = (string)value; break;							
						case "ContagemDiasContraParte": this.str.ContagemDiasContraParte = (string)value; break;							
						case "BaseAnoContraParte": this.str.BaseAnoContraParte = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "ComGarantia": this.str.ComGarantia = (string)value; break;							
						case "DiasUteis": this.str.DiasUteis = (string)value; break;							
						case "DiasCorridos": this.str.DiasCorridos = (string)value; break;							
						case "Corretagem": this.str.Corretagem = (string)value; break;							
						case "Emolumento": this.str.Emolumento = (string)value; break;							
						case "Registro": this.str.Registro = (string)value; break;							
						case "ValorIndicePartida": this.str.ValorIndicePartida = (string)value; break;							
						case "ValorIndicePartidaContraParte": this.str.ValorIndicePartidaContraParte = (string)value; break;							
						case "IdEstrategia": this.str.IdEstrategia = (string)value; break;							
						case "DataRegistro": this.str.DataRegistro = (string)value; break;							
						case "CodigoIsin": this.str.CodigoIsin = (string)value; break;							
						case "CpfcnpjContraParte": this.str.CpfcnpjContraParte = (string)value; break;							
						case "UserTimeStamp": this.str.UserTimeStamp = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "CdAtivoBolsaContraParte": this.str.CdAtivoBolsaContraParte = (string)value; break;							
						case "IdAtivoCarteira": this.str.IdAtivoCarteira = (string)value; break;							
						case "IdAtivoCarteiraContraParte": this.str.IdAtivoCarteiraContraParte = (string)value; break;							
						case "RealizaReset": this.str.RealizaReset = (string)value; break;							
						case "PeriodicidadeReset": this.str.PeriodicidadeReset = (string)value; break;							
						case "QtdePeriodicidade": this.str.QtdePeriodicidade = (string)value; break;							
						case "DataInicioReset": this.str.DataInicioReset = (string)value; break;							
						case "IdConta": this.str.IdConta = (string)value; break;							
						case "IdCategoriaMovimentacao": this.str.IdCategoriaMovimentacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAssessor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAssessor = (System.Int32?)value;
							break;
						
						case "TipoRegistro":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoRegistro = (System.Byte?)value;
							break;
						
						case "DataEmissao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEmissao = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "ValorBase":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBase = (System.Decimal?)value;
							break;
						
						case "TipoPonta":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoPonta = (System.Byte?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
						
						case "TaxaJuros":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaJuros = (System.Decimal?)value;
							break;
						
						case "Percentual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Percentual = (System.Decimal?)value;
							break;
						
						case "TipoApropriacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoApropriacao = (System.Byte?)value;
							break;
						
						case "ContagemDias":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContagemDias = (System.Byte?)value;
							break;
						
						case "BaseAno":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.BaseAno = (System.Int16?)value;
							break;
						
						case "TipoPontaContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoPontaContraParte = (System.Byte?)value;
							break;
						
						case "IdIndiceContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndiceContraParte = (System.Int16?)value;
							break;
						
						case "TaxaJurosContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaJurosContraParte = (System.Decimal?)value;
							break;
						
						case "PercentualContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualContraParte = (System.Decimal?)value;
							break;
						
						case "TipoApropriacaoContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoApropriacaoContraParte = (System.Byte?)value;
							break;
						
						case "ContagemDiasContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContagemDiasContraParte = (System.Byte?)value;
							break;
						
						case "BaseAnoContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.BaseAnoContraParte = (System.Int16?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "DiasUteis":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DiasUteis = (System.Int32?)value;
							break;
						
						case "DiasCorridos":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DiasCorridos = (System.Int32?)value;
							break;
						
						case "Corretagem":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Corretagem = (System.Decimal?)value;
							break;
						
						case "Emolumento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Emolumento = (System.Decimal?)value;
							break;
						
						case "Registro":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Registro = (System.Decimal?)value;
							break;
						
						case "ValorIndicePartida":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIndicePartida = (System.Decimal?)value;
							break;
						
						case "ValorIndicePartidaContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIndicePartidaContraParte = (System.Decimal?)value;
							break;
						
						case "IdEstrategia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEstrategia = (System.Int32?)value;
							break;
						
						case "DataRegistro":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRegistro = (System.DateTime?)value;
							break;
						
						case "UserTimeStamp":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.UserTimeStamp = (System.DateTime?)value;
							break;
						
						case "IdAtivoCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAtivoCarteira = (System.Int32?)value;
							break;
						
						case "IdAtivoCarteiraContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAtivoCarteiraContraParte = (System.Int32?)value;
							break;
						
						case "PeriodicidadeReset":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PeriodicidadeReset = (System.Int32?)value;
							break;
						
						case "QtdePeriodicidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.QtdePeriodicidade = (System.Int32?)value;
							break;
						
						case "DataInicioReset":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicioReset = (System.DateTime?)value;
							break;
						
						case "IdConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConta = (System.Int32?)value;
							break;
						
						case "IdCategoriaMovimentacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCategoriaMovimentacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OperacaoSwap.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.IdAssessor
		/// </summary>
		virtual public System.Int32? IdAssessor
		{
			get
			{
				return base.GetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdAssessor);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdAssessor, value))
				{
					this._UpToAssessorByIdAssessor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.TipoRegistro
		/// </summary>
		virtual public System.Byte? TipoRegistro
		{
			get
			{
				return base.GetSystemByte(OperacaoSwapMetadata.ColumnNames.TipoRegistro);
			}
			
			set
			{
				base.SetSystemByte(OperacaoSwapMetadata.ColumnNames.TipoRegistro, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.NumeroContrato
		/// </summary>
		virtual public System.String NumeroContrato
		{
			get
			{
				return base.GetSystemString(OperacaoSwapMetadata.ColumnNames.NumeroContrato);
			}
			
			set
			{
				base.SetSystemString(OperacaoSwapMetadata.ColumnNames.NumeroContrato, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.DataEmissao
		/// </summary>
		virtual public System.DateTime? DataEmissao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoSwapMetadata.ColumnNames.DataEmissao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoSwapMetadata.ColumnNames.DataEmissao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(OperacaoSwapMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoSwapMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.ValorBase
		/// </summary>
		virtual public System.Decimal? ValorBase
		{
			get
			{
				return base.GetSystemDecimal(OperacaoSwapMetadata.ColumnNames.ValorBase);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoSwapMetadata.ColumnNames.ValorBase, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.TipoPonta
		/// </summary>
		virtual public System.Byte? TipoPonta
		{
			get
			{
				return base.GetSystemByte(OperacaoSwapMetadata.ColumnNames.TipoPonta);
			}
			
			set
			{
				base.SetSystemByte(OperacaoSwapMetadata.ColumnNames.TipoPonta, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(OperacaoSwapMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				if(base.SetSystemInt16(OperacaoSwapMetadata.ColumnNames.IdIndice, value))
				{
					this._UpToIndiceByIdIndice = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.TaxaJuros
		/// </summary>
		virtual public System.Decimal? TaxaJuros
		{
			get
			{
				return base.GetSystemDecimal(OperacaoSwapMetadata.ColumnNames.TaxaJuros);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoSwapMetadata.ColumnNames.TaxaJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.Percentual
		/// </summary>
		virtual public System.Decimal? Percentual
		{
			get
			{
				return base.GetSystemDecimal(OperacaoSwapMetadata.ColumnNames.Percentual);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoSwapMetadata.ColumnNames.Percentual, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.TipoApropriacao
		/// </summary>
		virtual public System.Byte? TipoApropriacao
		{
			get
			{
				return base.GetSystemByte(OperacaoSwapMetadata.ColumnNames.TipoApropriacao);
			}
			
			set
			{
				base.SetSystemByte(OperacaoSwapMetadata.ColumnNames.TipoApropriacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.ContagemDias
		/// </summary>
		virtual public System.Byte? ContagemDias
		{
			get
			{
				return base.GetSystemByte(OperacaoSwapMetadata.ColumnNames.ContagemDias);
			}
			
			set
			{
				base.SetSystemByte(OperacaoSwapMetadata.ColumnNames.ContagemDias, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.BaseAno
		/// </summary>
		virtual public System.Int16? BaseAno
		{
			get
			{
				return base.GetSystemInt16(OperacaoSwapMetadata.ColumnNames.BaseAno);
			}
			
			set
			{
				base.SetSystemInt16(OperacaoSwapMetadata.ColumnNames.BaseAno, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.TipoPontaContraParte
		/// </summary>
		virtual public System.Byte? TipoPontaContraParte
		{
			get
			{
				return base.GetSystemByte(OperacaoSwapMetadata.ColumnNames.TipoPontaContraParte);
			}
			
			set
			{
				base.SetSystemByte(OperacaoSwapMetadata.ColumnNames.TipoPontaContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.IdIndiceContraParte
		/// </summary>
		virtual public System.Int16? IdIndiceContraParte
		{
			get
			{
				return base.GetSystemInt16(OperacaoSwapMetadata.ColumnNames.IdIndiceContraParte);
			}
			
			set
			{
				base.SetSystemInt16(OperacaoSwapMetadata.ColumnNames.IdIndiceContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.TaxaJurosContraParte
		/// </summary>
		virtual public System.Decimal? TaxaJurosContraParte
		{
			get
			{
				return base.GetSystemDecimal(OperacaoSwapMetadata.ColumnNames.TaxaJurosContraParte);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoSwapMetadata.ColumnNames.TaxaJurosContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.PercentualContraParte
		/// </summary>
		virtual public System.Decimal? PercentualContraParte
		{
			get
			{
				return base.GetSystemDecimal(OperacaoSwapMetadata.ColumnNames.PercentualContraParte);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoSwapMetadata.ColumnNames.PercentualContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.TipoApropriacaoContraParte
		/// </summary>
		virtual public System.Byte? TipoApropriacaoContraParte
		{
			get
			{
				return base.GetSystemByte(OperacaoSwapMetadata.ColumnNames.TipoApropriacaoContraParte);
			}
			
			set
			{
				base.SetSystemByte(OperacaoSwapMetadata.ColumnNames.TipoApropriacaoContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.ContagemDiasContraParte
		/// </summary>
		virtual public System.Byte? ContagemDiasContraParte
		{
			get
			{
				return base.GetSystemByte(OperacaoSwapMetadata.ColumnNames.ContagemDiasContraParte);
			}
			
			set
			{
				base.SetSystemByte(OperacaoSwapMetadata.ColumnNames.ContagemDiasContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.BaseAnoContraParte
		/// </summary>
		virtual public System.Int16? BaseAnoContraParte
		{
			get
			{
				return base.GetSystemInt16(OperacaoSwapMetadata.ColumnNames.BaseAnoContraParte);
			}
			
			set
			{
				base.SetSystemInt16(OperacaoSwapMetadata.ColumnNames.BaseAnoContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.ComGarantia
		/// </summary>
		virtual public System.String ComGarantia
		{
			get
			{
				return base.GetSystemString(OperacaoSwapMetadata.ColumnNames.ComGarantia);
			}
			
			set
			{
				base.SetSystemString(OperacaoSwapMetadata.ColumnNames.ComGarantia, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.DiasUteis
		/// </summary>
		virtual public System.Int32? DiasUteis
		{
			get
			{
				return base.GetSystemInt32(OperacaoSwapMetadata.ColumnNames.DiasUteis);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoSwapMetadata.ColumnNames.DiasUteis, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.DiasCorridos
		/// </summary>
		virtual public System.Int32? DiasCorridos
		{
			get
			{
				return base.GetSystemInt32(OperacaoSwapMetadata.ColumnNames.DiasCorridos);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoSwapMetadata.ColumnNames.DiasCorridos, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.Corretagem
		/// </summary>
		virtual public System.Decimal? Corretagem
		{
			get
			{
				return base.GetSystemDecimal(OperacaoSwapMetadata.ColumnNames.Corretagem);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoSwapMetadata.ColumnNames.Corretagem, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.Emolumento
		/// </summary>
		virtual public System.Decimal? Emolumento
		{
			get
			{
				return base.GetSystemDecimal(OperacaoSwapMetadata.ColumnNames.Emolumento);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoSwapMetadata.ColumnNames.Emolumento, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.Registro
		/// </summary>
		virtual public System.Decimal? Registro
		{
			get
			{
				return base.GetSystemDecimal(OperacaoSwapMetadata.ColumnNames.Registro);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoSwapMetadata.ColumnNames.Registro, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.ValorIndicePartida
		/// </summary>
		virtual public System.Decimal? ValorIndicePartida
		{
			get
			{
				return base.GetSystemDecimal(OperacaoSwapMetadata.ColumnNames.ValorIndicePartida);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoSwapMetadata.ColumnNames.ValorIndicePartida, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.ValorIndicePartidaContraParte
		/// </summary>
		virtual public System.Decimal? ValorIndicePartidaContraParte
		{
			get
			{
				return base.GetSystemDecimal(OperacaoSwapMetadata.ColumnNames.ValorIndicePartidaContraParte);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoSwapMetadata.ColumnNames.ValorIndicePartidaContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.IdEstrategia
		/// </summary>
		virtual public System.Int32? IdEstrategia
		{
			get
			{
				return base.GetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdEstrategia);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdEstrategia, value))
				{
					this._UpToEstrategiaByIdEstrategia = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.DataRegistro
		/// </summary>
		virtual public System.DateTime? DataRegistro
		{
			get
			{
				return base.GetSystemDateTime(OperacaoSwapMetadata.ColumnNames.DataRegistro);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoSwapMetadata.ColumnNames.DataRegistro, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.CodigoIsin
		/// </summary>
		virtual public System.String CodigoIsin
		{
			get
			{
				return base.GetSystemString(OperacaoSwapMetadata.ColumnNames.CodigoIsin);
			}
			
			set
			{
				base.SetSystemString(OperacaoSwapMetadata.ColumnNames.CodigoIsin, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.CpfcnpjContraParte
		/// </summary>
		virtual public System.String CpfcnpjContraParte
		{
			get
			{
				return base.GetSystemString(OperacaoSwapMetadata.ColumnNames.CpfcnpjContraParte);
			}
			
			set
			{
				base.SetSystemString(OperacaoSwapMetadata.ColumnNames.CpfcnpjContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.UserTimeStamp
		/// </summary>
		virtual public System.DateTime? UserTimeStamp
		{
			get
			{
				return base.GetSystemDateTime(OperacaoSwapMetadata.ColumnNames.UserTimeStamp);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoSwapMetadata.ColumnNames.UserTimeStamp, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(OperacaoSwapMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(OperacaoSwapMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.CdAtivoBolsaContraParte
		/// </summary>
		virtual public System.String CdAtivoBolsaContraParte
		{
			get
			{
				return base.GetSystemString(OperacaoSwapMetadata.ColumnNames.CdAtivoBolsaContraParte);
			}
			
			set
			{
				if(base.SetSystemString(OperacaoSwapMetadata.ColumnNames.CdAtivoBolsaContraParte, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsaContraParte = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.IdAtivoCarteira
		/// </summary>
		virtual public System.Int32? IdAtivoCarteira
		{
			get
			{
				return base.GetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdAtivoCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdAtivoCarteira, value))
				{
					this._UpToCarteiraByIdAtivoCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.IdAtivoCarteiraContraParte
		/// </summary>
		virtual public System.Int32? IdAtivoCarteiraContraParte
		{
			get
			{
				return base.GetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdAtivoCarteiraContraParte);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdAtivoCarteiraContraParte, value))
				{
					this._UpToCarteiraByIdAtivoCarteiraContraParte = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.RealizaReset
		/// </summary>
		virtual public System.String RealizaReset
		{
			get
			{
				return base.GetSystemString(OperacaoSwapMetadata.ColumnNames.RealizaReset);
			}
			
			set
			{
				base.SetSystemString(OperacaoSwapMetadata.ColumnNames.RealizaReset, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.PeriodicidadeReset
		/// </summary>
		virtual public System.Int32? PeriodicidadeReset
		{
			get
			{
				return base.GetSystemInt32(OperacaoSwapMetadata.ColumnNames.PeriodicidadeReset);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoSwapMetadata.ColumnNames.PeriodicidadeReset, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.QtdePeriodicidade
		/// </summary>
		virtual public System.Int32? QtdePeriodicidade
		{
			get
			{
				return base.GetSystemInt32(OperacaoSwapMetadata.ColumnNames.QtdePeriodicidade);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoSwapMetadata.ColumnNames.QtdePeriodicidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.DataInicioReset
		/// </summary>
		virtual public System.DateTime? DataInicioReset
		{
			get
			{
				return base.GetSystemDateTime(OperacaoSwapMetadata.ColumnNames.DataInicioReset);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoSwapMetadata.ColumnNames.DataInicioReset, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.IdConta
		/// </summary>
		virtual public System.Int32? IdConta
		{
			get
			{
				return base.GetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdConta);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdConta, value))
				{
					this._UpToContaCorrenteByIdConta = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoSwap.IdCategoriaMovimentacao
		/// </summary>
		virtual public System.Int32? IdCategoriaMovimentacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdCategoriaMovimentacao);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoSwapMetadata.ColumnNames.IdCategoriaMovimentacao, value))
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected Assessor _UpToAssessorByIdAssessor;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsaContraParte;
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdAtivoCarteira;
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdAtivoCarteiraContraParte;
		[CLSCompliant(false)]
		internal protected CategoriaMovimentacao _UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
        internal protected Financial.Investidor.ContaCorrente _UpToContaCorrenteByIdConta;
		[CLSCompliant(false)]
		internal protected Estrategia _UpToEstrategiaByIdEstrategia;
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndice;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOperacaoSwap entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAssessor
			{
				get
				{
					System.Int32? data = entity.IdAssessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAssessor = null;
					else entity.IdAssessor = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoRegistro
			{
				get
				{
					System.Byte? data = entity.TipoRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoRegistro = null;
					else entity.TipoRegistro = Convert.ToByte(value);
				}
			}
				
			public System.String NumeroContrato
			{
				get
				{
					System.String data = entity.NumeroContrato;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroContrato = null;
					else entity.NumeroContrato = Convert.ToString(value);
				}
			}
				
			public System.String DataEmissao
			{
				get
				{
					System.DateTime? data = entity.DataEmissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEmissao = null;
					else entity.DataEmissao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorBase
			{
				get
				{
					System.Decimal? data = entity.ValorBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBase = null;
					else entity.ValorBase = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoPonta
			{
				get
				{
					System.Byte? data = entity.TipoPonta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPonta = null;
					else entity.TipoPonta = Convert.ToByte(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String TaxaJuros
			{
				get
				{
					System.Decimal? data = entity.TaxaJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaJuros = null;
					else entity.TaxaJuros = Convert.ToDecimal(value);
				}
			}
				
			public System.String Percentual
			{
				get
				{
					System.Decimal? data = entity.Percentual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Percentual = null;
					else entity.Percentual = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoApropriacao
			{
				get
				{
					System.Byte? data = entity.TipoApropriacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoApropriacao = null;
					else entity.TipoApropriacao = Convert.ToByte(value);
				}
			}
				
			public System.String ContagemDias
			{
				get
				{
					System.Byte? data = entity.ContagemDias;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContagemDias = null;
					else entity.ContagemDias = Convert.ToByte(value);
				}
			}
				
			public System.String BaseAno
			{
				get
				{
					System.Int16? data = entity.BaseAno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseAno = null;
					else entity.BaseAno = Convert.ToInt16(value);
				}
			}
				
			public System.String TipoPontaContraParte
			{
				get
				{
					System.Byte? data = entity.TipoPontaContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPontaContraParte = null;
					else entity.TipoPontaContraParte = Convert.ToByte(value);
				}
			}
				
			public System.String IdIndiceContraParte
			{
				get
				{
					System.Int16? data = entity.IdIndiceContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndiceContraParte = null;
					else entity.IdIndiceContraParte = Convert.ToInt16(value);
				}
			}
				
			public System.String TaxaJurosContraParte
			{
				get
				{
					System.Decimal? data = entity.TaxaJurosContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaJurosContraParte = null;
					else entity.TaxaJurosContraParte = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercentualContraParte
			{
				get
				{
					System.Decimal? data = entity.PercentualContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualContraParte = null;
					else entity.PercentualContraParte = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoApropriacaoContraParte
			{
				get
				{
					System.Byte? data = entity.TipoApropriacaoContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoApropriacaoContraParte = null;
					else entity.TipoApropriacaoContraParte = Convert.ToByte(value);
				}
			}
				
			public System.String ContagemDiasContraParte
			{
				get
				{
					System.Byte? data = entity.ContagemDiasContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContagemDiasContraParte = null;
					else entity.ContagemDiasContraParte = Convert.ToByte(value);
				}
			}
				
			public System.String BaseAnoContraParte
			{
				get
				{
					System.Int16? data = entity.BaseAnoContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseAnoContraParte = null;
					else entity.BaseAnoContraParte = Convert.ToInt16(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String ComGarantia
			{
				get
				{
					System.String data = entity.ComGarantia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ComGarantia = null;
					else entity.ComGarantia = Convert.ToString(value);
				}
			}
				
			public System.String DiasUteis
			{
				get
				{
					System.Int32? data = entity.DiasUteis;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasUteis = null;
					else entity.DiasUteis = Convert.ToInt32(value);
				}
			}
				
			public System.String DiasCorridos
			{
				get
				{
					System.Int32? data = entity.DiasCorridos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasCorridos = null;
					else entity.DiasCorridos = Convert.ToInt32(value);
				}
			}
				
			public System.String Corretagem
			{
				get
				{
					System.Decimal? data = entity.Corretagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Corretagem = null;
					else entity.Corretagem = Convert.ToDecimal(value);
				}
			}
				
			public System.String Emolumento
			{
				get
				{
					System.Decimal? data = entity.Emolumento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Emolumento = null;
					else entity.Emolumento = Convert.ToDecimal(value);
				}
			}
				
			public System.String Registro
			{
				get
				{
					System.Decimal? data = entity.Registro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Registro = null;
					else entity.Registro = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIndicePartida
			{
				get
				{
					System.Decimal? data = entity.ValorIndicePartida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIndicePartida = null;
					else entity.ValorIndicePartida = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIndicePartidaContraParte
			{
				get
				{
					System.Decimal? data = entity.ValorIndicePartidaContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIndicePartidaContraParte = null;
					else entity.ValorIndicePartidaContraParte = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdEstrategia
			{
				get
				{
					System.Int32? data = entity.IdEstrategia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEstrategia = null;
					else entity.IdEstrategia = Convert.ToInt32(value);
				}
			}
				
			public System.String DataRegistro
			{
				get
				{
					System.DateTime? data = entity.DataRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRegistro = null;
					else entity.DataRegistro = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodigoIsin
			{
				get
				{
					System.String data = entity.CodigoIsin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoIsin = null;
					else entity.CodigoIsin = Convert.ToString(value);
				}
			}
				
			public System.String CpfcnpjContraParte
			{
				get
				{
					System.String data = entity.CpfcnpjContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CpfcnpjContraParte = null;
					else entity.CpfcnpjContraParte = Convert.ToString(value);
				}
			}
				
			public System.String UserTimeStamp
			{
				get
				{
					System.DateTime? data = entity.UserTimeStamp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UserTimeStamp = null;
					else entity.UserTimeStamp = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String CdAtivoBolsaContraParte
			{
				get
				{
					System.String data = entity.CdAtivoBolsaContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsaContraParte = null;
					else entity.CdAtivoBolsaContraParte = Convert.ToString(value);
				}
			}
				
			public System.String IdAtivoCarteira
			{
				get
				{
					System.Int32? data = entity.IdAtivoCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAtivoCarteira = null;
					else entity.IdAtivoCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAtivoCarteiraContraParte
			{
				get
				{
					System.Int32? data = entity.IdAtivoCarteiraContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAtivoCarteiraContraParte = null;
					else entity.IdAtivoCarteiraContraParte = Convert.ToInt32(value);
				}
			}
				
			public System.String RealizaReset
			{
				get
				{
					System.String data = entity.RealizaReset;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RealizaReset = null;
					else entity.RealizaReset = Convert.ToString(value);
				}
			}
				
			public System.String PeriodicidadeReset
			{
				get
				{
					System.Int32? data = entity.PeriodicidadeReset;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PeriodicidadeReset = null;
					else entity.PeriodicidadeReset = Convert.ToInt32(value);
				}
			}
				
			public System.String QtdePeriodicidade
			{
				get
				{
					System.Int32? data = entity.QtdePeriodicidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdePeriodicidade = null;
					else entity.QtdePeriodicidade = Convert.ToInt32(value);
				}
			}
				
			public System.String DataInicioReset
			{
				get
				{
					System.DateTime? data = entity.DataInicioReset;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicioReset = null;
					else entity.DataInicioReset = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdConta
			{
				get
				{
					System.Int32? data = entity.IdConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConta = null;
					else entity.IdConta = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCategoriaMovimentacao
			{
				get
				{
					System.Int32? data = entity.IdCategoriaMovimentacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCategoriaMovimentacao = null;
					else entity.IdCategoriaMovimentacao = Convert.ToInt32(value);
				}
			}
			

			private esOperacaoSwap entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOperacaoSwapQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOperacaoSwap can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OperacaoSwap : esOperacaoSwap
	{

				
		#region EventoSwapCollectionByIdOperacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EventoSwap_OpSwap_FK
		/// </summary>

		[XmlIgnore]
		public EventoSwapCollection EventoSwapCollectionByIdOperacao
		{
			get
			{
				if(this._EventoSwapCollectionByIdOperacao == null)
				{
					this._EventoSwapCollectionByIdOperacao = new EventoSwapCollection();
					this._EventoSwapCollectionByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoSwapCollectionByIdOperacao", this._EventoSwapCollectionByIdOperacao);
				
					if(this.IdOperacao != null)
					{
						this._EventoSwapCollectionByIdOperacao.Query.Where(this._EventoSwapCollectionByIdOperacao.Query.IdOperacao == this.IdOperacao);
						this._EventoSwapCollectionByIdOperacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoSwapCollectionByIdOperacao.fks.Add(EventoSwapMetadata.ColumnNames.IdOperacao, this.IdOperacao);
					}
				}

				return this._EventoSwapCollectionByIdOperacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoSwapCollectionByIdOperacao != null) 
				{ 
					this.RemovePostSave("EventoSwapCollectionByIdOperacao"); 
					this._EventoSwapCollectionByIdOperacao = null;
					
				} 
			} 			
		}

		private EventoSwapCollection _EventoSwapCollectionByIdOperacao;
		#endregion

				
		#region ExcecoesTributacaoIRCollectionByIdOperacaoSwap - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__ExcecoesT__IdOpe__676A338E
		/// </summary>

		[XmlIgnore]
		public ExcecoesTributacaoIRCollection ExcecoesTributacaoIRCollectionByIdOperacaoSwap
		{
			get
			{
				if(this._ExcecoesTributacaoIRCollectionByIdOperacaoSwap == null)
				{
					this._ExcecoesTributacaoIRCollectionByIdOperacaoSwap = new ExcecoesTributacaoIRCollection();
					this._ExcecoesTributacaoIRCollectionByIdOperacaoSwap.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ExcecoesTributacaoIRCollectionByIdOperacaoSwap", this._ExcecoesTributacaoIRCollectionByIdOperacaoSwap);
				
					if(this.IdOperacao != null)
					{
						this._ExcecoesTributacaoIRCollectionByIdOperacaoSwap.Query.Where(this._ExcecoesTributacaoIRCollectionByIdOperacaoSwap.Query.IdOperacaoSwap == this.IdOperacao);
						this._ExcecoesTributacaoIRCollectionByIdOperacaoSwap.Query.Load();

						// Auto-hookup Foreign Keys
						this._ExcecoesTributacaoIRCollectionByIdOperacaoSwap.fks.Add(ExcecoesTributacaoIRMetadata.ColumnNames.IdOperacaoSwap, this.IdOperacao);
					}
				}

				return this._ExcecoesTributacaoIRCollectionByIdOperacaoSwap;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ExcecoesTributacaoIRCollectionByIdOperacaoSwap != null) 
				{ 
					this.RemovePostSave("ExcecoesTributacaoIRCollectionByIdOperacaoSwap"); 
					this._ExcecoesTributacaoIRCollectionByIdOperacaoSwap = null;
					
				} 
			} 			
		}

		private ExcecoesTributacaoIRCollection _ExcecoesTributacaoIRCollectionByIdOperacaoSwap;
		#endregion

				
		#region PosicaoSwapCollectionByIdOperacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Operacao_PosicaoSwap_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapCollection PosicaoSwapCollectionByIdOperacao
		{
			get
			{
				if(this._PosicaoSwapCollectionByIdOperacao == null)
				{
					this._PosicaoSwapCollectionByIdOperacao = new PosicaoSwapCollection();
					this._PosicaoSwapCollectionByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapCollectionByIdOperacao", this._PosicaoSwapCollectionByIdOperacao);
				
					if(this.IdOperacao != null)
					{
						this._PosicaoSwapCollectionByIdOperacao.Query.Where(this._PosicaoSwapCollectionByIdOperacao.Query.IdOperacao == this.IdOperacao);
						this._PosicaoSwapCollectionByIdOperacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapCollectionByIdOperacao.fks.Add(PosicaoSwapMetadata.ColumnNames.IdOperacao, this.IdOperacao);
					}
				}

				return this._PosicaoSwapCollectionByIdOperacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapCollectionByIdOperacao != null) 
				{ 
					this.RemovePostSave("PosicaoSwapCollectionByIdOperacao"); 
					this._PosicaoSwapCollectionByIdOperacao = null;
					
				} 
			} 			
		}

		private PosicaoSwapCollection _PosicaoSwapCollectionByIdOperacao;
		#endregion

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_OperacaoSwap_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAssessorByIdAssessor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Assessor_OperacaoSwap_FK1
		/// </summary>

		[XmlIgnore]
		public Assessor UpToAssessorByIdAssessor
		{
			get
			{
				if(this._UpToAssessorByIdAssessor == null
					&& IdAssessor != null					)
				{
					this._UpToAssessorByIdAssessor = new Assessor();
					this._UpToAssessorByIdAssessor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAssessorByIdAssessor", this._UpToAssessorByIdAssessor);
					this._UpToAssessorByIdAssessor.Query.Where(this._UpToAssessorByIdAssessor.Query.IdAssessor == this.IdAssessor);
					this._UpToAssessorByIdAssessor.Query.Load();
				}

				return this._UpToAssessorByIdAssessor;
			}
			
			set
			{
				this.RemovePreSave("UpToAssessorByIdAssessor");
				

				if(value == null)
				{
					this.IdAssessor = null;
					this._UpToAssessorByIdAssessor = null;
				}
				else
				{
					this.IdAssessor = value.IdAssessor;
					this._UpToAssessorByIdAssessor = value;
					this.SetPreSave("UpToAssessorByIdAssessor", this._UpToAssessorByIdAssessor);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OperacaoSwap_AtivoBolsa_FK
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsaContraParte - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OperacaoSwap_AtivoBolsaCP_FK
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsaContraParte
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsaContraParte == null
					&& CdAtivoBolsaContraParte != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsaContraParte = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsaContraParte.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsaContraParte", this._UpToAtivoBolsaByCdAtivoBolsaContraParte);
					this._UpToAtivoBolsaByCdAtivoBolsaContraParte.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsaContraParte.Query.CdAtivoBolsa == this.CdAtivoBolsaContraParte);
					this._UpToAtivoBolsaByCdAtivoBolsaContraParte.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsaContraParte;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsaContraParte");
				

				if(value == null)
				{
					this.CdAtivoBolsaContraParte = null;
					this._UpToAtivoBolsaByCdAtivoBolsaContraParte = null;
				}
				else
				{
					this.CdAtivoBolsaContraParte = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsaContraParte = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsaContraParte", this._UpToAtivoBolsaByCdAtivoBolsaContraParte);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCarteiraByIdAtivoCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OperacaoSwap_Carteira_FK
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdAtivoCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdAtivoCarteira == null
					&& IdAtivoCarteira != null					)
				{
					this._UpToCarteiraByIdAtivoCarteira = new Carteira();
					this._UpToCarteiraByIdAtivoCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdAtivoCarteira", this._UpToCarteiraByIdAtivoCarteira);
					this._UpToCarteiraByIdAtivoCarteira.Query.Where(this._UpToCarteiraByIdAtivoCarteira.Query.IdCarteira == this.IdAtivoCarteira);
					this._UpToCarteiraByIdAtivoCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdAtivoCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdAtivoCarteira");
				

				if(value == null)
				{
					this.IdAtivoCarteira = null;
					this._UpToCarteiraByIdAtivoCarteira = null;
				}
				else
				{
					this.IdAtivoCarteira = value.IdCarteira;
					this._UpToCarteiraByIdAtivoCarteira = value;
					this.SetPreSave("UpToCarteiraByIdAtivoCarteira", this._UpToCarteiraByIdAtivoCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCarteiraByIdAtivoCarteiraContraParte - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OperacaoSwap_CarteiraCP_FK
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdAtivoCarteiraContraParte
		{
			get
			{
				if(this._UpToCarteiraByIdAtivoCarteiraContraParte == null
					&& IdAtivoCarteiraContraParte != null					)
				{
					this._UpToCarteiraByIdAtivoCarteiraContraParte = new Carteira();
					this._UpToCarteiraByIdAtivoCarteiraContraParte.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdAtivoCarteiraContraParte", this._UpToCarteiraByIdAtivoCarteiraContraParte);
					this._UpToCarteiraByIdAtivoCarteiraContraParte.Query.Where(this._UpToCarteiraByIdAtivoCarteiraContraParte.Query.IdCarteira == this.IdAtivoCarteiraContraParte);
					this._UpToCarteiraByIdAtivoCarteiraContraParte.Query.Load();
				}

				return this._UpToCarteiraByIdAtivoCarteiraContraParte;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdAtivoCarteiraContraParte");
				

				if(value == null)
				{
					this.IdAtivoCarteiraContraParte = null;
					this._UpToCarteiraByIdAtivoCarteiraContraParte = null;
				}
				else
				{
					this.IdAtivoCarteiraContraParte = value.IdCarteira;
					this._UpToCarteiraByIdAtivoCarteiraContraParte = value;
					this.SetPreSave("UpToCarteiraByIdAtivoCarteiraContraParte", this._UpToCarteiraByIdAtivoCarteiraContraParte);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCategoriaMovimentacaoByIdCategoriaMovimentacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OperacaoSwap_CatMovimentacao_FK1
		/// </summary>

		[XmlIgnore]
		public CategoriaMovimentacao UpToCategoriaMovimentacaoByIdCategoriaMovimentacao
		{
			get
			{
				if(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao == null
					&& IdCategoriaMovimentacao != null					)
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = new CategoriaMovimentacao();
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Where(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.IdCategoriaMovimentacao == this.IdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Load();
				}

				return this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
			}
			
			set
			{
				this.RemovePreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao");
				

				if(value == null)
				{
					this.IdCategoriaMovimentacao = null;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
				else
				{
					this.IdCategoriaMovimentacao = value.IdCategoriaMovimentacao;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = value;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_OperacaoSwap_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContaCorrenteByIdConta - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_OperacaoSwap_ContaCorrente
		/// </summary>

		[XmlIgnore]
        public Financial.Investidor.ContaCorrente UpToContaCorrenteByIdConta
		{
			get
			{
				if(this._UpToContaCorrenteByIdConta == null
					&& IdConta != null					)
				{
                    this._UpToContaCorrenteByIdConta = new Financial.Investidor.ContaCorrente();
					this._UpToContaCorrenteByIdConta.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
					this._UpToContaCorrenteByIdConta.Query.Where(this._UpToContaCorrenteByIdConta.Query.IdConta == this.IdConta);
					this._UpToContaCorrenteByIdConta.Query.Load();
				}

				return this._UpToContaCorrenteByIdConta;
			}
			
			set
			{
				this.RemovePreSave("UpToContaCorrenteByIdConta");
				

				if(value == null)
				{
					this.IdConta = null;
					this._UpToContaCorrenteByIdConta = null;
				}
				else
				{
					this.IdConta = value.IdConta;
					this._UpToContaCorrenteByIdConta = value;
					this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
				}
				
			}
		}
		#endregion
		

				
		#region UpToEstrategiaByIdEstrategia - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Estrategia_OperacaoSwap_FK1
		/// </summary>

		[XmlIgnore]
		public Estrategia UpToEstrategiaByIdEstrategia
		{
			get
			{
				if(this._UpToEstrategiaByIdEstrategia == null
					&& IdEstrategia != null					)
				{
					this._UpToEstrategiaByIdEstrategia = new Estrategia();
					this._UpToEstrategiaByIdEstrategia.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEstrategiaByIdEstrategia", this._UpToEstrategiaByIdEstrategia);
					this._UpToEstrategiaByIdEstrategia.Query.Where(this._UpToEstrategiaByIdEstrategia.Query.IdEstrategia == this.IdEstrategia);
					this._UpToEstrategiaByIdEstrategia.Query.Load();
				}

				return this._UpToEstrategiaByIdEstrategia;
			}
			
			set
			{
				this.RemovePreSave("UpToEstrategiaByIdEstrategia");
				

				if(value == null)
				{
					this.IdEstrategia = null;
					this._UpToEstrategiaByIdEstrategia = null;
				}
				else
				{
					this.IdEstrategia = value.IdEstrategia;
					this._UpToEstrategiaByIdEstrategia = value;
					this.SetPreSave("UpToEstrategiaByIdEstrategia", this._UpToEstrategiaByIdEstrategia);
				}
				
			}
		}
		#endregion
		

				
		#region UpToIndiceByIdIndice - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Indice_OperacaoSwap_FK1
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndice
		{
			get
			{
				if(this._UpToIndiceByIdIndice == null
					&& IdIndice != null					)
				{
					this._UpToIndiceByIdIndice = new Indice();
					this._UpToIndiceByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
					this._UpToIndiceByIdIndice.Query.Where(this._UpToIndiceByIdIndice.Query.IdIndice == this.IdIndice);
					this._UpToIndiceByIdIndice.Query.Load();
				}

				return this._UpToIndiceByIdIndice;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndice");
				

				if(value == null)
				{
					this.IdIndice = null;
					this._UpToIndiceByIdIndice = null;
				}
				else
				{
					this.IdIndice = value.IdIndice;
					this._UpToIndiceByIdIndice = value;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "EventoSwapCollectionByIdOperacao", typeof(EventoSwapCollection), new EventoSwap()));
			props.Add(new esPropertyDescriptor(this, "ExcecoesTributacaoIRCollectionByIdOperacaoSwap", typeof(ExcecoesTributacaoIRCollection), new ExcecoesTributacaoIR()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapCollectionByIdOperacao", typeof(PosicaoSwapCollection), new PosicaoSwap()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToAssessorByIdAssessor != null)
			{
				this.IdAssessor = this._UpToAssessorByIdAssessor.IdAssessor;
			}
			if(!this.es.IsDeleted && this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao != null)
			{
				this.IdCategoriaMovimentacao = this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.IdCategoriaMovimentacao;
			}
			if(!this.es.IsDeleted && this._UpToContaCorrenteByIdConta != null)
			{
				this.IdConta = this._UpToContaCorrenteByIdConta.IdConta;
			}
			if(!this.es.IsDeleted && this._UpToEstrategiaByIdEstrategia != null)
			{
				this.IdEstrategia = this._UpToEstrategiaByIdEstrategia.IdEstrategia;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._EventoSwapCollectionByIdOperacao != null)
			{
				foreach(EventoSwap obj in this._EventoSwapCollectionByIdOperacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOperacao = this.IdOperacao;
					}
				}
			}
			if(this._ExcecoesTributacaoIRCollectionByIdOperacaoSwap != null)
			{
				foreach(ExcecoesTributacaoIR obj in this._ExcecoesTributacaoIRCollectionByIdOperacaoSwap)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOperacaoSwap = this.IdOperacao;
					}
				}
			}
			if(this._PosicaoSwapCollectionByIdOperacao != null)
			{
				foreach(PosicaoSwap obj in this._PosicaoSwapCollectionByIdOperacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOperacao = this.IdOperacao;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOperacaoSwapQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoSwapMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAssessor
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.IdAssessor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoRegistro
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.TipoRegistro, esSystemType.Byte);
			}
		} 
		
		public esQueryItem NumeroContrato
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.NumeroContrato, esSystemType.String);
			}
		} 
		
		public esQueryItem DataEmissao
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.DataEmissao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorBase
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.ValorBase, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoPonta
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.TipoPonta, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TaxaJuros
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.TaxaJuros, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Percentual
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.Percentual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoApropriacao
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.TipoApropriacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ContagemDias
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.ContagemDias, esSystemType.Byte);
			}
		} 
		
		public esQueryItem BaseAno
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.BaseAno, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TipoPontaContraParte
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.TipoPontaContraParte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdIndiceContraParte
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.IdIndiceContraParte, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TaxaJurosContraParte
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.TaxaJurosContraParte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercentualContraParte
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.PercentualContraParte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoApropriacaoContraParte
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.TipoApropriacaoContraParte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ContagemDiasContraParte
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.ContagemDiasContraParte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem BaseAnoContraParte
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.BaseAnoContraParte, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ComGarantia
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.ComGarantia, esSystemType.String);
			}
		} 
		
		public esQueryItem DiasUteis
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.DiasUteis, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DiasCorridos
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.DiasCorridos, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Corretagem
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.Corretagem, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Emolumento
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.Emolumento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Registro
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.Registro, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIndicePartida
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.ValorIndicePartida, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIndicePartidaContraParte
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.ValorIndicePartidaContraParte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdEstrategia
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.IdEstrategia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataRegistro
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.DataRegistro, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodigoIsin
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.CodigoIsin, esSystemType.String);
			}
		} 
		
		public esQueryItem CpfcnpjContraParte
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.CpfcnpjContraParte, esSystemType.String);
			}
		} 
		
		public esQueryItem UserTimeStamp
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.UserTimeStamp, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem CdAtivoBolsaContraParte
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.CdAtivoBolsaContraParte, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAtivoCarteira
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.IdAtivoCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAtivoCarteiraContraParte
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.IdAtivoCarteiraContraParte, esSystemType.Int32);
			}
		} 
		
		public esQueryItem RealizaReset
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.RealizaReset, esSystemType.String);
			}
		} 
		
		public esQueryItem PeriodicidadeReset
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.PeriodicidadeReset, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QtdePeriodicidade
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.QtdePeriodicidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataInicioReset
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.DataInicioReset, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdConta
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.IdConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCategoriaMovimentacao
		{
			get
			{
				return new esQueryItem(this, OperacaoSwapMetadata.ColumnNames.IdCategoriaMovimentacao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OperacaoSwapCollection")]
	public partial class OperacaoSwapCollection : esOperacaoSwapCollection, IEnumerable<OperacaoSwap>
	{
		public OperacaoSwapCollection()
		{

		}
		
		public static implicit operator List<OperacaoSwap>(OperacaoSwapCollection coll)
		{
			List<OperacaoSwap> list = new List<OperacaoSwap>();
			
			foreach (OperacaoSwap emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OperacaoSwapMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoSwapQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OperacaoSwap(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OperacaoSwap();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OperacaoSwapQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoSwapQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OperacaoSwapQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OperacaoSwap AddNew()
		{
			OperacaoSwap entity = base.AddNewEntity() as OperacaoSwap;
			
			return entity;
		}

		public OperacaoSwap FindByPrimaryKey(System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(idOperacao) as OperacaoSwap;
		}


		#region IEnumerable<OperacaoSwap> Members

		IEnumerator<OperacaoSwap> IEnumerable<OperacaoSwap>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OperacaoSwap;
			}
		}

		#endregion
		
		private OperacaoSwapQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OperacaoSwap' table
	/// </summary>

	[Serializable]
	public partial class OperacaoSwap : esOperacaoSwap
	{
		public OperacaoSwap()
		{

		}
	
		public OperacaoSwap(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoSwapMetadata.Meta();
			}
		}
		
		
		
		override protected esOperacaoSwapQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoSwapQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OperacaoSwapQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoSwapQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OperacaoSwapQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OperacaoSwapQuery query;
	}



	[Serializable]
	public partial class OperacaoSwapQuery : esOperacaoSwapQuery
	{
		public OperacaoSwapQuery()
		{

		}		
		
		public OperacaoSwapQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OperacaoSwapMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OperacaoSwapMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.IdAssessor, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.IdAssessor;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.TipoRegistro, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.TipoRegistro;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.NumeroContrato, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.NumeroContrato;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.DataEmissao, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.DataEmissao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.DataVencimento, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.ValorBase, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.ValorBase;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.TipoPonta, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.TipoPonta;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.IdIndice, 9, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.TaxaJuros, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.TaxaJuros;	
			c.NumericPrecision = 10;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.Percentual, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.Percentual;	
			c.NumericPrecision = 10;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.TipoApropriacao, 12, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.TipoApropriacao;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.ContagemDias, 13, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.ContagemDias;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.BaseAno, 14, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.BaseAno;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.TipoPontaContraParte, 15, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.TipoPontaContraParte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.IdIndiceContraParte, 16, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.IdIndiceContraParte;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.TaxaJurosContraParte, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.TaxaJurosContraParte;	
			c.NumericPrecision = 10;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.PercentualContraParte, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.PercentualContraParte;	
			c.NumericPrecision = 10;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.TipoApropriacaoContraParte, 19, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.TipoApropriacaoContraParte;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.ContagemDiasContraParte, 20, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.ContagemDiasContraParte;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.BaseAnoContraParte, 21, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.BaseAnoContraParte;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.IdAgente, 22, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.ComGarantia, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.ComGarantia;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.DiasUteis, 24, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.DiasUteis;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.DiasCorridos, 25, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.DiasCorridos;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.Corretagem, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.Corretagem;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.Emolumento, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.Emolumento;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.Registro, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.Registro;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.ValorIndicePartida, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.ValorIndicePartida;	
			c.NumericPrecision = 20;
			c.NumericScale = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.ValorIndicePartidaContraParte, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.ValorIndicePartidaContraParte;	
			c.NumericPrecision = 20;
			c.NumericScale = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.IdEstrategia, 31, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.IdEstrategia;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.DataRegistro, 32, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.DataRegistro;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.CodigoIsin, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.CodigoIsin;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.CpfcnpjContraParte, 34, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.CpfcnpjContraParte;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.UserTimeStamp, 35, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.UserTimeStamp;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"(getdate())";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.CdAtivoBolsa, 36, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.CdAtivoBolsaContraParte, 37, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.CdAtivoBolsaContraParte;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.IdAtivoCarteira, 38, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.IdAtivoCarteira;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.IdAtivoCarteiraContraParte, 39, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.IdAtivoCarteiraContraParte;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.RealizaReset, 40, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.RealizaReset;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.PeriodicidadeReset, 41, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.PeriodicidadeReset;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.QtdePeriodicidade, 42, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.QtdePeriodicidade;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.DataInicioReset, 43, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.DataInicioReset;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.IdConta, 44, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.IdConta;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoSwapMetadata.ColumnNames.IdCategoriaMovimentacao, 45, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoSwapMetadata.PropertyNames.IdCategoriaMovimentacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OperacaoSwapMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdAssessor = "IdAssessor";
			 public const string TipoRegistro = "TipoRegistro";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string DataEmissao = "DataEmissao";
			 public const string DataVencimento = "DataVencimento";
			 public const string ValorBase = "ValorBase";
			 public const string TipoPonta = "TipoPonta";
			 public const string IdIndice = "IdIndice";
			 public const string TaxaJuros = "TaxaJuros";
			 public const string Percentual = "Percentual";
			 public const string TipoApropriacao = "TipoApropriacao";
			 public const string ContagemDias = "ContagemDias";
			 public const string BaseAno = "BaseAno";
			 public const string TipoPontaContraParte = "TipoPontaContraParte";
			 public const string IdIndiceContraParte = "IdIndiceContraParte";
			 public const string TaxaJurosContraParte = "TaxaJurosContraParte";
			 public const string PercentualContraParte = "PercentualContraParte";
			 public const string TipoApropriacaoContraParte = "TipoApropriacaoContraParte";
			 public const string ContagemDiasContraParte = "ContagemDiasContraParte";
			 public const string BaseAnoContraParte = "BaseAnoContraParte";
			 public const string IdAgente = "IdAgente";
			 public const string ComGarantia = "ComGarantia";
			 public const string DiasUteis = "DiasUteis";
			 public const string DiasCorridos = "DiasCorridos";
			 public const string Corretagem = "Corretagem";
			 public const string Emolumento = "Emolumento";
			 public const string Registro = "Registro";
			 public const string ValorIndicePartida = "ValorIndicePartida";
			 public const string ValorIndicePartidaContraParte = "ValorIndicePartidaContraParte";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string DataRegistro = "DataRegistro";
			 public const string CodigoIsin = "CodigoIsin";
			 public const string CpfcnpjContraParte = "CpfcnpjContraParte";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string CdAtivoBolsaContraParte = "CdAtivoBolsaContraParte";
			 public const string IdAtivoCarteira = "IdAtivoCarteira";
			 public const string IdAtivoCarteiraContraParte = "IdAtivoCarteiraContraParte";
			 public const string RealizaReset = "RealizaReset";
			 public const string PeriodicidadeReset = "PeriodicidadeReset";
			 public const string QtdePeriodicidade = "QtdePeriodicidade";
			 public const string DataInicioReset = "DataInicioReset";
			 public const string IdConta = "IdConta";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdAssessor = "IdAssessor";
			 public const string TipoRegistro = "TipoRegistro";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string DataEmissao = "DataEmissao";
			 public const string DataVencimento = "DataVencimento";
			 public const string ValorBase = "ValorBase";
			 public const string TipoPonta = "TipoPonta";
			 public const string IdIndice = "IdIndice";
			 public const string TaxaJuros = "TaxaJuros";
			 public const string Percentual = "Percentual";
			 public const string TipoApropriacao = "TipoApropriacao";
			 public const string ContagemDias = "ContagemDias";
			 public const string BaseAno = "BaseAno";
			 public const string TipoPontaContraParte = "TipoPontaContraParte";
			 public const string IdIndiceContraParte = "IdIndiceContraParte";
			 public const string TaxaJurosContraParte = "TaxaJurosContraParte";
			 public const string PercentualContraParte = "PercentualContraParte";
			 public const string TipoApropriacaoContraParte = "TipoApropriacaoContraParte";
			 public const string ContagemDiasContraParte = "ContagemDiasContraParte";
			 public const string BaseAnoContraParte = "BaseAnoContraParte";
			 public const string IdAgente = "IdAgente";
			 public const string ComGarantia = "ComGarantia";
			 public const string DiasUteis = "DiasUteis";
			 public const string DiasCorridos = "DiasCorridos";
			 public const string Corretagem = "Corretagem";
			 public const string Emolumento = "Emolumento";
			 public const string Registro = "Registro";
			 public const string ValorIndicePartida = "ValorIndicePartida";
			 public const string ValorIndicePartidaContraParte = "ValorIndicePartidaContraParte";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string DataRegistro = "DataRegistro";
			 public const string CodigoIsin = "CodigoIsin";
			 public const string CpfcnpjContraParte = "CpfcnpjContraParte";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string CdAtivoBolsaContraParte = "CdAtivoBolsaContraParte";
			 public const string IdAtivoCarteira = "IdAtivoCarteira";
			 public const string IdAtivoCarteiraContraParte = "IdAtivoCarteiraContraParte";
			 public const string RealizaReset = "RealizaReset";
			 public const string PeriodicidadeReset = "PeriodicidadeReset";
			 public const string QtdePeriodicidade = "QtdePeriodicidade";
			 public const string DataInicioReset = "DataInicioReset";
			 public const string IdConta = "IdConta";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OperacaoSwapMetadata))
			{
				if(OperacaoSwapMetadata.mapDelegates == null)
				{
					OperacaoSwapMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OperacaoSwapMetadata.meta == null)
				{
					OperacaoSwapMetadata.meta = new OperacaoSwapMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAssessor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoRegistro", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("NumeroContrato", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataEmissao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorBase", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoPonta", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TaxaJuros", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Percentual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoApropriacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ContagemDias", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("BaseAno", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TipoPontaContraParte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdIndiceContraParte", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TaxaJurosContraParte", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PercentualContraParte", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoApropriacaoContraParte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ContagemDiasContraParte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("BaseAnoContraParte", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ComGarantia", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DiasUteis", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DiasCorridos", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Corretagem", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Emolumento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Registro", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIndicePartida", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIndicePartidaContraParte", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdEstrategia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataRegistro", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CodigoIsin", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CpfcnpjContraParte", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("UserTimeStamp", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CdAtivoBolsaContraParte", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAtivoCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAtivoCarteiraContraParte", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RealizaReset", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("PeriodicidadeReset", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QtdePeriodicidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataInicioReset", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCategoriaMovimentacao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "OperacaoSwap";
				meta.Destination = "OperacaoSwap";
				
				meta.spInsert = "proc_OperacaoSwapInsert";				
				meta.spUpdate = "proc_OperacaoSwapUpdate";		
				meta.spDelete = "proc_OperacaoSwapDelete";
				meta.spLoadAll = "proc_OperacaoSwapLoadAll";
				meta.spLoadByPrimaryKey = "proc_OperacaoSwapLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OperacaoSwapMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
