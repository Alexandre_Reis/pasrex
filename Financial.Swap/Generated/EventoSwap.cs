/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 09/11/2015 16:09:49
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Swap
{

	[Serializable]
	abstract public class esEventoSwapCollection : esEntityCollection
	{
		public esEventoSwapCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EventoSwapCollection";
		}

		#region Query Logic
		protected void InitQuery(esEventoSwapQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEventoSwapQuery);
		}
		#endregion
		
		virtual public EventoSwap DetachEntity(EventoSwap entity)
		{
			return base.DetachEntity(entity) as EventoSwap;
		}
		
		virtual public EventoSwap AttachEntity(EventoSwap entity)
		{
			return base.AttachEntity(entity) as EventoSwap;
		}
		
		virtual public void Combine(EventoSwapCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EventoSwap this[int index]
		{
			get
			{
				return base[index] as EventoSwap;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EventoSwap);
		}
	}



	[Serializable]
	abstract public class esEventoSwap : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEventoSwapQuery GetDynamicQuery()
		{
			return null;
		}

		public esEventoSwap()
		{

		}

		public esEventoSwap(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEventoSwap)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEventoSwap);
			else
				return LoadByPrimaryKeyStoredProcedure(idEventoSwap);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEventoSwap)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEventoSwap);
			else
				return LoadByPrimaryKeyStoredProcedure(idEventoSwap);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEventoSwap)
		{
			esEventoSwapQuery query = this.GetDynamicQuery();
			query.Where(query.IdEventoSwap == idEventoSwap);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEventoSwap)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEventoSwap",idEventoSwap);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEventoSwap": this.str.IdEventoSwap = (string)value; break;							
						case "TipoEvento": this.str.TipoEvento = (string)value; break;							
						case "DataVigencia": this.str.DataVigencia = (string)value; break;							
						case "DataEvento": this.str.DataEvento = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "PontaSwap": this.str.PontaSwap = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEventoSwap":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoSwap = (System.Int32?)value;
							break;
						
						case "TipoEvento":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.TipoEvento = (System.Int16?)value;
							break;
						
						case "DataVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVigencia = (System.DateTime?)value;
							break;
						
						case "DataEvento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEvento = (System.DateTime?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "PontaSwap":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.PontaSwap = (System.Int16?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EventoSwap.IdEventoSwap
		/// </summary>
		virtual public System.Int32? IdEventoSwap
		{
			get
			{
				return base.GetSystemInt32(EventoSwapMetadata.ColumnNames.IdEventoSwap);
			}
			
			set
			{
				base.SetSystemInt32(EventoSwapMetadata.ColumnNames.IdEventoSwap, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoSwap.TipoEvento
		/// </summary>
		virtual public System.Int16? TipoEvento
		{
			get
			{
				return base.GetSystemInt16(EventoSwapMetadata.ColumnNames.TipoEvento);
			}
			
			set
			{
				base.SetSystemInt16(EventoSwapMetadata.ColumnNames.TipoEvento, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoSwap.DataVigencia
		/// </summary>
		virtual public System.DateTime? DataVigencia
		{
			get
			{
				return base.GetSystemDateTime(EventoSwapMetadata.ColumnNames.DataVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(EventoSwapMetadata.ColumnNames.DataVigencia, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoSwap.DataEvento
		/// </summary>
		virtual public System.DateTime? DataEvento
		{
			get
			{
				return base.GetSystemDateTime(EventoSwapMetadata.ColumnNames.DataEvento);
			}
			
			set
			{
				base.SetSystemDateTime(EventoSwapMetadata.ColumnNames.DataEvento, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoSwap.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(EventoSwapMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoSwapMetadata.ColumnNames.IdOperacao, value))
				{
					this._UpToOperacaoSwapByIdOperacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoSwap.PontaSwap
		/// </summary>
		virtual public System.Int16? PontaSwap
		{
			get
			{
				return base.GetSystemInt16(EventoSwapMetadata.ColumnNames.PontaSwap);
			}
			
			set
			{
				base.SetSystemInt16(EventoSwapMetadata.ColumnNames.PontaSwap, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoSwap.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(EventoSwapMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(EventoSwapMetadata.ColumnNames.Valor, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected OperacaoSwap _UpToOperacaoSwapByIdOperacao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEventoSwap entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEventoSwap
			{
				get
				{
					System.Int32? data = entity.IdEventoSwap;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoSwap = null;
					else entity.IdEventoSwap = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoEvento
			{
				get
				{
					System.Int16? data = entity.TipoEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoEvento = null;
					else entity.TipoEvento = Convert.ToInt16(value);
				}
			}
				
			public System.String DataVigencia
			{
				get
				{
					System.DateTime? data = entity.DataVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVigencia = null;
					else entity.DataVigencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataEvento
			{
				get
				{
					System.DateTime? data = entity.DataEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEvento = null;
					else entity.DataEvento = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String PontaSwap
			{
				get
				{
					System.Int16? data = entity.PontaSwap;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PontaSwap = null;
					else entity.PontaSwap = Convert.ToInt16(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
			

			private esEventoSwap entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEventoSwapQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEventoSwap can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EventoSwap : esEventoSwap
	{

				
		#region UpToOperacaoSwapByIdOperacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EventoSwap_OpSwap_FK
		/// </summary>

		[XmlIgnore]
		public OperacaoSwap UpToOperacaoSwapByIdOperacao
		{
			get
			{
				if(this._UpToOperacaoSwapByIdOperacao == null
					&& IdOperacao != null					)
				{
					this._UpToOperacaoSwapByIdOperacao = new OperacaoSwap();
					this._UpToOperacaoSwapByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOperacaoSwapByIdOperacao", this._UpToOperacaoSwapByIdOperacao);
					this._UpToOperacaoSwapByIdOperacao.Query.Where(this._UpToOperacaoSwapByIdOperacao.Query.IdOperacao == this.IdOperacao);
					this._UpToOperacaoSwapByIdOperacao.Query.Load();
				}

				return this._UpToOperacaoSwapByIdOperacao;
			}
			
			set
			{
				this.RemovePreSave("UpToOperacaoSwapByIdOperacao");
				

				if(value == null)
				{
					this.IdOperacao = null;
					this._UpToOperacaoSwapByIdOperacao = null;
				}
				else
				{
					this.IdOperacao = value.IdOperacao;
					this._UpToOperacaoSwapByIdOperacao = value;
					this.SetPreSave("UpToOperacaoSwapByIdOperacao", this._UpToOperacaoSwapByIdOperacao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToOperacaoSwapByIdOperacao != null)
			{
				this.IdOperacao = this._UpToOperacaoSwapByIdOperacao.IdOperacao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEventoSwapQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EventoSwapMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEventoSwap
		{
			get
			{
				return new esQueryItem(this, EventoSwapMetadata.ColumnNames.IdEventoSwap, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoEvento
		{
			get
			{
				return new esQueryItem(this, EventoSwapMetadata.ColumnNames.TipoEvento, esSystemType.Int16);
			}
		} 
		
		public esQueryItem DataVigencia
		{
			get
			{
				return new esQueryItem(this, EventoSwapMetadata.ColumnNames.DataVigencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataEvento
		{
			get
			{
				return new esQueryItem(this, EventoSwapMetadata.ColumnNames.DataEvento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, EventoSwapMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PontaSwap
		{
			get
			{
				return new esQueryItem(this, EventoSwapMetadata.ColumnNames.PontaSwap, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, EventoSwapMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EventoSwapCollection")]
	public partial class EventoSwapCollection : esEventoSwapCollection, IEnumerable<EventoSwap>
	{
		public EventoSwapCollection()
		{

		}
		
		public static implicit operator List<EventoSwap>(EventoSwapCollection coll)
		{
			List<EventoSwap> list = new List<EventoSwap>();
			
			foreach (EventoSwap emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EventoSwapMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoSwapQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EventoSwap(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EventoSwap();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EventoSwapQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoSwapQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EventoSwapQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EventoSwap AddNew()
		{
			EventoSwap entity = base.AddNewEntity() as EventoSwap;
			
			return entity;
		}

		public EventoSwap FindByPrimaryKey(System.Int32 idEventoSwap)
		{
			return base.FindByPrimaryKey(idEventoSwap) as EventoSwap;
		}


		#region IEnumerable<EventoSwap> Members

		IEnumerator<EventoSwap> IEnumerable<EventoSwap>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EventoSwap;
			}
		}

		#endregion
		
		private EventoSwapQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EventoSwap' table
	/// </summary>

	[Serializable]
	public partial class EventoSwap : esEventoSwap
	{
		public EventoSwap()
		{

		}
	
		public EventoSwap(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EventoSwapMetadata.Meta();
			}
		}
		
		
		
		override protected esEventoSwapQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoSwapQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EventoSwapQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoSwapQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EventoSwapQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EventoSwapQuery query;
	}



	[Serializable]
	public partial class EventoSwapQuery : esEventoSwapQuery
	{
		public EventoSwapQuery()
		{

		}		
		
		public EventoSwapQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EventoSwapMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EventoSwapMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EventoSwapMetadata.ColumnNames.IdEventoSwap, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoSwapMetadata.PropertyNames.IdEventoSwap;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoSwapMetadata.ColumnNames.TipoEvento, 1, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = EventoSwapMetadata.PropertyNames.TipoEvento;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoSwapMetadata.ColumnNames.DataVigencia, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EventoSwapMetadata.PropertyNames.DataVigencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoSwapMetadata.ColumnNames.DataEvento, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EventoSwapMetadata.PropertyNames.DataEvento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoSwapMetadata.ColumnNames.IdOperacao, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoSwapMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoSwapMetadata.ColumnNames.PontaSwap, 5, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = EventoSwapMetadata.PropertyNames.PontaSwap;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoSwapMetadata.ColumnNames.Valor, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EventoSwapMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 10;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EventoSwapMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEventoSwap = "IdEventoSwap";
			 public const string TipoEvento = "TipoEvento";
			 public const string DataVigencia = "DataVigencia";
			 public const string DataEvento = "DataEvento";
			 public const string IdOperacao = "IdOperacao";
			 public const string PontaSwap = "PontaSwap";
			 public const string Valor = "Valor";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEventoSwap = "IdEventoSwap";
			 public const string TipoEvento = "TipoEvento";
			 public const string DataVigencia = "DataVigencia";
			 public const string DataEvento = "DataEvento";
			 public const string IdOperacao = "IdOperacao";
			 public const string PontaSwap = "PontaSwap";
			 public const string Valor = "Valor";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EventoSwapMetadata))
			{
				if(EventoSwapMetadata.mapDelegates == null)
				{
					EventoSwapMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EventoSwapMetadata.meta == null)
				{
					EventoSwapMetadata.meta = new EventoSwapMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEventoSwap", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoEvento", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("DataVigencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataEvento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PontaSwap", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "EventoSwap";
				meta.Destination = "EventoSwap";
				
				meta.spInsert = "proc_EventoSwapInsert";				
				meta.spUpdate = "proc_EventoSwapUpdate";		
				meta.spDelete = "proc_EventoSwapDelete";
				meta.spLoadAll = "proc_EventoSwapLoadAll";
				meta.spLoadByPrimaryKey = "proc_EventoSwapLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EventoSwapMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
