/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 09/11/2015 14:52:12
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	









		
using Financial.Common;
using Financial.Investidor;

		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Swap
{

	[Serializable]
	abstract public class esPosicaoSwapAberturaCollection : esEntityCollection
	{
		public esPosicaoSwapAberturaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoSwapAberturaCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoSwapAberturaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoSwapAberturaQuery);
		}
		#endregion
		
		virtual public PosicaoSwapAbertura DetachEntity(PosicaoSwapAbertura entity)
		{
			return base.DetachEntity(entity) as PosicaoSwapAbertura;
		}
		
		virtual public PosicaoSwapAbertura AttachEntity(PosicaoSwapAbertura entity)
		{
			return base.AttachEntity(entity) as PosicaoSwapAbertura;
		}
		
		virtual public void Combine(PosicaoSwapAberturaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoSwapAbertura this[int index]
		{
			get
			{
				return base[index] as PosicaoSwapAbertura;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoSwapAbertura);
		}
	}



	[Serializable]
	abstract public class esPosicaoSwapAbertura : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoSwapAberturaQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoSwapAbertura()
		{

		}

		public esPosicaoSwapAbertura(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPosicaoSwapAberturaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esPosicaoSwapAberturaQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);			parms.Add("DataHistorico",dataHistorico);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAssessor": this.str.IdAssessor = (string)value; break;							
						case "TipoRegistro": this.str.TipoRegistro = (string)value; break;							
						case "NumeroContrato": this.str.NumeroContrato = (string)value; break;							
						case "DataEmissao": this.str.DataEmissao = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "ValorBase": this.str.ValorBase = (string)value; break;							
						case "TipoPonta": this.str.TipoPonta = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;							
						case "TaxaJuros": this.str.TaxaJuros = (string)value; break;							
						case "Percentual": this.str.Percentual = (string)value; break;							
						case "TipoApropriacao": this.str.TipoApropriacao = (string)value; break;							
						case "ContagemDias": this.str.ContagemDias = (string)value; break;							
						case "BaseAno": this.str.BaseAno = (string)value; break;							
						case "TipoPontaContraParte": this.str.TipoPontaContraParte = (string)value; break;							
						case "IdIndiceContraParte": this.str.IdIndiceContraParte = (string)value; break;							
						case "TaxaJurosContraParte": this.str.TaxaJurosContraParte = (string)value; break;							
						case "PercentualContraParte": this.str.PercentualContraParte = (string)value; break;							
						case "TipoApropriacaoContraParte": this.str.TipoApropriacaoContraParte = (string)value; break;							
						case "ContagemDiasContraParte": this.str.ContagemDiasContraParte = (string)value; break;							
						case "BaseAnoContraParte": this.str.BaseAnoContraParte = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "ComGarantia": this.str.ComGarantia = (string)value; break;							
						case "DiasUteis": this.str.DiasUteis = (string)value; break;							
						case "DiasCorridos": this.str.DiasCorridos = (string)value; break;							
						case "ValorParte": this.str.ValorParte = (string)value; break;							
						case "ValorContraParte": this.str.ValorContraParte = (string)value; break;							
						case "Saldo": this.str.Saldo = (string)value; break;							
						case "ValorIndicePartida": this.str.ValorIndicePartida = (string)value; break;							
						case "ValorIndicePartidaContraParte": this.str.ValorIndicePartidaContraParte = (string)value; break;							
						case "IdEstrategia": this.str.IdEstrategia = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "CdAtivoBolsaContraParte": this.str.CdAtivoBolsaContraParte = (string)value; break;							
						case "IdAtivoCarteira": this.str.IdAtivoCarteira = (string)value; break;							
						case "IdAtivoCarteiraContraParte": this.str.IdAtivoCarteiraContraParte = (string)value; break;							
						case "DataUltimoEvento": this.str.DataUltimoEvento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAssessor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAssessor = (System.Int32?)value;
							break;
						
						case "TipoRegistro":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoRegistro = (System.Byte?)value;
							break;
						
						case "DataEmissao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEmissao = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "ValorBase":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBase = (System.Decimal?)value;
							break;
						
						case "TipoPonta":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoPonta = (System.Byte?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
						
						case "TaxaJuros":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaJuros = (System.Decimal?)value;
							break;
						
						case "Percentual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Percentual = (System.Decimal?)value;
							break;
						
						case "TipoApropriacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoApropriacao = (System.Byte?)value;
							break;
						
						case "ContagemDias":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContagemDias = (System.Byte?)value;
							break;
						
						case "BaseAno":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.BaseAno = (System.Int16?)value;
							break;
						
						case "TipoPontaContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoPontaContraParte = (System.Byte?)value;
							break;
						
						case "IdIndiceContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndiceContraParte = (System.Int16?)value;
							break;
						
						case "TaxaJurosContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaJurosContraParte = (System.Decimal?)value;
							break;
						
						case "PercentualContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualContraParte = (System.Decimal?)value;
							break;
						
						case "TipoApropriacaoContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoApropriacaoContraParte = (System.Byte?)value;
							break;
						
						case "ContagemDiasContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContagemDiasContraParte = (System.Byte?)value;
							break;
						
						case "BaseAnoContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.BaseAnoContraParte = (System.Int16?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "DiasUteis":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DiasUteis = (System.Int32?)value;
							break;
						
						case "DiasCorridos":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DiasCorridos = (System.Int32?)value;
							break;
						
						case "ValorParte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorParte = (System.Decimal?)value;
							break;
						
						case "ValorContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorContraParte = (System.Decimal?)value;
							break;
						
						case "Saldo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Saldo = (System.Decimal?)value;
							break;
						
						case "ValorIndicePartida":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIndicePartida = (System.Decimal?)value;
							break;
						
						case "ValorIndicePartidaContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIndicePartidaContraParte = (System.Decimal?)value;
							break;
						
						case "IdEstrategia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEstrategia = (System.Int32?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "IdAtivoCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAtivoCarteira = (System.Int32?)value;
							break;
						
						case "IdAtivoCarteiraContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAtivoCarteiraContraParte = (System.Int32?)value;
							break;
						
						case "DataUltimoEvento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimoEvento = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(PosicaoSwapAberturaMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoSwapAberturaMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.IdAssessor
		/// </summary>
		virtual public System.Int32? IdAssessor
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdAssessor);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdAssessor, value))
				{
					this._UpToAssessorByIdAssessor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.TipoRegistro
		/// </summary>
		virtual public System.Byte? TipoRegistro
		{
			get
			{
				return base.GetSystemByte(PosicaoSwapAberturaMetadata.ColumnNames.TipoRegistro);
			}
			
			set
			{
				base.SetSystemByte(PosicaoSwapAberturaMetadata.ColumnNames.TipoRegistro, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.NumeroContrato
		/// </summary>
		virtual public System.String NumeroContrato
		{
			get
			{
				return base.GetSystemString(PosicaoSwapAberturaMetadata.ColumnNames.NumeroContrato);
			}
			
			set
			{
				base.SetSystemString(PosicaoSwapAberturaMetadata.ColumnNames.NumeroContrato, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.DataEmissao
		/// </summary>
		virtual public System.DateTime? DataEmissao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoSwapAberturaMetadata.ColumnNames.DataEmissao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoSwapAberturaMetadata.ColumnNames.DataEmissao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(PosicaoSwapAberturaMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoSwapAberturaMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.ValorBase
		/// </summary>
		virtual public System.Decimal? ValorBase
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.ValorBase);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.ValorBase, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.TipoPonta
		/// </summary>
		virtual public System.Byte? TipoPonta
		{
			get
			{
				return base.GetSystemByte(PosicaoSwapAberturaMetadata.ColumnNames.TipoPonta);
			}
			
			set
			{
				base.SetSystemByte(PosicaoSwapAberturaMetadata.ColumnNames.TipoPonta, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(PosicaoSwapAberturaMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				if(base.SetSystemInt16(PosicaoSwapAberturaMetadata.ColumnNames.IdIndice, value))
				{
					this._UpToIndiceByIdIndice = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.TaxaJuros
		/// </summary>
		virtual public System.Decimal? TaxaJuros
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.TaxaJuros);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.TaxaJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.Percentual
		/// </summary>
		virtual public System.Decimal? Percentual
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.Percentual);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.Percentual, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.TipoApropriacao
		/// </summary>
		virtual public System.Byte? TipoApropriacao
		{
			get
			{
				return base.GetSystemByte(PosicaoSwapAberturaMetadata.ColumnNames.TipoApropriacao);
			}
			
			set
			{
				base.SetSystemByte(PosicaoSwapAberturaMetadata.ColumnNames.TipoApropriacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.ContagemDias
		/// </summary>
		virtual public System.Byte? ContagemDias
		{
			get
			{
				return base.GetSystemByte(PosicaoSwapAberturaMetadata.ColumnNames.ContagemDias);
			}
			
			set
			{
				base.SetSystemByte(PosicaoSwapAberturaMetadata.ColumnNames.ContagemDias, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.BaseAno
		/// </summary>
		virtual public System.Int16? BaseAno
		{
			get
			{
				return base.GetSystemInt16(PosicaoSwapAberturaMetadata.ColumnNames.BaseAno);
			}
			
			set
			{
				base.SetSystemInt16(PosicaoSwapAberturaMetadata.ColumnNames.BaseAno, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.TipoPontaContraParte
		/// </summary>
		virtual public System.Byte? TipoPontaContraParte
		{
			get
			{
				return base.GetSystemByte(PosicaoSwapAberturaMetadata.ColumnNames.TipoPontaContraParte);
			}
			
			set
			{
				base.SetSystemByte(PosicaoSwapAberturaMetadata.ColumnNames.TipoPontaContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.IdIndiceContraParte
		/// </summary>
		virtual public System.Int16? IdIndiceContraParte
		{
			get
			{
				return base.GetSystemInt16(PosicaoSwapAberturaMetadata.ColumnNames.IdIndiceContraParte);
			}
			
			set
			{
				if(base.SetSystemInt16(PosicaoSwapAberturaMetadata.ColumnNames.IdIndiceContraParte, value))
				{
					this._UpToIndiceByIdIndiceContraParte = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.TaxaJurosContraParte
		/// </summary>
		virtual public System.Decimal? TaxaJurosContraParte
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.TaxaJurosContraParte);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.TaxaJurosContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.PercentualContraParte
		/// </summary>
		virtual public System.Decimal? PercentualContraParte
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.PercentualContraParte);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.PercentualContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.TipoApropriacaoContraParte
		/// </summary>
		virtual public System.Byte? TipoApropriacaoContraParte
		{
			get
			{
				return base.GetSystemByte(PosicaoSwapAberturaMetadata.ColumnNames.TipoApropriacaoContraParte);
			}
			
			set
			{
				base.SetSystemByte(PosicaoSwapAberturaMetadata.ColumnNames.TipoApropriacaoContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.ContagemDiasContraParte
		/// </summary>
		virtual public System.Byte? ContagemDiasContraParte
		{
			get
			{
				return base.GetSystemByte(PosicaoSwapAberturaMetadata.ColumnNames.ContagemDiasContraParte);
			}
			
			set
			{
				base.SetSystemByte(PosicaoSwapAberturaMetadata.ColumnNames.ContagemDiasContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.BaseAnoContraParte
		/// </summary>
		virtual public System.Int16? BaseAnoContraParte
		{
			get
			{
				return base.GetSystemInt16(PosicaoSwapAberturaMetadata.ColumnNames.BaseAnoContraParte);
			}
			
			set
			{
				base.SetSystemInt16(PosicaoSwapAberturaMetadata.ColumnNames.BaseAnoContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.ComGarantia
		/// </summary>
		virtual public System.String ComGarantia
		{
			get
			{
				return base.GetSystemString(PosicaoSwapAberturaMetadata.ColumnNames.ComGarantia);
			}
			
			set
			{
				base.SetSystemString(PosicaoSwapAberturaMetadata.ColumnNames.ComGarantia, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.DiasUteis
		/// </summary>
		virtual public System.Int32? DiasUteis
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.DiasUteis);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.DiasUteis, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.DiasCorridos
		/// </summary>
		virtual public System.Int32? DiasCorridos
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.DiasCorridos);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.DiasCorridos, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.ValorParte
		/// </summary>
		virtual public System.Decimal? ValorParte
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.ValorParte);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.ValorParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.ValorContraParte
		/// </summary>
		virtual public System.Decimal? ValorContraParte
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.ValorContraParte);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.ValorContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.Saldo
		/// </summary>
		virtual public System.Decimal? Saldo
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.Saldo);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.Saldo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.ValorIndicePartida
		/// </summary>
		virtual public System.Decimal? ValorIndicePartida
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.ValorIndicePartida);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.ValorIndicePartida, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.ValorIndicePartidaContraParte
		/// </summary>
		virtual public System.Decimal? ValorIndicePartidaContraParte
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.ValorIndicePartidaContraParte);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.ValorIndicePartidaContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.IdEstrategia
		/// </summary>
		virtual public System.Int32? IdEstrategia
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdEstrategia);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdEstrategia, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoSwapAberturaMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(PosicaoSwapAberturaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				base.SetSystemString(PosicaoSwapAberturaMetadata.ColumnNames.CdAtivoBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.CdAtivoBolsaContraParte
		/// </summary>
		virtual public System.String CdAtivoBolsaContraParte
		{
			get
			{
				return base.GetSystemString(PosicaoSwapAberturaMetadata.ColumnNames.CdAtivoBolsaContraParte);
			}
			
			set
			{
				base.SetSystemString(PosicaoSwapAberturaMetadata.ColumnNames.CdAtivoBolsaContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.IdAtivoCarteira
		/// </summary>
		virtual public System.Int32? IdAtivoCarteira
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdAtivoCarteira);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdAtivoCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.IdAtivoCarteiraContraParte
		/// </summary>
		virtual public System.Int32? IdAtivoCarteiraContraParte
		{
			get
			{
				return base.GetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdAtivoCarteiraContraParte);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoSwapAberturaMetadata.ColumnNames.IdAtivoCarteiraContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoSwapAbertura.DataUltimoEvento
		/// </summary>
		virtual public System.DateTime? DataUltimoEvento
		{
			get
			{
				return base.GetSystemDateTime(PosicaoSwapAberturaMetadata.ColumnNames.DataUltimoEvento);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoSwapAberturaMetadata.ColumnNames.DataUltimoEvento, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected Assessor _UpToAssessorByIdAssessor;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndice;
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndiceContraParte;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoSwapAbertura entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAssessor
			{
				get
				{
					System.Int32? data = entity.IdAssessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAssessor = null;
					else entity.IdAssessor = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoRegistro
			{
				get
				{
					System.Byte? data = entity.TipoRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoRegistro = null;
					else entity.TipoRegistro = Convert.ToByte(value);
				}
			}
				
			public System.String NumeroContrato
			{
				get
				{
					System.String data = entity.NumeroContrato;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroContrato = null;
					else entity.NumeroContrato = Convert.ToString(value);
				}
			}
				
			public System.String DataEmissao
			{
				get
				{
					System.DateTime? data = entity.DataEmissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEmissao = null;
					else entity.DataEmissao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorBase
			{
				get
				{
					System.Decimal? data = entity.ValorBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBase = null;
					else entity.ValorBase = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoPonta
			{
				get
				{
					System.Byte? data = entity.TipoPonta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPonta = null;
					else entity.TipoPonta = Convert.ToByte(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String TaxaJuros
			{
				get
				{
					System.Decimal? data = entity.TaxaJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaJuros = null;
					else entity.TaxaJuros = Convert.ToDecimal(value);
				}
			}
				
			public System.String Percentual
			{
				get
				{
					System.Decimal? data = entity.Percentual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Percentual = null;
					else entity.Percentual = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoApropriacao
			{
				get
				{
					System.Byte? data = entity.TipoApropriacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoApropriacao = null;
					else entity.TipoApropriacao = Convert.ToByte(value);
				}
			}
				
			public System.String ContagemDias
			{
				get
				{
					System.Byte? data = entity.ContagemDias;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContagemDias = null;
					else entity.ContagemDias = Convert.ToByte(value);
				}
			}
				
			public System.String BaseAno
			{
				get
				{
					System.Int16? data = entity.BaseAno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseAno = null;
					else entity.BaseAno = Convert.ToInt16(value);
				}
			}
				
			public System.String TipoPontaContraParte
			{
				get
				{
					System.Byte? data = entity.TipoPontaContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPontaContraParte = null;
					else entity.TipoPontaContraParte = Convert.ToByte(value);
				}
			}
				
			public System.String IdIndiceContraParte
			{
				get
				{
					System.Int16? data = entity.IdIndiceContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndiceContraParte = null;
					else entity.IdIndiceContraParte = Convert.ToInt16(value);
				}
			}
				
			public System.String TaxaJurosContraParte
			{
				get
				{
					System.Decimal? data = entity.TaxaJurosContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaJurosContraParte = null;
					else entity.TaxaJurosContraParte = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercentualContraParte
			{
				get
				{
					System.Decimal? data = entity.PercentualContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualContraParte = null;
					else entity.PercentualContraParte = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoApropriacaoContraParte
			{
				get
				{
					System.Byte? data = entity.TipoApropriacaoContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoApropriacaoContraParte = null;
					else entity.TipoApropriacaoContraParte = Convert.ToByte(value);
				}
			}
				
			public System.String ContagemDiasContraParte
			{
				get
				{
					System.Byte? data = entity.ContagemDiasContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContagemDiasContraParte = null;
					else entity.ContagemDiasContraParte = Convert.ToByte(value);
				}
			}
				
			public System.String BaseAnoContraParte
			{
				get
				{
					System.Int16? data = entity.BaseAnoContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseAnoContraParte = null;
					else entity.BaseAnoContraParte = Convert.ToInt16(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String ComGarantia
			{
				get
				{
					System.String data = entity.ComGarantia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ComGarantia = null;
					else entity.ComGarantia = Convert.ToString(value);
				}
			}
				
			public System.String DiasUteis
			{
				get
				{
					System.Int32? data = entity.DiasUteis;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasUteis = null;
					else entity.DiasUteis = Convert.ToInt32(value);
				}
			}
				
			public System.String DiasCorridos
			{
				get
				{
					System.Int32? data = entity.DiasCorridos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasCorridos = null;
					else entity.DiasCorridos = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorParte
			{
				get
				{
					System.Decimal? data = entity.ValorParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorParte = null;
					else entity.ValorParte = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorContraParte
			{
				get
				{
					System.Decimal? data = entity.ValorContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorContraParte = null;
					else entity.ValorContraParte = Convert.ToDecimal(value);
				}
			}
				
			public System.String Saldo
			{
				get
				{
					System.Decimal? data = entity.Saldo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Saldo = null;
					else entity.Saldo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIndicePartida
			{
				get
				{
					System.Decimal? data = entity.ValorIndicePartida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIndicePartida = null;
					else entity.ValorIndicePartida = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIndicePartidaContraParte
			{
				get
				{
					System.Decimal? data = entity.ValorIndicePartidaContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIndicePartidaContraParte = null;
					else entity.ValorIndicePartidaContraParte = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdEstrategia
			{
				get
				{
					System.Int32? data = entity.IdEstrategia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEstrategia = null;
					else entity.IdEstrategia = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String CdAtivoBolsaContraParte
			{
				get
				{
					System.String data = entity.CdAtivoBolsaContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsaContraParte = null;
					else entity.CdAtivoBolsaContraParte = Convert.ToString(value);
				}
			}
				
			public System.String IdAtivoCarteira
			{
				get
				{
					System.Int32? data = entity.IdAtivoCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAtivoCarteira = null;
					else entity.IdAtivoCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAtivoCarteiraContraParte
			{
				get
				{
					System.Int32? data = entity.IdAtivoCarteiraContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAtivoCarteiraContraParte = null;
					else entity.IdAtivoCarteiraContraParte = Convert.ToInt32(value);
				}
			}
				
			public System.String DataUltimoEvento
			{
				get
				{
					System.DateTime? data = entity.DataUltimoEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimoEvento = null;
					else entity.DataUltimoEvento = Convert.ToDateTime(value);
				}
			}
			

			private esPosicaoSwapAbertura entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoSwapAberturaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoSwapAbertura can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoSwapAbertura : esPosicaoSwapAbertura
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_PosicaoSwapAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAssessorByIdAssessor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Assessor_PosicaoSwapAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public Assessor UpToAssessorByIdAssessor
		{
			get
			{
				if(this._UpToAssessorByIdAssessor == null
					&& IdAssessor != null					)
				{
					this._UpToAssessorByIdAssessor = new Assessor();
					this._UpToAssessorByIdAssessor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAssessorByIdAssessor", this._UpToAssessorByIdAssessor);
					this._UpToAssessorByIdAssessor.Query.Where(this._UpToAssessorByIdAssessor.Query.IdAssessor == this.IdAssessor);
					this._UpToAssessorByIdAssessor.Query.Load();
				}

				return this._UpToAssessorByIdAssessor;
			}
			
			set
			{
				this.RemovePreSave("UpToAssessorByIdAssessor");
				

				if(value == null)
				{
					this.IdAssessor = null;
					this._UpToAssessorByIdAssessor = null;
				}
				else
				{
					this.IdAssessor = value.IdAssessor;
					this._UpToAssessorByIdAssessor = value;
					this.SetPreSave("UpToAssessorByIdAssessor", this._UpToAssessorByIdAssessor);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PosicaoSwapAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToIndiceByIdIndice - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Indice_PosicaoSwapAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndice
		{
			get
			{
				if(this._UpToIndiceByIdIndice == null
					&& IdIndice != null					)
				{
					this._UpToIndiceByIdIndice = new Indice();
					this._UpToIndiceByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
					this._UpToIndiceByIdIndice.Query.Where(this._UpToIndiceByIdIndice.Query.IdIndice == this.IdIndice);
					this._UpToIndiceByIdIndice.Query.Load();
				}

				return this._UpToIndiceByIdIndice;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndice");
				

				if(value == null)
				{
					this.IdIndice = null;
					this._UpToIndiceByIdIndice = null;
				}
				else
				{
					this.IdIndice = value.IdIndice;
					this._UpToIndiceByIdIndice = value;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
				}
				
			}
		}
		#endregion
		

				
		#region UpToIndiceByIdIndiceContraParte - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Indice_PosicaoSwapAbertura_FK2
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndiceContraParte
		{
			get
			{
				if(this._UpToIndiceByIdIndiceContraParte == null
					&& IdIndiceContraParte != null					)
				{
					this._UpToIndiceByIdIndiceContraParte = new Indice();
					this._UpToIndiceByIdIndiceContraParte.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndiceContraParte", this._UpToIndiceByIdIndiceContraParte);
					this._UpToIndiceByIdIndiceContraParte.Query.Where(this._UpToIndiceByIdIndiceContraParte.Query.IdIndice == this.IdIndiceContraParte);
					this._UpToIndiceByIdIndiceContraParte.Query.Load();
				}

				return this._UpToIndiceByIdIndiceContraParte;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndiceContraParte");
				

				if(value == null)
				{
					this.IdIndiceContraParte = null;
					this._UpToIndiceByIdIndiceContraParte = null;
				}
				else
				{
					this.IdIndiceContraParte = value.IdIndice;
					this._UpToIndiceByIdIndiceContraParte = value;
					this.SetPreSave("UpToIndiceByIdIndiceContraParte", this._UpToIndiceByIdIndiceContraParte);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToAssessorByIdAssessor != null)
			{
				this.IdAssessor = this._UpToAssessorByIdAssessor.IdAssessor;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoSwapAberturaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoSwapAberturaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAssessor
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.IdAssessor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoRegistro
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.TipoRegistro, esSystemType.Byte);
			}
		} 
		
		public esQueryItem NumeroContrato
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.NumeroContrato, esSystemType.String);
			}
		} 
		
		public esQueryItem DataEmissao
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.DataEmissao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorBase
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.ValorBase, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoPonta
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.TipoPonta, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TaxaJuros
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.TaxaJuros, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Percentual
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.Percentual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoApropriacao
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.TipoApropriacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ContagemDias
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.ContagemDias, esSystemType.Byte);
			}
		} 
		
		public esQueryItem BaseAno
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.BaseAno, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TipoPontaContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.TipoPontaContraParte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdIndiceContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.IdIndiceContraParte, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TaxaJurosContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.TaxaJurosContraParte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercentualContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.PercentualContraParte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoApropriacaoContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.TipoApropriacaoContraParte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ContagemDiasContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.ContagemDiasContraParte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem BaseAnoContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.BaseAnoContraParte, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ComGarantia
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.ComGarantia, esSystemType.String);
			}
		} 
		
		public esQueryItem DiasUteis
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.DiasUteis, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DiasCorridos
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.DiasCorridos, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.ValorParte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.ValorContraParte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Saldo
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.Saldo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIndicePartida
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.ValorIndicePartida, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIndicePartidaContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.ValorIndicePartidaContraParte, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdEstrategia
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.IdEstrategia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem CdAtivoBolsaContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.CdAtivoBolsaContraParte, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAtivoCarteira
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.IdAtivoCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAtivoCarteiraContraParte
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.IdAtivoCarteiraContraParte, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataUltimoEvento
		{
			get
			{
				return new esQueryItem(this, PosicaoSwapAberturaMetadata.ColumnNames.DataUltimoEvento, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PosicaoSwapAberturaCollection")]
	public partial class PosicaoSwapAberturaCollection : esPosicaoSwapAberturaCollection, IEnumerable<PosicaoSwapAbertura>
	{
		public PosicaoSwapAberturaCollection()
		{

		}
		
		public static implicit operator List<PosicaoSwapAbertura>(PosicaoSwapAberturaCollection coll)
		{
			List<PosicaoSwapAbertura> list = new List<PosicaoSwapAbertura>();
			
			foreach (PosicaoSwapAbertura emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoSwapAberturaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoSwapAberturaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoSwapAbertura(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoSwapAbertura();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoSwapAberturaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoSwapAberturaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoSwapAberturaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoSwapAbertura AddNew()
		{
			PosicaoSwapAbertura entity = base.AddNewEntity() as PosicaoSwapAbertura;
			
			return entity;
		}

		public PosicaoSwapAbertura FindByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			return base.FindByPrimaryKey(idPosicao, dataHistorico) as PosicaoSwapAbertura;
		}


		#region IEnumerable<PosicaoSwapAbertura> Members

		IEnumerator<PosicaoSwapAbertura> IEnumerable<PosicaoSwapAbertura>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoSwapAbertura;
			}
		}

		#endregion
		
		private PosicaoSwapAberturaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoSwapAbertura' table
	/// </summary>

	[Serializable]
	public partial class PosicaoSwapAbertura : esPosicaoSwapAbertura
	{
		public PosicaoSwapAbertura()
		{

		}
	
		public PosicaoSwapAbertura(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoSwapAberturaMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoSwapAberturaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoSwapAberturaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoSwapAberturaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoSwapAberturaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoSwapAberturaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoSwapAberturaQuery query;
	}



	[Serializable]
	public partial class PosicaoSwapAberturaQuery : esPosicaoSwapAberturaQuery
	{
		public PosicaoSwapAberturaQuery()
		{

		}		
		
		public PosicaoSwapAberturaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoSwapAberturaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoSwapAberturaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.DataHistorico, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.IdOperacao, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.IdCliente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.IdAssessor, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.IdAssessor;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.TipoRegistro, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.TipoRegistro;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.NumeroContrato, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.NumeroContrato;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.DataEmissao, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.DataEmissao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.DataVencimento, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.ValorBase, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.ValorBase;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.TipoPonta, 10, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.TipoPonta;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.IdIndice, 11, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.TaxaJuros, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.TaxaJuros;	
			c.NumericPrecision = 10;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.Percentual, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.Percentual;	
			c.NumericPrecision = 10;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.TipoApropriacao, 14, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.TipoApropriacao;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.ContagemDias, 15, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.ContagemDias;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.BaseAno, 16, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.BaseAno;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.TipoPontaContraParte, 17, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.TipoPontaContraParte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.IdIndiceContraParte, 18, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.IdIndiceContraParte;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.TaxaJurosContraParte, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.TaxaJurosContraParte;	
			c.NumericPrecision = 10;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.PercentualContraParte, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.PercentualContraParte;	
			c.NumericPrecision = 10;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.TipoApropriacaoContraParte, 21, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.TipoApropriacaoContraParte;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.ContagemDiasContraParte, 22, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.ContagemDiasContraParte;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.BaseAnoContraParte, 23, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.BaseAnoContraParte;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.IdAgente, 24, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.ComGarantia, 25, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.ComGarantia;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.DiasUteis, 26, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.DiasUteis;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.DiasCorridos, 27, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.DiasCorridos;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.ValorParte, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.ValorParte;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.ValorContraParte, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.ValorContraParte;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.Saldo, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.Saldo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.ValorIndicePartida, 31, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.ValorIndicePartida;	
			c.NumericPrecision = 20;
			c.NumericScale = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.ValorIndicePartidaContraParte, 32, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.ValorIndicePartidaContraParte;	
			c.NumericPrecision = 20;
			c.NumericScale = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.IdEstrategia, 33, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.IdEstrategia;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.ValorIR, 34, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.CdAtivoBolsa, 35, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.CdAtivoBolsaContraParte, 36, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.CdAtivoBolsaContraParte;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.IdAtivoCarteira, 37, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.IdAtivoCarteira;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.IdAtivoCarteiraContraParte, 38, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.IdAtivoCarteiraContraParte;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoSwapAberturaMetadata.ColumnNames.DataUltimoEvento, 39, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoSwapAberturaMetadata.PropertyNames.DataUltimoEvento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PosicaoSwapAberturaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdAssessor = "IdAssessor";
			 public const string TipoRegistro = "TipoRegistro";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string DataEmissao = "DataEmissao";
			 public const string DataVencimento = "DataVencimento";
			 public const string ValorBase = "ValorBase";
			 public const string TipoPonta = "TipoPonta";
			 public const string IdIndice = "IdIndice";
			 public const string TaxaJuros = "TaxaJuros";
			 public const string Percentual = "Percentual";
			 public const string TipoApropriacao = "TipoApropriacao";
			 public const string ContagemDias = "ContagemDias";
			 public const string BaseAno = "BaseAno";
			 public const string TipoPontaContraParte = "TipoPontaContraParte";
			 public const string IdIndiceContraParte = "IdIndiceContraParte";
			 public const string TaxaJurosContraParte = "TaxaJurosContraParte";
			 public const string PercentualContraParte = "PercentualContraParte";
			 public const string TipoApropriacaoContraParte = "TipoApropriacaoContraParte";
			 public const string ContagemDiasContraParte = "ContagemDiasContraParte";
			 public const string BaseAnoContraParte = "BaseAnoContraParte";
			 public const string IdAgente = "IdAgente";
			 public const string ComGarantia = "ComGarantia";
			 public const string DiasUteis = "DiasUteis";
			 public const string DiasCorridos = "DiasCorridos";
			 public const string ValorParte = "ValorParte";
			 public const string ValorContraParte = "ValorContraParte";
			 public const string Saldo = "Saldo";
			 public const string ValorIndicePartida = "ValorIndicePartida";
			 public const string ValorIndicePartidaContraParte = "ValorIndicePartidaContraParte";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string ValorIR = "ValorIR";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string CdAtivoBolsaContraParte = "CdAtivoBolsaContraParte";
			 public const string IdAtivoCarteira = "IdAtivoCarteira";
			 public const string IdAtivoCarteiraContraParte = "IdAtivoCarteiraContraParte";
			 public const string DataUltimoEvento = "DataUltimoEvento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdAssessor = "IdAssessor";
			 public const string TipoRegistro = "TipoRegistro";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string DataEmissao = "DataEmissao";
			 public const string DataVencimento = "DataVencimento";
			 public const string ValorBase = "ValorBase";
			 public const string TipoPonta = "TipoPonta";
			 public const string IdIndice = "IdIndice";
			 public const string TaxaJuros = "TaxaJuros";
			 public const string Percentual = "Percentual";
			 public const string TipoApropriacao = "TipoApropriacao";
			 public const string ContagemDias = "ContagemDias";
			 public const string BaseAno = "BaseAno";
			 public const string TipoPontaContraParte = "TipoPontaContraParte";
			 public const string IdIndiceContraParte = "IdIndiceContraParte";
			 public const string TaxaJurosContraParte = "TaxaJurosContraParte";
			 public const string PercentualContraParte = "PercentualContraParte";
			 public const string TipoApropriacaoContraParte = "TipoApropriacaoContraParte";
			 public const string ContagemDiasContraParte = "ContagemDiasContraParte";
			 public const string BaseAnoContraParte = "BaseAnoContraParte";
			 public const string IdAgente = "IdAgente";
			 public const string ComGarantia = "ComGarantia";
			 public const string DiasUteis = "DiasUteis";
			 public const string DiasCorridos = "DiasCorridos";
			 public const string ValorParte = "ValorParte";
			 public const string ValorContraParte = "ValorContraParte";
			 public const string Saldo = "Saldo";
			 public const string ValorIndicePartida = "ValorIndicePartida";
			 public const string ValorIndicePartidaContraParte = "ValorIndicePartidaContraParte";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string ValorIR = "ValorIR";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string CdAtivoBolsaContraParte = "CdAtivoBolsaContraParte";
			 public const string IdAtivoCarteira = "IdAtivoCarteira";
			 public const string IdAtivoCarteiraContraParte = "IdAtivoCarteiraContraParte";
			 public const string DataUltimoEvento = "DataUltimoEvento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoSwapAberturaMetadata))
			{
				if(PosicaoSwapAberturaMetadata.mapDelegates == null)
				{
					PosicaoSwapAberturaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoSwapAberturaMetadata.meta == null)
				{
					PosicaoSwapAberturaMetadata.meta = new PosicaoSwapAberturaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAssessor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoRegistro", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("NumeroContrato", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataEmissao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorBase", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoPonta", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TaxaJuros", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Percentual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoApropriacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ContagemDias", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("BaseAno", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TipoPontaContraParte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdIndiceContraParte", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TaxaJurosContraParte", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PercentualContraParte", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoApropriacaoContraParte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ContagemDiasContraParte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("BaseAnoContraParte", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ComGarantia", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DiasUteis", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DiasCorridos", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorParte", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorContraParte", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Saldo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIndicePartida", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIndicePartidaContraParte", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdEstrategia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CdAtivoBolsaContraParte", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAtivoCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAtivoCarteiraContraParte", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataUltimoEvento", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "PosicaoSwapAbertura";
				meta.Destination = "PosicaoSwapAbertura";
				
				meta.spInsert = "proc_PosicaoSwapAberturaInsert";				
				meta.spUpdate = "proc_PosicaoSwapAberturaUpdate";		
				meta.spDelete = "proc_PosicaoSwapAberturaDelete";
				meta.spLoadAll = "proc_PosicaoSwapAberturaLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoSwapAberturaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoSwapAberturaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
