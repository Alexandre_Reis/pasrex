﻿using log4net;
using Financial.Swap.Enums;
using System.Collections.Generic;
using System;
using Financial.Util;
using Financial.Common.Enums;
using System.Text;
using System.Data.SqlClient;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor.Enums;
using Financial.Investidor.Controller;
using Financial.Common;
using Financial.Investidor;
using Financial.Fundo.Enums;
using Financial.Fundo;

namespace Financial.Swap.Controller
{
    public class ControllerSwap
    {
        /// <summary>
        /// Reprocessa as posições, por tipo = abertura ou tipo = fechamento.
        /// Se fechamento, busca as posições de fechamento do dia, pela PosicaoHistorico.
        /// Se abertura, busca as posições de abertura do dia PosicaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoReprocessamento">Verificar se é necessario</param>
        /// thows ArgumentException se tipoReprocessamento for diferente dos valores do 
        /// Enum TipoReprocessamentoBMF
        public void ReprocessaPosicao(int idCliente, DateTime data, TipoReprocessamento tipoReprocessamento)
        {

            #region Delete PosicaoSwap
            // PosicaoBMF Delete
            PosicaoSwapCollection posicaoSwapCollectionDeletar = new PosicaoSwapCollection();
            try
            {
                posicaoSwapCollectionDeletar.DeletaPosicaoSwap(idCliente);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete PosicaoSwap");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Inserts das Posições baseado nas posições de fechamento, ou de abertura
            //Para SqlServer precisa dar um tratamento especial para a PK Identity
            // Nome da Configuração Default SQL
            if (esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider")
            {
                using (esTransactionScope scope = new esTransactionScope())
                {
                    esUtility u = new esUtility();
                    string sql = " set identity_insert PosicaoSwap on ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                    {
                        #region Insert PosicaoSwap a partir de PosicaoSwapHistorico
                        PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();
                        try
                        {
                            posicaoSwapCollection.InserePosicaoSwapHistorico(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoSwap");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }
                    else
                    {
                        #region Insert PosicaoSwap a partir de PosicaoSwapAbertura
                        PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();
                        try
                        {
                            posicaoSwapCollection.InserePosicaoSwapAbertura(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoSwap");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }

                    u = new esUtility();
                    sql = " set identity_insert PosicaoSwap off ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    scope.Complete();
                }
            }
            else
            {
                if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                {
                    #region Insert PosicaoSwap a partir de PosicaoSwapHistorico
                    PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();
                    try
                    {
                        posicaoSwapCollection.InserePosicaoSwapHistorico(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoSwap");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion
                }
                else
                {
                    #region Insert PosicaoSwap a partir de PosicaoSwapAbertura
                    PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();
                    try
                    {
                        posicaoSwapCollection.InserePosicaoSwapAbertura(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoSwap");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion
                }
            }
            #endregion

            #region Delete PosicaoSwapHistorico
            this.DeletaPosicoesHistorico(idCliente, data);
            #endregion

            if (tipoReprocessamento != TipoReprocessamento.CalculoDiario)
            {
                #region Delete PosicaoSwapAbertura
                // PosicaoBMFAbertura Delete
                PosicaoSwapAberturaCollection posicaoSwapAberturaCollection = new PosicaoSwapAberturaCollection();
                try
                {
                    posicaoSwapAberturaCollection.DeletaPosicaoSwapAberturaDataHistoricoMaior(idCliente, data);
                }
                catch (SqlException e)
                {
                    Console.WriteLine("Delete PosicaoSwapAbertura");
                    Console.WriteLine(e.LineNumber);
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                    Console.WriteLine(e.ToString());
                }
                #endregion
            }
        }

        /// <summary>
        /// Executa cálculos de abertura para as pontas dos swaps. 
        /// Carrega a posição atual para Posicao de Abertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaAbertura(int idCliente, DateTime data)
        {
            #region Cliente
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            #endregion

            PosicaoSwap posicaoSwap = new PosicaoSwap();

            if (cliente.AberturaIndexada.GetValueOrDefault(0) != (int)TipoAberturaIndexada.NaoExecuta)
            {
                posicaoSwap.AberturaIndexada(cliente, data);
            }
            else
            {
                //Calcula as curvas das pontas das posições de swap, onde as 2 pontas tem cálculo <> de indexado_D0
                posicaoSwap.CalculaSaldoPosicaoAbertura(idCliente, data);
                //
            }
            //Carrega a posição atual para Posicao de Abertura
            posicaoSwap.GeraPosicaoAbertura(idCliente, data);

            posicaoSwap.ProcessaVencimentoSwap(idCliente, data);

            #region Calcula Prazo Médio
            this.CalculaPrazoMedioSwap(idCliente, data);
            #endregion

            #region CalculaRentabilidade
            this.CalculaRentabilidadeSwap(data, idCliente);
            #endregion
            //            
        }

        /// <summary>
        /// Backup de PosicaoAtual para PosicaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaDivulgacao(int idCliente, DateTime data)
        {
            PosicaoSwap posicaoSwap = new PosicaoSwap();
            posicaoSwap.GeraBackup(idCliente, data);
        }

        /// <summary>
        /// Deleta as posicoes de historico em PosicaoRendaFixa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaPosicoesHistorico(int idCliente, DateTime data)
        {
            PosicaoSwapHistoricoCollection posicaoSwapHistoricoCollection = new PosicaoSwapHistoricoCollection();
            try
            {
                posicaoSwapHistoricoCollection.DeletaPosicaoSwapHistoricoDataHistoricoMaiorIgual(idCliente, data);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete PosicaoSwapHistorico");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
        }

        /// <summary>
        /// Realiza todos os cálculos financeiros e despesas, atualiza a custódia, gera fluxos de pagamento futuros.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaFechamento(int idCliente, DateTime data, ParametrosProcessamento parametrosProcessamento)
        {
            this.ReprocessaPosicao(idCliente, data, TipoReprocessamento.CalculoDiario);

            PosicaoSwap posicaoSwap = new PosicaoSwap();
            OperacaoSwap operacaoSwap = new OperacaoSwap();
            LiquidacaoSwap liquidacaoSwap = new LiquidacaoSwap();
            EventoSwap eventoSwap = new EventoSwap();

            if (!parametrosProcessamento.IgnoraOperacoes)
            {
                //Abertura de novas posições
                operacaoSwap.ProcessaOperacaoSwap(idCliente, data);
                //
            }

            //Calcula as curvas das pontas das posições de swap, onde uma das 2 pontas tem cálculo = indexado_D0
            posicaoSwap.CalculaSaldoPosicaoFechamento(idCliente, data);
            //

            if (!parametrosProcessamento.IgnoraOperacoes)
            {
                //Liquidação das antecipações
                liquidacaoSwap.ProcessaAntecipacaoSwap(idCliente, data);
                //

                //Tratamento de swaps vencendo na data
                posicaoSwap.ProcessaVencimentoSwap(idCliente, data);
                //
            }

            #region Evento de reset
            eventoSwap.ExecutaReset(idCliente, data);
            #endregion

            if (parametrosProcessamento.RemuneraRF)
            {
                posicaoSwap.CalculaSaldoPosicaoAbertura(idCliente, data);
            }

            #region Calcula Prazo Médio
            this.CalculaPrazoMedioSwap(idCliente, data);
            #endregion

            #region CalculaRentabilidade
            this.CalculaRentabilidadeSwap(data, idCliente);
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaPrazoMedioSwap(int idCliente, DateTime data)
        {
            PrazoMedioCollection prazoMedioColl = new PrazoMedioCollection();
            prazoMedioColl.DeletaPrazoMedioMaiorIgual(idCliente, data, TipoAtivoAuxiliar.OperacaoSwap);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);
            string calculaPrazoMedio = string.IsNullOrEmpty(carteira.CalculaPrazoMedio) ? string.Empty : carteira.CalculaPrazoMedio;

            if (!calculaPrazoMedio.Equals("S"))
                return;

            #region Objetos
            PosicaoSwapCollection posicaoSwapColl = new PosicaoSwapCollection();
            PosicaoSwapQuery posicaoSwapQuery = new PosicaoSwapQuery("posicaoSwap");
            IndiceQuery indiceQueryAtivo = new IndiceQuery("indiceAtivo");
            IndiceQuery indiceQueryPassivo = new IndiceQuery("indicePassivo");
            string fieldIndiceAtivo = "DescricaoIndiceAtivo";
            string fieldIndicePassivo = "DescricaoIndicePassivo";
            #endregion

            posicaoSwapQuery.Select(posicaoSwapQuery,
                                    //Ativo
                                    indiceQueryAtivo.Descricao.As(fieldIndiceAtivo),
                                    posicaoSwapQuery.PercentualContraParte,
                                    posicaoSwapQuery.TaxaJurosContraParte,
                                    //Passivo
                                    indiceQueryPassivo.Descricao.As(fieldIndicePassivo),
                                    posicaoSwapQuery.Percentual,
                                    posicaoSwapQuery.TaxaJuros);
            posicaoSwapQuery.LeftJoin(indiceQueryAtivo).On(posicaoSwapQuery.IdIndiceContraParte.Equal(indiceQueryAtivo.IdIndice));
            posicaoSwapQuery.LeftJoin(indiceQueryPassivo).On(posicaoSwapQuery.IdIndice.Equal(indiceQueryPassivo.IdIndice));
            posicaoSwapQuery.Where(posicaoSwapQuery.IdCliente.Equal(idCliente));

            posicaoSwapColl.Load(posicaoSwapQuery);

            foreach (PosicaoSwap posicaoSwap in posicaoSwapColl)
            {
                int prazoDC = Calendario.NumeroDias(data, posicaoSwap.DataVencimento.Value);
                decimal valorPosicao = posicaoSwap.ValorContraParte.Value;
                string descricaoCompleta = this.RetornaDescricaoCompleta(posicaoSwap);

                PrazoMedio prazoMedio = new PrazoMedio();
                prazoMedio.InserePrazoMedio(data, idCliente, descricaoCompleta, (int)TipoAtivoAuxiliar.OperacaoSwap, valorPosicao, prazoDC, posicaoSwap.IdPosicao.Value);
            }
            prazoMedioColl.Save();
        }

        /// <summary>
        /// Calcula rentabilidade de Swap
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        /// <param name="enumTipoProcessamento"></param>
        public void CalculaRentabilidadeSwap(DateTime data, int idCliente)
        {
            #region Se carteira me n�o calcula rentabilidade ( precisa desenvolver )
            CarteiraMae carteiraMae = new CarteiraMae();
            if (carteiraMae.IsCarteiraMae(idCliente))
                return;
            #endregion

            #region EntitySpaces
            string tablePosicaoFechamento = "PosicaoFechamento";
            string tableIndiceAtivo = "IndiceAtivo";
            string tableIndicePassivo = "IndicePassivo";
            string tablePosicaoAbertura = "PosicaoAbertura";
            string tablePosicaoHistorico = "PosicaoHistorico";
            string tableLiquidacaoSwap = "Liquidacao";
            string fieldIndiceAtivo = "DescricaoIndiceAtivo";
            string fieldIndicePassivo = "DescricaoIndicePassivo";
            string fieldSaldoFechamento = "SaldoFechamento";
            string fieldSaldoAbertura = "SaldoAbertura";
            string fieldValorFechamentoPassivo = "ValorFechamentoPassivo";
            string fieldValorFechamentoAtivo = "ValorFechamentoContraAtivo";
            string fieldValorAberturaPassivo = "ValorAberturaPassivo";
            string fieldValorAberturaAtivo = "ValorAberturaAtivo";
            string fieldValorIR = "ValorIR";
            string fieldValorIRHistorico = "ValorIRHistorico";
            #endregion

            #region Objetos
            Dictionary<int, string> dicMoedasAtivo = new Dictionary<int, string>();
            Dictionary<string, decimal> dicCotacao = new Dictionary<string, decimal>();
            Dictionary<string, Rentabilidade.RentabilidadeConversao> dicRentConversao = new Dictionary<string, Rentabilidade.RentabilidadeConversao>();
            List<LiquidacaoSwap> lstLiquidacaoSwap = new List<LiquidacaoSwap>();
            List<int> lstEventosRendas = new List<int>();
            //Querys
            PosicaoSwapQuery posicaoSwapQuery = new PosicaoSwapQuery(tablePosicaoFechamento);
            PosicaoSwapAberturaQuery posicaoSwapAberturaQuery = new PosicaoSwapAberturaQuery(tablePosicaoAbertura);
            PosicaoSwapHistoricoQuery posicaoSwapHistoricoQuery = new PosicaoSwapHistoricoQuery(tablePosicaoHistorico);
            LiquidacaoSwapQuery liquidacaoQuery = new LiquidacaoSwapQuery(tableLiquidacaoSwap);
            IndiceQuery indiceQueryAtivo = new IndiceQuery(tableIndiceAtivo);
            IndiceQuery indiceQueryPassivo = new IndiceQuery(tableIndicePassivo); 

            //Collection
            PosicaoSwapCollection posicaoSwapColl = new PosicaoSwapCollection();
            RentabilidadeCollection rentabilidadeColl = new RentabilidadeCollection();
            LiquidacaoSwapCollection liquidacaoColl = new LiquidacaoSwapCollection();

            //Comuns        
            ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            Cliente cliente = new Cliente();
            Moeda moeda = new Moeda();
            LiquidacaoSwap liquidacaoAux;
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            #endregion

            #region Variaveis
            decimal fatorBaixa = 0;
            decimal valorFechamentoBruto = 0;
            decimal valorAberturaBruto = 0;
            decimal valorFechamentoLiquido = 0;
            decimal valorAberturaLiquido = 0;
            decimal valorEntrada = 0;
            decimal valorSaida = 0;
            decimal valorRenda = 0;
            decimal valorRentabilidade = 0;
            decimal valorIR = 0;
            decimal valorIRHistorico = 0;
            decimal cambio = 0;
            string descricaoCompleta = string.Empty;
            string descricaoMoedaCarteira = string.Empty;
            string descricaoMoedaAtivo = string.Empty;
            string keyConversao = string.Empty;
            int idPosicao = 0;
            short idMoedaAtivo = 0;
            short idMoedaCarteira = 0;
            byte tipoConversao;
            decimal valorSaidaLiquido = 0;
            decimal valorSaidaBruto = 0;
            decimal saldoFechamento = 0;
            decimal saldoAbertura = 0;
            decimal valorRentabilidadeBrutaDiaria = 0;
            decimal valorRentabilidadeBrutaAcumulada = 0;
            decimal valorRentabilidadeLiquida = 0;

            #endregion

            #region Deleta Rentabilidade
            rentabilidadeColl.DeletaRentabilidadeMaiorIgual(idCliente, data, TipoAtivoAuxiliar.OperacaoSwap);
            #endregion

            #region Carrega as Posi�ções
            posicaoSwapQuery.Select(posicaoSwapQuery.Saldo.As(fieldSaldoFechamento),
                                    posicaoSwapQuery.ValorBase,
                                    posicaoSwapHistoricoQuery.Saldo.Coalesce(tablePosicaoAbertura + ".Saldo").As(fieldSaldoAbertura),
                                    posicaoSwapQuery.IdPosicao,
                                    posicaoSwapQuery.IdOperacao,
                                    posicaoSwapQuery.DataEmissao,
                                    posicaoSwapQuery.DataVencimento,
                                    posicaoSwapQuery.ValorIR.As(fieldValorIR),
                                    posicaoSwapHistoricoQuery.ValorIR.As(fieldValorIRHistorico),

                                    //Ativo
                                    indiceQueryAtivo.Descricao.As(fieldIndiceAtivo),
                                    posicaoSwapQuery.PercentualContraParte,
                                    posicaoSwapQuery.TaxaJurosContraParte,
                                    posicaoSwapQuery.ValorContraParte.As(fieldValorFechamentoAtivo),
                                    posicaoSwapHistoricoQuery.ValorContraParte.Coalesce(tablePosicaoAbertura + ".ValorContraParte").As(fieldValorAberturaAtivo),
                                    //Passivo
                                    indiceQueryPassivo.Descricao.As(fieldIndicePassivo),
                                    posicaoSwapQuery.Percentual,
                                    posicaoSwapQuery.TaxaJuros,
                                    posicaoSwapQuery.ValorParte.As(fieldValorFechamentoPassivo),
                                    posicaoSwapHistoricoQuery.ValorParte.Coalesce(tablePosicaoAbertura + ".ValorParte").As(fieldValorAberturaPassivo));
            posicaoSwapQuery.LeftJoin(indiceQueryAtivo).On(posicaoSwapQuery.IdIndiceContraParte.Equal(indiceQueryAtivo.IdIndice));
            posicaoSwapQuery.LeftJoin(indiceQueryPassivo).On(posicaoSwapQuery.IdIndice.Equal(indiceQueryPassivo.IdIndice));
            posicaoSwapQuery.LeftJoin(posicaoSwapAberturaQuery).On(posicaoSwapAberturaQuery.IdOperacao.Equal(posicaoSwapQuery.IdOperacao)
                                                                   & posicaoSwapAberturaQuery.DataHistorico.Equal("'" + dataAnterior.ToString("yyyyMMdd") + "'"));
            posicaoSwapQuery.LeftJoin(posicaoSwapHistoricoQuery).On(posicaoSwapHistoricoQuery.IdOperacao.Equal(posicaoSwapQuery.IdOperacao)
                                                                   & posicaoSwapHistoricoQuery.DataHistorico.Equal("'" + dataAnterior.ToString("yyyyMMdd") + "'"));
            posicaoSwapQuery.Where(posicaoSwapQuery.IdCliente.Equal(idCliente));

            if (!posicaoSwapColl.Load(posicaoSwapQuery))
                return;
            #endregion

            #region Carrega Moeda e Carteira
            cliente.LoadByPrimaryKey(idCliente);

            if (moeda.LoadByPrimaryKey(cliente.IdMoeda.Value))
            {
                descricaoMoedaCarteira = moeda.Nome;
                idMoedaCarteira = moeda.IdMoeda.Value;
            }
            #endregion

            #region Carrega Rendas dos Ativos
            liquidacaoQuery.Select(liquidacaoQuery.IdCliente,
                                   liquidacaoQuery.Data,
                                   liquidacaoQuery.IdPosicao,
                                   liquidacaoQuery.ValorAntecipacao);
            liquidacaoQuery.Where(liquidacaoQuery.IdCliente.Equal(idCliente)
                                  & liquidacaoQuery.Data.Equal(data));
            liquidacaoQuery.OrderBy(liquidacaoQuery.IdPosicao.Ascending);

            if (liquidacaoColl.Load(liquidacaoQuery))
                lstLiquidacaoSwap = (List<LiquidacaoSwap>)liquidacaoColl;
            #endregion

            foreach (PosicaoSwap posicaoSwap in posicaoSwapColl)
            {
                idPosicao = posicaoSwap.IdPosicao.Value;               

                idMoedaAtivo = 1; //Real
                valorFechamentoBruto = 0;
                valorAberturaBruto = 0;
                valorEntrada = 0;
                valorSaida = 0;
                valorRenda = 0;
                valorRentabilidade = 0;
                valorIR = 0;
                valorIRHistorico = 0;

                #region Carrega moeda do Ativo
                if (!dicMoedasAtivo.ContainsKey(idMoedaAtivo))
                {
                    moeda = new Moeda();
                    moeda.LoadByPrimaryKey(idMoedaAtivo);
                    dicMoedasAtivo.Add(moeda.IdMoeda.Value, moeda.Nome);
                }
                descricaoMoedaAtivo = dicMoedasAtivo[idMoedaAtivo];
                #endregion

                #region Valor de Saída
                fatorBaixa = 1;
                liquidacaoAux = new LiquidacaoSwap();
                liquidacaoAux = lstLiquidacaoSwap.Find(delegate(LiquidacaoSwap x) { return x.IdPosicao == idPosicao; });
                if (liquidacaoAux != null && liquidacaoAux.IdPosicao.Value > 0)
                {
                    valorSaidaBruto = Convert.ToDecimal(liquidacaoAux.ValorAntecipacao.Value);
                    valorSaidaLiquido = valorSaidaBruto - Convert.ToDecimal(liquidacaoAux.ValorAntecipacao.GetValueOrDefault(0));
                    fatorBaixa = valorSaidaBruto / posicaoSwap.ValorBase.Value;
                }
                else
                {
                    valorSaida = 0;
                }
                #endregion

                descricaoCompleta = this.RetornaDescricaoCompleta(posicaoSwap);

                #region Popula objeto - Ativo
                if (!Decimal.TryParse(posicaoSwap.GetColumn(fieldSaldoFechamento).ToString(), out saldoFechamento))
                    saldoFechamento = 0;

                if (!Decimal.TryParse(posicaoSwap.GetColumn(fieldSaldoAbertura).ToString(), out saldoAbertura))
                    saldoAbertura = 0;

                if (!Decimal.TryParse(posicaoSwap.GetColumn(fieldValorFechamentoAtivo).ToString(), out valorFechamentoBruto))
                    valorFechamentoBruto = 0;

                if (!Decimal.TryParse(posicaoSwap.GetColumn(fieldValorAberturaAtivo).ToString(), out valorAberturaBruto))
                    valorAberturaBruto = 0;

                if (!Decimal.TryParse(posicaoSwap.GetColumn(fieldValorIR).ToString(), out valorIR))
                    valorIR = 0;

                if (!Decimal.TryParse(posicaoSwap.GetColumn(fieldValorIRHistorico).ToString(), out valorIRHistorico))
                    valorIRHistorico = 0;

                valorSaida = valorSaida * fatorBaixa;

                #region Calcula valor liquido
                valorFechamentoLiquido = valorFechamentoBruto;
                if (saldoFechamento > 0)
                {
                    valorFechamentoLiquido -= valorIR;
                }

                valorAberturaLiquido = valorAberturaBruto;
                if (saldoAbertura > 0)
                {
                    valorAberturaLiquido -= valorIRHistorico;
                }
                #endregion

                if (posicaoSwap.DataEmissao.Value != data)
                {
                    #region Rentabilidade Bruta
                    try
                    {
                        valorRentabilidadeBrutaDiaria = ((valorFechamentoBruto + valorSaida) / (valorAberturaBruto + valorEntrada - valorRenda)) - 1;
                    }
                    catch (Exception ex)
                    {
                        valorRentabilidadeBrutaDiaria = 0;
                    }
                    #endregion

                    #region Rentabilidade Liquida
                    try
                    {
                        valorRentabilidadeLiquida = ((valorFechamentoLiquido + valorSaida) / (valorAberturaLiquido + valorEntrada - valorRenda)) - 1;
                    }
                    catch (Exception ex)
                    {
                        valorRentabilidadeLiquida = 0;
                    }
                    #endregion
                }
                else
                {
                    valorRentabilidadeBrutaDiaria = 0;
                    valorRentabilidadeLiquida = 0;
                    valorEntrada = posicaoSwap.ValorBase.Value;
                }

                Rentabilidade rentabilidadeAtivo = rentabilidadeColl.AddNew();
                rentabilidadeAtivo.CodigoAtivo = "Ponta Ativo -> " + descricaoCompleta;
                rentabilidadeAtivo.TipoAtivo = (int)TipoAtivoAuxiliar.OperacaoSwap;
                rentabilidadeAtivo.Data = data;
                rentabilidadeAtivo.IdCliente = idCliente;
                rentabilidadeAtivo.IdOperacaoSwap = posicaoSwap.IdOperacao.HasValue ? posicaoSwap.IdOperacao.Value : 0;

                //Ativo
                rentabilidadeAtivo.CodigoMoedaAtivo = descricaoMoedaAtivo;
                rentabilidadeAtivo.PatrimonioFinalMoedaPortfolio = rentabilidadeAtivo.PatrimonioFinalMoedaAtivo = valorFechamentoBruto;
                rentabilidadeAtivo.PatrimonioInicialMoedaPortfolio = rentabilidadeAtivo.PatrimonioInicialMoedaAtivo = valorAberturaBruto;
                rentabilidadeAtivo.RentabilidadeMoedaPortfolio = rentabilidadeAtivo.RentabilidadeMoedaAtivo = valorRentabilidadeLiquida;
                rentabilidadeAtivo.ValorFinanceiroEntradaMoedaPortfolio = rentabilidadeAtivo.ValorFinanceiroEntradaMoedaAtivo = valorEntrada;
                rentabilidadeAtivo.ValorFinanceiroSaidaMoedaPortfolio = rentabilidadeAtivo.ValorFinanceiroSaidaMoedaAtivo = valorSaida;
                rentabilidadeAtivo.ValorFinanceiroRendasMoedaPortfolio = rentabilidadeAtivo.ValorFinanceiroRendasMoedaAtivo = valorRenda;
                rentabilidadeAtivo.RentabilidadeBrutaDiariaMoedaAtivo = rentabilidadeAtivo.RentabilidadeBrutaDiariaMoedaPortfolio = valorRentabilidadeBrutaDiaria;
                rentabilidadeAtivo.RentabilidadeBrutaAcumMoedaAtivo = rentabilidadeAtivo.RentabilidadeBrutaAcumMoedaPortfolio = valorRentabilidadeBrutaAcumulada;
                rentabilidadeAtivo.RentabilidadeGrossUpDiariaMoedaAtivo = rentabilidadeAtivo.RentabilidadeGrossUpDiariaMoedaPortfolio = 0;
                rentabilidadeAtivo.RentabilidadeGrossUpAcumMoedaAtivo = rentabilidadeAtivo.RentabilidadeGrossUpAcumMoedaPortfolio = 0;
                rentabilidadeAtivo.PatrimonioFinalBrutoMoedaAtivo = rentabilidadeAtivo.PatrimonioFinalBrutoMoedaPortfolio = valorFechamentoBruto;
                rentabilidadeAtivo.PatrimonioInicialBrutoMoedaPortfolio = rentabilidadeAtivo.PatrimonioInicialBrutoMoedaAtivo = valorAberturaBruto;
                rentabilidadeAtivo.PatrimonioFinalGrossUpMoedaAtivo = rentabilidadeAtivo.PatrimonioFinalGrossUpMoedaPortfolio = valorFechamentoBruto;
                rentabilidadeAtivo.PatrimonioInicialGrossUpMoedaAtivo = rentabilidadeAtivo.PatrimonioInicialGrossUpMoedaPortfolio = valorAberturaBruto;

                //Portfolio
                rentabilidadeAtivo.CodigoMoedaPortfolio = descricaoMoedaCarteira;

                //Converte os valores para a moeda da carteira
                rentabilidadeAtivo.ConverteValores(idMoedaAtivo, idMoedaCarteira, ref dicRentConversao);
                #endregion

                #region Popula objeto - Passiva
                if (!Decimal.TryParse(posicaoSwap.GetColumn(fieldSaldoFechamento).ToString(), out saldoFechamento))
                    saldoFechamento = 0;

                if (!Decimal.TryParse(posicaoSwap.GetColumn(fieldSaldoAbertura).ToString(), out saldoAbertura))
                    saldoAbertura = 0;

                if (!Decimal.TryParse(posicaoSwap.GetColumn(fieldValorFechamentoPassivo).ToString(), out valorFechamentoBruto))
                    valorFechamentoBruto = 0;

                if (!Decimal.TryParse(posicaoSwap.GetColumn(fieldValorAberturaPassivo).ToString(), out valorAberturaBruto))
                    valorAberturaBruto = 0;

                if (!Decimal.TryParse(posicaoSwap.GetColumn(fieldValorIR).ToString(), out valorIR))
                    valorIR = 0;

                if (!Decimal.TryParse(posicaoSwap.GetColumn(fieldValorIRHistorico).ToString(), out valorIRHistorico))
                    valorIRHistorico = 0;

                valorSaida = valorSaida * fatorBaixa;

                #region Calcula valor liquido
                valorFechamentoLiquido = valorFechamentoBruto;
                if (saldoFechamento < 0)
                {
                    valorFechamentoLiquido -= valorIR;
                }

                valorAberturaLiquido = valorAberturaBruto;
                if (saldoAbertura < 0)
                {
                    valorAberturaLiquido -= valorIRHistorico;
                }
                #endregion

                if (posicaoSwap.DataEmissao.Value != data)
                {
                    #region Rentabilidade Bruta
                    try
                    {
                        valorRentabilidadeBrutaDiaria = ((valorFechamentoBruto + valorSaida) / (valorAberturaBruto + valorEntrada - valorRenda)) - 1;
                    }
                    catch (Exception ex)
                    {
                        valorRentabilidadeBrutaDiaria = 0;
                    }
                    #endregion

                    #region Rentabilidade Liquida
                    try
                    {
                        valorRentabilidadeLiquida = ((valorFechamentoLiquido + valorSaida) / (valorAberturaLiquido + valorEntrada - valorRenda)) - 1;
                    }
                    catch (Exception ex)
                    {
                        valorRentabilidadeLiquida = 0;
                    }
                    #endregion
                }
                else
                {
                    valorRentabilidadeBrutaDiaria = 0;
                    valorRentabilidadeLiquida = 0;
                    valorEntrada = posicaoSwap.ValorBase.Value; 

                }

                Rentabilidade rentabilidadePassivo = rentabilidadeColl.AddNew();
                rentabilidadePassivo.CodigoAtivo = "Ponta Passiva -> " + descricaoCompleta;
                rentabilidadePassivo.TipoAtivo = (int)TipoAtivoAuxiliar.OperacaoSwap;
                rentabilidadePassivo.Data = data;
                rentabilidadePassivo.IdCliente = idCliente;
                rentabilidadePassivo.IdOperacaoSwap = posicaoSwap.IdOperacao.HasValue ? posicaoSwap.IdOperacao.Value : 0;

                //Ativo
                rentabilidadePassivo.CodigoMoedaAtivo = descricaoMoedaAtivo;
                rentabilidadePassivo.PatrimonioFinalMoedaPortfolio = rentabilidadePassivo.PatrimonioFinalMoedaAtivo = valorFechamentoBruto;
                rentabilidadePassivo.PatrimonioInicialMoedaPortfolio = rentabilidadePassivo.PatrimonioInicialMoedaAtivo = valorAberturaBruto;
                rentabilidadePassivo.RentabilidadeMoedaPortfolio = rentabilidadePassivo.RentabilidadeMoedaAtivo = valorRentabilidadeLiquida;
                rentabilidadePassivo.ValorFinanceiroEntradaMoedaPortfolio = rentabilidadePassivo.ValorFinanceiroEntradaMoedaAtivo = valorEntrada;
                rentabilidadePassivo.ValorFinanceiroSaidaMoedaPortfolio = rentabilidadePassivo.ValorFinanceiroSaidaMoedaAtivo = valorSaida;
                rentabilidadePassivo.ValorFinanceiroRendasMoedaPortfolio = rentabilidadePassivo.ValorFinanceiroRendasMoedaAtivo = valorRenda;
                rentabilidadePassivo.RentabilidadeBrutaDiariaMoedaAtivo = rentabilidadePassivo.RentabilidadeBrutaDiariaMoedaPortfolio = valorRentabilidadeBrutaDiaria;
                rentabilidadePassivo.RentabilidadeBrutaAcumMoedaAtivo = rentabilidadePassivo.RentabilidadeBrutaAcumMoedaPortfolio = valorRentabilidadeBrutaAcumulada;
                rentabilidadePassivo.RentabilidadeGrossUpDiariaMoedaAtivo = rentabilidadePassivo.RentabilidadeGrossUpDiariaMoedaPortfolio = 0;
                rentabilidadePassivo.RentabilidadeGrossUpAcumMoedaAtivo = rentabilidadePassivo.RentabilidadeGrossUpAcumMoedaPortfolio = 0;
                rentabilidadePassivo.PatrimonioFinalBrutoMoedaAtivo = rentabilidadePassivo.PatrimonioFinalBrutoMoedaPortfolio = valorFechamentoBruto;
                rentabilidadePassivo.PatrimonioInicialBrutoMoedaPortfolio = rentabilidadePassivo.PatrimonioInicialBrutoMoedaAtivo = valorAberturaBruto;
                rentabilidadePassivo.PatrimonioFinalGrossUpMoedaAtivo = rentabilidadePassivo.PatrimonioFinalGrossUpMoedaPortfolio = valorFechamentoBruto;
                rentabilidadePassivo.PatrimonioInicialGrossUpMoedaAtivo = rentabilidadePassivo.PatrimonioInicialGrossUpMoedaPortfolio = valorAberturaBruto;

                //Portfolio
                rentabilidadePassivo.CodigoMoedaPortfolio = descricaoMoedaCarteira;

                //Converte os valores para a moeda da carteira
                rentabilidadePassivo.ConverteValores(idMoedaAtivo, idMoedaCarteira, ref dicRentConversao);
                #endregion

            }

            rentabilidadeColl.Save();
        }

        /// <summary>
        /// Para Usar a função, é necessário passar como parametro a PosicaoSwap contemplando as informações de índices
        /// </summary>
        /// <param name="posicaoSwap"></param>
        /// <returns></returns>
        public string RetornaDescricaoCompleta(esEntity posicao)
        {
            StringBuilder sbDescricao = new StringBuilder();
            if (posicao is PosicaoSwap)
            {
                PosicaoSwap posicaoSwap = (PosicaoSwap)posicao;

                sbDescricao.Append("Emissão: ");
                sbDescricao.Append(String.Format("{0:dd/MM/yyyy}", posicaoSwap.DataEmissao.Value));
                sbDescricao.Append(" - Vcto: ");
                sbDescricao.Append(String.Format("{0:dd/MM/yyyy}", posicaoSwap.DataVencimento.Value));

                #region Ponta Ativa
                sbDescricao.Append(" Ativo -> ");
                if (!string.IsNullOrEmpty(posicaoSwap.GetColumn("DescricaoIndiceAtivo").ToString()))
                {
                    if (posicaoSwap.PercentualContraParte.HasValue)
                        sbDescricao.Append(posicaoSwap.PercentualContraParte.Value.ToString());
                    else
                        sbDescricao.Append("0,00");
                    sbDescricao.Append("% ");

                    sbDescricao.Append(posicaoSwap.GetColumn("DescricaoIndiceAtivo"));
                }
                else
                {
                    sbDescricao.Append("Pré ");
                }

                if (posicaoSwap.TaxaJurosContraParte.HasValue)
                {
                    if (posicaoSwap.TaxaJurosContraParte.Value > 0)
                    {
                        sbDescricao.Append(" + ");
                        sbDescricao.Append(posicaoSwap.TaxaJurosContraParte.Value.ToString());
                        sbDescricao.Append("%");
                    }
                }
                #endregion

                #region Ponta Passiva
                sbDescricao.Append("/ Passivo -> ");
                if (!string.IsNullOrEmpty(posicaoSwap.GetColumn("DescricaoIndicePassivo").ToString()))
                {
                    if (posicaoSwap.Percentual.HasValue)
                        sbDescricao.Append(posicaoSwap.Percentual.Value.ToString());
                    else
                        sbDescricao.Append("0,00");
                    sbDescricao.Append("% ");

                    sbDescricao.Append(posicaoSwap.GetColumn("DescricaoIndicePassivo"));
                }
                else
                {
                    sbDescricao.Append("Pré ");
                }

                if (posicaoSwap.TaxaJuros.HasValue)
                {
                    if (posicaoSwap.TaxaJuros.Value > 0)
                    {
                        sbDescricao.Append(" + ");
                        sbDescricao.Append(posicaoSwap.TaxaJuros.Value.ToString());
                        sbDescricao.Append("%");
                    }
                }
                #endregion
            }
            else if (posicao is PosicaoSwapHistorico)
            {
                PosicaoSwapHistorico posicaoSwapHistorico = (PosicaoSwapHistorico)posicao;

                sbDescricao.Append("Emissão: ");
                sbDescricao.Append(String.Format("{0:dd/MM/yyyy}", posicaoSwapHistorico.DataEmissao.Value));
                sbDescricao.Append(" - Vcto: ");
                sbDescricao.Append(String.Format("{0:dd/MM/yyyy}", posicaoSwapHistorico.DataVencimento.Value));

                #region Ponta Ativa
                sbDescricao.Append(" Ativo -> ");
                if (!string.IsNullOrEmpty(posicaoSwapHistorico.GetColumn("DescricaoIndiceAtivo").ToString()))
                {
                    if (posicaoSwapHistorico.PercentualContraParte.HasValue)
                        sbDescricao.Append(posicaoSwapHistorico.PercentualContraParte.Value.ToString());
                    else
                        sbDescricao.Append("0,00");
                    sbDescricao.Append("% ");

                    sbDescricao.Append(posicaoSwapHistorico.GetColumn("DescricaoIndiceAtivo"));
                }
                else
                {
                    sbDescricao.Append("Pré ");
                }

                if (posicaoSwapHistorico.TaxaJurosContraParte.HasValue)
                {
                    if (posicaoSwapHistorico.TaxaJurosContraParte.Value > 0)
                    {
                        sbDescricao.Append(" + ");
                        sbDescricao.Append(posicaoSwapHistorico.TaxaJurosContraParte.Value.ToString());
                        sbDescricao.Append("%");
                    }
                }
                #endregion

                #region Ponta Passiva
                sbDescricao.Append("/ Passivo -> ");
                if (!string.IsNullOrEmpty(posicaoSwapHistorico.GetColumn("DescricaoIndicePassivo").ToString()))
                {
                    if (posicaoSwapHistorico.Percentual.HasValue)
                        sbDescricao.Append(posicaoSwapHistorico.Percentual.Value.ToString());
                    else
                        sbDescricao.Append("0,00");
                    sbDescricao.Append("% ");

                    sbDescricao.Append(posicaoSwapHistorico.GetColumn("DescricaoIndicePassivo"));
                }
                else
                {
                    sbDescricao.Append("Pré ");
                }

                if (posicaoSwapHistorico.TaxaJuros.HasValue)
                {
                    if (posicaoSwapHistorico.TaxaJuros.Value > 0)
                    {
                        sbDescricao.Append(" + ");
                        sbDescricao.Append(posicaoSwapHistorico.TaxaJuros.Value.ToString());
                        sbDescricao.Append("%");
                    }
                }
                #endregion
            }
            return sbDescricao.ToString();
        }
    }
}
