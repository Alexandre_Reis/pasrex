﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Swap.Enums;

namespace Financial.Swap
{
	public partial class PosicaoSwapCollection : esPosicaoSwapCollection
	{
        // Construtor
        // Cria uma nova PosicaoSwapCollection com os dados de PosicaoSwapHistoricoCollection
        // Todos do dados de PosicaoSwapHistorico são copiados com excessão da dataHistorico
        public PosicaoSwapCollection(PosicaoSwapHistoricoCollection posicaoSwapHistoricoCollection) {
            for (int i = 0; i < posicaoSwapHistoricoCollection.Count; i++) {
                //
                PosicaoSwap p = new PosicaoSwap();

                // Para cada Coluna de PosicaoSwapHistorico copia para PosicaoSwap
                foreach (esColumnMetadata colPosicaoHistorico in posicaoSwapHistoricoCollection.es.Meta.Columns) {
                    // Copia todas as colunas menos a Data Historico
                    if (colPosicaoHistorico.PropertyName != PosicaoSwapHistoricoMetadata.ColumnNames.DataHistorico) {
                        esColumnMetadata colPosicaoSwap = p.es.Meta.Columns.FindByPropertyName(colPosicaoHistorico.PropertyName);
                        if (posicaoSwapHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name) != null) {
                            p.SetColumn(colPosicaoSwap.Name, posicaoSwapHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name));
                        }
                    }
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Carrega o objeto PosicaoSwapCollection com todos os campos de PosicaoSwap.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoSwapCompleta(int idCliente)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente);
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PosicaoSwapCollection com todos os campos de PosicaoSwap.
        /// Filtra por:
        /// TipoPonta.NotEqual((byte)TipoPontaSwap.IndexadoD0),
        /// TipoPontaContraParte.NotEqual((byte)TipoPontaSwap.IndexadoD0)
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaPosicaoSwapAbertura(int idCliente)
        {
            List<byte> lstTipoPonta = new List<byte>();
            lstTipoPonta.Add((byte)TipoPontaSwap.IndexadoD0);
            lstTipoPonta.Add((byte)TipoPontaSwap.AtivoBolsa);
            lstTipoPonta.Add((byte)TipoPontaSwap.FundoInvestimento);

            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCliente == idCliente &
                       (this.Query.TipoPonta.NotIn(lstTipoPonta.ToArray()) |
                        this.Query.TipoPontaContraParte.NotIn(lstTipoPonta.ToArray())));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoSwapCollection com todos os campos de PosicaoSwap.
        /// Filtra por:
        /// TipoPonta.Equal((byte)TipoPontaSwap.IndexadoD0) |
        /// TipoPontaContraParte.Equal((byte)TipoPontaSwap.IndexadoD0)
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaPosicaoSwapFechamento(int idCliente)
        {
            List<byte> lstTipoPonta = new List<byte>();
            lstTipoPonta.Add((byte)TipoPontaSwap.IndexadoD0);
            lstTipoPonta.Add((byte)TipoPontaSwap.AtivoBolsa);
            lstTipoPonta.Add((byte)TipoPontaSwap.FundoInvestimento);

            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCliente == idCliente &
                       (this.Query.TipoPonta.In(lstTipoPonta.ToArray()) |
                        this.Query.TipoPontaContraParte.In(lstTipoPonta.ToArray())));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoSwapCollection com os campos NumeroContrato, IdPosicao, Saldo.
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <param name="dataVencimento"></param>
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaPosicaoSwapVencimento(int idCliente, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.NumeroContrato, this.Query.IdPosicao, this.Query.Saldo, this.Query.DataVencimento, this.Query.DataEmissao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataVencimento.Equal(dataVencimento));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Deleta todas as posições do cliente passado.
        /// </summary>
        /// <param name="idCliente"></param>
        public void DeletaPosicaoSwap(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao)
                 .Where(this.Query.IdCliente == idCliente);
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Método de inserção em PosicaoSwap, a partir de PosicaoSwapHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void InserePosicaoSwapHistorico(int idCliente, DateTime dataHistorico)
        {
            PosicaoSwapHistoricoCollection posicaoSwapHistoricoCollection = new PosicaoSwapHistoricoCollection();
            posicaoSwapHistoricoCollection.BuscaPosicaoSwapHistoricoCompleta(idCliente, dataHistorico);
            for (int i = 0; i < posicaoSwapHistoricoCollection.Count; i++)
            {
                #region Copia de posicaoSwapHistorico para posicaoSwap
                PosicaoSwapHistorico posicaoSwapHistorico = posicaoSwapHistoricoCollection[i];
                // Copia para PosicaoSwap
                //PosicaoSwap posicaoSwap = new PosicaoSwap();
                esParameters esParams = new esParameters();
                esParams.Add("IdPosicao", posicaoSwapHistorico.IdPosicao.Value);

                if (!posicaoSwapHistorico.IdOperacao.HasValue)
                {
                    esParams.Add("IdOperacao", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdOperacao", posicaoSwapHistorico.IdOperacao);
                }
                
                esParams.Add("IdCliente", posicaoSwapHistorico.IdCliente.Value);

                if (!posicaoSwapHistorico.IdAssessor.HasValue)
                {
                    esParams.Add("IdAssessor", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdAssessor", posicaoSwapHistorico.IdAssessor.Value);
                }
                
                esParams.Add("TipoRegistro", posicaoSwapHistorico.TipoRegistro.Value);
                esParams.Add("NumeroContrato", posicaoSwapHistorico.NumeroContrato);
                esParams.Add("DataEmissao", posicaoSwapHistorico.DataEmissao.Value);
                esParams.Add("DataVencimento", posicaoSwapHistorico.DataVencimento.Value);
                esParams.Add("ValorBase", posicaoSwapHistorico.ValorBase.Value);
                esParams.Add("TipoPonta", posicaoSwapHistorico.TipoPonta.Value);                

                if (!posicaoSwapHistorico.IdIndice.HasValue)
                {
                    esParams.Add("IdIndice", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdIndice", posicaoSwapHistorico.IdIndice.Value);
                }

                if (!posicaoSwapHistorico.TaxaJuros.HasValue)
                {
                    esParams.Add("TaxaJuros", DBNull.Value);
                }
                else
                {
                    esParams.Add("TaxaJuros", posicaoSwapHistorico.TaxaJuros.Value);
                }

                if (!posicaoSwapHistorico.Percentual.HasValue)
                {
                    esParams.Add("Percentual", DBNull.Value);
                }
                else
                {
                    esParams.Add("Percentual", posicaoSwapHistorico.Percentual.Value);
                }

                if (!posicaoSwapHistorico.TipoApropriacao.HasValue)
                {
                    esParams.Add("TipoApropriacao", DBNull.Value);
                }
                else
                {
                    esParams.Add("TipoApropriacao", posicaoSwapHistorico.TipoApropriacao.Value);
                }
                                
                if (!posicaoSwapHistorico.ContagemDias.HasValue)
                {
                    esParams.Add("ContagemDias", DBNull.Value);
                }
                else
                {
                    esParams.Add("ContagemDias", posicaoSwapHistorico.ContagemDias.Value);
                }
                                
                if (!posicaoSwapHistorico.BaseAno.HasValue)
                {
                    esParams.Add("BaseAno", DBNull.Value);
                }
                else
                {
                    esParams.Add("BaseAno", posicaoSwapHistorico.BaseAno.Value);
                }

                esParams.Add("TipoPontaContraParte", posicaoSwapHistorico.TipoPontaContraParte.Value);

                if (!posicaoSwapHistorico.IdIndiceContraParte.HasValue)
                {
                    esParams.Add("IdIndiceContraParte", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdIndiceContraParte", posicaoSwapHistorico.IdIndiceContraParte.Value);
                }

                if (!posicaoSwapHistorico.TaxaJurosContraParte.HasValue)
                {
                    esParams.Add("TaxaJurosContraParte", DBNull.Value);
                }
                else
                {
                    esParams.Add("TaxaJurosContraParte", posicaoSwapHistorico.TaxaJurosContraParte.Value);
                }

                if (!posicaoSwapHistorico.PercentualContraParte.HasValue)
                {
                    esParams.Add("PercentualContraParte", DBNull.Value);
                }
                else
                {
                    esParams.Add("PercentualContraParte", posicaoSwapHistorico.PercentualContraParte.Value);
                }

                if (!posicaoSwapHistorico.TipoApropriacaoContraParte.HasValue)
                {
                    esParams.Add("TipoApropriacaoContraParte", DBNull.Value);
                }
                else
                {
                    esParams.Add("TipoApropriacaoContraParte", posicaoSwapHistorico.TipoApropriacaoContraParte.Value);
                }

                if (!posicaoSwapHistorico.ContagemDiasContraParte.HasValue)
                {
                    esParams.Add("ContagemDiasContraParte", DBNull.Value);
                }
                else
                {
                    esParams.Add("ContagemDiasContraParte", posicaoSwapHistorico.ContagemDiasContraParte.Value);
                }

                if (!posicaoSwapHistorico.BaseAnoContraParte.HasValue)
                {
                    esParams.Add("BaseAnoContraParte", DBNull.Value);
                }
                else
                {
                    esParams.Add("BaseAnoContraParte", posicaoSwapHistorico.BaseAnoContraParte.Value);
                }

                if (!posicaoSwapHistorico.IdAgente.HasValue)
                {
                    esParams.Add("IdAgente", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdAgente", posicaoSwapHistorico.IdAgente.Value);
                }

                if (!posicaoSwapHistorico.DataUltimoEvento.HasValue)
                {
                    esParams.Add("DataUltimoEvento", DBNull.Value);
                }
                else
                {
                    esParams.Add("DataUltimoEvento", posicaoSwapHistorico.DataUltimoEvento.Value);
                }                 

                esParams.Add("ComGarantia", posicaoSwapHistorico.ComGarantia);
                esParams.Add("DiasUteis", posicaoSwapHistorico.DiasUteis.Value);
                esParams.Add("DiasCorridos", posicaoSwapHistorico.DiasCorridos.Value);
                esParams.Add("ValorParte", posicaoSwapHistorico.ValorParte.Value);
                esParams.Add("ValorContraParte", posicaoSwapHistorico.ValorContraParte.Value);
                esParams.Add("Saldo", posicaoSwapHistorico.Saldo.Value);

                if (!posicaoSwapHistorico.ValorIndicePartida.HasValue)
                {
                    esParams.Add("ValorIndicePartida", DBNull.Value);
                }
                else
                {
                    esParams.Add("ValorIndicePartida", posicaoSwapHistorico.ValorIndicePartida.Value);
                }

                if (!posicaoSwapHistorico.ValorIndicePartidaContraParte.HasValue)
                {
                    esParams.Add("ValorIndicePartidaContraParte", DBNull.Value);
                }
                else
                {
                    esParams.Add("ValorIndicePartidaContraParte", posicaoSwapHistorico.ValorIndicePartidaContraParte.Value);
                }

                if (!posicaoSwapHistorico.IdEstrategia.HasValue)
                {
                    esParams.Add("IdEstrategia", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdEstrategia", posicaoSwapHistorico.IdEstrategia.Value);
                }

                esParams.Add("ValorIR", posicaoSwapHistorico.ValorIR.Value);

                if (posicaoSwapHistorico.IdAtivoCarteiraContraParte.HasValue)
                    esParams.Add("IdAtivoCarteiraContraParte", posicaoSwapHistorico.IdAtivoCarteiraContraParte.Value);
                else
                    esParams.Add("IdAtivoCarteiraContraParte", DBNull.Value);

                if (posicaoSwapHistorico.IdAtivoCarteira.HasValue)
                    esParams.Add("IdAtivoCarteira", posicaoSwapHistorico.IdAtivoCarteira.Value);
                else
                    esParams.Add("IdAtivoCarteira", DBNull.Value);

                if (!string.IsNullOrEmpty(posicaoSwapHistorico.CdAtivoBolsaContraParte))
                    esParams.Add("CdAtivoBolsaContraParte", posicaoSwapHistorico.CdAtivoBolsaContraParte);
                else
                    esParams.Add("CdAtivoBolsaContraParte", DBNull.Value);

                if (!string.IsNullOrEmpty(posicaoSwapHistorico.CdAtivoBolsa))
                    esParams.Add("CdAtivoBolsa", posicaoSwapHistorico.CdAtivoBolsa);
                else
                    esParams.Add("CdAtivoBolsa", DBNull.Value);

                StringBuilder sqlBuilder = new StringBuilder();
                sqlBuilder.Append("SET IDENTITY_INSERT PosicaoSwap ON ");
                //
                sqlBuilder.Append("INSERT INTO PosicaoSwap (");

                sqlBuilder.Append(PosicaoSwapMetadata.ColumnNames.IdPosicao);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdOperacao);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdAssessor);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.TipoRegistro);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.NumeroContrato);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.DataEmissao);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ValorBase);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.TipoPonta);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdIndice);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.TaxaJuros);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.Percentual);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.TipoApropriacao);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ContagemDias);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.BaseAno);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.TipoPontaContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdIndiceContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.TaxaJurosContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.PercentualContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.TipoApropriacaoContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ContagemDiasContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.BaseAnoContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ComGarantia);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.DiasUteis);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.DiasCorridos);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ValorParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ValorContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.Saldo);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ValorIndicePartida);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ValorIndicePartidaContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdEstrategia);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ValorIR);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdAtivoCarteiraContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdAtivoCarteira);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.CdAtivoBolsaContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.CdAtivoBolsa);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.DataUltimoEvento);
                
                sqlBuilder.Append(") VALUES ( ");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdPosicao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdOperacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdAssessor);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.TipoRegistro);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.NumeroContrato);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.DataEmissao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ValorBase);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.TipoPonta);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdIndice);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.TaxaJuros);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.Percentual);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.TipoApropriacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ContagemDias);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.BaseAno);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.TipoPontaContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdIndiceContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.TaxaJurosContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.PercentualContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.TipoApropriacaoContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ContagemDiasContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.BaseAnoContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ComGarantia);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.DiasUteis);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.DiasCorridos);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ValorParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ValorContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.Saldo);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ValorIndicePartida);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ValorIndicePartidaContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdEstrategia);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ValorIR);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdAtivoCarteiraContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdAtivoCarteira);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.CdAtivoBolsaContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.CdAtivoBolsa);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.DataUltimoEvento);
                sqlBuilder.Append(")");
                sqlBuilder.Append("SET IDENTITY_INSERT PosicaoSwap OFF ");
                esUtility u = new esUtility();
                u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
                //
                #endregion
            }
        }

        /// <summary>
        /// Método de inserção em PosicaoSwap, a partir de PosicaoSwapAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void InserePosicaoSwapAbertura(int idCliente, DateTime dataHistorico)
        {
            PosicaoSwapAberturaCollection posicaoSwapAberturaCollection = new PosicaoSwapAberturaCollection();
            posicaoSwapAberturaCollection.BuscaPosicaoSwapAberturaCompleta(idCliente, dataHistorico);
            for (int i = 0; i < posicaoSwapAberturaCollection.Count; i++)
            {
                #region Copia de posicaoSwapAbertura para posicaoSwap
                PosicaoSwapAbertura posicaoSwapAbertura = posicaoSwapAberturaCollection[i];
                // Copia para PosicaoSwap
                //PosicaoSwap posicaoSwap = new PosicaoSwap();
                esParameters esParams = new esParameters();
                esParams.Add("IdPosicao", posicaoSwapAbertura.IdPosicao.Value);

                if (!posicaoSwapAbertura.IdOperacao.HasValue)
                {
                    esParams.Add("IdOperacao", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdOperacao", posicaoSwapAbertura.IdOperacao);
                }

                esParams.Add("IdCliente", posicaoSwapAbertura.IdCliente.Value);

                if (!posicaoSwapAbertura.IdAssessor.HasValue)
                {
                    esParams.Add("IdAssessor", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdAssessor", posicaoSwapAbertura.IdAssessor.Value);
                }

                esParams.Add("TipoRegistro", posicaoSwapAbertura.TipoRegistro.Value);
                esParams.Add("NumeroContrato", posicaoSwapAbertura.NumeroContrato);
                esParams.Add("DataEmissao", posicaoSwapAbertura.DataEmissao.Value);
                esParams.Add("DataVencimento", posicaoSwapAbertura.DataVencimento.Value);
                esParams.Add("ValorBase", posicaoSwapAbertura.ValorBase.Value);
                esParams.Add("TipoPonta", posicaoSwapAbertura.TipoPonta.Value);

                if (!posicaoSwapAbertura.IdIndice.HasValue)
                {
                    esParams.Add("IdIndice", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdIndice", posicaoSwapAbertura.IdIndice.Value);
                }

                if (!posicaoSwapAbertura.TaxaJuros.HasValue)
                {
                    esParams.Add("TaxaJuros", DBNull.Value);
                }
                else
                {
                    esParams.Add("TaxaJuros", posicaoSwapAbertura.TaxaJuros.Value);
                }

                if (!posicaoSwapAbertura.Percentual.HasValue)
                {
                    esParams.Add("Percentual", DBNull.Value);
                }
                else
                {
                    esParams.Add("Percentual", posicaoSwapAbertura.Percentual.Value);
                }

                if (!posicaoSwapAbertura.TipoApropriacao.HasValue)
                {
                    esParams.Add("TipoApropriacao", DBNull.Value);
                }
                else
                {
                    esParams.Add("TipoApropriacao", posicaoSwapAbertura.TipoApropriacao.Value);
                }

                if (!posicaoSwapAbertura.ContagemDias.HasValue)
                {
                    esParams.Add("ContagemDias", DBNull.Value);
                }
                else
                {
                    esParams.Add("ContagemDias", posicaoSwapAbertura.ContagemDias.Value);
                }

                if (!posicaoSwapAbertura.BaseAno.HasValue)
                {
                    esParams.Add("BaseAno", DBNull.Value);
                }
                else
                {
                    esParams.Add("BaseAno", posicaoSwapAbertura.BaseAno.Value);
                }

                esParams.Add("TipoPontaContraParte", posicaoSwapAbertura.TipoPontaContraParte.Value);

                if (!posicaoSwapAbertura.IdIndiceContraParte.HasValue)
                {
                    esParams.Add("IdIndiceContraParte", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdIndiceContraParte", posicaoSwapAbertura.IdIndiceContraParte.Value);
                }

                if (!posicaoSwapAbertura.TaxaJurosContraParte.HasValue)
                {
                    esParams.Add("TaxaJurosContraParte", DBNull.Value);
                }
                else
                {
                    esParams.Add("TaxaJurosContraParte", posicaoSwapAbertura.TaxaJurosContraParte.Value);
                }

                if (!posicaoSwapAbertura.PercentualContraParte.HasValue)
                {
                    esParams.Add("PercentualContraParte", DBNull.Value);
                }
                else
                {
                    esParams.Add("PercentualContraParte", posicaoSwapAbertura.PercentualContraParte.Value);
                }

                if (!posicaoSwapAbertura.TipoApropriacaoContraParte.HasValue)
                {
                    esParams.Add("TipoApropriacaoContraParte", DBNull.Value);
                }
                else
                {
                    esParams.Add("TipoApropriacaoContraParte", posicaoSwapAbertura.TipoApropriacaoContraParte.Value);
                }

                if (!posicaoSwapAbertura.ContagemDiasContraParte.HasValue)
                {
                    esParams.Add("ContagemDiasContraParte", DBNull.Value);
                }
                else
                {
                    esParams.Add("ContagemDiasContraParte", posicaoSwapAbertura.ContagemDiasContraParte.Value);
                }

                if (!posicaoSwapAbertura.BaseAnoContraParte.HasValue)
                {
                    esParams.Add("BaseAnoContraParte", DBNull.Value);
                }
                else
                {
                    esParams.Add("BaseAnoContraParte", posicaoSwapAbertura.BaseAnoContraParte.Value);
                }

                if (!posicaoSwapAbertura.IdAgente.HasValue)
                {
                    esParams.Add("IdAgente", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdAgente", posicaoSwapAbertura.IdAgente.Value);
                }

                if (!posicaoSwapAbertura.DataUltimoEvento.HasValue)
                {
                    esParams.Add("DataUltimoEvento", DBNull.Value);
                }
                else
                {
                    esParams.Add("DataUltimoEvento", posicaoSwapAbertura.DataUltimoEvento.Value);
                }      

                esParams.Add("ComGarantia", posicaoSwapAbertura.ComGarantia);
                esParams.Add("DiasUteis", posicaoSwapAbertura.DiasUteis.Value);
                esParams.Add("DiasCorridos", posicaoSwapAbertura.DiasCorridos.Value);
                esParams.Add("ValorParte", posicaoSwapAbertura.ValorParte.Value);
                esParams.Add("ValorContraParte", posicaoSwapAbertura.ValorContraParte.Value);
                esParams.Add("Saldo", posicaoSwapAbertura.Saldo.Value);

                if (!posicaoSwapAbertura.ValorIndicePartida.HasValue)
                {
                    esParams.Add("ValorIndicePartida", DBNull.Value);
                }
                else
                {
                    esParams.Add("ValorIndicePartida", posicaoSwapAbertura.ValorIndicePartida.Value);
                }

                if (!posicaoSwapAbertura.ValorIndicePartidaContraParte.HasValue)
                {
                    esParams.Add("ValorIndicePartidaContraParte", DBNull.Value);
                }
                else
                {
                    esParams.Add("ValorIndicePartidaContraParte", posicaoSwapAbertura.ValorIndicePartidaContraParte.Value);
                }

                if (!posicaoSwapAbertura.IdEstrategia.HasValue)
                {
                    esParams.Add("IdEstrategia", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdEstrategia", posicaoSwapAbertura.IdEstrategia.Value);
                }

                esParams.Add("ValorIR", posicaoSwapAbertura.ValorIR.Value);

                if(posicaoSwapAbertura.IdAtivoCarteiraContraParte.HasValue)
                    esParams.Add("IdAtivoCarteiraContraParte", posicaoSwapAbertura.IdAtivoCarteiraContraParte.Value);
                else
                    esParams.Add("IdAtivoCarteiraContraParte", DBNull.Value);

                if (posicaoSwapAbertura.IdAtivoCarteira.HasValue)
                    esParams.Add("IdAtivoCarteira", posicaoSwapAbertura.IdAtivoCarteira.Value);
                else
                    esParams.Add("IdAtivoCarteira", DBNull.Value);

                if (!string.IsNullOrEmpty(posicaoSwapAbertura.CdAtivoBolsaContraParte))
                    esParams.Add("CdAtivoBolsaContraParte", posicaoSwapAbertura.CdAtivoBolsaContraParte);
                else
                    esParams.Add("CdAtivoBolsaContraParte", DBNull.Value);

                if (!string.IsNullOrEmpty(posicaoSwapAbertura.CdAtivoBolsa))
                    esParams.Add("CdAtivoBolsa", posicaoSwapAbertura.CdAtivoBolsa);
                else
                    esParams.Add("CdAtivoBolsa", DBNull.Value);

                StringBuilder sqlBuilder = new StringBuilder();
                sqlBuilder.Append("SET IDENTITY_INSERT PosicaoSwap ON ");
                //
                sqlBuilder.Append("INSERT INTO PosicaoSwap (");

                sqlBuilder.Append(PosicaoSwapMetadata.ColumnNames.IdPosicao);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdOperacao);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdAssessor);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.TipoRegistro);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.NumeroContrato);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.DataEmissao);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ValorBase);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.TipoPonta);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdIndice);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.TaxaJuros);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.Percentual);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.TipoApropriacao);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ContagemDias);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.BaseAno);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.TipoPontaContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdIndiceContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.TaxaJurosContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.PercentualContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.TipoApropriacaoContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ContagemDiasContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.BaseAnoContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ComGarantia);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.DiasUteis);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.DiasCorridos);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ValorParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ValorContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.Saldo);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ValorIndicePartida);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ValorIndicePartidaContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdEstrategia);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.ValorIR);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdAtivoCarteiraContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.IdAtivoCarteira);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.CdAtivoBolsaContraParte);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.CdAtivoBolsa);
                sqlBuilder.Append("," + PosicaoSwapMetadata.ColumnNames.DataUltimoEvento);

                sqlBuilder.Append(") VALUES ( ");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdPosicao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdOperacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdAssessor);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.TipoRegistro);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.NumeroContrato);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.DataEmissao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ValorBase);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.TipoPonta);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdIndice);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.TaxaJuros);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.Percentual);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.TipoApropriacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ContagemDias);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.BaseAno);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.TipoPontaContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdIndiceContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.TaxaJurosContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.PercentualContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.TipoApropriacaoContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ContagemDiasContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.BaseAnoContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ComGarantia);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.DiasUteis);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.DiasCorridos);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ValorParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ValorContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.Saldo);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ValorIndicePartida);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ValorIndicePartidaContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdEstrategia);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.ValorIR);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdAtivoCarteiraContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.IdAtivoCarteira);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.CdAtivoBolsaContraParte);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.CdAtivoBolsa);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoSwapMetadata.ColumnNames.DataUltimoEvento);
                sqlBuilder.Append(")");
                sqlBuilder.Append("SET IDENTITY_INSERT PosicaoSwap OFF ");
                esUtility u = new esUtility();
                u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
                //
                #endregion
            }
        }

	}
}
