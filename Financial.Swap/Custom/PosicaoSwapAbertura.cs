﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Swap
{
	public partial class PosicaoSwapAbertura : esPosicaoSwapAbertura
	{
        /// <summary>
        /// Retorna o valor base da posicao (IdPosicao) na data passada.
        /// </summary>
        /// <param name="IdPosicao"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaValorBasePosicao(int IdPosicao, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBase)
                 .Where(this.Query.IdPosicao.Equal(IdPosicao),
                        this.Query.DataHistorico.Equal(data));

            this.Query.Load();

            decimal valorBase = 0;
            if (this.es.HasData && this.ValorBase.HasValue)
            {
                valorBase = this.ValorBase.Value;
            }

            return valorBase;
        }

        /// <summary>
        /// Retorna o total de saldo em posições de swap do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>
        public decimal RetornaValorSaldoTotal(int idCliente, DateTime dataHistorico) {

            this.QueryReset();
            this.Query
                 .Select(this.Query.Saldo.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico == dataHistorico,
                        this.Query.ValorBase.NotEqual(0));

            this.Query.Load();

            return this.Saldo.HasValue ? this.Saldo.Value : 0;
        }
	}
}
