﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Swap.Enums;
using log4net;

namespace Financial.Swap
{
	public partial class LiquidacaoSwapCollection : esLiquidacaoSwapCollection
	{
        /// <summary>
        /// Deleta registros com o tipo de Liquidação = Vencimento.
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <param name="data"></param>
        public void DeletaVencimentoSwap(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.Equal(data),
                        this.Query.TipoLiquidacao == TipoLiquidacaoSwap.Vencimento);

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Carrega o objeto LiquidacaoSwapCollection com os campos IdOperacao, ValorAntecipacao.
        /// Filtra para TipoLiquidacao = Antecipacao.
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <param name="data"></param>
        public void BuscaAntecipacaoSwap(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.Equal(data),
                        this.Query.TipoLiquidacao == TipoLiquidacaoSwap.Antecipacao);

            this.Query.Load();                        
        }

	}
}
