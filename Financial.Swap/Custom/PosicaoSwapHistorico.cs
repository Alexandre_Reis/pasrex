﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Swap
{
	public partial class PosicaoSwapHistorico : esPosicaoSwapHistorico
	{
        /// <summary>
        /// Retorna o total de saldo em posições de swap do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>
        public decimal RetornaValorSaldoTotal(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Saldo.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();

            return this.Saldo.HasValue ? this.Saldo.Value : 0;
        }

	}
}
