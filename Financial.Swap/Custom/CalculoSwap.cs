﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Util;
using Financial.Swap.Enums;
using Financial.Swap.Exceptions;
using Financial.Util.Enums;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.Tributo.Custom;
using Financial.RendaFixa;

namespace Financial.Swap
{
    public class CalculoSwap
    {
        /// <summary>
        /// Retorna o valor corrigido para pontas de swap atualizadas pela taxa Anbid (dias úteis 252).                
        /// </summary>
        /// <param name="posicaoSwap">objeto com os parâmetros necessários para o cálculo</param>
        /// <param name="data"></param>
        /// <param name="pontaSwap">indica se é ponta parte ou contra-parte</param>
        /// <returns>valor atualizado da curva</returns>
        public decimal RetornaCurvaAnbid(PosicaoSwap posicaoSwap, DateTime data, DateTime dataInicioCorrecao, PontaSwap pontaSwap)
        {
            #region Carrega parâmetros do swap (taxa e valorAtual)
            decimal taxa;
            decimal valorAtual;
            decimal valorBase = posicaoSwap.ValorBase.Value;
            if (pontaSwap == PontaSwap.Parte)
            {
                taxa = posicaoSwap.TaxaJuros.Value;
                valorAtual = posicaoSwap.ValorParte.Value;
            }
            else
            {
                taxa = posicaoSwap.TaxaJurosContraParte.Value;
                valorAtual = posicaoSwap.ValorContraParte.Value;
            }
            #endregion

            int diasUteis = Calendario.NumeroDias(dataInicioCorrecao, data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            decimal taxaAnbid = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.ANBID, dataInicioCorrecao);
            decimal fatorAnbid = (decimal)Math.Pow((double)(1 + (taxaAnbid / 100M)), (double)(diasUteis / 252M));

            decimal fatorJuros = (decimal)Math.Pow((double)(1 + (taxa / 100M)), (double)(diasUteis / 252M));
            decimal valorAtualizado = Math.Round((valorBase * fatorJuros * fatorAnbid), 2);
            
            return valorAtualizado;
        }
        
        /// <summary>
        /// Retorna o valor corrigido para pontas de swap atualizadas por taxa pré-fixada linear 360 dias corridos.
        /// Obs: hoje existente apenas no Cetip.
        /// </summary>
        /// <param name="posicaoSwap">objeto com os parâmetros necessários para o cálculo</param>
        /// <param name="data"></param>
        /// <param name="pontaSwap">indica se é ponta parte ou contra-parte</param>
        /// <returns>valor atualizado da curva</returns>
        public decimal RetornaCurvaPre360(PosicaoSwap posicaoSwap, DateTime data, DateTime dataInicioCorrecao, PontaSwap pontaSwap)
        {
            #region Carrega parâmetros do swap
            decimal taxa;
            decimal valorAtual;
            decimal valorBase = posicaoSwap.ValorBase.Value;
            if (pontaSwap == PontaSwap.Parte)
            {
                taxa = posicaoSwap.TaxaJuros.Value;
                valorAtual = posicaoSwap.ValorParte.Value;
            }
            else
            {
                taxa = posicaoSwap.TaxaJurosContraParte.Value;
                valorAtual = posicaoSwap.ValorContraParte.Value;
            }
            #endregion

            decimal valorAtualizado = 0;
            int tipoRegistro = posicaoSwap.TipoRegistro.Value;
            int diasCorridos = Calendario.NumeroDias(dataInicioCorrecao, data);
            if (tipoRegistro == (int)TipoRegistroSwap.BMF)
            {
                throw new SwapErroConfiguracaoException("Configuração errada de Swap 360 (tipo de registro correto = Cetip).");
            }
            else
            {
                decimal fatorJuros = (decimal)Math.Round(Math.Pow((double)(1 + (taxa / 100M)),
                                                                  (double)(diasCorridos / 360M)), 9);
                valorAtualizado = Utilitario.Truncate((valorBase * fatorJuros), 2);
            }
            return valorAtualizado;
        }

        /// <summary>
        /// Retorna o valor corrigido para pontas de swap atualizadas por taxa pré-fixada 252 dias úteis.                
        /// </summary>
        /// <param name="posicaoSwap">objeto com os parâmetros necessários para o cálculo</param>
        /// <param name="data"></param>
        /// <param name="pontaSwap">indica se é ponta parte ou contra-parte</param>
        /// <returns>valor atualizado da curva</returns>
        public decimal RetornaCurvaPre252(PosicaoSwap posicaoSwap, DateTime data, DateTime dataInicioCorrecao, PontaSwap pontaSwap)
        {
            #region Carrega parâmetros do swap            
            decimal taxa;
            decimal valorAtual;
            decimal valorBase = posicaoSwap.ValorBase.Value;
            if (pontaSwap == PontaSwap.Parte)
            {
                taxa = posicaoSwap.TaxaJuros.Value;
                valorAtual = posicaoSwap.ValorParte.Value;
            }
            else
            {
                taxa = posicaoSwap.TaxaJurosContraParte.Value;
                valorAtual = posicaoSwap.ValorContraParte.Value;
            }
            #endregion

            decimal valorAtualizado = 0;
            int tipoRegistro = posicaoSwap.TipoRegistro.Value;
            int diasUteis = Calendario.NumeroDias(dataInicioCorrecao, data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            if (tipoRegistro == (int)TipoRegistroSwap.CETIP)
            {
                decimal fatorJuros = (decimal)Math.Round(Math.Pow((double)(1 + (taxa / 100M)),
                                                                  (double)(diasUteis / 252M)), 9);
                valorAtualizado = Utilitario.Truncate((valorBase * fatorJuros), 2);
            }
            else
            {
                decimal fatorJuros = (decimal)Math.Pow((double)(1 + (taxa / 100M)), (double)(diasUteis / 252M));
                valorAtualizado = Math.Round((valorBase * fatorJuros), 2);
            }

            return valorAtualizado;
        }

        /// <summary>
        /// Retorna o valor corrigido para pontas de swap atualizadas pela variação do Ouro BMF.                
        /// </summary>
        /// <param name="posicaoSwap">objeto com os parâmetros necessários para o cálculo</param>
        /// <param name="data"></param>
        /// <param name="pontaSwap">indica se é ponta parte ou contra-parte</param>
        /// <returns>valor atualizado da curva</returns>
        public decimal RetornaCurvaOuroBMF(PosicaoSwap posicaoSwap, DateTime dataInicioCorrecao, DateTime data, PontaSwap pontaSwap)
        {
            #region Carrega parâmetros do swap
            decimal taxa;
            decimal percentual;
            decimal valorAtual;
            int idIndice;
            decimal valorBase = posicaoSwap.ValorBase.Value;
            if (pontaSwap == PontaSwap.Parte)
            {
                taxa = posicaoSwap.TaxaJuros.Value;
                percentual = posicaoSwap.Percentual.Value;
                valorAtual = posicaoSwap.ValorParte.Value;
                idIndice = posicaoSwap.IdIndice.Value;
            }
            else
            {
                taxa = posicaoSwap.TaxaJurosContraParte.Value;
                percentual = posicaoSwap.PercentualContraParte.Value;
                valorAtual = posicaoSwap.ValorContraParte.Value;
                idIndice = posicaoSwap.IdIndiceContraParte.Value;
            }
            #endregion

            #region Busca cotação inicial e final (OZ1 da BMF)
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            DateTime dataInicio = Calendario.SubtraiDiaUtil(dataInicioCorrecao, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            decimal cotacaoInicio = cotacaoIndice.BuscaCotacaoIndice(idIndice, dataInicio);

            DateTime dataFim = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            decimal cotacaoFim = cotacaoIndice.BuscaCotacaoIndice(idIndice, dataFim);
            #endregion

            decimal correcaoMonetaria = Utilitario.Truncate((((cotacaoFim / cotacaoInicio) - 1)
                                                                * (percentual / 100) + 1), 8);

            int tipoRegistro = posicaoSwap.TipoRegistro.Value;
            int diasCorridos = Calendario.NumeroDias(dataInicioCorrecao, data);

            decimal fatorJuros;
            if (tipoRegistro == (int)TipoRegistroSwap.CETIP)
            {
                fatorJuros = Math.Round((decimal)Math.Pow((double)(1 + (taxa / 100M)),
                                                          (double)((diasCorridos) / 360M)), 9);
            }
            else
            {
                fatorJuros = (decimal)Math.Pow((double)(1 + (taxa / 100M)),
                                               (double)((diasCorridos) / 360M));
            }

            decimal valorAtualizado = Utilitario.Truncate(valorBase * correcaoMonetaria * fatorJuros, 2);
            return valorAtualizado;
        }

        /// <summary>
        /// Retorna o valor corrigido para pontas de swap atualizadas por índice de preços (IGPM, IGP-DI, IPCA, INPC).                
        /// </summary>
        /// <param name="posicaoSwap">objeto com os parâmetros necessários para o cálculo</param>
        /// <param name="data"></param>
        /// <param name="pontaSwap">indica se é ponta parte ou contra-parte</param>
        /// <returns>valor atualizado da curva</returns>
        public decimal RetornaCurvaIndicePreco(PosicaoSwap posicaoSwap, DateTime dataInicioCorrecao, DateTime data, PontaSwap pontaSwap)
        {
            #region Carrega parâmetros do swap
            decimal taxa;
            decimal percentual;
            decimal valorAtual;
            int idIndice;
            decimal valorBase = posicaoSwap.ValorBase.Value;
            int contagemDias;
            short baseAno;
            byte apropriacao;

            if (pontaSwap == PontaSwap.Parte)
            {
                taxa = posicaoSwap.TaxaJuros.Value;
                percentual = posicaoSwap.Percentual.Value;
                valorAtual = posicaoSwap.ValorParte.Value;
                idIndice = posicaoSwap.IdIndice.Value;
                contagemDias = posicaoSwap.ContagemDias.Value;
                baseAno = posicaoSwap.BaseAno.Value;
                apropriacao = posicaoSwap.TipoApropriacao.Value;
            }
            else
            {
                taxa = posicaoSwap.TaxaJurosContraParte.Value;
                percentual = posicaoSwap.PercentualContraParte.Value;
                valorAtual = posicaoSwap.ValorContraParte.Value;
                idIndice = posicaoSwap.IdIndiceContraParte.Value;
                contagemDias = posicaoSwap.ContagemDiasContraParte.Value;
                baseAno = posicaoSwap.BaseAnoContraParte.Value;
                apropriacao = posicaoSwap.TipoApropriacaoContraParte.Value;
            }
            #endregion

            decimal correcaoMonetaria = this.CalculaFatorCorrecao(data, dataInicioCorrecao, idIndice, percentual, contagemDias, posicaoSwap.DataVencimento.Value);

            int tipoRegistro = posicaoSwap.TipoRegistro.Value;

            int dias;
            if (contagemDias.Equals((int)ContagemDiasSwap.Corridos))
                dias = Calendario.NumeroDias(dataInicioCorrecao, data);
            else
                dias = Calendario.NumeroDias(dataInicioCorrecao, data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            decimal baseAnoCalculo = 252M;
            switch (baseAno)
            {
                case (int)BaseCalculoSwap.Base252:
                    baseAnoCalculo = 252M;
                    break;
                case (int)BaseCalculoSwap.Base360:
                    baseAnoCalculo = 360M;
                    break;
                case (int)BaseCalculoSwap.Base365:
                    baseAnoCalculo = 365M;
                    break;
            }

            decimal fatorJuros;
            decimal valorAtualizado;
            if (tipoRegistro == (int)TipoRegistroSwap.CETIP)
            {
                if(apropriacao.Equals((byte)TipoApropriacaoSwap.Exponencial))
                    fatorJuros = Math.Round((decimal)Math.Pow((double)(1 + (taxa / 100M)), (double)((dias) / baseAnoCalculo)), 9);
                else
                    fatorJuros = Math.Round((decimal)(double)(1 + (taxa / 100M) * (dias / baseAnoCalculo)), 9);

                valorAtualizado = Utilitario.Truncate(valorBase * correcaoMonetaria * fatorJuros, 2);
            }
            else
            {
                if (apropriacao.Equals((byte)TipoApropriacaoSwap.Exponencial))
                    fatorJuros = (decimal)Math.Pow((double)(1 + (taxa / 100M)), (double)((dias) / baseAnoCalculo));
                else
                    fatorJuros = (decimal)(1 + (taxa / 100M) * (dias / baseAnoCalculo));

                valorAtualizado = Math.Round(valorBase * correcaoMonetaria * fatorJuros, 2);
            }

            return valorAtualizado;
        }

        /// <summary>
        /// Calcula correção monetária para qq indice.
        /// </summary>
        /// <param name="dataCalculo"></param>
        /// <param name="dataBase"></param>
        /// <param name="papelRendaFixa"></param>
        /// <param name="tituloRendaFixa"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="indice"></param>
        /// <param name="taxa"></param>
        /// <returns></returns>
        public decimal CalculaFatorCorrecao(DateTime dataCalculo, DateTime dataBase, int idIndice, decimal percentual, int contagemDias, DateTime dataVencimento)
        {
            Indice indice = new Indice();
            indice.LoadByPrimaryKey((short)idIndice);

            decimal fatorIndice = 1;

            byte tipoIndice = indice.Tipo.Value;
            if (indice.IdIndiceBase.HasValue)
            {
                idIndice = indice.IdIndiceBase.Value;
            }

            if (tipoIndice == (byte)TipoIndice.Decimal &&
                    (indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal ||
                     indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_1 ||
                     indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_2 ||
                     indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_3))
            {
                if (contagemDias == (byte)ContagemDiasSwap.Corridos)
                {
                    fatorIndice = CalculoFinanceiro.CalculaFatorIndicePreco(dataBase, dataCalculo, dataVencimento.Day, indice, percentual);
                }
                else
                {
                    fatorIndice = CalculoFinanceiro.CalculaFatorIndicePreco(dataBase, dataCalculo, dataVencimento.Day,
                                                            indice, percentual, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
            }
            else
            {
                if (tipoIndice == (int)TipoIndice.Decimal)
                {
                    fatorIndice = CalculoFinanceiro.CalculaFatorIndiceDecimal(dataBase, dataCalculo, idIndice, percentual);
                }
                else if (indice.IdIndice.Value == (byte)ListaIndiceFixo.TR)
                {
                    fatorIndice = CalculoFinanceiro.CalculaFatorTR(dataBase, dataCalculo, dataVencimento.Day, percentual);
                }
                else
                {
                    //Para índices percentuais, não leva em conta o próprio dia de cálculo
                    DateTime dataFimParam = dataCalculo;

                    if (dataBase != dataCalculo)
                    {
                        dataFimParam = Calendario.SubtraiDiaUtil(dataCalculo, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    //
                    fatorIndice = CalculoFinanceiro.CalculaFatorIndicePercentual(dataBase, dataFimParam, indice, percentual, true);

                }
            }

            return fatorIndice;
        }

        /// <summary>
        /// Retorna o valor corrigido para pontas de swap atualizadas pelo CDI ou pela SELIC.
        /// Obs: somente para swaps registrados no Cetip.
        /// </summary>
        /// <param name="posicaoSwap">objeto com os parâmetros necessários para o cálculo</param>
        /// <param name="dataFim"></param>
        /// <param name="pontaSwap">indica se é ponta parte ou contra-parte</param>
        /// <returns>valor atualizado da curva</returns>
        public decimal RetornaCurvaCDI_SELIC_Cetip(PosicaoSwap posicaoSwap, DateTime dataInicioCorrecao, DateTime dataFim, PontaSwap pontaSwap)
        {
            #region Carrega parâmetros do swap
            DateTime dataVencimento = posicaoSwap.DataVencimento.Value;
            decimal taxa;
            decimal percentual;
            decimal valorAtual;
            int idIndice;
            decimal valorBase = posicaoSwap.ValorBase.Value;
            if (pontaSwap == PontaSwap.Parte)
            {
                taxa = posicaoSwap.TaxaJuros.Value;
                percentual = posicaoSwap.Percentual.Value;
                valorAtual = posicaoSwap.ValorParte.Value;
                idIndice = posicaoSwap.IdIndice.Value;
            }
            else
            {
                taxa = posicaoSwap.TaxaJurosContraParte.Value;
                percentual = posicaoSwap.PercentualContraParte.Value;
                valorAtual = posicaoSwap.ValorContraParte.Value;
                idIndice = posicaoSwap.IdIndiceContraParte.Value;
            }
            #endregion

            #region Calcula produtório do índice (CDI ou SELIC)
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataFim, 1);
            decimal correcaoMonetaria = 1;

            CotacaoIndiceCollection cotacaoIndiceCollection = new CotacaoIndiceCollection();
            cotacaoIndiceCollection.BuscaCotacao(dataInicioCorrecao, dataAnterior, idIndice);

            foreach (CotacaoIndice cotacaoIndice in cotacaoIndiceCollection)
            {
                DateTime data = cotacaoIndice.Data.Value;
                decimal cotacao = cotacaoIndice.Valor.Value;
                decimal fatorDia = Math.Round((decimal)Math.Pow(((double)(cotacao / 100M + 1)), (double)(1 / 252M)), 8);

                fatorDia = Utilitario.Truncate((fatorDia - 1) * (percentual / 100), 16) + 1;
                correcaoMonetaria = correcaoMonetaria * Math.Round(fatorDia, 16);
            }

            correcaoMonetaria = Math.Round(correcaoMonetaria, 16);
            #endregion

            //Passa de 360 dias corridos para 252 dias úteis
            DateTime dataTransicaoContagemDias = new DateTime(2000, 01, 09);
            //Taxa negativa passa a ser contada como positiva
            DateTime dataTransicaoTaxaNegativa = new DateTime(2002, 12, 16);

            decimal valorAtualizado;
            int tipoRegistro = posicaoSwap.TipoRegistro.Value;

            decimal fatorJuros;
            if (dataInicioCorrecao > dataTransicaoContagemDias)
            {
                int diasUteis = Calendario.NumeroDias(dataInicioCorrecao, dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                if (taxa < 0 && dataInicioCorrecao >= dataTransicaoTaxaNegativa)
                {
                    fatorJuros = (decimal)Math.Abs(Math.Round(Math.Abs(Math.Pow((double)(1 + (taxa / 100M)),
                                                                                (double)(diasUteis / 252M))), 9));
                }
                else
                {
                    fatorJuros = (decimal)Math.Round(Math.Pow((double)(1 + (taxa / 100M)),
                                                              (double)(diasUteis / 252M)), 9);
                }
            }
            else
            {
                int diasCorridos = Calendario.NumeroDias(dataInicioCorrecao, dataFim);
                if (taxa < 0 && dataInicioCorrecao >= dataTransicaoTaxaNegativa)
                {
                    fatorJuros = (decimal)Math.Abs(Math.Round(Math.Abs(Math.Pow((double)(1 + (taxa / 100M)),
                                                                                (double)(diasCorridos / 360M))), 9));
                }
                else
                {
                    fatorJuros = (decimal)Math.Round(Math.Pow((double)(1 + (taxa / 100M)),
                                                              (double)(diasCorridos / 360M)), 9);
                }
            }
            valorAtualizado = Utilitario.Truncate((valorBase * correcaoMonetaria * fatorJuros), 2);

            return valorAtualizado;
        }

        /// <summary>
        /// Retorna o valor corrigido para pontas de swap atualizadas pelo CDI ou pela SELIC.
        /// Obs: somente para swaps registrados na BMF.
        /// </summary>
        /// <param name="posicaoSwap">objeto com os parâmetros necessários para o cálculo</param>
        /// <param name="dataFim"></param>
        /// <param name="pontaSwap">indica se é ponta parte ou contra-parte</param>
        /// <returns>valor atualizado da curva</returns>
        public decimal RetornaCurvaCDI_SELIC_BMF(PosicaoSwap posicaoSwap, DateTime dataInicioCorrecao, DateTime dataFim, PontaSwap pontaSwap)
        {
            #region Carrega parâmetros do swap
            DateTime dataVencimento = posicaoSwap.DataVencimento.Value;
            decimal taxa;
            decimal percentual;
            decimal valorAtual;
            int idIndice;
            decimal valorBase = posicaoSwap.ValorBase.Value;
            if (pontaSwap == PontaSwap.Parte)
            {
                taxa = posicaoSwap.TaxaJuros.Value;
                percentual = posicaoSwap.Percentual.Value;
                valorAtual = posicaoSwap.ValorParte.Value;
                idIndice = posicaoSwap.IdIndice.Value;
            }
            else
            {
                taxa = posicaoSwap.TaxaJurosContraParte.Value;
                percentual = posicaoSwap.PercentualContraParte.Value;
                valorAtual = posicaoSwap.ValorContraParte.Value;
                idIndice = posicaoSwap.IdIndiceContraParte.Value;
            }
            #endregion

            #region Calcula produtório do índice (CDI ou SELIC)
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataFim, 1);
            decimal correcaoMonetaria = 1;

            CotacaoIndiceCollection cotacaoIndiceCollection = new CotacaoIndiceCollection();
            cotacaoIndiceCollection.BuscaCotacao(dataInicioCorrecao, dataAnterior, idIndice);

            foreach (CotacaoIndice cotacaoIndice in cotacaoIndiceCollection)
            {
                DateTime data = cotacaoIndice.Data.Value;
                decimal cotacao = cotacaoIndice.Valor.Value;
                decimal fatorDia = (decimal)Math.Pow(((double)(cotacao / 100M + 1)), (double)(1 / 252M));

                fatorDia = Utilitario.Truncate((fatorDia - 1) * (percentual / 100) + 1, 16);
                correcaoMonetaria = correcaoMonetaria * fatorDia;
            }

            correcaoMonetaria = Math.Round(correcaoMonetaria, 16);
            #endregion

            //Passa de 360 dias corridos para 252 dias úteis
            DateTime dataTransicaoContagemDias = new DateTime(2000, 01, 09);

            decimal valorAtualizado;
            int tipoRegistro = posicaoSwap.TipoRegistro.Value;

            decimal fatorJuros;
            if (dataInicioCorrecao > dataTransicaoContagemDias)
            {
                int diasUteis = Calendario.NumeroDias(dataInicioCorrecao, dataVencimento, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                fatorJuros = (decimal)Math.Pow((double)(1 + (taxa / 100M)),
                                               (double)(diasUteis / 252M));
            }
            else
            {
                int diasCorridos = Calendario.NumeroDias(dataInicioCorrecao, dataVencimento);
                fatorJuros = (decimal)Math.Pow((double)(1 + (taxa / 100M)),
                                               (double)(diasCorridos / 360M));
            }
            valorAtualizado = Math.Round((valorBase * correcaoMonetaria * fatorJuros), 2);

            return valorAtualizado;
        }

        /// <summary>
        /// Retorna o valor corrigido para pontas de swap atualizadas por índice de ações.
        /// Obs: hoje existente apenas na BMF.
        /// </summary>
        /// <param name="posicaoSwap">objeto com os parâmetros necessários para o cálculo</param>
        /// <param name="data"></param>
        /// <param name="pontaSwap">indica se é ponta parte ou contra-parte</param>
        /// <returns>valor atualizado da curva</returns>
        public decimal RetornaCurvaIndiceAcoes(PosicaoSwap posicaoSwap, DateTime dataInicioCorrecao, DateTime data, PontaSwap pontaSwap)
        {
            #region Carrega parâmetros do swap
            decimal taxa;
            decimal percentual;
            decimal valorAtual;
            int idIndice;
            decimal valorBase = posicaoSwap.ValorBase.Value;
            if (pontaSwap == PontaSwap.Parte)
            {
                taxa = posicaoSwap.TaxaJuros.Value;
                percentual = posicaoSwap.Percentual.Value;
                valorAtual = posicaoSwap.ValorParte.Value;
                idIndice = posicaoSwap.IdIndice.Value;
            }
            else
            {
                taxa = posicaoSwap.TaxaJurosContraParte.Value;
                percentual = posicaoSwap.PercentualContraParte.Value;
                valorAtual = posicaoSwap.ValorContraParte.Value;
                idIndice = posicaoSwap.IdIndiceContraParte.Value;
            }
            #endregion

            #region Busca cotação inicial e final
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            DateTime dataInicio = Calendario.SubtraiDiaUtil(dataInicioCorrecao, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            decimal cotacaoInicio = cotacaoIndice.BuscaCotacaoIndice(idIndice, dataInicio);

            DateTime dataFim = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            decimal cotacaoFim = cotacaoIndice.BuscaCotacaoIndice(idIndice, dataFim);
            #endregion

            decimal valorAtualizado;
            int diasUteis = Calendario.NumeroDias(dataInicioCorrecao, data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            decimal fatorJuros = (decimal)Math.Pow((double)(1 + (taxa / 100M)),
                                               (double)(diasUteis / 252M));

            decimal correcaoMonetaria = Math.Round((((cotacaoFim / cotacaoInicio) - 1) * (percentual / 100) + 1), 4);

            valorAtualizado = Math.Round((valorBase * correcaoMonetaria * fatorJuros), 2);

            return valorAtualizado;
        }

        /// <summary>
        /// Retorna o valor corrigido para pontas de swap atualizadas pelo Dólar (exponencial 360 dias corridos).
        /// Obs: hoje existente apenas no Cetip.
        /// </summary>
        /// <param name="posicaoSwap">objeto com os parâmetros necessários para o cálculo</param>
        /// <param name="data"></param>
        /// <param name="pontaSwap">indica se é ponta parte ou contra-parte</param>
        /// <returns>valor atualizado da curva</returns>
        public decimal RetornaCurvaDolarExponencial(PosicaoSwap posicaoSwap, DateTime dataInicioCorrecao, DateTime data, PontaSwap pontaSwap)
        {
            #region Carrega parâmetros do swap
            decimal taxa;
            decimal percentual;
            decimal valorAtual;
            int idIndice;
            decimal valorBase = posicaoSwap.ValorBase.Value;
            if (pontaSwap == PontaSwap.Parte)
            {
                taxa = posicaoSwap.TaxaJuros.Value;
                percentual = posicaoSwap.Percentual.Value;
                valorAtual = posicaoSwap.ValorParte.Value;
                idIndice = posicaoSwap.IdIndice.Value;
            }
            else
            {
                taxa = posicaoSwap.TaxaJurosContraParte.Value;
                percentual = posicaoSwap.PercentualContraParte.Value;
                valorAtual = posicaoSwap.ValorContraParte.Value;
                idIndice = posicaoSwap.IdIndiceContraParte.Value;
            }
            #endregion

            #region Busca cotação inicial e final
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            DateTime dataInicio = Calendario.SubtraiDiaUtil(dataInicioCorrecao, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            decimal cotacaoInicio = cotacaoIndice.BuscaCotacaoIndice(idIndice, dataInicio);

            DateTime dataFim = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            decimal cotacaoFim = cotacaoIndice.BuscaCotacaoIndice(idIndice, dataFim);
            #endregion

            decimal valorAtualizado;
            int tipoRegistro = posicaoSwap.TipoRegistro.Value;
            int diasCorridos = Calendario.NumeroDias(dataInicioCorrecao, data);
            
            decimal fatorJuros;
            fatorJuros = Math.Round((decimal)Math.Pow((double)(1 + (taxa / 100M)),
                                                      (double)(diasCorridos / 360M)), 9);
                
            decimal correcaoMonetaria = Utilitario.Truncate((((cotacaoFim / cotacaoInicio) - 1) * (percentual / 100) + 1), 8);

            valorAtualizado = Utilitario.Truncate((valorBase * correcaoMonetaria * fatorJuros), 2);
            return valorAtualizado;
        }

        /// <summary>
        /// Retorna o valor corrigido para pontas de swap atualizadas pela TJLP.        
        /// </summary>
        /// <param name="posicaoSwap">objeto com os parâmetros necessários para o cálculo</param>
        /// <param name="data"></param>
        /// <param name="pontaSwap">indica se é ponta parte ou contra-parte</param>
        /// <returns>valor atualizado da curva</returns>
        public decimal RetornaCurvaTJLP(PosicaoSwap posicaoSwap, DateTime dataInicioCorrecao, DateTime data, PontaSwap pontaSwap)
        {
            #region Carrega parâmetros do swap
            decimal taxa;
            decimal percentual;
            decimal valorAtual;
            int idIndice;
            decimal valorBase = posicaoSwap.ValorBase.Value;
            if (pontaSwap == PontaSwap.Parte)
            {
                taxa = posicaoSwap.TaxaJuros.Value;
                percentual = posicaoSwap.Percentual.Value;
                valorAtual = posicaoSwap.ValorParte.Value;
                idIndice = posicaoSwap.IdIndice.Value;
            }
            else
            {
                taxa = posicaoSwap.TaxaJurosContraParte.Value;
                percentual = posicaoSwap.PercentualContraParte.Value;
                valorAtual = posicaoSwap.ValorContraParte.Value;
                idIndice = posicaoSwap.IdIndiceContraParte.Value;
            }
            #endregion

            #region Calcula produtório da TJLP
            DateTime dataAux = dataInicioCorrecao;
            decimal correcaoMonetaria = 1;
            while (dataAux < data) //Calcula o produtório até d-1 da data
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                decimal cotacao = cotacaoIndice.BuscaCotacaoIndice(idIndice, dataAux);
                decimal fatorDia = (decimal)Math.Pow(((double)(cotacao / 100M + 1)), (double)(1 / 252M));

                fatorDia = Math.Round((fatorDia - 1) * (percentual / 100) + 1, 10);
                fatorDia = Utilitario.Truncate(fatorDia, 8);
                correcaoMonetaria = correcaoMonetaria * Math.Round(fatorDia, 10);
                correcaoMonetaria = Utilitario.Truncate(correcaoMonetaria, 8);

                dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }
            correcaoMonetaria = Math.Round(correcaoMonetaria, 16);
            #endregion
                        
            decimal valorAtualizado;
            int tipoRegistro = posicaoSwap.TipoRegistro.Value;

            decimal fatorJuros;
            int diasCorridos = Calendario.NumeroDias(dataInicioCorrecao, data);
            fatorJuros = (decimal)Math.Round(Math.Pow((double)(1 + (taxa / 100M)),
                                                      (double)(diasCorridos / 360M)), 9);
            
            valorAtualizado = Utilitario.Truncate((valorBase * correcaoMonetaria * fatorJuros), 2);

            return valorAtualizado;
        }

        /// <summary>
        /// Retorna o valor corrigido para pontas de swap atualizadas por qualquer índice indexado.
        /// </summary>
        /// <param name="posicaoSwap">objeto com os parâmetros necessários para o cálculo</param>
        /// <param name="data"></param>
        /// <param name="pontaSwap">indica se é ponta parte ou contra-parte</param>
        /// <returns>valor atualizado da curva</returns>
        public decimal RetornaCurvaIndexadoD0(PosicaoSwap posicaoSwap, DateTime dataInicioCorrecao, DateTime data, PontaSwap pontaSwap)
        {
            #region Carrega parâmetros do swap
            decimal taxa;
            decimal percentual;
            decimal valorAtual;
            int idIndice;
            int baseCalculo; //Somente se aplica a Curva Outros
            int contagemDias; //Somente se aplica a Curva Outros
            int tipoApropriacao; //Somente se aplica a Curva Outros
            decimal valorBase = posicaoSwap.ValorBase.Value;
            if (pontaSwap == PontaSwap.Parte)
            {
                taxa = posicaoSwap.TaxaJuros.Value;
                percentual = posicaoSwap.Percentual.Value;
                valorAtual = posicaoSwap.ValorParte.Value;
                idIndice = posicaoSwap.IdIndice.Value;                
                baseCalculo = posicaoSwap.BaseAno.Value;
                contagemDias = posicaoSwap.ContagemDias.Value;
                tipoApropriacao = posicaoSwap.TipoApropriacao.Value;
            }
            else
            {
                taxa = posicaoSwap.TaxaJurosContraParte.Value;
                percentual = posicaoSwap.PercentualContraParte.Value;
                valorAtual = posicaoSwap.ValorContraParte.Value;
                idIndice = posicaoSwap.IdIndiceContraParte.Value;
                baseCalculo = posicaoSwap.BaseAnoContraParte.Value;
                contagemDias = posicaoSwap.ContagemDiasContraParte.Value;
                tipoApropriacao = posicaoSwap.TipoApropriacaoContraParte.Value;
            }
            #endregion

            Indice indice = new Indice();
            indice.LoadByPrimaryKey((short)idIndice);

            int tipoIndice = indice.Tipo.Value;
            byte tipoDivulgacao = indice.TipoDivulgacao.Value;

            decimal fatorIndice = 1;
            //Testa se trabalha com indice informado na partida
            if ((pontaSwap == PontaSwap.Parte && posicaoSwap.ValorIndicePartida.HasValue && posicaoSwap.ValorIndicePartida.Value != 0) ||
                (pontaSwap == PontaSwap.ContraParte && posicaoSwap.ValorIndicePartidaContraParte.HasValue && posicaoSwap.ValorIndicePartidaContraParte.Value != 0))
            {
                decimal cotacaoInicio = 0;
                if (pontaSwap == PontaSwap.Parte)
                {
                    cotacaoInicio = posicaoSwap.ValorIndicePartida.Value;
                }
                else
                {
                    cotacaoInicio = posicaoSwap.ValorIndicePartidaContraParte.Value;
                }

                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                cotacaoIndice.BuscaCotacaoIndice(idIndice, data);
                decimal cotacaoFim = cotacaoIndice.Valor.Value;

                if (cotacaoInicio != 0) //Se cotação do índice está zerada, assume fator = 1
                {
                    fatorIndice = (((cotacaoFim / cotacaoInicio) - 1) * (percentual / 100M) + 1);
                }

            }
            else
            {
                fatorIndice = CalculoFinanceiro.CalculaFatorIndiceDecimal(dataInicioCorrecao, data, idIndice, percentual);                
            }

            #region Fator juros
            decimal fatorJuros;
            if (tipoApropriacao == (int)TipoApropriacaoSwap.Exponencial)
            {
                if (contagemDias == (int)ContagemDiasSwap.Uteis)
                {   //Pre Exponencial Dias Uteis
                    fatorJuros = CalculoFinanceiro.CalculaFatorPreExponencial(dataInicioCorrecao, data, taxa,
                                                                             (BaseCalculo)baseCalculo,
                                                                              LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {   //Pre Exponencial Dias Corridos
                    fatorJuros = CalculoFinanceiro.CalculaFatorPreExponencial(dataInicioCorrecao, data, taxa,
                                                                             (BaseCalculo)baseCalculo);
                }
            }
            else
            {
                if (contagemDias == (int)ContagemDiasSwap.Uteis)
                {   //Pre Linear Dias Uteis
                    fatorJuros = CalculoFinanceiro.CalculaFatorPreLinear(dataInicioCorrecao, data, taxa, (BaseCalculo)baseCalculo,
                                                                         LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {   //Pre Linear Dias Corridos
                    fatorJuros = CalculoFinanceiro.CalculaFatorPreLinear(dataInicioCorrecao, data, taxa, (BaseCalculo)baseCalculo);
                }
            }
            #endregion

            decimal valorAtualizado = Math.Round((valorBase * fatorIndice * fatorJuros), 2);

            return valorAtualizado;
        }

        /// <summary>
        /// Retorna o valor corrigido para pontas de swap atualizadas por moeda (Dólar, Euro, Iene, Peso Argentino).
        /// </summary>
        /// <param name="posicaoSwap">objeto com os parâmetros necessários para o cálculo</param>
        /// <param name="data"></param>
        /// <param name="pontaSwap">indica se é ponta parte ou contra-parte</param>
        /// <returns>valor atualizado da curva</returns>
        public decimal RetornaCurvaIndiceDecimal(PosicaoSwap posicaoSwap, DateTime dataInicioCorrecao, DateTime data, PontaSwap pontaSwap)
        {
            #region Carrega parâmetros do swap
            decimal taxa;
            decimal percentual;
            decimal valorAtual;
            decimal valorBase = posicaoSwap.ValorBase.Value;
            int idIndice;
            if (pontaSwap == PontaSwap.Parte)
            {
                taxa = posicaoSwap.TaxaJuros.Value;
                percentual = posicaoSwap.Percentual.Value;
                valorAtual = posicaoSwap.ValorParte.Value;
                idIndice = posicaoSwap.IdIndice.Value;
            }
            else
            {
                taxa = posicaoSwap.TaxaJurosContraParte.Value;
                percentual = posicaoSwap.PercentualContraParte.Value;
                valorAtual = posicaoSwap.ValorContraParte.Value;
                idIndice = posicaoSwap.IdIndiceContraParte.Value;
            }
            #endregion

            #region Busca cotação inicial e final
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            decimal cotacaoInicio = 0;
            if ((pontaSwap == PontaSwap.Parte && posicaoSwap.ValorIndicePartida.HasValue && posicaoSwap.ValorIndicePartida != 0) ||
                (pontaSwap == PontaSwap.ContraParte && posicaoSwap.ValorIndicePartidaContraParte.HasValue && posicaoSwap.ValorIndicePartidaContraParte != 0))
            {
                if (pontaSwap == PontaSwap.Parte)
                {
                    cotacaoInicio = posicaoSwap.ValorIndicePartida.Value;
                }
                else
                {
                    cotacaoInicio = posicaoSwap.ValorIndicePartidaContraParte.Value;
                }
            }
            else
            {
                DateTime dataInicio = Calendario.SubtraiDiaUtil(dataInicioCorrecao, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                cotacaoInicio = cotacaoIndice.BuscaCotacaoIndice(idIndice, dataInicio);
            }

            DateTime dataFim = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            decimal cotacaoFim = cotacaoIndice.BuscaCotacaoIndice(idIndice, dataFim);
            #endregion

            decimal valorAtualizado;
            int tipoRegistro = posicaoSwap.TipoRegistro.Value;
            int diasCorridos = Calendario.NumeroDias(dataInicioCorrecao, data);
            if (tipoRegistro == (int)TipoRegistroSwap.CETIP)
            {
                #region CETIP
                decimal fatorJuros;
                if (taxa < 0)
                {
                    fatorJuros = Math.Abs((Math.Round(Math.Abs((((taxa / 36000) * (diasCorridos)))), 9)) - 1);
                }
                else
                {
                    fatorJuros = Math.Round((1 + ((taxa / 36000) * (diasCorridos))), 9);
                }
                decimal correcaoMonetaria = Utilitario.Truncate((((cotacaoFim / cotacaoInicio) - 1) * (percentual / 100) + 1), 8);
                #endregion

                valorAtualizado = Utilitario.Truncate((valorBase * correcaoMonetaria * fatorJuros), 2);
            }
            else
            {
                #region BMF
                decimal fatorJuros;
                if (taxa < 0)
                {
                    fatorJuros = Math.Abs((Math.Abs((((taxa / 36000) * (diasCorridos))))) - 1);
                }
                else
                {
                    fatorJuros = (1 + ((taxa / 36000) * (diasCorridos)));
                }
                decimal correcaoMonetaria = Math.Round((((cotacaoFim / cotacaoInicio) - 1) * (percentual / 100) + 1), 8);
                #endregion

                valorAtualizado = Math.Round((valorBase * correcaoMonetaria * fatorJuros), 2);
            }
            return valorAtualizado;
        }

        /// <summary>
        /// Retorna o valor corrigido para indices percentuais genéricos.
        /// </summary>
        /// <param name="posicaoSwap">objeto com os parâmetros necessários para o cálculo</param>
        /// <param name="dataFim"></param>
        /// <param name="pontaSwap">indica se é ponta parte ou contra-parte</param>
        /// <returns>valor atualizado da curva</returns>
        public decimal RetornaCurvaIndicePercentual(PosicaoSwap posicaoSwap, DateTime dataInicioCorrecao, DateTime dataFim, PontaSwap pontaSwap)
        {
            DateTime dataVencimento = posicaoSwap.DataVencimento.Value;
            decimal taxa;
            decimal percentual;
            decimal valorAtual;
            int idIndice;
            int baseAno;
            decimal valorBase = posicaoSwap.ValorBase.Value;
            byte contagemDias = 0;
            byte tipoApropriacao = 0;
            #region Carrega parâmetros do swap dependendo de cada ponta
            if (pontaSwap == PontaSwap.Parte)
            {
                taxa = posicaoSwap.TaxaJuros.Value;
                percentual = posicaoSwap.Percentual.Value;
                valorAtual = posicaoSwap.ValorParte.Value;
                idIndice = posicaoSwap.IdIndice.Value;
                baseAno = (int)posicaoSwap.BaseAno.Value;
                contagemDias = posicaoSwap.ContagemDias.Value;
                tipoApropriacao = posicaoSwap.TipoApropriacao.Value;
            }
            else
            {
                taxa = posicaoSwap.TaxaJurosContraParte.Value;
                percentual = posicaoSwap.PercentualContraParte.Value;
                valorAtual = posicaoSwap.ValorContraParte.Value;
                idIndice = posicaoSwap.IdIndiceContraParte.Value;
                baseAno = (int)posicaoSwap.BaseAnoContraParte.Value;
                contagemDias = posicaoSwap.ContagemDiasContraParte.Value;
                tipoApropriacao = posicaoSwap.TipoApropriacaoContraParte.Value;
            }
            #endregion

            #region Calcula produtório do índice
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataFim, 1);
            decimal correcaoMonetaria = 1;

            CotacaoIndiceCollection cotacaoIndiceCollection = new CotacaoIndiceCollection();
            cotacaoIndiceCollection.BuscaCotacao(dataInicioCorrecao, dataAnterior, idIndice);

            foreach (CotacaoIndice cotacaoIndice in cotacaoIndiceCollection)
            {
                DateTime data = cotacaoIndice.Data.Value;
                decimal cotacao = cotacaoIndice.Valor.Value;
                decimal fatorDia = (decimal)Math.Pow(((double)(cotacao / 100M + 1)), (double)(1 / 252M));

                fatorDia = Utilitario.Truncate((fatorDia - 1) * (percentual / 100) + 1, 16);
                correcaoMonetaria = correcaoMonetaria * fatorDia;
            }

            correcaoMonetaria = Math.Round(correcaoMonetaria, 16);
            #endregion

            decimal valorAtualizado;
            
            decimal fatorJuros;
            int dias = 0;
            if (contagemDias == (byte)ContagemDiasSwap.Uteis)
            {
                dias = Calendario.NumeroDias(dataInicioCorrecao, dataVencimento, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }
            else
            {
                dias = Calendario.NumeroDias(dataInicioCorrecao, dataVencimento);
            }

            if (tipoApropriacao == (byte)TipoApropriacaoSwap.Exponencial)
            {
                fatorJuros = (decimal)Math.Pow((double)(1 + (taxa / 100M)), (double)(dias / baseAno));
            }
            else
            {
                fatorJuros = CalculoFinanceiro.CalculaFatorPreLinear(dataInicioCorrecao, dataFim, taxa, (BaseCalculo)baseAno);
            }
            
            valorAtualizado = Math.Round((valorBase * correcaoMonetaria * fatorJuros), 2);

            return valorAtualizado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="posicaoSwap"></param>
        /// <param name="pontaSwap"></param>
        /// <returns></returns>
        public decimal CalculaPonta(DateTime data, PosicaoSwap posicaoSwap, PontaSwap pontaSwap)
        {
            EventoSwap eventoSwap = new EventoSwap();
            decimal valorPonta = 0;

            int tipoPonta = pontaSwap == PontaSwap.Parte ? posicaoSwap.TipoPonta.Value : posicaoSwap.TipoPontaContraParte.Value;           
            decimal valorBase = posicaoSwap.ValorBase.Value;
            DateTime dataInicioCorrecao = posicaoSwap.DataUltimoEvento.GetValueOrDefault(posicaoSwap.DataEmissao.Value);

            if (tipoPonta == (int)TipoPontaSwap.PreFixado)
            {
                #region Pre-Fixado (252, 360)
                int baseAno = pontaSwap == PontaSwap.Parte ? posicaoSwap.BaseAno.Value : posicaoSwap.BaseAnoContraParte.Value;           
                if (baseAno == (int)BaseCalculoSwap.Base252)
                {
                    valorPonta = this.RetornaCurvaPre252(posicaoSwap, data, dataInicioCorrecao, pontaSwap);
                }
                else
                {
                    valorPonta = this.RetornaCurvaPre360(posicaoSwap, data, dataInicioCorrecao, pontaSwap);
                }
                #endregion
            }
            else if (tipoPonta == (int)TipoPontaSwap.Indexado)
            {
                #region Indexado + Tx Juros
                int idIndice = pontaSwap == PontaSwap.Parte ? posicaoSwap.IdIndice.Value : posicaoSwap.IdIndiceContraParte.Value;           
                int tipoRegistro = posicaoSwap.TipoRegistro.Value;

                switch (idIndice)
                {
                    case ListaIndiceFixo.ANBID:
                        valorPonta = this.RetornaCurvaAnbid(posicaoSwap, data, dataInicioCorrecao, pontaSwap);
                        break;

                    #region CDI, SELIC
                    case ListaIndiceFixo.CDI:
                    case ListaIndiceFixo.SELIC:
                        if (tipoRegistro == (int)TipoRegistroSwap.CETIP)
                        {
                            valorPonta = this.RetornaCurvaCDI_SELIC_Cetip(posicaoSwap, dataInicioCorrecao, data, pontaSwap);
                        }
                        else
                        {
                            valorPonta = this.RetornaCurvaCDI_SELIC_BMF(posicaoSwap, dataInicioCorrecao, data, pontaSwap);
                        }
                        break;
                    #endregion

                    #region Moeda (Dolar, Euro, Yene, Peso Argentino)
                    case ListaIndiceFixo.PTAX_800COMPRA:
                    case ListaIndiceFixo.PTAX_800FLU:
                    case ListaIndiceFixo.PTAX_800VENDA:
                    case ListaIndiceFixo.EURO:
                    case ListaIndiceFixo.YENE:
                    case ListaIndiceFixo.PESOARGENTINO:
                        if (tipoRegistro == (int)TipoRegistroSwap.BMF)
                        {
                            if (idIndice != ListaIndiceFixo.PTAX_800COMPRA & idIndice != ListaIndiceFixo.PTAX_800VENDA)
                            {
                                throw new SwapErroConfiguracaoException("Configuração errada de Swap Dólar (tipo de cotação não permitida para registro BMF).");
                            }
                            valorPonta = this.RetornaCurvaIndiceDecimal(posicaoSwap, dataInicioCorrecao, data, pontaSwap);
                        }
                        else
                        {
                            if (idIndice == ListaIndiceFixo.PTAX_800COMPRA || idIndice == ListaIndiceFixo.PTAX_800FLU ||
                                idIndice == ListaIndiceFixo.PTAX_800VENDA)
                            {
                                int tipo = posicaoSwap.TipoApropriacao.Value;
                                if (tipo == (int)TipoApropriacaoSwap.Exponencial)
                                {
                                    valorPonta = this.RetornaCurvaDolarExponencial(posicaoSwap, dataInicioCorrecao, data, pontaSwap);
                                }
                                else
                                {
                                    valorPonta = this.RetornaCurvaIndiceDecimal(posicaoSwap, dataInicioCorrecao, data, pontaSwap);
                                }
                            }
                            else
                            {
                                valorPonta = this.RetornaCurvaIndiceDecimal(posicaoSwap, dataInicioCorrecao, data, pontaSwap);
                            }
                        }
                        break;
                    #endregion

                    case ListaIndiceFixo.OUROBMF_FECHA:
                    case ListaIndiceFixo.OUROBMF_MEDIO:
                        valorPonta = this.RetornaCurvaOuroBMF(posicaoSwap, dataInicioCorrecao, data, pontaSwap);
                        break;

                    #region Indice de Preço (IGPM, IPCA, INPC, IGP-DI)
                    case ListaIndiceFixo.IGPM:
                    case ListaIndiceFixo.IPCA:
                    case ListaIndiceFixo.INPC:
                    case ListaIndiceFixo.IGPDI:
                        valorPonta = this.RetornaCurvaIndicePreco(posicaoSwap, dataInicioCorrecao, data, pontaSwap);
                        break;
                    #endregion

                    #region Indice de Ações (Ibovespa, IBRX)
                    case ListaIndiceFixo.IBOVESPA_FECHA:
                    case ListaIndiceFixo.IBOVESPA_MEDIO:
                    case ListaIndiceFixo.IBRX50_FECHA:
                    case ListaIndiceFixo.IBRX50_MEDIO:
                        valorPonta = this.RetornaCurvaIndiceAcoes(posicaoSwap, dataInicioCorrecao, data, pontaSwap);
                        break;
                    #endregion

                    case ListaIndiceFixo.TJLP:
                        valorPonta = this.RetornaCurvaTJLP(posicaoSwap, dataInicioCorrecao, data, pontaSwap);
                        break;

                    default:
                        Indice indice = new Indice();
                        indice.LoadByPrimaryKey((short)idIndice);

                        if (indice.Tipo.Value == (byte)TipoIndice.Decimal)
                        {
                            valorPonta = this.RetornaCurvaIndiceDecimal(posicaoSwap, dataInicioCorrecao, data, pontaSwap);
                        }
                        else
                        {
                            valorPonta = this.RetornaCurvaIndicePercentual(posicaoSwap, dataInicioCorrecao, data, pontaSwap);
                        }
                        break;
                }
                #endregion

            }
            else if (tipoPonta == (int)TipoPontaSwap.IndexadoD0)
            {
                valorPonta = this.RetornaCurvaIndexadoD0(posicaoSwap, dataInicioCorrecao, data, pontaSwap);
            }
            else if (tipoPonta == (int)TipoPontaSwap.AtivoBolsa)
            {
                string cdAtivoBolsa = pontaSwap == PontaSwap.Parte ? posicaoSwap.CdAtivoBolsa : posicaoSwap.CdAtivoBolsaContraParte; 
                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                decimal cotacaoAtual = 0;
                decimal cotacaoAnterior = 0;

                if (cotacaoBolsa.BuscaCotacaoBolsa(cdAtivoBolsa, data))
                    cotacaoAtual = cotacaoBolsa.PUFechamento.Value;

                cotacaoBolsa = new CotacaoBolsa();
                if (cotacaoBolsa.BuscaCotacaoBolsa(cdAtivoBolsa, dataInicioCorrecao))
                    cotacaoAnterior = cotacaoBolsa.PUFechamento.Value;

                valorPonta = (valorBase / cotacaoAnterior) * cotacaoAtual;
            }
            else if (tipoPonta == (int)TipoPontaSwap.FundoInvestimento)
            {
                HistoricoCota historicoCota = new HistoricoCota();
                int idAticoCarteira = pontaSwap == PontaSwap.Parte ? posicaoSwap.IdAtivoCarteira.Value : posicaoSwap.IdAtivoCarteiraContraParte.Value;                                 
                decimal cotaAtual = 0;
                decimal cotaAnterior = 0;

                if (historicoCota.BuscaValorCota(idAticoCarteira, data))
                    cotaAtual = historicoCota.CotaFechamento.Value;

                historicoCota = new HistoricoCota();
                if (historicoCota.BuscaValorCota(idAticoCarteira, dataInicioCorrecao))
                    cotaAnterior = historicoCota.CotaFechamento.Value;

                valorPonta = (valorBase / cotaAnterior) * cotaAtual;
            }

            decimal valorAjuste = eventoSwap.BuscaValorAjuste(posicaoSwap.IdOperacao.Value, pontaSwap, data);

            return (valorPonta + valorAjuste);
        }

        public decimal CalculaIR(decimal saldo, DateTime dataInicio, DateTime dataFim)
        {
            decimal valorIR = 0;

            if (saldo > 0)
            {
                CalculoTributo calculoTributo = new CalculoTributo();
                decimal aliquotaIR = calculoTributo.RetornaAliquotaIRLongoPrazo(dataInicio, dataFim);

                valorIR = Math.Round(saldo * aliquotaIR / 100M, 2);
            }
            return valorIR;
        }
    }
}
