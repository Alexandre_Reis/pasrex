using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Swap.Enums;

namespace Financial.Swap
{
	public partial class OperacaoSwapCollection : esOperacaoSwapCollection
	{
        /// <summary>
        /// Carrega o objeto OperacaoSwapCollection com todos os campos de OperacaoSwap.
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaOperacaoSwap(int idCliente, DateTime data)
        {        
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataRegistro.Equal(data));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto OperacaoSwapCollection com os campos IdOperacao, ValorBase, Corretagem, 
        /// Emolumento, Registro.
        /// Filtra apenas swaps registrados na BMF.
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaOperacaoCalculoTaxas(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, 
                         this.Query.ValorBase, 
                         this.Query.Corretagem, 
                         this.Query.Emolumento, 
                         this.query.Registro,
                         this.query.IdConta)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataEmissao.Equal(data),
                        this.Query.TipoRegistro.Equal(TipoRegistroSwap.BMF));

            bool retorno = this.Query.Load();

            return retorno;
        }

	}
}
