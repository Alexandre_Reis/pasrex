﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 09/11/2015 16:09:58
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Swap.Enums;
using Financial.RendaFixa;
using Financial.ContaCorrente.Enums;
using Financial.ContaCorrente;
using Financial.Util;
using Financial.Common.Enums;

namespace Financial.Swap
{
	public partial class EventoSwap : esEventoSwap
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaReset(int idCliente, DateTime data)
        {
            CalculoSwap calculoSwap = new CalculoSwap();
            List<EventoSwap> lstEventoSwap = new List<EventoSwap>();
            PosicaoSwapCollection posicaoSwapColl = new PosicaoSwapCollection();
            EventoSwapCollection eventoSwapColl = new EventoSwapCollection();
            PosicaoSwapQuery posicaoSwapQuery = new PosicaoSwapQuery("posicao");
            EventoSwapQuery eventoSwapQuery = new EventoSwapQuery("evento");
            LiquidacaoCollection liquidacaoCollectionInserir = new LiquidacaoCollection();

            #region Carrega Posições 
            posicaoSwapQuery.es.Distinct = true;
            posicaoSwapQuery.Select(posicaoSwapQuery);
            posicaoSwapQuery.InnerJoin(eventoSwapQuery).On(posicaoSwapQuery.IdOperacao.Equal(eventoSwapQuery.IdOperacao));
            posicaoSwapQuery.Where(eventoSwapQuery.DataEvento.Equal(data)
                                   & eventoSwapQuery.TipoEvento.Equal((byte)TipoEventoSwap.Reset)
                                   & eventoSwapQuery.PontaSwap.Equal((byte)Financial.Swap.Enums.PontaSwap.Ambas));            

            if(!posicaoSwapColl.Load(posicaoSwapQuery))
                return;
            #endregion

            #region Carrega Eventos
            eventoSwapColl.Query.Where(eventoSwapColl.Query.TipoEvento.Equal((byte)TipoEventoSwap.Reset) 
                                       & eventoSwapColl.Query.DataEvento.Equal(data));

            eventoSwapColl.Query.OrderBy(eventoSwapColl.Query.DataVigencia.Descending);
            eventoSwapColl.Query.Load();
                
            lstEventoSwap = ((List<EventoSwap>)eventoSwapColl);
            #endregion 

            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
            //

            foreach (PosicaoSwap posicaoSwap in posicaoSwapColl)
            {
                decimal saldo = posicaoSwap.Saldo.Value;
                decimal valorIR = posicaoSwap.ValorIR.Value;
                string numeroContrato = posicaoSwap.NumeroContrato;

                #region Lança em liquidação
                string descricao = "Reset de Swap - número do contrato " + numeroContrato;
                if (saldo != 0)
                {
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.IdCliente = idCliente;
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = data;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = saldo;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Swap.LiquidacaoAntecipacao;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdConta = idContaDefault;

                    liquidacaoCollectionInserir.AttachEntity(liquidacao);
                }
                #endregion

                #region Lança em Liquidacao (IR)
                if (valorIR != 0)
                {
                    descricao = "IR s/ " + descricao + numeroContrato;
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.IdCliente = idCliente;
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = data;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = valorIR * -1;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Swap.LiquidacaoAntecipacao;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdConta = idContaDefault;

                    liquidacaoCollectionInserir.AttachEntity(liquidacao);
                }
                #endregion


                int idOperacao = posicaoSwap.IdOperacao.Value;

                EventoSwap evento = lstEventoSwap.Find(delegate(EventoSwap x) { return x.IdOperacao == idOperacao; });

                decimal valorBaseOriginal = posicaoSwap.ValorBase.Value;
                decimal valorResetNovo = evento.Valor.Value;

                posicaoSwap.DataUltimoEvento = data;
                posicaoSwap.ValorBase = valorResetNovo;

                decimal valorPonta = calculoSwap.CalculaPonta(data, posicaoSwap, Financial.Swap.Enums.PontaSwap.Parte);
                decimal valorPontaContraParte = calculoSwap.CalculaPonta(data, posicaoSwap, Financial.Swap.Enums.PontaSwap.ContraParte);
                decimal novoSaldo = valorPonta - valorPontaContraParte;

                posicaoSwap.ValorParte = valorPonta;
                posicaoSwap.ValorContraParte = valorPontaContraParte;
                posicaoSwap.Saldo = novoSaldo;
                posicaoSwap.ValorIR = 0;

            }

            posicaoSwapColl.Save();
            liquidacaoCollectionInserir.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idOperacao"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="qtdePeriodicidade"></param>
        /// <param name="periodicidadeReset"></param>
        public void GeraFluxoReset(int idOperacao, DateTime dataVigencia, decimal valor, DateTime dataInicio, DateTime dataFim, int qtdePeriodicidade, int periodicidadeReset)
        {
            EventoSwapCollection eventoSwapColl = new EventoSwapCollection();


            eventoSwapColl.Query.Where(eventoSwapColl.Query.IdOperacao.Equal(idOperacao));
            if (eventoSwapColl.Query.Load())
            {
                eventoSwapColl.MarkAllAsDeleted();
                eventoSwapColl.Save();

                eventoSwapColl = new EventoSwapCollection();
            }

            do
            {
                if (periodicidadeReset == (int)TipoPeriodicidadeReset.Dias)
                {
                    dataInicio = dataInicio.AddDays(qtdePeriodicidade);
                }
                else if (periodicidadeReset == (int)TipoPeriodicidadeReset.Meses)
                {
                    dataInicio = dataInicio.AddMonths(qtdePeriodicidade);
                }
                else if (periodicidadeReset == (int)TipoPeriodicidadeReset.Anos)
                {
                    dataInicio = dataInicio.AddYears(qtdePeriodicidade);
                }

                if (dataInicio > dataFim)
                    break;

                if (!Calendario.IsDiaUtil(dataInicio, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                    dataInicio = Calendario.AdicionaDiaUtil(dataInicio, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                EventoSwap eventoSwap = eventoSwapColl.AddNew();
                eventoSwap.IdOperacao = idOperacao;
                eventoSwap.DataVigencia = dataVigencia;
                eventoSwap.DataEvento = dataInicio;
                eventoSwap.Valor = valor;
                eventoSwap.PontaSwap = (short)Financial.Swap.Enums.PontaSwap.Ambas;
                eventoSwap.TipoEvento = (short)Financial.Swap.Enums.TipoEventoSwap.Reset;



            } while (dataInicio < dataFim);

            eventoSwapColl.Save();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idOperacao"></param>
        /// <param name="dataEvento"></param>
        /// <returns></returns>
        public decimal BuscaValorAjuste(int idOperacao, PontaSwap pontaSwap, DateTime dataEvento)
        {
            List<EventoSwap> lstEventoSwap = new List<EventoSwap>();
            EventoSwapCollection eventoSwapColl = new EventoSwapCollection();
            decimal ajuste = 0;

            eventoSwapColl.Query.Where(eventoSwapColl.Query.TipoEvento.Equal((byte)TipoEventoSwap.Ajuste)
                                        & eventoSwapColl.Query.PontaSwap.Equal((int)pontaSwap)
                                        & eventoSwapColl.Query.IdOperacao.Equal(idOperacao)
                                        & eventoSwapColl.Query.DataEvento.Equal(dataEvento));
            eventoSwapColl.Query.OrderBy(eventoSwapColl.Query.DataVigencia.Descending);

            if(eventoSwapColl.Query.Load())
            {
                lstEventoSwap = ((List<EventoSwap>)eventoSwapColl);
                EventoSwap evento = lstEventoSwap.Find(delegate(EventoSwap x) { return x.IdOperacao == idOperacao; });
                ajuste = evento.Valor.Value;
            }

            return ajuste;
        }

        /// <summary>
        /// Deleta todos os eventos de Swap de uma operação
        /// </summary>
        /// <param name="idOperacao"></param>
        /// <param name="eventoSwap"></param>
        /// <returns></returns>
        public void DeletaEvento(int idOperacao, int eventoSwap)
        {
            EventoSwapCollection eventoSwapColl = new EventoSwapCollection();

            eventoSwapColl.Query.Where(eventoSwapColl.Query.IdOperacao.Equal(idOperacao)
                                       && eventoSwapColl.Query.TipoEvento.Equal(eventoSwap));

            if(eventoSwapColl.Query.Load())
            {
                eventoSwapColl.MarkAllAsDeleted();
                eventoSwapColl.Save();
            }
        }
	}
}
