﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Swap.Exceptions;
using Financial.Investidor;
using Financial.RendaFixa;
using Financial.Tributo.Custom;

namespace Financial.Swap
{
	public partial class LiquidacaoSwap : esLiquidacaoSwap
	{
        /// <summary>
        /// Processa as antecipações de swap do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaAntecipacaoSwap(int idCliente, DateTime data)
        {
            LiquidacaoCollection liquidacaoCollectionInserir = new LiquidacaoCollection();
            LiquidacaoSwapCollection liquidacaoSwapCollection = new LiquidacaoSwapCollection();
            liquidacaoSwapCollection.BuscaAntecipacaoSwap(idCliente, data);

            int idContaDefault = 0;
            if (liquidacaoSwapCollection.Count > 0)
            {
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
                //
            }

            ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
            clienteRendaFixa.LoadByPrimaryKey(idCliente);
            
            for (int i = 0; i < liquidacaoSwapCollection.Count; i++)
            {
                LiquidacaoSwap liquidacaoSwap = (LiquidacaoSwap) liquidacaoSwapCollection[i];
                decimal valorAntecipacao = liquidacaoSwap.ValorAntecipacao.Value;
                
                int idPosicao = liquidacaoSwap.IdPosicao.Value;

                #region Busca informações em PosicaoSwap
                PosicaoSwap posicaoSwap = new PosicaoSwap();
                if (!posicaoSwap.LoadByPrimaryKey(idPosicao))
                {
                    throw new PosicaoSwapInexistenteException("Posição inexistente de Swap " + idPosicao);
                }

                DateTime dataUltimoEvento = posicaoSwap.DataUltimoEvento.GetValueOrDefault(posicaoSwap.DataEmissao.Value);
                string numeroContrato = posicaoSwap.NumeroContrato;
                decimal valorBase = posicaoSwap.ValorBase.Value;
                decimal saldo = posicaoSwap.Saldo.Value;
                decimal valorParte = posicaoSwap.ValorParte.Value;
                decimal valorContraParte = posicaoSwap.ValorContraParte.Value;
                #endregion

                decimal novoValorBase = valorBase - valorAntecipacao;
                decimal valorParteLiquidar = Math.Round(valorParte * valorAntecipacao / valorBase, 2);
                decimal valorContraParteLiquidar = Math.Round(valorContraParte * valorAntecipacao / valorBase, 2);
                decimal saldoLiquidar = Math.Round(saldo * valorAntecipacao / valorBase, 2);

                decimal valorIR = 0;
                if (clienteRendaFixa.es.HasData && clienteRendaFixa.IsentoIR == "N")
                {
                    CalculoSwap calculoSwap = new CalculoSwap();
                    valorIR = calculoSwap.CalculaIR(saldoLiquidar, dataUltimoEvento, data);
                }

                #region Atualiza PosicaoSwap
                posicaoSwap.ValorBase = novoValorBase;
                posicaoSwap.ValorParte -= valorParteLiquidar;
                posicaoSwap.ValorContraParte -= valorContraParteLiquidar;
                posicaoSwap.Saldo -= saldoLiquidar;
                posicaoSwap.Save();
                #endregion

                #region Lança em Liquidacao
                string descricao = "Antecipação de Swap - número do contrato " + numeroContrato;                
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.IdCliente = idCliente;
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = data;
                liquidacao.Descricao = descricao;
                liquidacao.Valor = saldoLiquidar;
                liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = OrigemLancamentoLiquidacao.Swap.LiquidacaoAntecipacao;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdConta = idContaDefault;

                liquidacaoCollectionInserir.AttachEntity(liquidacao);
                #endregion

                #region Lança em Liquidacao (IR)
                if (valorIR != 0)
                {
                    descricao = "IR s/ Antecipação de Swap - número do contrato " + numeroContrato;
                    liquidacao = new Liquidacao();
                    liquidacao.IdCliente = idCliente;
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = data;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = valorIR * -1;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Swap.LiquidacaoAntecipacao;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdConta = idContaDefault;

                    liquidacaoCollectionInserir.AttachEntity(liquidacao);
                }
                #endregion

                liquidacaoSwap.ValorIR = valorIR;

                liquidacaoCollectionInserir.Save();
            }
        }

        /// <summary>
        /// Retorna o total (valor) liquidado da posicao (IdPosicao) na data passada.
        /// </summary>
        /// <param name="IdPosicao"></param>
        /// <param name="data"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public decimal RetornaTotalLiquidado(int IdPosicao, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorAntecipacao.Sum())
                 .Where(this.Query.IdPosicao.Equal(IdPosicao),
                        this.Query.Data.Equal(data));

            this.Query.Load();

            decimal valorAntecipacao = 0;
            if (this.es.HasData && this.ValorAntecipacao.HasValue)
            {
                valorAntecipacao = this.ValorAntecipacao.Value;
            }

            return valorAntecipacao;
        }

	}
}
