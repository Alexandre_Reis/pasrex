﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Util;
using Financial.Swap.Enums;
using Financial.Swap.Exceptions;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using log4net;
using Financial.RendaFixa;
using Financial.Tributo.Custom;
using Financial.Fundo.Enums;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Bolsa;

namespace Financial.Swap
{
    public partial class PosicaoSwap : esPosicaoSwap
    {
        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCliente, DateTime data)
        {
            try
            {
                PosicaoSwapHistoricoCollection posicaoSwapDeletarHistoricoCollection = new PosicaoSwapHistoricoCollection();
                posicaoSwapDeletarHistoricoCollection.DeletaPosicaoSwapHistoricoDataHistoricoMaiorIgual(idCliente, data);
            }
            catch { }

            PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();
            posicaoSwapCollection.BuscaPosicaoSwapCompleta(idCliente);
            //
            PosicaoSwapHistoricoCollection posicaoSwapHistoricoCollection = new PosicaoSwapHistoricoCollection();
            //
            #region Copia de PosicaoSwap para PosicaoSwapHistorico
            for (int i = 0; i < posicaoSwapCollection.Count; i++)
            {
                PosicaoSwap posicaoSwap = posicaoSwapCollection[i];

                // Copia de PosicaoSwap para PosicaoSwapHistorico
                PosicaoSwapHistorico posicaoSwapHistorico = posicaoSwapHistoricoCollection.AddNew();
                //
                posicaoSwapHistorico.DataHistorico = data;
                posicaoSwapHistorico.IdPosicao = posicaoSwap.IdPosicao;
                posicaoSwapHistorico.IdOperacao = posicaoSwap.IdOperacao;
                posicaoSwapHistorico.IdCliente = posicaoSwap.IdCliente;
                posicaoSwapHistorico.IdAssessor = posicaoSwap.IdAssessor;
                posicaoSwapHistorico.TipoRegistro = posicaoSwap.TipoRegistro;
                posicaoSwapHistorico.NumeroContrato = posicaoSwap.NumeroContrato;
                posicaoSwapHistorico.DataEmissao = posicaoSwap.DataEmissao;
                posicaoSwapHistorico.DataVencimento = posicaoSwap.DataVencimento;
                posicaoSwapHistorico.ValorBase = posicaoSwap.ValorBase;
                posicaoSwapHistorico.TipoPonta = posicaoSwap.TipoPonta;
                posicaoSwapHistorico.IdIndice = posicaoSwap.IdIndice;
                posicaoSwapHistorico.TaxaJuros = posicaoSwap.TaxaJuros;
                posicaoSwapHistorico.Percentual = posicaoSwap.Percentual;
                posicaoSwapHistorico.TipoApropriacao = posicaoSwap.TipoApropriacao;
                posicaoSwapHistorico.ContagemDias = posicaoSwap.ContagemDias;
                posicaoSwapHistorico.BaseAno = posicaoSwap.BaseAno;
                posicaoSwapHistorico.TipoPontaContraParte = posicaoSwap.TipoPontaContraParte;
                posicaoSwapHistorico.IdIndiceContraParte = posicaoSwap.IdIndiceContraParte;
                posicaoSwapHistorico.TaxaJurosContraParte = posicaoSwap.TaxaJurosContraParte;
                posicaoSwapHistorico.PercentualContraParte = posicaoSwap.PercentualContraParte;
                posicaoSwapHistorico.TipoApropriacaoContraParte = posicaoSwap.TipoApropriacaoContraParte;
                posicaoSwapHistorico.ContagemDiasContraParte = posicaoSwap.ContagemDiasContraParte;
                posicaoSwapHistorico.BaseAnoContraParte = posicaoSwap.BaseAnoContraParte;
                posicaoSwapHistorico.IdAgente = posicaoSwap.IdAgente;
                posicaoSwapHistorico.ComGarantia = posicaoSwap.ComGarantia;
                posicaoSwapHistorico.DiasUteis = posicaoSwap.DiasUteis;
                posicaoSwapHistorico.DiasCorridos = posicaoSwap.DiasCorridos;
                posicaoSwapHistorico.ValorParte = posicaoSwap.ValorParte;
                posicaoSwapHistorico.ValorContraParte = posicaoSwap.ValorContraParte;
                posicaoSwapHistorico.Saldo = posicaoSwap.Saldo;
                posicaoSwapHistorico.ValorIndicePartida = posicaoSwap.ValorIndicePartida;                
                posicaoSwapHistorico.ValorIndicePartidaContraParte = posicaoSwap.ValorIndicePartidaContraParte;
                posicaoSwapHistorico.IdEstrategia = posicaoSwap.IdEstrategia;
                posicaoSwapHistorico.ValorIR = posicaoSwap.ValorIR;
                posicaoSwapHistorico.CdAtivoBolsa = posicaoSwap.CdAtivoBolsa;
                posicaoSwapHistorico.CdAtivoBolsaContraParte = posicaoSwap.CdAtivoBolsaContraParte;
                posicaoSwapHistorico.IdAtivoCarteira = posicaoSwap.IdAtivoCarteira;
                posicaoSwapHistorico.IdAtivoCarteiraContraParte = posicaoSwap.IdAtivoCarteiraContraParte;
                posicaoSwapHistorico.DataUltimoEvento = posicaoSwap.DataUltimoEvento;
            }
            #endregion

            posicaoSwapHistoricoCollection.Save();
        }

        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraPosicaoAbertura(int idCliente, DateTime data)
        {
            PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();
            posicaoSwapCollection.BuscaPosicaoSwapCompleta(idCliente);
            //
            PosicaoSwapAberturaCollection posicaoSwapAberturaCollection = new PosicaoSwapAberturaCollection();
            //
            #region Copia de PosicaoSwap para PosicaoSwapAbertura
            for (int i = 0; i < posicaoSwapCollection.Count; i++)
            {
                PosicaoSwap posicaoSwap = posicaoSwapCollection[i];

                // Copia de PosicaoSwap para PosicaoSwapAbertura
                PosicaoSwapAbertura posicaoSwapAbertura = posicaoSwapAberturaCollection.AddNew();
                //
                posicaoSwapAbertura.DataHistorico = data;
                posicaoSwapAbertura.IdPosicao = posicaoSwap.IdPosicao;
                posicaoSwapAbertura.IdOperacao = posicaoSwap.IdOperacao;
                posicaoSwapAbertura.IdCliente = posicaoSwap.IdCliente;
                posicaoSwapAbertura.IdAssessor = posicaoSwap.IdAssessor;
                posicaoSwapAbertura.TipoRegistro = posicaoSwap.TipoRegistro;
                posicaoSwapAbertura.NumeroContrato = posicaoSwap.NumeroContrato;
                posicaoSwapAbertura.DataEmissao = posicaoSwap.DataEmissao;
                posicaoSwapAbertura.DataVencimento = posicaoSwap.DataVencimento;
                posicaoSwapAbertura.ValorBase = posicaoSwap.ValorBase;
                posicaoSwapAbertura.TipoPonta = posicaoSwap.TipoPonta;
                posicaoSwapAbertura.IdIndice = posicaoSwap.IdIndice;
                posicaoSwapAbertura.TaxaJuros = posicaoSwap.TaxaJuros;
                posicaoSwapAbertura.Percentual = posicaoSwap.Percentual;
                posicaoSwapAbertura.TipoApropriacao = posicaoSwap.TipoApropriacao;
                posicaoSwapAbertura.ContagemDias = posicaoSwap.ContagemDias;
                posicaoSwapAbertura.BaseAno = posicaoSwap.BaseAno;
                posicaoSwapAbertura.TipoPontaContraParte = posicaoSwap.TipoPontaContraParte;
                posicaoSwapAbertura.IdIndiceContraParte = posicaoSwap.IdIndiceContraParte;
                posicaoSwapAbertura.TaxaJurosContraParte = posicaoSwap.TaxaJurosContraParte;
                posicaoSwapAbertura.PercentualContraParte = posicaoSwap.PercentualContraParte;
                posicaoSwapAbertura.TipoApropriacaoContraParte = posicaoSwap.TipoApropriacaoContraParte;
                posicaoSwapAbertura.ContagemDiasContraParte = posicaoSwap.ContagemDiasContraParte;
                posicaoSwapAbertura.BaseAnoContraParte = posicaoSwap.BaseAnoContraParte;
                posicaoSwapAbertura.IdAgente = posicaoSwap.IdAgente;
                posicaoSwapAbertura.ComGarantia = posicaoSwap.ComGarantia;
                posicaoSwapAbertura.DiasUteis = posicaoSwap.DiasUteis;
                posicaoSwapAbertura.DiasCorridos = posicaoSwap.DiasCorridos;
                posicaoSwapAbertura.ValorParte = posicaoSwap.ValorParte;
                posicaoSwapAbertura.ValorContraParte = posicaoSwap.ValorContraParte;
                posicaoSwapAbertura.Saldo = posicaoSwap.Saldo;
                posicaoSwapAbertura.ValorIndicePartida = posicaoSwap.ValorIndicePartida;
                posicaoSwapAbertura.ValorIndicePartidaContraParte = posicaoSwap.ValorIndicePartidaContraParte;
                posicaoSwapAbertura.IdEstrategia = posicaoSwap.IdEstrategia;
                posicaoSwapAbertura.ValorIR = posicaoSwap.ValorIR;
                posicaoSwapAbertura.CdAtivoBolsa = posicaoSwap.CdAtivoBolsa;
                posicaoSwapAbertura.CdAtivoBolsaContraParte = posicaoSwap.CdAtivoBolsaContraParte;
                posicaoSwapAbertura.IdAtivoCarteira = posicaoSwap.IdAtivoCarteira;
                posicaoSwapAbertura.IdAtivoCarteiraContraParte = posicaoSwap.IdAtivoCarteiraContraParte;
                posicaoSwapAbertura.DataUltimoEvento = posicaoSwap.DataUltimoEvento;
            }
            #endregion

            posicaoSwapAberturaCollection.Save();
        }

        /// <summary>
        /// Retorna o total de saldo em posições de swap do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaValorSaldoTotal(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Saldo.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.ValorBase.NotEqual(0));

            this.Query.Load();

            return this.Saldo.HasValue ? this.Saldo.Value : 0;
        }

        /// <summary>
        /// Calcula os valores atualizados das 2 pontas das posições de swap, computando o saldo líquido entre elas.
        /// Somente para pontas do tipo Indexado_D0.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaSaldoPosicaoFechamento(int idCliente, DateTime data)
        {
            PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();
            posicaoSwapCollection.BuscaPosicaoSwapFechamento(idCliente);

            ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
            if (posicaoSwapCollection.Count > 0)
            {
                clienteRendaFixa.LoadByPrimaryKey(idCliente);
            }

            for (int i = 0; i < posicaoSwapCollection.Count; i++)
            {
                PosicaoSwap posicaoSwap = posicaoSwapCollection[i];
                DateTime dataUltimoEvento = posicaoSwap.DataUltimoEvento.GetValueOrDefault(posicaoSwap.DataEmissao.Value);
                CalculoSwap calculoSwap = new CalculoSwap();

                decimal valorPonta = 0;
                #region Cálculo da ponta da parte
                valorPonta = calculoSwap.CalculaPonta(data, posicaoSwap, Financial.Swap.Enums.PontaSwap.Parte);
                #endregion

                decimal valorPontaContraParte = 0;
                #region Cálculo da ponta da contraparte
                valorPontaContraParte = calculoSwap.CalculaPonta(data, posicaoSwap, Financial.Swap.Enums.PontaSwap.ContraParte);
                #endregion

                decimal saldo = valorPonta - valorPontaContraParte;

                decimal valorIR = 0;
                if (clienteRendaFixa.es.HasData && clienteRendaFixa.IsentoIR == "N")
                {
                    valorIR = calculoSwap.CalculaIR(saldo, dataUltimoEvento, data);
                }

                posicaoSwap.ValorParte = valorPonta;
                posicaoSwap.ValorContraParte = valorPontaContraParte;
                posicaoSwap.Saldo = saldo;
                posicaoSwap.ValorIR = valorIR;
            }

            posicaoSwapCollection.Save();

        }

        /// <summary>
        /// Calcula os valores atualizados das 2 pontas das posições de swap, computando o saldo líquido entre elas.
        /// Somente para pontas diferentes de Indexado_D0.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaSaldoPosicaoAbertura(int idCliente, DateTime data)
        {
            PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();
            posicaoSwapCollection.BuscaPosicaoSwapAbertura(idCliente);

            ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
            if (posicaoSwapCollection.Count > 0)
            {                
                clienteRendaFixa.LoadByPrimaryKey(idCliente);
            }

            for (int i = 0; i < posicaoSwapCollection.Count; i++)
            {
                PosicaoSwap posicaoSwap = posicaoSwapCollection[i];
                DateTime dataUltimoEvento = posicaoSwap.DataUltimoEvento.GetValueOrDefault(posicaoSwap.DataEmissao.Value);
                CalculoSwap calculoSwap = new CalculoSwap();

                decimal valorPonta = 0;
                #region Cálculo da ponta da parte
                valorPonta = calculoSwap.CalculaPonta(data, posicaoSwap, PontaSwap.Parte);
                #endregion

                decimal valorPontaContraParte = 0;
                #region Cálculo da ponta da contraparte
                valorPontaContraParte = calculoSwap.CalculaPonta(data, posicaoSwap, PontaSwap.ContraParte);
                #endregion

                decimal saldo = valorPonta - valorPontaContraParte;

                decimal valorIR = 0;
                if (clienteRendaFixa.es.HasData && clienteRendaFixa.IsentoIR == "N")
                {
                    valorIR = calculoSwap.CalculaIR(saldo, dataUltimoEvento, data);
                }

                posicaoSwap.ValorParte = valorPonta;
                posicaoSwap.ValorContraParte = valorPontaContraParte;
                posicaoSwap.Saldo = saldo;
                posicaoSwap.ValorIR = valorIR;
            }

            posicaoSwapCollection.Save();

        }

        /// <summary>
        /// Baixa as posições de swap vencendo no dia. Lança em Liquidacao o valor do saldo remanescente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaVencimentoSwap(int idCliente, DateTime data)
        {
            #region Deleta todas as liquidações de vencimento do dia lançadas em LiquidacaoSwap
            LiquidacaoSwapCollection liquidacaoSwapCollectionDeletar = new LiquidacaoSwapCollection();
            liquidacaoSwapCollectionDeletar.DeletaVencimentoSwap(idCliente, data);
            #endregion

            LiquidacaoSwapCollection liquidacaoSwapCollectionInserir = new LiquidacaoSwapCollection();
            LiquidacaoCollection liquidacaoCollectionInserir = new LiquidacaoCollection();
            PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();
            posicaoSwapCollection.BuscaPosicaoSwapVencimento(idCliente, data);

            ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
            if (posicaoSwapCollection.Count > 0)
            {
                clienteRendaFixa.LoadByPrimaryKey(idCliente);
            }

            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
            //

            for (int i = 0; i < posicaoSwapCollection.Count; i++)
            {
                PosicaoSwap posicaoSwap = (PosicaoSwap)posicaoSwapCollection[i];

                int idPosicao = posicaoSwap.IdPosicao.Value;
                decimal saldo = posicaoSwap.Saldo.Value;
                string numeroContrato = posicaoSwap.NumeroContrato;
                DateTime dataUltimoEvento = posicaoSwap.DataUltimoEvento.GetValueOrDefault(posicaoSwap.DataEmissao.Value);

                decimal valorIR = 0;
                if (clienteRendaFixa.es.HasData && clienteRendaFixa.IsentoIR == "N")
                {
                    CalculoSwap calculoSwap = new CalculoSwap();
                    valorIR = calculoSwap.CalculaIR(saldo, dataUltimoEvento, data);
                }

                #region Lança em Liquidacao               
                string descricao = "Vencimento de Swap - número do contrato " + numeroContrato;
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.IdCliente = idCliente;
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = data;
                liquidacao.Descricao = descricao;
                liquidacao.Valor = saldo;
                liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = OrigemLancamentoLiquidacao.Swap.LiquidacaoVencimento;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdConta = idContaDefault;

                liquidacaoCollectionInserir.AttachEntity(liquidacao);

                if (valorIR > 0)
                {
                    liquidacao = new Liquidacao();
                    liquidacao.IdCliente = idCliente;
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = data;
                    liquidacao.Descricao = "IR s/ " + descricao;
                    liquidacao.Valor = valorIR * (-1);
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Swap.LiquidacaoVencimento;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdConta = idContaDefault;

                    liquidacaoCollectionInserir.AttachEntity(liquidacao);
                }
                #endregion

                #region Lança em LiquidacaoSwap
                LiquidacaoSwap liquidacaoSwap = new LiquidacaoSwap();
                liquidacaoSwap.AddNew();
                liquidacaoSwap.Data = data;
                liquidacaoSwap.TipoLiquidacao = (byte)TipoLiquidacaoSwap.Vencimento;
                liquidacaoSwap.IdCliente = idCliente;
                liquidacaoSwap.IdPosicao = idPosicao;
                liquidacaoSwap.ValorAntecipacao = saldo;
                liquidacaoSwap.ValorIR = valorIR;

                liquidacaoSwapCollectionInserir.AttachEntity(liquidacaoSwap);
                #endregion
            }

            posicaoSwapCollection.MarkAllAsDeleted(); //Deleta todas as posições de swap vencendo no dia
            posicaoSwapCollection.Save();

            liquidacaoSwapCollectionInserir.Save();
            liquidacaoCollectionInserir.Save();

        }

        /// <summary>
        /// Retorna o valor de mercado. Usado para apuração das regras de enquadramento.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaValorMercadoEnquadra(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Saldo.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.ValorBase.NotEqual(0));

            this.Query.Load();

            return this.Saldo.HasValue ? this.Saldo.Value : 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoAberturaIndexada"></param>
        public void AberturaIndexada(Cliente cliente, DateTime data)
        {
            PosicaoSwapCollection posicaoColl = new PosicaoSwapCollection();
            PosicaoSwapQuery posicaoQuery = new PosicaoSwapQuery("Posicao");

            TipoAberturaIndexada tipoAberturaIndexada = (TipoAberturaIndexada)cliente.AberturaIndexada;
            short idIndexador = cliente.IdIndiceAbertura.Value;
            int idCliente = cliente.IdCliente.Value;
            int idLocalFeriado = cliente.IdLocal.Value;
            decimal cotacaoIndexador = 0;

            #region Carrega Posição
            
            if (tipoAberturaIndexada != TipoAberturaIndexada.Mandatorio)            
                return;            

            posicaoQuery.Select(posicaoQuery);
            posicaoQuery.Where(posicaoQuery.IdCliente.Equal(idCliente));

            if (!posicaoColl.Load(posicaoQuery))
                return;
            #endregion

            #region Busca Cotacao
            DateTime dataAnterior;
            if (idLocalFeriado != LocalFeriadoFixo.Brasil)
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, idLocalFeriado, TipoFeriado.Outros);
            }
            else
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, idLocalFeriado, TipoFeriado.Brasil);
            }
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            cotacaoIndice.Query.Where(cotacaoIndice.Query.IdIndice.Equal(idIndexador)
                                      & cotacaoIndice.Query.Data.Equal(dataAnterior));

            if (cotacaoIndice.Query.Load())
            {
                cotacaoIndexador = Convert.ToDecimal(Math.Pow((double)(cotacaoIndice.Valor.Value / 100 + 1), (double)1 / 252));
            }
            else
            {
                Indice indice = new Indice();
                indice.LoadByPrimaryKey(idIndexador);
                throw new Exception("Cotação não cadastrada para o Indexador - " + indice.Descricao + " na data - " + dataAnterior);
            }
            #endregion

            foreach (PosicaoSwap posicaoSwap in posicaoColl)
            {
                posicaoSwap.ValorParte = posicaoSwap.ValorParte.Value * cotacaoIndexador;
                posicaoSwap.ValorContraParte = posicaoSwap.ValorContraParte.Value * cotacaoIndexador;
                posicaoSwap.Saldo = posicaoSwap.ValorParte.Value - posicaoSwap.ValorContraParte.Value;
            }

            posicaoColl.Save();
        }

    }
}
