﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.ContaCorrente;
using Financial.Util;
using Financial.BMF;
using Financial.Common.Enums;
using Financial.ContaCorrente.Enums;
using Financial.Swap.Enums;

namespace Financial.Swap
{
	public partial class OperacaoSwap : esOperacaoSwap
	{
        /// <summary>
        /// Cria as posições de swap a partir das operações do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaOperacaoSwap(int idCliente, DateTime data)
        {
            PosicaoSwapCollection posicaoSwapCollectionInserir = new PosicaoSwapCollection();
            OperacaoSwapCollection operacaoSwapCollection = new OperacaoSwapCollection();
            operacaoSwapCollection.BuscaOperacaoSwap(idCliente, data);

            for (int i = 0; i < operacaoSwapCollection.Count; i++)
            {
                #region Carrega variáveis de OperacaoSwap
                OperacaoSwap operacaoSwap = operacaoSwapCollection[i];
                int idOperacao = operacaoSwap.IdOperacao.Value;
                int? idAssessor = operacaoSwap.IdAssessor;
                int? idAgente = operacaoSwap.IdAgente;
                int tipoRegistro = operacaoSwap.TipoRegistro.Value;
                string numeroContrato = operacaoSwap.NumeroContrato;
                DateTime dataEmissao = operacaoSwap.DataEmissao.Value;
                DateTime dataVencimento = operacaoSwap.DataVencimento.Value;
                decimal valorBase = operacaoSwap.ValorBase.Value;
                int tipoPonta = operacaoSwap.TipoPonta.Value;                
                int tipoPontaContraParte = operacaoSwap.TipoPontaContraParte.Value;                                
                string comGarantia = operacaoSwap.ComGarantia;
                int diasUteis = operacaoSwap.DiasUteis.Value;
                int diasCorridos = operacaoSwap.DiasCorridos.Value;
                decimal? valorIndicePartida = operacaoSwap.ValorIndicePartida;
                decimal? valorIndicePartidaContraParte = operacaoSwap.ValorIndicePartidaContraParte;
                int? idEstraegia = operacaoSwap.IdEstrategia;
                #endregion

                #region Insere em PosicaoSwap
                PosicaoSwap posicaoSwap = new PosicaoSwap();
                posicaoSwap.AddNew();
                posicaoSwap.IdOperacao = idOperacao;
                posicaoSwap.IdCliente = idCliente;
                posicaoSwap.IdAssessor = idAssessor;
                posicaoSwap.TipoRegistro = (byte)tipoRegistro;
                posicaoSwap.NumeroContrato = numeroContrato;
                posicaoSwap.DataEmissao = dataEmissao;
                posicaoSwap.DataVencimento = dataVencimento;
                posicaoSwap.ValorBase = valorBase;
                posicaoSwap.ValorParte = valorBase;
                posicaoSwap.ValorContraParte = valorBase;
                posicaoSwap.Saldo = 0;
                posicaoSwap.TipoPonta = (byte)tipoPonta;

                if (posicaoSwap.TipoPonta.Value == (int)TipoPontaSwap.AtivoBolsa)
                {
                    posicaoSwap.CdAtivoBolsa = operacaoSwap.CdAtivoBolsa;
                }
                else if (posicaoSwap.TipoPonta.Value == (int)TipoPontaSwap.FundoInvestimento)
                {
                    posicaoSwap.IdAtivoCarteira = operacaoSwap.IdAtivoCarteira;
                }
                else
                {
                    int? idIndice = operacaoSwap.IdIndice;
                    decimal taxaJuros = operacaoSwap.TaxaJuros.Value;
                    decimal percentual = operacaoSwap.Percentual.Value;
                    int tipoApropriacao = operacaoSwap.TipoApropriacao.Value;
                    int contagemDias = operacaoSwap.ContagemDias.Value;
                    short baseAno = operacaoSwap.BaseAno.Value;

                    if (idIndice.HasValue)
                    {
                        posicaoSwap.IdIndice = (short)idIndice;
                    }

                    posicaoSwap.TaxaJuros = taxaJuros;
                    posicaoSwap.Percentual = percentual;
                    posicaoSwap.TipoApropriacao = (byte)tipoApropriacao;
                    posicaoSwap.ContagemDias = (byte)contagemDias;
                    posicaoSwap.BaseAno = baseAno;
                }

                posicaoSwap.TipoPontaContraParte = (byte)tipoPontaContraParte;
                if (posicaoSwap.TipoPontaContraParte.Value == (int)TipoPontaSwap.AtivoBolsa)
                {
                    posicaoSwap.CdAtivoBolsaContraParte = operacaoSwap.CdAtivoBolsaContraParte;
                }
                else if (posicaoSwap.TipoPontaContraParte.Value == (int)TipoPontaSwap.FundoInvestimento)
                {
                    posicaoSwap.IdAtivoCarteiraContraParte = operacaoSwap.IdAtivoCarteiraContraParte;
                }
                else
                {
                    int? idIndiceContraParte = operacaoSwap.IdIndiceContraParte;
                    decimal taxaJurosContraParte = operacaoSwap.TaxaJurosContraParte.Value;
                    decimal percentualContraParte = operacaoSwap.PercentualContraParte.Value;
                    int tipoApropriacaoContraParte = operacaoSwap.TipoApropriacaoContraParte.Value;
                    int contagemDiasContraParte = operacaoSwap.ContagemDiasContraParte.Value;
                    short baseAnoContraParte = operacaoSwap.BaseAnoContraParte.Value;

                    if (idIndiceContraParte.HasValue)
                    {
                        posicaoSwap.IdIndiceContraParte = (short)idIndiceContraParte;
                    }

                    posicaoSwap.TaxaJurosContraParte = taxaJurosContraParte;
                    posicaoSwap.PercentualContraParte = percentualContraParte;
                    posicaoSwap.TipoApropriacaoContraParte = (byte)tipoApropriacaoContraParte;
                    posicaoSwap.ContagemDiasContraParte = (byte)contagemDiasContraParte;
                    posicaoSwap.BaseAnoContraParte = baseAnoContraParte;
                }

                posicaoSwap.IdAgente = idAgente;
                posicaoSwap.ComGarantia = comGarantia;
                posicaoSwap.DiasUteis = diasUteis;
                posicaoSwap.DiasCorridos = diasCorridos;
                posicaoSwap.ValorIndicePartida = valorIndicePartida;
                posicaoSwap.ValorIndicePartidaContraParte = valorIndicePartidaContraParte;

                posicaoSwap.IdEstrategia = idEstraegia;

                posicaoSwapCollectionInserir.AttachEntity(posicaoSwap);
                #endregion
            }

            posicaoSwapCollectionInserir.Save();
        }

        /// <summary>
        /// Calcula as taxas de emolumento e registro incidentes nas operações de swap.
        /// Lança em Liquidacao os valores calculados.
        /// Obs: hoje buscando apenas Swaps registrados na BMF.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaTaxas(int idCliente, DateTime data)
        {
            LiquidacaoCollection liquidacaoCollectionInserir = new LiquidacaoCollection();
            OperacaoSwapCollection operacaoSwapCollection = new OperacaoSwapCollection();
            operacaoSwapCollection.BuscaOperacaoCalculoTaxas(idCliente, data);

            for (int i = 0; i < operacaoSwapCollection.Count; i++)
            {
                OperacaoSwap operacaoSwap = operacaoSwapCollection[i];
                decimal valorBase = operacaoSwap.ValorBase.Value;

                decimal emolumento = Math.Round(Math.Round(valorBase / 10000000, 0) * 
                                                ParametrosConfiguracaoSistema.RendaFixaSwap.TaxaEmolumento, 2);

                //Clientes sócios da BMF têm desconto no registro.
                //Hoje usa o mesmo % para sócio e Investidor Institucional.
                ClienteBMF clienteBMF = new ClienteBMF();
                clienteBMF.BuscaSocioInvestidor(idCliente);
                if (clienteBMF.IsSocio() || clienteBMF.IsInvestidorInstitucional())
                {
                    emolumento = Utilitario.Truncate(ParametrosConfiguracaoSistema.RendaFixaSwap.DescontoSocio * emolumento, 2);
                }

                decimal registro = Math.Round(emolumento * ParametrosConfiguracaoSistema.RendaFixaSwap.PercentualRegistro, 2);

                operacaoSwap.Emolumento = emolumento;
                operacaoSwap.Registro = registro;

                #region Lança em Liquidacao
                DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                string descricao = "Taxas Operacionais de Swap";
                decimal valor = emolumento + registro;
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.IdCliente = idCliente;
                liquidacao.DataLancamento = data;   
                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = descricao;
                liquidacao.Valor = valor;
                liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = OrigemLancamentoLiquidacao.Swap.DespesasTaxas;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;

                liquidacaoCollectionInserir.AttachEntity(liquidacao);
                #endregion
            }

            operacaoSwapCollection.Save();
            liquidacaoCollectionInserir.Save();
        }

	}
}
