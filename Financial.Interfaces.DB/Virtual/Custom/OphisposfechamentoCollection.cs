﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.InterfacesDB
{
	public partial class OphisposfechamentoCollection : esOphisposfechamentoCollection
	{
        /// <summary>
        /// Busca posições no OpenVirtual a serem importadas.
        /// </summary>
        /// <param name="codigoCliente"></param>
        /// <param name="codigoCliente2"></param>
        /// <param name="dataOperacao"></param>
        public void BuscaPosicoesVirtual(string codigo, DateTime dataPosicao)
        {
            this.es.Connection.Name = "VIRTUAL";

            this.QueryReset();

            this.Query.Where(this.Query.FcMatricula.Like("%" + codigo + "%"),
                             this.Query.FdTrandata.Equal(dataPosicao),
                             this.Query.FcTpoper.In("V-FINAL"),
                             this.Query.FnQtdebancado.GreaterThan(0));

            this.Query.Load();
        }
	}
}
