﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.InterfacesDB
{
	public partial class OphisbolCollection : esOphisbolCollection
	{
        /// <summary>
        /// Busca operações no OpenVirtual a serem importadas.
        /// </summary>
        /// <param name="codigoCliente"></param>
        /// <param name="codigoCliente2"></param>
        /// <param name="dataOperacao"></param>
        public void BuscaOperacoesVirtual(string codigo, DateTime dataOperacao)
        {
            this.es.Connection.Name = "VIRTUAL";
            
            this.QueryReset();

            this.Query.Where(this.Query.FcMatricula.Like("%" + codigo + "%"),
                             this.Query.FdDatoper.Equal(dataOperacao),
                             this.Query.FcTpoper.In("V-FINAL", "RESGATE"));

            this.Query.Load();
        }
	}
}
