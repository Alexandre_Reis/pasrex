﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.InterfacesDB
{
	public partial class OpdiabolCollection : esOpdiabolCollection
	{
        public OpdiabolCollection(OphisbolCollection ophisbolCollection)
        {
            for (int i = 0; i < ophisbolCollection.Count; i++)
            {
                //
                Opdiabol p = new Opdiabol();

                // Para cada Coluna de Ophisbol copia para Opdiabol
                foreach (esColumnMetadata colOphisbol in ophisbolCollection.es.Meta.Columns)
                {
                    // Copia todas as colunas menos a Data Historico
                    if (colOphisbol.Name != OphisbolMetadata.ColumnNames.FdTrandata)
                    {
                        esColumnMetadata colOpdiabol = p.es.Meta.Columns.FindByPropertyName(colOphisbol.PropertyName);
                        if (ophisbolCollection[i].GetColumn(colOphisbol.Name) != null)
                        {
                            p.SetColumn(colOpdiabol.Name, ophisbolCollection[i].GetColumn(colOphisbol.Name));
                        }
                    }
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Busca operações no OpenVirtual a serem importadas.
        /// </summary>
        /// <param name="codigoCliente"></param>
        /// <param name="codigoCliente2"></param>
        /// <param name="dataOperacao"></param>
        public void BuscaOperacoesVirtual(string codigo, DateTime dataOperacao)
        {
            this.es.Connection.Name = "VIRTUAL";

            this.QueryReset();

            this.Query.Where(this.Query.FcMatricula.Like("%" + codigo + "%"),
                             this.Query.FdDatoper.Equal(dataOperacao),
                             this.Query.FcTpoper.In("V-FINAL", "RESGATE"));
            this.Query.Load();
        }
	}
}
