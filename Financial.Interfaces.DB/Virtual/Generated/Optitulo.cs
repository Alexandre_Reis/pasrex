/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/12/2013 17:28:41
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;











































































































































































































namespace Financial.InterfacesDB
{

	[Serializable]
	abstract public class esOptituloCollection : esEntityCollection
	{
		public esOptituloCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OptituloCollection";
		}

		#region Query Logic
		protected void InitQuery(esOptituloQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOptituloQuery);
		}
		#endregion
		
		virtual public Optitulo DetachEntity(Optitulo entity)
		{
			return base.DetachEntity(entity) as Optitulo;
		}
		
		virtual public Optitulo AttachEntity(Optitulo entity)
		{
			return base.AttachEntity(entity) as Optitulo;
		}
		
		virtual public void Combine(OptituloCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Optitulo this[int index]
		{
			get
			{
				return base[index] as Optitulo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Optitulo);
		}
	}



	[Serializable]
	abstract public class esOptitulo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOptituloQuery GetDynamicQuery()
		{
			return null;
		}

		public esOptitulo()
		{

		}

		public esOptitulo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String fcCodtitulo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(fcCodtitulo);
			else
				return LoadByPrimaryKeyStoredProcedure(fcCodtitulo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.String fcCodtitulo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esOptituloQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.FcCodtitulo == fcCodtitulo);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String fcCodtitulo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(fcCodtitulo);
			else
				return LoadByPrimaryKeyStoredProcedure(fcCodtitulo);
		}

		private bool LoadByPrimaryKeyDynamic(System.String fcCodtitulo)
		{
			esOptituloQuery query = this.GetDynamicQuery();
			query.Where(query.FcCodtitulo == fcCodtitulo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String fcCodtitulo)
		{
			esParameters parms = new esParameters();
			parms.Add("FC_CODTITULO",fcCodtitulo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "FcCodpapel": this.str.FcCodpapel = (string)value; break;							
						case "FcCodtitulo": this.str.FcCodtitulo = (string)value; break;							
						case "FcNometitulo": this.str.FcNometitulo = (string)value; break;							
						case "FcMinemonico": this.str.FcMinemonico = (string)value; break;							
						case "FcCodemissor": this.str.FcCodemissor = (string)value; break;							
						case "FcIndice": this.str.FcIndice = (string)value; break;							
						case "FcIndice2": this.str.FcIndice2 = (string)value; break;							
						case "FcCodcetsel": this.str.FcCodcetsel = (string)value; break;							
						case "FcCustodia": this.str.FcCustodia = (string)value; break;							
						case "FdDatlimite": this.str.FdDatlimite = (string)value; break;							
						case "FnNominal": this.str.FnNominal = (string)value; break;							
						case "FdDatemissao": this.str.FdDatemissao = (string)value; break;							
						case "FdDatvencimento": this.str.FdDatvencimento = (string)value; break;							
						case "FnTaxanominal": this.str.FnTaxanominal = (string)value; break;							
						case "FcTipotaxa": this.str.FcTipotaxa = (string)value; break;							
						case "FnPercentind1": this.str.FnPercentind1 = (string)value; break;							
						case "FnSpread": this.str.FnSpread = (string)value; break;							
						case "FcAgenda": this.str.FcAgenda = (string)value; break;							
						case "FdDatabase": this.str.FdDatabase = (string)value; break;							
						case "FcCodmercado": this.str.FcCodmercado = (string)value; break;							
						case "FdRentabilidade": this.str.FdRentabilidade = (string)value; break;							
						case "FcEspecie": this.str.FcEspecie = (string)value; break;							
						case "FcTipo": this.str.FcTipo = (string)value; break;							
						case "FcCriteriotit": this.str.FcCriteriotit = (string)value; break;							
						case "FcDiapreco": this.str.FcDiapreco = (string)value; break;							
						case "FnLimitetjlp": this.str.FnLimitetjlp = (string)value; break;							
						case "FcTratamentolimite": this.str.FcTratamentolimite = (string)value; break;							
						case "FnValornominalemi": this.str.FnValornominalemi = (string)value; break;							
						case "FcTipoagenda": this.str.FcTipoagenda = (string)value; break;							
						case "FcIsin": this.str.FcIsin = (string)value; break;							
						case "FcAntecipacao": this.str.FcAntecipacao = (string)value; break;							
						case "FnBasespread": this.str.FnBasespread = (string)value; break;							
						case "FlDebinfra": this.str.FlDebinfra = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "FdDatlimite":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatlimite = (System.DateTime?)value;
							break;
						
						case "FnNominal":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnNominal = (System.Double?)value;
							break;
						
						case "FdDatemissao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatemissao = (System.DateTime?)value;
							break;
						
						case "FdDatvencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatvencimento = (System.DateTime?)value;
							break;
						
						case "FnTaxanominal":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnTaxanominal = (System.Double?)value;
							break;
						
						case "FnPercentind1":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPercentind1 = (System.Double?)value;
							break;
						
						case "FnSpread":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnSpread = (System.Double?)value;
							break;
						
						case "FdDatabase":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatabase = (System.DateTime?)value;
							break;
						
						case "FdRentabilidade":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdRentabilidade = (System.DateTime?)value;
							break;
						
						case "FnLimitetjlp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnLimitetjlp = (System.Double?)value;
							break;
						
						case "FnValornominalemi":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnValornominalemi = (System.Double?)value;
							break;
						
						case "FnBasespread":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FnBasespread = (System.Int32?)value;
							break;
						
						case "FlDebinfra":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FlDebinfra = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OPTITULO.FC_CODPAPEL
		/// </summary>
		virtual public System.String FcCodpapel
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcCodpapel);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcCodpapel, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_CODTITULO
		/// </summary>
		virtual public System.String FcCodtitulo
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcCodtitulo);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcCodtitulo, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_NOMETITULO
		/// </summary>
		virtual public System.String FcNometitulo
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcNometitulo);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcNometitulo, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_MINEMONICO
		/// </summary>
		virtual public System.String FcMinemonico
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcMinemonico);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcMinemonico, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_CODEMISSOR
		/// </summary>
		virtual public System.String FcCodemissor
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcCodemissor);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcCodemissor, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_INDICE
		/// </summary>
		virtual public System.String FcIndice
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcIndice);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcIndice, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_INDICE2
		/// </summary>
		virtual public System.String FcIndice2
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcIndice2);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcIndice2, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_CODCETSEL
		/// </summary>
		virtual public System.String FcCodcetsel
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcCodcetsel);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcCodcetsel, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_CUSTODIA
		/// </summary>
		virtual public System.String FcCustodia
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcCustodia);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FD_DATLIMITE
		/// </summary>
		virtual public System.DateTime? FdDatlimite
		{
			get
			{
				return base.GetSystemDateTime(OptituloMetadata.ColumnNames.FdDatlimite);
			}
			
			set
			{
				base.SetSystemDateTime(OptituloMetadata.ColumnNames.FdDatlimite, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FN_NOMINAL
		/// </summary>
		virtual public System.Double? FnNominal
		{
			get
			{
				return base.GetSystemDouble(OptituloMetadata.ColumnNames.FnNominal);
			}
			
			set
			{
				base.SetSystemDouble(OptituloMetadata.ColumnNames.FnNominal, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FD_DATEMISSAO
		/// </summary>
		virtual public System.DateTime? FdDatemissao
		{
			get
			{
				return base.GetSystemDateTime(OptituloMetadata.ColumnNames.FdDatemissao);
			}
			
			set
			{
				base.SetSystemDateTime(OptituloMetadata.ColumnNames.FdDatemissao, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FD_DATVENCIMENTO
		/// </summary>
		virtual public System.DateTime? FdDatvencimento
		{
			get
			{
				return base.GetSystemDateTime(OptituloMetadata.ColumnNames.FdDatvencimento);
			}
			
			set
			{
				base.SetSystemDateTime(OptituloMetadata.ColumnNames.FdDatvencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FN_TAXANOMINAL
		/// </summary>
		virtual public System.Double? FnTaxanominal
		{
			get
			{
				return base.GetSystemDouble(OptituloMetadata.ColumnNames.FnTaxanominal);
			}
			
			set
			{
				base.SetSystemDouble(OptituloMetadata.ColumnNames.FnTaxanominal, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_TIPOTAXA
		/// </summary>
		virtual public System.String FcTipotaxa
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcTipotaxa);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcTipotaxa, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FN_PERCENTIND1
		/// </summary>
		virtual public System.Double? FnPercentind1
		{
			get
			{
				return base.GetSystemDouble(OptituloMetadata.ColumnNames.FnPercentind1);
			}
			
			set
			{
				base.SetSystemDouble(OptituloMetadata.ColumnNames.FnPercentind1, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FN_SPREAD
		/// </summary>
		virtual public System.Double? FnSpread
		{
			get
			{
				return base.GetSystemDouble(OptituloMetadata.ColumnNames.FnSpread);
			}
			
			set
			{
				base.SetSystemDouble(OptituloMetadata.ColumnNames.FnSpread, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_AGENDA
		/// </summary>
		virtual public System.String FcAgenda
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcAgenda);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcAgenda, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.fd_database
		/// </summary>
		virtual public System.DateTime? FdDatabase
		{
			get
			{
				return base.GetSystemDateTime(OptituloMetadata.ColumnNames.FdDatabase);
			}
			
			set
			{
				base.SetSystemDateTime(OptituloMetadata.ColumnNames.FdDatabase, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_CODMERCADO
		/// </summary>
		virtual public System.String FcCodmercado
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcCodmercado);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcCodmercado, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FD_RENTABILIDADE
		/// </summary>
		virtual public System.DateTime? FdRentabilidade
		{
			get
			{
				return base.GetSystemDateTime(OptituloMetadata.ColumnNames.FdRentabilidade);
			}
			
			set
			{
				base.SetSystemDateTime(OptituloMetadata.ColumnNames.FdRentabilidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_ESPECIE
		/// </summary>
		virtual public System.String FcEspecie
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcEspecie);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcEspecie, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_TIPO
		/// </summary>
		virtual public System.String FcTipo
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcTipo);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcTipo, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_CRITERIOTIT
		/// </summary>
		virtual public System.String FcCriteriotit
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcCriteriotit);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcCriteriotit, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_DIAPRECO
		/// </summary>
		virtual public System.String FcDiapreco
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcDiapreco);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcDiapreco, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FN_LIMITETJLP
		/// </summary>
		virtual public System.Double? FnLimitetjlp
		{
			get
			{
				return base.GetSystemDouble(OptituloMetadata.ColumnNames.FnLimitetjlp);
			}
			
			set
			{
				base.SetSystemDouble(OptituloMetadata.ColumnNames.FnLimitetjlp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_TRATAMENTOLIMITE
		/// </summary>
		virtual public System.String FcTratamentolimite
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcTratamentolimite);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcTratamentolimite, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FN_VALORNOMINALEMI
		/// </summary>
		virtual public System.Double? FnValornominalemi
		{
			get
			{
				return base.GetSystemDouble(OptituloMetadata.ColumnNames.FnValornominalemi);
			}
			
			set
			{
				base.SetSystemDouble(OptituloMetadata.ColumnNames.FnValornominalemi, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_TIPOAGENDA
		/// </summary>
		virtual public System.String FcTipoagenda
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcTipoagenda);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcTipoagenda, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_ISIN
		/// </summary>
		virtual public System.String FcIsin
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcIsin);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcIsin, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FC_ANTECIPACAO
		/// </summary>
		virtual public System.String FcAntecipacao
		{
			get
			{
				return base.GetSystemString(OptituloMetadata.ColumnNames.FcAntecipacao);
			}
			
			set
			{
				base.SetSystemString(OptituloMetadata.ColumnNames.FcAntecipacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FN_BASESPREAD
		/// </summary>
		virtual public System.Int32? FnBasespread
		{
			get
			{
				return base.GetSystemInt32(OptituloMetadata.ColumnNames.FnBasespread);
			}
			
			set
			{
				base.SetSystemInt32(OptituloMetadata.ColumnNames.FnBasespread, value);
			}
		}
		
		/// <summary>
		/// Maps to OPTITULO.FL_DEBINFRA
		/// </summary>
		virtual public System.Int32? FlDebinfra
		{
			get
			{
				return base.GetSystemInt32(OptituloMetadata.ColumnNames.FlDebinfra);
			}
			
			set
			{
				base.SetSystemInt32(OptituloMetadata.ColumnNames.FlDebinfra, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOptitulo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String FcCodpapel
			{
				get
				{
					System.String data = entity.FcCodpapel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodpapel = null;
					else entity.FcCodpapel = Convert.ToString(value);
				}
			}
				
			public System.String FcCodtitulo
			{
				get
				{
					System.String data = entity.FcCodtitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodtitulo = null;
					else entity.FcCodtitulo = Convert.ToString(value);
				}
			}
				
			public System.String FcNometitulo
			{
				get
				{
					System.String data = entity.FcNometitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNometitulo = null;
					else entity.FcNometitulo = Convert.ToString(value);
				}
			}
				
			public System.String FcMinemonico
			{
				get
				{
					System.String data = entity.FcMinemonico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcMinemonico = null;
					else entity.FcMinemonico = Convert.ToString(value);
				}
			}
				
			public System.String FcCodemissor
			{
				get
				{
					System.String data = entity.FcCodemissor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodemissor = null;
					else entity.FcCodemissor = Convert.ToString(value);
				}
			}
				
			public System.String FcIndice
			{
				get
				{
					System.String data = entity.FcIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcIndice = null;
					else entity.FcIndice = Convert.ToString(value);
				}
			}
				
			public System.String FcIndice2
			{
				get
				{
					System.String data = entity.FcIndice2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcIndice2 = null;
					else entity.FcIndice2 = Convert.ToString(value);
				}
			}
				
			public System.String FcCodcetsel
			{
				get
				{
					System.String data = entity.FcCodcetsel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodcetsel = null;
					else entity.FcCodcetsel = Convert.ToString(value);
				}
			}
				
			public System.String FcCustodia
			{
				get
				{
					System.String data = entity.FcCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCustodia = null;
					else entity.FcCustodia = Convert.ToString(value);
				}
			}
				
			public System.String FdDatlimite
			{
				get
				{
					System.DateTime? data = entity.FdDatlimite;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatlimite = null;
					else entity.FdDatlimite = Convert.ToDateTime(value);
				}
			}
				
			public System.String FnNominal
			{
				get
				{
					System.Double? data = entity.FnNominal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnNominal = null;
					else entity.FnNominal = Convert.ToDouble(value);
				}
			}
				
			public System.String FdDatemissao
			{
				get
				{
					System.DateTime? data = entity.FdDatemissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatemissao = null;
					else entity.FdDatemissao = Convert.ToDateTime(value);
				}
			}
				
			public System.String FdDatvencimento
			{
				get
				{
					System.DateTime? data = entity.FdDatvencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatvencimento = null;
					else entity.FdDatvencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String FnTaxanominal
			{
				get
				{
					System.Double? data = entity.FnTaxanominal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnTaxanominal = null;
					else entity.FnTaxanominal = Convert.ToDouble(value);
				}
			}
				
			public System.String FcTipotaxa
			{
				get
				{
					System.String data = entity.FcTipotaxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTipotaxa = null;
					else entity.FcTipotaxa = Convert.ToString(value);
				}
			}
				
			public System.String FnPercentind1
			{
				get
				{
					System.Double? data = entity.FnPercentind1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPercentind1 = null;
					else entity.FnPercentind1 = Convert.ToDouble(value);
				}
			}
				
			public System.String FnSpread
			{
				get
				{
					System.Double? data = entity.FnSpread;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnSpread = null;
					else entity.FnSpread = Convert.ToDouble(value);
				}
			}
				
			public System.String FcAgenda
			{
				get
				{
					System.String data = entity.FcAgenda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcAgenda = null;
					else entity.FcAgenda = Convert.ToString(value);
				}
			}
				
			public System.String FdDatabase
			{
				get
				{
					System.DateTime? data = entity.FdDatabase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatabase = null;
					else entity.FdDatabase = Convert.ToDateTime(value);
				}
			}
				
			public System.String FcCodmercado
			{
				get
				{
					System.String data = entity.FcCodmercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodmercado = null;
					else entity.FcCodmercado = Convert.ToString(value);
				}
			}
				
			public System.String FdRentabilidade
			{
				get
				{
					System.DateTime? data = entity.FdRentabilidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdRentabilidade = null;
					else entity.FdRentabilidade = Convert.ToDateTime(value);
				}
			}
				
			public System.String FcEspecie
			{
				get
				{
					System.String data = entity.FcEspecie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcEspecie = null;
					else entity.FcEspecie = Convert.ToString(value);
				}
			}
				
			public System.String FcTipo
			{
				get
				{
					System.String data = entity.FcTipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTipo = null;
					else entity.FcTipo = Convert.ToString(value);
				}
			}
				
			public System.String FcCriteriotit
			{
				get
				{
					System.String data = entity.FcCriteriotit;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCriteriotit = null;
					else entity.FcCriteriotit = Convert.ToString(value);
				}
			}
				
			public System.String FcDiapreco
			{
				get
				{
					System.String data = entity.FcDiapreco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcDiapreco = null;
					else entity.FcDiapreco = Convert.ToString(value);
				}
			}
				
			public System.String FnLimitetjlp
			{
				get
				{
					System.Double? data = entity.FnLimitetjlp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnLimitetjlp = null;
					else entity.FnLimitetjlp = Convert.ToDouble(value);
				}
			}
				
			public System.String FcTratamentolimite
			{
				get
				{
					System.String data = entity.FcTratamentolimite;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTratamentolimite = null;
					else entity.FcTratamentolimite = Convert.ToString(value);
				}
			}
				
			public System.String FnValornominalemi
			{
				get
				{
					System.Double? data = entity.FnValornominalemi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnValornominalemi = null;
					else entity.FnValornominalemi = Convert.ToDouble(value);
				}
			}
				
			public System.String FcTipoagenda
			{
				get
				{
					System.String data = entity.FcTipoagenda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTipoagenda = null;
					else entity.FcTipoagenda = Convert.ToString(value);
				}
			}
				
			public System.String FcIsin
			{
				get
				{
					System.String data = entity.FcIsin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcIsin = null;
					else entity.FcIsin = Convert.ToString(value);
				}
			}
				
			public System.String FcAntecipacao
			{
				get
				{
					System.String data = entity.FcAntecipacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcAntecipacao = null;
					else entity.FcAntecipacao = Convert.ToString(value);
				}
			}
				
			public System.String FnBasespread
			{
				get
				{
					System.Int32? data = entity.FnBasespread;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnBasespread = null;
					else entity.FnBasespread = Convert.ToInt32(value);
				}
			}
				
			public System.String FlDebinfra
			{
				get
				{
					System.Int32? data = entity.FlDebinfra;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlDebinfra = null;
					else entity.FlDebinfra = Convert.ToInt32(value);
				}
			}
			

			private esOptitulo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOptituloQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOptitulo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Optitulo : esOptitulo
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOptituloQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OptituloMetadata.Meta();
			}
		}	
		

		public esQueryItem FcCodpapel
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcCodpapel, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodtitulo
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcCodtitulo, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNometitulo
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcNometitulo, esSystemType.String);
			}
		} 
		
		public esQueryItem FcMinemonico
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcMinemonico, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodemissor
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcCodemissor, esSystemType.String);
			}
		} 
		
		public esQueryItem FcIndice
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcIndice, esSystemType.String);
			}
		} 
		
		public esQueryItem FcIndice2
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcIndice2, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodcetsel
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcCodcetsel, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCustodia
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcCustodia, esSystemType.String);
			}
		} 
		
		public esQueryItem FdDatlimite
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FdDatlimite, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FnNominal
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FnNominal, esSystemType.Double);
			}
		} 
		
		public esQueryItem FdDatemissao
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FdDatemissao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FdDatvencimento
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FdDatvencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FnTaxanominal
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FnTaxanominal, esSystemType.Double);
			}
		} 
		
		public esQueryItem FcTipotaxa
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcTipotaxa, esSystemType.String);
			}
		} 
		
		public esQueryItem FnPercentind1
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FnPercentind1, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnSpread
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FnSpread, esSystemType.Double);
			}
		} 
		
		public esQueryItem FcAgenda
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcAgenda, esSystemType.String);
			}
		} 
		
		public esQueryItem FdDatabase
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FdDatabase, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FcCodmercado
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcCodmercado, esSystemType.String);
			}
		} 
		
		public esQueryItem FdRentabilidade
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FdRentabilidade, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FcEspecie
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcEspecie, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTipo
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcTipo, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCriteriotit
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcCriteriotit, esSystemType.String);
			}
		} 
		
		public esQueryItem FcDiapreco
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcDiapreco, esSystemType.String);
			}
		} 
		
		public esQueryItem FnLimitetjlp
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FnLimitetjlp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FcTratamentolimite
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcTratamentolimite, esSystemType.String);
			}
		} 
		
		public esQueryItem FnValornominalemi
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FnValornominalemi, esSystemType.Double);
			}
		} 
		
		public esQueryItem FcTipoagenda
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcTipoagenda, esSystemType.String);
			}
		} 
		
		public esQueryItem FcIsin
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcIsin, esSystemType.String);
			}
		} 
		
		public esQueryItem FcAntecipacao
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FcAntecipacao, esSystemType.String);
			}
		} 
		
		public esQueryItem FnBasespread
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FnBasespread, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FlDebinfra
		{
			get
			{
				return new esQueryItem(this, OptituloMetadata.ColumnNames.FlDebinfra, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OptituloCollection")]
	public partial class OptituloCollection : esOptituloCollection, IEnumerable<Optitulo>
	{
		public OptituloCollection()
		{

		}
		
		public static implicit operator List<Optitulo>(OptituloCollection coll)
		{
			List<Optitulo> list = new List<Optitulo>();
			
			foreach (Optitulo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OptituloMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OptituloQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Optitulo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Optitulo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OptituloQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OptituloQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OptituloQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Optitulo AddNew()
		{
			Optitulo entity = base.AddNewEntity() as Optitulo;
			
			return entity;
		}

		public Optitulo FindByPrimaryKey(System.String fcCodtitulo)
		{
			return base.FindByPrimaryKey(fcCodtitulo) as Optitulo;
		}


		#region IEnumerable<Optitulo> Members

		IEnumerator<Optitulo> IEnumerable<Optitulo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Optitulo;
			}
		}

		#endregion
		
		private OptituloQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OPTITULO' table
	/// </summary>

	[Serializable]
	public partial class Optitulo : esOptitulo
	{
		public Optitulo()
		{

		}
	
		public Optitulo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OptituloMetadata.Meta();
			}
		}
		
		
		
		override protected esOptituloQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OptituloQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OptituloQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OptituloQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OptituloQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OptituloQuery query;
	}



	[Serializable]
	public partial class OptituloQuery : esOptituloQuery
	{
		public OptituloQuery()
		{

		}		
		
		public OptituloQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OptituloMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OptituloMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcCodpapel, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcCodpapel;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcCodtitulo, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcCodtitulo;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 6;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcNometitulo, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcNometitulo;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcMinemonico, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcMinemonico;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcCodemissor, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcCodemissor;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcIndice, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcIndice;
			c.CharacterMaxLength = 15;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcIndice2, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcIndice2;
			c.CharacterMaxLength = 15;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcCodcetsel, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcCodcetsel;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcCustodia, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcCustodia;
			c.CharacterMaxLength = 6;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FdDatlimite, 9, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OptituloMetadata.PropertyNames.FdDatlimite;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FnNominal, 10, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OptituloMetadata.PropertyNames.FnNominal;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FdDatemissao, 11, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OptituloMetadata.PropertyNames.FdDatemissao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FdDatvencimento, 12, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OptituloMetadata.PropertyNames.FdDatvencimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FnTaxanominal, 13, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OptituloMetadata.PropertyNames.FnTaxanominal;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcTipotaxa, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcTipotaxa;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FnPercentind1, 15, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OptituloMetadata.PropertyNames.FnPercentind1;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FnSpread, 16, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OptituloMetadata.PropertyNames.FnSpread;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcAgenda, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcAgenda;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FdDatabase, 18, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OptituloMetadata.PropertyNames.FdDatabase;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcCodmercado, 19, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcCodmercado;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FdRentabilidade, 20, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OptituloMetadata.PropertyNames.FdRentabilidade;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcEspecie, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcEspecie;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcTipo, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcTipo;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcCriteriotit, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcCriteriotit;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcDiapreco, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcDiapreco;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FnLimitetjlp, 25, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OptituloMetadata.PropertyNames.FnLimitetjlp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcTratamentolimite, 26, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcTratamentolimite;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FnValornominalemi, 27, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OptituloMetadata.PropertyNames.FnValornominalemi;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcTipoagenda, 28, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcTipoagenda;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcIsin, 29, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcIsin;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FcAntecipacao, 30, typeof(System.String), esSystemType.String);
			c.PropertyName = OptituloMetadata.PropertyNames.FcAntecipacao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FnBasespread, 31, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OptituloMetadata.PropertyNames.FnBasespread;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OptituloMetadata.ColumnNames.FlDebinfra, 32, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OptituloMetadata.PropertyNames.FlDebinfra;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OptituloMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string FcCodpapel = "FC_CODPAPEL";
			 public const string FcCodtitulo = "FC_CODTITULO";
			 public const string FcNometitulo = "FC_NOMETITULO";
			 public const string FcMinemonico = "FC_MINEMONICO";
			 public const string FcCodemissor = "FC_CODEMISSOR";
			 public const string FcIndice = "FC_INDICE";
			 public const string FcIndice2 = "FC_INDICE2";
			 public const string FcCodcetsel = "FC_CODCETSEL";
			 public const string FcCustodia = "FC_CUSTODIA";
			 public const string FdDatlimite = "FD_DATLIMITE";
			 public const string FnNominal = "FN_NOMINAL";
			 public const string FdDatemissao = "FD_DATEMISSAO";
			 public const string FdDatvencimento = "FD_DATVENCIMENTO";
			 public const string FnTaxanominal = "FN_TAXANOMINAL";
			 public const string FcTipotaxa = "FC_TIPOTAXA";
			 public const string FnPercentind1 = "FN_PERCENTIND1";
			 public const string FnSpread = "FN_SPREAD";
			 public const string FcAgenda = "FC_AGENDA";
			 public const string FdDatabase = "fd_database";
			 public const string FcCodmercado = "FC_CODMERCADO";
			 public const string FdRentabilidade = "FD_RENTABILIDADE";
			 public const string FcEspecie = "FC_ESPECIE";
			 public const string FcTipo = "FC_TIPO";
			 public const string FcCriteriotit = "FC_CRITERIOTIT";
			 public const string FcDiapreco = "FC_DIAPRECO";
			 public const string FnLimitetjlp = "FN_LIMITETJLP";
			 public const string FcTratamentolimite = "FC_TRATAMENTOLIMITE";
			 public const string FnValornominalemi = "FN_VALORNOMINALEMI";
			 public const string FcTipoagenda = "FC_TIPOAGENDA";
			 public const string FcIsin = "FC_ISIN";
			 public const string FcAntecipacao = "FC_ANTECIPACAO";
			 public const string FnBasespread = "FN_BASESPREAD";
			 public const string FlDebinfra = "FL_DEBINFRA";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string FcCodpapel = "FcCodpapel";
			 public const string FcCodtitulo = "FcCodtitulo";
			 public const string FcNometitulo = "FcNometitulo";
			 public const string FcMinemonico = "FcMinemonico";
			 public const string FcCodemissor = "FcCodemissor";
			 public const string FcIndice = "FcIndice";
			 public const string FcIndice2 = "FcIndice2";
			 public const string FcCodcetsel = "FcCodcetsel";
			 public const string FcCustodia = "FcCustodia";
			 public const string FdDatlimite = "FdDatlimite";
			 public const string FnNominal = "FnNominal";
			 public const string FdDatemissao = "FdDatemissao";
			 public const string FdDatvencimento = "FdDatvencimento";
			 public const string FnTaxanominal = "FnTaxanominal";
			 public const string FcTipotaxa = "FcTipotaxa";
			 public const string FnPercentind1 = "FnPercentind1";
			 public const string FnSpread = "FnSpread";
			 public const string FcAgenda = "FcAgenda";
			 public const string FdDatabase = "FdDatabase";
			 public const string FcCodmercado = "FcCodmercado";
			 public const string FdRentabilidade = "FdRentabilidade";
			 public const string FcEspecie = "FcEspecie";
			 public const string FcTipo = "FcTipo";
			 public const string FcCriteriotit = "FcCriteriotit";
			 public const string FcDiapreco = "FcDiapreco";
			 public const string FnLimitetjlp = "FnLimitetjlp";
			 public const string FcTratamentolimite = "FcTratamentolimite";
			 public const string FnValornominalemi = "FnValornominalemi";
			 public const string FcTipoagenda = "FcTipoagenda";
			 public const string FcIsin = "FcIsin";
			 public const string FcAntecipacao = "FcAntecipacao";
			 public const string FnBasespread = "FnBasespread";
			 public const string FlDebinfra = "FlDebinfra";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OptituloMetadata))
			{
				if(OptituloMetadata.mapDelegates == null)
				{
					OptituloMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OptituloMetadata.meta == null)
				{
					OptituloMetadata.meta = new OptituloMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("FC_CODPAPEL", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODTITULO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NOMETITULO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_MINEMONICO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODEMISSOR", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_INDICE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_INDICE2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODCETSEL", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CUSTODIA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FD_DATLIMITE", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FN_NOMINAL", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FD_DATEMISSAO", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FD_DATVENCIMENTO", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FN_TAXANOMINAL", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FC_TIPOTAXA", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FN_PERCENTIND1", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_SPREAD", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FC_AGENDA", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("fd_database", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FC_CODMERCADO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FD_RENTABILIDADE", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FC_ESPECIE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TIPO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CRITERIOTIT", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_DIAPRECO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FN_LIMITETJLP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FC_TRATAMENTOLIMITE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FN_VALORNOMINALEMI", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FC_TIPOAGENDA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ISIN", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ANTECIPACAO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FN_BASESPREAD", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FL_DEBINFRA", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "OPTITULO";
				meta.Destination = "OPTITULO";
				
				meta.spInsert = "proc_OPTITULOInsert";				
				meta.spUpdate = "proc_OPTITULOUpdate";		
				meta.spDelete = "proc_OPTITULODelete";
				meta.spLoadAll = "proc_OPTITULOLoadAll";
				meta.spLoadByPrimaryKey = "proc_OPTITULOLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OptituloMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
