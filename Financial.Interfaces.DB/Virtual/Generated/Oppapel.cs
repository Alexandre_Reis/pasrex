/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/12/2013 17:28:41
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;











































































































































































































namespace Financial.InterfacesDB
{

	[Serializable]
	abstract public class esOppapelCollection : esEntityCollection
	{
		public esOppapelCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OppapelCollection";
		}

		#region Query Logic
		protected void InitQuery(esOppapelQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOppapelQuery);
		}
		#endregion
		
		virtual public Oppapel DetachEntity(Oppapel entity)
		{
			return base.DetachEntity(entity) as Oppapel;
		}
		
		virtual public Oppapel AttachEntity(Oppapel entity)
		{
			return base.AttachEntity(entity) as Oppapel;
		}
		
		virtual public void Combine(OppapelCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Oppapel this[int index]
		{
			get
			{
				return base[index] as Oppapel;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Oppapel);
		}
	}



	[Serializable]
	abstract public class esOppapel : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOppapelQuery GetDynamicQuery()
		{
			return null;
		}

		public esOppapel()
		{

		}

		public esOppapel(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String fcCodpapel)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(fcCodpapel);
			else
				return LoadByPrimaryKeyStoredProcedure(fcCodpapel);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.String fcCodpapel)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esOppapelQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.FcCodpapel == fcCodpapel);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String fcCodpapel)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(fcCodpapel);
			else
				return LoadByPrimaryKeyStoredProcedure(fcCodpapel);
		}

		private bool LoadByPrimaryKeyDynamic(System.String fcCodpapel)
		{
			esOppapelQuery query = this.GetDynamicQuery();
			query.Where(query.FcCodpapel == fcCodpapel);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String fcCodpapel)
		{
			esParameters parms = new esParameters();
			parms.Add("FC_CODPAPEL",fcCodpapel);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "FcCodpapel": this.str.FcCodpapel = (string)value; break;							
						case "FcPapel": this.str.FcPapel = (string)value; break;							
						case "FcTipopapel": this.str.FcTipopapel = (string)value; break;							
						case "FcDescricao": this.str.FcDescricao = (string)value; break;							
						case "FcRentabilidade": this.str.FcRentabilidade = (string)value; break;							
						case "FnDecimais": this.str.FnDecimais = (string)value; break;							
						case "FcCod1088": this.str.FcCod1088 = (string)value; break;							
						case "FcTipocurva": this.str.FcTipocurva = (string)value; break;							
						case "FcBasecalculo": this.str.FcBasecalculo = (string)value; break;							
						case "FcTipovolume": this.str.FcTipovolume = (string)value; break;							
						case "FcClasse": this.str.FcClasse = (string)value; break;							
						case "FcCriterio": this.str.FcCriterio = (string)value; break;							
						case "FcCustodia": this.str.FcCustodia = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "FnDecimais":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnDecimais = (System.Double?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OPPAPEL.FC_CODPAPEL
		/// </summary>
		virtual public System.String FcCodpapel
		{
			get
			{
				return base.GetSystemString(OppapelMetadata.ColumnNames.FcCodpapel);
			}
			
			set
			{
				base.SetSystemString(OppapelMetadata.ColumnNames.FcCodpapel, value);
			}
		}
		
		/// <summary>
		/// Maps to OPPAPEL.FC_PAPEL
		/// </summary>
		virtual public System.String FcPapel
		{
			get
			{
				return base.GetSystemString(OppapelMetadata.ColumnNames.FcPapel);
			}
			
			set
			{
				base.SetSystemString(OppapelMetadata.ColumnNames.FcPapel, value);
			}
		}
		
		/// <summary>
		/// Maps to OPPAPEL.FC_TIPOPAPEL
		/// </summary>
		virtual public System.String FcTipopapel
		{
			get
			{
				return base.GetSystemString(OppapelMetadata.ColumnNames.FcTipopapel);
			}
			
			set
			{
				base.SetSystemString(OppapelMetadata.ColumnNames.FcTipopapel, value);
			}
		}
		
		/// <summary>
		/// Maps to OPPAPEL.FC_DESCRICAO
		/// </summary>
		virtual public System.String FcDescricao
		{
			get
			{
				return base.GetSystemString(OppapelMetadata.ColumnNames.FcDescricao);
			}
			
			set
			{
				base.SetSystemString(OppapelMetadata.ColumnNames.FcDescricao, value);
			}
		}
		
		/// <summary>
		/// Maps to OPPAPEL.FC_RENTABILIDADE
		/// </summary>
		virtual public System.String FcRentabilidade
		{
			get
			{
				return base.GetSystemString(OppapelMetadata.ColumnNames.FcRentabilidade);
			}
			
			set
			{
				base.SetSystemString(OppapelMetadata.ColumnNames.FcRentabilidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OPPAPEL.FN_DECIMAIS
		/// </summary>
		virtual public System.Double? FnDecimais
		{
			get
			{
				return base.GetSystemDouble(OppapelMetadata.ColumnNames.FnDecimais);
			}
			
			set
			{
				base.SetSystemDouble(OppapelMetadata.ColumnNames.FnDecimais, value);
			}
		}
		
		/// <summary>
		/// Maps to OPPAPEL.FC_COD1088
		/// </summary>
		virtual public System.String FcCod1088
		{
			get
			{
				return base.GetSystemString(OppapelMetadata.ColumnNames.FcCod1088);
			}
			
			set
			{
				base.SetSystemString(OppapelMetadata.ColumnNames.FcCod1088, value);
			}
		}
		
		/// <summary>
		/// Maps to OPPAPEL.FC_TIPOCURVA
		/// </summary>
		virtual public System.String FcTipocurva
		{
			get
			{
				return base.GetSystemString(OppapelMetadata.ColumnNames.FcTipocurva);
			}
			
			set
			{
				base.SetSystemString(OppapelMetadata.ColumnNames.FcTipocurva, value);
			}
		}
		
		/// <summary>
		/// Maps to OPPAPEL.FC_BASECALCULO
		/// </summary>
		virtual public System.String FcBasecalculo
		{
			get
			{
				return base.GetSystemString(OppapelMetadata.ColumnNames.FcBasecalculo);
			}
			
			set
			{
				base.SetSystemString(OppapelMetadata.ColumnNames.FcBasecalculo, value);
			}
		}
		
		/// <summary>
		/// Maps to OPPAPEL.FC_TIPOVOLUME
		/// </summary>
		virtual public System.String FcTipovolume
		{
			get
			{
				return base.GetSystemString(OppapelMetadata.ColumnNames.FcTipovolume);
			}
			
			set
			{
				base.SetSystemString(OppapelMetadata.ColumnNames.FcTipovolume, value);
			}
		}
		
		/// <summary>
		/// Maps to OPPAPEL.FC_CLASSE
		/// </summary>
		virtual public System.String FcClasse
		{
			get
			{
				return base.GetSystemString(OppapelMetadata.ColumnNames.FcClasse);
			}
			
			set
			{
				base.SetSystemString(OppapelMetadata.ColumnNames.FcClasse, value);
			}
		}
		
		/// <summary>
		/// Maps to OPPAPEL.FC_CRITERIO
		/// </summary>
		virtual public System.String FcCriterio
		{
			get
			{
				return base.GetSystemString(OppapelMetadata.ColumnNames.FcCriterio);
			}
			
			set
			{
				base.SetSystemString(OppapelMetadata.ColumnNames.FcCriterio, value);
			}
		}
		
		/// <summary>
		/// Maps to OPPAPEL.FC_CUSTODIA
		/// </summary>
		virtual public System.String FcCustodia
		{
			get
			{
				return base.GetSystemString(OppapelMetadata.ColumnNames.FcCustodia);
			}
			
			set
			{
				base.SetSystemString(OppapelMetadata.ColumnNames.FcCustodia, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOppapel entity)
			{
				this.entity = entity;
			}
			
	
			public System.String FcCodpapel
			{
				get
				{
					System.String data = entity.FcCodpapel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodpapel = null;
					else entity.FcCodpapel = Convert.ToString(value);
				}
			}
				
			public System.String FcPapel
			{
				get
				{
					System.String data = entity.FcPapel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcPapel = null;
					else entity.FcPapel = Convert.ToString(value);
				}
			}
				
			public System.String FcTipopapel
			{
				get
				{
					System.String data = entity.FcTipopapel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTipopapel = null;
					else entity.FcTipopapel = Convert.ToString(value);
				}
			}
				
			public System.String FcDescricao
			{
				get
				{
					System.String data = entity.FcDescricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcDescricao = null;
					else entity.FcDescricao = Convert.ToString(value);
				}
			}
				
			public System.String FcRentabilidade
			{
				get
				{
					System.String data = entity.FcRentabilidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcRentabilidade = null;
					else entity.FcRentabilidade = Convert.ToString(value);
				}
			}
				
			public System.String FnDecimais
			{
				get
				{
					System.Double? data = entity.FnDecimais;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnDecimais = null;
					else entity.FnDecimais = Convert.ToDouble(value);
				}
			}
				
			public System.String FcCod1088
			{
				get
				{
					System.String data = entity.FcCod1088;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCod1088 = null;
					else entity.FcCod1088 = Convert.ToString(value);
				}
			}
				
			public System.String FcTipocurva
			{
				get
				{
					System.String data = entity.FcTipocurva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTipocurva = null;
					else entity.FcTipocurva = Convert.ToString(value);
				}
			}
				
			public System.String FcBasecalculo
			{
				get
				{
					System.String data = entity.FcBasecalculo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcBasecalculo = null;
					else entity.FcBasecalculo = Convert.ToString(value);
				}
			}
				
			public System.String FcTipovolume
			{
				get
				{
					System.String data = entity.FcTipovolume;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTipovolume = null;
					else entity.FcTipovolume = Convert.ToString(value);
				}
			}
				
			public System.String FcClasse
			{
				get
				{
					System.String data = entity.FcClasse;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcClasse = null;
					else entity.FcClasse = Convert.ToString(value);
				}
			}
				
			public System.String FcCriterio
			{
				get
				{
					System.String data = entity.FcCriterio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCriterio = null;
					else entity.FcCriterio = Convert.ToString(value);
				}
			}
				
			public System.String FcCustodia
			{
				get
				{
					System.String data = entity.FcCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCustodia = null;
					else entity.FcCustodia = Convert.ToString(value);
				}
			}
			

			private esOppapel entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOppapelQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOppapel can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Oppapel : esOppapel
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOppapelQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OppapelMetadata.Meta();
			}
		}	
		

		public esQueryItem FcCodpapel
		{
			get
			{
				return new esQueryItem(this, OppapelMetadata.ColumnNames.FcCodpapel, esSystemType.String);
			}
		} 
		
		public esQueryItem FcPapel
		{
			get
			{
				return new esQueryItem(this, OppapelMetadata.ColumnNames.FcPapel, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTipopapel
		{
			get
			{
				return new esQueryItem(this, OppapelMetadata.ColumnNames.FcTipopapel, esSystemType.String);
			}
		} 
		
		public esQueryItem FcDescricao
		{
			get
			{
				return new esQueryItem(this, OppapelMetadata.ColumnNames.FcDescricao, esSystemType.String);
			}
		} 
		
		public esQueryItem FcRentabilidade
		{
			get
			{
				return new esQueryItem(this, OppapelMetadata.ColumnNames.FcRentabilidade, esSystemType.String);
			}
		} 
		
		public esQueryItem FnDecimais
		{
			get
			{
				return new esQueryItem(this, OppapelMetadata.ColumnNames.FnDecimais, esSystemType.Double);
			}
		} 
		
		public esQueryItem FcCod1088
		{
			get
			{
				return new esQueryItem(this, OppapelMetadata.ColumnNames.FcCod1088, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTipocurva
		{
			get
			{
				return new esQueryItem(this, OppapelMetadata.ColumnNames.FcTipocurva, esSystemType.String);
			}
		} 
		
		public esQueryItem FcBasecalculo
		{
			get
			{
				return new esQueryItem(this, OppapelMetadata.ColumnNames.FcBasecalculo, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTipovolume
		{
			get
			{
				return new esQueryItem(this, OppapelMetadata.ColumnNames.FcTipovolume, esSystemType.String);
			}
		} 
		
		public esQueryItem FcClasse
		{
			get
			{
				return new esQueryItem(this, OppapelMetadata.ColumnNames.FcClasse, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCriterio
		{
			get
			{
				return new esQueryItem(this, OppapelMetadata.ColumnNames.FcCriterio, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCustodia
		{
			get
			{
				return new esQueryItem(this, OppapelMetadata.ColumnNames.FcCustodia, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OppapelCollection")]
	public partial class OppapelCollection : esOppapelCollection, IEnumerable<Oppapel>
	{
		public OppapelCollection()
		{

		}
		
		public static implicit operator List<Oppapel>(OppapelCollection coll)
		{
			List<Oppapel> list = new List<Oppapel>();
			
			foreach (Oppapel emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OppapelMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OppapelQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Oppapel(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Oppapel();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OppapelQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OppapelQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OppapelQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Oppapel AddNew()
		{
			Oppapel entity = base.AddNewEntity() as Oppapel;
			
			return entity;
		}

		public Oppapel FindByPrimaryKey(System.String fcCodpapel)
		{
			return base.FindByPrimaryKey(fcCodpapel) as Oppapel;
		}


		#region IEnumerable<Oppapel> Members

		IEnumerator<Oppapel> IEnumerable<Oppapel>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Oppapel;
			}
		}

		#endregion
		
		private OppapelQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OPPAPEL' table
	/// </summary>

	[Serializable]
	public partial class Oppapel : esOppapel
	{
		public Oppapel()
		{

		}
	
		public Oppapel(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OppapelMetadata.Meta();
			}
		}
		
		
		
		override protected esOppapelQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OppapelQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OppapelQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OppapelQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OppapelQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OppapelQuery query;
	}



	[Serializable]
	public partial class OppapelQuery : esOppapelQuery
	{
		public OppapelQuery()
		{

		}		
		
		public OppapelQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OppapelMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OppapelMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OppapelMetadata.ColumnNames.FcCodpapel, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = OppapelMetadata.PropertyNames.FcCodpapel;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OppapelMetadata.ColumnNames.FcPapel, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = OppapelMetadata.PropertyNames.FcPapel;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OppapelMetadata.ColumnNames.FcTipopapel, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = OppapelMetadata.PropertyNames.FcTipopapel;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OppapelMetadata.ColumnNames.FcDescricao, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = OppapelMetadata.PropertyNames.FcDescricao;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OppapelMetadata.ColumnNames.FcRentabilidade, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = OppapelMetadata.PropertyNames.FcRentabilidade;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OppapelMetadata.ColumnNames.FnDecimais, 5, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OppapelMetadata.PropertyNames.FnDecimais;	
			c.NumericPrecision = 15;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OppapelMetadata.ColumnNames.FcCod1088, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = OppapelMetadata.PropertyNames.FcCod1088;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OppapelMetadata.ColumnNames.FcTipocurva, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = OppapelMetadata.PropertyNames.FcTipocurva;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OppapelMetadata.ColumnNames.FcBasecalculo, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = OppapelMetadata.PropertyNames.FcBasecalculo;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OppapelMetadata.ColumnNames.FcTipovolume, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = OppapelMetadata.PropertyNames.FcTipovolume;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OppapelMetadata.ColumnNames.FcClasse, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = OppapelMetadata.PropertyNames.FcClasse;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OppapelMetadata.ColumnNames.FcCriterio, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = OppapelMetadata.PropertyNames.FcCriterio;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OppapelMetadata.ColumnNames.FcCustodia, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = OppapelMetadata.PropertyNames.FcCustodia;
			c.CharacterMaxLength = 15;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OppapelMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string FcCodpapel = "FC_CODPAPEL";
			 public const string FcPapel = "FC_PAPEL";
			 public const string FcTipopapel = "FC_TIPOPAPEL";
			 public const string FcDescricao = "FC_DESCRICAO";
			 public const string FcRentabilidade = "FC_RENTABILIDADE";
			 public const string FnDecimais = "FN_DECIMAIS";
			 public const string FcCod1088 = "FC_COD1088";
			 public const string FcTipocurva = "FC_TIPOCURVA";
			 public const string FcBasecalculo = "FC_BASECALCULO";
			 public const string FcTipovolume = "FC_TIPOVOLUME";
			 public const string FcClasse = "FC_CLASSE";
			 public const string FcCriterio = "FC_CRITERIO";
			 public const string FcCustodia = "FC_CUSTODIA";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string FcCodpapel = "FcCodpapel";
			 public const string FcPapel = "FcPapel";
			 public const string FcTipopapel = "FcTipopapel";
			 public const string FcDescricao = "FcDescricao";
			 public const string FcRentabilidade = "FcRentabilidade";
			 public const string FnDecimais = "FnDecimais";
			 public const string FcCod1088 = "FcCod1088";
			 public const string FcTipocurva = "FcTipocurva";
			 public const string FcBasecalculo = "FcBasecalculo";
			 public const string FcTipovolume = "FcTipovolume";
			 public const string FcClasse = "FcClasse";
			 public const string FcCriterio = "FcCriterio";
			 public const string FcCustodia = "FcCustodia";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OppapelMetadata))
			{
				if(OppapelMetadata.mapDelegates == null)
				{
					OppapelMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OppapelMetadata.meta == null)
				{
					OppapelMetadata.meta = new OppapelMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("FC_CODPAPEL", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_PAPEL", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TIPOPAPEL", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_DESCRICAO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_RENTABILIDADE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FN_DECIMAIS", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FC_COD1088", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TIPOCURVA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_BASECALCULO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TIPOVOLUME", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FC_CLASSE", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FC_CRITERIO", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FC_CUSTODIA", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "OPPAPEL";
				meta.Destination = "OPPAPEL";
				
				meta.spInsert = "proc_OPPAPELInsert";				
				meta.spUpdate = "proc_OPPAPELUpdate";		
				meta.spDelete = "proc_OPPAPELDelete";
				meta.spLoadAll = "proc_OPPAPELLoadAll";
				meta.spLoadByPrimaryKey = "proc_OPPAPELLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OppapelMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
