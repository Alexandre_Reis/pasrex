/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/12/2013 17:28:37
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;











































































































































































































namespace Financial.InterfacesDB
{

	[Serializable]
	abstract public class esGbclienteCollection : esEntityCollection
	{
		public esGbclienteCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "GbclienteCollection";
		}

		#region Query Logic
		protected void InitQuery(esGbclienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esGbclienteQuery);
		}
		#endregion
		
		virtual public Gbcliente DetachEntity(Gbcliente entity)
		{
			return base.DetachEntity(entity) as Gbcliente;
		}
		
		virtual public Gbcliente AttachEntity(Gbcliente entity)
		{
			return base.AttachEntity(entity) as Gbcliente;
		}
		
		virtual public void Combine(GbclienteCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Gbcliente this[int index]
		{
			get
			{
				return base[index] as Gbcliente;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Gbcliente);
		}
	}



	[Serializable]
	abstract public class esGbcliente : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esGbclienteQuery GetDynamicQuery()
		{
			return null;
		}

		public esGbcliente()
		{

		}

		public esGbcliente(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String fcMatricula)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(fcMatricula);
			else
				return LoadByPrimaryKeyStoredProcedure(fcMatricula);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.String fcMatricula)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esGbclienteQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.FcMatricula == fcMatricula);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String fcMatricula)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(fcMatricula);
			else
				return LoadByPrimaryKeyStoredProcedure(fcMatricula);
		}

		private bool LoadByPrimaryKeyDynamic(System.String fcMatricula)
		{
			esGbclienteQuery query = this.GetDynamicQuery();
			query.Where(query.FcMatricula == fcMatricula);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String fcMatricula)
		{
			esParameters parms = new esParameters();
			parms.Add("FC_MATRICULA",fcMatricula);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "FcMatricula": this.str.FcMatricula = (string)value; break;							
						case "FcCodbmf": this.str.FcCodbmf = (string)value; break;							
						case "FcCodbolsa": this.str.FcCodbolsa = (string)value; break;							
						case "FcCodbolsa2": this.str.FcCodbolsa2 = (string)value; break;							
						case "FcNome": this.str.FcNome = (string)value; break;							
						case "FcAbreviado": this.str.FcAbreviado = (string)value; break;							
						case "FcCgccpf": this.str.FcCgccpf = (string)value; break;							
						case "FcNasc": this.str.FcNasc = (string)value; break;							
						case "FcLiquidante": this.str.FcLiquidante = (string)value; break;							
						case "FcTipocliente": this.str.FcTipocliente = (string)value; break;							
						case "FcTipotributo": this.str.FcTipotributo = (string)value; break;							
						case "FcId": this.str.FcId = (string)value; break;							
						case "FcExpedidor": this.str.FcExpedidor = (string)value; break;							
						case "FcCartorio": this.str.FcCartorio = (string)value; break;							
						case "FcTrabalho": this.str.FcTrabalho = (string)value; break;							
						case "FcProfissao": this.str.FcProfissao = (string)value; break;							
						case "FcPai": this.str.FcPai = (string)value; break;							
						case "FcMae": this.str.FcMae = (string)value; break;							
						case "FcNacional": this.str.FcNacional = (string)value; break;							
						case "FcEstcivil": this.str.FcEstcivil = (string)value; break;							
						case "FcConjuge": this.str.FcConjuge = (string)value; break;							
						case "FcDestino": this.str.FcDestino = (string)value; break;							
						case "FcEmail": this.str.FcEmail = (string)value; break;							
						case "FcTelcom": this.str.FcTelcom = (string)value; break;							
						case "FcRamal": this.str.FcRamal = (string)value; break;							
						case "FcFax": this.str.FcFax = (string)value; break;							
						case "FcEnderes": this.str.FcEnderes = (string)value; break;							
						case "FcEndecom": this.str.FcEndecom = (string)value; break;							
						case "FcBairrocom": this.str.FcBairrocom = (string)value; break;							
						case "FcCidadecom": this.str.FcCidadecom = (string)value; break;							
						case "FcUfcom": this.str.FcUfcom = (string)value; break;							
						case "FcCepcom": this.str.FcCepcom = (string)value; break;							
						case "FcBairrores": this.str.FcBairrores = (string)value; break;							
						case "FcCidaderes": this.str.FcCidaderes = (string)value; break;							
						case "FcCepres": this.str.FcCepres = (string)value; break;							
						case "FcUfres": this.str.FcUfres = (string)value; break;							
						case "FcTelres": this.str.FcTelres = (string)value; break;							
						case "FcNumbanco": this.str.FcNumbanco = (string)value; break;							
						case "FcBanco": this.str.FcBanco = (string)value; break;							
						case "FcNumagen": this.str.FcNumagen = (string)value; break;							
						case "FcAgencia": this.str.FcAgencia = (string)value; break;							
						case "FcPraca": this.str.FcPraca = (string)value; break;							
						case "FcConta": this.str.FcConta = (string)value; break;							
						case "FcCetip": this.str.FcCetip = (string)value; break;							
						case "FcSelic": this.str.FcSelic = (string)value; break;							
						case "FcAnexo4": this.str.FcAnexo4 = (string)value; break;							
						case "FcNivelRemOpen": this.str.FcNivelRemOpen = (string)value; break;							
						case "FcIsentoIof": this.str.FcIsentoIof = (string)value; break;							
						case "FcIsentoIr": this.str.FcIsentoIr = (string)value; break;							
						case "FcSocioBmf": this.str.FcSocioBmf = (string)value; break;							
						case "FcMaebmf": this.str.FcMaebmf = (string)value; break;							
						case "FcFatmaebmf": this.str.FcFatmaebmf = (string)value; break;							
						case "FcTipoliqbmf": this.str.FcTipoliqbmf = (string)value; break;							
						case "FcLiqmaebmf": this.str.FcLiqmaebmf = (string)value; break;							
						case "FcContatobmf": this.str.FcContatobmf = (string)value; break;							
						case "FcAssessorbmf": this.str.FcAssessorbmf = (string)value; break;							
						case "FdUltmovBmf": this.str.FdUltmovBmf = (string)value; break;							
						case "FcMaebolsa": this.str.FcMaebolsa = (string)value; break;							
						case "FcTipoliqbolsa": this.str.FcTipoliqbolsa = (string)value; break;							
						case "FcFatmaebolsa": this.str.FcFatmaebolsa = (string)value; break;							
						case "FcLiqmaebolsa": this.str.FcLiqmaebolsa = (string)value; break;							
						case "FcContatobolsa": this.str.FcContatobolsa = (string)value; break;							
						case "FcAssessorbolsa": this.str.FcAssessorbolsa = (string)value; break;							
						case "FcCustpropria": this.str.FcCustpropria = (string)value; break;							
						case "FcTaxacust": this.str.FcTaxacust = (string)value; break;							
						case "FnPercust": this.str.FnPercust = (string)value; break;							
						case "FdUltmov": this.str.FdUltmov = (string)value; break;							
						case "FcCliinst": this.str.FcCliinst = (string)value; break;							
						case "FcUsuinst": this.str.FcUsuinst = (string)value; break;							
						case "FcFormaliq": this.str.FcFormaliq = (string)value; break;							
						case "FcNomecd1": this.str.FcNomecd1 = (string)value; break;							
						case "FcNomecd2": this.str.FcNomecd2 = (string)value; break;							
						case "FcCpfcd1": this.str.FcCpfcd1 = (string)value; break;							
						case "FcCpfcd2": this.str.FcCpfcd2 = (string)value; break;							
						case "FcRgcd1": this.str.FcRgcd1 = (string)value; break;							
						case "FcRgcd2": this.str.FcRgcd2 = (string)value; break;							
						case "FcNomeao1": this.str.FcNomeao1 = (string)value; break;							
						case "FcNomeao2": this.str.FcNomeao2 = (string)value; break;							
						case "FcCpfao1": this.str.FcCpfao1 = (string)value; break;							
						case "FcCpfao2": this.str.FcCpfao2 = (string)value; break;							
						case "FcRgao1": this.str.FcRgao1 = (string)value; break;							
						case "FcRgao2": this.str.FcRgao2 = (string)value; break;							
						case "FcNomeui1": this.str.FcNomeui1 = (string)value; break;							
						case "FcNomeui2": this.str.FcNomeui2 = (string)value; break;							
						case "FcCodui1": this.str.FcCodui1 = (string)value; break;							
						case "FcCodui2": this.str.FcCodui2 = (string)value; break;							
						case "FcAdcvm": this.str.FcAdcvm = (string)value; break;							
						case "FcCodcvm": this.str.FcCodcvm = (string)value; break;							
						case "FcTitular": this.str.FcTitular = (string)value; break;							
						case "FcCodadm": this.str.FcCodadm = (string)value; break;							
						case "FcNomeadm": this.str.FcNomeadm = (string)value; break;							
						case "FcCodadmusu": this.str.FcCodadmusu = (string)value; break;							
						case "FcNomeadmusu": this.str.FcNomeadmusu = (string)value; break;							
						case "FcCodusu": this.str.FcCodusu = (string)value; break;							
						case "FcCtbcli": this.str.FcCtbcli = (string)value; break;							
						case "FlCtbpro": this.str.FlCtbpro = (string)value; break;							
						case "FcTransmissor": this.str.FcTransmissor = (string)value; break;							
						case "FcTiporddef": this.str.FcTiporddef = (string)value; break;							
						case "FdDatacad": this.str.FdDatacad = (string)value; break;							
						case "FcIrfdt": this.str.FcIrfdt = (string)value; break;							
						case "FcCodgrupo": this.str.FcCodgrupo = (string)value; break;							
						case "FcLimitemae": this.str.FcLimitemae = (string)value; break;							
						case "FlConsolidaSelic": this.str.FlConsolidaSelic = (string)value; break;							
						case "FlConsolidaCetip": this.str.FlConsolidaCetip = (string)value; break;							
						case "FlConsolidaAdm": this.str.FlConsolidaAdm = (string)value; break;							
						case "FcCodconsolidafin": this.str.FcCodconsolidafin = (string)value; break;							
						case "FcFaixaselic": this.str.FcFaixaselic = (string)value; break;							
						case "FcFatura": this.str.FcFatura = (string)value; break;							
						case "FcContareserva": this.str.FcContareserva = (string)value; break;							
						case "FcBrokercetip": this.str.FcBrokercetip = (string)value; break;							
						case "FcBrokerselic": this.str.FcBrokerselic = (string)value; break;							
						case "FcCodgerente": this.str.FcCodgerente = (string)value; break;							
						case "FnPercentual": this.str.FnPercentual = (string)value; break;							
						case "FnLimite": this.str.FnLimite = (string)value; break;							
						case "FlEspecial": this.str.FlEspecial = (string)value; break;							
						case "FcMinemoemi": this.str.FcMinemoemi = (string)value; break;							
						case "FcCodflutuante": this.str.FcCodflutuante = (string)value; break;							
						case "FlEmissor": this.str.FlEmissor = (string)value; break;							
						case "FlResidente": this.str.FlResidente = (string)value; break;							
						case "FlIsentotxcustodia": this.str.FlIsentotxcustodia = (string)value; break;							
						case "FcPerfil": this.str.FcPerfil = (string)value; break;							
						case "FlExtrato": this.str.FlExtrato = (string)value; break;							
						case "FlEmiliquida": this.str.FlEmiliquida = (string)value; break;							
						case "FdEmiliqdata": this.str.FdEmiliqdata = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "FcNasc":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FcNasc = (System.DateTime?)value;
							break;
						
						case "FdUltmovBmf":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdUltmovBmf = (System.DateTime?)value;
							break;
						
						case "FnPercust":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FnPercust = (System.Int32?)value;
							break;
						
						case "FdUltmov":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdUltmov = (System.DateTime?)value;
							break;
						
						case "FlCtbpro":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FlCtbpro = (System.Int32?)value;
							break;
						
						case "FdDatacad":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatacad = (System.DateTime?)value;
							break;
						
						case "FlConsolidaSelic":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FlConsolidaSelic = (System.Int32?)value;
							break;
						
						case "FlConsolidaCetip":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FlConsolidaCetip = (System.Int32?)value;
							break;
						
						case "FlConsolidaAdm":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FlConsolidaAdm = (System.Int32?)value;
							break;
						
						case "FnPercentual":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPercentual = (System.Double?)value;
							break;
						
						case "FnLimite":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnLimite = (System.Double?)value;
							break;
						
						case "FlEspecial":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FlEspecial = (System.Int32?)value;
							break;
						
						case "FlEmissor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FlEmissor = (System.Int32?)value;
							break;
						
						case "FlResidente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FlResidente = (System.Int32?)value;
							break;
						
						case "FlIsentotxcustodia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FlIsentotxcustodia = (System.Int32?)value;
							break;
						
						case "FlExtrato":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FlExtrato = (System.Int32?)value;
							break;
						
						case "FlEmiliquida":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FlEmiliquida = (System.Int32?)value;
							break;
						
						case "FdEmiliqdata":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdEmiliqdata = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_MATRICULA
		/// </summary>
		virtual public System.String FcMatricula
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcMatricula);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcMatricula, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CODBMF
		/// </summary>
		virtual public System.String FcCodbmf
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCodbmf);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCodbmf, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CODBOLSA
		/// </summary>
		virtual public System.String FcCodbolsa
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCodbolsa);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCodbolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CODBOLSA2
		/// </summary>
		virtual public System.String FcCodbolsa2
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCodbolsa2);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCodbolsa2, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_NOME
		/// </summary>
		virtual public System.String FcNome
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcNome);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcNome, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_ABREVIADO
		/// </summary>
		virtual public System.String FcAbreviado
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcAbreviado);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcAbreviado, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CGCCPF
		/// </summary>
		virtual public System.String FcCgccpf
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCgccpf);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCgccpf, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_NASC
		/// </summary>
		virtual public System.DateTime? FcNasc
		{
			get
			{
				return base.GetSystemDateTime(GbclienteMetadata.ColumnNames.FcNasc);
			}
			
			set
			{
				base.SetSystemDateTime(GbclienteMetadata.ColumnNames.FcNasc, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_LIQUIDANTE
		/// </summary>
		virtual public System.String FcLiquidante
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcLiquidante);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcLiquidante, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_TIPOCLIENTE
		/// </summary>
		virtual public System.String FcTipocliente
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcTipocliente);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcTipocliente, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_TIPOTRIBUTO
		/// </summary>
		virtual public System.String FcTipotributo
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcTipotributo);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcTipotributo, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_ID
		/// </summary>
		virtual public System.String FcId
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcId);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcId, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_EXPEDIDOR
		/// </summary>
		virtual public System.String FcExpedidor
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcExpedidor);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcExpedidor, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CARTORIO
		/// </summary>
		virtual public System.String FcCartorio
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCartorio);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCartorio, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_TRABALHO
		/// </summary>
		virtual public System.String FcTrabalho
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcTrabalho);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcTrabalho, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_PROFISSAO
		/// </summary>
		virtual public System.String FcProfissao
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcProfissao);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcProfissao, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_PAI
		/// </summary>
		virtual public System.String FcPai
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcPai);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcPai, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_MAE
		/// </summary>
		virtual public System.String FcMae
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcMae);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcMae, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_NACIONAL
		/// </summary>
		virtual public System.String FcNacional
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcNacional);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcNacional, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_ESTCIVIL
		/// </summary>
		virtual public System.String FcEstcivil
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcEstcivil);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcEstcivil, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CONJUGE
		/// </summary>
		virtual public System.String FcConjuge
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcConjuge);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcConjuge, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_DESTINO
		/// </summary>
		virtual public System.String FcDestino
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcDestino);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcDestino, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_EMAIL
		/// </summary>
		virtual public System.String FcEmail
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcEmail);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcEmail, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_TELCOM
		/// </summary>
		virtual public System.String FcTelcom
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcTelcom);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcTelcom, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_RAMAL
		/// </summary>
		virtual public System.String FcRamal
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcRamal);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcRamal, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_FAX
		/// </summary>
		virtual public System.String FcFax
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcFax);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcFax, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_ENDERES
		/// </summary>
		virtual public System.String FcEnderes
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcEnderes);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcEnderes, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_ENDECOM
		/// </summary>
		virtual public System.String FcEndecom
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcEndecom);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcEndecom, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_BAIRROCOM
		/// </summary>
		virtual public System.String FcBairrocom
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcBairrocom);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcBairrocom, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CIDADECOM
		/// </summary>
		virtual public System.String FcCidadecom
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCidadecom);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCidadecom, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_UFCOM
		/// </summary>
		virtual public System.String FcUfcom
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcUfcom);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcUfcom, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CEPCOM
		/// </summary>
		virtual public System.String FcCepcom
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCepcom);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCepcom, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_BAIRRORES
		/// </summary>
		virtual public System.String FcBairrores
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcBairrores);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcBairrores, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CIDADERES
		/// </summary>
		virtual public System.String FcCidaderes
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCidaderes);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCidaderes, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CEPRES
		/// </summary>
		virtual public System.String FcCepres
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCepres);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCepres, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_UFRES
		/// </summary>
		virtual public System.String FcUfres
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcUfres);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcUfres, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_TELRES
		/// </summary>
		virtual public System.String FcTelres
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcTelres);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcTelres, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_NUMBANCO
		/// </summary>
		virtual public System.String FcNumbanco
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcNumbanco);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcNumbanco, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_BANCO
		/// </summary>
		virtual public System.String FcBanco
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcBanco);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcBanco, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_NUMAGEN
		/// </summary>
		virtual public System.String FcNumagen
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcNumagen);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcNumagen, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_AGENCIA
		/// </summary>
		virtual public System.String FcAgencia
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcAgencia);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcAgencia, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_PRACA
		/// </summary>
		virtual public System.String FcPraca
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcPraca);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcPraca, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CONTA
		/// </summary>
		virtual public System.String FcConta
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcConta);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcConta, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CETIP
		/// </summary>
		virtual public System.String FcCetip
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCetip);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCetip, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_SELIC
		/// </summary>
		virtual public System.String FcSelic
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcSelic);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcSelic, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_ANEXO4
		/// </summary>
		virtual public System.String FcAnexo4
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcAnexo4);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcAnexo4, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_NIVEL_REM_OPEN
		/// </summary>
		virtual public System.String FcNivelRemOpen
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcNivelRemOpen);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcNivelRemOpen, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_ISENTO_IOF
		/// </summary>
		virtual public System.String FcIsentoIof
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcIsentoIof);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcIsentoIof, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_ISENTO_IR
		/// </summary>
		virtual public System.String FcIsentoIr
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcIsentoIr);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcIsentoIr, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_SOCIO_BMF
		/// </summary>
		virtual public System.String FcSocioBmf
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcSocioBmf);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcSocioBmf, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_MAEBMF
		/// </summary>
		virtual public System.String FcMaebmf
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcMaebmf);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcMaebmf, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_FATMAEBMF
		/// </summary>
		virtual public System.String FcFatmaebmf
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcFatmaebmf);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcFatmaebmf, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_TIPOLIQBMF
		/// </summary>
		virtual public System.String FcTipoliqbmf
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcTipoliqbmf);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcTipoliqbmf, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_LIQMAEBMF
		/// </summary>
		virtual public System.String FcLiqmaebmf
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcLiqmaebmf);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcLiqmaebmf, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CONTATOBMF
		/// </summary>
		virtual public System.String FcContatobmf
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcContatobmf);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcContatobmf, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_ASSESSORBMF
		/// </summary>
		virtual public System.String FcAssessorbmf
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcAssessorbmf);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcAssessorbmf, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FD_ULTMOV_BMF
		/// </summary>
		virtual public System.DateTime? FdUltmovBmf
		{
			get
			{
				return base.GetSystemDateTime(GbclienteMetadata.ColumnNames.FdUltmovBmf);
			}
			
			set
			{
				base.SetSystemDateTime(GbclienteMetadata.ColumnNames.FdUltmovBmf, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_MAEBOLSA
		/// </summary>
		virtual public System.String FcMaebolsa
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcMaebolsa);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcMaebolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_TIPOLIQBOLSA
		/// </summary>
		virtual public System.String FcTipoliqbolsa
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcTipoliqbolsa);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcTipoliqbolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_FATMAEBOLSA
		/// </summary>
		virtual public System.String FcFatmaebolsa
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcFatmaebolsa);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcFatmaebolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_LIQMAEBOLSA
		/// </summary>
		virtual public System.String FcLiqmaebolsa
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcLiqmaebolsa);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcLiqmaebolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CONTATOBOLSA
		/// </summary>
		virtual public System.String FcContatobolsa
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcContatobolsa);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcContatobolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_ASSESSORBOLSA
		/// </summary>
		virtual public System.String FcAssessorbolsa
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcAssessorbolsa);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcAssessorbolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CUSTPROPRIA
		/// </summary>
		virtual public System.String FcCustpropria
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCustpropria);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCustpropria, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_TAXACUST
		/// </summary>
		virtual public System.String FcTaxacust
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcTaxacust);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcTaxacust, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FN_PERCUST
		/// </summary>
		virtual public System.Int32? FnPercust
		{
			get
			{
				return base.GetSystemInt32(GbclienteMetadata.ColumnNames.FnPercust);
			}
			
			set
			{
				base.SetSystemInt32(GbclienteMetadata.ColumnNames.FnPercust, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FD_ULTMOV
		/// </summary>
		virtual public System.DateTime? FdUltmov
		{
			get
			{
				return base.GetSystemDateTime(GbclienteMetadata.ColumnNames.FdUltmov);
			}
			
			set
			{
				base.SetSystemDateTime(GbclienteMetadata.ColumnNames.FdUltmov, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CLIINST
		/// </summary>
		virtual public System.String FcCliinst
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCliinst);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCliinst, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_USUINST
		/// </summary>
		virtual public System.String FcUsuinst
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcUsuinst);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcUsuinst, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_FORMALIQ
		/// </summary>
		virtual public System.String FcFormaliq
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcFormaliq);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcFormaliq, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_NOMECD1
		/// </summary>
		virtual public System.String FcNomecd1
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcNomecd1);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcNomecd1, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_NOMECD2
		/// </summary>
		virtual public System.String FcNomecd2
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcNomecd2);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcNomecd2, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CPFCD1
		/// </summary>
		virtual public System.String FcCpfcd1
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCpfcd1);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCpfcd1, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CPFCD2
		/// </summary>
		virtual public System.String FcCpfcd2
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCpfcd2);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCpfcd2, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_RGCD1
		/// </summary>
		virtual public System.String FcRgcd1
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcRgcd1);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcRgcd1, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_RGCD2
		/// </summary>
		virtual public System.String FcRgcd2
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcRgcd2);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcRgcd2, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_NOMEAO1
		/// </summary>
		virtual public System.String FcNomeao1
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcNomeao1);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcNomeao1, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_NOMEAO2
		/// </summary>
		virtual public System.String FcNomeao2
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcNomeao2);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcNomeao2, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CPFAO1
		/// </summary>
		virtual public System.String FcCpfao1
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCpfao1);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCpfao1, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CPFAO2
		/// </summary>
		virtual public System.String FcCpfao2
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCpfao2);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCpfao2, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_RGAO1
		/// </summary>
		virtual public System.String FcRgao1
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcRgao1);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcRgao1, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_RGAO2
		/// </summary>
		virtual public System.String FcRgao2
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcRgao2);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcRgao2, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_NOMEUI1
		/// </summary>
		virtual public System.String FcNomeui1
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcNomeui1);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcNomeui1, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_NOMEUI2
		/// </summary>
		virtual public System.String FcNomeui2
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcNomeui2);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcNomeui2, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CODUI1
		/// </summary>
		virtual public System.String FcCodui1
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCodui1);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCodui1, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CODUI2
		/// </summary>
		virtual public System.String FcCodui2
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCodui2);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCodui2, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_ADCVM
		/// </summary>
		virtual public System.String FcAdcvm
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcAdcvm);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcAdcvm, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CODCVM
		/// </summary>
		virtual public System.String FcCodcvm
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCodcvm);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCodcvm, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_TITULAR
		/// </summary>
		virtual public System.String FcTitular
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcTitular);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcTitular, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CODADM
		/// </summary>
		virtual public System.String FcCodadm
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCodadm);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCodadm, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_NOMEADM
		/// </summary>
		virtual public System.String FcNomeadm
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcNomeadm);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcNomeadm, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CODADMUSU
		/// </summary>
		virtual public System.String FcCodadmusu
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCodadmusu);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCodadmusu, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_NOMEADMUSU
		/// </summary>
		virtual public System.String FcNomeadmusu
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcNomeadmusu);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcNomeadmusu, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CODUSU
		/// </summary>
		virtual public System.String FcCodusu
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCodusu);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCodusu, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CTBCLI
		/// </summary>
		virtual public System.String FcCtbcli
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCtbcli);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCtbcli, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FL_CTBPRO
		/// </summary>
		virtual public System.Int32? FlCtbpro
		{
			get
			{
				return base.GetSystemInt32(GbclienteMetadata.ColumnNames.FlCtbpro);
			}
			
			set
			{
				base.SetSystemInt32(GbclienteMetadata.ColumnNames.FlCtbpro, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_TRANSMISSOR
		/// </summary>
		virtual public System.String FcTransmissor
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcTransmissor);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcTransmissor, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_TIPORDDEF
		/// </summary>
		virtual public System.String FcTiporddef
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcTiporddef);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcTiporddef, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FD_DATACAD
		/// </summary>
		virtual public System.DateTime? FdDatacad
		{
			get
			{
				return base.GetSystemDateTime(GbclienteMetadata.ColumnNames.FdDatacad);
			}
			
			set
			{
				base.SetSystemDateTime(GbclienteMetadata.ColumnNames.FdDatacad, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_IRFDT
		/// </summary>
		virtual public System.String FcIrfdt
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcIrfdt);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcIrfdt, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CODGRUPO
		/// </summary>
		virtual public System.String FcCodgrupo
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCodgrupo);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCodgrupo, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_LIMITEMAE
		/// </summary>
		virtual public System.String FcLimitemae
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcLimitemae);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcLimitemae, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FL_CONSOLIDA_SELIC
		/// </summary>
		virtual public System.Int32? FlConsolidaSelic
		{
			get
			{
				return base.GetSystemInt32(GbclienteMetadata.ColumnNames.FlConsolidaSelic);
			}
			
			set
			{
				base.SetSystemInt32(GbclienteMetadata.ColumnNames.FlConsolidaSelic, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FL_CONSOLIDA_CETIP
		/// </summary>
		virtual public System.Int32? FlConsolidaCetip
		{
			get
			{
				return base.GetSystemInt32(GbclienteMetadata.ColumnNames.FlConsolidaCetip);
			}
			
			set
			{
				base.SetSystemInt32(GbclienteMetadata.ColumnNames.FlConsolidaCetip, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FL_CONSOLIDA_ADM
		/// </summary>
		virtual public System.Int32? FlConsolidaAdm
		{
			get
			{
				return base.GetSystemInt32(GbclienteMetadata.ColumnNames.FlConsolidaAdm);
			}
			
			set
			{
				base.SetSystemInt32(GbclienteMetadata.ColumnNames.FlConsolidaAdm, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CODCONSOLIDAFIN
		/// </summary>
		virtual public System.String FcCodconsolidafin
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCodconsolidafin);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCodconsolidafin, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_FAIXASELIC
		/// </summary>
		virtual public System.String FcFaixaselic
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcFaixaselic);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcFaixaselic, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_FATURA
		/// </summary>
		virtual public System.String FcFatura
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcFatura);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcFatura, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CONTARESERVA
		/// </summary>
		virtual public System.String FcContareserva
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcContareserva);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcContareserva, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_BROKERCETIP
		/// </summary>
		virtual public System.String FcBrokercetip
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcBrokercetip);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcBrokercetip, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_BROKERSELIC
		/// </summary>
		virtual public System.String FcBrokerselic
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcBrokerselic);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcBrokerselic, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CODGERENTE
		/// </summary>
		virtual public System.String FcCodgerente
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCodgerente);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCodgerente, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FN_PERCENTUAL
		/// </summary>
		virtual public System.Double? FnPercentual
		{
			get
			{
				return base.GetSystemDouble(GbclienteMetadata.ColumnNames.FnPercentual);
			}
			
			set
			{
				base.SetSystemDouble(GbclienteMetadata.ColumnNames.FnPercentual, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FN_LIMITE
		/// </summary>
		virtual public System.Double? FnLimite
		{
			get
			{
				return base.GetSystemDouble(GbclienteMetadata.ColumnNames.FnLimite);
			}
			
			set
			{
				base.SetSystemDouble(GbclienteMetadata.ColumnNames.FnLimite, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FL_ESPECIAL
		/// </summary>
		virtual public System.Int32? FlEspecial
		{
			get
			{
				return base.GetSystemInt32(GbclienteMetadata.ColumnNames.FlEspecial);
			}
			
			set
			{
				base.SetSystemInt32(GbclienteMetadata.ColumnNames.FlEspecial, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_MINEMOEMI
		/// </summary>
		virtual public System.String FcMinemoemi
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcMinemoemi);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcMinemoemi, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_CODFLUTUANTE
		/// </summary>
		virtual public System.String FcCodflutuante
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcCodflutuante);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcCodflutuante, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FL_EMISSOR
		/// </summary>
		virtual public System.Int32? FlEmissor
		{
			get
			{
				return base.GetSystemInt32(GbclienteMetadata.ColumnNames.FlEmissor);
			}
			
			set
			{
				base.SetSystemInt32(GbclienteMetadata.ColumnNames.FlEmissor, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FL_RESIDENTE
		/// </summary>
		virtual public System.Int32? FlResidente
		{
			get
			{
				return base.GetSystemInt32(GbclienteMetadata.ColumnNames.FlResidente);
			}
			
			set
			{
				base.SetSystemInt32(GbclienteMetadata.ColumnNames.FlResidente, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FL_ISENTOTXCUSTODIA
		/// </summary>
		virtual public System.Int32? FlIsentotxcustodia
		{
			get
			{
				return base.GetSystemInt32(GbclienteMetadata.ColumnNames.FlIsentotxcustodia);
			}
			
			set
			{
				base.SetSystemInt32(GbclienteMetadata.ColumnNames.FlIsentotxcustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FC_PERFIL
		/// </summary>
		virtual public System.String FcPerfil
		{
			get
			{
				return base.GetSystemString(GbclienteMetadata.ColumnNames.FcPerfil);
			}
			
			set
			{
				base.SetSystemString(GbclienteMetadata.ColumnNames.FcPerfil, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FL_EXTRATO
		/// </summary>
		virtual public System.Int32? FlExtrato
		{
			get
			{
				return base.GetSystemInt32(GbclienteMetadata.ColumnNames.FlExtrato);
			}
			
			set
			{
				base.SetSystemInt32(GbclienteMetadata.ColumnNames.FlExtrato, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FL_EMILIQUIDA
		/// </summary>
		virtual public System.Int32? FlEmiliquida
		{
			get
			{
				return base.GetSystemInt32(GbclienteMetadata.ColumnNames.FlEmiliquida);
			}
			
			set
			{
				base.SetSystemInt32(GbclienteMetadata.ColumnNames.FlEmiliquida, value);
			}
		}
		
		/// <summary>
		/// Maps to GBCLIENTE.FD_EMILIQDATA
		/// </summary>
		virtual public System.DateTime? FdEmiliqdata
		{
			get
			{
				return base.GetSystemDateTime(GbclienteMetadata.ColumnNames.FdEmiliqdata);
			}
			
			set
			{
				base.SetSystemDateTime(GbclienteMetadata.ColumnNames.FdEmiliqdata, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esGbcliente entity)
			{
				this.entity = entity;
			}
			
	
			public System.String FcMatricula
			{
				get
				{
					System.String data = entity.FcMatricula;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcMatricula = null;
					else entity.FcMatricula = Convert.ToString(value);
				}
			}
				
			public System.String FcCodbmf
			{
				get
				{
					System.String data = entity.FcCodbmf;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodbmf = null;
					else entity.FcCodbmf = Convert.ToString(value);
				}
			}
				
			public System.String FcCodbolsa
			{
				get
				{
					System.String data = entity.FcCodbolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodbolsa = null;
					else entity.FcCodbolsa = Convert.ToString(value);
				}
			}
				
			public System.String FcCodbolsa2
			{
				get
				{
					System.String data = entity.FcCodbolsa2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodbolsa2 = null;
					else entity.FcCodbolsa2 = Convert.ToString(value);
				}
			}
				
			public System.String FcNome
			{
				get
				{
					System.String data = entity.FcNome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNome = null;
					else entity.FcNome = Convert.ToString(value);
				}
			}
				
			public System.String FcAbreviado
			{
				get
				{
					System.String data = entity.FcAbreviado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcAbreviado = null;
					else entity.FcAbreviado = Convert.ToString(value);
				}
			}
				
			public System.String FcCgccpf
			{
				get
				{
					System.String data = entity.FcCgccpf;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCgccpf = null;
					else entity.FcCgccpf = Convert.ToString(value);
				}
			}
				
			public System.String FcNasc
			{
				get
				{
					System.DateTime? data = entity.FcNasc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNasc = null;
					else entity.FcNasc = Convert.ToDateTime(value);
				}
			}
				
			public System.String FcLiquidante
			{
				get
				{
					System.String data = entity.FcLiquidante;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcLiquidante = null;
					else entity.FcLiquidante = Convert.ToString(value);
				}
			}
				
			public System.String FcTipocliente
			{
				get
				{
					System.String data = entity.FcTipocliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTipocliente = null;
					else entity.FcTipocliente = Convert.ToString(value);
				}
			}
				
			public System.String FcTipotributo
			{
				get
				{
					System.String data = entity.FcTipotributo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTipotributo = null;
					else entity.FcTipotributo = Convert.ToString(value);
				}
			}
				
			public System.String FcId
			{
				get
				{
					System.String data = entity.FcId;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcId = null;
					else entity.FcId = Convert.ToString(value);
				}
			}
				
			public System.String FcExpedidor
			{
				get
				{
					System.String data = entity.FcExpedidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcExpedidor = null;
					else entity.FcExpedidor = Convert.ToString(value);
				}
			}
				
			public System.String FcCartorio
			{
				get
				{
					System.String data = entity.FcCartorio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCartorio = null;
					else entity.FcCartorio = Convert.ToString(value);
				}
			}
				
			public System.String FcTrabalho
			{
				get
				{
					System.String data = entity.FcTrabalho;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTrabalho = null;
					else entity.FcTrabalho = Convert.ToString(value);
				}
			}
				
			public System.String FcProfissao
			{
				get
				{
					System.String data = entity.FcProfissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcProfissao = null;
					else entity.FcProfissao = Convert.ToString(value);
				}
			}
				
			public System.String FcPai
			{
				get
				{
					System.String data = entity.FcPai;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcPai = null;
					else entity.FcPai = Convert.ToString(value);
				}
			}
				
			public System.String FcMae
			{
				get
				{
					System.String data = entity.FcMae;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcMae = null;
					else entity.FcMae = Convert.ToString(value);
				}
			}
				
			public System.String FcNacional
			{
				get
				{
					System.String data = entity.FcNacional;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNacional = null;
					else entity.FcNacional = Convert.ToString(value);
				}
			}
				
			public System.String FcEstcivil
			{
				get
				{
					System.String data = entity.FcEstcivil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcEstcivil = null;
					else entity.FcEstcivil = Convert.ToString(value);
				}
			}
				
			public System.String FcConjuge
			{
				get
				{
					System.String data = entity.FcConjuge;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcConjuge = null;
					else entity.FcConjuge = Convert.ToString(value);
				}
			}
				
			public System.String FcDestino
			{
				get
				{
					System.String data = entity.FcDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcDestino = null;
					else entity.FcDestino = Convert.ToString(value);
				}
			}
				
			public System.String FcEmail
			{
				get
				{
					System.String data = entity.FcEmail;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcEmail = null;
					else entity.FcEmail = Convert.ToString(value);
				}
			}
				
			public System.String FcTelcom
			{
				get
				{
					System.String data = entity.FcTelcom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTelcom = null;
					else entity.FcTelcom = Convert.ToString(value);
				}
			}
				
			public System.String FcRamal
			{
				get
				{
					System.String data = entity.FcRamal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcRamal = null;
					else entity.FcRamal = Convert.ToString(value);
				}
			}
				
			public System.String FcFax
			{
				get
				{
					System.String data = entity.FcFax;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcFax = null;
					else entity.FcFax = Convert.ToString(value);
				}
			}
				
			public System.String FcEnderes
			{
				get
				{
					System.String data = entity.FcEnderes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcEnderes = null;
					else entity.FcEnderes = Convert.ToString(value);
				}
			}
				
			public System.String FcEndecom
			{
				get
				{
					System.String data = entity.FcEndecom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcEndecom = null;
					else entity.FcEndecom = Convert.ToString(value);
				}
			}
				
			public System.String FcBairrocom
			{
				get
				{
					System.String data = entity.FcBairrocom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcBairrocom = null;
					else entity.FcBairrocom = Convert.ToString(value);
				}
			}
				
			public System.String FcCidadecom
			{
				get
				{
					System.String data = entity.FcCidadecom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCidadecom = null;
					else entity.FcCidadecom = Convert.ToString(value);
				}
			}
				
			public System.String FcUfcom
			{
				get
				{
					System.String data = entity.FcUfcom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcUfcom = null;
					else entity.FcUfcom = Convert.ToString(value);
				}
			}
				
			public System.String FcCepcom
			{
				get
				{
					System.String data = entity.FcCepcom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCepcom = null;
					else entity.FcCepcom = Convert.ToString(value);
				}
			}
				
			public System.String FcBairrores
			{
				get
				{
					System.String data = entity.FcBairrores;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcBairrores = null;
					else entity.FcBairrores = Convert.ToString(value);
				}
			}
				
			public System.String FcCidaderes
			{
				get
				{
					System.String data = entity.FcCidaderes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCidaderes = null;
					else entity.FcCidaderes = Convert.ToString(value);
				}
			}
				
			public System.String FcCepres
			{
				get
				{
					System.String data = entity.FcCepres;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCepres = null;
					else entity.FcCepres = Convert.ToString(value);
				}
			}
				
			public System.String FcUfres
			{
				get
				{
					System.String data = entity.FcUfres;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcUfres = null;
					else entity.FcUfres = Convert.ToString(value);
				}
			}
				
			public System.String FcTelres
			{
				get
				{
					System.String data = entity.FcTelres;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTelres = null;
					else entity.FcTelres = Convert.ToString(value);
				}
			}
				
			public System.String FcNumbanco
			{
				get
				{
					System.String data = entity.FcNumbanco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNumbanco = null;
					else entity.FcNumbanco = Convert.ToString(value);
				}
			}
				
			public System.String FcBanco
			{
				get
				{
					System.String data = entity.FcBanco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcBanco = null;
					else entity.FcBanco = Convert.ToString(value);
				}
			}
				
			public System.String FcNumagen
			{
				get
				{
					System.String data = entity.FcNumagen;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNumagen = null;
					else entity.FcNumagen = Convert.ToString(value);
				}
			}
				
			public System.String FcAgencia
			{
				get
				{
					System.String data = entity.FcAgencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcAgencia = null;
					else entity.FcAgencia = Convert.ToString(value);
				}
			}
				
			public System.String FcPraca
			{
				get
				{
					System.String data = entity.FcPraca;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcPraca = null;
					else entity.FcPraca = Convert.ToString(value);
				}
			}
				
			public System.String FcConta
			{
				get
				{
					System.String data = entity.FcConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcConta = null;
					else entity.FcConta = Convert.ToString(value);
				}
			}
				
			public System.String FcCetip
			{
				get
				{
					System.String data = entity.FcCetip;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCetip = null;
					else entity.FcCetip = Convert.ToString(value);
				}
			}
				
			public System.String FcSelic
			{
				get
				{
					System.String data = entity.FcSelic;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcSelic = null;
					else entity.FcSelic = Convert.ToString(value);
				}
			}
				
			public System.String FcAnexo4
			{
				get
				{
					System.String data = entity.FcAnexo4;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcAnexo4 = null;
					else entity.FcAnexo4 = Convert.ToString(value);
				}
			}
				
			public System.String FcNivelRemOpen
			{
				get
				{
					System.String data = entity.FcNivelRemOpen;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNivelRemOpen = null;
					else entity.FcNivelRemOpen = Convert.ToString(value);
				}
			}
				
			public System.String FcIsentoIof
			{
				get
				{
					System.String data = entity.FcIsentoIof;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcIsentoIof = null;
					else entity.FcIsentoIof = Convert.ToString(value);
				}
			}
				
			public System.String FcIsentoIr
			{
				get
				{
					System.String data = entity.FcIsentoIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcIsentoIr = null;
					else entity.FcIsentoIr = Convert.ToString(value);
				}
			}
				
			public System.String FcSocioBmf
			{
				get
				{
					System.String data = entity.FcSocioBmf;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcSocioBmf = null;
					else entity.FcSocioBmf = Convert.ToString(value);
				}
			}
				
			public System.String FcMaebmf
			{
				get
				{
					System.String data = entity.FcMaebmf;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcMaebmf = null;
					else entity.FcMaebmf = Convert.ToString(value);
				}
			}
				
			public System.String FcFatmaebmf
			{
				get
				{
					System.String data = entity.FcFatmaebmf;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcFatmaebmf = null;
					else entity.FcFatmaebmf = Convert.ToString(value);
				}
			}
				
			public System.String FcTipoliqbmf
			{
				get
				{
					System.String data = entity.FcTipoliqbmf;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTipoliqbmf = null;
					else entity.FcTipoliqbmf = Convert.ToString(value);
				}
			}
				
			public System.String FcLiqmaebmf
			{
				get
				{
					System.String data = entity.FcLiqmaebmf;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcLiqmaebmf = null;
					else entity.FcLiqmaebmf = Convert.ToString(value);
				}
			}
				
			public System.String FcContatobmf
			{
				get
				{
					System.String data = entity.FcContatobmf;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcContatobmf = null;
					else entity.FcContatobmf = Convert.ToString(value);
				}
			}
				
			public System.String FcAssessorbmf
			{
				get
				{
					System.String data = entity.FcAssessorbmf;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcAssessorbmf = null;
					else entity.FcAssessorbmf = Convert.ToString(value);
				}
			}
				
			public System.String FdUltmovBmf
			{
				get
				{
					System.DateTime? data = entity.FdUltmovBmf;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdUltmovBmf = null;
					else entity.FdUltmovBmf = Convert.ToDateTime(value);
				}
			}
				
			public System.String FcMaebolsa
			{
				get
				{
					System.String data = entity.FcMaebolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcMaebolsa = null;
					else entity.FcMaebolsa = Convert.ToString(value);
				}
			}
				
			public System.String FcTipoliqbolsa
			{
				get
				{
					System.String data = entity.FcTipoliqbolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTipoliqbolsa = null;
					else entity.FcTipoliqbolsa = Convert.ToString(value);
				}
			}
				
			public System.String FcFatmaebolsa
			{
				get
				{
					System.String data = entity.FcFatmaebolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcFatmaebolsa = null;
					else entity.FcFatmaebolsa = Convert.ToString(value);
				}
			}
				
			public System.String FcLiqmaebolsa
			{
				get
				{
					System.String data = entity.FcLiqmaebolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcLiqmaebolsa = null;
					else entity.FcLiqmaebolsa = Convert.ToString(value);
				}
			}
				
			public System.String FcContatobolsa
			{
				get
				{
					System.String data = entity.FcContatobolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcContatobolsa = null;
					else entity.FcContatobolsa = Convert.ToString(value);
				}
			}
				
			public System.String FcAssessorbolsa
			{
				get
				{
					System.String data = entity.FcAssessorbolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcAssessorbolsa = null;
					else entity.FcAssessorbolsa = Convert.ToString(value);
				}
			}
				
			public System.String FcCustpropria
			{
				get
				{
					System.String data = entity.FcCustpropria;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCustpropria = null;
					else entity.FcCustpropria = Convert.ToString(value);
				}
			}
				
			public System.String FcTaxacust
			{
				get
				{
					System.String data = entity.FcTaxacust;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTaxacust = null;
					else entity.FcTaxacust = Convert.ToString(value);
				}
			}
				
			public System.String FnPercust
			{
				get
				{
					System.Int32? data = entity.FnPercust;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPercust = null;
					else entity.FnPercust = Convert.ToInt32(value);
				}
			}
				
			public System.String FdUltmov
			{
				get
				{
					System.DateTime? data = entity.FdUltmov;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdUltmov = null;
					else entity.FdUltmov = Convert.ToDateTime(value);
				}
			}
				
			public System.String FcCliinst
			{
				get
				{
					System.String data = entity.FcCliinst;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCliinst = null;
					else entity.FcCliinst = Convert.ToString(value);
				}
			}
				
			public System.String FcUsuinst
			{
				get
				{
					System.String data = entity.FcUsuinst;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcUsuinst = null;
					else entity.FcUsuinst = Convert.ToString(value);
				}
			}
				
			public System.String FcFormaliq
			{
				get
				{
					System.String data = entity.FcFormaliq;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcFormaliq = null;
					else entity.FcFormaliq = Convert.ToString(value);
				}
			}
				
			public System.String FcNomecd1
			{
				get
				{
					System.String data = entity.FcNomecd1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNomecd1 = null;
					else entity.FcNomecd1 = Convert.ToString(value);
				}
			}
				
			public System.String FcNomecd2
			{
				get
				{
					System.String data = entity.FcNomecd2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNomecd2 = null;
					else entity.FcNomecd2 = Convert.ToString(value);
				}
			}
				
			public System.String FcCpfcd1
			{
				get
				{
					System.String data = entity.FcCpfcd1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCpfcd1 = null;
					else entity.FcCpfcd1 = Convert.ToString(value);
				}
			}
				
			public System.String FcCpfcd2
			{
				get
				{
					System.String data = entity.FcCpfcd2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCpfcd2 = null;
					else entity.FcCpfcd2 = Convert.ToString(value);
				}
			}
				
			public System.String FcRgcd1
			{
				get
				{
					System.String data = entity.FcRgcd1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcRgcd1 = null;
					else entity.FcRgcd1 = Convert.ToString(value);
				}
			}
				
			public System.String FcRgcd2
			{
				get
				{
					System.String data = entity.FcRgcd2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcRgcd2 = null;
					else entity.FcRgcd2 = Convert.ToString(value);
				}
			}
				
			public System.String FcNomeao1
			{
				get
				{
					System.String data = entity.FcNomeao1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNomeao1 = null;
					else entity.FcNomeao1 = Convert.ToString(value);
				}
			}
				
			public System.String FcNomeao2
			{
				get
				{
					System.String data = entity.FcNomeao2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNomeao2 = null;
					else entity.FcNomeao2 = Convert.ToString(value);
				}
			}
				
			public System.String FcCpfao1
			{
				get
				{
					System.String data = entity.FcCpfao1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCpfao1 = null;
					else entity.FcCpfao1 = Convert.ToString(value);
				}
			}
				
			public System.String FcCpfao2
			{
				get
				{
					System.String data = entity.FcCpfao2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCpfao2 = null;
					else entity.FcCpfao2 = Convert.ToString(value);
				}
			}
				
			public System.String FcRgao1
			{
				get
				{
					System.String data = entity.FcRgao1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcRgao1 = null;
					else entity.FcRgao1 = Convert.ToString(value);
				}
			}
				
			public System.String FcRgao2
			{
				get
				{
					System.String data = entity.FcRgao2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcRgao2 = null;
					else entity.FcRgao2 = Convert.ToString(value);
				}
			}
				
			public System.String FcNomeui1
			{
				get
				{
					System.String data = entity.FcNomeui1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNomeui1 = null;
					else entity.FcNomeui1 = Convert.ToString(value);
				}
			}
				
			public System.String FcNomeui2
			{
				get
				{
					System.String data = entity.FcNomeui2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNomeui2 = null;
					else entity.FcNomeui2 = Convert.ToString(value);
				}
			}
				
			public System.String FcCodui1
			{
				get
				{
					System.String data = entity.FcCodui1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodui1 = null;
					else entity.FcCodui1 = Convert.ToString(value);
				}
			}
				
			public System.String FcCodui2
			{
				get
				{
					System.String data = entity.FcCodui2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodui2 = null;
					else entity.FcCodui2 = Convert.ToString(value);
				}
			}
				
			public System.String FcAdcvm
			{
				get
				{
					System.String data = entity.FcAdcvm;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcAdcvm = null;
					else entity.FcAdcvm = Convert.ToString(value);
				}
			}
				
			public System.String FcCodcvm
			{
				get
				{
					System.String data = entity.FcCodcvm;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodcvm = null;
					else entity.FcCodcvm = Convert.ToString(value);
				}
			}
				
			public System.String FcTitular
			{
				get
				{
					System.String data = entity.FcTitular;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTitular = null;
					else entity.FcTitular = Convert.ToString(value);
				}
			}
				
			public System.String FcCodadm
			{
				get
				{
					System.String data = entity.FcCodadm;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodadm = null;
					else entity.FcCodadm = Convert.ToString(value);
				}
			}
				
			public System.String FcNomeadm
			{
				get
				{
					System.String data = entity.FcNomeadm;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNomeadm = null;
					else entity.FcNomeadm = Convert.ToString(value);
				}
			}
				
			public System.String FcCodadmusu
			{
				get
				{
					System.String data = entity.FcCodadmusu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodadmusu = null;
					else entity.FcCodadmusu = Convert.ToString(value);
				}
			}
				
			public System.String FcNomeadmusu
			{
				get
				{
					System.String data = entity.FcNomeadmusu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNomeadmusu = null;
					else entity.FcNomeadmusu = Convert.ToString(value);
				}
			}
				
			public System.String FcCodusu
			{
				get
				{
					System.String data = entity.FcCodusu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodusu = null;
					else entity.FcCodusu = Convert.ToString(value);
				}
			}
				
			public System.String FcCtbcli
			{
				get
				{
					System.String data = entity.FcCtbcli;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCtbcli = null;
					else entity.FcCtbcli = Convert.ToString(value);
				}
			}
				
			public System.String FlCtbpro
			{
				get
				{
					System.Int32? data = entity.FlCtbpro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlCtbpro = null;
					else entity.FlCtbpro = Convert.ToInt32(value);
				}
			}
				
			public System.String FcTransmissor
			{
				get
				{
					System.String data = entity.FcTransmissor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTransmissor = null;
					else entity.FcTransmissor = Convert.ToString(value);
				}
			}
				
			public System.String FcTiporddef
			{
				get
				{
					System.String data = entity.FcTiporddef;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTiporddef = null;
					else entity.FcTiporddef = Convert.ToString(value);
				}
			}
				
			public System.String FdDatacad
			{
				get
				{
					System.DateTime? data = entity.FdDatacad;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatacad = null;
					else entity.FdDatacad = Convert.ToDateTime(value);
				}
			}
				
			public System.String FcIrfdt
			{
				get
				{
					System.String data = entity.FcIrfdt;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcIrfdt = null;
					else entity.FcIrfdt = Convert.ToString(value);
				}
			}
				
			public System.String FcCodgrupo
			{
				get
				{
					System.String data = entity.FcCodgrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodgrupo = null;
					else entity.FcCodgrupo = Convert.ToString(value);
				}
			}
				
			public System.String FcLimitemae
			{
				get
				{
					System.String data = entity.FcLimitemae;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcLimitemae = null;
					else entity.FcLimitemae = Convert.ToString(value);
				}
			}
				
			public System.String FlConsolidaSelic
			{
				get
				{
					System.Int32? data = entity.FlConsolidaSelic;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlConsolidaSelic = null;
					else entity.FlConsolidaSelic = Convert.ToInt32(value);
				}
			}
				
			public System.String FlConsolidaCetip
			{
				get
				{
					System.Int32? data = entity.FlConsolidaCetip;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlConsolidaCetip = null;
					else entity.FlConsolidaCetip = Convert.ToInt32(value);
				}
			}
				
			public System.String FlConsolidaAdm
			{
				get
				{
					System.Int32? data = entity.FlConsolidaAdm;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlConsolidaAdm = null;
					else entity.FlConsolidaAdm = Convert.ToInt32(value);
				}
			}
				
			public System.String FcCodconsolidafin
			{
				get
				{
					System.String data = entity.FcCodconsolidafin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodconsolidafin = null;
					else entity.FcCodconsolidafin = Convert.ToString(value);
				}
			}
				
			public System.String FcFaixaselic
			{
				get
				{
					System.String data = entity.FcFaixaselic;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcFaixaselic = null;
					else entity.FcFaixaselic = Convert.ToString(value);
				}
			}
				
			public System.String FcFatura
			{
				get
				{
					System.String data = entity.FcFatura;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcFatura = null;
					else entity.FcFatura = Convert.ToString(value);
				}
			}
				
			public System.String FcContareserva
			{
				get
				{
					System.String data = entity.FcContareserva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcContareserva = null;
					else entity.FcContareserva = Convert.ToString(value);
				}
			}
				
			public System.String FcBrokercetip
			{
				get
				{
					System.String data = entity.FcBrokercetip;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcBrokercetip = null;
					else entity.FcBrokercetip = Convert.ToString(value);
				}
			}
				
			public System.String FcBrokerselic
			{
				get
				{
					System.String data = entity.FcBrokerselic;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcBrokerselic = null;
					else entity.FcBrokerselic = Convert.ToString(value);
				}
			}
				
			public System.String FcCodgerente
			{
				get
				{
					System.String data = entity.FcCodgerente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodgerente = null;
					else entity.FcCodgerente = Convert.ToString(value);
				}
			}
				
			public System.String FnPercentual
			{
				get
				{
					System.Double? data = entity.FnPercentual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPercentual = null;
					else entity.FnPercentual = Convert.ToDouble(value);
				}
			}
				
			public System.String FnLimite
			{
				get
				{
					System.Double? data = entity.FnLimite;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnLimite = null;
					else entity.FnLimite = Convert.ToDouble(value);
				}
			}
				
			public System.String FlEspecial
			{
				get
				{
					System.Int32? data = entity.FlEspecial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlEspecial = null;
					else entity.FlEspecial = Convert.ToInt32(value);
				}
			}
				
			public System.String FcMinemoemi
			{
				get
				{
					System.String data = entity.FcMinemoemi;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcMinemoemi = null;
					else entity.FcMinemoemi = Convert.ToString(value);
				}
			}
				
			public System.String FcCodflutuante
			{
				get
				{
					System.String data = entity.FcCodflutuante;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodflutuante = null;
					else entity.FcCodflutuante = Convert.ToString(value);
				}
			}
				
			public System.String FlEmissor
			{
				get
				{
					System.Int32? data = entity.FlEmissor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlEmissor = null;
					else entity.FlEmissor = Convert.ToInt32(value);
				}
			}
				
			public System.String FlResidente
			{
				get
				{
					System.Int32? data = entity.FlResidente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlResidente = null;
					else entity.FlResidente = Convert.ToInt32(value);
				}
			}
				
			public System.String FlIsentotxcustodia
			{
				get
				{
					System.Int32? data = entity.FlIsentotxcustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlIsentotxcustodia = null;
					else entity.FlIsentotxcustodia = Convert.ToInt32(value);
				}
			}
				
			public System.String FcPerfil
			{
				get
				{
					System.String data = entity.FcPerfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcPerfil = null;
					else entity.FcPerfil = Convert.ToString(value);
				}
			}
				
			public System.String FlExtrato
			{
				get
				{
					System.Int32? data = entity.FlExtrato;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlExtrato = null;
					else entity.FlExtrato = Convert.ToInt32(value);
				}
			}
				
			public System.String FlEmiliquida
			{
				get
				{
					System.Int32? data = entity.FlEmiliquida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlEmiliquida = null;
					else entity.FlEmiliquida = Convert.ToInt32(value);
				}
			}
				
			public System.String FdEmiliqdata
			{
				get
				{
					System.DateTime? data = entity.FdEmiliqdata;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdEmiliqdata = null;
					else entity.FdEmiliqdata = Convert.ToDateTime(value);
				}
			}
			

			private esGbcliente entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esGbclienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esGbcliente can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Gbcliente : esGbcliente
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esGbclienteQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return GbclienteMetadata.Meta();
			}
		}	
		

		public esQueryItem FcMatricula
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcMatricula, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodbmf
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCodbmf, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodbolsa
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCodbolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodbolsa2
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCodbolsa2, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNome
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcNome, esSystemType.String);
			}
		} 
		
		public esQueryItem FcAbreviado
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcAbreviado, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCgccpf
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCgccpf, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNasc
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcNasc, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FcLiquidante
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcLiquidante, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTipocliente
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcTipocliente, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTipotributo
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcTipotributo, esSystemType.String);
			}
		} 
		
		public esQueryItem FcId
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcId, esSystemType.String);
			}
		} 
		
		public esQueryItem FcExpedidor
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcExpedidor, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCartorio
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCartorio, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTrabalho
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcTrabalho, esSystemType.String);
			}
		} 
		
		public esQueryItem FcProfissao
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcProfissao, esSystemType.String);
			}
		} 
		
		public esQueryItem FcPai
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcPai, esSystemType.String);
			}
		} 
		
		public esQueryItem FcMae
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcMae, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNacional
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcNacional, esSystemType.String);
			}
		} 
		
		public esQueryItem FcEstcivil
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcEstcivil, esSystemType.String);
			}
		} 
		
		public esQueryItem FcConjuge
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcConjuge, esSystemType.String);
			}
		} 
		
		public esQueryItem FcDestino
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcDestino, esSystemType.String);
			}
		} 
		
		public esQueryItem FcEmail
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcEmail, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTelcom
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcTelcom, esSystemType.String);
			}
		} 
		
		public esQueryItem FcRamal
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcRamal, esSystemType.String);
			}
		} 
		
		public esQueryItem FcFax
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcFax, esSystemType.String);
			}
		} 
		
		public esQueryItem FcEnderes
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcEnderes, esSystemType.String);
			}
		} 
		
		public esQueryItem FcEndecom
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcEndecom, esSystemType.String);
			}
		} 
		
		public esQueryItem FcBairrocom
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcBairrocom, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCidadecom
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCidadecom, esSystemType.String);
			}
		} 
		
		public esQueryItem FcUfcom
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcUfcom, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCepcom
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCepcom, esSystemType.String);
			}
		} 
		
		public esQueryItem FcBairrores
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcBairrores, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCidaderes
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCidaderes, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCepres
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCepres, esSystemType.String);
			}
		} 
		
		public esQueryItem FcUfres
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcUfres, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTelres
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcTelres, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNumbanco
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcNumbanco, esSystemType.String);
			}
		} 
		
		public esQueryItem FcBanco
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcBanco, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNumagen
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcNumagen, esSystemType.String);
			}
		} 
		
		public esQueryItem FcAgencia
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcAgencia, esSystemType.String);
			}
		} 
		
		public esQueryItem FcPraca
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcPraca, esSystemType.String);
			}
		} 
		
		public esQueryItem FcConta
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcConta, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCetip
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCetip, esSystemType.String);
			}
		} 
		
		public esQueryItem FcSelic
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcSelic, esSystemType.String);
			}
		} 
		
		public esQueryItem FcAnexo4
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcAnexo4, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNivelRemOpen
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcNivelRemOpen, esSystemType.String);
			}
		} 
		
		public esQueryItem FcIsentoIof
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcIsentoIof, esSystemType.String);
			}
		} 
		
		public esQueryItem FcIsentoIr
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcIsentoIr, esSystemType.String);
			}
		} 
		
		public esQueryItem FcSocioBmf
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcSocioBmf, esSystemType.String);
			}
		} 
		
		public esQueryItem FcMaebmf
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcMaebmf, esSystemType.String);
			}
		} 
		
		public esQueryItem FcFatmaebmf
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcFatmaebmf, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTipoliqbmf
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcTipoliqbmf, esSystemType.String);
			}
		} 
		
		public esQueryItem FcLiqmaebmf
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcLiqmaebmf, esSystemType.String);
			}
		} 
		
		public esQueryItem FcContatobmf
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcContatobmf, esSystemType.String);
			}
		} 
		
		public esQueryItem FcAssessorbmf
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcAssessorbmf, esSystemType.String);
			}
		} 
		
		public esQueryItem FdUltmovBmf
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FdUltmovBmf, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FcMaebolsa
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcMaebolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTipoliqbolsa
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcTipoliqbolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem FcFatmaebolsa
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcFatmaebolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem FcLiqmaebolsa
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcLiqmaebolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem FcContatobolsa
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcContatobolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem FcAssessorbolsa
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcAssessorbolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCustpropria
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCustpropria, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTaxacust
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcTaxacust, esSystemType.String);
			}
		} 
		
		public esQueryItem FnPercust
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FnPercust, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FdUltmov
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FdUltmov, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FcCliinst
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCliinst, esSystemType.String);
			}
		} 
		
		public esQueryItem FcUsuinst
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcUsuinst, esSystemType.String);
			}
		} 
		
		public esQueryItem FcFormaliq
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcFormaliq, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNomecd1
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcNomecd1, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNomecd2
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcNomecd2, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCpfcd1
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCpfcd1, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCpfcd2
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCpfcd2, esSystemType.String);
			}
		} 
		
		public esQueryItem FcRgcd1
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcRgcd1, esSystemType.String);
			}
		} 
		
		public esQueryItem FcRgcd2
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcRgcd2, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNomeao1
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcNomeao1, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNomeao2
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcNomeao2, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCpfao1
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCpfao1, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCpfao2
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCpfao2, esSystemType.String);
			}
		} 
		
		public esQueryItem FcRgao1
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcRgao1, esSystemType.String);
			}
		} 
		
		public esQueryItem FcRgao2
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcRgao2, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNomeui1
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcNomeui1, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNomeui2
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcNomeui2, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodui1
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCodui1, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodui2
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCodui2, esSystemType.String);
			}
		} 
		
		public esQueryItem FcAdcvm
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcAdcvm, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodcvm
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCodcvm, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTitular
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcTitular, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodadm
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCodadm, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNomeadm
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcNomeadm, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodadmusu
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCodadmusu, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNomeadmusu
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcNomeadmusu, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodusu
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCodusu, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCtbcli
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCtbcli, esSystemType.String);
			}
		} 
		
		public esQueryItem FlCtbpro
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FlCtbpro, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FcTransmissor
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcTransmissor, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTiporddef
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcTiporddef, esSystemType.String);
			}
		} 
		
		public esQueryItem FdDatacad
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FdDatacad, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FcIrfdt
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcIrfdt, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodgrupo
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCodgrupo, esSystemType.String);
			}
		} 
		
		public esQueryItem FcLimitemae
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcLimitemae, esSystemType.String);
			}
		} 
		
		public esQueryItem FlConsolidaSelic
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FlConsolidaSelic, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FlConsolidaCetip
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FlConsolidaCetip, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FlConsolidaAdm
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FlConsolidaAdm, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FcCodconsolidafin
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCodconsolidafin, esSystemType.String);
			}
		} 
		
		public esQueryItem FcFaixaselic
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcFaixaselic, esSystemType.String);
			}
		} 
		
		public esQueryItem FcFatura
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcFatura, esSystemType.String);
			}
		} 
		
		public esQueryItem FcContareserva
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcContareserva, esSystemType.String);
			}
		} 
		
		public esQueryItem FcBrokercetip
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcBrokercetip, esSystemType.String);
			}
		} 
		
		public esQueryItem FcBrokerselic
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcBrokerselic, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodgerente
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCodgerente, esSystemType.String);
			}
		} 
		
		public esQueryItem FnPercentual
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FnPercentual, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnLimite
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FnLimite, esSystemType.Double);
			}
		} 
		
		public esQueryItem FlEspecial
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FlEspecial, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FcMinemoemi
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcMinemoemi, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodflutuante
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcCodflutuante, esSystemType.String);
			}
		} 
		
		public esQueryItem FlEmissor
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FlEmissor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FlResidente
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FlResidente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FlIsentotxcustodia
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FlIsentotxcustodia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FcPerfil
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FcPerfil, esSystemType.String);
			}
		} 
		
		public esQueryItem FlExtrato
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FlExtrato, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FlEmiliquida
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FlEmiliquida, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FdEmiliqdata
		{
			get
			{
				return new esQueryItem(this, GbclienteMetadata.ColumnNames.FdEmiliqdata, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("GbclienteCollection")]
	public partial class GbclienteCollection : esGbclienteCollection, IEnumerable<Gbcliente>
	{
		public GbclienteCollection()
		{

		}
		
		public static implicit operator List<Gbcliente>(GbclienteCollection coll)
		{
			List<Gbcliente> list = new List<Gbcliente>();
			
			foreach (Gbcliente emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  GbclienteMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GbclienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Gbcliente(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Gbcliente();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public GbclienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GbclienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(GbclienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Gbcliente AddNew()
		{
			Gbcliente entity = base.AddNewEntity() as Gbcliente;
			
			return entity;
		}

		public Gbcliente FindByPrimaryKey(System.String fcMatricula)
		{
			return base.FindByPrimaryKey(fcMatricula) as Gbcliente;
		}


		#region IEnumerable<Gbcliente> Members

		IEnumerator<Gbcliente> IEnumerable<Gbcliente>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Gbcliente;
			}
		}

		#endregion
		
		private GbclienteQuery query;
	}


	/// <summary>
	/// Encapsulates the 'GBCLIENTE' table
	/// </summary>

	[Serializable]
	public partial class Gbcliente : esGbcliente
	{
		public Gbcliente()
		{

		}
	
		public Gbcliente(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GbclienteMetadata.Meta();
			}
		}
		
		
		
		override protected esGbclienteQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GbclienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public GbclienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GbclienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(GbclienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private GbclienteQuery query;
	}



	[Serializable]
	public partial class GbclienteQuery : esGbclienteQuery
	{
		public GbclienteQuery()
		{

		}		
		
		public GbclienteQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class GbclienteMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GbclienteMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcMatricula, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcMatricula;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCodbmf, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCodbmf;
			c.CharacterMaxLength = 7;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCodbolsa, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCodbolsa;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCodbolsa2, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCodbolsa2;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcNome, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcNome;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcAbreviado, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcAbreviado;
			c.CharacterMaxLength = 29;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCgccpf, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCgccpf;
			c.CharacterMaxLength = 18;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcNasc, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcNasc;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcLiquidante, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcLiquidante;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcTipocliente, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcTipocliente;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcTipotributo, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcTipotributo;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcId, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcId;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcExpedidor, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcExpedidor;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCartorio, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCartorio;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcTrabalho, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcTrabalho;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcProfissao, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcProfissao;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcPai, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcPai;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcMae, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcMae;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcNacional, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcNacional;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcEstcivil, 19, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcEstcivil;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcConjuge, 20, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcConjuge;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcDestino, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcDestino;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcEmail, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcEmail;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcTelcom, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcTelcom;
			c.CharacterMaxLength = 15;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcRamal, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcRamal;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcFax, 25, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcFax;
			c.CharacterMaxLength = 15;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcEnderes, 26, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcEnderes;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcEndecom, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcEndecom;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcBairrocom, 28, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcBairrocom;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCidadecom, 29, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCidadecom;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcUfcom, 30, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcUfcom;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCepcom, 31, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCepcom;
			c.CharacterMaxLength = 9;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcBairrores, 32, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcBairrores;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCidaderes, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCidaderes;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCepres, 34, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCepres;
			c.CharacterMaxLength = 9;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcUfres, 35, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcUfres;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcTelres, 36, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcTelres;
			c.CharacterMaxLength = 15;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcNumbanco, 37, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcNumbanco;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcBanco, 38, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcBanco;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcNumagen, 39, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcNumagen;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcAgencia, 40, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcAgencia;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcPraca, 41, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcPraca;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcConta, 42, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcConta;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCetip, 43, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCetip;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcSelic, 44, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcSelic;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcAnexo4, 45, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcAnexo4;
			c.CharacterMaxLength = 19;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcNivelRemOpen, 46, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcNivelRemOpen;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcIsentoIof, 47, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcIsentoIof;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcIsentoIr, 48, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcIsentoIr;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcSocioBmf, 49, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcSocioBmf;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcMaebmf, 50, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcMaebmf;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcFatmaebmf, 51, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcFatmaebmf;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcTipoliqbmf, 52, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcTipoliqbmf;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcLiqmaebmf, 53, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcLiqmaebmf;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcContatobmf, 54, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcContatobmf;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcAssessorbmf, 55, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcAssessorbmf;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FdUltmovBmf, 56, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = GbclienteMetadata.PropertyNames.FdUltmovBmf;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcMaebolsa, 57, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcMaebolsa;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcTipoliqbolsa, 58, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcTipoliqbolsa;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcFatmaebolsa, 59, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcFatmaebolsa;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcLiqmaebolsa, 60, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcLiqmaebolsa;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcContatobolsa, 61, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcContatobolsa;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcAssessorbolsa, 62, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcAssessorbolsa;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCustpropria, 63, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCustpropria;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcTaxacust, 64, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcTaxacust;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FnPercust, 65, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GbclienteMetadata.PropertyNames.FnPercust;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FdUltmov, 66, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = GbclienteMetadata.PropertyNames.FdUltmov;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCliinst, 67, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCliinst;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcUsuinst, 68, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcUsuinst;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcFormaliq, 69, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcFormaliq;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcNomecd1, 70, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcNomecd1;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcNomecd2, 71, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcNomecd2;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCpfcd1, 72, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCpfcd1;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCpfcd2, 73, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCpfcd2;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcRgcd1, 74, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcRgcd1;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcRgcd2, 75, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcRgcd2;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcNomeao1, 76, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcNomeao1;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcNomeao2, 77, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcNomeao2;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCpfao1, 78, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCpfao1;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCpfao2, 79, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCpfao2;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcRgao1, 80, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcRgao1;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcRgao2, 81, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcRgao2;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcNomeui1, 82, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcNomeui1;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcNomeui2, 83, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcNomeui2;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCodui1, 84, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCodui1;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCodui2, 85, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCodui2;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcAdcvm, 86, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcAdcvm;
			c.CharacterMaxLength = 19;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCodcvm, 87, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCodcvm;
			c.CharacterMaxLength = 19;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcTitular, 88, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcTitular;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCodadm, 89, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCodadm;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcNomeadm, 90, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcNomeadm;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCodadmusu, 91, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCodadmusu;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcNomeadmusu, 92, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcNomeadmusu;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCodusu, 93, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCodusu;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCtbcli, 94, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCtbcli;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FlCtbpro, 95, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GbclienteMetadata.PropertyNames.FlCtbpro;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcTransmissor, 96, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcTransmissor;
			c.CharacterMaxLength = 15;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcTiporddef, 97, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcTiporddef;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FdDatacad, 98, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = GbclienteMetadata.PropertyNames.FdDatacad;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcIrfdt, 99, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcIrfdt;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCodgrupo, 100, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCodgrupo;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcLimitemae, 101, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcLimitemae;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FlConsolidaSelic, 102, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GbclienteMetadata.PropertyNames.FlConsolidaSelic;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FlConsolidaCetip, 103, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GbclienteMetadata.PropertyNames.FlConsolidaCetip;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FlConsolidaAdm, 104, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GbclienteMetadata.PropertyNames.FlConsolidaAdm;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCodconsolidafin, 105, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCodconsolidafin;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcFaixaselic, 106, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcFaixaselic;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcFatura, 107, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcFatura;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcContareserva, 108, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcContareserva;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcBrokercetip, 109, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcBrokercetip;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcBrokerselic, 110, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcBrokerselic;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCodgerente, 111, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCodgerente;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FnPercentual, 112, typeof(System.Double), esSystemType.Double);
			c.PropertyName = GbclienteMetadata.PropertyNames.FnPercentual;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FnLimite, 113, typeof(System.Double), esSystemType.Double);
			c.PropertyName = GbclienteMetadata.PropertyNames.FnLimite;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FlEspecial, 114, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GbclienteMetadata.PropertyNames.FlEspecial;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcMinemoemi, 115, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcMinemoemi;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcCodflutuante, 116, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcCodflutuante;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FlEmissor, 117, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GbclienteMetadata.PropertyNames.FlEmissor;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FlResidente, 118, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GbclienteMetadata.PropertyNames.FlResidente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FlIsentotxcustodia, 119, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GbclienteMetadata.PropertyNames.FlIsentotxcustodia;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FcPerfil, 120, typeof(System.String), esSystemType.String);
			c.PropertyName = GbclienteMetadata.PropertyNames.FcPerfil;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FlExtrato, 121, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GbclienteMetadata.PropertyNames.FlExtrato;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FlEmiliquida, 122, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GbclienteMetadata.PropertyNames.FlEmiliquida;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GbclienteMetadata.ColumnNames.FdEmiliqdata, 123, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = GbclienteMetadata.PropertyNames.FdEmiliqdata;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public GbclienteMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string FcMatricula = "FC_MATRICULA";
			 public const string FcCodbmf = "FC_CODBMF";
			 public const string FcCodbolsa = "FC_CODBOLSA";
			 public const string FcCodbolsa2 = "FC_CODBOLSA2";
			 public const string FcNome = "FC_NOME";
			 public const string FcAbreviado = "FC_ABREVIADO";
			 public const string FcCgccpf = "FC_CGCCPF";
			 public const string FcNasc = "FC_NASC";
			 public const string FcLiquidante = "FC_LIQUIDANTE";
			 public const string FcTipocliente = "FC_TIPOCLIENTE";
			 public const string FcTipotributo = "FC_TIPOTRIBUTO";
			 public const string FcId = "FC_ID";
			 public const string FcExpedidor = "FC_EXPEDIDOR";
			 public const string FcCartorio = "FC_CARTORIO";
			 public const string FcTrabalho = "FC_TRABALHO";
			 public const string FcProfissao = "FC_PROFISSAO";
			 public const string FcPai = "FC_PAI";
			 public const string FcMae = "FC_MAE";
			 public const string FcNacional = "FC_NACIONAL";
			 public const string FcEstcivil = "FC_ESTCIVIL";
			 public const string FcConjuge = "FC_CONJUGE";
			 public const string FcDestino = "FC_DESTINO";
			 public const string FcEmail = "FC_EMAIL";
			 public const string FcTelcom = "FC_TELCOM";
			 public const string FcRamal = "FC_RAMAL";
			 public const string FcFax = "FC_FAX";
			 public const string FcEnderes = "FC_ENDERES";
			 public const string FcEndecom = "FC_ENDECOM";
			 public const string FcBairrocom = "FC_BAIRROCOM";
			 public const string FcCidadecom = "FC_CIDADECOM";
			 public const string FcUfcom = "FC_UFCOM";
			 public const string FcCepcom = "FC_CEPCOM";
			 public const string FcBairrores = "FC_BAIRRORES";
			 public const string FcCidaderes = "FC_CIDADERES";
			 public const string FcCepres = "FC_CEPRES";
			 public const string FcUfres = "FC_UFRES";
			 public const string FcTelres = "FC_TELRES";
			 public const string FcNumbanco = "FC_NUMBANCO";
			 public const string FcBanco = "FC_BANCO";
			 public const string FcNumagen = "FC_NUMAGEN";
			 public const string FcAgencia = "FC_AGENCIA";
			 public const string FcPraca = "FC_PRACA";
			 public const string FcConta = "FC_CONTA";
			 public const string FcCetip = "FC_CETIP";
			 public const string FcSelic = "FC_SELIC";
			 public const string FcAnexo4 = "FC_ANEXO4";
			 public const string FcNivelRemOpen = "FC_NIVEL_REM_OPEN";
			 public const string FcIsentoIof = "FC_ISENTO_IOF";
			 public const string FcIsentoIr = "FC_ISENTO_IR";
			 public const string FcSocioBmf = "FC_SOCIO_BMF";
			 public const string FcMaebmf = "FC_MAEBMF";
			 public const string FcFatmaebmf = "FC_FATMAEBMF";
			 public const string FcTipoliqbmf = "FC_TIPOLIQBMF";
			 public const string FcLiqmaebmf = "FC_LIQMAEBMF";
			 public const string FcContatobmf = "FC_CONTATOBMF";
			 public const string FcAssessorbmf = "FC_ASSESSORBMF";
			 public const string FdUltmovBmf = "FD_ULTMOV_BMF";
			 public const string FcMaebolsa = "FC_MAEBOLSA";
			 public const string FcTipoliqbolsa = "FC_TIPOLIQBOLSA";
			 public const string FcFatmaebolsa = "FC_FATMAEBOLSA";
			 public const string FcLiqmaebolsa = "FC_LIQMAEBOLSA";
			 public const string FcContatobolsa = "FC_CONTATOBOLSA";
			 public const string FcAssessorbolsa = "FC_ASSESSORBOLSA";
			 public const string FcCustpropria = "FC_CUSTPROPRIA";
			 public const string FcTaxacust = "FC_TAXACUST";
			 public const string FnPercust = "FN_PERCUST";
			 public const string FdUltmov = "FD_ULTMOV";
			 public const string FcCliinst = "FC_CLIINST";
			 public const string FcUsuinst = "FC_USUINST";
			 public const string FcFormaliq = "FC_FORMALIQ";
			 public const string FcNomecd1 = "FC_NOMECD1";
			 public const string FcNomecd2 = "FC_NOMECD2";
			 public const string FcCpfcd1 = "FC_CPFCD1";
			 public const string FcCpfcd2 = "FC_CPFCD2";
			 public const string FcRgcd1 = "FC_RGCD1";
			 public const string FcRgcd2 = "FC_RGCD2";
			 public const string FcNomeao1 = "FC_NOMEAO1";
			 public const string FcNomeao2 = "FC_NOMEAO2";
			 public const string FcCpfao1 = "FC_CPFAO1";
			 public const string FcCpfao2 = "FC_CPFAO2";
			 public const string FcRgao1 = "FC_RGAO1";
			 public const string FcRgao2 = "FC_RGAO2";
			 public const string FcNomeui1 = "FC_NOMEUI1";
			 public const string FcNomeui2 = "FC_NOMEUI2";
			 public const string FcCodui1 = "FC_CODUI1";
			 public const string FcCodui2 = "FC_CODUI2";
			 public const string FcAdcvm = "FC_ADCVM";
			 public const string FcCodcvm = "FC_CODCVM";
			 public const string FcTitular = "FC_TITULAR";
			 public const string FcCodadm = "FC_CODADM";
			 public const string FcNomeadm = "FC_NOMEADM";
			 public const string FcCodadmusu = "FC_CODADMUSU";
			 public const string FcNomeadmusu = "FC_NOMEADMUSU";
			 public const string FcCodusu = "FC_CODUSU";
			 public const string FcCtbcli = "FC_CTBCLI";
			 public const string FlCtbpro = "FL_CTBPRO";
			 public const string FcTransmissor = "FC_TRANSMISSOR";
			 public const string FcTiporddef = "FC_TIPORDDEF";
			 public const string FdDatacad = "FD_DATACAD";
			 public const string FcIrfdt = "FC_IRFDT";
			 public const string FcCodgrupo = "FC_CODGRUPO";
			 public const string FcLimitemae = "FC_LIMITEMAE";
			 public const string FlConsolidaSelic = "FL_CONSOLIDA_SELIC";
			 public const string FlConsolidaCetip = "FL_CONSOLIDA_CETIP";
			 public const string FlConsolidaAdm = "FL_CONSOLIDA_ADM";
			 public const string FcCodconsolidafin = "FC_CODCONSOLIDAFIN";
			 public const string FcFaixaselic = "FC_FAIXASELIC";
			 public const string FcFatura = "FC_FATURA";
			 public const string FcContareserva = "FC_CONTARESERVA";
			 public const string FcBrokercetip = "FC_BROKERCETIP";
			 public const string FcBrokerselic = "FC_BROKERSELIC";
			 public const string FcCodgerente = "FC_CODGERENTE";
			 public const string FnPercentual = "FN_PERCENTUAL";
			 public const string FnLimite = "FN_LIMITE";
			 public const string FlEspecial = "FL_ESPECIAL";
			 public const string FcMinemoemi = "FC_MINEMOEMI";
			 public const string FcCodflutuante = "FC_CODFLUTUANTE";
			 public const string FlEmissor = "FL_EMISSOR";
			 public const string FlResidente = "FL_RESIDENTE";
			 public const string FlIsentotxcustodia = "FL_ISENTOTXCUSTODIA";
			 public const string FcPerfil = "FC_PERFIL";
			 public const string FlExtrato = "FL_EXTRATO";
			 public const string FlEmiliquida = "FL_EMILIQUIDA";
			 public const string FdEmiliqdata = "FD_EMILIQDATA";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string FcMatricula = "FcMatricula";
			 public const string FcCodbmf = "FcCodbmf";
			 public const string FcCodbolsa = "FcCodbolsa";
			 public const string FcCodbolsa2 = "FcCodbolsa2";
			 public const string FcNome = "FcNome";
			 public const string FcAbreviado = "FcAbreviado";
			 public const string FcCgccpf = "FcCgccpf";
			 public const string FcNasc = "FcNasc";
			 public const string FcLiquidante = "FcLiquidante";
			 public const string FcTipocliente = "FcTipocliente";
			 public const string FcTipotributo = "FcTipotributo";
			 public const string FcId = "FcId";
			 public const string FcExpedidor = "FcExpedidor";
			 public const string FcCartorio = "FcCartorio";
			 public const string FcTrabalho = "FcTrabalho";
			 public const string FcProfissao = "FcProfissao";
			 public const string FcPai = "FcPai";
			 public const string FcMae = "FcMae";
			 public const string FcNacional = "FcNacional";
			 public const string FcEstcivil = "FcEstcivil";
			 public const string FcConjuge = "FcConjuge";
			 public const string FcDestino = "FcDestino";
			 public const string FcEmail = "FcEmail";
			 public const string FcTelcom = "FcTelcom";
			 public const string FcRamal = "FcRamal";
			 public const string FcFax = "FcFax";
			 public const string FcEnderes = "FcEnderes";
			 public const string FcEndecom = "FcEndecom";
			 public const string FcBairrocom = "FcBairrocom";
			 public const string FcCidadecom = "FcCidadecom";
			 public const string FcUfcom = "FcUfcom";
			 public const string FcCepcom = "FcCepcom";
			 public const string FcBairrores = "FcBairrores";
			 public const string FcCidaderes = "FcCidaderes";
			 public const string FcCepres = "FcCepres";
			 public const string FcUfres = "FcUfres";
			 public const string FcTelres = "FcTelres";
			 public const string FcNumbanco = "FcNumbanco";
			 public const string FcBanco = "FcBanco";
			 public const string FcNumagen = "FcNumagen";
			 public const string FcAgencia = "FcAgencia";
			 public const string FcPraca = "FcPraca";
			 public const string FcConta = "FcConta";
			 public const string FcCetip = "FcCetip";
			 public const string FcSelic = "FcSelic";
			 public const string FcAnexo4 = "FcAnexo4";
			 public const string FcNivelRemOpen = "FcNivelRemOpen";
			 public const string FcIsentoIof = "FcIsentoIof";
			 public const string FcIsentoIr = "FcIsentoIr";
			 public const string FcSocioBmf = "FcSocioBmf";
			 public const string FcMaebmf = "FcMaebmf";
			 public const string FcFatmaebmf = "FcFatmaebmf";
			 public const string FcTipoliqbmf = "FcTipoliqbmf";
			 public const string FcLiqmaebmf = "FcLiqmaebmf";
			 public const string FcContatobmf = "FcContatobmf";
			 public const string FcAssessorbmf = "FcAssessorbmf";
			 public const string FdUltmovBmf = "FdUltmovBmf";
			 public const string FcMaebolsa = "FcMaebolsa";
			 public const string FcTipoliqbolsa = "FcTipoliqbolsa";
			 public const string FcFatmaebolsa = "FcFatmaebolsa";
			 public const string FcLiqmaebolsa = "FcLiqmaebolsa";
			 public const string FcContatobolsa = "FcContatobolsa";
			 public const string FcAssessorbolsa = "FcAssessorbolsa";
			 public const string FcCustpropria = "FcCustpropria";
			 public const string FcTaxacust = "FcTaxacust";
			 public const string FnPercust = "FnPercust";
			 public const string FdUltmov = "FdUltmov";
			 public const string FcCliinst = "FcCliinst";
			 public const string FcUsuinst = "FcUsuinst";
			 public const string FcFormaliq = "FcFormaliq";
			 public const string FcNomecd1 = "FcNomecd1";
			 public const string FcNomecd2 = "FcNomecd2";
			 public const string FcCpfcd1 = "FcCpfcd1";
			 public const string FcCpfcd2 = "FcCpfcd2";
			 public const string FcRgcd1 = "FcRgcd1";
			 public const string FcRgcd2 = "FcRgcd2";
			 public const string FcNomeao1 = "FcNomeao1";
			 public const string FcNomeao2 = "FcNomeao2";
			 public const string FcCpfao1 = "FcCpfao1";
			 public const string FcCpfao2 = "FcCpfao2";
			 public const string FcRgao1 = "FcRgao1";
			 public const string FcRgao2 = "FcRgao2";
			 public const string FcNomeui1 = "FcNomeui1";
			 public const string FcNomeui2 = "FcNomeui2";
			 public const string FcCodui1 = "FcCodui1";
			 public const string FcCodui2 = "FcCodui2";
			 public const string FcAdcvm = "FcAdcvm";
			 public const string FcCodcvm = "FcCodcvm";
			 public const string FcTitular = "FcTitular";
			 public const string FcCodadm = "FcCodadm";
			 public const string FcNomeadm = "FcNomeadm";
			 public const string FcCodadmusu = "FcCodadmusu";
			 public const string FcNomeadmusu = "FcNomeadmusu";
			 public const string FcCodusu = "FcCodusu";
			 public const string FcCtbcli = "FcCtbcli";
			 public const string FlCtbpro = "FlCtbpro";
			 public const string FcTransmissor = "FcTransmissor";
			 public const string FcTiporddef = "FcTiporddef";
			 public const string FdDatacad = "FdDatacad";
			 public const string FcIrfdt = "FcIrfdt";
			 public const string FcCodgrupo = "FcCodgrupo";
			 public const string FcLimitemae = "FcLimitemae";
			 public const string FlConsolidaSelic = "FlConsolidaSelic";
			 public const string FlConsolidaCetip = "FlConsolidaCetip";
			 public const string FlConsolidaAdm = "FlConsolidaAdm";
			 public const string FcCodconsolidafin = "FcCodconsolidafin";
			 public const string FcFaixaselic = "FcFaixaselic";
			 public const string FcFatura = "FcFatura";
			 public const string FcContareserva = "FcContareserva";
			 public const string FcBrokercetip = "FcBrokercetip";
			 public const string FcBrokerselic = "FcBrokerselic";
			 public const string FcCodgerente = "FcCodgerente";
			 public const string FnPercentual = "FnPercentual";
			 public const string FnLimite = "FnLimite";
			 public const string FlEspecial = "FlEspecial";
			 public const string FcMinemoemi = "FcMinemoemi";
			 public const string FcCodflutuante = "FcCodflutuante";
			 public const string FlEmissor = "FlEmissor";
			 public const string FlResidente = "FlResidente";
			 public const string FlIsentotxcustodia = "FlIsentotxcustodia";
			 public const string FcPerfil = "FcPerfil";
			 public const string FlExtrato = "FlExtrato";
			 public const string FlEmiliquida = "FlEmiliquida";
			 public const string FdEmiliqdata = "FdEmiliqdata";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GbclienteMetadata))
			{
				if(GbclienteMetadata.mapDelegates == null)
				{
					GbclienteMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GbclienteMetadata.meta == null)
				{
					GbclienteMetadata.meta = new GbclienteMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("FC_MATRICULA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODBOLSA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODBOLSA2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NOME", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ABREVIADO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CGCCPF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NASC", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FC_LIQUIDANTE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TIPOCLIENTE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TIPOTRIBUTO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ID", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_EXPEDIDOR", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CARTORIO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TRABALHO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_PROFISSAO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_PAI", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_MAE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NACIONAL", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ESTCIVIL", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CONJUGE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_DESTINO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_EMAIL", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TELCOM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_RAMAL", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_FAX", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ENDERES", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ENDECOM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_BAIRROCOM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CIDADECOM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_UFCOM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CEPCOM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_BAIRRORES", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CIDADERES", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CEPRES", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_UFRES", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TELRES", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NUMBANCO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_BANCO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NUMAGEN", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_AGENCIA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_PRACA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CONTA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CETIP", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_SELIC", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ANEXO4", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NIVEL_REM_OPEN", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ISENTO_IOF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ISENTO_IR", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_SOCIO_BMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_MAEBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_FATMAEBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TIPOLIQBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_LIQMAEBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CONTATOBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ASSESSORBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FD_ULTMOV_BMF", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FC_MAEBOLSA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TIPOLIQBOLSA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_FATMAEBOLSA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_LIQMAEBOLSA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CONTATOBOLSA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ASSESSORBOLSA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CUSTPROPRIA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TAXACUST", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FN_PERCUST", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FD_ULTMOV", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FC_CLIINST", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_USUINST", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_FORMALIQ", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NOMECD1", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NOMECD2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CPFCD1", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CPFCD2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_RGCD1", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_RGCD2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NOMEAO1", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NOMEAO2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CPFAO1", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CPFAO2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_RGAO1", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_RGAO2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NOMEUI1", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NOMEUI2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODUI1", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODUI2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ADCVM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODCVM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TITULAR", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODADM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NOMEADM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODADMUSU", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NOMEADMUSU", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODUSU", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CTBCLI", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FL_CTBPRO", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FC_TRANSMISSOR", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TIPORDDEF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FD_DATACAD", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FC_IRFDT", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODGRUPO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_LIMITEMAE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FL_CONSOLIDA_SELIC", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FL_CONSOLIDA_CETIP", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FL_CONSOLIDA_ADM", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FC_CODCONSOLIDAFIN", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_FAIXASELIC", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_FATURA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CONTARESERVA", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FC_BROKERCETIP", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_BROKERSELIC", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODGERENTE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FN_PERCENTUAL", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_LIMITE", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FL_ESPECIAL", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FC_MINEMOEMI", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODFLUTUANTE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FL_EMISSOR", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FL_RESIDENTE", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FL_ISENTOTXCUSTODIA", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FC_PERFIL", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FL_EXTRATO", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FL_EMILIQUIDA", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FD_EMILIQDATA", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "GBCLIENTE";
				meta.Destination = "GBCLIENTE";
				
				meta.spInsert = "proc_GBCLIENTEInsert";				
				meta.spUpdate = "proc_GBCLIENTEUpdate";		
				meta.spDelete = "proc_GBCLIENTEDelete";
				meta.spLoadAll = "proc_GBCLIENTELoadAll";
				meta.spLoadByPrimaryKey = "proc_GBCLIENTELoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GbclienteMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
