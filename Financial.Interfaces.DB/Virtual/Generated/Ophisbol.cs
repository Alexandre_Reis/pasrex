/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/12/2013 17:28:40
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;











































































































































































































namespace Financial.InterfacesDB
{

	[Serializable]
	abstract public class esOphisbolCollection : esEntityCollection
	{
		public esOphisbolCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OphisbolCollection";
		}

		#region Query Logic
		protected void InitQuery(esOphisbolQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOphisbolQuery);
		}
		#endregion
		
		virtual public Ophisbol DetachEntity(Ophisbol entity)
		{
			return base.DetachEntity(entity) as Ophisbol;
		}
		
		virtual public Ophisbol AttachEntity(Ophisbol entity)
		{
			return base.AttachEntity(entity) as Ophisbol;
		}
		
		virtual public void Combine(OphisbolCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Ophisbol this[int index]
		{
			get
			{
				return base[index] as Ophisbol;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Ophisbol);
		}
	}



	[Serializable]
	abstract public class esOphisbol : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOphisbolQuery GetDynamicQuery()
		{
			return null;
		}

		public esOphisbol()
		{

		}

		public esOphisbol(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Double fnCodbol, System.DateTime fdTrandata)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(fnCodbol, fdTrandata);
			else
				return LoadByPrimaryKeyStoredProcedure(fnCodbol, fdTrandata);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Double fnCodbol, System.DateTime fdTrandata)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esOphisbolQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.FnCodbol == fnCodbol, query.FdTrandata == fdTrandata);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Double fnCodbol, System.DateTime fdTrandata)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(fnCodbol, fdTrandata);
			else
				return LoadByPrimaryKeyStoredProcedure(fnCodbol, fdTrandata);
		}

		private bool LoadByPrimaryKeyDynamic(System.Double fnCodbol, System.DateTime fdTrandata)
		{
			esOphisbolQuery query = this.GetDynamicQuery();
			query.Where(query.FnCodbol == fnCodbol, query.FdTrandata == fdTrandata);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Double fnCodbol, System.DateTime fdTrandata)
		{
			esParameters parms = new esParameters();
			parms.Add("FN_CODBOL",fnCodbol);			parms.Add("FD_TRANDATA",fdTrandata);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "FnCodbol": this.str.FnCodbol = (string)value; break;							
						case "FnCodbolreferencia": this.str.FnCodbolreferencia = (string)value; break;							
						case "FcMatricula": this.str.FcMatricula = (string)value; break;							
						case "FcCodempresa": this.str.FcCodempresa = (string)value; break;							
						case "FcAssessor": this.str.FcAssessor = (string)value; break;							
						case "FcCodliquida": this.str.FcCodliquida = (string)value; break;							
						case "FcTpoper": this.str.FcTpoper = (string)value; break;							
						case "FcCodtitulo": this.str.FcCodtitulo = (string)value; break;							
						case "FcCodmercado": this.str.FcCodmercado = (string)value; break;							
						case "FcCodbroker": this.str.FcCodbroker = (string)value; break;							
						case "FcCodlimite": this.str.FcCodlimite = (string)value; break;							
						case "FcCodbanco": this.str.FcCodbanco = (string)value; break;							
						case "FcConta": this.str.FcConta = (string)value; break;							
						case "FcIfcedente": this.str.FcIfcedente = (string)value; break;							
						case "FcIfcessionario": this.str.FcIfcessionario = (string)value; break;							
						case "FcCodvinculo": this.str.FcCodvinculo = (string)value; break;							
						case "FdAquisicao": this.str.FdAquisicao = (string)value; break;							
						case "FdDatoper": this.str.FdDatoper = (string)value; break;							
						case "FdHorabol": this.str.FdHorabol = (string)value; break;							
						case "FdHoracombinada": this.str.FdHoracombinada = (string)value; break;							
						case "FnTxnominal": this.str.FnTxnominal = (string)value; break;							
						case "FnFir": this.str.FnFir = (string)value; break;							
						case "FnPuaquisicao": this.str.FnPuaquisicao = (string)value; break;							
						case "FnTxoper": this.str.FnTxoper = (string)value; break;							
						case "FnIr": this.str.FnIr = (string)value; break;							
						case "FnIof": this.str.FnIof = (string)value; break;							
						case "FnIofcomp": this.str.FnIofcomp = (string)value; break;							
						case "FnTxcomp": this.str.FnTxcomp = (string)value; break;							
						case "FnIrcomp": this.str.FnIrcomp = (string)value; break;							
						case "FnQtdeoper": this.str.FnQtdeoper = (string)value; break;							
						case "FnPuoper": this.str.FnPuoper = (string)value; break;							
						case "FnValorbruto": this.str.FnValorbruto = (string)value; break;							
						case "FnPucomp": this.str.FnPucomp = (string)value; break;							
						case "FnValorcomp": this.str.FnValorcomp = (string)value; break;							
						case "FdDatcomp": this.str.FdDatcomp = (string)value; break;							
						case "FcComando": this.str.FcComando = (string)value; break;							
						case "FcIndiceger": this.str.FcIndiceger = (string)value; break;							
						case "FnPercentger": this.str.FnPercentger = (string)value; break;							
						case "FnTxger": this.str.FnTxger = (string)value; break;							
						case "FcRemufaixa": this.str.FcRemufaixa = (string)value; break;							
						case "FcControle": this.str.FcControle = (string)value; break;							
						case "FcTipoestoque": this.str.FcTipoestoque = (string)value; break;							
						case "FcGrupa": this.str.FcGrupa = (string)value; break;							
						case "FdDatlastro": this.str.FdDatlastro = (string)value; break;							
						case "FnBollastro": this.str.FnBollastro = (string)value; break;							
						case "FnPulastro": this.str.FnPulastro = (string)value; break;							
						case "FcStatus": this.str.FcStatus = (string)value; break;							
						case "FdHoraenvio": this.str.FdHoraenvio = (string)value; break;							
						case "FdAgendamento": this.str.FdAgendamento = (string)value; break;							
						case "FdDatliquidacao": this.str.FdDatliquidacao = (string)value; break;							
						case "FcCodmensagem": this.str.FcCodmensagem = (string)value; break;							
						case "FcTipolei": this.str.FcTipolei = (string)value; break;							
						case "FcControleif": this.str.FcControleif = (string)value; break;							
						case "FcNivelpref": this.str.FcNivelpref = (string)value; break;							
						case "FcChaveassoc": this.str.FcChaveassoc = (string)value; break;							
						case "FcNumopsel": this.str.FcNumopsel = (string)value; break;							
						case "FcNumopselret": this.str.FcNumopselret = (string)value; break;							
						case "FcTpleilao": this.str.FcTpleilao = (string)value; break;							
						case "FdTrandata": this.str.FdTrandata = (string)value; break;							
						case "FcNsu": this.str.FcNsu = (string)value; break;							
						case "FcRdcret": this.str.FcRdcret = (string)value; break;							
						case "FcDestino": this.str.FcDestino = (string)value; break;							
						case "FcObs": this.str.FcObs = (string)value; break;							
						case "FnCodbolPre": this.str.FnCodbolPre = (string)value; break;							
						case "FlBrokerces": this.str.FlBrokerces = (string)value; break;							
						case "FlBrokerced": this.str.FlBrokerced = (string)value; break;							
						case "FdDatcomptermo": this.str.FdDatcomptermo = (string)value; break;							
						case "FnBase": this.str.FnBase = (string)value; break;							
						case "FdDatcompini": this.str.FdDatcompini = (string)value; break;							
						case "FcUnilateralidade": this.str.FcUnilateralidade = (string)value; break;							
						case "FcUsuario": this.str.FcUsuario = (string)value; break;							
						case "FcOrigem": this.str.FcOrigem = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "FnCodbol":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnCodbol = (System.Double?)value;
							break;
						
						case "FnCodbolreferencia":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnCodbolreferencia = (System.Double?)value;
							break;
						
						case "FdAquisicao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdAquisicao = (System.DateTime?)value;
							break;
						
						case "FdDatoper":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatoper = (System.DateTime?)value;
							break;
						
						case "FdHorabol":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdHorabol = (System.DateTime?)value;
							break;
						
						case "FdHoracombinada":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdHoracombinada = (System.DateTime?)value;
							break;
						
						case "FnTxnominal":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnTxnominal = (System.Double?)value;
							break;
						
						case "FnFir":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnFir = (System.Double?)value;
							break;
						
						case "FnPuaquisicao":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPuaquisicao = (System.Double?)value;
							break;
						
						case "FnTxoper":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnTxoper = (System.Double?)value;
							break;
						
						case "FnIr":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnIr = (System.Double?)value;
							break;
						
						case "FnIof":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnIof = (System.Double?)value;
							break;
						
						case "FnIofcomp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnIofcomp = (System.Double?)value;
							break;
						
						case "FnTxcomp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnTxcomp = (System.Double?)value;
							break;
						
						case "FnIrcomp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnIrcomp = (System.Double?)value;
							break;
						
						case "FnQtdeoper":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnQtdeoper = (System.Double?)value;
							break;
						
						case "FnPuoper":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPuoper = (System.Double?)value;
							break;
						
						case "FnValorbruto":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnValorbruto = (System.Double?)value;
							break;
						
						case "FnPucomp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPucomp = (System.Double?)value;
							break;
						
						case "FnValorcomp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnValorcomp = (System.Double?)value;
							break;
						
						case "FdDatcomp":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatcomp = (System.DateTime?)value;
							break;
						
						case "FnPercentger":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPercentger = (System.Double?)value;
							break;
						
						case "FnTxger":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnTxger = (System.Double?)value;
							break;
						
						case "FdDatlastro":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatlastro = (System.DateTime?)value;
							break;
						
						case "FnBollastro":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnBollastro = (System.Double?)value;
							break;
						
						case "FnPulastro":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPulastro = (System.Double?)value;
							break;
						
						case "FdHoraenvio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdHoraenvio = (System.DateTime?)value;
							break;
						
						case "FdAgendamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdAgendamento = (System.DateTime?)value;
							break;
						
						case "FdDatliquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatliquidacao = (System.DateTime?)value;
							break;
						
						case "FdTrandata":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdTrandata = (System.DateTime?)value;
							break;
						
						case "FnCodbolPre":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnCodbolPre = (System.Double?)value;
							break;
						
						case "FlBrokerces":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FlBrokerces = (System.Int32?)value;
							break;
						
						case "FlBrokerced":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FlBrokerced = (System.Int32?)value;
							break;
						
						case "FdDatcomptermo":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatcomptermo = (System.DateTime?)value;
							break;
						
						case "FnBase":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FnBase = (System.Int32?)value;
							break;
						
						case "FdDatcompini":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatcompini = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OPHISBOL.FN_CODBOL
		/// </summary>
		virtual public System.Double? FnCodbol
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnCodbol);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnCodbol, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_CODBOLREFERENCIA
		/// </summary>
		virtual public System.Double? FnCodbolreferencia
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnCodbolreferencia);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnCodbolreferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_MATRICULA
		/// </summary>
		virtual public System.String FcMatricula
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcMatricula);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcMatricula, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_CODEMPRESA
		/// </summary>
		virtual public System.String FcCodempresa
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcCodempresa);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcCodempresa, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_ASSESSOR
		/// </summary>
		virtual public System.String FcAssessor
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcAssessor);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcAssessor, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_CODLIQUIDA
		/// </summary>
		virtual public System.String FcCodliquida
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcCodliquida);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcCodliquida, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_TPOPER
		/// </summary>
		virtual public System.String FcTpoper
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcTpoper);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcTpoper, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_CODTITULO
		/// </summary>
		virtual public System.String FcCodtitulo
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcCodtitulo);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcCodtitulo, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_CODMERCADO
		/// </summary>
		virtual public System.String FcCodmercado
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcCodmercado);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcCodmercado, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_CODBROKER
		/// </summary>
		virtual public System.String FcCodbroker
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcCodbroker);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcCodbroker, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_CODLIMITE
		/// </summary>
		virtual public System.String FcCodlimite
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcCodlimite);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcCodlimite, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_CODBANCO
		/// </summary>
		virtual public System.String FcCodbanco
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcCodbanco);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcCodbanco, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_CONTA
		/// </summary>
		virtual public System.String FcConta
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcConta);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcConta, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_IFCEDENTE
		/// </summary>
		virtual public System.String FcIfcedente
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcIfcedente);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcIfcedente, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_IFCESSIONARIO
		/// </summary>
		virtual public System.String FcIfcessionario
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcIfcessionario);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcIfcessionario, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_CODVINCULO
		/// </summary>
		virtual public System.String FcCodvinculo
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcCodvinculo);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcCodvinculo, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FD_AQUISICAO
		/// </summary>
		virtual public System.DateTime? FdAquisicao
		{
			get
			{
				return base.GetSystemDateTime(OphisbolMetadata.ColumnNames.FdAquisicao);
			}
			
			set
			{
				base.SetSystemDateTime(OphisbolMetadata.ColumnNames.FdAquisicao, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FD_DATOPER
		/// </summary>
		virtual public System.DateTime? FdDatoper
		{
			get
			{
				return base.GetSystemDateTime(OphisbolMetadata.ColumnNames.FdDatoper);
			}
			
			set
			{
				base.SetSystemDateTime(OphisbolMetadata.ColumnNames.FdDatoper, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FD_HORABOL
		/// </summary>
		virtual public System.DateTime? FdHorabol
		{
			get
			{
				return base.GetSystemDateTime(OphisbolMetadata.ColumnNames.FdHorabol);
			}
			
			set
			{
				base.SetSystemDateTime(OphisbolMetadata.ColumnNames.FdHorabol, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FD_HORACOMBINADA
		/// </summary>
		virtual public System.DateTime? FdHoracombinada
		{
			get
			{
				return base.GetSystemDateTime(OphisbolMetadata.ColumnNames.FdHoracombinada);
			}
			
			set
			{
				base.SetSystemDateTime(OphisbolMetadata.ColumnNames.FdHoracombinada, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_TXNOMINAL
		/// </summary>
		virtual public System.Double? FnTxnominal
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnTxnominal);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnTxnominal, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_FIR
		/// </summary>
		virtual public System.Double? FnFir
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnFir);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnFir, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_PUAQUISICAO
		/// </summary>
		virtual public System.Double? FnPuaquisicao
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnPuaquisicao);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnPuaquisicao, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_TXOPER
		/// </summary>
		virtual public System.Double? FnTxoper
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnTxoper);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnTxoper, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_IR
		/// </summary>
		virtual public System.Double? FnIr
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnIr);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnIr, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_IOF
		/// </summary>
		virtual public System.Double? FnIof
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnIof);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnIof, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_IOFCOMP
		/// </summary>
		virtual public System.Double? FnIofcomp
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnIofcomp);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnIofcomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_TXCOMP
		/// </summary>
		virtual public System.Double? FnTxcomp
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnTxcomp);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnTxcomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_IRCOMP
		/// </summary>
		virtual public System.Double? FnIrcomp
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnIrcomp);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnIrcomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_QTDEOPER
		/// </summary>
		virtual public System.Double? FnQtdeoper
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnQtdeoper);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnQtdeoper, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_PUOPER
		/// </summary>
		virtual public System.Double? FnPuoper
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnPuoper);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnPuoper, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_VALORBRUTO
		/// </summary>
		virtual public System.Double? FnValorbruto
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnValorbruto);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnValorbruto, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_PUCOMP
		/// </summary>
		virtual public System.Double? FnPucomp
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnPucomp);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnPucomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_VALORCOMP
		/// </summary>
		virtual public System.Double? FnValorcomp
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnValorcomp);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnValorcomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FD_DATCOMP
		/// </summary>
		virtual public System.DateTime? FdDatcomp
		{
			get
			{
				return base.GetSystemDateTime(OphisbolMetadata.ColumnNames.FdDatcomp);
			}
			
			set
			{
				base.SetSystemDateTime(OphisbolMetadata.ColumnNames.FdDatcomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_COMANDO
		/// </summary>
		virtual public System.String FcComando
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcComando);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcComando, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_INDICEGER
		/// </summary>
		virtual public System.String FcIndiceger
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcIndiceger);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcIndiceger, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_PERCENTGER
		/// </summary>
		virtual public System.Double? FnPercentger
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnPercentger);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnPercentger, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_TXGER
		/// </summary>
		virtual public System.Double? FnTxger
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnTxger);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnTxger, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_REMUFAIXA
		/// </summary>
		virtual public System.String FcRemufaixa
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcRemufaixa);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcRemufaixa, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_CONTROLE
		/// </summary>
		virtual public System.String FcControle
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcControle);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcControle, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_TIPOESTOQUE
		/// </summary>
		virtual public System.String FcTipoestoque
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcTipoestoque);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcTipoestoque, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_GRUPA
		/// </summary>
		virtual public System.String FcGrupa
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcGrupa);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcGrupa, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FD_DATLASTRO
		/// </summary>
		virtual public System.DateTime? FdDatlastro
		{
			get
			{
				return base.GetSystemDateTime(OphisbolMetadata.ColumnNames.FdDatlastro);
			}
			
			set
			{
				base.SetSystemDateTime(OphisbolMetadata.ColumnNames.FdDatlastro, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_BOLLASTRO
		/// </summary>
		virtual public System.Double? FnBollastro
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnBollastro);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnBollastro, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_PULASTRO
		/// </summary>
		virtual public System.Double? FnPulastro
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnPulastro);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnPulastro, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_STATUS
		/// </summary>
		virtual public System.String FcStatus
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcStatus);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcStatus, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FD_HORAENVIO
		/// </summary>
		virtual public System.DateTime? FdHoraenvio
		{
			get
			{
				return base.GetSystemDateTime(OphisbolMetadata.ColumnNames.FdHoraenvio);
			}
			
			set
			{
				base.SetSystemDateTime(OphisbolMetadata.ColumnNames.FdHoraenvio, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FD_AGENDAMENTO
		/// </summary>
		virtual public System.DateTime? FdAgendamento
		{
			get
			{
				return base.GetSystemDateTime(OphisbolMetadata.ColumnNames.FdAgendamento);
			}
			
			set
			{
				base.SetSystemDateTime(OphisbolMetadata.ColumnNames.FdAgendamento, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FD_DATLIQUIDACAO
		/// </summary>
		virtual public System.DateTime? FdDatliquidacao
		{
			get
			{
				return base.GetSystemDateTime(OphisbolMetadata.ColumnNames.FdDatliquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(OphisbolMetadata.ColumnNames.FdDatliquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_CODMENSAGEM
		/// </summary>
		virtual public System.String FcCodmensagem
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcCodmensagem);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcCodmensagem, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_TIPOLEI
		/// </summary>
		virtual public System.String FcTipolei
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcTipolei);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcTipolei, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_CONTROLEIF
		/// </summary>
		virtual public System.String FcControleif
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcControleif);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcControleif, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_NIVELPREF
		/// </summary>
		virtual public System.String FcNivelpref
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcNivelpref);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcNivelpref, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_CHAVEASSOC
		/// </summary>
		virtual public System.String FcChaveassoc
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcChaveassoc);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcChaveassoc, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_NUMOPSEL
		/// </summary>
		virtual public System.String FcNumopsel
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcNumopsel);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcNumopsel, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_NUMOPSELRET
		/// </summary>
		virtual public System.String FcNumopselret
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcNumopselret);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcNumopselret, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_TPLEILAO
		/// </summary>
		virtual public System.String FcTpleilao
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcTpleilao);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcTpleilao, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FD_TRANDATA
		/// </summary>
		virtual public System.DateTime? FdTrandata
		{
			get
			{
				return base.GetSystemDateTime(OphisbolMetadata.ColumnNames.FdTrandata);
			}
			
			set
			{
				base.SetSystemDateTime(OphisbolMetadata.ColumnNames.FdTrandata, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_NSU
		/// </summary>
		virtual public System.String FcNsu
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcNsu);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcNsu, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_RDCRET
		/// </summary>
		virtual public System.String FcRdcret
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcRdcret);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcRdcret, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_DESTINO
		/// </summary>
		virtual public System.String FcDestino
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcDestino);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcDestino, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_OBS
		/// </summary>
		virtual public System.String FcObs
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcObs);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcObs, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_CODBOL_PRE
		/// </summary>
		virtual public System.Double? FnCodbolPre
		{
			get
			{
				return base.GetSystemDouble(OphisbolMetadata.ColumnNames.FnCodbolPre);
			}
			
			set
			{
				base.SetSystemDouble(OphisbolMetadata.ColumnNames.FnCodbolPre, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FL_BROKERCES
		/// </summary>
		virtual public System.Int32? FlBrokerces
		{
			get
			{
				return base.GetSystemInt32(OphisbolMetadata.ColumnNames.FlBrokerces);
			}
			
			set
			{
				base.SetSystemInt32(OphisbolMetadata.ColumnNames.FlBrokerces, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FL_BROKERCED
		/// </summary>
		virtual public System.Int32? FlBrokerced
		{
			get
			{
				return base.GetSystemInt32(OphisbolMetadata.ColumnNames.FlBrokerced);
			}
			
			set
			{
				base.SetSystemInt32(OphisbolMetadata.ColumnNames.FlBrokerced, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FD_DATCOMPTERMO
		/// </summary>
		virtual public System.DateTime? FdDatcomptermo
		{
			get
			{
				return base.GetSystemDateTime(OphisbolMetadata.ColumnNames.FdDatcomptermo);
			}
			
			set
			{
				base.SetSystemDateTime(OphisbolMetadata.ColumnNames.FdDatcomptermo, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FN_BASE
		/// </summary>
		virtual public System.Int32? FnBase
		{
			get
			{
				return base.GetSystemInt32(OphisbolMetadata.ColumnNames.FnBase);
			}
			
			set
			{
				base.SetSystemInt32(OphisbolMetadata.ColumnNames.FnBase, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FD_DATCOMPINI
		/// </summary>
		virtual public System.DateTime? FdDatcompini
		{
			get
			{
				return base.GetSystemDateTime(OphisbolMetadata.ColumnNames.FdDatcompini);
			}
			
			set
			{
				base.SetSystemDateTime(OphisbolMetadata.ColumnNames.FdDatcompini, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_UNILATERALIDADE
		/// </summary>
		virtual public System.String FcUnilateralidade
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcUnilateralidade);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcUnilateralidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_USUARIO
		/// </summary>
		virtual public System.String FcUsuario
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcUsuario);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcUsuario, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISBOL.FC_ORIGEM
		/// </summary>
		virtual public System.String FcOrigem
		{
			get
			{
				return base.GetSystemString(OphisbolMetadata.ColumnNames.FcOrigem);
			}
			
			set
			{
				base.SetSystemString(OphisbolMetadata.ColumnNames.FcOrigem, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOphisbol entity)
			{
				this.entity = entity;
			}
			
	
			public System.String FnCodbol
			{
				get
				{
					System.Double? data = entity.FnCodbol;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnCodbol = null;
					else entity.FnCodbol = Convert.ToDouble(value);
				}
			}
				
			public System.String FnCodbolreferencia
			{
				get
				{
					System.Double? data = entity.FnCodbolreferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnCodbolreferencia = null;
					else entity.FnCodbolreferencia = Convert.ToDouble(value);
				}
			}
				
			public System.String FcMatricula
			{
				get
				{
					System.String data = entity.FcMatricula;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcMatricula = null;
					else entity.FcMatricula = Convert.ToString(value);
				}
			}
				
			public System.String FcCodempresa
			{
				get
				{
					System.String data = entity.FcCodempresa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodempresa = null;
					else entity.FcCodempresa = Convert.ToString(value);
				}
			}
				
			public System.String FcAssessor
			{
				get
				{
					System.String data = entity.FcAssessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcAssessor = null;
					else entity.FcAssessor = Convert.ToString(value);
				}
			}
				
			public System.String FcCodliquida
			{
				get
				{
					System.String data = entity.FcCodliquida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodliquida = null;
					else entity.FcCodliquida = Convert.ToString(value);
				}
			}
				
			public System.String FcTpoper
			{
				get
				{
					System.String data = entity.FcTpoper;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTpoper = null;
					else entity.FcTpoper = Convert.ToString(value);
				}
			}
				
			public System.String FcCodtitulo
			{
				get
				{
					System.String data = entity.FcCodtitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodtitulo = null;
					else entity.FcCodtitulo = Convert.ToString(value);
				}
			}
				
			public System.String FcCodmercado
			{
				get
				{
					System.String data = entity.FcCodmercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodmercado = null;
					else entity.FcCodmercado = Convert.ToString(value);
				}
			}
				
			public System.String FcCodbroker
			{
				get
				{
					System.String data = entity.FcCodbroker;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodbroker = null;
					else entity.FcCodbroker = Convert.ToString(value);
				}
			}
				
			public System.String FcCodlimite
			{
				get
				{
					System.String data = entity.FcCodlimite;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodlimite = null;
					else entity.FcCodlimite = Convert.ToString(value);
				}
			}
				
			public System.String FcCodbanco
			{
				get
				{
					System.String data = entity.FcCodbanco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodbanco = null;
					else entity.FcCodbanco = Convert.ToString(value);
				}
			}
				
			public System.String FcConta
			{
				get
				{
					System.String data = entity.FcConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcConta = null;
					else entity.FcConta = Convert.ToString(value);
				}
			}
				
			public System.String FcIfcedente
			{
				get
				{
					System.String data = entity.FcIfcedente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcIfcedente = null;
					else entity.FcIfcedente = Convert.ToString(value);
				}
			}
				
			public System.String FcIfcessionario
			{
				get
				{
					System.String data = entity.FcIfcessionario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcIfcessionario = null;
					else entity.FcIfcessionario = Convert.ToString(value);
				}
			}
				
			public System.String FcCodvinculo
			{
				get
				{
					System.String data = entity.FcCodvinculo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodvinculo = null;
					else entity.FcCodvinculo = Convert.ToString(value);
				}
			}
				
			public System.String FdAquisicao
			{
				get
				{
					System.DateTime? data = entity.FdAquisicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdAquisicao = null;
					else entity.FdAquisicao = Convert.ToDateTime(value);
				}
			}
				
			public System.String FdDatoper
			{
				get
				{
					System.DateTime? data = entity.FdDatoper;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatoper = null;
					else entity.FdDatoper = Convert.ToDateTime(value);
				}
			}
				
			public System.String FdHorabol
			{
				get
				{
					System.DateTime? data = entity.FdHorabol;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdHorabol = null;
					else entity.FdHorabol = Convert.ToDateTime(value);
				}
			}
				
			public System.String FdHoracombinada
			{
				get
				{
					System.DateTime? data = entity.FdHoracombinada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdHoracombinada = null;
					else entity.FdHoracombinada = Convert.ToDateTime(value);
				}
			}
				
			public System.String FnTxnominal
			{
				get
				{
					System.Double? data = entity.FnTxnominal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnTxnominal = null;
					else entity.FnTxnominal = Convert.ToDouble(value);
				}
			}
				
			public System.String FnFir
			{
				get
				{
					System.Double? data = entity.FnFir;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnFir = null;
					else entity.FnFir = Convert.ToDouble(value);
				}
			}
				
			public System.String FnPuaquisicao
			{
				get
				{
					System.Double? data = entity.FnPuaquisicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPuaquisicao = null;
					else entity.FnPuaquisicao = Convert.ToDouble(value);
				}
			}
				
			public System.String FnTxoper
			{
				get
				{
					System.Double? data = entity.FnTxoper;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnTxoper = null;
					else entity.FnTxoper = Convert.ToDouble(value);
				}
			}
				
			public System.String FnIr
			{
				get
				{
					System.Double? data = entity.FnIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnIr = null;
					else entity.FnIr = Convert.ToDouble(value);
				}
			}
				
			public System.String FnIof
			{
				get
				{
					System.Double? data = entity.FnIof;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnIof = null;
					else entity.FnIof = Convert.ToDouble(value);
				}
			}
				
			public System.String FnIofcomp
			{
				get
				{
					System.Double? data = entity.FnIofcomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnIofcomp = null;
					else entity.FnIofcomp = Convert.ToDouble(value);
				}
			}
				
			public System.String FnTxcomp
			{
				get
				{
					System.Double? data = entity.FnTxcomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnTxcomp = null;
					else entity.FnTxcomp = Convert.ToDouble(value);
				}
			}
				
			public System.String FnIrcomp
			{
				get
				{
					System.Double? data = entity.FnIrcomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnIrcomp = null;
					else entity.FnIrcomp = Convert.ToDouble(value);
				}
			}
				
			public System.String FnQtdeoper
			{
				get
				{
					System.Double? data = entity.FnQtdeoper;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnQtdeoper = null;
					else entity.FnQtdeoper = Convert.ToDouble(value);
				}
			}
				
			public System.String FnPuoper
			{
				get
				{
					System.Double? data = entity.FnPuoper;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPuoper = null;
					else entity.FnPuoper = Convert.ToDouble(value);
				}
			}
				
			public System.String FnValorbruto
			{
				get
				{
					System.Double? data = entity.FnValorbruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnValorbruto = null;
					else entity.FnValorbruto = Convert.ToDouble(value);
				}
			}
				
			public System.String FnPucomp
			{
				get
				{
					System.Double? data = entity.FnPucomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPucomp = null;
					else entity.FnPucomp = Convert.ToDouble(value);
				}
			}
				
			public System.String FnValorcomp
			{
				get
				{
					System.Double? data = entity.FnValorcomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnValorcomp = null;
					else entity.FnValorcomp = Convert.ToDouble(value);
				}
			}
				
			public System.String FdDatcomp
			{
				get
				{
					System.DateTime? data = entity.FdDatcomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatcomp = null;
					else entity.FdDatcomp = Convert.ToDateTime(value);
				}
			}
				
			public System.String FcComando
			{
				get
				{
					System.String data = entity.FcComando;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcComando = null;
					else entity.FcComando = Convert.ToString(value);
				}
			}
				
			public System.String FcIndiceger
			{
				get
				{
					System.String data = entity.FcIndiceger;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcIndiceger = null;
					else entity.FcIndiceger = Convert.ToString(value);
				}
			}
				
			public System.String FnPercentger
			{
				get
				{
					System.Double? data = entity.FnPercentger;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPercentger = null;
					else entity.FnPercentger = Convert.ToDouble(value);
				}
			}
				
			public System.String FnTxger
			{
				get
				{
					System.Double? data = entity.FnTxger;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnTxger = null;
					else entity.FnTxger = Convert.ToDouble(value);
				}
			}
				
			public System.String FcRemufaixa
			{
				get
				{
					System.String data = entity.FcRemufaixa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcRemufaixa = null;
					else entity.FcRemufaixa = Convert.ToString(value);
				}
			}
				
			public System.String FcControle
			{
				get
				{
					System.String data = entity.FcControle;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcControle = null;
					else entity.FcControle = Convert.ToString(value);
				}
			}
				
			public System.String FcTipoestoque
			{
				get
				{
					System.String data = entity.FcTipoestoque;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTipoestoque = null;
					else entity.FcTipoestoque = Convert.ToString(value);
				}
			}
				
			public System.String FcGrupa
			{
				get
				{
					System.String data = entity.FcGrupa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcGrupa = null;
					else entity.FcGrupa = Convert.ToString(value);
				}
			}
				
			public System.String FdDatlastro
			{
				get
				{
					System.DateTime? data = entity.FdDatlastro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatlastro = null;
					else entity.FdDatlastro = Convert.ToDateTime(value);
				}
			}
				
			public System.String FnBollastro
			{
				get
				{
					System.Double? data = entity.FnBollastro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnBollastro = null;
					else entity.FnBollastro = Convert.ToDouble(value);
				}
			}
				
			public System.String FnPulastro
			{
				get
				{
					System.Double? data = entity.FnPulastro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPulastro = null;
					else entity.FnPulastro = Convert.ToDouble(value);
				}
			}
				
			public System.String FcStatus
			{
				get
				{
					System.String data = entity.FcStatus;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcStatus = null;
					else entity.FcStatus = Convert.ToString(value);
				}
			}
				
			public System.String FdHoraenvio
			{
				get
				{
					System.DateTime? data = entity.FdHoraenvio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdHoraenvio = null;
					else entity.FdHoraenvio = Convert.ToDateTime(value);
				}
			}
				
			public System.String FdAgendamento
			{
				get
				{
					System.DateTime? data = entity.FdAgendamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdAgendamento = null;
					else entity.FdAgendamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String FdDatliquidacao
			{
				get
				{
					System.DateTime? data = entity.FdDatliquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatliquidacao = null;
					else entity.FdDatliquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String FcCodmensagem
			{
				get
				{
					System.String data = entity.FcCodmensagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodmensagem = null;
					else entity.FcCodmensagem = Convert.ToString(value);
				}
			}
				
			public System.String FcTipolei
			{
				get
				{
					System.String data = entity.FcTipolei;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTipolei = null;
					else entity.FcTipolei = Convert.ToString(value);
				}
			}
				
			public System.String FcControleif
			{
				get
				{
					System.String data = entity.FcControleif;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcControleif = null;
					else entity.FcControleif = Convert.ToString(value);
				}
			}
				
			public System.String FcNivelpref
			{
				get
				{
					System.String data = entity.FcNivelpref;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNivelpref = null;
					else entity.FcNivelpref = Convert.ToString(value);
				}
			}
				
			public System.String FcChaveassoc
			{
				get
				{
					System.String data = entity.FcChaveassoc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcChaveassoc = null;
					else entity.FcChaveassoc = Convert.ToString(value);
				}
			}
				
			public System.String FcNumopsel
			{
				get
				{
					System.String data = entity.FcNumopsel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNumopsel = null;
					else entity.FcNumopsel = Convert.ToString(value);
				}
			}
				
			public System.String FcNumopselret
			{
				get
				{
					System.String data = entity.FcNumopselret;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNumopselret = null;
					else entity.FcNumopselret = Convert.ToString(value);
				}
			}
				
			public System.String FcTpleilao
			{
				get
				{
					System.String data = entity.FcTpleilao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTpleilao = null;
					else entity.FcTpleilao = Convert.ToString(value);
				}
			}
				
			public System.String FdTrandata
			{
				get
				{
					System.DateTime? data = entity.FdTrandata;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdTrandata = null;
					else entity.FdTrandata = Convert.ToDateTime(value);
				}
			}
				
			public System.String FcNsu
			{
				get
				{
					System.String data = entity.FcNsu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNsu = null;
					else entity.FcNsu = Convert.ToString(value);
				}
			}
				
			public System.String FcRdcret
			{
				get
				{
					System.String data = entity.FcRdcret;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcRdcret = null;
					else entity.FcRdcret = Convert.ToString(value);
				}
			}
				
			public System.String FcDestino
			{
				get
				{
					System.String data = entity.FcDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcDestino = null;
					else entity.FcDestino = Convert.ToString(value);
				}
			}
				
			public System.String FcObs
			{
				get
				{
					System.String data = entity.FcObs;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcObs = null;
					else entity.FcObs = Convert.ToString(value);
				}
			}
				
			public System.String FnCodbolPre
			{
				get
				{
					System.Double? data = entity.FnCodbolPre;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnCodbolPre = null;
					else entity.FnCodbolPre = Convert.ToDouble(value);
				}
			}
				
			public System.String FlBrokerces
			{
				get
				{
					System.Int32? data = entity.FlBrokerces;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlBrokerces = null;
					else entity.FlBrokerces = Convert.ToInt32(value);
				}
			}
				
			public System.String FlBrokerced
			{
				get
				{
					System.Int32? data = entity.FlBrokerced;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlBrokerced = null;
					else entity.FlBrokerced = Convert.ToInt32(value);
				}
			}
				
			public System.String FdDatcomptermo
			{
				get
				{
					System.DateTime? data = entity.FdDatcomptermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatcomptermo = null;
					else entity.FdDatcomptermo = Convert.ToDateTime(value);
				}
			}
				
			public System.String FnBase
			{
				get
				{
					System.Int32? data = entity.FnBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnBase = null;
					else entity.FnBase = Convert.ToInt32(value);
				}
			}
				
			public System.String FdDatcompini
			{
				get
				{
					System.DateTime? data = entity.FdDatcompini;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatcompini = null;
					else entity.FdDatcompini = Convert.ToDateTime(value);
				}
			}
				
			public System.String FcUnilateralidade
			{
				get
				{
					System.String data = entity.FcUnilateralidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcUnilateralidade = null;
					else entity.FcUnilateralidade = Convert.ToString(value);
				}
			}
				
			public System.String FcUsuario
			{
				get
				{
					System.String data = entity.FcUsuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcUsuario = null;
					else entity.FcUsuario = Convert.ToString(value);
				}
			}
				
			public System.String FcOrigem
			{
				get
				{
					System.String data = entity.FcOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcOrigem = null;
					else entity.FcOrigem = Convert.ToString(value);
				}
			}
			

			private esOphisbol entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOphisbolQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOphisbol can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Ophisbol : esOphisbol
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOphisbolQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OphisbolMetadata.Meta();
			}
		}	
		

		public esQueryItem FnCodbol
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnCodbol, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnCodbolreferencia
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnCodbolreferencia, esSystemType.Double);
			}
		} 
		
		public esQueryItem FcMatricula
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcMatricula, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodempresa
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcCodempresa, esSystemType.String);
			}
		} 
		
		public esQueryItem FcAssessor
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcAssessor, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodliquida
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcCodliquida, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTpoper
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcTpoper, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodtitulo
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcCodtitulo, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodmercado
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcCodmercado, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodbroker
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcCodbroker, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodlimite
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcCodlimite, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodbanco
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcCodbanco, esSystemType.String);
			}
		} 
		
		public esQueryItem FcConta
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcConta, esSystemType.String);
			}
		} 
		
		public esQueryItem FcIfcedente
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcIfcedente, esSystemType.String);
			}
		} 
		
		public esQueryItem FcIfcessionario
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcIfcessionario, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodvinculo
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcCodvinculo, esSystemType.String);
			}
		} 
		
		public esQueryItem FdAquisicao
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FdAquisicao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FdDatoper
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FdDatoper, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FdHorabol
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FdHorabol, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FdHoracombinada
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FdHoracombinada, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FnTxnominal
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnTxnominal, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnFir
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnFir, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnPuaquisicao
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnPuaquisicao, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnTxoper
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnTxoper, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnIr
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnIr, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnIof
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnIof, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnIofcomp
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnIofcomp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnTxcomp
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnTxcomp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnIrcomp
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnIrcomp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnQtdeoper
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnQtdeoper, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnPuoper
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnPuoper, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnValorbruto
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnValorbruto, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnPucomp
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnPucomp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnValorcomp
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnValorcomp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FdDatcomp
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FdDatcomp, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FcComando
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcComando, esSystemType.String);
			}
		} 
		
		public esQueryItem FcIndiceger
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcIndiceger, esSystemType.String);
			}
		} 
		
		public esQueryItem FnPercentger
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnPercentger, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnTxger
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnTxger, esSystemType.Double);
			}
		} 
		
		public esQueryItem FcRemufaixa
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcRemufaixa, esSystemType.String);
			}
		} 
		
		public esQueryItem FcControle
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcControle, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTipoestoque
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcTipoestoque, esSystemType.String);
			}
		} 
		
		public esQueryItem FcGrupa
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcGrupa, esSystemType.String);
			}
		} 
		
		public esQueryItem FdDatlastro
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FdDatlastro, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FnBollastro
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnBollastro, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnPulastro
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnPulastro, esSystemType.Double);
			}
		} 
		
		public esQueryItem FcStatus
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcStatus, esSystemType.String);
			}
		} 
		
		public esQueryItem FdHoraenvio
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FdHoraenvio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FdAgendamento
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FdAgendamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FdDatliquidacao
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FdDatliquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FcCodmensagem
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcCodmensagem, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTipolei
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcTipolei, esSystemType.String);
			}
		} 
		
		public esQueryItem FcControleif
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcControleif, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNivelpref
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcNivelpref, esSystemType.String);
			}
		} 
		
		public esQueryItem FcChaveassoc
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcChaveassoc, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNumopsel
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcNumopsel, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNumopselret
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcNumopselret, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTpleilao
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcTpleilao, esSystemType.String);
			}
		} 
		
		public esQueryItem FdTrandata
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FdTrandata, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FcNsu
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcNsu, esSystemType.String);
			}
		} 
		
		public esQueryItem FcRdcret
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcRdcret, esSystemType.String);
			}
		} 
		
		public esQueryItem FcDestino
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcDestino, esSystemType.String);
			}
		} 
		
		public esQueryItem FcObs
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcObs, esSystemType.String);
			}
		} 
		
		public esQueryItem FnCodbolPre
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnCodbolPre, esSystemType.Double);
			}
		} 
		
		public esQueryItem FlBrokerces
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FlBrokerces, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FlBrokerced
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FlBrokerced, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FdDatcomptermo
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FdDatcomptermo, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FnBase
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FnBase, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FdDatcompini
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FdDatcompini, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FcUnilateralidade
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcUnilateralidade, esSystemType.String);
			}
		} 
		
		public esQueryItem FcUsuario
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcUsuario, esSystemType.String);
			}
		} 
		
		public esQueryItem FcOrigem
		{
			get
			{
				return new esQueryItem(this, OphisbolMetadata.ColumnNames.FcOrigem, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OphisbolCollection")]
	public partial class OphisbolCollection : esOphisbolCollection, IEnumerable<Ophisbol>
	{
		public OphisbolCollection()
		{

		}
		
		public static implicit operator List<Ophisbol>(OphisbolCollection coll)
		{
			List<Ophisbol> list = new List<Ophisbol>();
			
			foreach (Ophisbol emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OphisbolMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OphisbolQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Ophisbol(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Ophisbol();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OphisbolQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OphisbolQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OphisbolQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Ophisbol AddNew()
		{
			Ophisbol entity = base.AddNewEntity() as Ophisbol;
			
			return entity;
		}

		public Ophisbol FindByPrimaryKey(System.Double fnCodbol, System.DateTime fdTrandata)
		{
			return base.FindByPrimaryKey(fnCodbol, fdTrandata) as Ophisbol;
		}


		#region IEnumerable<Ophisbol> Members

		IEnumerator<Ophisbol> IEnumerable<Ophisbol>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Ophisbol;
			}
		}

		#endregion
		
		private OphisbolQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OPHISBOL' table
	/// </summary>

	[Serializable]
	public partial class Ophisbol : esOphisbol
	{
		public Ophisbol()
		{

		}
	
		public Ophisbol(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OphisbolMetadata.Meta();
			}
		}
		
		
		
		override protected esOphisbolQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OphisbolQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OphisbolQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OphisbolQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OphisbolQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OphisbolQuery query;
	}



	[Serializable]
	public partial class OphisbolQuery : esOphisbolQuery
	{
		public OphisbolQuery()
		{

		}		
		
		public OphisbolQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OphisbolMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OphisbolMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnCodbol, 0, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnCodbol;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 15;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnCodbolreferencia, 1, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnCodbolreferencia;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcMatricula, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcMatricula;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcCodempresa, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcCodempresa;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcAssessor, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcAssessor;
			c.CharacterMaxLength = 7;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcCodliquida, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcCodliquida;
			c.CharacterMaxLength = 7;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcTpoper, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcTpoper;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcCodtitulo, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcCodtitulo;
			c.CharacterMaxLength = 6;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcCodmercado, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcCodmercado;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcCodbroker, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcCodbroker;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcCodlimite, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcCodlimite;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcCodbanco, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcCodbanco;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcConta, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcConta;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcIfcedente, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcIfcedente;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcIfcessionario, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcIfcessionario;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcCodvinculo, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcCodvinculo;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FdAquisicao, 16, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisbolMetadata.PropertyNames.FdAquisicao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FdDatoper, 17, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisbolMetadata.PropertyNames.FdDatoper;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FdHorabol, 18, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisbolMetadata.PropertyNames.FdHorabol;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FdHoracombinada, 19, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisbolMetadata.PropertyNames.FdHoracombinada;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnTxnominal, 20, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnTxnominal;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnFir, 21, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnFir;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnPuaquisicao, 22, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnPuaquisicao;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnTxoper, 23, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnTxoper;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnIr, 24, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnIr;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnIof, 25, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnIof;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnIofcomp, 26, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnIofcomp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnTxcomp, 27, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnTxcomp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnIrcomp, 28, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnIrcomp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnQtdeoper, 29, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnQtdeoper;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnPuoper, 30, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnPuoper;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnValorbruto, 31, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnValorbruto;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnPucomp, 32, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnPucomp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnValorcomp, 33, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnValorcomp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FdDatcomp, 34, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisbolMetadata.PropertyNames.FdDatcomp;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcComando, 35, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcComando;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcIndiceger, 36, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcIndiceger;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnPercentger, 37, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnPercentger;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnTxger, 38, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnTxger;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcRemufaixa, 39, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcRemufaixa;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcControle, 40, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcControle;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcTipoestoque, 41, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcTipoestoque;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcGrupa, 42, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcGrupa;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FdDatlastro, 43, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisbolMetadata.PropertyNames.FdDatlastro;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnBollastro, 44, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnBollastro;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnPulastro, 45, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnPulastro;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcStatus, 46, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcStatus;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FdHoraenvio, 47, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisbolMetadata.PropertyNames.FdHoraenvio;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FdAgendamento, 48, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisbolMetadata.PropertyNames.FdAgendamento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FdDatliquidacao, 49, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisbolMetadata.PropertyNames.FdDatliquidacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcCodmensagem, 50, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcCodmensagem;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcTipolei, 51, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcTipolei;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcControleif, 52, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcControleif;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcNivelpref, 53, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcNivelpref;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcChaveassoc, 54, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcChaveassoc;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcNumopsel, 55, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcNumopsel;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcNumopselret, 56, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcNumopselret;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcTpleilao, 57, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcTpleilao;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FdTrandata, 58, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisbolMetadata.PropertyNames.FdTrandata;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcNsu, 59, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcNsu;
			c.CharacterMaxLength = 23;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcRdcret, 60, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcRdcret;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcDestino, 61, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcDestino;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcObs, 62, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcObs;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnCodbolPre, 63, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnCodbolPre;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FlBrokerces, 64, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OphisbolMetadata.PropertyNames.FlBrokerces;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FlBrokerced, 65, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OphisbolMetadata.PropertyNames.FlBrokerced;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FdDatcomptermo, 66, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisbolMetadata.PropertyNames.FdDatcomptermo;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FnBase, 67, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OphisbolMetadata.PropertyNames.FnBase;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FdDatcompini, 68, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisbolMetadata.PropertyNames.FdDatcompini;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcUnilateralidade, 69, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcUnilateralidade;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcUsuario, 70, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcUsuario;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisbolMetadata.ColumnNames.FcOrigem, 71, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisbolMetadata.PropertyNames.FcOrigem;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OphisbolMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string FnCodbol = "FN_CODBOL";
			 public const string FnCodbolreferencia = "FN_CODBOLREFERENCIA";
			 public const string FcMatricula = "FC_MATRICULA";
			 public const string FcCodempresa = "FC_CODEMPRESA";
			 public const string FcAssessor = "FC_ASSESSOR";
			 public const string FcCodliquida = "FC_CODLIQUIDA";
			 public const string FcTpoper = "FC_TPOPER";
			 public const string FcCodtitulo = "FC_CODTITULO";
			 public const string FcCodmercado = "FC_CODMERCADO";
			 public const string FcCodbroker = "FC_CODBROKER";
			 public const string FcCodlimite = "FC_CODLIMITE";
			 public const string FcCodbanco = "FC_CODBANCO";
			 public const string FcConta = "FC_CONTA";
			 public const string FcIfcedente = "FC_IFCEDENTE";
			 public const string FcIfcessionario = "FC_IFCESSIONARIO";
			 public const string FcCodvinculo = "FC_CODVINCULO";
			 public const string FdAquisicao = "FD_AQUISICAO";
			 public const string FdDatoper = "FD_DATOPER";
			 public const string FdHorabol = "FD_HORABOL";
			 public const string FdHoracombinada = "FD_HORACOMBINADA";
			 public const string FnTxnominal = "FN_TXNOMINAL";
			 public const string FnFir = "FN_FIR";
			 public const string FnPuaquisicao = "FN_PUAQUISICAO";
			 public const string FnTxoper = "FN_TXOPER";
			 public const string FnIr = "FN_IR";
			 public const string FnIof = "FN_IOF";
			 public const string FnIofcomp = "FN_IOFCOMP";
			 public const string FnTxcomp = "FN_TXCOMP";
			 public const string FnIrcomp = "FN_IRCOMP";
			 public const string FnQtdeoper = "FN_QTDEOPER";
			 public const string FnPuoper = "FN_PUOPER";
			 public const string FnValorbruto = "FN_VALORBRUTO";
			 public const string FnPucomp = "FN_PUCOMP";
			 public const string FnValorcomp = "FN_VALORCOMP";
			 public const string FdDatcomp = "FD_DATCOMP";
			 public const string FcComando = "FC_COMANDO";
			 public const string FcIndiceger = "FC_INDICEGER";
			 public const string FnPercentger = "FN_PERCENTGER";
			 public const string FnTxger = "FN_TXGER";
			 public const string FcRemufaixa = "FC_REMUFAIXA";
			 public const string FcControle = "FC_CONTROLE";
			 public const string FcTipoestoque = "FC_TIPOESTOQUE";
			 public const string FcGrupa = "FC_GRUPA";
			 public const string FdDatlastro = "FD_DATLASTRO";
			 public const string FnBollastro = "FN_BOLLASTRO";
			 public const string FnPulastro = "FN_PULASTRO";
			 public const string FcStatus = "FC_STATUS";
			 public const string FdHoraenvio = "FD_HORAENVIO";
			 public const string FdAgendamento = "FD_AGENDAMENTO";
			 public const string FdDatliquidacao = "FD_DATLIQUIDACAO";
			 public const string FcCodmensagem = "FC_CODMENSAGEM";
			 public const string FcTipolei = "FC_TIPOLEI";
			 public const string FcControleif = "FC_CONTROLEIF";
			 public const string FcNivelpref = "FC_NIVELPREF";
			 public const string FcChaveassoc = "FC_CHAVEASSOC";
			 public const string FcNumopsel = "FC_NUMOPSEL";
			 public const string FcNumopselret = "FC_NUMOPSELRET";
			 public const string FcTpleilao = "FC_TPLEILAO";
			 public const string FdTrandata = "FD_TRANDATA";
			 public const string FcNsu = "FC_NSU";
			 public const string FcRdcret = "FC_RDCRET";
			 public const string FcDestino = "FC_DESTINO";
			 public const string FcObs = "FC_OBS";
			 public const string FnCodbolPre = "FN_CODBOL_PRE";
			 public const string FlBrokerces = "FL_BROKERCES";
			 public const string FlBrokerced = "FL_BROKERCED";
			 public const string FdDatcomptermo = "FD_DATCOMPTERMO";
			 public const string FnBase = "FN_BASE";
			 public const string FdDatcompini = "FD_DATCOMPINI";
			 public const string FcUnilateralidade = "FC_UNILATERALIDADE";
			 public const string FcUsuario = "FC_USUARIO";
			 public const string FcOrigem = "FC_ORIGEM";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string FnCodbol = "FnCodbol";
			 public const string FnCodbolreferencia = "FnCodbolreferencia";
			 public const string FcMatricula = "FcMatricula";
			 public const string FcCodempresa = "FcCodempresa";
			 public const string FcAssessor = "FcAssessor";
			 public const string FcCodliquida = "FcCodliquida";
			 public const string FcTpoper = "FcTpoper";
			 public const string FcCodtitulo = "FcCodtitulo";
			 public const string FcCodmercado = "FcCodmercado";
			 public const string FcCodbroker = "FcCodbroker";
			 public const string FcCodlimite = "FcCodlimite";
			 public const string FcCodbanco = "FcCodbanco";
			 public const string FcConta = "FcConta";
			 public const string FcIfcedente = "FcIfcedente";
			 public const string FcIfcessionario = "FcIfcessionario";
			 public const string FcCodvinculo = "FcCodvinculo";
			 public const string FdAquisicao = "FdAquisicao";
			 public const string FdDatoper = "FdDatoper";
			 public const string FdHorabol = "FdHorabol";
			 public const string FdHoracombinada = "FdHoracombinada";
			 public const string FnTxnominal = "FnTxnominal";
			 public const string FnFir = "FnFir";
			 public const string FnPuaquisicao = "FnPuaquisicao";
			 public const string FnTxoper = "FnTxoper";
			 public const string FnIr = "FnIr";
			 public const string FnIof = "FnIof";
			 public const string FnIofcomp = "FnIofcomp";
			 public const string FnTxcomp = "FnTxcomp";
			 public const string FnIrcomp = "FnIrcomp";
			 public const string FnQtdeoper = "FnQtdeoper";
			 public const string FnPuoper = "FnPuoper";
			 public const string FnValorbruto = "FnValorbruto";
			 public const string FnPucomp = "FnPucomp";
			 public const string FnValorcomp = "FnValorcomp";
			 public const string FdDatcomp = "FdDatcomp";
			 public const string FcComando = "FcComando";
			 public const string FcIndiceger = "FcIndiceger";
			 public const string FnPercentger = "FnPercentger";
			 public const string FnTxger = "FnTxger";
			 public const string FcRemufaixa = "FcRemufaixa";
			 public const string FcControle = "FcControle";
			 public const string FcTipoestoque = "FcTipoestoque";
			 public const string FcGrupa = "FcGrupa";
			 public const string FdDatlastro = "FdDatlastro";
			 public const string FnBollastro = "FnBollastro";
			 public const string FnPulastro = "FnPulastro";
			 public const string FcStatus = "FcStatus";
			 public const string FdHoraenvio = "FdHoraenvio";
			 public const string FdAgendamento = "FdAgendamento";
			 public const string FdDatliquidacao = "FdDatliquidacao";
			 public const string FcCodmensagem = "FcCodmensagem";
			 public const string FcTipolei = "FcTipolei";
			 public const string FcControleif = "FcControleif";
			 public const string FcNivelpref = "FcNivelpref";
			 public const string FcChaveassoc = "FcChaveassoc";
			 public const string FcNumopsel = "FcNumopsel";
			 public const string FcNumopselret = "FcNumopselret";
			 public const string FcTpleilao = "FcTpleilao";
			 public const string FdTrandata = "FdTrandata";
			 public const string FcNsu = "FcNsu";
			 public const string FcRdcret = "FcRdcret";
			 public const string FcDestino = "FcDestino";
			 public const string FcObs = "FcObs";
			 public const string FnCodbolPre = "FnCodbolPre";
			 public const string FlBrokerces = "FlBrokerces";
			 public const string FlBrokerced = "FlBrokerced";
			 public const string FdDatcomptermo = "FdDatcomptermo";
			 public const string FnBase = "FnBase";
			 public const string FdDatcompini = "FdDatcompini";
			 public const string FcUnilateralidade = "FcUnilateralidade";
			 public const string FcUsuario = "FcUsuario";
			 public const string FcOrigem = "FcOrigem";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OphisbolMetadata))
			{
				if(OphisbolMetadata.mapDelegates == null)
				{
					OphisbolMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OphisbolMetadata.meta == null)
				{
					OphisbolMetadata.meta = new OphisbolMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("FN_CODBOL", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_CODBOLREFERENCIA", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FC_MATRICULA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODEMPRESA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ASSESSOR", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODLIQUIDA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TPOPER", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODTITULO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODMERCADO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODBROKER", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODLIMITE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODBANCO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CONTA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_IFCEDENTE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_IFCESSIONARIO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODVINCULO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FD_AQUISICAO", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FD_DATOPER", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FD_HORABOL", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FD_HORACOMBINADA", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FN_TXNOMINAL", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_FIR", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_PUAQUISICAO", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_TXOPER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_IR", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_IOF", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_IOFCOMP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_TXCOMP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_IRCOMP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_QTDEOPER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_PUOPER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_VALORBRUTO", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_PUCOMP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_VALORCOMP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FD_DATCOMP", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FC_COMANDO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_INDICEGER", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FN_PERCENTGER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_TXGER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FC_REMUFAIXA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CONTROLE", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FC_TIPOESTOQUE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_GRUPA", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FD_DATLASTRO", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FN_BOLLASTRO", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_PULASTRO", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FC_STATUS", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FD_HORAENVIO", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FD_AGENDAMENTO", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FD_DATLIQUIDACAO", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FC_CODMENSAGEM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TIPOLEI", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CONTROLEIF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NIVELPREF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CHAVEASSOC", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NUMOPSEL", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NUMOPSELRET", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TPLEILAO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FD_TRANDATA", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FC_NSU", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_RDCRET", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_DESTINO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_OBS", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FN_CODBOL_PRE", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FL_BROKERCES", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FL_BROKERCED", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FD_DATCOMPTERMO", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FN_BASE", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FD_DATCOMPINI", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FC_UNILATERALIDADE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_USUARIO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ORIGEM", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "OPHISBOL";
				meta.Destination = "OPHISBOL";
				
				meta.spInsert = "proc_OPHISBOLInsert";				
				meta.spUpdate = "proc_OPHISBOLUpdate";		
				meta.spDelete = "proc_OPHISBOLDelete";
				meta.spLoadAll = "proc_OPHISBOLLoadAll";
				meta.spLoadByPrimaryKey = "proc_OPHISBOLLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OphisbolMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
