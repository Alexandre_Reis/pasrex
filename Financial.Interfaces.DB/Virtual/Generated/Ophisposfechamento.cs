/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/12/2013 17:28:41
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;











































































































































































































namespace Financial.InterfacesDB
{

	[Serializable]
	abstract public class esOphisposfechamentoCollection : esEntityCollection
	{
		public esOphisposfechamentoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OphisposfechamentoCollection";
		}

		#region Query Logic
		protected void InitQuery(esOphisposfechamentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOphisposfechamentoQuery);
		}
		#endregion
		
		virtual public Ophisposfechamento DetachEntity(Ophisposfechamento entity)
		{
			return base.DetachEntity(entity) as Ophisposfechamento;
		}
		
		virtual public Ophisposfechamento AttachEntity(Ophisposfechamento entity)
		{
			return base.AttachEntity(entity) as Ophisposfechamento;
		}
		
		virtual public void Combine(OphisposfechamentoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Ophisposfechamento this[int index]
		{
			get
			{
				return base[index] as Ophisposfechamento;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Ophisposfechamento);
		}
	}



	[Serializable]
	abstract public class esOphisposfechamento : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOphisposfechamentoQuery GetDynamicQuery()
		{
			return null;
		}

		public esOphisposfechamento()
		{

		}

		public esOphisposfechamento(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Double fnCodbol, System.DateTime fdTrandata)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(fnCodbol, fdTrandata);
			else
				return LoadByPrimaryKeyStoredProcedure(fnCodbol, fdTrandata);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Double fnCodbol, System.DateTime fdTrandata)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esOphisposfechamentoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.FnCodbol == fnCodbol, query.FdTrandata == fdTrandata);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Double fnCodbol, System.DateTime fdTrandata)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(fnCodbol, fdTrandata);
			else
				return LoadByPrimaryKeyStoredProcedure(fnCodbol, fdTrandata);
		}

		private bool LoadByPrimaryKeyDynamic(System.Double fnCodbol, System.DateTime fdTrandata)
		{
			esOphisposfechamentoQuery query = this.GetDynamicQuery();
			query.Where(query.FnCodbol == fnCodbol, query.FdTrandata == fdTrandata);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Double fnCodbol, System.DateTime fdTrandata)
		{
			esParameters parms = new esParameters();
			parms.Add("FN_CODBOL",fnCodbol);			parms.Add("FD_TRANDATA",fdTrandata);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "FnCodbol": this.str.FnCodbol = (string)value; break;							
						case "FcMatricula": this.str.FcMatricula = (string)value; break;							
						case "FcCodempresa": this.str.FcCodempresa = (string)value; break;							
						case "FcAssessor": this.str.FcAssessor = (string)value; break;							
						case "FcTpoper": this.str.FcTpoper = (string)value; break;							
						case "FcCodliquida": this.str.FcCodliquida = (string)value; break;							
						case "FcCodmercado": this.str.FcCodmercado = (string)value; break;							
						case "FcCodbroker": this.str.FcCodbroker = (string)value; break;							
						case "FcCodlimite": this.str.FcCodlimite = (string)value; break;							
						case "FcCodtitulo": this.str.FcCodtitulo = (string)value; break;							
						case "FdAquisicao": this.str.FdAquisicao = (string)value; break;							
						case "FdDatoper": this.str.FdDatoper = (string)value; break;							
						case "FdTrandata": this.str.FdTrandata = (string)value; break;							
						case "FnTxnominal": this.str.FnTxnominal = (string)value; break;							
						case "FnFir": this.str.FnFir = (string)value; break;							
						case "FnPrazoDc": this.str.FnPrazoDc = (string)value; break;							
						case "FnPrazoDu": this.str.FnPrazoDu = (string)value; break;							
						case "FnPuaquisicao": this.str.FnPuaquisicao = (string)value; break;							
						case "FnTxoper": this.str.FnTxoper = (string)value; break;							
						case "FnIr": this.str.FnIr = (string)value; break;							
						case "FnIof": this.str.FnIof = (string)value; break;							
						case "FnIofcomp": this.str.FnIofcomp = (string)value; break;							
						case "FnTxcomp": this.str.FnTxcomp = (string)value; break;							
						case "FnIrcomp": this.str.FnIrcomp = (string)value; break;							
						case "FnQtdeoper": this.str.FnQtdeoper = (string)value; break;							
						case "FnPuoper": this.str.FnPuoper = (string)value; break;							
						case "FnValorbruto": this.str.FnValorbruto = (string)value; break;							
						case "FnPucomp": this.str.FnPucomp = (string)value; break;							
						case "FnValorcomp": this.str.FnValorcomp = (string)value; break;							
						case "FdDatcomp": this.str.FdDatcomp = (string)value; break;							
						case "FcComando": this.str.FcComando = (string)value; break;							
						case "FcCodbanco": this.str.FcCodbanco = (string)value; break;							
						case "FcConta": this.str.FcConta = (string)value; break;							
						case "FcCodvinculo": this.str.FcCodvinculo = (string)value; break;							
						case "FcIndiceger": this.str.FcIndiceger = (string)value; break;							
						case "FnPercentger": this.str.FnPercentger = (string)value; break;							
						case "FnTxger": this.str.FnTxger = (string)value; break;							
						case "FcRemufaixa": this.str.FcRemufaixa = (string)value; break;							
						case "FcControle": this.str.FcControle = (string)value; break;							
						case "FnQtdebancado": this.str.FnQtdebancado = (string)value; break;							
						case "FnQtdevinculado": this.str.FnQtdevinculado = (string)value; break;							
						case "FnPucurva": this.str.FnPucurva = (string)value; break;							
						case "FnPu550551": this.str.FnPu550551 = (string)value; break;							
						case "FnFincurva": this.str.FnFincurva = (string)value; break;							
						case "FnFin550551": this.str.FnFin550551 = (string)value; break;							
						case "FcTipoestoque": this.str.FcTipoestoque = (string)value; break;							
						case "FcInstituicao": this.str.FcInstituicao = (string)value; break;							
						case "FnAjuste": this.str.FnAjuste = (string)value; break;							
						case "FcGrupa": this.str.FcGrupa = (string)value; break;							
						case "FnPuger": this.str.FnPuger = (string)value; break;							
						case "FnFinger": this.str.FnFinger = (string)value; break;							
						case "FnIofger": this.str.FnIofger = (string)value; break;							
						case "FnIrger": this.str.FnIrger = (string)value; break;							
						case "FnQtderedesconto": this.str.FnQtderedesconto = (string)value; break;							
						case "FnCodbolPos": this.str.FnCodbolPos = (string)value; break;							
						case "FdDtrepact": this.str.FdDtrepact = (string)value; break;							
						case "FcOrigem": this.str.FcOrigem = (string)value; break;							
						case "FdDatapuimp": this.str.FdDatapuimp = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "FnCodbol":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnCodbol = (System.Double?)value;
							break;
						
						case "FdAquisicao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdAquisicao = (System.DateTime?)value;
							break;
						
						case "FdDatoper":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatoper = (System.DateTime?)value;
							break;
						
						case "FdTrandata":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdTrandata = (System.DateTime?)value;
							break;
						
						case "FnTxnominal":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnTxnominal = (System.Double?)value;
							break;
						
						case "FnFir":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnFir = (System.Double?)value;
							break;
						
						case "FnPrazoDc":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPrazoDc = (System.Double?)value;
							break;
						
						case "FnPrazoDu":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPrazoDu = (System.Double?)value;
							break;
						
						case "FnPuaquisicao":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPuaquisicao = (System.Double?)value;
							break;
						
						case "FnTxoper":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnTxoper = (System.Double?)value;
							break;
						
						case "FnIr":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnIr = (System.Double?)value;
							break;
						
						case "FnIof":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnIof = (System.Double?)value;
							break;
						
						case "FnIofcomp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnIofcomp = (System.Double?)value;
							break;
						
						case "FnTxcomp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnTxcomp = (System.Double?)value;
							break;
						
						case "FnIrcomp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnIrcomp = (System.Double?)value;
							break;
						
						case "FnQtdeoper":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnQtdeoper = (System.Double?)value;
							break;
						
						case "FnPuoper":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPuoper = (System.Double?)value;
							break;
						
						case "FnValorbruto":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnValorbruto = (System.Double?)value;
							break;
						
						case "FnPucomp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPucomp = (System.Double?)value;
							break;
						
						case "FnValorcomp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnValorcomp = (System.Double?)value;
							break;
						
						case "FdDatcomp":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatcomp = (System.DateTime?)value;
							break;
						
						case "FnPercentger":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPercentger = (System.Double?)value;
							break;
						
						case "FnTxger":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnTxger = (System.Double?)value;
							break;
						
						case "FnQtdebancado":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnQtdebancado = (System.Double?)value;
							break;
						
						case "FnQtdevinculado":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnQtdevinculado = (System.Double?)value;
							break;
						
						case "FnPucurva":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPucurva = (System.Double?)value;
							break;
						
						case "FnPu550551":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPu550551 = (System.Double?)value;
							break;
						
						case "FnFincurva":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnFincurva = (System.Double?)value;
							break;
						
						case "FnFin550551":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnFin550551 = (System.Double?)value;
							break;
						
						case "FnAjuste":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnAjuste = (System.Double?)value;
							break;
						
						case "FnPuger":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPuger = (System.Double?)value;
							break;
						
						case "FnFinger":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnFinger = (System.Double?)value;
							break;
						
						case "FnIofger":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnIofger = (System.Double?)value;
							break;
						
						case "FnIrger":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnIrger = (System.Double?)value;
							break;
						
						case "FnQtderedesconto":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnQtderedesconto = (System.Double?)value;
							break;
						
						case "FnCodbolPos":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnCodbolPos = (System.Double?)value;
							break;
						
						case "FdDtrepact":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDtrepact = (System.DateTime?)value;
							break;
						
						case "FdDatapuimp":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatapuimp = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_CODBOL
		/// </summary>
		virtual public System.Double? FnCodbol
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnCodbol);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnCodbol, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_MATRICULA
		/// </summary>
		virtual public System.String FcMatricula
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcMatricula);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcMatricula, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_CODEMPRESA
		/// </summary>
		virtual public System.String FcCodempresa
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodempresa);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodempresa, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_ASSESSOR
		/// </summary>
		virtual public System.String FcAssessor
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcAssessor);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcAssessor, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_TPOPER
		/// </summary>
		virtual public System.String FcTpoper
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcTpoper);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcTpoper, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_CODLIQUIDA
		/// </summary>
		virtual public System.String FcCodliquida
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodliquida);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodliquida, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_CODMERCADO
		/// </summary>
		virtual public System.String FcCodmercado
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodmercado);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodmercado, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_CODBROKER
		/// </summary>
		virtual public System.String FcCodbroker
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodbroker);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodbroker, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_CODLIMITE
		/// </summary>
		virtual public System.String FcCodlimite
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodlimite);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodlimite, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_CODTITULO
		/// </summary>
		virtual public System.String FcCodtitulo
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodtitulo);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodtitulo, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FD_AQUISICAO
		/// </summary>
		virtual public System.DateTime? FdAquisicao
		{
			get
			{
				return base.GetSystemDateTime(OphisposfechamentoMetadata.ColumnNames.FdAquisicao);
			}
			
			set
			{
				base.SetSystemDateTime(OphisposfechamentoMetadata.ColumnNames.FdAquisicao, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FD_DATOPER
		/// </summary>
		virtual public System.DateTime? FdDatoper
		{
			get
			{
				return base.GetSystemDateTime(OphisposfechamentoMetadata.ColumnNames.FdDatoper);
			}
			
			set
			{
				base.SetSystemDateTime(OphisposfechamentoMetadata.ColumnNames.FdDatoper, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FD_TRANDATA
		/// </summary>
		virtual public System.DateTime? FdTrandata
		{
			get
			{
				return base.GetSystemDateTime(OphisposfechamentoMetadata.ColumnNames.FdTrandata);
			}
			
			set
			{
				base.SetSystemDateTime(OphisposfechamentoMetadata.ColumnNames.FdTrandata, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_TXNOMINAL
		/// </summary>
		virtual public System.Double? FnTxnominal
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnTxnominal);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnTxnominal, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_FIR
		/// </summary>
		virtual public System.Double? FnFir
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnFir);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnFir, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_PRAZO_DC
		/// </summary>
		virtual public System.Double? FnPrazoDc
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPrazoDc);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPrazoDc, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_PRAZO_DU
		/// </summary>
		virtual public System.Double? FnPrazoDu
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPrazoDu);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPrazoDu, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_PUAQUISICAO
		/// </summary>
		virtual public System.Double? FnPuaquisicao
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPuaquisicao);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPuaquisicao, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_TXOPER
		/// </summary>
		virtual public System.Double? FnTxoper
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnTxoper);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnTxoper, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_IR
		/// </summary>
		virtual public System.Double? FnIr
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnIr);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnIr, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_IOF
		/// </summary>
		virtual public System.Double? FnIof
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnIof);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnIof, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_IOFCOMP
		/// </summary>
		virtual public System.Double? FnIofcomp
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnIofcomp);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnIofcomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_TXCOMP
		/// </summary>
		virtual public System.Double? FnTxcomp
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnTxcomp);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnTxcomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_IRCOMP
		/// </summary>
		virtual public System.Double? FnIrcomp
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnIrcomp);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnIrcomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_QTDEOPER
		/// </summary>
		virtual public System.Double? FnQtdeoper
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnQtdeoper);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnQtdeoper, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_PUOPER
		/// </summary>
		virtual public System.Double? FnPuoper
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPuoper);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPuoper, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_VALORBRUTO
		/// </summary>
		virtual public System.Double? FnValorbruto
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnValorbruto);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnValorbruto, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_PUCOMP
		/// </summary>
		virtual public System.Double? FnPucomp
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPucomp);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPucomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_VALORCOMP
		/// </summary>
		virtual public System.Double? FnValorcomp
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnValorcomp);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnValorcomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FD_DATCOMP
		/// </summary>
		virtual public System.DateTime? FdDatcomp
		{
			get
			{
				return base.GetSystemDateTime(OphisposfechamentoMetadata.ColumnNames.FdDatcomp);
			}
			
			set
			{
				base.SetSystemDateTime(OphisposfechamentoMetadata.ColumnNames.FdDatcomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_COMANDO
		/// </summary>
		virtual public System.String FcComando
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcComando);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcComando, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_CODBANCO
		/// </summary>
		virtual public System.String FcCodbanco
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodbanco);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodbanco, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_CONTA
		/// </summary>
		virtual public System.String FcConta
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcConta);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcConta, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_CODVINCULO
		/// </summary>
		virtual public System.String FcCodvinculo
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodvinculo);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcCodvinculo, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_INDICEGER
		/// </summary>
		virtual public System.String FcIndiceger
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcIndiceger);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcIndiceger, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_PERCENTGER
		/// </summary>
		virtual public System.Double? FnPercentger
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPercentger);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPercentger, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_TXGER
		/// </summary>
		virtual public System.Double? FnTxger
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnTxger);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnTxger, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_REMUFAIXA
		/// </summary>
		virtual public System.String FcRemufaixa
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcRemufaixa);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcRemufaixa, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_CONTROLE
		/// </summary>
		virtual public System.String FcControle
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcControle);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcControle, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_QTDEBANCADO
		/// </summary>
		virtual public System.Double? FnQtdebancado
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnQtdebancado);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnQtdebancado, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_QTDEVINCULADO
		/// </summary>
		virtual public System.Double? FnQtdevinculado
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnQtdevinculado);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnQtdevinculado, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_PUCURVA
		/// </summary>
		virtual public System.Double? FnPucurva
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPucurva);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPucurva, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_PU550551
		/// </summary>
		virtual public System.Double? FnPu550551
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPu550551);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPu550551, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_FINCURVA
		/// </summary>
		virtual public System.Double? FnFincurva
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnFincurva);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnFincurva, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_FIN550551
		/// </summary>
		virtual public System.Double? FnFin550551
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnFin550551);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnFin550551, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_TIPOESTOQUE
		/// </summary>
		virtual public System.String FcTipoestoque
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcTipoestoque);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcTipoestoque, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_INSTITUICAO
		/// </summary>
		virtual public System.String FcInstituicao
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcInstituicao);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcInstituicao, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_AJUSTE
		/// </summary>
		virtual public System.Double? FnAjuste
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnAjuste);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnAjuste, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_GRUPA
		/// </summary>
		virtual public System.String FcGrupa
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcGrupa);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcGrupa, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_PUGER
		/// </summary>
		virtual public System.Double? FnPuger
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPuger);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnPuger, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_FINGER
		/// </summary>
		virtual public System.Double? FnFinger
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnFinger);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnFinger, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_IOFGER
		/// </summary>
		virtual public System.Double? FnIofger
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnIofger);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnIofger, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_IRGER
		/// </summary>
		virtual public System.Double? FnIrger
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnIrger);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnIrger, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_QTDEREDESCONTO
		/// </summary>
		virtual public System.Double? FnQtderedesconto
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnQtderedesconto);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnQtderedesconto, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FN_CODBOL_POS
		/// </summary>
		virtual public System.Double? FnCodbolPos
		{
			get
			{
				return base.GetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnCodbolPos);
			}
			
			set
			{
				base.SetSystemDouble(OphisposfechamentoMetadata.ColumnNames.FnCodbolPos, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FD_DTREPACT
		/// </summary>
		virtual public System.DateTime? FdDtrepact
		{
			get
			{
				return base.GetSystemDateTime(OphisposfechamentoMetadata.ColumnNames.FdDtrepact);
			}
			
			set
			{
				base.SetSystemDateTime(OphisposfechamentoMetadata.ColumnNames.FdDtrepact, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FC_ORIGEM
		/// </summary>
		virtual public System.String FcOrigem
		{
			get
			{
				return base.GetSystemString(OphisposfechamentoMetadata.ColumnNames.FcOrigem);
			}
			
			set
			{
				base.SetSystemString(OphisposfechamentoMetadata.ColumnNames.FcOrigem, value);
			}
		}
		
		/// <summary>
		/// Maps to OPHISPOSFECHAMENTO.FD_DATAPUIMP
		/// </summary>
		virtual public System.DateTime? FdDatapuimp
		{
			get
			{
				return base.GetSystemDateTime(OphisposfechamentoMetadata.ColumnNames.FdDatapuimp);
			}
			
			set
			{
				base.SetSystemDateTime(OphisposfechamentoMetadata.ColumnNames.FdDatapuimp, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOphisposfechamento entity)
			{
				this.entity = entity;
			}
			
	
			public System.String FnCodbol
			{
				get
				{
					System.Double? data = entity.FnCodbol;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnCodbol = null;
					else entity.FnCodbol = Convert.ToDouble(value);
				}
			}
				
			public System.String FcMatricula
			{
				get
				{
					System.String data = entity.FcMatricula;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcMatricula = null;
					else entity.FcMatricula = Convert.ToString(value);
				}
			}
				
			public System.String FcCodempresa
			{
				get
				{
					System.String data = entity.FcCodempresa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodempresa = null;
					else entity.FcCodempresa = Convert.ToString(value);
				}
			}
				
			public System.String FcAssessor
			{
				get
				{
					System.String data = entity.FcAssessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcAssessor = null;
					else entity.FcAssessor = Convert.ToString(value);
				}
			}
				
			public System.String FcTpoper
			{
				get
				{
					System.String data = entity.FcTpoper;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTpoper = null;
					else entity.FcTpoper = Convert.ToString(value);
				}
			}
				
			public System.String FcCodliquida
			{
				get
				{
					System.String data = entity.FcCodliquida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodliquida = null;
					else entity.FcCodliquida = Convert.ToString(value);
				}
			}
				
			public System.String FcCodmercado
			{
				get
				{
					System.String data = entity.FcCodmercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodmercado = null;
					else entity.FcCodmercado = Convert.ToString(value);
				}
			}
				
			public System.String FcCodbroker
			{
				get
				{
					System.String data = entity.FcCodbroker;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodbroker = null;
					else entity.FcCodbroker = Convert.ToString(value);
				}
			}
				
			public System.String FcCodlimite
			{
				get
				{
					System.String data = entity.FcCodlimite;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodlimite = null;
					else entity.FcCodlimite = Convert.ToString(value);
				}
			}
				
			public System.String FcCodtitulo
			{
				get
				{
					System.String data = entity.FcCodtitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodtitulo = null;
					else entity.FcCodtitulo = Convert.ToString(value);
				}
			}
				
			public System.String FdAquisicao
			{
				get
				{
					System.DateTime? data = entity.FdAquisicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdAquisicao = null;
					else entity.FdAquisicao = Convert.ToDateTime(value);
				}
			}
				
			public System.String FdDatoper
			{
				get
				{
					System.DateTime? data = entity.FdDatoper;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatoper = null;
					else entity.FdDatoper = Convert.ToDateTime(value);
				}
			}
				
			public System.String FdTrandata
			{
				get
				{
					System.DateTime? data = entity.FdTrandata;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdTrandata = null;
					else entity.FdTrandata = Convert.ToDateTime(value);
				}
			}
				
			public System.String FnTxnominal
			{
				get
				{
					System.Double? data = entity.FnTxnominal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnTxnominal = null;
					else entity.FnTxnominal = Convert.ToDouble(value);
				}
			}
				
			public System.String FnFir
			{
				get
				{
					System.Double? data = entity.FnFir;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnFir = null;
					else entity.FnFir = Convert.ToDouble(value);
				}
			}
				
			public System.String FnPrazoDc
			{
				get
				{
					System.Double? data = entity.FnPrazoDc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPrazoDc = null;
					else entity.FnPrazoDc = Convert.ToDouble(value);
				}
			}
				
			public System.String FnPrazoDu
			{
				get
				{
					System.Double? data = entity.FnPrazoDu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPrazoDu = null;
					else entity.FnPrazoDu = Convert.ToDouble(value);
				}
			}
				
			public System.String FnPuaquisicao
			{
				get
				{
					System.Double? data = entity.FnPuaquisicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPuaquisicao = null;
					else entity.FnPuaquisicao = Convert.ToDouble(value);
				}
			}
				
			public System.String FnTxoper
			{
				get
				{
					System.Double? data = entity.FnTxoper;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnTxoper = null;
					else entity.FnTxoper = Convert.ToDouble(value);
				}
			}
				
			public System.String FnIr
			{
				get
				{
					System.Double? data = entity.FnIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnIr = null;
					else entity.FnIr = Convert.ToDouble(value);
				}
			}
				
			public System.String FnIof
			{
				get
				{
					System.Double? data = entity.FnIof;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnIof = null;
					else entity.FnIof = Convert.ToDouble(value);
				}
			}
				
			public System.String FnIofcomp
			{
				get
				{
					System.Double? data = entity.FnIofcomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnIofcomp = null;
					else entity.FnIofcomp = Convert.ToDouble(value);
				}
			}
				
			public System.String FnTxcomp
			{
				get
				{
					System.Double? data = entity.FnTxcomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnTxcomp = null;
					else entity.FnTxcomp = Convert.ToDouble(value);
				}
			}
				
			public System.String FnIrcomp
			{
				get
				{
					System.Double? data = entity.FnIrcomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnIrcomp = null;
					else entity.FnIrcomp = Convert.ToDouble(value);
				}
			}
				
			public System.String FnQtdeoper
			{
				get
				{
					System.Double? data = entity.FnQtdeoper;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnQtdeoper = null;
					else entity.FnQtdeoper = Convert.ToDouble(value);
				}
			}
				
			public System.String FnPuoper
			{
				get
				{
					System.Double? data = entity.FnPuoper;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPuoper = null;
					else entity.FnPuoper = Convert.ToDouble(value);
				}
			}
				
			public System.String FnValorbruto
			{
				get
				{
					System.Double? data = entity.FnValorbruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnValorbruto = null;
					else entity.FnValorbruto = Convert.ToDouble(value);
				}
			}
				
			public System.String FnPucomp
			{
				get
				{
					System.Double? data = entity.FnPucomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPucomp = null;
					else entity.FnPucomp = Convert.ToDouble(value);
				}
			}
				
			public System.String FnValorcomp
			{
				get
				{
					System.Double? data = entity.FnValorcomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnValorcomp = null;
					else entity.FnValorcomp = Convert.ToDouble(value);
				}
			}
				
			public System.String FdDatcomp
			{
				get
				{
					System.DateTime? data = entity.FdDatcomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatcomp = null;
					else entity.FdDatcomp = Convert.ToDateTime(value);
				}
			}
				
			public System.String FcComando
			{
				get
				{
					System.String data = entity.FcComando;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcComando = null;
					else entity.FcComando = Convert.ToString(value);
				}
			}
				
			public System.String FcCodbanco
			{
				get
				{
					System.String data = entity.FcCodbanco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodbanco = null;
					else entity.FcCodbanco = Convert.ToString(value);
				}
			}
				
			public System.String FcConta
			{
				get
				{
					System.String data = entity.FcConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcConta = null;
					else entity.FcConta = Convert.ToString(value);
				}
			}
				
			public System.String FcCodvinculo
			{
				get
				{
					System.String data = entity.FcCodvinculo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodvinculo = null;
					else entity.FcCodvinculo = Convert.ToString(value);
				}
			}
				
			public System.String FcIndiceger
			{
				get
				{
					System.String data = entity.FcIndiceger;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcIndiceger = null;
					else entity.FcIndiceger = Convert.ToString(value);
				}
			}
				
			public System.String FnPercentger
			{
				get
				{
					System.Double? data = entity.FnPercentger;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPercentger = null;
					else entity.FnPercentger = Convert.ToDouble(value);
				}
			}
				
			public System.String FnTxger
			{
				get
				{
					System.Double? data = entity.FnTxger;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnTxger = null;
					else entity.FnTxger = Convert.ToDouble(value);
				}
			}
				
			public System.String FcRemufaixa
			{
				get
				{
					System.String data = entity.FcRemufaixa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcRemufaixa = null;
					else entity.FcRemufaixa = Convert.ToString(value);
				}
			}
				
			public System.String FcControle
			{
				get
				{
					System.String data = entity.FcControle;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcControle = null;
					else entity.FcControle = Convert.ToString(value);
				}
			}
				
			public System.String FnQtdebancado
			{
				get
				{
					System.Double? data = entity.FnQtdebancado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnQtdebancado = null;
					else entity.FnQtdebancado = Convert.ToDouble(value);
				}
			}
				
			public System.String FnQtdevinculado
			{
				get
				{
					System.Double? data = entity.FnQtdevinculado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnQtdevinculado = null;
					else entity.FnQtdevinculado = Convert.ToDouble(value);
				}
			}
				
			public System.String FnPucurva
			{
				get
				{
					System.Double? data = entity.FnPucurva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPucurva = null;
					else entity.FnPucurva = Convert.ToDouble(value);
				}
			}
				
			public System.String FnPu550551
			{
				get
				{
					System.Double? data = entity.FnPu550551;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPu550551 = null;
					else entity.FnPu550551 = Convert.ToDouble(value);
				}
			}
				
			public System.String FnFincurva
			{
				get
				{
					System.Double? data = entity.FnFincurva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnFincurva = null;
					else entity.FnFincurva = Convert.ToDouble(value);
				}
			}
				
			public System.String FnFin550551
			{
				get
				{
					System.Double? data = entity.FnFin550551;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnFin550551 = null;
					else entity.FnFin550551 = Convert.ToDouble(value);
				}
			}
				
			public System.String FcTipoestoque
			{
				get
				{
					System.String data = entity.FcTipoestoque;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTipoestoque = null;
					else entity.FcTipoestoque = Convert.ToString(value);
				}
			}
				
			public System.String FcInstituicao
			{
				get
				{
					System.String data = entity.FcInstituicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcInstituicao = null;
					else entity.FcInstituicao = Convert.ToString(value);
				}
			}
				
			public System.String FnAjuste
			{
				get
				{
					System.Double? data = entity.FnAjuste;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnAjuste = null;
					else entity.FnAjuste = Convert.ToDouble(value);
				}
			}
				
			public System.String FcGrupa
			{
				get
				{
					System.String data = entity.FcGrupa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcGrupa = null;
					else entity.FcGrupa = Convert.ToString(value);
				}
			}
				
			public System.String FnPuger
			{
				get
				{
					System.Double? data = entity.FnPuger;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPuger = null;
					else entity.FnPuger = Convert.ToDouble(value);
				}
			}
				
			public System.String FnFinger
			{
				get
				{
					System.Double? data = entity.FnFinger;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnFinger = null;
					else entity.FnFinger = Convert.ToDouble(value);
				}
			}
				
			public System.String FnIofger
			{
				get
				{
					System.Double? data = entity.FnIofger;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnIofger = null;
					else entity.FnIofger = Convert.ToDouble(value);
				}
			}
				
			public System.String FnIrger
			{
				get
				{
					System.Double? data = entity.FnIrger;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnIrger = null;
					else entity.FnIrger = Convert.ToDouble(value);
				}
			}
				
			public System.String FnQtderedesconto
			{
				get
				{
					System.Double? data = entity.FnQtderedesconto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnQtderedesconto = null;
					else entity.FnQtderedesconto = Convert.ToDouble(value);
				}
			}
				
			public System.String FnCodbolPos
			{
				get
				{
					System.Double? data = entity.FnCodbolPos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnCodbolPos = null;
					else entity.FnCodbolPos = Convert.ToDouble(value);
				}
			}
				
			public System.String FdDtrepact
			{
				get
				{
					System.DateTime? data = entity.FdDtrepact;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDtrepact = null;
					else entity.FdDtrepact = Convert.ToDateTime(value);
				}
			}
				
			public System.String FcOrigem
			{
				get
				{
					System.String data = entity.FcOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcOrigem = null;
					else entity.FcOrigem = Convert.ToString(value);
				}
			}
				
			public System.String FdDatapuimp
			{
				get
				{
					System.DateTime? data = entity.FdDatapuimp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatapuimp = null;
					else entity.FdDatapuimp = Convert.ToDateTime(value);
				}
			}
			

			private esOphisposfechamento entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOphisposfechamentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOphisposfechamento can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Ophisposfechamento : esOphisposfechamento
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOphisposfechamentoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OphisposfechamentoMetadata.Meta();
			}
		}	
		

		public esQueryItem FnCodbol
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnCodbol, esSystemType.Double);
			}
		} 
		
		public esQueryItem FcMatricula
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcMatricula, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodempresa
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcCodempresa, esSystemType.String);
			}
		} 
		
		public esQueryItem FcAssessor
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcAssessor, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTpoper
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcTpoper, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodliquida
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcCodliquida, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodmercado
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcCodmercado, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodbroker
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcCodbroker, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodlimite
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcCodlimite, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodtitulo
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcCodtitulo, esSystemType.String);
			}
		} 
		
		public esQueryItem FdAquisicao
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FdAquisicao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FdDatoper
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FdDatoper, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FdTrandata
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FdTrandata, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FnTxnominal
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnTxnominal, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnFir
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnFir, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnPrazoDc
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnPrazoDc, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnPrazoDu
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnPrazoDu, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnPuaquisicao
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnPuaquisicao, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnTxoper
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnTxoper, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnIr
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnIr, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnIof
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnIof, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnIofcomp
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnIofcomp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnTxcomp
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnTxcomp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnIrcomp
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnIrcomp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnQtdeoper
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnQtdeoper, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnPuoper
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnPuoper, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnValorbruto
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnValorbruto, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnPucomp
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnPucomp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnValorcomp
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnValorcomp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FdDatcomp
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FdDatcomp, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FcComando
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcComando, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodbanco
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcCodbanco, esSystemType.String);
			}
		} 
		
		public esQueryItem FcConta
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcConta, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodvinculo
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcCodvinculo, esSystemType.String);
			}
		} 
		
		public esQueryItem FcIndiceger
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcIndiceger, esSystemType.String);
			}
		} 
		
		public esQueryItem FnPercentger
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnPercentger, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnTxger
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnTxger, esSystemType.Double);
			}
		} 
		
		public esQueryItem FcRemufaixa
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcRemufaixa, esSystemType.String);
			}
		} 
		
		public esQueryItem FcControle
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcControle, esSystemType.String);
			}
		} 
		
		public esQueryItem FnQtdebancado
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnQtdebancado, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnQtdevinculado
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnQtdevinculado, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnPucurva
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnPucurva, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnPu550551
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnPu550551, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnFincurva
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnFincurva, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnFin550551
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnFin550551, esSystemType.Double);
			}
		} 
		
		public esQueryItem FcTipoestoque
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcTipoestoque, esSystemType.String);
			}
		} 
		
		public esQueryItem FcInstituicao
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcInstituicao, esSystemType.String);
			}
		} 
		
		public esQueryItem FnAjuste
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnAjuste, esSystemType.Double);
			}
		} 
		
		public esQueryItem FcGrupa
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcGrupa, esSystemType.String);
			}
		} 
		
		public esQueryItem FnPuger
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnPuger, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnFinger
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnFinger, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnIofger
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnIofger, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnIrger
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnIrger, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnQtderedesconto
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnQtderedesconto, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnCodbolPos
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FnCodbolPos, esSystemType.Double);
			}
		} 
		
		public esQueryItem FdDtrepact
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FdDtrepact, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FcOrigem
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FcOrigem, esSystemType.String);
			}
		} 
		
		public esQueryItem FdDatapuimp
		{
			get
			{
				return new esQueryItem(this, OphisposfechamentoMetadata.ColumnNames.FdDatapuimp, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OphisposfechamentoCollection")]
	public partial class OphisposfechamentoCollection : esOphisposfechamentoCollection, IEnumerable<Ophisposfechamento>
	{
		public OphisposfechamentoCollection()
		{

		}
		
		public static implicit operator List<Ophisposfechamento>(OphisposfechamentoCollection coll)
		{
			List<Ophisposfechamento> list = new List<Ophisposfechamento>();
			
			foreach (Ophisposfechamento emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OphisposfechamentoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OphisposfechamentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Ophisposfechamento(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Ophisposfechamento();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OphisposfechamentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OphisposfechamentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OphisposfechamentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Ophisposfechamento AddNew()
		{
			Ophisposfechamento entity = base.AddNewEntity() as Ophisposfechamento;
			
			return entity;
		}

		public Ophisposfechamento FindByPrimaryKey(System.Double fnCodbol, System.DateTime fdTrandata)
		{
			return base.FindByPrimaryKey(fnCodbol, fdTrandata) as Ophisposfechamento;
		}


		#region IEnumerable<Ophisposfechamento> Members

		IEnumerator<Ophisposfechamento> IEnumerable<Ophisposfechamento>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Ophisposfechamento;
			}
		}

		#endregion
		
		private OphisposfechamentoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OPHISPOSFECHAMENTO' table
	/// </summary>

	[Serializable]
	public partial class Ophisposfechamento : esOphisposfechamento
	{
		public Ophisposfechamento()
		{

		}
	
		public Ophisposfechamento(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OphisposfechamentoMetadata.Meta();
			}
		}
		
		
		
		override protected esOphisposfechamentoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OphisposfechamentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OphisposfechamentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OphisposfechamentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OphisposfechamentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OphisposfechamentoQuery query;
	}



	[Serializable]
	public partial class OphisposfechamentoQuery : esOphisposfechamentoQuery
	{
		public OphisposfechamentoQuery()
		{

		}		
		
		public OphisposfechamentoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OphisposfechamentoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OphisposfechamentoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnCodbol, 0, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnCodbol;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 15;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcMatricula, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcMatricula;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcCodempresa, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcCodempresa;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcAssessor, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcAssessor;
			c.CharacterMaxLength = 7;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcTpoper, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcTpoper;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcCodliquida, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcCodliquida;
			c.CharacterMaxLength = 6;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcCodmercado, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcCodmercado;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcCodbroker, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcCodbroker;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcCodlimite, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcCodlimite;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcCodtitulo, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcCodtitulo;
			c.CharacterMaxLength = 6;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FdAquisicao, 10, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FdAquisicao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FdDatoper, 11, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FdDatoper;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FdTrandata, 12, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FdTrandata;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnTxnominal, 13, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnTxnominal;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnFir, 14, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnFir;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnPrazoDc, 15, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnPrazoDc;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnPrazoDu, 16, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnPrazoDu;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnPuaquisicao, 17, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnPuaquisicao;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnTxoper, 18, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnTxoper;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnIr, 19, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnIr;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnIof, 20, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnIof;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnIofcomp, 21, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnIofcomp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnTxcomp, 22, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnTxcomp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnIrcomp, 23, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnIrcomp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnQtdeoper, 24, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnQtdeoper;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnPuoper, 25, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnPuoper;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnValorbruto, 26, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnValorbruto;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnPucomp, 27, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnPucomp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnValorcomp, 28, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnValorcomp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FdDatcomp, 29, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FdDatcomp;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcComando, 30, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcComando;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcCodbanco, 31, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcCodbanco;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcConta, 32, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcConta;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcCodvinculo, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcCodvinculo;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcIndiceger, 34, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcIndiceger;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnPercentger, 35, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnPercentger;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnTxger, 36, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnTxger;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcRemufaixa, 37, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcRemufaixa;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcControle, 38, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcControle;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnQtdebancado, 39, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnQtdebancado;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnQtdevinculado, 40, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnQtdevinculado;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnPucurva, 41, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnPucurva;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnPu550551, 42, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnPu550551;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnFincurva, 43, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnFincurva;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnFin550551, 44, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnFin550551;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcTipoestoque, 45, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcTipoestoque;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcInstituicao, 46, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcInstituicao;
			c.CharacterMaxLength = 15;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnAjuste, 47, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnAjuste;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcGrupa, 48, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcGrupa;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnPuger, 49, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnPuger;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnFinger, 50, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnFinger;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnIofger, 51, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnIofger;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnIrger, 52, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnIrger;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnQtderedesconto, 53, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnQtderedesconto;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FnCodbolPos, 54, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FnCodbolPos;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FdDtrepact, 55, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FdDtrepact;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FcOrigem, 56, typeof(System.String), esSystemType.String);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FcOrigem;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OphisposfechamentoMetadata.ColumnNames.FdDatapuimp, 57, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OphisposfechamentoMetadata.PropertyNames.FdDatapuimp;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OphisposfechamentoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string FnCodbol = "FN_CODBOL";
			 public const string FcMatricula = "FC_MATRICULA";
			 public const string FcCodempresa = "FC_CODEMPRESA";
			 public const string FcAssessor = "FC_ASSESSOR";
			 public const string FcTpoper = "FC_TPOPER";
			 public const string FcCodliquida = "FC_CODLIQUIDA";
			 public const string FcCodmercado = "FC_CODMERCADO";
			 public const string FcCodbroker = "FC_CODBROKER";
			 public const string FcCodlimite = "FC_CODLIMITE";
			 public const string FcCodtitulo = "FC_CODTITULO";
			 public const string FdAquisicao = "FD_AQUISICAO";
			 public const string FdDatoper = "FD_DATOPER";
			 public const string FdTrandata = "FD_TRANDATA";
			 public const string FnTxnominal = "FN_TXNOMINAL";
			 public const string FnFir = "FN_FIR";
			 public const string FnPrazoDc = "FN_PRAZO_DC";
			 public const string FnPrazoDu = "FN_PRAZO_DU";
			 public const string FnPuaquisicao = "FN_PUAQUISICAO";
			 public const string FnTxoper = "FN_TXOPER";
			 public const string FnIr = "FN_IR";
			 public const string FnIof = "FN_IOF";
			 public const string FnIofcomp = "FN_IOFCOMP";
			 public const string FnTxcomp = "FN_TXCOMP";
			 public const string FnIrcomp = "FN_IRCOMP";
			 public const string FnQtdeoper = "FN_QTDEOPER";
			 public const string FnPuoper = "FN_PUOPER";
			 public const string FnValorbruto = "FN_VALORBRUTO";
			 public const string FnPucomp = "FN_PUCOMP";
			 public const string FnValorcomp = "FN_VALORCOMP";
			 public const string FdDatcomp = "FD_DATCOMP";
			 public const string FcComando = "FC_COMANDO";
			 public const string FcCodbanco = "FC_CODBANCO";
			 public const string FcConta = "FC_CONTA";
			 public const string FcCodvinculo = "FC_CODVINCULO";
			 public const string FcIndiceger = "FC_INDICEGER";
			 public const string FnPercentger = "FN_PERCENTGER";
			 public const string FnTxger = "FN_TXGER";
			 public const string FcRemufaixa = "FC_REMUFAIXA";
			 public const string FcControle = "FC_CONTROLE";
			 public const string FnQtdebancado = "FN_QTDEBANCADO";
			 public const string FnQtdevinculado = "FN_QTDEVINCULADO";
			 public const string FnPucurva = "FN_PUCURVA";
			 public const string FnPu550551 = "FN_PU550551";
			 public const string FnFincurva = "FN_FINCURVA";
			 public const string FnFin550551 = "FN_FIN550551";
			 public const string FcTipoestoque = "FC_TIPOESTOQUE";
			 public const string FcInstituicao = "FC_INSTITUICAO";
			 public const string FnAjuste = "FN_AJUSTE";
			 public const string FcGrupa = "FC_GRUPA";
			 public const string FnPuger = "FN_PUGER";
			 public const string FnFinger = "FN_FINGER";
			 public const string FnIofger = "FN_IOFGER";
			 public const string FnIrger = "FN_IRGER";
			 public const string FnQtderedesconto = "FN_QTDEREDESCONTO";
			 public const string FnCodbolPos = "FN_CODBOL_POS";
			 public const string FdDtrepact = "FD_DTREPACT";
			 public const string FcOrigem = "FC_ORIGEM";
			 public const string FdDatapuimp = "FD_DATAPUIMP";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string FnCodbol = "FnCodbol";
			 public const string FcMatricula = "FcMatricula";
			 public const string FcCodempresa = "FcCodempresa";
			 public const string FcAssessor = "FcAssessor";
			 public const string FcTpoper = "FcTpoper";
			 public const string FcCodliquida = "FcCodliquida";
			 public const string FcCodmercado = "FcCodmercado";
			 public const string FcCodbroker = "FcCodbroker";
			 public const string FcCodlimite = "FcCodlimite";
			 public const string FcCodtitulo = "FcCodtitulo";
			 public const string FdAquisicao = "FdAquisicao";
			 public const string FdDatoper = "FdDatoper";
			 public const string FdTrandata = "FdTrandata";
			 public const string FnTxnominal = "FnTxnominal";
			 public const string FnFir = "FnFir";
			 public const string FnPrazoDc = "FnPrazoDc";
			 public const string FnPrazoDu = "FnPrazoDu";
			 public const string FnPuaquisicao = "FnPuaquisicao";
			 public const string FnTxoper = "FnTxoper";
			 public const string FnIr = "FnIr";
			 public const string FnIof = "FnIof";
			 public const string FnIofcomp = "FnIofcomp";
			 public const string FnTxcomp = "FnTxcomp";
			 public const string FnIrcomp = "FnIrcomp";
			 public const string FnQtdeoper = "FnQtdeoper";
			 public const string FnPuoper = "FnPuoper";
			 public const string FnValorbruto = "FnValorbruto";
			 public const string FnPucomp = "FnPucomp";
			 public const string FnValorcomp = "FnValorcomp";
			 public const string FdDatcomp = "FdDatcomp";
			 public const string FcComando = "FcComando";
			 public const string FcCodbanco = "FcCodbanco";
			 public const string FcConta = "FcConta";
			 public const string FcCodvinculo = "FcCodvinculo";
			 public const string FcIndiceger = "FcIndiceger";
			 public const string FnPercentger = "FnPercentger";
			 public const string FnTxger = "FnTxger";
			 public const string FcRemufaixa = "FcRemufaixa";
			 public const string FcControle = "FcControle";
			 public const string FnQtdebancado = "FnQtdebancado";
			 public const string FnQtdevinculado = "FnQtdevinculado";
			 public const string FnPucurva = "FnPucurva";
			 public const string FnPu550551 = "FnPu550551";
			 public const string FnFincurva = "FnFincurva";
			 public const string FnFin550551 = "FnFin550551";
			 public const string FcTipoestoque = "FcTipoestoque";
			 public const string FcInstituicao = "FcInstituicao";
			 public const string FnAjuste = "FnAjuste";
			 public const string FcGrupa = "FcGrupa";
			 public const string FnPuger = "FnPuger";
			 public const string FnFinger = "FnFinger";
			 public const string FnIofger = "FnIofger";
			 public const string FnIrger = "FnIrger";
			 public const string FnQtderedesconto = "FnQtderedesconto";
			 public const string FnCodbolPos = "FnCodbolPos";
			 public const string FdDtrepact = "FdDtrepact";
			 public const string FcOrigem = "FcOrigem";
			 public const string FdDatapuimp = "FdDatapuimp";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OphisposfechamentoMetadata))
			{
				if(OphisposfechamentoMetadata.mapDelegates == null)
				{
					OphisposfechamentoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OphisposfechamentoMetadata.meta == null)
				{
					OphisposfechamentoMetadata.meta = new OphisposfechamentoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("FN_CODBOL", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FC_MATRICULA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODEMPRESA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ASSESSOR", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TPOPER", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODLIQUIDA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODMERCADO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODBROKER", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODLIMITE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODTITULO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FD_AQUISICAO", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FD_DATOPER", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FD_TRANDATA", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FN_TXNOMINAL", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_FIR", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_PRAZO_DC", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_PRAZO_DU", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_PUAQUISICAO", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_TXOPER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_IR", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_IOF", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_IOFCOMP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_TXCOMP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_IRCOMP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_QTDEOPER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_PUOPER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_VALORBRUTO", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_PUCOMP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_VALORCOMP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FD_DATCOMP", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FC_COMANDO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODBANCO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CONTA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODVINCULO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_INDICEGER", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FN_PERCENTGER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_TXGER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FC_REMUFAIXA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CONTROLE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FN_QTDEBANCADO", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_QTDEVINCULADO", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_PUCURVA", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_PU550551", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_FINCURVA", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_FIN550551", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FC_TIPOESTOQUE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_INSTITUICAO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FN_AJUSTE", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FC_GRUPA", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FN_PUGER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_FINGER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_IOFGER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_IRGER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_QTDEREDESCONTO", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_CODBOL_POS", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FD_DTREPACT", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FC_ORIGEM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FD_DATAPUIMP", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "OPHISPOSFECHAMENTO";
				meta.Destination = "OPHISPOSFECHAMENTO";
				
				meta.spInsert = "proc_OPHISPOSFECHAMENTOInsert";				
				meta.spUpdate = "proc_OPHISPOSFECHAMENTOUpdate";		
				meta.spDelete = "proc_OPHISPOSFECHAMENTODelete";
				meta.spLoadAll = "proc_OPHISPOSFECHAMENTOLoadAll";
				meta.spLoadByPrimaryKey = "proc_OPHISPOSFECHAMENTOLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OphisposfechamentoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
