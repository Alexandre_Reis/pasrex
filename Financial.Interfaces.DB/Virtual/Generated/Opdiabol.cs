/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/12/2013 17:28:40
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;











































































































































































































namespace Financial.InterfacesDB
{

	[Serializable]
	abstract public class esOpdiabolCollection : esEntityCollection
	{
		public esOpdiabolCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OpdiabolCollection";
		}

		#region Query Logic
		protected void InitQuery(esOpdiabolQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOpdiabolQuery);
		}
		#endregion
		
		virtual public Opdiabol DetachEntity(Opdiabol entity)
		{
			return base.DetachEntity(entity) as Opdiabol;
		}
		
		virtual public Opdiabol AttachEntity(Opdiabol entity)
		{
			return base.AttachEntity(entity) as Opdiabol;
		}
		
		virtual public void Combine(OpdiabolCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Opdiabol this[int index]
		{
			get
			{
				return base[index] as Opdiabol;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Opdiabol);
		}
	}



	[Serializable]
	abstract public class esOpdiabol : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOpdiabolQuery GetDynamicQuery()
		{
			return null;
		}

		public esOpdiabol()
		{

		}

		public esOpdiabol(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Double fnCodbol)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(fnCodbol);
			else
				return LoadByPrimaryKeyStoredProcedure(fnCodbol);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Double fnCodbol)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esOpdiabolQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.FnCodbol == fnCodbol);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Double fnCodbol)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(fnCodbol);
			else
				return LoadByPrimaryKeyStoredProcedure(fnCodbol);
		}

		private bool LoadByPrimaryKeyDynamic(System.Double fnCodbol)
		{
			esOpdiabolQuery query = this.GetDynamicQuery();
			query.Where(query.FnCodbol == fnCodbol);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Double fnCodbol)
		{
			esParameters parms = new esParameters();
			parms.Add("FN_CODBOL",fnCodbol);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "FnCodbol": this.str.FnCodbol = (string)value; break;							
						case "FnCodbolreferencia": this.str.FnCodbolreferencia = (string)value; break;							
						case "FcMatricula": this.str.FcMatricula = (string)value; break;							
						case "FcCodempresa": this.str.FcCodempresa = (string)value; break;							
						case "FcAssessor": this.str.FcAssessor = (string)value; break;							
						case "FcCodliquida": this.str.FcCodliquida = (string)value; break;							
						case "FcTpoper": this.str.FcTpoper = (string)value; break;							
						case "FcCodtitulo": this.str.FcCodtitulo = (string)value; break;							
						case "FcCodmercado": this.str.FcCodmercado = (string)value; break;							
						case "FcCodbroker": this.str.FcCodbroker = (string)value; break;							
						case "FcCodlimite": this.str.FcCodlimite = (string)value; break;							
						case "FcCodbanco": this.str.FcCodbanco = (string)value; break;							
						case "FcConta": this.str.FcConta = (string)value; break;							
						case "FcIfcedente": this.str.FcIfcedente = (string)value; break;							
						case "FcIfcessionario": this.str.FcIfcessionario = (string)value; break;							
						case "FcCodvinculo": this.str.FcCodvinculo = (string)value; break;							
						case "FdAquisicao": this.str.FdAquisicao = (string)value; break;							
						case "FdDatoper": this.str.FdDatoper = (string)value; break;							
						case "FdHorabol": this.str.FdHorabol = (string)value; break;							
						case "FdHoracombinada": this.str.FdHoracombinada = (string)value; break;							
						case "FnTxnominal": this.str.FnTxnominal = (string)value; break;							
						case "FnFir": this.str.FnFir = (string)value; break;							
						case "FnPuaquisicao": this.str.FnPuaquisicao = (string)value; break;							
						case "FnTxoper": this.str.FnTxoper = (string)value; break;							
						case "FnIr": this.str.FnIr = (string)value; break;							
						case "FnIof": this.str.FnIof = (string)value; break;							
						case "FnIofcomp": this.str.FnIofcomp = (string)value; break;							
						case "FnTxcomp": this.str.FnTxcomp = (string)value; break;							
						case "FnIrcomp": this.str.FnIrcomp = (string)value; break;							
						case "FnQtdeoper": this.str.FnQtdeoper = (string)value; break;							
						case "FnPuoper": this.str.FnPuoper = (string)value; break;							
						case "FnValorbruto": this.str.FnValorbruto = (string)value; break;							
						case "FnPucomp": this.str.FnPucomp = (string)value; break;							
						case "FnValorcomp": this.str.FnValorcomp = (string)value; break;							
						case "FdDatcomp": this.str.FdDatcomp = (string)value; break;							
						case "FcComando": this.str.FcComando = (string)value; break;							
						case "FcIndiceger": this.str.FcIndiceger = (string)value; break;							
						case "FnPercentger": this.str.FnPercentger = (string)value; break;							
						case "FnTxger": this.str.FnTxger = (string)value; break;							
						case "FcRemufaixa": this.str.FcRemufaixa = (string)value; break;							
						case "FcControle": this.str.FcControle = (string)value; break;							
						case "FcTipoestoque": this.str.FcTipoestoque = (string)value; break;							
						case "FcGrupa": this.str.FcGrupa = (string)value; break;							
						case "FdDatlastro": this.str.FdDatlastro = (string)value; break;							
						case "FnBollastro": this.str.FnBollastro = (string)value; break;							
						case "FnPulastro": this.str.FnPulastro = (string)value; break;							
						case "FcStatus": this.str.FcStatus = (string)value; break;							
						case "FdHoraenvio": this.str.FdHoraenvio = (string)value; break;							
						case "FdAgendamento": this.str.FdAgendamento = (string)value; break;							
						case "FdDatliquidacao": this.str.FdDatliquidacao = (string)value; break;							
						case "FcCodmensagem": this.str.FcCodmensagem = (string)value; break;							
						case "FcTipolei": this.str.FcTipolei = (string)value; break;							
						case "FcControleif": this.str.FcControleif = (string)value; break;							
						case "FcNivelpref": this.str.FcNivelpref = (string)value; break;							
						case "FcChaveassoc": this.str.FcChaveassoc = (string)value; break;							
						case "FcNumopsel": this.str.FcNumopsel = (string)value; break;							
						case "FcNumopselret": this.str.FcNumopselret = (string)value; break;							
						case "FcTpleilao": this.str.FcTpleilao = (string)value; break;							
						case "FcNsu": this.str.FcNsu = (string)value; break;							
						case "FcRdcret": this.str.FcRdcret = (string)value; break;							
						case "FcDestino": this.str.FcDestino = (string)value; break;							
						case "FcObs": this.str.FcObs = (string)value; break;							
						case "FnCodbolPre": this.str.FnCodbolPre = (string)value; break;							
						case "FlBrokerces": this.str.FlBrokerces = (string)value; break;							
						case "FlBrokerced": this.str.FlBrokerced = (string)value; break;							
						case "FdDatcomptermo": this.str.FdDatcomptermo = (string)value; break;							
						case "FnBase": this.str.FnBase = (string)value; break;							
						case "FdDatcompini": this.str.FdDatcompini = (string)value; break;							
						case "FcUnilateralidade": this.str.FcUnilateralidade = (string)value; break;							
						case "FcUsuario": this.str.FcUsuario = (string)value; break;							
						case "FcOrigem": this.str.FcOrigem = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "FnCodbol":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnCodbol = (System.Double?)value;
							break;
						
						case "FnCodbolreferencia":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnCodbolreferencia = (System.Double?)value;
							break;
						
						case "FdAquisicao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdAquisicao = (System.DateTime?)value;
							break;
						
						case "FdDatoper":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatoper = (System.DateTime?)value;
							break;
						
						case "FdHorabol":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdHorabol = (System.DateTime?)value;
							break;
						
						case "FdHoracombinada":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdHoracombinada = (System.DateTime?)value;
							break;
						
						case "FnTxnominal":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnTxnominal = (System.Double?)value;
							break;
						
						case "FnFir":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnFir = (System.Double?)value;
							break;
						
						case "FnPuaquisicao":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPuaquisicao = (System.Double?)value;
							break;
						
						case "FnTxoper":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnTxoper = (System.Double?)value;
							break;
						
						case "FnIr":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnIr = (System.Double?)value;
							break;
						
						case "FnIof":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnIof = (System.Double?)value;
							break;
						
						case "FnIofcomp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnIofcomp = (System.Double?)value;
							break;
						
						case "FnTxcomp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnTxcomp = (System.Double?)value;
							break;
						
						case "FnIrcomp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnIrcomp = (System.Double?)value;
							break;
						
						case "FnQtdeoper":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnQtdeoper = (System.Double?)value;
							break;
						
						case "FnPuoper":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPuoper = (System.Double?)value;
							break;
						
						case "FnValorbruto":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnValorbruto = (System.Double?)value;
							break;
						
						case "FnPucomp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPucomp = (System.Double?)value;
							break;
						
						case "FnValorcomp":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnValorcomp = (System.Double?)value;
							break;
						
						case "FdDatcomp":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatcomp = (System.DateTime?)value;
							break;
						
						case "FnPercentger":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPercentger = (System.Double?)value;
							break;
						
						case "FnTxger":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnTxger = (System.Double?)value;
							break;
						
						case "FdDatlastro":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatlastro = (System.DateTime?)value;
							break;
						
						case "FnBollastro":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnBollastro = (System.Double?)value;
							break;
						
						case "FnPulastro":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnPulastro = (System.Double?)value;
							break;
						
						case "FdHoraenvio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdHoraenvio = (System.DateTime?)value;
							break;
						
						case "FdAgendamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdAgendamento = (System.DateTime?)value;
							break;
						
						case "FdDatliquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatliquidacao = (System.DateTime?)value;
							break;
						
						case "FnCodbolPre":
						
							if (value == null || value.GetType().ToString() == "System.Double")
								this.FnCodbolPre = (System.Double?)value;
							break;
						
						case "FlBrokerces":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FlBrokerces = (System.Int32?)value;
							break;
						
						case "FlBrokerced":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FlBrokerced = (System.Int32?)value;
							break;
						
						case "FdDatcomptermo":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatcomptermo = (System.DateTime?)value;
							break;
						
						case "FnBase":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FnBase = (System.Int32?)value;
							break;
						
						case "FdDatcompini":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.FdDatcompini = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OPDIABOL.FN_CODBOL
		/// </summary>
		virtual public System.Double? FnCodbol
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnCodbol);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnCodbol, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_CODBOLREFERENCIA
		/// </summary>
		virtual public System.Double? FnCodbolreferencia
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnCodbolreferencia);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnCodbolreferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_MATRICULA
		/// </summary>
		virtual public System.String FcMatricula
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcMatricula);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcMatricula, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_CODEMPRESA
		/// </summary>
		virtual public System.String FcCodempresa
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcCodempresa);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcCodempresa, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_ASSESSOR
		/// </summary>
		virtual public System.String FcAssessor
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcAssessor);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcAssessor, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_CODLIQUIDA
		/// </summary>
		virtual public System.String FcCodliquida
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcCodliquida);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcCodliquida, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_TPOPER
		/// </summary>
		virtual public System.String FcTpoper
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcTpoper);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcTpoper, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_CODTITULO
		/// </summary>
		virtual public System.String FcCodtitulo
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcCodtitulo);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcCodtitulo, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_CODMERCADO
		/// </summary>
		virtual public System.String FcCodmercado
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcCodmercado);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcCodmercado, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_CODBROKER
		/// </summary>
		virtual public System.String FcCodbroker
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcCodbroker);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcCodbroker, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_CODLIMITE
		/// </summary>
		virtual public System.String FcCodlimite
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcCodlimite);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcCodlimite, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_CODBANCO
		/// </summary>
		virtual public System.String FcCodbanco
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcCodbanco);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcCodbanco, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_CONTA
		/// </summary>
		virtual public System.String FcConta
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcConta);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcConta, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_IFCEDENTE
		/// </summary>
		virtual public System.String FcIfcedente
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcIfcedente);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcIfcedente, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_IFCESSIONARIO
		/// </summary>
		virtual public System.String FcIfcessionario
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcIfcessionario);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcIfcessionario, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_CODVINCULO
		/// </summary>
		virtual public System.String FcCodvinculo
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcCodvinculo);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcCodvinculo, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FD_AQUISICAO
		/// </summary>
		virtual public System.DateTime? FdAquisicao
		{
			get
			{
				return base.GetSystemDateTime(OpdiabolMetadata.ColumnNames.FdAquisicao);
			}
			
			set
			{
				base.SetSystemDateTime(OpdiabolMetadata.ColumnNames.FdAquisicao, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FD_DATOPER
		/// </summary>
		virtual public System.DateTime? FdDatoper
		{
			get
			{
				return base.GetSystemDateTime(OpdiabolMetadata.ColumnNames.FdDatoper);
			}
			
			set
			{
				base.SetSystemDateTime(OpdiabolMetadata.ColumnNames.FdDatoper, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FD_HORABOL
		/// </summary>
		virtual public System.DateTime? FdHorabol
		{
			get
			{
				return base.GetSystemDateTime(OpdiabolMetadata.ColumnNames.FdHorabol);
			}
			
			set
			{
				base.SetSystemDateTime(OpdiabolMetadata.ColumnNames.FdHorabol, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FD_HORACOMBINADA
		/// </summary>
		virtual public System.DateTime? FdHoracombinada
		{
			get
			{
				return base.GetSystemDateTime(OpdiabolMetadata.ColumnNames.FdHoracombinada);
			}
			
			set
			{
				base.SetSystemDateTime(OpdiabolMetadata.ColumnNames.FdHoracombinada, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_TXNOMINAL
		/// </summary>
		virtual public System.Double? FnTxnominal
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnTxnominal);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnTxnominal, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_FIR
		/// </summary>
		virtual public System.Double? FnFir
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnFir);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnFir, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_PUAQUISICAO
		/// </summary>
		virtual public System.Double? FnPuaquisicao
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnPuaquisicao);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnPuaquisicao, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_TXOPER
		/// </summary>
		virtual public System.Double? FnTxoper
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnTxoper);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnTxoper, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_IR
		/// </summary>
		virtual public System.Double? FnIr
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnIr);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnIr, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_IOF
		/// </summary>
		virtual public System.Double? FnIof
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnIof);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnIof, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_IOFCOMP
		/// </summary>
		virtual public System.Double? FnIofcomp
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnIofcomp);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnIofcomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_TXCOMP
		/// </summary>
		virtual public System.Double? FnTxcomp
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnTxcomp);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnTxcomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_IRCOMP
		/// </summary>
		virtual public System.Double? FnIrcomp
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnIrcomp);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnIrcomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_QTDEOPER
		/// </summary>
		virtual public System.Double? FnQtdeoper
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnQtdeoper);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnQtdeoper, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_PUOPER
		/// </summary>
		virtual public System.Double? FnPuoper
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnPuoper);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnPuoper, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_VALORBRUTO
		/// </summary>
		virtual public System.Double? FnValorbruto
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnValorbruto);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnValorbruto, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_PUCOMP
		/// </summary>
		virtual public System.Double? FnPucomp
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnPucomp);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnPucomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_VALORCOMP
		/// </summary>
		virtual public System.Double? FnValorcomp
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnValorcomp);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnValorcomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FD_DATCOMP
		/// </summary>
		virtual public System.DateTime? FdDatcomp
		{
			get
			{
				return base.GetSystemDateTime(OpdiabolMetadata.ColumnNames.FdDatcomp);
			}
			
			set
			{
				base.SetSystemDateTime(OpdiabolMetadata.ColumnNames.FdDatcomp, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_COMANDO
		/// </summary>
		virtual public System.String FcComando
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcComando);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcComando, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_INDICEGER
		/// </summary>
		virtual public System.String FcIndiceger
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcIndiceger);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcIndiceger, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_PERCENTGER
		/// </summary>
		virtual public System.Double? FnPercentger
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnPercentger);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnPercentger, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_TXGER
		/// </summary>
		virtual public System.Double? FnTxger
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnTxger);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnTxger, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_REMUFAIXA
		/// </summary>
		virtual public System.String FcRemufaixa
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcRemufaixa);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcRemufaixa, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_CONTROLE
		/// </summary>
		virtual public System.String FcControle
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcControle);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcControle, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_TIPOESTOQUE
		/// </summary>
		virtual public System.String FcTipoestoque
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcTipoestoque);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcTipoestoque, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_GRUPA
		/// </summary>
		virtual public System.String FcGrupa
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcGrupa);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcGrupa, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FD_DATLASTRO
		/// </summary>
		virtual public System.DateTime? FdDatlastro
		{
			get
			{
				return base.GetSystemDateTime(OpdiabolMetadata.ColumnNames.FdDatlastro);
			}
			
			set
			{
				base.SetSystemDateTime(OpdiabolMetadata.ColumnNames.FdDatlastro, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_BOLLASTRO
		/// </summary>
		virtual public System.Double? FnBollastro
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnBollastro);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnBollastro, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_PULASTRO
		/// </summary>
		virtual public System.Double? FnPulastro
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnPulastro);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnPulastro, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_STATUS
		/// </summary>
		virtual public System.String FcStatus
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcStatus);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcStatus, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FD_HORAENVIO
		/// </summary>
		virtual public System.DateTime? FdHoraenvio
		{
			get
			{
				return base.GetSystemDateTime(OpdiabolMetadata.ColumnNames.FdHoraenvio);
			}
			
			set
			{
				base.SetSystemDateTime(OpdiabolMetadata.ColumnNames.FdHoraenvio, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FD_AGENDAMENTO
		/// </summary>
		virtual public System.DateTime? FdAgendamento
		{
			get
			{
				return base.GetSystemDateTime(OpdiabolMetadata.ColumnNames.FdAgendamento);
			}
			
			set
			{
				base.SetSystemDateTime(OpdiabolMetadata.ColumnNames.FdAgendamento, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FD_DATLIQUIDACAO
		/// </summary>
		virtual public System.DateTime? FdDatliquidacao
		{
			get
			{
				return base.GetSystemDateTime(OpdiabolMetadata.ColumnNames.FdDatliquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(OpdiabolMetadata.ColumnNames.FdDatliquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_CODMENSAGEM
		/// </summary>
		virtual public System.String FcCodmensagem
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcCodmensagem);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcCodmensagem, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_TIPOLEI
		/// </summary>
		virtual public System.String FcTipolei
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcTipolei);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcTipolei, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_CONTROLEIF
		/// </summary>
		virtual public System.String FcControleif
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcControleif);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcControleif, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_NIVELPREF
		/// </summary>
		virtual public System.String FcNivelpref
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcNivelpref);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcNivelpref, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_CHAVEASSOC
		/// </summary>
		virtual public System.String FcChaveassoc
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcChaveassoc);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcChaveassoc, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_NUMOPSEL
		/// </summary>
		virtual public System.String FcNumopsel
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcNumopsel);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcNumopsel, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_NUMOPSELRET
		/// </summary>
		virtual public System.String FcNumopselret
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcNumopselret);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcNumopselret, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_TPLEILAO
		/// </summary>
		virtual public System.String FcTpleilao
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcTpleilao);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcTpleilao, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_NSU
		/// </summary>
		virtual public System.String FcNsu
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcNsu);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcNsu, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_RDCRET
		/// </summary>
		virtual public System.String FcRdcret
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcRdcret);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcRdcret, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_DESTINO
		/// </summary>
		virtual public System.String FcDestino
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcDestino);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcDestino, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_OBS
		/// </summary>
		virtual public System.String FcObs
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcObs);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcObs, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_CODBOL_PRE
		/// </summary>
		virtual public System.Double? FnCodbolPre
		{
			get
			{
				return base.GetSystemDouble(OpdiabolMetadata.ColumnNames.FnCodbolPre);
			}
			
			set
			{
				base.SetSystemDouble(OpdiabolMetadata.ColumnNames.FnCodbolPre, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FL_BROKERCES
		/// </summary>
		virtual public System.Int32? FlBrokerces
		{
			get
			{
				return base.GetSystemInt32(OpdiabolMetadata.ColumnNames.FlBrokerces);
			}
			
			set
			{
				base.SetSystemInt32(OpdiabolMetadata.ColumnNames.FlBrokerces, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FL_BROKERCED
		/// </summary>
		virtual public System.Int32? FlBrokerced
		{
			get
			{
				return base.GetSystemInt32(OpdiabolMetadata.ColumnNames.FlBrokerced);
			}
			
			set
			{
				base.SetSystemInt32(OpdiabolMetadata.ColumnNames.FlBrokerced, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FD_DATCOMPTERMO
		/// </summary>
		virtual public System.DateTime? FdDatcomptermo
		{
			get
			{
				return base.GetSystemDateTime(OpdiabolMetadata.ColumnNames.FdDatcomptermo);
			}
			
			set
			{
				base.SetSystemDateTime(OpdiabolMetadata.ColumnNames.FdDatcomptermo, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FN_BASE
		/// </summary>
		virtual public System.Int32? FnBase
		{
			get
			{
				return base.GetSystemInt32(OpdiabolMetadata.ColumnNames.FnBase);
			}
			
			set
			{
				base.SetSystemInt32(OpdiabolMetadata.ColumnNames.FnBase, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FD_DATCOMPINI
		/// </summary>
		virtual public System.DateTime? FdDatcompini
		{
			get
			{
				return base.GetSystemDateTime(OpdiabolMetadata.ColumnNames.FdDatcompini);
			}
			
			set
			{
				base.SetSystemDateTime(OpdiabolMetadata.ColumnNames.FdDatcompini, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_UNILATERALIDADE
		/// </summary>
		virtual public System.String FcUnilateralidade
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcUnilateralidade);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcUnilateralidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_USUARIO
		/// </summary>
		virtual public System.String FcUsuario
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcUsuario);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcUsuario, value);
			}
		}
		
		/// <summary>
		/// Maps to OPDIABOL.FC_ORIGEM
		/// </summary>
		virtual public System.String FcOrigem
		{
			get
			{
				return base.GetSystemString(OpdiabolMetadata.ColumnNames.FcOrigem);
			}
			
			set
			{
				base.SetSystemString(OpdiabolMetadata.ColumnNames.FcOrigem, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOpdiabol entity)
			{
				this.entity = entity;
			}
			
	
			public System.String FnCodbol
			{
				get
				{
					System.Double? data = entity.FnCodbol;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnCodbol = null;
					else entity.FnCodbol = Convert.ToDouble(value);
				}
			}
				
			public System.String FnCodbolreferencia
			{
				get
				{
					System.Double? data = entity.FnCodbolreferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnCodbolreferencia = null;
					else entity.FnCodbolreferencia = Convert.ToDouble(value);
				}
			}
				
			public System.String FcMatricula
			{
				get
				{
					System.String data = entity.FcMatricula;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcMatricula = null;
					else entity.FcMatricula = Convert.ToString(value);
				}
			}
				
			public System.String FcCodempresa
			{
				get
				{
					System.String data = entity.FcCodempresa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodempresa = null;
					else entity.FcCodempresa = Convert.ToString(value);
				}
			}
				
			public System.String FcAssessor
			{
				get
				{
					System.String data = entity.FcAssessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcAssessor = null;
					else entity.FcAssessor = Convert.ToString(value);
				}
			}
				
			public System.String FcCodliquida
			{
				get
				{
					System.String data = entity.FcCodliquida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodliquida = null;
					else entity.FcCodliquida = Convert.ToString(value);
				}
			}
				
			public System.String FcTpoper
			{
				get
				{
					System.String data = entity.FcTpoper;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTpoper = null;
					else entity.FcTpoper = Convert.ToString(value);
				}
			}
				
			public System.String FcCodtitulo
			{
				get
				{
					System.String data = entity.FcCodtitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodtitulo = null;
					else entity.FcCodtitulo = Convert.ToString(value);
				}
			}
				
			public System.String FcCodmercado
			{
				get
				{
					System.String data = entity.FcCodmercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodmercado = null;
					else entity.FcCodmercado = Convert.ToString(value);
				}
			}
				
			public System.String FcCodbroker
			{
				get
				{
					System.String data = entity.FcCodbroker;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodbroker = null;
					else entity.FcCodbroker = Convert.ToString(value);
				}
			}
				
			public System.String FcCodlimite
			{
				get
				{
					System.String data = entity.FcCodlimite;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodlimite = null;
					else entity.FcCodlimite = Convert.ToString(value);
				}
			}
				
			public System.String FcCodbanco
			{
				get
				{
					System.String data = entity.FcCodbanco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodbanco = null;
					else entity.FcCodbanco = Convert.ToString(value);
				}
			}
				
			public System.String FcConta
			{
				get
				{
					System.String data = entity.FcConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcConta = null;
					else entity.FcConta = Convert.ToString(value);
				}
			}
				
			public System.String FcIfcedente
			{
				get
				{
					System.String data = entity.FcIfcedente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcIfcedente = null;
					else entity.FcIfcedente = Convert.ToString(value);
				}
			}
				
			public System.String FcIfcessionario
			{
				get
				{
					System.String data = entity.FcIfcessionario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcIfcessionario = null;
					else entity.FcIfcessionario = Convert.ToString(value);
				}
			}
				
			public System.String FcCodvinculo
			{
				get
				{
					System.String data = entity.FcCodvinculo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodvinculo = null;
					else entity.FcCodvinculo = Convert.ToString(value);
				}
			}
				
			public System.String FdAquisicao
			{
				get
				{
					System.DateTime? data = entity.FdAquisicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdAquisicao = null;
					else entity.FdAquisicao = Convert.ToDateTime(value);
				}
			}
				
			public System.String FdDatoper
			{
				get
				{
					System.DateTime? data = entity.FdDatoper;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatoper = null;
					else entity.FdDatoper = Convert.ToDateTime(value);
				}
			}
				
			public System.String FdHorabol
			{
				get
				{
					System.DateTime? data = entity.FdHorabol;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdHorabol = null;
					else entity.FdHorabol = Convert.ToDateTime(value);
				}
			}
				
			public System.String FdHoracombinada
			{
				get
				{
					System.DateTime? data = entity.FdHoracombinada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdHoracombinada = null;
					else entity.FdHoracombinada = Convert.ToDateTime(value);
				}
			}
				
			public System.String FnTxnominal
			{
				get
				{
					System.Double? data = entity.FnTxnominal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnTxnominal = null;
					else entity.FnTxnominal = Convert.ToDouble(value);
				}
			}
				
			public System.String FnFir
			{
				get
				{
					System.Double? data = entity.FnFir;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnFir = null;
					else entity.FnFir = Convert.ToDouble(value);
				}
			}
				
			public System.String FnPuaquisicao
			{
				get
				{
					System.Double? data = entity.FnPuaquisicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPuaquisicao = null;
					else entity.FnPuaquisicao = Convert.ToDouble(value);
				}
			}
				
			public System.String FnTxoper
			{
				get
				{
					System.Double? data = entity.FnTxoper;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnTxoper = null;
					else entity.FnTxoper = Convert.ToDouble(value);
				}
			}
				
			public System.String FnIr
			{
				get
				{
					System.Double? data = entity.FnIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnIr = null;
					else entity.FnIr = Convert.ToDouble(value);
				}
			}
				
			public System.String FnIof
			{
				get
				{
					System.Double? data = entity.FnIof;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnIof = null;
					else entity.FnIof = Convert.ToDouble(value);
				}
			}
				
			public System.String FnIofcomp
			{
				get
				{
					System.Double? data = entity.FnIofcomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnIofcomp = null;
					else entity.FnIofcomp = Convert.ToDouble(value);
				}
			}
				
			public System.String FnTxcomp
			{
				get
				{
					System.Double? data = entity.FnTxcomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnTxcomp = null;
					else entity.FnTxcomp = Convert.ToDouble(value);
				}
			}
				
			public System.String FnIrcomp
			{
				get
				{
					System.Double? data = entity.FnIrcomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnIrcomp = null;
					else entity.FnIrcomp = Convert.ToDouble(value);
				}
			}
				
			public System.String FnQtdeoper
			{
				get
				{
					System.Double? data = entity.FnQtdeoper;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnQtdeoper = null;
					else entity.FnQtdeoper = Convert.ToDouble(value);
				}
			}
				
			public System.String FnPuoper
			{
				get
				{
					System.Double? data = entity.FnPuoper;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPuoper = null;
					else entity.FnPuoper = Convert.ToDouble(value);
				}
			}
				
			public System.String FnValorbruto
			{
				get
				{
					System.Double? data = entity.FnValorbruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnValorbruto = null;
					else entity.FnValorbruto = Convert.ToDouble(value);
				}
			}
				
			public System.String FnPucomp
			{
				get
				{
					System.Double? data = entity.FnPucomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPucomp = null;
					else entity.FnPucomp = Convert.ToDouble(value);
				}
			}
				
			public System.String FnValorcomp
			{
				get
				{
					System.Double? data = entity.FnValorcomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnValorcomp = null;
					else entity.FnValorcomp = Convert.ToDouble(value);
				}
			}
				
			public System.String FdDatcomp
			{
				get
				{
					System.DateTime? data = entity.FdDatcomp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatcomp = null;
					else entity.FdDatcomp = Convert.ToDateTime(value);
				}
			}
				
			public System.String FcComando
			{
				get
				{
					System.String data = entity.FcComando;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcComando = null;
					else entity.FcComando = Convert.ToString(value);
				}
			}
				
			public System.String FcIndiceger
			{
				get
				{
					System.String data = entity.FcIndiceger;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcIndiceger = null;
					else entity.FcIndiceger = Convert.ToString(value);
				}
			}
				
			public System.String FnPercentger
			{
				get
				{
					System.Double? data = entity.FnPercentger;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPercentger = null;
					else entity.FnPercentger = Convert.ToDouble(value);
				}
			}
				
			public System.String FnTxger
			{
				get
				{
					System.Double? data = entity.FnTxger;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnTxger = null;
					else entity.FnTxger = Convert.ToDouble(value);
				}
			}
				
			public System.String FcRemufaixa
			{
				get
				{
					System.String data = entity.FcRemufaixa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcRemufaixa = null;
					else entity.FcRemufaixa = Convert.ToString(value);
				}
			}
				
			public System.String FcControle
			{
				get
				{
					System.String data = entity.FcControle;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcControle = null;
					else entity.FcControle = Convert.ToString(value);
				}
			}
				
			public System.String FcTipoestoque
			{
				get
				{
					System.String data = entity.FcTipoestoque;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTipoestoque = null;
					else entity.FcTipoestoque = Convert.ToString(value);
				}
			}
				
			public System.String FcGrupa
			{
				get
				{
					System.String data = entity.FcGrupa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcGrupa = null;
					else entity.FcGrupa = Convert.ToString(value);
				}
			}
				
			public System.String FdDatlastro
			{
				get
				{
					System.DateTime? data = entity.FdDatlastro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatlastro = null;
					else entity.FdDatlastro = Convert.ToDateTime(value);
				}
			}
				
			public System.String FnBollastro
			{
				get
				{
					System.Double? data = entity.FnBollastro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnBollastro = null;
					else entity.FnBollastro = Convert.ToDouble(value);
				}
			}
				
			public System.String FnPulastro
			{
				get
				{
					System.Double? data = entity.FnPulastro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnPulastro = null;
					else entity.FnPulastro = Convert.ToDouble(value);
				}
			}
				
			public System.String FcStatus
			{
				get
				{
					System.String data = entity.FcStatus;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcStatus = null;
					else entity.FcStatus = Convert.ToString(value);
				}
			}
				
			public System.String FdHoraenvio
			{
				get
				{
					System.DateTime? data = entity.FdHoraenvio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdHoraenvio = null;
					else entity.FdHoraenvio = Convert.ToDateTime(value);
				}
			}
				
			public System.String FdAgendamento
			{
				get
				{
					System.DateTime? data = entity.FdAgendamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdAgendamento = null;
					else entity.FdAgendamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String FdDatliquidacao
			{
				get
				{
					System.DateTime? data = entity.FdDatliquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatliquidacao = null;
					else entity.FdDatliquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String FcCodmensagem
			{
				get
				{
					System.String data = entity.FcCodmensagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcCodmensagem = null;
					else entity.FcCodmensagem = Convert.ToString(value);
				}
			}
				
			public System.String FcTipolei
			{
				get
				{
					System.String data = entity.FcTipolei;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTipolei = null;
					else entity.FcTipolei = Convert.ToString(value);
				}
			}
				
			public System.String FcControleif
			{
				get
				{
					System.String data = entity.FcControleif;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcControleif = null;
					else entity.FcControleif = Convert.ToString(value);
				}
			}
				
			public System.String FcNivelpref
			{
				get
				{
					System.String data = entity.FcNivelpref;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNivelpref = null;
					else entity.FcNivelpref = Convert.ToString(value);
				}
			}
				
			public System.String FcChaveassoc
			{
				get
				{
					System.String data = entity.FcChaveassoc;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcChaveassoc = null;
					else entity.FcChaveassoc = Convert.ToString(value);
				}
			}
				
			public System.String FcNumopsel
			{
				get
				{
					System.String data = entity.FcNumopsel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNumopsel = null;
					else entity.FcNumopsel = Convert.ToString(value);
				}
			}
				
			public System.String FcNumopselret
			{
				get
				{
					System.String data = entity.FcNumopselret;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNumopselret = null;
					else entity.FcNumopselret = Convert.ToString(value);
				}
			}
				
			public System.String FcTpleilao
			{
				get
				{
					System.String data = entity.FcTpleilao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcTpleilao = null;
					else entity.FcTpleilao = Convert.ToString(value);
				}
			}
				
			public System.String FcNsu
			{
				get
				{
					System.String data = entity.FcNsu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcNsu = null;
					else entity.FcNsu = Convert.ToString(value);
				}
			}
				
			public System.String FcRdcret
			{
				get
				{
					System.String data = entity.FcRdcret;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcRdcret = null;
					else entity.FcRdcret = Convert.ToString(value);
				}
			}
				
			public System.String FcDestino
			{
				get
				{
					System.String data = entity.FcDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcDestino = null;
					else entity.FcDestino = Convert.ToString(value);
				}
			}
				
			public System.String FcObs
			{
				get
				{
					System.String data = entity.FcObs;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcObs = null;
					else entity.FcObs = Convert.ToString(value);
				}
			}
				
			public System.String FnCodbolPre
			{
				get
				{
					System.Double? data = entity.FnCodbolPre;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnCodbolPre = null;
					else entity.FnCodbolPre = Convert.ToDouble(value);
				}
			}
				
			public System.String FlBrokerces
			{
				get
				{
					System.Int32? data = entity.FlBrokerces;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlBrokerces = null;
					else entity.FlBrokerces = Convert.ToInt32(value);
				}
			}
				
			public System.String FlBrokerced
			{
				get
				{
					System.Int32? data = entity.FlBrokerced;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FlBrokerced = null;
					else entity.FlBrokerced = Convert.ToInt32(value);
				}
			}
				
			public System.String FdDatcomptermo
			{
				get
				{
					System.DateTime? data = entity.FdDatcomptermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatcomptermo = null;
					else entity.FdDatcomptermo = Convert.ToDateTime(value);
				}
			}
				
			public System.String FnBase
			{
				get
				{
					System.Int32? data = entity.FnBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FnBase = null;
					else entity.FnBase = Convert.ToInt32(value);
				}
			}
				
			public System.String FdDatcompini
			{
				get
				{
					System.DateTime? data = entity.FdDatcompini;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FdDatcompini = null;
					else entity.FdDatcompini = Convert.ToDateTime(value);
				}
			}
				
			public System.String FcUnilateralidade
			{
				get
				{
					System.String data = entity.FcUnilateralidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcUnilateralidade = null;
					else entity.FcUnilateralidade = Convert.ToString(value);
				}
			}
				
			public System.String FcUsuario
			{
				get
				{
					System.String data = entity.FcUsuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcUsuario = null;
					else entity.FcUsuario = Convert.ToString(value);
				}
			}
				
			public System.String FcOrigem
			{
				get
				{
					System.String data = entity.FcOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FcOrigem = null;
					else entity.FcOrigem = Convert.ToString(value);
				}
			}
			

			private esOpdiabol entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOpdiabolQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOpdiabol can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Opdiabol : esOpdiabol
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOpdiabolQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OpdiabolMetadata.Meta();
			}
		}	
		

		public esQueryItem FnCodbol
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnCodbol, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnCodbolreferencia
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnCodbolreferencia, esSystemType.Double);
			}
		} 
		
		public esQueryItem FcMatricula
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcMatricula, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodempresa
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcCodempresa, esSystemType.String);
			}
		} 
		
		public esQueryItem FcAssessor
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcAssessor, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodliquida
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcCodliquida, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTpoper
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcTpoper, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodtitulo
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcCodtitulo, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodmercado
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcCodmercado, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodbroker
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcCodbroker, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodlimite
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcCodlimite, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodbanco
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcCodbanco, esSystemType.String);
			}
		} 
		
		public esQueryItem FcConta
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcConta, esSystemType.String);
			}
		} 
		
		public esQueryItem FcIfcedente
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcIfcedente, esSystemType.String);
			}
		} 
		
		public esQueryItem FcIfcessionario
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcIfcessionario, esSystemType.String);
			}
		} 
		
		public esQueryItem FcCodvinculo
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcCodvinculo, esSystemType.String);
			}
		} 
		
		public esQueryItem FdAquisicao
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FdAquisicao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FdDatoper
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FdDatoper, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FdHorabol
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FdHorabol, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FdHoracombinada
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FdHoracombinada, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FnTxnominal
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnTxnominal, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnFir
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnFir, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnPuaquisicao
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnPuaquisicao, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnTxoper
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnTxoper, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnIr
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnIr, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnIof
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnIof, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnIofcomp
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnIofcomp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnTxcomp
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnTxcomp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnIrcomp
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnIrcomp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnQtdeoper
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnQtdeoper, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnPuoper
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnPuoper, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnValorbruto
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnValorbruto, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnPucomp
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnPucomp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnValorcomp
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnValorcomp, esSystemType.Double);
			}
		} 
		
		public esQueryItem FdDatcomp
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FdDatcomp, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FcComando
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcComando, esSystemType.String);
			}
		} 
		
		public esQueryItem FcIndiceger
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcIndiceger, esSystemType.String);
			}
		} 
		
		public esQueryItem FnPercentger
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnPercentger, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnTxger
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnTxger, esSystemType.Double);
			}
		} 
		
		public esQueryItem FcRemufaixa
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcRemufaixa, esSystemType.String);
			}
		} 
		
		public esQueryItem FcControle
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcControle, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTipoestoque
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcTipoestoque, esSystemType.String);
			}
		} 
		
		public esQueryItem FcGrupa
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcGrupa, esSystemType.String);
			}
		} 
		
		public esQueryItem FdDatlastro
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FdDatlastro, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FnBollastro
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnBollastro, esSystemType.Double);
			}
		} 
		
		public esQueryItem FnPulastro
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnPulastro, esSystemType.Double);
			}
		} 
		
		public esQueryItem FcStatus
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcStatus, esSystemType.String);
			}
		} 
		
		public esQueryItem FdHoraenvio
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FdHoraenvio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FdAgendamento
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FdAgendamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FdDatliquidacao
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FdDatliquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FcCodmensagem
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcCodmensagem, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTipolei
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcTipolei, esSystemType.String);
			}
		} 
		
		public esQueryItem FcControleif
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcControleif, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNivelpref
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcNivelpref, esSystemType.String);
			}
		} 
		
		public esQueryItem FcChaveassoc
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcChaveassoc, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNumopsel
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcNumopsel, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNumopselret
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcNumopselret, esSystemType.String);
			}
		} 
		
		public esQueryItem FcTpleilao
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcTpleilao, esSystemType.String);
			}
		} 
		
		public esQueryItem FcNsu
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcNsu, esSystemType.String);
			}
		} 
		
		public esQueryItem FcRdcret
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcRdcret, esSystemType.String);
			}
		} 
		
		public esQueryItem FcDestino
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcDestino, esSystemType.String);
			}
		} 
		
		public esQueryItem FcObs
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcObs, esSystemType.String);
			}
		} 
		
		public esQueryItem FnCodbolPre
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnCodbolPre, esSystemType.Double);
			}
		} 
		
		public esQueryItem FlBrokerces
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FlBrokerces, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FlBrokerced
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FlBrokerced, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FdDatcomptermo
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FdDatcomptermo, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FnBase
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FnBase, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FdDatcompini
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FdDatcompini, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FcUnilateralidade
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcUnilateralidade, esSystemType.String);
			}
		} 
		
		public esQueryItem FcUsuario
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcUsuario, esSystemType.String);
			}
		} 
		
		public esQueryItem FcOrigem
		{
			get
			{
				return new esQueryItem(this, OpdiabolMetadata.ColumnNames.FcOrigem, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OpdiabolCollection")]
	public partial class OpdiabolCollection : esOpdiabolCollection, IEnumerable<Opdiabol>
	{
		public OpdiabolCollection()
		{

		}
		
		public static implicit operator List<Opdiabol>(OpdiabolCollection coll)
		{
			List<Opdiabol> list = new List<Opdiabol>();
			
			foreach (Opdiabol emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OpdiabolMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OpdiabolQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Opdiabol(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Opdiabol();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OpdiabolQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OpdiabolQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OpdiabolQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Opdiabol AddNew()
		{
			Opdiabol entity = base.AddNewEntity() as Opdiabol;
			
			return entity;
		}

		public Opdiabol FindByPrimaryKey(System.Double fnCodbol)
		{
			return base.FindByPrimaryKey(fnCodbol) as Opdiabol;
		}


		#region IEnumerable<Opdiabol> Members

		IEnumerator<Opdiabol> IEnumerable<Opdiabol>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Opdiabol;
			}
		}

		#endregion
		
		private OpdiabolQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OPDIABOL' table
	/// </summary>

	[Serializable]
	public partial class Opdiabol : esOpdiabol
	{
		public Opdiabol()
		{

		}
	
		public Opdiabol(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OpdiabolMetadata.Meta();
			}
		}
		
		
		
		override protected esOpdiabolQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OpdiabolQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OpdiabolQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OpdiabolQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OpdiabolQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OpdiabolQuery query;
	}



	[Serializable]
	public partial class OpdiabolQuery : esOpdiabolQuery
	{
		public OpdiabolQuery()
		{

		}		
		
		public OpdiabolQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OpdiabolMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OpdiabolMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnCodbol, 0, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnCodbol;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 15;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnCodbolreferencia, 1, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnCodbolreferencia;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcMatricula, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcMatricula;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcCodempresa, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcCodempresa;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcAssessor, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcAssessor;
			c.CharacterMaxLength = 7;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcCodliquida, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcCodliquida;
			c.CharacterMaxLength = 7;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcTpoper, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcTpoper;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcCodtitulo, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcCodtitulo;
			c.CharacterMaxLength = 6;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcCodmercado, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcCodmercado;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcCodbroker, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcCodbroker;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcCodlimite, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcCodlimite;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcCodbanco, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcCodbanco;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcConta, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcConta;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcIfcedente, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcIfcedente;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcIfcessionario, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcIfcessionario;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcCodvinculo, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcCodvinculo;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FdAquisicao, 16, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FdAquisicao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FdDatoper, 17, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FdDatoper;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FdHorabol, 18, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FdHorabol;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FdHoracombinada, 19, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FdHoracombinada;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnTxnominal, 20, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnTxnominal;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnFir, 21, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnFir;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnPuaquisicao, 22, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnPuaquisicao;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnTxoper, 23, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnTxoper;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnIr, 24, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnIr;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnIof, 25, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnIof;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnIofcomp, 26, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnIofcomp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnTxcomp, 27, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnTxcomp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnIrcomp, 28, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnIrcomp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnQtdeoper, 29, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnQtdeoper;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnPuoper, 30, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnPuoper;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnValorbruto, 31, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnValorbruto;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnPucomp, 32, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnPucomp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnValorcomp, 33, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnValorcomp;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FdDatcomp, 34, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FdDatcomp;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcComando, 35, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcComando;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcIndiceger, 36, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcIndiceger;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnPercentger, 37, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnPercentger;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnTxger, 38, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnTxger;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcRemufaixa, 39, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcRemufaixa;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcControle, 40, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcControle;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcTipoestoque, 41, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcTipoestoque;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcGrupa, 42, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcGrupa;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FdDatlastro, 43, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FdDatlastro;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnBollastro, 44, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnBollastro;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnPulastro, 45, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnPulastro;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcStatus, 46, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcStatus;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FdHoraenvio, 47, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FdHoraenvio;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FdAgendamento, 48, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FdAgendamento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FdDatliquidacao, 49, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FdDatliquidacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcCodmensagem, 50, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcCodmensagem;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcTipolei, 51, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcTipolei;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcControleif, 52, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcControleif;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcNivelpref, 53, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcNivelpref;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcChaveassoc, 54, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcChaveassoc;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcNumopsel, 55, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcNumopsel;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcNumopselret, 56, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcNumopselret;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcTpleilao, 57, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcTpleilao;
			c.CharacterMaxLength = 4;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcNsu, 58, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcNsu;
			c.CharacterMaxLength = 23;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcRdcret, 59, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcRdcret;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcDestino, 60, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcDestino;
			c.CharacterMaxLength = 11;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcObs, 61, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcObs;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnCodbolPre, 62, typeof(System.Double), esSystemType.Double);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnCodbolPre;	
			c.NumericPrecision = 15;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FlBrokerces, 63, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FlBrokerces;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FlBrokerced, 64, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FlBrokerced;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FdDatcomptermo, 65, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FdDatcomptermo;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FnBase, 66, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FnBase;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FdDatcompini, 67, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FdDatcompini;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcUnilateralidade, 68, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcUnilateralidade;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcUsuario, 69, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcUsuario;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OpdiabolMetadata.ColumnNames.FcOrigem, 70, typeof(System.String), esSystemType.String);
			c.PropertyName = OpdiabolMetadata.PropertyNames.FcOrigem;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OpdiabolMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string FnCodbol = "FN_CODBOL";
			 public const string FnCodbolreferencia = "FN_CODBOLREFERENCIA";
			 public const string FcMatricula = "FC_MATRICULA";
			 public const string FcCodempresa = "FC_CODEMPRESA";
			 public const string FcAssessor = "FC_ASSESSOR";
			 public const string FcCodliquida = "FC_CODLIQUIDA";
			 public const string FcTpoper = "FC_TPOPER";
			 public const string FcCodtitulo = "FC_CODTITULO";
			 public const string FcCodmercado = "FC_CODMERCADO";
			 public const string FcCodbroker = "FC_CODBROKER";
			 public const string FcCodlimite = "FC_CODLIMITE";
			 public const string FcCodbanco = "FC_CODBANCO";
			 public const string FcConta = "FC_CONTA";
			 public const string FcIfcedente = "FC_IFCEDENTE";
			 public const string FcIfcessionario = "FC_IFCESSIONARIO";
			 public const string FcCodvinculo = "FC_CODVINCULO";
			 public const string FdAquisicao = "FD_AQUISICAO";
			 public const string FdDatoper = "FD_DATOPER";
			 public const string FdHorabol = "FD_HORABOL";
			 public const string FdHoracombinada = "FD_HORACOMBINADA";
			 public const string FnTxnominal = "FN_TXNOMINAL";
			 public const string FnFir = "FN_FIR";
			 public const string FnPuaquisicao = "FN_PUAQUISICAO";
			 public const string FnTxoper = "FN_TXOPER";
			 public const string FnIr = "FN_IR";
			 public const string FnIof = "FN_IOF";
			 public const string FnIofcomp = "FN_IOFCOMP";
			 public const string FnTxcomp = "FN_TXCOMP";
			 public const string FnIrcomp = "FN_IRCOMP";
			 public const string FnQtdeoper = "FN_QTDEOPER";
			 public const string FnPuoper = "FN_PUOPER";
			 public const string FnValorbruto = "FN_VALORBRUTO";
			 public const string FnPucomp = "FN_PUCOMP";
			 public const string FnValorcomp = "FN_VALORCOMP";
			 public const string FdDatcomp = "FD_DATCOMP";
			 public const string FcComando = "FC_COMANDO";
			 public const string FcIndiceger = "FC_INDICEGER";
			 public const string FnPercentger = "FN_PERCENTGER";
			 public const string FnTxger = "FN_TXGER";
			 public const string FcRemufaixa = "FC_REMUFAIXA";
			 public const string FcControle = "FC_CONTROLE";
			 public const string FcTipoestoque = "FC_TIPOESTOQUE";
			 public const string FcGrupa = "FC_GRUPA";
			 public const string FdDatlastro = "FD_DATLASTRO";
			 public const string FnBollastro = "FN_BOLLASTRO";
			 public const string FnPulastro = "FN_PULASTRO";
			 public const string FcStatus = "FC_STATUS";
			 public const string FdHoraenvio = "FD_HORAENVIO";
			 public const string FdAgendamento = "FD_AGENDAMENTO";
			 public const string FdDatliquidacao = "FD_DATLIQUIDACAO";
			 public const string FcCodmensagem = "FC_CODMENSAGEM";
			 public const string FcTipolei = "FC_TIPOLEI";
			 public const string FcControleif = "FC_CONTROLEIF";
			 public const string FcNivelpref = "FC_NIVELPREF";
			 public const string FcChaveassoc = "FC_CHAVEASSOC";
			 public const string FcNumopsel = "FC_NUMOPSEL";
			 public const string FcNumopselret = "FC_NUMOPSELRET";
			 public const string FcTpleilao = "FC_TPLEILAO";
			 public const string FcNsu = "FC_NSU";
			 public const string FcRdcret = "FC_RDCRET";
			 public const string FcDestino = "FC_DESTINO";
			 public const string FcObs = "FC_OBS";
			 public const string FnCodbolPre = "FN_CODBOL_PRE";
			 public const string FlBrokerces = "FL_BROKERCES";
			 public const string FlBrokerced = "FL_BROKERCED";
			 public const string FdDatcomptermo = "FD_DATCOMPTERMO";
			 public const string FnBase = "FN_BASE";
			 public const string FdDatcompini = "FD_DATCOMPINI";
			 public const string FcUnilateralidade = "FC_UNILATERALIDADE";
			 public const string FcUsuario = "FC_USUARIO";
			 public const string FcOrigem = "FC_ORIGEM";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string FnCodbol = "FnCodbol";
			 public const string FnCodbolreferencia = "FnCodbolreferencia";
			 public const string FcMatricula = "FcMatricula";
			 public const string FcCodempresa = "FcCodempresa";
			 public const string FcAssessor = "FcAssessor";
			 public const string FcCodliquida = "FcCodliquida";
			 public const string FcTpoper = "FcTpoper";
			 public const string FcCodtitulo = "FcCodtitulo";
			 public const string FcCodmercado = "FcCodmercado";
			 public const string FcCodbroker = "FcCodbroker";
			 public const string FcCodlimite = "FcCodlimite";
			 public const string FcCodbanco = "FcCodbanco";
			 public const string FcConta = "FcConta";
			 public const string FcIfcedente = "FcIfcedente";
			 public const string FcIfcessionario = "FcIfcessionario";
			 public const string FcCodvinculo = "FcCodvinculo";
			 public const string FdAquisicao = "FdAquisicao";
			 public const string FdDatoper = "FdDatoper";
			 public const string FdHorabol = "FdHorabol";
			 public const string FdHoracombinada = "FdHoracombinada";
			 public const string FnTxnominal = "FnTxnominal";
			 public const string FnFir = "FnFir";
			 public const string FnPuaquisicao = "FnPuaquisicao";
			 public const string FnTxoper = "FnTxoper";
			 public const string FnIr = "FnIr";
			 public const string FnIof = "FnIof";
			 public const string FnIofcomp = "FnIofcomp";
			 public const string FnTxcomp = "FnTxcomp";
			 public const string FnIrcomp = "FnIrcomp";
			 public const string FnQtdeoper = "FnQtdeoper";
			 public const string FnPuoper = "FnPuoper";
			 public const string FnValorbruto = "FnValorbruto";
			 public const string FnPucomp = "FnPucomp";
			 public const string FnValorcomp = "FnValorcomp";
			 public const string FdDatcomp = "FdDatcomp";
			 public const string FcComando = "FcComando";
			 public const string FcIndiceger = "FcIndiceger";
			 public const string FnPercentger = "FnPercentger";
			 public const string FnTxger = "FnTxger";
			 public const string FcRemufaixa = "FcRemufaixa";
			 public const string FcControle = "FcControle";
			 public const string FcTipoestoque = "FcTipoestoque";
			 public const string FcGrupa = "FcGrupa";
			 public const string FdDatlastro = "FdDatlastro";
			 public const string FnBollastro = "FnBollastro";
			 public const string FnPulastro = "FnPulastro";
			 public const string FcStatus = "FcStatus";
			 public const string FdHoraenvio = "FdHoraenvio";
			 public const string FdAgendamento = "FdAgendamento";
			 public const string FdDatliquidacao = "FdDatliquidacao";
			 public const string FcCodmensagem = "FcCodmensagem";
			 public const string FcTipolei = "FcTipolei";
			 public const string FcControleif = "FcControleif";
			 public const string FcNivelpref = "FcNivelpref";
			 public const string FcChaveassoc = "FcChaveassoc";
			 public const string FcNumopsel = "FcNumopsel";
			 public const string FcNumopselret = "FcNumopselret";
			 public const string FcTpleilao = "FcTpleilao";
			 public const string FcNsu = "FcNsu";
			 public const string FcRdcret = "FcRdcret";
			 public const string FcDestino = "FcDestino";
			 public const string FcObs = "FcObs";
			 public const string FnCodbolPre = "FnCodbolPre";
			 public const string FlBrokerces = "FlBrokerces";
			 public const string FlBrokerced = "FlBrokerced";
			 public const string FdDatcomptermo = "FdDatcomptermo";
			 public const string FnBase = "FnBase";
			 public const string FdDatcompini = "FdDatcompini";
			 public const string FcUnilateralidade = "FcUnilateralidade";
			 public const string FcUsuario = "FcUsuario";
			 public const string FcOrigem = "FcOrigem";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OpdiabolMetadata))
			{
				if(OpdiabolMetadata.mapDelegates == null)
				{
					OpdiabolMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OpdiabolMetadata.meta == null)
				{
					OpdiabolMetadata.meta = new OpdiabolMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("FN_CODBOL", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_CODBOLREFERENCIA", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FC_MATRICULA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODEMPRESA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ASSESSOR", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODLIQUIDA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TPOPER", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODTITULO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODMERCADO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODBROKER", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODLIMITE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODBANCO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CONTA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_IFCEDENTE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_IFCESSIONARIO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CODVINCULO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FD_AQUISICAO", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FD_DATOPER", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FD_HORABOL", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FD_HORACOMBINADA", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FN_TXNOMINAL", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_FIR", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_PUAQUISICAO", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_TXOPER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_IR", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_IOF", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_IOFCOMP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_TXCOMP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_IRCOMP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_QTDEOPER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_PUOPER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_VALORBRUTO", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_PUCOMP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_VALORCOMP", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FD_DATCOMP", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FC_COMANDO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_INDICEGER", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FN_PERCENTGER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_TXGER", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FC_REMUFAIXA", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CONTROLE", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FC_TIPOESTOQUE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_GRUPA", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FD_DATLASTRO", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FN_BOLLASTRO", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FN_PULASTRO", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FC_STATUS", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FD_HORAENVIO", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FD_AGENDAMENTO", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FD_DATLIQUIDACAO", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FC_CODMENSAGEM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TIPOLEI", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CONTROLEIF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NIVELPREF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_CHAVEASSOC", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NUMOPSEL", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NUMOPSELRET", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_TPLEILAO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_NSU", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_RDCRET", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_DESTINO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_OBS", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FN_CODBOL_PRE", new esTypeMap("float", "System.Double"));
				meta.AddTypeMap("FL_BROKERCES", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FL_BROKERCED", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FD_DATCOMPTERMO", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FN_BASE", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FD_DATCOMPINI", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FC_UNILATERALIDADE", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_USUARIO", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FC_ORIGEM", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "OPDIABOL";
				meta.Destination = "OPDIABOL";
				
				meta.spInsert = "proc_OPDIABOLInsert";				
				meta.spUpdate = "proc_OPDIABOLUpdate";		
				meta.spDelete = "proc_OPDIABOLDelete";
				meta.spLoadAll = "proc_OPDIABOLLoadAll";
				meta.spLoadByPrimaryKey = "proc_OPDIABOLLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OpdiabolMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
