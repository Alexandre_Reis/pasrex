﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.InterfacesDB
{
    public partial class QuotesCollection : esQuotesCollection
	{
        /// <summary>
        /// Busca cotações mais recentes (somente para ações).
        /// </summary>
        /// <param name="data"></param>
        /// <param name="horario"></param>
        /// <param name="listaPapel"></param>
        public void BuscaCotacoesRecentesAcoes(DateTime data, DateTime horario, List<string> listaPapel)
        {
            this.es.Connection.Name = "CMA";

            this.QueryReset();

            this.Query.Select(this.Query.Papel, this.Query.Campo001, this.Query.Campo025);
            this.Query.Where(this.Query.Data.Equal(data),
                             this.Query.Papel.In(listaPapel),   
                             this.Query.Horario.GreaterThanOrEqual(horario));
            this.Query.Load();
        }

	}
}
