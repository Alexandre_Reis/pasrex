/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 03/07/2009 19:21:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.InterfacesDB
{

	[Serializable]
	abstract public class esQuotesCollection : esEntityCollection
	{
		public esQuotesCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "QuotesCollection";
		}

		#region Query Logic
		protected void InitQuery(esQuotesQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esQuotesQuery);
		}
		#endregion
		
		virtual public Quotes DetachEntity(Quotes entity)
		{
			return base.DetachEntity(entity) as Quotes;
		}
		
		virtual public Quotes AttachEntity(Quotes entity)
		{
			return base.AttachEntity(entity) as Quotes;
		}
		
		virtual public void Combine(QuotesCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Quotes this[int index]
		{
			get
			{
				return base[index] as Quotes;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Quotes);
		}
	}



	[Serializable]
	abstract public class esQuotes : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esQuotesQuery GetDynamicQuery()
		{
			return null;
		}

		public esQuotes()
		{

		}

		public esQuotes(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esQuotesQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 id)
		{
			esQuotesQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "Campo001": this.str.Campo001 = (string)value; break;							
						case "Campo025": this.str.Campo025 = (string)value; break;							
						case "Papel": this.str.Papel = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Horario": this.str.Horario = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Id = (System.Int32?)value;
							break;
						
						case "Campo001":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Campo001 = (System.Decimal?)value;
							break;
						
						case "Campo025":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Campo025 = (System.Decimal?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Horario":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Horario = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to quotes.Id
		/// </summary>
		virtual public System.Int32? Id
		{
			get
			{
				return base.GetSystemInt32(QuotesMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemInt32(QuotesMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to quotes.campo001
		/// </summary>
		virtual public System.Decimal? Campo001
		{
			get
			{
				return base.GetSystemDecimal(QuotesMetadata.ColumnNames.Campo001);
			}
			
			set
			{
				base.SetSystemDecimal(QuotesMetadata.ColumnNames.Campo001, value);
			}
		}
		
		/// <summary>
		/// Maps to quotes.campo025
		/// </summary>
		virtual public System.Decimal? Campo025
		{
			get
			{
				return base.GetSystemDecimal(QuotesMetadata.ColumnNames.Campo025);
			}
			
			set
			{
				base.SetSystemDecimal(QuotesMetadata.ColumnNames.Campo025, value);
			}
		}
		
		/// <summary>
		/// Maps to quotes.papel
		/// </summary>
		virtual public System.String Papel
		{
			get
			{
				return base.GetSystemString(QuotesMetadata.ColumnNames.Papel);
			}
			
			set
			{
				base.SetSystemString(QuotesMetadata.ColumnNames.Papel, value);
			}
		}
		
		/// <summary>
		/// Maps to quotes.data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(QuotesMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(QuotesMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to quotes.horario
		/// </summary>
		virtual public System.DateTime? Horario
		{
			get
			{
				return base.GetSystemDateTime(QuotesMetadata.ColumnNames.Horario);
			}
			
			set
			{
				base.SetSystemDateTime(QuotesMetadata.ColumnNames.Horario, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esQuotes entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Int32? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToInt32(value);
				}
			}
				
			public System.String Campo001
			{
				get
				{
					System.Decimal? data = entity.Campo001;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Campo001 = null;
					else entity.Campo001 = Convert.ToDecimal(value);
				}
			}
				
			public System.String Campo025
			{
				get
				{
					System.Decimal? data = entity.Campo025;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Campo025 = null;
					else entity.Campo025 = Convert.ToDecimal(value);
				}
			}
				
			public System.String Papel
			{
				get
				{
					System.String data = entity.Papel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Papel = null;
					else entity.Papel = Convert.ToString(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Horario
			{
				get
				{
					System.DateTime? data = entity.Horario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Horario = null;
					else entity.Horario = Convert.ToDateTime(value);
				}
			}
			

			private esQuotes entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esQuotesQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esQuotes can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Quotes : esQuotes
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esQuotesQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return QuotesMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, QuotesMetadata.ColumnNames.Id, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Campo001
		{
			get
			{
				return new esQueryItem(this, QuotesMetadata.ColumnNames.Campo001, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Campo025
		{
			get
			{
				return new esQueryItem(this, QuotesMetadata.ColumnNames.Campo025, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Papel
		{
			get
			{
				return new esQueryItem(this, QuotesMetadata.ColumnNames.Papel, esSystemType.String);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, QuotesMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Horario
		{
			get
			{
				return new esQueryItem(this, QuotesMetadata.ColumnNames.Horario, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("QuotesCollection")]
	public partial class QuotesCollection : esQuotesCollection, IEnumerable<Quotes>
	{
		public QuotesCollection()
		{

		}
		
		public static implicit operator List<Quotes>(QuotesCollection coll)
		{
			List<Quotes> list = new List<Quotes>();
			
			foreach (Quotes emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  QuotesMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new QuotesQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Quotes(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Quotes();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public QuotesQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new QuotesQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(QuotesQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Quotes AddNew()
		{
			Quotes entity = base.AddNewEntity() as Quotes;
			
			return entity;
		}

		public Quotes FindByPrimaryKey(System.Int32 id)
		{
			return base.FindByPrimaryKey(id) as Quotes;
		}


		#region IEnumerable<Quotes> Members

		IEnumerator<Quotes> IEnumerable<Quotes>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Quotes;
			}
		}

		#endregion
		
		private QuotesQuery query;
	}


	/// <summary>
	/// Encapsulates the 'quotes' table
	/// </summary>

	[Serializable]
	public partial class Quotes : esQuotes
	{
		public Quotes()
		{

		}
	
		public Quotes(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return QuotesMetadata.Meta();
			}
		}
		
		
		
		override protected esQuotesQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new QuotesQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public QuotesQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new QuotesQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(QuotesQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private QuotesQuery query;
	}



	[Serializable]
	public partial class QuotesQuery : esQuotesQuery
	{
		public QuotesQuery()
		{

		}		
		
		public QuotesQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class QuotesMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected QuotesMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(QuotesMetadata.ColumnNames.Id, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = QuotesMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(QuotesMetadata.ColumnNames.Campo001, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = QuotesMetadata.PropertyNames.Campo001;	
			c.NumericPrecision = 20;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(QuotesMetadata.ColumnNames.Campo025, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = QuotesMetadata.PropertyNames.Campo025;	
			c.NumericPrecision = 20;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(QuotesMetadata.ColumnNames.Papel, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = QuotesMetadata.PropertyNames.Papel;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(QuotesMetadata.ColumnNames.Data, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = QuotesMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(QuotesMetadata.ColumnNames.Horario, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = QuotesMetadata.PropertyNames.Horario;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public QuotesMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "Id";
			 public const string Campo001 = "campo001";
			 public const string Campo025 = "campo025";
			 public const string Papel = "papel";
			 public const string Data = "data";
			 public const string Horario = "horario";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string Campo001 = "Campo001";
			 public const string Campo025 = "Campo025";
			 public const string Papel = "Papel";
			 public const string Data = "Data";
			 public const string Horario = "Horario";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(QuotesMetadata))
			{
				if(QuotesMetadata.mapDelegates == null)
				{
					QuotesMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (QuotesMetadata.meta == null)
				{
					QuotesMetadata.meta = new QuotesMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Id", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("campo001", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("campo025", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("papel", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("horario", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "quotes";
				meta.Destination = "quotes";
				
				meta.spInsert = "proc_quotesInsert";				
				meta.spUpdate = "proc_quotesUpdate";		
				meta.spDelete = "proc_quotesDelete";
				meta.spLoadAll = "proc_quotesLoadAll";
				meta.spLoadByPrimaryKey = "proc_quotesLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private QuotesMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
