﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using Ionic.Zip;
using System.Xml;
using System.Configuration;

namespace RoboImportaCVM
{

    class Functions
    {
        public List<string> log;
        private DateTime _dataImportacao;
        private string _dataImportacaoString
        {
            get
            {
                return _dataImportacao.ToString("yyyy-MM-dd");
            }
        }
        private DataTable _dataTableFundos;
        private DataTable _dataTableInstituicoes;
        private Dictionary<string, int> _dicEstrategiaMapping = new Dictionary<string, int>();
        const string NOME_FUNDO_PROVISORIO = "CADASTRO FUNDO PENDENTE";

        private DataRow BuscaRowFundoAtivo(DataRow[] rows)
        {
            DataRow rowFundoAtivo = rows[0];
            foreach (DataRow row in rows)
            {
                if (row["datafim"] == DBNull.Value)
                {
                    rowFundoAtivo = row;
                }
            }
            return rowFundoAtivo;
        }

        private void ParseXMLCotas(string xmlString)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlString);

            const string NODE_INFORME_DIARIO = "/ROOT/INFORMES/INFORME_DIARIO";
            XmlNodeList informeDiarioNodeList = xmlDocument.SelectNodes(NODE_INFORME_DIARIO);

            if (informeDiarioNodeList.Count <= 0)
            {
                return;
            }

            Dictionary<string, int> dicEstrategiaMapping = this.InitDictionaryEstrategiaMapping();

            using (SqlConnection conn = this.CreateSqlConnection())
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandTimeout = 600;

                //Carregar in-memory a tabela de codigo dos fundos ou meter cnpj nessa tabela
                DataTable dataTableMapping = CarregaTabelaFundosAnbima(cmd, "*");
                _dataTableInstituicoes = CarregaTabelaInstituicoesAnbima(cmd, "*");

                const string COMMAND_TEXT = "delete from fundos_dia where codfundo = '{0}' and data = '{1}' " +
                    "insert into fundos_dia (codfundo, data, pl, valcota) values ('{0}', '{1}', {2}, {3})";

                foreach (XmlNode informeDiarioNode in informeDiarioNodeList)
                {
                    string cnpj = "", data = "", cota = "", pl = "";
                    #region Parse Child Values
                    foreach (XmlNode childInformeDiarioNode in informeDiarioNode.ChildNodes)
                    {
                        string name = childInformeDiarioNode.Name;
                        string value = childInformeDiarioNode.FirstChild.Value;

                        if (name == "CNPJ_FDO")
                        {
                            cnpj = value.PadLeft(14, '0');
                        }
                        else if (name == "DT_COMPTC")
                        {
                            data = value;
                        }
                        else if (name == "VL_TOTAL")
                        {
                            //ignore
                        }
                        else if (name == "VL_QUOTA")
                        {
                            cota = value.Replace(",", ".");
                        }
                        else if (name == "PATRIM_LIQ")
                        {
                            pl = value.Replace(",", ".");
                        }
                    }
                    #endregion

                    //Encontrar codfundo
                    string codFundo = "";
                    DataRow[] rows = dataTableMapping.Select("cnpj = '" + cnpj + "'");
                    if (rows.Length > 0)
                    {
                        DataRow rowFundoAtivo = rows[0];
                        if (rows.Length > 1)
                        {
                            rowFundoAtivo = BuscaRowFundoAtivo(rows);
                        }

                        codFundo = Convert.ToString(rowFundoAtivo["codfundo"]);
                    }
                    else
                    {
                        //Fundo não encontrado... Pode ser cota de um fundo que ainda não veio. 
                        //Criar um fundo com flag de não validado para depois tentar complementar (usar um nome tipo CADASTRO_PENDENTE)
                        codFundo = CriaCadastroFundoProvisorio(conn, cnpj);
                        dataTableMapping.Rows.Add(codFundo, null, null, null, null, null, null, null, null, null, null, cnpj,
                            null, null, null, null, null, null, null, null, null, null);
                    }

                    cmd.CommandText = String.Format(COMMAND_TEXT, codFundo, data, pl, cota);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private DataTable CarregaTabelaFundosAnbima(SqlCommand cmd, string camposSelect)
        {
            DataTable dataTableMapping = new DataTable();

            using (SqlDataAdapter adapterMapping = new SqlDataAdapter())
            {
                //Carregar in-memory a tabela de codigo dos fundos ou meter cnpj nessa tabela
                dataTableMapping.TableName = "fundos";
                cmd.CommandText = "select " + camposSelect + " from fundos";
                adapterMapping.SelectCommand = cmd;
                adapterMapping.Fill(dataTableMapping);
            }
            return dataTableMapping;
        }

        private DataTable CarregaTabelaInstituicoesAnbima(SqlCommand cmd, string camposSelect)
        {
            DataTable dataTableMapping = new DataTable();

            using (SqlDataAdapter adapterMapping = new SqlDataAdapter())
            {
                //Carregar in-memory a tabela de codigo dos fundos ou meter cnpj nessa tabela
                dataTableMapping.TableName = "instituicoes";
                cmd.CommandText = "select " + camposSelect + " from instituicoes";
                adapterMapping.SelectCommand = cmd;
                adapterMapping.Fill(dataTableMapping);
            }
            return dataTableMapping;
        }

        private string CriaInstituicao(SqlConnection conn, string nomeInstituicao)
        {
            string codInstituicao;

            nomeInstituicao = nomeInstituicao.Replace("'", "''");
            nomeInstituicao = nomeInstituicao.Length > 40 ? nomeInstituicao.Substring(0, 40) : nomeInstituicao;


            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandTimeout = 600;
                cmd.CommandText = "select max(codinst) from instituicoes where codinst < '999998'";
                object maxCodInstituicao = cmd.ExecuteScalar();
                codInstituicao = (Int32.Parse(maxCodInstituicao.ToString()) + 1).ToString();
            }

            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandTimeout = 600;
                cmd.CommandText = String.Format("insert into instituicoes (codinst, fantasia, codmae, instmae, instfin, empab, codcvm, instadm, datahora) " +
                "values ('{0}', '{1}', '{0}', 'S', 'N', 'N', null ,'N', '{2}')",
                codInstituicao, nomeInstituicao, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                cmd.ExecuteNonQuery();
            }

            return codInstituicao;
        }

        private Dictionary<string, int> InitDictionaryEstrategiaMapping()
        {
            _dicEstrategiaMapping = new Dictionary<string, int>();
            _dicEstrategiaMapping.Add("Fundo Cambial", 338);
            _dicEstrategiaMapping.Add("Fundo Multimercado", 211);
            _dicEstrategiaMapping.Add("Fundo de Ações", 197);
            _dicEstrategiaMapping.Add("Fundo Referenciado", 207);
            _dicEstrategiaMapping.Add("Fundo de Renda Fixa", 207);
            _dicEstrategiaMapping.Add("Fundo de Curto Prazo", 207);
            _dicEstrategiaMapping.Add("Fundo da Dívida Externa", 207);
            _dicEstrategiaMapping.Add("DEFAULT", 211);
            return _dicEstrategiaMapping;
        }

        private string GravaCadastroFundo(SqlConnection conn, string codFundo, string nomeAdministrador, string nomeFundo, string classeFundo, string dataInicioFundo,
            string cnpjFundo, string formaCondominio, string fundoExclusivo)
        {

            bool insert = String.IsNullOrEmpty(codFundo);

            if (insert)
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandTimeout = 600;
                    cmd.CommandText = "select max(codfundo) from fundos";
                    object maxCodFundo = cmd.ExecuteScalar();
                    codFundo = (Int32.Parse(maxCodFundo.ToString()) + 1).ToString();
                }
            }

            string codInstituicao;

            DataRow[] rowsInstituicoes = _dataTableInstituicoes.Select("fantasia = '" + nomeAdministrador.ToUpper() + "'");
            if (rowsInstituicoes.Length == 0)
            {
                //Criar instituicao
                codInstituicao = CriaInstituicao(conn, nomeAdministrador);

                //Inserir no datatable para encontrar numa busca posterior
                _dataTableInstituicoes.Rows.Add(codInstituicao, nomeAdministrador, null, null, null, null, null, null, null);
            }
            else
            {
                codInstituicao = Convert.ToString(rowsInstituicoes[0]["codinst"]);
            }

            //Utilizar tabela DE-PARA
            int codTipoFundo;
            if (_dicEstrategiaMapping.ContainsKey(classeFundo))
            {
                codTipoFundo = _dicEstrategiaMapping[classeFundo];
            }
            else
            {
                codTipoFundo = _dicEstrategiaMapping["DEFAULT"];
            }

            string fundoAberto = formaCondominio.ToUpper() == "ABERTO" ? "S" : "N";
            fundoExclusivo = fundoExclusivo.ToUpper() == "SIM" ? "S" : "N";

            const string PERFIL_COTA = "0";
            const string COTA_ABERTURA = "N";


            const string COMMAND_TEXT_INSERT = "insert into fundos (codfundo, codinst, fantasia, codtipo, dataini, perfil_cota, razaosoc, cnpj, aberto, exclusivo, cota_abertura) " +
            "values ('{0}', '{1}', '{2}', {3}, '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}')";

            const string COMMAND_TEXT_UPDATE = "update fundos set " +
                "codinst = '{0}'," +
                "fantasia = '{1}', " +
                "codtipo = {2}, " +
                "dataini = '{3}', " +
                "perfil_cota = '{4}', " +
                "razaosoc = '{5}', " +
                "cnpj = '{6}', " +
                "aberto = '{7}', " +
                "exclusivo = '{8}', " +
                "cota_abertura = '{9}' " +
                "where codfundo = '{10}'";

            nomeFundo = nomeFundo.Replace("'", "''");
            string nomeFantasiaFundo = nomeFundo.Length > 40 ? nomeFundo.Substring(0, 40) : nomeFundo;
            string razaoSocialFundo = nomeFundo.Length > 60 ? nomeFundo.Substring(0, 60) : nomeFundo;

            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandTimeout = 600;
                if (insert)
                {
                    cmd.CommandText = String.Format(COMMAND_TEXT_INSERT, codFundo, codInstituicao, nomeFantasiaFundo, codTipoFundo, dataInicioFundo, PERFIL_COTA,
                        razaoSocialFundo, cnpjFundo, fundoAberto, fundoExclusivo, COTA_ABERTURA);
                }
                else
                {
                    cmd.CommandText = String.Format(COMMAND_TEXT_UPDATE, codInstituicao, nomeFantasiaFundo, codTipoFundo, dataInicioFundo, PERFIL_COTA,
                        razaoSocialFundo, cnpjFundo, fundoAberto, fundoExclusivo, COTA_ABERTURA, codFundo);
                }

                cmd.ExecuteNonQuery();
            }
            return codFundo;
        }

        private string CriaCadastroFundoProvisorio(SqlConnection conn, string cnpjFundo)
        {

            const string NOME_ADMINISTRADOR_PROVISORIO = "CUSTODIANTE MÚLTIPLO";
            const string CLASSE_FUNDO_PROVISORIO = "Fundo Multimercado";

            string codFundo = GravaCadastroFundo(conn, null, NOME_ADMINISTRADOR_PROVISORIO, NOME_FUNDO_PROVISORIO, CLASSE_FUNDO_PROVISORIO,
                DateTime.Now.ToString("yyyy-MM-dd"), cnpjFundo, "N", "N");

            return codFundo;
        }

        private void ParseXMLCadastroFundos(string xmlString)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xmlString);

            const string NODE_CADASTRO = "/ROOT/PARTICIPANTES/CADASTRO";
            XmlNodeList cadastroNodeList = xmlDocument.SelectNodes(NODE_CADASTRO);

            Dictionary<string, int> dicEstrategiaMapping = this.InitDictionaryEstrategiaMapping();


            if (cadastroNodeList.Count <= 0)
            {
                return;
            }

            using (SqlConnection conn = this.CreateSqlConnection())
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandTimeout = 600;

                //Carregar in-memory a tabela de codigo dos fundos ou meter cnpj nessa tabela
                _dataTableFundos = CarregaTabelaFundosAnbima(cmd, "*");
                _dataTableInstituicoes = CarregaTabelaInstituicoesAnbima(cmd, "*");

                foreach (XmlNode cadastroNode in cadastroNodeList)
                {
                    string cnpjFundo = "", nomeFundo = "", formaCondominio = "", nomeAdministrador = "", dataInicioFundo = "", classeFundo = "",
                        fundoExclusivo = "";

                    #region Parse Child Values
                    foreach (XmlNode childCadastroNode in cadastroNode.ChildNodes)
                    {
                        string name = childCadastroNode.Name;
                        string value = childCadastroNode.FirstChild != null ? childCadastroNode.FirstChild.Value : null;

                        if (name == "CNPJ")
                        {
                            cnpjFundo = value.PadLeft(14, '0');
                        }
                        else if (name == "NOME")
                        {
                            nomeFundo = value;
                        }
                        else if (name == "NOME_ADMINISTRADOR")
                        {
                            nomeAdministrador = value;
                        }
                        else if (name == "DT_CONSTITUICAO")
                        {
                            dataInicioFundo = value;
                        }
                        else if (name == "CLASSE")
                        {
                            classeFundo = value;
                        }
                        else if (name == "FORMA_CONDOMINIO")
                        {
                            formaCondominio = value;
                        }
                        else if (name == "EXCLUSIVO")
                        {
                            fundoExclusivo = value;
                        }

                    }
                    #endregion

                    //Encontrar codfundo
                    cnpjFundo = cnpjFundo.Replace("-", "").Replace(".", "").Replace("/", "");
                    DataRow[] rows = _dataTableFundos.Select("cnpj = '" + cnpjFundo + "'");
                    if (rows.Length > 0)
                    {

                        DataRow rowFundoAtivo = rows[0];
                        if (rows.Length > 1)
                        {
                            rowFundoAtivo = BuscaRowFundoAtivo(rows);
                        }

                        //ATUALMENTE A ATUALIZACAO DO FUNDO ESTA OCORRENDO APENAS PARA FUNDOS COM CADASTRO PENDENTE (Cotas que chegaram antes do fundo)
                        //Fundo encontrado - analisar diferenças. Se houver diferencas, atualizar
                        if (Convert.ToString(rowFundoAtivo["fantasia"]).Trim() == NOME_FUNDO_PROVISORIO)
                        {
                            string codFundo = (string)rows[0]["codfundo"];

                            GravaCadastroFundo(conn, codFundo, nomeAdministrador, nomeFundo, classeFundo, dataInicioFundo, cnpjFundo, formaCondominio,
                                fundoExclusivo);
                        }
                    }
                    else
                    {
                        GravaCadastroFundo(conn, null, nomeAdministrador, nomeFundo, classeFundo, dataInicioFundo, cnpjFundo, formaCondominio,
                            fundoExclusivo);

                    }
                }
            }
        }

        private void DownloadFile(string zipFilePath, string tipoImportacao)
        {
            const int NUMERO_SISTEMA = 1296;
            const string SENHA_SISTEMA = "6375";

            br.gov.cvm.www.WsDownloadInfs service = new br.gov.cvm.www.WsDownloadInfs();
            service.Login(NUMERO_SISTEMA, SENHA_SISTEMA);

            string url;

            if (tipoImportacao == "cotas")
            {
                //Download de cotas
                url = service.solicAutorizDownloadArqEntregaPorData(209, _dataImportacaoString, "Atualizacao Sistema Financial");
                //string url = service.solicAutorizDownloadArqAnual(209, "Atualizacao Sistema Financial");

            }
            else
            {
                //Download de cadastro de fundos
                url = service.solicAutorizDownloadCadastro(_dataImportacaoString, "Atualizacao Sistema Financial");
            }


            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ProtocolVersion = Version.Parse("1.0");
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {

                using (Stream stream = response.GetResponseStream())
                {
                    using (FileStream fstream = new FileStream(zipFilePath, FileMode.Create))
                    {
                        byte[] buffer = new byte[8192];
                        int maxCount = buffer.Length;
                        int count;
                        while ((count = stream.Read(buffer, 0, maxCount)) > 0)
                        {
                            fstream.Write(buffer, 0, count);
                        }
                    }
                }
            }

        }

        private string ExtractFile(string pathDestino, string zipFilePath)
        {
            string xmlFilePath = "";
            using (ZipFile zipFile = ZipFile.Read(zipFilePath))
            {
                foreach (ZipEntry zipEntry in zipFile)
                {
                    xmlFilePath = pathDestino + @"\" + zipEntry.FileName;
                    zipEntry.Extract(pathDestino, ExtractExistingFileAction.OverwriteSilently);
                }
            }

            return xmlFilePath;
        }

        private bool ImportaCotas(DateTime dataImportacao)
        {
            _dataImportacao = dataImportacao;
            string pathDestino = AppDomain.CurrentDomain.BaseDirectory + @"downloads";
            string zipFilePath = pathDestino + @"\cvm_cotas_" + _dataImportacaoString + ".zip";
            string logMessage;

            if (File.Exists(zipFilePath))
            {
                //Já processado - não reprocessar
                logMessage = String.Format("{0} - Arquivo já importado: \"{1}\" - Não foi feita nova importação", DateTime.Now.ToString(), zipFilePath);
                log.Add(logMessage);
                Console.WriteLine(logMessage);

                return false;
            }

            try
            {
                DownloadFile(zipFilePath, "cotas");
            }
            catch (Exception e)
            {
                logMessage = String.Format("{0} - Erro ao fazer download do arquivo \"{1}\" - {2}", DateTime.Now.ToString(), zipFilePath, e.Message);
                log.Add(logMessage);
                Console.WriteLine(logMessage);

                return false;
            }

            string xmlFilePath = ExtractFile(pathDestino, zipFilePath);
            string xmlString = File.ReadAllText(xmlFilePath);

            try
            {
                ParseXMLCotas(xmlString);
            }
            catch (Exception e)
            {
                logMessage = String.Format("{0} - Erro ao fazer parse XML do arquivo baixado \"{1}\" - {2}", DateTime.Now.ToString(), zipFilePath, e.Message);
                log.Add(logMessage);
                Console.WriteLine(logMessage);

                //Apagar arquivo para forçar reprocessamento do ParseXML
                File.Delete(zipFilePath);
                return false;
            }

            logMessage = String.Format("{0} - Arquivo importado com sucesso: \"{1}\"", DateTime.Now.ToString(), zipFilePath);
            log.Add(logMessage);
            Console.WriteLine(logMessage);
            return true;
        }

        private bool ImportaCadastroFundos(DateTime dataImportacao)
        {
            _dataImportacao = dataImportacao;
            string pathDestino = AppDomain.CurrentDomain.BaseDirectory + @"downloads";
            string zipFilePath = pathDestino + @"\cvm_cadastrofundos_" + _dataImportacaoString + ".zip";
            string logMessage;

            if (File.Exists(zipFilePath))
            {
                //Já processado - não reprocessar
                logMessage = String.Format("{0} - Arquivo já importado: \"{1}\" - Não foi feita nova importação", DateTime.Now.ToString(), zipFilePath);
                log.Add(logMessage);
                Console.WriteLine(logMessage);

                return false;
            }

            try
            {
                DownloadFile(zipFilePath, "cadastrofundos");
            }
            catch (Exception e)
            {
                logMessage = String.Format("{0} - Erro ao fazer download do arquivo \"{1}\" - {2}", DateTime.Now.ToString(), zipFilePath, e.Message);
                log.Add(logMessage);
                Console.WriteLine(logMessage);

                return false;
            }

            string xmlFilePath = ExtractFile(pathDestino, zipFilePath);
            string xmlString = File.ReadAllText(xmlFilePath);
            try
            {
                ParseXMLCadastroFundos(xmlString);
            }
            catch (Exception e)
            {
                logMessage = String.Format("{0} - Erro ao fazer parse XML do arquivo baixado \"{1}\" - {2}", DateTime.Now.ToString(), zipFilePath, e.Message);
                log.Add(logMessage);
                Console.WriteLine(logMessage);

                File.Delete(zipFilePath);
                return false;
            }

            logMessage = String.Format("{0} - Arquivo importado com sucesso: \"{1}\"", DateTime.Now.ToString(), zipFilePath);
            log.Add(logMessage);
            Console.WriteLine(logMessage);
            return true;
        }

        private SqlConnection CreateSqlConnection()
        {
            string connectionString = ConfigurationManager.AppSettings["SIAnbidConnectionString"];
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            return conn;
        }

        public void ImportaCotasRecentes(DateTime? dataFimImportacao)
        {
            log = new List<string>();
            if (!dataFimImportacao.HasValue)
            {
                dataFimImportacao = DateTime.Today;
            }

            DateTime dataInicioImportacao = dataFimImportacao.Value.AddDays(-6);

            for (DateTime dataImportar = dataInicioImportacao; dataImportar <= dataFimImportacao; )
            {
                ImportaCotas(dataImportar);

                //Calcular proxima data util a importar
                dataImportar = dataImportar.AddDays(1);
                if (dataImportar.DayOfWeek == DayOfWeek.Saturday)
                {
                    dataImportar = dataImportar.AddDays(2);
                }
                else if (dataImportar.DayOfWeek == DayOfWeek.Sunday)
                {
                    dataImportar = dataImportar.AddDays(1);
                }
            }

            string logPath = AppDomain.CurrentDomain.BaseDirectory + @"logs";
            string logFilePath = logPath + @"\cotas_" + _dataImportacaoString + ".txt";

            System.IO.File.AppendAllLines(logFilePath, log.ToArray());
        }

        public void ImportaFundosRecentes(DateTime? dataFimImportacao)
        {
            log = new List<string>();
            if (!dataFimImportacao.HasValue)
            {
                dataFimImportacao = DateTime.Today;
            }
            
            DateTime dataInicioImportacao = dataFimImportacao.Value.AddDays(-6);

            for (DateTime dataImportar = dataInicioImportacao; dataImportar <= dataFimImportacao; )
            {
                ImportaCadastroFundos(dataImportar);

                //Calcular proxima data util a importar
                dataImportar = dataImportar.AddDays(1);
                if (dataImportar.DayOfWeek == DayOfWeek.Saturday)
                {
                    dataImportar = dataImportar.AddDays(2);
                }
                else if (dataImportar.DayOfWeek == DayOfWeek.Sunday)
                {
                    dataImportar = dataImportar.AddDays(1);
                }
            }

            string logPath = AppDomain.CurrentDomain.BaseDirectory + @"logs";
            string logFilePath = logPath + @"\cadastrofundos_" + _dataImportacaoString + ".txt";

            System.IO.File.AppendAllLines(logFilePath, log.ToArray());

        }
    }

    class Program
    {

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                System.Console.WriteLine("Informe o parametro de importacao. (-f para cadastro fundos ou -c para cotas. Opcionalmente -d:DATA para alterar data fim da importacao)");
                return;
            }

            DateTime? dataFimImportacao = null;
            bool importaFundos = false;
            bool importaCotas = false;
            Functions functions = new Functions();

            foreach (string arg in args)
            {
                string argLower = arg.ToLower();
                if (argLower == "-f")
                {
                    importaFundos = true;
                }
                else if (argLower == "-c")
                {
                    importaCotas = true;
                }
                else if (argLower.StartsWith("-d:"))
                {
                    dataFimImportacao = DateTime.Parse(argLower.Replace("-d:", ""));
                }
            }

            if (importaFundos)
            {
                functions.ImportaFundosRecentes(dataFimImportacao);
            }

            if (importaCotas)
            {
                functions.ImportaCotasRecentes(dataFimImportacao);
            }
            
        }


    }
}
