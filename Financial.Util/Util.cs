﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Financial.Common.Enums;
using System.Net;
using ICSharpCode.SharpZipLib.Zip;
using System.Configuration;
using System.IO.Compression;
using Financial.Util.Enums;
using Financial.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Financial.Util.Exceptions;
using System.Reflection;
using System.Diagnostics;
using Financial.Util.Properties;
using System.Globalization;
using Financial.BMF;
using Financial.BMF.Enums;
using Financial.BMF.Exceptions;
using EntitySpaces.Core;
using Financial.Bolsa;
using Financial.Util.ConfiguracaoSistema;
using System.Drawing;
using System.Text.RegularExpressions;
using FileHelpers;
using System.Net.Mail;
using System.Data;
using System.ComponentModel;
using Microsoft.Office.Interop.Excel;
using System.Web;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Web.UI;


namespace Financial.Util
{

    public class LogUtil
    {
        public static string LogStack()
        {
            StackTrace trace = new System.Diagnostics.StackTrace();
            string debug = "";
            foreach (StackFrame frame in trace.GetFrames())
            {
                MethodBase method = frame.GetMethod();
                if (method.Name.Equals("LogStack")) continue;
                debug += string.Format("{0}::{1}",
                    method.ReflectedType != null ? method.ReflectedType.Name : string.Empty,
                    method.Name);
            }
            return debug;
        }
    }

    public class EmailUtil
    {
        public void SendMail(string in_Body, string in_Subject, string in_From, string in_To)
        {
            MailAddress from = new MailAddress(in_From);

            List<string> listaTo = new List<string>();
            string listaEmail = in_To.Replace(",", ";");
            string[] emails = listaEmail.Split(new Char[] { ';' });
            foreach (string email in emails)
            {
                MailAddress to = new MailAddress(email);

                MailMessage msg = new MailMessage(from, to);
                msg.From = from;

                msg.IsBodyHtml = false;

                msg.Subject = in_Subject;
                msg.Body = in_Body;

                SmtpClient smtp = new SmtpClient();
                //smtp.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;

                string smtpEnableSSL = ConfigurationManager.AppSettings["SMTPEnableSSL"];

                if (smtpEnableSSL == "true")
                {
                    smtp.EnableSsl = true;
                }

                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

                string smtpUsername = ConfigurationManager.AppSettings["SMTPCredentialsUsername"];
                if (!string.IsNullOrEmpty(smtpUsername))
                {
                    string smtpPassword = ConfigurationManager.AppSettings["SMTPCredentialsPassword"];
                    smtp.Credentials = new System.Net.NetworkCredential(smtpUsername, smtpPassword);
                }

                smtp.Send(msg);
            }

        }
    }

    public class CollectionHelper
    {
        private CollectionHelper()
        {
        }

        public static System.Data.DataTable ConvertTo<T>(IList<T> list)
        {
            System.Data.DataTable table = CreateTable<T>();
            Type entityType = typeof(T);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach (T item in list)
            {
                DataRow row = table.NewRow();

                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item);
                }

                table.Rows.Add(row);
            }

            return table;
        }

        public static IList<T> ConvertTo<T>(IList<DataRow> rows)
        {
            IList<T> list = null;

            if (rows != null)
            {
                list = new List<T>();

                foreach (DataRow row in rows)
                {
                    T item = CreateItem<T>(row);
                    list.Add(item);
                }
            }

            return list;
        }

        public static IList<T> ConvertTo<T>(System.Data.DataTable table)
        {
            if (table == null)
            {
                return null;
            }

            List<DataRow> rows = new List<DataRow>();

            foreach (DataRow row in table.Rows)
            {
                rows.Add(row);
            }

            return ConvertTo<T>(rows);
        }

        public static T CreateItem<T>(DataRow row)
        {
            T obj = default(T);
            if (row != null)
            {
                obj = Activator.CreateInstance<T>();

                foreach (DataColumn column in row.Table.Columns)
                {
                    PropertyInfo prop = obj.GetType().GetProperty(column.ColumnName);
                    try
                    {
                        object value = row[column.ColumnName];
                        prop.SetValue(obj, value, null);
                    }
                    catch
                    {
                        // You can log something here
                        throw;
                    }
                }
            }

            return obj;
        }

        public static System.Data.DataTable CreateTable<T>()
        {
            Type entityType = typeof(T);
            System.Data.DataTable table = new System.Data.DataTable(entityType.Name);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, prop.PropertyType);
            }

            return table;
        }
    }

    #region Funções relativas a Utilitario
    /// <summary>
    ///  Classe com métodos Utilitarios
    /// </summary>
    public static class Utilitario
    {
        /// <summary>
        /// Verifica se o CNPJ é válido
        /// </summary>
        /// <param name="cnpj">Valor a ser verificado</param>
        /// <returns>Condição de validação</returns>
        public static bool ValidaCNPJ(string vrCnpj)
        {
            string CNPJ = vrCnpj.Replace(".", "");
            CNPJ = CNPJ.Replace("/", "");
            CNPJ = CNPJ.Replace("-", "");
            int[] digitos, soma, resultado;
            int nrDig;
            string ftmt;
            bool[] CNPJOk;
            ftmt = "6543298765432";
            digitos = new int[14];
            soma = new int[2];
            soma[0] = 0;
            soma[1] = 0;
            resultado = new int[2];
            resultado[0] = 0;
            resultado[1] = 0;
            CNPJOk = new bool[2];
            CNPJOk[0] = false;
            CNPJOk[1] = false;
            try
            {
                for (nrDig = 0; nrDig < 14; nrDig++)
                {
                    digitos[nrDig] = int.Parse(CNPJ.Substring(nrDig, 1));
                    if (nrDig <= 11) soma[0] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig + 1, 1)));
                    if (nrDig <= 12) soma[1] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig, 1)));
                }
                for (nrDig = 0; nrDig < 2; nrDig++)
                {
                    resultado[nrDig] = (soma[nrDig] % 11);
                    if ((resultado[nrDig] == 0) || (resultado[nrDig] == 1))
                        CNPJOk[nrDig] = (digitos[12 + nrDig] == 0);
                    else
                        CNPJOk[nrDig] = (digitos[12 + nrDig] == (11 - resultado[nrDig]));
                }
                return (CNPJOk[0] && CNPJOk[1]);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Verifica se o CPF é válido
        /// </summary>
        /// <param name="cpf"></param>
        /// <returns></returns>
        public static bool ValidaCPF(string vrCPF)
        {
            string valor = vrCPF.Replace(".", "");
            valor = valor.Replace("-", "");

            if (valor.Length != 11)
                return false;

            bool igual = true;
            for (int i = 1; i < 11 && igual; i++)
                if (valor[i] != valor[0])
                    igual = false;

            if (igual || valor == "12345678909")
                return false;

            int[] numeros = new int[11];

            for (int i = 0; i < 11; i++)
                numeros[i] = int.Parse(valor[i].ToString());

            int soma = 0;
            for (int i = 0; i < 9; i++)
                soma += (10 - i) * numeros[i];

            int resultado = soma % 11;
            if (resultado == 1 || resultado == 0)
            {
                if (numeros[9] != 0) return false;
            }
            else if (numeros[9] != 11 - resultado) return false;

            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += (11 - i) * numeros[i];

            resultado = soma % 11;
            if (resultado == 1 || resultado == 0)
            {
                if (numeros[10] != 0) return false;
            }
            else if (numeros[10] != 11 - resultado) return false;

            return true;
        }


        /// <summary>
        /// Classe com as Informações do Servidor Proxy
        /// </summary>
        public static class WebProxyInfo
        {
            private static string proxyServer = "";
            private static string proxyPort = "";
            private static string proxyLogin = "";
            private static string proxyPassword = "";
            private static string proxyDomain = "";

            /// <summary>
            /// Servidor Proxy
            /// </summary>
            public static string ProxyServer
            {
                get { return proxyServer; }
                set { proxyServer = value; }
            }

            /// <summary>
            /// Porta do Servidor Proxy
            /// </summary>
            public static string ProxyPort
            {
                get { return proxyPort; }
                set { proxyPort = value; }
            }

            /// <summary>
            /// Login do Proxy
            /// </summary>
            public static string ProxyLogin
            {
                get { return proxyLogin; }
                set { proxyLogin = value; }
            }

            /// <summary>
            /// Senha do Proxy
            /// </summary>
            public static string ProxyPassword
            {
                get { return proxyPassword; }
                set { proxyPassword = value; }
            }

            /// <summary>
            /// Dominio do Proxy
            /// </summary>
            public static string ProxyDomain
            {
                get { return proxyDomain; }
                set { proxyDomain = value; }
            }
        }

        #region Conversões
        /// <summary>
        /// Classe Usada para fazer Conversão Específica de Tipos
        /// </summary>
        public class ConvertTipos
        {
            /// <summary>
            /// Convert uma string em notação Cientifica para Decimal
            /// Formatos suportados: 1,35745780858339E+02
            ///                      1,35745780858339E-02
            ///                      1,35745780858339E05
            ///                      1,35745780858339E5
            /// 
            /// </summary>
            /// <param name="stringNotacaoCientifica">string em notação cientifica</param>
            /// <returns>Decimal representando a string em notação cientifica</returns>
            /// <exception cref="Exception">Lança Exceção quando a string passada não estiver em notação Cientifica</exception>
            public static decimal NotacaoCientificaToDecimal(string stringNotacaoCientifica)
            {
                stringNotacaoCientifica = stringNotacaoCientifica.Replace(",", ".");

                NumberFormatInfo formato = new NumberFormatInfo();
                formato.CurrencyDecimalSeparator = ".";

                decimal novoNumero = 0;
                //
                if (stringNotacaoCientifica.ToUpper().Trim().Contains("E"))
                {
                    string delimitador = "E";
                    string[] auxiliar = stringNotacaoCientifica.Split(delimitador.ToCharArray());

                    decimal numero = Convert.ToDecimal(auxiliar[0], formato);
                    int divisor = Convert.ToInt32(auxiliar[1]);
                    novoNumero = numero * (decimal)(Math.Pow(10.0, (double)divisor));
                }
                else
                {
                    throw new Exception("String is not in Cientific Notation");
                }
                return novoNumero;
            }
        }
        #endregion

        public static IWebProxy GetWebProxy()
        {
            IWebProxy proxy = null;

            //Checa se tem proxy no Web.Config
            string internetProxy = ConfigurationManager.AppSettings["InternetProxy"];
            if (!String.IsNullOrEmpty(internetProxy))
            {
                //Validate proxy address
                if (internetProxy.ToLower() == "default")
                {
                    proxy = WebRequest.GetSystemWebProxy();
                    if (proxy != null)
                    {
                        proxy.Credentials = CredentialCache.DefaultCredentials;
                        //proxy.UseDefaultCredentials = true;
                    }
                    else
                    {
                        throw new Exception("Não existe default proxy configurado");
                    }
                }
                else
                {

                    Uri proxyURI = new Uri(internetProxy);

                    string internetProxyUsername = ConfigurationManager.AppSettings["InternetProxyUsername"];
                    if (!String.IsNullOrEmpty(internetProxyUsername))
                    {
                        string proxyUsername = ConfigurationManager.AppSettings["InternetProxyUsername"];
                        string proxyPassword = ConfigurationManager.AppSettings["InternetProxyPassword"];
                        ICredentials credentials = new NetworkCredential(proxyUsername, proxyPassword);
                        proxy = new WebProxy(proxyURI, false, null, credentials);
                    }
                    else
                    {
                        proxy = new WebProxy(proxyURI);
                    }
                }
            }
            else
            {
                string proxyServer = WebProxyInfo.ProxyServer;
                proxy = new WebProxy(proxyServer, Convert.ToInt32(WebProxyInfo.ProxyPort));
                string proxyLogin = WebProxyInfo.ProxyLogin;

                if (!String.IsNullOrEmpty(proxyLogin))
                {
                    string proxyDomain = WebProxyInfo.ProxyDomain;

                    if (!String.IsNullOrEmpty(proxyDomain))
                    {
                        proxy.Credentials = new NetworkCredential(proxyLogin, WebProxyInfo.ProxyPassword, proxyDomain);
                    }
                    else
                    {
                        proxy.Credentials = new NetworkCredential(proxyLogin, WebProxyInfo.ProxyPassword);
                    }
                }
            }

            return proxy;
        }

        #region Stream to byte[]
        public static byte[] ConvertStreamToByteArray(Stream sr)
        {

            byte[] b;
            using (BinaryReader br = new BinaryReader(sr))
            {
                b = br.ReadBytes((int)sr.Length);
            }

            return b;
        }
        #endregion

        #region ConexaoInternet
        /// <summary>
        /// 
        /// </summary>
        public class Internet
        {
            /// <summary>
            /// Verifica se existe conexao com a internet
            /// </summary>
            /// throws SemConexaoComInternet
            public static bool IsConnectedToInternet()
            {
                return true;//funcionalidade desabilitada

                try
                {
                    // tenta Conectar no google
                    //HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create("http://www.google.com");
                    HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create("http://www.google.com.br");

                    //Checa se tem proxy no Windows ou no Web.Config
                    string proxyServer = WebProxyInfo.ProxyServer;
                    string internetProxy = ConfigurationManager.AppSettings["InternetProxy"];

                    if (!String.IsNullOrEmpty(proxyServer) || !String.IsNullOrEmpty(internetProxy))
                    {
                        IWebProxy proxy = GetWebProxy();
                        myHttpWebRequest.Proxy = proxy;
                    }


                    // Get the associated response for the above request.
                    HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                    myHttpWebResponse.Close();
                }
                catch (WebException e)
                {
                    throw e;

                    if (e.Status == WebExceptionStatus.NameResolutionFailure)
                    {
                        Console.WriteLine("Connection failure");
                        Console.WriteLine("Status Code : {0}", ((HttpWebResponse)e.Response).StatusCode);
                        Console.WriteLine("Status Description : {0}", ((HttpWebResponse)e.Response).StatusDescription);
                    }
                    throw new SemConexaoComInternet("Conexão com Internet não disponivel");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                return true;
            }

            /// <summary>
            /// Outro metodo mais rapido - em fase de teste
            /// </summary>
            /// <returns></returns>
            public static bool IsConnectedToInternet1()
            {
                try
                {
                    IPHostEntry ipHostEntry = Dns.GetHostByName("www.google.com");
                    return true;
                }
                catch
                {
                    return false; // host not reachable. 
                }
            }

            /// <summary>
            /// Define se Existe resposta de uma pagina http
            /// </summary>
            /// <returns></returns>
            public static bool IsConnectedToInternet(string endereco)
            {
                try
                {
                    IPHostEntry ipHostEntry = Dns.GetHostByName(endereco);
                    return true;
                }
                catch
                {
                    return false; // host not reachable. 
                }
            }
        }
        #endregion

        #region Clone
        /// <summary>
        ///  Clona um objeto passado.
        /// </summary>
        /// <param name="objetoClonar"></param>
        /// <returns>Novo objeto clonado</returns>
        public static Object Clone(Object objetoClonar)
        {
            // Serialize 
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, objetoClonar);

            // Deserialize into a new entity 
            ms.Position = 0;
            Object objetoClonado = bf.Deserialize(ms);
            ms.Close();

            return objetoClonado;
        }
        #endregion

        #region Pega string no meio de Outra string
        public static string[] GetStringInBetween(string strBegin, string strEnd, string strSource, bool includeBegin, bool includeEnd)
        {

            string[] result ={ "", "" };
            int iIndexOfBegin = strSource.IndexOf(strBegin);

            if (iIndexOfBegin != -1)
            {

                // include the Begin string if desired
                if (includeBegin)
                {
                    iIndexOfBegin -= strBegin.Length;
                }

                strSource = strSource.Substring(iIndexOfBegin + strBegin.Length);

                int iEnd = strSource.IndexOf(strEnd);

                if (iEnd != -1)
                {

                    // include the End string if desired
                    if (includeEnd)
                    {
                        iEnd += strEnd.Length;
                    }

                    result[0] = strSource.Substring(0, iEnd);

                    // advance beyond this segment
                    if (iEnd + strEnd.Length < strSource.Length)
                    {
                        result[1] = strSource.Substring(iEnd + strEnd.Length);
                    }
                }
            }
            else
            {
                // stay where we are
                result[1] = strSource;
            }

            return result;
        }
        #endregion

        #region Truncate
        /// <summary>
        /// Trunca um numero com determinado numero de casas decimais.
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="numeroDigitos"></param>
        /// <returns></returns>
        public static decimal Truncate(decimal valor, int numeroDigitos)
        {
            valor = Math.Round(valor, 17);
            Int64 valorInteiro = (Int64)Math.Truncate(valor) ;
            decimal parteDecimal = valor - valorInteiro;
            
            decimal stepper = (decimal)(Math.Pow(10.0, (double)numeroDigitos));
            Int64 numAux = (Int64)(stepper * parteDecimal);
            parteDecimal = (numAux / stepper);
            decimal valorTruncado = valorInteiro + parteDecimal;
            return valorTruncado;
        }
        #endregion

        #region RoundMax
        /// <summary>
        /// Faz o Round sempre arredondando o ,5 para cima.
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="numeroDigitos"></param>
        /// <returns></returns>
        public static decimal RoundMax(decimal valor, int numeroDigitos)
        {
            return Math.Round(valor, numeroDigitos, MidpointRounding.AwayFromZero);
        }
        #endregion

        #region Download
        /// <summary>
        /// Download File da internet.
        /// </summary>
        /// <param name="endereco">Endereço http do arquivo</param>
        /// <param name="pathDestino">path + nome do arquivo de Download</param>
        /// <returns></returns>
        /// throws EnderecoHttpInvalido
        // TODO: Testar com DownloadFileAsync
        public static bool DownloadFile(string endereco, string pathDestino)
        {
            try
            {
                WebClient webClient = new WebClient();
                const string FILE_CONTROLLER_URL = "FileController.ashx";
                string wsProviderURL = ConfigurationManager.AppSettings["WebServicesProviderUrl"];
                if (!string.IsNullOrEmpty(wsProviderURL))
                {
                    wsProviderURL = wsProviderURL + "/" + FILE_CONTROLLER_URL;
                    string downloadURLEncoded = HttpUtility.HtmlEncode(endereco);

                    string[] filenameSplitted = pathDestino.Split('/');
                    string filename = filenameSplitted[filenameSplitted.Length - 1];
                    string filenameEncoded = HttpUtility.HtmlEncode(filename);

                    string url = String.Format("{0}?url={1}&filename={2}", wsProviderURL, downloadURLEncoded, filenameEncoded);

                    webClient.DownloadFile(url, pathDestino);
                    FileInfo fileInfo = new FileInfo(pathDestino);
                    if (fileInfo.Length == 0)
                    {
                        File.Delete(pathDestino);
                        return false;
                    }

                    return true;
                }
            }
            catch
            {
                return false;
            }

            if (Utilitario.Internet.IsConnectedToInternet())
            {
                WebClient webClient = new WebClient();

                //Checa se tem proxy no Windows ou no Web.Config
                string proxyServer = WebProxyInfo.ProxyServer;
                string internetProxy = ConfigurationManager.AppSettings["InternetProxy"];

                if (!String.IsNullOrEmpty(proxyServer) || !String.IsNullOrEmpty(internetProxy))
                {

                    IWebProxy proxy = GetWebProxy();
                    webClient.Proxy = proxy;
                }
                else
                {
                    webClient.Proxy = GlobalProxySelection.GetEmptyWebProxy();
                }
                //  

                try
                {
                    webClient.DownloadFile(endereco, pathDestino);
                }
                catch (WebException e)
                {
                    if (e.Status == WebExceptionStatus.ProtocolError)
                    {
                        StringBuilder msg = new StringBuilder();
                        msg.AppendLine("Endereço http Inválido: (" + endereco + ")");
                        //msg.AppendLine("Status Code: " + ((HttpWebResponse)e.Response).StatusCode);
                        //msg.AppendLine("Status Description: " + ((HttpWebResponse)e.Response).StatusDescription);
                        //msg.AppendLine(e.Message + e.StackTrace);

                        throw new EnderecoHttpInvalido(msg.ToString());
                        //return false;
                    }
                    else
                    {
                        if (e.Status == WebExceptionStatus.NameResolutionFailure)
                        {
                            // Try Again
                            WebClient ClientAux = new WebClient();
                            ClientAux.DownloadFile(endereco, pathDestino);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message + e.StackTrace);
                    return false;
                }

                return true;
            }
            // Não está Conectado Na Internet
            else
            {
                return false;
            }
        }

        //versao alternativa utilizando HttpWebRequest - TO DO: Adaptar codigo de forma mais parecida com o DownloadFile
        public static bool DownloadFileUsingHttpWebRequest(string endereco, string pathDestino)
        {
            if (!Utilitario.Internet.IsConnectedToInternet())
            {
                return false;
            }

            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(endereco);

            //O codigo abaixo ignora erros de certificado (alguns orgaos como site da fazenda utilizam o horrendo certificado do SERPRO, por exemplo)
            System.Net.ServicePointManager.ServerCertificateValidationCallback +=
    delegate(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                            System.Security.Cryptography.X509Certificates.X509Chain chain,
                            System.Net.Security.SslPolicyErrors sslPolicyErrors)
    {
        return true;
    };

            //Checa se tem proxy no Windows ou no Web.Config
            string proxyServer = WebProxyInfo.ProxyServer;
            string internetProxy = ConfigurationManager.AppSettings["InternetProxy"];

            if (!String.IsNullOrEmpty(proxyServer) || !String.IsNullOrEmpty(internetProxy))
            {
                IWebProxy proxy = GetWebProxy();
                req.Proxy = proxy;
            }
            else
            {
                req.Proxy = GlobalProxySelection.GetEmptyWebProxy();
            }

            req.Credentials = CredentialCache.DefaultCredentials;
            req.UnsafeAuthenticatedConnectionSharing = true;
            req.UserAgent = "anything";

            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            using (Stream stream = resp.GetResponseStream())
            {
                using (FileStream fstream = new FileStream(pathDestino, FileMode.Create))
                {
                    byte[] buffer = new byte[8192];
                    int maxCount = buffer.Length;
                    int count;
                    while ((count = stream.Read(buffer, 0, maxCount)) > 0)
                        fstream.Write(buffer, 0, count);
                }
            }

            resp.Close();

            return true;
        }
        #endregion

        #region Unzip File
        /// <summary>
        /// Unzip all Files.
        /// </summary>
        /// <param name="arquivo">nome do arquivo para unzip</param>
        /// <param name="diretorioDestino">diretorio destino do arquivo</param>
        /// <returns></returns>
        public static bool UnzipFile(string arquivo, string diretorioDestino)
        {
            FastZip zip = new FastZip();
            string diretorioUnzip = diretorioDestino;
            string diretorioZip = diretorioUnzip + arquivo;

            string filter = "";

            try
            {
                zip.ExtractZip(diretorioZip, @diretorioUnzip, filter);
            }
            catch (Exception e)
            {
                Console.Write(e.Message + e.StackTrace);
            }
            return true;
        }

        /// <summary>
        /// Unzip Specific File
        /// </summary>
        /// <param name="arquivo">nome do arquivo para unzip</param>
        /// <param name="diretorioDestino">diretorio destino do arquivo</param>
        /// <param name="filtro">regular expressions separadas por ponto e virgula
        /// A filter is a sequence of independant regular expressions separated by semi-colons ';'
        /// Each expression can be prefixed by a plus '+' sign or a minus '-' sign to denote the expression
        /// is intended to include or exclude names.  If neither a plus or minus sign is found include is the default
        /// A given name is tested for inclusion before checking exclusions. Only names matching an include spec
        /// and not matching an exclude spec are deemed to match the filter.
        /// An empty filter matches any name.
        /// example: The following expression includes all name ending in '.dat' with the exception of 'dummy.dat'
        /// "+\.dat$;-^dummy\.dat$"
        /// </param>
        /// <returns></returns>
        public static bool UnzipFile(string arquivo, string diretorioDestino, string filtro)
        {
            FastZip zip = new FastZip();
            string diretorioUnzip = diretorioDestino;
            string diretorioZip = diretorioUnzip + arquivo;

            try
            {
                zip.ExtractZip(diretorioZip, @diretorioUnzip, filtro);
            }
            catch (Exception e)
            {
                Console.Write(e.Message + e.StackTrace);
            }
            return true;
        }

        /// <summary>
        ///  Ainda não testada: TODO
        /// </summary>
        /// <param name="arquivo"></param>
        /// <param name="pathDestino"></param>
        /// <returns></returns>
        public static bool UnzipFileTeste(string arquivo, string pathDestino)
        {

            string diretorioUnzip = Settings.Default.DiretorioBase;
            string diretorioZip = diretorioUnzip + arquivo;
            //string filter = "";

            using (ZipInputStream s = new ZipInputStream(File.OpenRead(diretorioZip)))
            {
                ZipEntry theEntry;
                while ((theEntry = s.GetNextEntry()) != null)
                {
                    int size = 2048;
                    byte[] data = new byte[2048];

                    bool decompress = s.CanDecompressEntry;
                    //Console.Write(s.);
                    while (true)
                    {
                        size = s.Read(data, 0, data.Length);
                        if (size > 0)
                        {
                            Console.Write(new ASCIIEncoding().GetString(data, 0, size));
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            return true;
        }
        #endregion

        #region Rename File
        /// <summary>
        /// Renomeia um arquivo.
        /// </summary>
        /// <param name="nomeArquivoCompleto">nome do arquivo completo</param>
        /// <param name="novoNomeArquivoCompleto">novo nome do arquivo completo</param>
        /// <returns>true se Arquivo renomeado para novo nome</returns>
        /// throws RenameFileArquivoInexistente 
        public static bool RenameFile(string nomeArquivoCompleto, string novoNomeArquivoCompleto)
        {
            if (!File.Exists(nomeArquivoCompleto.ToString()))
            {
                throw new RenameFileArquivoInexistente("Arquivo " + nomeArquivoCompleto + " não existe");
            }
            try
            {
                System.IO.File.Move(@nomeArquivoCompleto, @novoNomeArquivoCompleto.ToString());
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message + e.StackTrace);
            }
            return true;
        }
        #endregion

        #region Right String
        /// <summary>
        /// Retorna N caracteres de uma string começando da direita.
        /// </summary>
        /// <param name="stringRight">string que se deseja pegar os carecteres comecando pela direita</param>
        /// <param name="length">numero de caracteres desejados da string comecando pela direita</param>
        /// <returns>N caracteres de uma string começando da direita</returns>
        public static string Right(string stringRight, int length)
        {
            if (stringRight == "")
            {
                return "";
            }
            if (length > stringRight.Length)
            {
                length = stringRight.Length;
            }
            return stringRight.Substring(stringRight.Length - length, length);
        }
        #endregion

        #region Mid
        /// <summary>
        /// Override da função Substring, tratando strings com length menor que o passado.
        /// </summary>
        /// <param name="stringMid"></param>
        /// <param name="start"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string Mid(string stringMid, int start, int length)
        {
            if (stringMid == "")
            {
                return "";
            }
            if (length > stringMid.Length)
            {
                length = stringMid.Length;
            }
            return stringMid.Substring(start, length);
        }
        #endregion

        #region GZipDecompress

        /// <summary>
        /// Descompacta arquivo ZIP, usando o componente GZipDecompress.
        /// </summary>
        /// <param name="filename"></param>
        public static void GZipDecompress(string filename)
        {
            FileStream infile;
            try
            {
                // Open the file as a FileStream object.
                infile = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
                byte[] buffer = new byte[infile.Length];
                // Read the file to ensure it is readable.
                int count = infile.Read(buffer, 0, buffer.Length);
                if (count != buffer.Length)
                {
                    infile.Close();
                    Console.WriteLine("Test Failed: Unable to read data from file");
                    return;
                }
                infile.Close();
                MemoryStream ms = new MemoryStream();
                ms.Position = 0;
                GZipStream zipStream = new GZipStream(ms, CompressionMode.Decompress);
                byte[] decompressedBuffer = new byte[buffer.Length + 100];
                // Use the ReadAllBytesFromStream to read the stream.
                //int totalCount = GZipTest.ReadAllBytesFromStream(zipStream, decompressedBuffer);
                zipStream.Close();
            }
            catch (InvalidDataException)
            {
                Console.WriteLine("Error: The file being read contains invalid data.");
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Error:The file specified was not found.");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Error: path is a zero-length string, contains only white space, or contains one or more invalid characters");
            }
            catch (PathTooLongException)
            {
                Console.WriteLine("Error: The specified path, file name, or both exceed the system-defined maximum length. For example, on Windows-based platforms, paths must be less than 248 characters, and file names must be less than 260 characters.");
            }
            catch (DirectoryNotFoundException)
            {
                Console.WriteLine("Error: The specified path is invalid, such as being on an unmapped drive.");
            }
            catch (IOException)
            {
                Console.WriteLine("Error: An I/O error occurred while opening the file.");
            }
            catch (UnauthorizedAccessException)
            {
                Console.WriteLine("Error: path specified a file that is read-only, the path is a directory, or caller does not have the required permissions.");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Error: You must provide parameters for MyGZIP.");
            }
        }
        #endregion

        #region IsInteger
        /// <summary>
        /// 
        /// </summary>
        /// <param name="valor"></param>
        /// <returns>bool indicando que o valor passado pode ser convertido para inteiro.</returns>
        public static bool IsInteger(string valor)
        {
            try
            {
                Convert.ToInt32(valor);
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region IsCaracterEspecial
        /// <summary>
        /// 
        /// </summary>
        /// <param name="valor"></param>
        /// <returns>bool indicando se o caracter passado é caracter Especial.</returns>
        public static bool IsCaracterEspecial(char c)
        {
            List<char> l = new List<char>(new Char[] { 
                '!', '@', '#', '$', '%', '&', '*', '(', ')', '-', '_',
                '+', '=', '{', '[', '^', '~', '}', ']', '|', '<', 
                ',' , '>', '.' , ';', ':', '?', '*', '/', '\\', '\''
            });

            return l.Contains(c);
        }
        #endregion

        #region IsDate
        /// <summary>
        /// Testa se a data passada é válida. 
        /// TODO: Só Funciona para data em formato Português
        /// </summary>
        /// <param name="data">formato = "dd/MM/yyyy"</param>
        /// <returns></returns>        
        public static bool IsDate(string data)
        {
            CultureInfo culturaLocal = new CultureInfo("pt-BR");
            culturaLocal.DateTimeFormat.LongDatePattern = "dd/MM/yyyy";
            culturaLocal.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            culturaLocal.DateTimeFormat.DateSeparator = "/";

            try
            {
                Convert.ToDateTime(data, culturaLocal);
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region IsEmailValid
        public static bool IsEmailValid(string email)
        {
            return Regex.IsMatch(email, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
        }
        #endregion

        #region FormataData
        /// <summary>
        /// retorna uma string com a data formata no formato desejado
        /// </summary>
        /// <param name="data"></param>
        /// <param name="formato"></param>
        /// <returns></returns>        
        public static StringBuilder FormataData(DateTime data, PatternData formato)
        {
            string formatoString = StringEnum.GetStringValue(formato);
            return new StringBuilder(data.ToString(formatoString));
        }
        #endregion

        #region GetMimeType
        public static string GetMimeType(string fileName)
        {
            fileName = fileName.ToLower();

            string fileExtension = System.IO.Path.GetExtension(fileName);
            string mimeType = "";

            if (fileExtension == ".gif")
            {
                mimeType = "image/gif";
            }
            else if (fileExtension == ".jpg")
            {
                mimeType = "image/jpeg";
            }
            else if (fileExtension == ".png")
            {
                mimeType = "image/png";
            }

            return mimeType;
        }
        #endregion

        #region MascaraCPF
        /// <summary>
        /// Retorna a string de CPF formatada com '/',  '.' e '-'
        /// Se o tamanho do número passado for diferente de 11, retorna string vazia.
        /// </summary>
        /// <param name="cpf">string de cpf com números apenas</param>
        /// <returns></returns>
        public static string MascaraCPF(string cpf)
        {

            if (String.IsNullOrEmpty(cpf))
            {
                return "";
            }

            cpf = cpf.Trim();
            if (cpf.Length != 11)
            {
                return cpf;
            }

            return cpf.Substring(0, 3) + "." + cpf.Substring(3, 3) + "." +
                   cpf.Substring(6, 3) + "-" + Right(cpf, 2);
        }
        #endregion

        #region MascaraCNPJ
        /// <summary>
        /// Retorna a string de CNPJ formatada com '/',  '.' e '-'
        /// Se o tamanho do número passado for diferente de 14, retorna string vazia.
        /// </summary>
        /// <param name="cnpj">string de cnpj com números apenas</param>
        /// <returns></returns>
        public static string MascaraCNPJ(string cnpj)
        {

            if (String.IsNullOrEmpty(cnpj))
            {
                return "";
            }

            cnpj = cnpj.Trim();

            if (cnpj.Length != 14)
            {
                return cnpj;
            }

            return cnpj.Substring(0, 2) + "." + cnpj.Substring(2, 3) + "." +
                   cnpj.Substring(5, 3) + "/" + cnpj.Substring(8, 4) + "-" + Right(cnpj, 2);
        }
        #endregion

        #region MascaraCEP
        /// <summary>
        /// Retorna a string de CEP no formato 11111-111
        /// Se o tamanho do número passado for diferente de 8 retorna sem fazer nada
        /// </summary>
        /// <param name="cep">string de cep com números apenas</param>
        /// <returns></returns>
        public static string MascaraCEP(string cep)
        {
            cep = cep.Trim();
            return (cep.Length == 8) ? cep.Substring(0, 5) + "-" + cep.Substring(5, 3) : cep;
        }

        /// <summary>
        /// Retorna a string de CEP no formato 11111-111 somente se Pessoa Morar No Brasil
        /// Se o tamanho do número passado for diferente de 8 retorna sem fazer nada
        /// Se o pais for diferente de Brasil retorna sem fazer nada
        /// </summary>
        /// <param name="cep">string de cep com números apenas</param>
        /// <param name="pais">pais</param>
        /// <returns></returns>
        public static string MascaraCEP(string cep, string pais)
        {
            cep = cep.Trim();
            pais = pais.Trim().ToLower();
            return (cep.Length == 8 && pais == "brasil") ? cep.Substring(0, 5) + "-" + cep.Substring(5, 3) : cep;
        }

        #endregion

        #region Mes em String
        /// <summary>
        /// Retorna o mês em string dado o numero do mes
        /// Numero do mes - entre 1 e 12
        /// </summary>
        /// <param name="mes"></param>
        /// <returns></returns>
        public static string RetornaMesString(int mes)
        {
            string mesRetorno = "";
            if (mes < 1 || mes > 12)
            {
                return "";
            }
            else
            {
                #region Define Mes
                switch (mes)
                {
                    case 1: mesRetorno = "Janeiro";
                        break;
                    case 2: mesRetorno = "Fevereiro";
                        break;
                    case 3: mesRetorno = "Março";
                        break;
                    case 4: mesRetorno = "Abril";
                        break;
                    case 5: mesRetorno = "Maio";
                        break;
                    case 6: mesRetorno = "Junho";
                        break;
                    case 7: mesRetorno = "Julho";
                        break;
                    case 8: mesRetorno = "Agosto";
                        break;
                    case 9: mesRetorno = "Setembro";
                        break;
                    case 10: mesRetorno = "Outubro";
                        break;
                    case 11: mesRetorno = "Novembro";
                        break;
                    case 12: mesRetorno = "Dezembro";
                        break;
                }
                #endregion
            }
            return mesRetorno;
        }
        #endregion

        #region ValorExtenso
        /// <summary>
        /// Dado um valor e um idioma, converte o valor decimal para um valor por Extenso
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="lingua">Para Traduções usar a Classe NumberToEnglish</param>
        /// <returns></returns>
        public static string ValorExtenso(decimal valor, PatternLingua lingua)
        {
            string valorExtenso;
            if (lingua == PatternLingua.Portugues)
            {
                valorExtenso = ValorExtensoPortugues(valor);
            }
            else
            {
                valorExtenso = ValorExtensoIngles(valor);
            }

            valorExtenso = valorExtenso.Trim();

            //Joga a 1a letra em maiusculo
            if (!String.IsNullOrEmpty(valorExtenso))
            {
                valorExtenso = valorExtenso.Substring(0, 1).ToUpper() + Utilitario.Right(valorExtenso, valorExtenso.Length - 1);
            }
            //
            return valorExtenso;
        }

        /// <summary>
        /// Retorna valor em extenso a partir de um valor decimal informado.
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        [Obsolete("Usar a Classe NumberToEnglish")]
        public static string ValorExtensoIngles(decimal valor)
        {

            //#region Versão Antiga

            //valor = Math.Round(valor, 2);
            //string valorExtenso = Convert.ToString(valor);

            //#region Carrega Lista de números
            //List<string> listaNumeros = new List<string>();
            //listaNumeros.Add(" one hundred");
            //listaNumeros.Add(" two hundred");
            //listaNumeros.Add(" three hundred");
            //listaNumeros.Add(" four hundred");
            //listaNumeros.Add(" five hundred");
            //listaNumeros.Add(" six hundred");
            //listaNumeros.Add(" seven hundred");
            //listaNumeros.Add(" eight hundred");
            //listaNumeros.Add(" nine hundred");

            //listaNumeros.Add(" ten");
            //listaNumeros.Add(" twenty");
            //listaNumeros.Add(" thirty");
            //listaNumeros.Add(" fourty");
            //listaNumeros.Add(" fifty");
            //listaNumeros.Add(" sixty");
            //listaNumeros.Add(" seventy");
            //listaNumeros.Add(" eighty");
            //listaNumeros.Add(" ninety");

            //listaNumeros.Add(" eleven");
            //listaNumeros.Add(" twelve");
            //listaNumeros.Add(" thirteen");
            //listaNumeros.Add(" fourteen");
            //listaNumeros.Add(" fifteen");
            //listaNumeros.Add(" sixteen");
            //listaNumeros.Add(" seventeen");
            //listaNumeros.Add(" eighteen");
            //listaNumeros.Add(" nineteen");

            //listaNumeros.Add(" one");
            //listaNumeros.Add(" two");
            //listaNumeros.Add(" three");
            //listaNumeros.Add(" four");
            //listaNumeros.Add(" five");
            //listaNumeros.Add(" six");
            //listaNumeros.Add(" seven");
            //listaNumeros.Add(" eight");
            //listaNumeros.Add(" nine");

            //listaNumeros.Add(" billion  billions");
            //listaNumeros.Add(" million  millions");
            //listaNumeros.Add(" thousand     thousand    ");
            //listaNumeros.Add("");
            //listaNumeros.Add("");
            //listaNumeros.Add("");
            //listaNumeros.Add("");
            //listaNumeros.Add("");
            //listaNumeros.Add("");
            //#endregion

            //if (!valorExtenso.Contains(",")) {
            //    valorExtenso = valorExtenso + ",00";
            //}
            //else {
            //    string aux = Right(valorExtenso, 2);
            //    if (Mid(aux, 0, 1) == ",") {
            //        valorExtenso = valorExtenso + "0";
            //    }
            //}

            //valorExtenso = Right("000000000000" + Mid(valorExtenso, 0, valorExtenso.IndexOf(",")) + "0" +
            //                     Mid(valorExtenso, valorExtenso.IndexOf(",") + 1, 2), 15);

            //string valorExtensoFinal = "";
            //for (int i = 1; i <= 5; i++) {
            //    if (i == 5 && valorExtensoFinal.Length != 0) {
            //        valorExtensoFinal += (valorExtensoFinal.Length > 3) ?
            //                                (Mid(valorExtenso, 3, 9) == "000000000" || Mid(valorExtenso, 6, 6) == "000000") ?
            //                                      " de " : " dollars" : " dollar";
            //    }
            //    string bloco = Mid(valorExtenso, i * 3 - 2 - 1, 3);

            //    for (int j = 1; j <= 3; j++) {
            //        decimal numero = Convert.ToInt32(Mid(bloco, j - 1, 1));
            //        if (j == 1) {
            //            if (numero != 0) {
            //                if (Right(bloco, 2) == "00" && numero == 1) {
            //                    valorExtensoFinal += " one hundred";
            //                }
            //                else {
            //                    valorExtensoFinal += listaNumeros[ValorExtensoAuxiliar(1, numero - 1, 9)];
            //                }
            //            }
            //        }
            //        if (j == 2) {
            //            if (numero != 0) {
            //                if (Convert.ToInt32(Right(bloco, 2)) > 10 && Convert.ToUInt32(Right(bloco, 2)) < 20) {
            //                    valorExtensoFinal += (valorExtensoFinal.Length != 0) ? " ," : "";
            //                    valorExtensoFinal += listaNumeros[ValorExtensoAuxiliar(3, Convert.ToInt32(Right(bloco, 1)) - 1, 9)];
            //                    j = 4;
            //                }
            //                else {
            //                    valorExtensoFinal += (valorExtensoFinal.Length != 0) ? " ," : "";
            //                    valorExtensoFinal += listaNumeros[ValorExtensoAuxiliar(2, numero - 1, 9)];
            //                }
            //            }
            //        }
            //        if (j == 3) {
            //            if (numero != 0) {
            //                valorExtensoFinal += (valorExtensoFinal.Length != 0) ? " ," : "";
            //                valorExtensoFinal += listaNumeros[ValorExtensoAuxiliar(4, numero - 1, 9)];
            //            }
            //        }
            //    }

            //    valorExtensoFinal += (bloco != "000" && valorExtensoFinal.Length != 0) ?
            //                                (Convert.ToInt32(bloco) > 1) ? Right(listaNumeros[ValorExtensoAuxiliar(5, i, 9) - 1], 8).TrimEnd() :
            //                                                              (Mid(listaNumeros[ValorExtensoAuxiliar(5, i, 9) - 1], 0, 8).TrimEnd())
            //                                                              :
            //                                                              "";
            //}

            //if (valorExtensoFinal.Length != 0 && Right(valorExtenso, 2) != "00") {
            //    valorExtensoFinal = valorExtensoFinal.Trim() +
            //                        ((Convert.ToInt32(Right(valorExtenso, 2)) > 1) ? " cents" : " cent");
            //}

            //if (Right(valorExtensoFinal, 8) != "cents" && Right(valorExtensoFinal, 7) != "cent" && Right(valorExtensoFinal, 5) != "dollars") {
            //    valorExtensoFinal += "dollars";
            //}

            //if (valorExtensoFinal != "") {
            //    valorExtensoFinal = Mid(valorExtensoFinal, 0, 1).ToUpper() +
            //                                Mid(valorExtensoFinal, 1, valorExtensoFinal.Length - 1);
            //}

            //return valorExtensoFinal;

            //#endregion

            NumberToEnglish n = new NumberToEnglish();

            // Valor String com "." para Decimal
            string numeroDecimal = Convert.ToString(valor);
            //
            return n.changeCurrencyToWords(numeroDecimal);
        }

        /// <summary>
        /// Retorna valor em extenso a partir de um valor decimal informado.
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static string ValorExtensoPortugues(decimal valor)
        {
            valor = Math.Round(valor, 2);

            if (valor == 1M)
            {
                return "Um real";
            }

            string valorExtenso = Convert.ToString(valor);

            #region Carrega Lista de números
            List<string> listaNumeros = new List<string>();
            listaNumeros.Add(" cento");
            listaNumeros.Add(" duzentos");
            listaNumeros.Add(" trezentos");
            listaNumeros.Add(" quatrocentos");
            listaNumeros.Add(" quinhentos");
            listaNumeros.Add(" seiscentos");
            listaNumeros.Add(" setecentos");
            listaNumeros.Add(" oitocentos");
            listaNumeros.Add(" novecentos");

            listaNumeros.Add(" dez");
            listaNumeros.Add(" vinte");
            listaNumeros.Add(" trinta");
            listaNumeros.Add(" quarenta");
            listaNumeros.Add(" cinquenta");
            listaNumeros.Add(" sessenta");
            listaNumeros.Add(" setenta");
            listaNumeros.Add(" oitenta");
            listaNumeros.Add(" noventa");

            listaNumeros.Add(" onze");
            listaNumeros.Add(" doze");
            listaNumeros.Add(" treze");
            listaNumeros.Add(" quatorze");
            listaNumeros.Add(" quinze");
            listaNumeros.Add(" dezesseis");
            listaNumeros.Add(" dezessete");
            listaNumeros.Add(" dezoito");
            listaNumeros.Add(" dezenove");

            listaNumeros.Add(" um");
            listaNumeros.Add(" dois");
            listaNumeros.Add(" três");
            listaNumeros.Add(" quatro");
            listaNumeros.Add(" cinco");
            listaNumeros.Add(" seis");
            listaNumeros.Add(" sete");
            listaNumeros.Add(" oito");
            listaNumeros.Add(" nove");

            listaNumeros.Add(" bilhão  bilhões");
            listaNumeros.Add(" milhão  milhões");
            listaNumeros.Add(" mil     mil    ");
            listaNumeros.Add("");
            listaNumeros.Add("");
            listaNumeros.Add("");
            listaNumeros.Add("");
            listaNumeros.Add("");
            listaNumeros.Add("");
            #endregion

            if (!valorExtenso.Contains(","))
            {
                valorExtenso = valorExtenso + ",00";
            }
            else
            {
                string aux = Right(valorExtenso, 2);
                if (Mid(aux, 0, 1) == ",")
                {
                    valorExtenso = valorExtenso + "0";
                }
            }

            valorExtenso = Right("000000000000" + Mid(valorExtenso, 0, valorExtenso.IndexOf(",")) + "0" +
                                 Mid(valorExtenso, valorExtenso.IndexOf(",") + 1, 2), 15);

            string valorExtensoFinal = "";
            for (int i = 1; i <= 5; i++)
            {
                if (i == 5 && valorExtensoFinal.Length != 0)
                {
                    valorExtensoFinal += (valorExtensoFinal.Length > 3) ?
                                            (Mid(valorExtenso, 3, 9) == "000000000" || Mid(valorExtenso, 6, 6) == "000000") ?
                                                  " de " : " reais" : " real";
                }
                string bloco = Mid(valorExtenso, i * 3 - 2 - 1, 3);

                for (int j = 1; j <= 3; j++)
                {
                    decimal numero = Convert.ToInt32(Mid(bloco, j - 1, 1));
                    if (j == 1)
                    {
                        if (numero != 0)
                        {
                            if (Right(bloco, 2) == "00" && numero == 1)
                            {
                                valorExtensoFinal += " cem";
                            }
                            else
                            {
                                valorExtensoFinal += listaNumeros[ValorExtensoAuxiliar(1, numero - 1, 9)];
                            }
                        }
                    }
                    if (j == 2)
                    {
                        if (numero != 0)
                        {
                            if (Convert.ToInt32(Right(bloco, 2)) > 10 && Convert.ToUInt32(Right(bloco, 2)) < 20)
                            {
                                valorExtensoFinal += (valorExtensoFinal.Length != 0) ? " e" : "";
                                valorExtensoFinal += listaNumeros[ValorExtensoAuxiliar(3, Convert.ToInt32(Right(bloco, 1)) - 1, 9)];
                                j = 4;
                            }
                            else
                            {
                                valorExtensoFinal += (valorExtensoFinal.Length != 0) ? " e" : "";
                                valorExtensoFinal += listaNumeros[ValorExtensoAuxiliar(2, numero - 1, 9)];
                            }
                        }
                    }
                    if (j == 3)
                    {
                        if (numero != 0)
                        {
                            valorExtensoFinal += (valorExtensoFinal.Length != 0) ? " e" : "";
                            valorExtensoFinal += listaNumeros[ValorExtensoAuxiliar(4, numero - 1, 9)];
                        }
                    }
                }

                valorExtensoFinal += (bloco != "000" && valorExtensoFinal.Length != 0) ?
                                            (Convert.ToInt32(bloco) > 1) ? Right(listaNumeros[ValorExtensoAuxiliar(5, i, 9) - 1], 8).TrimEnd() :
                                                                          (Mid(listaNumeros[ValorExtensoAuxiliar(5, i, 9) - 1], 0, 8).TrimEnd())
                                                                          :
                                                                          "";
            }

            if (valorExtensoFinal.Length != 0 && Right(valorExtenso, 2) != "00")
            {
                valorExtensoFinal = valorExtensoFinal.Trim() +
                                    ((Convert.ToInt32(Right(valorExtenso, 2)) > 1) ? " centavos" : " centavo");
            }

            if (Right(valorExtensoFinal, 8) != "centavos" && Right(valorExtensoFinal, 7) != "centavo" && Right(valorExtensoFinal, 5) != "reais")
            {
                valorExtensoFinal += "reais";
            }

            if (valorExtensoFinal != "")
            {
                valorExtensoFinal = Mid(valorExtensoFinal, 0, 1).ToUpper() +
                                            Mid(valorExtensoFinal, 1, valorExtensoFinal.Length - 1);
            }

            return valorExtensoFinal;
        }

        /// <summary>
        /// Função usada apenas pela ValorExtenso
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        private static int ValorExtensoAuxiliar(decimal x, decimal y, decimal z)
        {
            return (int)(((x - 1) * z) + y);
        }
        #endregion

        #region InsertString
        /// <summary>
        /// Retorna uma string com n casas preenchidas pelo char passado, de acordo com length passado.
        /// </summary>
        /// <param name="stringInsert"></param>
        /// <param name="length">numero de caracteres desejados da string comecando pela direita</param>
        /// <returns>N caracteres de uma string começando da direita</returns>
        public static string InsertString(string stringInsert, int length)
        {
            StringBuilder stringRetorno = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                stringRetorno.Append(stringInsert);
            }

            return stringRetorno.ToString();
        }
        #endregion

        #region ExtractNumbers
        /// <summary>
        /// Retorna uma string apenas com números.
        /// </summary>
        /// <param name="expr"></param>
        /// <returns></returns>
        public static string ExtractNumbers(string expr)
        {
            return string.Join(null, System.Text.RegularExpressions.Regex.Split(expr, "[^\\d]"));
        }
        #endregion

        #region Tratamentos de System.DBNull

        /// <summary>
        /// Trata Inserção de Datetime que pode ser Null no DataTable
        /// </summary>
        /// <param name="data"></param>
        /// <returns>DBNull para ser usado no banco ou a Data passada se essa tiver valor</returns>
        public static object DataTableTrataNullValue(DateTime? data)
        {
            if (data.HasValue)
            {
                return data;
            }
            else
            {
                return DBNull.Value;
            }
        }

        /// <summary>
        /// Trata Inserção de Valor Null no DataTable
        /// </summary>
        /// <param name="valor"></param>
        /// <returns>DBNull para ser usado no banco ou o valor decimal passado se esse tiver valor</returns>
        public static object DataTableTrataNullValue(decimal? valor)
        {
            if (valor.HasValue)
            {
                return valor;
            }
            else
            {
                return DBNull.Value;
            }
        }

        public static string CastColumnValueToStringTrim(object columnValue)
        {
            if (columnValue == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return ((string)columnValue).Trim();
            }
        }

        public static int? CastColumnValueToInt(object columnValue)
        {
            if (columnValue == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return (int)columnValue;
            }
        }

        public static short? CastColumnValueToShort(object columnValue)
        {
            if (columnValue == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return (short)columnValue;
            }
        }

        public static byte? CastColumnValueToByte(object columnValue)
        {
            if (columnValue == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return (byte)columnValue;
            }
        }

        public static decimal? CastColumnValueToDecimal(object columnValue)
        {
            if (columnValue == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return (decimal)columnValue;
            }
        }

        public static DateTime? CastColumnValueToDateTime(object columnValue)
        {
            if (columnValue == System.DBNull.Value)
            {
                return null;
            }
            else
            {
                return (DateTime)columnValue;
            }
        }
        #endregion

        #region RemoveCaracteresEspeciais
        public static string RemoveCaracteresEspeciais(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') | c == ' ')
                {
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }
        #endregion

        #region RemoveAcentos
        public static string RemoveAcentos(string texto)
        {
            string s = texto.Normalize(NormalizationForm.FormD);

            StringBuilder sb = new StringBuilder();

            for (int k = 0; k < s.Length; k++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(s[k]);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(s[k]);
                }
            }
            return sb.ToString();
        }
        #endregion

        #region Gerar Senha String Aleatoria
        /// <summary>
        /// Gera uma senha string aleatória.
        /// Usada para relatorios PDF com senha
        /// </summary>
        /// <returns></returns>
        public static string GeraSenha()
        {
            string guid = Guid.NewGuid().ToString().Replace("-", "");

            Random clsRan = new Random();
            Int32 tamanhoSenha = clsRan.Next(6, 18);

            string senha = "";
            for (Int32 i = 0; i <= tamanhoSenha; i++)
            {
                senha += guid.Substring(clsRan.Next(1, guid.Length), 1);
            }

            return senha;

            // Outro método
            /*
            string result;
            Random random = new Random();

            result = string.Empty;

            for (int i = 0; i < tamanho; i++)
                result += baseDados.Substring(random.Next(baseDados.Length), 1);

            return result;
            */
        }
        #endregion

        #region CalculaDV
        /// <summary>
        /// Calcula DV para codigos passados de cliente e corretora.
        /// </summary>
        /// <param name="codigoCliente"></param>
        /// <param name="codigoCorretora"></param>
        /// <returns></returns>
        public static int CalculaDV(int codigoCliente, int codigoCorretora)
        {
            char pad = '0';
            string strCliente = codigoCliente.ToString().PadLeft(7, pad);
            string strCorretora = codigoCorretora.ToString().PadLeft(5, pad);

            int c1 = Convert.ToInt32(strCliente.Substring(0, 1));
            int c2 = Convert.ToInt32(strCliente.Substring(1, 1));

            int u1 = Convert.ToInt32(strCorretora.Substring(0, 1));
            int u2 = Convert.ToInt32(strCorretora.Substring(1, 1));
            int u3 = Convert.ToInt32(strCorretora.Substring(2, 1));
            int u4 = Convert.ToInt32(strCorretora.Substring(3, 1));
            int u5 = Convert.ToInt32(strCorretora.Substring(4, 1));

            int c3 = Convert.ToInt32(strCliente.Substring(2, 1));
            int c4 = Convert.ToInt32(strCliente.Substring(3, 1));
            int c5 = Convert.ToInt32(strCliente.Substring(4, 1));
            int c6 = Convert.ToInt32(strCliente.Substring(5, 1));
            int c7 = Convert.ToInt32(strCliente.Substring(6, 1));

            int soma = (c1 * 5 + c2 * 4 + u1 * 3 + u2 * 2 + u3 * 9 + u4 * 8 + u5 * 7 + c3 * 6 + c4 * 5 + c5 * 4 + c6 * 3 + c7 * 2);
            int resto = 0;
            int quociente = Math.DivRem(soma, 11, out resto);

            int dv = resto > 1 ? (11 - resto) : 0;

            return dv;
        }
        #endregion

        /// <summary>
        /// Usada para pegar o IP da máquina, aplica-se somente a Windows Form!
        /// </summary>
        /// <returns></returns>
        public static string GetLocalIp()
        {
            System.Net.IPHostEntry ipEntry = System.Net.Dns.GetHostByName(Environment.MachineName);

            foreach (System.Net.IPAddress ip in ipEntry.AddressList)
            {
                if (System.Net.IPAddress.IsLoopback(ip) == false)
                    return ip.ToString();
            }

            return System.Net.IPAddress.Loopback.ToString();
        }
    }

    #region Classe para Traduzir Number em Texto em Inglês

    /// <summary>
    /// 
    /// </summary>
    public class NumberToEnglish
    {
        public String changeNumericToWords(double numb)
        {
            String num = numb.ToString();
            return changeToWords(num, false);
        }
        public String changeCurrencyToWords(String numb)
        {
            return changeToWords(numb, true);
        }
        public String changeNumericToWords(String numb)
        {
            return changeToWords(numb, false);
        }
        public String changeCurrencyToWords(double numb)
        {
            return changeToWords(numb.ToString(), true);
        }
        private String changeToWords(String numb, bool isCurrency)
        {
            String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            String endStr = (isCurrency) ? ("") : ("");
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = (isCurrency) ? ("and") : ("point");// just to separate whole numbers from points/cents
                        endStr = (isCurrency) ? ("Cents " + endStr) : ("");
                        pointStr = translateCents(points);
                    }
                }
                val = String.Format("{0} {1}{2} {3}", translateWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
            }
            catch { ;}
            return val;
        }
        private String translateWholeNumber(String number)
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX
                bool isDone = false;//test if already translated
                double dblAmt = (Convert.ToDouble(number));
                //if ((dblAmt > 0) && number.StartsWith("0"))
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric
                    beginsZero = number.StartsWith("0");
                    int numDigits = number.Length;
                    int pos = 0;//store digit grouping
                    String place = "";//digit grouping name:hundres,thousand,etc...
                    switch (numDigits)
                    {
                        case 1://ones' range
                            word = ones(number);
                            isDone = true;
                            break;
                        case 2://tens' range
                            word = tens(number);
                            isDone = true;
                            break;
                        case 3://hundreds' range
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;
                        case 10://Billions's range
                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                        //add extra case options for anything above Billion...
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)
                        word = translateWholeNumber(number.Substring(0, pos)) + place + translateWholeNumber(number.Substring(pos));
                        //check for trailing zeros
                        if (beginsZero) word = " and " + word.Trim();
                    }
                    //ignore digit grouping names
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { ;}
            return word.Trim();
        }
        private String tens(String digit)
        {
            int digt = Convert.ToInt32(digit);
            String name = null;
            switch (digt)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (digt > 0)
                    {
                        name = tens(digit.Substring(0, 1) + "0") + " " + ones(digit.Substring(1));
                    }
                    break;
            }
            return name;
        }
        private String ones(String digit)
        {
            int digt = Convert.ToInt32(digit);
            String name = "";
            switch (digt)
            {
                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }
        private String translateCents(String cents)
        {
            String cts = "", digit = "", engOne = "";
            for (int i = 0; i < cents.Length; i++)
            {
                digit = cents[i].ToString();
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = ones(digit);
                }
                cts += " " + engOne;
            }
            return cts;
        }
    }

    #endregion

    #endregion

    #region Funções relativas a Calendario
    public class DiasNaoUteisContainer
    {
        private IDictionary<DateTime, int> diasNaoUteis;
        private List<DateTime> feriados;

        public DiasNaoUteisContainer(int idLocal, DateTime dataInicio, DateTime dataFim)
        {
            this.diasNaoUteis = new Dictionary<DateTime, int>();
            this.feriados = new List<DateTime>();
            FeriadoCollection feriadoCollection = new FeriadoCollection();
            feriadoCollection.BuscaFeriado(idLocal, dataInicio, dataFim);
            int anoInicial = dataInicio.Year;
            int anoFinal = dataFim.Year;

            DateTime dataAux = new DateTime(anoInicial, 1, 1);
            DateTime dataLimite = new DateTime(anoFinal, 12, 31);

            foreach (Feriado feriado in feriadoCollection)
            {
                this.feriados.Add(feriado.Data.Value);
            }

            while (dataAux <= dataLimite)
            {
                if (Calendario.IsFinalSemana(dataAux) || feriados.Contains(dataAux))
                {
                    diasNaoUteis.Add(new KeyValuePair<DateTime, int>(dataAux, diasNaoUteis.Count));
                }

                dataAux = dataAux.AddDays(1);
            }
        }

        public DiasNaoUteisContainer()
        {
        }

        /// <summary>
        ///  Retorna Numero de dias entre duas Datas Considerando os dias Uteis e feriados.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="localFeriado"></param>
        /// <param name="tipoFeriado"></param>
        /// <returns></returns>
        public int CalculaNumeroDias(DateTime dataInicio, DateTime dataFim)
        {
            TimeSpan timeSpan = dataFim - dataInicio;
            int numeroDias = timeSpan.Days;
            KeyValuePair<DateTime, int>? kvpDataInicioNaoUtil = PegaDiaNaoUtilMaisProximo(dataInicio, true, numeroDias);
            KeyValuePair<DateTime, int>? kvpDataFimNaoUtil = PegaDiaNaoUtilMaisProximo(dataFim, false, numeroDias);

            //Se qualquer uma das datas mais próximas for NULL, não encontramos dias não úteis.
            //Se encontramos 1 dia não útil, as datas de Inicio e Fim são iguais e a diferença será 0.
            int numeroDiasNaoUteis = 0;
            if (kvpDataInicioNaoUtil != null)
            {
                numeroDiasNaoUteis = kvpDataFimNaoUtil.Value.Value - kvpDataInicioNaoUtil.Value.Value + 1;
            }

            //Posso ter um range de datas ou apenas uma data
            return numeroDias - numeroDiasNaoUteis;
        }

        public KeyValuePair<DateTime, int>? PegaDiaNaoUtilMaisProximo(DateTime data, bool futura, int numeroDias)
        {
            int numeroTestes = Math.Min(5, numeroDias);
            for (int i = 0; i <= numeroTestes; i++)
            {
                int daysToAdd = i * (futura ? 1 : -1);
                DateTime dataCandidata = data.AddDays(daysToAdd);
                if (this.diasNaoUteis.ContainsKey(dataCandidata))
                {
                    return new KeyValuePair<DateTime, int>(dataCandidata, this.diasNaoUteis[dataCandidata]);
                }
            }
            return null;
        }
    }

    /// <summary>
    ///  Funções relativas a Calendario
    /// </summary>
    public static class Calendario
    {
        private static Dictionary<string, DateTime[]> listaFeriados = new Dictionary<string, DateTime[]>();

        private static DateTime[] InicializaListaFeriados(int idLocal, TipoFeriado tipoFeriado)
        {
            string key = idLocal + "," + (byte)tipoFeriado;

            if (listaFeriados.ContainsKey(key))
            {
                return listaFeriados[key];
            }

            FeriadoCollection feriados = new FeriadoCollection();
            feriados.Query.Select(feriados.Query.Data);
            feriados.Query.es.DefaultConjunction = EntitySpaces.Interfaces.esConjunction.Or;
            if (tipoFeriado == TipoFeriado.Brasil)
            {
                feriados.Query.Where(feriados.Query.IdLocal.Equal(idLocal), feriados.Query.IdLocal.Equal((short)LocalFeriadoFixo.Brasil));
            }
            else if (tipoFeriado == TipoFeriado.NovaYorkBMF)
            {
                feriados.Query.Where(feriados.Query.IdLocal.Equal(idLocal), feriados.Query.IdLocal.Equal((short)LocalFeriadoFixo.Brasil),
                    feriados.Query.IdLocal.Equal((short)LocalFeriadoFixo.NovaYork));
            }
            else if (tipoFeriado == TipoFeriado.Outros)
            {
                feriados.Query.Where(feriados.Query.IdLocal.Equal(idLocal));
            }
            else
            {
                feriados.Query.Where(feriados.Query.IdLocal.Equal(idLocal), feriados.Query.IdLocal.Equal((short)LocalFeriadoFixo.Brasil));
            }

            feriados.Query.OrderBy(feriados.Query.Data.Ascending);

            feriados.Load(feriados.Query);

            listaFeriados[key] = new DateTime[feriados.Count];

            for (int feriadoCount = 0; feriadoCount < feriados.Count; feriadoCount++)
            {
                listaFeriados[key][feriadoCount] = feriados[feriadoCount].Data.Value;
            }

            return listaFeriados[key];
        }

        public static DateTime RetornaProximoComeCotas(DateTime data)
        {
            DateTime dataComeCotas = data;

            DateTime comeCotasMaio = new DateTime(data.Year, 06, 01);
            comeCotasMaio = Calendario.SubtraiDiaUtil(comeCotasMaio, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            DateTime comeCotasNovembro = new DateTime(data.Year, 12, 01);
            comeCotasNovembro = Calendario.SubtraiDiaUtil(comeCotasNovembro, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            if(DateTime.Compare(data, comeCotasMaio) < 0)
            {
                dataComeCotas = comeCotasMaio;
            }
            else if (DateTime.Compare(data, comeCotasMaio) == 0)
            {
                dataComeCotas = comeCotasNovembro;
            }
            else if (DateTime.Compare(data, comeCotasNovembro) < 0)
            {
                dataComeCotas = comeCotasNovembro;
            }
            else
            {
                comeCotasMaio = new DateTime(data.Year + 1, 06, 01);
                dataComeCotas = Calendario.SubtraiDiaUtil(comeCotasMaio, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }

            return dataComeCotas;

        }

        /// <summary>
        /// Retorna a data referente à segunda feira da semana.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DateTime RetornaPrimeiraSegundaFeira(DateTime data)
        {
            if (data.Day == (int)DayOfWeek.Monday)
            {
                return data;
            }

            while (data.DayOfWeek != DayOfWeek.Monday)
            {
                data = data.AddDays(-1);
            }

            return data;
        }

        /// <summary>
        /// Retorna a data referente à segunda feira da semana.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="semanas"></param>
        /// <returns></returns>
        public static DateTime RetornaSegundaFeira(DateTime data, int semanas)
        {
            DateTime dataAux = data;

            bool termino = false;
            int cont = 1;
            while (!termino)
            {
                while (data.DayOfWeek != DayOfWeek.Monday)
                {
                    data = data.AddDays(1);
                }

                if (semanas == cont)
                {
                    termino = true;
                }

                dataAux = data;
                data = data.AddDays(1);
                cont += 1;
            }

            return dataAux;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns>true se data está no ultimo dia util do mes</returns>
        public static bool IsUltimoDiaUtilMes(DateTime data)
        {
            /* Adiciona 1 dia util à data
               Se trocar o mes então a data passada esta no ultimo dia do mes
             */
            int mesAtual = data.Month;
            DateTime dataAux = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            int mesAux = dataAux.Month;
            //
            return mesAtual != mesAux;
        }

        /// <summary>
        /// Retorna o primeiro dia útil do mês, dada a data passada.
        /// Considera IdLocal = Brasil
        ///           TipoFeriado = Brasil
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DateTime RetornaPrimeiroDiaUtilMes(DateTime data)
        {
            return Calendario.RetornaPrimeiroDiaUtilMes(data, 0, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }

        /// <summary>
        /// Retorna o primeiro dia útil do mês, dada a data passada.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="numeroMeses">0 - retorna o primeiro dia do mes em questao referente à data</param>
        /// <param name="idLocal"></param>
        /// <param name="tipoFeriado"></param>
        /// <returns></returns>
        public static DateTime RetornaPrimeiroDiaUtilMes(DateTime data, int numeroMeses, int idLocal, TipoFeriado tipoFeriado)
        {
            DateTime dataAux = data.AddMonths(numeroMeses);
            DateTime dataAux1 = new DateTime(dataAux.Year, dataAux.Month, 1);
            if (!IsDiaUtil(dataAux1, idLocal, tipoFeriado))
            {
                dataAux1 = AdicionaDiaUtil(dataAux1, 1, idLocal, tipoFeriado);
            }
            return dataAux1;
        }

        /// <summary>
        /// Retorna o último dia útil do mês, dada a data passada.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="numeroMeses">0 - retorna o ultimo dia do mes em questao referente à data</param>
        /// <param name="idLocal"></param>
        /// <param name="tipoFeriado"></param>
        /// <returns></returns>
        public static DateTime RetornaUltimoDiaUtilMes(DateTime data, int numeroMeses, int idLocal, TipoFeriado tipoFeriado)
        {

            DateTime dataAux = RetornaPrimeiroDiaCorridoMes(data, numeroMeses + 1);
            DateTime dataAux1 = SubtraiDiaUtil(dataAux, 1, idLocal, tipoFeriado);
            return dataAux1;
        }

        /// <summary>
        /// Retorna o último dia útil do mês, dada a data passada.
        /// Considera IdLocal = Brasil
        ///           TipoFeriado = Brasil
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DateTime RetornaUltimoDiaUtilMes(DateTime data)
        {
            return Calendario.RetornaUltimoDiaUtilMes(data, 0);
        }

        /// <summary>
        /// Retorna o último dia útil do mês, dada a data passada.
        /// Considera IdLocal = Brasil
        ///           TipoFeriado = Brasil
        /// </summary>
        /// <param name="data"></param>
        /// <param name="numeroMeses">0 - retorna o ultimo dia do mes em questao referente à data</param>
        /// <returns></returns>
        public static DateTime RetornaUltimoDiaUtilMes(DateTime data, int numeroMeses)
        {
            return Calendario.RetornaUltimoDiaUtilMes(data, numeroMeses, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }

        /// <summary>
        /// Retorna o primeiro dia corrido do mês, dada a data passada.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="numeroMeses">0 - retorna o primeiro dia do mes em questao referente à data</param>
        /// <returns></returns>
        public static DateTime RetornaPrimeiroDiaCorridoMes(DateTime data, int numeroMeses)
        {
            DateTime dataAux = data.AddMonths(numeroMeses);
            DateTime dataAux1 = new DateTime(dataAux.Year, dataAux.Month, 1);
            return dataAux1;
        }

        /// <summary>
        /// Retorna o último dia corrido do mês, dada a data passada.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="numeroMeses">0 - retorna o último dia do mes em questao referente à data</param>
        /// <returns></returns>
        public static DateTime RetornaUltimoDiaCorridoMes(DateTime data, int numeroMeses)
        {
            DateTime dataAux = data.AddMonths(numeroMeses + 1);
            DateTime dataAux1 = RetornaPrimeiroDiaCorridoMes(dataAux, 0);
            dataAux1 = dataAux1.AddDays(-1);
            return dataAux1;
        }

        /// <summary>
        /// Retorna o primeiro dia útil do ano, dada a data passada.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="numeroAnos">0 - retorna o primeiro dia do ano em questao referente à data
        ///                          1 - retorna o primeiro dia do 2o ano em relação à data passada
        ///                          ...</param>
        /// <param name="idLocal"></param>
        /// <param name="tipoFeriado"></param>
        /// <returns></returns>
        public static DateTime RetornaPrimeiroDiaUtilAno(DateTime data, int numeroAnos, int idLocal, TipoFeriado tipoFeriado)
        {
            DateTime dataAux = data.AddYears(numeroAnos);
            DateTime dataAux1 = new DateTime(dataAux.Year, 1, 1);
            if (!IsDiaUtil(dataAux1, (int)idLocal, tipoFeriado))
            {
                dataAux1 = Calendario.AdicionaDiaUtil(dataAux1, 1, (int)idLocal, tipoFeriado);
            }
            return dataAux1;
        }

        /// <summary>
        /// Retorna o primeiro dia útil do ano, dada a data passada.
        /// Leva em conta apenas feriados nacionais.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="numeroAnos">0 - retorna o primeiro dia do ano em questao referente à data
        ///                          1 - retorna o primeiro dia do 2o ano em relação à data passada
        ///                          ...</param>
        /// <returns></returns>
        public static DateTime RetornaPrimeiroDiaUtilAno(DateTime data, int numeroAnos)
        {
            return RetornaPrimeiroDiaUtilAno(data, numeroAnos, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }

        /// <summary>
        /// Retorna o primeiro dia útil do ano, dada a data passada.
        /// Considera apenas feriados nacionais.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DateTime RetornaPrimeiroDiaUtilAno(DateTime data)
        {
            return Calendario.RetornaPrimeiroDiaUtilAno(data, 0);
        }

        /// <summary>
        /// Retorna o último dia útil do ano, dada a data passada.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="numeroAnos">0 - retorna o último dia do ano em questao referente à data
        ///                          1 - retorna o último dia do 2o ano em relação à data passada
        ///                          ...</param>
        /// <param name="idLocal"></param>
        /// <param name="tipoFeriado"></param>
        /// <returns></returns>
        public static DateTime RetornaUltimoDiaUtilAno(DateTime data, int numeroAnos, int idLocal, TipoFeriado tipoFeriado)
        {
            DateTime dataAux = data.AddYears(numeroAnos);
            DateTime dataAux1 = new DateTime(dataAux.Year, 12, 31);
            if (!IsDiaUtil(dataAux1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
            {
                dataAux1 = Calendario.SubtraiDiaUtil(dataAux1, 1, (int)idLocal, tipoFeriado);
            }
            return dataAux1;
        }

        /// <summary>
        /// Retorna o último dia útil do ano, dada a data passada.
        /// Leva em conta apenas feriados nacionais.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="numeroAnos">0 - retorna o último dia do ano em questao referente à data
        ///                          1 - retorna o último dia do 2o ano em relação à data passada
        ///                          ...</param>
        /// <returns></returns>
        public static DateTime RetornaUltimoDiaUtilAno(DateTime data, int numeroAnos)
        {
            return RetornaUltimoDiaUtilAno(data, numeroAnos, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }

        /// <summary>
        /// Retorna o último dia útil do ano, dada a data passada.
        /// Considera LocalFeriado = Brasil e TipoFeriado = Brasil
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DateTime RetornaUltimoDiaUtilAno(DateTime data)
        {
            return Calendario.RetornaUltimoDiaUtilAno(data, 0);
        }

        /// <summary>
        /// Retorna o último dia corrido do ano, dada a data passada.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="numeroAnos">0 - retorna o último dia do ano em questao referente à data</param>
        /// <returns></returns>
        public static DateTime RetornaUltimoDiaCorridoAno(DateTime data, int numeroAnos)
        {
            DateTime dataAux = new DateTime(data.Year, 12, 31);
            if (numeroAnos > 0)
            {
                dataAux = dataAux.AddYears(numeroAnos);
            }

            return dataAux;
        }

        /// <summary>
        /// Retorna o dia, exatos n meses para trás em relação à data passada.
        /// Se não for dia útil, retorna o primeiro dia útil anterior.
        /// Leva em conta apenas feriados nacionais.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="numeroMeses">1 - retorna o dia 1 mês antes
        ///                           2 - retorna o dia 2 meses antes
        ///                           ...</param>
        /// <returns></returns>
        public static DateTime RetornaDiaMesAnterior(DateTime data, int numeroMeses)
        {
            DateTime dataAux = data.AddMonths(numeroMeses * -1);

            if (!IsDiaUtil(dataAux, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
            {
                dataAux = SubtraiDiaUtil(dataAux, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }

            return dataAux;
        }

        /// <summary>
        /// Adiciona número de dias úteis a uma data, dado um IdLocal.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ArgumentException">se numerodiasUteis for menor que 0</exception>        
        public static DateTime AdicionaDiaUtil(DateTime data, int numeroDiasUteis, int idLocal, TipoFeriado tipoFeriado)
        {

            #region ArgumentosInvalido - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("numeroDiasUteis não pode ser negativo.");

            if (numeroDiasUteis < 0)
            {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            DateTime dataAux = new DateTime(data.Year, data.Month, data.Day);

            int i = 0;
            int numeroDias = numeroDiasUteis;
            while (i != numeroDias)
            {
                dataAux = dataAux.AddDays(1);
                // Se não for final de semana e feriado é um dia util
                if (!Calendario.IsFinalSemana(dataAux) && !Calendario.IsFeriado(dataAux, idLocal, tipoFeriado))
                {
                    i++;
                }
            }
            return dataAux;
        }

        /// <summary>
        /// Adiciona número de dias úteis de uma data usando LocalFeriado = Brasil e TipoFeriado = Brasil
        /// </summary>
        /// <returns></returns>        
        public static DateTime AdicionaDiaUtil(DateTime data, int numeroDiasUteis)
        {
            return Calendario.AdicionaDiaUtil(data, numeroDiasUteis, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }

        /// <summary>
        /// Subtrai número de dias úteis a uma data, dado um IdLocal.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ArgumentException">se numerodiasUteis for menor que 0</exception>
        public static DateTime SubtraiDiaUtil(DateTime data, int numeroDiasUteis, int idLocal, TipoFeriado tipoFeriado)
        {

            #region ArgumentosInvalido - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("numeroDiasUteis não pode ser negativo.");

            if (numeroDiasUteis < 0)
            {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            DateTime dataAux = new DateTime(data.Year, data.Month, data.Day);

            int i = 0;
            int numeroDias = numeroDiasUteis;
            while (i != numeroDias)
            {
                dataAux = dataAux.AddDays(-1);

                // Se não for final de semana e feriado é um dia util
                if (!Calendario.IsFinalSemana(dataAux) && !Calendario.IsFeriado(dataAux, idLocal, tipoFeriado))
                {
                    i++;
                }
            }
            return dataAux;
        }

        /// <summary>
        /// Subtrai número de dias úteis de uma data usando LocalFeriado = Brasil e TipoFeriado = Brasil
        /// </summary>
        /// <returns></returns>        
        public static DateTime SubtraiDiaUtil(DateTime data, int numeroDiasUteis)
        {
            return Calendario.SubtraiDiaUtil(data, numeroDiasUteis, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idLocal"></param>
        /// <param name="tipoFeriado"></param>
        /// <returns>boolean indicando se determinada data num determinado local é dia útis</returns>        
        /// TODO: lancar exceção se tipoFeriado não estiver no enum
        public static bool IsDiaUtil(DateTime data, int idLocal, TipoFeriado tipoFeriado)
        {
            if (IsFeriado(data, idLocal, tipoFeriado) || IsFinalSemana(data))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns>boolean indicando se determinada data é dia útil</returns>        
        public static bool IsDiaUtil(DateTime data)
        {
            if (IsFeriado(data) || IsFinalSemana(data))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idLocal"></param>
        /// <param name="tipoFeriado"></param>
        /// <returns>boolean indicando se determinada data num determinado local é feriado</returns>        
        /// TODO: lancar exceção se tipoFeriado não estiver no enum
        public static bool IsFeriado(DateTime data, int idLocal, TipoFeriado tipoFeriado)
        {
            Feriado feriado = new Feriado();

            switch (tipoFeriado)
            {
                case TipoFeriado.Brasil:
                    return feriado.BuscaFeriadoBrasil(data, idLocal);
                case TipoFeriado.NovaYorkBMF:
                    return feriado.BuscaFeriadoNovaYorkBMF(data, idLocal);
                case TipoFeriado.Outros:
                    return feriado.BuscaFeriadoLocal(data, idLocal);
                default:
                    return feriado.BuscaFeriadoBrasil(data, idLocal);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns> boolean indicando se determinada data é feriado</returns>        
        public static bool IsFeriado(DateTime data)
        {
            Feriado feriado = new Feriado();
            return feriado.BuscaFeriadoBrasil(data, (int)LocalFeriadoFixo.Brasil);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns>boolean indicando se a data é final de semana</returns>
        public static bool IsFinalSemana(DateTime data)
        {
            return (data.DayOfWeek == DayOfWeek.Sunday || data.DayOfWeek == DayOfWeek.Saturday);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns>boolean indicando se a data é domingo</returns>
        public static bool IsDomingo(DateTime data)
        {
            return (data.DayOfWeek == DayOfWeek.Sunday);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns>boolean indicando se a data é sabado</returns>
        public static bool IsSabado(DateTime data)
        {
            return (data.DayOfWeek == DayOfWeek.Saturday);
        }

        /// <summary>
        /// Retorna número de anos, considerando dia e mes da data inicio e fim.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public static int NumeroAnos(DateTime dataInicio, DateTime dataFim)
        {
            DateTime dataAux = dataInicio;

            int i = 0;
            while (dataAux <= dataFim)
            {
                dataAux = dataAux.AddYears(1);

                if (dataAux <= dataFim)
                {
                    i++;
                }
            }
            return i;
        }

        /// <summary>
        /// Retorna Numero de dias entre duas Datas considerando dias corridos.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        /// throws ArgumentException 
        ///     - se dataInicio > dataFim
        public static int NumeroDias(DateTime dataInicio, DateTime dataFim)
        {

            #region ArgumentoIncorreto - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("DataInicio deve ser menor que DataFim");

            if (dataInicio.CompareTo(dataFim) > 0)
            {
                string debug = Financial.Util.LogUtil.LogStack();
                throw new ArgumentException(mensagem.ToString() + ": " + debug);
            }
            #endregion

            TimeSpan numDias = dataFim - dataInicio;
            return numDias.Days;
        }

        /// <summary>
        /// Retorna Numero de dias entre duas Datas considerando ano com 360 dias.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        /// throws ArgumentException 
        ///     - se dataInicio > dataFim
        public static int NumeroDias360(DateTime dataInicio, DateTime dataFim)
        {

            #region ArgumentoIncorreto - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("DataInicio deve ser menor que DataFim");

            if (dataInicio.CompareTo(dataFim) > 0)
            {
                string debug = Financial.Util.LogUtil.LogStack();
                throw new ArgumentException(mensagem.ToString() + ": " + debug);
            }
            #endregion

            if (dataInicio.Day > 30)
            {
                dataInicio = dataInicio.AddDays(-1);
            }
            if (dataFim.Day > 30)
            {
                dataFim = dataFim.AddDays(-1);
            }

            int diaInicial = dataInicio.Day;
            int diaFinal = dataFim.Day;
            int diferencaMeses = Calendario.NumeroMeses(dataInicio, dataFim, true);

            int numeroDias = (int)(diferencaMeses * 30M) + (diaFinal - diaInicial);
            return numeroDias;
        }

        /// <summary>
        /// Retorna Numero de Meses entre duas Datas
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        /// throws ArgumentException se dataInicio > dataFim
        public static int NumeroMeses(DateTime dataInicio, DateTime dataFim)
        {
            return NumeroMeses(dataInicio, dataFim, false);
        }

        /// <summary>
        /// Retorna Numero de Meses entre duas Datas, pode trabalhar com meses completos ou incompletos.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        /// throws ArgumentException se dataInicio > dataFim
        public static int NumeroMeses(DateTime dataInicio, DateTime dataFim, bool incompleto)
        {

            #region ArgumentoIncorreto - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("DataInicio deve ser menor que DataFim");

            if (dataInicio.CompareTo(dataFim) > 0)
            {
                string debug = Financial.Util.LogUtil.LogStack();
                throw new ArgumentException(mensagem.ToString() + ": " + debug);
            }
            #endregion

            int numMeses = 0;

            /* Exemplo:
            05/05/2007
            01/05/2008	11 meses

            05/05/2007	
            05/05/2008	12 meses
            	
            05/05/2007	
            06/05/2008	12 meses
            */

            // Iguala os dias
            DateTime dataInicioAux = new DateTime(Convert.ToInt32(dataInicio.Year),
                                                   Convert.ToInt32(dataInicio.Month),
                                                   1);
            //
            DateTime dataFimAux = new DateTime(Convert.ToInt32(dataFim.Year),
                                               Convert.ToInt32(dataFim.Month),
                                               1);

            // Adiciona 1 mes na dataInicio até que as datas se igualem
            while (dataInicioAux.CompareTo(dataFimAux) != 0)
            {
                numMeses++;
                dataInicioAux = dataInicioAux.AddMonths(1);
            }

            if (!incompleto)
            {
                // Se o dia da data Inicio for maior que o Dia da data Final o Mês ainda não está completo
                // Mes Incompleto
                if (dataInicio.Day > dataFim.Day)
                {
                    numMeses--;
                }
            }

            return numMeses;
        }

        /// <summary>
        ///  Retorna Numero de dias úteis entre duas Datas Considerando os dias Uteis e feriados.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="localFeriado"></param>
        /// <param name="tipoFeriado"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">
        /// - se dataInicio > dataFim
        /// - se localFeriado não estiver dentro dos valores do Enum Common.LocalFeriadoFixo
        /// </exception>
        public static int NumeroDiasOld(DateTime dataInicio, DateTime dataFim, int idLocal, TipoFeriado tipoFeriado)
        {
            #region ArgumentoIncorreto - throw ArgumentException
            if (dataInicio.CompareTo(dataFim) > 0)
            {
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("DataInicio deve ser menor que DataFim");
                string debug = Financial.Util.LogUtil.LogStack();
                throw new ArgumentException(mensagem.ToString() + ": " + debug);
            }

            List<int> valoresPossiveis = LocalFeriadoFixo.Values();
            if (!valoresPossiveis.Contains(idLocal))
            {
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("localFeriado incorreto. Os valores possiveis estão no enum LocalFeriadoFixo");
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            int contadorNumeroDiasUteis = 0;
            while (dataInicio.CompareTo(dataFim) < 0)
            {
                dataInicio = Calendario.AdicionaDiaUtil(dataInicio, 1, idLocal, tipoFeriado);
                if (dataInicio.CompareTo(dataFim) <= 0)
                {
                    contadorNumeroDiasUteis++;
                }
            }

            return contadorNumeroDiasUteis;
        }

        public static int NumeroDias(DateTime dataInicio, DateTime dataFim, int idLocal, TipoFeriado tipoFeriado)
        {
            #region ArgumentoIncorreto - throw ArgumentException
            if (dataInicio.CompareTo(dataFim) > 0)
            {
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("DataInicio deve ser menor que DataFim");
                string debug = Financial.Util.LogUtil.LogStack();
                throw new ArgumentException(mensagem.ToString() + ": " + debug);
            }
            #endregion

            dataInicio = dataInicio.Date;
            dataFim = dataFim.Date;

            DateTime[] feriados = InicializaListaFeriados(idLocal, tipoFeriado);

            double diasUteis = 1 + ((dataFim - dataInicio).TotalDays * 5 - (dataInicio.DayOfWeek - dataFim.DayOfWeek) * 2) / 7;
            if ((int)dataFim.DayOfWeek == 6) diasUteis--;
            if ((int)dataInicio.DayOfWeek == 0) diasUteis--;

            // subtrair o numero de feriados no intervalo
            bool dataInicioIsFeriado = false;
            foreach (DateTime feriado in feriados)
            {
                DateTime dataFeriado = feriado.Date;

                if (dataFeriado == dataInicio)
                {
                    dataInicioIsFeriado = true;
                }

                if (dataFeriado > dataFim)
                {
                    //Como as datas de feriados estao ordenadas, é seguro pularmos fora do loop assim que chegarmos a um feriado fora da intervalo
                    break;
                }

                if (dataInicio <= dataFeriado && dataFeriado <= dataFim && !(IsFinalSemana(dataFeriado)))
                {
                    --diasUteis;
                }
            }

            if (!IsFinalSemana(dataInicio) && !dataInicioIsFeriado)
            {
                //Ajustar a data pois nao queremos incluir o dia inicial
                diasUteis--;
            }

            return Convert.ToInt32(diasUteis);

        }

        /// <summary>
        ///  Retorna Numero de sabados e domingos entre duas Datas.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        /// throws ArgumentException 
        ///     - se dataInicio > dataFim
        public static int NumeroSabadosDomingos(DateTime dataInicio, DateTime dataFim)
        {

            #region ArgumentoIncorreto - throw ArgumentException
            if (dataInicio.CompareTo(dataFim) > 0)
            {
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("DataInicio deve ser menor que DataFim");
                string debug = Financial.Util.LogUtil.LogStack();
                throw new ArgumentException(mensagem.ToString() + ": " + debug);
            }
            #endregion

            int finalSemana = 0;
            DateTime data = new DateTime(dataInicio.Year, dataInicio.Month, dataInicio.Day);
            while (data.CompareTo(dataFim) < 0)
            {
                data = data.AddDays(1);
                if (Calendario.IsFinalSemana(data))
                {
                    finalSemana++;
                }
            }
            return finalSemana;
        }

        /// <summary>
        /// Recebe uma data em formato string e testa se é data válida. 
        /// Se não for, procura a data válida anterior mais próxima.
        /// TODO: 
        /// não verifica possiveis formatos de data diferentes e se for primeiro dia do mes calcula errado
        /// </summary>
        /// <param name="data"></param>        
        /// <returns></returns>
        public static DateTime RetornaDataValida(string data)
        {
            if (data.Length != 10)
            {
                throw new Exception("Data Inválida.");
            }
            while (!Utilitario.IsDate(data))
            {
                int dia = Convert.ToInt32(data.Substring(0, 2));
                dia -= 1;
                data = Convert.ToString(dia) + Utilitario.Right(data, 8);
            }
            return Convert.ToDateTime(data);
        }

        /// <summary>
        /// Retorna a Data Da Pascoa Para um Determinado Ano
        /// </summary>
        /// <param name="year">Ano Desejado</param>
        /// <returns></returns>
        public static DateTime DataPascoa(int year)
        {
            int nGold = 0;
            int nCent = 0;
            int nCorx = 0;
            int nCorz = 0;
            int nSunday = 0;
            int nEpact = 0;
            int nMoon = 0;
            int nMonth = 0;
            int nDay = 0;

            // nGold, é o Golden number
            nGold = (year % 19) + 1;

            // nCent é o Século
            nCent = (year / 100) + 1;

            // nCorx é o número de anos em que o bissexto foi retirado para sincronizar
            nCorx = ((3 * nCent) / 4 - 12);

            // nCorz é uma correção para sincronizar a Páscoa com a órbita da Lua
            nCorz = ((8 * nCent + 5) / 25 - 5);
            nSunday = ((5 * year) / 4 - nCorx - 10);

            // nEpact é a ocorrencia de uma Lua cheia
            nEpact = ((11 * nGold + 20 + nCorz - nCorx) % 30);

            if (nEpact < 0)
                nEpact = nEpact + 30;

            if (((nEpact == 25) && (nGold > 11)) || (nEpact == 24))
                nEpact = nEpact + 1;

            // Lua cheia - a n ésima lua de Março é o calendário da lua cheia
            nMoon = 44 - nEpact;

            if (nMoon < 21)
                nMoon = nMoon + 30;

            // Avança ao domingo
            nMoon = (nMoon + 7 - ((nSunday + nMoon) % 7));

            // obtém o mês e o dia
            if (nMoon > 31)
            {
                nMonth = 4;
                nDay = nMoon - 31;
            }
            else
            {
                nMonth = 3;
                nDay = nMoon;
            }

            return new DateTime(year, nMonth, nDay);
        }

        /// <summary>
        /// Retorna a Data Do Carnaval Para um Determinado Ano
        /// </summary>
        /// <param name="year">Ano Desejado</param>
        /// <returns></returns>
        public static DateTime DataCarnaval(int year)
        {
            DateTime dtPascoa = DataPascoa(year);
            DateTime dtCarnaval = dtPascoa.AddDays(-47);

            return dtCarnaval;
        }

        /// <summary>
        /// Retorna a Data Da Semana Santa Para um Determinado Ano
        /// </summary>
        /// <param name="year">Ano Desejado</param>
        /// <returns></returns>
        public static DateTime DataSextaSanta(int year)
        {
            DateTime dtPascoa = DataPascoa(year);
            DateTime dtSextaSanta = dtPascoa.AddDays(-2);

            return dtSextaSanta;
        }

        /// <summary>
        /// Retorna a Data De CorpusChristi Para um Determinado Ano
        /// </summary>
        /// <param name="year">Ano Desejado</param>
        /// <returns></returns>
        public static DateTime DataCorpusChristi(int year)
        {
            DateTime dtPascoa = DataPascoa(year);
            DateTime dtCorpusChristi = dtPascoa.AddDays(60);

            return dtCorpusChristi;
        }

        public static int ConverteMesExtensoNumerico(string monthName, CultureInfo culture)
        {
            return DateTime.ParseExact(monthName, "MMMM", culture).Month;
        }
    }
    #endregion

    #region Calculo Financeiro
    /// <summary>
    ///  Funções relativas a Calculo Financeiro
    /// </summary>
    public static class CalculoFinanceiro
    {
        #region Métodos para conversão da tabela de probabilidades da curva normal (dado %, retorna o z equivalente)
        public static class PercentToZvalueConverter
        {
            private const int Start = -5;
            private const int StartMean = 0;
            private const double End = 5d;

            public static double CalculateCumulativeZ(double percent)
            {
                return CalcZ(Start, End, percent);
            }

            public static double CalculateComplementaryCumulativeZ(double percent)
            {
                return -CalcZ(Start, End, percent);
            }

            public static double CalculateCumulativeFromMeanZ(double percent)
            {
                if (percent > 50d)
                    return double.NaN;

                int sign = Math.Sign(percent);
                double result = CalcZ(StartMean, End, Math.Abs(percent));
                return sign * result;
            }

            private static double CalcZ(int start, double end, double percent)
            {
                if (percent < 0 || percent > 100)
                    return double.NaN;

                if (percent == 100)
                    return end;

                double ratio = percent / 100d;

                double result = start;
                double step = 0.0001d;
                double cst = 1d / Math.Sqrt(2 * Math.PI);

                double area = 0;
                for (double i = start; i < end; i += step)
                {
                    area += step * cst / Math.Pow(Math.E, (Math.Pow((i + 0.5d * step), 2) * 0.5));

                    result = i;
                    if (area > ratio)
                        break;
                }

                return result;
            }
        }
        #endregion

        #region Calcula modelo EWMA
        //*************************Below is how to use it**************************************************
        //List<double> inputData = new List<double>
        //{
        //    52.0, 47.0, 53.0, 49.3, 50.1, 47.0, 51.0, 50.1, 51.2, 50.5, 49.6, 47.6, 49.9, 51.3, 47.8, 51.2, 52.6, 52.4, 53.6, 52.1
        //};

        //// Put in first plase EMWA initial value (emwa0)
        //inputData.Insert(0, 50.0);

        //double lambda = 0.3d;
        //List<double> emwa = EmwaCalculator.EmwaCalculator.Calculate(lambda, inputData);
        //****************************************************************************************************
        public static List<double> CalculateEWMA(double lambda, IList<double> inputData)
        {
            List<double> result = null;

            if (inputData == null)
                return result;
            if (inputData.Count == 0)
                return result;
            if (lambda <= 0 || lambda > 1)
                return result;

            result = new List<double>(inputData.Count - 1);

            double lambdaComplement = 1 - lambda;
            double emwa = inputData[0];
            double currData;
            for (int i = 1; i < inputData.Count; i++)
            {
                currData = inputData[i];
                emwa = lambda * currData + lambdaComplement * emwa;
                result.Add(emwa);
            }

            return result;
        }
        #endregion

        #region Funções para cálculo da TIR
        public static class IrrNewtonRaphson
        {
            public static double Calculate(IList<double> cashFlow)
            {
                return Calculate(cashFlow, 0.000001d);

                CultureInfo culturaCorrente = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");

                Application a = new Application();

                double irr = 0;
                try
                {
                    irr = a.WorksheetFunction.Irr(cashFlow, 0.1d);
                }
                catch (Exception)
                {
                }
                finally
                {
                    System.Threading.Thread.CurrentThread.CurrentCulture = culturaCorrente;
                }

                System.Threading.Thread.CurrentThread.CurrentCulture = culturaCorrente;

                return irr;
            }
            public static double Calculate(IList<double> cashFlow, double error)
            {
                return CalculateAboutGuess(cashFlow, 0, error);
            }
            public static double CalculateAboutGuess(IList<double> cashFlow, double guessValue, double error)
            {
                double result = -1;

                if (cashFlow == null)
                    return result;
                if (cashFlow.Count <= 1)
                    return result;

                double first = cashFlow[0];

                if (first == 0)
                    return result;

                if (double.IsInfinity(first) || double.IsNaN(first))
                    return result;

                double leftBound = guessValue - 1d, rightBound = guessValue + 1d, step = 0.1d;

                double currIrr, currCheck, bestCheck = double.MaxValue;
                for (double i = leftBound; i <= rightBound; i += step)
                {
                    currIrr = IrrNewtonRaphson.CalculateGuess(cashFlow, i, error);
                    currCheck = IrrNewtonRaphson.CheckResult(currIrr, cashFlow);
                    currCheck = Math.Abs(currCheck);

                    if (currCheck < bestCheck)
                    {
                        bestCheck = currCheck;
                        result = currIrr;
                    }
                }

                if (Math.Abs(Math.Log10(Math.Abs(first - bestCheck)) - Math.Log10(Math.Abs(first))) > Math.Abs(Math.Log10(100) - Math.Log10(2)))
                    result = -1;

                return result;
            }

            public static double CalculateGuess(IList<double> cashFlow, double guessValue, double error)
            {
                double result = double.PositiveInfinity;

                if (cashFlow == null)
                    return result;
                if (cashFlow.Count <= 1)
                    return result;

                List<double> cashFlowList = new List<double>(cashFlow);

                double fX0 = cashFlowList[0];
                List<double> fX = cashFlowList.GetRange(1, cashFlowList.Count - 1);
                List<double> deriviative = CalcFirstDeriviative(fX);

                double sumFX;
                double currFX, currDeriviative, currError = (error + error);
                double currX = guessValue, nextX;
                do
                {
                    CalcSums(currX, fX, deriviative, out sumFX, out currDeriviative);
                    currFX = fX0 + sumFX;

                    nextX = currX - (currFX / currDeriviative);
                    currError = Math.Abs(nextX - currX);

                    currX = nextX;
                }
                while (currError > error);

                result = currX;
                return result;
            }

            private static List<double> CalcFirstDeriviative(List<double> fX)
            {
                List<double> result = new List<double>();

                for (int i = 0; i < fX.Count; i++)
                    result.Add((-(i + 1)) * fX[i]);

                return result;
            }
            private static void CalcSums(double currX, List<double> fX, List<double> deriviative, out double sumFX, out double sumDeriviative)
            {
                sumFX = sumDeriviative = 0;
                double currPow, val, valFx, valDrv;

                for (int i = 0; i < fX.Count; i++)
                {
                    currPow = -(i + 1);
                    val = (1 + currX);
                    valFx = Math.Pow(val, currPow);
                    valDrv = Math.Pow(val, currPow - 1);

                    sumFX += (valFx * fX[i]);
                    sumDeriviative += (valDrv * deriviative[i]);
                }
            }
            private static bool IsInBound(double val, double leftBound, double rightBound)
            {
                bool result = false;

                if (val >= leftBound && val <= rightBound)
                    result = true;

                return result;
            }
            public static double CheckResult(double result, IList<double> cashFlow)
            {
                List<double> cashFlowList = new List<double>(cashFlow);

                double fX0 = cashFlowList[0];
                List<double> fX = cashFlowList.GetRange(1, cashFlowList.Count - 1);

                double sumFX = 0;
                double currPow, param, paramPow;

                for (int i = 0; i < fX.Count; i++)
                {
                    currPow = -(i + 1);
                    param = (1 + result);
                    paramPow = Math.Pow(param, currPow);

                    sumFX += (paramPow * fX[i]);
                }

                return fX0 + sumFX;
            }


            public static double Calculate(IList<CashFlow> cashFlow, ref bool falha)
            {
                return Calculate(cashFlow, 0.000001d, ref falha);
            }
            public static double Calculate(IList<CashFlow> cashFlow, double error, ref bool falha)
            {
                return CalculateAboutGuess(cashFlow, 0, error, ref falha);
            }
            public static double CalculateAboutGuess(IList<CashFlow> cashFlow, double guessValue, double error, ref bool falha)
            {
                double result = -1;

                if (cashFlow == null)
                    return result;
                if (cashFlow.Count <= 1)
                    return result;

                double first = cashFlow[0].valor;
                if (first == 0)
                    return result;

                if (double.IsInfinity(first) || double.IsNaN(first))
                    return result;

                double leftBound = guessValue - 1d, rightBound = guessValue + 1d, step = 0.1d;

                double currIrr, currCheck, bestCheck = double.MaxValue;
                for (double i = leftBound; i <= rightBound; i += step)
                {
                    currIrr = IrrNewtonRaphson.CalculateGuess(cashFlow, i, error);
                    currCheck = IrrNewtonRaphson.CheckResult(currIrr, cashFlow);
                    currCheck = Math.Abs(currCheck);

                    if (currCheck < bestCheck)
                    {
                        bestCheck = currCheck;
                        result = currIrr;
                    }
                }

                //if (Math.Abs(Math.Log10(Math.Abs(first - bestCheck)) - Math.Log10(Math.Abs(first))) > Math.Abs(Math.Log10(100) - Math.Log10(2)))
                //    result = -1;

                falha = false;
                if (bestCheck > 1)
                {
                    falha = true;
                }

                return result;
            }
            public static double CalculateGuess(IList<CashFlow> cashFlow, double guessValue, double error)
            {
                double result = double.PositiveInfinity;

                if (cashFlow == null)
                    return result;
                if (cashFlow.Count <= 1)
                    return result;

                List<CashFlow> cashFlowList = new List<CashFlow>(cashFlow);

                CashFlow fX0 = cashFlowList[0];
                List<CashFlow> fX = cashFlowList.GetRange(1, cashFlowList.Count - 1);
                List<DeriviativeItem> deriviative = CalcFirstDeriviative(fX, fX0);

                double sumFX;
                double currFX, currDeriviative, currError = (error + error);
                double currX = guessValue, nextX;

                int coutAtempts = 100;
                do
                {
                    CalcSums(currX, fX, deriviative, out sumFX, out currDeriviative);
                    currFX = fX0.valor + sumFX;

                    nextX = currX - (currFX / currDeriviative);
                    currError = Math.Abs(nextX - currX);

                    if (--coutAtempts == 0)
                        break;

                    currX = nextX;
                }
                while (currError > error);

                result = currX;
                return result;
            }
            private static List<DeriviativeItem> CalcFirstDeriviative(List<CashFlow> fX, CashFlow firstFlow)
            {
                List<DeriviativeItem> result = new List<DeriviativeItem>();
                CashFlow curr;
                DateTime diff;
                DeriviativeItem item = DeriviativeItem.Empty;

                for (int i = 0; i < fX.Count; i++)
                {
                    curr = fX[i];
                    diff = curr.data;

                    item.Power = -((double)diff.Subtract(firstFlow.data).Days / 365d);
                    item.Value = item.Power * curr.valor;

                    result.Add(item);
                }

                return result;
            }
            private static void CalcSums(double currX, List<CashFlow> fX, List<DeriviativeItem> deriviative, out double sumFX, out double sumDeriviative)
            {
                sumFX = sumDeriviative = 0;
                double currPow, val, valFx, valDrv;
                DeriviativeItem currDerItem;

                for (int i = 0; i < fX.Count; i++)
                {
                    currDerItem = deriviative[i];

                    currPow = currDerItem.Power;
                    val = (1 + currX);
                    valFx = Math.Pow(val, currPow);
                    valDrv = Math.Pow(val, currPow - 1);

                    sumFX += (valFx * fX[i].valor);
                    sumDeriviative += (valDrv * currDerItem.Value);
                }
            }
            public static double CheckResult(double result, IList<CashFlow> cashFlow)
            {
                List<CashFlow> cashFlowList = new List<CashFlow>(cashFlow);

                CashFlow firstFlow = cashFlowList[0];
                CashFlow curr;

                DateTime diff;

                List<CashFlow> fX = cashFlowList.GetRange(1, cashFlowList.Count - 1);

                double sumFX = 0;
                double currPow, param, paramPow;

                for (int i = 0; i < fX.Count; i++)
                {
                    curr = fX[i];

                    diff = curr.data;

                    currPow = -((double)diff.Subtract(firstFlow.data).Days / 365d);
                    param = (1 + result);
                    paramPow = Math.Pow(param, currPow);

                    sumFX += (paramPow * curr.valor);
                }

                return firstFlow.valor + sumFX;
            }

            #region Nested Types

            private struct DeriviativeItem
            {
                public static readonly DeriviativeItem Empty = new DeriviativeItem();

                public double Value;
                public double Power;
            }

            #endregion
        }
        #endregion

        /// <summary>
        /// Classe usada opcionalmente para guardar toda a série de indices usada para fazer uma determinada correção de um valor.
        /// </summary>
        public class CorrecaoIndice
        {
            public DateTime dataCalculo;
            public DateTime dataCalculoEfetiva;
            public DateTime dataIndice;
            public decimal valorIndice;
            public int numeroDias;
            public int numeroDiasTotal;
            public decimal fatorCorrecao;
            public decimal fatorAcumulado;
        }

        public class CashFlow
        {
            public double valor;
            public DateTime data;
            public int? numeroDias;
            public CashFlow(double valor, DateTime data)
            {
                this.valor = valor;
                this.data = data;
                this.numeroDias = null;
            }
            public CashFlow(double valor, DateTime data, int numeroDias)
            {
                this.valor = valor;
                this.data = data;
                this.numeroDias = numeroDias;
            }
        }

        private static double CalculaVPTIR(List<CashFlow> cashFlowList, CashFlow cashFlow, double tir, DateTime dataCalculo, ContagemDias contagemDias)
        {
            DiasNaoUteisContainer diasNaoUteisContainer = new DiasNaoUteisContainer();
            int numeroDias = 0;
            int baseCalculo;
            if (contagemDias == ContagemDias.Uteis)
            {
                DateTime dataUltima = cashFlowList[cashFlowList.Count - 1].data;
                diasNaoUteisContainer = new DiasNaoUteisContainer((int)LocalFeriadoFixo.Brasil, dataCalculo, dataUltima);
                numeroDias = diasNaoUteisContainer.CalculaNumeroDias(dataCalculo, cashFlow.data);
                baseCalculo = (int)BaseCalculo.Base252;
            }
            else
            {
                numeroDias = Calendario.NumeroDias360(dataCalculo, cashFlow.data);
                baseCalculo = (int)BaseCalculo.Base360;
            }
            return (numeroDias == 0) ? cashFlow.valor :
                    (cashFlow.valor / (Math.Pow(1 + tir, (Double)((double)numeroDias / (double)baseCalculo))));
        }

        /// <summary>
        /// Ajusta o fluxo de caixa incluindo zeros em dias sem fluxo. Usado para cálculo da TIR.
        /// </summary>
        /// <param name="cashFlowList"></param>
        /// <returns></returns>
        public static double[] RetornaFluxoCaixaAjustado(List<CashFlow> cashFlowList)
        {
            List<double> lista = new List<double>();

            DateTime dataAnterior = cashFlowList[0].data.AddDays(-1);
            foreach (CashFlow cashFlow in cashFlowList)
            {
                if (cashFlow.data.AddDays(-1) != dataAnterior)
                {
                    for (int j = 0; j < Calendario.NumeroDias(dataAnterior, cashFlow.data) - 1; j++)
                    {
                        lista.Add(0);
                    }
                }

                lista.Add(cashFlow.valor);

                dataAnterior = cashFlow.data;
            }

            double[] cashFlows = lista.ToArray();

            return cashFlows;
        }

        /// <summary>
        /// Calcula a TIR (formato ano 252 ou 360, dependendo se por dias uteis ou corridos) levando em conta um cashflow por dias corridos.
        /// </summary>
        /// <param name="cashFlowList"></param>
        /// <param name="dataCalculo"></param>
        /// <param name="contagemDias"></param>
        /// <returns></returns>
        public static double CalculaTIR(List<CashFlow> cashFlowList, DateTime dataCalculo, ContagemDias contagemDias)
        {
            return CalculaTIR_XIRR(cashFlowList, dataCalculo, contagemDias);
        }

        /// <summary>
        /// Calcula a TIR (formato ano 252 ou 360, dependendo se por dias uteis ou corridos) levando em conta um cashflow por dias corridos.
        /// Método XIRR do Excel.
        /// </summary>
        /// <param name="cashFlowList"></param>
        /// <param name="dataCalculo"></param>
        /// <param name="contagemDias"></param>
        /// <returns></returns>
        public static double CalculaTIR_XIRR(List<CashFlow> cashFlowList, DateTime dataCalculo, ContagemDias contagemDias)
        {
            //ICalculator calculator = new NewtonRaphsonIRRCalculator(cashFlows);
            //double tirCorrida = calculator.Execute();
            //Application excel = new Application();
            //double tirDiaria = excel.WorksheetFunction.Irr(cashFlows, 0.01f);
            bool falha = false;
            double tirAnual = IrrNewtonRaphson.Calculate(cashFlowList, ref falha);

            if (falha)
            {
                return CalculaTIR_IRR(cashFlowList, dataCalculo, contagemDias);
            }
            else
            {
                return tirAnual;
            }
        }

        /// <summary>
        /// Calcula a TIR (formato ano 252 ou 360, dependendo se por dias uteis ou corridos) levando em conta um cashflow por dias corridos.
        /// Método IRR do Excel com zeros intercalados em datas sem fluxo.
        /// </summary>
        /// <param name="cashFlowList"></param>
        /// <param name="dataCalculo"></param>
        /// <param name="contagemDias"></param>
        /// <returns></returns>
        public static double CalculaTIR_IRR(List<CashFlow> cashFlowList, DateTime dataCalculo, ContagemDias contagemDias)
        {
            double[] cashFlows = RetornaFluxoCaixaAjustado(cashFlowList);

            //ICalculator calculator = new NewtonRaphsonIRRCalculator(cashFlows);
            //double tirCorrida = calculator.Execute();
            //Application excel = new Application();
            //double tirDiaria = excel.WorksheetFunction.Irr(cashFlows, 0.01f);
            double tirDiaria = IrrNewtonRaphson.Calculate(cashFlows);

            DiasNaoUteisContainer diasNaoUteisContainer = new DiasNaoUteisContainer();
            int baseCalculo;
            if (contagemDias == ContagemDias.Uteis)
            {
                baseCalculo = (int)BaseCalculo.Base252;
            }
            else
            {
                baseCalculo = (int)BaseCalculo.Base365;
            }

            double tirFinal = Math.Pow((tirDiaria + (double)1M), (double)baseCalculo) - (double)1M;

            return tirFinal;
        }

        /// <summary>
        /// Calcula o Valor Presente Liquido de uma série de cashflows. Pode ser usado para cálculo do PU diário dada uma TIR informada.
        /// Usa DiasNaoUteisContainer para acelerar o cálculo por dias úteis.
        /// </summary>
        /// <param name="taxa"></param>
        /// <param name="dataInicio"></param>
        /// <param name="baseCalculo"></param>
        /// <param name="listaValores"></param>
        /// <param name="listaDatas"></param>
        /// <returns></returns>
        public static decimal CalculaVPL(decimal taxa, DateTime dataInicio, BaseCalculo baseCalculo, List<decimal> listaValores, List<DateTime> listaDatas)
        {
            List<CashFlow> listaCF = new List<CashFlow>();
            return CalculaVPL(taxa, dataInicio, baseCalculo, listaValores, listaDatas, out listaCF);
        }

        /// <summary>
        /// Calcula o Valor Presente Liquido de uma série de cashflows. Pode ser usado para cálculo do PU diário dada uma TIR informada.
        /// Usa DiasNaoUteisContainer para acelerar o cálculo por dias úteis.
        /// </summary>
        /// <param name="taxa"></param>
        /// <param name="dataInicio"></param>
        /// <param name="baseCalculo"></param>
        /// <param name="listaValores"></param>
        /// <param name="listaDatas"></param>
        /// <returns></returns>
        public static decimal CalculaVPL(decimal taxa, DateTime dataInicio, BaseCalculo baseCalculo, List<decimal> listaValores, List<DateTime> listaDatas, out List<CashFlow> listaCF)
        {
            listaCF = new List<CashFlow>();

            decimal vpl = 0;
            int cont = 0;

            DateTime dataUltima = listaDatas[listaDatas.Count - 1];

            DiasNaoUteisContainer diasNaoUteisContainer = new DiasNaoUteisContainer();
            if (baseCalculo == BaseCalculo.Base252)
            {
                diasNaoUteisContainer = new DiasNaoUteisContainer((int)LocalFeriadoFixo.Brasil, dataInicio, dataUltima);
            }

            foreach (decimal valorFluxo in listaValores)
            {
                DateTime dataFluxo = listaDatas[cont];

                int numeroDias = 0;

                if (baseCalculo == BaseCalculo.Base252)
                {
                    numeroDias = diasNaoUteisContainer.CalculaNumeroDias(dataInicio, dataFluxo);
                }
                else if (baseCalculo == BaseCalculo.Base360)
                {
                    numeroDias = Calendario.NumeroDias360(dataInicio, dataFluxo);
                }
                else
                {
                    numeroDias = Calendario.NumeroDias(dataInicio, dataFluxo);
                }

                decimal fator = (decimal)Math.Pow((double)(1 + taxa / 100M), (double)((decimal)numeroDias / (int)baseCalculo));
                decimal valorPresente = valorFluxo / fator;

                vpl += valorPresente;

                cont += 1;

                #region Preenche lista de CashFlow
                CashFlow cf = new CashFlow((double)valorPresente, dataFluxo, numeroDias);

                listaCF.Add(cf);
                #endregion
            }

            return vpl;
        }

        /// <summary>
        /// Calcula fator de juros, considerando apropriação exponencial.
        /// </summary>
        /// <param name="numeroDias"></param>
        /// <param name="taxaOperacao"></param>
        /// <param name="baseCalculo"></param>
        /// <returns></returns>
        public static decimal CalculaFatorPreExponencial(int numeroDias, decimal taxaOperacao, BaseCalculo baseCalculo)
        {
            decimal expoente = (decimal)numeroDias / (decimal)baseCalculo;
            decimal baseExponenciacao = (taxaOperacao / 100) + 1;
            decimal fator = (decimal)Math.Pow((double)baseExponenciacao, (double)expoente);
            return fator;
        }

        /// <summary>
        /// Calcula fator de juros, considerando apropriação exponencial e dias corridos.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="taxaOperacao"></param>
        /// <param name="baseCalculo"></param>
        /// <returns></returns>
        public static decimal CalculaFatorPreExponencial(DateTime dataInicio, DateTime dataFim,
                       decimal taxaOperacao, BaseCalculo baseCalculo)
        {
            int numeroDias = Calendario.NumeroDias(dataInicio, dataFim);
            decimal expoente = (decimal)numeroDias / (decimal)baseCalculo;
            decimal baseExponenciacao = (taxaOperacao / 100) + 1;
            decimal fator = (decimal)Math.Pow((double)baseExponenciacao, (double)expoente);
            return fator;
        }

        /// <summary>
        /// Calcula fator de juros, considerando apropriação exponencial e dias úteis.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="taxaOperacao"></param>
        /// <param name="baseCalculo"></param>
        /// <param name="idLocal"></param>
        /// <param name="tipoFeriado"></param>
        /// <returns></returns>
        public static decimal CalculaFatorPreExponencial(DateTime dataInicio, DateTime dataFim,
                       decimal taxaOperacao, BaseCalculo baseCalculo, int idLocal, TipoFeriado tipoFeriado)
        {
            int numeroDias = Calendario.NumeroDias(dataInicio, dataFim, idLocal, tipoFeriado);

            decimal expoente = (decimal)numeroDias / (decimal)baseCalculo;
            decimal baseExponenciacao = (taxaOperacao / 100) + 1;
            decimal fator = (decimal)Math.Pow((double)baseExponenciacao, (double)expoente);
            return fator;
        }

        /// <summary>
        /// Calcula fator de juros, considerando apropriação linear e dias corridos.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="taxaOperacao"></param>
        /// <param name="baseCalculo"></param>
        /// <returns></returns>
        public static decimal CalculaFatorPreLinear(DateTime dataInicio, DateTime dataFim,
                       decimal taxaOperacao, BaseCalculo baseCalculo)
        {
            int numeroDias = Calendario.NumeroDias(dataInicio, dataFim);
            decimal multiplicante = taxaOperacao / 100;
            decimal multiplicador = (decimal)numeroDias / (decimal)baseCalculo;
            decimal fator = (multiplicante * multiplicador) + 1;
            return fator;
        }

        /// <summary>
        /// Calcula fator de juros, considerando apropriação linear e dias úteis.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="taxaOperacao"></param>
        /// <param name="baseCalculo"></param>
        /// <param name="idLocal"></param>
        /// <param name="tipoFeriado"></param>
        /// <returns></returns>        
        public static decimal CalculaFatorPreLinear(DateTime dataInicio, DateTime dataFim,
                       decimal taxaOperacao, BaseCalculo baseCalculo, int idLocal, TipoFeriado tipoFeriado)
        {
            int numeroDias = Calendario.NumeroDias(dataInicio, dataFim, idLocal, tipoFeriado);
            decimal multiplicante = taxaOperacao / 100;
            decimal multiplicador = (decimal)numeroDias / (decimal)baseCalculo;
            decimal fator = (multiplicante * multiplicador) + 1;
            return fator;
        }

        /// <summary>
        /// Calcula taxa de juros, considerando apropriação exponencial e dias úteis.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="baseCalculo"></param>
        /// <param name="puInicio"></param>
        /// <param name="puFim"></param>
        /// <param name="idLocal"></param>
        /// <param name="tipoFeriado"></param>
        /// <returns></returns>
        public static decimal CalculaTaxaExponencial(DateTime dataInicio, DateTime dataFim, BaseCalculo baseCalculo,
                                                     decimal puInicio, decimal puFim, int idLocal, TipoFeriado tipoFeriado)
        {
            int diasUteis = Calendario.NumeroDias(dataInicio, dataFim, idLocal, tipoFeriado);

            if (diasUteis == 0)
            {
                return 0;
            }
            else
            {
                decimal baseTaxa = (decimal)(puFim / puInicio);
                decimal expoenteTaxa = (decimal)baseCalculo / (decimal)diasUteis;
                decimal fator = (decimal)(Math.Pow((double)baseTaxa, (double)expoenteTaxa));
                decimal taxa = (fator - 1M) * 100M;
                return taxa;
            }
        }

        /// <summary>
        /// Calcula taxa de juros, considerando apropriação exponencial e dias úteis.
        /// </summary>
        /// <param name="numeroDias"></param>
        /// <param name="baseCalculo"></param>
        /// <param name="puInicio"></param>
        /// <param name="puFim"></param>
        /// <returns></returns>
        public static decimal CalculaTaxaExponencial(int numeroDias, BaseCalculo baseCalculo,
                                                     decimal puInicio, decimal puFim)
        {
            decimal baseTaxa = (decimal)(puFim / puInicio);
            decimal expoenteTaxa = (decimal)baseCalculo / (decimal)numeroDias;
            decimal fator = (decimal)(Math.Pow((double)baseTaxa, (double)expoenteTaxa));
            decimal taxa = (fator - 1M) * 100M;
            return taxa;
        }

        /// <summary>
        /// Converte taxa de juros de efetiva no período1 (periodoEfetivo) para efetiva em outro período (periodoCalcular).
        /// </summary>
        /// <param name="periodoCalcular"></param>
        /// <param name="periodoEfetivo"></param>
        /// <param name="taxa"></param>
        /// <returns></returns>
        public static decimal ConverteBaseTaxa(int periodoEfetivo, int periodoCalcular, decimal taxa)
        {
            decimal expoenteTaxa = (decimal)periodoCalcular / (decimal)periodoEfetivo;
            decimal taxaCalculada = (decimal)(Math.Pow((double)(1M + taxa / 100M), (double)expoenteTaxa) - 1);
            taxaCalculada = taxaCalculada * 100M;
            return taxaCalculada;
        }

        /// <summary>
        /// TODO: Comentar
        /// </summary>
        /// <param name="taxa"></param>
        /// <param name="baseCalculo"></param>
        /// <returns></returns>
        public static decimal ConverteBaseFator(decimal taxa, BaseCalculo baseCalculo)
        {
            decimal expoenteTaxa = 1 / (decimal)baseCalculo;
            return (decimal)(Math.Pow((double)((taxa / 100) + 1), (double)expoenteTaxa));
        }

        /// <summary>
        /// Interpola 2 vértices, retornando a taxa interpolada em formato a.a.
        /// Contagem de dias úteis. Base = 252.
        /// Pode usar as seguintes estruturas de juros: DI1 da BMF, DDI da BMF, Ponta Pre Swap BMF.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="estruturaCurva">Indica o tipo de interpolação a ser usado.</param>
        /// <returns></returns>
        public static decimal InterpolaExponencial(DateTime data, DateTime dataVencimento, EstruturaCurva estruturaCurva)
        {
            #region Interpolação pela ETTJ dos Futuros de DI1 da BMF
            if (estruturaCurva == EstruturaCurva.FuturoBMF_DI1)
            {
                int DiasUteisVencimento = Calendario.NumeroDias(data, dataVencimento,
                                                LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                #region Verifica se existe algum DI1 com este vencimento. Se existir, calculo a taxa dele direto.
                AtivoBMF ativoBMF = new AtivoBMF();
                if (ativoBMF.BuscaSerieAtivo("DI1", TipoMercadoBMF.Futuro, dataVencimento))
                {
                    string serie = ativoBMF.Serie;
                    CotacaoBMF cotacaoBMFAtivo = new CotacaoBMF();
                    cotacaoBMFAtivo.BuscaCotacaoBMF("DI1", serie, data);
                    decimal pu = cotacaoBMFAtivo.PUFechamento.Value;
                    decimal taxaFinal = (decimal)Math.Pow((double)(100000M / pu), (double)(252M / DiasUteisVencimento));
                    return taxaFinal;
                }
                #endregion

                #region Vértice Anterior
                CotacaoBMF cotacaoBMF = new CotacaoBMF();
                ativoBMF = new AtivoBMF();
                ativoBMF.BuscaSerieAnterior("DI1", TipoMercadoBMF.Futuro, dataVencimento, data);

                decimal fatorVerticeAnterior;
                int diasUteisVerticeAnterior;
                //Se não achar vértice anterior, pega o último CDI diário e considera como vértice de 1 dia.
                if (!ativoBMF.es.HasData)
                {
                    DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    decimal cotacaoCDI = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.CDI, dataAnterior);
                    fatorVerticeAnterior = (decimal)Math.Pow((double)(cotacaoCDI / 100M + 1), (double)(1M / 252M));
                    diasUteisVerticeAnterior = 1;
                }
                else
                {
                    string serieAnterior = ativoBMF.Serie;
                    DateTime dataVencimentoAnterior = ativoBMF.DataVencimento.Value;

                    cotacaoBMF.BuscaCotacaoBMF("DI1", serieAnterior, data);
                    decimal puAnterior = cotacaoBMF.PUFechamento.Value;

                    diasUteisVerticeAnterior = Calendario.NumeroDias(data, dataVencimentoAnterior,
                                                                        LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    //Calcula a taxa a partir do PU do DI1
                    fatorVerticeAnterior = 100000M / puAnterior;
                }
                #endregion

                #region Vértice Posterior
                //Vertice Posterior
                ativoBMF = new AtivoBMF();
                ativoBMF.BuscaSeriePosterior("DI1", TipoMercadoBMF.Futuro, dataVencimento);
                string seriePosterior = ativoBMF.Serie;
                DateTime dataVencimentoPosterior = ativoBMF.DataVencimento.Value;

                cotacaoBMF = new CotacaoBMF();
                cotacaoBMF.BuscaCotacaoBMF("DI1", seriePosterior, data);
                decimal puPosterior = cotacaoBMF.PUFechamento.Value;

                int diasUteisVerticePosterior = Calendario.NumeroDias(data, dataVencimentoPosterior,
                                                                    LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                //Calcula a taxa a partir do PU do DI1
                decimal fatorVerticePosterior = 100000M / puPosterior;
                #endregion

                #region Cálculo final da interpolação da taxa
                decimal expoente1 = (decimal)(diasUteisVerticePosterior - DiasUteisVencimento) /
                                    (diasUteisVerticePosterior - diasUteisVerticeAnterior);
                decimal fator1 = (decimal)Math.Pow((double)fatorVerticeAnterior, (double)expoente1);

                decimal expoente2 = (decimal)(DiasUteisVencimento - diasUteisVerticeAnterior) /
                                    (diasUteisVerticePosterior - diasUteisVerticeAnterior);
                decimal fator2 = (decimal)Math.Pow((double)fatorVerticePosterior, (double)expoente2);

                decimal fatorFinal = fator1 * fator2;
                decimal taxaInterpolada = (decimal)Math.Pow((double)fatorFinal, (double)(252M / DiasUteisVencimento));
                return taxaInterpolada;
                #endregion

            }
            #endregion

            #region Interpolação pela ETTJ dos Cupons Cambiais (Sujos) >> DDI da BMF
            if (estruturaCurva == EstruturaCurva.FuturoBMF_DDI)
            {
                int DiasUteisVencimento = Calendario.NumeroDias(data, dataVencimento,
                                                LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                #region Verifica se existe algum DI1 com este vencimento. Se existir, calculo a taxa dele direto.
                AtivoBMF ativoBMF = new AtivoBMF();
                if (ativoBMF.BuscaSerieAtivo("DDI", TipoMercadoBMF.Futuro, dataVencimento))
                {
                    string serie = ativoBMF.Serie;
                    CotacaoBMF cotacaoBMFAtivo = new CotacaoBMF();
                    cotacaoBMFAtivo.BuscaCotacaoBMF("DDI", serie, data);
                    decimal pu = cotacaoBMFAtivo.PUFechamento.Value;
                    decimal taxaFinal = (decimal)Math.Pow((double)(100000M / pu), (double)(252M / DiasUteisVencimento));
                    return taxaFinal;
                }
                #endregion

                #region Vértice Anterior
                CotacaoBMF cotacaoBMF = new CotacaoBMF();
                ativoBMF = new AtivoBMF();
                ativoBMF.BuscaSerieAnterior("DDI", TipoMercadoBMF.Futuro, dataVencimento, data);

                decimal fatorVerticeAnterior;
                int diasUteisVerticeAnterior;
                //Se não achar vértice anterior, pega o último CDI diário e considera como vértice de 1 dia.
                if (!ativoBMF.es.HasData)
                {
                    DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    decimal cotacaoCDI = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.CDI, dataAnterior);
                    fatorVerticeAnterior = (decimal)Math.Pow((double)(cotacaoCDI / 100M + 1), (double)(1M / 252M));
                    diasUteisVerticeAnterior = 1;
                }
                else
                {
                    string serieAnterior = ativoBMF.Serie;
                    DateTime dataVencimentoAnterior = ativoBMF.DataVencimento.Value;

                    cotacaoBMF.BuscaCotacaoBMF("DDI", serieAnterior, data);
                    decimal puAnterior = cotacaoBMF.PUFechamento.Value;

                    diasUteisVerticeAnterior = Calendario.NumeroDias(data, dataVencimentoAnterior,
                                                                        LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    //Calcula a taxa a partir do PU do DI1
                    fatorVerticeAnterior = 100000M / puAnterior;
                }
                #endregion

                #region Vértice Posterior
                //Vertice Posterior
                ativoBMF = new AtivoBMF();
                ativoBMF.BuscaSeriePosterior("DDI", TipoMercadoBMF.Futuro, dataVencimento);
                string seriePosterior = ativoBMF.Serie;
                DateTime dataVencimentoPosterior = ativoBMF.DataVencimento.Value;

                cotacaoBMF = new CotacaoBMF();
                cotacaoBMF.BuscaCotacaoBMF("DDI", seriePosterior, data);
                decimal puPosterior = cotacaoBMF.PUFechamento.Value;

                int diasUteisVerticePosterior = Calendario.NumeroDias(data, dataVencimentoPosterior,
                                                                    LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                //Calcula a taxa a partir do PU do DI1
                decimal fatorVerticePosterior = 100000M / puPosterior;
                #endregion

                #region Cálculo final da interpolação da taxa
                decimal expoente1 = (decimal)(diasUteisVerticePosterior - DiasUteisVencimento) /
                                    (diasUteisVerticePosterior - diasUteisVerticeAnterior);
                decimal fator1 = (decimal)Math.Pow((double)fatorVerticeAnterior, (double)expoente1);

                decimal expoente2 = (decimal)(DiasUteisVencimento - diasUteisVerticeAnterior) /
                                    (diasUteisVerticePosterior - diasUteisVerticeAnterior);
                decimal fator2 = (decimal)Math.Pow((double)fatorVerticePosterior, (double)expoente2);

                decimal fatorFinal = fator1 * fator2;
                decimal taxaInterpolada = (decimal)Math.Pow((double)fatorFinal, (double)(252M / DiasUteisVencimento));
                return taxaInterpolada;
                #endregion

            }
            #endregion

            #region Interpolação pela ETTJ dos Swaps Pre x DI da BMF
            else if (estruturaCurva == EstruturaCurva.SwapBMF_Pre)
            {
                int DiasUteisVencimento = Calendario.NumeroDias(data, dataVencimento,
                                                LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                #region Verifica se existe algum swap com este vencimento. Se existir, calculo a taxa dele direto.
                TabelaTaxaSwap tabelaTaxaSwap = new TabelaTaxaSwap();
                if (tabelaTaxaSwap.BuscaTabelaTaxaSwap(data, "PRE", DiasUteisVencimento))
                {
                    return tabelaTaxaSwap.Valor.Value;
                }
                #endregion

                #region Vértice Anterior
                tabelaTaxaSwap = new TabelaTaxaSwap();
                tabelaTaxaSwap.BuscaTabelaTaxaSwapAnterior(data, "PRE", DiasUteisVencimento);

                decimal fatorVerticeAnterior;
                int diasUteisVerticeAnterior;
                //Se não achar vértice anterior, pega o último CDI diário e considera como vértice de 1 dia.
                if (!tabelaTaxaSwap.es.HasData)
                {
                    DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    decimal cotacaoCDI = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.CDI, dataAnterior);
                    fatorVerticeAnterior = (decimal)Math.Pow((double)(cotacaoCDI / 100M + 1), (double)(1M / 252M));
                    diasUteisVerticeAnterior = 1;
                }
                else
                {
                    diasUteisVerticeAnterior = tabelaTaxaSwap.DiasUteis.Value;
                    decimal taxaVerticeAnterior = tabelaTaxaSwap.Valor.Value;
                    //Passa o fator de a.a. para fator no periodo.
                    fatorVerticeAnterior = (decimal)Math.Pow((double)(taxaVerticeAnterior / 100M + 1),
                                                                    (double)(DiasUteisVencimento / 252M));
                }
                #endregion

                #region Vértice Posterior
                tabelaTaxaSwap = new TabelaTaxaSwap();
                tabelaTaxaSwap.BuscaTabelaTaxaSwapPosterior(data, "PRE", DiasUteisVencimento);

                decimal fatorVerticePosterior;
                int diasUteisVerticePosterior;

                diasUteisVerticePosterior = tabelaTaxaSwap.DiasUteis.Value;
                decimal taxaVerticePosterior = tabelaTaxaSwap.Valor.Value;
                //Passa o fator de a.a. para fator no periodo.
                fatorVerticePosterior = (decimal)Math.Pow((double)(taxaVerticePosterior / 100M + 1),
                                                                (double)(DiasUteisVencimento / 252M));
                #endregion

                #region Cálculo final da interpolação da taxa
                decimal expoente1 = (decimal)(diasUteisVerticePosterior - DiasUteisVencimento) /
                                    (diasUteisVerticePosterior - diasUteisVerticeAnterior);
                decimal fator1 = (decimal)Math.Pow((double)fatorVerticeAnterior, (double)expoente1);

                decimal expoente2 = (decimal)(DiasUteisVencimento - diasUteisVerticeAnterior) /
                                    (diasUteisVerticePosterior - diasUteisVerticeAnterior);
                decimal fator2 = (decimal)Math.Pow((double)fatorVerticePosterior, (double)expoente2);

                decimal fatorFinal = fator1 * fator2;
                decimal taxaInterpolada = (decimal)Math.Pow((double)fatorFinal, (double)(252M / DiasUteisVencimento));
                return taxaInterpolada;
                #endregion
            }
            else
            {
                return 0;
            }
            #endregion
        }

        /// <summary>
        /// Calcula fator do índice, dado o percentual "modificador". Aplica-se a índices decimais ou percentuais. 
        /// Se não achar registro de cotação no período, retorna fator = 1.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="idIndice"></param>
        /// <param name="percentualIndice"></param>
        /// <param name="indiceDecimalDia">se True - busca as cotações das próprias datas Inicio e Fim
        ///                                se False - busca as cotações de D-1 em relação às datas Inicio e Fim</param>
        /// <returns>Fator do período.</returns>
        public static decimal CalculaFatorIndice(DateTime dataInicio, DateTime dataFim, int idIndice,
                                                 decimal percentualIndice, bool indiceDecimalDia)
        {
            CotacaoIndiceCollection cotacaoIndiceCollection = new CotacaoIndiceCollection();
            Indice indice = new Indice();

            indice.LoadByPrimaryKey((short)idIndice);
            int tipoIndice = indice.Tipo.Value;
            int tipoDivulgacao = indice.TipoDivulgacao.Value;
            int? idIndiceBase = indice.IdIndiceBase;
            decimal? percentualIndiceBase = indice.Percentual;

            if (idIndiceBase.HasValue)
            {
                idIndice = idIndiceBase.Value;
            }

            if (percentualIndiceBase.HasValue)
            {
                percentualIndice = percentualIndiceBase.Value;
            }

            //Testa se deve pegar as cotações em D0 ou o default usual (D-1)
            if (tipoIndice == (int)TipoIndice.Decimal && !indiceDecimalDia)
            {
                dataInicio = Calendario.SubtraiDiaUtil(dataInicio, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                dataFim = Calendario.SubtraiDiaUtil(dataFim, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }

            decimal fator = 1;
            if (tipoIndice == (byte)TipoIndice.Decimal &&
                        (tipoDivulgacao == (byte)TipoDivulgacaoIndice.Mensal ||
                         tipoDivulgacao == (byte)TipoDivulgacaoIndice.Mensal_1 ||
                         tipoDivulgacao == (byte)TipoDivulgacaoIndice.Mensal_2 ||
                         tipoDivulgacao == (byte)TipoDivulgacaoIndice.Mensal_3))
            {
                fator = CalculaFatorIndicePreco(dataInicio, dataFim, indice, percentualIndice);
            }
            else
            {
                if (tipoIndice == (int)TipoIndice.Decimal)
                {
                    fator = CalculaFatorIndiceDecimal(dataInicio, dataFim, idIndice, percentualIndice);
                }
                else
                {
                    fator = CalculaFatorIndicePercentual(dataInicio, dataFim, indice, percentualIndice, false);
                }
            }

            return fator;
        }

        /// <summary>
        /// Calcula o fator acumulado para taxa Anbid (dias úteis 252).  
        /// Obs: Não trabalha com percentual modificador.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>        
        /// <returns></returns>
        public static decimal CalculaFatorAnbid(DateTime dataInicio, DateTime dataFim)
        {
            int diasUteis = Calendario.NumeroDias(dataInicio, dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            decimal taxaAnbid = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.ANBID, dataInicio);
            decimal fatorAnbid = (decimal)Math.Pow((double)(1 + (taxaAnbid / 100M)), (double)(diasUteis / 252M));

            fatorAnbid = Math.Round(fatorAnbid, 8);

            return fatorAnbid;
        }

        /// <summary>
        /// Calcula o fator acumulado para TR (considera o dia da data de vencimento como base para o dia da TR em meses anteriores).
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="diaAniversario"></param>
        /// <param name="percentualIndice"></param>
        /// <returns></returns>
        public static decimal CalculaFatorTR(DateTime dataInicio, DateTime dataFim, int diaAniversario, decimal percentualIndice)
        {
            CotacaoIndice cotacaoIndice = new CotacaoIndice();

            decimal fatorAcumulado = 1;

            DateTime dataCotacao = new DateTime();

            bool dataValida = false;
            while (!dataValida)
            {
                try
                {
                    DateTime data = new DateTime(dataFim.Year, dataFim.Month, diaAniversario);
                    dataCotacao = data.AddMonths(-1);
                    dataValida = true;
                }
                catch (Exception)
                {
                    diaAniversario = diaAniversario - 1;
                }
            }


            DateTime dataAux = dataFim;

            while (dataAux > dataInicio)
            {
                decimal cotacaoTR = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.TR, dataCotacao);

                DateTime dataAnterior = new DateTime();
                if (dataCotacao < dataInicio)
                {
                    dataAnterior = dataInicio;
                }
                else
                {
                    dataAnterior = dataCotacao;
                }

                int numeroDiasMes = Calendario.NumeroDias(dataCotacao, dataCotacao.AddMonths(1), (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                int numeroDias = Calendario.NumeroDias(dataAnterior, dataAux, (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                decimal fator = Convert.ToDecimal(Math.Pow((double)(1M + cotacaoTR / 100M), (double)(numeroDias / numeroDiasMes)));
                fatorAcumulado = fatorAcumulado * fator;

                dataAux = dataAnterior;
                dataCotacao = dataCotacao.AddMonths(-1);
            }

            return fatorAcumulado;
        }

        /// <summary>
        /// Calcula o fator acumulado para TJLP.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public static decimal CalculaFatorTJLP(DateTime dataInicio, DateTime dataFim, out List<CorrecaoIndice> listaCorrecaoIndice)
        {
            listaCorrecaoIndice = new List<CorrecaoIndice>();

            decimal fatorAcumulado = 1;
            DateTime dataAux = dataInicio;
            DateTime dataAnterior = dataAux;
            decimal taxaTJLP = 0;
            while (dataAux <= dataFim)
            {
                DateTime dataPrimeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(dataAux, 0);
                DateTime dataUltimoDiaMes = Calendario.RetornaUltimoDiaCorridoMes(dataAux, 0);

                if (dataAnterior.Month != dataAux.Month)
                {
                    CotacaoIndiceCollection cotacaoIndiceCollection = new CotacaoIndiceCollection();
                    cotacaoIndiceCollection.Query.Select(cotacaoIndiceCollection.Query.Valor);
                    cotacaoIndiceCollection.Query.Where(cotacaoIndiceCollection.Query.Data.GreaterThanOrEqual(dataPrimeiroDiaMes),
                                                        cotacaoIndiceCollection.Query.Data.LessThanOrEqual(dataUltimoDiaMes),
                                                        cotacaoIndiceCollection.Query.IdIndice.Equal(ListaIndiceFixo.TJLP));
                    cotacaoIndiceCollection.Query.Load();

                    taxaTJLP = cotacaoIndiceCollection.Count > 0 ? cotacaoIndiceCollection[0].Valor.Value : 0;
                }

                int diasMes = Calendario.NumeroDias(dataPrimeiroDiaMes, dataUltimoDiaMes);

                decimal expoente = (decimal)1M / (decimal)diasMes;
                decimal baseExponenciacao = (taxaTJLP / 100) + 1;
                decimal fatorDia = (decimal)Math.Pow((double)baseExponenciacao, (double)expoente);

                fatorAcumulado = fatorAcumulado * fatorDia;

                CorrecaoIndice correcaoIndice = new CorrecaoIndice();
                correcaoIndice.dataCalculo = dataAux;
                correcaoIndice.dataCalculoEfetiva = dataAux;
                correcaoIndice.dataIndice = dataAux;
                correcaoIndice.fatorCorrecao = fatorDia;
                correcaoIndice.fatorAcumulado = fatorAcumulado;
                correcaoIndice.numeroDias = 1;
                correcaoIndice.numeroDiasTotal = diasMes;
                correcaoIndice.valorIndice = taxaTJLP;

                listaCorrecaoIndice.Add(correcaoIndice);

                dataAnterior = dataAux;
                dataAux = dataAux.AddDays(1);
            }

            return fatorAcumulado;
        }

        /// <summary>
        /// Calcula o fator acumulado para TR, usando o método data a data (usado no cálculo da poupança).  
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        private static decimal CalculaFatorPoupanca(DateTime dataInicio, DateTime dataFim, out List<CorrecaoIndice> listaCorrecaoIndice)
        {
            decimal taxa = 0.5M;
            listaCorrecaoIndice = new List<CorrecaoIndice>();

            DateTime dataAux = dataInicio;

            bool final = false;
            decimal fator = 1;
            while (!final)
            {
                DateTime dataMesProx = dataAux.AddMonths(1);
                if (dataMesProx > dataFim)
                {
                    final = true;
                }
                else
                {
                    decimal cotacaoTR = 0;
                    try
                    {
                        CotacaoIndice cotacaoIndice = new CotacaoIndice();
                        cotacaoTR = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.TR, dataAux);
                    }
                    catch (Exception)
                    {
                    }

                    decimal fatorTR = cotacaoTR / 100M + 1M;
                    fator = fator * fatorTR * (taxa / 100M + 1M);

                    CorrecaoIndice correcaoIndice = new CorrecaoIndice();
                    correcaoIndice.dataCalculo = dataAux;
                    correcaoIndice.dataCalculoEfetiva = dataAux;
                    correcaoIndice.dataIndice = dataAux;
                    correcaoIndice.fatorCorrecao = fatorTR;
                    correcaoIndice.fatorAcumulado = fator;
                    correcaoIndice.numeroDias = 0;
                    correcaoIndice.numeroDiasTotal = 0;
                    correcaoIndice.valorIndice = cotacaoTR;

                    listaCorrecaoIndice.Add(correcaoIndice);

                    dataAux = dataAux.AddMonths(1);
                }
            }

            return fator;
        }

        /// <summary>
        /// Calcula fator do índice, dado o percentual "modificador". Aplica-se a índices percentuais. 
        /// Base de cálculo = 252 dias.
        /// Tratamento especial para cálculo da TR.
        /// Se não achar registro de cotação no período, retorna fator = 1.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="idIndice"></param>
        /// <param name="percentualIndice"></param>
        /// <returns>Fator do período.</returns>
        public static decimal CalculaFatorIndicePercentual(DateTime dataInicio, DateTime dataFim, Indice indice,
                                                           decimal percentualIndice, bool trunca)
        {
            List<CorrecaoIndice> listaCorrecaoIndice = new List<CorrecaoIndice>();
            return CalculaFatorIndicePercentual(dataInicio, dataFim, indice, percentualIndice, trunca, out listaCorrecaoIndice);
        }

        /// <summary>
        /// Calcula fator do índice, dado o percentual "modificador". Aplica-se a índices percentuais. 
        /// Base de cálculo = 252 dias.
        /// Tratamento especial para cálculo da TR.
        /// Se não achar registro de cotação no período, retorna fator = 1.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="idIndice"></param>
        /// <param name="percentualIndice"></param>
        /// <returns>Fator do período.</returns>
        public static decimal CalculaFatorIndicePercentual(DateTime dataInicio, DateTime dataFim, Indice indice,
                                                           decimal percentualIndice, bool trunca, out List<CorrecaoIndice> listaCorrecaoIndice)
        {
            listaCorrecaoIndice = new List<CorrecaoIndice>();

            int idIndice = indice.IdIndice.Value;
            if (indice.IdIndiceBase.HasValue)
            {
                idIndice = indice.IdIndiceBase.Value;
            }
            byte tipoDivulgacao = indice.TipoDivulgacao.Value;

            CotacaoIndiceCollection cotacaoIndiceCollection = new CotacaoIndiceCollection();

            decimal fator = 1;

            if (idIndice == (int)ListaIndiceFixo.POUPANCA)
            {
                return CalculaFatorPoupanca(dataInicio, dataFim, out listaCorrecaoIndice);
            }
            else if (idIndice == (int)ListaIndiceFixo.TJLP)
            {
                return CalculaFatorTJLP(dataInicio, dataFim, out listaCorrecaoIndice);
            }

            switch (tipoDivulgacao)
            {
                case (byte)TipoDivulgacaoIndice.Diario_1:
                    dataInicio = Calendario.SubtraiDiaUtil(dataInicio, 1);
                    dataFim = Calendario.SubtraiDiaUtil(dataFim, 1);
                    break;
                case (byte)TipoDivulgacaoIndice.Diario_2:
                    dataInicio = Calendario.SubtraiDiaUtil(dataInicio, 2);
                    dataFim = Calendario.SubtraiDiaUtil(dataFim, 2);
                    break;
                case (byte)TipoDivulgacaoIndice.Diario_3:
                    dataInicio = Calendario.SubtraiDiaUtil(dataInicio, 3);
                    dataFim = Calendario.SubtraiDiaUtil(dataFim, 3);
                    break;
            }

            if (cotacaoIndiceCollection.BuscaCotacao(dataInicio, dataFim, idIndice))
            {
                foreach (CotacaoIndice cotacaoIndice in cotacaoIndiceCollection)
                {
                    decimal valor = cotacaoIndice.Valor.Value;
                    decimal fatorDia = 1;
                    if (trunca)
                    {
                        fatorDia = Math.Round(CalculaFatorPreExponencial(1, valor, BaseCalculo.Base252), 8);
                        fatorDia = Utilitario.Truncate((fatorDia - 1) * (percentualIndice / 100M) + 1, 16);
                        fator = Utilitario.Truncate(fator * fatorDia, 16);
                        fator = Math.Round(fator, 8);
                    }
                    else
                    {
                        fatorDia = Math.Round(CalculaFatorPreExponencial(1, valor, BaseCalculo.Base252), 8);
                        fatorDia = Utilitario.Truncate((fatorDia - 1) * (percentualIndice / 100M) + 1, 16);
                        fator = Utilitario.Truncate(fator * fatorDia, 16);
                    }

                    CorrecaoIndice correcaoIndice = new CorrecaoIndice();
                    correcaoIndice.dataCalculo = cotacaoIndice.Data.Value;
                    correcaoIndice.dataCalculoEfetiva = cotacaoIndice.Data.Value;
                    correcaoIndice.dataIndice = cotacaoIndice.Data.Value;
                    correcaoIndice.fatorCorrecao = fatorDia;
                    correcaoIndice.fatorAcumulado = fator;
                    correcaoIndice.numeroDias = 1;
                    correcaoIndice.numeroDiasTotal = 1;
                    correcaoIndice.valorIndice = valor;

                    listaCorrecaoIndice.Add(correcaoIndice);
                }
            }

            return fator;
        }

        /// <summary>
        /// Calcula fator do índice, dado o percentual "modificador". Aplica-se a índices decimais.
        /// Tratamento especial para índices mensais.
        /// Se não achar registro de cotação no período, retorna fator = 1.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="idIndice"></param>
        /// <param name="percentualIndice"></param>
        /// <param name="tipoDivulgacao"></param>
        /// <returns>Fator do período.</returns>
        public static decimal CalculaFatorIndiceDecimal(DateTime dataInicio, DateTime dataFim, int idIndice,
                                                        decimal percentualIndice)
        {
            List<CorrecaoIndice> listaCorrecaoIndice = new List<CorrecaoIndice>();
            return CalculaFatorIndiceDecimal(dataInicio, dataFim, idIndice, percentualIndice, out listaCorrecaoIndice);
        }

        /// <summary>
        /// Calcula fator do índice, dado o percentual "modificador". Aplica-se a índices decimais.
        /// Tratamento especial para índices mensais.
        /// Se não achar registro de cotação no período, retorna fator = 1.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="idIndice"></param>
        /// <param name="percentualIndice"></param>
        /// <param name="tipoDivulgacao"></param>
        /// <returns>Fator do período.</returns>
        public static decimal CalculaFatorIndiceDecimal(DateTime dataInicio, DateTime dataFim, int idIndice,
                                                        decimal percentualIndice, out List<CorrecaoIndice> listaCorrecaoIndice)
        {
            listaCorrecaoIndice = new List<CorrecaoIndice>();

            CotacaoIndiceCollection cotacaoIndiceCollection = new CotacaoIndiceCollection();

            decimal fator = 1;

            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            cotacaoIndice.BuscaCotacaoIndice(idIndice, dataInicio);
            decimal cotacaoInicio = cotacaoIndice.Valor.Value;

            cotacaoIndice = new CotacaoIndice();
            cotacaoIndice.BuscaCotacaoIndice(idIndice, dataFim);
            decimal cotacaoFim = cotacaoIndice.Valor.Value;

            if (cotacaoInicio != 0) //Se cotação do índice está zerada, assume fator = 1
            {
                fator = (((cotacaoFim / cotacaoInicio) - 1) * (percentualIndice / 100M) + 1);
            }

            CorrecaoIndice correcaoIndice = new CorrecaoIndice();
            correcaoIndice.dataCalculo = dataFim;
            correcaoIndice.dataCalculoEfetiva = dataFim;
            correcaoIndice.dataIndice = dataFim;
            correcaoIndice.fatorCorrecao = fator;
            correcaoIndice.fatorAcumulado = fator;
            correcaoIndice.numeroDias = 1;
            correcaoIndice.numeroDiasTotal = 1;
            correcaoIndice.valorIndice = fator;

            listaCorrecaoIndice.Add(correcaoIndice);

            return fator;
        }

        /// <summary>
        /// Calcula fator do índice, dado o percentual "modificador". Aplica-se a índices decimais. 
        /// Base de cálculo = 30 dias (mensal).
        /// Se não achar registro de cotação no período, retorna fator = 1.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="idIndice"></param>
        /// <param name="percentualIndice"></param>
        /// <returns>Fator do período.</returns>
        public static decimal CalculaFatorIndicePercentualMensal(DateTime dataInicio, DateTime dataFim, int idIndice, decimal percentualIndice)
        {

            CotacaoIndiceCollection cotacaoIndiceCollection = new CotacaoIndiceCollection();

            decimal fator = 1;

            DateTime dataAux = dataInicio;
            while (dataAux.Month <= dataFim.Month)
            {
                DateTime primeiroDiaMes = new DateTime(dataAux.Year, dataAux.Month, 01);
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                decimal cotacao = cotacaoIndice.RetornaCotacaoIndiceMes(idIndice, primeiroDiaMes);

                DateTime ultimoDiaMes = Calendario.RetornaUltimoDiaCorridoMes(dataAux, 0);
                if (dataAux.Month < dataFim.Month)
                {
                    if (dataAux.Day > 1) //Calcula prorata do mês se estiver em dia dif. do 1o dia do mês
                    {
                        int diasNumerador = ultimoDiaMes.Day - dataAux.Day;
                        fator = fator * (cotacao * (diasNumerador / 30) / 100M + 1M);
                    }
                    else
                    {
                        fator = fator * (cotacao / 100M + 1M);
                    }
                }
                else //Mês em questão, precisa calcular o prorata, se estiver em dia dif. do último dia do mês
                {
                    if (dataFim.Day < ultimoDiaMes.Day)
                    {
                        int diasNumerador = dataFim.Day; //Calcula exatos número de dias
                        fator = fator * (cotacao * (diasNumerador / 30) / 100M + 1M);
                    }
                    else
                    {
                        fator = fator * (cotacao / 100M + 1M);
                    }
                }
                dataAux = primeiroDiaMes.AddMonths(1);
            }

            fator = (fator - 1M) * (percentualIndice / 100M) + 1M;
            return fator;
        }

        /// <summary>
        /// Calcula fator do índice para IGPM, IPCA etc, para dias corridos.
        /// Se não achar registro de cotação no período, retorna fator = 1.
        /// </summary>
        /// <param name="listaDatas"></param>
        /// <param name="indice"></param>
        /// <param name="listaDatas"></param>
        /// <param name="percentualIndice"></param>
        /// <param name="diaAniversario"></param>
        /// <returns>Fator do período.</returns>
        public static decimal CalculaFatorIndicePreco(List<DateTime> listaDatas, Indice indice, decimal percentualIndice, int diaAniversario)
        {
            List<CorrecaoIndice> listaCorrecaoIndice = new List<CorrecaoIndice>();
            return CalculaFatorIndicePreco(listaDatas, indice, percentualIndice, diaAniversario, null, null, out listaCorrecaoIndice);
        }

        /// <summary>
        /// Calcula fator do índice para IGPM, IPCA etc, para dias uteis.
        /// Se não achar registro de cotação no período, retorna fator = 1.
        /// </summary>
        /// <param name="listaDatas"></param>
        /// <param name="indice"></param>
        /// <param name="percentualIndice"></param>
        /// <param name="diaAniversario"></param>
        /// <param name="idLocal"></param>
        /// <param name="tipoFeriado"></param>
        /// <returns>Fator do período.</returns>
        public static decimal CalculaFatorIndicePreco(List<DateTime> listaDatas, Indice indice, decimal percentualIndice, int diaAniversario,
                                                        int? idLocal, TipoFeriado? tipoFeriado)
        {
            List<CorrecaoIndice> listaCorrecaoIndice = new List<CorrecaoIndice>();
            return CalculaFatorIndicePreco(listaDatas, indice, percentualIndice, diaAniversario, idLocal, tipoFeriado, out listaCorrecaoIndice);
        }

        /// <summary>
        /// Calcula fator do índice para IGPM, IPCA etc.
        /// O parametro listaCorrecaoIndice serve para preenchimento de uma lista com os valores usados para correção.
        /// Se não achar registro de cotação no período, retorna fator = 1.
        /// </summary>
        /// <param name="listaDatas"></param>
        /// <param name="indice"></param>
        /// <param name="percentualIndice"></param>
        /// <param name="diaAniversario"></param>
        /// <param name="idLocal"></param>
        /// <param name="tipoFeriado"></param>
        /// <param name="listaCorrecaoIndice"></param>
        /// <returns>Fator do período.</returns>
        public static decimal CalculaFatorIndicePreco(List<DateTime> listaDatas, Indice indice, decimal percentualIndice, int diaAniversario,
                                                        int? idLocal, TipoFeriado? tipoFeriado, out List<CorrecaoIndice> listaCorrecaoIndice)
        {
            listaCorrecaoIndice = new List<CorrecaoIndice>();

            #region Carrega informações de Indice (idIndice e tipoDivulgacao)
            int idIndice = indice.IdIndice.Value;
            byte tipoDivulgacao = indice.TipoDivulgacao.Value;

            if (indice.IdIndiceBase.HasValue)
            {
                idIndice = indice.IdIndiceBase.Value;
            }
            #endregion

            int cont = 0;
            decimal fatorAcumulado = 1;
            foreach (DateTime data in listaDatas)
            {
                DateTime dataProxAniversarioReal = data.AddMonths(1);

                int diaAniversarioAux = diaAniversario;
                bool dataValida = false;
                while (!dataValida)
                {
                    try
                    {
                        dataProxAniversarioReal = new DateTime(dataProxAniversarioReal.Year, dataProxAniversarioReal.Month, diaAniversarioAux);
                        dataValida = true;
                    }
                    catch (Exception e)
                    {
                        diaAniversarioAux -= 1;
                    }
                }

                if (idLocal.HasValue)
                {
                    #region Trata dia nao util para dataProxAniversarioReal
                    if (!Calendario.IsDiaUtil(dataProxAniversarioReal, idLocal.Value, tipoFeriado.Value))
                    {
                        dataProxAniversarioReal = Calendario.AdicionaDiaUtil(dataProxAniversarioReal, 1, idLocal.Value, tipoFeriado.Value);
                    }
                    #endregion
                }

                DateTime dataProxAniversariolista = new DateTime();
                if ((cont + 1) < listaDatas.Count)
                {
                    dataProxAniversariolista = listaDatas[cont + 1];

                    decimal razaoDias = 1;
                    int numeroDias = 0;
                    int numeroDiasMes = 0;

                    DateTime dataAniversarioMes = data;
                    if (cont == 0) //Trata possivel descasamento do dia da data de emissão com o dia da data de vencimento
                    {
                        diaAniversarioAux = diaAniversario;
                        dataValida = false;
                        while (!dataValida)
                        {
                            try
                            {
                                dataAniversarioMes = new DateTime(data.Year, data.Month, diaAniversarioAux);
                                dataValida = true;
                            }
                            catch (Exception e)
                            {
                                diaAniversarioAux -= 1;
                            }
                        }

                        if (idLocal.HasValue)
                        {
                            if (!Calendario.IsDiaUtil(dataAniversarioMes, idLocal.Value, tipoFeriado.Value))
                            {
                                dataAniversarioMes = Calendario.AdicionaDiaUtil(dataAniversarioMes, 1, idLocal.Value, tipoFeriado.Value);
                            }
                        }
                    }

                    if (idLocal.HasValue)
                    {
                        numeroDias = Calendario.NumeroDias(data, dataProxAniversariolista, idLocal.Value, tipoFeriado.Value);
                        numeroDiasMes = Calendario.NumeroDias(dataAniversarioMes, dataProxAniversarioReal, idLocal.Value, tipoFeriado.Value);
                    }
                    else
                    {
                        numeroDias = Calendario.NumeroDias(data, dataProxAniversariolista);
                        numeroDiasMes = Calendario.NumeroDias(dataAniversarioMes, dataProxAniversarioReal);
                    }

                    if ((dataProxAniversariolista != dataProxAniversarioReal) || (dataAniversarioMes != data))
                    {
                        razaoDias = (decimal)numeroDias / numeroDiasMes;
                    }


                    #region Busca cotacoes do indice de preco na data (cotacaoIndiceMes) e data anterior (cotacaoIndiceMesAnterior)
                    DateTime dataIndice = data;

                    switch (tipoDivulgacao)
                    {
                        case (byte)TipoDivulgacaoIndice.Mensal_1:
                            dataIndice = Calendario.RetornaPrimeiroDiaCorridoMes(dataIndice, -1);
                            break;
                        case (byte)TipoDivulgacaoIndice.Mensal_2:
                            dataIndice = Calendario.RetornaPrimeiroDiaCorridoMes(dataIndice, -2);
                            break;
                        case (byte)TipoDivulgacaoIndice.Mensal_3:
                            dataIndice = Calendario.RetornaPrimeiroDiaCorridoMes(dataIndice, -3);
                            break;
                    }

                    DateTime dataIndiceAnterior = Calendario.RetornaPrimeiroDiaCorridoMes(dataIndice, -1);

                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    decimal cotacaoIndiceMes = cotacaoIndice.RetornaCotacaoIndiceMes(idIndice, dataIndice);
                    decimal cotacaoIndiceMesAnterior = cotacaoIndice.RetornaCotacaoIndiceMes(idIndice, dataIndiceAnterior);
                    #endregion

                    if (cotacaoIndiceMes != 0 && cotacaoIndiceMesAnterior != 0)
                    {
                        decimal fatorIndice = cotacaoIndiceMes / cotacaoIndiceMesAnterior;

                        decimal fatorPeriodo = (decimal)Math.Pow((double)fatorIndice, (double)(razaoDias));

                        fatorAcumulado = fatorAcumulado * fatorPeriodo;

                        CorrecaoIndice correcaoIndice = new CorrecaoIndice();
                        correcaoIndice.dataCalculo = data;

                        correcaoIndice.dataCalculoEfetiva = data;
                        if (idLocal.HasValue && !Calendario.IsDiaUtil(data, idLocal.Value, tipoFeriado.Value))
                        {
                            correcaoIndice.dataCalculoEfetiva = Calendario.AdicionaDiaUtil(data, 1, idLocal.Value, tipoFeriado.Value);
                        }

                        correcaoIndice.dataIndice = dataIndice;
                        correcaoIndice.fatorCorrecao = fatorPeriodo;
                        correcaoIndice.fatorAcumulado = fatorAcumulado;
                        correcaoIndice.numeroDias = numeroDias;
                        correcaoIndice.numeroDiasTotal = numeroDiasMes;
                        correcaoIndice.valorIndice = (fatorIndice - 1) * 100M;

                        listaCorrecaoIndice.Add(correcaoIndice);
                    }
                }

                cont += 1;
            }

            fatorAcumulado = (fatorAcumulado - 1M) * (percentualIndice / 100M) + 1M;

            return fatorAcumulado;
        }


        /// <summary>
        /// Calcula fator do índice para IGPM, IPCA etc, para dias corridos.
        /// Se não achar registro de cotação no período, retorna fator = 1.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataReferencia"></param>
        /// <param name="diaAniversario"></param>
        /// <param name="indice"></param>
        /// <param name="percentualIndice"></param>
        /// <returns>Fator do período.</returns>
        public static decimal CalculaFatorIndicePreco(DateTime dataInicio, DateTime dataReferencia, int diaAniversario, Indice indice,
                                                    decimal percentualIndice)
        {
            List<DateTime> listaDatas = RetornaDatasAniversario(dataInicio, dataReferencia, diaAniversario);
            return CalculaFatorIndicePreco(listaDatas, indice, percentualIndice, diaAniversario);
        }

        /// <summary>
        /// Calcula fator do índice para IGPM, IPCA etc, para dias uteis.
        /// Se não achar registro de cotação no período, retorna fator = 1.
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataReferencia"></param>
        /// <param name="diaAniversario"></param>
        /// <param name="percentualIndice"></param>
        /// <param name="indice"></param>
        /// <param name="idLocal"></param>
        /// <param name="tipoFeriado"></param>
        /// <returns>Fator do período.</returns>
        public static decimal CalculaFatorIndicePreco(DateTime dataInicio, DateTime dataReferencia, int diaAniversario, Indice indice,
                                                    decimal percentualIndice, int idLocal, TipoFeriado tipoFeriado)
        {
            List<DateTime> listaDatas = RetornaDatasAniversario(dataInicio, dataReferencia, diaAniversario, idLocal, tipoFeriado);
            return CalculaFatorIndicePreco(listaDatas, indice, percentualIndice, diaAniversario, idLocal, tipoFeriado);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="indice"></param>
        /// <param name="percentualIndice"></param>
        /// <returns></returns>
        public static decimal CalculaFatorIndicePreco(DateTime dataInicio, DateTime dataFim, Indice indice,
                                                    decimal percentualIndice)
        {
            return CalculaFatorIndicePreco(dataInicio, dataFim, dataFim.Day, indice, percentualIndice);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="indice"></param>
        /// <param name="percentualIndice"></param>
        /// <param name="idLocal"></param>
        /// <param name="tipoFeriado"></param>
        /// <returns></returns>
        public static decimal CalculaFatorIndicePreco(DateTime dataInicio, DateTime dataFim, Indice indice,
                                                    decimal percentualIndice, bool mesExato, int idLocal, TipoFeriado tipoFeriado)
        {
            return CalculaFatorIndicePreco(dataInicio, dataFim, dataFim.Day, indice, percentualIndice, idLocal, tipoFeriado);
        }


        /// <summary>
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataRefererencia"></param>
        /// <param name="diaAniversario"></param>
        public static List<DateTime> RetornaDatasAniversario(DateTime dataInicio, DateTime dataFim)
        {
            return RetornaDatasAniversario(dataInicio, dataFim, dataFim.Day);
        }

        /// <summary>
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataRefererencia"></param>
        /// <param name="diaAniversario"></param>
        public static List<DateTime> RetornaDatasAniversario(DateTime dataInicio, DateTime dataFim, int idLocal, TipoFeriado tipoFeriado)
        {
            return RetornaDatasAniversario(dataInicio, dataFim, dataFim.Day, idLocal, tipoFeriado);
        }

        /// <summary>
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        public static List<DateTime> RetornaDatasAniversario(DateTime dataInicio, DateTime dataRefererencia, int diaAniversario)
        {
            return RetornaDatasAniversario(dataInicio, dataRefererencia, diaAniversario, null, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="localFeriado"></param>
        /// <param name="tipoferiado"></param>
        /// <returns></returns>
        public static List<DateTime> RetornaDatasAniversario(DateTime dataInicio, DateTime dataRefererencia, int diaAniversario,
                                                             int? idLocal, TipoFeriado? tipoFeriado)
        {
            List<DateTime> retorno = new List<DateTime>();

            if (idLocal.HasValue)
            {
                #region Dia util para dataInicio
                if (!Calendario.IsDiaUtil(dataInicio, idLocal.Value, tipoFeriado.Value))
                {
                    dataInicio = Calendario.AdicionaDiaUtil(dataInicio, 1, idLocal.Value, tipoFeriado.Value);
                }
                #endregion
            }

            retorno.Add(dataInicio);
            retorno.Add(dataRefererencia);
            //
            DateTime dataAux = new DateTime();
            bool dataValida = false;
            while (!dataValida)
            {
                try
                {
                    dataAux = new DateTime(dataRefererencia.Year, dataRefererencia.Month, diaAniversario);
                    dataValida = true;
                }
                catch (Exception e)
                {
                    diaAniversario -= 1;
                }
            }


            if (idLocal.HasValue)
            {
                #region Dia util para dataAux
                if (!Calendario.IsDiaUtil(dataAux, idLocal.Value, tipoFeriado.Value))
                {
                    DateTime dataAuxUtil = Calendario.AdicionaDiaUtil(dataAux, 1, idLocal.Value, tipoFeriado.Value);
                    if (dataAuxUtil.Month != dataAux.Month) //nao deixa virar para o mês seguinte
                    {
                        dataAuxUtil = Calendario.SubtraiDiaUtil(dataAux, 1, idLocal.Value, tipoFeriado.Value);
                    }

                    dataAux = dataAuxUtil;
                }
                #endregion
            }

            if (dataAux.Day < dataRefererencia.Day && dataAux > dataInicio)
            {
                retorno.Add(dataAux);
            }

            while (dataAux >= dataInicio)
            {
                // Subtrai 1 mes NA MESMA DATA DE ANIVERSARIO
                DateTime dataAuxMesAnterior = dataAux.AddMonths(-1);

                dataValida = false;
                while (!dataValida)
                {
                    try
                    {
                        dataAux = new DateTime(dataAuxMesAnterior.Year, dataAuxMesAnterior.Month, diaAniversario);
                        dataValida = true;
                    }
                    catch (Exception e)
                    {
                        diaAniversario -= 1;
                    }
                }

                // Se cair num dia Não Util adiciona (ou subtrai) o primeiro dia util subsequente (ou anterior)
                if (idLocal.HasValue && !Calendario.IsDiaUtil(dataAux, idLocal.Value, tipoFeriado.Value))
                {
                    DateTime dataAniversario = Calendario.AdicionaDiaUtil(dataAux, 1, idLocal.Value, tipoFeriado.Value);
                    if (dataAniversario.Month != dataAux.Month) //nao deixa virar para o mês seguinte
                    {
                        dataAniversario = Calendario.SubtraiDiaUtil(dataAux, 1, idLocal.Value, tipoFeriado.Value);
                    }

                    if (dataAniversario > dataInicio)
                    {
                        retorno.Add(dataAniversario);
                    }
                }
                else
                {
                    if (dataAux > dataInicio)
                    {
                        retorno.Add(dataAux);
                    }
                }
            }

            // Ordena Datas
            retorno.Sort(delegate(DateTime x, DateTime y) { return x.CompareTo(y); });
            return retorno;
        }

        /// <summary>
        /// Retorna o numero indice novo, a partir da taxa informada. Calcula baseado no nr indice do mes anterior, se nao estiver disponivel retorna 0.
        /// </summary>
        /// <param name="idIndice"></param>
        /// <param name="dataIndice"></param>
        /// <param name="taxa"></param>
        /// <returns></returns>
        public static decimal RetornaNumeroIndice(int idIndice, DateTime dataIndice, decimal taxa)
        {
            DateTime dataMesAnterior = Calendario.RetornaPrimeiroDiaCorridoMes(dataIndice, -1);

            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            decimal cotacaoIndiceMes = cotacaoIndice.RetornaCotacaoIndiceMes(idIndice, dataMesAnterior);

            decimal numeroIndice = 0;
            if (idIndice == (int)ListaIndiceFixo.IGPM)
            {
                numeroIndice = Math.Round(cotacaoIndiceMes * (taxa / 100M + 1M), 3);
            }
            else if (idIndice == (int)ListaIndiceFixo.IPCA)
            {
                numeroIndice = Math.Round(cotacaoIndiceMes * (taxa / 100M + 1M), 2);
            }
            else
            {
                numeroIndice = Math.Round(cotacaoIndiceMes * (taxa / 100M + 1M), 10);
            }

            return numeroIndice;
        }

    }
    #endregion

    #region Token
    /// <summary>
    ///  Funções relativas a Token
    /// </summary>
    public class StringTokenizer : IEnumerable
    {
        private string[] elements;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="source"></param>
        /// <param name="delimiters"></param>
        public StringTokenizer(string source, char[] delimiters)
        {
            // Parse the string into tokens:
            elements = source.Split(delimiters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            return elements.Length;
        }

        /// <summary>
        /// IEnumerable Interface Implementation: 
        /// Declaration of the GetEnumerator() method required by IEnumerable
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return new TokenEnumerator(this);
        }

        // Inner class implements IEnumerator interface:
        private class TokenEnumerator : IEnumerator
        {
            private int position = -1;
            private StringTokenizer t;

            public TokenEnumerator(StringTokenizer t)
            {
                this.t = t;
            }

            // Declare the MoveNext method required by IEnumerator:
            public bool MoveNext()
            {
                if (position < t.elements.Length - 1)
                {
                    position++;
                    return true;
                }
                else
                {
                    return false;
                }
            }

            // Declare the Reset method required by IEnumerator:
            public void Reset()
            {
                position = -1;
            }

            // Declare the Current property required by IEnumerator:
            public object Current
            {
                get
                {
                    return t.elements[position];
                }
            }
        }
    }
    #endregion

    #region Informações sobre o metodo em execução
    /// <summary>
    ///  Funções relativas ao método em execução
    /// </summary>
    public static class InformacaoMetodo
    {
        /// <summary>
        /// Retorna o nome do Método Chamador
        /// </summary>
        public static string MetodoCorrente
        {
            get
            {
                try
                {
                    // Acesso ao chamador
                    return new StackTrace().GetFrame(1).GetMethod().Name;
                }
                catch (Exception)
                {
                    return "Unknown Method";
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// TODO: deve-se pegar o metodo pai. Esse metodo não funciona
        public static string MetodoCorrente1()
        {
            //get { return collectionBdin; }               

            string metodo = MethodBase.GetCurrentMethod().Name;
            ParameterInfo[] parametros = MethodBase.GetCurrentMethod().GetParameters();

            StringBuilder nomeMetodo = new StringBuilder();
            nomeMetodo.Append(metodo).Append("(");
            for (int i = 0; i < parametros.Length; i++)
            {
                if (i == 0)
                {
                    nomeMetodo.Append(parametros[i].ParameterType);
                    nomeMetodo.Append(" " + parametros[i].Name);
                }
                else
                {
                    nomeMetodo.Append(", " + parametros[i].ParameterType);
                    nomeMetodo.Append(" " + parametros[i].Name);
                }
            }
            nomeMetodo.Append(metodo).Append(")");
            return nomeMetodo.ToString();

            //return null;
        }
    }
    #endregion

    #region EnumString
    /// <summary>
    ///  Classe para permitir o uso de enum de Strings
    /// </summary>
    public static class StringEnum
    {
        #region Static implementation

        /// <summary>
        /// Gets a string value for a particular enum value.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <returns>String Value associated via a <see cref="StringValueAttribute"/> attribute, or null if not found.</returns>
        public static string GetStringValue(Enum value)
        {
            string output = null;
            Type type = value.GetType();
            Hashtable _stringValues = new Hashtable();

            if (_stringValues.ContainsKey(value))
                output = (_stringValues[value] as StringValueAttribute).Value;
            else
            {
                //Look for our 'StringValueAttribute' in the field's custom attributes
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] attrs = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (attrs.Length > 0)
                {
                    _stringValues.Add(value, attrs[0]);
                    output = attrs[0].Value;
                }

            }
            return output;

        }

        /// <summary>
        /// Parses the supplied enum and string value to find an associated enum value (case sensitive).
        /// </summary>
        /// <param name="type">Type.</param>
        /// <param name="stringValue">String value.</param>
        /// <returns>Enum value associated with the string value, or null if not found.</returns>
        public static object Parse(Type type, string stringValue)
        {
            return Parse(type, stringValue, false);
        }

        /// <summary>
        /// Parses the supplied enum and string value to find an associated enum value.
        /// </summary>
        /// <param name="type">Type.</param>
        /// <param name="stringValue">String value.</param>
        /// <param name="ignoreCase">Denotes whether to conduct a case-insensitive match on the supplied string value</param>
        /// <returns>Enum value associated with the string value, or null if not found.</returns>
        public static object Parse(Type type, string stringValue, bool ignoreCase)
        {
            object output = null;
            string enumStringValue = null;

            if (!type.IsEnum)
                throw new ArgumentException(String.Format("Supplied type must be an Enum.  Type was {0}", type.ToString()));

            //Look for our string value associated with fields in this enum
            foreach (FieldInfo fi in type.GetFields())
            {
                //Check for our custom attribute
                StringValueAttribute[] attrs = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (attrs.Length > 0)
                    enumStringValue = attrs[0].Value;

                //Check for equality then select actual enum value.
                if (string.Compare(enumStringValue, stringValue, ignoreCase) == 0)
                {
                    output = Enum.Parse(type, fi.Name);
                    break;
                }
            }

            return output;
        }

        /// <summary>
        /// Return the existence of the given string value within the enum.
        /// </summary>
        /// <param name="stringValue">String value.</param>
        /// <param name="enumType">Type of enum</param>
        /// <returns>Existence of the string value</returns>
        public static bool IsStringDefined(Type enumType, string stringValue)
        {
            return Parse(enumType, stringValue) != null;
        }

        /// <summary>
        /// Return the existence of the given string value within the enum.
        /// </summary>
        /// <param name="stringValue">String value.</param>
        /// <param name="enumType">Type of enum</param>
        /// <param name="ignoreCase">Denotes whether to conduct a case-insensitive match on the supplied string value</param>
        /// <returns>Existence of the string value</returns>
        public static bool IsStringDefined(Type enumType, string stringValue, bool ignoreCase)
        {
            return Parse(enumType, stringValue, ignoreCase) != null;
        }

        #endregion
    }

    #region Class StringValueAttribute

    /// <summary>
    /// Simple attribute class for storing String Values
    /// </summary>
    public class StringValueAttribute : Attribute
    {
        private string _value;

        /// <summary>
        /// Creates a new <see cref="StringValueAttribute"/> instance.
        /// </summary>
        /// <param name="value">Value.</param>
        public StringValueAttribute(string value)
        {
            _value = value;
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value></value>
        public string Value
        {
            get { return _value; }
        }
    }

    #endregion

    #endregion

    #region Funções relativas as Configurações do Sistema
    /// <summary>
    /// Fornece os Parâmetros de configuração do Sistema
    /// Realiza o Load dos valores de Configuração
    /// </summary>
    public static class ParametrosConfiguracaoSistema
    {
        #region Bolsa
        /// <summary>
        /// Fornece os Parâmetros de configuração do componente de Bolsa
        /// </summary>
        public static class Bolsa
        {
            /// <summary>
            /// 
            /// </summary>
            public static int TipoVisualizacao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Bolsa.TipoVisualizacao);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int PrioridadeCasamentoVencimento
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Bolsa.PrioridadeCasamentoVencimento);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static bool MTMTermoVendido
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Bolsa.MTMTermoVendido);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static bool RenovacaoTermo
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Bolsa.RenovacaoTermo);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int CodigoAgenteDefault
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Bolsa.CodigoAgenteDefault);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static bool CompensaIRDayTrade
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Bolsa.CompensaIRDayTrade);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int EntradaOperacoes
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Bolsa.EntradaOperacoes);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int CustodiaBTC
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Bolsa.CustodiaBTC);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static bool ConsolidacaoCustoMedio
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Bolsa.ConsolidacaoCustoMedio);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static bool IsencaoIRAcoes
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Bolsa.IsencaoIRAcoes);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static bool EmiteMsgSemCotacaoBolsa
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Bolsa.EmiteMsgSemCotacaoBolsa);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }
        }
        #endregion

        #region BMF
        /// <summary>
        /// Fornece os Parâmetros de configuração do componente de BMF
        /// </summary>
        public static class BMF
        {
            /// <summary>
            /// 
            /// </summary>
            public static decimal DescontoSocio
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.BMF.DescontoSocio);
                    return config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int TipoVisualizacao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.BMF.TipoVisualizacao);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int TipoCalculoTaxasBMF
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.BMF.TipoCalculoTaxasBMF);
                    return (int)config.ValorNumerico.Value;
                }
            }
        }
        #endregion

        #region RendaFixaSwap
        /// <summary>
        /// Fornece os Parâmetros de configuração do componente de Swap
        /// </summary>
        public static class RendaFixaSwap
        {
            /// <summary>
            /// 
            /// </summary>
            public static decimal TaxaEmolumento
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.RendaFixaSwap.TaxaEmolumento);
                    return config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static decimal PercentualRegistro
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.RendaFixaSwap.PercentualRegistro);
                    return config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static decimal DescontoSocio
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.RendaFixaSwap.DescontoSocio);
                    return config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static bool EmiteMsgSemCotacao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.RendaFixaSwap.EmiteMsgSemCotacao);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int ControleCustodia
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.RendaFixaSwap.ControleCustodia);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static bool AliquotaUnicaGrossup
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.RendaFixaSwap.AliquotaUnicaGrossup);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }
        }
        #endregion

        #region Fundo
        /// <summary>
        /// Fornece os Parâmetros de configuração do componente de Fundo
        /// </summary>
        public static class Fundo
        {
            /// <summary>
            /// 
            /// </summary>
            public static int TratamentoCotaInexistente
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Fundo.TratamentoCotaInexistente);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static string RecalculaCota
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Fundo.RecalculaCota);
                    return (string)config.ValorTexto;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static string UsaCotaInicialPosicaoIR
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Fundo.UsaCotaInicialPosicaoIR);
                    return (string)config.ValorTexto;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int CotaBruta
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Fundo.CotaBruta);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static string ZeraCaixa
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Fundo.ZeraCaixa);
                    return (string)config.ValorTexto;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int RetornoBenchmark
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Fundo.RetornoBenchmark);
                    return (int)config.ValorNumerico.Value;
                }
            }

            public static bool SimulaYMFCOT
            {
                get
                {
                    Configuracao config = new Configuracao();
                    if (config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Fundo.SimulaYMFCOT))
                    {
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            public static string RetornoFDICAjustado
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Fundo.RetornoFDICAjustado);
                    return (string)config.ValorTexto;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int? DistribuidorPadrao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Fundo.DistribuidorPadrao);
                    int? retorno = null;
                    if (config.ValorNumerico.HasValue)
                    {
                        retorno = (int)config.ValorNumerico.Value;
                    }
                    return retorno;
                }
            }

            public static bool PfeeCotaFundos {
                get {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Fundo.PfeeCotasFundos);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }

        }
        #endregion

        #region Integracoes
        /// <summary>
        /// Fornece os Parâmetros de configuração do processamento de integrações
        /// </summary>
        public static class Integracoes
        {
            /// <summary>
            /// 
            /// </summary>
            public static int IntegracaoBolsa
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Integracoes.IntegracaoBolsa);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int IntegracaoBMF
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Integracoes.IntegracaoBMF);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int IntegracaoCC
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Integracoes.IntegracaoCC);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int IntegracaoRendaFixa
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Integracoes.IntegracaoRendaFixa);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static string IntegracaoCodigosCC
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Integracoes.IntegracaoCodigosCC);
                    return (string)config.ValorTexto;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static string SchemaSinacor
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Integracoes.SchemaSinacor);
                    return (string)config.ValorTexto;
                }
            }

            public static string UsuarioIntegracao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Integracoes.UsuarioIntegracao);
                    return (string)config.ValorTexto;
                }
            }

            public static string SenhaIntegracao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Integracoes.SenhaIntegracao);
                    return (string)config.ValorTexto;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static string SchemaBTC
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Integracoes.SchemaBTC);
                    return (string)config.ValorTexto;
                }
            }

            

            public static string NomeInstituicao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Integracoes.NomeInstituicao);
                    return (string)config.ValorTexto;
                }
            }

            public static string CodInstituicao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Integracoes.CodInstituicao);
                    return (string)config.ValorTexto;
                }
            }

            public static string ResponsavelInstituicao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Integracoes.ResponsavelInstituicao);
                    return (string)config.ValorTexto;
                }
            }

            public static string TelefoneInstituicao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Integracoes.TelefoneInstituicao);
                    return (string)config.ValorTexto;
                }
            }


            public static string EmailInstituicao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Integracoes.EmailInstituicao);
                    return (string)config.ValorTexto;
                }
            }
            public static int IntegraGalgoPlCota
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Integracoes.IntegraGalgoPlCota);
                    return (int)config.ValorNumerico.Value;
                }
            }

        }
        #endregion

        #region Segurança
        /// <summary>
        /// Fornece os Parâmetros de Configuração de Segurança
        /// </summary>
        public static class Seguranca
        {
            public static class Menu
            {
                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoAcoesOpcoes
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.AcoesOpcoes);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoTermoOpcoes
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.TermoAcoes);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoBTCAcoes
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.BTC_Aluguel);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoBMF
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.BMF);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoRendaFixa
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.RendaFixa);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoFundos
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Fundos);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoSwap
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Swap);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoGerencial
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Gerencial);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoCalculoDespesas
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.CalculoDespesas);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoRebates
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Rebates);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoEnquadramento
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Enquadramento);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoCotista
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Cotista);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoContabil
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.Contabil);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoApuracaoIR
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.ApuracaoIR);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoInformeFundos
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.InformeFundos);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoInformeClubes
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.InformeClubes);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoFrontOffice
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.FrontOffice);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }

                /// <summary>
                /// 
                /// </summary>
                public static bool AcessoCarteiraSimulada
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.Menu.CarteiraSimulada);
                        return config.ValorTexto.Trim().ToUpper() == "S";
                    }
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int TamanhoMinimoSenha
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.TamanhoMinimoSenha);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int TempoExpiracaoSenha
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.TempoExpiracaoSenha);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int TentativasBloqueioSenha
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.TentativasBloqueioSenha);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int HistoricoSenhas
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.HistoricoSenhas);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int MinimoCaracterEspecialSenha
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.MinimoCaracterEspecialSenha);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int MinimoNumeroSenha
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.MinimoNumeroSenha);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int LetrasMaiusculas
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.LetrasMaiusculas);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static int JanelaTempoResetSenha
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.JanelaTempoResetSenha);
                    return (int)config.ValorNumerico.Value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static bool AcessoSimultaneo
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Seguranca.AcessoSimultaneo);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }
        }
        #endregion

        #region Configuracao de Relatorios
        /// <summary>
        /// Fornece os Parâmetros de Configuração de Relatorios
        /// </summary>
        public static class ConfiguracaoRelatorios
        {

            public static class RelatorioExtratoCliente
            {
                /// <summary>
                /// Parametro que se encontra na coluna ValorTexto em Configuracao 
                /// Formato: header1Style - header2Style     - header3Style   - oddRowStyle - evenRowStyle - 
                ///          line1Style   - headerFieldStyle - positivoStyle  - negativoStyle
                ///
                /// color1 Fore, color1 Back,
                /// color2 Fore, color2 Back,
                /// color3 Fore, color3 Back,
                /// color4 Fore, color4 Back,
                /// color5 Fore, color5 Back,
                /// color6 Fore, color6 Back,
                /// color7 Fore, color7 Back,
                /// color8 Fore, color8 Back
                /// color9 Fore, color9 Back
                /// 
                /// Ex: #ffffff, #002940, #002940, #D7CFB9, #002940, #F9F2E2, #002940, #FDFAF3, #002940, #F9F2E2, #002940, #ffffff, #002940, #ffffff, #002940, #ffffff, #ff0000, #ffffff
                /// 
                /// Retorna null se nao achar a configuracao
                /// </summary>
                /// 
                public static string CoresExtrato
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCliente.CoresExtrato))
                        {
                            return null;
                        }
                        else
                        {
                            return !String.IsNullOrEmpty(config.ValorTexto) ? config.ValorTexto.Trim() : "";
                        }
                    }
                    set
                    {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCliente.CoresExtrato))
                        {
                            throw new ArgumentNullException("ID Configuração CoresExtrato não está no banco");
                        }
                        else
                        {
                            config.ValorTexto = value;
                            config.Save();
                        }
                    }
                }

                public static int ExibePizzaTopN 
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCliente.ExibePizzaTopN))
                        {
                            return 99;
                        }
                        else
                        {                            
                            return (int)config.ValorNumerico.Value;
                        }
                    }
                    set
                    {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCliente.ExibePizzaTopN))
                        {
                            throw new ArgumentNullException("ID Configuração ExibePizzaTopN não está no banco");
                        }
                        else
                        {
                            config.ValorNumerico = value;
                            config.Save();
                        }
                    }
                }

                public static string EstiloGraficoDistribuicao
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCliente.EstiloGraficoDistribuicao))
                        {
                            return null;
                        }
                        else
                        {
                            return !String.IsNullOrEmpty(config.ValorTexto) ? config.ValorTexto.Trim() : "";
                        }
                    }
                    set
                    {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCliente.EstiloGraficoDistribuicao))
                        {
                            throw new ArgumentNullException("ID Configuração EstiloGraficoDistribuicao não está no banco");
                        }
                        else
                        {
                            config.ValorTexto = value;
                            config.Save();
                        }
                    }
                }

                /// <summary>
                /// Parametro ObservacaoExtrato presente na tabela de Configuracao relativo ao texto HTML do relatorio de Extrato Cliente
                /// </summary>
                public static string ObservacaoExtrato
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCliente.ObservacaoExtrato))
                        {
                            return null;
                        }
                        else
                        {
                            return !String.IsNullOrEmpty(config.ValorTexto) ? config.ValorTexto.Trim() : "";
                        }
                    }
                    set
                    {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCliente.ObservacaoExtrato))
                        {
                            throw new ArgumentNullException("ID Configuração ObservacaoExtrato não está no banco");
                        }
                        else
                        {
                            config.ValorTexto = value;
                            config.Save();
                        }
                    }
                }

                /// <summary>
                /// Mostra Patrimônio no Cabeçalho do relatorio ExtratoCliente
                /// </summary>
                public static string MostraPatrimonio {
                    get {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCliente.MostraPatrimonio)) {
                            return null;
                        }
                        else {
                            return !String.IsNullOrEmpty(config.ValorTexto) ? config.ValorTexto.Trim() : "";
                        }
                    }
                    set {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCliente.MostraPatrimonio)) {
                            throw new ArgumentNullException("ID Configuração Mostra Patrimônio não está no banco");
                        }
                        else {
                            config.ValorTexto = value;
                            config.Save();
                        }
                    }
                }

                public static bool JanelaMovelSubReportRetornoCarteira
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCliente.JanelaMovelSubReportRetornoCarteira))
                        {
                            return false;
                        }
                        else
                        {
                            return !string.IsNullOrEmpty(config.ValorTexto) && config.ValorTexto == "S";
                        }
                    }
                    set
                    {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCliente.JanelaMovelSubReportRetornoCarteira))
                        {
                            throw new ArgumentNullException("ID Configuração Janela Model SubReportRetornoCarteira não está no banco");
                        }
                        else
                        {
                            config.ValorTexto = (value == true) ? "S" : "N";
                            config.Save();
                        }
                    }
                }

                public static int FiltraTopNPieChart
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCliente.FiltraTopNPieChart))
                        {
                            return 0;
                        }
                        else
                        {
                            return config.ValorNumerico.HasValue ? Convert.ToInt32(config.ValorNumerico.Value) : 0;
                        }
                    }
                    set
                    {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCliente.FiltraTopNPieChart))
                        {
                            throw new ArgumentNullException("ID Configuração Filtra Top N Pie Chart não está no banco");
                        }
                        else
                        {
                            config.ValorNumerico = value;
                            config.Save();
                        }
                    }
                }

                /// <summary>
                /// Analisa o parametro CoresExtrato e devolve o Style do Header1
                /// [0] = ForeGround Color
                /// [1] = BackGround Color
                /// </summary>
                public static Color[] Header1_Style
                {
                    get
                    {
                        // Padrao se não tiver valor
                        Color[] colorRetorno = new Color[2] { Color.White, ColorTranslator.FromHtml("#002940") };
                        //
                        string coresExtrato = ConfiguracaoRelatorios.RelatorioExtratoCliente.CoresExtrato;
                        if (!String.IsNullOrEmpty(coresExtrato))
                        {
                            string[] colorHeader1 = coresExtrato.Split(',');
                            if (colorHeader1.Length >= 2)
                            {
                                colorRetorno[0] = ColorTranslator.FromHtml(colorHeader1[0]);
                                colorRetorno[1] = ColorTranslator.FromHtml(colorHeader1[1]);
                            }
                        }
                        return colorRetorno;
                    }
                }

                /// <summary>
                /// Analisa o parametro CoresExtrato e devolve o Style do Header2
                /// [0] = ForeGround Color
                /// [1] = BackGround Color 
                /// </summary>
                public static Color[] Header2_Style
                {
                    get
                    {
                        // Padrao se não tiver valor
                        Color[] colorRetorno = new Color[2] { ColorTranslator.FromHtml("#002940"), ColorTranslator.FromHtml("#D7CFB9") };
                        //
                        string coresExtrato = ConfiguracaoRelatorios.RelatorioExtratoCliente.CoresExtrato;
                        if (!String.IsNullOrEmpty(coresExtrato))
                        {
                            string[] colorHeader2 = coresExtrato.Split(',');
                            if (colorHeader2.Length >= 4)
                            {
                                colorRetorno[0] = ColorTranslator.FromHtml(colorHeader2[2]);
                                colorRetorno[1] = ColorTranslator.FromHtml(colorHeader2[3]);
                            }
                        }
                        return colorRetorno;
                    }
                }

                /// <summary>
                /// Analisa o parametro CoresExtrato e devolve o Style do Header3
                /// [0] = ForeGround Color
                /// [1] = BackGround Color 
                /// </summary>
                public static Color[] Header3_Style
                {
                    get
                    {
                        // Padrao se não tiver valor
                        Color[] colorRetorno = new Color[2] { ColorTranslator.FromHtml("#002940"), ColorTranslator.FromHtml("#F9F2E2") };
                        //
                        string coresExtrato = ConfiguracaoRelatorios.RelatorioExtratoCliente.CoresExtrato;
                        if (!String.IsNullOrEmpty(coresExtrato))
                        {
                            string[] colorHeader3 = coresExtrato.Split(',');
                            if (colorHeader3.Length >= 6)
                            {
                                colorRetorno[0] = ColorTranslator.FromHtml(colorHeader3[4]);
                                colorRetorno[1] = ColorTranslator.FromHtml(colorHeader3[5]);
                            }
                        }
                        return colorRetorno;
                    }
                }

                /// <summary>
                /// Analisa o parametro CoresExtrato e devolve o Style do Header_Field
                /// [0] = ForeGround Color
                /// [1] = BackGround Color 
                /// </summary>
                public static Color[] HeaderField_Style
                {
                    get
                    {
                        // Padrao se não tiver valor
                        Color[] colorRetorno = new Color[2] { ColorTranslator.FromHtml("#002940"), Color.White };
                        //
                        string coresExtrato = ConfiguracaoRelatorios.RelatorioExtratoCliente.CoresExtrato;
                        if (!String.IsNullOrEmpty(coresExtrato))
                        {
                            string[] colorHeaderField = coresExtrato.Split(',');
                            if (colorHeaderField.Length >= 14)
                            {
                                colorRetorno[0] = ColorTranslator.FromHtml(colorHeaderField[12]);
                                colorRetorno[1] = ColorTranslator.FromHtml(colorHeaderField[13]);
                            }
                        }
                        return colorRetorno;
                    }
                }

                /// <summary>
                /// Analisa o parametro CoresExtrato e devolve o Style das linhas Pares
                /// [0] = ForeGround Color
                /// [1] = BackGround Color 
                /// </summary>
                public static Color[] Even_Style
                {
                    get
                    {
                        // Padrao se não tiver valor
                        Color[] colorRetorno = new Color[2] { ColorTranslator.FromHtml("#002940"), ColorTranslator.FromHtml("#F9F2E2") };
                        //
                        string coresExtrato = ConfiguracaoRelatorios.RelatorioExtratoCliente.CoresExtrato;
                        if (!String.IsNullOrEmpty(coresExtrato))
                        {
                            string[] colorEven = coresExtrato.Split(',');
                            if (colorEven.Length >= 10)
                            {
                                colorRetorno[0] = ColorTranslator.FromHtml(colorEven[8]);
                                colorRetorno[1] = ColorTranslator.FromHtml(colorEven[9]);
                            }
                        }
                        return colorRetorno;
                    }
                }

                /// <summary>
                /// Analisa o parametro CoresExtrato e devolve o Style das linhas Impares
                /// [0] = ForeGround Color
                /// [1] = BackGround Color 
                /// </summary>
                public static Color[] Odd_Style
                {
                    get
                    {
                        // Padrao se não tiver valor
                        Color[] colorRetorno = new Color[2] { ColorTranslator.FromHtml("#002940"), ColorTranslator.FromHtml("#FDFAF3") };
                        //
                        string coresExtrato = ConfiguracaoRelatorios.RelatorioExtratoCliente.CoresExtrato;
                        if (!String.IsNullOrEmpty(coresExtrato))
                        {
                            string[] colorOdd = coresExtrato.Split(',');
                            if (colorOdd.Length >= 8)
                            {
                                colorRetorno[0] = ColorTranslator.FromHtml(colorOdd[6]);
                                colorRetorno[1] = ColorTranslator.FromHtml(colorOdd[7]);
                            }
                        }
                        return colorRetorno;
                    }
                }
            }

            public static class RelatorioExtratoCotista
            {

                /// <summary>
                /// Parametro ObservacaoCotista presente na tabela de Configuracao relativo ao texto HTML do relatorio de Extrato Cotista
                /// </summary>
                public static string ObservacaoCotista
                {
                    get
                    {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCotista.ObservacaoCotista))
                        {
                            return null;
                        }
                        else
                        {
                            return !String.IsNullOrEmpty(config.ValorTexto) ? config.ValorTexto.Trim() : "";
                        }
                    }
                    set
                    {
                        Configuracao config = new Configuracao();
                        if (!config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.ConfiguracaoRelatorios.RelatorioExtratoCotista.ObservacaoCotista))
                        {
                            throw new ArgumentNullException("ID Configuração ObservacaoCotista não está no banco");
                        }
                        else
                        {
                            config.ValorTexto = value;
                            config.Save();
                        }
                    }
                }
            }

        }

        #endregion

        #region Outras
        /// <summary>
        /// Fornece Diversas Configurações do Sistema
        /// </summary>
        public static class Outras
        {
            /// <summary>
            // Se true, mostra o combo de agente de liquidação na tela de operação de bolsa
            /// </summary>
            public static bool AgenteLiquidacaoExclusivo
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.AgenteLiquidacaoExclusivo);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }
            /// <summary>
            /// Se true Mostra o Campo CalculaContabil no Cadastro de Cliente 
            /// e os Campos IdEventoProvisao/IdEventoPagamento nos Cadastros de TabelaProvisão/TabelaTaxaAdministracao/TabelaTaxaPerformance
            /// Se for vazio ou false nao mostra os campos
            /// </summary>
            public static bool CalculaContatil
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.CalculaContatil);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }
            /// <summary>
            ///  Se true, mostra o combo de c/c na tela de liquidação
            /// </summary>
            public static bool MultiConta
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.MultiConta);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }
            /// <summary>
            /// Se true, dá permissão automática aos clientes e cotistas para todos os usuários que tenham TipoTrava = Nenhum
            /// </summary>
            public static bool PermissaoInternoAuto
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.PermissaoInternoAuto);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }
            /// <summary>
            /// Se true, deixa liberado o campo de Data de Lancamento na tela de Liquidacao
            /// </summary>
            public static bool LiberaDataLancamentoLiquidacao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.LiberaDataLancamentoLiquidacao);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }
            /// <summary>
            /// Se true, reseta a data inicio cota após um resgate total
            /// </summary>
            public static bool ResetDataInicio
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.ResetDataInicio);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }

            public static bool ProcessamentoViaWebService
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.ProcessamentoViaWebService);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static bool CalculaFatorIndice
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.CalculaFatorIndice);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static bool ProcessaIndicadorCarteira
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.ProcessaIndicadorCarteira);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }

            public static string UsuarioIntegracao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.UsuarioIntegracao);
                    return config.ValorTexto;
                }
            }

            public static string SenhaIntegracao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.SenhaIntegracao);
                    return config.ValorTexto;
                }
            }

            public static string ContaIntegracao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.ContaIntegracao);
                    return config.ValorTexto;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static bool SuitabilityRendaFixa
            {
                get
                {
                    Configuracao config = new Configuracao();
                    bool existeConfiguracao = config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.SuitabilityRendaFixa);
                    return existeConfiguracao && config.ValorTexto.Trim().ToUpper() == "S";
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static bool DescontarRendimentoCupom
            {
                get
                {
                    Configuracao config = new Configuracao();
                    bool existeConfiguracao = config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.DescontarRendimentoCupom);
                    return existeConfiguracao && config.ValorTexto.Trim().ToUpper() == "S";
                }
            }

            public static string EmailBackOffice
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.EmailBackOffice);
                    return config.ValorTexto;
                }
            }

            public static string EmailBackOfficeInstitucional
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.EmailBackOfficeInstitucional);
                    return config.ValorTexto;
                }
            }
			
            public static bool RetroagirCarteirasDependentes
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.RetroagirCarteirasDependentes);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }

            public static bool PermitirOperacoesRetroativas
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.PermitirOperacoesRetroativas);
                    return config.ValorTexto.Trim().ToUpper() == "S";
                }
            }

            public static DateTime DataBase
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.DataBase);
                    DateTime dataBase = new DateTime(Convert.ToInt32(config.ValorTexto.Substring(6, 4)),
                                                     Convert.ToInt32(config.ValorTexto.Substring(3, 2)),
                                                     Convert.ToInt32(config.ValorTexto.Substring(0, 2)));
                    return dataBase;
                }
            }			
			
			public static string DisclaimerPosicao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.DisclaimerPosicao);
                    return config.ValorTexto;
                }
            }

            public static string DisclaimerOperacao
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.DisclaimerOperacao);
                    return config.ValorTexto;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static string IdAutomaticoPessoa
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.IdAutomaticoPessoa);
                    return (string)config.ValorTexto;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static string PermiteDuplDocumentoPessoa
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.PermiteDuplDocumentoPessoa);
                    return (string)config.ValorTexto;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static string PropagaAlterNomeCliente
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.PropagaAlterNomeCliente);
                    return (string)config.ValorTexto;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static string EstendeConceitoPessoaAgente
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.EstendeConceitoPessoaAgente);
                    return (string)config.ValorTexto;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static string PropagaAlterNomeAgente
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.PropagaAlterNomeAgente);
                    return (string)config.ValorTexto;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static string ValidaQtdeResgateProcessamento
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.ValidaQtdeResgateProcessamento);
                    return (string)config.ValorTexto;
                }
            }


            /// <summary>
            /// 
            /// </summary>
            public static string AbortaProcRFPorErroCadastraTitulo
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.AbortaProcRFPorErroCadastraTitulo);
                    return (string)config.ValorTexto;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static string CalculaCofUsandoPLD0
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.CalculaCofUsandoPLD0);
                    return (string)config.ValorTexto;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public static string TipoVisualizacaoFundoResgateCotista
            {
                get
                {
                    Configuracao config = new Configuracao();
                    config.LoadByPrimaryKey(ConfiguracaoRegrasNegocio.Outras.TipoVisualizacaoFundoResgateCotista);
                    return (string)config.ValorTexto;
                }
            }
        }
        #endregion
    }
    #endregion

    #region Extensões de Bibliotecas
    public class ConvertFloat : ConverterBase
    {
        public override object StringToField(string from)
        {
            from = from.Replace(".", System.Globalization.CultureInfo.CurrentCulture.
                NumberFormat.NumberDecimalSeparator);

            Decimal convertedDecimal;
            if (from.ToUpper().Contains("E"))
            {
                //A conversao de notacao float nao funciona com separador decimal americano
                convertedDecimal = Decimal.Parse(from,
                    System.Globalization.NumberStyles.Float);

            }
            else
            {
                convertedDecimal = Convert.ToDecimal(from);
            }

            return convertedDecimal;
        }
    }

    public class ConvertFlexDate : ConverterBase
    {
        /// <summary>
        /// conversor para campos de data que possuem ou não milissegundos (com todas suas variacoes)
        /// </summary>
        /// <param name="from">the string format of date - first the day</param>
        /// <returns></returns>

        public override object StringToField(string from)
        {
            DateTime dt;

            const string FILEHELPER_DATEFORMAT = "dd/MM/yyyy";
            const string FILEHELPER_DATEFORMAT_SECONDS = "dd/MM/yyyy HH:mm:ss";
            const string FILEHELPER_DATEFORMAT_MINUTES = "dd/MM/yyyy HH:mm";
            const string FILEHELPER_DATEFORMAT_MS = "dd/MM/yyyy HH:mm:ss:FFF";

            if (DateTime.TryParseExact(from, FILEHELPER_DATEFORMAT, null, DateTimeStyles.None, out dt))
            {
                return dt;
            }

            if (DateTime.TryParseExact(from, FILEHELPER_DATEFORMAT_SECONDS, null, DateTimeStyles.None, out dt))
            {
                return dt;
            }

            if (DateTime.TryParseExact(from, FILEHELPER_DATEFORMAT_MINUTES, null, DateTimeStyles.None, out dt))
            {
                return dt;
            }

            if (DateTime.TryParseExact(from, FILEHELPER_DATEFORMAT_MS, null, DateTimeStyles.None, out dt))
            {
                return dt;
            }

            throw new ArgumentException("can not make a date from " + from, "from");

        }
    }
    #endregion

    #region Criptografia
    public class Crypto
    {
        private static byte[] _salt = Encoding.ASCII.GetBytes("o6806642kbM7c5");

        /// <summary>
        /// Encrypt the given string using AES.  The string can be decrypted using 
        /// DecryptStringAES().  The sharedSecret parameters must match.
        /// </summary>
        /// <param name="plainText">The text to encrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for encryption.</param>
        public static string EncryptStringAES(string plainText, string sharedSecret)
        {
            if (string.IsNullOrEmpty(plainText))
                throw new ArgumentNullException("plainText");
            if (string.IsNullOrEmpty(sharedSecret))
                throw new ArgumentNullException("sharedSecret");

            string outStr = null;                       // Encrypted string to return
            RijndaelManaged aesAlg = null;              // RijndaelManaged object used to encrypt the data.

            try
            {
                // generate the key from the shared secret and the salt
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, _salt);

                // Create a RijndaelManaged object
                aesAlg = new RijndaelManaged();
                aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

                // Create a decryptor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    // prepend the IV
                    msEncrypt.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(int));
                    msEncrypt.Write(aesAlg.IV, 0, aesAlg.IV.Length);
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                    }
                    outStr = Convert.ToBase64String(msEncrypt.ToArray());
                }
            }
            finally
            {
                // Clear the RijndaelManaged object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            // Return the encrypted bytes from the memory stream.
            return outStr;
        }

        /// <summary>
        /// Decrypt the given string.  Assumes the string was encrypted using 
        /// EncryptStringAES(), using an identical sharedSecret.
        /// </summary>
        /// <param name="cipherText">The text to decrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for decryption.</param>
        public static string DecryptStringAES(string cipherText, string sharedSecret)
        {
            if (string.IsNullOrEmpty(cipherText))
                throw new ArgumentNullException("cipherText");
            if (string.IsNullOrEmpty(sharedSecret))
                throw new ArgumentNullException("sharedSecret");

            // Declare the RijndaelManaged object
            // used to decrypt the data.
            RijndaelManaged aesAlg = null;

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            try
            {
                // generate the key from the shared secret and the salt
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, _salt);

                // Create the streams used for decryption.                
                byte[] bytes = Convert.FromBase64String(cipherText);
                using (MemoryStream msDecrypt = new MemoryStream(bytes))
                {
                    // Create a RijndaelManaged object
                    // with the specified key and IV.
                    aesAlg = new RijndaelManaged();
                    aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                    // Get the initialization vector from the encrypted stream
                    aesAlg.IV = ReadByteArray(msDecrypt);
                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }
            finally
            {
                // Clear the RijndaelManaged object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            return plaintext;
        }

        private static byte[] ReadByteArray(Stream s)
        {
            byte[] rawLength = new byte[sizeof(int)];
            if (s.Read(rawLength, 0, rawLength.Length) != rawLength.Length)
            {
                throw new SystemException("Stream did not contain properly formatted byte array");
            }

            byte[] buffer = new byte[BitConverter.ToInt32(rawLength, 0)];
            if (s.Read(buffer, 0, buffer.Length) != buffer.Length)
            {
                throw new SystemException("Did not read byte array properly");
            }

            return buffer;
        }
    }
    #endregion

    #region Funções relativas a Strings
    public static class StringExt
    {
        public static string Truncate(string value, int maxLength)
        {
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }


        /// <summary>
        /// Remove itens duplicados de um array de string
        /// </summary>
        /// <param name="myStringArray"></param>
        /// <returns></returns>
        public static string[] RemoveItensDuplicados(string[] myStringArray)
        {
            List<String> myStringList = new List<string>();

            foreach (string s in myStringArray)
            {
                if (!myStringList.Contains(s))
                {
                    myStringList.Add(s);
                }
            }
            return myStringList.ToArray();
        }
    }

    #endregion

    #region Funções Cadastro Dinâmico
    public static class CadastroDinamico
    {
        /// <summary>
        /// Converte o valor para o tipo especificado
        /// </summary>
        /// <param name="value">O valor a ser convertido</param>
        /// <param name="conversionType">O tipo a ser convertido</param>
        /// <returns>Retorna o valor convertido</returns>
        public static object ChangeType(object value, Type conversionType)
        {
            try
            {
                if (conversionType == null)
                {
                    throw new ArgumentNullException("conversionType");
                }
                if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                {
                    if (value == null)
                    {
                        return null;
                    }
                    NullableConverter nullableConverter = new NullableConverter(conversionType);
                    conversionType = nullableConverter.UnderlyingType;
                }
                return Convert.ChangeType(value, conversionType);
            }
            catch
            {
                //Caso não consiga converter o valor, uma nova instância é gerada para setar um valor default
                return value = Activator.CreateInstance(conversionType);
            }
        }

        /// <summary>
        /// Preenche a entidade com os valores dos controles 
        /// OBS: É necessário que os controles da tela possuam seu ID correspondente as colunas do Banco de Dados
        /// </summary>
        /// <typeparam name="T">Tipo de classe a ser preenchida</typeparam>
        /// <param name="controlCollection">Lista de controles a ser percorrida</param>
        /// <param name="Entidade">Entidade do update</param>
        /// <returns>Entidade preenchida</returns>
        public static T PreencheEntidade<T>(ControlCollection controlCollection, T Entidade) where T : new()
        {
            if (Entidade == null)
                Entidade = new T();

            //Instancia lista de controles obrigatórios
            List<string> listaCamposObrigatorios = new List<string>();

            #region Campos Obrigatórios
            foreach (Control control in controlCollection)
            {
                if (control.GetType().Name.Equals("Label"))
                {
                    //Verifica se o campo tem preenchimento obrigatório
                    if (control.GetType().GetProperty("CssClass").GetValue(control, null).Equals("labelRequired"))
                    {
                        //Devolve o nome da propriedade
                        string controle = control.ClientID.Substring(control.ClientID.IndexOf("label")).Replace("label", "");
                        //Adiciona os campos obrigatórios
                        listaCamposObrigatorios.Add(controle);
                    }
                }
            }
            #endregion

            #region Preenche Entidade
            //Percorre a lista de controles
            foreach (Control control in controlCollection)
            {
                //Verifica somente os controles que possuem valor para preencher a entidade
                if (!(control is Label))
                {
                    //Instância valor que irá receber o valor dos controles
                    object valor = new object();

                    //Retira os prefixos dos controles para encontrar as propriedades da entidade(WebControls e Controles do DevExpress)
                    string propriedade = control.ClientID.Substring(control.ClientID.LastIndexOf("_")).Replace("text", "").Replace("drop", "").Replace("rbl", "").Replace("_", "");

                    //Verifica se o controle é um dropdown
                    if (control.GetType().GetProperty("Value") != null && control.GetType().GetProperty("Value").GetValue(control, null) != null)
                        //DropDown
                        valor = control.GetType().GetProperty("Value").GetValue(control, null);
                    else if (control.GetType().GetProperty("Text") != null && control.GetType().GetProperty("Text").GetValue(control, null) != null)
                        //TextBox
                        valor = control.GetType().GetProperty("Text").GetValue(control, null);

                    //Verifica se há campos obrigatórios
                    if (listaCamposObrigatorios.Contains(propriedade) && valor.Equals(string.Empty))
                        throw new Exception("Campos com * são obrigatórios!");

                    //Verifica se foi encontrado essa propriedade na entidade
                    if (Entidade.GetType().GetProperty(propriedade) != null)
                        //Preenche a entidade com o valor encontrado
                        Entidade.GetType().GetProperty(propriedade).SetValue(Entidade,
                                                                             ChangeType(valor, Entidade.GetType().GetProperty(propriedade).PropertyType),
                                                                             null);
                }
            }
            #endregion

            //Retorna a entidade preenchida
            return Entidade;
        }
    }
    #endregion

    #region Função Converte Tipos de Dados
    public static class ConverteTipo
    {
        public static long? TryParseInt64(string input)
        {
            long outValue;
            return Int64.TryParse(input, out outValue) ? (long?)outValue : null;
        }
        public static int? TryParseInt(string input)
        {
            int outValue;
            return int.TryParse(input, out outValue) ? (int?)outValue : null;
        }
        public static DateTime? TryParseDateTime(string input)
        {
            DateTime outValue;
            return DateTime.TryParse(input, out outValue) ? (DateTime?)outValue : null;
        }
        public static decimal? TryParseDecimal(string input)
        {
            decimal outValue;
            return decimal.TryParse(input, out outValue) ? (decimal?)outValue : null;
        }
    }
    #endregion
}