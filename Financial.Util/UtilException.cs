﻿using System;

namespace Financial.Util.Exceptions {            
    /// <summary>
    /// Classe base de Exceção do componente de Util
    /// </summary>
    public class UtilException: Exception {
        /// <summary>
        ///  Construtor
        /// </summary>
        public UtilException() {  }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public UtilException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public UtilException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }
    
    /// <summary>
    /// Exceção de SemConexaoComInternet
    /// </summary>
    public class SemConexaoComInternet : UtilException {
         /// <summary>
        ///  Construtor
        /// </summary>
        public SemConexaoComInternet() {  }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public SemConexaoComInternet(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de EnderecoHttpInvalido
    /// </summary>
    public class EnderecoHttpInvalido : UtilException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public EnderecoHttpInvalido() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public EnderecoHttpInvalido(string mensagem) : base(mensagem) { }

          /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public EnderecoHttpInvalido(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

/// <summary>
    /// Exceção de Impossivel renomear um arquivo
    /// </summary>
    public class RenameFileArquivoInexistente : UtilException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public RenameFileArquivoInexistente() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public RenameFileArquivoInexistente(string mensagem) : base(mensagem) { }
    }
}


