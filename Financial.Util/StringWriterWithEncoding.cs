using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Financial.Util {
    /// <summary>
    /// 
    /// </summary>
    public class StringWriterWithEncoding : StringWriter {
        Encoding encoding;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="encoding"></param>
        public StringWriterWithEncoding(Encoding encoding) {
            this.encoding = encoding;            
        }

        /// <summary>
        /// 
        /// </summary>
        public override Encoding Encoding {
            get { return encoding; }
        }
    }
}
