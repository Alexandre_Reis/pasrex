﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.1.1210.0
                       MyGeneration Version # 1.2.0.7
                           10/03/2008 10:29:27
-------------------------------------------------------------------------------
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util.Enums;

namespace Financial.Util.ConfiguracaoSistema {
    public partial class Configuracao : esConfiguracao {

        /// <summary>
        /// Retorna o Parametro CoresParametro presente na Tabela de Configuracao
        /// Se não existir na Tabela retorna null
        /// </summary>
        /// <returns></returns>
        public string RetornaCoresExtrato() {
            
            if (!this.LoadByPrimaryKey(4000)) {
                return null;
            }
            else {
                return !String.IsNullOrEmpty(this.ValorTexto) ? this.ValorTexto.Trim() : "";
            }
        } 
    }
}
