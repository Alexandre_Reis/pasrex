﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Financial.Util.Enums
{

    #region Galgo

    public enum AprovarCotaGalgo
    {
        Aprovar = 0,
        Aprovada = 1,
        NaoAprovar = 2
    }

    public enum MetodoWcfGalgo
    {
        Consumir = 0,
        Pendentes = 1,
        Reenviados = 2
    }

    public enum IntegraGalgoPlCota
    {
        NaoIntegra = 0,
        IntegraTodas = 1,
        IntegraValidaCotasRepetidas = 2
    }
    #endregion

    #region Línguas
    /// <summary>
    /// Alguns padroes de Língua:    
    /// </summary>
    public enum PatternLingua
    {
        /// <summary></summary>
        Portugues = 1,

        /// <summary></summary>
        Ingles = 2,

        /// <summary></summary>
        Espanhol = 3        
    }
    #endregion

    #region Cálculo Financeiro
    /// <summary>
    /// Tipo de curva usada em apropriações de juros (exponencial vs linear)
    /// </summary>
    public enum TipoApropriacao
    {
        /// <summary></summary>
        Exponencial = 1,

        /// <summary></summary>
        Linear = 2        
    }

    /// <summary>
    /// Convenção da base de cálculo a ser usada.
    /// </summary>
    public enum BaseCalculo {
        Base252 = 252,        
        Base360 = 360,
        Base365 = 365
    }
    
    /// <summary>
    /// Dias úteis ou corridos.
    /// </summary>
    public enum ContagemDias {
        Uteis = 1,
        Corridos = 2,
        Dias360 = 3
    }

    /// <summary>
    /// Informa o tipo de ETTJ a ser usada nas eventuais interpolações.
    /// </summary>
    public enum EstruturaCurva
    {
        FuturoBMF_DI1 = 1,
        FuturoBMF_DDI = 2,
        SwapBMF_Pre = 10
    }
    #endregion

    public enum UnidadeMoeda
    {
        Milhar = 1,
        Milhao = 2
    }

    public enum TipoMercado
    {
        [StringValue("Bolsa")]
        Bolsa = 1,
        [StringValue("BMF")]
        BMF = 2,
        [StringValue("Renda Fixa")]
        RendaFixa = 3,
        [StringValue("Swap")]
        Swap = 4,
        [StringValue("Fundo")]
        Fundos = 5,
        [StringValue("Liquidação")]
        Liquidacao = 6,
        [StringValue("Caixa")]
        Caixa = 7,
        [StringValue("Cotista")]
        Cotista = 8
    }

    public static class TipoMercadoDescricao
    {
        public static string RetornaDescricao(int idTipoMercado)
        {
            return Enum.GetName(typeof(TipoMercado), idTipoMercado);
        }

        public static string RetornaStringValue(int idTipoMercado)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoMercado), RetornaDescricao(idTipoMercado)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoPerfilProcessamento
    {
        Automatico = 1,
        Manual = 2
    }

    public enum TipoComeCotas
    {
        Cotista = 1,
        Fundo = 2
    }

    /// <summary>
    /// Todos os padroes de Data:
    /// http://authors.aspalliance.com/aspxtreme/sys/demos/datetimeformats.aspx
    /// </summary>
    public enum PatternData {
        
        /// <summary>
        /// formato MM/dd/yyyy
        /// </summary>
        [StringValue("MM/dd/yyyy")]                       
        formato1 = 1,

        /// <summary>
        /// formato dd/MM/yyyy
        /// </summary>        
        [StringValue("dd/MM/yyyy")]
        formato2 = 2,

        /// <summary>
        /// formato ddMMyyyy
        /// </summary>        
        [StringValue("ddMMyyyy")]
        formato3 = 3,

        /// <summary>
        /// formato yyyyMMdd
        /// </summary>        
        [StringValue("yyyyMMdd")]
        formato4 = 4,

        /// <summary>
        /// formato yyMMdd
        /// </summary>        
        [StringValue("yyMMdd")]
        formato5 = 5,
    }

    public enum KeyHashTable
    {
        [StringValue("Perfil MTM")]
        PerfilMTM = 1,

        [StringValue("Taxa Curva")]
        TaxaCurva = 2,

        [StringValue("Curvas Compostas")]
        CurvasCompostas = 3,

        [StringValue("Fincs - Indexadores")]
        FincsIndexador = 4
    }


    /// <summary>
    /// Enum com o Mapeamento entre as Configurações existentes no sistema e o banco de Dados
    /// </summary>
    public static class ConfiguracaoRegrasNegocio {
        public static class Bolsa
        {
            public const int TipoVisualizacao = 1;
            public const int PrioridadeCasamentoVencimento = 2;
            public const int MTMTermoVendido = 3;            
            public const int CodigoAgenteDefault = 4;
            public const int CompensaIRDayTrade = 5;
            public const int EntradaOperacoes = 6;
            public const int CustodiaBTC = 7;
            public const int RenovacaoTermo = 8;
            public const int ConsolidacaoCustoMedio = 9;
            public const int IsencaoIRAcoes = 10;
            public const int EmiteMsgSemCotacaoBolsa = 11;
        }

        public static class BMF
        {
            public const int DescontoSocio = 30;
            public const int TipoVisualizacao = 31;
            public const int TipoCalculoTaxasBMF = 32;                        
        }

        public static class RendaFixaSwap
        {
            public const int TaxaEmolumento = 40;
            public const int PercentualRegistro = 41;
            public const int DescontoSocio = 42;
            public const int EmiteMsgSemCotacao = 43;
            public const int ControleCustodia = 44;
            public const int AliquotaUnicaGrossup = 45;
        }

        public static class Fundo
        {
            public const int TratamentoCotaInexistente = 100;
            public const int RecalculaCota = 101;
            public const int CotaBruta = 102;
            public const int ZeraCaixa = 103;
            public const int RetornoBenchmark = 104;
            public const int UsaCotaInicialPosicaoIR = 105;
            public const int SimulaYMFCOT = 106;
            public const int RetornoFDICAjustado = 107;
            public const int DistribuidorPadrao = 108;
            public const int PfeeCotasFundos = 109;
        }

        public static class Integracoes
        {
            public const int IntegracaoBolsa = 1000;
            public const int IntegracaoBMF = 1001;
            public const int IntegracaoCC = 1002;
            public const int IntegracaoRendaFixa = 1003;
            public const int IntegracaoCodigosCC = 1020;
            public const int SchemaSinacor = 1030;
            public const int SchemaBTC = 1031;
            public const int UsuarioIntegracao = 1040;
            public const int SenhaIntegracao = 1041;
            public const int NomeInstituicao = 1050;
            public const int CodInstituicao = 1051;
            public const int ResponsavelInstituicao = 1052;
            public const int TelefoneInstituicao = 1053;
            public const int EmailInstituicao = 1054;
            public const int IntegraGalgoPlCota = 1060;
        }

        public static class Seguranca {
            public const int TamanhoMinimoSenha = 2000;
            public const int TempoExpiracaoSenha = 2010;
            public const int TentativasBloqueioSenha = 2020;
            public const int HistoricoSenhas = 2030;
            public const int MinimoCaracterEspecialSenha = 2040;
            public const int MinimoNumeroSenha = 2041;
            public const int LetrasMaiusculas = 2042;
            public const int JanelaTempoResetSenha = 2050;
            public const int AcessoSimultaneo = 2060;

            public static class Menu {
                public const int AcoesOpcoes = 3000;
                public const int TermoAcoes = 3001;
                public const int BTC_Aluguel = 3002;
                public const int BMF = 3003;
                public const int RendaFixa = 3004;
                public const int Fundos = 3005;
                public const int Swap = 3006;
                public const int Gerencial = 3007;
                public const int Rebates = 3008;
                public const int CalculoDespesas = 3009;
                public const int Enquadramento = 3010;
                public const int Cotista = 3011;
                public const int Contabil = 3012;
                public const int ApuracaoIR = 3013;
                public const int InformeFundos = 3014;
                public const int InformeClubes = 3015;
                public const int FrontOffice = 3016;
                public const int CarteiraSimulada = 3017;
            }
        }

        public static class ConfiguracaoRelatorios {            

            public static class RelatorioExtratoCliente {
                // Mudado de const para static readonly para ver se relatorio-designer funciona 
                public static readonly int CoresExtrato = 4000;
                public static readonly int ObservacaoExtrato = 4001;
                public static readonly int EstiloGraficoDistribuicao = 4002;
                public static readonly int MostraPatrimonio = 4003;
                public static readonly int FiltraTopNPieChart = 4004;
                public static readonly int JanelaMovelSubReportRetornoCarteira = 4005;
                public static readonly int ExibePizzaTopN = 5060; 
            }

            public static class RelatorioExtratoCotista {                
                public static readonly int ObservacaoCotista = 4010;
            }
        }

        public static class Outras {
            public const int AgenteLiquidacaoExclusivo = 5000;
            public const int CalculaContatil = 5001;
            public const int MultiConta = 5002;
            public const int PermissaoInternoAuto = 5003;
            public const int LiberaDataLancamentoLiquidacao = 5004;
            public const int ResetDataInicio = 5010;
            public const int ProcessamentoViaWebService = 5011;
            public const int CalculaFatorIndice = 5012;
            public const int ProcessaIndicadorCarteira = 5013;
            public const int UsuarioIntegracao = 5020;
            public const int SenhaIntegracao = 5021;
            public const int ContaIntegracao = 5022;
            public const int SuitabilityRendaFixa = 5030;
            public const int EmailBackOffice = 5040;
            public const int EmailBackOfficeInstitucional = 5041;
            public const int DisclaimerPosicao = 5042;
            public const int DisclaimerOperacao = 5043;
            public const int RetroagirCarteirasDependentes = 5014;
            public const int PermitirOperacoesRetroativas = 5015;
            public const int DataBase = 5016;
            public const int IdAutomaticoPessoa = 5050;
            public const int PermiteDuplDocumentoPessoa = 5051;
            public const int PropagaAlterNomeCliente = 5052;
            public const int EstendeConceitoPessoaAgente = 5053;
            public const int PropagaAlterNomeAgente = 5054;
            public const int ValidaQtdeResgateProcessamento = 5055;
            public const int TipoVisualizacaoFundoResgateCotista = 5056;
            public const int DescontarRendimentoCupom = 5057;

            //Se versão v1.1.X -- 'Outras' >= 6100
            public const int AbortaProcRFPorErroCadastraTitulo = 6100;
            public const int CalculaCofUsandoPLD0 = 6101;
        }

    }
}