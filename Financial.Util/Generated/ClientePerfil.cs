/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/05/2015 12:56:46
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;
using Financial.Investidor;



namespace Financial.Util
{

	[Serializable]
	abstract public class esClientePerfilCollection : esEntityCollection
	{
		public esClientePerfilCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ClientePerfilCollection";
		}

		#region Query Logic
		protected void InitQuery(esClientePerfilQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esClientePerfilQuery);
		}
		#endregion
		
		virtual public ClientePerfil DetachEntity(ClientePerfil entity)
		{
			return base.DetachEntity(entity) as ClientePerfil;
		}
		
		virtual public ClientePerfil AttachEntity(ClientePerfil entity)
		{
			return base.AttachEntity(entity) as ClientePerfil;
		}
		
		virtual public void Combine(ClientePerfilCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ClientePerfil this[int index]
		{
			get
			{
				return base[index] as ClientePerfil;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ClientePerfil);
		}
	}



	[Serializable]
	abstract public class esClientePerfil : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esClientePerfilQuery GetDynamicQuery()
		{
			return null;
		}

		public esClientePerfil()
		{

		}

		public esClientePerfil(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente, System.Int32 idPerfil)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idPerfil);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idPerfil);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente, System.Int32 idPerfil)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idPerfil);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idPerfil);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente, System.Int32 idPerfil)
		{
			esClientePerfilQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente, query.IdPerfil == idPerfil);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente, System.Int32 idPerfil)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);			parms.Add("IdPerfil",idPerfil);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPerfil": this.str.IdPerfil = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "Sequencia": this.str.Sequencia = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPerfil":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfil = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "Sequencia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Sequencia = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ClientePerfil.IdPerfil
		/// </summary>
		virtual public System.Int32? IdPerfil
		{
			get
			{
				return base.GetSystemInt32(ClientePerfilMetadata.ColumnNames.IdPerfil);
			}
			
			set
			{
				if(base.SetSystemInt32(ClientePerfilMetadata.ColumnNames.IdPerfil, value))
				{
					this._UpToPerfilProcessamentoByIdPerfil = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ClientePerfil.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ClientePerfilMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(ClientePerfilMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ClientePerfil.Sequencia
		/// </summary>
		virtual public System.Int32? Sequencia
		{
			get
			{
				return base.GetSystemInt32(ClientePerfilMetadata.ColumnNames.Sequencia);
			}
			
			set
			{
				base.SetSystemInt32(ClientePerfilMetadata.ColumnNames.Sequencia, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected PerfilProcessamento _UpToPerfilProcessamentoByIdPerfil;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esClientePerfil entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPerfil
			{
				get
				{
					System.Int32? data = entity.IdPerfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfil = null;
					else entity.IdPerfil = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String Sequencia
			{
				get
				{
					System.Int32? data = entity.Sequencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Sequencia = null;
					else entity.Sequencia = Convert.ToInt32(value);
				}
			}
			

			private esClientePerfil entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esClientePerfilQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esClientePerfil can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ClientePerfil : esClientePerfil
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_ClientePerfil_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPerfilProcessamentoByIdPerfil - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Perfil_ClientePerfil_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilProcessamento UpToPerfilProcessamentoByIdPerfil
		{
			get
			{
				if(this._UpToPerfilProcessamentoByIdPerfil == null
					&& IdPerfil != null					)
				{
					this._UpToPerfilProcessamentoByIdPerfil = new PerfilProcessamento();
					this._UpToPerfilProcessamentoByIdPerfil.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPerfilProcessamentoByIdPerfil", this._UpToPerfilProcessamentoByIdPerfil);
					this._UpToPerfilProcessamentoByIdPerfil.Query.Where(this._UpToPerfilProcessamentoByIdPerfil.Query.IdPerfil == this.IdPerfil);
					this._UpToPerfilProcessamentoByIdPerfil.Query.Load();
				}

				return this._UpToPerfilProcessamentoByIdPerfil;
			}
			
			set
			{
				this.RemovePreSave("UpToPerfilProcessamentoByIdPerfil");
				

				if(value == null)
				{
					this.IdPerfil = null;
					this._UpToPerfilProcessamentoByIdPerfil = null;
				}
				else
				{
					this.IdPerfil = value.IdPerfil;
					this._UpToPerfilProcessamentoByIdPerfil = value;
					this.SetPreSave("UpToPerfilProcessamentoByIdPerfil", this._UpToPerfilProcessamentoByIdPerfil);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToPerfilProcessamentoByIdPerfil != null)
			{
				this.IdPerfil = this._UpToPerfilProcessamentoByIdPerfil.IdPerfil;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esClientePerfilQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ClientePerfilMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPerfil
		{
			get
			{
				return new esQueryItem(this, ClientePerfilMetadata.ColumnNames.IdPerfil, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ClientePerfilMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Sequencia
		{
			get
			{
				return new esQueryItem(this, ClientePerfilMetadata.ColumnNames.Sequencia, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ClientePerfilCollection")]
	public partial class ClientePerfilCollection : esClientePerfilCollection, IEnumerable<ClientePerfil>
	{
		public ClientePerfilCollection()
		{

		}
		
		public static implicit operator List<ClientePerfil>(ClientePerfilCollection coll)
		{
			List<ClientePerfil> list = new List<ClientePerfil>();
			
			foreach (ClientePerfil emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ClientePerfilMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClientePerfilQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ClientePerfil(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ClientePerfil();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ClientePerfilQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClientePerfilQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ClientePerfilQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ClientePerfil AddNew()
		{
			ClientePerfil entity = base.AddNewEntity() as ClientePerfil;
			
			return entity;
		}

		public ClientePerfil FindByPrimaryKey(System.Int32 idCliente, System.Int32 idPerfil)
		{
			return base.FindByPrimaryKey(idCliente, idPerfil) as ClientePerfil;
		}


		#region IEnumerable<ClientePerfil> Members

		IEnumerator<ClientePerfil> IEnumerable<ClientePerfil>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ClientePerfil;
			}
		}

		#endregion
		
		private ClientePerfilQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ClientePerfil' table
	/// </summary>

	[Serializable]
	public partial class ClientePerfil : esClientePerfil
	{
		public ClientePerfil()
		{

		}
	
		public ClientePerfil(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ClientePerfilMetadata.Meta();
			}
		}
		
		
		
		override protected esClientePerfilQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClientePerfilQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ClientePerfilQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClientePerfilQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ClientePerfilQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ClientePerfilQuery query;
	}



	[Serializable]
	public partial class ClientePerfilQuery : esClientePerfilQuery
	{
		public ClientePerfilQuery()
		{

		}		
		
		public ClientePerfilQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ClientePerfilMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ClientePerfilMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ClientePerfilMetadata.ColumnNames.IdPerfil, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClientePerfilMetadata.PropertyNames.IdPerfil;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClientePerfilMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClientePerfilMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClientePerfilMetadata.ColumnNames.Sequencia, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClientePerfilMetadata.PropertyNames.Sequencia;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ClientePerfilMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPerfil = "IdPerfil";
			 public const string IdCliente = "IdCliente";
			 public const string Sequencia = "Sequencia";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPerfil = "IdPerfil";
			 public const string IdCliente = "IdCliente";
			 public const string Sequencia = "Sequencia";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ClientePerfilMetadata))
			{
				if(ClientePerfilMetadata.mapDelegates == null)
				{
					ClientePerfilMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ClientePerfilMetadata.meta == null)
				{
					ClientePerfilMetadata.meta = new ClientePerfilMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPerfil", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Sequencia", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "ClientePerfil";
				meta.Destination = "ClientePerfil";
				
				meta.spInsert = "proc_ClientePerfilInsert";				
				meta.spUpdate = "proc_ClientePerfilUpdate";		
				meta.spDelete = "proc_ClientePerfilDelete";
				meta.spLoadAll = "proc_ClientePerfilLoadAll";
				meta.spLoadByPrimaryKey = "proc_ClientePerfilLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ClientePerfilMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
