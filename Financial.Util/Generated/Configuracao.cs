/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 09/01/2009 15:15:35
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		























		






	
















				
		
		




		









		
				
				








	















					
								
											
	









		





		




				
				







namespace Financial.Util.ConfiguracaoSistema
{

	[Serializable]
	abstract public class esConfiguracaoCollection : esEntityCollection
	{
		public esConfiguracaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ConfiguracaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esConfiguracaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esConfiguracaoQuery);
		}
		#endregion
		
		virtual public Configuracao DetachEntity(Configuracao entity)
		{
			return base.DetachEntity(entity) as Configuracao;
		}
		
		virtual public Configuracao AttachEntity(Configuracao entity)
		{
			return base.AttachEntity(entity) as Configuracao;
		}
		
		virtual public void Combine(ConfiguracaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Configuracao this[int index]
		{
			get
			{
				return base[index] as Configuracao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Configuracao);
		}
	}



	[Serializable]
	abstract public class esConfiguracao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esConfiguracaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esConfiguracao()
		{

		}

		public esConfiguracao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 id)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 id)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esConfiguracaoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Id == id);
		
			return query.Load();
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 id)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(id);
			else
				return LoadByPrimaryKeyStoredProcedure(id);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 id)
		{
			esConfiguracaoQuery query = this.GetDynamicQuery();
			query.Where(query.Id == id);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 id)
		{
			esParameters parms = new esParameters();
			parms.Add("Id",id);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Id": this.str.Id = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "ValorNumerico": this.str.ValorNumerico = (string)value; break;							
						case "ValorTexto": this.str.ValorTexto = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Id":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Id = (System.Int32?)value;
							break;
						
						case "ValorNumerico":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorNumerico = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Configuracao.Id
		/// </summary>
		virtual public System.Int32? Id
		{
			get
			{
				return base.GetSystemInt32(ConfiguracaoMetadata.ColumnNames.Id);
			}
			
			set
			{
				base.SetSystemInt32(ConfiguracaoMetadata.ColumnNames.Id, value);
			}
		}
		
		/// <summary>
		/// Maps to Configuracao.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(ConfiguracaoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(ConfiguracaoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to Configuracao.ValorNumerico
		/// </summary>
		virtual public System.Decimal? ValorNumerico
		{
			get
			{
				return base.GetSystemDecimal(ConfiguracaoMetadata.ColumnNames.ValorNumerico);
			}
			
			set
			{
				base.SetSystemDecimal(ConfiguracaoMetadata.ColumnNames.ValorNumerico, value);
			}
		}
		
		/// <summary>
		/// Maps to Configuracao.ValorTexto
		/// </summary>
		virtual public System.String ValorTexto
		{
			get
			{
				return base.GetSystemString(ConfiguracaoMetadata.ColumnNames.ValorTexto);
			}
			
			set
			{
				base.SetSystemString(ConfiguracaoMetadata.ColumnNames.ValorTexto, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esConfiguracao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Id
			{
				get
				{
					System.Int32? data = entity.Id;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Id = null;
					else entity.Id = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String ValorNumerico
			{
				get
				{
					System.Decimal? data = entity.ValorNumerico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorNumerico = null;
					else entity.ValorNumerico = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorTexto
			{
				get
				{
					System.String data = entity.ValorTexto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorTexto = null;
					else entity.ValorTexto = Convert.ToString(value);
				}
			}
			

			private esConfiguracao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esConfiguracaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esConfiguracao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Configuracao : esConfiguracao
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esConfiguracaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ConfiguracaoMetadata.Meta();
			}
		}	
		

		public esQueryItem Id
		{
			get
			{
				return new esQueryItem(this, ConfiguracaoMetadata.ColumnNames.Id, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, ConfiguracaoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem ValorNumerico
		{
			get
			{
				return new esQueryItem(this, ConfiguracaoMetadata.ColumnNames.ValorNumerico, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorTexto
		{
			get
			{
				return new esQueryItem(this, ConfiguracaoMetadata.ColumnNames.ValorTexto, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ConfiguracaoCollection")]
	public partial class ConfiguracaoCollection : esConfiguracaoCollection, IEnumerable<Configuracao>
	{
		public ConfiguracaoCollection()
		{

		}
		
		public static implicit operator List<Configuracao>(ConfiguracaoCollection coll)
		{
			List<Configuracao> list = new List<Configuracao>();
			
			foreach (Configuracao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ConfiguracaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ConfiguracaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Configuracao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Configuracao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ConfiguracaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ConfiguracaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ConfiguracaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Configuracao AddNew()
		{
			Configuracao entity = base.AddNewEntity() as Configuracao;
			
			return entity;
		}

		public Configuracao FindByPrimaryKey(System.Int32 id)
		{
			return base.FindByPrimaryKey(id) as Configuracao;
		}


		#region IEnumerable<Configuracao> Members

		IEnumerator<Configuracao> IEnumerable<Configuracao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Configuracao;
			}
		}

		#endregion
		
		private ConfiguracaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Configuracao' table
	/// </summary>

	[Serializable]
	public partial class Configuracao : esConfiguracao
	{
		public Configuracao()
		{

		}
	
		public Configuracao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ConfiguracaoMetadata.Meta();
			}
		}
		
		
		
		override protected esConfiguracaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ConfiguracaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ConfiguracaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ConfiguracaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ConfiguracaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ConfiguracaoQuery query;
	}



	[Serializable]
	public partial class ConfiguracaoQuery : esConfiguracaoQuery
	{
		public ConfiguracaoQuery()
		{

		}		
		
		public ConfiguracaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ConfiguracaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ConfiguracaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ConfiguracaoMetadata.ColumnNames.Id, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ConfiguracaoMetadata.PropertyNames.Id;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ConfiguracaoMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ConfiguracaoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ConfiguracaoMetadata.ColumnNames.ValorNumerico, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ConfiguracaoMetadata.PropertyNames.ValorNumerico;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ConfiguracaoMetadata.ColumnNames.ValorTexto, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = ConfiguracaoMetadata.PropertyNames.ValorTexto;
			c.CharacterMaxLength = 400;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ConfiguracaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Id = "Id";
			 public const string Descricao = "Descricao";
			 public const string ValorNumerico = "ValorNumerico";
			 public const string ValorTexto = "ValorTexto";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Id = "Id";
			 public const string Descricao = "Descricao";
			 public const string ValorNumerico = "ValorNumerico";
			 public const string ValorTexto = "ValorTexto";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ConfiguracaoMetadata))
			{
				if(ConfiguracaoMetadata.mapDelegates == null)
				{
					ConfiguracaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ConfiguracaoMetadata.meta == null)
				{
					ConfiguracaoMetadata.meta = new ConfiguracaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Id", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ValorNumerico", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorTexto", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "Configuracao";
				meta.Destination = "Configuracao";
				
				meta.spInsert = "proc_ConfiguracaoInsert";				
				meta.spUpdate = "proc_ConfiguracaoUpdate";		
				meta.spDelete = "proc_ConfiguracaoDelete";
				meta.spLoadAll = "proc_ConfiguracaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_ConfiguracaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ConfiguracaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
