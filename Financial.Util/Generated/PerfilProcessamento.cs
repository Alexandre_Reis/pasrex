/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/05/2015 15:15:14
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;



namespace Financial.Util
{

	[Serializable]
	abstract public class esPerfilProcessamentoCollection : esEntityCollection
	{
		public esPerfilProcessamentoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PerfilProcessamentoCollection";
		}

		#region Query Logic
		protected void InitQuery(esPerfilProcessamentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPerfilProcessamentoQuery);
		}
		#endregion
		
		virtual public PerfilProcessamento DetachEntity(PerfilProcessamento entity)
		{
			return base.DetachEntity(entity) as PerfilProcessamento;
		}
		
		virtual public PerfilProcessamento AttachEntity(PerfilProcessamento entity)
		{
			return base.AttachEntity(entity) as PerfilProcessamento;
		}
		
		virtual public void Combine(PerfilProcessamentoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PerfilProcessamento this[int index]
		{
			get
			{
				return base[index] as PerfilProcessamento;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PerfilProcessamento);
		}
	}



	[Serializable]
	abstract public class esPerfilProcessamento : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPerfilProcessamentoQuery GetDynamicQuery()
		{
			return null;
		}

		public esPerfilProcessamento()
		{

		}

		public esPerfilProcessamento(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPerfil)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfil);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfil);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPerfil)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfil);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfil);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPerfil)
		{
			esPerfilProcessamentoQuery query = this.GetDynamicQuery();
			query.Where(query.IdPerfil == idPerfil);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPerfil)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPerfil",idPerfil);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPerfil": this.str.IdPerfil = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;							
						case "DiasFechamento": this.str.DiasFechamento = (string)value; break;							
						case "DiasRetroagir": this.str.DiasRetroagir = (string)value; break;							
						case "AberturaAutomatica": this.str.AberturaAutomatica = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPerfil":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfil = (System.Int32?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Tipo = (System.Int32?)value;
							break;
						
						case "DiasFechamento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DiasFechamento = (System.Int32?)value;
							break;
						
						case "DiasRetroagir":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DiasRetroagir = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PerfilProcessamento.IdPerfil
		/// </summary>
		virtual public System.Int32? IdPerfil
		{
			get
			{
				return base.GetSystemInt32(PerfilProcessamentoMetadata.ColumnNames.IdPerfil);
			}
			
			set
			{
				base.SetSystemInt32(PerfilProcessamentoMetadata.ColumnNames.IdPerfil, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilProcessamento.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(PerfilProcessamentoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(PerfilProcessamentoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilProcessamento.Tipo
		/// </summary>
		virtual public System.Int32? Tipo
		{
			get
			{
				return base.GetSystemInt32(PerfilProcessamentoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemInt32(PerfilProcessamentoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilProcessamento.DiasFechamento
		/// </summary>
		virtual public System.Int32? DiasFechamento
		{
			get
			{
				return base.GetSystemInt32(PerfilProcessamentoMetadata.ColumnNames.DiasFechamento);
			}
			
			set
			{
				base.SetSystemInt32(PerfilProcessamentoMetadata.ColumnNames.DiasFechamento, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilProcessamento.DiasRetroagir
		/// </summary>
		virtual public System.Int32? DiasRetroagir
		{
			get
			{
				return base.GetSystemInt32(PerfilProcessamentoMetadata.ColumnNames.DiasRetroagir);
			}
			
			set
			{
				base.SetSystemInt32(PerfilProcessamentoMetadata.ColumnNames.DiasRetroagir, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilProcessamento.AberturaAutomatica
		/// </summary>
		virtual public System.String AberturaAutomatica
		{
			get
			{
				return base.GetSystemString(PerfilProcessamentoMetadata.ColumnNames.AberturaAutomatica);
			}
			
			set
			{
				base.SetSystemString(PerfilProcessamentoMetadata.ColumnNames.AberturaAutomatica, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPerfilProcessamento entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPerfil
			{
				get
				{
					System.Int32? data = entity.IdPerfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfil = null;
					else entity.IdPerfil = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Int32? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToInt32(value);
				}
			}
				
			public System.String DiasFechamento
			{
				get
				{
					System.Int32? data = entity.DiasFechamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasFechamento = null;
					else entity.DiasFechamento = Convert.ToInt32(value);
				}
			}
				
			public System.String DiasRetroagir
			{
				get
				{
					System.Int32? data = entity.DiasRetroagir;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasRetroagir = null;
					else entity.DiasRetroagir = Convert.ToInt32(value);
				}
			}
				
			public System.String AberturaAutomatica
			{
				get
				{
					System.String data = entity.AberturaAutomatica;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AberturaAutomatica = null;
					else entity.AberturaAutomatica = Convert.ToString(value);
				}
			}
			

			private esPerfilProcessamento entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPerfilProcessamentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPerfilProcessamento can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PerfilProcessamento : esPerfilProcessamento
	{

				
		#region AgendaProcessosCollectionByIdPerfil - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PerfilProcessamento_AgendaProcessos_FK1
		/// </summary>

		[XmlIgnore]
		public AgendaProcessosCollection AgendaProcessosCollectionByIdPerfil
		{
			get
			{
				if(this._AgendaProcessosCollectionByIdPerfil == null)
				{
					this._AgendaProcessosCollectionByIdPerfil = new AgendaProcessosCollection();
					this._AgendaProcessosCollectionByIdPerfil.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AgendaProcessosCollectionByIdPerfil", this._AgendaProcessosCollectionByIdPerfil);
				
					if(this.IdPerfil != null)
					{
						this._AgendaProcessosCollectionByIdPerfil.Query.Where(this._AgendaProcessosCollectionByIdPerfil.Query.IdPerfil == this.IdPerfil);
						this._AgendaProcessosCollectionByIdPerfil.Query.Load();

						// Auto-hookup Foreign Keys
						this._AgendaProcessosCollectionByIdPerfil.fks.Add(AgendaProcessosMetadata.ColumnNames.IdPerfil, this.IdPerfil);
					}
				}

				return this._AgendaProcessosCollectionByIdPerfil;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AgendaProcessosCollectionByIdPerfil != null) 
				{ 
					this.RemovePostSave("AgendaProcessosCollectionByIdPerfil"); 
					this._AgendaProcessosCollectionByIdPerfil = null;
					
				} 
			} 			
		}

		private AgendaProcessosCollection _AgendaProcessosCollectionByIdPerfil;
		#endregion

		#region UpToClienteCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - Perfil_ClientePerfil_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteCollection UpToClienteCollection
		{
			get
			{
				if(this._UpToClienteCollection == null)
				{
					this._UpToClienteCollection = new ClienteCollection();
					this._UpToClienteCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToClienteCollection", this._UpToClienteCollection);
					this._UpToClienteCollection.ManyToManyPerfilProcessamentoCollection(this.IdPerfil);
				}

				return this._UpToClienteCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToClienteCollection != null) 
				{ 
					this.RemovePostSave("UpToClienteCollection"); 
					this._UpToClienteCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - Perfil_ClientePerfil_FK1
		/// </summary>
		public void AssociateClienteCollection(Cliente entity)
		{
			if (this._ClientePerfilCollection == null)
			{
				this._ClientePerfilCollection = new ClientePerfilCollection();
				this._ClientePerfilCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("ClientePerfilCollection", this._ClientePerfilCollection);
			}

			ClientePerfil obj = this._ClientePerfilCollection.AddNew();
			obj.IdPerfil = this.IdPerfil;
			obj.IdCliente = entity.IdCliente;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - Perfil_ClientePerfil_FK1
		/// </summary>
		public void DissociateClienteCollection(Cliente entity)
		{
			if (this._ClientePerfilCollection == null)
			{
				this._ClientePerfilCollection = new ClientePerfilCollection();
				this._ClientePerfilCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("ClientePerfilCollection", this._ClientePerfilCollection);
			}

			ClientePerfil obj = this._ClientePerfilCollection.AddNew();
			obj.IdPerfil = this.IdPerfil;
			obj.IdCliente = entity.IdCliente;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private ClienteCollection _UpToClienteCollection;
		private ClientePerfilCollection _ClientePerfilCollection;
		#endregion

				
		#region ClientePerfilCollectionByIdPerfil - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Perfil_ClientePerfil_FK1
		/// </summary>

		[XmlIgnore]
		public ClientePerfilCollection ClientePerfilCollectionByIdPerfil
		{
			get
			{
				if(this._ClientePerfilCollectionByIdPerfil == null)
				{
					this._ClientePerfilCollectionByIdPerfil = new ClientePerfilCollection();
					this._ClientePerfilCollectionByIdPerfil.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ClientePerfilCollectionByIdPerfil", this._ClientePerfilCollectionByIdPerfil);
				
					if(this.IdPerfil != null)
					{
						this._ClientePerfilCollectionByIdPerfil.Query.Where(this._ClientePerfilCollectionByIdPerfil.Query.IdPerfil == this.IdPerfil);
						this._ClientePerfilCollectionByIdPerfil.Query.Load();

						// Auto-hookup Foreign Keys
						this._ClientePerfilCollectionByIdPerfil.fks.Add(ClientePerfilMetadata.ColumnNames.IdPerfil, this.IdPerfil);
					}
				}

				return this._ClientePerfilCollectionByIdPerfil;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClientePerfilCollectionByIdPerfil != null) 
				{ 
					this.RemovePostSave("ClientePerfilCollectionByIdPerfil"); 
					this._ClientePerfilCollectionByIdPerfil = null;
					
				} 
			} 			
		}

		private ClientePerfilCollection _ClientePerfilCollectionByIdPerfil;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "AgendaProcessosCollectionByIdPerfil", typeof(AgendaProcessosCollection), new AgendaProcessos()));
			props.Add(new esPropertyDescriptor(this, "ClientePerfilCollectionByIdPerfil", typeof(ClientePerfilCollection), new ClientePerfil()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._AgendaProcessosCollectionByIdPerfil != null)
			{
				foreach(AgendaProcessos obj in this._AgendaProcessosCollectionByIdPerfil)
				{
					if(obj.es.IsAdded)
					{
						obj.IdPerfil = this.IdPerfil;
					}
				}
			}
			if(this._ClientePerfilCollection != null)
			{
				foreach(ClientePerfil obj in this._ClientePerfilCollection)
				{
					if(obj.es.IsAdded)
					{
						obj.IdPerfil = this.IdPerfil;
					}
				}
			}
			if(this._ClientePerfilCollectionByIdPerfil != null)
			{
				foreach(ClientePerfil obj in this._ClientePerfilCollectionByIdPerfil)
				{
					if(obj.es.IsAdded)
					{
						obj.IdPerfil = this.IdPerfil;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}
	
	public partial class PerfilProcessamentoCollection : esPerfilProcessamentoCollection
	{
		#region ManyToManyClienteCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyClienteCollection(System.Int32? IdCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente", IdCliente);
	
			return base.Load( esQueryType.ManyToMany, 
				"PerfilProcessamento,ClientePerfil|IdPerfil,IdPerfil|IdCliente",	parms);
		}
		#endregion
	}




	[Serializable]
	abstract public class esPerfilProcessamentoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PerfilProcessamentoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPerfil
		{
			get
			{
				return new esQueryItem(this, PerfilProcessamentoMetadata.ColumnNames.IdPerfil, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, PerfilProcessamentoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, PerfilProcessamentoMetadata.ColumnNames.Tipo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DiasFechamento
		{
			get
			{
				return new esQueryItem(this, PerfilProcessamentoMetadata.ColumnNames.DiasFechamento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DiasRetroagir
		{
			get
			{
				return new esQueryItem(this, PerfilProcessamentoMetadata.ColumnNames.DiasRetroagir, esSystemType.Int32);
			}
		} 
		
		public esQueryItem AberturaAutomatica
		{
			get
			{
				return new esQueryItem(this, PerfilProcessamentoMetadata.ColumnNames.AberturaAutomatica, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PerfilProcessamentoCollection")]
	public partial class PerfilProcessamentoCollection : esPerfilProcessamentoCollection, IEnumerable<PerfilProcessamento>
	{
		public PerfilProcessamentoCollection()
		{

		}
		
		public static implicit operator List<PerfilProcessamento>(PerfilProcessamentoCollection coll)
		{
			List<PerfilProcessamento> list = new List<PerfilProcessamento>();
			
			foreach (PerfilProcessamento emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PerfilProcessamentoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilProcessamentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PerfilProcessamento(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PerfilProcessamento();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PerfilProcessamentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilProcessamentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PerfilProcessamentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PerfilProcessamento AddNew()
		{
			PerfilProcessamento entity = base.AddNewEntity() as PerfilProcessamento;
			
			return entity;
		}

		public PerfilProcessamento FindByPrimaryKey(System.Int32 idPerfil)
		{
			return base.FindByPrimaryKey(idPerfil) as PerfilProcessamento;
		}


		#region IEnumerable<PerfilProcessamento> Members

		IEnumerator<PerfilProcessamento> IEnumerable<PerfilProcessamento>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PerfilProcessamento;
			}
		}

		#endregion
		
		private PerfilProcessamentoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PerfilProcessamento' table
	/// </summary>

	[Serializable]
	public partial class PerfilProcessamento : esPerfilProcessamento
	{
		public PerfilProcessamento()
		{

		}
	
		public PerfilProcessamento(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PerfilProcessamentoMetadata.Meta();
			}
		}
		
		
		
		override protected esPerfilProcessamentoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilProcessamentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PerfilProcessamentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilProcessamentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PerfilProcessamentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PerfilProcessamentoQuery query;
	}



	[Serializable]
	public partial class PerfilProcessamentoQuery : esPerfilProcessamentoQuery
	{
		public PerfilProcessamentoQuery()
		{

		}		
		
		public PerfilProcessamentoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PerfilProcessamentoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PerfilProcessamentoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PerfilProcessamentoMetadata.ColumnNames.IdPerfil, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilProcessamentoMetadata.PropertyNames.IdPerfil;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilProcessamentoMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = PerfilProcessamentoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilProcessamentoMetadata.ColumnNames.Tipo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilProcessamentoMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilProcessamentoMetadata.ColumnNames.DiasFechamento, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilProcessamentoMetadata.PropertyNames.DiasFechamento;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilProcessamentoMetadata.ColumnNames.DiasRetroagir, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilProcessamentoMetadata.PropertyNames.DiasRetroagir;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilProcessamentoMetadata.ColumnNames.AberturaAutomatica, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = PerfilProcessamentoMetadata.PropertyNames.AberturaAutomatica;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PerfilProcessamentoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPerfil = "IdPerfil";
			 public const string Descricao = "Descricao";
			 public const string Tipo = "Tipo";
			 public const string DiasFechamento = "DiasFechamento";
			 public const string DiasRetroagir = "DiasRetroagir";
			 public const string AberturaAutomatica = "AberturaAutomatica";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPerfil = "IdPerfil";
			 public const string Descricao = "Descricao";
			 public const string Tipo = "Tipo";
			 public const string DiasFechamento = "DiasFechamento";
			 public const string DiasRetroagir = "DiasRetroagir";
			 public const string AberturaAutomatica = "AberturaAutomatica";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PerfilProcessamentoMetadata))
			{
				if(PerfilProcessamentoMetadata.mapDelegates == null)
				{
					PerfilProcessamentoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PerfilProcessamentoMetadata.meta == null)
				{
					PerfilProcessamentoMetadata.meta = new PerfilProcessamentoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPerfil", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DiasFechamento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DiasRetroagir", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AberturaAutomatica", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "PerfilProcessamento";
				meta.Destination = "PerfilProcessamento";
				
				meta.spInsert = "proc_PerfilProcessamentoInsert";				
				meta.spUpdate = "proc_PerfilProcessamentoUpdate";		
				meta.spDelete = "proc_PerfilProcessamentoDelete";
				meta.spLoadAll = "proc_PerfilProcessamentoLoadAll";
				meta.spLoadByPrimaryKey = "proc_PerfilProcessamentoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PerfilProcessamentoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
