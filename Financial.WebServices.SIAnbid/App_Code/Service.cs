using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;
using System.IO;
using System.Xml; 
using System.Collections.Generic;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Service : System.Web.Services.WebService
{
    public Service()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    private string FormataCodFundo(string codFundo)
    {
        return codFundo.PadLeft(6, '0');
    }

    [WebMethod]
    public DataTable RetornaCotas(string codFundo, DateTime? dataInicio)
    {
        codFundo = this.FormataCodFundo(codFundo);
        SqlConnection conn = this.CreateSqlConnection();

        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adapter = new SqlDataAdapter();
        DataTable dataTable = new DataTable();
        dataTable.TableName = "HistoricoCota";

        string queryDataInicio = "";
        if (dataInicio.HasValue)
        {
            queryDataInicio = String.Format(" and data >= '{0}'", dataInicio.Value.ToString("yyyy-MM-dd"));
        }
        cmd.CommandText = String.Format("select * from fundos_dia where valcota IS NOT NULL and codfundo='{0}'{1}", codFundo, queryDataInicio);
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.Connection = conn;

        adapter.SelectCommand = cmd;
        adapter.Fill(dataTable);

	dataTable.Columns.Add("dataString", typeof(String));

	foreach (DataRow row in dataTable.Rows)
	{
	    row[8] = Convert.ToDateTime(row[1]).ToString("yyyy-MM-dd");
	}

        return dataTable;

    }

    private class Filters : List<string>
    {
        public void Add(string filterName, string filterValue)
        {
            if (!string.IsNullOrEmpty(filterValue))
            {
                filterValue = filterValue.Trim();
                filterValue = filterValue.Replace(" ", "%");
                this.Add(String.Format("{0} like '%{1}%'", filterName, filterValue));
            }
        }
    }
        
    [WebMethod]
    public DataTable CarregaFundo(string codFundo, string selectColumns, bool loadExtendedProperties)
    {
        codFundo = this.FormataCodFundo(codFundo);
        SqlConnection conn = this.CreateSqlConnection();

        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adapter = new SqlDataAdapter();
        DataTable dataTable = new DataTable();
        dataTable.TableName = "Fundo";

        string selectColumnsQuery = string.IsNullOrEmpty(selectColumns) ? "*" : selectColumns;
        
        string movCota = "";
        string taxaAdm = "";
        string query = "";

        if (loadExtendedProperties)
        {
            query = "SELECT f.*, m.*, " +
                "adm.fantasia as admin_fantasia, " +
                "ges.fantasia as gestor_fantasia, " +
                "t.*, " +
                "tf.*, " +
                "ta.*, " +
                "ff.* " +
                "FROM fundos f " +

                "LEFT OUTER JOIN fundos_mov_cota m on f.codfundo = m.codfundo " +
                "LEFT OUTER JOIN instituicoes adm on f.codinst = adm.codinst " +
                "LEFT OUTER JOIN instituicoes ges on f.gestor = ges.codinst " +
                "INNER JOIN fundos_tipo t on f.codtipo = t.codtipo " +
                "LEFT OUTER JOIN fundos_tipo_financial tf on f.codtipo = tf.codtipo " +
                "LEFT OUTER JOIN taxa_adm ta on f.codfundo = ta.codfundo " +
                "LEFT OUTER JOIN fundos_financial ff on f.codfundo = ff.codfundo " +

                "where f.codfundo = '{0}' ";

                movCota = "and m.data in ( " +
                "    select max(fundos_mov_cota.data) from fundos_mov_cota where f.codfundo = fundos_mov_cota.codfundo " +
                ") ";

                taxaAdm = "and ta.data in ( " +
                "    select max(taxa_adm.data) from taxa_adm where f.codfundo = taxa_adm.codfundo " +
                ") ";

                query = query + movCota + taxaAdm;

                cmd.CommandText = String.Format(query, codFundo);
        }
        else
        {
            string filterQuery = String.Format("codfundo = '{0}'", codFundo);
            query = "select {0} from fundos where {1}";
            cmd.CommandText = String.Format(query, selectColumnsQuery, filterQuery);
        }

        cmd.CommandType = System.Data.CommandType.Text;
        cmd.Connection = conn;

        adapter.SelectCommand = cmd;
        adapter.Fill(dataTable);

        if (dataTable.Rows.Count == 0)
        {
            cmd.CommandText = String.Format(query.Replace(taxaAdm, ""), codFundo);
            adapter.SelectCommand = cmd;
            adapter.Fill(dataTable);
        }
        if (dataTable.Rows.Count == 0)
        {
            cmd.CommandText = String.Format(query.Replace(movCota, ""), codFundo);
            adapter.SelectCommand = cmd;
            adapter.Fill(dataTable);
        }

        if (dataTable.Rows.Count == 0)
        {
            cmd.CommandText = String.Format(query.Replace(taxaAdm, "").Replace(movCota, ""), codFundo);
            adapter.SelectCommand = cmd;
            adapter.Fill(dataTable);
        }

        if (loadExtendedProperties)
        {
            const string COLUMNNAME_DATA_INICIO_COTA = "DataInicioCota";
            const string COLUMNNAME_COTA_INICIAL = "CotaInicial";

            dataTable.Columns.Add(COLUMNNAME_DATA_INICIO_COTA, typeof(System.DateTime));
            dataTable.Columns.Add(COLUMNNAME_COTA_INICIAL, typeof(System.Decimal));

            DataTable cotas = this.RetornaCotas(codFundo, null);
            DateTime dataInicioCota = DateTime.MaxValue;
            Decimal cotaInicial = 0;

            foreach (DataRow cota in cotas.Rows)
            {
                DateTime dataCota = (DateTime)cota["data"];
                if (dataCota < dataInicioCota)
                {
                    dataInicioCota = dataCota;
                    cotaInicial = (decimal)cota["valcota"];
                }
            }
            if (dataInicioCota != DateTime.MaxValue)
            {
                dataTable.Rows[0][COLUMNNAME_DATA_INICIO_COTA] = dataInicioCota;
                dataTable.Rows[0][COLUMNNAME_COTA_INICIAL] = cotaInicial;
            }
        }

        return dataTable;
    }

    [WebMethod]
    public DataTable BuscaFundos(string cnpj, string nomeFantasia, string codFundo, string selectColumns)
    {
        const string WHERE_OPERAND = " AND ";

        SqlConnection conn = this.CreateSqlConnection();

        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter adapter = new SqlDataAdapter();
        DataTable dataTable = new DataTable();
        dataTable.TableName = "Fundo";

        string selectColumnsQuery = string.IsNullOrEmpty(selectColumns) ? "*" : selectColumns;

        Filters filters = new Filters();
        filters.Add("cnpj", cnpj);
        filters.Add("fantasia", nomeFantasia);
        filters.Add("codfundo", codFundo);

        if (filters.Count == 0)
        {
            return null;
        }

        string filterQuery = String.Join(WHERE_OPERAND, filters.ToArray());

        string query = "select {0} from fundos where {1}";
        cmd.CommandText = String.Format(query, selectColumnsQuery, filterQuery);
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.Connection = conn;

        adapter.SelectCommand = cmd;
        adapter.Fill(dataTable);
        return dataTable;
    }

    private SqlConnection CreateSqlConnection()
    {
        string connectionString = ConfigurationManager.ConnectionStrings["SIAnbidConnectionString"].ConnectionString;
        SqlConnection conn = new SqlConnection(connectionString);
        conn.Open();
        return conn;
    }

}
