﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.BMF.Properties;

namespace Financial.BMF.Enums
{
    public enum TipoCotacaoBMF
    {
        Medio = 1,
        Fechamento = 2
    }
    
    public static class TipoOperacaoBMF
    {
        public const string Compra = "C";
        public const string Venda = "V";
        public const string CompraDaytrade = "CD";
        public const string VendaDaytrade = "VD";
        public const string Deposito = "DE";
        public const string Retirada = "RE";

        /// <summary>
        ///     Valores possiveis para o Enum
        /// </summary>
        /// <returns></returns>
        public static List<string> Values()
        {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Compra);
            valoresPossiveis.Add(Venda);
            valoresPossiveis.Add(CompraDaytrade);
            valoresPossiveis.Add(VendaDaytrade);
            valoresPossiveis.Add(Deposito);
            valoresPossiveis.Add(Retirada);
            return valoresPossiveis;
        }

        /// <summary>
        ///    Classe Usada para Retornar os textos das Constantes
        /// </summary>
        public static class str {
            /// <summary>
            /// Retorna os tipoOperacões como string
            /// </summary>
            /// <exception>throws ArgumentException se não existir um enum com o valor fornecido</exception> 
            /// <returns></returns>            
            //HACK: Verificar possibilidade de construir através de refletion sem usar switch case
            public static string RetornaTexto(string tipoOperacaoBMF) {
                string tipoOperacao = "";
                switch (tipoOperacaoBMF) {
                    case TipoOperacaoBMF.Compra:
                        tipoOperacao = Resources._TipoOperacaoBMF_Compra;
                        break;
                    case TipoOperacaoBMF.Venda:
                        tipoOperacao = Resources._TipoOperacaoBMF_Venda;
                        break;
                    case TipoOperacaoBMF.CompraDaytrade:
                        tipoOperacao = Resources._TipoOperacaoBMF_CompraDaytrade;
                        break;
                    case TipoOperacaoBMF.VendaDaytrade:
                        tipoOperacao = Resources._TipoOperacaoBMF_VendaDaytrade;
                        break;
                    case TipoOperacaoBMF.Deposito:
                        tipoOperacao = Resources._TipoOperacaoBMF_Deposito;
                        break;
                    case TipoOperacaoBMF.Retirada:
                        tipoOperacao = Resources._TipoOperacaoBMF_Retirada;
                        break;
                    default:
                        throw new ArgumentException("TipoOperacaoBMF não possue Constante com valor " + tipoOperacaoBMF);
                }
                return tipoOperacao;
            }
        }
    }

    /// <summary>
    /// Enum dos tipos de origem, conforme abaixo:
    /// Primaria - Operacao Manual ou integrada
    /// ExercicioOpcaoCompra - Oriunda de Exercicio de Compra de Opcoes
    /// ExercicioOpcaoVenda - Oriunda de Exercicio de Venda de Opcoes
    /// VencimentoOpcao - Oriunda de Vencimento de Opcoes
    /// TransferenciaCorretora - Oriunda de Transferencia de Corretoras
    /// </summary>
    public static class OrigemOperacaoBMF
    {
        public const int Primaria = 1;
        public const int ExercicioOpcaoCompra = 2;
        public const int ExercicioOpcaoVenda = 3;
        public const int VencimentoOpcao = 4;
        public const int TransferenciaCorretora = 5;
        public const int VencimentoFuturo = 6;
        public const int AjusteFuturo = 7;

        /// <summary>
        /// Valores possiveis para o Enum
        /// </summary>
        /// <returns></returns>
        public static List<int> Values()
        {
            List<int> valoresPossiveis = new List<int>();
            valoresPossiveis.Add(Primaria);
            valoresPossiveis.Add(ExercicioOpcaoCompra);
            valoresPossiveis.Add(ExercicioOpcaoVenda);
            valoresPossiveis.Add(VencimentoOpcao);
            valoresPossiveis.Add(TransferenciaCorretora);
            valoresPossiveis.Add(VencimentoFuturo);
            valoresPossiveis.Add(AjusteFuturo);

            return valoresPossiveis;
        }

        /// <summary>
        ///    Classe Usada para Retornar os textos das Constantes
        /// </summary>
        public static class str {
            /// <summary>
            /// Retorna  como string
            /// </summary>
            /// <returns></returns>
            public static string RetornaTexto(int origemOperacaoBMF) {
                string origemOperacao = "";
                switch (origemOperacaoBMF) {
                    case OrigemOperacaoBMF.Primaria:
                        origemOperacao = "Primária";
                        break;
                    case OrigemOperacaoBMF.ExercicioOpcaoCompra:
                        origemOperacao = "Exercício Opção Compra";
                        break;
                    case OrigemOperacaoBMF.ExercicioOpcaoVenda:
                        origemOperacao = "Exercício Opção Venda";
                        break;
                    case OrigemOperacaoBMF.VencimentoOpcao:
                        origemOperacao = "Vencimento Opção";
                        break;
                    case OrigemOperacaoBMF.TransferenciaCorretora:
                        origemOperacao = "Transferência Corretora";
                        break;
                    case OrigemOperacaoBMF.VencimentoFuturo:
                        origemOperacao = "Vencimento Futuro";
                        break;
                }
                return origemOperacao;
            }
        }
    }

    public enum FonteOperacaoBMF
    {
        Interno = 1,
        Manual = 2,
        Sinacor = 3,
        ArquivoRCOMIESP = 4,
        NotaPDFSinacor = 5,
        OrdemBMF = 100,
        Gerencial = 105
    }

    public static class TipoSerieOpcaoBMF
    {
        public const string Compra = "C";
        public const string Venda = "V";
    }

    public static class TipoMercadoBMF
    {
        public const int Disponivel = 1;
        public const int Futuro = 2;
        public const int OpcaoDisponivel = 3;
        public const int OpcaoFuturo = 4;
        public const int Termo = 5;        

        /// <summary>
        ///     Valores possiveis para o Enum
        /// </summary>
        /// <returns></returns>
        public static List<int> Values() {
            List<int> valoresPossiveis = new List<int>();
            valoresPossiveis.Add(Disponivel);
            valoresPossiveis.Add(Futuro);
            valoresPossiveis.Add(OpcaoDisponivel);
            valoresPossiveis.Add(OpcaoFuturo);
            valoresPossiveis.Add(Termo);
            
            return valoresPossiveis;
        }

        /// <summary>
        ///    Classe Usada para Retornar os textos das Constantes
        /// </summary>
        public static class str {
            /// <summary>
            /// Mapeamento do valor do enum para o texto
            /// </summary>
            /// <param name="tipoMercadoBMF"></param>
            /// <exception>throws ArgumentException se não existir um enum com o valor fornecido</exception> 
            /// <returns></returns>            
            //HACK: Verificar possibilidade de construir através de refletion sem usar switch case
            public static string RetornaTexto(int tipoMercadoBMF) {
                string tipoMercado = "";
                switch (tipoMercadoBMF) {
                    case TipoMercadoBMF.Disponivel:
                        tipoMercado = Resources._TipoMercado_Disponivel;
                        break;
                    case TipoMercadoBMF.Futuro:
                        tipoMercado = Resources._TipoMercado_Futuro;
                        break;
                    case TipoMercadoBMF.OpcaoDisponivel:
                        tipoMercado = Resources._TipoMercado_OpcaoDisponivel;
                        break;
                    case TipoMercadoBMF.OpcaoFuturo:
                        tipoMercado = Resources._TipoMercado_OpcaoFuturo;
                        break;
                    case TipoMercadoBMF.Termo:
                        tipoMercado = Resources._TipoMercado_Termo;
                        break;
                    default:
                        throw new ArgumentException("TipoMercadoBMF não possue Constante com valor " + tipoMercado);
                }
                return tipoMercado;
            }
        }
    }
        
    public static class TipoOrdemBMF
    {
        public const string Compra = "C";
        public const string Venda = "V";        
    }
        
    public enum OrigemOrdemBMF
    {
        Primaria = 1,
        ExercicioOpcaoCompra = 2,
        ExercicioOpcaoVenda = 2
    }
    
    public enum FonteOrdemBMF
    {
        Interno = 1,
        Manual = 2,
        Sinacor = 3,
        ArquivoRCOMIESP = 4        
    }
        
    /// <summary>
    /// Indica se a corretagem é calculada por % da TOB(tabela) ou por R$ fixo por operação.
    /// </summary>
    public enum TipoCalculoCorretagem
    {
        TOB = 1,
        ValorFixo = 2        
    }

    public static class TipoEstrategia
    {
        public static class FRC
        {
            #region Constantes
            public const int DDI_CURTO = 10;
            public const int DDI_LONGO = 11;
            #endregion
        }

        public static class VCA
        {
            #region Constantes
            public const int DOL_FUT = 20;
            public const int DLA_OPC = 21;
            #endregion
        }

        public static class VID
        {
            #region Constantes
            public const int DI1_FUT = 30;
            public const int IDI_OPC = 31;
            #endregion
        }

        public static class VOI
        {
            #region Constantes
            public const int IND_FUT = 40;
            public const int IND_OPC = 41;
            #endregion
        }

        public static class VTC
        {
            #region Constantes
            public const int DOL_FUT = 50;
            public const int DOL_OPC = 51;
            #endregion
        }

        public static class VTF
        {
            #region Constantes
            public const int DI1_FUT_LONGO = 60;
            public const int DI1_FUT_CURTO = 61;
            public const int DI1_OPC = 62;
            #endregion
        }

        public static class DR1 //Rolagem de dolar
        {
            #region Constantes
            public const int DOL_CURTO = 70;
            public const int DOL_LONGO = 71;
            #endregion
        }

        public static class IR1 //Rolagem de indice
        {
            #region Constantes
            public const int IND_CURTO = 80;
            public const int IND_LONGO = 81;
            #endregion
        }

        public static class BR1 //Rolagem de boi gordo
        {
            #region Constantes
            public const int BOI_CURTO = 90;
            public const int BOI_LONGO = 91;
            #endregion
        }

        public static class CR1 //Rolagem de café arabica
        {
            #region Constantes
            public const int CRF_CURTO = 100;
            public const int CRF_LONGO = 101;
            #endregion
        }
                
        public const int OperacaoSimples = 0;
    }

    public enum TipoCalculoTaxasBMF {
        Parametros = 1,
        Direto = 2,
        NaoCalcula = 3,
        NaoCalculaPerm = 4,
        ApenasPerm = 5
    }

    public enum TipoPlataformaOperacional
    {
        Normal = 1,
        DMA = 2
    }

    public enum StatusOrdemBMF
    {
        Digitado = 1,
        Aprovado = 2,
        Processado = 3,
        Cancelado = 4
    }

}
