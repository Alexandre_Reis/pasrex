﻿using System;

namespace Financial.BMF.Exceptions
{
    /// <summary>
    /// Classe base de Exceção do componente de BMF
    /// </summary>
    public class BMFException : Exception
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public BMFException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public BMFException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public BMFException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    /// <summary>
    /// Exceção de cotacaoBMF
    /// </summary>
    public class CotacaoBMFNaoCadastradoException : BMFException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public CotacaoBMFNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public CotacaoBMFNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de tabelaCustosBMF
    /// </summary>
    public class TabelaCustosBMFNaoCadastradoException : BMFException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public TabelaCustosBMFNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public TabelaCustosBMFNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de ClienteBMF
    /// </summary>
    public class ClienteBMFNaoCadastradoException : BMFException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ClienteBMFNaoCadastradoException () { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ClienteBMFNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de AtivoBMF
    /// </summary>
    public class AtivoBMFNaoCadastradoException : BMFException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public AtivoBMFNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public AtivoBMFNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de ClasseBMF
    /// </summary>
    public class ClasseBMFNaoCadastradaException : BMFException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ClasseBMFNaoCadastradaException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ClasseBMFNaoCadastradaException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de DataVencimentoInvalida
    /// </summary>
    public class DataVencimentoInvalidaException : BMFException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public DataVencimentoInvalidaException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public DataVencimentoInvalidaException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de TabelaCalculoBMF
    /// </summary>
    public class TabelaCalculoBMFNaoCadastradoException : BMFException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public TabelaCalculoBMFNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public TabelaCalculoBMFNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de tabelaVencimentosBMF
    /// </summary>
    public class TabelaVencimentosBMFNaoCadastradoException : BMFException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public TabelaVencimentosBMFNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public TabelaVencimentosBMFNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de OperacaoBMF
    /// </summary>
    public class OperacaoBMFNaoLancadaException : BMFException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public OperacaoBMFNaoLancadaException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public OperacaoBMFNaoLancadaException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de DeltaNaoCadastrado
    /// </summary>
    public class DeltaNaoCadastradoException : BMFException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public DeltaNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public DeltaNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de PerfilCorretagemBMF
    /// </summary>
    public class TabelaCorretagemBMFNaoCadastradoException : BMFException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public TabelaCorretagemBMFNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public TabelaCorretagemBMFNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de TabelaTOB
    /// </summary>
    public class TabelaTOBNaoCadastradoException : BMFException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public TabelaTOBNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public TabelaTOBNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de CodigoClienteBMFInvalido
    /// </summary>
    public class CodigoClienteBMFInvalido : BMFException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public CodigoClienteBMFInvalido() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public CodigoClienteBMFInvalido(string mensagem) : base(mensagem) { }
    }
    
}


