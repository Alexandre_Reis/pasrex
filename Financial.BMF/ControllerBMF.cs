﻿using log4net;
using Financial.BMF.Enums;
using System.Collections.Generic;
using System;
using Financial.Util;
using Financial.Common.Enums;
using System.Text;
using System.Data.SqlClient;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.BMF.Properties;
using Financial.Investidor.Enums;
using Financial.Investidor;
using Financial.Investidor.Controller;
using Financial.Fundo.Enums;
using Financial.Fundo;
using Financial.Common;

namespace Financial.BMF.Controller
{

    public class ControllerBMF
    {
        public ControllerBMF()
        {

        }

        /// <summary>
        /// Reprocessa as posições, por tipo = abertura ou tipo = fechamento.
        /// Se fechamento, busca as posições de fechamento do dia, pela PosicaoHistorico.
        /// Se abertura, busca as posições de abertura do dia PosicaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoReprocessamento">Verificar se é necessario</param>
        /// thows ArgumentException se tipoReprocessamento for diferente dos valores do 
        /// Enum TipoReprocessamentoBMF
        public void ReprocessaPosicao(int idCliente, DateTime data, TipoReprocessamento tipoReprocessamento)
        {
            #region Delete PosicaoBMF
            PosicaoBMFCollection posicaoBMFCollectionDeletar = new PosicaoBMFCollection();
            try
            {
                posicaoBMFCollectionDeletar.DeletaPosicaoBMF(idCliente);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete PosicaoBMF");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            //Para SqlServer precisa dar um tratamento especial para a PK Identity
            // Nome da Configuração Default SQL
            if (esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider")
            {
                using (esTransactionScope scope = new esTransactionScope())
                {
                    esUtility u = new esUtility();
                    string sql = " set identity_insert PosicaoBMF on ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                    {
                        #region Insert PosicaoBMF a partir de PosicaoBMFHistorico
                        PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
                        try
                        {
                            posicaoBMFCollection.InserePosicaoBMFHistorico(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoBMF");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }
                    else
                    {
                        #region Insert PosicaoBMF a partir de PosicaoBMFAbertura
                        PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
                        try
                        {
                            posicaoBMFCollection.InserePosicaoBMFAbertura(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoBMF");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }

                    u = new esUtility();
                    sql = " set identity_insert PosicaoBMF off ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    scope.Complete();
                }
            }
            else
            {
                //TODO ESCREVER NA UNHA O SQL, POIS O ORACLE NÃO PERMITE DESLIGAR A SEQUENCE

                if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                {
                    #region Insert PosicaoBMF a partir de PosicaoBMFHistorico
                    PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
                    try
                    {
                        posicaoBMFCollection.InserePosicaoBMFHistorico(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoBMF");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion
                }
                else
                {
                    #region Insert PosicaoBMF a partir de PosicaoBMFHistorico
                    PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
                    try
                    {
                        posicaoBMFCollection.InserePosicaoBMFAbertura(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoBMF");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion
                }
            }

            this.DeletaPosicoesAbertura(idCliente, data);

            this.DeletaPosicoesHistorico(idCliente, data);

        }

        /// <summary>
        /// Carrega as posições de PosicaoBMF para PosicaoBMFAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaAbertura(int idCliente, DateTime data)
        {
            PosicaoBMF posicaoBMF = new PosicaoBMF();
            //

            posicaoBMF.CalculaAjustePosicao(idCliente, data);

            #region LancaCCAjustePosicao
            if (ParametrosConfiguracaoSistema.BMF.TipoVisualizacao == (int)VizualizacaoOperacaoBMF.Analitico)
            {
                posicaoBMF.LancaCCAjustePosicaoAnalitico(idCliente, data);
            }
            #endregion


            #region Ajuste de operação
            OperacaoBMF operacaoBMF = new OperacaoBMF();
            operacaoBMF.CalculaAjusteOperacao(idCliente, data, (byte)ParametrosConfiguracaoSistema.BMF.TipoVisualizacao);
            #endregion

            #region Taxa de permanência
            if (ParametrosConfiguracaoSistema.BMF.TipoCalculoTaxasBMF != (int)TipoCalculoTaxasBMF.NaoCalculaPerm &&
                ParametrosConfiguracaoSistema.BMF.TipoCalculoTaxasBMF != (int)TipoCalculoTaxasBMF.NaoCalcula)
            {
                posicaoBMF = new PosicaoBMF();
                posicaoBMF.CalculaTaxaPermanencia(idCliente, data);
            }
            #endregion


            #region Baixa de contratos vencidos
            posicaoBMF = new PosicaoBMF();
            posicaoBMF.ProcessaVencimento(idCliente, data);
            #endregion

            #region Calcula Prazo Médio 
            this.CalculaPrazoMedioBMF(data, idCliente);
            #endregion

            #region Calcula Rentabilidade
            this.CalculaRentabilidadeBMF(data, idCliente);
            #endregion

            //Carrega a posição atual para Posicao de Abertura
            posicaoBMF.GeraPosicaoAbertura(idCliente, data);


        }

        /// <summary>
        /// Backup de PosicaoAtual para PosicaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaDivulgacao(int idCliente, DateTime data)
        {
            this.DeletaPosicoesHistorico(idCliente, data);

            PosicaoBMF posicaoBMF = new PosicaoBMF();
            posicaoBMF.GeraBackup(idCliente, data);
        }

        /// <summary>
        /// Realiza todos os cálculos financeiros e despesas, atualiza a custódia, gera fluxos de pagamento futuros.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaFechamento(int idCliente, DateTime data, ParametrosProcessamento parametrosProcessamento)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdMoeda);
            campos.Add(cliente.Query.DataImplantacao);
            cliente.LoadByPrimaryKey(campos, idCliente);

            this.ReprocessaPosicao(idCliente, data, TipoReprocessamento.CalculoDiario);

            OrdemBMF ordemBMF = new OrdemBMF();
            OperacaoBMF operacaoBMF = new OperacaoBMF();
            TransferenciaBMF transferenciaBMF = new TransferenciaBMF();
            PosicaoBMF posicaoBMF = new PosicaoBMF();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            if (parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.BMF.SinacorPosicaoInicial) && data == cliente.DataImplantacao.Value)
            {
                posicaoBMF.ImportaPosicoesSinacor(idCliente, data);
                return;
            }

            if (!parametrosProcessamento.IgnoraOperacoes)
            {
                #region ProcessaTransferenciaAgenteCustodia
                transferenciaBMF.ProcessaTransferenciaAgenteCustodia(idCliente, data);
                #endregion

                #region Exercício automático
                /*posicaoBMF = new PosicaoBMF();
            posicaoBMF.GeraExercicioAutomatico(idCliente, data);*/
                #endregion

                #region Integração Sinacor (só roda se integraBMF = true)
                if (parametrosProcessamento.IntegraBMF)
                {
                    operacaoBMF.IntegraOperacoesSinacor(idCliente, data);
                }
                #endregion

                #region Trata operações estruturadas
                ordemBMF.ConverteFRP(idCliente, data);

                ordemBMF = new OrdemBMF();
                ordemBMF.ConverteVTC(idCliente, data);

                ordemBMF = new OrdemBMF();
                ordemBMF.ConverteVCA(idCliente, data);

                ordemBMF = new OrdemBMF();
                ordemBMF.ConverteFRC(idCliente, data);

                ordemBMF = new OrdemBMF();
                ordemBMF.ConverteVID(idCliente, data);

                ordemBMF = new OrdemBMF();
                ordemBMF.ConverteVOI(idCliente, data);

                ordemBMF = new OrdemBMF();
                ordemBMF.ConverteVTF(idCliente, data);
                #endregion

                #region Casamento DayTrade
                ordemBMF = new OrdemBMF();
                ordemBMF.CasaDayTrade(idCliente, data);
                #endregion

                #region CarregaOrdemCasada
                operacaoBMF = new OperacaoBMF();
                operacaoBMF.CarregaOrdemCasada(idCliente, data);
                #endregion

                #region Cálculo de despesas e corretagem
                operacaoBMF.CalculaCorretagemOperacao(idCliente, data);

                //O 1o tipo usa os valores unitários já prontos informados pela BMF
                if (ParametrosConfiguracaoSistema.BMF.TipoCalculoTaxasBMF == (int)TipoCalculoTaxasBMF.Parametros)
                {
                    operacaoBMF = new OperacaoBMF();
                    operacaoBMF.CalculaTaxasOperacaoParametrizadas(idCliente, data);
                }
                if (ParametrosConfiguracaoSistema.BMF.TipoCalculoTaxasBMF == (int)TipoCalculoTaxasBMF.Direto ||
                    ParametrosConfiguracaoSistema.BMF.TipoCalculoTaxasBMF == (int)TipoCalculoTaxasBMF.NaoCalculaPerm)
                {
                    operacaoBMF = new OperacaoBMF();
                    operacaoBMF.CalculaTaxasOperacao(idCliente, data);
                }
                #endregion

                #region CalculaValoresLiquidos
                operacaoBMF = new OperacaoBMF();
                operacaoBMF.CalculaValoresLiquidos(idCliente, data);
                #endregion

                #region Ajuste de posição
                posicaoBMF.ZeraAjusteDiario(idCliente, data);
                posicaoBMF.CalculaAjustePosicao(idCliente, data);
                #endregion

                #region LancaCCAjustePosicao
                if (ParametrosConfiguracaoSistema.BMF.TipoVisualizacao == (int)VizualizacaoOperacaoBMF.Analitico)
                {
                    posicaoBMF.LancaCCAjustePosicaoAnalitico(idCliente, data);
                }
                #endregion

                #region Ajuste de operação
                operacaoBMF = new OperacaoBMF();
                operacaoBMF.CalculaAjusteOperacao(idCliente, data, (byte)ParametrosConfiguracaoSistema.BMF.TipoVisualizacao);
                #endregion

                #region Processa posições (compras/vendas)
                operacaoBMF = new OperacaoBMF();
                operacaoBMF.ProcessaVenda(idCliente, data);

                operacaoBMF = new OperacaoBMF();
                operacaoBMF.ProcessaCompra(idCliente, data);
                #endregion

                #region Atualiza Ajuste
                operacaoBMF.AtualizaAjuste(idCliente, data);
                #endregion

                #region ProcessaTransferenciaAgenteCustodia
                transferenciaBMF.ProcessaTransferenciaAgenteCustodia(idCliente, data);
                #endregion

                #region LancaCCOperacao
                operacaoBMF = new OperacaoBMF();

                if (ParametrosConfiguracaoSistema.BMF.TipoVisualizacao == (int)VizualizacaoOperacaoBMF.Analitico)
                {
                    operacaoBMF.LancaCCOperacao(idCliente, data, VizualizacaoOperacaoBMF.Analitico);
                }
                else if (ParametrosConfiguracaoSistema.BMF.TipoVisualizacao == (int)VizualizacaoOperacaoBMF.Consolidado)
                {
                    operacaoBMF.LancaCCOperacao(idCliente, data, VizualizacaoOperacaoBMF.Consolidado);
                }
                #endregion

                #region Taxa de permanência
                if (ParametrosConfiguracaoSistema.BMF.TipoCalculoTaxasBMF != (int)TipoCalculoTaxasBMF.NaoCalculaPerm &&
                    ParametrosConfiguracaoSistema.BMF.TipoCalculoTaxasBMF != (int)TipoCalculoTaxasBMF.NaoCalcula)
                {
                    posicaoBMF = new PosicaoBMF();
                    posicaoBMF.CalculaTaxaPermanencia(idCliente, data);
                }
                #endregion

                #region Cálculo de despesas no vencimento
                //posicaoBMF = new PosicaoBMF();
                //posicaoBMF.CalculaTaxasVencimento(idCliente, data);
                #endregion

                #region Baixa de contratos vencidos
                posicaoBMF = new PosicaoBMF();
                posicaoBMF.ProcessaVencimento(idCliente, data);
                #endregion

                #region Calcula Rentabilidade
                this.CalculaRentabilidadeBMF(data, idCliente);
                #endregion

            }

            #region Atualiza valores
            posicaoBMF = new PosicaoBMF();
            posicaoBMF.AtualizaValores(idCliente, data);
            #endregion

            #region Calcula Prazo Médio
            this.CalculaPrazoMedioBMF(data, idCliente);
            #endregion

            #region Calcula Rentabilidade
            this.CalculaRentabilidadeBMF(data, idCliente);
            #endregion
        }

        /// <summary>
        /// Deleta as posicoes de abertura em PosicaoBMF.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaPosicoesAbertura(int idCliente, DateTime data)
        {
            PosicaoBMFAberturaCollection posicaoBMFAberturaCollection = new PosicaoBMFAberturaCollection();
            posicaoBMFAberturaCollection.DeletaPosicaoBMFAberturaDataHistoricoMaior(idCliente, data);
        }

        /// <summary>
        /// Deleta as posicoes de historico em PosicaoBMF.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaPosicoesHistorico(int idCliente, DateTime data)
        {
            PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
            posicaoBMFHistoricoCollection.DeletaPosicaoBMFHistoricoDataHistoricoMaiorIgual(idCliente, data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        public void CalculaPrazoMedioBMF(DateTime data, int idCliente)
        {
            PrazoMedioCollection prazoMedioColl = new PrazoMedioCollection();
            prazoMedioColl.DeletaPrazoMedioMaiorIgual(idCliente, data, TipoAtivoAuxiliar.OperacaoBMF);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);
            string calculaPrazoMedio = string.IsNullOrEmpty(carteira.CalculaPrazoMedio) ? string.Empty : carteira.CalculaPrazoMedio;

            if (!calculaPrazoMedio.Equals("S"))
                return;

            PosicaoBMFQuery posicaoBMFQuery = new PosicaoBMFQuery("posicaoBMF");
            PosicaoBMFCollection posicaoBMFColl = new PosicaoBMFCollection();
            posicaoBMFQuery.Select(posicaoBMFQuery.ValorMercado.Sum(),
                                   posicaoBMFQuery.IdCliente);
            posicaoBMFQuery.Where(posicaoBMFQuery.IdCliente.Equal(idCliente));
            posicaoBMFQuery.GroupBy(posicaoBMFQuery.IdCliente);

            if (posicaoBMFColl.Load(posicaoBMFQuery))
            {
                PrazoMedio prazoMedio = new PrazoMedio();
                prazoMedio.InserePrazoMedio(data, idCliente, "Posição BMF", (int)TipoAtivoAuxiliar.OperacaoBMF, posicaoBMFColl[0].ValorMercado.Value, 1, null);
            }
        }

        /// <summary>
        /// Calcula rentabilidade de Ativos de BMF
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        /// <param name="enumTipoProcessamento"></param>
        public void CalculaRentabilidadeBMF(DateTime data, int idCliente)
        {
            #region Se carteira mãe não calcula rentabilidade ( precisa desenvolver )
            CarteiraMae carteiraMae = new CarteiraMae();
            if (carteiraMae.IsCarteiraMae(idCliente))
                return;
            #endregion

            #region EntitySpaces
            string tableAtivoBMF = "AtivoBMF";
            string tablePosicaoFechamento = "PosicaoFechamento";
            string tablePosicaoAbertura = "PosicaoAbertura";
            string tablePosicaoHistorico = "PosicaoHistorico";
            string tableMoedaAtivo = "MoedaAtivo";
            string fieldQtdeAbertura = "QtdeAbertura";
            string fieldQtdeFechamento = "QtdeFechamento";
            string fieldValorFechamentoLiquido = "ValorFechamentoLiquido";
            string fieldValorAberturaLiquido = "ValorAberturaLiquido";
            string fieldDescricaoMoedaAtivo = "DescricaoMoedaAtivo";
            string fieldIdMoedaAtivo = "IdMoedaAtivo";
            string fieldValorFechamentoBruto = "ValorFechamentoBruto";
            string fieldValorAberturaBruto = "ValorAberturaBruto";
            #endregion

            #region Objetos
            Dictionary<int, string> dicMoedasAtivo = new Dictionary<int, string>();
            Dictionary<string, decimal> dicCotacao = new Dictionary<string, decimal>();
            Dictionary<string, Rentabilidade.RentabilidadeConversao> dicRentConversao = new Dictionary<string, Rentabilidade.RentabilidadeConversao>();
            List<OperacaoBMF> lstOperacoesCredito = new List<OperacaoBMF>();
            List<OperacaoBMF> lstOperacoesDebito = new List<OperacaoBMF>();
            List<Rentabilidade> lstRentabilidadeAnterior = new List<Rentabilidade>();
            List<int> lstEventosRendas = new List<int>();

            //Querys
            PosicaoBMFQuery posicaoBMFQuery = new PosicaoBMFQuery(tablePosicaoFechamento);
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery(tableAtivoBMF);
            PosicaoBMFAberturaQuery posicaoBMFAberturaQuery = new PosicaoBMFAberturaQuery(tablePosicaoAbertura);
            PosicaoBMFHistoricoQuery posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery(tablePosicaoHistorico);
            OperacaoBMFQuery operacaoBMFDebitoQuery = new OperacaoBMFQuery();
            OperacaoBMFQuery operacaoBMFCreditoQuery = new OperacaoBMFQuery();
            MoedaQuery moedaQuery = new MoedaQuery(tableMoedaAtivo);

            //Collection
            PosicaoBMFCollection posicaoBMFColl = new PosicaoBMFCollection();
            RentabilidadeCollection rentabilidadeColl = new RentabilidadeCollection();
            OperacaoBMFCollection operacaoBMFDebitoColl = new OperacaoBMFCollection();
            OperacaoBMFCollection operacaoBMFCreditoColl = new OperacaoBMFCollection();

            //Comuns        
            ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            Cliente cliente = new Cliente();
            Moeda moeda = new Moeda();
            OperacaoBMF operacaoAux;
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            #endregion

            #region Variaveis
            decimal valorFechamentoLiquido = 0;
            decimal valorAberturaLiquido = 0;
            decimal valorFechamentoBruto = 0;
            decimal valorAberturaBruto = 0;
            decimal valorEntrada = 0;
            decimal valorSaidaLiquido = 0;
            decimal valorSaidaBruto = 0;
            decimal valorRenda = 0;
            decimal valorRentabilidadeLiquido = 0;
            decimal valorRentabilidadeBrutaDiaria = 0;
            decimal valorRentabilidadeBrutaAcumulada = 0;
            decimal qtdeEntrada = 0;
            decimal qtdeSaida = 0;
            decimal qtdeAbertura = 0;
            decimal qtdeFechamento = 0;
            decimal cambio = 0;
            string descricaoCompleta = string.Empty;
            string descricaoMoedaCarteira = string.Empty;
            string descricaoMoedaAtivo = string.Empty;
            string keyConversao = string.Empty;
            short idMoedaAtivo = 0;
            short idMoedaCarteira = 0;
            byte tipoConversao;
            string serie;
            string cdAtivoBMF;
            #endregion

            #region Deleta Rentabilidade
            rentabilidadeColl.DeletaRentabilidadeMaiorIgual(idCliente, data, TipoAtivoAuxiliar.OperacaoBMF);
            #endregion

            #region Listas de tipos de operações
            List<string> lstTipoOperacaoCredito = new List<string>();
            lstTipoOperacaoCredito.Add(TipoOperacaoBMF.Compra);
            lstTipoOperacaoCredito.Add(TipoOperacaoBMF.CompraDaytrade);
            lstTipoOperacaoCredito.Add(TipoOperacaoBMF.Retirada);

            List<string> lstTipoOperacaoDebito = new List<string>();
            lstTipoOperacaoDebito.Add(TipoOperacaoBMF.Venda);
            lstTipoOperacaoDebito.Add(TipoOperacaoBMF.VendaDaytrade);
            lstTipoOperacaoDebito.Add(TipoOperacaoBMF.Deposito);
            #endregion

            #region Carrega as Posições
            posicaoBMFQuery.Select(posicaoBMFQuery.CdAtivoBMF,
                                    posicaoBMFQuery.Serie,
                                    posicaoBMFQuery.DataVencimento,
                                    posicaoBMFQuery.ValorMercado.Sum().As(fieldValorFechamentoBruto),
                                    posicaoBMFQuery.ValorMercado.Sum().As(fieldValorFechamentoLiquido),
                                    posicaoBMFQuery.Quantidade.Sum().As(fieldQtdeFechamento),
                                    posicaoBMFAberturaQuery.ValorMercado.Sum().Coalesce("0").As(fieldValorAberturaBruto),
                                    posicaoBMFAberturaQuery.ValorMercado.Sum().As(fieldValorAberturaLiquido),
                                    posicaoBMFAberturaQuery.Quantidade.Sum().Coalesce("0").As(fieldQtdeAbertura),
                                    moedaQuery.Nome.As(fieldDescricaoMoedaAtivo),
                                    ativoBMFQuery.IdMoeda.Coalesce("0").As(fieldIdMoedaAtivo));
            posicaoBMFQuery.InnerJoin(ativoBMFQuery).On(posicaoBMFQuery.CdAtivoBMF.Equal(ativoBMFQuery.CdAtivoBMF)
                                                       & posicaoBMFQuery.Serie.Equal(ativoBMFQuery.Serie));
            posicaoBMFQuery.LeftJoin(moedaQuery).On(moedaQuery.IdMoeda.Equal(ativoBMFQuery.IdMoeda));
            posicaoBMFQuery.LeftJoin(posicaoBMFAberturaQuery).On(posicaoBMFAberturaQuery.CdAtivoBMF.Equal(posicaoBMFQuery.CdAtivoBMF)
                                                                   & posicaoBMFAberturaQuery.Serie.Equal(posicaoBMFQuery.Serie)
                                                                   & posicaoBMFAberturaQuery.IdAgente.Equal(posicaoBMFQuery.IdAgente)
                                                                   & posicaoBMFAberturaQuery.IdCliente.Equal(posicaoBMFQuery.IdCliente)
                                                                   & posicaoBMFAberturaQuery.DataHistorico.Equal("'" + data.ToString("yyyyMMdd") + "'"));
            posicaoBMFQuery.Where(posicaoBMFQuery.IdCliente.Equal(idCliente));
            posicaoBMFQuery.GroupBy(posicaoBMFQuery.CdAtivoBMF,
                                    posicaoBMFQuery.Serie,
                                    posicaoBMFQuery.DataVencimento,
                                    posicaoBMFQuery.IdCliente,
                                    moedaQuery.Nome,
                                    ativoBMFQuery.IdMoeda.Coalesce("0"));

            if (!posicaoBMFColl.Load(posicaoBMFQuery))
                return;
            #endregion

            #region Carrega as Operações que movimentaram a Operação na data de processamento (Crédito)
            operacaoBMFCreditoQuery.Select(operacaoBMFCreditoQuery.CdAtivoBMF,
                                           operacaoBMFCreditoQuery.Serie,
                                           operacaoBMFCreditoQuery.Valor.Sum(),
                                           operacaoBMFCreditoQuery.Quantidade.Sum());
            operacaoBMFCreditoQuery.Where(operacaoBMFCreditoQuery.IdCliente.Equal(idCliente)
                                          & operacaoBMFCreditoQuery.Data.Equal(data)
                                          & operacaoBMFCreditoQuery.TipoOperacao.In(TipoOperacaoBMF.Compra, TipoOperacaoBMF.CompraDaytrade, TipoOperacaoBMF.Deposito));
            operacaoBMFCreditoQuery.GroupBy(operacaoBMFCreditoQuery.CdAtivoBMF,
                                            operacaoBMFCreditoQuery.Serie);
            operacaoBMFCreditoQuery.OrderBy(operacaoBMFCreditoQuery.CdAtivoBMF.Ascending,
                                            operacaoBMFCreditoQuery.Serie.Ascending);

            if (operacaoBMFCreditoColl.Load(operacaoBMFCreditoQuery))
                lstOperacoesCredito = (List<OperacaoBMF>)operacaoBMFCreditoColl;
            #endregion

            #region Carrega as Operações que movimentaram a Operação na data de processamento (Débito)
            operacaoBMFDebitoQuery.Select(operacaoBMFDebitoQuery.CdAtivoBMF,
                                           operacaoBMFDebitoQuery.Serie,
                                           operacaoBMFDebitoQuery.Valor.Sum(),
                                           operacaoBMFDebitoQuery.Quantidade.Sum(),
                                           operacaoBMFDebitoQuery.ValorLiquido.Sum());
            operacaoBMFDebitoQuery.Where(operacaoBMFDebitoQuery.IdCliente.Equal(idCliente)
                                         & operacaoBMFDebitoQuery.Data.Equal(data)
                                         & operacaoBMFDebitoQuery.TipoOperacao.In(TipoOperacaoBMF.Venda, TipoOperacaoBMF.VendaDaytrade, TipoOperacaoBMF.Retirada));
            operacaoBMFDebitoQuery.GroupBy(operacaoBMFDebitoQuery.CdAtivoBMF,
                                            operacaoBMFDebitoQuery.Serie);
            operacaoBMFDebitoQuery.OrderBy(operacaoBMFDebitoQuery.CdAtivoBMF.Ascending,
                                            operacaoBMFDebitoQuery.Serie.Ascending);

            if (operacaoBMFDebitoColl.Load(operacaoBMFDebitoQuery))
                lstOperacoesDebito = (List<OperacaoBMF>)operacaoBMFDebitoColl;
            #endregion

            #region Carrega Moeda e Carteira
            cliente.LoadByPrimaryKey(idCliente);

            if (moeda.LoadByPrimaryKey(cliente.IdMoeda.Value))
            {
                descricaoMoedaCarteira = moeda.Nome;
                idMoedaCarteira = moeda.IdMoeda.Value;
            }
            #endregion

            #region Carrega Rendas dos Ativos

            #endregion

            #region Carrega Rentabilidade do dia anterior
            RentabilidadeCollection rentabilidadeAntCollection = new RentabilidadeCollection();
            rentabilidadeAntCollection.Query.Where(rentabilidadeAntCollection.Query.IdCliente.Equal(idCliente) &
                                                   rentabilidadeAntCollection.Query.TipoAtivo.In((int)TipoAtivoAuxiliar.OperacaoBMF) &
                                                   rentabilidadeAntCollection.Query.Data.Equal(dataAnterior));

            if (rentabilidadeAntCollection.Query.Load())
                lstRentabilidadeAnterior = (List<Rentabilidade>)rentabilidadeAntCollection;
            #endregion

            foreach (PosicaoBMF posicaoBMF in posicaoBMFColl)
            {
                cdAtivoBMF = posicaoBMF.CdAtivoBMF;
                serie = posicaoBMF.Serie;
                idMoedaAtivo = Convert.ToInt16(posicaoBMF.GetColumn(fieldIdMoedaAtivo));
                descricaoMoedaAtivo = posicaoBMF.GetColumn(fieldDescricaoMoedaAtivo).ToString();
                valorFechamentoLiquido = 0;
                valorAberturaLiquido = 0;
                valorEntrada = 0;
                valorSaidaLiquido = 0;
                valorRenda = 0;
                valorRentabilidadeLiquido = 0;
                qtdeEntrada = 0;
                qtdeSaida = 0;
                qtdeAbertura = 0;
                qtdeFechamento = 0;

                #region Operações Entrada
                operacaoAux = new OperacaoBMF();
                operacaoAux = lstOperacoesCredito.Find(delegate(OperacaoBMF x) { return x.CdAtivoBMF.Equals(cdAtivoBMF) && x.Serie.Equals(serie); });
                if (operacaoAux != null && !String.IsNullOrEmpty(operacaoAux.Serie))
                {
                    valorEntrada = Convert.ToDecimal(operacaoAux.Valor.Value);
                    qtdeEntrada = Convert.ToDecimal(operacaoAux.Quantidade.Value);
                }
                else
                {
                    valorEntrada = 0;
                    qtdeEntrada = 0;
                }
                #endregion

                #region Operações Saida
                operacaoAux = new OperacaoBMF();
                operacaoAux = lstOperacoesDebito.Find(delegate(OperacaoBMF x) { return x.CdAtivoBMF.Equals(cdAtivoBMF) && x.Serie.Equals(serie); });
                if (operacaoAux != null && !String.IsNullOrEmpty(operacaoAux.Serie))
                {
                    valorSaidaLiquido = Convert.ToDecimal(operacaoAux.ValorLiquido.Value);
                    valorSaidaBruto = Convert.ToDecimal(operacaoAux.Valor.Value);
                    qtdeSaida = Convert.ToDecimal(operacaoAux.Quantidade.Value);
                }
                else
                {
                    valorSaidaLiquido = 0;
                    qtdeSaida = 0;
                }
                #endregion

                #region Rendas

                #endregion

                if (!Decimal.TryParse(posicaoBMF.GetColumn(fieldValorAberturaBruto).ToString(), out valorAberturaBruto))
                    valorAberturaBruto = 0;

                if (!Decimal.TryParse(posicaoBMF.GetColumn(fieldValorFechamentoBruto).ToString(), out valorFechamentoBruto))
                    valorFechamentoBruto = 0;

                if (!Decimal.TryParse(posicaoBMF.GetColumn(fieldValorFechamentoLiquido).ToString(), out valorFechamentoLiquido))
                    valorFechamentoLiquido = 0;

                if (!Decimal.TryParse(posicaoBMF.GetColumn(fieldValorAberturaLiquido).ToString(), out valorAberturaLiquido))
                    valorAberturaLiquido = 0;

                if (!Decimal.TryParse(posicaoBMF.GetColumn(fieldQtdeAbertura).ToString(), out qtdeAbertura))
                    qtdeAbertura = 0;

                if (!Decimal.TryParse(posicaoBMF.GetColumn(fieldQtdeFechamento).ToString(), out qtdeFechamento))
                    qtdeFechamento = 0;

                #region Rentabilidade Bruta
                try
                {
                    valorRentabilidadeBrutaDiaria = ((valorFechamentoBruto + valorSaidaBruto) / (valorAberturaBruto + valorEntrada - valorRenda)) - 1;
                }
                catch (Exception ex)
                {
                    valorRentabilidadeBrutaDiaria = 0;
                }
                #endregion

                #region Rentabilidade Liquida
                try
                {
                    valorRentabilidadeLiquido = ((valorFechamentoLiquido + valorSaidaLiquido) / (valorAberturaLiquido + valorEntrada - valorRenda)) - 1;
                }
                catch (Exception ex)
                {
                    valorRentabilidadeLiquido = 0;
                }
                #endregion

                #region Rentabilidade Acumulada
                valorRentabilidadeBrutaAcumulada = valorRentabilidadeBrutaDiaria;
                Rentabilidade rentabilidadeAux = new Rentabilidade();
                rentabilidadeAux = lstRentabilidadeAnterior.Find(delegate(Rentabilidade x) { return x.CdAtivoBMF.Equals(posicaoBMF.CdAtivoBMF) && x.SerieBMF.Equals(posicaoBMF.Serie); });
                if (rentabilidadeAux != null && rentabilidadeAux.IdRentabilidade.Value > 0)
                {
                    if (rentabilidadeAux.RentabilidadeBrutaAcumMoedaPortfolio.GetValueOrDefault(0) > 0)
                        valorRentabilidadeBrutaAcumulada = ((1 + rentabilidadeAux.RentabilidadeBrutaAcumMoedaPortfolio.Value) / 100) * ((1 + valorRentabilidadeBrutaDiaria) / 100);
                }
                #endregion

                descricaoCompleta = posicaoBMF.CdAtivoBMF + " - " + posicaoBMF.Serie + (posicaoBMF.DataVencimento.HasValue ? " - Vencto: " + String.Format("{0:dd/MM/yyyy}", posicaoBMF.DataVencimento.Value) : "");

                #region Popula objeto
                Rentabilidade rentabilidade = rentabilidadeColl.AddNew();
                rentabilidade.CodigoAtivo = descricaoCompleta;
                rentabilidade.TipoAtivo = (int)TipoAtivoAuxiliar.OperacaoBMF;
                rentabilidade.Data = data;
                rentabilidade.IdCliente = idCliente;
                rentabilidade.CdAtivoBMF = cdAtivoBMF;
                rentabilidade.SerieBMF = serie;
                rentabilidade.QuantidadeFinalAtivo = qtdeFechamento;
                rentabilidade.QuantidadeInicialAtivo = qtdeAbertura;
                rentabilidade.QuantidadeTotalEntradaAtivo = qtdeEntrada;
                rentabilidade.QuantidadeTotalSaidaAtivo = qtdeSaida;

                //Ativo
                rentabilidade.CodigoMoedaAtivo = descricaoMoedaAtivo;
                rentabilidade.PatrimonioFinalMoedaPortfolio = rentabilidade.PatrimonioFinalMoedaAtivo = valorFechamentoLiquido;
                rentabilidade.PatrimonioInicialMoedaPortfolio = rentabilidade.PatrimonioInicialMoedaAtivo = valorAberturaLiquido;
                rentabilidade.RentabilidadeMoedaPortfolio = rentabilidade.RentabilidadeMoedaAtivo = valorRentabilidadeLiquido;
                rentabilidade.ValorFinanceiroEntradaMoedaPortfolio = rentabilidade.ValorFinanceiroEntradaMoedaAtivo = valorEntrada;
                rentabilidade.ValorFinanceiroSaidaMoedaPortfolio = rentabilidade.ValorFinanceiroSaidaMoedaAtivo = valorSaidaLiquido;
                rentabilidade.ValorFinanceiroRendasMoedaPortfolio = rentabilidade.ValorFinanceiroRendasMoedaAtivo = valorRenda;
                rentabilidade.RentabilidadeBrutaDiariaMoedaAtivo = rentabilidade.RentabilidadeBrutaDiariaMoedaPortfolio = valorRentabilidadeBrutaDiaria;
                rentabilidade.RentabilidadeBrutaAcumMoedaAtivo = rentabilidade.RentabilidadeBrutaAcumMoedaPortfolio = valorRentabilidadeBrutaAcumulada;
                rentabilidade.RentabilidadeGrossUpDiariaMoedaAtivo = rentabilidade.RentabilidadeGrossUpDiariaMoedaPortfolio = 0;
                rentabilidade.RentabilidadeGrossUpAcumMoedaAtivo = rentabilidade.RentabilidadeGrossUpAcumMoedaPortfolio = 0;
                rentabilidade.PatrimonioFinalBrutoMoedaAtivo = rentabilidade.PatrimonioFinalBrutoMoedaPortfolio = valorFechamentoBruto;
                rentabilidade.PatrimonioInicialBrutoMoedaPortfolio = rentabilidade.PatrimonioInicialBrutoMoedaAtivo = valorAberturaBruto;
                rentabilidade.PatrimonioFinalGrossUpMoedaAtivo = rentabilidade.PatrimonioFinalGrossUpMoedaPortfolio = valorFechamentoBruto;
                rentabilidade.PatrimonioInicialGrossUpMoedaAtivo = rentabilidade.PatrimonioInicialGrossUpMoedaPortfolio = valorAberturaBruto;

                //Portfolio
                rentabilidade.CodigoMoedaPortfolio = descricaoMoedaCarteira;

                //Converte os valores para a moeda da carteira
                rentabilidade.ConverteValores(idMoedaAtivo, idMoedaCarteira, ref dicRentConversao);
                #endregion

            }

            rentabilidadeColl.Save();
        }
    }
}
