/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2012 19:02:01
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;















using Financial.Enquadra;























		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esClasseBMFCollection : esEntityCollection
	{
		public esClasseBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ClasseBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esClasseBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esClasseBMFQuery);
		}
		#endregion
		
		virtual public ClasseBMF DetachEntity(ClasseBMF entity)
		{
			return base.DetachEntity(entity) as ClasseBMF;
		}
		
		virtual public ClasseBMF AttachEntity(ClasseBMF entity)
		{
			return base.AttachEntity(entity) as ClasseBMF;
		}
		
		virtual public void Combine(ClasseBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ClasseBMF this[int index]
		{
			get
			{
				return base[index] as ClasseBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ClasseBMF);
		}
	}



	[Serializable]
	abstract public class esClasseBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esClasseBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esClasseBMF()
		{

		}

		public esClasseBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String cdAtivoBMF)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivoBMF);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivoBMF);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.String cdAtivoBMF)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esClasseBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.CdAtivoBMF == cdAtivoBMF);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String cdAtivoBMF)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivoBMF);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivoBMF);
		}

		private bool LoadByPrimaryKeyDynamic(System.String cdAtivoBMF)
		{
			esClasseBMFQuery query = this.GetDynamicQuery();
			query.Where(query.CdAtivoBMF == cdAtivoBMF);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String cdAtivoBMF)
		{
			esParameters parms = new esParameters();
			parms.Add("CdAtivoBMF",cdAtivoBMF);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ClasseBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(ClasseBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				base.SetSystemString(ClasseBMFMetadata.ColumnNames.CdAtivoBMF, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esClasseBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
			

			private esClasseBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esClasseBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esClasseBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ClasseBMF : esClasseBMF
	{

				
		#region AcumuladoCorretagemBMFCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ClasseBMF_CorretagemAcumuladaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AcumuladoCorretagemBMFCollection AcumuladoCorretagemBMFCollectionByCdAtivoBMF
		{
			get
			{
				if(this._AcumuladoCorretagemBMFCollectionByCdAtivoBMF == null)
				{
					this._AcumuladoCorretagemBMFCollectionByCdAtivoBMF = new AcumuladoCorretagemBMFCollection();
					this._AcumuladoCorretagemBMFCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AcumuladoCorretagemBMFCollectionByCdAtivoBMF", this._AcumuladoCorretagemBMFCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null)
					{
						this._AcumuladoCorretagemBMFCollectionByCdAtivoBMF.Query.Where(this._AcumuladoCorretagemBMFCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._AcumuladoCorretagemBMFCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._AcumuladoCorretagemBMFCollectionByCdAtivoBMF.fks.Add(AcumuladoCorretagemBMFMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
					}
				}

				return this._AcumuladoCorretagemBMFCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AcumuladoCorretagemBMFCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("AcumuladoCorretagemBMFCollectionByCdAtivoBMF"); 
					this._AcumuladoCorretagemBMFCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private AcumuladoCorretagemBMFCollection _AcumuladoCorretagemBMFCollectionByCdAtivoBMF;
		#endregion

				
		#region AtivoBMFCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ClasseBMF_AtivoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBMFCollection AtivoBMFCollectionByCdAtivoBMF
		{
			get
			{
				if(this._AtivoBMFCollectionByCdAtivoBMF == null)
				{
					this._AtivoBMFCollectionByCdAtivoBMF = new AtivoBMFCollection();
					this._AtivoBMFCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AtivoBMFCollectionByCdAtivoBMF", this._AtivoBMFCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null)
					{
						this._AtivoBMFCollectionByCdAtivoBMF.Query.Where(this._AtivoBMFCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._AtivoBMFCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._AtivoBMFCollectionByCdAtivoBMF.fks.Add(AtivoBMFMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
					}
				}

				return this._AtivoBMFCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AtivoBMFCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("AtivoBMFCollectionByCdAtivoBMF"); 
					this._AtivoBMFCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private AtivoBMFCollection _AtivoBMFCollectionByCdAtivoBMF;
		#endregion

				
		#region CustodiaBMFCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ClasseBMF_CustodiaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public CustodiaBMFCollection CustodiaBMFCollectionByCdAtivoBMF
		{
			get
			{
				if(this._CustodiaBMFCollectionByCdAtivoBMF == null)
				{
					this._CustodiaBMFCollectionByCdAtivoBMF = new CustodiaBMFCollection();
					this._CustodiaBMFCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CustodiaBMFCollectionByCdAtivoBMF", this._CustodiaBMFCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null)
					{
						this._CustodiaBMFCollectionByCdAtivoBMF.Query.Where(this._CustodiaBMFCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._CustodiaBMFCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._CustodiaBMFCollectionByCdAtivoBMF.fks.Add(CustodiaBMFMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
					}
				}

				return this._CustodiaBMFCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CustodiaBMFCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("CustodiaBMFCollectionByCdAtivoBMF"); 
					this._CustodiaBMFCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private CustodiaBMFCollection _CustodiaBMFCollectionByCdAtivoBMF;
		#endregion

				
		#region EnquadraItemFuturoBMFCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ClasseBMF_EnquadraItemFuturoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemFuturoBMFCollection EnquadraItemFuturoBMFCollectionByCdAtivoBMF
		{
			get
			{
				if(this._EnquadraItemFuturoBMFCollectionByCdAtivoBMF == null)
				{
					this._EnquadraItemFuturoBMFCollectionByCdAtivoBMF = new EnquadraItemFuturoBMFCollection();
					this._EnquadraItemFuturoBMFCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraItemFuturoBMFCollectionByCdAtivoBMF", this._EnquadraItemFuturoBMFCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null)
					{
						this._EnquadraItemFuturoBMFCollectionByCdAtivoBMF.Query.Where(this._EnquadraItemFuturoBMFCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._EnquadraItemFuturoBMFCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraItemFuturoBMFCollectionByCdAtivoBMF.fks.Add(EnquadraItemFuturoBMFMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
					}
				}

				return this._EnquadraItemFuturoBMFCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemFuturoBMFCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("EnquadraItemFuturoBMFCollectionByCdAtivoBMF"); 
					this._EnquadraItemFuturoBMFCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private EnquadraItemFuturoBMFCollection _EnquadraItemFuturoBMFCollectionByCdAtivoBMF;
		#endregion

				
		#region EnquadraItemOpcaoBMFCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ClasseBMF_EnquadraItemOpcaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemOpcaoBMFCollection EnquadraItemOpcaoBMFCollectionByCdAtivoBMF
		{
			get
			{
				if(this._EnquadraItemOpcaoBMFCollectionByCdAtivoBMF == null)
				{
					this._EnquadraItemOpcaoBMFCollectionByCdAtivoBMF = new EnquadraItemOpcaoBMFCollection();
					this._EnquadraItemOpcaoBMFCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraItemOpcaoBMFCollectionByCdAtivoBMF", this._EnquadraItemOpcaoBMFCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null)
					{
						this._EnquadraItemOpcaoBMFCollectionByCdAtivoBMF.Query.Where(this._EnquadraItemOpcaoBMFCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._EnquadraItemOpcaoBMFCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraItemOpcaoBMFCollectionByCdAtivoBMF.fks.Add(EnquadraItemOpcaoBMFMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
					}
				}

				return this._EnquadraItemOpcaoBMFCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemOpcaoBMFCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("EnquadraItemOpcaoBMFCollectionByCdAtivoBMF"); 
					this._EnquadraItemOpcaoBMFCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private EnquadraItemOpcaoBMFCollection _EnquadraItemOpcaoBMFCollectionByCdAtivoBMF;
		#endregion

				
		#region TabelaCalculoBMFCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ClasseBMF_TabelaCalculoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaCalculoBMFCollection TabelaCalculoBMFCollectionByCdAtivoBMF
		{
			get
			{
				if(this._TabelaCalculoBMFCollectionByCdAtivoBMF == null)
				{
					this._TabelaCalculoBMFCollectionByCdAtivoBMF = new TabelaCalculoBMFCollection();
					this._TabelaCalculoBMFCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaCalculoBMFCollectionByCdAtivoBMF", this._TabelaCalculoBMFCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null)
					{
						this._TabelaCalculoBMFCollectionByCdAtivoBMF.Query.Where(this._TabelaCalculoBMFCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._TabelaCalculoBMFCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaCalculoBMFCollectionByCdAtivoBMF.fks.Add(TabelaCalculoBMFMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
					}
				}

				return this._TabelaCalculoBMFCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaCalculoBMFCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("TabelaCalculoBMFCollectionByCdAtivoBMF"); 
					this._TabelaCalculoBMFCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private TabelaCalculoBMFCollection _TabelaCalculoBMFCollectionByCdAtivoBMF;
		#endregion

				
		#region TabelaTOBCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ClasseBMF_TabelaTOB_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaTOBCollection TabelaTOBCollectionByCdAtivoBMF
		{
			get
			{
				if(this._TabelaTOBCollectionByCdAtivoBMF == null)
				{
					this._TabelaTOBCollectionByCdAtivoBMF = new TabelaTOBCollection();
					this._TabelaTOBCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaTOBCollectionByCdAtivoBMF", this._TabelaTOBCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null)
					{
						this._TabelaTOBCollectionByCdAtivoBMF.Query.Where(this._TabelaTOBCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._TabelaTOBCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaTOBCollectionByCdAtivoBMF.fks.Add(TabelaTOBMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
					}
				}

				return this._TabelaTOBCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaTOBCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("TabelaTOBCollectionByCdAtivoBMF"); 
					this._TabelaTOBCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private TabelaTOBCollection _TabelaTOBCollectionByCdAtivoBMF;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "AcumuladoCorretagemBMFCollectionByCdAtivoBMF", typeof(AcumuladoCorretagemBMFCollection), new AcumuladoCorretagemBMF()));
			props.Add(new esPropertyDescriptor(this, "AtivoBMFCollectionByCdAtivoBMF", typeof(AtivoBMFCollection), new AtivoBMF()));
			props.Add(new esPropertyDescriptor(this, "CustodiaBMFCollectionByCdAtivoBMF", typeof(CustodiaBMFCollection), new CustodiaBMF()));
			props.Add(new esPropertyDescriptor(this, "EnquadraItemFuturoBMFCollectionByCdAtivoBMF", typeof(EnquadraItemFuturoBMFCollection), new EnquadraItemFuturoBMF()));
			props.Add(new esPropertyDescriptor(this, "EnquadraItemOpcaoBMFCollectionByCdAtivoBMF", typeof(EnquadraItemOpcaoBMFCollection), new EnquadraItemOpcaoBMF()));
			props.Add(new esPropertyDescriptor(this, "TabelaCalculoBMFCollectionByCdAtivoBMF", typeof(TabelaCalculoBMFCollection), new TabelaCalculoBMF()));
			props.Add(new esPropertyDescriptor(this, "TabelaTOBCollectionByCdAtivoBMF", typeof(TabelaTOBCollection), new TabelaTOB()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esClasseBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ClasseBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, ClasseBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ClasseBMFCollection")]
	public partial class ClasseBMFCollection : esClasseBMFCollection, IEnumerable<ClasseBMF>
	{
		public ClasseBMFCollection()
		{

		}
		
		public static implicit operator List<ClasseBMF>(ClasseBMFCollection coll)
		{
			List<ClasseBMF> list = new List<ClasseBMF>();
			
			foreach (ClasseBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ClasseBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClasseBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ClasseBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ClasseBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ClasseBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClasseBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ClasseBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ClasseBMF AddNew()
		{
			ClasseBMF entity = base.AddNewEntity() as ClasseBMF;
			
			return entity;
		}

		public ClasseBMF FindByPrimaryKey(System.String cdAtivoBMF)
		{
			return base.FindByPrimaryKey(cdAtivoBMF) as ClasseBMF;
		}


		#region IEnumerable<ClasseBMF> Members

		IEnumerator<ClasseBMF> IEnumerable<ClasseBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ClasseBMF;
			}
		}

		#endregion
		
		private ClasseBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ClasseBMF' table
	/// </summary>

	[Serializable]
	public partial class ClasseBMF : esClasseBMF
	{
		public ClasseBMF()
		{

		}
	
		public ClasseBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ClasseBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esClasseBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClasseBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ClasseBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClasseBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ClasseBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ClasseBMFQuery query;
	}



	[Serializable]
	public partial class ClasseBMFQuery : esClasseBMFQuery
	{
		public ClasseBMFQuery()
		{

		}		
		
		public ClasseBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ClasseBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ClasseBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ClasseBMFMetadata.ColumnNames.CdAtivoBMF, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = ClasseBMFMetadata.PropertyNames.CdAtivoBMF;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ClasseBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CdAtivoBMF = "CdAtivoBMF";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CdAtivoBMF = "CdAtivoBMF";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ClasseBMFMetadata))
			{
				if(ClasseBMFMetadata.mapDelegates == null)
				{
					ClasseBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ClasseBMFMetadata.meta == null)
				{
					ClasseBMFMetadata.meta = new ClasseBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "ClasseBMF";
				meta.Destination = "ClasseBMF";
				
				meta.spInsert = "proc_ClasseBMFInsert";				
				meta.spUpdate = "proc_ClasseBMFUpdate";		
				meta.spDelete = "proc_ClasseBMFDelete";
				meta.spLoadAll = "proc_ClasseBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_ClasseBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ClasseBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
