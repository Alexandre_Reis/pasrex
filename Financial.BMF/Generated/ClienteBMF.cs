/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2012 19:02:03
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;




using Financial.Common;
using Financial.Investidor;


































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esClienteBMFCollection : esEntityCollection
	{
		public esClienteBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ClienteBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esClienteBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esClienteBMFQuery);
		}
		#endregion
		
		virtual public ClienteBMF DetachEntity(ClienteBMF entity)
		{
			return base.DetachEntity(entity) as ClienteBMF;
		}
		
		virtual public ClienteBMF AttachEntity(ClienteBMF entity)
		{
			return base.AttachEntity(entity) as ClienteBMF;
		}
		
		virtual public void Combine(ClienteBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ClienteBMF this[int index]
		{
			get
			{
				return base[index] as ClienteBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ClienteBMF);
		}
	}



	[Serializable]
	abstract public class esClienteBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esClienteBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esClienteBMF()
		{

		}

		public esClienteBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esClienteBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente)
		{
			esClienteBMFQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "Socio": this.str.Socio = (string)value; break;							
						case "InvestidorInstitucional": this.str.InvestidorInstitucional = (string)value; break;							
						case "TipoCotacao": this.str.TipoCotacao = (string)value; break;							
						case "CodigoSinacor": this.str.CodigoSinacor = (string)value; break;							
						case "IdAssessor": this.str.IdAssessor = (string)value; break;							
						case "TipoPlataforma": this.str.TipoPlataforma = (string)value; break;
                        case "IdCustodianteBmf": this.str.IdCustodianteBmf = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "TipoCotacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCotacao = (System.Byte?)value;
							break;
						
						case "IdAssessor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAssessor = (System.Int32?)value;
							break;
						
						case "TipoPlataforma":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoPlataforma = (System.Byte?)value;
							break;
                        case "IdCustodianteBmf":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdCustodianteBmf = (System.Int32?)value;
                            break;

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ClienteBMF.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ClienteBMFMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(ClienteBMFMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteBMF.Socio
		/// </summary>
		virtual public System.String Socio
		{
			get
			{
				return base.GetSystemString(ClienteBMFMetadata.ColumnNames.Socio);
			}
			
			set
			{
				base.SetSystemString(ClienteBMFMetadata.ColumnNames.Socio, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteBMF.InvestidorInstitucional
		/// </summary>
		virtual public System.String InvestidorInstitucional
		{
			get
			{
				return base.GetSystemString(ClienteBMFMetadata.ColumnNames.InvestidorInstitucional);
			}
			
			set
			{
				base.SetSystemString(ClienteBMFMetadata.ColumnNames.InvestidorInstitucional, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteBMF.TipoCotacao
		/// </summary>
		virtual public System.Byte? TipoCotacao
		{
			get
			{
				return base.GetSystemByte(ClienteBMFMetadata.ColumnNames.TipoCotacao);
			}
			
			set
			{
				base.SetSystemByte(ClienteBMFMetadata.ColumnNames.TipoCotacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteBMF.CodigoSinacor
		/// </summary>
		virtual public System.String CodigoSinacor
		{
			get
			{
				return base.GetSystemString(ClienteBMFMetadata.ColumnNames.CodigoSinacor);
			}
			
			set
			{
				base.SetSystemString(ClienteBMFMetadata.ColumnNames.CodigoSinacor, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteBMF.IdAssessor
		/// </summary>
		virtual public System.Int32? IdAssessor
		{
			get
			{
				return base.GetSystemInt32(ClienteBMFMetadata.ColumnNames.IdAssessor);
			}
			
			set
			{
				if(base.SetSystemInt32(ClienteBMFMetadata.ColumnNames.IdAssessor, value))
				{
					this._UpToAssessorByIdAssessor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ClienteBMF.TipoPlataforma
		/// </summary>
		virtual public System.Byte? TipoPlataforma
		{
			get
			{
				return base.GetSystemByte(ClienteBMFMetadata.ColumnNames.TipoPlataforma);
			}
			
			set
			{
				base.SetSystemByte(ClienteBMFMetadata.ColumnNames.TipoPlataforma, value);
			}
		}

        /// <summary>
        /// Maps to ClienteBMF.IdCustodianteBmf
        /// </summary>
        virtual public System.Int32? IdCustodianteBmf
        {
            get
            {
                return base.GetSystemInt32(ClienteBMFMetadata.ColumnNames.IdCustodianteBmf);
            }

            set
            {
                if (base.SetSystemInt32(ClienteBMFMetadata.ColumnNames.IdCustodianteBmf, value))
                {
                    this._UpToAgenteMercadoByIdCustodianteBmf = null;
                }
            }
        }

        [CLSCompliant(false)]
        internal protected AgenteMercado _UpToAgenteMercadoByIdCustodianteBmf;
		[CLSCompliant(false)]
		internal protected Assessor _UpToAssessorByIdAssessor;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esClienteBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String Socio
			{
				get
				{
					System.String data = entity.Socio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Socio = null;
					else entity.Socio = Convert.ToString(value);
				}
			}
				
			public System.String InvestidorInstitucional
			{
				get
				{
					System.String data = entity.InvestidorInstitucional;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InvestidorInstitucional = null;
					else entity.InvestidorInstitucional = Convert.ToString(value);
				}
			}
				
			public System.String TipoCotacao
			{
				get
				{
					System.Byte? data = entity.TipoCotacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCotacao = null;
					else entity.TipoCotacao = Convert.ToByte(value);
				}
			}
				
			public System.String CodigoSinacor
			{
				get
				{
					System.String data = entity.CodigoSinacor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoSinacor = null;
					else entity.CodigoSinacor = Convert.ToString(value);
				}
			}
				
			public System.String IdAssessor
			{
				get
				{
					System.Int32? data = entity.IdAssessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAssessor = null;
					else entity.IdAssessor = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoPlataforma
			{
				get
				{
					System.Byte? data = entity.TipoPlataforma;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPlataforma = null;
					else entity.TipoPlataforma = Convert.ToByte(value);
				}
			}

            public System.String IdCustodianteBmf
            {
                get
                {
                    System.Int32? data = entity.IdCustodianteBmf;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdCustodianteBmf = null;
                    else entity.IdCustodianteBmf = Convert.ToInt32(value);
                }
            }

			private esClienteBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esClienteBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esClienteBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ClienteBMF : esClienteBMF
	{

        #region UpToAgenteMercadoByIdCustodianteBmf - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - FK_ClienteBMFCustodianteBmf_AgenteMercado
        /// </summary>

        [XmlIgnore]
        public AgenteMercado UpToAgenteMercadoByIdCustodianteBmf
        {
            get
            {
                if (this._UpToAgenteMercadoByIdCustodianteBmf == null
                    && IdCustodianteBmf != null)
                {
                    this._UpToAgenteMercadoByIdCustodianteBmf = new AgenteMercado();
                    this._UpToAgenteMercadoByIdCustodianteBmf.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToAgenteMercadoByIdCustodianteBmf", this._UpToAgenteMercadoByIdCustodianteBmf);
                    this._UpToAgenteMercadoByIdCustodianteBmf.Query.Where(this._UpToAgenteMercadoByIdCustodianteBmf.Query.IdAgente == this.IdCustodianteBmf);
                    this._UpToAgenteMercadoByIdCustodianteBmf.Query.Load();
                }

                return this._UpToAgenteMercadoByIdCustodianteBmf;
            }

            set
            {
                this.RemovePreSave("UpToAgenteMercadoByIdCustodianteBmf");


                if (value == null)
                {
                    this.IdCustodianteBmf = null;
                    this._UpToAgenteMercadoByIdCustodianteBmf = null;
                }
                else
                {
                    this.IdCustodianteBmf = value.IdAgente;
                    this._UpToAgenteMercadoByIdCustodianteBmf = value;
                    this.SetPreSave("UpToAgenteMercadoByIdCustodianteBmf", this._UpToAgenteMercadoByIdCustodianteBmf);
                }

            }
        }
        #endregion
		#region UpToAssessorByIdAssessor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Assessor_ClienteBMF_FK1
		/// </summary>

		[XmlIgnore]
		public Assessor UpToAssessorByIdAssessor
		{
			get
			{
				if(this._UpToAssessorByIdAssessor == null
					&& IdAssessor != null					)
				{
					this._UpToAssessorByIdAssessor = new Assessor();
					this._UpToAssessorByIdAssessor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAssessorByIdAssessor", this._UpToAssessorByIdAssessor);
					this._UpToAssessorByIdAssessor.Query.Where(this._UpToAssessorByIdAssessor.Query.IdAssessor == this.IdAssessor);
					this._UpToAssessorByIdAssessor.Query.Load();
				}

				return this._UpToAssessorByIdAssessor;
			}
			
			set
			{
				this.RemovePreSave("UpToAssessorByIdAssessor");
				

				if(value == null)
				{
					this.IdAssessor = null;
					this._UpToAssessorByIdAssessor = null;
				}
				else
				{
					this.IdAssessor = value.IdAssessor;
					this._UpToAssessorByIdAssessor = value;
					this.SetPreSave("UpToAssessorByIdAssessor", this._UpToAssessorByIdAssessor);
				}
				
			}
		}
		#endregion
		

		#region UpToCliente - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Cliente_ClienteBMF_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToCliente
		{
			get
			{
				if(this._UpToCliente == null
					&& IdCliente != null					)
				{
					this._UpToCliente = new Cliente();
					this._UpToCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCliente", this._UpToCliente);
					this._UpToCliente.Query.Where(this._UpToCliente.Query.IdCliente == this.IdCliente);
					this._UpToCliente.Query.Load();
				}

				return this._UpToCliente;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToCliente");

				if(value == null)
				{
					this._UpToCliente = null;
				}
				else
				{
					this._UpToCliente = value;
					this.SetPreSave("UpToCliente", this._UpToCliente);
				}
				
				
			} 
		}

		private Cliente _UpToCliente;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
            if (!this.es.IsDeleted && this._UpToAgenteMercadoByIdCustodianteBmf != null)
            {
                this.IdCustodianteBmf = this._UpToAgenteMercadoByIdCustodianteBmf.IdAgente;
            }
			if(!this.es.IsDeleted && this._UpToAssessorByIdAssessor != null)
			{
				this.IdAssessor = this._UpToAssessorByIdAssessor.IdAssessor;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esClienteBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ClienteBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ClienteBMFMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Socio
		{
			get
			{
				return new esQueryItem(this, ClienteBMFMetadata.ColumnNames.Socio, esSystemType.String);
			}
		} 
		
		public esQueryItem InvestidorInstitucional
		{
			get
			{
				return new esQueryItem(this, ClienteBMFMetadata.ColumnNames.InvestidorInstitucional, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoCotacao
		{
			get
			{
				return new esQueryItem(this, ClienteBMFMetadata.ColumnNames.TipoCotacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CodigoSinacor
		{
			get
			{
				return new esQueryItem(this, ClienteBMFMetadata.ColumnNames.CodigoSinacor, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAssessor
		{
			get
			{
				return new esQueryItem(this, ClienteBMFMetadata.ColumnNames.IdAssessor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoPlataforma
		{
			get
			{
				return new esQueryItem(this, ClienteBMFMetadata.ColumnNames.TipoPlataforma, esSystemType.Byte);
			}
		}
        public esQueryItem IdCustodianteBmf
        {
            get
            {
                return new esQueryItem(this, ClienteBMFMetadata.ColumnNames.IdCustodianteBmf, esSystemType.Int32);
            }
        } 
	}



	[Serializable]
	[XmlType("ClienteBMFCollection")]
	public partial class ClienteBMFCollection : esClienteBMFCollection, IEnumerable<ClienteBMF>
	{
		public ClienteBMFCollection()
		{

		}
		
		public static implicit operator List<ClienteBMF>(ClienteBMFCollection coll)
		{
			List<ClienteBMF> list = new List<ClienteBMF>();
			
			foreach (ClienteBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ClienteBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClienteBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ClienteBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ClienteBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ClienteBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClienteBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ClienteBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ClienteBMF AddNew()
		{
			ClienteBMF entity = base.AddNewEntity() as ClienteBMF;
			
			return entity;
		}

		public ClienteBMF FindByPrimaryKey(System.Int32 idCliente)
		{
			return base.FindByPrimaryKey(idCliente) as ClienteBMF;
		}


		#region IEnumerable<ClienteBMF> Members

		IEnumerator<ClienteBMF> IEnumerable<ClienteBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ClienteBMF;
			}
		}

		#endregion
		
		private ClienteBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ClienteBMF' table
	/// </summary>

	[Serializable]
	public partial class ClienteBMF : esClienteBMF
	{
		public ClienteBMF()
		{

		}
	
		public ClienteBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ClienteBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esClienteBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClienteBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ClienteBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClienteBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ClienteBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ClienteBMFQuery query;
	}



	[Serializable]
	public partial class ClienteBMFQuery : esClienteBMFQuery
	{
		public ClienteBMFQuery()
		{

		}		
		
		public ClienteBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ClienteBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ClienteBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ClienteBMFMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteBMFMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteBMFMetadata.ColumnNames.Socio, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteBMFMetadata.PropertyNames.Socio;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteBMFMetadata.ColumnNames.InvestidorInstitucional, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteBMFMetadata.PropertyNames.InvestidorInstitucional;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteBMFMetadata.ColumnNames.TipoCotacao, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClienteBMFMetadata.PropertyNames.TipoCotacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteBMFMetadata.ColumnNames.CodigoSinacor, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteBMFMetadata.PropertyNames.CodigoSinacor;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteBMFMetadata.ColumnNames.IdAssessor, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteBMFMetadata.PropertyNames.IdAssessor;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteBMFMetadata.ColumnNames.TipoPlataforma, 6, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClienteBMFMetadata.PropertyNames.TipoPlataforma;	
			c.NumericPrecision = 3;
			_columns.Add(c);
            c = new esColumnMetadata(ClienteBMFMetadata.ColumnNames.IdCustodianteBmf, 8, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ClienteBMFMetadata.PropertyNames.IdCustodianteBmf;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c); 
				
		}
		#endregion
	
		static public ClienteBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string Socio = "Socio";
			 public const string InvestidorInstitucional = "InvestidorInstitucional";
			 public const string TipoCotacao = "TipoCotacao";
			 public const string CodigoSinacor = "CodigoSinacor";
			 public const string IdAssessor = "IdAssessor";
			 public const string TipoPlataforma = "TipoPlataforma";
            public const string IdCustodianteBmf = "IdCustodianteBmf";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string Socio = "Socio";
			 public const string InvestidorInstitucional = "InvestidorInstitucional";
			 public const string TipoCotacao = "TipoCotacao";
			 public const string CodigoSinacor = "CodigoSinacor";
			 public const string IdAssessor = "IdAssessor";
			 public const string TipoPlataforma = "TipoPlataforma";
            public const string IdCustodianteBmf = "IdCustodianteBmf";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ClienteBMFMetadata))
			{
				if(ClienteBMFMetadata.mapDelegates == null)
				{
					ClienteBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ClienteBMFMetadata.meta == null)
				{
					ClienteBMFMetadata.meta = new ClienteBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Socio", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("InvestidorInstitucional", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TipoCotacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CodigoSinacor", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAssessor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoPlataforma", new esTypeMap("tinyint", "System.Byte"));
                meta.AddTypeMap("IdCustodianteBmf", new esTypeMap("int", "System.Int32"));			
				
				
				meta.Source = "ClienteBMF";
				meta.Destination = "ClienteBMF";
				
				meta.spInsert = "proc_ClienteBMFInsert";				
				meta.spUpdate = "proc_ClienteBMFUpdate";		
				meta.spDelete = "proc_ClienteBMFDelete";
				meta.spLoadAll = "proc_ClienteBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_ClienteBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ClienteBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
