/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2012 19:02:04
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esCotacaoBMFCollection : esEntityCollection
	{
		public esCotacaoBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CotacaoBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esCotacaoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCotacaoBMFQuery);
		}
		#endregion
		
		virtual public CotacaoBMF DetachEntity(CotacaoBMF entity)
		{
			return base.DetachEntity(entity) as CotacaoBMF;
		}
		
		virtual public CotacaoBMF AttachEntity(CotacaoBMF entity)
		{
			return base.AttachEntity(entity) as CotacaoBMF;
		}
		
		virtual public void Combine(CotacaoBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CotacaoBMF this[int index]
		{
			get
			{
				return base[index] as CotacaoBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CotacaoBMF);
		}
	}



	[Serializable]
	abstract public class esCotacaoBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCotacaoBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esCotacaoBMF()
		{

		}

		public esCotacaoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.String cdAtivoBMF, System.String serie)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, cdAtivoBMF, serie);
			else
				return LoadByPrimaryKeyStoredProcedure(data, cdAtivoBMF, serie);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime data, System.String cdAtivoBMF, System.String serie)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCotacaoBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Data == data, query.CdAtivoBMF == cdAtivoBMF, query.Serie == serie);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.String cdAtivoBMF, System.String serie)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, cdAtivoBMF, serie);
			else
				return LoadByPrimaryKeyStoredProcedure(data, cdAtivoBMF, serie);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.String cdAtivoBMF, System.String serie)
		{
			esCotacaoBMFQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.CdAtivoBMF == cdAtivoBMF, query.Serie == serie);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.String cdAtivoBMF, System.String serie)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("CdAtivoBMF",cdAtivoBMF);			parms.Add("Serie",serie);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Serie": this.str.Serie = (string)value; break;							
						case "PUFechamento": this.str.PUFechamento = (string)value; break;							
						case "PUMedio": this.str.PUMedio = (string)value; break;							
						case "PUCorrigido": this.str.PUCorrigido = (string)value; break;							
						case "PUGerencial": this.str.PUGerencial = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "PUFechamento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUFechamento = (System.Decimal?)value;
							break;
						
						case "PUMedio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUMedio = (System.Decimal?)value;
							break;
						
						case "PUCorrigido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCorrigido = (System.Decimal?)value;
							break;
						
						case "PUGerencial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUGerencial = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CotacaoBMF.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(CotacaoBMFMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoBMFMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(CotacaoBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(CotacaoBMFMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CotacaoBMF.Serie
		/// </summary>
		virtual public System.String Serie
		{
			get
			{
				return base.GetSystemString(CotacaoBMFMetadata.ColumnNames.Serie);
			}
			
			set
			{
				if(base.SetSystemString(CotacaoBMFMetadata.ColumnNames.Serie, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CotacaoBMF.PUFechamento
		/// </summary>
		virtual public System.Decimal? PUFechamento
		{
			get
			{
				return base.GetSystemDecimal(CotacaoBMFMetadata.ColumnNames.PUFechamento);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoBMFMetadata.ColumnNames.PUFechamento, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoBMF.PUMedio
		/// </summary>
		virtual public System.Decimal? PUMedio
		{
			get
			{
				return base.GetSystemDecimal(CotacaoBMFMetadata.ColumnNames.PUMedio);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoBMFMetadata.ColumnNames.PUMedio, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoBMF.PUCorrigido
		/// </summary>
		virtual public System.Decimal? PUCorrigido
		{
			get
			{
				return base.GetSystemDecimal(CotacaoBMFMetadata.ColumnNames.PUCorrigido);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoBMFMetadata.ColumnNames.PUCorrigido, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoBMF.PUGerencial
		/// </summary>
		virtual public System.Decimal? PUGerencial
		{
			get
			{
				return base.GetSystemDecimal(CotacaoBMFMetadata.ColumnNames.PUGerencial);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoBMFMetadata.ColumnNames.PUGerencial, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBMF _UpToAtivoBMFByCdAtivoBMF;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCotacaoBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Serie
			{
				get
				{
					System.String data = entity.Serie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Serie = null;
					else entity.Serie = Convert.ToString(value);
				}
			}
				
			public System.String PUFechamento
			{
				get
				{
					System.Decimal? data = entity.PUFechamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUFechamento = null;
					else entity.PUFechamento = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUMedio
			{
				get
				{
					System.Decimal? data = entity.PUMedio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUMedio = null;
					else entity.PUMedio = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCorrigido
			{
				get
				{
					System.Decimal? data = entity.PUCorrigido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCorrigido = null;
					else entity.PUCorrigido = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUGerencial
			{
				get
				{
					System.Decimal? data = entity.PUGerencial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUGerencial = null;
					else entity.PUGerencial = Convert.ToDecimal(value);
				}
			}
			

			private esCotacaoBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCotacaoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCotacaoBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CotacaoBMF : esCotacaoBMF
	{

				
		#region UpToAtivoBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBMF_CotacaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBMF UpToAtivoBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToAtivoBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					&& Serie != null					)
				{
					this._UpToAtivoBMFByCdAtivoBMF = new AtivoBMF();
					this._UpToAtivoBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.Serie == this.Serie);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToAtivoBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this.Serie = null;
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this.Serie = value.Serie;
					this._UpToAtivoBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCotacaoBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, CotacaoBMFMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, CotacaoBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Serie
		{
			get
			{
				return new esQueryItem(this, CotacaoBMFMetadata.ColumnNames.Serie, esSystemType.String);
			}
		} 
		
		public esQueryItem PUFechamento
		{
			get
			{
				return new esQueryItem(this, CotacaoBMFMetadata.ColumnNames.PUFechamento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUMedio
		{
			get
			{
				return new esQueryItem(this, CotacaoBMFMetadata.ColumnNames.PUMedio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCorrigido
		{
			get
			{
				return new esQueryItem(this, CotacaoBMFMetadata.ColumnNames.PUCorrigido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUGerencial
		{
			get
			{
				return new esQueryItem(this, CotacaoBMFMetadata.ColumnNames.PUGerencial, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CotacaoBMFCollection")]
	public partial class CotacaoBMFCollection : esCotacaoBMFCollection, IEnumerable<CotacaoBMF>
	{
		public CotacaoBMFCollection()
		{

		}
		
		public static implicit operator List<CotacaoBMF>(CotacaoBMFCollection coll)
		{
			List<CotacaoBMF> list = new List<CotacaoBMF>();
			
			foreach (CotacaoBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CotacaoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CotacaoBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CotacaoBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CotacaoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CotacaoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CotacaoBMF AddNew()
		{
			CotacaoBMF entity = base.AddNewEntity() as CotacaoBMF;
			
			return entity;
		}

		public CotacaoBMF FindByPrimaryKey(System.DateTime data, System.String cdAtivoBMF, System.String serie)
		{
			return base.FindByPrimaryKey(data, cdAtivoBMF, serie) as CotacaoBMF;
		}


		#region IEnumerable<CotacaoBMF> Members

		IEnumerator<CotacaoBMF> IEnumerable<CotacaoBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CotacaoBMF;
			}
		}

		#endregion
		
		private CotacaoBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CotacaoBMF' table
	/// </summary>

	[Serializable]
	public partial class CotacaoBMF : esCotacaoBMF
	{
		public CotacaoBMF()
		{

		}
	
		public CotacaoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esCotacaoBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CotacaoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CotacaoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CotacaoBMFQuery query;
	}



	[Serializable]
	public partial class CotacaoBMFQuery : esCotacaoBMFQuery
	{
		public CotacaoBMFQuery()
		{

		}		
		
		public CotacaoBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CotacaoBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CotacaoBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CotacaoBMFMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoBMFMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoBMFMetadata.ColumnNames.CdAtivoBMF, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CotacaoBMFMetadata.PropertyNames.CdAtivoBMF;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoBMFMetadata.ColumnNames.Serie, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = CotacaoBMFMetadata.PropertyNames.Serie;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoBMFMetadata.ColumnNames.PUFechamento, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoBMFMetadata.PropertyNames.PUFechamento;	
			c.NumericPrecision = 25;
			c.NumericScale = 15;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoBMFMetadata.ColumnNames.PUMedio, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoBMFMetadata.PropertyNames.PUMedio;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoBMFMetadata.ColumnNames.PUCorrigido, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoBMFMetadata.PropertyNames.PUCorrigido;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoBMFMetadata.ColumnNames.PUGerencial, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoBMFMetadata.PropertyNames.PUGerencial;	
			c.NumericPrecision = 25;
			c.NumericScale = 16;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CotacaoBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string PUFechamento = "PUFechamento";
			 public const string PUMedio = "PUMedio";
			 public const string PUCorrigido = "PUCorrigido";
			 public const string PUGerencial = "PUGerencial";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string PUFechamento = "PUFechamento";
			 public const string PUMedio = "PUMedio";
			 public const string PUCorrigido = "PUCorrigido";
			 public const string PUGerencial = "PUGerencial";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CotacaoBMFMetadata))
			{
				if(CotacaoBMFMetadata.mapDelegates == null)
				{
					CotacaoBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CotacaoBMFMetadata.meta == null)
				{
					CotacaoBMFMetadata.meta = new CotacaoBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Serie", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PUFechamento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUMedio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCorrigido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUGerencial", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CotacaoBMF";
				meta.Destination = "CotacaoBMF";
				
				meta.spInsert = "proc_CotacaoBMFInsert";				
				meta.spUpdate = "proc_CotacaoBMFUpdate";		
				meta.spDelete = "proc_CotacaoBMFDelete";
				meta.spLoadAll = "proc_CotacaoBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_CotacaoBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CotacaoBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
