/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2012 19:02:06
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esTabelaTOBCollection : esEntityCollection
	{
		public esTabelaTOBCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaTOBCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaTOBQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaTOBQuery);
		}
		#endregion
		
		virtual public TabelaTOB DetachEntity(TabelaTOB entity)
		{
			return base.DetachEntity(entity) as TabelaTOB;
		}
		
		virtual public TabelaTOB AttachEntity(TabelaTOB entity)
		{
			return base.AttachEntity(entity) as TabelaTOB;
		}
		
		virtual public void Combine(TabelaTOBCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaTOB this[int index]
		{
			get
			{
				return base[index] as TabelaTOB;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaTOB);
		}
	}



	[Serializable]
	abstract public class esTabelaTOB : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaTOBQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaTOB()
		{

		}

		public esTabelaTOB(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.String cdAtivoBMF, System.Byte tipoMercado)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, cdAtivoBMF, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, cdAtivoBMF, tipoMercado);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.String cdAtivoBMF, System.Byte tipoMercado)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaTOBQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.CdAtivoBMF == cdAtivoBMF, query.TipoMercado == tipoMercado);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.String cdAtivoBMF, System.Byte tipoMercado)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, cdAtivoBMF, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, cdAtivoBMF, tipoMercado);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.String cdAtivoBMF, System.Byte tipoMercado)
		{
			esTabelaTOBQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.CdAtivoBMF == cdAtivoBMF, query.TipoMercado == tipoMercado);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.String cdAtivoBMF, System.Byte tipoMercado)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("CdAtivoBMF",cdAtivoBMF);			parms.Add("TipoMercado",tipoMercado);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "Tob": this.str.Tob = (string)value; break;							
						case "TOBDayTrade": this.str.TOBDayTrade = (string)value; break;							
						case "TOBMinima": this.str.TOBMinima = (string)value; break;							
						case "TOBDayTradeMinima": this.str.TOBDayTradeMinima = (string)value; break;							
						case "TOBExercicio": this.str.TOBExercicio = (string)value; break;							
						case "TOBExercicioCasado": this.str.TOBExercicioCasado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoMercado = (System.Byte?)value;
							break;
						
						case "Tob":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Tob = (System.Decimal?)value;
							break;
						
						case "TOBDayTrade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TOBDayTrade = (System.Decimal?)value;
							break;
						
						case "TOBMinima":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TOBMinima = (System.Decimal?)value;
							break;
						
						case "TOBDayTradeMinima":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TOBDayTradeMinima = (System.Decimal?)value;
							break;
						
						case "TOBExercicio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TOBExercicio = (System.Decimal?)value;
							break;
						
						case "TOBExercicioCasado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TOBExercicioCasado = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaTOB.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaTOBMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaTOBMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTOB.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(TabelaTOBMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(TabelaTOBMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToClasseBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaTOB.TipoMercado
		/// </summary>
		virtual public System.Byte? TipoMercado
		{
			get
			{
				return base.GetSystemByte(TabelaTOBMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemByte(TabelaTOBMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTOB.TOB
		/// </summary>
		virtual public System.Decimal? Tob
		{
			get
			{
				return base.GetSystemDecimal(TabelaTOBMetadata.ColumnNames.Tob);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTOBMetadata.ColumnNames.Tob, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTOB.TOBDayTrade
		/// </summary>
		virtual public System.Decimal? TOBDayTrade
		{
			get
			{
				return base.GetSystemDecimal(TabelaTOBMetadata.ColumnNames.TOBDayTrade);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTOBMetadata.ColumnNames.TOBDayTrade, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTOB.TOBMinima
		/// </summary>
		virtual public System.Decimal? TOBMinima
		{
			get
			{
				return base.GetSystemDecimal(TabelaTOBMetadata.ColumnNames.TOBMinima);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTOBMetadata.ColumnNames.TOBMinima, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTOB.TOBDayTradeMinima
		/// </summary>
		virtual public System.Decimal? TOBDayTradeMinima
		{
			get
			{
				return base.GetSystemDecimal(TabelaTOBMetadata.ColumnNames.TOBDayTradeMinima);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTOBMetadata.ColumnNames.TOBDayTradeMinima, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTOB.TOBExercicio
		/// </summary>
		virtual public System.Decimal? TOBExercicio
		{
			get
			{
				return base.GetSystemDecimal(TabelaTOBMetadata.ColumnNames.TOBExercicio);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTOBMetadata.ColumnNames.TOBExercicio, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTOB.TOBExercicioCasado
		/// </summary>
		virtual public System.Decimal? TOBExercicioCasado
		{
			get
			{
				return base.GetSystemDecimal(TabelaTOBMetadata.ColumnNames.TOBExercicioCasado);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTOBMetadata.ColumnNames.TOBExercicioCasado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected ClasseBMF _UpToClasseBMFByCdAtivoBMF;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaTOB entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Byte? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToByte(value);
				}
			}
				
			public System.String Tob
			{
				get
				{
					System.Decimal? data = entity.Tob;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tob = null;
					else entity.Tob = Convert.ToDecimal(value);
				}
			}
				
			public System.String TOBDayTrade
			{
				get
				{
					System.Decimal? data = entity.TOBDayTrade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TOBDayTrade = null;
					else entity.TOBDayTrade = Convert.ToDecimal(value);
				}
			}
				
			public System.String TOBMinima
			{
				get
				{
					System.Decimal? data = entity.TOBMinima;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TOBMinima = null;
					else entity.TOBMinima = Convert.ToDecimal(value);
				}
			}
				
			public System.String TOBDayTradeMinima
			{
				get
				{
					System.Decimal? data = entity.TOBDayTradeMinima;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TOBDayTradeMinima = null;
					else entity.TOBDayTradeMinima = Convert.ToDecimal(value);
				}
			}
				
			public System.String TOBExercicio
			{
				get
				{
					System.Decimal? data = entity.TOBExercicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TOBExercicio = null;
					else entity.TOBExercicio = Convert.ToDecimal(value);
				}
			}
				
			public System.String TOBExercicioCasado
			{
				get
				{
					System.Decimal? data = entity.TOBExercicioCasado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TOBExercicioCasado = null;
					else entity.TOBExercicioCasado = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaTOB entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaTOBQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaTOB can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaTOB : esTabelaTOB
	{

				
		#region UpToClasseBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ClasseBMF_TabelaTOB_FK1
		/// </summary>

		[XmlIgnore]
		public ClasseBMF UpToClasseBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToClasseBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					)
				{
					this._UpToClasseBMFByCdAtivoBMF = new ClasseBMF();
					this._UpToClasseBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClasseBMFByCdAtivoBMF", this._UpToClasseBMFByCdAtivoBMF);
					this._UpToClasseBMFByCdAtivoBMF.Query.Where(this._UpToClasseBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToClasseBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToClasseBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToClasseBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this._UpToClasseBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this._UpToClasseBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToClasseBMFByCdAtivoBMF", this._UpToClasseBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaTOBQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaTOBMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaTOBMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, TabelaTOBMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, TabelaTOBMetadata.ColumnNames.TipoMercado, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Tob
		{
			get
			{
				return new esQueryItem(this, TabelaTOBMetadata.ColumnNames.Tob, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TOBDayTrade
		{
			get
			{
				return new esQueryItem(this, TabelaTOBMetadata.ColumnNames.TOBDayTrade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TOBMinima
		{
			get
			{
				return new esQueryItem(this, TabelaTOBMetadata.ColumnNames.TOBMinima, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TOBDayTradeMinima
		{
			get
			{
				return new esQueryItem(this, TabelaTOBMetadata.ColumnNames.TOBDayTradeMinima, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TOBExercicio
		{
			get
			{
				return new esQueryItem(this, TabelaTOBMetadata.ColumnNames.TOBExercicio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TOBExercicioCasado
		{
			get
			{
				return new esQueryItem(this, TabelaTOBMetadata.ColumnNames.TOBExercicioCasado, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaTOBCollection")]
	public partial class TabelaTOBCollection : esTabelaTOBCollection, IEnumerable<TabelaTOB>
	{
		public TabelaTOBCollection()
		{

		}
		
		public static implicit operator List<TabelaTOB>(TabelaTOBCollection coll)
		{
			List<TabelaTOB> list = new List<TabelaTOB>();
			
			foreach (TabelaTOB emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaTOBMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaTOBQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaTOB(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaTOB();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaTOBQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaTOBQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaTOBQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaTOB AddNew()
		{
			TabelaTOB entity = base.AddNewEntity() as TabelaTOB;
			
			return entity;
		}

		public TabelaTOB FindByPrimaryKey(System.DateTime dataReferencia, System.String cdAtivoBMF, System.Byte tipoMercado)
		{
			return base.FindByPrimaryKey(dataReferencia, cdAtivoBMF, tipoMercado) as TabelaTOB;
		}


		#region IEnumerable<TabelaTOB> Members

		IEnumerator<TabelaTOB> IEnumerable<TabelaTOB>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaTOB;
			}
		}

		#endregion
		
		private TabelaTOBQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaTOB' table
	/// </summary>

	[Serializable]
	public partial class TabelaTOB : esTabelaTOB
	{
		public TabelaTOB()
		{

		}
	
		public TabelaTOB(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaTOBMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaTOBQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaTOBQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaTOBQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaTOBQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaTOBQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaTOBQuery query;
	}



	[Serializable]
	public partial class TabelaTOBQuery : esTabelaTOBQuery
	{
		public TabelaTOBQuery()
		{

		}		
		
		public TabelaTOBQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaTOBMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaTOBMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaTOBMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaTOBMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTOBMetadata.ColumnNames.CdAtivoBMF, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaTOBMetadata.PropertyNames.CdAtivoBMF;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTOBMetadata.ColumnNames.TipoMercado, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTOBMetadata.PropertyNames.TipoMercado;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTOBMetadata.ColumnNames.Tob, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTOBMetadata.PropertyNames.Tob;	
			c.NumericPrecision = 12;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTOBMetadata.ColumnNames.TOBDayTrade, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTOBMetadata.PropertyNames.TOBDayTrade;	
			c.NumericPrecision = 12;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTOBMetadata.ColumnNames.TOBMinima, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTOBMetadata.PropertyNames.TOBMinima;	
			c.NumericPrecision = 12;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTOBMetadata.ColumnNames.TOBDayTradeMinima, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTOBMetadata.PropertyNames.TOBDayTradeMinima;	
			c.NumericPrecision = 12;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTOBMetadata.ColumnNames.TOBExercicio, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTOBMetadata.PropertyNames.TOBExercicio;	
			c.NumericPrecision = 12;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTOBMetadata.ColumnNames.TOBExercicioCasado, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTOBMetadata.PropertyNames.TOBExercicioCasado;	
			c.NumericPrecision = 12;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaTOBMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string TipoMercado = "TipoMercado";
			 public const string Tob = "TOB";
			 public const string TOBDayTrade = "TOBDayTrade";
			 public const string TOBMinima = "TOBMinima";
			 public const string TOBDayTradeMinima = "TOBDayTradeMinima";
			 public const string TOBExercicio = "TOBExercicio";
			 public const string TOBExercicioCasado = "TOBExercicioCasado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string TipoMercado = "TipoMercado";
			 public const string Tob = "Tob";
			 public const string TOBDayTrade = "TOBDayTrade";
			 public const string TOBMinima = "TOBMinima";
			 public const string TOBDayTradeMinima = "TOBDayTradeMinima";
			 public const string TOBExercicio = "TOBExercicio";
			 public const string TOBExercicioCasado = "TOBExercicioCasado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaTOBMetadata))
			{
				if(TabelaTOBMetadata.mapDelegates == null)
				{
					TabelaTOBMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaTOBMetadata.meta == null)
				{
					TabelaTOBMetadata.meta = new TabelaTOBMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TOB", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TOBDayTrade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TOBMinima", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TOBDayTradeMinima", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TOBExercicio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TOBExercicioCasado", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaTOB";
				meta.Destination = "TabelaTOB";
				
				meta.spInsert = "proc_TabelaTOBInsert";				
				meta.spUpdate = "proc_TabelaTOBUpdate";		
				meta.spDelete = "proc_TabelaTOBDelete";
				meta.spLoadAll = "proc_TabelaTOBLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaTOBLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaTOBMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
