/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2012 19:02:06
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
















			





















		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esTabelaCustosBMFCollection : esEntityCollection
	{
		public esTabelaCustosBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaCustosBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaCustosBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaCustosBMFQuery);
		}
		#endregion
		
		virtual public TabelaCustosBMF DetachEntity(TabelaCustosBMF entity)
		{
			return base.DetachEntity(entity) as TabelaCustosBMF;
		}
		
		virtual public TabelaCustosBMF AttachEntity(TabelaCustosBMF entity)
		{
			return base.AttachEntity(entity) as TabelaCustosBMF;
		}
		
		virtual public void Combine(TabelaCustosBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaCustosBMF this[int index]
		{
			get
			{
				return base[index] as TabelaCustosBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaCustosBMF);
		}
	}



	[Serializable]
	abstract public class esTabelaCustosBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaCustosBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaCustosBMF()
		{

		}

		public esTabelaCustosBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, cdAtivoBMF, serie);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, cdAtivoBMF, serie);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaCustosBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.CdAtivoBMF == cdAtivoBMF, query.Serie == serie);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, cdAtivoBMF, serie);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, cdAtivoBMF, serie);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			esTabelaCustosBMFQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.CdAtivoBMF == cdAtivoBMF, query.Serie == serie);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("CdAtivoBMF",cdAtivoBMF);			parms.Add("Serie",serie);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Serie": this.str.Serie = (string)value; break;							
						case "Emolumento": this.str.Emolumento = (string)value; break;							
						case "EmolumentoDT": this.str.EmolumentoDT = (string)value; break;							
						case "EmolumentoSocio": this.str.EmolumentoSocio = (string)value; break;							
						case "EmolumentoDTSocio": this.str.EmolumentoDTSocio = (string)value; break;							
						case "Registro": this.str.Registro = (string)value; break;							
						case "RegistroDT": this.str.RegistroDT = (string)value; break;							
						case "RegistroSocio": this.str.RegistroSocio = (string)value; break;							
						case "RegistroDTSocio": this.str.RegistroDTSocio = (string)value; break;							
						case "FatorReducao": this.str.FatorReducao = (string)value; break;							
						case "Permanencia": this.str.Permanencia = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "Emolumento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Emolumento = (System.Decimal?)value;
							break;
						
						case "EmolumentoDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.EmolumentoDT = (System.Decimal?)value;
							break;
						
						case "EmolumentoSocio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.EmolumentoSocio = (System.Decimal?)value;
							break;
						
						case "EmolumentoDTSocio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.EmolumentoDTSocio = (System.Decimal?)value;
							break;
						
						case "Registro":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Registro = (System.Decimal?)value;
							break;
						
						case "RegistroDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RegistroDT = (System.Decimal?)value;
							break;
						
						case "RegistroSocio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RegistroSocio = (System.Decimal?)value;
							break;
						
						case "RegistroDTSocio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RegistroDTSocio = (System.Decimal?)value;
							break;
						
						case "FatorReducao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.FatorReducao = (System.Decimal?)value;
							break;
						
						case "Permanencia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Permanencia = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaCustosBMF.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaCustosBMFMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaCustosBMFMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(TabelaCustosBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(TabelaCustosBMFMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBMF.Serie
		/// </summary>
		virtual public System.String Serie
		{
			get
			{
				return base.GetSystemString(TabelaCustosBMFMetadata.ColumnNames.Serie);
			}
			
			set
			{
				if(base.SetSystemString(TabelaCustosBMFMetadata.ColumnNames.Serie, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBMF.Emolumento
		/// </summary>
		virtual public System.Decimal? Emolumento
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.Emolumento);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.Emolumento, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBMF.EmolumentoDT
		/// </summary>
		virtual public System.Decimal? EmolumentoDT
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.EmolumentoDT);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.EmolumentoDT, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBMF.EmolumentoSocio
		/// </summary>
		virtual public System.Decimal? EmolumentoSocio
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.EmolumentoSocio);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.EmolumentoSocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBMF.EmolumentoDTSocio
		/// </summary>
		virtual public System.Decimal? EmolumentoDTSocio
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.EmolumentoDTSocio);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.EmolumentoDTSocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBMF.Registro
		/// </summary>
		virtual public System.Decimal? Registro
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.Registro);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.Registro, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBMF.RegistroDT
		/// </summary>
		virtual public System.Decimal? RegistroDT
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.RegistroDT);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.RegistroDT, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBMF.RegistroSocio
		/// </summary>
		virtual public System.Decimal? RegistroSocio
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.RegistroSocio);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.RegistroSocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBMF.RegistroDTSocio
		/// </summary>
		virtual public System.Decimal? RegistroDTSocio
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.RegistroDTSocio);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.RegistroDTSocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBMF.FatorReducao
		/// </summary>
		virtual public System.Decimal? FatorReducao
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.FatorReducao);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.FatorReducao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBMF.Permanencia
		/// </summary>
		virtual public System.Decimal? Permanencia
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.Permanencia);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBMFMetadata.ColumnNames.Permanencia, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBMF _UpToAtivoBMFByCdAtivoBMF;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaCustosBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Serie
			{
				get
				{
					System.String data = entity.Serie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Serie = null;
					else entity.Serie = Convert.ToString(value);
				}
			}
				
			public System.String Emolumento
			{
				get
				{
					System.Decimal? data = entity.Emolumento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Emolumento = null;
					else entity.Emolumento = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmolumentoDT
			{
				get
				{
					System.Decimal? data = entity.EmolumentoDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmolumentoDT = null;
					else entity.EmolumentoDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmolumentoSocio
			{
				get
				{
					System.Decimal? data = entity.EmolumentoSocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmolumentoSocio = null;
					else entity.EmolumentoSocio = Convert.ToDecimal(value);
				}
			}
				
			public System.String EmolumentoDTSocio
			{
				get
				{
					System.Decimal? data = entity.EmolumentoDTSocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmolumentoDTSocio = null;
					else entity.EmolumentoDTSocio = Convert.ToDecimal(value);
				}
			}
				
			public System.String Registro
			{
				get
				{
					System.Decimal? data = entity.Registro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Registro = null;
					else entity.Registro = Convert.ToDecimal(value);
				}
			}
				
			public System.String RegistroDT
			{
				get
				{
					System.Decimal? data = entity.RegistroDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RegistroDT = null;
					else entity.RegistroDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String RegistroSocio
			{
				get
				{
					System.Decimal? data = entity.RegistroSocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RegistroSocio = null;
					else entity.RegistroSocio = Convert.ToDecimal(value);
				}
			}
				
			public System.String RegistroDTSocio
			{
				get
				{
					System.Decimal? data = entity.RegistroDTSocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RegistroDTSocio = null;
					else entity.RegistroDTSocio = Convert.ToDecimal(value);
				}
			}
				
			public System.String FatorReducao
			{
				get
				{
					System.Decimal? data = entity.FatorReducao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FatorReducao = null;
					else entity.FatorReducao = Convert.ToDecimal(value);
				}
			}
				
			public System.String Permanencia
			{
				get
				{
					System.Decimal? data = entity.Permanencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Permanencia = null;
					else entity.Permanencia = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaCustosBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaCustosBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaCustosBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaCustosBMF : esTabelaCustosBMF
	{

				
		#region UpToAtivoBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBMF_TabelaCustosBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBMF UpToAtivoBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToAtivoBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					&& Serie != null					)
				{
					this._UpToAtivoBMFByCdAtivoBMF = new AtivoBMF();
					this._UpToAtivoBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.Serie == this.Serie);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToAtivoBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this.Serie = null;
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this.Serie = value.Serie;
					this._UpToAtivoBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaCustosBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCustosBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBMFMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Serie
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBMFMetadata.ColumnNames.Serie, esSystemType.String);
			}
		} 
		
		public esQueryItem Emolumento
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBMFMetadata.ColumnNames.Emolumento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem EmolumentoDT
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBMFMetadata.ColumnNames.EmolumentoDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem EmolumentoSocio
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBMFMetadata.ColumnNames.EmolumentoSocio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem EmolumentoDTSocio
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBMFMetadata.ColumnNames.EmolumentoDTSocio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Registro
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBMFMetadata.ColumnNames.Registro, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RegistroDT
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBMFMetadata.ColumnNames.RegistroDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RegistroSocio
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBMFMetadata.ColumnNames.RegistroSocio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RegistroDTSocio
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBMFMetadata.ColumnNames.RegistroDTSocio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem FatorReducao
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBMFMetadata.ColumnNames.FatorReducao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Permanencia
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBMFMetadata.ColumnNames.Permanencia, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaCustosBMFCollection")]
	public partial class TabelaCustosBMFCollection : esTabelaCustosBMFCollection, IEnumerable<TabelaCustosBMF>
	{
		public TabelaCustosBMFCollection()
		{

		}
		
		public static implicit operator List<TabelaCustosBMF>(TabelaCustosBMFCollection coll)
		{
			List<TabelaCustosBMF> list = new List<TabelaCustosBMF>();
			
			foreach (TabelaCustosBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaCustosBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCustosBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaCustosBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaCustosBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaCustosBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCustosBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaCustosBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaCustosBMF AddNew()
		{
			TabelaCustosBMF entity = base.AddNewEntity() as TabelaCustosBMF;
			
			return entity;
		}

		public TabelaCustosBMF FindByPrimaryKey(System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			return base.FindByPrimaryKey(dataReferencia, cdAtivoBMF, serie) as TabelaCustosBMF;
		}


		#region IEnumerable<TabelaCustosBMF> Members

		IEnumerator<TabelaCustosBMF> IEnumerable<TabelaCustosBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaCustosBMF;
			}
		}

		#endregion
		
		private TabelaCustosBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaCustosBMF' table
	/// </summary>

	[Serializable]
	public partial class TabelaCustosBMF : esTabelaCustosBMF
	{
		public TabelaCustosBMF()
		{

		}
	
		public TabelaCustosBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCustosBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaCustosBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCustosBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaCustosBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCustosBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaCustosBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaCustosBMFQuery query;
	}



	[Serializable]
	public partial class TabelaCustosBMFQuery : esTabelaCustosBMFQuery
	{
		public TabelaCustosBMFQuery()
		{

		}		
		
		public TabelaCustosBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaCustosBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaCustosBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaCustosBMFMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaCustosBMFMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBMFMetadata.ColumnNames.CdAtivoBMF, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaCustosBMFMetadata.PropertyNames.CdAtivoBMF;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBMFMetadata.ColumnNames.Serie, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaCustosBMFMetadata.PropertyNames.Serie;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBMFMetadata.ColumnNames.Emolumento, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBMFMetadata.PropertyNames.Emolumento;	
			c.NumericPrecision = 16;
			c.NumericScale = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBMFMetadata.ColumnNames.EmolumentoDT, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBMFMetadata.PropertyNames.EmolumentoDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBMFMetadata.ColumnNames.EmolumentoSocio, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBMFMetadata.PropertyNames.EmolumentoSocio;	
			c.NumericPrecision = 16;
			c.NumericScale = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBMFMetadata.ColumnNames.EmolumentoDTSocio, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBMFMetadata.PropertyNames.EmolumentoDTSocio;	
			c.NumericPrecision = 16;
			c.NumericScale = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBMFMetadata.ColumnNames.Registro, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBMFMetadata.PropertyNames.Registro;	
			c.NumericPrecision = 16;
			c.NumericScale = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBMFMetadata.ColumnNames.RegistroDT, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBMFMetadata.PropertyNames.RegistroDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBMFMetadata.ColumnNames.RegistroSocio, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBMFMetadata.PropertyNames.RegistroSocio;	
			c.NumericPrecision = 16;
			c.NumericScale = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBMFMetadata.ColumnNames.RegistroDTSocio, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBMFMetadata.PropertyNames.RegistroDTSocio;	
			c.NumericPrecision = 16;
			c.NumericScale = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBMFMetadata.ColumnNames.FatorReducao, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBMFMetadata.PropertyNames.FatorReducao;	
			c.NumericPrecision = 16;
			c.NumericScale = 7;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBMFMetadata.ColumnNames.Permanencia, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBMFMetadata.PropertyNames.Permanencia;	
			c.NumericPrecision = 16;
			c.NumericScale = 7;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaCustosBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string Emolumento = "Emolumento";
			 public const string EmolumentoDT = "EmolumentoDT";
			 public const string EmolumentoSocio = "EmolumentoSocio";
			 public const string EmolumentoDTSocio = "EmolumentoDTSocio";
			 public const string Registro = "Registro";
			 public const string RegistroDT = "RegistroDT";
			 public const string RegistroSocio = "RegistroSocio";
			 public const string RegistroDTSocio = "RegistroDTSocio";
			 public const string FatorReducao = "FatorReducao";
			 public const string Permanencia = "Permanencia";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string Emolumento = "Emolumento";
			 public const string EmolumentoDT = "EmolumentoDT";
			 public const string EmolumentoSocio = "EmolumentoSocio";
			 public const string EmolumentoDTSocio = "EmolumentoDTSocio";
			 public const string Registro = "Registro";
			 public const string RegistroDT = "RegistroDT";
			 public const string RegistroSocio = "RegistroSocio";
			 public const string RegistroDTSocio = "RegistroDTSocio";
			 public const string FatorReducao = "FatorReducao";
			 public const string Permanencia = "Permanencia";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaCustosBMFMetadata))
			{
				if(TabelaCustosBMFMetadata.mapDelegates == null)
				{
					TabelaCustosBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaCustosBMFMetadata.meta == null)
				{
					TabelaCustosBMFMetadata.meta = new TabelaCustosBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Serie", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Emolumento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("EmolumentoDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("EmolumentoSocio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("EmolumentoDTSocio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Registro", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RegistroDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RegistroSocio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RegistroDTSocio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("FatorReducao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Permanencia", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaCustosBMF";
				meta.Destination = "TabelaCustosBMF";
				
				meta.spInsert = "proc_TabelaCustosBMFInsert";				
				meta.spUpdate = "proc_TabelaCustosBMFUpdate";		
				meta.spDelete = "proc_TabelaCustosBMFDelete";
				meta.spLoadAll = "proc_TabelaCustosBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaCustosBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaCustosBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
