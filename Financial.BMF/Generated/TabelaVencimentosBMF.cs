/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2012 19:02:06
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;


















			



















		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esTabelaVencimentosBMFCollection : esEntityCollection
	{
		public esTabelaVencimentosBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaVencimentosBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaVencimentosBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaVencimentosBMFQuery);
		}
		#endregion
		
		virtual public TabelaVencimentosBMF DetachEntity(TabelaVencimentosBMF entity)
		{
			return base.DetachEntity(entity) as TabelaVencimentosBMF;
		}
		
		virtual public TabelaVencimentosBMF AttachEntity(TabelaVencimentosBMF entity)
		{
			return base.AttachEntity(entity) as TabelaVencimentosBMF;
		}
		
		virtual public void Combine(TabelaVencimentosBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaVencimentosBMF this[int index]
		{
			get
			{
				return base[index] as TabelaVencimentosBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaVencimentosBMF);
		}
	}



	[Serializable]
	abstract public class esTabelaVencimentosBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaVencimentosBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaVencimentosBMF()
		{

		}

		public esTabelaVencimentosBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, cdAtivoBMF, serie);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, cdAtivoBMF, serie);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaVencimentosBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.CdAtivoBMF == cdAtivoBMF, query.Serie == serie);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, cdAtivoBMF, serie);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, cdAtivoBMF, serie);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			esTabelaVencimentosBMFQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.CdAtivoBMF == cdAtivoBMF, query.Serie == serie);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("CdAtivoBMF",cdAtivoBMF);			parms.Add("Serie",serie);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Serie": this.str.Serie = (string)value; break;							
						case "DiasUteis": this.str.DiasUteis = (string)value; break;							
						case "DiasCorridos": this.str.DiasCorridos = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "DiasUteis":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DiasUteis = (System.Int32?)value;
							break;
						
						case "DiasCorridos":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DiasCorridos = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaVencimentosBMF.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaVencimentosBMFMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaVencimentosBMFMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaVencimentosBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(TabelaVencimentosBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(TabelaVencimentosBMFMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaVencimentosBMF.Serie
		/// </summary>
		virtual public System.String Serie
		{
			get
			{
				return base.GetSystemString(TabelaVencimentosBMFMetadata.ColumnNames.Serie);
			}
			
			set
			{
				if(base.SetSystemString(TabelaVencimentosBMFMetadata.ColumnNames.Serie, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaVencimentosBMF.DiasUteis
		/// </summary>
		virtual public System.Int32? DiasUteis
		{
			get
			{
				return base.GetSystemInt32(TabelaVencimentosBMFMetadata.ColumnNames.DiasUteis);
			}
			
			set
			{
				base.SetSystemInt32(TabelaVencimentosBMFMetadata.ColumnNames.DiasUteis, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaVencimentosBMF.DiasCorridos
		/// </summary>
		virtual public System.Int32? DiasCorridos
		{
			get
			{
				return base.GetSystemInt32(TabelaVencimentosBMFMetadata.ColumnNames.DiasCorridos);
			}
			
			set
			{
				base.SetSystemInt32(TabelaVencimentosBMFMetadata.ColumnNames.DiasCorridos, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBMF _UpToAtivoBMFByCdAtivoBMF;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaVencimentosBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Serie
			{
				get
				{
					System.String data = entity.Serie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Serie = null;
					else entity.Serie = Convert.ToString(value);
				}
			}
				
			public System.String DiasUteis
			{
				get
				{
					System.Int32? data = entity.DiasUteis;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasUteis = null;
					else entity.DiasUteis = Convert.ToInt32(value);
				}
			}
				
			public System.String DiasCorridos
			{
				get
				{
					System.Int32? data = entity.DiasCorridos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasCorridos = null;
					else entity.DiasCorridos = Convert.ToInt32(value);
				}
			}
			

			private esTabelaVencimentosBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaVencimentosBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaVencimentosBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaVencimentosBMF : esTabelaVencimentosBMF
	{

				
		#region UpToAtivoBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBMF_TabelaVencimentosBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBMF UpToAtivoBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToAtivoBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					&& Serie != null					)
				{
					this._UpToAtivoBMFByCdAtivoBMF = new AtivoBMF();
					this._UpToAtivoBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.Serie == this.Serie);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToAtivoBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this.Serie = null;
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this.Serie = value.Serie;
					this._UpToAtivoBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaVencimentosBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaVencimentosBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaVencimentosBMFMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, TabelaVencimentosBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Serie
		{
			get
			{
				return new esQueryItem(this, TabelaVencimentosBMFMetadata.ColumnNames.Serie, esSystemType.String);
			}
		} 
		
		public esQueryItem DiasUteis
		{
			get
			{
				return new esQueryItem(this, TabelaVencimentosBMFMetadata.ColumnNames.DiasUteis, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DiasCorridos
		{
			get
			{
				return new esQueryItem(this, TabelaVencimentosBMFMetadata.ColumnNames.DiasCorridos, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaVencimentosBMFCollection")]
	public partial class TabelaVencimentosBMFCollection : esTabelaVencimentosBMFCollection, IEnumerable<TabelaVencimentosBMF>
	{
		public TabelaVencimentosBMFCollection()
		{

		}
		
		public static implicit operator List<TabelaVencimentosBMF>(TabelaVencimentosBMFCollection coll)
		{
			List<TabelaVencimentosBMF> list = new List<TabelaVencimentosBMF>();
			
			foreach (TabelaVencimentosBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaVencimentosBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaVencimentosBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaVencimentosBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaVencimentosBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaVencimentosBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaVencimentosBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaVencimentosBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaVencimentosBMF AddNew()
		{
			TabelaVencimentosBMF entity = base.AddNewEntity() as TabelaVencimentosBMF;
			
			return entity;
		}

		public TabelaVencimentosBMF FindByPrimaryKey(System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			return base.FindByPrimaryKey(dataReferencia, cdAtivoBMF, serie) as TabelaVencimentosBMF;
		}


		#region IEnumerable<TabelaVencimentosBMF> Members

		IEnumerator<TabelaVencimentosBMF> IEnumerable<TabelaVencimentosBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaVencimentosBMF;
			}
		}

		#endregion
		
		private TabelaVencimentosBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaVencimentosBMF' table
	/// </summary>

	[Serializable]
	public partial class TabelaVencimentosBMF : esTabelaVencimentosBMF
	{
		public TabelaVencimentosBMF()
		{

		}
	
		public TabelaVencimentosBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaVencimentosBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaVencimentosBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaVencimentosBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaVencimentosBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaVencimentosBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaVencimentosBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaVencimentosBMFQuery query;
	}



	[Serializable]
	public partial class TabelaVencimentosBMFQuery : esTabelaVencimentosBMFQuery
	{
		public TabelaVencimentosBMFQuery()
		{

		}		
		
		public TabelaVencimentosBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaVencimentosBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaVencimentosBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaVencimentosBMFMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaVencimentosBMFMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaVencimentosBMFMetadata.ColumnNames.CdAtivoBMF, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaVencimentosBMFMetadata.PropertyNames.CdAtivoBMF;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaVencimentosBMFMetadata.ColumnNames.Serie, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaVencimentosBMFMetadata.PropertyNames.Serie;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaVencimentosBMFMetadata.ColumnNames.DiasUteis, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaVencimentosBMFMetadata.PropertyNames.DiasUteis;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaVencimentosBMFMetadata.ColumnNames.DiasCorridos, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaVencimentosBMFMetadata.PropertyNames.DiasCorridos;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaVencimentosBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string DiasUteis = "DiasUteis";
			 public const string DiasCorridos = "DiasCorridos";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string DiasUteis = "DiasUteis";
			 public const string DiasCorridos = "DiasCorridos";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaVencimentosBMFMetadata))
			{
				if(TabelaVencimentosBMFMetadata.mapDelegates == null)
				{
					TabelaVencimentosBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaVencimentosBMFMetadata.meta == null)
				{
					TabelaVencimentosBMFMetadata.meta = new TabelaVencimentosBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Serie", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DiasUteis", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DiasCorridos", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "TabelaVencimentosBMF";
				meta.Destination = "TabelaVencimentosBMF";
				
				meta.spInsert = "proc_TabelaVencimentosBMFInsert";				
				meta.spUpdate = "proc_TabelaVencimentosBMFUpdate";		
				meta.spDelete = "proc_TabelaVencimentosBMFDelete";
				meta.spLoadAll = "proc_TabelaVencimentosBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaVencimentosBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaVencimentosBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
