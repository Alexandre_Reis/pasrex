/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2012 19:02:06
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;












using Financial.Common;
using Financial.Investidor;										


























		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esTransferenciaBMFCollection : esEntityCollection
	{
		public esTransferenciaBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TransferenciaBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esTransferenciaBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTransferenciaBMFQuery);
		}
		#endregion
		
		virtual public TransferenciaBMF DetachEntity(TransferenciaBMF entity)
		{
			return base.DetachEntity(entity) as TransferenciaBMF;
		}
		
		virtual public TransferenciaBMF AttachEntity(TransferenciaBMF entity)
		{
			return base.AttachEntity(entity) as TransferenciaBMF;
		}
		
		virtual public void Combine(TransferenciaBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TransferenciaBMF this[int index]
		{
			get
			{
				return base[index] as TransferenciaBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TransferenciaBMF);
		}
	}



	[Serializable]
	abstract public class esTransferenciaBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTransferenciaBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esTransferenciaBMF()
		{

		}

		public esTransferenciaBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTransferencia)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTransferencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idTransferencia);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTransferencia)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTransferenciaBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTransferencia == idTransferencia);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTransferencia)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTransferencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idTransferencia);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTransferencia)
		{
			esTransferenciaBMFQuery query = this.GetDynamicQuery();
			query.Where(query.IdTransferencia == idTransferencia);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTransferencia)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTransferencia",idTransferencia);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTransferencia": this.str.IdTransferencia = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Serie": this.str.Serie = (string)value; break;							
						case "IdAgenteOrigem": this.str.IdAgenteOrigem = (string)value; break;							
						case "IdAgenteDestino": this.str.IdAgenteDestino = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTransferencia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTransferencia = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgenteOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteOrigem = (System.Int32?)value;
							break;
						
						case "IdAgenteDestino":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteDestino = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Quantidade = (System.Int32?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TransferenciaBMF.IdTransferencia
		/// </summary>
		virtual public System.Int32? IdTransferencia
		{
			get
			{
				return base.GetSystemInt32(TransferenciaBMFMetadata.ColumnNames.IdTransferencia);
			}
			
			set
			{
				base.SetSystemInt32(TransferenciaBMFMetadata.ColumnNames.IdTransferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaBMF.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(TransferenciaBMFMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(TransferenciaBMFMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(TransferenciaBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(TransferenciaBMFMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaBMF.Serie
		/// </summary>
		virtual public System.String Serie
		{
			get
			{
				return base.GetSystemString(TransferenciaBMFMetadata.ColumnNames.Serie);
			}
			
			set
			{
				if(base.SetSystemString(TransferenciaBMFMetadata.ColumnNames.Serie, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaBMF.IdAgenteOrigem
		/// </summary>
		virtual public System.Int32? IdAgenteOrigem
		{
			get
			{
				return base.GetSystemInt32(TransferenciaBMFMetadata.ColumnNames.IdAgenteOrigem);
			}
			
			set
			{
				if(base.SetSystemInt32(TransferenciaBMFMetadata.ColumnNames.IdAgenteOrigem, value))
				{
					this._UpToAgenteMercadoByIdAgenteOrigem = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaBMF.IdAgenteDestino
		/// </summary>
		virtual public System.Int32? IdAgenteDestino
		{
			get
			{
				return base.GetSystemInt32(TransferenciaBMFMetadata.ColumnNames.IdAgenteDestino);
			}
			
			set
			{
				if(base.SetSystemInt32(TransferenciaBMFMetadata.ColumnNames.IdAgenteDestino, value))
				{
					this._UpToAgenteMercadoByIdAgenteDestino = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaBMF.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(TransferenciaBMFMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(TransferenciaBMFMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaBMF.Quantidade
		/// </summary>
		virtual public System.Int32? Quantidade
		{
			get
			{
				return base.GetSystemInt32(TransferenciaBMFMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemInt32(TransferenciaBMFMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaBMF.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(TransferenciaBMFMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(TransferenciaBMFMetadata.ColumnNames.Pu, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteOrigem;
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteDestino;
		[CLSCompliant(false)]
		internal protected AtivoBMF _UpToAtivoBMFByCdAtivoBMF;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTransferenciaBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTransferencia
			{
				get
				{
					System.Int32? data = entity.IdTransferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTransferencia = null;
					else entity.IdTransferencia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Serie
			{
				get
				{
					System.String data = entity.Serie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Serie = null;
					else entity.Serie = Convert.ToString(value);
				}
			}
				
			public System.String IdAgenteOrigem
			{
				get
				{
					System.Int32? data = entity.IdAgenteOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteOrigem = null;
					else entity.IdAgenteOrigem = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteDestino
			{
				get
				{
					System.Int32? data = entity.IdAgenteDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteDestino = null;
					else entity.IdAgenteDestino = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Int32? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToInt32(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
			

			private esTransferenciaBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTransferenciaBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTransferenciaBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TransferenciaBMF : esTransferenciaBMF
	{

				
		#region UpToAgenteMercadoByIdAgenteOrigem - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_TransferenciaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteOrigem
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteOrigem == null
					&& IdAgenteOrigem != null					)
				{
					this._UpToAgenteMercadoByIdAgenteOrigem = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteOrigem", this._UpToAgenteMercadoByIdAgenteOrigem);
					this._UpToAgenteMercadoByIdAgenteOrigem.Query.Where(this._UpToAgenteMercadoByIdAgenteOrigem.Query.IdAgente == this.IdAgenteOrigem);
					this._UpToAgenteMercadoByIdAgenteOrigem.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteOrigem;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteOrigem");
				

				if(value == null)
				{
					this.IdAgenteOrigem = null;
					this._UpToAgenteMercadoByIdAgenteOrigem = null;
				}
				else
				{
					this.IdAgenteOrigem = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteOrigem = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteOrigem", this._UpToAgenteMercadoByIdAgenteOrigem);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAgenteMercadoByIdAgenteDestino - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_TransferenciaBMF_FK2
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteDestino
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteDestino == null
					&& IdAgenteDestino != null					)
				{
					this._UpToAgenteMercadoByIdAgenteDestino = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteDestino", this._UpToAgenteMercadoByIdAgenteDestino);
					this._UpToAgenteMercadoByIdAgenteDestino.Query.Where(this._UpToAgenteMercadoByIdAgenteDestino.Query.IdAgente == this.IdAgenteDestino);
					this._UpToAgenteMercadoByIdAgenteDestino.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteDestino;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteDestino");
				

				if(value == null)
				{
					this.IdAgenteDestino = null;
					this._UpToAgenteMercadoByIdAgenteDestino = null;
				}
				else
				{
					this.IdAgenteDestino = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteDestino = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteDestino", this._UpToAgenteMercadoByIdAgenteDestino);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBMF_TransferenciaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBMF UpToAtivoBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToAtivoBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					&& Serie != null					)
				{
					this._UpToAtivoBMFByCdAtivoBMF = new AtivoBMF();
					this._UpToAtivoBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.Serie == this.Serie);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToAtivoBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this.Serie = null;
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this.Serie = value.Serie;
					this._UpToAtivoBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_TransferenciaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteOrigem != null)
			{
				this.IdAgenteOrigem = this._UpToAgenteMercadoByIdAgenteOrigem.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteDestino != null)
			{
				this.IdAgenteDestino = this._UpToAgenteMercadoByIdAgenteDestino.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTransferenciaBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TransferenciaBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTransferencia
		{
			get
			{
				return new esQueryItem(this, TransferenciaBMFMetadata.ColumnNames.IdTransferencia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, TransferenciaBMFMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, TransferenciaBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Serie
		{
			get
			{
				return new esQueryItem(this, TransferenciaBMFMetadata.ColumnNames.Serie, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAgenteOrigem
		{
			get
			{
				return new esQueryItem(this, TransferenciaBMFMetadata.ColumnNames.IdAgenteOrigem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteDestino
		{
			get
			{
				return new esQueryItem(this, TransferenciaBMFMetadata.ColumnNames.IdAgenteDestino, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, TransferenciaBMFMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, TransferenciaBMFMetadata.ColumnNames.Quantidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, TransferenciaBMFMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TransferenciaBMFCollection")]
	public partial class TransferenciaBMFCollection : esTransferenciaBMFCollection, IEnumerable<TransferenciaBMF>
	{
		public TransferenciaBMFCollection()
		{

		}
		
		public static implicit operator List<TransferenciaBMF>(TransferenciaBMFCollection coll)
		{
			List<TransferenciaBMF> list = new List<TransferenciaBMF>();
			
			foreach (TransferenciaBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TransferenciaBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TransferenciaBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TransferenciaBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TransferenciaBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TransferenciaBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TransferenciaBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TransferenciaBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TransferenciaBMF AddNew()
		{
			TransferenciaBMF entity = base.AddNewEntity() as TransferenciaBMF;
			
			return entity;
		}

		public TransferenciaBMF FindByPrimaryKey(System.Int32 idTransferencia)
		{
			return base.FindByPrimaryKey(idTransferencia) as TransferenciaBMF;
		}


		#region IEnumerable<TransferenciaBMF> Members

		IEnumerator<TransferenciaBMF> IEnumerable<TransferenciaBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TransferenciaBMF;
			}
		}

		#endregion
		
		private TransferenciaBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TransferenciaBMF' table
	/// </summary>

	[Serializable]
	public partial class TransferenciaBMF : esTransferenciaBMF
	{
		public TransferenciaBMF()
		{

		}
	
		public TransferenciaBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TransferenciaBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esTransferenciaBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TransferenciaBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TransferenciaBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TransferenciaBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TransferenciaBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TransferenciaBMFQuery query;
	}



	[Serializable]
	public partial class TransferenciaBMFQuery : esTransferenciaBMFQuery
	{
		public TransferenciaBMFQuery()
		{

		}		
		
		public TransferenciaBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TransferenciaBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TransferenciaBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TransferenciaBMFMetadata.ColumnNames.IdTransferencia, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaBMFMetadata.PropertyNames.IdTransferencia;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaBMFMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaBMFMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaBMFMetadata.ColumnNames.CdAtivoBMF, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TransferenciaBMFMetadata.PropertyNames.CdAtivoBMF;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaBMFMetadata.ColumnNames.Serie, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TransferenciaBMFMetadata.PropertyNames.Serie;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaBMFMetadata.ColumnNames.IdAgenteOrigem, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaBMFMetadata.PropertyNames.IdAgenteOrigem;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaBMFMetadata.ColumnNames.IdAgenteDestino, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaBMFMetadata.PropertyNames.IdAgenteDestino;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaBMFMetadata.ColumnNames.Data, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TransferenciaBMFMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaBMFMetadata.ColumnNames.Quantidade, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaBMFMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaBMFMetadata.ColumnNames.Pu, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TransferenciaBMFMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 20;
			c.NumericScale = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TransferenciaBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTransferencia = "IdTransferencia";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string IdAgenteOrigem = "IdAgenteOrigem";
			 public const string IdAgenteDestino = "IdAgenteDestino";
			 public const string Data = "Data";
			 public const string Quantidade = "Quantidade";
			 public const string Pu = "PU";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTransferencia = "IdTransferencia";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string IdAgenteOrigem = "IdAgenteOrigem";
			 public const string IdAgenteDestino = "IdAgenteDestino";
			 public const string Data = "Data";
			 public const string Quantidade = "Quantidade";
			 public const string Pu = "Pu";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TransferenciaBMFMetadata))
			{
				if(TransferenciaBMFMetadata.mapDelegates == null)
				{
					TransferenciaBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TransferenciaBMFMetadata.meta == null)
				{
					TransferenciaBMFMetadata.meta = new TransferenciaBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTransferencia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Serie", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAgenteOrigem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteDestino", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TransferenciaBMF";
				meta.Destination = "TransferenciaBMF";
				
				meta.spInsert = "proc_TransferenciaBMFInsert";				
				meta.spUpdate = "proc_TransferenciaBMFUpdate";		
				meta.spDelete = "proc_TransferenciaBMFDelete";
				meta.spLoadAll = "proc_TransferenciaBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_TransferenciaBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TransferenciaBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
