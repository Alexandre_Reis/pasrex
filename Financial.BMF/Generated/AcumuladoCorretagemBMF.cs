/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2012 19:01:58
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;













using Financial.Common;
using Financial.Investidor;										

























		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esAcumuladoCorretagemBMFCollection : esEntityCollection
	{
		public esAcumuladoCorretagemBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AcumuladoCorretagemBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esAcumuladoCorretagemBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAcumuladoCorretagemBMFQuery);
		}
		#endregion
		
		virtual public AcumuladoCorretagemBMF DetachEntity(AcumuladoCorretagemBMF entity)
		{
			return base.DetachEntity(entity) as AcumuladoCorretagemBMF;
		}
		
		virtual public AcumuladoCorretagemBMF AttachEntity(AcumuladoCorretagemBMF entity)
		{
			return base.AttachEntity(entity) as AcumuladoCorretagemBMF;
		}
		
		virtual public void Combine(AcumuladoCorretagemBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public AcumuladoCorretagemBMF this[int index]
		{
			get
			{
				return base[index] as AcumuladoCorretagemBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(AcumuladoCorretagemBMF);
		}
	}



	[Serializable]
	abstract public class esAcumuladoCorretagemBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAcumuladoCorretagemBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esAcumuladoCorretagemBMF()
		{

		}

		public esAcumuladoCorretagemBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente, System.Int32 idAgente, System.String cdAtivoBMF, System.DateTime data)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idAgente, cdAtivoBMF, data);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idAgente, cdAtivoBMF, data);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente, System.Int32 idAgente, System.String cdAtivoBMF, System.DateTime data)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esAcumuladoCorretagemBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente, query.IdAgente == idAgente, query.CdAtivoBMF == cdAtivoBMF, query.Data == data);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente, System.Int32 idAgente, System.String cdAtivoBMF, System.DateTime data)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idAgente, cdAtivoBMF, data);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idAgente, cdAtivoBMF, data);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente, System.Int32 idAgente, System.String cdAtivoBMF, System.DateTime data)
		{
			esAcumuladoCorretagemBMFQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente, query.IdAgente == idAgente, query.CdAtivoBMF == cdAtivoBMF, query.Data == data);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente, System.Int32 idAgente, System.String cdAtivoBMF, System.DateTime data)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);			parms.Add("IdAgente",idAgente);			parms.Add("CdAtivoBMF",cdAtivoBMF);			parms.Add("Data",data);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "SaldoAcumulado": this.str.SaldoAcumulado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "SaldoAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.SaldoAcumulado = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to AcumuladoCorretagemBMF.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(AcumuladoCorretagemBMFMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(AcumuladoCorretagemBMFMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AcumuladoCorretagemBMF.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(AcumuladoCorretagemBMFMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(AcumuladoCorretagemBMFMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AcumuladoCorretagemBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(AcumuladoCorretagemBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(AcumuladoCorretagemBMFMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToClasseBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AcumuladoCorretagemBMF.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(AcumuladoCorretagemBMFMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(AcumuladoCorretagemBMFMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to AcumuladoCorretagemBMF.SaldoAcumulado
		/// </summary>
		virtual public System.Decimal? SaldoAcumulado
		{
			get
			{
				return base.GetSystemDecimal(AcumuladoCorretagemBMFMetadata.ColumnNames.SaldoAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(AcumuladoCorretagemBMFMetadata.ColumnNames.SaldoAcumulado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected ClasseBMF _UpToClasseBMFByCdAtivoBMF;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAcumuladoCorretagemBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String SaldoAcumulado
			{
				get
				{
					System.Decimal? data = entity.SaldoAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SaldoAcumulado = null;
					else entity.SaldoAcumulado = Convert.ToDecimal(value);
				}
			}
			

			private esAcumuladoCorretagemBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAcumuladoCorretagemBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAcumuladoCorretagemBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class AcumuladoCorretagemBMF : esAcumuladoCorretagemBMF
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_CorretagemAcumuladaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClasseBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ClasseBMF_CorretagemAcumuladaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public ClasseBMF UpToClasseBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToClasseBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					)
				{
					this._UpToClasseBMFByCdAtivoBMF = new ClasseBMF();
					this._UpToClasseBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClasseBMFByCdAtivoBMF", this._UpToClasseBMFByCdAtivoBMF);
					this._UpToClasseBMFByCdAtivoBMF.Query.Where(this._UpToClasseBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToClasseBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToClasseBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToClasseBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this._UpToClasseBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this._UpToClasseBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToClasseBMFByCdAtivoBMF", this._UpToClasseBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_AcumuladoCorretagemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAcumuladoCorretagemBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AcumuladoCorretagemBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, AcumuladoCorretagemBMFMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, AcumuladoCorretagemBMFMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, AcumuladoCorretagemBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, AcumuladoCorretagemBMFMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem SaldoAcumulado
		{
			get
			{
				return new esQueryItem(this, AcumuladoCorretagemBMFMetadata.ColumnNames.SaldoAcumulado, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("AcumuladoCorretagemBMFCollection")]
	public partial class AcumuladoCorretagemBMFCollection : esAcumuladoCorretagemBMFCollection, IEnumerable<AcumuladoCorretagemBMF>
	{
		public AcumuladoCorretagemBMFCollection()
		{

		}
		
		public static implicit operator List<AcumuladoCorretagemBMF>(AcumuladoCorretagemBMFCollection coll)
		{
			List<AcumuladoCorretagemBMF> list = new List<AcumuladoCorretagemBMF>();
			
			foreach (AcumuladoCorretagemBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AcumuladoCorretagemBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AcumuladoCorretagemBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new AcumuladoCorretagemBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new AcumuladoCorretagemBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AcumuladoCorretagemBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AcumuladoCorretagemBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AcumuladoCorretagemBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public AcumuladoCorretagemBMF AddNew()
		{
			AcumuladoCorretagemBMF entity = base.AddNewEntity() as AcumuladoCorretagemBMF;
			
			return entity;
		}

		public AcumuladoCorretagemBMF FindByPrimaryKey(System.Int32 idCliente, System.Int32 idAgente, System.String cdAtivoBMF, System.DateTime data)
		{
			return base.FindByPrimaryKey(idCliente, idAgente, cdAtivoBMF, data) as AcumuladoCorretagemBMF;
		}


		#region IEnumerable<AcumuladoCorretagemBMF> Members

		IEnumerator<AcumuladoCorretagemBMF> IEnumerable<AcumuladoCorretagemBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as AcumuladoCorretagemBMF;
			}
		}

		#endregion
		
		private AcumuladoCorretagemBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'AcumuladoCorretagemBMF' table
	/// </summary>

	[Serializable]
	public partial class AcumuladoCorretagemBMF : esAcumuladoCorretagemBMF
	{
		public AcumuladoCorretagemBMF()
		{

		}
	
		public AcumuladoCorretagemBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AcumuladoCorretagemBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esAcumuladoCorretagemBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AcumuladoCorretagemBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AcumuladoCorretagemBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AcumuladoCorretagemBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AcumuladoCorretagemBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AcumuladoCorretagemBMFQuery query;
	}



	[Serializable]
	public partial class AcumuladoCorretagemBMFQuery : esAcumuladoCorretagemBMFQuery
	{
		public AcumuladoCorretagemBMFQuery()
		{

		}		
		
		public AcumuladoCorretagemBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AcumuladoCorretagemBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AcumuladoCorretagemBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AcumuladoCorretagemBMFMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AcumuladoCorretagemBMFMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AcumuladoCorretagemBMFMetadata.ColumnNames.IdAgente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AcumuladoCorretagemBMFMetadata.PropertyNames.IdAgente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AcumuladoCorretagemBMFMetadata.ColumnNames.CdAtivoBMF, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = AcumuladoCorretagemBMFMetadata.PropertyNames.CdAtivoBMF;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AcumuladoCorretagemBMFMetadata.ColumnNames.Data, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AcumuladoCorretagemBMFMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AcumuladoCorretagemBMFMetadata.ColumnNames.SaldoAcumulado, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AcumuladoCorretagemBMFMetadata.PropertyNames.SaldoAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public AcumuladoCorretagemBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Data = "Data";
			 public const string SaldoAcumulado = "SaldoAcumulado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Data = "Data";
			 public const string SaldoAcumulado = "SaldoAcumulado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AcumuladoCorretagemBMFMetadata))
			{
				if(AcumuladoCorretagemBMFMetadata.mapDelegates == null)
				{
					AcumuladoCorretagemBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AcumuladoCorretagemBMFMetadata.meta == null)
				{
					AcumuladoCorretagemBMFMetadata.meta = new AcumuladoCorretagemBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("SaldoAcumulado", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "AcumuladoCorretagemBMF";
				meta.Destination = "AcumuladoCorretagemBMF";
				
				meta.spInsert = "proc_AcumuladoCorretagemBMFInsert";				
				meta.spUpdate = "proc_AcumuladoCorretagemBMFUpdate";		
				meta.spDelete = "proc_AcumuladoCorretagemBMFDelete";
				meta.spLoadAll = "proc_AcumuladoCorretagemBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_AcumuladoCorretagemBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AcumuladoCorretagemBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
