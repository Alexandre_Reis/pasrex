/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2012 19:02:04
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





using Financial.Common;
using Financial.Investidor;

































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esCustodiaBMFCollection : esEntityCollection
	{
		public esCustodiaBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CustodiaBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esCustodiaBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCustodiaBMFQuery);
		}
		#endregion
		
		virtual public CustodiaBMF DetachEntity(CustodiaBMF entity)
		{
			return base.DetachEntity(entity) as CustodiaBMF;
		}
		
		virtual public CustodiaBMF AttachEntity(CustodiaBMF entity)
		{
			return base.AttachEntity(entity) as CustodiaBMF;
		}
		
		virtual public void Combine(CustodiaBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CustodiaBMF this[int index]
		{
			get
			{
				return base[index] as CustodiaBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CustodiaBMF);
		}
	}



	[Serializable]
	abstract public class esCustodiaBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCustodiaBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esCustodiaBMF()
		{

		}

		public esCustodiaBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int32 idCliente, System.String cdAtivoBMF, System.Int32 idAgente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idCliente, cdAtivoBMF, idAgente);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idCliente, cdAtivoBMF, idAgente);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime data, System.Int32 idCliente, System.String cdAtivoBMF, System.Int32 idAgente)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCustodiaBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Data == data, query.IdCliente == idCliente, query.CdAtivoBMF == cdAtivoBMF, query.IdAgente == idAgente);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int32 idCliente, System.String cdAtivoBMF, System.Int32 idAgente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idCliente, cdAtivoBMF, idAgente);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idCliente, cdAtivoBMF, idAgente);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int32 idCliente, System.String cdAtivoBMF, System.Int32 idAgente)
		{
			esCustodiaBMFQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.IdCliente == idCliente, query.CdAtivoBMF == cdAtivoBMF, query.IdAgente == idAgente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int32 idCliente, System.String cdAtivoBMF, System.Int32 idAgente)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("IdCliente",idCliente);			parms.Add("CdAtivoBMF",cdAtivoBMF);			parms.Add("IdAgente",idAgente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "ValorDiario": this.str.ValorDiario = (string)value; break;							
						case "ValorAcumulado": this.str.ValorAcumulado = (string)value; break;							
						case "ValorPagar": this.str.ValorPagar = (string)value; break;							
						case "DataPagamento": this.str.DataPagamento = (string)value; break;
                        case "IdConta": this.str.IdConta = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "ValorDiario":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorDiario = (System.Decimal?)value;
							break;
						
						case "ValorAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAcumulado = (System.Decimal?)value;
							break;
						
						case "ValorPagar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPagar = (System.Decimal?)value;
							break;
						
						case "DataPagamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPagamento = (System.DateTime?)value;
							break;

                        case "IdConta":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdConta = (System.Int32?)value;
                            break;

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CustodiaBMF.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(CustodiaBMFMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(CustodiaBMFMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to CustodiaBMF.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(CustodiaBMFMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(CustodiaBMFMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CustodiaBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(CustodiaBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(CustodiaBMFMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToClasseBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CustodiaBMF.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(CustodiaBMFMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(CustodiaBMFMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CustodiaBMF.ValorDiario
		/// </summary>
		virtual public System.Decimal? ValorDiario
		{
			get
			{
				return base.GetSystemDecimal(CustodiaBMFMetadata.ColumnNames.ValorDiario);
			}
			
			set
			{
				base.SetSystemDecimal(CustodiaBMFMetadata.ColumnNames.ValorDiario, value);
			}
		}
		
		/// <summary>
		/// Maps to CustodiaBMF.ValorAcumulado
		/// </summary>
		virtual public System.Decimal? ValorAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CustodiaBMFMetadata.ColumnNames.ValorAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CustodiaBMFMetadata.ColumnNames.ValorAcumulado, value);
			}
		}
		
		/// <summary>
		/// Maps to CustodiaBMF.ValorPagar
		/// </summary>
		virtual public System.Decimal? ValorPagar
		{
			get
			{
				return base.GetSystemDecimal(CustodiaBMFMetadata.ColumnNames.ValorPagar);
			}
			
			set
			{
				base.SetSystemDecimal(CustodiaBMFMetadata.ColumnNames.ValorPagar, value);
			}
		}
		
		/// <summary>
		/// Maps to CustodiaBMF.DataPagamento
		/// </summary>
		virtual public System.DateTime? DataPagamento
		{
			get
			{
				return base.GetSystemDateTime(CustodiaBMFMetadata.ColumnNames.DataPagamento);
			}
			
			set
			{
				base.SetSystemDateTime(CustodiaBMFMetadata.ColumnNames.DataPagamento, value);
			}
		}

        /// <summary>
        /// Maps to CustodiaBMF.IdConta
        /// </summary>
        virtual public System.Int32? IdConta
        {
            get
            {
                return base.GetSystemInt32(CustodiaBMFMetadata.ColumnNames.IdConta);
            }

            set
            {
                if (base.SetSystemInt32(CustodiaBMFMetadata.ColumnNames.IdConta, value))
                {
                    this._UpToContaCorrenteByIdConta = null;
                }
            }
        }

		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected ClasseBMF _UpToClasseBMFByCdAtivoBMF;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
        [CLSCompliant(false)]
        internal protected Financial.Investidor.ContaCorrente _UpToContaCorrenteByIdConta;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCustodiaBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorDiario
			{
				get
				{
					System.Decimal? data = entity.ValorDiario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDiario = null;
					else entity.ValorDiario = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorAcumulado
			{
				get
				{
					System.Decimal? data = entity.ValorAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAcumulado = null;
					else entity.ValorAcumulado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPagar
			{
				get
				{
					System.Decimal? data = entity.ValorPagar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPagar = null;
					else entity.ValorPagar = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataPagamento
			{
				get
				{
					System.DateTime? data = entity.DataPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPagamento = null;
					else entity.DataPagamento = Convert.ToDateTime(value);
				}
			}

            public System.String IdConta
            {
                get
                {
                    System.Int32? data = entity.IdConta;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdConta = null;
                    else entity.IdConta = Convert.ToInt32(value);
                }
            }

			private esCustodiaBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCustodiaBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCustodiaBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CustodiaBMF : esCustodiaBMF
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_CustodiaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClasseBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ClasseBMF_CustodiaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public ClasseBMF UpToClasseBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToClasseBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					)
				{
					this._UpToClasseBMFByCdAtivoBMF = new ClasseBMF();
					this._UpToClasseBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClasseBMFByCdAtivoBMF", this._UpToClasseBMFByCdAtivoBMF);
					this._UpToClasseBMFByCdAtivoBMF.Query.Where(this._UpToClasseBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToClasseBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToClasseBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToClasseBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this._UpToClasseBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this._UpToClasseBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToClasseBMFByCdAtivoBMF", this._UpToClasseBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_CustodiaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion

        #region UpToContaCorrenteByIdConta - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - FK_CustodiaBMF_ContaCorrente
        /// </summary>

        [XmlIgnore]
        public Financial.Investidor.ContaCorrente UpToContaCorrenteByIdConta
        {
            get
            {
                if (this._UpToContaCorrenteByIdConta == null
                    && IdConta != null)
                {
                    this._UpToContaCorrenteByIdConta = new Financial.Investidor.ContaCorrente();
                    this._UpToContaCorrenteByIdConta.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
                    this._UpToContaCorrenteByIdConta.Query.Where(this._UpToContaCorrenteByIdConta.Query.IdConta == this.IdConta);
                    this._UpToContaCorrenteByIdConta.Query.Load();
                }

                return this._UpToContaCorrenteByIdConta;
            }

            set
            {
                this.RemovePreSave("UpToContaCorrenteByIdConta");


                if (value == null)
                {
                    this.IdConta = null;
                    this._UpToContaCorrenteByIdConta = null;
                }
                else
                {
                    this.IdConta = value.IdConta;
                    this._UpToContaCorrenteByIdConta = value;
                    this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
                }

            }
        }
        #endregion
		
		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
            if (!this.es.IsDeleted && this._UpToContaCorrenteByIdConta != null)
            {
                this.IdConta = this._UpToContaCorrenteByIdConta.IdConta;
            }
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCustodiaBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CustodiaBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, CustodiaBMFMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, CustodiaBMFMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, CustodiaBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, CustodiaBMFMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorDiario
		{
			get
			{
				return new esQueryItem(this, CustodiaBMFMetadata.ColumnNames.ValorDiario, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorAcumulado
		{
			get
			{
				return new esQueryItem(this, CustodiaBMFMetadata.ColumnNames.ValorAcumulado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPagar
		{
			get
			{
				return new esQueryItem(this, CustodiaBMFMetadata.ColumnNames.ValorPagar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataPagamento
		{
			get
			{
				return new esQueryItem(this, CustodiaBMFMetadata.ColumnNames.DataPagamento, esSystemType.DateTime);
			}
		}

        public esQueryItem IdConta
        {
            get
            {
                return new esQueryItem(this, CustodiaBMFMetadata.ColumnNames.IdConta, esSystemType.Int32);
            }
        } 
	}



	[Serializable]
	[XmlType("CustodiaBMFCollection")]
	public partial class CustodiaBMFCollection : esCustodiaBMFCollection, IEnumerable<CustodiaBMF>
	{
		public CustodiaBMFCollection()
		{

		}
		
		public static implicit operator List<CustodiaBMF>(CustodiaBMFCollection coll)
		{
			List<CustodiaBMF> list = new List<CustodiaBMF>();
			
			foreach (CustodiaBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CustodiaBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CustodiaBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CustodiaBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CustodiaBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CustodiaBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CustodiaBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CustodiaBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CustodiaBMF AddNew()
		{
			CustodiaBMF entity = base.AddNewEntity() as CustodiaBMF;
			
			return entity;
		}

		public CustodiaBMF FindByPrimaryKey(System.DateTime data, System.Int32 idCliente, System.String cdAtivoBMF, System.Int32 idAgente)
		{
			return base.FindByPrimaryKey(data, idCliente, cdAtivoBMF, idAgente) as CustodiaBMF;
		}


		#region IEnumerable<CustodiaBMF> Members

		IEnumerator<CustodiaBMF> IEnumerable<CustodiaBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CustodiaBMF;
			}
		}

		#endregion
		
		private CustodiaBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CustodiaBMF' table
	/// </summary>

	[Serializable]
	public partial class CustodiaBMF : esCustodiaBMF
	{
		public CustodiaBMF()
		{

		}
	
		public CustodiaBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CustodiaBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esCustodiaBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CustodiaBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CustodiaBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CustodiaBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CustodiaBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CustodiaBMFQuery query;
	}



	[Serializable]
	public partial class CustodiaBMFQuery : esCustodiaBMFQuery
	{
		public CustodiaBMFQuery()
		{

		}		
		
		public CustodiaBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CustodiaBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CustodiaBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CustodiaBMFMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CustodiaBMFMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CustodiaBMFMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CustodiaBMFMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CustodiaBMFMetadata.ColumnNames.CdAtivoBMF, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = CustodiaBMFMetadata.PropertyNames.CdAtivoBMF;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CustodiaBMFMetadata.ColumnNames.IdAgente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CustodiaBMFMetadata.PropertyNames.IdAgente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CustodiaBMFMetadata.ColumnNames.ValorDiario, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CustodiaBMFMetadata.PropertyNames.ValorDiario;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CustodiaBMFMetadata.ColumnNames.ValorAcumulado, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CustodiaBMFMetadata.PropertyNames.ValorAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CustodiaBMFMetadata.ColumnNames.ValorPagar, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CustodiaBMFMetadata.PropertyNames.ValorPagar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CustodiaBMFMetadata.ColumnNames.DataPagamento, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CustodiaBMFMetadata.PropertyNames.DataPagamento;
			c.NumericPrecision = 0;
			_columns.Add(c);

            c = new esColumnMetadata(CustodiaBMFMetadata.ColumnNames.IdConta, 8, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = CustodiaBMFMetadata.PropertyNames.IdConta;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c); 

		}
		#endregion
	
		static public CustodiaBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string IdAgente = "IdAgente";
			 public const string ValorDiario = "ValorDiario";
			 public const string ValorAcumulado = "ValorAcumulado";
			 public const string ValorPagar = "ValorPagar";
			 public const string DataPagamento = "DataPagamento";
             public const string IdConta = "IdConta";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string IdAgente = "IdAgente";
			 public const string ValorDiario = "ValorDiario";
			 public const string ValorAcumulado = "ValorAcumulado";
			 public const string ValorPagar = "ValorPagar";
			 public const string DataPagamento = "DataPagamento";
             public const string IdConta = "IdConta";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CustodiaBMFMetadata))
			{
				if(CustodiaBMFMetadata.mapDelegates == null)
				{
					CustodiaBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CustodiaBMFMetadata.meta == null)
				{
					CustodiaBMFMetadata.meta = new CustodiaBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorDiario", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorAcumulado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPagar", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataPagamento", new esTypeMap("datetime", "System.DateTime"));
                meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				
				
				meta.Source = "CustodiaBMF";
				meta.Destination = "CustodiaBMF";
				
				meta.spInsert = "proc_CustodiaBMFInsert";				
				meta.spUpdate = "proc_CustodiaBMFUpdate";		
				meta.spDelete = "proc_CustodiaBMFDelete";
				meta.spLoadAll = "proc_CustodiaBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_CustodiaBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CustodiaBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
