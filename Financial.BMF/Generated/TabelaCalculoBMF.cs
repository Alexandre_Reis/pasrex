/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2012 19:02:06
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;






































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esTabelaCalculoBMFCollection : esEntityCollection
	{
		public esTabelaCalculoBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaCalculoBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaCalculoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaCalculoBMFQuery);
		}
		#endregion
		
		virtual public TabelaCalculoBMF DetachEntity(TabelaCalculoBMF entity)
		{
			return base.DetachEntity(entity) as TabelaCalculoBMF;
		}
		
		virtual public TabelaCalculoBMF AttachEntity(TabelaCalculoBMF entity)
		{
			return base.AttachEntity(entity) as TabelaCalculoBMF;
		}
		
		virtual public void Combine(TabelaCalculoBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaCalculoBMF this[int index]
		{
			get
			{
				return base[index] as TabelaCalculoBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaCalculoBMF);
		}
	}



	[Serializable]
	abstract public class esTabelaCalculoBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaCalculoBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaCalculoBMF()
		{

		}

		public esTabelaCalculoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.String cdAtivoBMF, System.Byte tipoMercado)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, cdAtivoBMF, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, cdAtivoBMF, tipoMercado);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.String cdAtivoBMF, System.Byte tipoMercado)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaCalculoBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.CdAtivoBMF == cdAtivoBMF, query.TipoMercado == tipoMercado);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.String cdAtivoBMF, System.Byte tipoMercado)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, cdAtivoBMF, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, cdAtivoBMF, tipoMercado);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.String cdAtivoBMF, System.Byte tipoMercado)
		{
			esTabelaCalculoBMFQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.CdAtivoBMF == cdAtivoBMF, query.TipoMercado == tipoMercado);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.String cdAtivoBMF, System.Byte tipoMercado)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("CdAtivoBMF",cdAtivoBMF);			parms.Add("TipoMercado",tipoMercado);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "P": this.str.P = (string)value; break;							
						case "DiasMaximo": this.str.DiasMaximo = (string)value; break;							
						case "DiasMinimo": this.str.DiasMinimo = (string)value; break;							
						case "Alfa": this.str.Alfa = (string)value; break;							
						case "Beta": this.str.Beta = (string)value; break;							
						case "PercentualDayTrade": this.str.PercentualDayTrade = (string)value; break;							
						case "PercentualRegistro": this.str.PercentualRegistro = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoMercado = (System.Byte?)value;
							break;
						
						case "P":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.P = (System.Decimal?)value;
							break;
						
						case "DiasMaximo":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.DiasMaximo = (System.Int16?)value;
							break;
						
						case "DiasMinimo":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.DiasMinimo = (System.Int16?)value;
							break;
						
						case "Alfa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Alfa = (System.Decimal?)value;
							break;
						
						case "Beta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Beta = (System.Decimal?)value;
							break;
						
						case "PercentualDayTrade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualDayTrade = (System.Decimal?)value;
							break;
						
						case "PercentualRegistro":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualRegistro = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaCalculoBMF.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaCalculoBMFMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaCalculoBMFMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCalculoBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(TabelaCalculoBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(TabelaCalculoBMFMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToClasseBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaCalculoBMF.TipoMercado
		/// </summary>
		virtual public System.Byte? TipoMercado
		{
			get
			{
				return base.GetSystemByte(TabelaCalculoBMFMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemByte(TabelaCalculoBMFMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCalculoBMF.P
		/// </summary>
		virtual public System.Decimal? P
		{
			get
			{
				return base.GetSystemDecimal(TabelaCalculoBMFMetadata.ColumnNames.P);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCalculoBMFMetadata.ColumnNames.P, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCalculoBMF.DiasMaximo
		/// </summary>
		virtual public System.Int16? DiasMaximo
		{
			get
			{
				return base.GetSystemInt16(TabelaCalculoBMFMetadata.ColumnNames.DiasMaximo);
			}
			
			set
			{
				base.SetSystemInt16(TabelaCalculoBMFMetadata.ColumnNames.DiasMaximo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCalculoBMF.DiasMinimo
		/// </summary>
		virtual public System.Int16? DiasMinimo
		{
			get
			{
				return base.GetSystemInt16(TabelaCalculoBMFMetadata.ColumnNames.DiasMinimo);
			}
			
			set
			{
				base.SetSystemInt16(TabelaCalculoBMFMetadata.ColumnNames.DiasMinimo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCalculoBMF.Alfa
		/// </summary>
		virtual public System.Decimal? Alfa
		{
			get
			{
				return base.GetSystemDecimal(TabelaCalculoBMFMetadata.ColumnNames.Alfa);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCalculoBMFMetadata.ColumnNames.Alfa, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCalculoBMF.Beta
		/// </summary>
		virtual public System.Decimal? Beta
		{
			get
			{
				return base.GetSystemDecimal(TabelaCalculoBMFMetadata.ColumnNames.Beta);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCalculoBMFMetadata.ColumnNames.Beta, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCalculoBMF.PercentualDayTrade
		/// </summary>
		virtual public System.Decimal? PercentualDayTrade
		{
			get
			{
				return base.GetSystemDecimal(TabelaCalculoBMFMetadata.ColumnNames.PercentualDayTrade);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCalculoBMFMetadata.ColumnNames.PercentualDayTrade, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCalculoBMF.PercentualRegistro
		/// </summary>
		virtual public System.Decimal? PercentualRegistro
		{
			get
			{
				return base.GetSystemDecimal(TabelaCalculoBMFMetadata.ColumnNames.PercentualRegistro);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCalculoBMFMetadata.ColumnNames.PercentualRegistro, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected ClasseBMF _UpToClasseBMFByCdAtivoBMF;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaCalculoBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Byte? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToByte(value);
				}
			}
				
			public System.String P
			{
				get
				{
					System.Decimal? data = entity.P;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.P = null;
					else entity.P = Convert.ToDecimal(value);
				}
			}
				
			public System.String DiasMaximo
			{
				get
				{
					System.Int16? data = entity.DiasMaximo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasMaximo = null;
					else entity.DiasMaximo = Convert.ToInt16(value);
				}
			}
				
			public System.String DiasMinimo
			{
				get
				{
					System.Int16? data = entity.DiasMinimo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasMinimo = null;
					else entity.DiasMinimo = Convert.ToInt16(value);
				}
			}
				
			public System.String Alfa
			{
				get
				{
					System.Decimal? data = entity.Alfa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Alfa = null;
					else entity.Alfa = Convert.ToDecimal(value);
				}
			}
				
			public System.String Beta
			{
				get
				{
					System.Decimal? data = entity.Beta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Beta = null;
					else entity.Beta = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercentualDayTrade
			{
				get
				{
					System.Decimal? data = entity.PercentualDayTrade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualDayTrade = null;
					else entity.PercentualDayTrade = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercentualRegistro
			{
				get
				{
					System.Decimal? data = entity.PercentualRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualRegistro = null;
					else entity.PercentualRegistro = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaCalculoBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaCalculoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaCalculoBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaCalculoBMF : esTabelaCalculoBMF
	{

				
		#region UpToClasseBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ClasseBMF_TabelaCalculoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public ClasseBMF UpToClasseBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToClasseBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					)
				{
					this._UpToClasseBMFByCdAtivoBMF = new ClasseBMF();
					this._UpToClasseBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClasseBMFByCdAtivoBMF", this._UpToClasseBMFByCdAtivoBMF);
					this._UpToClasseBMFByCdAtivoBMF.Query.Where(this._UpToClasseBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToClasseBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToClasseBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToClasseBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this._UpToClasseBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this._UpToClasseBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToClasseBMFByCdAtivoBMF", this._UpToClasseBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaCalculoBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCalculoBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaCalculoBMFMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, TabelaCalculoBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, TabelaCalculoBMFMetadata.ColumnNames.TipoMercado, esSystemType.Byte);
			}
		} 
		
		public esQueryItem P
		{
			get
			{
				return new esQueryItem(this, TabelaCalculoBMFMetadata.ColumnNames.P, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DiasMaximo
		{
			get
			{
				return new esQueryItem(this, TabelaCalculoBMFMetadata.ColumnNames.DiasMaximo, esSystemType.Int16);
			}
		} 
		
		public esQueryItem DiasMinimo
		{
			get
			{
				return new esQueryItem(this, TabelaCalculoBMFMetadata.ColumnNames.DiasMinimo, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Alfa
		{
			get
			{
				return new esQueryItem(this, TabelaCalculoBMFMetadata.ColumnNames.Alfa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Beta
		{
			get
			{
				return new esQueryItem(this, TabelaCalculoBMFMetadata.ColumnNames.Beta, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercentualDayTrade
		{
			get
			{
				return new esQueryItem(this, TabelaCalculoBMFMetadata.ColumnNames.PercentualDayTrade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercentualRegistro
		{
			get
			{
				return new esQueryItem(this, TabelaCalculoBMFMetadata.ColumnNames.PercentualRegistro, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaCalculoBMFCollection")]
	public partial class TabelaCalculoBMFCollection : esTabelaCalculoBMFCollection, IEnumerable<TabelaCalculoBMF>
	{
		public TabelaCalculoBMFCollection()
		{

		}
		
		public static implicit operator List<TabelaCalculoBMF>(TabelaCalculoBMFCollection coll)
		{
			List<TabelaCalculoBMF> list = new List<TabelaCalculoBMF>();
			
			foreach (TabelaCalculoBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaCalculoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCalculoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaCalculoBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaCalculoBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaCalculoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCalculoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaCalculoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaCalculoBMF AddNew()
		{
			TabelaCalculoBMF entity = base.AddNewEntity() as TabelaCalculoBMF;
			
			return entity;
		}

		public TabelaCalculoBMF FindByPrimaryKey(System.DateTime dataReferencia, System.String cdAtivoBMF, System.Byte tipoMercado)
		{
			return base.FindByPrimaryKey(dataReferencia, cdAtivoBMF, tipoMercado) as TabelaCalculoBMF;
		}


		#region IEnumerable<TabelaCalculoBMF> Members

		IEnumerator<TabelaCalculoBMF> IEnumerable<TabelaCalculoBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaCalculoBMF;
			}
		}

		#endregion
		
		private TabelaCalculoBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaCalculoBMF' table
	/// </summary>

	[Serializable]
	public partial class TabelaCalculoBMF : esTabelaCalculoBMF
	{
		public TabelaCalculoBMF()
		{

		}
	
		public TabelaCalculoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCalculoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaCalculoBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCalculoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaCalculoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCalculoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaCalculoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaCalculoBMFQuery query;
	}



	[Serializable]
	public partial class TabelaCalculoBMFQuery : esTabelaCalculoBMFQuery
	{
		public TabelaCalculoBMFQuery()
		{

		}		
		
		public TabelaCalculoBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaCalculoBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaCalculoBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaCalculoBMFMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaCalculoBMFMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCalculoBMFMetadata.ColumnNames.CdAtivoBMF, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaCalculoBMFMetadata.PropertyNames.CdAtivoBMF;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCalculoBMFMetadata.ColumnNames.TipoMercado, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaCalculoBMFMetadata.PropertyNames.TipoMercado;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCalculoBMFMetadata.ColumnNames.P, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCalculoBMFMetadata.PropertyNames.P;	
			c.NumericPrecision = 16;
			c.NumericScale = 7;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCalculoBMFMetadata.ColumnNames.DiasMaximo, 4, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TabelaCalculoBMFMetadata.PropertyNames.DiasMaximo;	
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCalculoBMFMetadata.ColumnNames.DiasMinimo, 5, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TabelaCalculoBMFMetadata.PropertyNames.DiasMinimo;	
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCalculoBMFMetadata.ColumnNames.Alfa, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCalculoBMFMetadata.PropertyNames.Alfa;	
			c.NumericPrecision = 16;
			c.NumericScale = 7;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCalculoBMFMetadata.ColumnNames.Beta, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCalculoBMFMetadata.PropertyNames.Beta;	
			c.NumericPrecision = 16;
			c.NumericScale = 7;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCalculoBMFMetadata.ColumnNames.PercentualDayTrade, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCalculoBMFMetadata.PropertyNames.PercentualDayTrade;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCalculoBMFMetadata.ColumnNames.PercentualRegistro, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCalculoBMFMetadata.PropertyNames.PercentualRegistro;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaCalculoBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string TipoMercado = "TipoMercado";
			 public const string P = "P";
			 public const string DiasMaximo = "DiasMaximo";
			 public const string DiasMinimo = "DiasMinimo";
			 public const string Alfa = "Alfa";
			 public const string Beta = "Beta";
			 public const string PercentualDayTrade = "PercentualDayTrade";
			 public const string PercentualRegistro = "PercentualRegistro";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string TipoMercado = "TipoMercado";
			 public const string P = "P";
			 public const string DiasMaximo = "DiasMaximo";
			 public const string DiasMinimo = "DiasMinimo";
			 public const string Alfa = "Alfa";
			 public const string Beta = "Beta";
			 public const string PercentualDayTrade = "PercentualDayTrade";
			 public const string PercentualRegistro = "PercentualRegistro";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaCalculoBMFMetadata))
			{
				if(TabelaCalculoBMFMetadata.mapDelegates == null)
				{
					TabelaCalculoBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaCalculoBMFMetadata.meta == null)
				{
					TabelaCalculoBMFMetadata.meta = new TabelaCalculoBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("P", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DiasMaximo", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("DiasMinimo", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Alfa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Beta", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PercentualDayTrade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PercentualRegistro", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaCalculoBMF";
				meta.Destination = "TabelaCalculoBMF";
				
				meta.spInsert = "proc_TabelaCalculoBMFInsert";				
				meta.spUpdate = "proc_TabelaCalculoBMFUpdate";		
				meta.spDelete = "proc_TabelaCalculoBMFDelete";
				meta.spLoadAll = "proc_TabelaCalculoBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaCalculoBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaCalculoBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
