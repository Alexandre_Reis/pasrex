/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/12/2015 17:23:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;


namespace Financial.BMF
{

	[Serializable]
	abstract public class esPerfilTaxaClearingBMFCollection : esEntityCollection
	{
		public esPerfilTaxaClearingBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PerfilTaxaClearingBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esPerfilTaxaClearingBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPerfilTaxaClearingBMFQuery);
		}
		#endregion
		
		virtual public PerfilTaxaClearingBMF DetachEntity(PerfilTaxaClearingBMF entity)
		{
			return base.DetachEntity(entity) as PerfilTaxaClearingBMF;
		}
		
		virtual public PerfilTaxaClearingBMF AttachEntity(PerfilTaxaClearingBMF entity)
		{
			return base.AttachEntity(entity) as PerfilTaxaClearingBMF;
		}
		
		virtual public void Combine(PerfilTaxaClearingBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PerfilTaxaClearingBMF this[int index]
		{
			get
			{
				return base[index] as PerfilTaxaClearingBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PerfilTaxaClearingBMF);
		}
	}



	[Serializable]
	abstract public class esPerfilTaxaClearingBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPerfilTaxaClearingBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esPerfilTaxaClearingBMF()
		{

		}

		public esPerfilTaxaClearingBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPerfil)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfil);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfil);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPerfil)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfil);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfil);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPerfil)
		{
			esPerfilTaxaClearingBMFQuery query = this.GetDynamicQuery();
			query.Where(query.IdPerfil == idPerfil);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPerfil)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPerfil",idPerfil);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPerfil": this.str.IdPerfil = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPerfil":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfil = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PerfilTaxaClearingBMF.IdPerfil
		/// </summary>
		virtual public System.Int32? IdPerfil
		{
			get
			{
				return base.GetSystemInt32(PerfilTaxaClearingBMFMetadata.ColumnNames.IdPerfil);
			}
			
			set
			{
				base.SetSystemInt32(PerfilTaxaClearingBMFMetadata.ColumnNames.IdPerfil, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilTaxaClearingBMF.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(PerfilTaxaClearingBMFMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(PerfilTaxaClearingBMFMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPerfilTaxaClearingBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPerfil
			{
				get
				{
					System.Int32? data = entity.IdPerfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfil = null;
					else entity.IdPerfil = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esPerfilTaxaClearingBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPerfilTaxaClearingBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPerfilTaxaClearingBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PerfilTaxaClearingBMF : esPerfilTaxaClearingBMF
	{

		#region UpToClienteCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - PerfilTaxaClearingBMFCliente_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteCollection UpToClienteCollection
		{
			get
			{
				if(this._UpToClienteCollection == null)
				{
					this._UpToClienteCollection = new ClienteCollection();
					this._UpToClienteCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToClienteCollection", this._UpToClienteCollection);
					//this._UpToClienteCollection.ManyToManyPerfilTaxaClearingBMFCollection(this.IdPerfil);
				}

				return this._UpToClienteCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToClienteCollection != null) 
				{ 
					this.RemovePostSave("UpToClienteCollection"); 
					this._UpToClienteCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - PerfilTaxaClearingBMFCliente_FK1
		/// </summary>
		public void AssociateClienteCollection(Cliente entity)
		{
			if (this._PerfilTaxaClearingBMFClienteCollection == null)
			{
				this._PerfilTaxaClearingBMFClienteCollection = new PerfilTaxaClearingBMFClienteCollection();
				this._PerfilTaxaClearingBMFClienteCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("PerfilTaxaClearingBMFClienteCollection", this._PerfilTaxaClearingBMFClienteCollection);
			}

			PerfilTaxaClearingBMFCliente obj = this._PerfilTaxaClearingBMFClienteCollection.AddNew();
			obj.IdPerfilTaxaClearingBMF = this.IdPerfil;
			obj.IdCliente = entity.IdCliente;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - PerfilTaxaClearingBMFCliente_FK1
		/// </summary>
		public void DissociateClienteCollection(Cliente entity)
		{
			if (this._PerfilTaxaClearingBMFClienteCollection == null)
			{
				this._PerfilTaxaClearingBMFClienteCollection = new PerfilTaxaClearingBMFClienteCollection();
				this._PerfilTaxaClearingBMFClienteCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("PerfilTaxaClearingBMFClienteCollection", this._PerfilTaxaClearingBMFClienteCollection);
			}

			PerfilTaxaClearingBMFCliente obj = this._PerfilTaxaClearingBMFClienteCollection.AddNew();
			obj.IdPerfilTaxaClearingBMF = this.IdPerfil;
			obj.IdCliente = entity.IdCliente;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private ClienteCollection _UpToClienteCollection;
		private PerfilTaxaClearingBMFClienteCollection _PerfilTaxaClearingBMFClienteCollection;
		#endregion

				
		#region PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PerfilTaxaClearingBMFCliente_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilTaxaClearingBMFClienteCollection PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF
		{
			get
			{
				if(this._PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF == null)
				{
					this._PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF = new PerfilTaxaClearingBMFClienteCollection();
					this._PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF", this._PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF);
				
					if(this.IdPerfil != null)
					{
						this._PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF.Query.Where(this._PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF.Query.IdPerfilTaxaClearingBMF == this.IdPerfil);
						this._PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF.fks.Add(PerfilTaxaClearingBMFClienteMetadata.ColumnNames.IdPerfilTaxaClearingBMF, this.IdPerfil);
					}
				}

				return this._PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF != null) 
				{ 
					this.RemovePostSave("PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF"); 
					this._PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF = null;
					
				} 
			} 			
		}

		private PerfilTaxaClearingBMFClienteCollection _PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF;
		#endregion

				
		#region TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TabelaTaxaClearingBMF_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaClearingBMFCollection TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF
		{
			get
			{
				if(this._TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF == null)
				{
					this._TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF = new TabelaTaxaClearingBMFCollection();
					this._TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF", this._TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF);
				
					if(this.IdPerfil != null)
					{
						this._TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF.Query.Where(this._TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF.Query.IdPerfilTaxaClearingBMF == this.IdPerfil);
						this._TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF.fks.Add(TabelaTaxaClearingBMFMetadata.ColumnNames.IdPerfilTaxaClearingBMF, this.IdPerfil);
					}
				}

				return this._TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF != null) 
				{ 
					this.RemovePostSave("TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF"); 
					this._TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF = null;
					
				} 
			} 			
		}

		private TabelaTaxaClearingBMFCollection _TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "PerfilTaxaClearingBMFClienteCollectionByIdPerfilTaxaClearingBMF", typeof(PerfilTaxaClearingBMFClienteCollection), new PerfilTaxaClearingBMFCliente()));
			props.Add(new esPropertyDescriptor(this, "TabelaTaxaClearingBMFCollectionByIdPerfilTaxaClearingBMF", typeof(TabelaTaxaClearingBMFCollection), new TabelaTaxaClearingBMF()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}
	
	public partial class PerfilTaxaClearingBMFCollection : esPerfilTaxaClearingBMFCollection
	{
		#region ManyToManyClienteCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyClienteCollection(System.Int32? IdCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente", IdCliente);
	
			return base.Load( esQueryType.ManyToMany, 
				"PerfilTaxaClearingBMF,PerfilTaxaClearingBMFCliente|IdPerfil,IdPerfilTaxaClearingBMF|IdCliente",	parms);
		}
		#endregion
	}




	[Serializable]
	abstract public class esPerfilTaxaClearingBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PerfilTaxaClearingBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPerfil
		{
			get
			{
				return new esQueryItem(this, PerfilTaxaClearingBMFMetadata.ColumnNames.IdPerfil, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, PerfilTaxaClearingBMFMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PerfilTaxaClearingBMFCollection")]
	public partial class PerfilTaxaClearingBMFCollection : esPerfilTaxaClearingBMFCollection, IEnumerable<PerfilTaxaClearingBMF>
	{
		public PerfilTaxaClearingBMFCollection()
		{

		}
		
		public static implicit operator List<PerfilTaxaClearingBMF>(PerfilTaxaClearingBMFCollection coll)
		{
			List<PerfilTaxaClearingBMF> list = new List<PerfilTaxaClearingBMF>();
			
			foreach (PerfilTaxaClearingBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PerfilTaxaClearingBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilTaxaClearingBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PerfilTaxaClearingBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PerfilTaxaClearingBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PerfilTaxaClearingBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilTaxaClearingBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PerfilTaxaClearingBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PerfilTaxaClearingBMF AddNew()
		{
			PerfilTaxaClearingBMF entity = base.AddNewEntity() as PerfilTaxaClearingBMF;
			
			return entity;
		}

		public PerfilTaxaClearingBMF FindByPrimaryKey(System.Int32 idPerfil)
		{
			return base.FindByPrimaryKey(idPerfil) as PerfilTaxaClearingBMF;
		}


		#region IEnumerable<PerfilTaxaClearingBMF> Members

		IEnumerator<PerfilTaxaClearingBMF> IEnumerable<PerfilTaxaClearingBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PerfilTaxaClearingBMF;
			}
		}

		#endregion
		
		private PerfilTaxaClearingBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PerfilTaxaClearingBMF' table
	/// </summary>

	[Serializable]
	public partial class PerfilTaxaClearingBMF : esPerfilTaxaClearingBMF
	{
		public PerfilTaxaClearingBMF()
		{

		}
	
		public PerfilTaxaClearingBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PerfilTaxaClearingBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esPerfilTaxaClearingBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilTaxaClearingBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PerfilTaxaClearingBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilTaxaClearingBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PerfilTaxaClearingBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PerfilTaxaClearingBMFQuery query;
	}



	[Serializable]
	public partial class PerfilTaxaClearingBMFQuery : esPerfilTaxaClearingBMFQuery
	{
		public PerfilTaxaClearingBMFQuery()
		{

		}		
		
		public PerfilTaxaClearingBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PerfilTaxaClearingBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PerfilTaxaClearingBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PerfilTaxaClearingBMFMetadata.ColumnNames.IdPerfil, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilTaxaClearingBMFMetadata.PropertyNames.IdPerfil;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilTaxaClearingBMFMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = PerfilTaxaClearingBMFMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PerfilTaxaClearingBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPerfil = "IdPerfil";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPerfil = "IdPerfil";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PerfilTaxaClearingBMFMetadata))
			{
				if(PerfilTaxaClearingBMFMetadata.mapDelegates == null)
				{
					PerfilTaxaClearingBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PerfilTaxaClearingBMFMetadata.meta == null)
				{
					PerfilTaxaClearingBMFMetadata.meta = new PerfilTaxaClearingBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPerfil", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "PerfilTaxaClearingBMF";
				meta.Destination = "PerfilTaxaClearingBMF";
				
				meta.spInsert = "proc_PerfilTaxaClearingBMFInsert";				
				meta.spUpdate = "proc_PerfilTaxaClearingBMFUpdate";		
				meta.spDelete = "proc_PerfilTaxaClearingBMFDelete";
				meta.spLoadAll = "proc_PerfilTaxaClearingBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_PerfilTaxaClearingBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PerfilTaxaClearingBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
