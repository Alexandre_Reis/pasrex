/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2012 19:02:05
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;











using Financial.Common;
using Financial.Investidor;							



























		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esPosicaoBMFHistoricoCollection : esEntityCollection
	{
		public esPosicaoBMFHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoBMFHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoBMFHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoBMFHistoricoQuery);
		}
		#endregion
		
		virtual public PosicaoBMFHistorico DetachEntity(PosicaoBMFHistorico entity)
		{
			return base.DetachEntity(entity) as PosicaoBMFHistorico;
		}
		
		virtual public PosicaoBMFHistorico AttachEntity(PosicaoBMFHistorico entity)
		{
			return base.AttachEntity(entity) as PosicaoBMFHistorico;
		}
		
		virtual public void Combine(PosicaoBMFHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoBMFHistorico this[int index]
		{
			get
			{
				return base[index] as PosicaoBMFHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoBMFHistorico);
		}
	}



	[Serializable]
	abstract public class esPosicaoBMFHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoBMFHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoBMFHistorico()
		{

		}

		public esPosicaoBMFHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPosicaoBMFHistoricoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esPosicaoBMFHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);			parms.Add("DataHistorico",dataHistorico);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Serie": this.str.Serie = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "PUMercado": this.str.PUMercado = (string)value; break;							
						case "PUCusto": this.str.PUCusto = (string)value; break;							
						case "PUCustoLiquido": this.str.PUCustoLiquido = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeInicial": this.str.QuantidadeInicial = (string)value; break;							
						case "ValorMercado": this.str.ValorMercado = (string)value; break;							
						case "ValorCustoLiquido": this.str.ValorCustoLiquido = (string)value; break;							
						case "ResultadoRealizar": this.str.ResultadoRealizar = (string)value; break;							
						case "AjusteDiario": this.str.AjusteDiario = (string)value; break;							
						case "AjusteAcumulado": this.str.AjusteAcumulado = (string)value; break;
                        case "IdConta": this.str.IdConta = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoMercado = (System.Byte?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "PUMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUMercado = (System.Decimal?)value;
							break;
						
						case "PUCusto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCusto = (System.Decimal?)value;
							break;
						
						case "PUCustoLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCustoLiquido = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Quantidade = (System.Int32?)value;
							break;
						
						case "QuantidadeInicial":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.QuantidadeInicial = (System.Int32?)value;
							break;
						
						case "ValorMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMercado = (System.Decimal?)value;
							break;
						
						case "ValorCustoLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCustoLiquido = (System.Decimal?)value;
							break;
						
						case "ResultadoRealizar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoRealizar = (System.Decimal?)value;
							break;
						
						case "AjusteDiario":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AjusteDiario = (System.Decimal?)value;
							break;
						
						case "AjusteAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AjusteAcumulado = (System.Decimal?)value;
							break;

                        case "IdConta":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdConta = (System.Int32?)value;
                            break;

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoBMFHistoricoMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoBMFHistoricoMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(PosicaoBMFHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoBMFHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PosicaoBMFHistoricoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoBMFHistoricoMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(PosicaoBMFHistoricoMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoBMFHistoricoMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(PosicaoBMFHistoricoMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(PosicaoBMFHistoricoMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.Serie
		/// </summary>
		virtual public System.String Serie
		{
			get
			{
				return base.GetSystemString(PosicaoBMFHistoricoMetadata.ColumnNames.Serie);
			}
			
			set
			{
				if(base.SetSystemString(PosicaoBMFHistoricoMetadata.ColumnNames.Serie, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.TipoMercado
		/// </summary>
		virtual public System.Byte? TipoMercado
		{
			get
			{
				return base.GetSystemByte(PosicaoBMFHistoricoMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemByte(PosicaoBMFHistoricoMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(PosicaoBMFHistoricoMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoBMFHistoricoMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.PUMercado
		/// </summary>
		virtual public System.Decimal? PUMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.PUMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.PUMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.PUCusto
		/// </summary>
		virtual public System.Decimal? PUCusto
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.PUCusto);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.PUCusto, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.PUCustoLiquido
		/// </summary>
		virtual public System.Decimal? PUCustoLiquido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.PUCustoLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.PUCustoLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.Quantidade
		/// </summary>
		virtual public System.Int32? Quantidade
		{
			get
			{
				return base.GetSystemInt32(PosicaoBMFHistoricoMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoBMFHistoricoMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.QuantidadeInicial
		/// </summary>
		virtual public System.Int32? QuantidadeInicial
		{
			get
			{
				return base.GetSystemInt32(PosicaoBMFHistoricoMetadata.ColumnNames.QuantidadeInicial);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoBMFHistoricoMetadata.ColumnNames.QuantidadeInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.ValorMercado
		/// </summary>
		virtual public System.Decimal? ValorMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.ValorMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.ValorMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.ValorCustoLiquido
		/// </summary>
		virtual public System.Decimal? ValorCustoLiquido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.ValorCustoLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.ValorCustoLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.ResultadoRealizar
		/// </summary>
		virtual public System.Decimal? ResultadoRealizar
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.ResultadoRealizar);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.ResultadoRealizar, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.AjusteDiario
		/// </summary>
		virtual public System.Decimal? AjusteDiario
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.AjusteDiario);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.AjusteDiario, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMFHistorico.AjusteAcumulado
		/// </summary>
		virtual public System.Decimal? AjusteAcumulado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.AjusteAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFHistoricoMetadata.ColumnNames.AjusteAcumulado, value);
			}
		}

        /// <summary>
        /// Maps to PosicaoBMFHistorico.IdConta
        /// </summary>
        virtual public System.Int32? IdConta
        {
            get
            {
                return base.GetSystemInt32(PosicaoBMFHistoricoMetadata.ColumnNames.IdConta);
            }

            set
            {
                if (base.SetSystemInt32(PosicaoBMFHistoricoMetadata.ColumnNames.IdConta, value))
                {
                    this._UpToContaCorrenteByIdConta = null;
                }
            }
        }

		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected AtivoBMF _UpToAtivoBMFByCdAtivoBMF;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
        [CLSCompliant(false)]
        internal protected Financial.Investidor.ContaCorrente _UpToContaCorrenteByIdConta;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoBMFHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Serie
			{
				get
				{
					System.String data = entity.Serie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Serie = null;
					else entity.Serie = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Byte? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToByte(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String PUMercado
			{
				get
				{
					System.Decimal? data = entity.PUMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUMercado = null;
					else entity.PUMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCusto
			{
				get
				{
					System.Decimal? data = entity.PUCusto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCusto = null;
					else entity.PUCusto = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCustoLiquido
			{
				get
				{
					System.Decimal? data = entity.PUCustoLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCustoLiquido = null;
					else entity.PUCustoLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Int32? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToInt32(value);
				}
			}
				
			public System.String QuantidadeInicial
			{
				get
				{
					System.Int32? data = entity.QuantidadeInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeInicial = null;
					else entity.QuantidadeInicial = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorMercado
			{
				get
				{
					System.Decimal? data = entity.ValorMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMercado = null;
					else entity.ValorMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCustoLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorCustoLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCustoLiquido = null;
					else entity.ValorCustoLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoRealizar
			{
				get
				{
					System.Decimal? data = entity.ResultadoRealizar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoRealizar = null;
					else entity.ResultadoRealizar = Convert.ToDecimal(value);
				}
			}
				
			public System.String AjusteDiario
			{
				get
				{
					System.Decimal? data = entity.AjusteDiario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AjusteDiario = null;
					else entity.AjusteDiario = Convert.ToDecimal(value);
				}
			}
				
			public System.String AjusteAcumulado
			{
				get
				{
					System.Decimal? data = entity.AjusteAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AjusteAcumulado = null;
					else entity.AjusteAcumulado = Convert.ToDecimal(value);
				}
			}

            public System.String IdConta
            {
                get
                {
                    System.Int32? data = entity.IdConta;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdConta = null;
                    else entity.IdConta = Convert.ToInt32(value);
                }
            }

			private esPosicaoBMFHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoBMFHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoBMFHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoBMFHistorico : esPosicaoBMFHistorico
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_PosicaoBMFHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBMF_PosicaoBMFHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBMF UpToAtivoBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToAtivoBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					&& Serie != null					)
				{
					this._UpToAtivoBMFByCdAtivoBMF = new AtivoBMF();
					this._UpToAtivoBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.Serie == this.Serie);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToAtivoBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this.Serie = null;
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this.Serie = value.Serie;
					this._UpToAtivoBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PosicaoBMFHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion

        #region UpToContaCorrenteByIdConta - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - FK_PosicaoBMFHistoricop_ContaCorrente
        /// </summary>

        [XmlIgnore]
        public Financial.Investidor.ContaCorrente UpToContaCorrenteByIdConta
        {
            get
            {
                if (this._UpToContaCorrenteByIdConta == null
                    && IdConta != null)
                {
                    this._UpToContaCorrenteByIdConta = new Financial.Investidor.ContaCorrente();
                    this._UpToContaCorrenteByIdConta.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
                    this._UpToContaCorrenteByIdConta.Query.Where(this._UpToContaCorrenteByIdConta.Query.IdConta == this.IdConta);
                    this._UpToContaCorrenteByIdConta.Query.Load();
                }

                return this._UpToContaCorrenteByIdConta;
            }

            set
            {
                this.RemovePreSave("UpToContaCorrenteByIdConta");


                if (value == null)
                {
                    this.IdConta = null;
                    this._UpToContaCorrenteByIdConta = null;
                }
                else
                {
                    this.IdConta = value.IdConta;
                    this._UpToContaCorrenteByIdConta = value;
                    this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
                }

            }
        }
        #endregion
		
		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
            if (!this.es.IsDeleted && this._UpToContaCorrenteByIdConta != null)
            {
                this.IdConta = this._UpToContaCorrenteByIdConta.IdConta;
            }
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoBMFHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoBMFHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Serie
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.Serie, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.TipoMercado, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PUMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.PUMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCusto
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.PUCusto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCustoLiquido
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.PUCustoLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.Quantidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QuantidadeInicial
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.QuantidadeInicial, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.ValorMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCustoLiquido
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.ValorCustoLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoRealizar
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.ResultadoRealizar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AjusteDiario
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.AjusteDiario, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AjusteAcumulado
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.AjusteAcumulado, esSystemType.Decimal);
			}
		}

        public esQueryItem IdConta
        {
            get
            {
                return new esQueryItem(this, PosicaoBMFHistoricoMetadata.ColumnNames.IdConta, esSystemType.Int32);
            }
        } 
	}



	[Serializable]
	[XmlType("PosicaoBMFHistoricoCollection")]
	public partial class PosicaoBMFHistoricoCollection : esPosicaoBMFHistoricoCollection, IEnumerable<PosicaoBMFHistorico>
	{
		public PosicaoBMFHistoricoCollection()
		{

		}
		
		public static implicit operator List<PosicaoBMFHistorico>(PosicaoBMFHistoricoCollection coll)
		{
			List<PosicaoBMFHistorico> list = new List<PosicaoBMFHistorico>();
			
			foreach (PosicaoBMFHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoBMFHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoBMFHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoBMFHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoBMFHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoBMFHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoBMFHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoBMFHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoBMFHistorico AddNew()
		{
			PosicaoBMFHistorico entity = base.AddNewEntity() as PosicaoBMFHistorico;
			
			return entity;
		}

		public PosicaoBMFHistorico FindByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			return base.FindByPrimaryKey(idPosicao, dataHistorico) as PosicaoBMFHistorico;
		}


		#region IEnumerable<PosicaoBMFHistorico> Members

		IEnumerator<PosicaoBMFHistorico> IEnumerable<PosicaoBMFHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoBMFHistorico;
			}
		}

		#endregion
		
		private PosicaoBMFHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoBMFHistorico' table
	/// </summary>

	[Serializable]
	public partial class PosicaoBMFHistorico : esPosicaoBMFHistorico
	{
		public PosicaoBMFHistorico()
		{

		}
	
		public PosicaoBMFHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoBMFHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoBMFHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoBMFHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoBMFHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoBMFHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoBMFHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoBMFHistoricoQuery query;
	}



	[Serializable]
	public partial class PosicaoBMFHistoricoQuery : esPosicaoBMFHistoricoQuery
	{
		public PosicaoBMFHistoricoQuery()
		{

		}		
		
		public PosicaoBMFHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoBMFHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoBMFHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.DataHistorico, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.IdAgente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.CdAtivoBMF, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.CdAtivoBMF;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.Serie, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.Serie;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.TipoMercado, 6, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.TipoMercado;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.DataVencimento, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.PUMercado, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.PUMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.PUCusto, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.PUCusto;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.PUCustoLiquido, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.PUCustoLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.Quantidade, 11, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.QuantidadeInicial, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.QuantidadeInicial;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.ValorMercado, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.ValorMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.ValorCustoLiquido, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.ValorCustoLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.ResultadoRealizar, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.ResultadoRealizar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.AjusteDiario, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.AjusteDiario;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.AjusteAcumulado, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.AjusteAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c);

            c = new esColumnMetadata(PosicaoBMFHistoricoMetadata.ColumnNames.IdConta, 18, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = PosicaoBMFHistoricoMetadata.PropertyNames.IdConta;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c);
				
		}
		#endregion
	
		static public PosicaoBMFHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataVencimento = "DataVencimento";
			 public const string PUMercado = "PUMercado";
			 public const string PUCusto = "PUCusto";
			 public const string PUCustoLiquido = "PUCustoLiquido";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string ValorMercado = "ValorMercado";
			 public const string ValorCustoLiquido = "ValorCustoLiquido";
			 public const string ResultadoRealizar = "ResultadoRealizar";
			 public const string AjusteDiario = "AjusteDiario";
			 public const string AjusteAcumulado = "AjusteAcumulado";
             public const string IdConta = "IdConta";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataVencimento = "DataVencimento";
			 public const string PUMercado = "PUMercado";
			 public const string PUCusto = "PUCusto";
			 public const string PUCustoLiquido = "PUCustoLiquido";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string ValorMercado = "ValorMercado";
			 public const string ValorCustoLiquido = "ValorCustoLiquido";
			 public const string ResultadoRealizar = "ResultadoRealizar";
			 public const string AjusteDiario = "AjusteDiario";
			 public const string AjusteAcumulado = "AjusteAcumulado";
             public const string IdConta = "IdConta";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoBMFHistoricoMetadata))
			{
				if(PosicaoBMFHistoricoMetadata.mapDelegates == null)
				{
					PosicaoBMFHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoBMFHistoricoMetadata.meta == null)
				{
					PosicaoBMFHistoricoMetadata.meta = new PosicaoBMFHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Serie", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PUMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCusto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCustoLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QuantidadeInicial", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCustoLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoRealizar", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AjusteDiario", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AjusteAcumulado", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				
				
				
				meta.Source = "PosicaoBMFHistorico";
				meta.Destination = "PosicaoBMFHistorico";
				
				meta.spInsert = "proc_PosicaoBMFHistoricoInsert";				
				meta.spUpdate = "proc_PosicaoBMFHistoricoUpdate";		
				meta.spDelete = "proc_PosicaoBMFHistoricoDelete";
				meta.spLoadAll = "proc_PosicaoBMFHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoBMFHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoBMFHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
