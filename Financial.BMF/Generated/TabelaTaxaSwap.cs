/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2012 19:02:06
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;

















			




















		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esTabelaTaxaSwapCollection : esEntityCollection
	{
		public esTabelaTaxaSwapCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaTaxaSwapCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaTaxaSwapQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaTaxaSwapQuery);
		}
		#endregion
		
		virtual public TabelaTaxaSwap DetachEntity(TabelaTaxaSwap entity)
		{
			return base.DetachEntity(entity) as TabelaTaxaSwap;
		}
		
		virtual public TabelaTaxaSwap AttachEntity(TabelaTaxaSwap entity)
		{
			return base.AttachEntity(entity) as TabelaTaxaSwap;
		}
		
		virtual public void Combine(TabelaTaxaSwapCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaTaxaSwap this[int index]
		{
			get
			{
				return base[index] as TabelaTaxaSwap;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaTaxaSwap);
		}
	}



	[Serializable]
	abstract public class esTabelaTaxaSwap : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaTaxaSwapQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaTaxaSwap()
		{

		}

		public esTabelaTaxaSwap(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.String codigoTaxa, System.Int16 diasUteis, System.Int16 diasCorridos)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, codigoTaxa, diasUteis, diasCorridos);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, codigoTaxa, diasUteis, diasCorridos);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.String codigoTaxa, System.Int16 diasUteis, System.Int16 diasCorridos)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaTaxaSwapQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.CodigoTaxa == codigoTaxa, query.DiasUteis == diasUteis, query.DiasCorridos == diasCorridos);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.String codigoTaxa, System.Int16 diasUteis, System.Int16 diasCorridos)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, codigoTaxa, diasUteis, diasCorridos);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, codigoTaxa, diasUteis, diasCorridos);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.String codigoTaxa, System.Int16 diasUteis, System.Int16 diasCorridos)
		{
			esTabelaTaxaSwapQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.CodigoTaxa == codigoTaxa, query.DiasUteis == diasUteis, query.DiasCorridos == diasCorridos);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.String codigoTaxa, System.Int16 diasUteis, System.Int16 diasCorridos)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("CodigoTaxa",codigoTaxa);			parms.Add("DiasUteis",diasUteis);			parms.Add("DiasCorridos",diasCorridos);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "CodigoTaxa": this.str.CodigoTaxa = (string)value; break;							
						case "DiasUteis": this.str.DiasUteis = (string)value; break;							
						case "DiasCorridos": this.str.DiasCorridos = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "DiasUteis":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.DiasUteis = (System.Int16?)value;
							break;
						
						case "DiasCorridos":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.DiasCorridos = (System.Int16?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaTaxaSwap.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaTaxaSwapMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaTaxaSwapMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaSwap.CodigoTaxa
		/// </summary>
		virtual public System.String CodigoTaxa
		{
			get
			{
				return base.GetSystemString(TabelaTaxaSwapMetadata.ColumnNames.CodigoTaxa);
			}
			
			set
			{
				base.SetSystemString(TabelaTaxaSwapMetadata.ColumnNames.CodigoTaxa, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaSwap.DiasUteis
		/// </summary>
		virtual public System.Int16? DiasUteis
		{
			get
			{
				return base.GetSystemInt16(TabelaTaxaSwapMetadata.ColumnNames.DiasUteis);
			}
			
			set
			{
				base.SetSystemInt16(TabelaTaxaSwapMetadata.ColumnNames.DiasUteis, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaSwap.DiasCorridos
		/// </summary>
		virtual public System.Int16? DiasCorridos
		{
			get
			{
				return base.GetSystemInt16(TabelaTaxaSwapMetadata.ColumnNames.DiasCorridos);
			}
			
			set
			{
				base.SetSystemInt16(TabelaTaxaSwapMetadata.ColumnNames.DiasCorridos, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaSwap.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(TabelaTaxaSwapMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTaxaSwapMetadata.ColumnNames.Valor, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaTaxaSwap entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodigoTaxa
			{
				get
				{
					System.String data = entity.CodigoTaxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoTaxa = null;
					else entity.CodigoTaxa = Convert.ToString(value);
				}
			}
				
			public System.String DiasUteis
			{
				get
				{
					System.Int16? data = entity.DiasUteis;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasUteis = null;
					else entity.DiasUteis = Convert.ToInt16(value);
				}
			}
				
			public System.String DiasCorridos
			{
				get
				{
					System.Int16? data = entity.DiasCorridos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasCorridos = null;
					else entity.DiasCorridos = Convert.ToInt16(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaTaxaSwap entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaTaxaSwapQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaTaxaSwap can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaTaxaSwap : esTabelaTaxaSwap
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaTaxaSwapQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaTaxaSwapMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaSwapMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodigoTaxa
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaSwapMetadata.ColumnNames.CodigoTaxa, esSystemType.String);
			}
		} 
		
		public esQueryItem DiasUteis
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaSwapMetadata.ColumnNames.DiasUteis, esSystemType.Int16);
			}
		} 
		
		public esQueryItem DiasCorridos
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaSwapMetadata.ColumnNames.DiasCorridos, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaSwapMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaTaxaSwapCollection")]
	public partial class TabelaTaxaSwapCollection : esTabelaTaxaSwapCollection, IEnumerable<TabelaTaxaSwap>
	{
		public TabelaTaxaSwapCollection()
		{

		}
		
		public static implicit operator List<TabelaTaxaSwap>(TabelaTaxaSwapCollection coll)
		{
			List<TabelaTaxaSwap> list = new List<TabelaTaxaSwap>();
			
			foreach (TabelaTaxaSwap emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaTaxaSwapMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaTaxaSwapQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaTaxaSwap(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaTaxaSwap();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaTaxaSwapQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaTaxaSwapQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaTaxaSwapQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaTaxaSwap AddNew()
		{
			TabelaTaxaSwap entity = base.AddNewEntity() as TabelaTaxaSwap;
			
			return entity;
		}

		public TabelaTaxaSwap FindByPrimaryKey(System.DateTime dataReferencia, System.String codigoTaxa, System.Int16 diasUteis, System.Int16 diasCorridos)
		{
			return base.FindByPrimaryKey(dataReferencia, codigoTaxa, diasUteis, diasCorridos) as TabelaTaxaSwap;
		}


		#region IEnumerable<TabelaTaxaSwap> Members

		IEnumerator<TabelaTaxaSwap> IEnumerable<TabelaTaxaSwap>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaTaxaSwap;
			}
		}

		#endregion
		
		private TabelaTaxaSwapQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaTaxaSwap' table
	/// </summary>

	[Serializable]
	public partial class TabelaTaxaSwap : esTabelaTaxaSwap
	{
		public TabelaTaxaSwap()
		{

		}
	
		public TabelaTaxaSwap(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaTaxaSwapMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaTaxaSwapQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaTaxaSwapQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaTaxaSwapQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaTaxaSwapQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaTaxaSwapQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaTaxaSwapQuery query;
	}



	[Serializable]
	public partial class TabelaTaxaSwapQuery : esTabelaTaxaSwapQuery
	{
		public TabelaTaxaSwapQuery()
		{

		}		
		
		public TabelaTaxaSwapQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaTaxaSwapMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaTaxaSwapMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaTaxaSwapMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaTaxaSwapMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaSwapMetadata.ColumnNames.CodigoTaxa, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaTaxaSwapMetadata.PropertyNames.CodigoTaxa;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaSwapMetadata.ColumnNames.DiasUteis, 2, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TabelaTaxaSwapMetadata.PropertyNames.DiasUteis;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaSwapMetadata.ColumnNames.DiasCorridos, 3, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TabelaTaxaSwapMetadata.PropertyNames.DiasCorridos;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaSwapMetadata.ColumnNames.Valor, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTaxaSwapMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 14;
			c.NumericScale = 7;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaTaxaSwapMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CodigoTaxa = "CodigoTaxa";
			 public const string DiasUteis = "DiasUteis";
			 public const string DiasCorridos = "DiasCorridos";
			 public const string Valor = "Valor";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CodigoTaxa = "CodigoTaxa";
			 public const string DiasUteis = "DiasUteis";
			 public const string DiasCorridos = "DiasCorridos";
			 public const string Valor = "Valor";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaTaxaSwapMetadata))
			{
				if(TabelaTaxaSwapMetadata.mapDelegates == null)
				{
					TabelaTaxaSwapMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaTaxaSwapMetadata.meta == null)
				{
					TabelaTaxaSwapMetadata.meta = new TabelaTaxaSwapMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CodigoTaxa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DiasUteis", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("DiasCorridos", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaTaxaSwap";
				meta.Destination = "TabelaTaxaSwap";
				
				meta.spInsert = "proc_TabelaTaxaSwapInsert";				
				meta.spUpdate = "proc_TabelaTaxaSwapUpdate";		
				meta.spDelete = "proc_TabelaTaxaSwapDelete";
				meta.spLoadAll = "proc_TabelaTaxaSwapLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaTaxaSwapLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaTaxaSwapMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
