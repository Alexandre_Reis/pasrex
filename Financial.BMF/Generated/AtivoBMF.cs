/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/07/2015 16:24:37
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;
using Financial.Gerencial;
using Financial.Fundo;



namespace Financial.BMF
{

	[Serializable]
	abstract public class esAtivoBMFCollection : esEntityCollection
	{
		public esAtivoBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AtivoBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esAtivoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAtivoBMFQuery);
		}
		#endregion
		
		virtual public AtivoBMF DetachEntity(AtivoBMF entity)
		{
			return base.DetachEntity(entity) as AtivoBMF;
		}
		
		virtual public AtivoBMF AttachEntity(AtivoBMF entity)
		{
			return base.AttachEntity(entity) as AtivoBMF;
		}
		
		virtual public void Combine(AtivoBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public AtivoBMF this[int index]
		{
			get
			{
				return base[index] as AtivoBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(AtivoBMF);
		}
	}



	[Serializable]
	abstract public class esAtivoBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAtivoBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esAtivoBMF()
		{

		}

		public esAtivoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String cdAtivoBMF, System.String serie)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivoBMF, serie);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivoBMF, serie);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.String cdAtivoBMF, System.String serie)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esAtivoBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.CdAtivoBMF == cdAtivoBMF, query.Serie == serie);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String cdAtivoBMF, System.String serie)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivoBMF, serie);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivoBMF, serie);
		}

		private bool LoadByPrimaryKeyDynamic(System.String cdAtivoBMF, System.String serie)
		{
			esAtivoBMFQuery query = this.GetDynamicQuery();
			query.Where(query.CdAtivoBMF == cdAtivoBMF, query.Serie == serie);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String cdAtivoBMF, System.String serie)
		{
			esParameters parms = new esParameters();
			parms.Add("CdAtivoBMF",cdAtivoBMF);			parms.Add("Serie",serie);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Serie": this.str.Serie = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "TipoSerie": this.str.TipoSerie = (string)value; break;							
						case "PrecoExercicio": this.str.PrecoExercicio = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "Peso": this.str.Peso = (string)value; break;							
						case "IdMoeda": this.str.IdMoeda = (string)value; break;							
						case "IdEstrategia": this.str.IdEstrategia = (string)value; break;							
						case "CodigoFeeder": this.str.CodigoFeeder = (string)value; break;							
						case "CodigoCDA": this.str.CodigoCDA = (string)value; break;							
						case "CodigoIsin": this.str.CodigoIsin = (string)value; break;							
						case "DataInicioVigencia": this.str.DataInicioVigencia = (string)value; break;							
						case "DataFimVigencia": this.str.DataFimVigencia = (string)value; break;							
						case "IdLocalCustodia": this.str.IdLocalCustodia = (string)value; break;							
						case "IdClearing": this.str.IdClearing = (string)value; break;							
						case "IdLocalNegociacao": this.str.IdLocalNegociacao = (string)value; break;							
						case "CodigoCusip": this.str.CodigoCusip = (string)value; break;							
						case "CodigoBDS": this.str.CodigoBDS = (string)value; break;
                        case "InvestimentoColetivoCvm": this.str.InvestimentoColetivoCvm = (string)value; break;
                    }
				}
				else
				{
					switch (name)
					{	
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoMercado = (System.Byte?)value;
							break;
						
						case "PrecoExercicio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrecoExercicio = (System.Decimal?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "Peso":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Peso = (System.Decimal?)value;
							break;
						
						case "IdMoeda":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMoeda = (System.Int32?)value;
							break;
						
						case "IdEstrategia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEstrategia = (System.Int32?)value;
							break;
						
						case "CodigoCDA":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoCDA = (System.Int32?)value;
							break;
						
						case "DataInicioVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicioVigencia = (System.DateTime?)value;
							break;
						
						case "DataFimVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFimVigencia = (System.DateTime?)value;
							break;
						
						case "IdLocalCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalCustodia = (System.Int32?)value;
							break;
						
						case "IdClearing":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClearing = (System.Int32?)value;
							break;
						
						case "IdLocalNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalNegociacao = (System.Int32?)value;
							break;
						
						case "CodigoBDS":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoBDS = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to AtivoBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(AtivoBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(AtivoBMFMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToClasseBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.Serie
		/// </summary>
		virtual public System.String Serie
		{
			get
			{
				return base.GetSystemString(AtivoBMFMetadata.ColumnNames.Serie);
			}
			
			set
			{
				base.SetSystemString(AtivoBMFMetadata.ColumnNames.Serie, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.TipoMercado
		/// </summary>
		virtual public System.Byte? TipoMercado
		{
			get
			{
				return base.GetSystemByte(AtivoBMFMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemByte(AtivoBMFMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.TipoSerie
		/// </summary>
		virtual public System.String TipoSerie
		{
			get
			{
				return base.GetSystemString(AtivoBMFMetadata.ColumnNames.TipoSerie);
			}
			
			set
			{
				base.SetSystemString(AtivoBMFMetadata.ColumnNames.TipoSerie, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.PrecoExercicio
		/// </summary>
		virtual public System.Decimal? PrecoExercicio
		{
			get
			{
				return base.GetSystemDecimal(AtivoBMFMetadata.ColumnNames.PrecoExercicio);
			}
			
			set
			{
				base.SetSystemDecimal(AtivoBMFMetadata.ColumnNames.PrecoExercicio, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(AtivoBMFMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(AtivoBMFMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.Peso
		/// </summary>
		virtual public System.Decimal? Peso
		{
			get
			{
				return base.GetSystemDecimal(AtivoBMFMetadata.ColumnNames.Peso);
			}
			
			set
			{
				base.SetSystemDecimal(AtivoBMFMetadata.ColumnNames.Peso, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.IdMoeda
		/// </summary>
		virtual public System.Int32? IdMoeda
		{
			get
			{
				return base.GetSystemInt32(AtivoBMFMetadata.ColumnNames.IdMoeda);
			}
			
			set
			{
				base.SetSystemInt32(AtivoBMFMetadata.ColumnNames.IdMoeda, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.IdEstrategia
		/// </summary>
		virtual public System.Int32? IdEstrategia
		{
			get
			{
				return base.GetSystemInt32(AtivoBMFMetadata.ColumnNames.IdEstrategia);
			}
			
			set
			{
				if(base.SetSystemInt32(AtivoBMFMetadata.ColumnNames.IdEstrategia, value))
				{
					this._UpToEstrategiaByIdEstrategia = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.CodigoFeeder
		/// </summary>
		virtual public System.String CodigoFeeder
		{
			get
			{
				return base.GetSystemString(AtivoBMFMetadata.ColumnNames.CodigoFeeder);
			}
			
			set
			{
				base.SetSystemString(AtivoBMFMetadata.ColumnNames.CodigoFeeder, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.CodigoCDA
		/// </summary>
		virtual public System.Int32? CodigoCDA
		{
			get
			{
				return base.GetSystemInt32(AtivoBMFMetadata.ColumnNames.CodigoCDA);
			}
			
			set
			{
				base.SetSystemInt32(AtivoBMFMetadata.ColumnNames.CodigoCDA, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.CodigoIsin
		/// </summary>
		virtual public System.String CodigoIsin
		{
			get
			{
				return base.GetSystemString(AtivoBMFMetadata.ColumnNames.CodigoIsin);
			}
			
			set
			{
				base.SetSystemString(AtivoBMFMetadata.ColumnNames.CodigoIsin, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.DataInicioVigencia
		/// </summary>
		virtual public System.DateTime? DataInicioVigencia
		{
			get
			{
				return base.GetSystemDateTime(AtivoBMFMetadata.ColumnNames.DataInicioVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(AtivoBMFMetadata.ColumnNames.DataInicioVigencia, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.DataFimVigencia
		/// </summary>
		virtual public System.DateTime? DataFimVigencia
		{
			get
			{
				return base.GetSystemDateTime(AtivoBMFMetadata.ColumnNames.DataFimVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(AtivoBMFMetadata.ColumnNames.DataFimVigencia, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.IdLocalCustodia
		/// </summary>
		virtual public System.Int32? IdLocalCustodia
		{
			get
			{
				return base.GetSystemInt32(AtivoBMFMetadata.ColumnNames.IdLocalCustodia);
			}
			
			set
			{
				base.SetSystemInt32(AtivoBMFMetadata.ColumnNames.IdLocalCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.IdClearing
		/// </summary>
		virtual public System.Int32? IdClearing
		{
			get
			{
				return base.GetSystemInt32(AtivoBMFMetadata.ColumnNames.IdClearing);
			}
			
			set
			{
				base.SetSystemInt32(AtivoBMFMetadata.ColumnNames.IdClearing, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.IdLocalNegociacao
		/// </summary>
		virtual public System.Int32? IdLocalNegociacao
		{
			get
			{
				return base.GetSystemInt32(AtivoBMFMetadata.ColumnNames.IdLocalNegociacao);
			}
			
			set
			{
				base.SetSystemInt32(AtivoBMFMetadata.ColumnNames.IdLocalNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.CodigoCusip
		/// </summary>
		virtual public System.String CodigoCusip
		{
			get
			{
				return base.GetSystemString(AtivoBMFMetadata.ColumnNames.CodigoCusip);
			}
			
			set
			{
				base.SetSystemString(AtivoBMFMetadata.ColumnNames.CodigoCusip, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBMF.CodigoBDS
		/// </summary>
		virtual public System.Int32? CodigoBDS
		{
			get
			{
				return base.GetSystemInt32(AtivoBMFMetadata.ColumnNames.CodigoBDS);
			}
			
			set
			{
				base.SetSystemInt32(AtivoBMFMetadata.ColumnNames.CodigoBDS, value);
			}
		}

        /// <summary>
        /// Maps to AtivoBMF.InvestimentoColetivoCvm
        /// </summary>
        virtual public System.String InvestimentoColetivoCvm
        {
            get
            {
                return base.GetSystemString(AtivoBMFMetadata.ColumnNames.InvestimentoColetivoCvm);
            }

            set
            {
                base.SetSystemString(AtivoBMFMetadata.ColumnNames.InvestimentoColetivoCvm, value);
            }
        }

        [CLSCompliant(false)]
		internal protected ClasseBMF _UpToClasseBMFByCdAtivoBMF;
		[CLSCompliant(false)]
		internal protected Estrategia _UpToEstrategiaByIdEstrategia;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAtivoBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Serie
			{
				get
				{
					System.String data = entity.Serie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Serie = null;
					else entity.Serie = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Byte? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToByte(value);
				}
			}
				
			public System.String TipoSerie
			{
				get
				{
					System.String data = entity.TipoSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoSerie = null;
					else entity.TipoSerie = Convert.ToString(value);
				}
			}
				
			public System.String PrecoExercicio
			{
				get
				{
					System.Decimal? data = entity.PrecoExercicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrecoExercicio = null;
					else entity.PrecoExercicio = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Peso
			{
				get
				{
					System.Decimal? data = entity.Peso;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Peso = null;
					else entity.Peso = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdMoeda
			{
				get
				{
					System.Int32? data = entity.IdMoeda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoeda = null;
					else entity.IdMoeda = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEstrategia
			{
				get
				{
					System.Int32? data = entity.IdEstrategia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEstrategia = null;
					else entity.IdEstrategia = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoFeeder
			{
				get
				{
					System.String data = entity.CodigoFeeder;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoFeeder = null;
					else entity.CodigoFeeder = Convert.ToString(value);
				}
			}
				
			public System.String CodigoCDA
			{
				get
				{
					System.Int32? data = entity.CodigoCDA;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCDA = null;
					else entity.CodigoCDA = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoIsin
			{
				get
				{
					System.String data = entity.CodigoIsin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoIsin = null;
					else entity.CodigoIsin = Convert.ToString(value);
				}
			}
				
			public System.String DataInicioVigencia
			{
				get
				{
					System.DateTime? data = entity.DataInicioVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicioVigencia = null;
					else entity.DataInicioVigencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataFimVigencia
			{
				get
				{
					System.DateTime? data = entity.DataFimVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFimVigencia = null;
					else entity.DataFimVigencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdLocalCustodia
			{
				get
				{
					System.Int32? data = entity.IdLocalCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalCustodia = null;
					else entity.IdLocalCustodia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdClearing
			{
				get
				{
					System.Int32? data = entity.IdClearing;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClearing = null;
					else entity.IdClearing = Convert.ToInt32(value);
				}
			}
				
			public System.String IdLocalNegociacao
			{
				get
				{
					System.Int32? data = entity.IdLocalNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalNegociacao = null;
					else entity.IdLocalNegociacao = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoCusip
			{
				get
				{
					System.String data = entity.CodigoCusip;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCusip = null;
					else entity.CodigoCusip = Convert.ToString(value);
				}
			}
				
			public System.String CodigoBDS
			{
				get
				{
					System.Int32? data = entity.CodigoBDS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoBDS = null;
					else entity.CodigoBDS = Convert.ToInt32(value);
				}
			}

            public System.String InvestimentoColetivoCvm
            {
                get
                {
                    System.String data = entity.InvestimentoColetivoCvm;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.InvestimentoColetivoCvm = null;
                    else entity.InvestimentoColetivoCvm = Convert.ToString(value);
                }
            }

            private esAtivoBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAtivoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAtivoBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class AtivoBMF : esAtivoBMF
	{

				
		#region CotacaoBMFCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBMF_CotacaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public CotacaoBMFCollection CotacaoBMFCollectionByCdAtivoBMF
		{
			get
			{
				if(this._CotacaoBMFCollectionByCdAtivoBMF == null)
				{
					this._CotacaoBMFCollectionByCdAtivoBMF = new CotacaoBMFCollection();
					this._CotacaoBMFCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CotacaoBMFCollectionByCdAtivoBMF", this._CotacaoBMFCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null && this.Serie != null)
					{
						this._CotacaoBMFCollectionByCdAtivoBMF.Query.Where(this._CotacaoBMFCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._CotacaoBMFCollectionByCdAtivoBMF.Query.Where(this._CotacaoBMFCollectionByCdAtivoBMF.Query.Serie == this.Serie);
						this._CotacaoBMFCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._CotacaoBMFCollectionByCdAtivoBMF.fks.Add(CotacaoBMFMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
						this._CotacaoBMFCollectionByCdAtivoBMF.fks.Add(CotacaoBMFMetadata.ColumnNames.Serie, this.Serie);
					}
				}

				return this._CotacaoBMFCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CotacaoBMFCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("CotacaoBMFCollectionByCdAtivoBMF"); 
					this._CotacaoBMFCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private CotacaoBMFCollection _CotacaoBMFCollectionByCdAtivoBMF;
		#endregion

				
		#region ExcecoesTributacaoIRCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_AtivoBMF
		/// </summary>

		[XmlIgnore]
		public ExcecoesTributacaoIRCollection ExcecoesTributacaoIRCollectionByCdAtivoBMF
		{
			get
			{
				if(this._ExcecoesTributacaoIRCollectionByCdAtivoBMF == null)
				{
					this._ExcecoesTributacaoIRCollectionByCdAtivoBMF = new ExcecoesTributacaoIRCollection();
					this._ExcecoesTributacaoIRCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ExcecoesTributacaoIRCollectionByCdAtivoBMF", this._ExcecoesTributacaoIRCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null && this.Serie != null)
					{
						this._ExcecoesTributacaoIRCollectionByCdAtivoBMF.Query.Where(this._ExcecoesTributacaoIRCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._ExcecoesTributacaoIRCollectionByCdAtivoBMF.Query.Where(this._ExcecoesTributacaoIRCollectionByCdAtivoBMF.Query.SerieBMF == this.Serie);
						this._ExcecoesTributacaoIRCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._ExcecoesTributacaoIRCollectionByCdAtivoBMF.fks.Add(ExcecoesTributacaoIRMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
						this._ExcecoesTributacaoIRCollectionByCdAtivoBMF.fks.Add(ExcecoesTributacaoIRMetadata.ColumnNames.SerieBMF, this.Serie);
					}
				}

				return this._ExcecoesTributacaoIRCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ExcecoesTributacaoIRCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("ExcecoesTributacaoIRCollectionByCdAtivoBMF"); 
					this._ExcecoesTributacaoIRCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private ExcecoesTributacaoIRCollection _ExcecoesTributacaoIRCollectionByCdAtivoBMF;
		#endregion

				
		#region GerOperacaoBMFCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBMF_GerOperacaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public GerOperacaoBMFCollection GerOperacaoBMFCollectionByCdAtivoBMF
		{
			get
			{
				if(this._GerOperacaoBMFCollectionByCdAtivoBMF == null)
				{
					this._GerOperacaoBMFCollectionByCdAtivoBMF = new GerOperacaoBMFCollection();
					this._GerOperacaoBMFCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerOperacaoBMFCollectionByCdAtivoBMF", this._GerOperacaoBMFCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null && this.Serie != null)
					{
						this._GerOperacaoBMFCollectionByCdAtivoBMF.Query.Where(this._GerOperacaoBMFCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._GerOperacaoBMFCollectionByCdAtivoBMF.Query.Where(this._GerOperacaoBMFCollectionByCdAtivoBMF.Query.Serie == this.Serie);
						this._GerOperacaoBMFCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerOperacaoBMFCollectionByCdAtivoBMF.fks.Add(GerOperacaoBMFMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
						this._GerOperacaoBMFCollectionByCdAtivoBMF.fks.Add(GerOperacaoBMFMetadata.ColumnNames.Serie, this.Serie);
					}
				}

				return this._GerOperacaoBMFCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerOperacaoBMFCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("GerOperacaoBMFCollectionByCdAtivoBMF"); 
					this._GerOperacaoBMFCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private GerOperacaoBMFCollection _GerOperacaoBMFCollectionByCdAtivoBMF;
		#endregion

				
		#region GerPosicaoBMFCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBMF_GerPosicaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBMFCollection GerPosicaoBMFCollectionByCdAtivoBMF
		{
			get
			{
				if(this._GerPosicaoBMFCollectionByCdAtivoBMF == null)
				{
					this._GerPosicaoBMFCollectionByCdAtivoBMF = new GerPosicaoBMFCollection();
					this._GerPosicaoBMFCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBMFCollectionByCdAtivoBMF", this._GerPosicaoBMFCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null && this.Serie != null)
					{
						this._GerPosicaoBMFCollectionByCdAtivoBMF.Query.Where(this._GerPosicaoBMFCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._GerPosicaoBMFCollectionByCdAtivoBMF.Query.Where(this._GerPosicaoBMFCollectionByCdAtivoBMF.Query.Serie == this.Serie);
						this._GerPosicaoBMFCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBMFCollectionByCdAtivoBMF.fks.Add(GerPosicaoBMFMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
						this._GerPosicaoBMFCollectionByCdAtivoBMF.fks.Add(GerPosicaoBMFMetadata.ColumnNames.Serie, this.Serie);
					}
				}

				return this._GerPosicaoBMFCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBMFCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("GerPosicaoBMFCollectionByCdAtivoBMF"); 
					this._GerPosicaoBMFCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private GerPosicaoBMFCollection _GerPosicaoBMFCollectionByCdAtivoBMF;
		#endregion

				
		#region GerPosicaoBMFAberturaCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBMF_GerPosicaoBMFAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBMFAberturaCollection GerPosicaoBMFAberturaCollectionByCdAtivoBMF
		{
			get
			{
				if(this._GerPosicaoBMFAberturaCollectionByCdAtivoBMF == null)
				{
					this._GerPosicaoBMFAberturaCollectionByCdAtivoBMF = new GerPosicaoBMFAberturaCollection();
					this._GerPosicaoBMFAberturaCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBMFAberturaCollectionByCdAtivoBMF", this._GerPosicaoBMFAberturaCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null && this.Serie != null)
					{
						this._GerPosicaoBMFAberturaCollectionByCdAtivoBMF.Query.Where(this._GerPosicaoBMFAberturaCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._GerPosicaoBMFAberturaCollectionByCdAtivoBMF.Query.Where(this._GerPosicaoBMFAberturaCollectionByCdAtivoBMF.Query.Serie == this.Serie);
						this._GerPosicaoBMFAberturaCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBMFAberturaCollectionByCdAtivoBMF.fks.Add(GerPosicaoBMFAberturaMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
						this._GerPosicaoBMFAberturaCollectionByCdAtivoBMF.fks.Add(GerPosicaoBMFAberturaMetadata.ColumnNames.Serie, this.Serie);
					}
				}

				return this._GerPosicaoBMFAberturaCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBMFAberturaCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("GerPosicaoBMFAberturaCollectionByCdAtivoBMF"); 
					this._GerPosicaoBMFAberturaCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private GerPosicaoBMFAberturaCollection _GerPosicaoBMFAberturaCollectionByCdAtivoBMF;
		#endregion

				
		#region GerPosicaoBMFHistoricoCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBMF_GerPosicaoBMFHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBMFHistoricoCollection GerPosicaoBMFHistoricoCollectionByCdAtivoBMF
		{
			get
			{
				if(this._GerPosicaoBMFHistoricoCollectionByCdAtivoBMF == null)
				{
					this._GerPosicaoBMFHistoricoCollectionByCdAtivoBMF = new GerPosicaoBMFHistoricoCollection();
					this._GerPosicaoBMFHistoricoCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBMFHistoricoCollectionByCdAtivoBMF", this._GerPosicaoBMFHistoricoCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null && this.Serie != null)
					{
						this._GerPosicaoBMFHistoricoCollectionByCdAtivoBMF.Query.Where(this._GerPosicaoBMFHistoricoCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._GerPosicaoBMFHistoricoCollectionByCdAtivoBMF.Query.Where(this._GerPosicaoBMFHistoricoCollectionByCdAtivoBMF.Query.Serie == this.Serie);
						this._GerPosicaoBMFHistoricoCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBMFHistoricoCollectionByCdAtivoBMF.fks.Add(GerPosicaoBMFHistoricoMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
						this._GerPosicaoBMFHistoricoCollectionByCdAtivoBMF.fks.Add(GerPosicaoBMFHistoricoMetadata.ColumnNames.Serie, this.Serie);
					}
				}

				return this._GerPosicaoBMFHistoricoCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBMFHistoricoCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("GerPosicaoBMFHistoricoCollectionByCdAtivoBMF"); 
					this._GerPosicaoBMFHistoricoCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private GerPosicaoBMFHistoricoCollection _GerPosicaoBMFHistoricoCollectionByCdAtivoBMF;
		#endregion

				
		#region OperacaoBMFCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBMF_OperacaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoBMFCollection OperacaoBMFCollectionByCdAtivoBMF
		{
			get
			{
				if(this._OperacaoBMFCollectionByCdAtivoBMF == null)
				{
					this._OperacaoBMFCollectionByCdAtivoBMF = new OperacaoBMFCollection();
					this._OperacaoBMFCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoBMFCollectionByCdAtivoBMF", this._OperacaoBMFCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null && this.Serie != null)
					{
						this._OperacaoBMFCollectionByCdAtivoBMF.Query.Where(this._OperacaoBMFCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._OperacaoBMFCollectionByCdAtivoBMF.Query.Where(this._OperacaoBMFCollectionByCdAtivoBMF.Query.Serie == this.Serie);
						this._OperacaoBMFCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoBMFCollectionByCdAtivoBMF.fks.Add(OperacaoBMFMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
						this._OperacaoBMFCollectionByCdAtivoBMF.fks.Add(OperacaoBMFMetadata.ColumnNames.Serie, this.Serie);
					}
				}

				return this._OperacaoBMFCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoBMFCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("OperacaoBMFCollectionByCdAtivoBMF"); 
					this._OperacaoBMFCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private OperacaoBMFCollection _OperacaoBMFCollectionByCdAtivoBMF;
		#endregion

				
		#region OrdemBMFCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBMF_OrdemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemBMFCollection OrdemBMFCollectionByCdAtivoBMF
		{
			get
			{
				if(this._OrdemBMFCollectionByCdAtivoBMF == null)
				{
					this._OrdemBMFCollectionByCdAtivoBMF = new OrdemBMFCollection();
					this._OrdemBMFCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemBMFCollectionByCdAtivoBMF", this._OrdemBMFCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null && this.Serie != null)
					{
						this._OrdemBMFCollectionByCdAtivoBMF.Query.Where(this._OrdemBMFCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._OrdemBMFCollectionByCdAtivoBMF.Query.Where(this._OrdemBMFCollectionByCdAtivoBMF.Query.Serie == this.Serie);
						this._OrdemBMFCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemBMFCollectionByCdAtivoBMF.fks.Add(OrdemBMFMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
						this._OrdemBMFCollectionByCdAtivoBMF.fks.Add(OrdemBMFMetadata.ColumnNames.Serie, this.Serie);
					}
				}

				return this._OrdemBMFCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemBMFCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("OrdemBMFCollectionByCdAtivoBMF"); 
					this._OrdemBMFCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private OrdemBMFCollection _OrdemBMFCollectionByCdAtivoBMF;
		#endregion

				
		#region PosicaoBMFCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBMF_PosicaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBMFCollection PosicaoBMFCollectionByCdAtivoBMF
		{
			get
			{
				if(this._PosicaoBMFCollectionByCdAtivoBMF == null)
				{
					this._PosicaoBMFCollectionByCdAtivoBMF = new PosicaoBMFCollection();
					this._PosicaoBMFCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBMFCollectionByCdAtivoBMF", this._PosicaoBMFCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null && this.Serie != null)
					{
						this._PosicaoBMFCollectionByCdAtivoBMF.Query.Where(this._PosicaoBMFCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._PosicaoBMFCollectionByCdAtivoBMF.Query.Where(this._PosicaoBMFCollectionByCdAtivoBMF.Query.Serie == this.Serie);
						this._PosicaoBMFCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBMFCollectionByCdAtivoBMF.fks.Add(PosicaoBMFMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
						this._PosicaoBMFCollectionByCdAtivoBMF.fks.Add(PosicaoBMFMetadata.ColumnNames.Serie, this.Serie);
					}
				}

				return this._PosicaoBMFCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBMFCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("PosicaoBMFCollectionByCdAtivoBMF"); 
					this._PosicaoBMFCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private PosicaoBMFCollection _PosicaoBMFCollectionByCdAtivoBMF;
		#endregion

				
		#region PosicaoBMFAberturaCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBMF_PosicaoBMFAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBMFAberturaCollection PosicaoBMFAberturaCollectionByCdAtivoBMF
		{
			get
			{
				if(this._PosicaoBMFAberturaCollectionByCdAtivoBMF == null)
				{
					this._PosicaoBMFAberturaCollectionByCdAtivoBMF = new PosicaoBMFAberturaCollection();
					this._PosicaoBMFAberturaCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBMFAberturaCollectionByCdAtivoBMF", this._PosicaoBMFAberturaCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null && this.Serie != null)
					{
						this._PosicaoBMFAberturaCollectionByCdAtivoBMF.Query.Where(this._PosicaoBMFAberturaCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._PosicaoBMFAberturaCollectionByCdAtivoBMF.Query.Where(this._PosicaoBMFAberturaCollectionByCdAtivoBMF.Query.Serie == this.Serie);
						this._PosicaoBMFAberturaCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBMFAberturaCollectionByCdAtivoBMF.fks.Add(PosicaoBMFAberturaMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
						this._PosicaoBMFAberturaCollectionByCdAtivoBMF.fks.Add(PosicaoBMFAberturaMetadata.ColumnNames.Serie, this.Serie);
					}
				}

				return this._PosicaoBMFAberturaCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBMFAberturaCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("PosicaoBMFAberturaCollectionByCdAtivoBMF"); 
					this._PosicaoBMFAberturaCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private PosicaoBMFAberturaCollection _PosicaoBMFAberturaCollectionByCdAtivoBMF;
		#endregion

				
		#region PosicaoBMFHistoricoCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBMF_PosicaoBMFHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBMFHistoricoCollection PosicaoBMFHistoricoCollectionByCdAtivoBMF
		{
			get
			{
				if(this._PosicaoBMFHistoricoCollectionByCdAtivoBMF == null)
				{
					this._PosicaoBMFHistoricoCollectionByCdAtivoBMF = new PosicaoBMFHistoricoCollection();
					this._PosicaoBMFHistoricoCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBMFHistoricoCollectionByCdAtivoBMF", this._PosicaoBMFHistoricoCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null && this.Serie != null)
					{
						this._PosicaoBMFHistoricoCollectionByCdAtivoBMF.Query.Where(this._PosicaoBMFHistoricoCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._PosicaoBMFHistoricoCollectionByCdAtivoBMF.Query.Where(this._PosicaoBMFHistoricoCollectionByCdAtivoBMF.Query.Serie == this.Serie);
						this._PosicaoBMFHistoricoCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBMFHistoricoCollectionByCdAtivoBMF.fks.Add(PosicaoBMFHistoricoMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
						this._PosicaoBMFHistoricoCollectionByCdAtivoBMF.fks.Add(PosicaoBMFHistoricoMetadata.ColumnNames.Serie, this.Serie);
					}
				}

				return this._PosicaoBMFHistoricoCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBMFHistoricoCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("PosicaoBMFHistoricoCollectionByCdAtivoBMF"); 
					this._PosicaoBMFHistoricoCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private PosicaoBMFHistoricoCollection _PosicaoBMFHistoricoCollectionByCdAtivoBMF;
		#endregion

				
		#region TabelaCustosBMFCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBMF_TabelaCustosBMF_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaCustosBMFCollection TabelaCustosBMFCollectionByCdAtivoBMF
		{
			get
			{
				if(this._TabelaCustosBMFCollectionByCdAtivoBMF == null)
				{
					this._TabelaCustosBMFCollectionByCdAtivoBMF = new TabelaCustosBMFCollection();
					this._TabelaCustosBMFCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaCustosBMFCollectionByCdAtivoBMF", this._TabelaCustosBMFCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null && this.Serie != null)
					{
						this._TabelaCustosBMFCollectionByCdAtivoBMF.Query.Where(this._TabelaCustosBMFCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._TabelaCustosBMFCollectionByCdAtivoBMF.Query.Where(this._TabelaCustosBMFCollectionByCdAtivoBMF.Query.Serie == this.Serie);
						this._TabelaCustosBMFCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaCustosBMFCollectionByCdAtivoBMF.fks.Add(TabelaCustosBMFMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
						this._TabelaCustosBMFCollectionByCdAtivoBMF.fks.Add(TabelaCustosBMFMetadata.ColumnNames.Serie, this.Serie);
					}
				}

				return this._TabelaCustosBMFCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaCustosBMFCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("TabelaCustosBMFCollectionByCdAtivoBMF"); 
					this._TabelaCustosBMFCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private TabelaCustosBMFCollection _TabelaCustosBMFCollectionByCdAtivoBMF;
		#endregion

				
		#region TabelaVencimentosBMFCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBMF_TabelaVencimentosBMF_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaVencimentosBMFCollection TabelaVencimentosBMFCollectionByCdAtivoBMF
		{
			get
			{
				if(this._TabelaVencimentosBMFCollectionByCdAtivoBMF == null)
				{
					this._TabelaVencimentosBMFCollectionByCdAtivoBMF = new TabelaVencimentosBMFCollection();
					this._TabelaVencimentosBMFCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaVencimentosBMFCollectionByCdAtivoBMF", this._TabelaVencimentosBMFCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null && this.Serie != null)
					{
						this._TabelaVencimentosBMFCollectionByCdAtivoBMF.Query.Where(this._TabelaVencimentosBMFCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._TabelaVencimentosBMFCollectionByCdAtivoBMF.Query.Where(this._TabelaVencimentosBMFCollectionByCdAtivoBMF.Query.Serie == this.Serie);
						this._TabelaVencimentosBMFCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaVencimentosBMFCollectionByCdAtivoBMF.fks.Add(TabelaVencimentosBMFMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
						this._TabelaVencimentosBMFCollectionByCdAtivoBMF.fks.Add(TabelaVencimentosBMFMetadata.ColumnNames.Serie, this.Serie);
					}
				}

				return this._TabelaVencimentosBMFCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaVencimentosBMFCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("TabelaVencimentosBMFCollectionByCdAtivoBMF"); 
					this._TabelaVencimentosBMFCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private TabelaVencimentosBMFCollection _TabelaVencimentosBMFCollectionByCdAtivoBMF;
		#endregion

				
		#region TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBMF_TabelaVolatilidadeEstrategia_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaVolatilidadeEstrategiaCollection TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF
		{
			get
			{
				if(this._TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF == null)
				{
					this._TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF = new TabelaVolatilidadeEstrategiaCollection();
					this._TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF", this._TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null && this.Serie != null)
					{
						this._TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF.Query.Where(this._TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF.Query.Where(this._TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF.Query.Serie == this.Serie);
						this._TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF.fks.Add(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
						this._TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF.fks.Add(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.Serie, this.Serie);
					}
				}

				return this._TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF"); 
					this._TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private TabelaVolatilidadeEstrategiaCollection _TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF;
		#endregion

				
		#region TransferenciaBMFCollectionByCdAtivoBMF - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBMF_TransferenciaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public TransferenciaBMFCollection TransferenciaBMFCollectionByCdAtivoBMF
		{
			get
			{
				if(this._TransferenciaBMFCollectionByCdAtivoBMF == null)
				{
					this._TransferenciaBMFCollectionByCdAtivoBMF = new TransferenciaBMFCollection();
					this._TransferenciaBMFCollectionByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaBMFCollectionByCdAtivoBMF", this._TransferenciaBMFCollectionByCdAtivoBMF);
				
					if(this.CdAtivoBMF != null && this.Serie != null)
					{
						this._TransferenciaBMFCollectionByCdAtivoBMF.Query.Where(this._TransferenciaBMFCollectionByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
						this._TransferenciaBMFCollectionByCdAtivoBMF.Query.Where(this._TransferenciaBMFCollectionByCdAtivoBMF.Query.Serie == this.Serie);
						this._TransferenciaBMFCollectionByCdAtivoBMF.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaBMFCollectionByCdAtivoBMF.fks.Add(TransferenciaBMFMetadata.ColumnNames.CdAtivoBMF, this.CdAtivoBMF);
						this._TransferenciaBMFCollectionByCdAtivoBMF.fks.Add(TransferenciaBMFMetadata.ColumnNames.Serie, this.Serie);
					}
				}

				return this._TransferenciaBMFCollectionByCdAtivoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaBMFCollectionByCdAtivoBMF != null) 
				{ 
					this.RemovePostSave("TransferenciaBMFCollectionByCdAtivoBMF"); 
					this._TransferenciaBMFCollectionByCdAtivoBMF = null;
					
				} 
			} 			
		}

		private TransferenciaBMFCollection _TransferenciaBMFCollectionByCdAtivoBMF;
		#endregion

				
		#region UpToClasseBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ClasseBMF_AtivoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public ClasseBMF UpToClasseBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToClasseBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					)
				{
					this._UpToClasseBMFByCdAtivoBMF = new ClasseBMF();
					this._UpToClasseBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClasseBMFByCdAtivoBMF", this._UpToClasseBMFByCdAtivoBMF);
					this._UpToClasseBMFByCdAtivoBMF.Query.Where(this._UpToClasseBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToClasseBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToClasseBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToClasseBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this._UpToClasseBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this._UpToClasseBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToClasseBMFByCdAtivoBMF", this._UpToClasseBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

				
		#region UpToEstrategiaByIdEstrategia - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Estrategia_AtivoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public Estrategia UpToEstrategiaByIdEstrategia
		{
			get
			{
				if(this._UpToEstrategiaByIdEstrategia == null
					&& IdEstrategia != null					)
				{
					this._UpToEstrategiaByIdEstrategia = new Estrategia();
					this._UpToEstrategiaByIdEstrategia.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEstrategiaByIdEstrategia", this._UpToEstrategiaByIdEstrategia);
					this._UpToEstrategiaByIdEstrategia.Query.Where(this._UpToEstrategiaByIdEstrategia.Query.IdEstrategia == this.IdEstrategia);
					this._UpToEstrategiaByIdEstrategia.Query.Load();
				}

				return this._UpToEstrategiaByIdEstrategia;
			}
			
			set
			{
				this.RemovePreSave("UpToEstrategiaByIdEstrategia");
				

				if(value == null)
				{
					this.IdEstrategia = null;
					this._UpToEstrategiaByIdEstrategia = null;
				}
				else
				{
					this.IdEstrategia = value.IdEstrategia;
					this._UpToEstrategiaByIdEstrategia = value;
					this.SetPreSave("UpToEstrategiaByIdEstrategia", this._UpToEstrategiaByIdEstrategia);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "CotacaoBMFCollectionByCdAtivoBMF", typeof(CotacaoBMFCollection), new CotacaoBMF()));
			props.Add(new esPropertyDescriptor(this, "ExcecoesTributacaoIRCollectionByCdAtivoBMF", typeof(ExcecoesTributacaoIRCollection), new ExcecoesTributacaoIR()));
			props.Add(new esPropertyDescriptor(this, "GerOperacaoBMFCollectionByCdAtivoBMF", typeof(GerOperacaoBMFCollection), new GerOperacaoBMF()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBMFCollectionByCdAtivoBMF", typeof(GerPosicaoBMFCollection), new GerPosicaoBMF()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBMFAberturaCollectionByCdAtivoBMF", typeof(GerPosicaoBMFAberturaCollection), new GerPosicaoBMFAbertura()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBMFHistoricoCollectionByCdAtivoBMF", typeof(GerPosicaoBMFHistoricoCollection), new GerPosicaoBMFHistorico()));
			props.Add(new esPropertyDescriptor(this, "OperacaoBMFCollectionByCdAtivoBMF", typeof(OperacaoBMFCollection), new OperacaoBMF()));
			props.Add(new esPropertyDescriptor(this, "OrdemBMFCollectionByCdAtivoBMF", typeof(OrdemBMFCollection), new OrdemBMF()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBMFCollectionByCdAtivoBMF", typeof(PosicaoBMFCollection), new PosicaoBMF()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBMFAberturaCollectionByCdAtivoBMF", typeof(PosicaoBMFAberturaCollection), new PosicaoBMFAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBMFHistoricoCollectionByCdAtivoBMF", typeof(PosicaoBMFHistoricoCollection), new PosicaoBMFHistorico()));
			props.Add(new esPropertyDescriptor(this, "TabelaCustosBMFCollectionByCdAtivoBMF", typeof(TabelaCustosBMFCollection), new TabelaCustosBMF()));
			props.Add(new esPropertyDescriptor(this, "TabelaVencimentosBMFCollectionByCdAtivoBMF", typeof(TabelaVencimentosBMFCollection), new TabelaVencimentosBMF()));
			props.Add(new esPropertyDescriptor(this, "TabelaVolatilidadeEstrategiaCollectionByCdAtivoBMF", typeof(TabelaVolatilidadeEstrategiaCollection), new TabelaVolatilidadeEstrategia()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaBMFCollectionByCdAtivoBMF", typeof(TransferenciaBMFCollection), new TransferenciaBMF()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEstrategiaByIdEstrategia != null)
			{
				this.IdEstrategia = this._UpToEstrategiaByIdEstrategia.IdEstrategia;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAtivoBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AtivoBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Serie
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.Serie, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.TipoMercado, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoSerie
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.TipoSerie, esSystemType.String);
			}
		} 
		
		public esQueryItem PrecoExercicio
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.PrecoExercicio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Peso
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.Peso, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdMoeda
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.IdMoeda, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEstrategia
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.IdEstrategia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoFeeder
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.CodigoFeeder, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoCDA
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.CodigoCDA, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoIsin
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.CodigoIsin, esSystemType.String);
			}
		} 
		
		public esQueryItem DataInicioVigencia
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.DataInicioVigencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataFimVigencia
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.DataFimVigencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdLocalCustodia
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.IdLocalCustodia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdClearing
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.IdClearing, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdLocalNegociacao
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.IdLocalNegociacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoCusip
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.CodigoCusip, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoBDS
		{
			get
			{
				return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.CodigoBDS, esSystemType.Int32);
			}
		}

        public esQueryItem InvestimentoColetivoCvm
        {
            get
            {
                return new esQueryItem(this, AtivoBMFMetadata.ColumnNames.InvestimentoColetivoCvm, esSystemType.String);
            }
        }
    }



	[Serializable]
	[XmlType("AtivoBMFCollection")]
	public partial class AtivoBMFCollection : esAtivoBMFCollection, IEnumerable<AtivoBMF>
	{
		public AtivoBMFCollection()
		{

		}
		
		public static implicit operator List<AtivoBMF>(AtivoBMFCollection coll)
		{
			List<AtivoBMF> list = new List<AtivoBMF>();
			
			foreach (AtivoBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AtivoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AtivoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new AtivoBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new AtivoBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AtivoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AtivoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AtivoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public AtivoBMF AddNew()
		{
			AtivoBMF entity = base.AddNewEntity() as AtivoBMF;
			
			return entity;
		}

		public AtivoBMF FindByPrimaryKey(System.String cdAtivoBMF, System.String serie)
		{
			return base.FindByPrimaryKey(cdAtivoBMF, serie) as AtivoBMF;
		}


		#region IEnumerable<AtivoBMF> Members

		IEnumerator<AtivoBMF> IEnumerable<AtivoBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as AtivoBMF;
			}
		}

		#endregion
		
		private AtivoBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'AtivoBMF' table
	/// </summary>

	[Serializable]
	public partial class AtivoBMF : esAtivoBMF
	{
		public AtivoBMF()
		{

		}
	
		public AtivoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AtivoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esAtivoBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AtivoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AtivoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AtivoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AtivoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AtivoBMFQuery query;
	}



	[Serializable]
	public partial class AtivoBMFQuery : esAtivoBMFQuery
	{
		public AtivoBMFQuery()
		{

		}		
		
		public AtivoBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AtivoBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AtivoBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.CdAtivoBMF, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.CdAtivoBMF;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.Serie, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.Serie;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.TipoMercado, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.TipoMercado;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.TipoSerie, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.TipoSerie;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.PrecoExercicio, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.PrecoExercicio;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.DataVencimento, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.Peso, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.Peso;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.IdMoeda, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.IdMoeda;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.IdEstrategia, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.IdEstrategia;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.CodigoFeeder, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.CodigoFeeder;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.CodigoCDA, 10, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.CodigoCDA;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.CodigoIsin, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.CodigoIsin;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.DataInicioVigencia, 12, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.DataInicioVigencia;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.DataFimVigencia, 13, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.DataFimVigencia;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.IdLocalCustodia, 14, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.IdLocalCustodia;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('3')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.IdClearing, 15, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.IdClearing;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('4')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.IdLocalNegociacao, 16, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.IdLocalNegociacao;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('5')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.CodigoCusip, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.CodigoCusip;
			c.CharacterMaxLength = 9;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.CodigoBDS, 18, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBMFMetadata.PropertyNames.CodigoBDS;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c);

            c = new esColumnMetadata(AtivoBMFMetadata.ColumnNames.InvestimentoColetivoCvm, 19, typeof(System.String), esSystemType.String);
            c.PropertyName = AtivoBMFMetadata.PropertyNames.InvestimentoColetivoCvm;
            c.CharacterMaxLength = 1;
            c.NumericPrecision = 0;
            c.HasDefault = true;
            c.Default = @"('N')";
            _columns.Add(c);

        }
		#endregion
	
		static public AtivoBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoSerie = "TipoSerie";
			 public const string PrecoExercicio = "PrecoExercicio";
			 public const string DataVencimento = "DataVencimento";
			 public const string Peso = "Peso";
			 public const string IdMoeda = "IdMoeda";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string CodigoFeeder = "CodigoFeeder";
			 public const string CodigoCDA = "CodigoCDA";
			 public const string CodigoIsin = "CodigoIsin";
			 public const string DataInicioVigencia = "DataInicioVigencia";
			 public const string DataFimVigencia = "DataFimVigencia";
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string IdClearing = "IdClearing";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string CodigoCusip = "CodigoCusip";
			 public const string CodigoBDS = "CodigoBDS";
             public const string InvestimentoColetivoCvm = "InvestimentoColetivoCvm";
        }
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoSerie = "TipoSerie";
			 public const string PrecoExercicio = "PrecoExercicio";
			 public const string DataVencimento = "DataVencimento";
			 public const string Peso = "Peso";
			 public const string IdMoeda = "IdMoeda";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string CodigoFeeder = "CodigoFeeder";
			 public const string CodigoCDA = "CodigoCDA";
			 public const string CodigoIsin = "CodigoIsin";
			 public const string DataInicioVigencia = "DataInicioVigencia";
			 public const string DataFimVigencia = "DataFimVigencia";
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string IdClearing = "IdClearing";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string CodigoCusip = "CodigoCusip";
			 public const string CodigoBDS = "CodigoBDS";
             public const string InvestimentoColetivoCvm = "InvestimentoColetivoCvm";
        }
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AtivoBMFMetadata))
			{
				if(AtivoBMFMetadata.mapDelegates == null)
				{
					AtivoBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AtivoBMFMetadata.meta == null)
				{
					AtivoBMFMetadata.meta = new AtivoBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Serie", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoSerie", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("PrecoExercicio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Peso", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdMoeda", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEstrategia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoFeeder", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoCDA", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoIsin", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataInicioVigencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataFimVigencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdLocalCustodia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdClearing", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdLocalNegociacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoCusip", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoBDS", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("InvestimentoColetivoCvm", new esTypeMap("char", "System.String"));



                meta.Source = "AtivoBMF";
				meta.Destination = "AtivoBMF";
				
				meta.spInsert = "proc_AtivoBMFInsert";				
				meta.spUpdate = "proc_AtivoBMFUpdate";		
				meta.spDelete = "proc_AtivoBMFDelete";
				meta.spLoadAll = "proc_AtivoBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_AtivoBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AtivoBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
