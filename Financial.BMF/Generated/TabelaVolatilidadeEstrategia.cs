/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2012 19:02:06
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esTabelaVolatilidadeEstrategiaCollection : esEntityCollection
	{
		public esTabelaVolatilidadeEstrategiaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaVolatilidadeEstrategiaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaVolatilidadeEstrategiaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaVolatilidadeEstrategiaQuery);
		}
		#endregion
		
		virtual public TabelaVolatilidadeEstrategia DetachEntity(TabelaVolatilidadeEstrategia entity)
		{
			return base.DetachEntity(entity) as TabelaVolatilidadeEstrategia;
		}
		
		virtual public TabelaVolatilidadeEstrategia AttachEntity(TabelaVolatilidadeEstrategia entity)
		{
			return base.AttachEntity(entity) as TabelaVolatilidadeEstrategia;
		}
		
		virtual public void Combine(TabelaVolatilidadeEstrategiaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaVolatilidadeEstrategia this[int index]
		{
			get
			{
				return base[index] as TabelaVolatilidadeEstrategia;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaVolatilidadeEstrategia);
		}
	}



	[Serializable]
	abstract public class esTabelaVolatilidadeEstrategia : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaVolatilidadeEstrategiaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaVolatilidadeEstrategia()
		{

		}

		public esTabelaVolatilidadeEstrategia(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, cdAtivoBMF, serie);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, cdAtivoBMF, serie);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaVolatilidadeEstrategiaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.CdAtivoBMF == cdAtivoBMF, query.Serie == serie);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, cdAtivoBMF, serie);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, cdAtivoBMF, serie);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			esTabelaVolatilidadeEstrategiaQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.CdAtivoBMF == cdAtivoBMF, query.Serie == serie);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("CdAtivoBMF",cdAtivoBMF);			parms.Add("Serie",serie);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Serie": this.str.Serie = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "PULongo": this.str.PULongo = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;							
						case "Delta": this.str.Delta = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoMercado = (System.Byte?)value;
							break;
						
						case "PULongo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PULongo = (System.Decimal?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
						
						case "Delta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Delta = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaVolatilidadeEstrategia.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaVolatilidadeEstrategia.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaVolatilidadeEstrategia.Serie
		/// </summary>
		virtual public System.String Serie
		{
			get
			{
				return base.GetSystemString(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.Serie);
			}
			
			set
			{
				if(base.SetSystemString(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.Serie, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaVolatilidadeEstrategia.TipoMercado
		/// </summary>
		virtual public System.Byte? TipoMercado
		{
			get
			{
				return base.GetSystemByte(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemByte(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaVolatilidadeEstrategia.PULongo
		/// </summary>
		virtual public System.Decimal? PULongo
		{
			get
			{
				return base.GetSystemDecimal(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.PULongo);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.PULongo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaVolatilidadeEstrategia.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.Pu, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaVolatilidadeEstrategia.Delta
		/// </summary>
		virtual public System.Decimal? Delta
		{
			get
			{
				return base.GetSystemDecimal(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.Delta);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.Delta, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBMF _UpToAtivoBMFByCdAtivoBMF;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaVolatilidadeEstrategia entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Serie
			{
				get
				{
					System.String data = entity.Serie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Serie = null;
					else entity.Serie = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Byte? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToByte(value);
				}
			}
				
			public System.String PULongo
			{
				get
				{
					System.Decimal? data = entity.PULongo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PULongo = null;
					else entity.PULongo = Convert.ToDecimal(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
				
			public System.String Delta
			{
				get
				{
					System.Decimal? data = entity.Delta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Delta = null;
					else entity.Delta = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaVolatilidadeEstrategia entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaVolatilidadeEstrategiaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaVolatilidadeEstrategia can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaVolatilidadeEstrategia : esTabelaVolatilidadeEstrategia
	{

				
		#region UpToAtivoBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBMF_TabelaVolatilidadeEstrategia_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBMF UpToAtivoBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToAtivoBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					&& Serie != null					)
				{
					this._UpToAtivoBMFByCdAtivoBMF = new AtivoBMF();
					this._UpToAtivoBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.Serie == this.Serie);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToAtivoBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this.Serie = null;
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this.Serie = value.Serie;
					this._UpToAtivoBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaVolatilidadeEstrategiaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaVolatilidadeEstrategiaMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaVolatilidadeEstrategiaMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, TabelaVolatilidadeEstrategiaMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Serie
		{
			get
			{
				return new esQueryItem(this, TabelaVolatilidadeEstrategiaMetadata.ColumnNames.Serie, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, TabelaVolatilidadeEstrategiaMetadata.ColumnNames.TipoMercado, esSystemType.Byte);
			}
		} 
		
		public esQueryItem PULongo
		{
			get
			{
				return new esQueryItem(this, TabelaVolatilidadeEstrategiaMetadata.ColumnNames.PULongo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, TabelaVolatilidadeEstrategiaMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Delta
		{
			get
			{
				return new esQueryItem(this, TabelaVolatilidadeEstrategiaMetadata.ColumnNames.Delta, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaVolatilidadeEstrategiaCollection")]
	public partial class TabelaVolatilidadeEstrategiaCollection : esTabelaVolatilidadeEstrategiaCollection, IEnumerable<TabelaVolatilidadeEstrategia>
	{
		public TabelaVolatilidadeEstrategiaCollection()
		{

		}
		
		public static implicit operator List<TabelaVolatilidadeEstrategia>(TabelaVolatilidadeEstrategiaCollection coll)
		{
			List<TabelaVolatilidadeEstrategia> list = new List<TabelaVolatilidadeEstrategia>();
			
			foreach (TabelaVolatilidadeEstrategia emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaVolatilidadeEstrategiaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaVolatilidadeEstrategiaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaVolatilidadeEstrategia(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaVolatilidadeEstrategia();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaVolatilidadeEstrategiaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaVolatilidadeEstrategiaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaVolatilidadeEstrategiaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaVolatilidadeEstrategia AddNew()
		{
			TabelaVolatilidadeEstrategia entity = base.AddNewEntity() as TabelaVolatilidadeEstrategia;
			
			return entity;
		}

		public TabelaVolatilidadeEstrategia FindByPrimaryKey(System.DateTime dataReferencia, System.String cdAtivoBMF, System.String serie)
		{
			return base.FindByPrimaryKey(dataReferencia, cdAtivoBMF, serie) as TabelaVolatilidadeEstrategia;
		}


		#region IEnumerable<TabelaVolatilidadeEstrategia> Members

		IEnumerator<TabelaVolatilidadeEstrategia> IEnumerable<TabelaVolatilidadeEstrategia>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaVolatilidadeEstrategia;
			}
		}

		#endregion
		
		private TabelaVolatilidadeEstrategiaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaVolatilidadeEstrategia' table
	/// </summary>

	[Serializable]
	public partial class TabelaVolatilidadeEstrategia : esTabelaVolatilidadeEstrategia
	{
		public TabelaVolatilidadeEstrategia()
		{

		}
	
		public TabelaVolatilidadeEstrategia(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaVolatilidadeEstrategiaMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaVolatilidadeEstrategiaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaVolatilidadeEstrategiaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaVolatilidadeEstrategiaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaVolatilidadeEstrategiaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaVolatilidadeEstrategiaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaVolatilidadeEstrategiaQuery query;
	}



	[Serializable]
	public partial class TabelaVolatilidadeEstrategiaQuery : esTabelaVolatilidadeEstrategiaQuery
	{
		public TabelaVolatilidadeEstrategiaQuery()
		{

		}		
		
		public TabelaVolatilidadeEstrategiaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaVolatilidadeEstrategiaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaVolatilidadeEstrategiaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaVolatilidadeEstrategiaMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.CdAtivoBMF, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaVolatilidadeEstrategiaMetadata.PropertyNames.CdAtivoBMF;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.Serie, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaVolatilidadeEstrategiaMetadata.PropertyNames.Serie;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.TipoMercado, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaVolatilidadeEstrategiaMetadata.PropertyNames.TipoMercado;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.PULongo, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaVolatilidadeEstrategiaMetadata.PropertyNames.PULongo;	
			c.NumericPrecision = 16;
			c.NumericScale = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.Pu, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaVolatilidadeEstrategiaMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 16;
			c.NumericScale = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaVolatilidadeEstrategiaMetadata.ColumnNames.Delta, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaVolatilidadeEstrategiaMetadata.PropertyNames.Delta;	
			c.NumericPrecision = 19;
			c.NumericScale = 7;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaVolatilidadeEstrategiaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string PULongo = "PULongo";
			 public const string Pu = "PU";
			 public const string Delta = "Delta";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string PULongo = "PULongo";
			 public const string Pu = "Pu";
			 public const string Delta = "Delta";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaVolatilidadeEstrategiaMetadata))
			{
				if(TabelaVolatilidadeEstrategiaMetadata.mapDelegates == null)
				{
					TabelaVolatilidadeEstrategiaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaVolatilidadeEstrategiaMetadata.meta == null)
				{
					TabelaVolatilidadeEstrategiaMetadata.meta = new TabelaVolatilidadeEstrategiaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Serie", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PULongo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Delta", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaVolatilidadeEstrategia";
				meta.Destination = "TabelaVolatilidadeEstrategia";
				
				meta.spInsert = "proc_TabelaVolatilidadeEstrategiaInsert";				
				meta.spUpdate = "proc_TabelaVolatilidadeEstrategiaUpdate";		
				meta.spDelete = "proc_TabelaVolatilidadeEstrategiaDelete";
				meta.spLoadAll = "proc_TabelaVolatilidadeEstrategiaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaVolatilidadeEstrategiaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaVolatilidadeEstrategiaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
