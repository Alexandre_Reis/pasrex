/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/12/2015 18:19:03
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esTabelaCorretagemBMFCollection : esEntityCollection
	{
		public esTabelaCorretagemBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaCorretagemBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaCorretagemBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaCorretagemBMFQuery);
		}
		#endregion
		
		virtual public TabelaCorretagemBMF DetachEntity(TabelaCorretagemBMF entity)
		{
			return base.DetachEntity(entity) as TabelaCorretagemBMF;
		}
		
		virtual public TabelaCorretagemBMF AttachEntity(TabelaCorretagemBMF entity)
		{
			return base.AttachEntity(entity) as TabelaCorretagemBMF;
		}
		
		virtual public void Combine(TabelaCorretagemBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaCorretagemBMF this[int index]
		{
			get
			{
				return base[index] as TabelaCorretagemBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaCorretagemBMF);
		}
	}



	[Serializable]
	abstract public class esTabelaCorretagemBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaCorretagemBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaCorretagemBMF()
		{

		}

		public esTabelaCorretagemBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.Int32 idPerfilCorretagem, System.Int32 faixaVencimento, System.Decimal faixaValor)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idPerfilCorretagem, faixaVencimento, faixaValor);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idPerfilCorretagem, faixaVencimento, faixaValor);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.Int32 idPerfilCorretagem, System.Int32 faixaVencimento, System.Decimal faixaValor)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaCorretagemBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.IdPerfilCorretagem == idPerfilCorretagem, query.FaixaVencimento == faixaVencimento, query.FaixaValor == faixaValor);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.Int32 idPerfilCorretagem, System.Int32 faixaVencimento, System.Decimal faixaValor)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idPerfilCorretagem, faixaVencimento, faixaValor);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idPerfilCorretagem, faixaVencimento, faixaValor);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.Int32 idPerfilCorretagem, System.Int32 faixaVencimento, System.Decimal faixaValor)
		{
			esTabelaCorretagemBMFQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.IdPerfilCorretagem == idPerfilCorretagem, query.FaixaVencimento == faixaVencimento, query.FaixaValor == faixaValor);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.Int32 idPerfilCorretagem, System.Int32 faixaVencimento, System.Decimal faixaValor)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("IdPerfilCorretagem",idPerfilCorretagem);			parms.Add("FaixaVencimento",faixaVencimento);			parms.Add("FaixaValor",faixaValor);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdPerfilCorretagem": this.str.IdPerfilCorretagem = (string)value; break;							
						case "FaixaVencimento": this.str.FaixaVencimento = (string)value; break;							
						case "FaixaValor": this.str.FaixaValor = (string)value; break;							
						case "TaxaCorretagem": this.str.TaxaCorretagem = (string)value; break;							
						case "TaxaCorretagemDT": this.str.TaxaCorretagemDT = (string)value; break;							
						case "FaixaVencimentoValidoPosteriores": this.str.FaixaVencimentoValidoPosteriores = (string)value; break;							
						case "TaxaVencimento": this.str.TaxaVencimento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdPerfilCorretagem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilCorretagem = (System.Int32?)value;
							break;
						
						case "FaixaVencimento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FaixaVencimento = (System.Int32?)value;
							break;
						
						case "FaixaValor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.FaixaValor = (System.Decimal?)value;
							break;
						
						case "TaxaCorretagem":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaCorretagem = (System.Decimal?)value;
							break;
						
						case "TaxaCorretagemDT":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaCorretagemDT = (System.Decimal?)value;
							break;
						
						case "TaxaVencimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaVencimento = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaCorretagemBMF.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaCorretagemBMFMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				if(base.SetSystemDateTime(TabelaCorretagemBMFMetadata.ColumnNames.DataReferencia, value))
				{
					this._UpToPerfilCorretagemBMFByDataReferencia = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaCorretagemBMF.IdPerfilCorretagem
		/// </summary>
		virtual public System.Int32? IdPerfilCorretagem
		{
			get
			{
				return base.GetSystemInt32(TabelaCorretagemBMFMetadata.ColumnNames.IdPerfilCorretagem);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaCorretagemBMFMetadata.ColumnNames.IdPerfilCorretagem, value))
				{
					this._UpToPerfilCorretagemBMFByDataReferencia = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaCorretagemBMF.FaixaVencimento
		/// </summary>
		virtual public System.Int32? FaixaVencimento
		{
			get
			{
				return base.GetSystemInt32(TabelaCorretagemBMFMetadata.ColumnNames.FaixaVencimento);
			}
			
			set
			{
				base.SetSystemInt32(TabelaCorretagemBMFMetadata.ColumnNames.FaixaVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCorretagemBMF.FaixaValor
		/// </summary>
		virtual public System.Decimal? FaixaValor
		{
			get
			{
				return base.GetSystemDecimal(TabelaCorretagemBMFMetadata.ColumnNames.FaixaValor);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCorretagemBMFMetadata.ColumnNames.FaixaValor, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCorretagemBMF.TaxaCorretagem
		/// </summary>
		virtual public System.Decimal? TaxaCorretagem
		{
			get
			{
				return base.GetSystemDecimal(TabelaCorretagemBMFMetadata.ColumnNames.TaxaCorretagem);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCorretagemBMFMetadata.ColumnNames.TaxaCorretagem, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCorretagemBMF.TaxaCorretagemDT
		/// </summary>
		virtual public System.Decimal? TaxaCorretagemDT
		{
			get
			{
				return base.GetSystemDecimal(TabelaCorretagemBMFMetadata.ColumnNames.TaxaCorretagemDT);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCorretagemBMFMetadata.ColumnNames.TaxaCorretagemDT, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCorretagemBMF.FaixaVencimentoValidoPosteriores
		/// </summary>
		virtual public System.String FaixaVencimentoValidoPosteriores
		{
			get
			{
				return base.GetSystemString(TabelaCorretagemBMFMetadata.ColumnNames.FaixaVencimentoValidoPosteriores);
			}
			
			set
			{
				base.SetSystemString(TabelaCorretagemBMFMetadata.ColumnNames.FaixaVencimentoValidoPosteriores, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCorretagemBMF.TaxaVencimento
		/// </summary>
		virtual public System.Decimal? TaxaVencimento
		{
			get
			{
				return base.GetSystemDecimal(TabelaCorretagemBMFMetadata.ColumnNames.TaxaVencimento);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCorretagemBMFMetadata.ColumnNames.TaxaVencimento, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected PerfilCorretagemBMF _UpToPerfilCorretagemBMFByDataReferencia;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaCorretagemBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdPerfilCorretagem
			{
				get
				{
					System.Int32? data = entity.IdPerfilCorretagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilCorretagem = null;
					else entity.IdPerfilCorretagem = Convert.ToInt32(value);
				}
			}
				
			public System.String FaixaVencimento
			{
				get
				{
					System.Int32? data = entity.FaixaVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FaixaVencimento = null;
					else entity.FaixaVencimento = Convert.ToInt32(value);
				}
			}
				
			public System.String FaixaValor
			{
				get
				{
					System.Decimal? data = entity.FaixaValor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FaixaValor = null;
					else entity.FaixaValor = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaCorretagem
			{
				get
				{
					System.Decimal? data = entity.TaxaCorretagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaCorretagem = null;
					else entity.TaxaCorretagem = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaCorretagemDT
			{
				get
				{
					System.Decimal? data = entity.TaxaCorretagemDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaCorretagemDT = null;
					else entity.TaxaCorretagemDT = Convert.ToDecimal(value);
				}
			}
				
			public System.String FaixaVencimentoValidoPosteriores
			{
				get
				{
					System.String data = entity.FaixaVencimentoValidoPosteriores;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FaixaVencimentoValidoPosteriores = null;
					else entity.FaixaVencimentoValidoPosteriores = Convert.ToString(value);
				}
			}
				
			public System.String TaxaVencimento
			{
				get
				{
					System.Decimal? data = entity.TaxaVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaVencimento = null;
					else entity.TaxaVencimento = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaCorretagemBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaCorretagemBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaCorretagemBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaCorretagemBMF : esTabelaCorretagemBMF
	{

				
		#region UpToPerfilCorretagemBMFByDataReferencia - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PerfilCorretagemBMF_TabelaCorretagemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilCorretagemBMF UpToPerfilCorretagemBMFByDataReferencia
		{
			get
			{
				if(this._UpToPerfilCorretagemBMFByDataReferencia == null
					&& DataReferencia != null					&& IdPerfilCorretagem != null					)
				{
					this._UpToPerfilCorretagemBMFByDataReferencia = new PerfilCorretagemBMF();
					this._UpToPerfilCorretagemBMFByDataReferencia.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPerfilCorretagemBMFByDataReferencia", this._UpToPerfilCorretagemBMFByDataReferencia);
					this._UpToPerfilCorretagemBMFByDataReferencia.Query.Where(this._UpToPerfilCorretagemBMFByDataReferencia.Query.DataReferencia == this.DataReferencia);
					this._UpToPerfilCorretagemBMFByDataReferencia.Query.Where(this._UpToPerfilCorretagemBMFByDataReferencia.Query.IdPerfilCorretagem == this.IdPerfilCorretagem);
					this._UpToPerfilCorretagemBMFByDataReferencia.Query.Load();
				}

				return this._UpToPerfilCorretagemBMFByDataReferencia;
			}
			
			set
			{
				this.RemovePreSave("UpToPerfilCorretagemBMFByDataReferencia");
				

				if(value == null)
				{
					this.DataReferencia = null;
					this.IdPerfilCorretagem = null;
					this._UpToPerfilCorretagemBMFByDataReferencia = null;
				}
				else
				{
					this.DataReferencia = value.DataReferencia;
					this.IdPerfilCorretagem = value.IdPerfilCorretagem;
					this._UpToPerfilCorretagemBMFByDataReferencia = value;
					this.SetPreSave("UpToPerfilCorretagemBMFByDataReferencia", this._UpToPerfilCorretagemBMFByDataReferencia);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToPerfilCorretagemBMFByDataReferencia != null)
			{
				this.DataReferencia = this._UpToPerfilCorretagemBMFByDataReferencia.DataReferencia;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaCorretagemBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCorretagemBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaCorretagemBMFMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdPerfilCorretagem
		{
			get
			{
				return new esQueryItem(this, TabelaCorretagemBMFMetadata.ColumnNames.IdPerfilCorretagem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FaixaVencimento
		{
			get
			{
				return new esQueryItem(this, TabelaCorretagemBMFMetadata.ColumnNames.FaixaVencimento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FaixaValor
		{
			get
			{
				return new esQueryItem(this, TabelaCorretagemBMFMetadata.ColumnNames.FaixaValor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaCorretagem
		{
			get
			{
				return new esQueryItem(this, TabelaCorretagemBMFMetadata.ColumnNames.TaxaCorretagem, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaCorretagemDT
		{
			get
			{
				return new esQueryItem(this, TabelaCorretagemBMFMetadata.ColumnNames.TaxaCorretagemDT, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem FaixaVencimentoValidoPosteriores
		{
			get
			{
				return new esQueryItem(this, TabelaCorretagemBMFMetadata.ColumnNames.FaixaVencimentoValidoPosteriores, esSystemType.String);
			}
		} 
		
		public esQueryItem TaxaVencimento
		{
			get
			{
				return new esQueryItem(this, TabelaCorretagemBMFMetadata.ColumnNames.TaxaVencimento, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaCorretagemBMFCollection")]
	public partial class TabelaCorretagemBMFCollection : esTabelaCorretagemBMFCollection, IEnumerable<TabelaCorretagemBMF>
	{
		public TabelaCorretagemBMFCollection()
		{

		}
		
		public static implicit operator List<TabelaCorretagemBMF>(TabelaCorretagemBMFCollection coll)
		{
			List<TabelaCorretagemBMF> list = new List<TabelaCorretagemBMF>();
			
			foreach (TabelaCorretagemBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaCorretagemBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCorretagemBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaCorretagemBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaCorretagemBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaCorretagemBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCorretagemBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaCorretagemBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaCorretagemBMF AddNew()
		{
			TabelaCorretagemBMF entity = base.AddNewEntity() as TabelaCorretagemBMF;
			
			return entity;
		}

		public TabelaCorretagemBMF FindByPrimaryKey(System.DateTime dataReferencia, System.Int32 idPerfilCorretagem, System.Int32 faixaVencimento, System.Decimal faixaValor)
		{
			return base.FindByPrimaryKey(dataReferencia, idPerfilCorretagem, faixaVencimento, faixaValor) as TabelaCorretagemBMF;
		}


		#region IEnumerable<TabelaCorretagemBMF> Members

		IEnumerator<TabelaCorretagemBMF> IEnumerable<TabelaCorretagemBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaCorretagemBMF;
			}
		}

		#endregion
		
		private TabelaCorretagemBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaCorretagemBMF' table
	/// </summary>

	[Serializable]
	public partial class TabelaCorretagemBMF : esTabelaCorretagemBMF
	{
		public TabelaCorretagemBMF()
		{

		}
	
		public TabelaCorretagemBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCorretagemBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaCorretagemBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCorretagemBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaCorretagemBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCorretagemBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaCorretagemBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaCorretagemBMFQuery query;
	}



	[Serializable]
	public partial class TabelaCorretagemBMFQuery : esTabelaCorretagemBMFQuery
	{
		public TabelaCorretagemBMFQuery()
		{

		}		
		
		public TabelaCorretagemBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaCorretagemBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaCorretagemBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaCorretagemBMFMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaCorretagemBMFMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCorretagemBMFMetadata.ColumnNames.IdPerfilCorretagem, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCorretagemBMFMetadata.PropertyNames.IdPerfilCorretagem;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCorretagemBMFMetadata.ColumnNames.FaixaVencimento, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCorretagemBMFMetadata.PropertyNames.FaixaVencimento;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCorretagemBMFMetadata.ColumnNames.FaixaValor, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCorretagemBMFMetadata.PropertyNames.FaixaValor;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCorretagemBMFMetadata.ColumnNames.TaxaCorretagem, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCorretagemBMFMetadata.PropertyNames.TaxaCorretagem;	
			c.NumericPrecision = 16;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCorretagemBMFMetadata.ColumnNames.TaxaCorretagemDT, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCorretagemBMFMetadata.PropertyNames.TaxaCorretagemDT;	
			c.NumericPrecision = 16;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCorretagemBMFMetadata.ColumnNames.FaixaVencimentoValidoPosteriores, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaCorretagemBMFMetadata.PropertyNames.FaixaVencimentoValidoPosteriores;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCorretagemBMFMetadata.ColumnNames.TaxaVencimento, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCorretagemBMFMetadata.PropertyNames.TaxaVencimento;	
			c.NumericPrecision = 16;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaCorretagemBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdPerfilCorretagem = "IdPerfilCorretagem";
			 public const string FaixaVencimento = "FaixaVencimento";
			 public const string FaixaValor = "FaixaValor";
			 public const string TaxaCorretagem = "TaxaCorretagem";
			 public const string TaxaCorretagemDT = "TaxaCorretagemDT";
			 public const string FaixaVencimentoValidoPosteriores = "FaixaVencimentoValidoPosteriores";
			 public const string TaxaVencimento = "TaxaVencimento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdPerfilCorretagem = "IdPerfilCorretagem";
			 public const string FaixaVencimento = "FaixaVencimento";
			 public const string FaixaValor = "FaixaValor";
			 public const string TaxaCorretagem = "TaxaCorretagem";
			 public const string TaxaCorretagemDT = "TaxaCorretagemDT";
			 public const string FaixaVencimentoValidoPosteriores = "FaixaVencimentoValidoPosteriores";
			 public const string TaxaVencimento = "TaxaVencimento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaCorretagemBMFMetadata))
			{
				if(TabelaCorretagemBMFMetadata.mapDelegates == null)
				{
					TabelaCorretagemBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaCorretagemBMFMetadata.meta == null)
				{
					TabelaCorretagemBMFMetadata.meta = new TabelaCorretagemBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdPerfilCorretagem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FaixaVencimento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FaixaValor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaCorretagem", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaCorretagemDT", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("FaixaVencimentoValidoPosteriores", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TaxaVencimento", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaCorretagemBMF";
				meta.Destination = "TabelaCorretagemBMF";
				
				meta.spInsert = "proc_TabelaCorretagemBMFInsert";				
				meta.spUpdate = "proc_TabelaCorretagemBMFUpdate";		
				meta.spDelete = "proc_TabelaCorretagemBMFDelete";
				meta.spLoadAll = "proc_TabelaCorretagemBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaCorretagemBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaCorretagemBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
