/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2012 19:02:05
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;








using Financial.Common;
using Financial.Investidor;





























		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esPerfilCorretagemBMFCollection : esEntityCollection
	{
		public esPerfilCorretagemBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PerfilCorretagemBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esPerfilCorretagemBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPerfilCorretagemBMFQuery);
		}
		#endregion
		
		virtual public PerfilCorretagemBMF DetachEntity(PerfilCorretagemBMF entity)
		{
			return base.DetachEntity(entity) as PerfilCorretagemBMF;
		}
		
		virtual public PerfilCorretagemBMF AttachEntity(PerfilCorretagemBMF entity)
		{
			return base.AttachEntity(entity) as PerfilCorretagemBMF;
		}
		
		virtual public void Combine(PerfilCorretagemBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PerfilCorretagemBMF this[int index]
		{
			get
			{
				return base[index] as PerfilCorretagemBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PerfilCorretagemBMF);
		}
	}



	[Serializable]
	abstract public class esPerfilCorretagemBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPerfilCorretagemBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esPerfilCorretagemBMF()
		{

		}

		public esPerfilCorretagemBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPerfilCorretagem, System.DateTime dataReferencia)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilCorretagem, dataReferencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilCorretagem, dataReferencia);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPerfilCorretagem, System.DateTime dataReferencia)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPerfilCorretagemBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPerfilCorretagem == idPerfilCorretagem, query.DataReferencia == dataReferencia);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPerfilCorretagem, System.DateTime dataReferencia)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilCorretagem, dataReferencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilCorretagem, dataReferencia);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPerfilCorretagem, System.DateTime dataReferencia)
		{
			esPerfilCorretagemBMFQuery query = this.GetDynamicQuery();
			query.Where(query.IdPerfilCorretagem == idPerfilCorretagem, query.DataReferencia == dataReferencia);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPerfilCorretagem, System.DateTime dataReferencia)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPerfilCorretagem",idPerfilCorretagem);			parms.Add("DataReferencia",dataReferencia);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPerfilCorretagem": this.str.IdPerfilCorretagem = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "GrupoAtivo": this.str.GrupoAtivo = (string)value; break;							
						case "TipoCorretagem": this.str.TipoCorretagem = (string)value; break;							
						case "TipoCorretagemDT": this.str.TipoCorretagemDT = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPerfilCorretagem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilCorretagem = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoMercado = (System.Byte?)value;
							break;
						
						case "TipoCorretagem":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCorretagem = (System.Byte?)value;
							break;
						
						case "TipoCorretagemDT":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCorretagemDT = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PerfilCorretagemBMF.IdPerfilCorretagem
		/// </summary>
		virtual public System.Int32? IdPerfilCorretagem
		{
			get
			{
				return base.GetSystemInt32(PerfilCorretagemBMFMetadata.ColumnNames.IdPerfilCorretagem);
			}
			
			set
			{
				base.SetSystemInt32(PerfilCorretagemBMFMetadata.ColumnNames.IdPerfilCorretagem, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilCorretagemBMF.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(PerfilCorretagemBMFMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(PerfilCorretagemBMFMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilCorretagemBMF.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PerfilCorretagemBMFMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilCorretagemBMFMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PerfilCorretagemBMF.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(PerfilCorretagemBMFMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilCorretagemBMFMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PerfilCorretagemBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(PerfilCorretagemBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				base.SetSystemString(PerfilCorretagemBMFMetadata.ColumnNames.CdAtivoBMF, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilCorretagemBMF.TipoMercado
		/// </summary>
		virtual public System.Byte? TipoMercado
		{
			get
			{
				return base.GetSystemByte(PerfilCorretagemBMFMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemByte(PerfilCorretagemBMFMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilCorretagemBMF.GrupoAtivo
		/// </summary>
		virtual public System.String GrupoAtivo
		{
			get
			{
				return base.GetSystemString(PerfilCorretagemBMFMetadata.ColumnNames.GrupoAtivo);
			}
			
			set
			{
				base.SetSystemString(PerfilCorretagemBMFMetadata.ColumnNames.GrupoAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilCorretagemBMF.TipoCorretagem
		/// </summary>
		virtual public System.Byte? TipoCorretagem
		{
			get
			{
				return base.GetSystemByte(PerfilCorretagemBMFMetadata.ColumnNames.TipoCorretagem);
			}
			
			set
			{
				base.SetSystemByte(PerfilCorretagemBMFMetadata.ColumnNames.TipoCorretagem, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilCorretagemBMF.TipoCorretagemDT
		/// </summary>
		virtual public System.Byte? TipoCorretagemDT
		{
			get
			{
				return base.GetSystemByte(PerfilCorretagemBMFMetadata.ColumnNames.TipoCorretagemDT);
			}
			
			set
			{
				base.SetSystemByte(PerfilCorretagemBMFMetadata.ColumnNames.TipoCorretagemDT, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPerfilCorretagemBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPerfilCorretagem
			{
				get
				{
					System.Int32? data = entity.IdPerfilCorretagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilCorretagem = null;
					else entity.IdPerfilCorretagem = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Byte? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToByte(value);
				}
			}
				
			public System.String GrupoAtivo
			{
				get
				{
					System.String data = entity.GrupoAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GrupoAtivo = null;
					else entity.GrupoAtivo = Convert.ToString(value);
				}
			}
				
			public System.String TipoCorretagem
			{
				get
				{
					System.Byte? data = entity.TipoCorretagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCorretagem = null;
					else entity.TipoCorretagem = Convert.ToByte(value);
				}
			}
				
			public System.String TipoCorretagemDT
			{
				get
				{
					System.Byte? data = entity.TipoCorretagemDT;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCorretagemDT = null;
					else entity.TipoCorretagemDT = Convert.ToByte(value);
				}
			}
			

			private esPerfilCorretagemBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPerfilCorretagemBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPerfilCorretagemBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PerfilCorretagemBMF : esPerfilCorretagemBMF
	{

				
		#region TabelaCorretagemBMFCollectionByDataReferencia - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PerfilCorretagemBMF_TabelaCorretagemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaCorretagemBMFCollection TabelaCorretagemBMFCollectionByDataReferencia
		{
			get
			{
				if(this._TabelaCorretagemBMFCollectionByDataReferencia == null)
				{
					this._TabelaCorretagemBMFCollectionByDataReferencia = new TabelaCorretagemBMFCollection();
					this._TabelaCorretagemBMFCollectionByDataReferencia.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaCorretagemBMFCollectionByDataReferencia", this._TabelaCorretagemBMFCollectionByDataReferencia);
				
					if(this.DataReferencia != null && this.IdPerfilCorretagem != null)
					{
						this._TabelaCorretagemBMFCollectionByDataReferencia.Query.Where(this._TabelaCorretagemBMFCollectionByDataReferencia.Query.DataReferencia == this.DataReferencia);
						this._TabelaCorretagemBMFCollectionByDataReferencia.Query.Where(this._TabelaCorretagemBMFCollectionByDataReferencia.Query.IdPerfilCorretagem == this.IdPerfilCorretagem);
						this._TabelaCorretagemBMFCollectionByDataReferencia.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaCorretagemBMFCollectionByDataReferencia.fks.Add(TabelaCorretagemBMFMetadata.ColumnNames.DataReferencia, this.DataReferencia);
						this._TabelaCorretagemBMFCollectionByDataReferencia.fks.Add(TabelaCorretagemBMFMetadata.ColumnNames.IdPerfilCorretagem, this.IdPerfilCorretagem);
					}
				}

				return this._TabelaCorretagemBMFCollectionByDataReferencia;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaCorretagemBMFCollectionByDataReferencia != null) 
				{ 
					this.RemovePostSave("TabelaCorretagemBMFCollectionByDataReferencia"); 
					this._TabelaCorretagemBMFCollectionByDataReferencia = null;
					
				} 
			} 			
		}

		private TabelaCorretagemBMFCollection _TabelaCorretagemBMFCollectionByDataReferencia;
		#endregion

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_PerfilCorretagemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PerfilCorretagemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "TabelaCorretagemBMFCollectionByDataReferencia", typeof(TabelaCorretagemBMFCollection), new TabelaCorretagemBMF()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._TabelaCorretagemBMFCollectionByDataReferencia != null)
			{
				foreach(TabelaCorretagemBMF obj in this._TabelaCorretagemBMFCollectionByDataReferencia)
				{
					if(obj.es.IsAdded)
					{
						obj.DataReferencia = this.DataReferencia;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPerfilCorretagemBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PerfilCorretagemBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPerfilCorretagem
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBMFMetadata.ColumnNames.IdPerfilCorretagem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBMFMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBMFMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBMFMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBMFMetadata.ColumnNames.TipoMercado, esSystemType.Byte);
			}
		} 
		
		public esQueryItem GrupoAtivo
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBMFMetadata.ColumnNames.GrupoAtivo, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoCorretagem
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBMFMetadata.ColumnNames.TipoCorretagem, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoCorretagemDT
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBMFMetadata.ColumnNames.TipoCorretagemDT, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PerfilCorretagemBMFCollection")]
	public partial class PerfilCorretagemBMFCollection : esPerfilCorretagemBMFCollection, IEnumerable<PerfilCorretagemBMF>
	{
		public PerfilCorretagemBMFCollection()
		{

		}
		
		public static implicit operator List<PerfilCorretagemBMF>(PerfilCorretagemBMFCollection coll)
		{
			List<PerfilCorretagemBMF> list = new List<PerfilCorretagemBMF>();
			
			foreach (PerfilCorretagemBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PerfilCorretagemBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilCorretagemBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PerfilCorretagemBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PerfilCorretagemBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PerfilCorretagemBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilCorretagemBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PerfilCorretagemBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PerfilCorretagemBMF AddNew()
		{
			PerfilCorretagemBMF entity = base.AddNewEntity() as PerfilCorretagemBMF;
			
			return entity;
		}

		public PerfilCorretagemBMF FindByPrimaryKey(System.Int32 idPerfilCorretagem, System.DateTime dataReferencia)
		{
			return base.FindByPrimaryKey(idPerfilCorretagem, dataReferencia) as PerfilCorretagemBMF;
		}


		#region IEnumerable<PerfilCorretagemBMF> Members

		IEnumerator<PerfilCorretagemBMF> IEnumerable<PerfilCorretagemBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PerfilCorretagemBMF;
			}
		}

		#endregion
		
		private PerfilCorretagemBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PerfilCorretagemBMF' table
	/// </summary>

	[Serializable]
	public partial class PerfilCorretagemBMF : esPerfilCorretagemBMF
	{
		public PerfilCorretagemBMF()
		{

		}
	
		public PerfilCorretagemBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PerfilCorretagemBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esPerfilCorretagemBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilCorretagemBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PerfilCorretagemBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilCorretagemBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PerfilCorretagemBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PerfilCorretagemBMFQuery query;
	}



	[Serializable]
	public partial class PerfilCorretagemBMFQuery : esPerfilCorretagemBMFQuery
	{
		public PerfilCorretagemBMFQuery()
		{

		}		
		
		public PerfilCorretagemBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PerfilCorretagemBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PerfilCorretagemBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PerfilCorretagemBMFMetadata.ColumnNames.IdPerfilCorretagem, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilCorretagemBMFMetadata.PropertyNames.IdPerfilCorretagem;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilCorretagemBMFMetadata.ColumnNames.DataReferencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PerfilCorretagemBMFMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilCorretagemBMFMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilCorretagemBMFMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilCorretagemBMFMetadata.ColumnNames.IdAgente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilCorretagemBMFMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilCorretagemBMFMetadata.ColumnNames.CdAtivoBMF, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = PerfilCorretagemBMFMetadata.PropertyNames.CdAtivoBMF;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilCorretagemBMFMetadata.ColumnNames.TipoMercado, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PerfilCorretagemBMFMetadata.PropertyNames.TipoMercado;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilCorretagemBMFMetadata.ColumnNames.GrupoAtivo, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = PerfilCorretagemBMFMetadata.PropertyNames.GrupoAtivo;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilCorretagemBMFMetadata.ColumnNames.TipoCorretagem, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PerfilCorretagemBMFMetadata.PropertyNames.TipoCorretagem;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilCorretagemBMFMetadata.ColumnNames.TipoCorretagemDT, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PerfilCorretagemBMFMetadata.PropertyNames.TipoCorretagemDT;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PerfilCorretagemBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPerfilCorretagem = "IdPerfilCorretagem";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string TipoMercado = "TipoMercado";
			 public const string GrupoAtivo = "GrupoAtivo";
			 public const string TipoCorretagem = "TipoCorretagem";
			 public const string TipoCorretagemDT = "TipoCorretagemDT";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPerfilCorretagem = "IdPerfilCorretagem";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string TipoMercado = "TipoMercado";
			 public const string GrupoAtivo = "GrupoAtivo";
			 public const string TipoCorretagem = "TipoCorretagem";
			 public const string TipoCorretagemDT = "TipoCorretagemDT";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PerfilCorretagemBMFMetadata))
			{
				if(PerfilCorretagemBMFMetadata.mapDelegates == null)
				{
					PerfilCorretagemBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PerfilCorretagemBMFMetadata.meta == null)
				{
					PerfilCorretagemBMFMetadata.meta = new PerfilCorretagemBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPerfilCorretagem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("GrupoAtivo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoCorretagem", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoCorretagemDT", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "PerfilCorretagemBMF";
				meta.Destination = "PerfilCorretagemBMF";
				
				meta.spInsert = "proc_PerfilCorretagemBMFInsert";				
				meta.spUpdate = "proc_PerfilCorretagemBMFUpdate";		
				meta.spDelete = "proc_PerfilCorretagemBMFDelete";
				meta.spLoadAll = "proc_PerfilCorretagemBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_PerfilCorretagemBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PerfilCorretagemBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
