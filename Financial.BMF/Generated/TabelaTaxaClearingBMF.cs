/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/12/2015 15:43:57
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.BMF
{

	[Serializable]
	abstract public class esTabelaTaxaClearingBMFCollection : esEntityCollection
	{
		public esTabelaTaxaClearingBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaTaxaClearingBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaTaxaClearingBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaTaxaClearingBMFQuery);
		}
		#endregion
		
		virtual public TabelaTaxaClearingBMF DetachEntity(TabelaTaxaClearingBMF entity)
		{
			return base.DetachEntity(entity) as TabelaTaxaClearingBMF;
		}
		
		virtual public TabelaTaxaClearingBMF AttachEntity(TabelaTaxaClearingBMF entity)
		{
			return base.AttachEntity(entity) as TabelaTaxaClearingBMF;
		}
		
		virtual public void Combine(TabelaTaxaClearingBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaTaxaClearingBMF this[int index]
		{
			get
			{
				return base[index] as TabelaTaxaClearingBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaTaxaClearingBMF);
		}
	}



	[Serializable]
	abstract public class esTabelaTaxaClearingBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaTaxaClearingBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaTaxaClearingBMF()
		{

		}

		public esTabelaTaxaClearingBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabela)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabela)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabela)
		{
			esTabelaTaxaClearingBMFQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabela == idTabela);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabela)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabela",idTabela);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "IdPerfilTaxaClearingBMF": this.str.IdPerfilTaxaClearingBMF = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "FaixaVencimento": this.str.FaixaVencimento = (string)value; break;							
						case "UltimoVencimento": this.str.UltimoVencimento = (string)value; break;							
						case "ValorFixoAgenteCustodia": this.str.ValorFixoAgenteCustodia = (string)value; break;							
						case "ValorFixoCorretora": this.str.ValorFixoCorretora = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "IdPerfilTaxaClearingBMF":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilTaxaClearingBMF = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoMercado = (System.Byte?)value;
							break;
						
						case "FaixaVencimento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FaixaVencimento = (System.Int32?)value;
							break;
						
						case "ValorFixoAgenteCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorFixoAgenteCustodia = (System.Decimal?)value;
							break;
						
						case "ValorFixoCorretora":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorFixoCorretora = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaTaxaClearingBMF.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaClearingBMFMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				base.SetSystemInt32(TabelaTaxaClearingBMFMetadata.ColumnNames.IdTabela, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaClearingBMF.IdPerfilTaxaClearingBMF
		/// </summary>
		virtual public System.Int32? IdPerfilTaxaClearingBMF
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaClearingBMFMetadata.ColumnNames.IdPerfilTaxaClearingBMF);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaTaxaClearingBMFMetadata.ColumnNames.IdPerfilTaxaClearingBMF, value))
				{
					this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaClearingBMF.TipoMercado
		/// </summary>
		virtual public System.Byte? TipoMercado
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaClearingBMFMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaClearingBMFMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaClearingBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(TabelaTaxaClearingBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				base.SetSystemString(TabelaTaxaClearingBMFMetadata.ColumnNames.CdAtivoBMF, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaClearingBMF.FaixaVencimento
		/// </summary>
		virtual public System.Int32? FaixaVencimento
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaClearingBMFMetadata.ColumnNames.FaixaVencimento);
			}
			
			set
			{
				base.SetSystemInt32(TabelaTaxaClearingBMFMetadata.ColumnNames.FaixaVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaClearingBMF.UltimoVencimento
		/// </summary>
		virtual public System.String UltimoVencimento
		{
			get
			{
				return base.GetSystemString(TabelaTaxaClearingBMFMetadata.ColumnNames.UltimoVencimento);
			}
			
			set
			{
				base.SetSystemString(TabelaTaxaClearingBMFMetadata.ColumnNames.UltimoVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaClearingBMF.ValorFixoAgenteCustodia
		/// </summary>
		virtual public System.Decimal? ValorFixoAgenteCustodia
		{
			get
			{
				return base.GetSystemDecimal(TabelaTaxaClearingBMFMetadata.ColumnNames.ValorFixoAgenteCustodia);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTaxaClearingBMFMetadata.ColumnNames.ValorFixoAgenteCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaClearingBMF.ValorFixoCorretora
		/// </summary>
		virtual public System.Decimal? ValorFixoCorretora
		{
			get
			{
				return base.GetSystemDecimal(TabelaTaxaClearingBMFMetadata.ColumnNames.ValorFixoCorretora);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTaxaClearingBMFMetadata.ColumnNames.ValorFixoCorretora, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected PerfilTaxaClearingBMF _UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaTaxaClearingBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPerfilTaxaClearingBMF
			{
				get
				{
					System.Int32? data = entity.IdPerfilTaxaClearingBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilTaxaClearingBMF = null;
					else entity.IdPerfilTaxaClearingBMF = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Byte? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToByte(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String FaixaVencimento
			{
				get
				{
					System.Int32? data = entity.FaixaVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FaixaVencimento = null;
					else entity.FaixaVencimento = Convert.ToInt32(value);
				}
			}
				
			public System.String UltimoVencimento
			{
				get
				{
					System.String data = entity.UltimoVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UltimoVencimento = null;
					else entity.UltimoVencimento = Convert.ToString(value);
				}
			}
				
			public System.String ValorFixoAgenteCustodia
			{
				get
				{
					System.Decimal? data = entity.ValorFixoAgenteCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorFixoAgenteCustodia = null;
					else entity.ValorFixoAgenteCustodia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorFixoCorretora
			{
				get
				{
					System.Decimal? data = entity.ValorFixoCorretora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorFixoCorretora = null;
					else entity.ValorFixoCorretora = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaTaxaClearingBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaTaxaClearingBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaTaxaClearingBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaTaxaClearingBMF : esTabelaTaxaClearingBMF
	{

				
		#region UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TabelaTaxaClearingBMF_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilTaxaClearingBMF UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF
		{
			get
			{
				if(this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF == null
					&& IdPerfilTaxaClearingBMF != null					)
				{
					this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF = new PerfilTaxaClearingBMF();
					this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF", this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF);
					this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF.Query.Where(this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF.Query.IdPerfil == this.IdPerfilTaxaClearingBMF);
					this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF.Query.Load();
				}

				return this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF");
				

				if(value == null)
				{
					this.IdPerfilTaxaClearingBMF = null;
					this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF = null;
				}
				else
				{
					this.IdPerfilTaxaClearingBMF = value.IdPerfil;
					this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF = value;
					this.SetPreSave("UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF", this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaTaxaClearingBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaTaxaClearingBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaClearingBMFMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPerfilTaxaClearingBMF
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaClearingBMFMetadata.ColumnNames.IdPerfilTaxaClearingBMF, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaClearingBMFMetadata.ColumnNames.TipoMercado, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaClearingBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem FaixaVencimento
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaClearingBMFMetadata.ColumnNames.FaixaVencimento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem UltimoVencimento
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaClearingBMFMetadata.ColumnNames.UltimoVencimento, esSystemType.String);
			}
		} 
		
		public esQueryItem ValorFixoAgenteCustodia
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaClearingBMFMetadata.ColumnNames.ValorFixoAgenteCustodia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorFixoCorretora
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaClearingBMFMetadata.ColumnNames.ValorFixoCorretora, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaTaxaClearingBMFCollection")]
	public partial class TabelaTaxaClearingBMFCollection : esTabelaTaxaClearingBMFCollection, IEnumerable<TabelaTaxaClearingBMF>
	{
		public TabelaTaxaClearingBMFCollection()
		{

		}
		
		public static implicit operator List<TabelaTaxaClearingBMF>(TabelaTaxaClearingBMFCollection coll)
		{
			List<TabelaTaxaClearingBMF> list = new List<TabelaTaxaClearingBMF>();
			
			foreach (TabelaTaxaClearingBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaTaxaClearingBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaTaxaClearingBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaTaxaClearingBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaTaxaClearingBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaTaxaClearingBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaTaxaClearingBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaTaxaClearingBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaTaxaClearingBMF AddNew()
		{
			TabelaTaxaClearingBMF entity = base.AddNewEntity() as TabelaTaxaClearingBMF;
			
			return entity;
		}

		public TabelaTaxaClearingBMF FindByPrimaryKey(System.Int32 idTabela)
		{
			return base.FindByPrimaryKey(idTabela) as TabelaTaxaClearingBMF;
		}


		#region IEnumerable<TabelaTaxaClearingBMF> Members

		IEnumerator<TabelaTaxaClearingBMF> IEnumerable<TabelaTaxaClearingBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaTaxaClearingBMF;
			}
		}

		#endregion
		
		private TabelaTaxaClearingBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaTaxaClearingBMF' table
	/// </summary>

	[Serializable]
	public partial class TabelaTaxaClearingBMF : esTabelaTaxaClearingBMF
	{
		public TabelaTaxaClearingBMF()
		{

		}
	
		public TabelaTaxaClearingBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaTaxaClearingBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaTaxaClearingBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaTaxaClearingBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaTaxaClearingBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaTaxaClearingBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaTaxaClearingBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaTaxaClearingBMFQuery query;
	}



	[Serializable]
	public partial class TabelaTaxaClearingBMFQuery : esTabelaTaxaClearingBMFQuery
	{
		public TabelaTaxaClearingBMFQuery()
		{

		}		
		
		public TabelaTaxaClearingBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaTaxaClearingBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaTaxaClearingBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaTaxaClearingBMFMetadata.ColumnNames.IdTabela, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaClearingBMFMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaClearingBMFMetadata.ColumnNames.IdPerfilTaxaClearingBMF, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaClearingBMFMetadata.PropertyNames.IdPerfilTaxaClearingBMF;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaClearingBMFMetadata.ColumnNames.TipoMercado, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaClearingBMFMetadata.PropertyNames.TipoMercado;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaClearingBMFMetadata.ColumnNames.CdAtivoBMF, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaTaxaClearingBMFMetadata.PropertyNames.CdAtivoBMF;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaClearingBMFMetadata.ColumnNames.FaixaVencimento, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaClearingBMFMetadata.PropertyNames.FaixaVencimento;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaClearingBMFMetadata.ColumnNames.UltimoVencimento, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaTaxaClearingBMFMetadata.PropertyNames.UltimoVencimento;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaClearingBMFMetadata.ColumnNames.ValorFixoAgenteCustodia, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTaxaClearingBMFMetadata.PropertyNames.ValorFixoAgenteCustodia;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaClearingBMFMetadata.ColumnNames.ValorFixoCorretora, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTaxaClearingBMFMetadata.PropertyNames.ValorFixoCorretora;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaTaxaClearingBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string IdPerfilTaxaClearingBMF = "IdPerfilTaxaClearingBMF";
			 public const string TipoMercado = "TipoMercado";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string FaixaVencimento = "FaixaVencimento";
			 public const string UltimoVencimento = "UltimoVencimento";
			 public const string ValorFixoAgenteCustodia = "ValorFixoAgenteCustodia";
			 public const string ValorFixoCorretora = "ValorFixoCorretora";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string IdPerfilTaxaClearingBMF = "IdPerfilTaxaClearingBMF";
			 public const string TipoMercado = "TipoMercado";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string FaixaVencimento = "FaixaVencimento";
			 public const string UltimoVencimento = "UltimoVencimento";
			 public const string ValorFixoAgenteCustodia = "ValorFixoAgenteCustodia";
			 public const string ValorFixoCorretora = "ValorFixoCorretora";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaTaxaClearingBMFMetadata))
			{
				if(TabelaTaxaClearingBMFMetadata.mapDelegates == null)
				{
					TabelaTaxaClearingBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaTaxaClearingBMFMetadata.meta == null)
				{
					TabelaTaxaClearingBMFMetadata.meta = new TabelaTaxaClearingBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPerfilTaxaClearingBMF", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FaixaVencimento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("UltimoVencimento", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ValorFixoAgenteCustodia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorFixoCorretora", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaTaxaClearingBMF";
				meta.Destination = "TabelaTaxaClearingBMF";
				
				meta.spInsert = "proc_TabelaTaxaClearingBMFInsert";				
				meta.spUpdate = "proc_TabelaTaxaClearingBMFUpdate";		
				meta.spDelete = "proc_TabelaTaxaClearingBMFDelete";
				meta.spLoadAll = "proc_TabelaTaxaClearingBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaTaxaClearingBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaTaxaClearingBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
