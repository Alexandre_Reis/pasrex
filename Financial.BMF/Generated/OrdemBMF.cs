/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/03/2016 18:55:47
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;
using Financial.Investidor;

namespace Financial.BMF
{

	[Serializable]
	abstract public class esOrdemBMFCollection : esEntityCollection
	{
		public esOrdemBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OrdemBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esOrdemBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOrdemBMFQuery);
		}
		#endregion
		
		virtual public OrdemBMF DetachEntity(OrdemBMF entity)
		{
			return base.DetachEntity(entity) as OrdemBMF;
		}
		
		virtual public OrdemBMF AttachEntity(OrdemBMF entity)
		{
			return base.AttachEntity(entity) as OrdemBMF;
		}
		
		virtual public void Combine(OrdemBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OrdemBMF this[int index]
		{
			get
			{
				return base[index] as OrdemBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OrdemBMF);
		}
	}



	[Serializable]
	abstract public class esOrdemBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOrdemBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esOrdemBMF()
		{

		}

		public esOrdemBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOrdem)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOrdem);
			else
				return LoadByPrimaryKeyStoredProcedure(idOrdem);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOrdem)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOrdem);
			else
				return LoadByPrimaryKeyStoredProcedure(idOrdem);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOrdem)
		{
			esOrdemBMFQuery query = this.GetDynamicQuery();
			query.Where(query.IdOrdem == idOrdem);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOrdem)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOrdem",idOrdem);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOrdem": this.str.IdOrdem = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAgenteCorretora": this.str.IdAgenteCorretora = (string)value; break;							
						case "IdAgenteLiquidacao": this.str.IdAgenteLiquidacao = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Serie": this.str.Serie = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "TipoOrdem": this.str.TipoOrdem = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeDayTrade": this.str.QuantidadeDayTrade = (string)value; break;							
						case "Corretagem": this.str.Corretagem = (string)value; break;							
						case "Emolumento": this.str.Emolumento = (string)value; break;							
						case "Registro": this.str.Registro = (string)value; break;							
						case "CustoWtr": this.str.CustoWtr = (string)value; break;							
						case "Origem": this.str.Origem = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "PontaEstrategia": this.str.PontaEstrategia = (string)value; break;							
						case "IdentificadorEstrategia": this.str.IdentificadorEstrategia = (string)value; break;							
						case "NumeroNegocioBMF": this.str.NumeroNegocioBMF = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;							
						case "CalculaDespesas": this.str.CalculaDespesas = (string)value; break;							
						case "PercentualDesconto": this.str.PercentualDesconto = (string)value; break;							
						case "DescontoEmolumento": this.str.DescontoEmolumento = (string)value; break;							
						case "IdMoeda": this.str.IdMoeda = (string)value; break;							
						case "IdLocalCustodia": this.str.IdLocalCustodia = (string)value; break;							
						case "IdLocalNegociacao": this.str.IdLocalNegociacao = (string)value; break;							
						case "IdClearing": this.str.IdClearing = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "UserTimeStamp": this.str.UserTimeStamp = (string)value; break;							
						case "TaxaClearingVlFixo": this.str.TaxaClearingVlFixo = (string)value; break;							
						case "IdCategoriaMovimentacao": this.str.IdCategoriaMovimentacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOrdem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOrdem = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgenteCorretora":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteCorretora = (System.Int32?)value;
							break;
						
						case "IdAgenteLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteLiquidacao = (System.Int32?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoMercado = (System.Byte?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Quantidade = (System.Int32?)value;
							break;
						
						case "QuantidadeDayTrade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.QuantidadeDayTrade = (System.Int32?)value;
							break;
						
						case "Corretagem":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Corretagem = (System.Decimal?)value;
							break;
						
						case "Emolumento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Emolumento = (System.Decimal?)value;
							break;
						
						case "Registro":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Registro = (System.Decimal?)value;
							break;
						
						case "CustoWtr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CustoWtr = (System.Decimal?)value;
							break;
						
						case "Origem":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Origem = (System.Byte?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "PontaEstrategia":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.PontaEstrategia = (System.Int16?)value;
							break;
						
						case "IdentificadorEstrategia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdentificadorEstrategia = (System.Int32?)value;
							break;
						
						case "NumeroNegocioBMF":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroNegocioBMF = (System.Int32?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
						
						case "PercentualDesconto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualDesconto = (System.Decimal?)value;
							break;
						
						case "DescontoEmolumento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.DescontoEmolumento = (System.Decimal?)value;
							break;
						
						case "IdMoeda":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMoeda = (System.Int32?)value;
							break;
						
						case "IdLocalCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalCustodia = (System.Int32?)value;
							break;
						
						case "IdLocalNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalNegociacao = (System.Int32?)value;
							break;
						
						case "IdClearing":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClearing = (System.Int32?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "UserTimeStamp":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.UserTimeStamp = (System.DateTime?)value;
							break;
						
						case "TaxaClearingVlFixo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaClearingVlFixo = (System.Decimal?)value;
							break;
						
						case "IdCategoriaMovimentacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCategoriaMovimentacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OrdemBMF.IdOrdem
		/// </summary>
		virtual public System.Int32? IdOrdem
		{
			get
			{
				return base.GetSystemInt32(OrdemBMFMetadata.ColumnNames.IdOrdem);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBMFMetadata.ColumnNames.IdOrdem, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(OrdemBMFMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemBMFMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.IdAgenteCorretora
		/// </summary>
		virtual public System.Int32? IdAgenteCorretora
		{
			get
			{
				return base.GetSystemInt32(OrdemBMFMetadata.ColumnNames.IdAgenteCorretora);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemBMFMetadata.ColumnNames.IdAgenteCorretora, value))
				{
					this._UpToAgenteMercadoByIdAgenteCorretora = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.IdAgenteLiquidacao
		/// </summary>
		virtual public System.Int32? IdAgenteLiquidacao
		{
			get
			{
				return base.GetSystemInt32(OrdemBMFMetadata.ColumnNames.IdAgenteLiquidacao);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBMFMetadata.ColumnNames.IdAgenteLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(OrdemBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(OrdemBMFMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.Serie
		/// </summary>
		virtual public System.String Serie
		{
			get
			{
				return base.GetSystemString(OrdemBMFMetadata.ColumnNames.Serie);
			}
			
			set
			{
				if(base.SetSystemString(OrdemBMFMetadata.ColumnNames.Serie, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(OrdemBMFMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemBMFMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.TipoMercado
		/// </summary>
		virtual public System.Byte? TipoMercado
		{
			get
			{
				return base.GetSystemByte(OrdemBMFMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemByte(OrdemBMFMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.TipoOrdem
		/// </summary>
		virtual public System.String TipoOrdem
		{
			get
			{
				return base.GetSystemString(OrdemBMFMetadata.ColumnNames.TipoOrdem);
			}
			
			set
			{
				base.SetSystemString(OrdemBMFMetadata.ColumnNames.TipoOrdem, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(OrdemBMFMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(OrdemBMFMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(OrdemBMFMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBMFMetadata.ColumnNames.Pu, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(OrdemBMFMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBMFMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.Quantidade
		/// </summary>
		virtual public System.Int32? Quantidade
		{
			get
			{
				return base.GetSystemInt32(OrdemBMFMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBMFMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.QuantidadeDayTrade
		/// </summary>
		virtual public System.Int32? QuantidadeDayTrade
		{
			get
			{
				return base.GetSystemInt32(OrdemBMFMetadata.ColumnNames.QuantidadeDayTrade);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBMFMetadata.ColumnNames.QuantidadeDayTrade, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.Corretagem
		/// </summary>
		virtual public System.Decimal? Corretagem
		{
			get
			{
				return base.GetSystemDecimal(OrdemBMFMetadata.ColumnNames.Corretagem);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBMFMetadata.ColumnNames.Corretagem, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.Emolumento
		/// </summary>
		virtual public System.Decimal? Emolumento
		{
			get
			{
				return base.GetSystemDecimal(OrdemBMFMetadata.ColumnNames.Emolumento);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBMFMetadata.ColumnNames.Emolumento, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.Registro
		/// </summary>
		virtual public System.Decimal? Registro
		{
			get
			{
				return base.GetSystemDecimal(OrdemBMFMetadata.ColumnNames.Registro);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBMFMetadata.ColumnNames.Registro, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.CustoWtr
		/// </summary>
		virtual public System.Decimal? CustoWtr
		{
			get
			{
				return base.GetSystemDecimal(OrdemBMFMetadata.ColumnNames.CustoWtr);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBMFMetadata.ColumnNames.CustoWtr, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.Origem
		/// </summary>
		virtual public System.Byte? Origem
		{
			get
			{
				return base.GetSystemByte(OrdemBMFMetadata.ColumnNames.Origem);
			}
			
			set
			{
				base.SetSystemByte(OrdemBMFMetadata.ColumnNames.Origem, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(OrdemBMFMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(OrdemBMFMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.PontaEstrategia
		/// </summary>
		virtual public System.Int16? PontaEstrategia
		{
			get
			{
				return base.GetSystemInt16(OrdemBMFMetadata.ColumnNames.PontaEstrategia);
			}
			
			set
			{
				base.SetSystemInt16(OrdemBMFMetadata.ColumnNames.PontaEstrategia, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.IdentificadorEstrategia
		/// </summary>
		virtual public System.Int32? IdentificadorEstrategia
		{
			get
			{
				return base.GetSystemInt32(OrdemBMFMetadata.ColumnNames.IdentificadorEstrategia);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBMFMetadata.ColumnNames.IdentificadorEstrategia, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.NumeroNegocioBMF
		/// </summary>
		virtual public System.Int32? NumeroNegocioBMF
		{
			get
			{
				return base.GetSystemInt32(OrdemBMFMetadata.ColumnNames.NumeroNegocioBMF);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBMFMetadata.ColumnNames.NumeroNegocioBMF, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(OrdemBMFMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBMFMetadata.ColumnNames.Taxa, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.CalculaDespesas
		/// </summary>
		virtual public System.String CalculaDespesas
		{
			get
			{
				return base.GetSystemString(OrdemBMFMetadata.ColumnNames.CalculaDespesas);
			}
			
			set
			{
				base.SetSystemString(OrdemBMFMetadata.ColumnNames.CalculaDespesas, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.PercentualDesconto
		/// </summary>
		virtual public System.Decimal? PercentualDesconto
		{
			get
			{
				return base.GetSystemDecimal(OrdemBMFMetadata.ColumnNames.PercentualDesconto);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBMFMetadata.ColumnNames.PercentualDesconto, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.DescontoEmolumento
		/// </summary>
		virtual public System.Decimal? DescontoEmolumento
		{
			get
			{
				return base.GetSystemDecimal(OrdemBMFMetadata.ColumnNames.DescontoEmolumento);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBMFMetadata.ColumnNames.DescontoEmolumento, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.IdMoeda
		/// </summary>
		virtual public System.Int32? IdMoeda
		{
			get
			{
				return base.GetSystemInt32(OrdemBMFMetadata.ColumnNames.IdMoeda);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBMFMetadata.ColumnNames.IdMoeda, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.IdLocalCustodia
		/// </summary>
		virtual public System.Int32? IdLocalCustodia
		{
			get
			{
				return base.GetSystemInt32(OrdemBMFMetadata.ColumnNames.IdLocalCustodia);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBMFMetadata.ColumnNames.IdLocalCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.IdLocalNegociacao
		/// </summary>
		virtual public System.Int32? IdLocalNegociacao
		{
			get
			{
				return base.GetSystemInt32(OrdemBMFMetadata.ColumnNames.IdLocalNegociacao);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBMFMetadata.ColumnNames.IdLocalNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.IdClearing
		/// </summary>
		virtual public System.Int32? IdClearing
		{
			get
			{
				return base.GetSystemInt32(OrdemBMFMetadata.ColumnNames.IdClearing);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBMFMetadata.ColumnNames.IdClearing, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(OrdemBMFMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(OrdemBMFMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.UserTimeStamp
		/// </summary>
		virtual public System.DateTime? UserTimeStamp
		{
			get
			{
				return base.GetSystemDateTime(OrdemBMFMetadata.ColumnNames.UserTimeStamp);
			}
			
			set
			{
				base.SetSystemDateTime(OrdemBMFMetadata.ColumnNames.UserTimeStamp, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.TaxaClearingVlFixo
		/// </summary>
		virtual public System.Decimal? TaxaClearingVlFixo
		{
			get
			{
				return base.GetSystemDecimal(OrdemBMFMetadata.ColumnNames.TaxaClearingVlFixo);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBMFMetadata.ColumnNames.TaxaClearingVlFixo, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBMF.IdCategoriaMovimentacao
		/// </summary>
		virtual public System.Int32? IdCategoriaMovimentacao
		{
			get
			{
				return base.GetSystemInt32(OrdemBMFMetadata.ColumnNames.IdCategoriaMovimentacao);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemBMFMetadata.ColumnNames.IdCategoriaMovimentacao, value))
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteCorretora;
		[CLSCompliant(false)]
		internal protected AtivoBMF _UpToAtivoBMFByCdAtivoBMF;
		[CLSCompliant(false)]
		internal protected CategoriaMovimentacao _UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOrdemBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOrdem
			{
				get
				{
					System.Int32? data = entity.IdOrdem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOrdem = null;
					else entity.IdOrdem = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteCorretora
			{
				get
				{
					System.Int32? data = entity.IdAgenteCorretora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteCorretora = null;
					else entity.IdAgenteCorretora = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteLiquidacao
			{
				get
				{
					System.Int32? data = entity.IdAgenteLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteLiquidacao = null;
					else entity.IdAgenteLiquidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Serie
			{
				get
				{
					System.String data = entity.Serie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Serie = null;
					else entity.Serie = Convert.ToString(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Byte? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToByte(value);
				}
			}
				
			public System.String TipoOrdem
			{
				get
				{
					System.String data = entity.TipoOrdem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOrdem = null;
					else entity.TipoOrdem = Convert.ToString(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Int32? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToInt32(value);
				}
			}
				
			public System.String QuantidadeDayTrade
			{
				get
				{
					System.Int32? data = entity.QuantidadeDayTrade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeDayTrade = null;
					else entity.QuantidadeDayTrade = Convert.ToInt32(value);
				}
			}
				
			public System.String Corretagem
			{
				get
				{
					System.Decimal? data = entity.Corretagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Corretagem = null;
					else entity.Corretagem = Convert.ToDecimal(value);
				}
			}
				
			public System.String Emolumento
			{
				get
				{
					System.Decimal? data = entity.Emolumento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Emolumento = null;
					else entity.Emolumento = Convert.ToDecimal(value);
				}
			}
				
			public System.String Registro
			{
				get
				{
					System.Decimal? data = entity.Registro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Registro = null;
					else entity.Registro = Convert.ToDecimal(value);
				}
			}
				
			public System.String CustoWtr
			{
				get
				{
					System.Decimal? data = entity.CustoWtr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CustoWtr = null;
					else entity.CustoWtr = Convert.ToDecimal(value);
				}
			}
				
			public System.String Origem
			{
				get
				{
					System.Byte? data = entity.Origem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Origem = null;
					else entity.Origem = Convert.ToByte(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String PontaEstrategia
			{
				get
				{
					System.Int16? data = entity.PontaEstrategia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PontaEstrategia = null;
					else entity.PontaEstrategia = Convert.ToInt16(value);
				}
			}
				
			public System.String IdentificadorEstrategia
			{
				get
				{
					System.Int32? data = entity.IdentificadorEstrategia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdentificadorEstrategia = null;
					else entity.IdentificadorEstrategia = Convert.ToInt32(value);
				}
			}
				
			public System.String NumeroNegocioBMF
			{
				get
				{
					System.Int32? data = entity.NumeroNegocioBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroNegocioBMF = null;
					else entity.NumeroNegocioBMF = Convert.ToInt32(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
				
			public System.String CalculaDespesas
			{
				get
				{
					System.String data = entity.CalculaDespesas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CalculaDespesas = null;
					else entity.CalculaDespesas = Convert.ToString(value);
				}
			}
				
			public System.String PercentualDesconto
			{
				get
				{
					System.Decimal? data = entity.PercentualDesconto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualDesconto = null;
					else entity.PercentualDesconto = Convert.ToDecimal(value);
				}
			}
				
			public System.String DescontoEmolumento
			{
				get
				{
					System.Decimal? data = entity.DescontoEmolumento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescontoEmolumento = null;
					else entity.DescontoEmolumento = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdMoeda
			{
				get
				{
					System.Int32? data = entity.IdMoeda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoeda = null;
					else entity.IdMoeda = Convert.ToInt32(value);
				}
			}
				
			public System.String IdLocalCustodia
			{
				get
				{
					System.Int32? data = entity.IdLocalCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalCustodia = null;
					else entity.IdLocalCustodia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdLocalNegociacao
			{
				get
				{
					System.Int32? data = entity.IdLocalNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalNegociacao = null;
					else entity.IdLocalNegociacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdClearing
			{
				get
				{
					System.Int32? data = entity.IdClearing;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClearing = null;
					else entity.IdClearing = Convert.ToInt32(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String UserTimeStamp
			{
				get
				{
					System.DateTime? data = entity.UserTimeStamp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UserTimeStamp = null;
					else entity.UserTimeStamp = Convert.ToDateTime(value);
				}
			}
				
			public System.String TaxaClearingVlFixo
			{
				get
				{
					System.Decimal? data = entity.TaxaClearingVlFixo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaClearingVlFixo = null;
					else entity.TaxaClearingVlFixo = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdCategoriaMovimentacao
			{
				get
				{
					System.Int32? data = entity.IdCategoriaMovimentacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCategoriaMovimentacao = null;
					else entity.IdCategoriaMovimentacao = Convert.ToInt32(value);
				}
			}
			

			private esOrdemBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOrdemBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOrdemBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OrdemBMF : esOrdemBMF
	{

				
		#region UpToAgenteMercadoByIdAgenteCorretora - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_OrdemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteCorretora
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteCorretora == null
					&& IdAgenteCorretora != null					)
				{
					this._UpToAgenteMercadoByIdAgenteCorretora = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteCorretora.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteCorretora", this._UpToAgenteMercadoByIdAgenteCorretora);
					this._UpToAgenteMercadoByIdAgenteCorretora.Query.Where(this._UpToAgenteMercadoByIdAgenteCorretora.Query.IdAgente == this.IdAgenteCorretora);
					this._UpToAgenteMercadoByIdAgenteCorretora.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteCorretora;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteCorretora");
				

				if(value == null)
				{
					this.IdAgenteCorretora = null;
					this._UpToAgenteMercadoByIdAgenteCorretora = null;
				}
				else
				{
					this.IdAgenteCorretora = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteCorretora = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteCorretora", this._UpToAgenteMercadoByIdAgenteCorretora);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBMF_OrdemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBMF UpToAtivoBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToAtivoBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					&& Serie != null					)
				{
					this._UpToAtivoBMFByCdAtivoBMF = new AtivoBMF();
					this._UpToAtivoBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.Serie == this.Serie);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToAtivoBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this.Serie = null;
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this.Serie = value.Serie;
					this._UpToAtivoBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCategoriaMovimentacaoByIdCategoriaMovimentacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OrdemBMF_CatMovimentacao_FK1
		/// </summary>

		[XmlIgnore]
		public CategoriaMovimentacao UpToCategoriaMovimentacaoByIdCategoriaMovimentacao
		{
			get
			{
				if(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao == null
					&& IdCategoriaMovimentacao != null					)
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = new CategoriaMovimentacao();
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Where(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.IdCategoriaMovimentacao == this.IdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Load();
				}

				return this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
			}
			
			set
			{
				this.RemovePreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao");
				

				if(value == null)
				{
					this.IdCategoriaMovimentacao = null;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
				else
				{
					this.IdCategoriaMovimentacao = value.IdCategoriaMovimentacao;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = value;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_OrdemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Trader_OrdemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteCorretora != null)
			{
				this.IdAgenteCorretora = this._UpToAgenteMercadoByIdAgenteCorretora.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao != null)
			{
				this.IdCategoriaMovimentacao = this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.IdCategoriaMovimentacao;
			}
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOrdemBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OrdemBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOrdem
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.IdOrdem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteCorretora
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.IdAgenteCorretora, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteLiquidacao
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.IdAgenteLiquidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Serie
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.Serie, esSystemType.String);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.TipoMercado, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoOrdem
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.TipoOrdem, esSystemType.String);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.Quantidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QuantidadeDayTrade
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.QuantidadeDayTrade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Corretagem
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.Corretagem, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Emolumento
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.Emolumento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Registro
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.Registro, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CustoWtr
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.CustoWtr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Origem
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.Origem, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem PontaEstrategia
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.PontaEstrategia, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdentificadorEstrategia
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.IdentificadorEstrategia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem NumeroNegocioBMF
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.NumeroNegocioBMF, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CalculaDespesas
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.CalculaDespesas, esSystemType.String);
			}
		} 
		
		public esQueryItem PercentualDesconto
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.PercentualDesconto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DescontoEmolumento
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.DescontoEmolumento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdMoeda
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.IdMoeda, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdLocalCustodia
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.IdLocalCustodia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdLocalNegociacao
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.IdLocalNegociacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdClearing
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.IdClearing, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem UserTimeStamp
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.UserTimeStamp, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TaxaClearingVlFixo
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.TaxaClearingVlFixo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdCategoriaMovimentacao
		{
			get
			{
				return new esQueryItem(this, OrdemBMFMetadata.ColumnNames.IdCategoriaMovimentacao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OrdemBMFCollection")]
	public partial class OrdemBMFCollection : esOrdemBMFCollection, IEnumerable<OrdemBMF>
	{
		public OrdemBMFCollection()
		{

		}
		
		public static implicit operator List<OrdemBMF>(OrdemBMFCollection coll)
		{
			List<OrdemBMF> list = new List<OrdemBMF>();
			
			foreach (OrdemBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OrdemBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OrdemBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OrdemBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OrdemBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OrdemBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OrdemBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OrdemBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OrdemBMF AddNew()
		{
			OrdemBMF entity = base.AddNewEntity() as OrdemBMF;
			
			return entity;
		}

		public OrdemBMF FindByPrimaryKey(System.Int32 idOrdem)
		{
			return base.FindByPrimaryKey(idOrdem) as OrdemBMF;
		}


		#region IEnumerable<OrdemBMF> Members

		IEnumerator<OrdemBMF> IEnumerable<OrdemBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OrdemBMF;
			}
		}

		#endregion
		
		private OrdemBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OrdemBMF' table
	/// </summary>

	[Serializable]
	public partial class OrdemBMF : esOrdemBMF
	{
		public OrdemBMF()
		{

		}
	
		public OrdemBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OrdemBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esOrdemBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OrdemBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OrdemBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OrdemBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OrdemBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OrdemBMFQuery query;
	}



	[Serializable]
	public partial class OrdemBMFQuery : esOrdemBMFQuery
	{
		public OrdemBMFQuery()
		{

		}		
		
		public OrdemBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OrdemBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OrdemBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.IdOrdem, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.IdOrdem;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.IdAgenteCorretora, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.IdAgenteCorretora;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.IdAgenteLiquidacao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.IdAgenteLiquidacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.CdAtivoBMF, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.CdAtivoBMF;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.Serie, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.Serie;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.IdTrader, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.TipoMercado, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.TipoMercado;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.TipoOrdem, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.TipoOrdem;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.Data, 9, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.Pu, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.Valor, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.Quantidade, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.QuantidadeDayTrade, 13, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.QuantidadeDayTrade;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.Corretagem, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.Corretagem;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.Emolumento, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.Emolumento;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.Registro, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.Registro;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.CustoWtr, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.CustoWtr;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.Origem, 18, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.Origem;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.Fonte, 19, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.PontaEstrategia, 20, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.PontaEstrategia;	
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.IdentificadorEstrategia, 21, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.IdentificadorEstrategia;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.NumeroNegocioBMF, 22, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.NumeroNegocioBMF;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.Taxa, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.CalculaDespesas, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.CalculaDespesas;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.PercentualDesconto, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.PercentualDesconto;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.DescontoEmolumento, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.DescontoEmolumento;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.IdMoeda, 27, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.IdMoeda;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.IdLocalCustodia, 28, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.IdLocalCustodia;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.IdLocalNegociacao, 29, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.IdLocalNegociacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.IdClearing, 30, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.IdClearing;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.DataOperacao, 31, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.UserTimeStamp, 32, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.UserTimeStamp;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"(getdate())";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.TaxaClearingVlFixo, 33, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.TaxaClearingVlFixo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBMFMetadata.ColumnNames.IdCategoriaMovimentacao, 34, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBMFMetadata.PropertyNames.IdCategoriaMovimentacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OrdemBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOrdem = "IdOrdem";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgenteCorretora = "IdAgenteCorretora";
			 public const string IdAgenteLiquidacao = "IdAgenteLiquidacao";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string IdTrader = "IdTrader";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoOrdem = "TipoOrdem";
			 public const string Data = "Data";
			 public const string Pu = "PU";
			 public const string Valor = "Valor";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeDayTrade = "QuantidadeDayTrade";
			 public const string Corretagem = "Corretagem";
			 public const string Emolumento = "Emolumento";
			 public const string Registro = "Registro";
			 public const string CustoWtr = "CustoWtr";
			 public const string Origem = "Origem";
			 public const string Fonte = "Fonte";
			 public const string PontaEstrategia = "PontaEstrategia";
			 public const string IdentificadorEstrategia = "IdentificadorEstrategia";
			 public const string NumeroNegocioBMF = "NumeroNegocioBMF";
			 public const string Taxa = "Taxa";
			 public const string CalculaDespesas = "CalculaDespesas";
			 public const string PercentualDesconto = "PercentualDesconto";
			 public const string DescontoEmolumento = "DescontoEmolumento";
			 public const string IdMoeda = "IdMoeda";
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IdClearing = "IdClearing";
			 public const string DataOperacao = "DataOperacao";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string TaxaClearingVlFixo = "TaxaClearingVlFixo";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOrdem = "IdOrdem";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgenteCorretora = "IdAgenteCorretora";
			 public const string IdAgenteLiquidacao = "IdAgenteLiquidacao";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string IdTrader = "IdTrader";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoOrdem = "TipoOrdem";
			 public const string Data = "Data";
			 public const string Pu = "Pu";
			 public const string Valor = "Valor";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeDayTrade = "QuantidadeDayTrade";
			 public const string Corretagem = "Corretagem";
			 public const string Emolumento = "Emolumento";
			 public const string Registro = "Registro";
			 public const string CustoWtr = "CustoWtr";
			 public const string Origem = "Origem";
			 public const string Fonte = "Fonte";
			 public const string PontaEstrategia = "PontaEstrategia";
			 public const string IdentificadorEstrategia = "IdentificadorEstrategia";
			 public const string NumeroNegocioBMF = "NumeroNegocioBMF";
			 public const string Taxa = "Taxa";
			 public const string CalculaDespesas = "CalculaDespesas";
			 public const string PercentualDesconto = "PercentualDesconto";
			 public const string DescontoEmolumento = "DescontoEmolumento";
			 public const string IdMoeda = "IdMoeda";
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IdClearing = "IdClearing";
			 public const string DataOperacao = "DataOperacao";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string TaxaClearingVlFixo = "TaxaClearingVlFixo";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OrdemBMFMetadata))
			{
				if(OrdemBMFMetadata.mapDelegates == null)
				{
					OrdemBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OrdemBMFMetadata.meta == null)
				{
					OrdemBMFMetadata.meta = new OrdemBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOrdem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteCorretora", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteLiquidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Serie", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoOrdem", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QuantidadeDayTrade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Corretagem", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Emolumento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Registro", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CustoWtr", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Origem", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PontaEstrategia", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdentificadorEstrategia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("NumeroNegocioBMF", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CalculaDespesas", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("PercentualDesconto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DescontoEmolumento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdMoeda", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdLocalCustodia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdLocalNegociacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdClearing", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("UserTimeStamp", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TaxaClearingVlFixo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdCategoriaMovimentacao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "OrdemBMF";
				meta.Destination = "OrdemBMF";
				
				meta.spInsert = "proc_OrdemBMFInsert";				
				meta.spUpdate = "proc_OrdemBMFUpdate";		
				meta.spDelete = "proc_OrdemBMFDelete";
				meta.spLoadAll = "proc_OrdemBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_OrdemBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OrdemBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
