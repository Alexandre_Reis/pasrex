/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/12/2015 17:23:04
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using Financial.Investidor;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.BMF
{

	[Serializable]
	abstract public class esPerfilTaxaClearingBMFClienteCollection : esEntityCollection
	{
		public esPerfilTaxaClearingBMFClienteCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PerfilTaxaClearingBMFClienteCollection";
		}

		#region Query Logic
		protected void InitQuery(esPerfilTaxaClearingBMFClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPerfilTaxaClearingBMFClienteQuery);
		}
		#endregion
		
		virtual public PerfilTaxaClearingBMFCliente DetachEntity(PerfilTaxaClearingBMFCliente entity)
		{
			return base.DetachEntity(entity) as PerfilTaxaClearingBMFCliente;
		}
		
		virtual public PerfilTaxaClearingBMFCliente AttachEntity(PerfilTaxaClearingBMFCliente entity)
		{
			return base.AttachEntity(entity) as PerfilTaxaClearingBMFCliente;
		}
		
		virtual public void Combine(PerfilTaxaClearingBMFClienteCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PerfilTaxaClearingBMFCliente this[int index]
		{
			get
			{
				return base[index] as PerfilTaxaClearingBMFCliente;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PerfilTaxaClearingBMFCliente);
		}
	}



	[Serializable]
	abstract public class esPerfilTaxaClearingBMFCliente : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPerfilTaxaClearingBMFClienteQuery GetDynamicQuery()
		{
			return null;
		}

		public esPerfilTaxaClearingBMFCliente()
		{

		}

		public esPerfilTaxaClearingBMFCliente(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente, System.Int32 idPerfilTaxaClearingBMF)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idPerfilTaxaClearingBMF);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idPerfilTaxaClearingBMF);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente, System.Int32 idPerfilTaxaClearingBMF)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idPerfilTaxaClearingBMF);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idPerfilTaxaClearingBMF);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente, System.Int32 idPerfilTaxaClearingBMF)
		{
			esPerfilTaxaClearingBMFClienteQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente, query.IdPerfilTaxaClearingBMF == idPerfilTaxaClearingBMF);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente, System.Int32 idPerfilTaxaClearingBMF)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);			parms.Add("IdPerfilTaxaClearingBMF",idPerfilTaxaClearingBMF);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPerfilTaxaClearingBMF": this.str.IdPerfilTaxaClearingBMF = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPerfilTaxaClearingBMF":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilTaxaClearingBMF = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PerfilTaxaClearingBMFCliente.IdPerfilTaxaClearingBMF
		/// </summary>
		virtual public System.Int32? IdPerfilTaxaClearingBMF
		{
			get
			{
				return base.GetSystemInt32(PerfilTaxaClearingBMFClienteMetadata.ColumnNames.IdPerfilTaxaClearingBMF);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilTaxaClearingBMFClienteMetadata.ColumnNames.IdPerfilTaxaClearingBMF, value))
				{
					this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PerfilTaxaClearingBMFCliente.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PerfilTaxaClearingBMFClienteMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilTaxaClearingBMFClienteMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected PerfilTaxaClearingBMF _UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPerfilTaxaClearingBMFCliente entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPerfilTaxaClearingBMF
			{
				get
				{
					System.Int32? data = entity.IdPerfilTaxaClearingBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilTaxaClearingBMF = null;
					else entity.IdPerfilTaxaClearingBMF = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
			

			private esPerfilTaxaClearingBMFCliente entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPerfilTaxaClearingBMFClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPerfilTaxaClearingBMFCliente can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PerfilTaxaClearingBMFCliente : esPerfilTaxaClearingBMFCliente
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PerfilTaxaClearingBMFCliente_FK2
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PerfilTaxaClearingBMFCliente_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilTaxaClearingBMF UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF
		{
			get
			{
				if(this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF == null
					&& IdPerfilTaxaClearingBMF != null					)
				{
					this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF = new PerfilTaxaClearingBMF();
					this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF", this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF);
					this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF.Query.Where(this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF.Query.IdPerfil == this.IdPerfilTaxaClearingBMF);
					this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF.Query.Load();
				}

				return this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF");
				

				if(value == null)
				{
					this.IdPerfilTaxaClearingBMF = null;
					this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF = null;
				}
				else
				{
					this.IdPerfilTaxaClearingBMF = value.IdPerfil;
					this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF = value;
					this.SetPreSave("UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF", this._UpToPerfilTaxaClearingBMFByIdPerfilTaxaClearingBMF);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPerfilTaxaClearingBMFClienteQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PerfilTaxaClearingBMFClienteMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPerfilTaxaClearingBMF
		{
			get
			{
				return new esQueryItem(this, PerfilTaxaClearingBMFClienteMetadata.ColumnNames.IdPerfilTaxaClearingBMF, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PerfilTaxaClearingBMFClienteMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PerfilTaxaClearingBMFClienteCollection")]
	public partial class PerfilTaxaClearingBMFClienteCollection : esPerfilTaxaClearingBMFClienteCollection, IEnumerable<PerfilTaxaClearingBMFCliente>
	{
		public PerfilTaxaClearingBMFClienteCollection()
		{

		}
		
		public static implicit operator List<PerfilTaxaClearingBMFCliente>(PerfilTaxaClearingBMFClienteCollection coll)
		{
			List<PerfilTaxaClearingBMFCliente> list = new List<PerfilTaxaClearingBMFCliente>();
			
			foreach (PerfilTaxaClearingBMFCliente emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PerfilTaxaClearingBMFClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilTaxaClearingBMFClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PerfilTaxaClearingBMFCliente(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PerfilTaxaClearingBMFCliente();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PerfilTaxaClearingBMFClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilTaxaClearingBMFClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PerfilTaxaClearingBMFClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PerfilTaxaClearingBMFCliente AddNew()
		{
			PerfilTaxaClearingBMFCliente entity = base.AddNewEntity() as PerfilTaxaClearingBMFCliente;
			
			return entity;
		}

		public PerfilTaxaClearingBMFCliente FindByPrimaryKey(System.Int32 idCliente, System.Int32 idPerfilTaxaClearingBMF)
		{
			return base.FindByPrimaryKey(idCliente, idPerfilTaxaClearingBMF) as PerfilTaxaClearingBMFCliente;
		}


		#region IEnumerable<PerfilTaxaClearingBMFCliente> Members

		IEnumerator<PerfilTaxaClearingBMFCliente> IEnumerable<PerfilTaxaClearingBMFCliente>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PerfilTaxaClearingBMFCliente;
			}
		}

		#endregion
		
		private PerfilTaxaClearingBMFClienteQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PerfilTaxaClearingBMFCliente' table
	/// </summary>

	[Serializable]
	public partial class PerfilTaxaClearingBMFCliente : esPerfilTaxaClearingBMFCliente
	{
		public PerfilTaxaClearingBMFCliente()
		{

		}
	
		public PerfilTaxaClearingBMFCliente(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PerfilTaxaClearingBMFClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esPerfilTaxaClearingBMFClienteQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilTaxaClearingBMFClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PerfilTaxaClearingBMFClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilTaxaClearingBMFClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PerfilTaxaClearingBMFClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PerfilTaxaClearingBMFClienteQuery query;
	}



	[Serializable]
	public partial class PerfilTaxaClearingBMFClienteQuery : esPerfilTaxaClearingBMFClienteQuery
	{
		public PerfilTaxaClearingBMFClienteQuery()
		{

		}		
		
		public PerfilTaxaClearingBMFClienteQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PerfilTaxaClearingBMFClienteMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PerfilTaxaClearingBMFClienteMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PerfilTaxaClearingBMFClienteMetadata.ColumnNames.IdPerfilTaxaClearingBMF, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilTaxaClearingBMFClienteMetadata.PropertyNames.IdPerfilTaxaClearingBMF;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilTaxaClearingBMFClienteMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilTaxaClearingBMFClienteMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PerfilTaxaClearingBMFClienteMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPerfilTaxaClearingBMF = "IdPerfilTaxaClearingBMF";
			 public const string IdCliente = "IdCliente";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPerfilTaxaClearingBMF = "IdPerfilTaxaClearingBMF";
			 public const string IdCliente = "IdCliente";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PerfilTaxaClearingBMFClienteMetadata))
			{
				if(PerfilTaxaClearingBMFClienteMetadata.mapDelegates == null)
				{
					PerfilTaxaClearingBMFClienteMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PerfilTaxaClearingBMFClienteMetadata.meta == null)
				{
					PerfilTaxaClearingBMFClienteMetadata.meta = new PerfilTaxaClearingBMFClienteMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPerfilTaxaClearingBMF", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "PerfilTaxaClearingBMFCliente";
				meta.Destination = "PerfilTaxaClearingBMFCliente";
				
				meta.spInsert = "proc_PerfilTaxaClearingBMFClienteInsert";				
				meta.spUpdate = "proc_PerfilTaxaClearingBMFClienteUpdate";		
				meta.spDelete = "proc_PerfilTaxaClearingBMFClienteDelete";
				meta.spLoadAll = "proc_PerfilTaxaClearingBMFClienteLoadAll";
				meta.spLoadByPrimaryKey = "proc_PerfilTaxaClearingBMFClienteLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PerfilTaxaClearingBMFClienteMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
