/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/03/2016 18:55:46
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;
using Financial.Investidor;

namespace Financial.BMF
{

	[Serializable]
	abstract public class esOperacaoBMFCollection : esEntityCollection
	{
		public esOperacaoBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OperacaoBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esOperacaoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOperacaoBMFQuery);
		}
		#endregion
		
		virtual public OperacaoBMF DetachEntity(OperacaoBMF entity)
		{
			return base.DetachEntity(entity) as OperacaoBMF;
		}
		
		virtual public OperacaoBMF AttachEntity(OperacaoBMF entity)
		{
			return base.AttachEntity(entity) as OperacaoBMF;
		}
		
		virtual public void Combine(OperacaoBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OperacaoBMF this[int index]
		{
			get
			{
				return base[index] as OperacaoBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OperacaoBMF);
		}
	}



	[Serializable]
	abstract public class esOperacaoBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOperacaoBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esOperacaoBMF()
		{

		}

		public esOperacaoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

        /// <summary>
        /// Loads an entity by primary key
        /// </summary>
        /// <remarks>
        /// EntitySpaces requires primary keys be defined on all tables.
        /// If a table does not have a primary key set,
        /// this method will not compile.
        /// Does not support sqlAcessType. It only works with DynamicQuery
        /// </remarks>
        /// <param name="fieldsToReturn">Fields desired</param>
        public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idOperacao)
        {
            esQueryItem[] fields = fieldsToReturn.ToArray();
            esOperacaoBMFQuery query = this.GetDynamicQuery();
            query
                .Select(fields)
                .Where(query.IdOperacao == idOperacao);

            return query.Load();
        }

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao)
		{
			esOperacaoBMFQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAgenteCorretora": this.str.IdAgenteCorretora = (string)value; break;							
						case "IdAgenteLiquidacao": this.str.IdAgenteLiquidacao = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Serie": this.str.Serie = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;							
						case "PULiquido": this.str.PULiquido = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Corretagem": this.str.Corretagem = (string)value; break;							
						case "Emolumento": this.str.Emolumento = (string)value; break;							
						case "Registro": this.str.Registro = (string)value; break;							
						case "CustoWtr": this.str.CustoWtr = (string)value; break;							
						case "Origem": this.str.Origem = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "Ajuste": this.str.Ajuste = (string)value; break;							
						case "PontaEstrategia": this.str.PontaEstrategia = (string)value; break;							
						case "IdentificadorEstrategia": this.str.IdentificadorEstrategia = (string)value; break;							
						case "ResultadoRealizado": this.str.ResultadoRealizado = (string)value; break;							
						case "NumeroNegocioBMF": this.str.NumeroNegocioBMF = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;							
						case "CalculaDespesas": this.str.CalculaDespesas = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "PercentualDesconto": this.str.PercentualDesconto = (string)value; break;							
						case "DescontoEmolumento": this.str.DescontoEmolumento = (string)value; break;							
						case "OutrosCustos": this.str.OutrosCustos = (string)value; break;							
						case "IdMoeda": this.str.IdMoeda = (string)value; break;							
						case "IdLocalCustodia": this.str.IdLocalCustodia = (string)value; break;							
						case "IdLocalNegociacao": this.str.IdLocalNegociacao = (string)value; break;							
						case "IdClearing": this.str.IdClearing = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "IdBoletaExterna": this.str.IdBoletaExterna = (string)value; break;							
						case "UserTimeStamp": this.str.UserTimeStamp = (string)value; break;							
						case "TaxaClearingVlFixo": this.str.TaxaClearingVlFixo = (string)value; break;							
						case "IdConta": this.str.IdConta = (string)value; break;							
						case "IdCategoriaMovimentacao": this.str.IdCategoriaMovimentacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgenteCorretora":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteCorretora = (System.Int32?)value;
							break;
						
						case "IdAgenteLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteLiquidacao = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoMercado = (System.Byte?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
						
						case "PULiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PULiquido = (System.Decimal?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Quantidade = (System.Int32?)value;
							break;
						
						case "Corretagem":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Corretagem = (System.Decimal?)value;
							break;
						
						case "Emolumento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Emolumento = (System.Decimal?)value;
							break;
						
						case "Registro":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Registro = (System.Decimal?)value;
							break;
						
						case "CustoWtr":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CustoWtr = (System.Decimal?)value;
							break;
						
						case "Origem":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Origem = (System.Byte?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "Ajuste":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Ajuste = (System.Decimal?)value;
							break;
						
						case "PontaEstrategia":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.PontaEstrategia = (System.Int16?)value;
							break;
						
						case "IdentificadorEstrategia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdentificadorEstrategia = (System.Int32?)value;
							break;
						
						case "ResultadoRealizado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoRealizado = (System.Decimal?)value;
							break;
						
						case "NumeroNegocioBMF":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroNegocioBMF = (System.Int32?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "PercentualDesconto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualDesconto = (System.Decimal?)value;
							break;
						
						case "DescontoEmolumento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.DescontoEmolumento = (System.Decimal?)value;
							break;
						
						case "OutrosCustos":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.OutrosCustos = (System.Decimal?)value;
							break;
						
						case "IdMoeda":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMoeda = (System.Int32?)value;
							break;
						
						case "IdLocalCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalCustodia = (System.Int32?)value;
							break;
						
						case "IdLocalNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalNegociacao = (System.Int32?)value;
							break;
						
						case "IdClearing":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClearing = (System.Int32?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "IdBoletaExterna":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdBoletaExterna = (System.Int32?)value;
							break;
						
						case "UserTimeStamp":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.UserTimeStamp = (System.DateTime?)value;
							break;
						
						case "TaxaClearingVlFixo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaClearingVlFixo = (System.Decimal?)value;
							break;
						
						case "IdConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConta = (System.Int32?)value;
							break;
						
						case "IdCategoriaMovimentacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCategoriaMovimentacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OperacaoBMF.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.IdAgenteCorretora
		/// </summary>
		virtual public System.Int32? IdAgenteCorretora
		{
			get
			{
				return base.GetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdAgenteCorretora);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdAgenteCorretora, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.IdAgenteLiquidacao
		/// </summary>
		virtual public System.Int32? IdAgenteLiquidacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdAgenteLiquidacao);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdAgenteLiquidacao, value))
				{
					this._UpToAgenteMercadoByIdAgenteLiquidacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(OperacaoBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(OperacaoBMFMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.Serie
		/// </summary>
		virtual public System.String Serie
		{
			get
			{
				return base.GetSystemString(OperacaoBMFMetadata.ColumnNames.Serie);
			}
			
			set
			{
				if(base.SetSystemString(OperacaoBMFMetadata.ColumnNames.Serie, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.TipoMercado
		/// </summary>
		virtual public System.Byte? TipoMercado
		{
			get
			{
				return base.GetSystemByte(OperacaoBMFMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemByte(OperacaoBMFMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.TipoOperacao
		/// </summary>
		virtual public System.String TipoOperacao
		{
			get
			{
				return base.GetSystemString(OperacaoBMFMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemString(OperacaoBMFMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(OperacaoBMFMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoBMFMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBMFMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBMFMetadata.ColumnNames.Pu, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.PULiquido
		/// </summary>
		virtual public System.Decimal? PULiquido
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBMFMetadata.ColumnNames.PULiquido);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBMFMetadata.ColumnNames.PULiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBMFMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBMFMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBMFMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBMFMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.Quantidade
		/// </summary>
		virtual public System.Int32? Quantidade
		{
			get
			{
				return base.GetSystemInt32(OperacaoBMFMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBMFMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.Corretagem
		/// </summary>
		virtual public System.Decimal? Corretagem
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBMFMetadata.ColumnNames.Corretagem);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBMFMetadata.ColumnNames.Corretagem, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.Emolumento
		/// </summary>
		virtual public System.Decimal? Emolumento
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBMFMetadata.ColumnNames.Emolumento);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBMFMetadata.ColumnNames.Emolumento, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.Registro
		/// </summary>
		virtual public System.Decimal? Registro
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBMFMetadata.ColumnNames.Registro);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBMFMetadata.ColumnNames.Registro, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.CustoWtr
		/// </summary>
		virtual public System.Decimal? CustoWtr
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBMFMetadata.ColumnNames.CustoWtr);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBMFMetadata.ColumnNames.CustoWtr, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.Origem
		/// </summary>
		virtual public System.Byte? Origem
		{
			get
			{
				return base.GetSystemByte(OperacaoBMFMetadata.ColumnNames.Origem);
			}
			
			set
			{
				base.SetSystemByte(OperacaoBMFMetadata.ColumnNames.Origem, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(OperacaoBMFMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(OperacaoBMFMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.Ajuste
		/// </summary>
		virtual public System.Decimal? Ajuste
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBMFMetadata.ColumnNames.Ajuste);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBMFMetadata.ColumnNames.Ajuste, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.PontaEstrategia
		/// </summary>
		virtual public System.Int16? PontaEstrategia
		{
			get
			{
				return base.GetSystemInt16(OperacaoBMFMetadata.ColumnNames.PontaEstrategia);
			}
			
			set
			{
				base.SetSystemInt16(OperacaoBMFMetadata.ColumnNames.PontaEstrategia, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.IdentificadorEstrategia
		/// </summary>
		virtual public System.Int32? IdentificadorEstrategia
		{
			get
			{
				return base.GetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdentificadorEstrategia);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdentificadorEstrategia, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.ResultadoRealizado
		/// </summary>
		virtual public System.Decimal? ResultadoRealizado
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBMFMetadata.ColumnNames.ResultadoRealizado);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBMFMetadata.ColumnNames.ResultadoRealizado, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.NumeroNegocioBMF
		/// </summary>
		virtual public System.Int32? NumeroNegocioBMF
		{
			get
			{
				return base.GetSystemInt32(OperacaoBMFMetadata.ColumnNames.NumeroNegocioBMF);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBMFMetadata.ColumnNames.NumeroNegocioBMF, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBMFMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBMFMetadata.ColumnNames.Taxa, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.CalculaDespesas
		/// </summary>
		virtual public System.String CalculaDespesas
		{
			get
			{
				return base.GetSystemString(OperacaoBMFMetadata.ColumnNames.CalculaDespesas);
			}
			
			set
			{
				base.SetSystemString(OperacaoBMFMetadata.ColumnNames.CalculaDespesas, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdTrader, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.PercentualDesconto
		/// </summary>
		virtual public System.Decimal? PercentualDesconto
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBMFMetadata.ColumnNames.PercentualDesconto);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBMFMetadata.ColumnNames.PercentualDesconto, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.DescontoEmolumento
		/// </summary>
		virtual public System.Decimal? DescontoEmolumento
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBMFMetadata.ColumnNames.DescontoEmolumento);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBMFMetadata.ColumnNames.DescontoEmolumento, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.OutrosCustos
		/// </summary>
		virtual public System.Decimal? OutrosCustos
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBMFMetadata.ColumnNames.OutrosCustos);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBMFMetadata.ColumnNames.OutrosCustos, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.IdMoeda
		/// </summary>
		virtual public System.Int32? IdMoeda
		{
			get
			{
				return base.GetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdMoeda);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdMoeda, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.IdLocalCustodia
		/// </summary>
		virtual public System.Int32? IdLocalCustodia
		{
			get
			{
				return base.GetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdLocalCustodia);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdLocalCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.IdLocalNegociacao
		/// </summary>
		virtual public System.Int32? IdLocalNegociacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdLocalNegociacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdLocalNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.IdClearing
		/// </summary>
		virtual public System.Int32? IdClearing
		{
			get
			{
				return base.GetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdClearing);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdClearing, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoBMFMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoBMFMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.IdBoletaExterna
		/// </summary>
		virtual public System.Int32? IdBoletaExterna
		{
			get
			{
				return base.GetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdBoletaExterna);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdBoletaExterna, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.UserTimeStamp
		/// </summary>
		virtual public System.DateTime? UserTimeStamp
		{
			get
			{
				return base.GetSystemDateTime(OperacaoBMFMetadata.ColumnNames.UserTimeStamp);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoBMFMetadata.ColumnNames.UserTimeStamp, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.TaxaClearingVlFixo
		/// </summary>
		virtual public System.Decimal? TaxaClearingVlFixo
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBMFMetadata.ColumnNames.TaxaClearingVlFixo);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBMFMetadata.ColumnNames.TaxaClearingVlFixo, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.IdConta
		/// </summary>
		virtual public System.Int32? IdConta
		{
			get
			{
				return base.GetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdConta);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdConta, value))
				{
					this._UpToContaCorrenteByIdConta = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBMF.IdCategoriaMovimentacao
		/// </summary>
		virtual public System.Int32? IdCategoriaMovimentacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdCategoriaMovimentacao);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoBMFMetadata.ColumnNames.IdCategoriaMovimentacao, value))
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteLiquidacao;
		[CLSCompliant(false)]
		internal protected AtivoBMF _UpToAtivoBMFByCdAtivoBMF;
		[CLSCompliant(false)]
		internal protected CategoriaMovimentacao _UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
        internal protected Financial.Investidor.ContaCorrente _UpToContaCorrenteByIdConta;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOperacaoBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteCorretora
			{
				get
				{
					System.Int32? data = entity.IdAgenteCorretora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteCorretora = null;
					else entity.IdAgenteCorretora = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteLiquidacao
			{
				get
				{
					System.Int32? data = entity.IdAgenteLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteLiquidacao = null;
					else entity.IdAgenteLiquidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Serie
			{
				get
				{
					System.String data = entity.Serie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Serie = null;
					else entity.Serie = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Byte? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToByte(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.String data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToString(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
				
			public System.String PULiquido
			{
				get
				{
					System.Decimal? data = entity.PULiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PULiquido = null;
					else entity.PULiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Int32? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToInt32(value);
				}
			}
				
			public System.String Corretagem
			{
				get
				{
					System.Decimal? data = entity.Corretagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Corretagem = null;
					else entity.Corretagem = Convert.ToDecimal(value);
				}
			}
				
			public System.String Emolumento
			{
				get
				{
					System.Decimal? data = entity.Emolumento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Emolumento = null;
					else entity.Emolumento = Convert.ToDecimal(value);
				}
			}
				
			public System.String Registro
			{
				get
				{
					System.Decimal? data = entity.Registro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Registro = null;
					else entity.Registro = Convert.ToDecimal(value);
				}
			}
				
			public System.String CustoWtr
			{
				get
				{
					System.Decimal? data = entity.CustoWtr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CustoWtr = null;
					else entity.CustoWtr = Convert.ToDecimal(value);
				}
			}
				
			public System.String Origem
			{
				get
				{
					System.Byte? data = entity.Origem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Origem = null;
					else entity.Origem = Convert.ToByte(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String Ajuste
			{
				get
				{
					System.Decimal? data = entity.Ajuste;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Ajuste = null;
					else entity.Ajuste = Convert.ToDecimal(value);
				}
			}
				
			public System.String PontaEstrategia
			{
				get
				{
					System.Int16? data = entity.PontaEstrategia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PontaEstrategia = null;
					else entity.PontaEstrategia = Convert.ToInt16(value);
				}
			}
				
			public System.String IdentificadorEstrategia
			{
				get
				{
					System.Int32? data = entity.IdentificadorEstrategia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdentificadorEstrategia = null;
					else entity.IdentificadorEstrategia = Convert.ToInt32(value);
				}
			}
				
			public System.String ResultadoRealizado
			{
				get
				{
					System.Decimal? data = entity.ResultadoRealizado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoRealizado = null;
					else entity.ResultadoRealizado = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumeroNegocioBMF
			{
				get
				{
					System.Int32? data = entity.NumeroNegocioBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroNegocioBMF = null;
					else entity.NumeroNegocioBMF = Convert.ToInt32(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
				
			public System.String CalculaDespesas
			{
				get
				{
					System.String data = entity.CalculaDespesas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CalculaDespesas = null;
					else entity.CalculaDespesas = Convert.ToString(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String PercentualDesconto
			{
				get
				{
					System.Decimal? data = entity.PercentualDesconto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualDesconto = null;
					else entity.PercentualDesconto = Convert.ToDecimal(value);
				}
			}
				
			public System.String DescontoEmolumento
			{
				get
				{
					System.Decimal? data = entity.DescontoEmolumento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescontoEmolumento = null;
					else entity.DescontoEmolumento = Convert.ToDecimal(value);
				}
			}
				
			public System.String OutrosCustos
			{
				get
				{
					System.Decimal? data = entity.OutrosCustos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OutrosCustos = null;
					else entity.OutrosCustos = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdMoeda
			{
				get
				{
					System.Int32? data = entity.IdMoeda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoeda = null;
					else entity.IdMoeda = Convert.ToInt32(value);
				}
			}
				
			public System.String IdLocalCustodia
			{
				get
				{
					System.Int32? data = entity.IdLocalCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalCustodia = null;
					else entity.IdLocalCustodia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdLocalNegociacao
			{
				get
				{
					System.Int32? data = entity.IdLocalNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalNegociacao = null;
					else entity.IdLocalNegociacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdClearing
			{
				get
				{
					System.Int32? data = entity.IdClearing;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClearing = null;
					else entity.IdClearing = Convert.ToInt32(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdBoletaExterna
			{
				get
				{
					System.Int32? data = entity.IdBoletaExterna;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdBoletaExterna = null;
					else entity.IdBoletaExterna = Convert.ToInt32(value);
				}
			}
				
			public System.String UserTimeStamp
			{
				get
				{
					System.DateTime? data = entity.UserTimeStamp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UserTimeStamp = null;
					else entity.UserTimeStamp = Convert.ToDateTime(value);
				}
			}
				
			public System.String TaxaClearingVlFixo
			{
				get
				{
					System.Decimal? data = entity.TaxaClearingVlFixo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaClearingVlFixo = null;
					else entity.TaxaClearingVlFixo = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdConta
			{
				get
				{
					System.Int32? data = entity.IdConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConta = null;
					else entity.IdConta = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCategoriaMovimentacao
			{
				get
				{
					System.Int32? data = entity.IdCategoriaMovimentacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCategoriaMovimentacao = null;
					else entity.IdCategoriaMovimentacao = Convert.ToInt32(value);
				}
			}
			

			private esOperacaoBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOperacaoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOperacaoBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OperacaoBMF : esOperacaoBMF
	{

				
		#region UpToAgenteMercadoByIdAgenteLiquidacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_OperacaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteLiquidacao
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteLiquidacao == null
					&& IdAgenteLiquidacao != null					)
				{
					this._UpToAgenteMercadoByIdAgenteLiquidacao = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteLiquidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteLiquidacao", this._UpToAgenteMercadoByIdAgenteLiquidacao);
					this._UpToAgenteMercadoByIdAgenteLiquidacao.Query.Where(this._UpToAgenteMercadoByIdAgenteLiquidacao.Query.IdAgente == this.IdAgenteLiquidacao);
					this._UpToAgenteMercadoByIdAgenteLiquidacao.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteLiquidacao;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteLiquidacao");
				

				if(value == null)
				{
					this.IdAgenteLiquidacao = null;
					this._UpToAgenteMercadoByIdAgenteLiquidacao = null;
				}
				else
				{
					this.IdAgenteLiquidacao = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteLiquidacao = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteLiquidacao", this._UpToAgenteMercadoByIdAgenteLiquidacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBMF_OperacaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBMF UpToAtivoBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToAtivoBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					&& Serie != null					)
				{
					this._UpToAtivoBMFByCdAtivoBMF = new AtivoBMF();
					this._UpToAtivoBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.Serie == this.Serie);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToAtivoBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this.Serie = null;
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this.Serie = value.Serie;
					this._UpToAtivoBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCategoriaMovimentacaoByIdCategoriaMovimentacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OperacaoBMF_CatMovimentacao_FK1
		/// </summary>

		[XmlIgnore]
		public CategoriaMovimentacao UpToCategoriaMovimentacaoByIdCategoriaMovimentacao
		{
			get
			{
				if(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao == null
					&& IdCategoriaMovimentacao != null					)
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = new CategoriaMovimentacao();
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Where(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.IdCategoriaMovimentacao == this.IdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Load();
				}

				return this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
			}
			
			set
			{
				this.RemovePreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao");
				

				if(value == null)
				{
					this.IdCategoriaMovimentacao = null;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
				else
				{
					this.IdCategoriaMovimentacao = value.IdCategoriaMovimentacao;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = value;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_OperacaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContaCorrenteByIdConta - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_OperacaoBmf_ContaCorrente
		/// </summary>

		[XmlIgnore]
        public Financial.Investidor.ContaCorrente UpToContaCorrenteByIdConta
		{
			get
			{
				if(this._UpToContaCorrenteByIdConta == null
					&& IdConta != null					)
				{
                    this._UpToContaCorrenteByIdConta = new Financial.Investidor.ContaCorrente();
					this._UpToContaCorrenteByIdConta.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
					this._UpToContaCorrenteByIdConta.Query.Where(this._UpToContaCorrenteByIdConta.Query.IdConta == this.IdConta);
					this._UpToContaCorrenteByIdConta.Query.Load();
				}

				return this._UpToContaCorrenteByIdConta;
			}
			
			set
			{
				this.RemovePreSave("UpToContaCorrenteByIdConta");
				

				if(value == null)
				{
					this.IdConta = null;
					this._UpToContaCorrenteByIdConta = null;
				}
				else
				{
					this.IdConta = value.IdConta;
					this._UpToContaCorrenteByIdConta = value;
					this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteLiquidacao != null)
			{
				this.IdAgenteLiquidacao = this._UpToAgenteMercadoByIdAgenteLiquidacao.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao != null)
			{
				this.IdCategoriaMovimentacao = this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.IdCategoriaMovimentacao;
			}
			if(!this.es.IsDeleted && this._UpToContaCorrenteByIdConta != null)
			{
				this.IdConta = this._UpToContaCorrenteByIdConta.IdConta;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOperacaoBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteCorretora
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.IdAgenteCorretora, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteLiquidacao
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.IdAgenteLiquidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Serie
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.Serie, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.TipoMercado, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.TipoOperacao, esSystemType.String);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PULiquido
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.PULiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.Quantidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Corretagem
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.Corretagem, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Emolumento
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.Emolumento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Registro
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.Registro, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CustoWtr
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.CustoWtr, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Origem
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.Origem, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Ajuste
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.Ajuste, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PontaEstrategia
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.PontaEstrategia, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdentificadorEstrategia
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.IdentificadorEstrategia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ResultadoRealizado
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.ResultadoRealizado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumeroNegocioBMF
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.NumeroNegocioBMF, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CalculaDespesas
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.CalculaDespesas, esSystemType.String);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PercentualDesconto
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.PercentualDesconto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DescontoEmolumento
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.DescontoEmolumento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem OutrosCustos
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.OutrosCustos, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdMoeda
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.IdMoeda, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdLocalCustodia
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.IdLocalCustodia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdLocalNegociacao
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.IdLocalNegociacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdClearing
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.IdClearing, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdBoletaExterna
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.IdBoletaExterna, esSystemType.Int32);
			}
		} 
		
		public esQueryItem UserTimeStamp
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.UserTimeStamp, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TaxaClearingVlFixo
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.TaxaClearingVlFixo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdConta
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.IdConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCategoriaMovimentacao
		{
			get
			{
				return new esQueryItem(this, OperacaoBMFMetadata.ColumnNames.IdCategoriaMovimentacao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OperacaoBMFCollection")]
	public partial class OperacaoBMFCollection : esOperacaoBMFCollection, IEnumerable<OperacaoBMF>
	{
		public OperacaoBMFCollection()
		{

		}
		
		public static implicit operator List<OperacaoBMF>(OperacaoBMFCollection coll)
		{
			List<OperacaoBMF> list = new List<OperacaoBMF>();
			
			foreach (OperacaoBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OperacaoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OperacaoBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OperacaoBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OperacaoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OperacaoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OperacaoBMF AddNew()
		{
			OperacaoBMF entity = base.AddNewEntity() as OperacaoBMF;
			
			return entity;
		}

		public OperacaoBMF FindByPrimaryKey(System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(idOperacao) as OperacaoBMF;
		}


		#region IEnumerable<OperacaoBMF> Members

		IEnumerator<OperacaoBMF> IEnumerable<OperacaoBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OperacaoBMF;
			}
		}

		#endregion
		
		private OperacaoBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OperacaoBMF' table
	/// </summary>

	[Serializable]
	public partial class OperacaoBMF : esOperacaoBMF
	{
		public OperacaoBMF()
		{

		}
	
		public OperacaoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esOperacaoBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OperacaoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OperacaoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OperacaoBMFQuery query;
	}



	[Serializable]
	public partial class OperacaoBMFQuery : esOperacaoBMFQuery
	{
		public OperacaoBMFQuery()
		{

		}		
		
		public OperacaoBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OperacaoBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OperacaoBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.IdAgenteCorretora, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.IdAgenteCorretora;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.IdAgenteLiquidacao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.IdAgenteLiquidacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.CdAtivoBMF, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.CdAtivoBMF;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.Serie, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.Serie;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.TipoMercado, 6, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.TipoMercado;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.TipoOperacao, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.TipoOperacao;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.Data, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.Pu, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.PULiquido, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.PULiquido;	
			c.NumericPrecision = 18;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.Valor, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.ValorLiquido, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.Quantidade, 13, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.Corretagem, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.Corretagem;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.Emolumento, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.Emolumento;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.Registro, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.Registro;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.CustoWtr, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.CustoWtr;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.Origem, 18, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.Origem;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.Fonte, 19, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.Ajuste, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.Ajuste;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.PontaEstrategia, 21, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.PontaEstrategia;	
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.IdentificadorEstrategia, 22, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.IdentificadorEstrategia;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.ResultadoRealizado, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.ResultadoRealizado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.NumeroNegocioBMF, 24, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.NumeroNegocioBMF;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.Taxa, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.CalculaDespesas, 26, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.CalculaDespesas;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.IdTrader, 27, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.PercentualDesconto, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.PercentualDesconto;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.DescontoEmolumento, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.DescontoEmolumento;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.OutrosCustos, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.OutrosCustos;	
			c.NumericPrecision = 15;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.IdMoeda, 31, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.IdMoeda;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.IdLocalCustodia, 32, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.IdLocalCustodia;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.IdLocalNegociacao, 33, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.IdLocalNegociacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.IdClearing, 34, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.IdClearing;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.DataOperacao, 35, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.IdBoletaExterna, 36, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.IdBoletaExterna;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.UserTimeStamp, 37, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.UserTimeStamp;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"(getdate())";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.TaxaClearingVlFixo, 38, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.TaxaClearingVlFixo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.IdConta, 39, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.IdConta;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBMFMetadata.ColumnNames.IdCategoriaMovimentacao, 40, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBMFMetadata.PropertyNames.IdCategoriaMovimentacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OperacaoBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgenteCorretora = "IdAgenteCorretora";
			 public const string IdAgenteLiquidacao = "IdAgenteLiquidacao";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Data = "Data";
			 public const string Pu = "PU";
			 public const string PULiquido = "PULiquido";
			 public const string Valor = "Valor";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string Quantidade = "Quantidade";
			 public const string Corretagem = "Corretagem";
			 public const string Emolumento = "Emolumento";
			 public const string Registro = "Registro";
			 public const string CustoWtr = "CustoWtr";
			 public const string Origem = "Origem";
			 public const string Fonte = "Fonte";
			 public const string Ajuste = "Ajuste";
			 public const string PontaEstrategia = "PontaEstrategia";
			 public const string IdentificadorEstrategia = "IdentificadorEstrategia";
			 public const string ResultadoRealizado = "ResultadoRealizado";
			 public const string NumeroNegocioBMF = "NumeroNegocioBMF";
			 public const string Taxa = "Taxa";
			 public const string CalculaDespesas = "CalculaDespesas";
			 public const string IdTrader = "IdTrader";
			 public const string PercentualDesconto = "PercentualDesconto";
			 public const string DescontoEmolumento = "DescontoEmolumento";
			 public const string OutrosCustos = "OutrosCustos";
			 public const string IdMoeda = "IdMoeda";
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IdClearing = "IdClearing";
			 public const string DataOperacao = "DataOperacao";
			 public const string IdBoletaExterna = "IdBoletaExterna";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string TaxaClearingVlFixo = "TaxaClearingVlFixo";
			 public const string IdConta = "IdConta";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgenteCorretora = "IdAgenteCorretora";
			 public const string IdAgenteLiquidacao = "IdAgenteLiquidacao";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Data = "Data";
			 public const string Pu = "Pu";
			 public const string PULiquido = "PULiquido";
			 public const string Valor = "Valor";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string Quantidade = "Quantidade";
			 public const string Corretagem = "Corretagem";
			 public const string Emolumento = "Emolumento";
			 public const string Registro = "Registro";
			 public const string CustoWtr = "CustoWtr";
			 public const string Origem = "Origem";
			 public const string Fonte = "Fonte";
			 public const string Ajuste = "Ajuste";
			 public const string PontaEstrategia = "PontaEstrategia";
			 public const string IdentificadorEstrategia = "IdentificadorEstrategia";
			 public const string ResultadoRealizado = "ResultadoRealizado";
			 public const string NumeroNegocioBMF = "NumeroNegocioBMF";
			 public const string Taxa = "Taxa";
			 public const string CalculaDespesas = "CalculaDespesas";
			 public const string IdTrader = "IdTrader";
			 public const string PercentualDesconto = "PercentualDesconto";
			 public const string DescontoEmolumento = "DescontoEmolumento";
			 public const string OutrosCustos = "OutrosCustos";
			 public const string IdMoeda = "IdMoeda";
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IdClearing = "IdClearing";
			 public const string DataOperacao = "DataOperacao";
			 public const string IdBoletaExterna = "IdBoletaExterna";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string TaxaClearingVlFixo = "TaxaClearingVlFixo";
			 public const string IdConta = "IdConta";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OperacaoBMFMetadata))
			{
				if(OperacaoBMFMetadata.mapDelegates == null)
				{
					OperacaoBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OperacaoBMFMetadata.meta == null)
				{
					OperacaoBMFMetadata.meta = new OperacaoBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteCorretora", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteLiquidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Serie", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PULiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Corretagem", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Emolumento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Registro", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CustoWtr", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Origem", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Ajuste", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PontaEstrategia", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdentificadorEstrategia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ResultadoRealizado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("NumeroNegocioBMF", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CalculaDespesas", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PercentualDesconto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DescontoEmolumento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("OutrosCustos", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdMoeda", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdLocalCustodia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdLocalNegociacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdClearing", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdBoletaExterna", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("UserTimeStamp", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TaxaClearingVlFixo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCategoriaMovimentacao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "OperacaoBMF";
				meta.Destination = "OperacaoBMF";
				
				meta.spInsert = "proc_OperacaoBMFInsert";				
				meta.spUpdate = "proc_OperacaoBMFUpdate";		
				meta.spDelete = "proc_OperacaoBMFDelete";
				meta.spLoadAll = "proc_OperacaoBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_OperacaoBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OperacaoBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
