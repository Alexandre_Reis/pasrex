/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2012 19:02:05
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;









using Financial.Common;
using Financial.Investidor;




























		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.BMF
{

	[Serializable]
	abstract public class esPosicaoBMFCollection : esEntityCollection
	{
		public esPosicaoBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoBMFQuery);
		}
		#endregion
		
		virtual public PosicaoBMF DetachEntity(PosicaoBMF entity)
		{
			return base.DetachEntity(entity) as PosicaoBMF;
		}
		
		virtual public PosicaoBMF AttachEntity(PosicaoBMF entity)
		{
			return base.AttachEntity(entity) as PosicaoBMF;
		}
		
		virtual public void Combine(PosicaoBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoBMF this[int index]
		{
			get
			{
				return base[index] as PosicaoBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoBMF);
		}
	}



	[Serializable]
	abstract public class esPosicaoBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoBMF()
		{

		}

		public esPosicaoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPosicao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPosicaoBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPosicao == idPosicao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao)
		{
			esPosicaoBMFQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Serie": this.str.Serie = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "PUMercado": this.str.PUMercado = (string)value; break;							
						case "PUCusto": this.str.PUCusto = (string)value; break;							
						case "PUCustoLiquido": this.str.PUCustoLiquido = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeInicial": this.str.QuantidadeInicial = (string)value; break;							
						case "ValorMercado": this.str.ValorMercado = (string)value; break;							
						case "ValorCustoLiquido": this.str.ValorCustoLiquido = (string)value; break;							
						case "ResultadoRealizar": this.str.ResultadoRealizar = (string)value; break;							
						case "AjusteDiario": this.str.AjusteDiario = (string)value; break;							
						case "AjusteAcumulado": this.str.AjusteAcumulado = (string)value; break;
                        case "IdConta": this.str.IdConta = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoMercado = (System.Byte?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "PUMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUMercado = (System.Decimal?)value;
							break;
						
						case "PUCusto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCusto = (System.Decimal?)value;
							break;
						
						case "PUCustoLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCustoLiquido = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Quantidade = (System.Int32?)value;
							break;
						
						case "QuantidadeInicial":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.QuantidadeInicial = (System.Int32?)value;
							break;
						
						case "ValorMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMercado = (System.Decimal?)value;
							break;
						
						case "ValorCustoLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCustoLiquido = (System.Decimal?)value;
							break;
						
						case "ResultadoRealizar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoRealizar = (System.Decimal?)value;
							break;
						
						case "AjusteDiario":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AjusteDiario = (System.Decimal?)value;
							break;
						
						case "AjusteAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AjusteAcumulado = (System.Decimal?)value;
							break;

                        case "IdConta":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdConta = (System.Int32?)value;
                            break;

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoBMF.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoBMFMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoBMFMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PosicaoBMFMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoBMFMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(PosicaoBMFMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoBMFMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(PosicaoBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(PosicaoBMFMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.Serie
		/// </summary>
		virtual public System.String Serie
		{
			get
			{
				return base.GetSystemString(PosicaoBMFMetadata.ColumnNames.Serie);
			}
			
			set
			{
				if(base.SetSystemString(PosicaoBMFMetadata.ColumnNames.Serie, value))
				{
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.TipoMercado
		/// </summary>
		virtual public System.Byte? TipoMercado
		{
			get
			{
				return base.GetSystemByte(PosicaoBMFMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemByte(PosicaoBMFMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(PosicaoBMFMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoBMFMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.PUMercado
		/// </summary>
		virtual public System.Decimal? PUMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFMetadata.ColumnNames.PUMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFMetadata.ColumnNames.PUMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.PUCusto
		/// </summary>
		virtual public System.Decimal? PUCusto
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFMetadata.ColumnNames.PUCusto);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFMetadata.ColumnNames.PUCusto, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.PUCustoLiquido
		/// </summary>
		virtual public System.Decimal? PUCustoLiquido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFMetadata.ColumnNames.PUCustoLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFMetadata.ColumnNames.PUCustoLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.Quantidade
		/// </summary>
		virtual public System.Int32? Quantidade
		{
			get
			{
				return base.GetSystemInt32(PosicaoBMFMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoBMFMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.QuantidadeInicial
		/// </summary>
		virtual public System.Int32? QuantidadeInicial
		{
			get
			{
				return base.GetSystemInt32(PosicaoBMFMetadata.ColumnNames.QuantidadeInicial);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoBMFMetadata.ColumnNames.QuantidadeInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.ValorMercado
		/// </summary>
		virtual public System.Decimal? ValorMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFMetadata.ColumnNames.ValorMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFMetadata.ColumnNames.ValorMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.ValorCustoLiquido
		/// </summary>
		virtual public System.Decimal? ValorCustoLiquido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFMetadata.ColumnNames.ValorCustoLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFMetadata.ColumnNames.ValorCustoLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.ResultadoRealizar
		/// </summary>
		virtual public System.Decimal? ResultadoRealizar
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFMetadata.ColumnNames.ResultadoRealizar);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFMetadata.ColumnNames.ResultadoRealizar, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.AjusteDiario
		/// </summary>
		virtual public System.Decimal? AjusteDiario
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFMetadata.ColumnNames.AjusteDiario);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFMetadata.ColumnNames.AjusteDiario, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBMF.AjusteAcumulado
		/// </summary>
		virtual public System.Decimal? AjusteAcumulado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBMFMetadata.ColumnNames.AjusteAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBMFMetadata.ColumnNames.AjusteAcumulado, value);
			}
		}

        /// <summary>
        /// Maps to PosicaoBMF.IdConta
        /// </summary>
        virtual public System.Int32? IdConta
        {
            get
            {
                return base.GetSystemInt32(PosicaoBMFMetadata.ColumnNames.IdConta);
            }

            set
            {
                if (base.SetSystemInt32(PosicaoBMFMetadata.ColumnNames.IdConta, value))
                {
                    this._UpToContaCorrenteByIdConta = null;
                }
            }
        }

		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected AtivoBMF _UpToAtivoBMFByCdAtivoBMF;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
        [CLSCompliant(false)]
        internal protected Financial.Investidor.ContaCorrente _UpToContaCorrenteByIdConta;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Serie
			{
				get
				{
					System.String data = entity.Serie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Serie = null;
					else entity.Serie = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Byte? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToByte(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String PUMercado
			{
				get
				{
					System.Decimal? data = entity.PUMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUMercado = null;
					else entity.PUMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCusto
			{
				get
				{
					System.Decimal? data = entity.PUCusto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCusto = null;
					else entity.PUCusto = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCustoLiquido
			{
				get
				{
					System.Decimal? data = entity.PUCustoLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCustoLiquido = null;
					else entity.PUCustoLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Int32? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToInt32(value);
				}
			}
				
			public System.String QuantidadeInicial
			{
				get
				{
					System.Int32? data = entity.QuantidadeInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeInicial = null;
					else entity.QuantidadeInicial = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorMercado
			{
				get
				{
					System.Decimal? data = entity.ValorMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMercado = null;
					else entity.ValorMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCustoLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorCustoLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCustoLiquido = null;
					else entity.ValorCustoLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoRealizar
			{
				get
				{
					System.Decimal? data = entity.ResultadoRealizar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoRealizar = null;
					else entity.ResultadoRealizar = Convert.ToDecimal(value);
				}
			}
				
			public System.String AjusteDiario
			{
				get
				{
					System.Decimal? data = entity.AjusteDiario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AjusteDiario = null;
					else entity.AjusteDiario = Convert.ToDecimal(value);
				}
			}
				
			public System.String AjusteAcumulado
			{
				get
				{
					System.Decimal? data = entity.AjusteAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AjusteAcumulado = null;
					else entity.AjusteAcumulado = Convert.ToDecimal(value);
				}
			}

            public System.String IdConta
            {
                get
                {
                    System.Int32? data = entity.IdConta;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdConta = null;
                    else entity.IdConta = Convert.ToInt32(value);
                }
            }

			private esPosicaoBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoBMF : esPosicaoBMF
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_PosicaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBMF_PosicaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBMF UpToAtivoBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToAtivoBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					&& Serie != null					)
				{
					this._UpToAtivoBMFByCdAtivoBMF = new AtivoBMF();
					this._UpToAtivoBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Where(this._UpToAtivoBMFByCdAtivoBMF.Query.Serie == this.Serie);
					this._UpToAtivoBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToAtivoBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this.Serie = null;
					this._UpToAtivoBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this.Serie = value.Serie;
					this._UpToAtivoBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToAtivoBMFByCdAtivoBMF", this._UpToAtivoBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PosicaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion

        #region UpToContaCorrenteByIdConta - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - FK_PosicaoBMF_ContaCorrente
        /// </summary>

        [XmlIgnore]
        public Financial.Investidor.ContaCorrente UpToContaCorrenteByIdConta
        {
            get
            {
                if (this._UpToContaCorrenteByIdConta == null
                    && IdConta != null)
                {
                    this._UpToContaCorrenteByIdConta = new Financial.Investidor.ContaCorrente();
                    this._UpToContaCorrenteByIdConta.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
                    this._UpToContaCorrenteByIdConta.Query.Where(this._UpToContaCorrenteByIdConta.Query.IdConta == this.IdConta);
                    this._UpToContaCorrenteByIdConta.Query.Load();
                }

                return this._UpToContaCorrenteByIdConta;
            }

            set
            {
                this.RemovePreSave("UpToContaCorrenteByIdConta");


                if (value == null)
                {
                    this.IdConta = null;
                    this._UpToContaCorrenteByIdConta = null;
                }
                else
                {
                    this.IdConta = value.IdConta;
                    this._UpToContaCorrenteByIdConta = value;
                    this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
                }

            }
        }
        #endregion
		
		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
            if (!this.es.IsDeleted && this._UpToContaCorrenteByIdConta != null)
            {
                this.IdConta = this._UpToContaCorrenteByIdConta.IdConta;
            }
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Serie
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.Serie, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.TipoMercado, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PUMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.PUMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCusto
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.PUCusto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCustoLiquido
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.PUCustoLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.Quantidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QuantidadeInicial
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.QuantidadeInicial, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.ValorMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCustoLiquido
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.ValorCustoLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoRealizar
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.ResultadoRealizar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AjusteDiario
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.AjusteDiario, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AjusteAcumulado
		{
			get
			{
				return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.AjusteAcumulado, esSystemType.Decimal);
			}
		}

        public esQueryItem IdConta
        {
            get
            {
                return new esQueryItem(this, PosicaoBMFMetadata.ColumnNames.IdConta, esSystemType.Int32);
            }
        } 
	}



	[Serializable]
	[XmlType("PosicaoBMFCollection")]
	public partial class PosicaoBMFCollection : esPosicaoBMFCollection, IEnumerable<PosicaoBMF>
	{
		public PosicaoBMFCollection()
		{

		}
		
		public static implicit operator List<PosicaoBMF>(PosicaoBMFCollection coll)
		{
			List<PosicaoBMF> list = new List<PosicaoBMF>();
			
			foreach (PosicaoBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoBMF AddNew()
		{
			PosicaoBMF entity = base.AddNewEntity() as PosicaoBMF;
			
			return entity;
		}

		public PosicaoBMF FindByPrimaryKey(System.Int32 idPosicao)
		{
			return base.FindByPrimaryKey(idPosicao) as PosicaoBMF;
		}


		#region IEnumerable<PosicaoBMF> Members

		IEnumerator<PosicaoBMF> IEnumerable<PosicaoBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoBMF;
			}
		}

		#endregion
		
		private PosicaoBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoBMF' table
	/// </summary>

	[Serializable]
	public partial class PosicaoBMF : esPosicaoBMF
	{
		public PosicaoBMF()
		{

		}
	
		public PosicaoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoBMFQuery query;
	}



	[Serializable]
	public partial class PosicaoBMFQuery : esPosicaoBMFQuery
	{
		public PosicaoBMFQuery()
		{

		}		
		
		public PosicaoBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.IdAgente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.CdAtivoBMF, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.CdAtivoBMF;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.Serie, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.Serie;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.TipoMercado, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.TipoMercado;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.DataVencimento, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.PUMercado, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.PUMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.PUCusto, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.PUCusto;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.PUCustoLiquido, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.PUCustoLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.Quantidade, 10, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.QuantidadeInicial, 11, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.QuantidadeInicial;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.ValorMercado, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.ValorMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.ValorCustoLiquido, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.ValorCustoLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.ResultadoRealizar, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.ResultadoRealizar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.AjusteDiario, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.AjusteDiario;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.AjusteAcumulado, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBMFMetadata.PropertyNames.AjusteAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c);

            c = new esColumnMetadata(PosicaoBMFMetadata.ColumnNames.IdConta, 17, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = PosicaoBMFMetadata.PropertyNames.IdConta;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c); 
				
		}
		#endregion
	
		static public PosicaoBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataVencimento = "DataVencimento";
			 public const string PUMercado = "PUMercado";
			 public const string PUCusto = "PUCusto";
			 public const string PUCustoLiquido = "PUCustoLiquido";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string ValorMercado = "ValorMercado";
			 public const string ValorCustoLiquido = "ValorCustoLiquido";
			 public const string ResultadoRealizar = "ResultadoRealizar";
			 public const string AjusteDiario = "AjusteDiario";
			 public const string AjusteAcumulado = "AjusteAcumulado";
             public const string IdConta = "IdConta";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataVencimento = "DataVencimento";
			 public const string PUMercado = "PUMercado";
			 public const string PUCusto = "PUCusto";
			 public const string PUCustoLiquido = "PUCustoLiquido";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string ValorMercado = "ValorMercado";
			 public const string ValorCustoLiquido = "ValorCustoLiquido";
			 public const string ResultadoRealizar = "ResultadoRealizar";
			 public const string AjusteDiario = "AjusteDiario";
			 public const string AjusteAcumulado = "AjusteAcumulado";
             public const string IdConta = "IdConta";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoBMFMetadata))
			{
				if(PosicaoBMFMetadata.mapDelegates == null)
				{
					PosicaoBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoBMFMetadata.meta == null)
				{
					PosicaoBMFMetadata.meta = new PosicaoBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Serie", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PUMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCusto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCustoLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QuantidadeInicial", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCustoLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoRealizar", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AjusteDiario", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AjusteAcumulado", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));	
				
				
				meta.Source = "PosicaoBMF";
				meta.Destination = "PosicaoBMF";
				
				meta.spInsert = "proc_PosicaoBMFInsert";				
				meta.spUpdate = "proc_PosicaoBMFUpdate";		
				meta.spDelete = "proc_PosicaoBMFDelete";
				meta.spLoadAll = "proc_PosicaoBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
