﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Interfaces.Import.BMF;

namespace Financial.BMF {
    public partial class TabelaTaxaSwap : esTabelaTaxaSwap {

        // Log
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaTaxaSwap));

        /// <summary>
        /// Carrega a TabelaTaxaSwap a partir do arquivo TaxaSwap.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>        
        public void CarregaTaxaSwap(DateTime data, string pathArquivo) {

            #region logEntrada
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            TaxaSwap taxaSwap = new TaxaSwap();            
            //
            TabelaTaxaSwapCollection tabelaTaxaSwapCollection = new TabelaTaxaSwapCollection();
            TabelaTaxaSwapCollection tabelaTaxaSwapCollectionAux = new TabelaTaxaSwapCollection();

            // Deleta de acordo com a data de Referencia
            tabelaTaxaSwapCollectionAux.DeletaTaxaSwap(data);
            
            // Pega as Taxas do arquivo TaxaSwap
            TaxaSwapCollection taxaSwapCollection = taxaSwap.ProcessaTaxaSwap(data, pathArquivo);
            //
            for (int i = 0; i < taxaSwapCollection.CollectionTaxaSwap.Count; i++) {
                taxaSwap = taxaSwapCollection.CollectionTaxaSwap[i];
                
                #region Copia informações para TabelaTaxaSwap
                TabelaTaxaSwap tabelaTaxaSwap = new TabelaTaxaSwap();
                tabelaTaxaSwap.DataReferencia = taxaSwap.DataReferencia;
                tabelaTaxaSwap.CodigoTaxa = taxaSwap.CodigoTaxa;
                tabelaTaxaSwap.DiasCorridos = (short)taxaSwap.NumeroDiasCorridosTaxaJuros;
                tabelaTaxaSwap.DiasUteis = (short)taxaSwap.NumeroDiasSaquesTaxaJuros;
                tabelaTaxaSwap.Valor = taxaSwap.TaxaTeorica;
                #endregion
                //
                tabelaTaxaSwapCollection.AttachEntity(tabelaTaxaSwap);
            }

            tabelaTaxaSwapCollection.Save();

            #region logSaida
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
        }

        /// <summary>
        /// Carrega o objeto TabelaTaxaSwap com o campo Valor.
        /// Busca baseada apenas no filtro de Data, CodigoTaxa e DiasUteis.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="codigoTaxa"></param>
        /// <param name="diasUteis"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaTabelaTaxaSwap(DateTime data, string codigoTaxa, int diasUteis)
        {
            // TODO logar
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }

            TabelaTaxaSwapCollection tabelaTaxaSwapCollection = new TabelaTaxaSwapCollection();

            tabelaTaxaSwapCollection.Query
                 .Select(tabelaTaxaSwapCollection.Query.Valor)
                 .Where(tabelaTaxaSwapCollection.Query.DataReferencia.Equal(data),
                        tabelaTaxaSwapCollection.Query.CodigoTaxa.Equal(codigoTaxa),
                        tabelaTaxaSwapCollection.Query.DiasUteis == diasUteis);

            tabelaTaxaSwapCollection.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled)
            {
                StringBuilder sql = new StringBuilder(tabelaTaxaSwapCollection.Query.es.LastQuery);
                sql = sql.Replace("@DataReferencia1", "'" + data.ToString("yyyy-MM-dd") + "'");
                sql = sql.Replace("@CodigoTaxa2", "'" + codigoTaxa + "'");
                sql = sql.Replace("@DiasUteis3", "'" + diasUteis + "'");                
                log.Info(sql);
            }
            #endregion

            if (tabelaTaxaSwapCollection.HasData)
            {
                // Copia informações de ativoBMFCollection para AtivoBMF
                this.AddNew();
                this.Valor = ((TabelaTaxaSwap)tabelaTaxaSwapCollection[0]).Valor;
                return true;
            }
            else { return false; }            
        }

        /// <summary>
        /// Carrega o objeto TabelaTaxaSwap com os campos DiasUteis, Valor.
        /// Busca o primeiro vértice posterior a data passada.        
        /// </summary>
        /// <param name="data"></param>
        /// <param name="codigoTaxa"></param>
        /// <param name="diasUteis"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaTabelaTaxaSwapPosterior(DateTime data, string codigoTaxa, int diasUteis)
        {
            // TODO logar
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }

            TabelaTaxaSwapCollection tabelaTaxaSwapCollection = new TabelaTaxaSwapCollection();

            tabelaTaxaSwapCollection.Query
                 .Select(tabelaTaxaSwapCollection.Query.DiasUteis, tabelaTaxaSwapCollection.Query.Valor)
                 .Where(tabelaTaxaSwapCollection.Query.DataReferencia.Equal(data),
                        tabelaTaxaSwapCollection.Query.CodigoTaxa.Equal(codigoTaxa),
                        tabelaTaxaSwapCollection.Query.DiasUteis.GreaterThan(diasUteis))
                 .OrderBy(tabelaTaxaSwapCollection.Query.DiasUteis.Ascending);

            tabelaTaxaSwapCollection.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled)
            {
                StringBuilder sql = new StringBuilder(tabelaTaxaSwapCollection.Query.es.LastQuery);
                sql = sql.Replace("@DataReferencia1", "'" + data.ToString("yyyy-MM-dd") + "'");
                sql = sql.Replace("@CodigoTaxa2", "'" + codigoTaxa + "'");
                sql = sql.Replace("@DiasUteis3", "'" + diasUteis + "'");
                log.Info(sql);
            }
            #endregion

            if (tabelaTaxaSwapCollection.HasData)
            {
                // Copia informações de ativoBMFCollection para AtivoBMF
                this.AddNew();
                this.DiasUteis = ((TabelaTaxaSwap)tabelaTaxaSwapCollection[0]).DiasUteis;
                this.Valor = ((TabelaTaxaSwap)tabelaTaxaSwapCollection[0]).Valor;
                return true;
            }
            else { return false; }
        }

        /// <summary>
        /// Carrega o objeto TabelaTaxaSwap com os campos DiasUteis, Valor.
        /// Busca o primeiro vértice anterior a data passada.        
        /// </summary>
        /// <param name="data"></param>
        /// <param name="codigoTaxa"></param>
        /// <param name="diasUteis"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaTabelaTaxaSwapAnterior(DateTime data, string codigoTaxa, int diasUteis)
        {
            // TODO logar
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }

            TabelaTaxaSwapCollection tabelaTaxaSwapCollection = new TabelaTaxaSwapCollection();

            tabelaTaxaSwapCollection.Query
                 .Select(tabelaTaxaSwapCollection.Query.DiasUteis, tabelaTaxaSwapCollection.Query.Valor)
                 .Where(tabelaTaxaSwapCollection.Query.DataReferencia.Equal(data),
                        tabelaTaxaSwapCollection.Query.CodigoTaxa.Equal(codigoTaxa),
                        tabelaTaxaSwapCollection.Query.DiasUteis.LessThan(diasUteis))
                 .OrderBy(tabelaTaxaSwapCollection.Query.DiasUteis.Descending);

            tabelaTaxaSwapCollection.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled)
            {
                StringBuilder sql = new StringBuilder(tabelaTaxaSwapCollection.Query.es.LastQuery);
                sql = sql.Replace("@DataReferencia1", "'" + data.ToString("yyyy-MM-dd") + "'");
                sql = sql.Replace("@CodigoTaxa2", "'" + codigoTaxa + "'");
                sql = sql.Replace("@DiasUteis3", "'" + diasUteis + "'");
                log.Info(sql);
            }
            #endregion

            if (tabelaTaxaSwapCollection.HasData)
            {
                // Copia informações de ativoBMFCollection para AtivoBMF
                this.AddNew();
                this.DiasUteis = ((TabelaTaxaSwap)tabelaTaxaSwapCollection[0]).DiasUteis;
                this.Valor = ((TabelaTaxaSwap)tabelaTaxaSwapCollection[0]).Valor;                
                return true;
            }
            else { return false; }
        }
    }
}
