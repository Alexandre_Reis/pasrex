﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;
using Financial.Common.Enums;
using log4net;
using Financial.BMF.Enums;
using Financial.Common;
using Financial.BMF.Exceptions;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.BMF.Properties;
using Financial.Investidor;
using Financial.Interfaces.Sinacor;
using System.Collections;

namespace Financial.BMF
{
    public partial class PosicaoBMF : esPosicaoBMF
    {
        const decimal percOutrosCustosRegistro = 16.6181M; //"Outros Custos", se aplica a registro e tx permanência

        /// <summary>
        /// Retorna o valor de mercado, de acordo com os filtros passados. Usado para apuração das regras
        /// de enquadramento.
        /// Filtra a consulta inicial por TipoMercado.Equal(TipoMercadoBMF.Futuro).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBMF"></param>
        /// <returns></returns>
        public decimal RetornaValorMercadoFuturoEnquadra(int idCliente, string cdAtivoBMF, string posicao)
        {
            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();

            posicaoBMFCollection.Query.Select(posicaoBMFCollection.Query.CdAtivoBMF, posicaoBMFCollection.Query.ValorMercado);
            posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.IdCliente.Equal(idCliente),
                                             posicaoBMFCollection.Query.TipoMercado.Equal(TipoMercadoBMF.Futuro));

            if (posicao == "C")
            {
                posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.Quantidade.GreaterThan(0));
            }
            else if (posicao == "V")
            {
                posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.Quantidade.LessThan(0));
            }

            posicaoBMFCollection.Query.Load();

            decimal totalValorMercado = 0;

            for (int i = 0; i < posicaoBMFCollection.Count; i++)
            {
                PosicaoBMF posicaoBMF = posicaoBMFCollection[i];
                string cdAtivoBMFCompara = posicaoBMF.CdAtivoBMF;
                decimal valorMercado = posicaoBMF.ValorMercado.Value;

                if (!String.IsNullOrEmpty(cdAtivoBMF))
                {
                    if (cdAtivoBMFCompara == cdAtivoBMF)
                    {
                        totalValorMercado += valorMercado;
                    }
                }
                else
                {
                    totalValorMercado += valorMercado;
                }
            }
            
            return totalValorMercado;
        }

        /// <summary>
        /// Retorna o valor de mercado, de acordo com os filtros passados. Usado para apuração das regras
        /// de enquadramento.
        /// Filtra a consulta inicial por TipoMercado.Equal(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBMF"></param>
        /// <returns></returns>
        public decimal RetornaValorMercadoOpcaoEnquadra(int idCliente, string cdAtivoBMF)
        {
            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();

            posicaoBMFCollection.Query
                 .Select(posicaoBMFCollection.Query.CdAtivoBMF, posicaoBMFCollection.Query.ValorMercado)
                 .Where(posicaoBMFCollection.Query.IdCliente.Equal(idCliente),
                        posicaoBMFCollection.Query.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo));

            posicaoBMFCollection.Query.Load();

            decimal totalValorMercado = 0;

            for (int i = 0; i < posicaoBMFCollection.Count; i++)
            {
                PosicaoBMF posicaoBMF = posicaoBMFCollection[i];
                string cdAtivoBMFCompara = posicaoBMF.CdAtivoBMF;
                decimal valorMercado = posicaoBMF.ValorMercado.Value;

                if (!String.IsNullOrEmpty(cdAtivoBMF))
                {
                    if (cdAtivoBMFCompara == cdAtivoBMF)
                    {
                        totalValorMercado += valorMercado;
                    }
                }
                else
                {
                    totalValorMercado += valorMercado;
                }

            }

            return totalValorMercado;
        }

        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCliente, DateTime data)
        {
            PosicaoBMFHistoricoCollection posicaoBMFDeletarHistoricoCollection = new PosicaoBMFHistoricoCollection();
            posicaoBMFDeletarHistoricoCollection.DeletaPosicaoBMFHistoricoDataHistoricoMaiorIgual(idCliente, data);

            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = "";
            string strFrom = "";

            sqlBuilder.Append("INSERT INTO PosicaoBMFHistorico (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoBMF WHERE IdCliente = " + idCliente;
            int count = 0;

            PosicaoBMFHistorico posicaoBMFHistorico = new PosicaoBMFHistorico();
            int columnCount = posicaoBMFHistorico.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoBMF in posicaoBMFHistorico.es.Meta.Columns)
            {
                count++;

                //Insert
                sqlBuilder.Append(colPosicaoBMF.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                if (colPosicaoBMF.Name == PosicaoBMFHistoricoMetadata.ColumnNames.DataHistorico)
                {
                    strSelect += "'" + data.ToString("yyyyMMdd") + "'";
                }
                else
                {
                    strSelect += colPosicaoBMF.Name;
                }

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());

        }

        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraPosicaoAbertura(int idCliente, DateTime data)
        {

            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = "";
            string strFrom = "";

            sqlBuilder.Append("INSERT INTO PosicaoBMFAbertura (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoBMF WHERE IdCliente = " + idCliente;
            int count = 0;

            PosicaoBMFAbertura posicaoBMFAbertura = new PosicaoBMFAbertura();
            int columnCount = posicaoBMFAbertura.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoBMF in posicaoBMFAbertura.es.Meta.Columns)
            {
                count++;

                //Insert
                sqlBuilder.Append(colPosicaoBMF.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                if (colPosicaoBMF.Name == PosicaoBMFHistoricoMetadata.ColumnNames.DataHistorico)
                {
                    strSelect += "'" + data.ToString("yyyyMMdd") + "'";
                }
                else
                {
                    strSelect += colPosicaoBMF.Name;
                }

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());

        }

        /// <summary>
        /// Retorna o valor de mercado do cliente das posições em Disponivel e Opções (s/ Disponivel e Futuro).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo,
                                                  TipoMercadoBMF.Disponivel));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente. Leva em conta o IdMoedaCliente passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idMoedaCliente"></param>
        /// <param name="data"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, int idMoedaCliente, DateTime data)
        {
            PosicaoBMFQuery posicaoBMFQuery = new PosicaoBMFQuery("P");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");

            posicaoBMFQuery.Select(posicaoBMFQuery.IdPosicao);
            posicaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFQuery.CdAtivoBMF);
            posicaoBMFQuery.Where(posicaoBMFQuery.IdCliente.Equal(idCliente),
                                    posicaoBMFQuery.Quantidade.NotEqual(0),
                                    ativoBMFQuery.IdMoeda.NotEqual(idMoedaCliente),
                                    posicaoBMFQuery.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo,
                                                                   TipoMercadoBMF.Disponivel));

            PosicaoBMFCollection posicaoBMFCollectionExiste = new PosicaoBMFCollection();
            posicaoBMFCollectionExiste.Load(posicaoBMFQuery);

            if (posicaoBMFCollectionExiste.Count == 0)
            {
                return this.RetornaValorMercado(idCliente);
            }

            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            posicaoBMFCollection.Query.Select(posicaoBMFCollection.Query.CdAtivoBMF, 
                                              posicaoBMFCollection.Query.Serie,  
                                              posicaoBMFCollection.Query.ValorMercado.Sum());
            posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.IdCliente.Equal(idCliente),
                                             posicaoBMFCollection.Query.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo,
                                                                                 TipoMercadoBMF.Disponivel));
            posicaoBMFCollection.Query.GroupBy(posicaoBMFCollection.Query.CdAtivoBMF, 
                                               posicaoBMFCollection.Query.Serie);
            posicaoBMFCollection.Query.Load();

            decimal totalValor = 0;

            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && posicaoBMFCollection.Count > 0)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }

            foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
            {
                string cdAtivoBMF = posicaoBMF.CdAtivoBMF;
                string serie = posicaoBMF.Serie;
                decimal valor = posicaoBMF.ValorMercado.Value;

                AtivoBMF ativoBMF = new AtivoBMF();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBMF.Query.IdMoeda);
                ativoBMF.LoadByPrimaryKey(campos, cdAtivoBMF, serie);
                int idMoedaAtivo = ativoBMF.IdMoeda.Value;

                if (idMoedaCliente != idMoedaAtivo)
                {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaAtivo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaAtivo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }

        /// <summary>
        /// Calcula ajuste de posição de futuros e DLA.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaAjustePosicao(int idCliente, DateTime data)
        {
            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();

            posicaoBMFCollection.BuscaPosicaoBMFCalculoAjuste(idCliente);

            OperacaoBMFCollection operacaoBMFCollectionDeletar = new OperacaoBMFCollection();
            operacaoBMFCollectionDeletar.DeletaOperacaoBMF(idCliente, (byte)OrigemOperacaoBMF.AjusteFuturo, data);

            for (int i = 0; i < posicaoBMFCollection.Count; i++)
            {
                #region Valores PosicaoBMF
                PosicaoBMF posicaoBMF = posicaoBMFCollection[i];
                string cdAtivoBMF = posicaoBMF.CdAtivoBMF;
                string serie = posicaoBMF.Serie;
                int idAgente = posicaoBMF.IdAgente.Value;
                int quantidade = posicaoBMF.Quantidade.Value;
                byte tipoMercado = posicaoBMF.TipoMercado.Value;
                int? idConta = ParametrosConfiguracaoSistema.Outras.MultiConta ? posicaoBMF.IdConta : null;

                #endregion

                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);

                decimal peso = ativoBMF.Peso.Value;
                DateTime dataVencimento = ativoBMF.DataVencimento.Value;                
                
                //Futuro de Dolar, MiniDolar e Euro só calculam o ajuste até D-1 do vencimento.
                if ((cdAtivoBMF == "DOL" || cdAtivoBMF == "WDL" || cdAtivoBMF == "EUR") && (dataVencimento == data))
                {
                    continue;
                }

                #region Cotacoes de cdAtivoBMF
                CotacaoBMF cotacaoBMF = new CotacaoBMF();

                DateTime dataAnterior = new DateTime();

                if (ativoBMF.IdMoeda.Value == (byte)ListaMoedaFixo.Real)
                {
                    dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                }
                else
                {
                    Cliente cliente = new Cliente();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(cliente.Query.IdMoeda);
                    cliente.LoadByPrimaryKey(campos, idCliente);
                    int idMoedaCliente = cliente.IdMoeda.Value;

                    if (idMoedaCliente == (int)ListaMoedaFixo.Real)
                    {
                        dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.NovaYork, TipoFeriado.NovaYorkBMF); //considera feriado brasil e NY
                    }
                    else
                    {
                        dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros); //considera feriado NY apenas
                    }
                }

                decimal puAnterior = 0;
                decimal puFechamento = 0;

                try
                {
                    //Busca cotacoes do dia e do dia anterior
                    //Futuros de Juros (DI1, DDI) usam o PU corrigido de D0, os outros usam o PU de fechamento de D-1 
                    cotacaoBMF.BuscaCotacaoBMF(cdAtivoBMF, serie, data);
                    puFechamento = cotacaoBMF.PUFechamento.Value;
                }
                catch (Exception)
                {
                    if ((ativoBMF.IdMoeda.Value == (byte)ListaMoedaFixo.Real && !Calendario.IsDiaUtil(data, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil)) ||
                        (ativoBMF.IdMoeda.Value == (byte)ListaMoedaFixo.Real && !Calendario.IsDiaUtil(data, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) ||
                        (ativoBMF.IdMoeda.Value == (byte)ListaMoedaFixo.Dolar && !Calendario.IsDiaUtil(data, (int)LocalFeriadoFixo.NovaYork, TipoFeriado.Outros)))
                    {
                        continue;
                    }
                    else
                    {
                        throw new CotacaoBMFNaoCadastradoException("Cotação de " + cdAtivoBMF + serie + " não cadastrada na data " + data);                    
                    }
                }

                try
                {
                    if (AtivoBMF.IsAtivoJuros(cdAtivoBMF))
                    {
                        cotacaoBMF.BuscaCotacaoBMF(cdAtivoBMF, serie, data);
                        puAnterior = cotacaoBMF.PUCorrigido.Value;

                        if (puAnterior == 0)
                        {
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            cotacaoIndice.LoadByPrimaryKey(dataAnterior, (short)ListaIndiceFixo.CDI);

                            cotacaoBMF.BuscaCotacaoBMF(cdAtivoBMF, serie, dataAnterior);

                            int prazoDu = Calendario.NumeroDias(dataAnterior, data, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                            if (cotacaoBMF.PUFechamento.GetValueOrDefault(0) != 0)
                                puAnterior = Convert.ToDecimal((double)cotacaoBMF.PUFechamento.Value * (Math.Pow(((double)cotacaoIndice.Valor.Value / (double)100) + 1, ((double)prazoDu / (double)252))));
                        }
                    }
                    else
                    {
                        cotacaoBMF.BuscaCotacaoBMF(cdAtivoBMF, serie, dataAnterior);
                        puAnterior = cotacaoBMF.PUFechamento.Value;
                    }
                }
                catch (Exception)
                {
                    if ((ativoBMF.IdMoeda.Value == (byte)ListaMoedaFixo.Real && !Calendario.IsDiaUtil(data, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil)) ||
                        (ativoBMF.IdMoeda.Value == (byte)ListaMoedaFixo.Real && !Calendario.IsDiaUtil(data, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) ||
                        (ativoBMF.IdMoeda.Value == (byte)ListaMoedaFixo.Dolar && !Calendario.IsDiaUtil(data, (int)LocalFeriadoFixo.NovaYork, TipoFeriado.Outros)))
                    {
                        continue;
                    }
                    else
                    {
                        if (AtivoBMF.IsAtivoJuros(cdAtivoBMF))
                        {
                            throw new CotacaoBMFNaoCadastradoException("Cotação de " + cdAtivoBMF + serie + " não cadastrada na data " + data);
                        }
                        else
                        {
                            throw new CotacaoBMFNaoCadastradoException("Cotação de " + cdAtivoBMF + serie + " não cadastrada na data " + dataAnterior);
                        }
                    }
                }                
                #endregion

                #region Cotacao PTax
                //DDI usa a Ptax de D-1 (Feriado Brasil), os outros usam a Ptax de D0

                decimal ptax;
                if (cdAtivoBMF == "DDI")
                {
                    DateTime dataAux = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    ptax = cotacaoBMF.BuscaPtaxAtivo(cdAtivoBMF, dataAux);
                }
                else
                {
                    ptax = cotacaoBMF.BuscaPtaxAtivo(cdAtivoBMF, data);
                }
                #endregion

                decimal ajuste = Utilitario.Truncate(Utilitario.Truncate((puFechamento - puAnterior), 7) * peso * ptax, 2);
                ajuste = Utilitario.Truncate(ajuste * quantidade, 2);

                // Faz o Update de PosicaoBMF
                posicaoBMF.AjusteDiario = ajuste;
                posicaoBMF.AjusteAcumulado += ajuste;

                string tipoOperacao;
                if (quantidade > 0)
                {
                    tipoOperacao = TipoOperacaoBMF.Compra;
                }
                else
                {
                    tipoOperacao = TipoOperacaoBMF.Venda;
                }

                #region Lanca em OperacaoBMF o ajuste de posição
                OperacaoBMF operacaoBMF = new OperacaoBMF();
                operacaoBMF.IdCliente = idCliente;
                operacaoBMF.IdAgenteCorretora = idAgente;
                operacaoBMF.IdAgenteLiquidacao = idAgente;
                operacaoBMF.IdLocalCustodia = (byte)LocalCustodiaFixo.CBLC;
                operacaoBMF.IdClearing = (byte)ClearingFixo.BOVESPA;                
                operacaoBMF.IdLocalNegociacao = (int)LocalNegociacaoFixo.Brasil;
                operacaoBMF.CdAtivoBMF = cdAtivoBMF;
                operacaoBMF.Serie = serie;                
                operacaoBMF.Ajuste = ajuste;                                                
                operacaoBMF.Data = data;
                operacaoBMF.Fonte = (byte)FonteOperacaoBMF.Interno;
                operacaoBMF.Origem = (byte)OrigemOperacaoBMF.AjusteFuturo;
                operacaoBMF.Quantidade = quantidade;                
                operacaoBMF.TipoMercado = tipoMercado;
                operacaoBMF.TipoOperacao = tipoOperacao;
                operacaoBMF.Pu = 0;
                operacaoBMF.PULiquido = 0;
                operacaoBMF.Valor = 0;
                operacaoBMF.ValorLiquido = 0;
                operacaoBMF.Corretagem = 0;
                operacaoBMF.Emolumento = 0;
                operacaoBMF.Registro = 0;
                operacaoBMF.CustoWtr = 0;
                operacaoBMF.ResultadoRealizado = 0;
                operacaoBMF.IdentificadorEstrategia = 0;
                operacaoBMF.CalculaDespesas = "N";
                operacaoBMF.PercentualDesconto = 0;
                operacaoBMF.IdMoeda = ativoBMF.IdMoeda.Value;
                operacaoBMF.DataOperacao = data;
                if (idConta.HasValue && idConta.Value > 0)
                {
                    operacaoBMF.IdConta = idConta;
                }
                operacaoBMFCollection.AttachEntity(operacaoBMF);
                #endregion
            }

            #region Save das Collections
            posicaoBMFCollection.Save();            
            liquidacaoCollection.Save();
            operacaoBMFCollection.Save();
            #endregion
        }

        /// <summary>
        /// Lança em Liquidacao os valores de ajustes de posição, de forma analítica.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void LancaCCAjustePosicaoAnalitico(int idCliente, DateTime data)
        {
            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            Hashtable hsContaAtivo = new Hashtable();

            posicaoBMFCollection.BuscaPosicaoBMFAjusteDiarioAnalitico(idCliente);
            for (int i = 0; i < posicaoBMFCollection.Count; i++)
            {
                PosicaoBMF posicaoBMF = posicaoBMFCollection[i];
                string cdAtivoBMF = posicaoBMF.CdAtivoBMF;
                string serie = posicaoBMF.Serie;
                int idAgente = posicaoBMF.IdAgente.Value;                
                decimal ajuste = posicaoBMF.AjusteDiario.Value;
                int? idConta = posicaoBMF.IdConta.Value ;

                AtivoBMF ativoBMF = new AtivoBMF();                
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBMF.Query.IdMoeda);
                ativoBMF.LoadByPrimaryKey(campos, cdAtivoBMF, serie);
                int idMoeda = ativoBMF.IdMoeda.Value;

                int idContaDefault = 0;
                #region Busca Conta Default
                if (hsContaAtivo.Contains(cdAtivoBMF + "-" + serie))
                {
                    idContaDefault = (int)hsContaAtivo[cdAtivoBMF + "-" + serie];
                }
                else
                {
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoeda);
                    hsContaAtivo.Add(cdAtivoBMF + "-" + serie, idContaDefault);
                }
                #endregion

                DateTime dataLiquidacao = new DateTime();

                if (idMoeda == (int)ListaMoedaFixo.Real)
                {
                    dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                }
                else
                {
                    dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros);

                    Cliente cliente = new Cliente();
                    campos = new List<esQueryItem>();
                    campos.Add(cliente.Query.IdMoeda);
                    cliente.LoadByPrimaryKey(idCliente);

                    if (cliente.IdMoeda.Value == (byte)ListaMoedaFixo.Real && !Calendario.IsDiaUtil(dataLiquidacao))
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(dataLiquidacao, 1);
                    }
                }

                byte situacaoLiquidacao = 0;
                if (idMoeda == (int)ListaMoedaFixo.Real)
                {
                    situacaoLiquidacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                }
                else
                {
                    situacaoLiquidacao = (byte)SituacaoLancamentoLiquidacao.Compensacao;
                }

                #region Lancamento em Liquidacao
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;

                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = "Ajuste de Posição de " + cdAtivoBMF + serie;
                liquidacao.Valor = ajuste;
                liquidacao.Situacao = situacaoLiquidacao;
                liquidacao.Origem = (int)OrigemLancamentoLiquidacao.BMF.AjustePosicao;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdAgente = idAgente;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = ParametrosConfiguracaoSistema.Outras.MultiConta && idConta.HasValue && idConta.Value > 0 ? idConta : idContaDefault;
                liquidacaoCollection.AttachEntity(liquidacao);
                #endregion
            }

            liquidacaoCollection.Save();
        }

        /// <summary>
        /// Calcula Taxa de Permanencia de futuros e SCC.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaTaxaPermanencia(int idCliente, DateTime data)
        {
            PosicaoBMFAberturaCollection posicaoBMFAberturaCollection = new PosicaoBMFAberturaCollection();
            CustodiaBMFCollection custodiaBMFCollection = new CustodiaBMFCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            //Deleta a Custodia para Permanencia dado o Cliente/Data
            custodiaBMFCollection.DeletaCustodiaPermanencia(idCliente, data);

            DateTime dataProxima = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            DateTime dataProxima2Dias = Calendario.AdicionaDiaUtil(dataProxima, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            posicaoBMFAberturaCollection.BuscaPosicaoBMFCalculoPermanencia(idCliente, data);

            Hashtable hsContaAtivo = new Hashtable();
            for (int i = 0; i < posicaoBMFAberturaCollection.Count; i++)
            {
                #region Valores PosicaoBMFAbertura
                PosicaoBMFAbertura posicaoBMFAbertura = posicaoBMFAberturaCollection[i];
                string cdAtivoBMF = posicaoBMFAbertura.CdAtivoBMF;
                string serie = posicaoBMFAbertura.Serie;
                int idAgente = posicaoBMFAbertura.IdAgente.Value;
                decimal quantidadePosicao = posicaoBMFAbertura.Quantidade.Value;
                int idMoedaAtivo = posicaoBMFAbertura.UpToAtivoBMFByCdAtivoBMF.IdMoeda.Value;
                int? idConta = ParametrosConfiguracaoSistema.Outras.MultiConta ? posicaoBMFAbertura.IdConta : null;

                #endregion

                int idContaDefault = 0;
                #region Busca Conta Default
                if (hsContaAtivo.Contains(cdAtivoBMF + "-" + serie))
                {
                    idContaDefault = (int)hsContaAtivo[cdAtivoBMF + "-" + serie];
                }
                else
                {
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoedaAtivo);
                    hsContaAtivo.Add(cdAtivoBMF + "-" + serie, idContaDefault);
                }

                int idContaCC = idConta.HasValue && idConta.Value > 0 ? idConta.Value : idContaDefault;

                #endregion

                if (cdAtivoBMF.Length == 3) //Futuros locais têm 3 letras, para evitar calcular para futuros offshore!
                {
                    AgenteMercado agenteMercado = new AgenteMercado();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(agenteMercado.Query.Nome);
                    agenteMercado.LoadByPrimaryKey(idAgente);
                    string nomeAgente = agenteMercado.Nome;

                    #region Calculo da Permanencia
                    OperacaoBMF operacaoBMF = new OperacaoBMF();

                    //Busco o total operado (Compra, Venda, Compra DT e Venda DT) na data
                    string tipoOperacao = TipoOperacaoBMF.Compra + "','" + TipoOperacaoBMF.Venda + "','" +
                                          TipoOperacaoBMF.CompraDaytrade + "','" + TipoOperacaoBMF.VendaDaytrade;
                    decimal quantidadeOperada = operacaoBMF.RetornaTotalOperado(idCliente, idAgente, cdAtivoBMF, data, tipoOperacao, idConta);

                    TabelaCustosBMF tabelaCustosBMF = new TabelaCustosBMF();

                    //Busco os parametros P de permanencia e o fator de redução
                    tabelaCustosBMF.BuscaTabelaCustosBMF(cdAtivoBMF, data);
                    decimal taxaPermanencia = tabelaCustosBMF.Permanencia.Value;
                    decimal fatorReducao = tabelaCustosBMF.FatorReducao.Value;

                    //Indicativo se o cliente é Socio BMF. Se for, é dado o desconto em cima da tx permanencia.
                    ClienteBMF clienteBMF = new ClienteBMF();
                    clienteBMF.BuscaSocioInvestidor(idCliente);
                    if (clienteBMF.IsSocio() || clienteBMF.IsInvestidorInstitucional())
                    {
                        taxaPermanencia = Utilitario.Truncate(taxaPermanencia * (1 - ParametrosConfiguracaoSistema.BMF.DescontoSocio / 100M), 3);
                    }

                    //Calculo da permanencia do dia
                    if (cdAtivoBMF == "SC2" || cdAtivoBMF == "SC3")
                    {
                        taxaPermanencia = Math.Max(taxaPermanencia, 0.001M);
                    }
                    decimal permanenciaDia = Utilitario.Truncate(Math.Max(0, taxaPermanencia * (quantidadePosicao - (fatorReducao * quantidadeOperada))), 2);
                    #endregion

                    #region Se zerou posição, atualiza o valor a pagar e a data de pagamento.
                    CustodiaBMF custodiaBMF = new CustodiaBMF();
                    CustodiaBMF custodiaBMFInserir = new CustodiaBMF();

                    bool zerouPosicao = false;
                    if (!this.BuscaExistePosicaoBMF(idCliente, idAgente, cdAtivoBMF, idConta))
                    {
                        zerouPosicao = true;

                        if (!custodiaBMF.BuscaCustodiaBMF(idCliente, cdAtivoBMF, idAgente, dataAnterior, idConta))
                        {
                            #region Insere CustodiaBMF Nova (Custodia de 1 dia apenas)
                            custodiaBMFInserir.AddNew();
                            custodiaBMFInserir.CdAtivoBMF = cdAtivoBMF;
                            custodiaBMFInserir.Data = data;
                            custodiaBMFInserir.IdAgente = idAgente;
                            custodiaBMFInserir.IdCliente = idCliente;
                            custodiaBMFInserir.ValorDiario = permanenciaDia;
                            custodiaBMFInserir.ValorAcumulado = 0;
                            custodiaBMFInserir.ValorPagar = permanenciaDia;
                            custodiaBMFInserir.DataPagamento = dataProxima;
                            if (idConta.HasValue && idConta.Value > 0)
                            {
                                custodiaBMFInserir.IdConta = idConta;
                            }
                            custodiaBMFInserir.Save();
                            #endregion
                        }
                        else
                        {
                            #region Insere CustodiaBMF Nova, pegando o valor acumulado em D-1
                            custodiaBMFInserir.AddNew();
                            custodiaBMFInserir.IdCliente = idCliente;
                            custodiaBMFInserir.IdAgente = idAgente;
                            custodiaBMFInserir.CdAtivoBMF = cdAtivoBMF;
                            custodiaBMFInserir.Data = data;
                            custodiaBMFInserir.ValorDiario = permanenciaDia;
                            custodiaBMFInserir.ValorAcumulado = 0;
                            custodiaBMFInserir.ValorPagar = custodiaBMF.ValorAcumulado + permanenciaDia;
                            custodiaBMFInserir.DataPagamento = dataProxima;
                            if (idConta.HasValue && idConta.Value > 0)
                            {
                                custodiaBMFInserir.IdConta = idConta;
                            }
                            custodiaBMFInserir.Save();
                            #endregion
                        }

                        decimal valorPagar = custodiaBMFInserir.ValorPagar.Value;

                        #region Nova Liquidacao (Pagamento de baixa de tx permanencia)
                        if (valorPagar > 0)
                        {
                            Liquidacao liquidacao = new Liquidacao();
                            liquidacao.DataLancamento = data;
                            liquidacao.DataVencimento = dataProxima;
                            liquidacao.Descricao = "Pagamento Taxa de Permanência Futuros - " + cdAtivoBMF + " (" + nomeAgente + ")";
                            liquidacao.Valor = valorPagar * -1;
                            liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                            liquidacao.Origem = (int)OrigemLancamentoLiquidacao.BMF.TaxaPermanencia;
                            liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                            liquidacao.IdAgente = idAgente;
                            liquidacao.IdCliente = idCliente;
                            liquidacao.IdConta = idContaCC;

                            liquidacaoCollection.AttachEntity(liquidacao);
                        }
                        #endregion

                        #region Nova Liquidacao (Outros custos sobre baixa de tx permanencia)
                        if (valorPagar > 0)
                        {
                            decimal outrosCustos = Math.Round(valorPagar * percOutrosCustosRegistro / 100, 2);
                            Liquidacao liquidacao = new Liquidacao();
                            liquidacao.DataLancamento = data;
                            liquidacao.DataVencimento = dataProxima;
                            liquidacao.Descricao = "Outros Custos s/ Pagamento Taxa de Perm. Futuros - " + cdAtivoBMF + " (" + nomeAgente + ")";
                            liquidacao.Valor = outrosCustos * -1;
                            liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                            liquidacao.Origem = (int)OrigemLancamentoLiquidacao.BMF.TaxaPermanencia;
                            liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                            liquidacao.IdAgente = idAgente;
                            liquidacao.IdCliente = idCliente;
                            liquidacao.IdConta = idContaCC;

                            liquidacaoCollection.AttachEntity(liquidacao);
                        }
                        #endregion

                        DateTime dataPrimeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                        #region Deleta todas as provisões de tx permanencia do mês
                        LiquidacaoCollection liquidacaoCollectionDeletar = new LiquidacaoCollection();
                        liquidacaoCollectionDeletar.Query.Select(liquidacaoCollectionDeletar.Query.IdLiquidacao);
                        liquidacaoCollectionDeletar.Query.Where(liquidacaoCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                                         liquidacaoCollectionDeletar.Query.IdAgente.Equal(idAgente),
                                                         liquidacaoCollectionDeletar.Query.DataLancamento.GreaterThanOrEqual(dataPrimeiroDiaMes),
                                                         liquidacaoCollectionDeletar.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.BMF.TaxaPermanencia),
                                                         liquidacaoCollectionDeletar.Query.Descricao.Like("Taxa de Permanência Futuros%"));
                        liquidacaoCollectionDeletar.Query.Load();
                        liquidacaoCollectionDeletar.MarkAllAsDeleted();
                        liquidacaoCollectionDeletar.Save();
                        #endregion
                    }
                    #endregion

                    #region Se penúltimo dia do mês, atualiza o vl a pagar e a data de pagto, senão atualiza o vl acumulado.
                    if (!zerouPosicao)
                    {
                        if (data.Month == dataProxima.Month && data.Month != dataProxima2Dias.Month)
                        {
                            if (!custodiaBMF.BuscaCustodiaBMF(idCliente, cdAtivoBMF, idAgente, dataAnterior, idConta))
                            {
                                #region Insere CustodiaBMF Nova (Custodia de 1 dia apenas)
                                custodiaBMFInserir.AddNew();
                                custodiaBMFInserir.CdAtivoBMF = cdAtivoBMF;
                                custodiaBMFInserir.Data = data;
                                custodiaBMFInserir.IdAgente = idAgente;
                                custodiaBMFInserir.IdCliente = idCliente;
                                custodiaBMFInserir.ValorDiario = permanenciaDia;
                                custodiaBMFInserir.ValorAcumulado = permanenciaDia;
                                custodiaBMFInserir.ValorPagar = permanenciaDia;
                                custodiaBMFInserir.DataPagamento = dataProxima;
                                if (idConta.HasValue && idConta.Value > 0)
                                {
                                    custodiaBMFInserir.IdConta = idConta;
                                }
                                custodiaBMFInserir.Save();
                                #endregion
                            }
                            else
                            {
                                #region Insere CustodiaBMF Nova, pegando o valor acumulado em D-1
                                custodiaBMFInserir.AddNew();
                                custodiaBMFInserir.IdCliente = idCliente;
                                custodiaBMFInserir.IdAgente = idAgente;
                                custodiaBMFInserir.CdAtivoBMF = cdAtivoBMF;
                                custodiaBMFInserir.Data = data;
                                custodiaBMFInserir.ValorDiario = permanenciaDia;
                                custodiaBMFInserir.ValorAcumulado = custodiaBMF.ValorAcumulado + permanenciaDia;
                                custodiaBMFInserir.ValorPagar = custodiaBMF.ValorAcumulado + permanenciaDia;
                                custodiaBMFInserir.DataPagamento = dataProxima;
                                if (idConta.HasValue && idConta.Value > 0)
                                {
                                    custodiaBMFInserir.IdConta = idConta;
                                }
                                custodiaBMFInserir.Save();
                                #endregion
                            }
                        }
                        else
                        {
                            DateTime ultimoDiaUtilMes = Calendario.RetornaUltimoDiaUtilMes(data, 0, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                            if (!custodiaBMF.BuscaCustodiaBMF(idCliente, cdAtivoBMF, idAgente, dataAnterior, idConta))
                            {
                                #region Insere CustodiaBMF Nova (Custodia de 1 dia apenas)
                                custodiaBMFInserir.AddNew();
                                custodiaBMFInserir.CdAtivoBMF = cdAtivoBMF;
                                custodiaBMFInserir.Data = data;
                                custodiaBMFInserir.IdAgente = idAgente;
                                custodiaBMFInserir.IdCliente = idCliente;
                                custodiaBMFInserir.ValorDiario = permanenciaDia;
                                custodiaBMFInserir.ValorAcumulado = permanenciaDia;
                                custodiaBMFInserir.ValorPagar = 0;
                                custodiaBMFInserir.DataPagamento = ultimoDiaUtilMes;
                                if (idConta.HasValue && idConta.Value > 0)
                                {
                                    custodiaBMFInserir.IdConta = idConta;
                                }
                                custodiaBMFInserir.Save();
                                #endregion
                            }
                            else
                            {
                                #region Insere CustodiaBMF Nova, pegando o valor acumulado em D-1
                                custodiaBMFInserir.AddNew();
                                custodiaBMFInserir.IdCliente = idCliente;
                                custodiaBMFInserir.IdAgente = idAgente;
                                custodiaBMFInserir.CdAtivoBMF = cdAtivoBMF;
                                custodiaBMFInserir.Data = data;
                                custodiaBMFInserir.ValorDiario = permanenciaDia;
                                custodiaBMFInserir.ValorAcumulado = custodiaBMF.ValorAcumulado + permanenciaDia;
                                custodiaBMFInserir.ValorPagar = 0;
                                custodiaBMFInserir.DataPagamento = ultimoDiaUtilMes;
                                if (idConta.HasValue && idConta.Value > 0)
                                {
                                    custodiaBMFInserir.IdConta = idConta;
                                }
                                custodiaBMFInserir.Save();
                                #endregion
                            }
                        }

                        #region Nova Liquidacao (Provisão de tx permanencia)
                        if (custodiaBMFInserir.ValorAcumulado.Value > 0)
                        {
                            Liquidacao liquidacao = new Liquidacao();
                            liquidacao.DataLancamento = data;
                            liquidacao.DataVencimento = custodiaBMFInserir.DataPagamento.Value;
                            liquidacao.Descricao = "Taxa de Permanência Futuros - " + cdAtivoBMF + " (" + nomeAgente + ")";
                            liquidacao.Valor = custodiaBMFInserir.ValorDiario.Value * -1;
                            liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                            liquidacao.Origem = (int)OrigemLancamentoLiquidacao.BMF.TaxaPermanencia;
                            liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                            liquidacao.IdAgente = idAgente;
                            liquidacao.IdCliente = idCliente;
                            liquidacao.IdConta = idContaCC;

                            liquidacaoCollection.AttachEntity(liquidacao);
                        }
                        #endregion
                    }
                    #endregion
                }
            }

            liquidacaoCollection.Save();
        }

        /// <summary>
        /// Retorna bool indicando se existe quantidade não zerada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgente"></param>
        /// <param name="cdAtivoBMF"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaExistePosicaoBMF(int idCliente, int idAgente, string cdAtivoBMF, int? idConta)
        {
            PosicaoBMFQuery posicaoBMFQuery = new PosicaoBMFQuery();
            posicaoBMFQuery
                 .Select(posicaoBMFQuery.Quantidade)
                 .Where(posicaoBMFQuery.IdCliente.Equal(idCliente),
                         posicaoBMFQuery.CdAtivoBMF.Equal(cdAtivoBMF),
                         posicaoBMFQuery.IdAgente.Equal(idAgente),
                         posicaoBMFQuery.Quantidade.NotEqual(0));

            if (idConta.HasValue && idConta.Value > 0)
            {
                posicaoBMFQuery.Where(posicaoBMFQuery.IdConta == idConta.Value);
            }

            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            return posicaoBMFCollection.Load(posicaoBMFQuery);
        }

        /// <summary>
        /// Calcula emolumento e corretagem em vencimento de contratos. 
        /// Cria uma operação com Origem = VencimentoFuturo para cada posição de contrato vencido.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaTaxasVencimento(int idCliente, DateTime data)
        {
            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();

            #region Inicialização das variáveis principais
            decimal formulaBase = 0;
            decimal emolumentoUnitario = 0;
            decimal emolumento = 0;
            decimal corretagem = 0;
            #endregion

            DateTime dataPosterior = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            posicaoBMFCollection.BuscaPosicaoBMFCalculoTaxasVencimento(idCliente, data, dataPosterior);
            for (int i = 0; i < posicaoBMFCollection.Count; i++)
            {
                #region Valores PosicaoBMF
                PosicaoBMF posicaoBMF = posicaoBMFCollection[i];
                int idAgente = posicaoBMF.IdAgente.Value;
                string cdAtivoBMF = posicaoBMF.CdAtivoBMF;
                string seriePosicao = posicaoBMF.Serie;
                int tipoMercado = posicaoBMF.TipoMercado.Value;
                DateTime dataVencimento = posicaoBMF.DataVencimento.Value;
                decimal quantidade = posicaoBMF.Quantidade.Value;
                #endregion

                #region Cálculo da Corretagem

                #region Busca a série para a cotação, segundo a classe de cdAtivoBMF.
                AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();
                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMFCollection.BuscaSeriesCalculoTaxasVencimento(cdAtivoBMF, data);
                if (!ativoBMFCollection.HasData)
                {
                    throw new AtivoBMFNaoCadastradoException("Vencimentos/Séries não encontrada para o ativo " + cdAtivoBMF);
                }
                //Se futuro cambial (mas não agricola), usa o primeiro mês em aberto, exceto o presente.
                if (AtivoBMF.IsAtivoCambial(cdAtivoBMF) && (!AtivoBMF.IsAgricolaCambial(cdAtivoBMF)))
                {
                    ativoBMF = (AtivoBMF)ativoBMFCollection[0];

                    if (ativoBMF.DataVencimento.Value.Month == data.Month)
                    {
                        if (!ativoBMFCollection.HasData)
                        {
                            throw new AtivoBMFNaoCadastradoException("2o vencimento em aberto não encontrado para o ativo " + cdAtivoBMF);
                        }
                        ativoBMF = (AtivoBMF)ativoBMFCollection[1];
                    }
                }
                else
                {
                    ativoBMF = (AtivoBMF)ativoBMFCollection[0];
                }

                //Se for DI1, DDI, pego a serie do ativo operado. Senão pego a série do conjunto trazido acima.
                string serie;
                if (cdAtivoBMF == "DI1" || cdAtivoBMF == "DDI")
                {
                    serie = seriePosicao;
                }
                else
                {
                    serie = ativoBMF.Serie;
                }
                #endregion

                #region Busca as cotações do dia e anterior do ativo
                CotacaoBMF cotacaoBMF = new CotacaoBMF();
                decimal cotacaoAnterior;

                if (cdAtivoBMF == "DI1" || cdAtivoBMF == "DDI")
                {
                    DateTime diaAnterior = Calendario.SubtraiDiaUtil(data, 2, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    cotacaoBMF.BuscaCotacaoBMF(cdAtivoBMF, serie, diaAnterior);
                    cotacaoAnterior = cotacaoBMF.PUCorrigido.Value;
                }
                else if (cdAtivoBMF == "DOL" || cdAtivoBMF == "DLA" || AtivoBMF.IsAtivoBond(cdAtivoBMF))
                {
                    cotacaoBMF.BuscaCotacaoBMF(cdAtivoBMF, serie, data);
                    cotacaoAnterior = cotacaoBMF.PUFechamento.Value;
                }
                else
                {
                    DateTime diaAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    cotacaoBMF.BuscaCotacaoBMF(cdAtivoBMF, serie, diaAnterior);
                    cotacaoAnterior = cotacaoBMF.PUFechamento.Value;
                }
                cotacaoBMF.BuscaCotacaoBMF(cdAtivoBMF, serie, data);
                decimal cotacaoDia = cotacaoBMF.PUFechamento.Value;
                #endregion

                decimal peso = posicaoBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value; //HIERARQUICO


                #endregion

                #region Cálculo do Emolumento Unitário
                if (cdAtivoBMF == "DDI")
                {
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    DateTime diaAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    decimal ptax = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.PTAX_800VENDA, diaAnterior);

                    //Desconto para todos os ativos a partir de 02/01/2006.
                    //DDI no vencimento tem emolumento diferenciado.
                    emolumentoUnitario = Utilitario.Truncate(0.05M * ptax * 0.95M, 2);

                    //Clientes sócios da BMF têm desconto no registro.
                    //Hoje usa o mesmo % para sócio e Investidor Institucional.
                    ClienteBMF clienteBMF = new ClienteBMF();
                    clienteBMF.BuscaSocioInvestidor(idCliente);
                    if (clienteBMF.IsSocio() || clienteBMF.IsInvestidorInstitucional())
                    {
                        emolumentoUnitario = Utilitario.Truncate(ParametrosConfiguracaoSistema.BMF.DescontoSocio / 100M * emolumentoUnitario, 2);
                    }
                    else
                    {
                        emolumentoUnitario = Utilitario.Truncate(emolumentoUnitario, 2);
                    }
                }
                else
                {
                    ativoBMF = new AtivoBMF();
                    ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);
                    TabelaCalculoBMF tabelaCalculoBMF = new TabelaCalculoBMF();
                    tabelaCalculoBMF.LoadByPrimaryKey(data, cdAtivoBMF, (byte)tipoMercado);
                    TabelaVencimentosBMF tabelaVencimentosBMF = new TabelaVencimentosBMF();
                    tabelaVencimentosBMF.LoadByPrimaryKey(data, cdAtivoBMF, serie);
                    int diasUteis = tabelaVencimentosBMF.DiasUteis.Value;
                    int diasCorridos = tabelaVencimentosBMF.DiasCorridos.Value;

                    formulaBase = ativoBMF.RetornaFormulaBaseEmolumento(data, ativoBMF, tabelaCalculoBMF,
                                                                        diasUteis, diasCorridos, TipoEstrategia.OperacaoSimples);

                    //Clientes sócios da BMF têm desconto no registro.
                    //Hoje usa o mesmo % para sócio e Investidor Institucional.
                    ClienteBMF clienteBMF = new ClienteBMF();
                    clienteBMF.BuscaSocioInvestidor(idCliente);
                    if (clienteBMF.IsSocio() || clienteBMF.IsInvestidorInstitucional())
                    {
                        emolumentoUnitario = Utilitario.Truncate(ParametrosConfiguracaoSistema.BMF.DescontoSocio / 100M * formulaBase, 2);
                    }
                    else
                    {
                        emolumentoUnitario = Utilitario.Truncate(formulaBase, 2);
                    }
                }

                //Teste de Emolumento Unitário Mínimo.
                emolumentoUnitario = Math.Max(emolumentoUnitario, 0.01M);
                #endregion

                emolumento = emolumentoUnitario * quantidade;

                #region Insert na OperacaoBMF
                string tipoOperacao;
                if (quantidade > 0)
                {
                    tipoOperacao = TipoOperacaoBMF.Retirada;
                }
                else
                {
                    tipoOperacao = TipoOperacaoBMF.Deposito;
                }
                OperacaoBMF operacaoBMF = new OperacaoBMF();
                operacaoBMF.IdCliente = idCliente;
                operacaoBMF.IdAgenteCorretora = idAgente;
                operacaoBMF.IdAgenteLiquidacao = idAgente;
                operacaoBMF.CdAtivoBMF = cdAtivoBMF;
                operacaoBMF.Serie = serie;
                operacaoBMF.TipoMercado = (byte)tipoMercado;
                operacaoBMF.TipoOperacao = tipoOperacao;
                operacaoBMF.Data = data;
                operacaoBMF.Pu = 0;
                operacaoBMF.Valor = 0;
                operacaoBMF.Quantidade = (int)Math.Abs(quantidade);
                operacaoBMF.Fonte = (int)FonteOperacaoBMF.Interno;
                operacaoBMF.Origem = OrigemOperacaoBMF.VencimentoFuturo;
                operacaoBMF.Emolumento = emolumento;
                operacaoBMF.Registro = 0; // Não se cobra registro no vencimento.
                operacaoBMF.Corretagem = corretagem;
                operacaoBMF.IdMoeda = (int)ListaMoedaFixo.Real; //MOEDA FIXA EM REAL

                operacaoBMFCollection.AttachEntity(operacaoBMF);
                #endregion

            }

            //custosBMFCollection.Save();
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMF com os campos IdPosicao, PUCusto, PUCustoLiquido, Quantidade, ValorCustoLiquido,
        /// IdCliente, CdAtivoBMF, Serie.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgente"></param>
        /// <param name="cdAtivo"></param>
        /// <param name="serie"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaPosicaoBMF(int idCliente, int idAgente, string cdAtivo, string serie, int? idConta)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.PUCusto, this.Query.PUCustoLiquido,
                    this.Query.Quantidade, this.Query.ValorCustoLiquido,
                    this.Query.IdCliente, this.Query.CdAtivoBMF,
                    this.Query.Serie)
                 .Where(this.Query.IdAgente == idAgente,
                        this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBMF == cdAtivo,
                        this.Query.Serie == serie);

            if (idConta.HasValue && idConta.Value > 0 )
            {
                this.Query.Where(this.Query.IdConta == idConta.Value);
            }

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Atualiza a posicao do cliente (quantidade, PU e PULiquido), 
        /// para o caso de posição comprada e operação de Compra.
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo comprada</param>
        /// <param name="pu">preço unitário da compra</param>
        /// <param name="puLiquido">puLiquido da compra</param>
        public void AtualizaPosicaoCompraComprado(int idPosicao, int quantidade, decimal pu, decimal puLiquido)
        {
            try
            {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e)
            {
                throw e;
            }

            decimal totalFinanceiroAntes = this.Quantidade.Value * this.PUCusto.Value;
            decimal totalFinanceiroCompra = quantidade * pu;
            decimal totalFinanceiroLiquidoAntes = this.Quantidade.Value * this.PUCustoLiquido.Value;
            decimal totalFinanceiroLiquidoCompra = quantidade * puLiquido;

            // nova Quantidade
            this.Quantidade = this.Quantidade + quantidade;
            // novo PU - soma totais e divide pela nova quantidade
            this.PUCusto = (totalFinanceiroAntes + totalFinanceiroCompra) / this.Quantidade;
            this.PUCustoLiquido = (totalFinanceiroLiquidoAntes + totalFinanceiroLiquidoCompra) / this.Quantidade;
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Atualiza a posicao do cliente (quantidade, PU e PULiquido),
        /// para o caso de posição Comprada e operação de Venda.
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo vendida</param>
        /// <param name="pu">pu da venda</param>
        /// <param name="puLiquido">puLiquido da venda</param>
        public void AtualizaPosicaoCompraVendido(int idPosicao, int quantidade, decimal pu, decimal puLiquido)
        {
            try
            {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e)
            {
                throw e;
            }

            // nova Quantidade
            this.Quantidade = this.Quantidade + quantidade;
            this.PUCusto = pu;
            this.PUCustoLiquido = puLiquido;
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Função para inserção na PosicaoBMF a partir do objeto operacaoBMF.
        /// </summary>
        /// <param name="operacaoBMF"></param>
        public void InserePosicaoBMF(OperacaoBMF operacaoBMF)
        {
            PosicaoBMF posicaoBMF = new PosicaoBMF();

            posicaoBMF.IdCliente = operacaoBMF.IdCliente;
            posicaoBMF.IdAgente = operacaoBMF.IdAgenteLiquidacao;
            posicaoBMF.CdAtivoBMF = operacaoBMF.CdAtivoBMF;
            posicaoBMF.Serie = operacaoBMF.Serie;
            posicaoBMF.TipoMercado = operacaoBMF.TipoMercado;
            if (operacaoBMF.UpToAtivoBMFByCdAtivoBMF.DataVencimento.HasValue)
            {
                posicaoBMF.DataVencimento = operacaoBMF.UpToAtivoBMFByCdAtivoBMF.DataVencimento;
            }
            posicaoBMF.PUCusto = operacaoBMF.Pu;
            posicaoBMF.PUCustoLiquido = operacaoBMF.PULiquido;
            posicaoBMF.Quantidade = operacaoBMF.Quantidade;
            posicaoBMF.QuantidadeInicial = operacaoBMF.Quantidade;

            if (operacaoBMF.UpToAtivoBMFByCdAtivoBMF.DataVencimento.HasValue)
            {
                posicaoBMF.DataVencimento = operacaoBMF.UpToAtivoBMFByCdAtivoBMF.DataVencimento;
            }

            posicaoBMF.ValorCustoLiquido = operacaoBMF.ValorLiquido.Value;

            posicaoBMF.AjusteAcumulado = operacaoBMF.Ajuste;
            posicaoBMF.AjusteDiario = operacaoBMF.Ajuste;

            if(ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoBMF.IdConta.HasValue && operacaoBMF.IdConta.Value>0)
                posicaoBMF.IdConta = operacaoBMF.IdConta;
            
            posicaoBMF.Save();
        }

        /// <summary>
        /// Atualiza a quantidade de uma posição específica.
        /// </summary>
        /// <param name="idPosicao">PK da posição.</param>
        /// <param name="quantidade">quantidade a ser alterada.</param>
        public void AtualizaQuantidade(int idPosicao, int quantidade)
        {
            try
            {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e)
            {
                throw e;
            }

            // nova Quantidade
            this.Quantidade = this.Quantidade + quantidade;
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Atualiza a posicao do cliente (quantidade, PU e PULiquido),
        /// para o caso de posição Vendida e operação de Venda.
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo vendida</param>
        /// <param name="pu">pu da venda</param>
        /// <param name="puLiquido">puLiquido da venda</param>
        public void AtualizaPosicaoVendaVendido(int idPosicao, int quantidade, decimal pu, decimal puLiquido)
        {
            this.LoadByPrimaryKey(idPosicao);
            //
            int moduloQuantidadePosicao = Math.Abs(this.Quantidade.Value);
            decimal totalFinanceiroAntes = moduloQuantidadePosicao * this.PUCusto.Value;
            decimal totalFinanceiroCompra = quantidade * pu;
            decimal totalFinanceiroLiquidoAntes = moduloQuantidadePosicao * this.PUCustoLiquido.Value;
            decimal totalFinanceiroLiquidoCompra = quantidade * puLiquido;

            // nova Quantidade - Soma em módulo e multiplica por -1
            this.Quantidade = -1 * (moduloQuantidadePosicao + quantidade);
            // novo PU - soma totais e divide pela nova quantidade
            this.PUCusto = (totalFinanceiroAntes + totalFinanceiroCompra) / Math.Abs(this.Quantidade.Value);
            this.PUCustoLiquido = (totalFinanceiroLiquidoAntes + totalFinanceiroLiquidoCompra) / Math.Abs(this.Quantidade.Value);
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Atualiza a posicao do cliente (quantidade, PU e PULiquido),
        /// para o caso de posição Comprada e operação de Venda.
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo vendida</param>
        /// <param name="pu">pu da venda</param>
        /// <param name="puLiquido">puLiquido da venda</param>
        public void AtualizaPosicaoVendaComprado(int idPosicao, int quantidade, decimal pu, decimal puLiquido)
        {
            // TODO: tratar quando nao existir     
            try
            {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e)
            {
                throw e;
            }

            // nova Quantidade
            this.Quantidade = this.Quantidade - quantidade;
            this.PUCusto = pu;
            this.PUCustoLiquido = puLiquido;
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        ///  Atualiza ValorMercado, ValorCustoLiquido, ResultadoRealizar e PuMercado de uma posicao, dado um cliente 
        ///  e uma data de cotação.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>        
        public void AtualizaValores(int idCliente, DateTime data)
        {
            CotacaoBMF cotacao = new CotacaoBMF();
            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            PosicaoBMF posicaoBMF = new PosicaoBMF();

            // Arraylist com os Ids das posicões zeradas
            List<int> listaPosicaoZerada = new List<int>();
            posicaoBMFCollection.BuscaPosicaoBMF(idCliente);

            for (int i = 0; i < posicaoBMFCollection.Count; i++)
            {
                posicaoBMF = posicaoBMFCollection[i];

                // Se posicaoBMF.Quantidade != 0 - update das posições
                if (posicaoBMF.Quantidade != 0)
                {
                    #region Busca cotação média ou fechamento, de acordo com o perfil do cliente BMF
                    try
                    {
                        cotacao.BuscaCotacaoBMF(posicaoBMF.CdAtivoBMF, posicaoBMF.Serie, data);
                    }
                    catch (Exception e)
                    {
                        AtivoBMF ativoBMF = new AtivoBMF();
                        ativoBMF.LoadByPrimaryKey(posicaoBMF.CdAtivoBMF, posicaoBMF.Serie);

                        if ((ativoBMF.IdMoeda.Value == (byte)ListaMoedaFixo.Real && !Calendario.IsDiaUtil(data, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil)) ||
                        (ativoBMF.IdMoeda.Value == (byte)ListaMoedaFixo.Real && !Calendario.IsDiaUtil(data, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) ||
                        (ativoBMF.IdMoeda.Value == (byte)ListaMoedaFixo.Dolar && !Calendario.IsDiaUtil(data, (int)LocalFeriadoFixo.NovaYork, TipoFeriado.Outros)))
                        {
                            continue;
                        }
                        else
                        {
                            throw new CotacaoBMFNaoCadastradoException("Cotação de " + ativoBMF.CdAtivoBMF + ativoBMF.Serie + " não cadastrada na data " + data);
                        }
                    }
                    

                    //Verifico se o cliente usa cotação média ou de fechamento
                    ClienteBMF clienteBMF = new ClienteBMF();
                    clienteBMF.LoadByPrimaryKey(idCliente);

                    if (!clienteBMF.es.HasData)
                    {
                        throw new ClienteBMFNaoCadastradoException("Cliente " + idCliente + " sem cadastro para BMF.");
                    }

                    decimal cotacaoDia;
                    if (clienteBMF.TipoCotacao == (int)TipoCotacaoBMF.Medio)
                    {
                        if (!cotacao.PUMedio.HasValue)
                        {
                            cotacao.PUMedio = 0;
                        }
                        cotacaoDia = cotacao.PUMedio.Value;
                    }
                    else
                    {
                        if (!cotacao.PUFechamento.HasValue)
                        {
                            cotacao.PUFechamento = 0;
                        }
                        cotacaoDia = cotacao.PUFechamento.Value;
                    }
                    #endregion

                    decimal peso = posicaoBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value; //HIERARQUICO

                    decimal ptax;
                    #region Cotacao PTax
                    //DDI usa a Ptax de D-1, os outros usam a Ptax de D0
                    CotacaoBMF cotacaoBMF = new CotacaoBMF();
                    if (posicaoBMF.CdAtivoBMF == "DDI")
                    {
                        DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                        ptax = cotacaoBMF.BuscaPtaxAtivo(posicaoBMF.CdAtivoBMF, dataAnterior);
                    }
                    else
                    {
                        ptax = cotacaoBMF.BuscaPtaxAtivo(posicaoBMF.CdAtivoBMF, data);
                    }
                    #endregion

                    decimal valorMercado = Utilitario.Truncate((posicaoBMF.Quantidade.Value * cotacaoDia) * peso * ptax, 2);
                    decimal valorCustoLiquido = Utilitario.Truncate((posicaoBMF.Quantidade.Value * posicaoBMF.PUCustoLiquido.Value) * peso * ptax, 2);

                    decimal resultadoRealizar = Utilitario.Truncate(posicaoBMF.Quantidade.Value * (cotacaoDia - posicaoBMF.PUCustoLiquido.Value) * peso * ptax, 2);

                    posicaoBMF.PUMercado = cotacaoDia;
                    posicaoBMF.ValorMercado = valorMercado;
                    posicaoBMF.ValorCustoLiquido = valorCustoLiquido;
                    posicaoBMF.ResultadoRealizar = resultadoRealizar;
                }
                // Se posicaoBMF.Quantidade = 0 - delete das posições
                else
                {
                    listaPosicaoZerada.Add(posicaoBMF.IdPosicao.Value);
                }
            }

            posicaoBMFCollection.Save();

            // deleta as posicões zeradas
            this.ExcluiPosicaoBMFZerada(listaPosicaoZerada);
        }

        /// <summary>
        /// Exclui da PosicaoBMF todas as posições com quantidade = 0.
        /// </summary>
        /// <param name="listaIdPosicao">ids das posições a serem excluidas</param>
        private void ExcluiPosicaoBMFZerada(List<int> listaIdPosicao)
        {
            // Deleta as posições zeradas sem carregá-las para a memoria                        
            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();

            for (int i = 0; i < listaIdPosicao.Count; i++)
            {
                PosicaoBMF posicaoBMF = posicaoBMFCollection.AddNew();
                posicaoBMF.IdPosicao = listaIdPosicao[i];
                posicaoBMF.AcceptChanges();
            }
            posicaoBMFCollection.MarkAllAsDeleted();
            posicaoBMFCollection.Save();
        }

        /// <summary>
        ///  Atualiza a quantidade de abertura, baseado na quantidade de fechamento do dia anterior.
        /// </summary>
        /// <param name="idCliente"></param>  
        /// Deprecated 
        /*
        public void AtualizaQuantidadeAbertura(int idCliente) {
            // TODO logar    
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }

            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            posicaoBMFCollection.BuscaPosicaoBMF(idCliente);

            for (int i = 0; i < posicaoBMFCollection.Count; i++) {
                PosicaoBMF posicaoBMF = (PosicaoBMF)posicaoBMFCollection[i];
                posicaoBMF.QuantidadeAbertura = posicaoBMF.Quantidade;
            }
            //Atualiza a quantidade de abertura
            posicaoBMFCollection.Save();

            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
        }*/

        /// <summary>
        /// Deleta posições vencidas no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaVencimento(int idCliente, DateTime data)
        {
            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();

            posicaoBMFCollection.BuscaPosicaoBMFVencimentoDia(idCliente, data);

            posicaoBMFCollection.MarkAllAsDeleted();
            posicaoBMFCollection.Save();
        }

        /// <summary>
        /// Gera boletas automáticas de exercício a partir da checagem de PU de exercicio vs PU atual.
        /// Aplica-se a posições em DLA, DOL, IDI.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraExercicioAutomatico(int idCliente, DateTime data)
        {
            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();

            posicaoBMFCollection.BuscaPosicaoBMFExercicioDia(idCliente, data, TipoMercadoBMF.OpcaoDisponivel);
            for (int i = 0; i < posicaoBMFCollection.Count; i++)
            {
                #region Valores PosicaoBMF
                PosicaoBMF posicaoBMF = (PosicaoBMF)posicaoBMFCollection[i];
                string cdAtivoBMF = posicaoBMF.CdAtivoBMF;
                string serie = posicaoBMF.Serie;
                int idAgente = posicaoBMF.IdAgente.Value;
                int quantidade = posicaoBMF.Quantidade.Value;
                #endregion

                #region Busca Tipo de Opcao (OPC, OPV), Preco de Exercicio e Peso
                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);
                string tipoSerie = ativoBMF.TipoSerie;
                decimal precoExercicio = ativoBMF.PrecoExercicio.Value;
                decimal peso = ativoBMF.Peso.Value;
                #endregion

                #region Busca Cotação para o ativo
                decimal puMercado;
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                if ((cdAtivoBMF == "DOL" || cdAtivoBMF == "DLA"))
                {
                    DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    puMercado = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.PTAX_800VENDA, dataAnterior) * 1000M;
                }
                else //IDI
                {
                    puMercado = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.IDI, data);
                }
                #endregion

                #region Testa se o PU de exercicio é maior que o PU de mercado
                bool geraExercicio;
                if (tipoSerie == TipoSerieOpcaoBMF.Compra && precoExercicio < puMercado)
                {
                    geraExercicio = true;
                }
                else if (tipoSerie == TipoSerieOpcaoBMF.Venda && precoExercicio > puMercado)
                {
                    geraExercicio = true;
                }
                else
                {
                    geraExercicio = false;
                }
                #endregion

                #region Define tipoOrdem e Origem
                //Se titular (Quantidade > 0), gera financeiro de venda (entrada de $)
                //Se lançador (Quantidade < 0), gera financeiro de compra (saída de $)
                string tipoOrdem;
                byte origem;
                if (quantidade > 0)
                {
                    tipoOrdem = TipoOrdemBMF.Venda;
                }
                else
                {
                    tipoOrdem = TipoOrdemBMF.Compra;
                }

                if (tipoSerie == TipoSerieOpcaoBMF.Compra)
                {
                    origem = (byte)OrigemOrdemBMF.ExercicioOpcaoCompra;
                }
                else
                {
                    origem = (byte)OrigemOrdemBMF.ExercicioOpcaoVenda;
                }
                #endregion

                #region Lança boleta de Exercicio
                if (geraExercicio)
                {
                    int quantidadeBoleta = Math.Abs(quantidade);
                    decimal puBoleta = Math.Abs(precoExercicio - puMercado);
                    decimal valorBoleta = Utilitario.Truncate(quantidadeBoleta * puBoleta * peso, 2);

                    OrdemBMF ordemBMF = new OrdemBMF();
                    ordemBMF.IdCliente = idCliente;
                    ordemBMF.IdAgenteCorretora = idAgente;
                    ordemBMF.IdAgenteLiquidacao = idAgente;
                    ordemBMF.CdAtivoBMF = cdAtivoBMF;
                    ordemBMF.Serie = serie;
                    ordemBMF.TipoMercado = TipoMercadoBMF.OpcaoDisponivel;
                    ordemBMF.TipoOrdem = tipoOrdem;
                    ordemBMF.Data = data;
                    ordemBMF.Pu = puBoleta;
                    ordemBMF.Valor = valorBoleta;
                    ordemBMF.Quantidade = quantidadeBoleta;
                    ordemBMF.Origem = origem;
                    ordemBMF.Fonte = (byte)FonteOrdemBMF.Interno;
                    ordemBMF.PercentualDesconto = 0;
                    ordemBMF.IdMoeda = (int)ListaMoedaFixo.Real; //MOEDA FIXO REAL

                    ordemBMFCollection.AttachEntity(ordemBMF);
                }
                #endregion
            }

            ordemBMFCollection.Save();
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMF com o campo AjusteDiarioSum(calculado).        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBMF"></param>
        public void BuscaTotalAjusteNormal(int idCliente, string cdAtivoBMF)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.AjusteDiario.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBMF.In(cdAtivoBMF)
                        );
            this.Query.Load();

            // Se valores dos campos são nulos retorna zero
            if (this.es.HasData)
            {
                this.AjusteDiario = this.AjusteDiario.HasValue ? this.AjusteDiario.Value : 0;
            }
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMF com o campo AjusteDiarioSum(calculado).        
        /// Filtra para não trazer "DOL", "IND", "DI1", "DDI".
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaTotalAjusteNormalOutros(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.AjusteDiario.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBMF.NotIn("DOL", "IND", "DI1", "DDI")
                        );
            this.Query.Load();
            
            // Se valores dos campos são nulos retorna zero
            if (this.es.HasData)
            {
                this.AjusteDiario = this.AjusteDiario.HasValue ? this.AjusteDiario.Value : 0;
            }
        }

        /// <summary>
        /// Deleta todos os ajustes diários lançados no dia, por boleta e tb em PosicaoBMF.
        /// Serve para garantir em caso de feriado BMF na data.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ZeraAjusteDiario(int idCliente, DateTime data)
        {
            //Deleta operações com Origem = Ajuste            
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Query.Select(operacaoBMFCollection.Query.IdOperacao);
            operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.IdCliente.Equal(idCliente),
                                               operacaoBMFCollection.Query.Data.Equal(data),
                                               operacaoBMFCollection.Query.Origem.Equal((byte)OrigemOperacaoBMF.AjusteFuturo));
            operacaoBMFCollection.Query.Load();
            operacaoBMFCollection.MarkAllAsDeleted();
            operacaoBMFCollection.Save();

            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            posicaoBMFCollection.Query.Select(posicaoBMFCollection.Query.IdPosicao,
                                              posicaoBMFCollection.Query.AjusteDiario);
            posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.IdCliente.Equal(idCliente));
            posicaoBMFCollection.Query.Load();

            foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
            {
                posicaoBMF.AjusteDiario = 0;
            }
            posicaoBMFCollection.Save();
        }

        /// <summary>
        /// Retorna a quantidade em posição do ativo do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaQuantidadeAtivo(int idCliente, string cdAtivoBMF, string serie)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                        this.Query.Serie.Equal(serie));

            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        /// <summary>
        /// Importa as posições a partir da TMFPOSIC (futuros).
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ImportaPosicoesSinacor(int idCliente, DateTime data)
        {   

            //Busca o IdAgenteMercado default
            int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
            AgenteMercado agenteMercado = new AgenteMercado();
            int idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);
                        
            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();

            int codigoClienteBMF = 0;
            #region Busca códigos do cliente (codigoCliente, codigoCliente2 na Bovespa)
            ClienteBMF clienteBMF = new ClienteBMF();

            //Se não achar pelo menos o código do cliente associado, sai da função
            if (!clienteBMF.LoadByPrimaryKey(idCliente))
            {
                throw new CodigoClienteBMFInvalido("Cód. BMF Inválido/Inexistente p/ Cliente ");
            }

            //Se não tiver código associado, lança exception
            if (String.IsNullOrEmpty(clienteBMF.CodigoSinacor))
            {
                throw new CodigoClienteBMFInvalido("Cód. BMF Inválido/Inexistente p/ IdCliente ");
            }

            //Se nenhum código é número, lança exception
            if (!Utilitario.IsInteger(clienteBMF.CodigoSinacor))
            {
                throw new CodigoClienteBMFInvalido("Cód. BMF Inválido/Inexistente p/ o IdCliente ");
            }

            if (!String.IsNullOrEmpty(clienteBMF.CodigoSinacor))
            {
                codigoClienteBMF = Convert.ToInt32(clienteBMF.CodigoSinacor);
            }
            #endregion

            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);

            if (codigoClienteBMF != 0)
            {
                TmfposicCollection tmfposicCollection = new TmfposicCollection();
                tmfposicCollection.es.Connection.Name = "Sinacor";
                tmfposicCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;
                tmfposicCollection.Query.Select(tmfposicCollection.Query.CdCommod,
                                                tmfposicCollection.Query.CdSerie,
                                                tmfposicCollection.Query.DataVenc,
                                                tmfposicCollection.Query.QtDiaAtu.Sum());
                tmfposicCollection.Query.Where(tmfposicCollection.Query.CdCliente.Equal(codigoClienteBMF),
                                               tmfposicCollection.Query.CdMercad.Like("FUT%"),
                                               tmfposicCollection.Query.DtDatmov.Equal(data));
                tmfposicCollection.Query.GroupBy(tmfposicCollection.Query.CdCommod,
                                                 tmfposicCollection.Query.CdSerie,
                                                 tmfposicCollection.Query.DataVenc);
                tmfposicCollection.Query.Load();

                foreach (Tmfposic tmfposic in tmfposicCollection)
                {
                    string cdAtivoBMF = tmfposic.CdCommod.Trim().ToUpper();
                    string serie = tmfposic.CdSerie.Trim().ToUpper();
                    DateTime? dataVencimento = tmfposic.DataVenc;
                    decimal quantidade = tmfposic.QtDiaAtu.Value;

                    AtivoBMF ativoBMF = new AtivoBMF();
                    ativoBMF.Query.Select(ativoBMF.Query.Peso);
                    ativoBMF.Query.Where(ativoBMF.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                         ativoBMF.Query.Serie.Equal(serie));
                    ativoBMF.Query.Load();

                    decimal cotacaoFechamento = 0;
                    CotacaoBMF cotacaoBMF = new CotacaoBMF();
                    cotacaoBMF.Query.Select(cotacaoBMF.Query.PUFechamento);
                    cotacaoBMF.Query.Where(cotacaoBMF.Query.Data.Equal(data),
                                           cotacaoBMF.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                           cotacaoBMF.Query.Serie.Equal(serie));
                    if (cotacaoBMF.Query.Load())
                    {
                        cotacaoFechamento = cotacaoBMF.PUFechamento.Value;
                    }

                    decimal valorMercado = Utilitario.Truncate(quantidade * cotacaoFechamento * ativoBMF.Peso.Value, 2);

                    PosicaoBMF posicaoBMF = posicaoBMFCollection.AddNew();
                    posicaoBMF.AjusteAcumulado = 0;
                    posicaoBMF.AjusteDiario = 0;
                    posicaoBMF.CdAtivoBMF = cdAtivoBMF;
                    posicaoBMF.DataVencimento = dataVencimento;
                    posicaoBMF.IdAgente = idAgenteMercado;
                    posicaoBMF.IdCliente = idCliente;
                    posicaoBMF.PUCusto = cotacaoFechamento;
                    posicaoBMF.PUCustoLiquido = cotacaoFechamento;
                    posicaoBMF.PUMercado = cotacaoFechamento;
                    posicaoBMF.Quantidade = (int)quantidade;
                    posicaoBMF.QuantidadeInicial = (int)quantidade;
                    posicaoBMF.ResultadoRealizar = 0;
                    posicaoBMF.Serie = serie;
                    posicaoBMF.TipoMercado = (byte)TipoMercadoBMF.Futuro;
                    posicaoBMF.ValorCustoLiquido = valorMercado;
                    posicaoBMF.ValorMercado = valorMercado;

                    if (ParametrosConfiguracaoSistema.Outras.MultiConta)
                    {
                        posicaoBMF.IdConta = idContaDefault;
                    }
                }
            }

            posicaoBMFCollection.Save();            

        }

        public decimal RetornaPUConvertido(decimal taxa, DateTime dataOperacao, DateTime dataVencimento)
        {
            int numeroDias = Calendario.NumeroDias(dataOperacao, dataVencimento, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            //PU = 100.000 / (1 + taxa) ^ (numeroDias / 252)
            decimal PU = Convert.ToDecimal((double)100000M / (Math.Pow(Convert.ToDouble(1 + (taxa / 100)), 
                (double)numeroDias / (double)252M)));

            return PU;
        }

        public decimal RetornaPUConvertidoDDI(decimal taxa, DateTime dataOperacao, DateTime dataVencimento)
        {
            int numeroDias = Calendario.NumeroDias(dataOperacao, dataVencimento);

            //PU = 100.000 / ((taxa/100) * (numeroDias / 360)+1)
            decimal PU = Convert.ToDecimal((double)100000M / (Convert.ToDouble(taxa / 100) * ((double)numeroDias / (double)360M) + 1));

            return PU;
        }

    }
}
