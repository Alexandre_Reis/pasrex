using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.BMF {
    public partial class TabelaVolatilidadeEstrategiaCollection : esTabelaVolatilidadeEstrategiaCollection {
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaVolatilidadeEstrategiaCollection));

        /// <summary>
        ///  Deleta tudo da TabelaVolatilidadeEstrategia de uma determinada data.
        /// </summary>
        /// <param name="data"></param>
        public void DeletaTabelaVolatilidadeEstrategia(DateTime data) {

            #region logEntrada
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            this.QueryReset();
            this.Query
              .Select(this.query.CdAtivoBMF, this.query.Serie, this.query.DataReferencia)
              .Where(this.query.DataReferencia == data);

            this.Query.Load();

            #region logSql
            if (log.IsInfoEnabled) {
                string sql = "Delete: " + this.query.es.LastQuery;
                sql = sql.Replace("@DataReferencia1", "'" + data.ToString("yyyy-MM-dd") + "'");
                log.Info(sql);
            }
            #endregion

            this.MarkAllAsDeleted();
            this.Save();

            #region logSaida
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
        }
    }
}
