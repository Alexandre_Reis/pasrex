using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.BMF
{
	public partial class TabelaVencimentosBMF : esTabelaVencimentosBMF
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaVencimentosBMF));

        /// <summary>
        /// Carrega o objeto TabelaVencimentosBMF com os campos DiasCorridos, DiasUteis.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="serie"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool BuscaTabelaVencimentosBMF(string cdAtivoBMF, string serie, DateTime data)
        {
            // TODO logar
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }

            this.QueryReset();
            this.Query
                 .Select(this.query.DiasCorridos, this.query.DiasUteis)
                 .Where(this.query.CdAtivoBMF == cdAtivoBMF,
                        this.query.Serie == serie,
                        this.query.DataReferencia == data);

            this.Query.Load();

            #region logSql
            if (log.IsInfoEnabled) {
                string sql = this.query.es.LastQuery;
                sql = sql.Replace("@CdAtivoBMF1", "'" + cdAtivoBMF + "'");
                sql = sql.Replace("@Serie2", "'" + serie + "'");
                sql = sql.Replace("@DataReferencia3", "'" + data.ToString("yyyy-MM-dd") + "'");

                log.Info(sql);
            }
            #endregion	            

            //TODO logar this.query.es.LastQuery;
            if (log.IsDebugEnabled)
            {
                log.Debug("Saida nomeMetodo: ");
            }

            return this.es.HasData;
        }
        
	}
}
