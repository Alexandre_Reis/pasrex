﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.BMF.Exceptions;
using Financial.Interfaces.Import.BMF;

namespace Financial.BMF {
    public partial class TabelaCustosBMF : esTabelaCustosBMF {
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaCustosBMF));

        /// <summary>
        /// Carrega o objeto TabelaCustosBMF com os campos Permanencia, FatorReducao.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="data"></param>
        public void BuscaTabelaCustosBMF(string cdAtivoBMF, DateTime data)
        {
            // TODO logar
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }

            TabelaCustosBMFCollection tabelaCustosBMFCollection = new TabelaCustosBMFCollection();

            tabelaCustosBMFCollection.Query
                 .Select(tabelaCustosBMFCollection.Query.Permanencia, tabelaCustosBMFCollection.Query.FatorReducao)
                 .Where(tabelaCustosBMFCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                        tabelaCustosBMFCollection.Query.DataReferencia.Equal(data));

            tabelaCustosBMFCollection.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled) {
                StringBuilder sql = new StringBuilder(tabelaCustosBMFCollection.Query.es.LastQuery);
                sql = sql.Replace("@CdAtivoBMF1", "'" + cdAtivoBMF + "'");
                sql = sql.Replace("@DataReferencia2", "'" + data.ToString("yyyy-MM-dd") + "'");
                log.Info(sql);
            }
            #endregion

            if (tabelaCustosBMFCollection.HasData) {
                // Copia informações de tabelaCustosBMFCollection para TabelaCustosBMF
                this.AddNew();
                this.Permanencia = ((TabelaCustosBMF)tabelaCustosBMFCollection[0]).Permanencia;
                this.FatorReducao = ((TabelaCustosBMF)tabelaCustosBMFCollection[0]).FatorReducao;
            }
            else {
                throw new TabelaCustosBMFNaoCadastradoException("Tabela de Custos BMF não cadastrada para o ativo " +
                                                                cdAtivoBMF + " na data " + data +
                                                                ". Sugestão: importar TarPar/TarPreg.");
            }
                
            //TODO logar this.query.es.LastQuery;
            if (log.IsDebugEnabled)
            {
                log.Debug("Saida nomeMetodo: ");
            }
        }

        /// <summary>
        /// Carrega o objeto TabelaCustosBMF com os campos Emolumento, EmolumentoDT, EmolumentoSocio,
        /// EmolumentoDTSocio, Registro, RegistroDT, RegistroSocio, RegistroDTSocio.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="serie"></param>
        /// <param name="data"></param>
        public void BuscaTabelaCustosBMF(string cdAtivoBMF, string serie, DateTime data) {
            // TODO logar
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }

            this.QueryReset();
            this.Query
                 .Select(this.query.Emolumento, this.query.EmolumentoDT, this.query.EmolumentoSocio,
                         this.query.EmolumentoDTSocio, this.query.Registro, this.query.RegistroDT,
                         this.query.RegistroSocio, this.query.RegistroDTSocio)
                 .Where(this.query.CdAtivoBMF.Equal(cdAtivoBMF),
                        this.query.Serie.Equal(serie),
                        this.query.DataReferencia.Equal(data));

            this.Query.Load();

            #region logSql
            if (log.IsInfoEnabled) {
                string sql = this.query.es.LastQuery;
                sql = sql.Replace("@CdAtivoBMF1", "'" + cdAtivoBMF + "'");
                sql = sql.Replace("@Serie2", "'" + serie + "'");
                sql = sql.Replace("@DataReferencia3", "'" + data.ToString("yyyy-MM-dd") + "'");

                log.Info(sql);
            }
            #endregion

            if (!this.es.HasData) {
                throw new TabelaCustosBMFNaoCadastradoException("Tabela de Custos BMF não cadastrada para o ativo " +
                                                                cdAtivoBMF + " e série " + serie + " na data " + data +
                                                                ". Sugestão: importar TarPar/TarPreg.");
            }

            //TODO logar this.query.es.LastQuery;
            if (log.IsDebugEnabled) {
                log.Debug("Saida nomeMetodo: ");
            }

        }

        /// <summary>
        ///  Carrega a tabela TabelaCustosBMF a partir do arquivo TarPreg.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaTarPreg(DateTime data, string pathArquivo) {
            #region logEntrada
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            TarPreg tarPreg = new TarPreg();
            TabelaCustosBMFCollection tabelaCustosBMFCollection = new TabelaCustosBMFCollection();
            TabelaCustosBMFCollection tabelaCustosBMFCollectionAux = new TabelaCustosBMFCollection();

            // Deleta a tabela de custos BMF da data em questão
            tabelaCustosBMFCollectionAux.DeletaTabelaCustosBMF(data);
            // Pega os registros da collection de TarPreg
            TarPregCollection tarPregCollection = tarPreg.ProcessaTarPreg(data, pathArquivo);
            //
            int i = 0;
            for (i = 0; i < tarPregCollection.CollectionTarPreg.Count; i++) {
                tarPreg = tarPregCollection.CollectionTarPreg[i];

                #region Copia informações para TarPreg
                TabelaCustosBMF tabelaCustosBMF = new TabelaCustosBMF();
                tabelaCustosBMF.DataReferencia = data;
                tabelaCustosBMF.CdAtivoBMF = tarPreg.CdAtivoBMF;

                //Se Ativo + Serie nao estiver cadastrado, gera exceção
                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMF.LoadByPrimaryKey(tarPreg.CdAtivoBMF, tarPreg.Serie);

                if (ativoBMF.es.HasData) {
                    tabelaCustosBMF.Serie = tarPreg.Serie;
                    tabelaCustosBMF.Emolumento = tarPreg.Emolumento;
                    tabelaCustosBMF.EmolumentoDT = tarPreg.EmolumentoDT;
                    tabelaCustosBMF.EmolumentoSocio = tarPreg.EmolumentoSocio;
                    tabelaCustosBMF.EmolumentoDTSocio = tarPreg.EmolumentoDTSocio;
                    tabelaCustosBMF.Registro = tarPreg.Registro;
                    tabelaCustosBMF.RegistroDT = tarPreg.RegistroDT;
                    tabelaCustosBMF.RegistroSocio = tarPreg.RegistroSocio;
                    tabelaCustosBMF.RegistroDTSocio = tarPreg.RegistroDTSocio;
                    tabelaCustosBMF.FatorReducao = tarPreg.FatorReducao;
                    tabelaCustosBMF.Permanencia = tarPreg.Permanencia;
                    tabelaCustosBMFCollection.AttachEntity(tabelaCustosBMF);
                }
                #endregion
            }

            tabelaCustosBMFCollection.Save();

            #region logSaida
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
        }
    }
}
