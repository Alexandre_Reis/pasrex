﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.BMF.Enums;

namespace Financial.BMF
{
    public partial class PosicaoBMFAberturaCollection : esPosicaoBMFAberturaCollection
    {
        public PosicaoBMFAberturaCollection(PosicaoBMFCollection posicaoBMFCollection, DateTime dataHistorico)
        {
            for (int i = 0; i < posicaoBMFCollection.Count; i++)
            {
                //
                PosicaoBMFAbertura p = new PosicaoBMFAbertura();

                // Para cada Coluna de PosicaoBMF copia para PosicaoBMF
                foreach (esColumnMetadata colPosicao in posicaoBMFCollection.es.Meta.Columns)
                {
                    // Copia todas as colunas menos a Data 
                    esColumnMetadata colPosicaoBMF = p.es.Meta.Columns.FindByPropertyName(colPosicao.PropertyName);
                    if (posicaoBMFCollection[i].GetColumn(colPosicao.Name) != null)
                    {
                        p.SetColumn(colPosicaoBMF.Name, posicaoBMFCollection[i].GetColumn(colPosicao.Name));
                    }
                    p.DataHistorico = dataHistorico;
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMFAberturaCollection com os campos CdAtivoBMF, IdAgente, Quantidade.Sum()
        /// Busca posições de futuros, SCC, SC2, SC3, DIA.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoBMFCalculoPermanencia(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBMF, this.Query.IdAgente, this.Query.IdConta, "<SUM( ABS(Quantidade) ) As Quantidade>")
                 .Where(this.Query.IdCliente == idCliente &
                        this.Query.DataHistorico == dataHistorico &
                        (
                                this.Query.TipoMercado == TipoMercadoBMF.Futuro |
                                this.Query.CdAtivoBMF.In("SCC", "SC2", "SC3", "DIA")
                        )
                 )
                 .GroupBy(this.Query.IdAgente, this.Query.CdAtivoBMF, this.Query.IdConta);

            this.Query.Load();
        }

        /// <summary>
        /// Deleta todas as posições históricas de abertura com data > que a data passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoBMFAberturaDataHistoricoMaior(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMFAberturaCollection com todos os campos.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaPosicaoBMFAberturaCompleta(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.IdCliente == idCliente);

            bool retorno = this.Query.Load();

            return retorno;
        }

    }
}
