using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.BMF {
    public partial class TabelaTaxaSwapCollection : esTabelaTaxaSwapCollection {
        // Log
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaTaxaSwapCollection));
        
        /// <summary>
        /// Deleta tudo da TabelaTaxaSwap de uma data.
        /// </summary>
        /// <param name="data"></param>
        public void DeletaTaxaSwap(DateTime data) {

            #region logEntrada
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
           
            this.QueryReset();
            this.Query
              .Select(this.query.DataReferencia, this.query.CodigoTaxa, this.query.DiasCorridos,
                      this.query.DiasUteis)
              .Where(this.query.DataReferencia.Equal(data));

            this.Query.Load();
            
            #region Log Sql
            if (log.IsInfoEnabled) {
                string sql = "Delete "+this.Query.es.LastQuery;
                sql = sql.Replace("@DataReferencia1", "'" + data + "'");
                log.Info(sql);
            }
            #endregion

            this.MarkAllAsDeleted();
            this.Save();

            #region logSaida
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
        }
    }
}
