﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.BMF.Enums;
using Financial.BMF.Exceptions;
using System.IO;
using Financial.Common;
using Financial.Investidor;
using Financial.Common.Enums;
using Financial.Util;
using Financial.Util.Enums;
using Financial.Interfaces.Import.BMF.Exceptions;
using Financial.Interfaces.Import.BMF;
using Financial.Interfaces.Sinacor;
using Financial.Common.Exceptions;
using Financial.Investidor.Enums;

namespace Financial.BMF {
    public partial class OrdemBMF : esOrdemBMF {
        private static readonly ILog log = LogManager.GetLogger(typeof(OrdemBMF));

        /// <summary>
        /// Casa as operações de C/V em daytrade, desde que a operação seja do mesmo cliente/ativo/corretora/data.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CasaDayTrade(int idCliente, DateTime data)
        {
            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
            OrdemBMFCollection ordemBMFCollectionCompra = new OrdemBMFCollection();
            OrdemBMFCollection ordemBMFCollectionVenda = new OrdemBMFCollection();
            OrdemBMF ordemBMFCompra = new OrdemBMF();
            OrdemBMF ordemBMFVenda = new OrdemBMF();
            //
            int quantidadeDayTrade = 0;
            // Atualiza quantidade daytrade = 0 de todas as ordens do cliente
            ordemBMFCollection.AtualizaOrdemBMF(idCliente, data, quantidadeDayTrade);
            //
            // Collection de Ordens de Venda
            ordemBMFCollectionVenda.BuscaOrdemBMFVenda(idCliente, data);

            for (int i = 0; i < ordemBMFCollectionVenda.Count; i++) {
                ordemBMFVenda = (OrdemBMF)ordemBMFCollectionVenda[i];
                int quantidadeVenda = ordemBMFVenda.Quantidade.Value;
                int quantidadeVendaDT = ordemBMFVenda.QuantidadeDayTrade.Value;
                int idOrdemVenda = ordemBMFVenda.IdOrdem.Value;
                int idAgenteCorretora = ordemBMFVenda.IdAgenteCorretora.Value;
                int idAgenteLiquidacao = ordemBMFVenda.IdAgenteLiquidacao.Value;
                string cdAtivoBMF = ordemBMFVenda.CdAtivoBMF;
                string serie = ordemBMFVenda.Serie;
                short pontaEstrategia = ordemBMFVenda.PontaEstrategia.Value;
                int quantidadeDiferenca;

                // Collection de ordens de compra
                if (!ordemBMFCollectionCompra.BuscaOrdemBMFCompra(idCliente, data, idAgenteCorretora,
                                                                  idAgenteLiquidacao, cdAtivoBMF, serie, 
                                                                  pontaEstrategia, false))
                { 
                    continue; 
                }

                for (int j = 0; j < ordemBMFCollectionCompra.Count; j++) {
                    ordemBMFCompra = (OrdemBMF)ordemBMFCollectionCompra[j];
                    int quantidadeCompra = ordemBMFCompra.Quantidade.Value;
                    int quantidadeCompraDT = ordemBMFCompra.QuantidadeDayTrade.Value;
                    int idOrdemCompra = ordemBMFCompra.IdOrdem.Value;

                    if (quantidadeCompra == quantidadeCompraDT) 
                        continue;
                    if (quantidadeVenda == quantidadeVendaDT) 
                        break;

                    int deltaQuantidadeVenda = quantidadeVenda - quantidadeVendaDT;
                    int deltaQuantidadeCompra = quantidadeCompra - quantidadeCompraDT;

                    if (deltaQuantidadeVenda < deltaQuantidadeCompra) {
                        quantidadeDiferenca = deltaQuantidadeVenda;
                    }
                    else {
                        quantidadeDiferenca = deltaQuantidadeCompra;
                    }

                    if (quantidadeDiferenca != 0) {
                        this.AtualizaOrdemBMF(idOrdemVenda, quantidadeDiferenca + quantidadeVendaDT);
                        this.AtualizaOrdemBMF(idOrdemCompra, quantidadeDiferenca + quantidadeCompraDT);
                        quantidadeVendaDT += quantidadeDiferenca;
                    }
                }
            }
        }

        /// <summary>
        /// Atualiza a quantidade de uma posição específica.
        /// </summary>
        /// <param name="idOrdem">PK da atualizacao</param>
        /// <param name="quantidadeDaytrade">quantidadeDaytrade</param>
        public void AtualizaOrdemBMF(int idOrdem, int quantidadeDaytrade) 
        {
            OrdemBMF ordemBMFAux = new OrdemBMF();
            ordemBMFAux.IdOrdem = idOrdem;
            ordemBMFAux.AcceptChanges();
            ordemBMFAux.QuantidadeDayTrade = quantidadeDaytrade;
            ordemBMFAux.Save();
        }

        /// <summary>
        /// Carrega as ordens de BMF do dia a partir do arquivo RComiesp.
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        /// <param name="nomeArquivo">Nome do Arquivo Completo</param>
        /// 
        /// <exception cref="Financial.BMF.Exceptions.AtivoBMFNaoCadastradoException">
        /// throws AtivoBMFNaoCadastradoException se o codigo do Ativo não estiver cadastrado na Base
        /// </exception>
        /// 
        /// <exception cref="Financial.BMF.Exceptions.ArquivoRComiespNaoEncontradoException">
        /// throws ArquivoRComiespNaoEncontradoException se arquivo não existe
        /// </exception>
        public void CarregaOrdemRComiesp(DateTime data, int idCliente, string nomeArquivo) 
        {
            #region GetIdAgente do Arquivo RComiesp
            if (!File.Exists(nomeArquivo)) {
                throw new ArquivoRComiespNaoEncontradoException("Arquivo RComiesp: {0} não encontrado" + nomeArquivo);
            }

            StreamReader sr = new StreamReader(nomeArquivo);
            string linha = sr.ReadLine();
            linha = sr.ReadLine(); // 2 Linha;
            int codigoBMFAgenteCorretora = Convert.ToInt32(linha.Substring(0, 6));
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            agenteMercado.BuscaIdAgenteMercadoBMF(codigoBMFAgenteCorretora);
            int idAgenteCorretora = agenteMercado.IdAgente.Value;
            sr.Close();
            #endregion
          
            RComiesp rcomiesp = new RComiesp();
            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
            OrdemBMFCollection ordemBMFCollectionAux = new OrdemBMFCollection();

            // Deleta as ordens de acordo com idCliente, idAgenteCorretora, data, e Fonte = ARQUIVORComiesp
            ordemBMFCollectionAux.DeletaOrdemBMFRComiesp(idCliente, idAgenteCorretora, data);
            //
            RComiespCollection rcomiespCollection = rcomiesp.ProcessaRComiesp(nomeArquivo, data);            
            // Filtro pelo TipoRegistro = Detalhes                        
            RComiespCollection rcomiespCollectionAux = new RComiespCollection();
            rcomiespCollectionAux.CollectionRComiesp = rcomiespCollection.CollectionRComiesp.FindAll(rcomiesp.FilterRComiespByTipoRegistroDetalhes);
            //
            for (int i = 0; i < rcomiespCollectionAux.CollectionRComiesp.Count; i++) {
                rcomiesp = rcomiespCollectionAux.CollectionRComiesp[i];
                //
                string codigoMercadoria = rcomiesp.CodigoMercadoria;
                string serie = rcomiesp.Serie;

                int codigoBMFAgente = rcomiesp.CodigoAgenteCorretora.Value;
                int codigoBMFCliente = rcomiesp.CodigoCliente.Value;

                CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBMF(codigoBMFAgente, codigoBMFCliente);
                if (idClienteExiste) {
                    int idClienteAux = codigoClienteAgente.IdCliente.Value;

                    #region Copia informações para OrdemBMF

                    // Lancamento de AtivoBMFNaoCadastradoException                                       
                    #region Excecao de AtivoBMFNaoCadastradoException
                    AtivoBMF ativoBMF = new AtivoBMF();
                    if (!ativoBMF.LoadByPrimaryKey(codigoMercadoria, serie)) {
                        throw new AtivoBMFNaoCadastradoException("Processando Arquivo RComiesp: AtivoBMF não cadastrado: " + codigoMercadoria + serie);
                    }
                    #endregion

                    // Preenche OrdemBMF
                    #region Preenche OrdemBMF
                    OrdemBMF ordemBMF = new OrdemBMF();
                    ordemBMF.IdCliente = idClienteAux;
                    ordemBMF.IdAgenteCorretora = idAgenteCorretora;
                    ordemBMF.IdAgenteLiquidacao = idAgenteCorretora;
                    ordemBMF.CdAtivoBMF = codigoMercadoria;
                    ordemBMF.Serie = serie;
                    ordemBMF.TipoMercado = (byte) rcomiesp.TipoMercado;
                    ordemBMF.TipoOrdem = rcomiesp.TipoOperacao;
                    ordemBMF.Data = data;
                    ordemBMF.Pu = rcomiesp.Preco;
                    //
                    decimal peso = ativoBMF.Peso.Value;
                    decimal valor = rcomiesp.Quantidade.Value * rcomiesp.Preco.Value * peso;
                    //
                    ordemBMF.Valor = valor;
                    ordemBMF.Quantidade = rcomiesp.Quantidade;
                    ordemBMF.Origem = (byte) OrigemOrdemBMF.Primaria;
                    ordemBMF.Fonte = (byte)FonteOrdemBMF.ArquivoRCOMIESP;
                    ordemBMF.NumeroNegocioBMF = rcomiesp.NumeroNegocio;
                    ordemBMF.CalculaDespesas = "S";
                    ordemBMF.IdMoeda = (int)ListaMoedaFixo.Real;
                    #endregion
                    //
                    ordemBMFCollection.AttachEntity(ordemBMF);
                    //             
                    #endregion
                }
            }

            ordemBMFCollection.Save();
        }

        /// <summary>
        /// Carrega as ordens de BMF do dia a partir do arquivo RComiesp.
        /// <param name="rcomiespCollection"></param>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        /// 
        /// <exception cref="Financial.BMF.Exceptions.AtivoBMFNaoCadastradoException">
        /// throws AtivoBMFNaoCadastradoException se o codigo do Ativo não estiver cadastrado na Base
        /// </exception>
        public void CarregaOrdemRComiesp(RComiespCollection rcomiespCollection, DateTime data, int idCliente) 
        {
            #region GetIdAgente do Arquivo RComiesp
            // 2 Linha;
            int codigoBMFAgenteCorretora = rcomiespCollection.CollectionRComiesp[1].CodigoAgenteCorretora.Value;
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            agenteMercado.BuscaIdAgenteMercadoBMF(codigoBMFAgenteCorretora);
            int idAgenteCorretora = agenteMercado.IdAgente.Value;
            #endregion

            RComiesp rcomiesp = new RComiesp();
            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
            OrdemBMFCollection ordemBMFCollectionAux = new OrdemBMFCollection();

            // Deleta as ordens de acordo com idCliente, idAgenteCorretora, data, e Fonte = ARQUIVORComiesp
            ordemBMFCollectionAux.DeletaOrdemBMFRComiesp(idCliente, idAgenteCorretora, data);
            //            
            // Filtro pelo TipoRegistro = Detalhes                        
            RComiespCollection rcomiespCollectionAux = new RComiespCollection();
            rcomiespCollectionAux.CollectionRComiesp = rcomiespCollection.CollectionRComiesp.FindAll(rcomiesp.FilterRComiespByTipoRegistroDetalhes);
            //
            for (int i = 0; i < rcomiespCollectionAux.CollectionRComiesp.Count; i++) {
                rcomiesp = rcomiespCollectionAux.CollectionRComiesp[i];
                //
                string codigoMercadoria = rcomiesp.CodigoMercadoria;
                string serie = rcomiesp.Serie;

                int codigoBMFAgente = rcomiesp.CodigoAgenteCorretora.Value;
                int codigoBMFCliente = rcomiesp.CodigoCliente.Value;

                CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBMF(codigoBMFAgente, codigoBMFCliente);
                if (idClienteExiste) {
                    int idClienteAux = codigoClienteAgente.IdCliente.Value;

                    #region Copia informações para OrdemBMF

                    // Lancamento de AtivoBMFNaoCadastradoException                                       
                    #region Excecao de AtivoBMFNaoCadastradoException
                    AtivoBMF ativoBMF = new AtivoBMF();
                    if (!ativoBMF.LoadByPrimaryKey(codigoMercadoria, serie)) {
                        throw new AtivoBMFNaoCadastradoException("Processando Arquivo RComiesp: AtivoBMF não cadastrado: " + codigoMercadoria + serie);
                    }
                    #endregion

                    // Preenche OrdemBMF
                    #region Preenche OrdemBMF
                    OrdemBMF ordemBMF = new OrdemBMF();
                    ordemBMF.IdCliente = idClienteAux;
                    ordemBMF.IdAgenteCorretora = idAgenteCorretora;
                    ordemBMF.IdAgenteLiquidacao = idAgenteCorretora;
                    ordemBMF.CdAtivoBMF = codigoMercadoria;
                    ordemBMF.Serie = serie;
                    ordemBMF.TipoMercado = (byte)rcomiesp.TipoMercado;
                    ordemBMF.TipoOrdem = rcomiesp.TipoOperacao;
                    ordemBMF.Data = data;
                    ordemBMF.Pu = rcomiesp.Preco;
                    //
                    decimal peso = ativoBMF.Peso.Value;
                    decimal valor = rcomiesp.Quantidade.Value * rcomiesp.Preco.Value * peso;
                    //
                    ordemBMF.Valor = valor;
                    ordemBMF.Quantidade = rcomiesp.Quantidade;
                    ordemBMF.Origem = (byte)OrigemOrdemBMF.Primaria;
                    ordemBMF.Fonte = (byte)FonteOrdemBMF.ArquivoRCOMIESP;
                    ordemBMF.NumeroNegocioBMF = rcomiesp.NumeroNegocio;
                    ordemBMF.CalculaDespesas = "S";
                    ordemBMF.IdMoeda = (int)ListaMoedaFixo.Real;
                    #endregion
                    //
                    ordemBMFCollection.AttachEntity(ordemBMF);
                    //             
                    #endregion
                }
            }

            ordemBMFCollection.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Ordem é Venda
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoOrdemVenda() {
            return this.TipoOrdem.Trim().Substring(0,1) == TipoOrdemBMF.Venda;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Ordem é Compra
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoOrdemCompra() {
            return this.TipoOrdem.Trim().Substring(0,1) == TipoOrdemBMF.Compra;
        }

        /// <summary>
        /// Converte as ordens de estrategias de FRC em DDI Ponta Longa e DDI Ponta Curta.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ConverteFRC(int idCliente, DateTime data)
        {
            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            OrdemBMFCollection ordemBMFCollectionInserir = new OrdemBMFCollection();            

            // Collection de Ordens de FRC
            ordemBMFCollection.BuscaOrdemBMF(idCliente, data, "FRC");

            string serieDDIPontaCurta = "";
            if (ordemBMFCollection.Count > 0)
            {
                #region Busca a 1a série em aberto do DDI, para compor a Ponta Curta
                AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();
                ativoBMFCollection.BuscaSeriesVincendas("DDI", TipoMercadoBMF.Futuro, data);

                if (!ativoBMFCollection.HasData)
                {
                    throw new AtivoBMFNaoCadastradoException("Séries não cadastradas do ativo DDI.");
                }

                DateTime dataVencimentoDDI = ((AtivoBMF)ativoBMFCollection[0]).DataVencimento.Value;
                DateTime dataAnterior2Dias = Calendario.SubtraiDiaUtil(dataVencimentoDDI, 2, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                //Se a data estiver 2 dias ou 1 dia antes do vencimento, deve ser usada a série seguinte do DDI.                
                if (data >= dataAnterior2Dias)
                {
                    serieDDIPontaCurta = ((AtivoBMF)ativoBMFCollection[1]).Serie;
                }
                else
                {
                    serieDDIPontaCurta = ((AtivoBMF)ativoBMFCollection[0]).Serie;
                }
                #endregion
            }

            for (int i = 0; i < ordemBMFCollection.Count; i++)
            {
                #region Valores de OrdemBMF
                OrdemBMF ordemBMF = (OrdemBMF)ordemBMFCollection[i];
                int idOrdem = ordemBMF.IdOrdem.Value;
                int quantidade = ordemBMF.Quantidade.Value;
                string tipoOrdem = ordemBMF.TipoOrdem.Trim().Substring(0,1);
                string serie = ordemBMF.Serie;
                decimal pu = ordemBMF.Pu.Value;                
                decimal taxa = ordemBMF.Taxa.Value;
                #endregion

                #region Calcula Dias Corridos da Ponta Curta e Ponta Longa
                //Ponta Curta
                AtivoBMF ativoDDIPontaCurta = new AtivoBMF();
                ativoDDIPontaCurta.LoadByPrimaryKey("DDI", serieDDIPontaCurta);
                DateTime dataVencimentoPontaCurta = ativoDDIPontaCurta.DataVencimento.Value;
                int diasCorridosPontaCurta = Calendario.NumeroDias(data, dataVencimentoPontaCurta);

                //Ponta Longa
                AtivoBMF ativoDDIPontaLonga = new AtivoBMF();
                ativoDDIPontaLonga.LoadByPrimaryKey("DDI", serie);
                DateTime dataVencimentoPontaLonga = ativoDDIPontaLonga.DataVencimento.Value;
                int diasCorridosPontaLonga = Calendario.NumeroDias(data, dataVencimentoPontaLonga);
                #endregion

                #region Busca Cotação de DDI
                CotacaoBMF cotacaoBMF = new CotacaoBMF();
                cotacaoBMF.BuscaCotacaoBMF("DDI", serieDDIPontaCurta, data);

                decimal cotacaoDDI = cotacaoBMF.PUFechamento.Value;
                if (cotacaoDDI == 0)
                {
                    cotacaoDDI = 1;
                }
                #endregion

                //Calcula Taxa, PU e Quantidade da Ponta Curta do DDI.
                decimal taxaPontaCurta = Math.Round(((100000M / cotacaoDDI) - 1) * 36000M / diasCorridosPontaCurta, 2);
                decimal puPontaCurta = Math.Round(100000M / ((taxaPontaCurta / 36000M * diasCorridosPontaCurta) + 1), 2);
                int quantidadePontaCurta = (int)Math.Round(quantidade / (1M + (taxa * (diasCorridosPontaLonga - diasCorridosPontaCurta) / 36000M)), 0M);

                #region Calcula Valor do Futuro Ponta Curta e Ponta Longa
                decimal peso = ordemBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value; //HIERARQUICO
                decimal valorFuturoPontaCurta = Utilitario.Truncate(quantidadePontaCurta * puPontaCurta * peso, 2);
                decimal valorFuturoPontaLonga = Utilitario.Truncate(quantidade * pu * peso, 2);
                #endregion

                #region Monta a OrdemBMF com a ordem de Futuro DDI Ponta Curta
                OrdemBMF ordemBMFDDICurto = new OrdemBMF();
                ordemBMFDDICurto.IdCliente = idCliente;
                ordemBMFDDICurto.IdAgenteCorretora = ordemBMF.IdAgenteCorretora;
                ordemBMFDDICurto.IdAgenteLiquidacao = ordemBMF.IdAgenteLiquidacao;
                ordemBMFDDICurto.CdAtivoBMF = "DDI";
                ordemBMFDDICurto.Serie = serieDDIPontaCurta;
                ordemBMFDDICurto.IdTrader = ordemBMF.IdTrader;
                ordemBMFDDICurto.TipoMercado = TipoMercadoBMF.Futuro;
                ordemBMFDDICurto.TipoOrdem = tipoOrdem;
                ordemBMFDDICurto.Data = data;
                ordemBMFDDICurto.Pu = puPontaCurta;
                ordemBMFDDICurto.Valor = valorFuturoPontaCurta;
                ordemBMFDDICurto.Quantidade = quantidadePontaCurta;
                ordemBMFDDICurto.Origem = ordemBMF.Origem;
                ordemBMFDDICurto.Fonte = ordemBMF.Fonte;
                ordemBMFDDICurto.PontaEstrategia = TipoEstrategia.FRC.DDI_CURTO;
                ordemBMFDDICurto.IdentificadorEstrategia = idOrdem;
                ordemBMFDDICurto.Taxa = taxaPontaCurta;
                ordemBMFDDICurto.CalculaDespesas = "S";
                ordemBMFDDICurto.PercentualDesconto = ordemBMF.PercentualDesconto;
                ordemBMFDDICurto.IdMoeda = (int)ListaMoedaFixo.Real;
                ordemBMFCollectionInserir.AttachEntity(ordemBMFDDICurto);
                #endregion

                //Inverte Compra/Venda para a Ponta Longa do DDI.
                string tipoOrdemPontaLonga;
                if (tipoOrdem == TipoOrdemBMF.Compra)
                {
                    tipoOrdemPontaLonga = TipoOrdemBMF.Venda;
                }
                else
                {
                    tipoOrdemPontaLonga = TipoOrdemBMF.Compra;
                }

                #region Monta a OrdemBMF com a ordem de Futuro DDI Ponta Longa
                OrdemBMF ordemBMFDDILongo = new OrdemBMF();
                ordemBMFDDILongo.IdCliente = idCliente;
                ordemBMFDDILongo.IdAgenteCorretora = ordemBMF.IdAgenteCorretora;
                ordemBMFDDILongo.IdAgenteLiquidacao = ordemBMF.IdAgenteLiquidacao;
                ordemBMFDDILongo.CdAtivoBMF = "DDI";
                ordemBMFDDILongo.Serie = serie;
                ordemBMFDDILongo.IdTrader = ordemBMF.IdTrader;
                ordemBMFDDILongo.TipoMercado = TipoMercadoBMF.Futuro;
                ordemBMFDDILongo.TipoOrdem = tipoOrdemPontaLonga;
                ordemBMFDDILongo.Data = data;
                ordemBMFDDILongo.Pu = pu;
                ordemBMFDDILongo.Valor = valorFuturoPontaLonga;
                ordemBMFDDILongo.Quantidade = quantidade;
                ordemBMFDDILongo.Origem = ordemBMF.Origem;
                ordemBMFDDILongo.Fonte = ordemBMF.Fonte;
                ordemBMFDDILongo.PontaEstrategia = TipoEstrategia.FRC.DDI_LONGO;
                ordemBMFDDILongo.IdentificadorEstrategia = idOrdem;
                ordemBMFDDILongo.CalculaDespesas = "S";
                ordemBMFDDILongo.PercentualDesconto = ordemBMF.PercentualDesconto;
                ordemBMFDDILongo.IdMoeda = (int)ListaMoedaFixo.Real;
                ordemBMFCollectionInserir.AttachEntity(ordemBMFDDILongo);                
                #endregion

            }

            #region Salva as novas ordens desdobradas e exclui as ordens de estrategia
            ordemBMFCollectionInserir.Save();
            //
            ordemBMFCollection.MarkAllAsDeleted();
            ordemBMFCollection.Save();

            #endregion
        }

        /// <summary>
        /// Converte as ordens de estrategias de VID em DI1 Futuro e IDI OpcaoDisponivel.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ConverteVID(int idCliente, DateTime data)
        {
            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();            
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            OrdemBMFCollection ordemBMFCollectionInserir = new OrdemBMFCollection();            
            
            // Collection de Ordens de VID
            ordemBMFCollection.BuscaOrdemBMF(idCliente, data, "VID");

            decimal valorIDI = 0;
            if (ordemBMFCollection.Count > 0)
            {
                valorIDI = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.IDI, data);
            }

            for (int i = 0; i < ordemBMFCollection.Count; i++)
            {
                #region Valores de OrdemBMF
                OrdemBMF ordemBMF = (OrdemBMF)ordemBMFCollection[i];
                int idOrdem = ordemBMF.IdOrdem.Value;
                int quantidade = ordemBMF.Quantidade.Value;
                string tipoOrdem = ordemBMF.TipoOrdem.Trim().Substring(0,1);
                string serie = ordemBMF.Serie;
                decimal pu = ordemBMF.Pu.Value;
                #endregion

                #region Busca Taxa e Delta da estratégia
                TabelaVolatilidadeEstrategia tabelaVolatilidadeEstrategia = new TabelaVolatilidadeEstrategia();

                if (!tabelaVolatilidadeEstrategia.LoadByPrimaryKey(data, "VID", serie))
                {
                    throw new DeltaNaoCadastradoException("Delta não cadastrado na tabela de Volatilidade para VID " + serie);
                }
                decimal delta = tabelaVolatilidadeEstrategia.Delta.Value;
                decimal taxa = tabelaVolatilidadeEstrategia.Pu.Value;
                #endregion

                //Busca dataVencimento e tipoSerie do ativoEstrategia (VID)
                AtivoBMF ativoEstrategia = new AtivoBMF();
                ativoEstrategia.LoadByPrimaryKey("VID", serie);
                DateTime dataVencimento = ativoEstrategia.DataVencimento.Value;
                string tipoSerie = ativoEstrategia.TipoSerie;
                
                #region VID com Opcao do tipo CALL gera operacao contraria de DI1
                string tipoOrdemFuturo;                
                if (tipoSerie == TipoSerieOpcaoBMF.Compra)
                {
                    if (tipoOrdem == TipoOrdemBMF.Compra)
                    {
                        tipoOrdemFuturo = TipoOrdemBMF.Venda;
                    }
                    else
                    {
                        tipoOrdemFuturo = TipoOrdemBMF.Compra;
                    }
                }
                else
                {
                    tipoOrdemFuturo = tipoOrdem;
                }
                #endregion
                
                //Busca Serie da ponta DI1
                AtivoBMF ativoFuturo = new AtivoBMF();
                string serieFuturo = ativoFuturo.BuscaSerie("DI1", TipoMercadoBMF.Futuro, dataVencimento);                

                #region Calcula pu dada a taxa
                TabelaVencimentosBMF tabelaVencimentosBMF = new TabelaVencimentosBMF();
                if (!tabelaVencimentosBMF.LoadByPrimaryKey(data, "DI1", serieFuturo))
                {
                    throw new TabelaVencimentosBMFNaoCadastradoException("TabelaVencimento não cadastrada para DI1" + serieFuturo + " na data " + data);
                }
                int diasUteis = tabelaVencimentosBMF.DiasUteis.Value;

                decimal fator = CalculoFinanceiro.CalculaFatorPreExponencial(diasUteis, taxa, BaseCalculo.Base252);
                decimal puFuturo = Math.Round(100000M / fator, 2);
                #endregion

                #region Calcula Quantidade do Futuro (DI1), obedecendo a regra de múltiplo de 5 contratos
                int quantidadeFuturo = (int)Math.Round((quantidade * delta * Utilitario.Truncate(valorIDI / puFuturo, 0)), 0);
                int resto = (int) quantidadeFuturo % 5;

                if (resto == 1 || resto == 2)                
                {
                    quantidadeFuturo = quantidadeFuturo - resto;
                }
                else if (resto == 3 || resto == 4)
                {
                    quantidadeFuturo = quantidadeFuturo + (5 - resto);
                }
                #endregion

                #region Calcula Valor do Futuro
                decimal peso = ordemBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value; //HIERARQUICO
                decimal valorFuturo = Utilitario.Truncate(quantidadeFuturo * puFuturo * peso, 2);
                #endregion                                

                #region Monta a OrdemBMF com a ordem de Futuro (DI1)
                OrdemBMF ordemBMFFuturo = new OrdemBMF();
                ordemBMFFuturo.IdCliente = ordemBMF.IdCliente;
                ordemBMFFuturo.IdAgenteCorretora = ordemBMF.IdAgenteCorretora;
                ordemBMFFuturo.IdAgenteLiquidacao = ordemBMF.IdAgenteLiquidacao;
                ordemBMFFuturo.CdAtivoBMF = "DI1";
                ordemBMFFuturo.Serie = serieFuturo;
                ordemBMFFuturo.IdTrader = ordemBMF.IdTrader;
                ordemBMFFuturo.TipoMercado = TipoMercadoBMF.Futuro;
                ordemBMFFuturo.TipoOrdem = tipoOrdemFuturo;
                ordemBMFFuturo.Data = ordemBMF.Data;
                ordemBMFFuturo.Pu = puFuturo;
                ordemBMFFuturo.Valor = valorFuturo;
                ordemBMFFuturo.Quantidade = quantidadeFuturo;
                ordemBMFFuturo.Origem = ordemBMF.Origem;
                ordemBMFFuturo.Fonte = ordemBMF.Fonte;
                ordemBMFFuturo.PontaEstrategia = TipoEstrategia.VID.DI1_FUT;
                ordemBMFFuturo.IdentificadorEstrategia = idOrdem;
                ordemBMFFuturo.CalculaDespesas = "S";
                ordemBMFFuturo.PercentualDesconto = ordemBMF.PercentualDesconto;
                ordemBMFFuturo.IdMoeda = (int)ListaMoedaFixo.Real;
                ordemBMFCollectionInserir.AttachEntity(ordemBMFFuturo);
                #endregion

                #region Monta a OrdemBMF com a ordem de OpcaoDisponivel (IDI)
                OrdemBMF ordemBMFOpcao = new OrdemBMF();
                ordemBMFOpcao.IdCliente = ordemBMF.IdCliente;
                ordemBMFOpcao.IdAgenteCorretora = ordemBMF.IdAgenteCorretora;
                ordemBMFOpcao.IdAgenteLiquidacao = ordemBMF.IdAgenteLiquidacao;
                ordemBMFOpcao.CdAtivoBMF = "IDI";
                ordemBMFOpcao.Serie = ordemBMF.Serie;
                ordemBMFOpcao.IdTrader = ordemBMF.IdTrader;
                ordemBMFOpcao.TipoMercado = TipoMercadoBMF.OpcaoDisponivel;
                ordemBMFOpcao.TipoOrdem = ordemBMF.TipoOrdem.Trim().Substring(0,1);
                ordemBMFOpcao.Data = ordemBMF.Data;
                ordemBMFOpcao.Pu = ordemBMF.Pu;
                ordemBMFOpcao.Valor = ordemBMF.Valor;
                ordemBMFOpcao.Quantidade = ordemBMF.Quantidade;
                ordemBMFOpcao.Origem = ordemBMF.Origem;
                ordemBMFOpcao.Fonte = ordemBMF.Fonte;
                ordemBMFOpcao.PontaEstrategia = TipoEstrategia.VID.IDI_OPC;
                ordemBMFOpcao.IdentificadorEstrategia = idOrdem;
                ordemBMFOpcao.CalculaDespesas = "S";
                ordemBMFOpcao.PercentualDesconto = ordemBMF.PercentualDesconto;
                ordemBMFOpcao.IdMoeda = (int)ListaMoedaFixo.Real;
                ordemBMFCollectionInserir.AttachEntity(ordemBMFOpcao);
                #endregion                               
                
            }

            #region Salva as novas ordens desdobradas e exclui as ordens de estrategia
            ordemBMFCollectionInserir.Save();
            //
            ordemBMFCollection.MarkAllAsDeleted();
            ordemBMFCollection.Save();

            #endregion
        }

        /// <summary>
        /// Converte as ordens de estrategias de VTF (VF1, VF2, VF3) em DI1 Futuro Ponta Longa e Curta e DI1 OpcaoFuturo.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ConverteVTF(int idCliente, DateTime data)
        {
            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            OrdemBMFCollection ordemBMFCollectionInserir = new OrdemBMFCollection();

            // Collection de Ordens de VF1, VF2, VF3
            ordemBMFCollection.BuscaOrdemBMF(idCliente, data, "VF1', 'VF2', 'VF3");

            for (int i = 0; i < ordemBMFCollection.Count; i++)
            {
                #region Valores de OrdemBMF
                OrdemBMF ordemBMF = (OrdemBMF)ordemBMFCollection[i];
                string cdAtivoBMF = ordemBMF.CdAtivoBMF;
                int idOrdem = ordemBMF.IdOrdem.Value;
                int quantidade = ordemBMF.Quantidade.Value;
                string tipoOrdem = ordemBMF.TipoOrdem.Trim().Substring(0,1);
                string serie = ordemBMF.Serie;
                decimal pu = ordemBMF.Pu.Value;
                decimal taxa = ordemBMF.Taxa.Value;
                #endregion

                //Busca dataVencimento e tipoSerie do ativoEstrategia (VTF)
                AtivoBMF ativoEstrategia = new AtivoBMF();
                ativoEstrategia.LoadByPrimaryKey(cdAtivoBMF, serie);
                DateTime dataVencimentoOriginal = ativoEstrategia.DataVencimento.Value;
                DateTime dataVencimento = ativoEstrategia.DataVencimento.Value;                
                string tipoSerie = ativoEstrategia.TipoSerie;

                switch (cdAtivoBMF)
                {
                    case "VF1":
                        dataVencimento.Date.AddMonths(3);
                        break;
                    case "VF2":
                        dataVencimento.Date.AddMonths(6);
                        break;
                    case "VF3":
                        dataVencimento.Date.AddMonths(12);
                        break;
                }

                DateTime dataVencimentoInicio = dataVencimento.Date.AddMonths(-5);
                DateTime dataVencimentoFim = dataVencimento.Date.AddMonths(5);

                #region Busca Serie e DiasUteis da Ponta Longa
                AtivoBMF ativoBMFLonga = new AtivoBMF();
                if (!ativoBMFLonga.BuscaAtivoBMF("DI1", TipoMercadoBMF.Futuro, dataVencimentoInicio, dataVencimentoFim))
                {
                    throw new AtivoBMFNaoCadastradoException("Ativo Futuro DI1 com data de vencimento entre " + dataVencimentoInicio + " e " + dataVencimentoFim + " não cadastrado.");
                }
                string serieLonga = ativoBMFLonga.Serie;

                TabelaVencimentosBMF tabelaVencimentosBMF = new TabelaVencimentosBMF();
                if (!tabelaVencimentosBMF.LoadByPrimaryKey(data, "DI1", serieLonga))
                {
                    throw new TabelaVencimentosBMFNaoCadastradoException("TabelaVencimento não cadastrada na data " + data + " para DI1" + serieLonga);
                }
                int diasUteisLonga = tabelaVencimentosBMF.DiasUteis.Value;
                #endregion

                #region Busca Serie e DiasUteis da Ponta Curta
                AtivoBMF ativoBMFCurta = new AtivoBMF();
                ativoBMFCurta.BuscaSerie("DI1", TipoMercadoBMF.Futuro, dataVencimentoOriginal);
                string serieCurta = ativoBMFCurta.Serie;

                tabelaVencimentosBMF = new TabelaVencimentosBMF();
                if (!tabelaVencimentosBMF.LoadByPrimaryKey(data, "DI1", serieCurta))
                {
                    throw new TabelaVencimentosBMFNaoCadastradoException("TabelaVencimento não cadastrada na data " + data + " para DI1" + serieCurta);
                }
                int diasUteisCurta = tabelaVencimentosBMF.DiasUteis.Value;
                #endregion

                #region Busca cotações de DI1 de D-1 para formar os PUs da ponta longa e ponta curta
                DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                
                CotacaoBMF cotacaoBMF = new CotacaoBMF();
                cotacaoBMF.BuscaCotacaoBMF("DI1", serieLonga, dataAnterior);
                decimal puPontaLonga = cotacaoBMF.PUFechamento.Value;

                cotacaoBMF = new CotacaoBMF();
                cotacaoBMF.BuscaCotacaoBMF("DI1", serieCurta, dataAnterior);
                decimal puPontaCurta = cotacaoBMF.PUFechamento.Value;
                #endregion

                #region VTF com Opcao do tipo CALL gera operacao contraria de DI1 (Somente Ponta Longa)
                string tipoOrdemFuturoLonga;
                if (tipoSerie == TipoSerieOpcaoBMF.Compra)
                {
                    if (tipoOrdem == TipoOrdemBMF.Compra)
                    {
                        tipoOrdemFuturoLonga = TipoOrdemBMF.Venda;
                    }
                    else
                    {
                        tipoOrdemFuturoLonga = TipoOrdemBMF.Compra;
                    }
                }
                else
                {
                    tipoOrdemFuturoLonga = tipoOrdem;
                }
                #endregion

                #region Calcula Quantidade do Futuro (DI1) Ponta Longa, obedecendo a regra de múltiplo de 5 contratos
                TabelaVolatilidadeEstrategia tabelaVolatilidadeEstrategia = new TabelaVolatilidadeEstrategia();

                if (!tabelaVolatilidadeEstrategia.LoadByPrimaryKey(data, cdAtivoBMF, serie))
                {
                    throw new DeltaNaoCadastradoException("Delta não cadastrado na tabela de Volatilidade para cdAtivoBMF" + serie);
                }
                decimal delta = tabelaVolatilidadeEstrategia.Delta.Value;
                decimal taxaLonga = tabelaVolatilidadeEstrategia.PULongo.Value;
                decimal taxaCurta = tabelaVolatilidadeEstrategia.Pu.Value;

                int quantidadeFuturoLonga = (int)Math.Round(quantidade * delta, 0);
                int resto = (int)quantidadeFuturoLonga % 5;

                if (resto == 1 || resto == 2)
                {
                    quantidadeFuturoLonga = quantidadeFuturoLonga - resto;
                }
                else if (resto == 3 || resto == 4)
                {
                    quantidadeFuturoLonga = quantidadeFuturoLonga + (5 - resto);
                }
                #endregion

                #region Calcula Quantidade do Futuro (DI1) Ponta Curta, obedecendo a regra de múltiplo de 5 contratos
                decimal fatorPontaLonga = (decimal)Math.Pow((double)(1M + taxaLonga / 100M), (double)diasUteisLonga);
                decimal fatorPontaCurta = (decimal)Math.Pow((double)(1M + taxaCurta / 100M), (double)diasUteisCurta);
                decimal fatorQuantidade = fatorPontaLonga / fatorPontaCurta;
                int quantidadeFuturoCurta = (int)Math.Round(quantidade / fatorQuantidade, 0); 

                resto = (int)quantidadeFuturoCurta % 5;

                if (resto == 1 || resto == 2)
                {
                    quantidadeFuturoCurta = quantidadeFuturoCurta - resto;
                }
                else if (resto == 3 || resto == 4)
                {
                    quantidadeFuturoCurta = quantidadeFuturoCurta + (5 - resto);
                }
                #endregion
                
                #region Calcula Valores dos Futuros
                decimal peso = ordemBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value; //HIERARQUICO
                decimal valorFuturoLonga = Utilitario.Truncate(quantidadeFuturoLonga * puPontaLonga * peso, 2);
                decimal valorFuturoCurta = Utilitario.Truncate(quantidadeFuturoCurta * puPontaCurta * peso, 2);
                #endregion                                
                
                #region Monta a OrdemBMF com a ordem de Futuro (DI1) Ponta Longa
                OrdemBMF ordemBMFFuturoLonga = new OrdemBMF();
                ordemBMFFuturoLonga.IdCliente = ordemBMF.IdCliente;
                ordemBMFFuturoLonga.IdAgenteCorretora = ordemBMF.IdAgenteCorretora;
                ordemBMFFuturoLonga.IdAgenteLiquidacao = ordemBMF.IdAgenteLiquidacao;
                ordemBMFFuturoLonga.CdAtivoBMF = "DI1";
                ordemBMFFuturoLonga.Serie = serieLonga;
                ordemBMFFuturoLonga.IdTrader = ordemBMF.IdTrader;
                ordemBMFFuturoLonga.TipoMercado = TipoMercadoBMF.Futuro;
                ordemBMFFuturoLonga.TipoOrdem = tipoOrdemFuturoLonga;
                ordemBMFFuturoLonga.Data = ordemBMF.Data;
                ordemBMFFuturoLonga.Pu = puPontaLonga;
                ordemBMFFuturoLonga.Valor = valorFuturoLonga;
                ordemBMFFuturoLonga.Quantidade = quantidadeFuturoLonga;
                ordemBMFFuturoLonga.Origem = ordemBMF.Origem;
                ordemBMFFuturoLonga.Fonte = ordemBMF.Fonte;
                ordemBMFFuturoLonga.PontaEstrategia = TipoEstrategia.VTF.DI1_FUT_LONGO;
                ordemBMFFuturoLonga.IdentificadorEstrategia = idOrdem;
                ordemBMFFuturoLonga.CalculaDespesas = "S";
                ordemBMFFuturoLonga.PercentualDesconto = ordemBMF.PercentualDesconto;
                ordemBMFFuturoLonga.IdMoeda = (int)ListaMoedaFixo.Real;
                ordemBMFCollectionInserir.AttachEntity(ordemBMFFuturoLonga);
                #endregion

                #region Monta a OrdemBMF com a ordem de Futuro (DI1) Ponta Curta
                OrdemBMF ordemBMFFuturoCurta = new OrdemBMF();
                ordemBMFFuturoCurta.IdCliente = ordemBMF.IdCliente;
                ordemBMFFuturoCurta.IdAgenteCorretora = ordemBMF.IdAgenteCorretora;
                ordemBMFFuturoCurta.IdAgenteLiquidacao = ordemBMF.IdAgenteLiquidacao;
                ordemBMFFuturoCurta.CdAtivoBMF = "DI1";
                ordemBMFFuturoCurta.Serie = serieCurta;
                ordemBMFFuturoCurta.IdTrader = ordemBMF.IdTrader;
                ordemBMFFuturoCurta.TipoMercado = TipoMercadoBMF.Futuro;
                ordemBMFFuturoCurta.TipoOrdem = tipoOrdem;
                ordemBMFFuturoCurta.Data = ordemBMF.Data;
                ordemBMFFuturoCurta.Pu = puPontaCurta;
                ordemBMFFuturoCurta.Valor = valorFuturoCurta;
                ordemBMFFuturoCurta.Quantidade = quantidadeFuturoCurta;
                ordemBMFFuturoCurta.Origem = ordemBMF.Origem;
                ordemBMFFuturoCurta.Fonte = ordemBMF.Fonte;
                ordemBMFFuturoCurta.PontaEstrategia = TipoEstrategia.VTF.DI1_FUT_CURTO;
                ordemBMFFuturoCurta.IdentificadorEstrategia = idOrdem;
                ordemBMFFuturoCurta.CalculaDespesas = "S";
                ordemBMFFuturoCurta.PercentualDesconto = ordemBMF.PercentualDesconto;
                ordemBMFFuturoCurta.IdMoeda = (int)ListaMoedaFixo.Real;
                ordemBMFCollectionInserir.AttachEntity(ordemBMFFuturoCurta);
                #endregion

                #region Monta a OrdemBMF com a ordem de OpcaoFuturo (DI1)
                OrdemBMF ordemBMFOpcao = new OrdemBMF();
                ordemBMFOpcao.IdCliente = ordemBMF.IdCliente;
                ordemBMFOpcao.IdAgenteCorretora = ordemBMF.IdAgenteCorretora;
                ordemBMFOpcao.IdAgenteLiquidacao = ordemBMF.IdAgenteLiquidacao;
                ordemBMFOpcao.CdAtivoBMF = "DI1";
                ordemBMFOpcao.Serie = ordemBMF.Serie;
                ordemBMFOpcao.IdTrader = ordemBMF.IdTrader;
                ordemBMFOpcao.TipoMercado = TipoMercadoBMF.OpcaoFuturo;
                ordemBMFOpcao.TipoOrdem = ordemBMF.TipoOrdem.Trim().Substring(0,1);
                ordemBMFOpcao.Data = ordemBMF.Data;
                ordemBMFOpcao.Pu = ordemBMF.Pu;
                ordemBMFOpcao.Valor = ordemBMF.Valor;
                ordemBMFOpcao.Quantidade = ordemBMF.Quantidade;
                ordemBMFOpcao.Origem = ordemBMF.Origem;
                ordemBMFOpcao.Fonte = ordemBMF.Fonte;
                ordemBMFOpcao.PontaEstrategia = TipoEstrategia.VTF.DI1_OPC;
                ordemBMFOpcao.IdentificadorEstrategia = idOrdem;
                ordemBMFOpcao.CalculaDespesas = "S";
                ordemBMFOpcao.PercentualDesconto = ordemBMF.PercentualDesconto;
                ordemBMFOpcao.IdMoeda = (int)ListaMoedaFixo.Real;
                ordemBMFCollectionInserir.AttachEntity(ordemBMFOpcao);
                #endregion

            }

            #region Salva as novas ordens desdobradas e exclui as ordens de estrategia
            ordemBMFCollectionInserir.Save();
            //
            ordemBMFCollection.MarkAllAsDeleted();
            ordemBMFCollection.Save();

            #endregion
        }

        /// <summary>
        /// Converte as ordens de estrategias de VOI em IND Futuro e IND OpcaoFuturo.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ConverteVOI(int idCliente, DateTime data)
        {
            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
            OrdemBMFCollection ordemBMFCollectionInserir = new OrdemBMFCollection();            

            // Collection de Ordens de VOI
            ordemBMFCollection.BuscaOrdemBMF(idCliente, data, "VOI");

            for (int i = 0; i < ordemBMFCollection.Count; i++)
            {
                #region Valores de OrdemBMF
                OrdemBMF ordemBMF = (OrdemBMF)ordemBMFCollection[i];
                int idOrdem = ordemBMF.IdOrdem.Value;
                int quantidade = ordemBMF.Quantidade.Value;
                string tipoOrdem = ordemBMF.TipoOrdem.Trim().Substring(0,1);
                string serie = ordemBMF.Serie;
                decimal pu = ordemBMF.Pu.Value;
                #endregion

                //Busca dataVencimento e tipoSerie do ativoEstrategia (VOI)
                AtivoBMF ativoEstrategia = new AtivoBMF();
                ativoEstrategia.LoadByPrimaryKey("VOI", serie);
                DateTime dataVencimento = ativoEstrategia.DataVencimento.Value;
                string tipoSerie = ativoEstrategia.TipoSerie;

                #region VOI com Opcao do tipo CALL gera operacao contraria de IND
                string tipoOrdemFuturo;
                if (tipoSerie == TipoSerieOpcaoBMF.Compra)
                {
                    if (tipoOrdem == TipoOrdemBMF.Compra)
                    {
                        tipoOrdemFuturo = TipoOrdemBMF.Venda;
                    }
                    else
                    {
                        tipoOrdemFuturo = TipoOrdemBMF.Compra;
                    }
                }
                else
                {
                    tipoOrdemFuturo = tipoOrdem;
                }
                #endregion

                //Busca Serie da ponta IND Futuro
                AtivoBMF ativoFuturo = new AtivoBMF();
                string serieFuturo = ativoFuturo.BuscaSerie("IND", TipoMercadoBMF.Futuro, dataVencimento);
                                
                #region Calcula Quantidade do Futuro (IND), obedecendo a regra de múltiplo de 5 contratos
                TabelaVolatilidadeEstrategia tabelaVolatilidadeEstrategia = new TabelaVolatilidadeEstrategia();

                if (!tabelaVolatilidadeEstrategia.LoadByPrimaryKey(data, "VOI", serie))
                {
                    throw new DeltaNaoCadastradoException("Delta não cadastrado na tabela de Volatilidade para VOI" + serie);
                }
                decimal delta = tabelaVolatilidadeEstrategia.Delta.Value;

                int quantidadeFuturo = (int) Math.Round(quantidade * delta, 0);
                int resto = (int)quantidadeFuturo % 5;

                if (resto == 1 || resto == 2)
                {
                    quantidadeFuturo = quantidadeFuturo - resto;
                }
                else if (resto == 3 || resto == 4)
                {
                    quantidadeFuturo = quantidadeFuturo + (5 - resto);
                }
                #endregion

                //O PU do futuro também vem na tabela de Volatilidade
                decimal puFuturo = tabelaVolatilidadeEstrategia.Pu.Value;
                
                #region Calcula Valor do Futuro
                decimal peso = ordemBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value; //HIERARQUICO
                decimal valorFuturo = Utilitario.Truncate(quantidadeFuturo * puFuturo * peso, 2);
                #endregion

                #region Monta a OrdemBMF com a ordem de Futuro (IND)
                OrdemBMF ordemBMFFuturo = new OrdemBMF();
                ordemBMFFuturo.IdCliente = ordemBMF.IdCliente;
                ordemBMFFuturo.IdAgenteCorretora = ordemBMF.IdAgenteCorretora;
                ordemBMFFuturo.IdAgenteLiquidacao = ordemBMF.IdAgenteLiquidacao;
                ordemBMFFuturo.CdAtivoBMF = "IND";
                ordemBMFFuturo.Serie = serieFuturo;
                ordemBMFFuturo.IdTrader = ordemBMF.IdTrader;
                ordemBMFFuturo.TipoMercado = TipoMercadoBMF.Futuro;
                ordemBMFFuturo.TipoOrdem = tipoOrdemFuturo;
                ordemBMFFuturo.Data = ordemBMF.Data;
                ordemBMFFuturo.Pu = puFuturo;
                ordemBMFFuturo.Valor = valorFuturo;
                ordemBMFFuturo.Quantidade = quantidadeFuturo;
                ordemBMFFuturo.Origem = ordemBMF.Origem;
                ordemBMFFuturo.Fonte = ordemBMF.Fonte;
                ordemBMFFuturo.PontaEstrategia = TipoEstrategia.VOI.IND_FUT;
                ordemBMFFuturo.IdentificadorEstrategia = idOrdem;
                ordemBMFFuturo.CalculaDespesas = "N";
                ordemBMFFuturo.PercentualDesconto = ordemBMF.PercentualDesconto;
                ordemBMFFuturo.IdMoeda = (int)ListaMoedaFixo.Real;
                ordemBMFCollectionInserir.AttachEntity(ordemBMFFuturo);
                #endregion

                #region Monta a OrdemBMF com a ordem de OpcaoFuturo (IND)
                OrdemBMF ordemBMFOpcao = new OrdemBMF();
                ordemBMFOpcao.IdCliente = ordemBMF.IdCliente;
                ordemBMFOpcao.IdAgenteCorretora = ordemBMF.IdAgenteCorretora;
                ordemBMFOpcao.IdAgenteLiquidacao = ordemBMF.IdAgenteLiquidacao;
                ordemBMFOpcao.CdAtivoBMF = "IND";
                ordemBMFOpcao.Serie = ordemBMF.Serie;
                ordemBMFOpcao.IdTrader = ordemBMF.IdTrader;
                ordemBMFOpcao.TipoMercado = TipoMercadoBMF.OpcaoFuturo;
                ordemBMFOpcao.TipoOrdem = ordemBMF.TipoOrdem.Trim().Substring(0,1);
                ordemBMFOpcao.Data = ordemBMF.Data;
                ordemBMFOpcao.Pu = ordemBMF.Pu;
                ordemBMFOpcao.Valor = ordemBMF.Valor;
                ordemBMFOpcao.Quantidade = ordemBMF.Quantidade;
                ordemBMFOpcao.Origem = ordemBMF.Origem;
                ordemBMFOpcao.Fonte = ordemBMF.Fonte;
                ordemBMFOpcao.PontaEstrategia = TipoEstrategia.VOI.IND_OPC;
                ordemBMFOpcao.IdentificadorEstrategia = idOrdem;
                ordemBMFOpcao.CalculaDespesas = "S";
                ordemBMFOpcao.PercentualDesconto = ordemBMF.PercentualDesconto;
                ordemBMFOpcao.IdMoeda = (int)ListaMoedaFixo.Real;
                ordemBMFCollectionInserir.AttachEntity(ordemBMFOpcao);
                #endregion                               
                
            }

            #region Salva as novas ordens desdobradas e exclui as ordens de estrategia
            ordemBMFCollectionInserir.Save();
            //
            ordemBMFCollection.MarkAllAsDeleted();
            ordemBMFCollection.Save();

            #endregion
        }

        /// <summary>
        /// Converte as ordens de estrategias de VCA em DOL Futuro e DLA OpcaoDisponivel.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ConverteVCA(int idCliente, DateTime data)
        {
            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
            OrdemBMFCollection ordemBMFCollectionInserir = new OrdemBMFCollection();
                    
            // Collection de Ordens de VCA
            ordemBMFCollection.BuscaOrdemBMF(idCliente, data, "VCA");

            for (int i = 0; i < ordemBMFCollection.Count; i++)
            {
                #region Valores de OrdemBMF
                OrdemBMF ordemBMF = (OrdemBMF)ordemBMFCollection[i];
                int idOrdem = ordemBMF.IdOrdem.Value;
                int quantidade = ordemBMF.Quantidade.Value;
                string tipoOrdem = ordemBMF.TipoOrdem.Trim().Substring(0,1);
                string serie = ordemBMF.Serie;
                decimal pu = ordemBMF.Pu.Value;                
                #endregion

                //Busca dataVencimento e tipoSerie do ativoEstrategia (VCA)
                AtivoBMF ativoEstrategia = new AtivoBMF();
                ativoEstrategia.LoadByPrimaryKey("VCA", serie);
                DateTime dataVencimento = ativoEstrategia.DataVencimento.Value;
                string tipoSerie = ativoEstrategia.TipoSerie;

                #region VCA com Opcao do tipo CALL gera operacao contraria de DOL
                string tipoOrdemFuturo;
                if (tipoSerie == TipoSerieOpcaoBMF.Compra)
                {
                    if (tipoOrdem == TipoOrdemBMF.Compra)
                    {
                        tipoOrdemFuturo = TipoOrdemBMF.Venda;
                    }
                    else
                    {
                        tipoOrdemFuturo = TipoOrdemBMF.Compra;
                    }
                }
                else
                {
                    tipoOrdemFuturo = tipoOrdem;
                }
                #endregion

                //Busca Serie da ponta DOL Futuro
                AtivoBMF ativoFuturo = new AtivoBMF();
                string serieFuturo = ativoFuturo.BuscaSerie("DOL", TipoMercadoBMF.Futuro, dataVencimento);

                #region Calcula Quantidade do Futuro (DOL), obedecendo a regra de múltiplo de 5 contratos
                TabelaVolatilidadeEstrategia tabelaVolatilidadeEstrategia = new TabelaVolatilidadeEstrategia();

                if (!tabelaVolatilidadeEstrategia.LoadByPrimaryKey(data, "VCA", serie))
                {
                    throw new DeltaNaoCadastradoException("Delta não cadastrado na tabela de Volatilidade para VCA" + serie);
                }
                decimal delta = tabelaVolatilidadeEstrategia.Delta.Value;

                int quantidadeFuturo = (int)Math.Round(quantidade * delta, 0);
                int resto = (int)quantidadeFuturo % 5;

                if (resto == 1 || resto == 2)
                {
                    quantidadeFuturo = quantidadeFuturo - resto;
                }
                else if (resto == 3 || resto == 4)
                {
                    quantidadeFuturo = quantidadeFuturo + (5 - resto);
                }
                #endregion

                //O PU do futuro também vem na tabela de Volatilidade
                decimal puFuturo = tabelaVolatilidadeEstrategia.Pu.Value;
                
                #region Calcula Valor do Futuro
                decimal peso = ordemBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value; //HIERARQUICO
                decimal valorFuturo = Utilitario.Truncate(quantidadeFuturo * puFuturo * peso, 2);
                #endregion

                #region Monta a OrdemBMF com a ordem de Futuro (DOL)
                OrdemBMF ordemBMFFuturo = new OrdemBMF();
                ordemBMFFuturo.IdCliente = ordemBMF.IdCliente;
                ordemBMFFuturo.IdAgenteCorretora = ordemBMF.IdAgenteCorretora;
                ordemBMFFuturo.IdAgenteLiquidacao = ordemBMF.IdAgenteLiquidacao;
                ordemBMFFuturo.CdAtivoBMF = "DOL";
                ordemBMFFuturo.Serie = serieFuturo;
                ordemBMFFuturo.IdTrader = ordemBMF.IdTrader;
                ordemBMFFuturo.TipoMercado = TipoMercadoBMF.Futuro;
                ordemBMFFuturo.TipoOrdem = tipoOrdemFuturo;
                ordemBMFFuturo.Data = ordemBMF.Data;
                ordemBMFFuturo.Pu = puFuturo;
                ordemBMFFuturo.Valor = valorFuturo;
                ordemBMFFuturo.Quantidade = quantidadeFuturo;
                ordemBMFFuturo.Origem = ordemBMF.Origem;
                ordemBMFFuturo.Fonte = ordemBMF.Fonte;
                ordemBMFFuturo.PontaEstrategia = TipoEstrategia.VCA.DOL_FUT;
                ordemBMFFuturo.IdentificadorEstrategia = idOrdem;
                ordemBMFFuturo.CalculaDespesas = "S";
                ordemBMFFuturo.PercentualDesconto = ordemBMF.PercentualDesconto;
                ordemBMFFuturo.IdMoeda = (int)ListaMoedaFixo.Real;
                ordemBMFCollectionInserir.AttachEntity(ordemBMFFuturo);
                #endregion

                #region Monta a OrdemBMF com a ordem de OpcaoFuturo (DLA)
                OrdemBMF ordemBMFOpcao = new OrdemBMF();
                ordemBMFOpcao.IdCliente = ordemBMF.IdCliente;
                ordemBMFOpcao.IdAgenteCorretora = ordemBMF.IdAgenteCorretora;
                ordemBMFOpcao.IdAgenteLiquidacao = ordemBMF.IdAgenteLiquidacao;
                ordemBMFOpcao.CdAtivoBMF = "DLA";
                ordemBMFOpcao.Serie = ordemBMF.Serie;
                ordemBMFOpcao.IdTrader = ordemBMF.IdTrader;
                ordemBMFOpcao.TipoMercado = TipoMercadoBMF.OpcaoDisponivel;
                ordemBMFOpcao.TipoOrdem = ordemBMF.TipoOrdem.Trim().Substring(0,1);
                ordemBMFOpcao.Data = ordemBMF.Data;
                ordemBMFOpcao.Pu = ordemBMF.Pu;
                ordemBMFOpcao.Valor = ordemBMF.Valor;
                ordemBMFOpcao.Quantidade = ordemBMF.Quantidade;
                ordemBMFOpcao.Origem = ordemBMF.Origem;
                ordemBMFOpcao.Fonte = ordemBMF.Fonte;
                ordemBMFOpcao.PontaEstrategia = TipoEstrategia.VCA.DLA_OPC;
                ordemBMFOpcao.IdentificadorEstrategia = idOrdem;
                ordemBMFOpcao.CalculaDespesas = "S";
                ordemBMFOpcao.PercentualDesconto = ordemBMF.PercentualDesconto;
                ordemBMFOpcao.IdMoeda = (int)ListaMoedaFixo.Real;
                ordemBMFCollectionInserir.AttachEntity(ordemBMFOpcao);
                #endregion
               
            }

            #region Salva as novas ordens desdobradas e exclui as ordens de estrategia
            ordemBMFCollectionInserir.Save();
            //
            ordemBMFCollection.MarkAllAsDeleted();
            ordemBMFCollection.Save();

            #endregion
        }

        /// <summary>
        /// Converte as ordens de estrategias de VTC em DOL Futuro e DOL OpcaoDisponivel.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ConverteVTC(int idCliente, DateTime data)
        {
            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
            OrdemBMFCollection ordemBMFCollectionInserir = new OrdemBMFCollection();
            
            // Collection de Ordens de VTC
            ordemBMFCollection.BuscaOrdemBMF(idCliente, data, "VTC");

            for (int i = 0; i < ordemBMFCollection.Count; i++)
            {
                #region Valores de OrdemBMF
                OrdemBMF ordemBMF = (OrdemBMF)ordemBMFCollection[i];
                int idOrdem = ordemBMF.IdOrdem.Value;
                int quantidade = ordemBMF.Quantidade.Value;
                string tipoOrdem = ordemBMF.TipoOrdem.Trim().Substring(0,1);
                string serie = ordemBMF.Serie;
                decimal pu = ordemBMF.Pu.Value;
                #endregion

                //Busca dataVencimento e tipoSerie do ativoEstrategia (VTC)
                AtivoBMF ativoEstrategia = new AtivoBMF();
                ativoEstrategia.LoadByPrimaryKey("VTC", serie);
                DateTime dataVencimento = ativoEstrategia.DataVencimento.Value;
                string tipoSerie = ativoEstrategia.TipoSerie;

                #region VTC com Opcao do tipo CALL gera operacao contraria de DOL
                string tipoOrdemFuturo;
                if (tipoSerie == TipoSerieOpcaoBMF.Compra)
                {
                    if (tipoOrdem == TipoOrdemBMF.Compra)
                    {
                        tipoOrdemFuturo = TipoOrdemBMF.Venda;
                    }
                    else
                    {
                        tipoOrdemFuturo = TipoOrdemBMF.Compra;
                    }
                }
                else
                {
                    tipoOrdemFuturo = tipoOrdem;
                }
                #endregion

                //Busca Serie da ponta DOL Futuro
                AtivoBMF ativoFuturo = new AtivoBMF();
                string serieFuturo = ativoFuturo.BuscaSerie("DOL", TipoMercadoBMF.Futuro, dataVencimento);

                #region Calcula Quantidade do Futuro (DOL), obedecendo a regra de múltiplo de 5 contratos
                TabelaVolatilidadeEstrategia tabelaVolatilidadeEstrategia = new TabelaVolatilidadeEstrategia();

                if (!tabelaVolatilidadeEstrategia.LoadByPrimaryKey(data, "VTC", serie))
                {
                    throw new DeltaNaoCadastradoException("Delta não cadastrado na tabela de Volatilidade para VTC" + serie);
                }
                decimal delta = tabelaVolatilidadeEstrategia.Delta.Value;

                int quantidadeFuturo = (int)Math.Round(quantidade * delta, 0);
                int resto = (int)quantidadeFuturo % 5;

                if (resto == 1 || resto == 2)
                {
                    quantidadeFuturo = quantidadeFuturo - resto;
                }
                else if (resto == 3 || resto == 4)
                {
                    quantidadeFuturo = quantidadeFuturo + (5 - resto);
                }
                #endregion

                //O PU do futuro também vem na tabela de Volatilidade
                decimal puFuturo = tabelaVolatilidadeEstrategia.Pu.Value;

                #region Calcula Valor do Futuro
                decimal peso = ordemBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value; //HIERARQUICO
                decimal valorFuturo = Utilitario.Truncate(quantidadeFuturo * puFuturo * peso, 2);
                #endregion

                #region Monta a OrdemBMF com a ordem de Futuro (DOL)
                OrdemBMF ordemBMFFuturo = new OrdemBMF();
                ordemBMFFuturo.IdCliente = ordemBMF.IdCliente;
                ordemBMFFuturo.IdAgenteCorretora = ordemBMF.IdAgenteCorretora;
                ordemBMFFuturo.IdAgenteLiquidacao = ordemBMF.IdAgenteLiquidacao;
                ordemBMFFuturo.CdAtivoBMF = "DOL";
                ordemBMFFuturo.Serie = serieFuturo;
                ordemBMFFuturo.IdTrader = ordemBMF.IdTrader;
                ordemBMFFuturo.TipoMercado = TipoMercadoBMF.Futuro;
                ordemBMFFuturo.TipoOrdem = tipoOrdemFuturo;
                ordemBMFFuturo.Data = ordemBMF.Data;                
                ordemBMFFuturo.Pu = puFuturo;
                ordemBMFFuturo.Valor = valorFuturo;
                ordemBMFFuturo.Quantidade = quantidadeFuturo;
                ordemBMFFuturo.Origem = ordemBMF.Origem;
                ordemBMFFuturo.Fonte = ordemBMF.Fonte;
                ordemBMFFuturo.PontaEstrategia = TipoEstrategia.VTC.DOL_FUT;
                ordemBMFFuturo.IdentificadorEstrategia = idOrdem;
                ordemBMFFuturo.CalculaDespesas = "S";
                ordemBMFFuturo.PercentualDesconto = ordemBMF.PercentualDesconto;
                ordemBMFFuturo.IdMoeda = (int)ListaMoedaFixo.Real;
                ordemBMFCollectionInserir.AttachEntity(ordemBMFFuturo);
                #endregion

                #region Monta a OrdemBMF com a ordem de OpcaoFuturo (DOL)
                OrdemBMF ordemBMFOpcao = new OrdemBMF();
                ordemBMFOpcao.AddNew();
                ordemBMFOpcao.IdCliente = ordemBMF.IdCliente;
                ordemBMFOpcao.IdAgenteCorretora = ordemBMF.IdAgenteCorretora;
                ordemBMFOpcao.IdAgenteLiquidacao = ordemBMF.IdAgenteLiquidacao;
                ordemBMFOpcao.CdAtivoBMF = "DOL";
                ordemBMFOpcao.Serie = ordemBMF.Serie;
                ordemBMFOpcao.IdTrader = ordemBMF.IdTrader;
                ordemBMFOpcao.TipoMercado = TipoMercadoBMF.OpcaoDisponivel;
                ordemBMFOpcao.TipoOrdem = ordemBMF.TipoOrdem.Trim().Substring(0,1);
                ordemBMFOpcao.Data = ordemBMF.Data;
                ordemBMFOpcao.Pu = ordemBMF.Pu;
                ordemBMFOpcao.Valor = ordemBMF.Valor;
                ordemBMFOpcao.Quantidade = ordemBMF.Quantidade;
                ordemBMFOpcao.Origem = ordemBMF.Origem;
                ordemBMFOpcao.Fonte = ordemBMF.Fonte;
                ordemBMFOpcao.PontaEstrategia = TipoEstrategia.VTC.DOL_OPC;
                ordemBMFOpcao.IdentificadorEstrategia = idOrdem;
                ordemBMFOpcao.CalculaDespesas = "S";
                ordemBMFOpcao.PercentualDesconto = ordemBMF.PercentualDesconto;
                ordemBMFOpcao.IdMoeda = (int)ListaMoedaFixo.Real;
                ordemBMFCollectionInserir.AttachEntity(ordemBMFOpcao);
                #endregion

            }

            #region Salva as novas ordens desdobradas e exclui as ordens de estrategia
            ordemBMFCollectionInserir.Save();
            //
            ordemBMFCollection.MarkAllAsDeleted();
            ordemBMFCollection.Save();
            #endregion
        }

        /// <summary>
        /// Converte as ordens de estrategias de FRP0/FRP1 em DOL Futuro.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ConverteFRP(int idCliente, DateTime data)
        {
            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
            OrdemBMF ordemBMF = new OrdemBMF();

            // Collection de Ordens de FRP
            ordemBMFCollection.BuscaOrdemBMF(idCliente, data, "FRP");

            for (int i = 0; i < ordemBMFCollection.Count; i++)
            {
                ordemBMF = ordemBMFCollection[i];

                string serieFRP = ordemBMF.Serie;

                AtivoBMF ativoBMF = new AtivoBMF();
                string serie;
                ativoBMF.BuscaSerieAbertoCambio("DOL", TipoMercadoBMF.Futuro, data, 1);
                serie = ativoBMF.Serie;

                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                decimal ptax = 1;

                if (serieFRP == "FRP0")
                {
                    ptax = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.PTAX_800VENDA, data);
                }
                else if (serieFRP == "FRP1")
                {
                    DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    ptax = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.PTAX_800VENDA, dataAnterior);
                }
                
                //Atualiza a ordem de FRP para DOL Futuro, mudando o CdAtivoBMF e o PU (incorporado à ptax)
                ordemBMF.CdAtivoBMF = "DOL";
                ordemBMF.Serie = serie;
                ordemBMF.Pu = ordemBMF.Pu.Value + (ptax * 1000);
            }

            ordemBMFCollection.Save();
        }

        /// <summary>
        /// Rateia corretagem e taxas a partir dos valores informados do dia.
        /// Somente para operações com CalculaDespesas = 'N' (caso TipoCalculoTaxas = NaoCalcula, rateia para todo mundo).
        /// As taxas são jogadas todas em Emolumento, o resto fica zerado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="corretagem"></param>
        /// <param name="taxas"></param>
        public void RateiaTaxasTudo(int idCliente, DateTime data, decimal totalCorretagem, decimal totalTaxas)
        {
            decimal corretagemResidual = totalCorretagem;
            decimal taxasResidual = totalTaxas;
            decimal totalValor = 0;
                        
            OrdemBMF ordemBMF = new OrdemBMF();
            ordemBMF.Query.Select(ordemBMF.Query.Valor.Sum());
            ordemBMF.Query.Where(ordemBMF.Query.IdCliente.Equal(idCliente),
                                    ordemBMF.Query.Data.Equal(data),                                    
                                    ordemBMF.Query.Fonte.NotIn((byte)FonteOperacaoBMF.Sinacor,
                                                               (byte)FonteOperacaoBMF.Interno));

            if (ParametrosConfiguracaoSistema.BMF.TipoCalculoTaxasBMF != (int)TipoCalculoTaxasBMF.NaoCalcula)
            {
                ordemBMF.Query.Where(ordemBMF.Query.CalculaDespesas.Equal("N"));
            }

            if (ordemBMF.Query.Load())
            {
                if (ordemBMF.Valor.HasValue)
                {
                    totalValor = ordemBMF.Valor.Value;
                }
            }
            else
            {
                return;
            }

            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
            ordemBMFCollection.Query.Where(ordemBMFCollection.Query.IdCliente.Equal(idCliente),
                                                ordemBMFCollection.Query.Data.Equal(data),
                                                ordemBMFCollection.Query.Fonte.NotIn((byte)FonteOrdemBMF.Sinacor,
                                                                  (byte)FonteOrdemBMF.Interno));

            if (ParametrosConfiguracaoSistema.BMF.TipoCalculoTaxasBMF != (int)TipoCalculoTaxasBMF.NaoCalcula)
            {
                ordemBMFCollection.Query.Where(ordemBMFCollection.Query.CalculaDespesas.Equal("N"));
            }

            ordemBMFCollection.Query.Load();

            #region Rateia Taxas
            for (int i = 0; i < ordemBMFCollection.Count; i++)
            {
                ordemBMF = new OrdemBMF();
                ordemBMF = ordemBMFCollection[i];
                decimal valor = ordemBMF.Valor.Value;

                // calculo das proporções
                decimal corretagem = Utilitario.Truncate(((totalCorretagem * valor) / totalValor), 2);
                decimal taxas = Utilitario.Truncate(((totalTaxas * valor) / totalValor), 2);

                corretagemResidual -= corretagem;
                taxasResidual -= taxas;

                // Se é a ultima ordem da corretora, garante-se que 100% da corretagem e taxas
                // foram distribuidas, jogando todo o residual para a ultima ordem
                if (i == ordemBMFCollection.Count - 1)
                {
                    corretagem += corretagemResidual;
                    taxas += taxasResidual;
                }


                if (totalCorretagem != 0) //Testa isso, já que posso ratear apenas os custos (emol e reg)
                {
                    ordemBMF.Corretagem = corretagem;
                }

                ordemBMF.Emolumento = taxas;
                ordemBMF.Registro = 0;
                ordemBMF.CustoWtr = 0;
            }
            #endregion

            // Salva a collection de operações
            ordemBMFCollection.Save();
        }

        /// <summary>
        /// Rateia corretagem e taxas a partir dos valores informados do dia.
        /// Somente para operações com CalculaDespesas = 'N' (caso TipoCalculoTaxas = NaoCalcula, rateia para todo mundo).
        /// As taxas são jogadas todas em Emolumento, o resto fica zerado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="corretagem"></param>
        /// <param name="taxas"></param>
        public void RateiaTaxasTudoPorIds(List<int> listaIds, decimal totalCorretagem, decimal totalTaxas)
        {
            decimal corretagemResidual = totalCorretagem;
            decimal taxasResidual = totalTaxas;
            decimal totalValor = 0;

            OrdemBMF ordemBMF = new OrdemBMF();
            ordemBMF.Query.Select(ordemBMF.Query.Valor.Sum());
            ordemBMF.Query.Where(ordemBMF.Query.IdOrdem.In(listaIds));

            if (ordemBMF.Query.Load())
            {
                if (ordemBMF.Valor.HasValue)
                {
                    totalValor = ordemBMF.Valor.Value;
                }
            }
            else
            {
                return;
            }

            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
            ordemBMFCollection.Query.Where(ordemBMFCollection.Query.Fonte.In(listaIds));

            ordemBMFCollection.Query.Load();

            #region Rateia Taxas
            for (int i = 0; i < ordemBMFCollection.Count; i++)
            {
                ordemBMF = new OrdemBMF();
                ordemBMF = ordemBMFCollection[i];
                decimal valor = ordemBMF.Valor.Value;

                // calculo das proporções
                decimal corretagem = Utilitario.Truncate(((totalCorretagem * valor) / totalValor), 2);
                decimal taxas = Utilitario.Truncate(((totalTaxas * valor) / totalValor), 2);

                corretagemResidual -= corretagem;
                taxasResidual -= taxas;

                // Se é a ultima ordem da corretora, garante-se que 100% da corretagem e taxas
                // foram distribuidas, jogando todo o residual para a ultima ordem
                if (i == ordemBMFCollection.Count - 1)
                {
                    corretagem += corretagemResidual;
                    taxas += taxasResidual;
                }


                if (totalCorretagem != 0) //Testa isso, já que posso ratear apenas os custos (emol e reg)
                {
                    ordemBMF.Corretagem = corretagem;
                }

                ordemBMF.Emolumento = taxas;
                ordemBMF.Registro = 0;
                ordemBMF.CustoWtr = 0;
                ordemBMF.CalculaDespesas = "N";
            }
            #endregion

            // Salva a collection de operações
            ordemBMFCollection.Save();
        }

        /// <summary>
        /// Carrega em OrdemBMF a partir de nota de corretagem PDF.
        /// </summary>
        /// <param name="streamArquivoPDF">Stream de Bytes do Arquivo PDF</param>
        /// <exception cref="AtivoNaoCadastradoException">Se Código do Ativo não estiver Cadastrado na Base</exception>
        /// <exception cref="CodigoClienteBovespaInvalido">Se não existe idCliente no Sistema com o CodigoClienteBovespa informado no arquivo</exception>
        public void CarregaNotaCorretagemPDF(Stream streamArquivoPDF)
        {
            OperacaoBMF operacaoBMF = new OperacaoBMF();
            NotaCorretagemBMFPDFCollection notaCorretagemBMFPDFCollection = new NotaCorretagemBMFPDFCollection();
            List<NotaCorretagemBMFPDF> listaNotaCorretagem = notaCorretagemBMFPDFCollection.RetornaOperacoesBMF(streamArquivoPDF);

            if (listaNotaCorretagem.Count == 0)
            {
                return;
            }

            int codigoBovespaAgente = listaNotaCorretagem[0].CodigoBovespa;

            #region GetIdAgente
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            agenteMercado.BuscaIdAgenteMercadoBMF(codigoBovespaAgente);

            int idAgenteCorretora = 0;
            if (agenteMercado.IdAgente.HasValue)
            {
                idAgenteCorretora = agenteMercado.IdAgente.Value;
            }
            else
            {
                throw new IdAgenteNaoCadastradoException("Agente com código Bovespa não encontrado - " + codigoBovespaAgente.ToString());
            }
            #endregion

            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();

            List<int> listaIdCliente = new List<int>();
            List<DateTime> listaDatas = new List<DateTime>();
            List<decimal> listaTaxas = new List<decimal>();
            foreach (NotaCorretagemBMFPDF notaCorretagem in listaNotaCorretagem)
            {
                #region Confere o IdCliente Interno
                int codigoClienteBovespa = notaCorretagem.CodigoCliente;
                CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespa);
                if (!idClienteExiste)
                {
                    throw new Exception("\nProcessando Arquivo Nota de Corretagem: Não Existe Conta por Agente Cadastrada para o Cliente " + codigoClienteBovespa + " na Corretora " + codigoBovespaAgente);
                }
                #endregion

                int idCliente = codigoClienteAgente.IdCliente.Value;

                this.CheckValidData(idCliente, notaCorretagem.DataPregao); //Este check vale tanto para Insert em OrdemBMF

                listaIdCliente.Add(idCliente);
                listaDatas.Add(notaCorretagem.DataPregao);

                listaTaxas.Add(notaCorretagem.TaxaRegistroBMF + notaCorretagem.TaxasBMF);

                foreach (DetalheNotaBMFPDF detalheNotaBMFPDF in notaCorretagem.ListaDetalheNotaBMFPDF)
                {
                    AtivoBMF ativoBMF = new AtivoBMF();
                    string cdAtivo = detalheNotaBMFPDF.Mercadoria.Trim();

                    string cdAtivoBMF = cdAtivo.Substring(0, 3).Trim();
                    string serie = cdAtivo.Replace(cdAtivoBMF, "").Trim();

                    if (!ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie))
                    {
                        throw new AtivoBMFNaoCadastradoException("Ativo não encontrado - " + cdAtivo + serie);
                    }

                    byte tipoMercado = ativoBMF.TipoMercado.Value;
                    int quantidade = Convert.ToInt32(detalheNotaBMFPDF.Quantidade);
                    decimal pu = detalheNotaBMFPDF.PrecoAjuste;
                    decimal corretagem = detalheNotaBMFPDF.TaxaOperacional;

                    string tipoOperacao = detalheNotaBMFPDF.CV;
                    if (detalheNotaBMFPDF.TipoNegocio.Contains("DAY"))
                    {
                        tipoOperacao = tipoOperacao + "D";
                    }

                    decimal valor = Utilitario.Truncate(quantidade * pu * ativoBMF.Peso.Value, 2);

                    #region Preenche OperacaoBMF
                    OrdemBMF ordemBMF = ordemBMFCollection.AddNew();
                    ordemBMF.CalculaDespesas = "N";
                    ordemBMF.CdAtivoBMF = ativoBMF.CdAtivoBMF;
                    ordemBMF.Serie = ativoBMF.Serie;
                    ordemBMF.Corretagem = corretagem;
                    ordemBMF.CustoWtr = 0;
                    ordemBMF.Data = notaCorretagem.DataPregao;
                    ordemBMF.Emolumento = 0;
                    ordemBMF.Fonte = (byte)FonteOperacaoBMF.NotaPDFSinacor;
                    ordemBMF.IdAgenteCorretora = idAgenteCorretora;
                    ordemBMF.IdAgenteLiquidacao = idAgenteCorretora;
                    ordemBMF.IdCliente = idCliente;
                    ordemBMF.IdMoeda = (int)ListaMoedaFixo.Real;
                    ordemBMF.Origem = (byte)OrigemOperacaoBMF.Primaria;
                    ordemBMF.Pu = pu;
                    ordemBMF.Quantidade = quantidade;
                    ordemBMF.Registro = 0;
                    ordemBMF.Serie = serie;
                    ordemBMF.TipoMercado = ativoBMF.TipoMercado.Value;
                    ordemBMF.TipoOrdem = tipoOperacao.Substring(0,1);

                    decimal taxa = pu;
                    if (ativoBMF.TipoMercado == (byte)TipoMercadoBMF.Futuro && (ativoBMF.CdAtivoBMF == "DI1" || ativoBMF.CdAtivoBMF == "DDI"))
                    {
                        if (ativoBMF.CdAtivoBMF == "DI1")
                        {
                            AtivoBMF ativoBMFPrazo = new AtivoBMF();
                            int numeroDiasVencimento = ativoBMFPrazo.RetornaPrazoDiasUteis(ordemBMF.CdAtivoBMF, ordemBMF.Serie, ordemBMF.Data.Value);
                            decimal fator = 1M + (taxa / 100M);
                            decimal expoente = numeroDiasVencimento / 252M;
                            decimal fatorDesconto = (decimal)Math.Pow((double)fator, (double)expoente);
                            decimal puCalculado = Math.Round(100000M / fatorDesconto, 2);
                            ordemBMF.Pu = puCalculado;
                        }
                        else if (ativoBMF.CdAtivoBMF == "DDI")
                        {
                            AtivoBMF ativoBMFPrazo = new AtivoBMF();
                            int numeroDiasVencimento = ativoBMFPrazo.RetornaPrazoDiasCorridos(ordemBMF.CdAtivoBMF, ordemBMF.Serie, ordemBMF.Data.Value);

                            decimal puCalculado = Math.Round(100000M / (1M + (taxa * numeroDiasVencimento / 36000M)), 2);
                            ordemBMF.Pu = puCalculado;
                        }

                        ordemBMF.TipoOrdem = ordemBMF.TipoOrdem == TipoOrdemBMF.Compra ? TipoOrdemBMF.Venda : TipoOrdemBMF.Compra;
                    }
                    else
                    {
                        ordemBMF.Pu = pu;
                    }

                    ordemBMF.Valor = ordemBMF.Pu.Value * ordemBMF.Quantidade.Value;
                    #endregion

                }
            }

            using (esTransactionScope scope = new esTransactionScope())
            {
                #region Deleta antes de salvar as collections
                for (int i = 0; i < listaIdCliente.Count; i++)
                {
                    int idClienteLista = listaIdCliente[i];
                    DateTime dataLista = listaDatas[i];

                    OrdemBMFCollection ordemBMFCollectionDeletar = new OrdemBMFCollection();
                    ordemBMFCollectionDeletar.Query.Select(ordemBMFCollectionDeletar.Query.IdOrdem);
                    ordemBMFCollectionDeletar.Query.Where(ordemBMFCollectionDeletar.Query.IdCliente.Equal(idClienteLista),
                                                             ordemBMFCollectionDeletar.Query.Data.Equal(dataLista),
                                                             ordemBMFCollectionDeletar.Query.IdAgenteCorretora.Equal(idAgenteCorretora),
                                                             ordemBMFCollectionDeletar.Query.Fonte.Equal((byte)FonteOperacaoBMF.NotaPDFSinacor));
                    ordemBMFCollectionDeletar.Query.Load();
                    ordemBMFCollectionDeletar.MarkAllAsDeleted();
                    ordemBMFCollectionDeletar.Save();
                }

                ordemBMFCollection.Save();
                #endregion

                OrdemBMF ordemBMF = new OrdemBMF();
                #region Rateio de Taxas
                for (int i = 0; i < listaIdCliente.Count; i++)
                {
                    int idClienteLista = listaIdCliente[i];
                    DateTime dataLista = listaDatas[i];

                    decimal taxas = listaTaxas[i];

                    if (taxas != 0)
                    {
                        ordemBMF.RateiaTaxasTudo(idClienteLista, dataLista, 0, taxas); //Rateia apenas as taxas, a corretagem mantem o que veio na linha detalhe
                    }
                }
                #endregion

                scope.Complete();
            }
        }

        private void CheckValidData(int idCliente, DateTime data)
        {
            //Verifica se a Ordem pode ser salva

            //Criterio: DataOrdem nao pode ser anterior a data cliente ou entao é no mesmo dia e o cliente não está fechado

            Cliente clienteCheck = new Cliente();
            clienteCheck.LoadByPrimaryKey(idCliente);

            if ((data < clienteCheck.DataDia.Value) || (data == clienteCheck.DataDia.Value && clienteCheck.Status.Value == (byte)StatusCliente.Divulgado))
            {
                throw new Exception("\nOrdem não foi processada. Problema: Data informada anterior à data do cliente ou status do cliente fechado.");
            }
        }
    }
}
