﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.BMF.Exceptions;
using log4net;

namespace Financial.BMF
{
	public partial class PerfilCorretagemBMF : esPerfilCorretagemBMF
	{
        /// <summary>
        /// Carrega o objeto PerfilCorretagemBMF com os campos IdPerfilCorretagem, DataReferencia,
        /// TipoCorretagem, TipoCorretagemDT.        
        /// Filtra GrupoAtivo por "Geral".
        /// Filtra DataReferencia menor ou igual a dataReferencia.
        /// Ordenado decrescente pela DataReferencia.
        /// </summary>
        /// <param name="dataReferencia"></param>
        /// <param name="idCliente"></param>
        /// <param name="idAgente"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaPerfilCorretagemGeral(DateTime dataReferencia, int idCliente, int idAgente)
        {
            PerfilCorretagemBMFCollection perfilCorretagemBMFCollection = new PerfilCorretagemBMFCollection();

            perfilCorretagemBMFCollection.Query
                 .Select(perfilCorretagemBMFCollection.Query.IdPerfilCorretagem, perfilCorretagemBMFCollection.Query.DataReferencia,
                         perfilCorretagemBMFCollection.Query.TipoCorretagem, perfilCorretagemBMFCollection.Query.TipoCorretagemDT)
                 .Where(perfilCorretagemBMFCollection.Query.IdCliente == idCliente,
                        perfilCorretagemBMFCollection.Query.IdAgente == idAgente,
                        perfilCorretagemBMFCollection.Query.GrupoAtivo.Equal("Geral"),
                        perfilCorretagemBMFCollection.Query.DataReferencia.LessThanOrEqual(dataReferencia))
                 .OrderBy(perfilCorretagemBMFCollection.Query.DataReferencia.Descending);

            perfilCorretagemBMFCollection.Query.Load();

            if (perfilCorretagemBMFCollection.HasData)
            {
                // Copia informações de perfilCorretagemBMFCollection para PerfilCorretagemBMF
                this.AddNew();
                this.IdPerfilCorretagem = ((PerfilCorretagemBMF)perfilCorretagemBMFCollection[0]).IdPerfilCorretagem;
                this.DataReferencia = ((PerfilCorretagemBMF)perfilCorretagemBMFCollection[0]).DataReferencia;
                this.TipoCorretagem = ((PerfilCorretagemBMF)perfilCorretagemBMFCollection[0]).TipoCorretagem;
                this.TipoCorretagemDT = ((PerfilCorretagemBMF)perfilCorretagemBMFCollection[0]).TipoCorretagemDT;
                return true;
            }
            else
            {
                return false;
            }            
        }

        /// <summary>
        /// Carrega o objeto PerfilCorretagemBMF com os campos IdPerfilCorretagem, DataReferencia,
        /// TipoCorretagem, TipoCorretagemDT.        
        /// Filtra DataReferencia menor ou igual a dataReferencia.
        /// Ordenado decrescente pela DataReferencia.
        /// </summary>
        /// <param name="dataReferencia"></param>
        /// <param name="idCliente"></param>
        /// <param name="idAgente"></param>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="tipoMercado"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaPerfilCorretagem(DateTime dataReferencia, int idCliente, int idAgente, string cdAtivoBMF, 
                                          int tipoMercado)
        {
            PerfilCorretagemBMFCollection perfilCorretagemBMFCollection = new PerfilCorretagemBMFCollection();

            perfilCorretagemBMFCollection.Query
                 .Select(perfilCorretagemBMFCollection.Query.IdPerfilCorretagem, perfilCorretagemBMFCollection.Query.DataReferencia,
                         perfilCorretagemBMFCollection.Query.TipoCorretagem, perfilCorretagemBMFCollection.Query.TipoCorretagemDT)
                 .Where(perfilCorretagemBMFCollection.Query.IdCliente == idCliente,
                        perfilCorretagemBMFCollection.Query.IdAgente == idAgente,
                        perfilCorretagemBMFCollection.Query.CdAtivoBMF.Like(cdAtivoBMF + "%"),
                        perfilCorretagemBMFCollection.Query.TipoMercado == tipoMercado,
                        perfilCorretagemBMFCollection.Query.DataReferencia.LessThanOrEqual(dataReferencia))
                 .OrderBy(perfilCorretagemBMFCollection.Query.DataReferencia.Descending);

            perfilCorretagemBMFCollection.Query.Load();

            if (perfilCorretagemBMFCollection.HasData)
            {
                // Copia informações de perfilCorretagemBMFCollection para PerfilCorretagemBMF
                this.AddNew();                
                this.IdPerfilCorretagem = ((PerfilCorretagemBMF)perfilCorretagemBMFCollection[0]).IdPerfilCorretagem;
                this.DataReferencia = ((PerfilCorretagemBMF)perfilCorretagemBMFCollection[0]).DataReferencia;
                this.TipoCorretagem = ((PerfilCorretagemBMF)perfilCorretagemBMFCollection[0]).TipoCorretagem;
                this.TipoCorretagemDT = ((PerfilCorretagemBMF)perfilCorretagemBMFCollection[0]).TipoCorretagemDT;
                return true;
            }
            else
            {
                return false;
            }            
        }

        /// <summary>
        /// Carrega o objeto PerfilCorretagemBMF com os campos IdPerfilCorretagem, DataReferencia,
        /// TipoCorretagem, TipoCorretagemDT.        
        /// Filtra DataReferencia menor ou igual a dataReferencia.
        /// Ordenado decrescente pela DataReferencia.
        /// </summary>
        /// <param name="dataReferencia"></param>
        /// <param name="idCliente"></param>
        /// <param name="idAgente"></param>
        /// <param name="grupoAtivo"></param>   
        /// <param name="tipoMercado"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaPerfilCorretagemGrupo(DateTime dataReferencia, int idCliente, int idAgente, 
                                               string grupoAtivo, int tipoMercado)
        {
            PerfilCorretagemBMFCollection perfilCorretagemBMFCollection = new PerfilCorretagemBMFCollection();

            perfilCorretagemBMFCollection.Query
                 .Select(perfilCorretagemBMFCollection.Query.IdPerfilCorretagem, perfilCorretagemBMFCollection.Query.DataReferencia,
                         perfilCorretagemBMFCollection.Query.TipoCorretagem, perfilCorretagemBMFCollection.Query.TipoCorretagemDT)
                 .Where(perfilCorretagemBMFCollection.Query.IdCliente == idCliente,
                        perfilCorretagemBMFCollection.Query.IdAgente == idAgente,
                        perfilCorretagemBMFCollection.Query.GrupoAtivo.Like("%" + grupoAtivo + "%"),
                        perfilCorretagemBMFCollection.Query.TipoMercado == tipoMercado,
                        perfilCorretagemBMFCollection.Query.DataReferencia.LessThanOrEqual(dataReferencia))
                 .OrderBy(perfilCorretagemBMFCollection.Query.DataReferencia.Descending);

            perfilCorretagemBMFCollection.Query.Load();

            if (perfilCorretagemBMFCollection.HasData)
            {
                // Copia informações de perfilCorretagemBMFCollection para PerfilCorretagemBMF
                this.AddNew();
                this.IdPerfilCorretagem = ((PerfilCorretagemBMF)perfilCorretagemBMFCollection[0]).IdPerfilCorretagem;
                this.DataReferencia = ((PerfilCorretagemBMF)perfilCorretagemBMFCollection[0]).DataReferencia;
                this.TipoCorretagem = ((PerfilCorretagemBMF)perfilCorretagemBMFCollection[0]).TipoCorretagem;
                this.TipoCorretagemDT = ((PerfilCorretagemBMF)perfilCorretagemBMFCollection[0]).TipoCorretagemDT;
                return true;
            }
            else
            {
                return false;
            }            
        }        
	}
}
