﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.BMF.Enums;
using Financial.Common.Enums;
using Financial.Util;

namespace Financial.BMF {
    public partial class TransferenciaBMF : esTransferenciaBMF {
        private static readonly ILog log = LogManager.GetLogger(typeof(TransferenciaBMF));

        /// <summary>
        /// Processa as operações de transferência de agente de custódia de BMF.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaTransferenciaAgenteCustodia(int idCliente, DateTime data) 
        {
            TransferenciaBMFCollection transferenciaBMFCollection = new TransferenciaBMFCollection();
            OperacaoBMFCollection operacaoBMFCollectionAux = new OperacaoBMFCollection();            
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();

            // Apaga as operações com origem = transferencia de custodia
            operacaoBMFCollectionAux.DeletaOperacaoBMF(idCliente, OrigemOperacaoBMF.TransferenciaCorretora, data);
            transferenciaBMFCollection.BuscaTransferenciaBMF(idCliente, data);
            for (int i = 0; i < transferenciaBMFCollection.Count; i++) {
                TransferenciaBMF transferenciaBMF = (TransferenciaBMF)transferenciaBMFCollection[i];

                string tipoOperacao = "";

                if (transferenciaBMF.Quantidade.Value > 0)
                {
                    tipoOperacao = TipoOperacaoBMF.Retirada;
                }
                else
                {
                    tipoOperacao = TipoOperacaoBMF.Deposito;
                }

                #region Operacao na Corretora Origem
                OperacaoBMF operacaoBMFOrigem = new OperacaoBMF();
                operacaoBMFOrigem.AddNew();
                operacaoBMFOrigem.TipoMercado = transferenciaBMF.UpToAtivoBMFByCdAtivoBMF.TipoMercado;
                operacaoBMFOrigem.TipoOperacao = tipoOperacao;
                operacaoBMFOrigem.Data = data;
                operacaoBMFOrigem.Serie = transferenciaBMF.Serie;

                operacaoBMFOrigem.Pu = 0;
                operacaoBMFOrigem.PULiquido = 0;
				operacaoBMFOrigem.Valor = 0;

                if (transferenciaBMF.Pu.HasValue)
                {
                    operacaoBMFOrigem.Pu = transferenciaBMF.Pu.Value;
                    operacaoBMFOrigem.PULiquido = transferenciaBMF.Pu.Value;
					operacaoBMFOrigem.Valor = transferenciaBMF.Pu.Value * Math.Abs(transferenciaBMF.Quantidade.Value);
                }

                operacaoBMFOrigem.Quantidade = Math.Abs(transferenciaBMF.Quantidade.Value);
                operacaoBMFOrigem.CdAtivoBMF = transferenciaBMF.CdAtivoBMF;
                operacaoBMFOrigem.IdAgenteCorretora = transferenciaBMF.IdAgenteOrigem;
                operacaoBMFOrigem.IdAgenteLiquidacao = transferenciaBMF.IdAgenteOrigem;
                //

                operacaoBMFOrigem.Origem = (byte)OrigemOperacaoBMF.TransferenciaCorretora;
                operacaoBMFOrigem.Fonte = (byte)FonteOperacaoBMF.Interno;
                operacaoBMFOrigem.IdCliente = idCliente;
                operacaoBMFOrigem.CalculaDespesas = "N";
                operacaoBMFOrigem.IdMoeda = (int)ListaMoedaFixo.Real;
                operacaoBMFOrigem.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                operacaoBMFOrigem.IdClearing = (int)ClearingFixo.BOVESPA;
                operacaoBMFOrigem.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                operacaoBMFOrigem.DataOperacao = data;
                #endregion

                operacaoBMFCollection.AttachEntity(operacaoBMFOrigem);


                /////////////////////////////////////////////////////////////////


                if (transferenciaBMF.Quantidade.Value > 0)
                {
                    tipoOperacao = TipoOperacaoBMF.Deposito;
                }
                else
                {
                    tipoOperacao = TipoOperacaoBMF.Retirada;
                }
                
                #region Operacao na Corretora Destino
                OperacaoBMF operacaoBMFDestino = new OperacaoBMF();
                operacaoBMFDestino.AddNew();
                operacaoBMFDestino.TipoMercado = transferenciaBMF.UpToAtivoBMFByCdAtivoBMF.TipoMercado;
                operacaoBMFDestino.Data = data;
                operacaoBMFDestino.Serie = transferenciaBMF.Serie;

                operacaoBMFDestino.Pu = 0;
                operacaoBMFDestino.PULiquido = 0;
                operacaoBMFDestino.Valor = 0;
                if (transferenciaBMF.Pu.HasValue)
                {
                    operacaoBMFDestino.Pu = transferenciaBMF.Pu.Value;
                    operacaoBMFDestino.PULiquido = transferenciaBMF.Pu.Value;
					operacaoBMFDestino.Valor = transferenciaBMF.Pu.Value * Math.Abs(transferenciaBMF.Quantidade.Value);
                }

                operacaoBMFDestino.Quantidade = Math.Abs(transferenciaBMF.Quantidade.Value);
                operacaoBMFDestino.CdAtivoBMF = transferenciaBMF.CdAtivoBMF;
                operacaoBMFDestino.Origem = (byte)OrigemOperacaoBMF.TransferenciaCorretora;
                operacaoBMFDestino.Fonte = (byte)FonteOperacaoBMF.Interno;
                operacaoBMFDestino.IdCliente = idCliente;
                //
                operacaoBMFDestino.TipoOperacao = tipoOperacao;
                operacaoBMFDestino.IdAgenteCorretora = transferenciaBMF.IdAgenteDestino;
                operacaoBMFDestino.IdAgenteLiquidacao = transferenciaBMF.IdAgenteDestino;
                operacaoBMFDestino.CalculaDespesas = "N";
                operacaoBMFDestino.IdMoeda = (int)ListaMoedaFixo.Real;
                operacaoBMFDestino.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                operacaoBMFDestino.IdClearing = (int)ClearingFixo.BOVESPA;
                operacaoBMFDestino.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                operacaoBMFDestino.DataOperacao = data; 
                #endregion

                operacaoBMFCollection.AttachEntity(operacaoBMFDestino);
            }

            operacaoBMFCollection.Save();
        }
    }
}
