using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.BMF
{
	public partial class TabelaCalculoBMFCollection : esTabelaCalculoBMFCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaCalculoBMFCollection));

        /// <summary>
        /// Carrega o objeto TabelaCalculoBMFCollection com os campos CdAtivoBMF, DataReferencia, DiasMinimo, 
        /// DiasMaximo, P.
        /// </summary>
        /// <param name="cdAtivoBMF">Pode ser usado o formato IN (...) da query.</param>
        /// <param name="data"></param>
        public void BuscaTabelaCalculoBMF(string cdAtivoBMF, DateTime data)
        {
            #region logEntrada
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            this.QueryReset();
            this.Query
                 .Select(this.query.CdAtivoBMF, this.query.DataReferencia, this.query.DiasMinimo, 
                         this.query.DiasMaximo, this.query.P)
                 .Where(this.query.CdAtivoBMF.In (cdAtivoBMF), 
                        this.query.DataReferencia == data);
            this.Query.Load();

            #region logSql
            if (log.IsInfoEnabled) {
                string sql = "Delete: " + this.query.es.LastQuery;
                sql = sql.Replace("@CdAtivoBMF1", "'" + cdAtivoBMF + "'");
                sql = sql.Replace("@DataReferencia2", "'" + data.ToString("yyyy-MM-dd") + "'");

                log.Info(sql);
            }
            #endregion	            

            #region logSaida
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
        }
	}
}
