﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Interfaces.Import.BMF;

namespace Financial.BMF {
    public partial class TabelaVolatilidadeEstrategia : esTabelaVolatilidadeEstrategia {
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaVolatilidadeEstrategia));

        /// <summary>
        /// Carrega a TabelaVolatilidadeEstrategia a partir do arquivo RefVol.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>        
        public void CarregaTabelaVolatilidadeEstrategiaRefVol(DateTime data, string pathArquivo) {
            #region logEntrada
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            RefVol refVol = new RefVol();
            TabelaVolatilidadeEstrategiaCollection tabelaVolatilidadeEstrategiaCollection = new TabelaVolatilidadeEstrategiaCollection();
            TabelaVolatilidadeEstrategiaCollection tabelaVolatilidadeEstrategiaCollectionAux = new TabelaVolatilidadeEstrategiaCollection();

            // Deleta a tabela de Volatilidade a partir da data em questão
            tabelaVolatilidadeEstrategiaCollectionAux.DeletaTabelaVolatilidadeEstrategia(data);
            // Pega os registros da collection de RefVol
            RefVolCollection refVolCollection = refVol.ProcessaRefVol(data, pathArquivo);
            //
            for (int i = 0; i < refVolCollection.CollectionRefVol.Count; i++) {
                refVol = refVolCollection.CollectionRefVol[i];

                #region Copia informações para TabelaVolatilidadeEstrategia 
                TabelaVolatilidadeEstrategia tabelaVolatilidadeEstrategia = new TabelaVolatilidadeEstrategia();
                                
                //Se Ativo + Serie nao estiver cadastrado não insere
                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMF.LoadByPrimaryKey(refVol.CodigoMercadoria, refVol.Vencimento);

                if (ativoBMF.es.HasData) {
                    tabelaVolatilidadeEstrategia.DataReferencia = data;
                    tabelaVolatilidadeEstrategia.CdAtivoBMF = refVol.CodigoMercadoria;
                    tabelaVolatilidadeEstrategia.Serie = refVol.Vencimento;
                    tabelaVolatilidadeEstrategia.TipoMercado = Convert.ToByte(refVol.TipoMercado);
                    tabelaVolatilidadeEstrategia.Pu = refVol.PrecoFuturo;
                    tabelaVolatilidadeEstrategia.PULongo = refVol.PrecoFuturoLongo;
                    tabelaVolatilidadeEstrategia.Delta = refVol.Delta;
                    //
                    tabelaVolatilidadeEstrategiaCollection.AttachEntity(tabelaVolatilidadeEstrategia);
                }
                #endregion
            }

            try {
                tabelaVolatilidadeEstrategiaCollection.Save();
            }
            catch {
                foreach (TabelaVolatilidadeEstrategia tab in tabelaVolatilidadeEstrategiaCollection.Errors) {
                    Console.WriteLine(tab.es.RowError);
                }
            }   

            #region logSaida
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
        }
    }
}
