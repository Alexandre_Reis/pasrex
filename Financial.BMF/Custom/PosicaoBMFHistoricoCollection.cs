﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.BMF.Enums;
using log4net;

namespace Financial.BMF
{
	public partial class PosicaoBMFHistoricoCollection : esPosicaoBMFHistoricoCollection
	{
        // Construtor
        // Cria uma nova PosicaoBMFHistoricoCollection com os dados de PosicaoBMFCollection
        // Todos os dados de PosicaoBMFCollection são copiados, adicionando a dataHistorico
        public PosicaoBMFHistoricoCollection(PosicaoBMFCollection posicaoBMFCollection, DateTime dataHistorico)
        {
            using (esTransactionScope scope = new esTransactionScope())
            {
                int max = 0;
                PosicaoBMFHistorico posicaoBMFHistoricoMax = new PosicaoBMFHistorico();
                posicaoBMFHistoricoMax.Query.Select(posicaoBMFHistoricoMax.Query.IdPosicao.Max());
                posicaoBMFHistoricoMax.Query.Load();

                if (posicaoBMFHistoricoMax.IdPosicao.HasValue)
                {
                    max = posicaoBMFHistoricoMax.IdPosicao.Value + 1;
                }
                else
                {
                    max = 1;
                }

                for (int i = 0; i < posicaoBMFCollection.Count; i++)
                {
                    PosicaoBMFHistorico p = new PosicaoBMFHistorico();

                    // Para cada Coluna de PosicaoBMF copia para PosicaoBMFHistorico
                    foreach (esColumnMetadata colPosicao in posicaoBMFCollection.es.Meta.Columns)
                    {
                        // Copia todas as colunas 
                        esColumnMetadata colPosicaoBMFHistorico = p.es.Meta.Columns.FindByPropertyName(colPosicao.PropertyName);
                        if (posicaoBMFCollection[i].GetColumn(colPosicao.Name) != null)
                        {
                            p.SetColumn(colPosicaoBMFHistorico.Name, posicaoBMFCollection[i].GetColumn(colPosicao.Name));
                        }
                    }
                    
                    p.DataHistorico = dataHistorico;
                    p.IdPosicao = max;

                    max += 1;

                    this.AttachEntity(p);
                }

                scope.Complete();
            }
        }
        
        /// <summary>
        /// Carrega o objeto PosicaoBMFHistoricoCollection com todos os campos.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaPosicaoBMFHistoricoCompleta(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.IdCliente == idCliente);

            bool retorno = this.Query.Load();
            
            return retorno;
        }

        /// <summary>
        /// Deleta todas as posições históricas com data >= que a data passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoBMFHistoricoDataHistoricoMaiorIgual(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta todas as posições históricas com data > que a data passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoBMFHistoricoDataHistoricoMaior(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMFCollection com os campos CdAtivoBMF, Serie, TipoMercado, Quantidade.Sum, PUMercado.Avg, PUCustoLiquido.Avg, 
        /// ValorMercado.Sum.
        /// Group By CdAtivoBMF, Serie
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>indica se retornou registro</returns>
        public bool BuscaPosicaoBMFAgrupado(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBMF,
                         this.Query.Serie,
                         this.Query.TipoMercado,
                         this.Query.Quantidade.Sum(),
                         this.Query.PUMercado.Avg(),
                         this.Query.PUCustoLiquido.Avg(),
                         this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico))
                 .GroupBy(this.Query.CdAtivoBMF,
                          this.Query.Serie,
                          this.Query.TipoMercado);

            bool retorno = this.Query.Load();

            return retorno;
        }
	}
}
