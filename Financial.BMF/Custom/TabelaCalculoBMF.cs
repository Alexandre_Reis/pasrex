﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.BMF.Exceptions;
using Financial.BMF.Enums;
using Financial.Interfaces.Import.BMF;

namespace Financial.BMF
{
	public partial class TabelaCalculoBMF : esTabelaCalculoBMF
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaCalculoBMF));

        /// <summary>
        /// Carrega parâmetros de tarifação para a TabelaCalculoBMF, a partir do arquivo TarPar.
        /// </summary>
        /// <param name="data">Data de referência do TarPar</param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>        
        public void CarregaTabelaCalculoBMFTarPar(DateTime data, string pathArquivo)
        {
            #region logEntrada
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            TarPar tarPar = new TarPar();
            TarParCollection tarParCollection = tarPar.ProcessaTarPar(data, pathArquivo);            

            using (esTransactionScope scope = new esTransactionScope())
            {
                for (int j = 0; j < tarParCollection.CollectionTarPar.Count; j++) {
                    tarPar = tarParCollection.CollectionTarPar[j];
                    decimal valor = (decimal)tarPar.Valor;

                    #region Define Categoria de Ativos
                    string[] listaAtivos;
                    switch (tarPar.Parametro)
                    {   
                        case "DIAS_UTEIS_MIN_DI_1_DIA":
                            #region Número de dias mínimo para DI1, IDI, DIA, D11, D12, D13, DDM, DMA, FRM
                            // Grupo de ativos a tratar
                            listaAtivos = new string[]
                               {"DI1", "IDI", "DIA", "D11", "D12", "D13", "DDM", "DMA", "FRM"};

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataDiasMinimo(listaAtivos[i], data, (int) valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                           #endregion                        
                        case "DIAS_UTEIS_MAX_DI_1_DIA":
                            #region Número de dias máximo para DI1, IDI, DIA, D11, D12, D13, DDM, DMA, FRM
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "DI1", "IDI", "DIA", "D11", "D12", "D13", "DDM", "DMA", "FRM" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataDiasMaximo(listaAtivos[i], data, (int) valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                        
                        case "DIAS_CORRIDOS_MIN_DDI":
                            #region Número de dias mínimo para DDI, SCC, SC2, SC3
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "DDI", "SCC", "SC2", "SC3" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataDiasMinimo(listaAtivos[i], data, (int) valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                        
                        case "DIAS_CORRIDOS_MAX_DDI":
                            #region Número de dias mínimo para DDI, SCC, SC2, SC3
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "DDI", "SCC", "SC2", "SC3" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataDiasMaximo(listaAtivos[i], data, (int) valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                        
                        case "PRAZO_MIN_FRA":
                            #region Número de dias mínimo para FRC
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] {"FRC"};

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataDiasMinimo(listaAtivos[i], data, (int) valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                                               
                        case "PRAZO_MAX_FRA": 
                            #region Número de dias máximo para FRC
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] {"FRC"};

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataDiasMinimo(listaAtivos[i], data, (int) valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                        
                        case "ALPHA_FRC":
                            #region Alpha para FRC
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "FRC" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataAlfa(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                        
                        case "BETA_FRC":
                            #region Beta para FRC
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "FRC" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataBeta(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;                            
                            #endregion
                        case "ALPHA_FRM":
                            #region Alpha para FRM
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "FRM" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataAlfa(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion
                        case "BETA_FRM":
                            #region Beta para FRM
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "FRM" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataBeta(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion
                        case "P_IBV":
                            #region P para IND
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "IND" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion
                        case "P_DOLAR":
                            #region P para DOL
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] {"DOL"};

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion
                        case "P_DI_1_DIA":
                            #region P para DI1, IDI, DIA, D11, D12, D13
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "DI1", "IDI", "DIA", "D11", "D12", "D13" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion
                        case "P_DDI":
                            #region P para DDI, SCC, SC2, SC3, FRC
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "DDI", "SCC", "SC2", "SC3", "FRC" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion
                        case "P_FRA":
                            #region P para FRA
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] {"FRC"};

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion
                        case "P_DAP":
                            #region P para DAP
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] {"DAP"};

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion
                        case "P_DDM":
                            #region P para DDM
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] {"DDM"};

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                                    
                        case "P_ACUCAR":
                            #region P para ISU
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "ISU" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                                    
                        case "P_ALCOOL":
                            #region P para ALA
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "ALA" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                                    
                        case "P_BEZERRO":
                            #region P para BZE
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "BZE" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                                    
                        case "P_BOI":
                            #region P para BGI
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "BGI" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                                    
                        case "P_CAFE_ARABICA":
                            #region P para ICF
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "ICF" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                                    
                        case "P_MILHO":
                            #region P para CNI
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "CNI" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                                    
                        case "P_SOJA":
                            #region P para SOJ
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "SOJ" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                                    
                        case "P_EURO":
                            #region P para EUR
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "EUR" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                                    
                        case "P_IGPM":
                            #region P para IGM
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "IGM" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                                    
                        case "P_IPCA":
                            #region P para IAP
                            // Grupo de ativos a tratar
                            listaAtivos = new string[] { "IAP" };

                            for (int i = 0; i < listaAtivos.Length; i++)
                            {
                                TabelaCalculoBMF tabelaCalculoBMF = this.TrataP(listaAtivos[i], data, valor);
                                tabelaCalculoBMF.Save();
                            }
                            break;
                            #endregion                                    
                    }
                    #endregion                
                }
                scope.Complete();
            }
                
            #region logSaida
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
        }

        /// <summary>
        /// Testa se o ativo é IDI (OpcaoDisponivel). O resto trata forçando o TipoMercado = Futuro.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="data"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        private TabelaCalculoBMF TrataDiasMinimo(string cdAtivoBMF, DateTime data, int valor)
        {
            int tipoMercado = cdAtivoBMF == "DDI" ? TipoMercadoBMF.OpcaoDisponivel : TipoMercadoBMF.Futuro; 

            TabelaCalculoBMF tabelaCalculoBMF = new TabelaCalculoBMF();
            if (tabelaCalculoBMF.LoadByPrimaryKey(data, cdAtivoBMF, (byte)tipoMercado))
            {
                tabelaCalculoBMF.DiasMinimo = (short) valor;
            }
            else
            {
                ClasseBMF classeBMF = new ClasseBMF();
                if (!classeBMF.LoadByPrimaryKey(cdAtivoBMF))
                {
                    throw new ClasseBMFNaoCadastradaException("Classe de Ativo " + cdAtivoBMF + " não cadastrada.");
                }

                tabelaCalculoBMF.AddNew();
                tabelaCalculoBMF.TipoMercado = (byte)tipoMercado;
                tabelaCalculoBMF.DataReferencia = data;
                tabelaCalculoBMF.CdAtivoBMF = cdAtivoBMF;
                tabelaCalculoBMF.DiasMinimo = (short) valor;
            }
            return tabelaCalculoBMF;
        }

        /// <summary>
        /// Testa se o ativo é IDI (OpcaoDisponivel). O resto trata forçando o TipoMercado = Futuro.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="data"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        private TabelaCalculoBMF TrataDiasMaximo(string cdAtivoBMF, DateTime data, int valor)
        {
            int tipoMercado = cdAtivoBMF == "DDI" ? TipoMercadoBMF.OpcaoDisponivel : TipoMercadoBMF.Futuro; 

            TabelaCalculoBMF tabelaCalculoBMF = new TabelaCalculoBMF();
            if (tabelaCalculoBMF.LoadByPrimaryKey(data, cdAtivoBMF, (byte)tipoMercado))
            {
                tabelaCalculoBMF.DiasMaximo = (short)valor;
            }
            else
            {
                ClasseBMF classeBMF = new ClasseBMF();
                if (!classeBMF.LoadByPrimaryKey(cdAtivoBMF))
                {
                    throw new ClasseBMFNaoCadastradaException("Classe de Ativo " + cdAtivoBMF + " não cadastrada.");
                }

                tabelaCalculoBMF.TipoMercado = (byte)tipoMercado;
                tabelaCalculoBMF.DataReferencia = data;
                tabelaCalculoBMF.CdAtivoBMF = cdAtivoBMF;
                tabelaCalculoBMF.DiasMaximo = (short)valor;
            }
            return tabelaCalculoBMF;
        }

        /// <summary>
        /// Trata forçando o TipoMercado = Futuro.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="data"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        private TabelaCalculoBMF TrataP(string cdAtivoBMF, DateTime data, decimal valor)
        {
            TabelaCalculoBMF tabelaCalculoBMF = new TabelaCalculoBMF();
            if (tabelaCalculoBMF.LoadByPrimaryKey(data, cdAtivoBMF, TipoMercadoBMF.Futuro))
            {
                tabelaCalculoBMF.P = (decimal)valor;
            }
            else
            {
                ClasseBMF classeBMF = new ClasseBMF();
                if (!classeBMF.LoadByPrimaryKey(cdAtivoBMF))
                {
                    throw new ClasseBMFNaoCadastradaException("Classe de Ativo " + cdAtivoBMF + " não cadastrada.");
                }

                tabelaCalculoBMF.TipoMercado = TipoMercadoBMF.Futuro;
                tabelaCalculoBMF.DataReferencia = data;
                tabelaCalculoBMF.CdAtivoBMF = cdAtivoBMF;
                tabelaCalculoBMF.P = (decimal)valor;
            }
            return tabelaCalculoBMF;
        }

        /// <summary>
        /// Trata forçando o TipoMercado = Futuro.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="data"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        private TabelaCalculoBMF TrataAlfa(string cdAtivoBMF, DateTime data, decimal valor)
        {
            TabelaCalculoBMF tabelaCalculoBMF = new TabelaCalculoBMF();
            if (tabelaCalculoBMF.LoadByPrimaryKey(data, cdAtivoBMF, TipoMercadoBMF.Futuro))
            {
                tabelaCalculoBMF.Alfa = (decimal)valor;
            }
            else
            {
                ClasseBMF classeBMF = new ClasseBMF();
                if (!classeBMF.LoadByPrimaryKey(cdAtivoBMF))
                {
                    throw new ClasseBMFNaoCadastradaException("Classe de Ativo " + cdAtivoBMF + " não cadastrada.");
                }

                tabelaCalculoBMF.TipoMercado = TipoMercadoBMF.Futuro;   
                tabelaCalculoBMF.DataReferencia = data;
                tabelaCalculoBMF.CdAtivoBMF = cdAtivoBMF;
                tabelaCalculoBMF.Alfa = (decimal)valor;
            }
            return tabelaCalculoBMF;
        }

        /// <summary>
        /// Trata forçando o TipoMercado = Futuro.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="data"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        private TabelaCalculoBMF TrataBeta(string cdAtivoBMF, DateTime data, decimal valor)
        {
            TabelaCalculoBMF tabelaCalculoBMF = new TabelaCalculoBMF();
            if (tabelaCalculoBMF.LoadByPrimaryKey(data, cdAtivoBMF, TipoMercadoBMF.Futuro))
            {
                tabelaCalculoBMF.Beta = (decimal)valor;
            }
            else
            {
                ClasseBMF classeBMF = new ClasseBMF();
                if (!classeBMF.LoadByPrimaryKey(cdAtivoBMF))
                {
                    throw new ClasseBMFNaoCadastradaException("Classe de Ativo " + cdAtivoBMF + " não cadastrada.");
                }

                tabelaCalculoBMF.TipoMercado = TipoMercadoBMF.Futuro;
                tabelaCalculoBMF.DataReferencia = data;
                tabelaCalculoBMF.CdAtivoBMF = cdAtivoBMF;
                tabelaCalculoBMF.Beta = (decimal)valor;
            }
            return tabelaCalculoBMF;
        }        
	}
}
