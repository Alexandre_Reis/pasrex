﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0709.0
                       MyGeneration Version # 1.2.0.7
                           19/7/2007 13:35:04
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.BMF.Enums;

namespace Financial.BMF
{
	public partial class PosicaoBMFAbertura : esPosicaoBMFAbertura
	{
        /// <summary>
        /// Retorna o valor de mercado do cliente das posições em Disponivel e Opções (s/ Disponivel e Futuro).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, DateTime dataHistorico) {
            this.QueryReset();
            this.Query
                 .Select(this.query.ValorMercado.Sum())
                 .Where(this.query.IdCliente == idCliente,
                        this.query.DataHistorico == dataHistorico,
                        this.query.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo,
                                                  TipoMercadoBMF.Disponivel));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }
	}
}
