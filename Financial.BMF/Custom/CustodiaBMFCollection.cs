﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.BMF
{
	public partial class CustodiaBMFCollection : esCustodiaBMFCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(CustodiaBMFCollection));

        /// <summary>
        /// Deleta a CustodiaBMF para um cliente em uma data, somente de ativos que 
        /// calculam taxa de permanência <> "OZ1".
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaCustodiaPermanencia(int idCliente, DateTime data)
        {
            #region logEntrada
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCliente, this.Query.CdAtivoBMF, this.Query.IdAgente, this.Query.Data)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data == data,
                        this.Query.CdAtivoBMF.NotEqual("OZ1"));                 

            this.Query.Load();

            #region logSql
            if (log.IsInfoEnabled) {
                string sql = "Delete: " +this.query.es.LastQuery;
                sql = sql.Replace("@IdCliente1", "'" + idCliente + "'");
                sql = sql.Replace("@Data2", "'" + data.ToString("yyyy-MM-dd")+"'");
                sql = sql.Replace("@CdAtivoBMF3", "'OZ1'");
                log.Info(sql);
            }
            #endregion	    

            this.MarkAllAsDeleted();
            this.Save();

            #region logSaida
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
        }
	}
}
