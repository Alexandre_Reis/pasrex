﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common.Enums;
using Financial.Common;
using Financial.Common.Exceptions;
using log4net;
using Financial.BMF.Exceptions;
using Financial.Interfaces.Import.BMF;
using Financial.Interfaces.Import.Offshore;
using Financial.Util;


namespace Financial.BMF
{
	public partial class CotacaoBMF : esCotacaoBMF
	{
        /// <summary>
        /// Carrega o objeto CotacaoBMF com os campos PUFechamento, PUCorrigido, PUMedio, PUGerencial.
        /// CotacaoBMFNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="serie"></param>
        /// <param name="data"></param>
        public void BuscaCotacaoBMF(string cdAtivoBMF, string serie, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.PUFechamento, this.Query.PUCorrigido, this.Query.PUMedio, this.Query.PUGerencial)
                 .Where(this.Query.CdAtivoBMF == cdAtivoBMF,
                        this.Query.Serie == serie,
                        this.Query.Data == data);

            this.Query.Load();

            if (!this.es.HasData || !this.PUFechamento.HasValue)
            {
                throw new CotacaoBMFNaoCadastradoException("Cotação de " + cdAtivoBMF + serie + " não cadastrada na data " + data);
            }
        }

        /// <summary>
        /// Retorna a Ptax associada ao cdAtivoBMF. Retorna 1 para o caso de ativo não nominado em USD.        
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal BuscaPtaxAtivo(string cdAtivoBMF, DateTime data)
        {
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            AtivoBMF ativoBMF = new AtivoBMF();
            int idIndice;

            switch (cdAtivoBMF)
            {
                case "SC2":
                case "SC3":
                case "SCC":
                case "DDI":
                case "COT":
                case "ICF":
                    idIndice = ListaIndiceFixo.PTAX_800VENDA;
                    return cotacaoIndice.BuscaCotacaoIndice(idIndice, data);                    
                case "ISU":
                case "SOJ":
                case "ISP":
                    idIndice = ListaIndiceFixo.PTAX_REF;
                    return cotacaoIndice.BuscaCotacaoIndice(idIndice, data);                    
                default:
                    if (AtivoBMF.IsAtivoBond(cdAtivoBMF))
                    {
                        idIndice = ListaIndiceFixo.PTAX_800VENDA;
                        return cotacaoIndice.BuscaCotacaoIndice(idIndice, data);                        
                    }
                    else { 
                        return 1; 
                    }                    
            }
        }

        public void CarregaCotacaoOffShoreBloomberg(DateTime dataInicio, DateTime dataFim)
        {
            CotacaoBloomberg cotacaoBloomberg = new CotacaoBloomberg();
            CotacaoBMFCollection cotacaoBMFCollection = new CotacaoBMFCollection();

            #region Monta Lista de String
            AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();
            ativoBMFCollection.BuscaAtivoOffshore();
            
            if (ativoBMFCollection.Count == 0)
            {
                return;
            }

            StringBuilder cdAtivos = new StringBuilder();

            List<string> ativos = new List<string>();
            List<string> especs = new List<string>();

            for (int i = 0; i < ativoBMFCollection.Count; i++)
            {
                if (ativoBMFCollection[i].CodigoFeeder != null)
                {
                    if (ativoBMFCollection[i].CodigoFeeder.IndexOf(':') > 0)
                    {
                        string[] codigoFeeder = ativoBMFCollection[i].CodigoFeeder.Split(':');
                        ativos.Add(codigoFeeder[0]);
                        especs.Add(codigoFeeder[1]);
                    }
                }
            }
            #endregion

            List<CotacaoBloomberg> cotacaoOffShoreCollection = cotacaoBloomberg.CarregaCotacaoBloomberg(ativos, especs, dataInicio, dataFim);

            for (int i = 0; i < cotacaoOffShoreCollection.Count; i++)
            {
                cotacaoBloomberg = cotacaoOffShoreCollection[i];
                //
                bool achou = false;
                string serie = cotacaoBloomberg.CdAtivo.Substring(3, cotacaoBloomberg.CdAtivo.Length - 3);
                string cdAtivoBMF = cotacaoBloomberg.CdAtivo.Substring(0, 3);
                AtivoBMF ativoBMF = new AtivoBMF();
                if (ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie))
                {
                    achou = true;
                }

                if (achou)
                {
                    CotacaoBMF cotacaoBMFDeletar = new CotacaoBMF();
                    if (cotacaoBMFDeletar.LoadByPrimaryKey(cotacaoBloomberg.Data, cdAtivoBMF, serie))
                    {
                        cotacaoBMFDeletar.MarkAsDeleted();
                        cotacaoBMFDeletar.Save();
                    }

                    CotacaoBMF cotacaoBMF = cotacaoBMFCollection.AddNew();
                    cotacaoBMF.Data = cotacaoBloomberg.Data;
                    cotacaoBMF.CdAtivoBMF = cdAtivoBMF;
                    cotacaoBMF.Serie = serie;
                    cotacaoBMF.PUFechamento = cotacaoBloomberg.Pu;
                    cotacaoBMF.PUMedio = cotacaoBloomberg.Pu;
                }
            }

            try
            {
                cotacaoBMFCollection.Save();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Carrega as cotações a partir do arquivo BDPregao.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaCotacaoBdPregao(DateTime data, string pathArquivo)
        {
            BdPregao bdPregao = new BdPregao();
            CotacaoBMFCollection cotacaoBMFCollection = new CotacaoBMFCollection();
            CotacaoBMFCollection cotacaoBMFCollectionAux = new CotacaoBMFCollection();

            // Deleta as Cotações do dia
            cotacaoBMFCollectionAux.DeletaCotacaoBMF(data);

            DateTime diaAnterior = Util.Calendario.SubtraiDiaUtil(data, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            // Pega as cotações do arquivo de BdPregao                            
            BdPregaoCollection bdPregaoCollection = bdPregao.ProcessaBdPregao(data, pathArquivo);
            for (int i = 0; i < bdPregaoCollection.CollectionBdPregao.Count; i++)
            {
                bdPregao = bdPregaoCollection.CollectionBdPregao[i];

                AtivoBMF ativoBMF = new AtivoBMF();

                if (ativoBMF.LoadByPrimaryKey(bdPregao.CdAtivoBMF, bdPregao.Serie))
                {
                    CotacaoBMF cotacaoBMF = cotacaoBMFCollection.AddNew();
                    cotacaoBMF.Data = data;

                    cotacaoBMF.CdAtivoBMF = bdPregao.CdAtivoBMF;
                    cotacaoBMF.Serie = bdPregao.Serie;
                    cotacaoBMF.PUFechamento = bdPregao.CotacaoFechamento.HasValue ? bdPregao.CotacaoFechamento : 0;
                    cotacaoBMF.PUCorrigido = bdPregao.AjusteAnterior.HasValue ? bdPregao.AjusteAnterior : 0;
                    cotacaoBMF.PUMedio = bdPregao.CotacaoMedio.HasValue ? bdPregao.CotacaoMedio : 0;

                    // Se PUMedio ou PUFechamento = 0 então pega a cotação do dia anterior
                    if (!cotacaoBMF.PUFechamento.HasValue || cotacaoBMF.PUFechamento == 0)
                    {
                        CotacaoBMF cotacaoDiaAnterior = new CotacaoBMF();
                        cotacaoDiaAnterior.LoadByPrimaryKey(diaAnterior, cotacaoBMF.Serie, cotacaoBMF.CdAtivoBMF);
                        // Se Foi carregado valor do dia anterior Então substitui valor dos PUs no dia atual
                        if (cotacaoDiaAnterior.es.HasData)
                        {
                            cotacaoBMF.Data = data;
                            cotacaoBMF.PUCorrigido = cotacaoDiaAnterior.PUCorrigido;
                            cotacaoBMF.PUFechamento = cotacaoDiaAnterior.PUFechamento;
                            cotacaoBMF.PUMedio = cotacaoDiaAnterior.PUMedio;
                        }
                    }
                }
            }                       

            // Salva todas as cotacoes da collection           
            try
            {
                cotacaoBMFCollection.Save();
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }

            // Insere as cotações do dia anterior que não estão no dia de hoje
            this.InsereCotacaoAnterior(data, diaAnterior);            
        }

        /// <summary>
        /// Carrega as cotações a partir do arquivo BDPregao.
        /// Desconsidera Foreign Key de AtivoBMF
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaCotacaoBdPregaoSemFK(DateTime data, string pathArquivo) {
            BdPregao bdPregao = new BdPregao();
            CotacaoBMFCollection cotacaoBMFCollection = new CotacaoBMFCollection();
            CotacaoBMFCollection cotacaoBMFCollectionAux = new CotacaoBMFCollection();

            // Deleta as Cotações do dia
            cotacaoBMFCollectionAux.DeletaCotacaoBMF(data);

            DateTime diaAnterior = Util.Calendario.SubtraiDiaUtil(data, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            // Pega as cotações do arquivo de BdPregao                            
            BdPregaoCollection bdPregaoCollection = bdPregao.ProcessaBdPregao(data, pathArquivo);
            for (int i = 0; i < bdPregaoCollection.CollectionBdPregao.Count; i++) {
                bdPregao = bdPregaoCollection.CollectionBdPregao[i];

                AtivoBMF ativoBMF = new AtivoBMF();

                CotacaoBMF cotacaoBMF = cotacaoBMFCollection.AddNew();
                cotacaoBMF.Data = data;

                cotacaoBMF.CdAtivoBMF = bdPregao.CdAtivoBMF;
                cotacaoBMF.Serie = bdPregao.Serie;
                cotacaoBMF.PUFechamento = bdPregao.CotacaoFechamento.HasValue ? bdPregao.CotacaoFechamento : 0;
                cotacaoBMF.PUCorrigido = bdPregao.AjusteAnterior.HasValue ? bdPregao.AjusteAnterior : 0;
                cotacaoBMF.PUMedio = bdPregao.CotacaoMedio.HasValue ? bdPregao.CotacaoMedio : 0;

                // Se PUMedio ou PUFechamento = 0 então pega a cotação do dia anterior
                if (!cotacaoBMF.PUFechamento.HasValue || cotacaoBMF.PUFechamento == 0) {
                    CotacaoBMF cotacaoDiaAnterior = new CotacaoBMF();
                    cotacaoDiaAnterior.LoadByPrimaryKey(diaAnterior, cotacaoBMF.Serie, cotacaoBMF.CdAtivoBMF);
                    // Se Foi carregado valor do dia anterior Então substitui valor dos PUs no dia atual
                    if (cotacaoDiaAnterior.es.HasData) {
                        cotacaoBMF.Data = data;
                        cotacaoBMF.PUCorrigido = cotacaoDiaAnterior.PUCorrigido;
                        cotacaoBMF.PUFechamento = cotacaoDiaAnterior.PUFechamento;
                        cotacaoBMF.PUMedio = cotacaoDiaAnterior.PUMedio;
                    }
                }
            }

            // Salva todas as cotacoes da collection           
            try {
                cotacaoBMFCollection.Save();
            }
            catch (System.Data.SqlClient.SqlException e) {
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }

            // Insere as cotações do dia anterior que não estão no dia de hoje
            this.InsereCotacaoAnterior(data, diaAnterior);
        }

        /// <summary>
        /// Função para inserção usando Sql parametrizado + ExecuteNonQuery.        
        /// </summary>
        /// <param name="data"></param>
        /// <param name="dataAnterior"></param>
        /// <returns></returns>
        private int InsereCotacaoAnterior(DateTime data, DateTime dataAnterior)
        {
            StringBuilder sql = new StringBuilder();
            esParameters esParams = new esParameters();            

            esParams.Add("dataAnterior", dataAnterior);
            esParams.Add("data", data);

            sql.Append(" INSERT INTO [COTACAOBMF] ([Data], [CdAtivoBMF], [Serie], [PUFechamento], [PUMedio], [PUCorrigido])");
            sql.Append(" SELECT @data, [CdAtivoBMF], [Serie], [PUFechamento], [PUMedio], [PUCorrigido] ");
            sql.Append(" FROM [COTACAOBMF] ");
            sql.Append(" WHERE [Data] = @dataAnterior ");
            sql.Append(" AND [CdAtivoBMF]+[Serie] NOT IN (SELECT [CdAtivoBMF]+[Serie] FROM COTACAOBMF ");
            sql.Append(" WHERE [Data] = @data " + ")");
            
            int retorno;
            using (esTransactionScope scope = new esTransactionScope())
            {
                retorno = this.ExecuteNonQuery(esQueryType.Text, sql.ToString(), esParams);
                scope.Complete();                
            }

            return retorno;
        }

        /// <summary>
        /// Carrega CotacaoBMF a partir do site do yahoo
        /// Exemplo: http://finance.yahoo.com/d/quotes.csv?s=XOM+BBDb.TO+&f=sl1
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaCotacaoOffShore(DateTime data, string pathArquivo)
        {
            CotacaoOffShore cotacaoOffShore = new CotacaoOffShore();
            //
            CotacaoBMFCollection cotacaoBMFCollection = new CotacaoBMFCollection();

            #region Monta Lista de String
            /* 1 elemento = até 100 elementos separados por + se existir
             * 2 elemento = mais 100 elementos se existir separados por +
             */
            AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();
            ativoBMFCollection.BuscaAtivoOffshore();
            // 

            // Se não tem Ativos não faz nada
            if (ativoBMFCollection.Count == 0)
            {
                return;
            }

            StringBuilder cdAtivos = new StringBuilder();

            const int QTD_ATIVOS = 100; // Quantidade de ativos por Grupo
            int tamanhoLista = (int)Math.Ceiling(ativoBMFCollection.Count / 100M);

            List<string> ativos = new List<string>(tamanhoLista);

            for (int i = 0; i < ativoBMFCollection.Count; i++)
            {
                //Faz o replace pois o Yahoo só entende sufixos com -
                string cdAtivo = "";
                if (!String.IsNullOrEmpty(ativoBMFCollection[i].CodigoFeeder))
                {
                    cdAtivo = ativoBMFCollection[i].CodigoFeeder;
                }
                else
                {
                    cdAtivo = ativoBMFCollection[i].CdAtivoBMF + ativoBMFCollection[i].Serie;
                }

                cdAtivo = cdAtivo.Replace("/", "-").Replace(".", "-");
                cdAtivo = cdAtivo + ".CME";

                cdAtivos.Append(cdAtivo + "+");

                if (i % QTD_ATIVOS == 0 && i != 0)
                { // Multiplos de QTD_ATIVOS
                    ativos.Add(cdAtivos.ToString());
                    cdAtivos = new StringBuilder(); // Zera a String     
                }
            }
            // Adiciona o ultimo grupo
            if (!String.IsNullOrEmpty(cdAtivos.ToString()))
            {
                ativos.Add(cdAtivos.ToString());
            }
            #endregion

            // Pega as Cotações do arquivo do .csv yahoo 
            List<CotacaoOffShore> cotacaoOffShoreCollection = cotacaoOffShore.ProcessaCotacaoOffShore(ativos, data, pathArquivo);

            for (int i = 0; i < cotacaoOffShoreCollection.Count; i++)
            {
                cotacaoOffShore = cotacaoOffShoreCollection[i];
                //
                bool achou = false;
                string serie = Utilitario.Right(cotacaoOffShore.CdAtivo.Replace(".CME", "").Replace("\"", "").Replace("-", "/"), 3);
                string cdAtivoBMF = cotacaoOffShore.CdAtivo.Replace(".CME", "").Replace("\"", "").Replace("-", "/").Replace(serie, "");                
                AtivoBMF ativoBMF = new AtivoBMF();
                if (!ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie))
                {
                    AtivoBMFCollection ativoBMFCollectionAux = new AtivoBMFCollection();
                    ativoBMFCollectionAux.Query.Where(ativoBMFCollectionAux.Query.CodigoFeeder.Equal(cdAtivoBMF + serie));
                    ativoBMFCollectionAux.Query.Load();

                    if (ativoBMFCollectionAux.Count > 0)
                    {
                        cdAtivoBMF = ativoBMFCollectionAux[0].CdAtivoBMF;
                        serie = ativoBMFCollectionAux[0].Serie;
                        achou = true;
                    }
                }
                else
                {
                    achou = true;
                }
                
                if (achou)
                {
                    CotacaoBMF cotacaoBMFDeletar = new CotacaoBMF();
                    if (cotacaoBMFDeletar.LoadByPrimaryKey(cotacaoOffShore.Data, cdAtivoBMF, serie))
                    {
                        cotacaoBMFDeletar.MarkAsDeleted();
                        cotacaoBMFDeletar.Save();
                    }

                    CotacaoBMF cotacaoBMF = cotacaoBMFCollection.AddNew();
                    cotacaoBMF.Data = cotacaoOffShore.Data;
                    cotacaoBMF.CdAtivoBMF = cdAtivoBMF;
                    cotacaoBMF.Serie = serie;
                    cotacaoBMF.PUFechamento = cotacaoOffShore.Pu;
                    cotacaoBMF.PUMedio = cotacaoOffShore.Pu;
                }
            }

            // Salva Todas as Cotações da Collection           
            try
            {
                cotacaoBMFCollection.Save();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            //Carrega as cotações do dia anterior que não tenham sido carregadas na data
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)LocalFeriadoFixo.NovaYork, TipoFeriado.Outros);
            foreach (AtivoBMF ativoBMFCotacao in ativoBMFCollection)
            {
                string cdAtivoBMFCotacao = ativoBMFCotacao.CdAtivoBMF;
                string serieCotacao = ativoBMFCotacao.Serie;
                CotacaoBMF cotacaoBMFChecar = new CotacaoBMF();
                if (!cotacaoBMFChecar.LoadByPrimaryKey(data, cdAtivoBMFCotacao, serieCotacao))
                {
                    CotacaoBMF cotacaoBMFAnterior = new CotacaoBMF();
                    if (cotacaoBMFAnterior.LoadByPrimaryKey(dataAnterior, cdAtivoBMFCotacao, serieCotacao))
                    {
                        CotacaoBMF cotacaoBMFInserir = new CotacaoBMF();
                        cotacaoBMFInserir.Data = data;
                        cotacaoBMFInserir.CdAtivoBMF = cdAtivoBMFCotacao;
                        cotacaoBMFInserir.Serie = serieCotacao;
                        cotacaoBMFInserir.PUFechamento = cotacaoBMFAnterior.PUFechamento;
                        cotacaoBMFInserir.PUMedio = cotacaoBMFAnterior.PUMedio;
                        cotacaoBMFInserir.Save();
                    }
                }
            }
        }
        
	}
}
