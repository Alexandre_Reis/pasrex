﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common.Enums;
using log4net;

namespace Financial.BMF
{
	public partial class CotacaoBMFCollection : esCotacaoBMFCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(CotacaoBMFCollection));

        /// <summary>
        /// Deleta todas as cotações de BMF para a data passada.
        /// </summary>
        /// <param name="data"></param>
        public void DeletaCotacaoBMF(DateTime data)
        {

            #region logEntrada
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            this.QueryReset();
            this.Query
              .Select(this.query.CdAtivoBMF, this.query.Serie, this.query.Data)
              .Where(this.query.Data.Equal(data));

            this.Query.Load();

            #region logSql
            if (log.IsInfoEnabled) {
                string sql = "Delete: " +this.query.es.LastQuery;
                sql = sql.Replace("@Data1", "'" + data.ToString("yyyy-MM-dd") + "'");
                log.Info(sql);
            }
            #endregion	         

            this.MarkAllAsDeleted();
            this.Save();

            #region logSaida
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
        }        
	}
}
