using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.BMF.Enums;

namespace Financial.BMF
{
	public partial class PosicaoBMFCollection : esPosicaoBMFCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(PosicaoBMFCollection));

        // Construtor
        // Cria uma nova PosicaoBMFCollection com os dados de PosicaoBMFHistoricoCollection
        // Todos do dados de PosicaoBMFHistoricoCollection são copiados com excessão da dataHistorico
        public PosicaoBMFCollection(PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection) {
            for (int i = 0; i < posicaoBMFHistoricoCollection.Count; i++) {
                //
                PosicaoBMF p = new PosicaoBMF();

                // Para cada Coluna de PosicaoBMFHistorico copia para PosicaoBMF
                foreach (esColumnMetadata colPosicaoHistorico in posicaoBMFHistoricoCollection.es.Meta.Columns) {
                    // Copia todas as colunas menos a Data Historico
                    if (colPosicaoHistorico.PropertyName != PosicaoBMFHistoricoMetadata.ColumnNames.DataHistorico) {
                        esColumnMetadata colPosicaoBMF = p.es.Meta.Columns.FindByPropertyName(colPosicaoHistorico.PropertyName);
                        if (posicaoBMFHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name) != null) {
                            p.SetColumn(colPosicaoBMF.Name, posicaoBMFHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name));
                        }
                    }
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMFCollection com todos os campos de PosicaoBMF.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoBMFCompleta(int idCliente)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente);
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMFCollection com os campos IdPosicao, CdAtivoBMF, Serie, IdAgente, 
        /// TipoMercado, Quantidade, AjusteDiario, AjusteAcumulado.
        /// Busca posições de futuros e DLA.
        /// </summary>
        /// <param name="idCliente"></param>        
        public void BuscaPosicaoBMFCalculoAjuste(int idCliente)
        {   
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.CdAtivoBMF, this.Query.Serie, this.Query.IdAgente,
                         this.Query.TipoMercado, this.Query.Quantidade, this.Query.AjusteDiario,
                         this.Query.AjusteAcumulado, this.Query.IdConta)
                 .Where(this.Query.IdCliente == idCliente &
                        (
                            this.Query.TipoMercado == TipoMercadoBMF.Futuro |
                            this.Query.CdAtivoBMF == ("DLA")
                        )                         
                  );
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMFCollection com os campos CdAtivoBMF, Serie, IdAgente, AjusteDiario.Sum().
        /// Busca posições de futuros e DLA.
        /// GroupBy(this.Query.CdAtivoBMF, this.Query.Serie, this.Query.IdAgente)
        /// </summary>
        /// <param name="idCliente"></param>        
        public void BuscaPosicaoBMFAjusteDiarioAnalitico(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBMF, this.Query.Serie, this.Query.IdAgente, this.Query.IdConta, this.Query.AjusteDiario.Sum())
                 .Where(this.Query.IdCliente == idCliente &
                            (
                                this.Query.TipoMercado == TipoMercadoBMF.Futuro |
                                this.Query.CdAtivoBMF == ("DLA")
                            )
                        )
                 .GroupBy(this.Query.CdAtivoBMF, this.Query.Serie, this.Query.IdAgente, this.Query.IdConta);
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMFCollection com os campos IdAgente, TipoMercado, AjusteDiario.Sum().
        /// Busca posições de futuros e DLA.
        /// .GroupBy(this.Query.IdAgente, this.Query.TipoMercado).
        /// </summary>
        /// <param name="idCliente"></param>        
        public void BuscaPosicaoBMFAjusteDiarioConsolidado(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgente, this.Query.TipoMercado, this.Query.AjusteDiario.Sum())
                 .Where(this.Query.IdCliente == idCliente &
                            (
                                this.Query.TipoMercado == TipoMercadoBMF.Futuro |
                                this.Query.CdAtivoBMF == ("DLA")
                            )
                       )
                 .GroupBy(this.Query.IdAgente, this.Query.TipoMercado);
                        
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMFCollection com os campos IdPosicao, IdAgente, CdAtivoBMF, Serie, 
        /// TipoMercado, Quantidade, DataVencimento.
        /// Busca posições de futuros de Dolar/Euro vencendo em D+1 da data, e outros vencendo no dia.
        /// </summary>
        /// <param name="idCliente"></param>        
        public void BuscaPosicaoBMFCalculoTaxasVencimento(int idCliente, DateTime data, DateTime dataPosterior)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.IdAgente, this.Query.CdAtivoBMF, this.Query.Serie, 
                         this.Query.TipoMercado, this.Query.Quantidade, this.Query.DataVencimento)
                 .Where(this.Query.IdCliente == idCliente &
                        (
                            (
                                this.Query.TipoMercado == TipoMercadoBMF.Futuro &
                                this.Query.CdAtivoBMF.In("DOL","WDL","EUR") &
                                this.Query.DataVencimento.Equal(dataPosterior)
                            )
                            |
                            (
                                this.Query.TipoMercado == TipoMercadoBMF.Futuro &
                                this.Query.CdAtivoBMF.NotIn("DOL","WDL","EUR") &
                                this.Query.DataVencimento.Equal(data)
                            )           
                        )
                 );
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMFCollection com os campos IdPosicao, CdAtivoBMF, Serie, Quantidade, 
        /// PUCustoLiquido, ValorMercado, ValorCustoLiquido, ResultadoRealizar, PUMercado.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoBMF(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.CdAtivoBMF, this.Query.Serie, this.Query.Quantidade,
                         this.Query.PUCustoLiquido, this.Query.ValorMercado, this.Query.ValorCustoLiquido,
                         this.Query.ResultadoRealizar, this.Query.PUMercado)
                 .Where(this.Query.IdCliente == idCliente);

            this.Query.Load();
        }

        /// <summary>
        /// Deleta todas as posições do cliente passado.
        /// </summary>
        /// <param name="idCliente"></param>
        public void DeletaPosicaoBMF(int idCliente) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.query.IdPosicao)
                 .Where(this.query.IdCliente == idCliente);
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Método de inserção em PosicaoBMF, a partir de PosicaoBMFHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void InserePosicaoBMFHistorico(int idCliente, DateTime dataHistorico)
        {
            #region Copia de posicaoBMFHistorico para PosicaoBMF
            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = string.Empty;
            string strFrom = string.Empty;

            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoBMF ON ");
            sqlBuilder.Append("INSERT INTO PosicaoBMF (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoBMFHistorico WHERE DataHistorico = " + "'" + dataHistorico.ToString("yyyyMMdd") + "' AND IdCliente = " + idCliente;
            int count = 0;

            PosicaoBMFHistorico posicaoBMFHistorico = new PosicaoBMFHistorico();
            int columnCount = posicaoBMFHistorico.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoBMF in posicaoBMFHistorico.es.Meta.Columns)
            {
                count++;

                if (colPosicaoBMF.Name == PosicaoBMFHistoricoMetadata.ColumnNames.DataHistorico)
                    continue;

                //Insert
                sqlBuilder.Append(colPosicaoBMF.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                strSelect += colPosicaoBMF.Name;

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();
            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoBMF OFF ");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());
            #endregion
        }


        /// <summary>
        /// Método de inserção em PosicaoBMF, a partir de PosicaoBMFAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataAbertura"></param>
        public void InserePosicaoBMFAbertura(int idCliente, DateTime dataAbertura)
        {
            #region Copia de posicaoBMFHistorico para PosicaoBMF
            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = string.Empty;
            string strFrom = string.Empty;

            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoBMF ON ");
            sqlBuilder.Append("INSERT INTO PosicaoBMF (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoBMFAbertura WHERE DataHistorico = " + "'" + dataAbertura.ToString("yyyyMMdd") + "' AND IdCliente = " + idCliente;
            int count = 0;

            PosicaoBMFAbertura posicaoBMFAbertura = new PosicaoBMFAbertura();
            int columnCount = posicaoBMFAbertura.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoBMF in posicaoBMFAbertura.es.Meta.Columns)
            {
                count++;

                if (colPosicaoBMF.Name == PosicaoBMFAberturaMetadata.ColumnNames.DataHistorico)
                    continue;

                //Insert
                sqlBuilder.Append(colPosicaoBMF.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                strSelect += colPosicaoBMF.Name;

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();
            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoBMF OFF ");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());
            #endregion
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMFCollection com o campo IdPosicao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataVencimento"></param>
        public void BuscaPosicaoBMFVencimentoDia(int idCliente, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataVencimento.Equal(dataVencimento));

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMFCollection com os campos IdAgente, CdAtivoBMF, Serie, Quantidade.        
        /// Filtra por CdAtivoBMF IN ("DLA", "DOL", "IDI").
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="cdAtivoBMF">Usado o formato IN()</param>
        public void BuscaPosicaoBMFExercicioDia(int idCliente, DateTime dataVencimento, int tipoMercado)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgente, this.Query.CdAtivoBMF, this.Query.Serie, this.Query.Quantidade)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataVencimento.Equal(dataVencimento),
                        this.Query.TipoMercado.Equal(tipoMercado), 
                        this.Query.CdAtivoBMF.In("DLA", "DOL", "IDI"));

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMFCollection com os campos CdAtivoBMF, Serie, TipoMercado, Quantidade.Sum, PUMercado.Avg, PUCustoLiquido.Avg, 
        /// ValorMercado.Sum.
        /// Group By CdAtivoBMF, Serie
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>indica se retornou registro</returns>
        public bool BuscaPosicaoBMFAgrupado(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBMF,
                         this.Query.Serie,
                         this.Query.TipoMercado,
                         this.Query.Quantidade.Sum(),
                         this.Query.PUMercado.Avg(),
                         this.Query.PUCustoLiquido.Avg(),
                         this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente)
                 .GroupBy(this.Query.CdAtivoBMF,
                          this.Query.Serie,
                          this.Query.TipoMercado);

            bool retorno = this.Query.Load();

            return retorno;
        }
	}
}
