﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.BMF.Enums;
using log4net;
using Financial.Util;
using Financial.BMF.Exceptions;
using Financial.Common.Enums;
using Financial.Common;
using Financial.Interfaces.Import.BMF;
using Financial.Interfaces.Sinacor;

namespace Financial.BMF {
    public partial class AtivoBMF : esAtivoBMF 
    {
        /// <summary>
        /// Retorna o CdAtivoBMF de um ativo informado como junção entre CdAtivoBMF + Serie.
        /// </summary>
        /// <param name="ativoBMF"></param>
        /// <returns></returns>
        public string RetornaCdAtivoSplitado(string ativoBMF)
        {
            string cdAtivoBMF = "";

            if (ativoBMF.Length > 3)
            {
                string[] strAtivo = ativoBMF.Split('-');
                return strAtivo[0];
            }

            return cdAtivoBMF;
        }

        /// <summary>
        /// Retorna a série de um ativo informado como junção entre CdAtivoBMF + Serie.
        /// </summary>
        /// <param name="ativoBMF"></param>
        /// <returns></returns>
        public string RetornaSerieSplitada(string ativoBMF)
        {
            string serie = "";

            if (ativoBMF.Length > 3)
            {
                string[] strAtivo = ativoBMF.Split('-');
                return strAtivo[1];        
            }

            return serie;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <returns>bool indicando se o ativo é agricola e nominado em reais.</returns>
        public static bool IsAgricolaReais(string cdAtivoBMF) {
            return cdAtivoBMF == "ALA" || cdAtivoBMF == "BGI" ||
                   cdAtivoBMF == "BZE" || cdAtivoBMF == "CNI";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <returns>bool indicando se o ativo é agricola e nominado em U$.</returns>
        public static bool IsAgricolaCambial(string cdAtivoBMF) {
            return cdAtivoBMF == "COT" || cdAtivoBMF == "ICF" ||
                   cdAtivoBMF == "ISU" || cdAtivoBMF == "SOJ" || cdAtivoBMF == "ETN" || cdAtivoBMF == "ETH";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <returns>bool indicando se o ativo é vinculado ao câmbio.</returns>
        public static bool IsAtivoCambial(string cdAtivoBMF) {
            return AtivoBMF.IsAgricolaCambial(cdAtivoBMF) ||
                   AtivoBMF.IsAtivoBond(cdAtivoBMF) ||
                   cdAtivoBMF == "DDI";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <returns>bool indicando se o ativo é Bradie (B09 até B40).</returns>
        public static bool IsAtivoBond(string cdAtivoBMF) {
            return (cdAtivoBMF.Length == 3 && cdAtivoBMF.Substring(0, 1) == "B" && 
                        Utilitario.IsInteger(cdAtivoBMF.Substring(1, 2)) || cdAtivoBMF == "T10");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <returns>bool indicando se o ativo é negociado em NovaYork</returns>
        public static bool IsNegociadoNovaYork(string cdAtivoBMF) {
            return cdAtivoBMF == "SOJ" || cdAtivoBMF == "BZE" || cdAtivoBMF == "BGI" || cdAtivoBMF == "WBG" ||
                   cdAtivoBMF == "ISU" || cdAtivoBMF == "ALA" || cdAtivoBMF == "CNL" || cdAtivoBMF == "ICF" ||
                   cdAtivoBMF == "CNI" || cdAtivoBMF == "COT" || cdAtivoBMF == "WCF" || cdAtivoBMF == "ETN";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se o mercado do ativo é A Vista</returns>
        public bool IsMercadoVista() {
            return this.TipoMercado == TipoMercadoBMF.Disponivel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se o mercado do ativo é Futuro</returns>
        public bool IsMercadoFuturo() {
            return this.TipoMercado == TipoMercadoBMF.Futuro;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se o mercado do ativo é Opcao sobre disponivel</returns>
        public bool IsMercadoOpcaoFuturo() {
            return this.TipoMercado == TipoMercadoBMF.OpcaoFuturo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se o mercado do ativo é Termo</returns>
        public bool IsMercadoTermo() {
            return this.TipoMercado == TipoMercadoBMF.Termo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se o ativo é de juros (DI1, DID, DDI)</returns>
        public static bool IsAtivoJuros(string cdAtivoBMF) {
            return (cdAtivoBMF == "DI1" || cdAtivoBMF == "DDI");
        }

        /// <summary>
        /// Retorna DataVencimento.
        /// AtivoBMFNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="serie"></param>
        /// <param name="tipoMercado"></param>
        /// <returns></returns>
        public DateTime BuscaDataVencimento(string cdAtivoBMF, string serie, int tipoMercado) {
            
            this.QueryReset();
            this.Query
                 .Select(this.query.DataVencimento)
                 .Where(this.query.CdAtivoBMF.Equal(cdAtivoBMF),
                        this.query.Serie.Equal(serie),
                        this.query.TipoMercado == tipoMercado);

            this.Query.Load();

            if (!this.es.HasData) {
                throw new AtivoBMFNaoCadastradoException("AtivoBMF " + cdAtivoBMF + serie + " não cadastrado. Mercado=" + tipoMercado);
            }
            
            return this.DataVencimento.Value;
        }

        /// <summary>
        /// Retorna o PU equivalente, dada a taxa e prazo informados. Aplica-se a ativos vinculados a tx de juros.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="taxa"></param>
        /// <param name="prazo"></param>
        /// <returns>PU convertido</returns>
        public decimal ConverteTaxaPU(string cdAtivoBMF, decimal taxa, int prazo) {
            decimal baseExponenciacao;
            decimal expoente;
            decimal pu;

            switch (cdAtivoBMF) {
                case "DI1":
                    baseExponenciacao = (1 + taxa / 100M);
                    expoente = (prazo / 252M);
                    pu = (decimal)Math.Pow((double)baseExponenciacao, (double)expoente);
                    break;
                default:
                    pu = (taxa / 36000M) * prazo + 1M;
                    break;
            }

            return Math.Round(100000 / pu, 2);
        }

        /// <summary>
        /// Carrega na AtivoBMF os ativos integrados do BdPregao
        /// </summary>
        /// <param name="data">Data de referência do BdPregao</param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaAtivoBdPregao(DateTime data, string pathArquivo) 
        {        
            BdPregao bdPregao = new BdPregao();
            BdPregaoCollection bdPregaoCollection = bdPregao.ProcessaBdPregao(data, pathArquivo);

            // Transação
            using (esTransactionScope scope = new esTransactionScope()) {
                for (int i = 0; i < bdPregaoCollection.CollectionBdPregao.Count; i++) {
                    bdPregao = bdPregaoCollection.CollectionBdPregao[i];

                    #region Copia informações para AtivoBMF
                    AtivoBMF ativoBMF = new AtivoBMF();

                    /* Verifica se registro existe na AtivoBMF
                     * Se registro existe = update
                     * Se registro não existe = insert
                     */
                    // Update
                    if (ativoBMF.LoadByPrimaryKey(bdPregao.CdAtivoBMF, bdPregao.Serie)) {
                        #region Update
                        ativoBMF.TipoSerie = bdPregao.TipoSerie;
                        ativoBMF.PrecoExercicio = bdPregao.PrecoExercicio;
                        ativoBMF.DataVencimento = bdPregao.DataVencimento;
                        // Update do Ativo
                        ativoBMF.Save();
                        #endregion
                    }
                    // Insert
                    else {
                        #region Insert

                        #region Trata ClasseBMF
                        // Verifica se registro não existe na ClasseBMF e insere se for o caso                         
                        ClasseBMF classeBMF = new ClasseBMF();
                        if (!classeBMF.LoadByPrimaryKey(bdPregao.CdAtivoBMF)) {
                            classeBMF.CdAtivoBMF = bdPregao.CdAtivoBMF;
                            classeBMF.Save();
                        }
                        #endregion

                        ativoBMF.IdMoeda = (int)ListaMoedaFixo.Real;
                        ativoBMF.CdAtivoBMF = bdPregao.CdAtivoBMF;
                        ativoBMF.Serie = bdPregao.Serie;
                        ativoBMF.TipoMercado = (byte)bdPregao.TipoMercado;
                        ativoBMF.TipoSerie = bdPregao.TipoSerie;
                        ativoBMF.PrecoExercicio = bdPregao.PrecoExercicio;
                        ativoBMF.DataVencimento = bdPregao.DataVencimento;
                        ativoBMF.Peso = bdPregao.Peso;
                        ativoBMF.CodigoIsin = "";
                        // Insert do Ativo                                
                        ativoBMF.Save();
                        #endregion
                    }
                    #endregion

                    #region Copia informações para TabelaVencimentosBMF
                    TabelaVencimentosBMF tabelaVencimentosBMF = new TabelaVencimentosBMF();

                    /* Verifica se registro existe na TabelaVencimentosBMF
                     * Se registro existe = update
                     * Se registro não existe = insert
                     */
                    // Update
                    if (tabelaVencimentosBMF.LoadByPrimaryKey(data, bdPregao.CdAtivoBMF, bdPregao.Serie)) {
                        #region Update
                        tabelaVencimentosBMF.DiasCorridos = bdPregao.NumeroDiasCorridos;
                        tabelaVencimentosBMF.DiasUteis = bdPregao.NumeroDiasUteis;
                        // Update de TabelaVencimentosBMF
                        tabelaVencimentosBMF.Save();
                        #endregion
                    }
                    // Insert
                    else {
                        #region Insert
                        tabelaVencimentosBMF.DataReferencia = bdPregao.Data;
                        tabelaVencimentosBMF.CdAtivoBMF = bdPregao.CdAtivoBMF;
                        tabelaVencimentosBMF.Serie = bdPregao.Serie;
                        tabelaVencimentosBMF.DiasCorridos = bdPregao.NumeroDiasCorridos;
                        tabelaVencimentosBMF.DiasUteis = bdPregao.NumeroDiasUteis;
                        // Insert de TabelaVencimentosBMF                              
                        tabelaVencimentosBMF.Save();
                        #endregion
                    }
                    #endregion
                }
                scope.Complete();
            }
        }

        /// <summary>
        /// Retorna o número de dias úteis entre a data passada e o vencimento do ativo.
        /// Preferencialmente usa a tabela de vencimentos. Se não tiver dados, calcula o prazo.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="serie"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int RetornaPrazoDiasUteis(string cdAtivoBMF, string serie, DateTime data) 
        {
            //Procuro antes na própria tabela de vencimentos.
            TabelaVencimentosBMF tabelaVencimentosBMF = new TabelaVencimentosBMF();
            if (tabelaVencimentosBMF.BuscaTabelaVencimentosBMF(cdAtivoBMF, serie, data)) {
                return tabelaVencimentosBMF.DiasUteis.Value;
            }

            //Se não existir na tabela de vencimentos, calculo aqui mesmo o prazo.
            this.QueryReset();
            this.Query
                 .Select(this.query.DataVencimento)
                 .Where(this.query.CdAtivoBMF == cdAtivoBMF,
                        this.query.Serie == serie);

            this.Query.Load();

            if (!this.DataVencimento.HasValue) {
                throw new DataVencimentoInvalidaException("Ativo " + cdAtivoBMF + serie + " possui data de vencimento nula.");
            }

            return Calendario.NumeroDias(data, this.DataVencimento.Value, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
        }

        public int RetornaPrazoDiasUteisBrasil(string cdAtivoBMF, string serie, DateTime data)
        {
            //Procuro antes na própria tabela de vencimentos.
            TabelaVencimentosBMF tabelaVencimentosBMF = new TabelaVencimentosBMF();
            if (tabelaVencimentosBMF.BuscaTabelaVencimentosBMF(cdAtivoBMF, serie, data))
            {
                return tabelaVencimentosBMF.DiasUteis.Value;
            }

            //Se não existir na tabela de vencimentos, calculo aqui mesmo o prazo.
            this.QueryReset();
            this.Query
                 .Select(this.query.DataVencimento)
                 .Where(this.query.CdAtivoBMF == cdAtivoBMF,
                        this.query.Serie == serie);

            this.Query.Load();

            if (!this.DataVencimento.HasValue)
            {
                throw new DataVencimentoInvalidaException("Ativo " + cdAtivoBMF + serie + " possui data de vencimento nula.");
            }

            return Calendario.NumeroDias(data, this.DataVencimento.Value, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        }

        /// <summary>
        /// Retorna o número de dias corridos entre a data passada e o vencimento do ativo.
        /// Preferencialmente usa a tabela de vencimentos. Se não tiver dados, calcula o prazo.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="serie"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int RetornaPrazoDiasCorridos(string cdAtivoBMF, string serie, DateTime data)
        {
            //Procuro antes na própria tabela de vencimentos.
            TabelaVencimentosBMF tabelaVencimentosBMF = new TabelaVencimentosBMF();
            if (tabelaVencimentosBMF.BuscaTabelaVencimentosBMF(cdAtivoBMF, serie, data))
            {
                return tabelaVencimentosBMF.DiasCorridos.Value;
            }

            //Se não existir na tabela de vencimentos, calculo aqui mesmo o prazo.
            this.QueryReset();
            this.Query
                 .Select(this.query.DataVencimento)
                 .Where(this.query.CdAtivoBMF == cdAtivoBMF,
                        this.query.Serie == serie);

            this.Query.Load();

            if (!this.DataVencimento.HasValue)
            {
                throw new DataVencimentoInvalidaException("Ativo " + cdAtivoBMF + serie + " possui data de vencimento nula.");
            }

            return Calendario.NumeroDias(data, this.DataVencimento.Value);
        }

        /// <summary>
        /// Calcula a Fórmula Base do emolumento previsto pela BMF e retorna o Emolumento Unitário.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="ativoBMF">objeto com DataVencimento, Serie.</param>
        /// <param name="tabelaCalculoBMF">objeto com os parâmetros para cálculo de emolumento unitário.</param>
        /// <param name="diasUteis">dias úteis entre a data passada e o vencimento do ativo.</param>
        /// <param name="diasCorridos">dias corridos entre a data passada e o vencimento do ativo.</param>
        /// <param name="pontaEstrategia">só se aplica a operações oriundas de estratégias (VOI, VID etc).</param>
        /// <returns></returns>
        public decimal RetornaFormulaBaseEmolumento(DateTime data, AtivoBMF ativoBMF, TabelaCalculoBMF tabelaCalculoBMF,
                                                     int diasUteis, int diasCorridos, int pontaEstrategia) {
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            decimal formulaBase = 0;
            DateTime primeiroDiaMes;
            DateTime ultimoDiaMesAnterior;
            int prazo;
            decimal multiplicador;
            decimal baseParte1;
            decimal expoenteParte1;
            decimal parte1;
            decimal baseParte2;
            decimal expoenteParte2;
            decimal parte2;
            decimal baseParte3;
            decimal expoenteParte3;
            decimal parte3;
            decimal valorIndice;

            #region Ativo
            switch (ativoBMF.CdAtivoBMF) {
                case "DI1":
                case "IDI":
                case "DIA":
                case "D11":
                case "D12":
                case "D13":
                    #region (Contratos Baseados em Taxa de Juro Prefixada em Reais) DI1, IDI, DIA, D11, D12, D13
                    cotacaoIndice = new CotacaoIndice();
                    if (ativoBMF.CdAtivoBMF == "IDI") {
                        primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                        ultimoDiaMesAnterior = Calendario.SubtraiDiaUtil(primeiroDiaMes, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                        multiplicador = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.IDI, ultimoDiaMesAnterior);
                    }
                    else {
                        multiplicador = ativoBMF.Peso.Value;
                    }
                    prazo = diasUteis;

                    baseParte1 = (decimal)(1 + tabelaCalculoBMF.P.Value / 100M);
                    expoenteParte1 = (decimal)(tabelaCalculoBMF.DiasMinimo.Value / 252M);
                    parte1 = (decimal)(Math.Pow((double)baseParte1, (double)expoenteParte1) - 1);

                    baseParte2 = (decimal)(1 + tabelaCalculoBMF.P.Value / 100M);
                    expoenteParte2 = (decimal)(prazo / 252M);
                    parte2 = (decimal)(Math.Pow((double)baseParte2, (double)expoenteParte2) - 1);

                    baseParte3 = (decimal)(1 + tabelaCalculoBMF.P.Value / 100M);
                    expoenteParte3 = (decimal)(tabelaCalculoBMF.DiasMaximo.Value / 252M);
                    parte3 = (decimal)(Math.Pow((double)baseParte3, (double)expoenteParte3) - 1);

                    formulaBase = 100000M * Math.Max(parte1, Math.Min(parte2, parte3));

                    if (ativoBMF.CdAtivoBMF == "DIA" || ativoBMF.CdAtivoBMF == "IDI") {
                        formulaBase = formulaBase * multiplicador / 100000M;
                    }

                    if (ativoBMF.TipoMercado == TipoMercadoBMF.OpcaoDisponivel ||
                        ativoBMF.TipoMercado == TipoMercadoBMF.OpcaoFuturo) {
                        formulaBase = formulaBase * 0.3M; //Opção de juros tem 30% de desconto s/ a FB.
                    }

                    //Desconto para todos os ativos a partir de 02/01/2006.
                    formulaBase = Utilitario.Truncate(formulaBase * 0.95M, 2);
                    return formulaBase;
                    #endregion

                case "DDI":
                case "SCC":
                case "SC2":
                case "SC3":
                    #region (Contratos Baseados em Taxa de Juro Indexada à Taxa de cambio em dolares) DDI, SCC, SC2, SC3
                    cotacaoIndice = new CotacaoIndice();
                    primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                    ultimoDiaMesAnterior = Calendario.SubtraiDiaUtil(primeiroDiaMes, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.PTAX_800VENDA, ultimoDiaMesAnterior);
                    prazo = diasCorridos;

                    parte1 = (tabelaCalculoBMF.P.Value / 100M) * (tabelaCalculoBMF.DiasMinimo.Value / 360M);
                    parte2 = (tabelaCalculoBMF.P.Value / 100M) * (prazo / 360M);
                    parte3 = (tabelaCalculoBMF.P.Value / 100M) * (tabelaCalculoBMF.DiasMaximo.Value / 360M);

                    if (ativoBMF.CdAtivoBMF == "SC2" || ativoBMF.CdAtivoBMF == "SC3") {
                        formulaBase = Utilitario.Truncate(50000 * Math.Max(parte1, Math.Min(parte2, parte3)), 4);
                    }
                    else {
                        formulaBase = Utilitario.Truncate(50000M * Math.Max(parte1, Math.Min(parte2, parte3)), 7);
                    }

                    //Desconto para todos os ativos a partir de 02/01/2006.
                    formulaBase = formulaBase * 0.95M;

                    if (ativoBMF.CdAtivoBMF == "DDI") {
                        formulaBase = Utilitario.Truncate(formulaBase * valorIndice, 2);
                    }
                    else if (ativoBMF.CdAtivoBMF == "SC2" || ativoBMF.CdAtivoBMF == "SC3") {
                        formulaBase = Utilitario.Truncate(formulaBase * (ativoBMF.Peso.Value / 50000M) * valorIndice, 4);
                    }
                    else {
                        formulaBase = Utilitario.Truncate(formulaBase * (ativoBMF.Peso.Value / 50000M) * valorIndice, 2);
                    }

                    return formulaBase;
                    #endregion

                case "DDM":
                case "DMA":
                case "DAP":
                    #region (Contratos Baseados em Taxa de Juro Indexada à Indice de Preços) DDM, DMA, DAP
                    prazo = diasUteis;

                    baseParte1 = (decimal)(1 + tabelaCalculoBMF.P.Value / 100M);
                    expoenteParte1 = (decimal)(tabelaCalculoBMF.DiasMinimo.Value / 252M);
                    parte1 = (decimal)(Math.Pow((double)baseParte1, (double)expoenteParte1) - 1);

                    baseParte2 = (decimal)(1 + tabelaCalculoBMF.P.Value / 100M);
                    expoenteParte2 = (decimal)(prazo / 252M);
                    parte2 = (decimal)(Math.Pow((double)baseParte2, (double)expoenteParte2) - 1);

                    baseParte3 = (decimal)(1 + tabelaCalculoBMF.P.Value / 100M);
                    expoenteParte3 = (decimal)(tabelaCalculoBMF.DiasMaximo.Value / 252M);
                    parte3 = (decimal)(Math.Pow((double)baseParte3, (double)expoenteParte3) - 1);

                    formulaBase = Utilitario.Truncate(100000M * Math.Max(parte1, Math.Min(parte2, parte3)), 2);

                    //Desconto para todos os ativos a partir de 02/01/2006.
                    formulaBase = formulaBase * 0.95M;

                    cotacaoIndice = new CotacaoIndice();
                    primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                    ultimoDiaMesAnterior = Calendario.SubtraiDiaUtil(primeiroDiaMes, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    if (ativoBMF.CdAtivoBMF == "DDM") {
                        valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.IGPM, ultimoDiaMesAnterior);
                        formulaBase = Utilitario.Truncate(formulaBase * valorIndice, 2);
                    }
                    else if (ativoBMF.CdAtivoBMF == "DDM") {
                        valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.IGPM, ultimoDiaMesAnterior);
                        formulaBase = Utilitario.Truncate(formulaBase * 10M * valorIndice, 2);
                    }
                    else {
                        valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.IPCA, ultimoDiaMesAnterior);
                        formulaBase = Utilitario.Truncate(formulaBase * ativoBMF.Peso.Value * valorIndice, 2);
                    }

                    return formulaBase;
                    #endregion

                case "FRM":
                    #region FRM
                    cotacaoIndice = new CotacaoIndice();
                    primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                    ultimoDiaMesAnterior = Calendario.SubtraiDiaUtil(primeiroDiaMes, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.IGPM, ultimoDiaMesAnterior);

                    #region Ponta Curta
                    prazo = diasUteis;

                    baseParte1 = (decimal)(1 + tabelaCalculoBMF.P.Value / 100M);
                    expoenteParte1 = (decimal)(tabelaCalculoBMF.DiasMinimo.Value / 252M);
                    parte1 = (decimal)(Math.Pow((double)baseParte1, (double)expoenteParte1) - 1);

                    baseParte2 = (decimal)(1 + tabelaCalculoBMF.P.Value / 100M);
                    expoenteParte2 = (decimal)(prazo / 252M);
                    parte2 = (decimal)(Math.Pow((double)baseParte2, (double)expoenteParte2) - 1);

                    baseParte3 = (decimal)(1 + tabelaCalculoBMF.P.Value / 100M);
                    expoenteParte3 = (decimal)(tabelaCalculoBMF.DiasMaximo.Value / 252M);
                    parte3 = (decimal)(Math.Pow((double)baseParte3, (double)expoenteParte3) - 1);

                    decimal pontaCurta = Utilitario.Truncate(50000M * Math.Max(parte1, Math.Min(parte2, parte3)), 2);
                    #endregion

                    #region Ponta Longa
                    prazo = Convert.ToInt32(diasUteis - (tabelaCalculoBMF.Beta.Value * diasUteis));

                    baseParte1 = (decimal)(1 + tabelaCalculoBMF.P.Value / 100);
                    expoenteParte1 = (decimal)(tabelaCalculoBMF.DiasMinimo.Value / 252);
                    parte1 = (decimal)(Math.Pow((double)baseParte1, (double)expoenteParte1) - 1);

                    baseParte2 = (decimal)(1 + tabelaCalculoBMF.P.Value / 100);
                    expoenteParte2 = (decimal)(prazo / 252);
                    parte2 = (decimal)(Math.Pow((double)baseParte2, (double)expoenteParte2) - 1);

                    baseParte3 = (decimal)(1 + tabelaCalculoBMF.P.Value / 100);
                    expoenteParte3 = (decimal)(tabelaCalculoBMF.DiasMaximo.Value / 252);
                    parte3 = (decimal)(Math.Pow((double)baseParte3, (double)expoenteParte3) - 1);

                    decimal pontaLonga = Utilitario.Truncate(50000 * Math.Max(parte1, Math.Min(parte2, parte3)), 2);
                    #endregion

                    formulaBase = Utilitario.Truncate(pontaLonga + (tabelaCalculoBMF.Alfa.Value * pontaCurta), 2);

                    //Desconto para todos os ativos a partir de 02/01/2006.
                    formulaBase = formulaBase * 0.95M;

                    formulaBase = Utilitario.Truncate(formulaBase * ativoBMF.Peso.Value * valorIndice, 2);

                    return formulaBase;
                    #endregion

                case "DLA":
                case "DOL":
                case "WDL":
                case "OFC":
                case "OFV":
                    #region (Contratos Baseados em Taxa de Cambio de Reais por Dolar) DLA, DOL, WDL, OFC, OFV
                    cotacaoIndice = new CotacaoIndice();
                    primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                    ultimoDiaMesAnterior = Calendario.SubtraiDiaUtil(primeiroDiaMes, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.PTAX_800VENDA, ultimoDiaMesAnterior);

                    DateTime dataPosterior = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa,
                                                                                       TipoFeriado.Brasil);
                    DateTime dataPosteriorD2 = Calendario.AdicionaDiaUtil(dataPosterior, 1, LocalFeriadoFixo.Bovespa,
                                                                                       TipoFeriado.Brasil);
                    if ((ativoBMF.DataVencimento == dataPosteriorD2 || ativoBMF.DataVencimento == dataPosterior) &&
                        (ativoBMF.CdAtivoBMF == "DOL") && (pontaEstrategia == TipoEstrategia.OperacaoSimples) &&
                        (ativoBMF.TipoMercado == TipoMercadoBMF.Futuro)) {
                        formulaBase = 0.05M * ativoBMF.Peso.Value / 50M;
                    }
                    else if ((ativoBMF.DataVencimento == dataPosteriorD2 || ativoBMF.DataVencimento == dataPosterior) &&
                            (ativoBMF.CdAtivoBMF == "WDL")) {
                        formulaBase = 0.15M * ativoBMF.Peso.Value / 50M;
                    }
                    else {
                        formulaBase = tabelaCalculoBMF.P.Value;
                    }

                    //Desconto para todos os ativos a partir de 02/01/2006.
                    //O desconto hoje vem embutido direto no percentual P.
                    //formulaBase = formulaBase * 0.95M;

                    if (ativoBMF.TipoMercado == TipoMercadoBMF.OpcaoDisponivel ||
                        ativoBMF.TipoMercado == TipoMercadoBMF.OpcaoFuturo) {
                        formulaBase = formulaBase * 0.3M;
                    }

                    formulaBase = Utilitario.Truncate(formulaBase * valorIndice, 2);

                    return formulaBase;
                    #endregion

                case "EUR":
                    #region (Contratos baseados em EURO) EUR
                    cotacaoIndice = new CotacaoIndice();
                    primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                    ultimoDiaMesAnterior = Calendario.SubtraiDiaUtil(primeiroDiaMes, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.EURO, ultimoDiaMesAnterior);

                    formulaBase = tabelaCalculoBMF.P.Value * ativoBMF.Peso.Value / 50M;

                    //Desconto para todos os ativos a partir de 02/01/2006.
                    formulaBase = formulaBase * 0.95M;

                    formulaBase = Utilitario.Truncate(formulaBase * valorIndice, 2);

                    return formulaBase;
                    #endregion

                case "IGM":
                case "IGF":
                case "IAP":
                    #region (Contratos baseados em Indice de Inflação) IGM, IGF, IAP
                    cotacaoIndice = new CotacaoIndice();
                    primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                    ultimoDiaMesAnterior = Calendario.SubtraiDiaUtil(primeiroDiaMes, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    if (ativoBMF.CdAtivoBMF == "IAP") {
                        valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.IPCA, ultimoDiaMesAnterior);
                    }
                    else {
                        valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.IGPM, ultimoDiaMesAnterior);
                    }

                    formulaBase = (tabelaCalculoBMF.P.Value / 100M) * ativoBMF.Peso.Value;

                    //Desconto para todos os ativos a partir de 02/01/2006.
                    formulaBase = formulaBase * 0.95M;

                    formulaBase = Utilitario.Truncate(formulaBase * valorIndice, 2);

                    return formulaBase;
                    #endregion

                case "IND":
                case "WIN":
                case "FCI":
                case "BRI":
                    #region (Contratos baseados em Indice de Preço de Ações) IND, WIN, FCI, BRI
                    cotacaoIndice = new CotacaoIndice();
                    primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                    ultimoDiaMesAnterior = Calendario.SubtraiDiaUtil(primeiroDiaMes, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    if (ativoBMF.CdAtivoBMF == "BRI") {
                        valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.IBRX50_MEDIOMENSAL, ultimoDiaMesAnterior);
                    }
                    else {
                        valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.IBOVESPA_MEDIOMENSAL, ultimoDiaMesAnterior);
                    }

                    formulaBase = Math.Min(4.5M, Math.Max(tabelaCalculoBMF.P.Value * valorIndice, 1));

                    //Desconto para todos os ativos a partir de 02/01/2006.
                    //formulaBase = formulaBase * 0.95;

                    formulaBase = Utilitario.Truncate(formulaBase * ativoBMF.Peso.Value, 2);

                    if (ativoBMF.TipoMercado == TipoMercadoBMF.OpcaoDisponivel ||
                        ativoBMF.TipoMercado == TipoMercadoBMF.OpcaoFuturo) {
                        formulaBase = formulaBase * 0.3M;
                    }

                    return formulaBase;
                    #endregion

                case "BCB":
                case "BEI":
                    #region (Contratos baseados em Titulos da Divida Externa) BCB, BEI
                    cotacaoIndice = new CotacaoIndice();
                    primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                    ultimoDiaMesAnterior = Calendario.SubtraiDiaUtil(primeiroDiaMes, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.PTAX_800VENDA, ultimoDiaMesAnterior);

                    formulaBase = tabelaCalculoBMF.P.Value * ativoBMF.Peso.Value / 500M;

                    //Desconto para todos os ativos a partir de 02/01/2006.
                    formulaBase = formulaBase * 0.95M;

                    formulaBase = Utilitario.Truncate(formulaBase * valorIndice, 2);

                    return formulaBase;
                    #endregion

                case "ALA":
                case "BZE":
                case "BGI":
                case "CNI":
                    #region (Contratos Agricolas em Reais) ALA, BZE, BGI, CNI
                    formulaBase = tabelaCalculoBMF.P.Value;

                    //Desconto para todos os ativos a partir de 02/01/2006.
                    formulaBase = formulaBase * 0.95M;

                    if (ativoBMF.TipoMercado == TipoMercadoBMF.OpcaoDisponivel ||
                        ativoBMF.TipoMercado == TipoMercadoBMF.OpcaoFuturo) {
                        formulaBase = formulaBase * 0.3M;
                    }

                    return formulaBase;
                    #endregion

                case "ISU":
                case "COT":
                case "ICF":
                case "SOJ":
                case "ETN":
                    #region (Contratos Agricolas em Dolar) ISU, COT, ICF, SOJ, ETN
                    cotacaoIndice = new CotacaoIndice();
                    primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                    ultimoDiaMesAnterior = Calendario.SubtraiDiaUtil(primeiroDiaMes, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.PTAX_800VENDA, ultimoDiaMesAnterior);

                    formulaBase = tabelaCalculoBMF.P.Value;

                    //Desconto para todos os ativos a partir de 02/01/2006.
                    formulaBase = formulaBase * 0.95M;

                    if (ativoBMF.TipoMercado == TipoMercadoBMF.OpcaoDisponivel ||
                        ativoBMF.TipoMercado == TipoMercadoBMF.OpcaoFuturo) {
                        formulaBase = formulaBase * 0.3M;
                    }

                    formulaBase = Utilitario.Truncate(formulaBase * valorIndice, 2);

                    return formulaBase;
                    #endregion

                case "OZ1":
                case "OZ2":
                case "OZ3":
                    #region (Contratos baseados em Ouro) OZ1, OZ2, OZ3
                    cotacaoIndice = new CotacaoIndice();
                    primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                    ultimoDiaMesAnterior = Calendario.SubtraiDiaUtil(primeiroDiaMes, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.PTAX_800VENDA, ultimoDiaMesAnterior);

                    if (ativoBMF.CdAtivoBMF == "OZ3") {
                        formulaBase = Utilitario.Truncate(tabelaCalculoBMF.P.Value * (ativoBMF.Peso.Value / 250M) *
                                                          valorIndice, 4);
                    }
                    else {
                        formulaBase = Utilitario.Truncate(tabelaCalculoBMF.P.Value * (ativoBMF.Peso.Value / 250M) *
                                                          valorIndice, 2);
                    }

                    //Desconto para todos os ativos a partir de 02/01/2006.
                    formulaBase = formulaBase * 0.95M;

                    if (ativoBMF.TipoMercado == TipoMercadoBMF.OpcaoDisponivel ||
                        ativoBMF.TipoMercado == TipoMercadoBMF.OpcaoFuturo) {
                        formulaBase = formulaBase * 0.3M;
                    }

                    return formulaBase;
                    #endregion

                default:
                    if (AtivoBMF.IsAtivoBond(ativoBMF.CdAtivoBMF)) {
                        #region (BRADIES - GLOBALs E T10) B09 a B40 e T10
                        cotacaoIndice = new CotacaoIndice();
                        primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                        ultimoDiaMesAnterior = Calendario.SubtraiDiaUtil(primeiroDiaMes, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                        valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.PTAX_800VENDA, ultimoDiaMesAnterior);

                        formulaBase = tabelaCalculoBMF.P.Value * ativoBMF.Peso.Value / 500M;

                        //Desconto para todos os ativos a partir de 02/01/2006.
                        formulaBase = formulaBase * 0.95M;

                        formulaBase = Utilitario.Truncate(formulaBase * valorIndice, 2);

                        return formulaBase;
                        #endregion
                    }
                    else {
                        throw new AtivoBMFNaoCadastradoException("Ativo não previsto para o cálculo da FB.");
                    }
            }
            #endregion
        }

        /// <summary>
        /// Calcula a Fórmula Base do emolumento para FRC, previsto pela BMF e retorna o Emolumento Unitário.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="ativoBMF">objeto com DataVencimento, Serie.</param>
        /// <param name="tabelaCalculoBMF">objeto com os parâmetros para cálculo de emolumento unitário.</param>
        /// <param name="diasCorridos">dias corridos entre a data passada e o vencimento do ativo.</param>
        /// <param name="diasCorridosDDICurto">dias corridos entre a data passada e o vencimento do ativo para a ponta curta.</param>
        /// <param name="pontaEstrategia">só se aplica a operações oriundas de estratégia.</param>
        /// <param name="calculaRegistroFRC">se calcula ou não registro.</param>
        /// <returns></returns>
        public decimal RetornaFormulaBaseEmolumentoFRC(DateTime data, AtivoBMF ativoBMF, TabelaCalculoBMF tabelaCalculoBMF,
                                                     int diasCorridos, int diasCorridosDDICurto, int pontaEstrategia,
                                                     bool calculaRegistroFRC) {
            decimal formulaBase;

            if (pontaEstrategia == TipoEstrategia.FRC.DDI_LONGO) {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                DateTime ultimoDiaMesAnterior = Calendario.RetornaUltimoDiaUtilMes(data, 0, LocalFeriadoFixo.Bovespa,
                                                                                   TipoFeriado.Brasil);
                decimal valorIndice = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.PTAX_800VENDA, ultimoDiaMesAnterior);

                int prazo;
                if (calculaRegistroFRC == true) {
                    prazo = 60;
                }
                else {
                    prazo = diasCorridos - Convert.ToInt32((tabelaCalculoBMF.Beta.Value * diasCorridosDDICurto));
                }

                decimal parte1 = (tabelaCalculoBMF.P.Value / 100M) * (tabelaCalculoBMF.DiasMinimo.Value / 360M);
                decimal parte2 = (tabelaCalculoBMF.P.Value / 100M) * (prazo / 360M);
                decimal parte3 = (tabelaCalculoBMF.P.Value / 100M) * (tabelaCalculoBMF.DiasMaximo.Value / 360M);

                formulaBase = Utilitario.Truncate(50000 * Math.Max(parte1, Math.Min(parte2, parte3)), 7);

                //Desconto para todos os ativos a partir de 02/01/2006.
                formulaBase = formulaBase * 0.95M;

                formulaBase = Utilitario.Truncate(formulaBase * valorIndice, 2);
            }
            else {
                formulaBase = 0;
            }

            return formulaBase;
        }

        /// <summary>
        /// Retorna o número de vencimentos a partir do ativo/serie/mercado passados.
        /// 0 para série não encontrada.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="serie"></param>
        /// <param name="tipoMercado"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public int RetornaNumeroVencimentos(string cdAtivoBMF, string serie, int tipoMercado, DateTime data) {
            
            AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();
            if (ativoBMFCollection.BuscaSeriesVincendas(cdAtivoBMF, tipoMercado, data))
            {
                int numeroVencimentos = 1;
                for (int i = 0; i < ativoBMFCollection.Count; i++) {
                    AtivoBMF ativoBMF = (AtivoBMF)ativoBMFCollection[i];
                    if (ativoBMF.Serie == serie) {
                        break;
                    }
                    else {
                        numeroVencimentos++;
                    }
                }
                return numeroVencimentos;
            }
            else {
                return 0;
            }
        }

        /// <summary>
        /// Retorna Serie do ativo.
        /// AtivoBMFNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="tipoMercado"></param>
        /// <param name="dataVencimento"></param>
        /// <returns></returns>
        public string BuscaSerie(string cdAtivoBMF, int tipoMercado, DateTime dataVencimento) {
            
            AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();

            ativoBMFCollection.Query
                 .Select(ativoBMFCollection.Query.Serie)
                 .Where(ativoBMFCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                        ativoBMFCollection.Query.TipoMercado == tipoMercado,
                        ativoBMFCollection.Query.DataVencimento.Equal(dataVencimento));

            ativoBMFCollection.Query.Load();

            if (ativoBMFCollection.HasData) {
                // Copia informações de ativoBMFCollection para AtivoBMF
                this.AddNew();
                this.Serie = ((AtivoBMF)ativoBMFCollection[0]).Serie;
            }
            else {
                throw new AtivoBMFNaoCadastradoException("AtivoBMF " + cdAtivoBMF + " não cadastrado. Mercado=" + tipoMercado + " DataVencimento=" + dataVencimento);
            }

            return this.Serie;
        }

        /// <summary>
        /// Carrega o objeto AtivoBMF com os campos Serie, DataVencimento.
        /// Ordena decrescente pela dataVencimento.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="tipoMercado"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="data">Usado para indicar uma data base para pesquisa.</param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaSerieAnterior(string cdAtivoBMF, int tipoMercado, DateTime dataVencimento, DateTime data) {
            
            AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();

            ativoBMFCollection.Query
                 .Select(ativoBMFCollection.Query.Serie, ativoBMFCollection.Query.DataVencimento)
                 .Where(ativoBMFCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                        ativoBMFCollection.Query.TipoMercado == tipoMercado,
                        ativoBMFCollection.Query.DataVencimento.LessThan(dataVencimento),
                        ativoBMFCollection.Query.DataVencimento.GreaterThan(data))
                 .OrderBy(ativoBMFCollection.Query.DataVencimento.Descending);

            ativoBMFCollection.Query.Load();

            if (ativoBMFCollection.HasData) {
                // Copia informações de ativoBMFCollection para AtivoBMF
                this.AddNew();
                this.Serie = ((AtivoBMF)ativoBMFCollection[0]).Serie;
                this.DataVencimento = ((AtivoBMF)ativoBMFCollection[0]).DataVencimento;
                return true;
            }
            else { 
                return false; 
            }          
        }

        /// <summary>
        /// Carrega o objeto AtivoBMF com os campos Serie, DataVencimento.
        /// Ordena crescente pela dataVencimento.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="tipoMercado"></param>
        /// <param name="dataVencimento"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaSeriePosterior(string cdAtivoBMF, int tipoMercado, DateTime dataVencimento) {
            
            AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();

            ativoBMFCollection.Query
                 .Select(ativoBMFCollection.Query.Serie, ativoBMFCollection.Query.DataVencimento)
                 .Where(ativoBMFCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                        ativoBMFCollection.Query.TipoMercado == tipoMercado,
                        ativoBMFCollection.Query.DataVencimento.GreaterThan(dataVencimento))
                 .OrderBy(ativoBMFCollection.Query.DataVencimento.Ascending);

            ativoBMFCollection.Query.Load();

            if (ativoBMFCollection.HasData) {
                // Copia informações de ativoBMFCollection para AtivoBMF
                this.AddNew();
                this.Serie = ((AtivoBMF)ativoBMFCollection[0]).Serie;
                this.DataVencimento = ((AtivoBMF)ativoBMFCollection[0]).DataVencimento;
                return true;
            }
            else { 
                return false; 
            }            
        }

        /// <summary>
        /// Carrega o objeto AtivoVencimentosBMF com os campos Serie, DataVencimento.        
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="tipoMercado"></param>
        /// <param name="dataVencimentoInicio">Filtra somente datas maiores ou igual a dataVencimentoInicio.</param>
        /// <param name="dataVencimentoFim">Filtra somente datas menores ou igual a dataVencimentoFim.</param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaAtivoBMF(string cdAtivoBMF, int tipoMercado,
                                  DateTime dataVencimentoInicio, DateTime dataVencimentoFim) {
           
            AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();

            ativoBMFCollection.Query
                 .Select(ativoBMFCollection.Query.Serie, ativoBMFCollection.Query.DataVencimento)
                 .Where(ativoBMFCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                        ativoBMFCollection.Query.TipoMercado == tipoMercado,
                        ativoBMFCollection.Query.DataVencimento.GreaterThanOrEqual(dataVencimentoInicio),
                        ativoBMFCollection.Query.DataVencimento.LessThanOrEqual(dataVencimentoFim))
                 .OrderBy(ativoBMFCollection.Query.DataVencimento.Ascending);

            ativoBMFCollection.Query.Load();

            if (ativoBMFCollection.HasData) {
                // Copia informações de ativoBMFCollection para AtivoBMF
                this.AddNew();
                this.Serie = ((AtivoBMF)ativoBMFCollection[0]).Serie;
                this.DataVencimento = ((AtivoBMF)ativoBMFCollection[0]).DataVencimento;
                return true;
            }
            else { return false; }            
        }

        /// <summary>
        /// Carrega o objeto AtivoBMF com os campos Serie, DataVencimento.        
        /// Filtra DataVencimento >= dataVencimento.
        /// Ordenado crescente pela dataVencimento.
        /// AtivoBMFNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="tipoMercado"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="contadorSerie">Informa qual série a ser buscada (1a série em aberto, 2a série em aberto...)</param>
        /// <returns></returns>
        public void BuscaSerieAberto(string cdAtivoBMF, int tipoMercado, DateTime dataVencimento, int contadorSerie) {
            
            AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();

            ativoBMFCollection.Query
                 .Select(ativoBMFCollection.Query.Serie, ativoBMFCollection.Query.DataVencimento)
                 .Where(ativoBMFCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                        ativoBMFCollection.Query.TipoMercado == tipoMercado,
                        ativoBMFCollection.Query.DataVencimento.GreaterThanOrEqual(dataVencimento))
                 .OrderBy(ativoBMFCollection.Query.DataVencimento.Ascending);

            ativoBMFCollection.Query.Load();

            if (ativoBMFCollection.HasData && ativoBMFCollection.Count >= contadorSerie) {
                // Copia informações de ativoBMFCollection para AtivoBMF
                this.AddNew();
                this.Serie = ((AtivoBMF)ativoBMFCollection[contadorSerie - 1]).Serie;
                this.DataVencimento = ((AtivoBMF)ativoBMFCollection[contadorSerie - 1]).DataVencimento;
            }
            else {
                throw new AtivoBMFNaoCadastradoException("AtivoBMF " + cdAtivoBMF + " não cadastrado. Mercado=" + tipoMercado + ", 2a série para DataVencimento >= " + dataVencimento);
            }

        }

        /// <summary>
        /// Carrega o objeto AtivoBMF com os campos Serie, DataVencimento.        
        /// Filtra DataVencimento > dataVencimento.
        /// Ordenado crescente pela dataVencimento.
        /// AtivoBMFNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="tipoMercado"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="contadorSerie">Informa qual série a ser buscada (1a série em aberto, 2a série em aberto...)</param>
        /// <returns></returns>
        public void BuscaSerieAbertoCambio(string cdAtivoBMF, int tipoMercado, DateTime dataVencimento, int contadorSerie) {
            
            AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();

            ativoBMFCollection.Query
                 .Select(ativoBMFCollection.Query.Serie, ativoBMFCollection.Query.DataVencimento)
                 .Where(ativoBMFCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                        ativoBMFCollection.Query.TipoMercado == tipoMercado,
                        ativoBMFCollection.Query.DataVencimento.GreaterThan(dataVencimento))
                 .OrderBy(ativoBMFCollection.Query.DataVencimento.Ascending);

            ativoBMFCollection.Query.Load();

            if (ativoBMFCollection.HasData && ativoBMFCollection.Count >= contadorSerie) {
                // Copia informações de ativoBMFCollection para AtivoBMF
                this.AddNew();
                this.Serie = ((AtivoBMF)ativoBMFCollection[contadorSerie - 1]).Serie;
                this.DataVencimento = ((AtivoBMF)ativoBMFCollection[contadorSerie - 1]).DataVencimento;
            }
            else {
                throw new AtivoBMFNaoCadastradoException("AtivoBMF " + cdAtivoBMF + " não cadastrado. Mercado=" + tipoMercado + ", 2a série para DataVencimento > " + dataVencimento);
            }
        }

        /// <summary>
        /// Carrega o objeto AtivoBMF com os campos Serie, DataVencimento.
        /// AtivoBMFNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="tipoMercado"></param>
        /// <param name="dataVencimento"></param>
        /// <returns>bool se achou registro.</returns>
        public bool BuscaSerieAtivo(string cdAtivoBMF, int tipoMercado, DateTime dataVencimento) {
            
            AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();

            ativoBMFCollection.Query
                 .Select(ativoBMFCollection.Query.Serie, Query.DataVencimento)
                 .Where(ativoBMFCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                        ativoBMFCollection.Query.TipoMercado == tipoMercado,
                        ativoBMFCollection.Query.DataVencimento.Equal(dataVencimento));
                
            ativoBMFCollection.Query.Load();

            if (ativoBMFCollection.HasData) {
                // Copia informações de ativoBMFCollection para AtivoBMF
                this.AddNew();
                this.Serie = ((AtivoBMF)ativoBMFCollection[0]).Serie;
                this.DataVencimento = ((AtivoBMF)ativoBMFCollection[0]).DataVencimento;
            }
                        
            return ativoBMFCollection.HasData;
        }

        /// <summary>
        /// Integra a partir da tabela do Sinacor.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="serie"></param>
        /// <returns>Retorna se achou dados na tabela do Sinacor</returns>
        public bool IntegraAtivoSinacor(string cdAtivoBMF, string serie)
        {
            Mfseries mfseries = new Mfseries();
            mfseries.Query.Where(mfseries.Query.Commod.Equal(cdAtivoBMF),
                                 mfseries.Query.Serie.Equal(serie));

            bool achou = false;
            if (mfseries.Query.Load())
            {
                achou = true;

                //TODO Precisa descobrir depois onde pegar o TipoSerie na tabela do Sinacor!!!!
                DateTime dataVencimento = mfseries.Datvct.Value;
                byte tipoMercado = ConverteMercadoSinacor(mfseries.Mercad);

                decimal precoExercicio = 0;
                if (mfseries.Preexe.HasValue)
                {
                    precoExercicio = mfseries.Preexe.Value;
                }
                decimal peso = mfseries.Qtdpads.Value;

                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMF.IdMoeda = (int)ListaMoedaFixo.Real;
                ativoBMF.CdAtivoBMF = cdAtivoBMF;
                ativoBMF.DataVencimento = dataVencimento;
                ativoBMF.Peso = peso;
                ativoBMF.PrecoExercicio = precoExercicio;
                ativoBMF.Serie = serie;
                ativoBMF.TipoMercado = tipoMercado;
                ativoBMF.CodigoIsin = "";
                ativoBMF.Save();

            }

            return achou;            
        }

        /// <summary>
        /// Converte o tipo de mercado, do tipo do Sinacor, para o tipo do Financial.
        /// </summary>
        /// <param name="mercado"></param>
        /// <returns></returns>
        public byte ConverteMercadoSinacor(string mercado)
        {
            byte tipoMercado = 0;
            
            switch (mercado)
            {
                case "VIS":
                case "DIS": tipoMercado = TipoMercadoBMF.Disponivel;
                    break;
                case "FUT": tipoMercado = TipoMercadoBMF.Futuro;
                    break;
                case "OPD": tipoMercado = TipoMercadoBMF.OpcaoDisponivel;
                    break;
                case "OPF": tipoMercado = TipoMercadoBMF.OpcaoFuturo;
                    break;
                case "TMO": tipoMercado = TipoMercadoBMF.Termo;
                    break;
            }

            return tipoMercado;

        }

    }
}