﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.BMF.Enums;
using Financial.Common.Enums;

namespace Financial.BMF
{
	public partial class OperacaoBMFCollection : esOperacaoBMFCollection
	{
        /// <summary>
        /// Carrega o objeto OperacaoBMFCollection com os campos IdOperacao, CdAtivoBMF, Serie, TipoMercado, Quantidade, 
        /// TipoOperacao, Emolumento, Registro, PontaEstrategia.            
        /// Filtra por operações que não sejam Opcões Flexíveis ("OFC", "OFV", "FCI", "FPI", "FCS", "FPS").
        /// Filtra por: CalculaDespesas.Equal("S").
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaOperacaoBMFCalculaTaxasParametrizadas(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.CdAtivoBMF, this.Query.Serie, this.Query.TipoMercado,
                         this.Query.Quantidade, this.Query.TipoOperacao, this.Query.Emolumento, this.Query.Registro,
                         this.Query.PontaEstrategia)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data == data,
                        this.Query.CdAtivoBMF.NotIn("OFC", "OFV", "FCI", "FPI", "FCS", "FPS"),
                        this.Query.Fonte.NotIn((int)FonteOperacaoBMF.Gerencial),
                        this.Query.CalculaDespesas.Equal("S"));

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoBMFCollection com os campos IdOperacao, CdAtivoBMF, Serie, IdAgenteLiquidacao, 
        /// Quantidade, TipoOperacao, Pu, Ajuste.        
        /// Entram todos os futuros e DLA.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaOperacaoBMFCalculaAjuste(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.CdAtivoBMF, this.Query.Serie, this.Query.IdAgenteLiquidacao,
                         this.Query.Quantidade, this.Query.TipoOperacao, this.Query.Pu, this.Query.Ajuste, this.Query.IdConta)
                 .Where(this.Query.IdCliente == idCliente &
                        this.Query.Origem.NotEqual((byte)OrigemOperacaoBMF.AjusteFuturo) &
                        this.Query.Data.Equal(data) &
                        (
                            this.Query.TipoMercado == TipoMercadoBMF.Futuro |
                            this.Query.CdAtivoBMF == ("DLA") )
                        );

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoBMFCollection com os campos IdOperacao, CdAtivoBMF, Serie, TipoMercado, Quantidade,
        /// TipoOperacao, IdAgenteCorretora, Emolumento, Registro, CustoWtr, OutrosCustos, PontaEstrategia, IdentificadorEstrategia, IdTrader.
        /// Busca operações não integradas do Sinacor com CalculaDespesas = 'S'.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaOperacaoBMFCalculaTaxas(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.CdAtivoBMF, this.Query.Serie, this.Query.TipoMercado,
                         this.Query.Quantidade, this.Query.TipoOperacao, this.Query.IdAgenteCorretora, this.Query.Emolumento, this.Query.Registro,
                         this.Query.CustoWtr, this.Query.OutrosCustos, this.Query.PontaEstrategia, this.Query.IdentificadorEstrategia,
                         this.Query.IdTrader)
                 .Where(this.Query.IdCliente == idCliente &
                        this.Query.Data == data &
                        this.Query.Fonte.NotIn((int)FonteOperacaoBMF.Sinacor, (int)FonteOperacaoBMF.Gerencial) &
                        this.Query.CalculaDespesas.Equal("S") &
                        this.Query.IdMoeda.Equal((byte)ListaMoedaFixo.Real),
                        this.Query.Origem.NotIn((byte)OrigemOperacaoBMF.AjusteFuturo,
                                                (byte)OrigemOperacaoBMF.TransferenciaCorretora));

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoBMFCollection com os campos IdOperacao, IdAgenteCorretora, IdAgenteLiquidacao,
        /// CdAtivoBMF, Serie, TipoMercado, Quantidade, TipoOperacao, Corretagem, Valor.
        /// Busca operações não integradas do Sinacor com CalculaDespesas = 'S'.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaOperacaoBMFCalculaCorretagem(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdAgenteCorretora, this.Query.IdAgenteLiquidacao, 
                         this.Query.CdAtivoBMF, this.Query.Serie, this.Query.TipoMercado,
                         this.Query.Quantidade, this.Query.TipoOperacao, this.Query.Corretagem,
                         this.Query.Valor, this.Query.PercentualDesconto, this.Query.IdMoeda, this.Query.TaxaClearingVlFixo, this.Query.IdConta)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data == data,
                        this.Query.Fonte.NotIn((int)FonteOperacaoBMF.Sinacor, (int)FonteOperacaoBMF.Gerencial),
                        this.Query.CalculaDespesas.Equal("S"));

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoBMFCollection com os campos IdOperacao, IdCliente, CdAtivoBMF, Serie, IdAgenteLiquidacao,
        /// TipoMercado, Quantidade, Pu, PULiquido, ValorLiquido, TipoOperacao, ResultadoRealizado, Ajuste.
        /// Busca operações de Compra e Depósito.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns></returns>
        public void BuscaOperacaoBMFCompraVendaDayTrade(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBMF, this.Query.Serie,
                         this.Query.IdAgenteLiquidacao, this.Query.TipoMercado, this.Query.Quantidade, this.Query.Pu,
                         this.Query.PULiquido, this.Query.ValorLiquido, this.Query.TipoOperacao,
                         this.Query.ResultadoRealizado, this.Query.Ajuste)
                 .Where(
                        this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOperacao.In(TipoOperacaoBMF.Venda, TipoOperacaoBMF.Retirada,TipoOperacaoBMF.Compra, TipoOperacaoBMF.Deposito, TipoOperacaoBMF.VendaDaytrade, TipoOperacaoBMF.CompraDaytrade),
                        this.Query.Origem.NotEqual((byte)OrigemOperacaoBMF.AjusteFuturo)
                 );

            this.Query.Load();
        }        


        /// <summary>
        /// Carrega o objeto OperacaoBMFCollection com os campos IdOperacao, IdCliente, CdAtivoBMF, Serie, IdAgenteLiquidacao,
        /// TipoMercado, Quantidade, Pu, PULiquido, ValorLiquido, TipoOperacao, ResultadoRealizado, Ajuste.
        /// Busca operações de Compra e Depósito.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns></returns>
        public void BuscaOperacaoBMFCompra(int idCliente, DateTime dataOperacao) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBMF, this.Query.Serie,
                         this.Query.IdAgenteLiquidacao, this.Query.TipoMercado, this.Query.Quantidade, this.Query.Pu,
                         this.Query.PULiquido, this.Query.ValorLiquido, this.Query.TipoOperacao, this.Query.Origem,
                         this.Query.ResultadoRealizado, this.Query.Ajuste, this.Query.IdConta)
                 .Where(
                        this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOperacao.In(TipoOperacaoBMF.Compra, TipoOperacaoBMF.Deposito),
                        this.Query.Origem.NotEqual((byte)OrigemOperacaoBMF.AjusteFuturo)
                 );

            this.Query.Load();         
        }        

        /// <summary>
        /// Carrega o objeto OperacaoBMFCollection com os campos IdOperacao, IdCliente, CdAtivoBMF, Serie, IdAgenteLiquidacao,
        /// TipoMercado, Quantidade, Pu, PULiquido, ValorLiquido, TipoOperacao, ResultadoRealizado, Ajuste.
        /// Busca operações de Venda e Retirada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns></returns>
        public void BuscaOperacaoBMFVenda(int idCliente, DateTime dataOperacao) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBMF, this.Query.Serie,
                         this.Query.IdAgenteLiquidacao, this.Query.TipoMercado, this.Query.Quantidade, this.Query.Pu,
                         this.Query.PULiquido, this.Query.ValorLiquido, this.Query.TipoOperacao, this.Query.Ajuste,
                         this.Query.ResultadoRealizado, this.Query.Origem, this.Query.IdConta)
                 .Where(
                        this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,                        
                        this.Query.TipoOperacao.In(TipoOperacaoBMF.Venda, TipoOperacaoBMF.Retirada),
                        this.Query.Origem.NotEqual((byte)OrigemOperacaoBMF.AjusteFuturo)
                 );

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoBMFCollection com os campos IdOperacao, IdCliente, CdAtivoBMF, Serie, TipoMercado,
        /// Quantidade, PULiquido, ValorLiquido, Pu, Corretagem, Emolumento, Registro, CustoWtr, OutrosCustos, Valor, TipoOperacao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>        
        public void BuscaOperacaoBMFValores(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBMF, this.Query.Serie,
                        this.Query.TipoMercado, this.Query.Quantidade, this.Query.PULiquido,
                        this.Query.ValorLiquido, this.Query.Pu, this.Query.Corretagem,
                        this.Query.Emolumento, this.Query.Registro, this.Query.CustoWtr, this.Query.OutrosCustos,
                        this.Query.Valor, this.Query.TipoOperacao, this.Query.TaxaClearingVlFixo,
						this.Query.IdConta)
                 .Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente.Equal(idCliente),
                        this.Query.Quantidade.NotEqual(0));

            this.Query.Load();
        }

        /// <summary>
        /// Deleta operações do dia, dada a origem informada.
        /// /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="origem"></param>
        /// throws ArgumentException se Origem for diferente dos tipos do enum OrigemOperacaoBMF
        public void DeletaOperacaoBMF(int idCliente, int origem, DateTime data) 
        {
            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("OrigemOperacaoBMF incorreto. Os valores possiveis estão no enum OrigemOperacaoBMF");

            List<int> valoresPossiveis = OrigemOperacaoBMF.Values();
            if (!valoresPossiveis.Contains(origem)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Origem == origem,
                        this.Query.Data.Equal(data));
            this.Query.Load();
    
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Carrega o objeto OperacaoBMFCollection com os campos IdAgenteLiquidacao, IdMoeda, TipoOperacao, TipoMercado, Origem,
        /// CdAtivoBMF, ValorLiquido.Sum()
        /// Busca operações que não sejam termo nem futuro.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns></returns>
        public void BuscaOperacaoBMFLancaOperacaoAnalitico(int idCliente, DateTime dataOperacao) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgenteLiquidacao, this.Query.IdMoeda, this.Query.TipoOperacao,
                        this.Query.TipoMercado, this.Query.Origem, this.Query.CdAtivoBMF,
                        this.Query.IdConta,
                        this.Query.Serie,
                        this.Query.Valor.Sum(),
						this.Query.TaxaClearingVlFixo.Sum())
                 .Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado.NotIn(TipoMercadoBMF.Termo, TipoMercadoBMF.Futuro),
                        this.Query.TipoOperacao
                                   .In(TipoOperacaoBMF.Compra, TipoOperacaoBMF.CompraDaytrade,
                                       TipoOperacaoBMF.Venda, TipoOperacaoBMF.VendaDaytrade),
                        this.Query.Origem.NotIn((byte)OrigemOperacaoBMF.AjusteFuturo)
                   )
                   .GroupBy(this.Query.IdAgenteLiquidacao,
                            this.Query.IdMoeda,
                            this.Query.TipoOperacao,
                            this.Query.TipoMercado,
                            this.Query.Origem,
                            this.Query.CdAtivoBMF,
                            this.Query.Serie,
                            this.Query.IdConta
                   );

            this.Query.Load();            
        }

        /// <summary>
        /// Carrega o objeto OperacaoBMFCollection com os campos IdAgenteLiquidacao, TipoOperacao, TipoMercado, 
        /// ValorLiquido.Sum(), Ajuste.Sum(), Corretagem.Sum(), Emolumento.Sum(), Registro.Sum(), OutrosCustos.Sum().
        /// Busca operações que não sejam termo nem opcao.    
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns></returns>
        public void BuscaOperacaoBMFLancaOperacaoConsolidadoFuturo(int idCliente, DateTime dataOperacao) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgenteLiquidacao,
                        this.Query.IdMoeda,
                        this.Query.CdAtivoBMF,
                        this.Query.IdConta,
                        this.Query.Ajuste.Sum(),
                        this.Query.Corretagem.Sum(),
                        this.Query.Emolumento.Sum(),
                        this.Query.Registro.Sum(),
                        this.Query.OutrosCustos.Sum(),
                        this.Query.TaxaClearingVlFixo.Sum())
                 .Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado.Equal((byte)TipoMercadoBMF.Futuro)                                   
                   )
                   .GroupBy(this.Query.IdAgenteLiquidacao, this.Query.IdMoeda, this.Query.CdAtivoBMF, this.Query.IdConta);

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoBMFCollection com os campos IdAgenteLiquidacao, ValorLiquido.Sum()
        /// Busca operações de compra ou compra DT de opções de BMF.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns></returns>
        public void BuscaOperacaoBMFLancaOperacaoConsolidadoCompra(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgenteLiquidacao,
                        this.Query.IdMoeda,
                        this.Query.CdAtivoBMF,
                        this.Query.IdConta,
                        this.Query.ValorLiquido.Sum(),
						this.Query.TaxaClearingVlFixo.Sum())
                 .Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado.In((byte)TipoMercadoBMF.OpcaoDisponivel, (byte)TipoMercadoBMF.OpcaoFuturo, (byte)TipoMercadoBMF.Disponivel),
                        this.Query.TipoOperacao
                                   .In(TipoOperacaoBMF.Compra, TipoOperacaoBMF.CompraDaytrade)
                   )
                   .GroupBy(this.Query.IdAgenteLiquidacao, this.Query.IdMoeda, this.Query.CdAtivoBMF, this.Query.IdConta);

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoBMFCollection com os campos IdAgenteLiquidacao, ValorLiquido.Sum()
        /// Busca operações de venda ou venda DT de opções de BMF.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns></returns>
        public void BuscaOperacaoBMFLancaOperacaoConsolidadoVenda(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgenteLiquidacao,
                        this.Query.IdMoeda,
                        this.Query.CdAtivoBMF,
                        this.Query.IdConta,
                        this.Query.ValorLiquido.Sum(),
						this.Query.TaxaClearingVlFixo.Sum())
                 .Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado.In((byte)TipoMercadoBMF.OpcaoDisponivel, (byte)TipoMercadoBMF.OpcaoFuturo, (byte)TipoMercadoBMF.Disponivel),
                        this.Query.TipoOperacao
                                   .In(TipoOperacaoBMF.Venda, TipoOperacaoBMF.VendaDaytrade)
                   )
                   .GroupBy(this.Query.IdAgenteLiquidacao, this.Query.IdMoeda, this.Query.CdAtivoBMF, this.Query.IdConta);

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoBMFCollection com os campos IdAgenteLiquidacao, IdMoeda, Corretagem.Sum(), Emolumento.Sum(),
        /// Registro.Sum(), CustoWtr.Sum(), OutrosCustos.Sum().
        /// Busca operações que não sejam termo nem futuro.    
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns></returns>
        public void BuscaOperacaoBMFLancaOperacaoDespesas(int idCliente, DateTime dataOperacao) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgenteLiquidacao,
                        this.Query.IdMoeda,
                        this.Query.CdAtivoBMF,
                        this.Query.IdConta,
                        this.Query.Corretagem.Sum(),
                        this.Query.Emolumento.Sum(),
                        this.Query.Registro.Sum(),
                        this.Query.CustoWtr.Sum(),
                        this.Query.OutrosCustos.Sum(),
                        this.Query.TaxaClearingVlFixo.Sum()
                 )
                 .Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOperacao
                                   .In(TipoOperacaoBMF.Compra, TipoOperacaoBMF.CompraDaytrade,
                                       TipoOperacaoBMF.Venda, TipoOperacaoBMF.VendaDaytrade)
                   )
                   .GroupBy(this.Query.IdAgenteLiquidacao, this.Query.IdMoeda, this.Query.CdAtivoBMF, this.Query.IdConta);

            this.Query.Load();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="fonte"></param>
        public void DeletaOperacao(int idCliente, int fonte, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Fonte == fonte,
                        this.Query.Data.Equal(data));
            this.Query.Load();         

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta todas as operações com Fonte=OrdemBMF e Fonte=RCOMIESP.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="fonte"></param>
        public void DeletaOperacaoOrdemCasada(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Fonte.In((byte)FonteOperacaoBMF.ArquivoRCOMIESP,
                                            (byte)FonteOperacaoBMF.OrdemBMF,
                                            (byte)FonteOperacaoBMF.NotaPDFSinacor),
                        this.Query.Data.Equal(data));
            this.Query.Load();
            
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Carrega o objeto OperacaoBMFCollection com o campo IdAgenteCorretora.
        /// Filtra por TipoOperacao.In(TipoOperacaoBMF.CompraDaytrade, TipoOperacaoBMF.VendaDaytrade).
        /// Busca por Query.es.Distinct = true.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data">Data da Operação usada para Filtro</param>
        /// <returns></returns>
        public void BuscaCorretorasOperadasDayTrade(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgenteCorretora)
                 .Where(
                        this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOperacao.In(TipoOperacaoBMF.CompraDaytrade, TipoOperacaoBMF.VendaDaytrade)
                 );
            this.Query.es.Distinct = true;

            this.Query.Load();
        }

        /// <summary>
        /// Deleta as operações de OperacaoBMF, com Fonte = Sinacor.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>        
        public void DeletaOperacaoBMFSinacor(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.IdOperacao)
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.Data.Equal(data),
                     this.Query.Fonte == FonteOperacaoBMF.Sinacor);

            this.Query.Load();

            if (this.HasData)
            {
                this.MarkAllAsDeleted();
                this.Save(); // Apaga OrdensBMF
            }
        }
	}
}
