﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.BMF.Enums;
using Financial.Common.Enums;
using Financial.Common;

namespace Financial.BMF
{
	public partial class PosicaoBMFHistorico : esPosicaoBMFHistorico
	{
        /// <summary>
        /// Carrega o objeto PosicaoBMFHistorico com o campo AjusteDiarioSum(calculado).        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="cdAtivoBMF"></param>
        public void BuscaTotalAjusteNormal(int idCliente, DateTime dataInicio, DateTime dataFim, string cdAtivoBMF)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.AjusteDiario.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.GreaterThanOrEqual(dataInicio),
                        this.Query.DataHistorico.LessThanOrEqual(dataFim),
                        this.Query.CdAtivoBMF.In(cdAtivoBMF)                        
                        );
            this.Query.Load();

            // Se valores dos campos são nulos retorna zero
            if (this.es.HasData)
            {
                this.AjusteDiario = this.AjusteDiario.HasValue ? this.AjusteDiario.Value : 0;
            }
        }

        /// <summary>
        /// Carrega o objeto PosicaoBMFHistorico com o campo AjusteDiarioSum(calculado).        
        /// Filtra para não trazer "DOL", "IND", "DI1", "DDI".
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        public void BuscaTotalAjusteNormalOutros(int idCliente, DateTime dataInicio, DateTime dataFim)
        {         
            this.QueryReset();
            this.Query
                 .Select(this.Query.AjusteDiario.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.GreaterThanOrEqual(dataInicio),
                        this.Query.DataHistorico.LessThanOrEqual(dataFim),
                        this.Query.CdAtivoBMF.NotIn("DOL", "IND", "DI1", "DDI")
                        );
            this.Query.Load();

            // Se valores dos campos são nulos retorna zero
            if (this.es.HasData)
            {
                this.AjusteDiario = this.AjusteDiario.HasValue ? this.AjusteDiario.Value : 0;
            }
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente das posições em Disponivel e Opções (s/ Disponivel e Futuro).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo,
                                                  TipoMercadoBMF.Disponivel));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente. Leva em conta o IdMoedaCliente passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idMoedaCliente"></param>
        /// <param name="data"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, int idMoedaCliente, DateTime data)
        {
            PosicaoBMFHistoricoQuery posicaoBMFQuery = new PosicaoBMFHistoricoQuery("P");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");

            posicaoBMFQuery.Select(posicaoBMFQuery.IdPosicao);
            posicaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFQuery.CdAtivoBMF);
            posicaoBMFQuery.Where(posicaoBMFQuery.IdCliente.Equal(idCliente),
                                    posicaoBMFQuery.DataHistorico.Equal(data),
                                    posicaoBMFQuery.Quantidade.NotEqual(0),
                                    ativoBMFQuery.IdMoeda.NotEqual(idMoedaCliente),
                                    posicaoBMFQuery.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo,
                                                                   TipoMercadoBMF.Disponivel));

            PosicaoBMFHistoricoCollection posicaoBMFCollectionExiste = new PosicaoBMFHistoricoCollection();
            posicaoBMFCollectionExiste.Load(posicaoBMFQuery);

            if (posicaoBMFCollectionExiste.Count == 0)
            {
                return this.RetornaValorMercado(idCliente,data);
            }

            PosicaoBMFHistoricoCollection posicaoBMFCollection = new PosicaoBMFHistoricoCollection();
            posicaoBMFCollection.Query.Select(posicaoBMFCollection.Query.CdAtivoBMF,
                                              posicaoBMFCollection.Query.Serie,
                                              posicaoBMFCollection.Query.ValorMercado.Sum());
            posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.IdCliente.Equal(idCliente),
                                             posicaoBMFCollection.Query.DataHistorico.Equal(data),
                                             posicaoBMFCollection.Query.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo,
                                                                                 TipoMercadoBMF.Disponivel));
            posicaoBMFCollection.Query.GroupBy(posicaoBMFCollection.Query.CdAtivoBMF,
                                               posicaoBMFCollection.Query.Serie);
            posicaoBMFCollection.Query.Load();

            decimal totalValor = 0;

            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && posicaoBMFCollection.Count > 0)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }

            foreach (PosicaoBMFHistorico posicaoBMF in posicaoBMFCollection)
            {
                string cdAtivoBMF = posicaoBMF.CdAtivoBMF;
                string serie = posicaoBMF.Serie;
                decimal valor = posicaoBMF.ValorMercado.Value;

                AtivoBMF ativoBMF = new AtivoBMF();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBMF.Query.IdMoeda);
                ativoBMF.LoadByPrimaryKey(campos, cdAtivoBMF, serie);
                int idMoedaAtivo = ativoBMF.IdMoeda.Value;

                if (idMoedaCliente != idMoedaAtivo)
                {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaAtivo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaAtivo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }
	}
}
