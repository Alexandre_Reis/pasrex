﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.BMF
{
	public partial class TabelaCorretagemBMF : esTabelaCorretagemBMF
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaCorretagemBMF));

        /// <summary>
        /// Carrega o objeto TabelaCorretagemBMF com os campos TaxaCorretagem, TaxaCorretagemDT.        
        /// Filtra FaixaVencimento menor ou igual a faixaVencimento.
        /// Filtra FaixaValor menor ou igual a faixaValor.
        /// Ordenado decrescente pela FaixaVencimento, FaixaValor.
        /// </summary>
        /// <param name="idPerfilCorretagem"></param>
        /// <param name="dataReferencia"></param>
        /// <param name="faixaVencimento"></param>
        /// <param name="faixaValor"></param>        
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaTabelaCorretagem(int idPerfilCorretagem, DateTime dataReferencia, int faixaVencimento,
                                          decimal faixaValor)
        {
            // TODO logar
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }

            TabelaCorretagemBMFCollection tabelaCorretagemBMFCollection = new TabelaCorretagemBMFCollection();

            tabelaCorretagemBMFCollection.Query
                 .Select(tabelaCorretagemBMFCollection.Query.TaxaCorretagem, tabelaCorretagemBMFCollection.Query.TaxaCorretagemDT)
                 .Where(tabelaCorretagemBMFCollection.Query.IdPerfilCorretagem == idPerfilCorretagem,
                        tabelaCorretagemBMFCollection.Query.DataReferencia.Equal(dataReferencia),
                        tabelaCorretagemBMFCollection.Query.FaixaVencimento.LessThanOrEqual(faixaVencimento),
                        tabelaCorretagemBMFCollection.Query.FaixaValor.LessThanOrEqual(faixaValor))
                 .OrderBy(tabelaCorretagemBMFCollection.Query.FaixaVencimento.Descending, tabelaCorretagemBMFCollection.Query.FaixaValor.Descending);

            tabelaCorretagemBMFCollection.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled)
            {
                StringBuilder sql = new StringBuilder(tabelaCorretagemBMFCollection.Query.es.LastQuery);
                sql = sql.Replace("@IdPerfilCorretagem1", "'" + idPerfilCorretagem + "'");
                sql = sql.Replace("@DataReferencia2", "'" + dataReferencia.ToString("yyyy-MM-dd") + "'");
                sql = sql.Replace("@FaixaVencimento3", "'" + faixaVencimento + "'");
                sql = sql.Replace("@FaixaValor4", "'" + faixaValor + "'");                
                log.Info(sql);
            }
            #endregion

            if (tabelaCorretagemBMFCollection.HasData)
            {
                // Copia informações de tabelaCorretagemBMFCollection para TabelaCorretagemBMF
                this.AddNew();
                this.TaxaCorretagem = ((TabelaCorretagemBMF)tabelaCorretagemBMFCollection[0]).TaxaCorretagem;
                this.TaxaCorretagemDT = ((TabelaCorretagemBMF)tabelaCorretagemBMFCollection[0]).TaxaCorretagemDT;
                return true;
            }
            else
            {
                return false;
            }          
        }

	}
}
