﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.BMF.Enums;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Util;
using Financial.Common.Exceptions;
using Financial.BMF.Exceptions;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.BMF.Properties;
using Financial.Interfaces.Sinacor;
using Financial.Investidor;
using System.IO;
using Financial.Interfaces.Import.BMF;
using Financial.Investidor.Enums;
using System.Collections;

namespace Financial.BMF {
    public partial class OperacaoBMF : esOperacaoBMF {
        private static readonly ILog log = LogManager.GetLogger(typeof(OperacaoBMF));

        private class EstruturaEmolumentos
        {
            private string cdAtivoBMF;

            public string CdAtivoBMF
            {
                get { return cdAtivoBMF; }
                set { cdAtivoBMF = value; }
            }

            private string serie;

            public string Serie
            {
                get { return serie; }
                set { serie = value; }
            }

            private bool isAlgoTrader;

            public bool IsAlgoTrader
            {
                get { return isAlgoTrader; }
                set { isAlgoTrader = value; }
            }

            int idAgente;

            public int IdAgente
            {
                get { return idAgente; }
                set { idAgente = value; }
            }

            private decimal custoUnitario;

            public decimal CustoUnitario
            {
                get { return custoUnitario; }
                set { custoUnitario = value; }
            }

            private decimal custoUnitarioDescontoDT;

            public decimal CustoUnitarioDescontoDT
            {
                get { return custoUnitarioDescontoDT; }
                set { custoUnitarioDescontoDT = value; }
            }
        }

        public DataTable Table {
            get {
                return this.Table;
            }
        }

        const decimal percOutrosCustosEmolumento = 10.1928M;
        const decimal percOutrosCustosRegistro = 16.6181M;

        /// <summary>
        /// Retorna a quantidade total de contratos operados.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgente"></param>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="data"></param>
        /// <param name="tipoOperacao">Pode ser passado no formato IN (...) da query</param>
        /// <returns></returns>
        public int RetornaTotalOperado(int idCliente, int idAgente, string cdAtivoBMF, DateTime data, string tipoOperacao, int? idConta) 
        {                        
            this.QueryReset();
            this.Query
                 .Select("<SUM( ABS(Quantidade) ) As Quantidade>")
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteLiquidacao == idAgente,
                        this.Query.CdAtivoBMF == cdAtivoBMF,
                        this.Query.Data == data,
                        this.Query.TipoOperacao.In(tipoOperacao),
                        this.Query.Origem.NotIn((byte)OrigemOperacaoBMF.AjusteFuturo,
                                                (byte)OrigemOperacaoBMF.VencimentoFuturo,
                                                (byte)OrigemOperacaoBMF.VencimentoOpcao));

            if (idConta.HasValue && idConta.Value > 0)
            {
                this.Query.Where(this.Query.IdConta == idConta.Value);
            }

            this.Query.Load();
         
            return this.Quantidade.HasValue ? (int)this.Quantidade : 0;
        }

        /// <summary>
        /// Retorna a quantidade total de contratos operados.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgente"></param>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public int RetornaTotalOperado(int idCliente, int idAgente, string cdAtivoBMF, 
                                     DateTime dataInicio, DateTime dataFim, int? idConta)
        {
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Query.Select(operacaoBMFCollection.Query.Quantidade);
            operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.IdCliente == idCliente,
                                              operacaoBMFCollection.Query.IdAgenteLiquidacao == idAgente,
                                              operacaoBMFCollection.Query.CdAtivoBMF == cdAtivoBMF,
                                              operacaoBMFCollection.Query.Data.GreaterThanOrEqual(dataInicio),
                                              operacaoBMFCollection.Query.Data.LessThanOrEqual(dataFim));

            if (idConta.HasValue && idConta.Value > 0)
            {
                operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.IdConta == idConta.Value);
            }

            operacaoBMFCollection.Query.Load();

            int totalOperado = 0;
            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                totalOperado += Math.Abs(operacaoBMF.Quantidade.Value);
            }

            return totalOperado;
        }

        /// <summary>
        /// Calcula as taxas de operação (emolumento e registro) segundo os valores unitários diários da BMF.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaTaxasOperacaoParametrizadas(int idCliente, DateTime data) 
        {
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();

            operacaoBMFCollection.BuscaOperacaoBMFCalculaTaxasParametrizadas(idCliente, data);
            for (int i = 0; i < operacaoBMFCollection.Count; i++) {

                #region Valores OperacaoBMF
                OperacaoBMF operacaoBMF = (OperacaoBMF)operacaoBMFCollection[i];
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                string serie = operacaoBMF.Serie;
                decimal quantidade = operacaoBMF.Quantidade.Value;
                string tipoOperacao = operacaoBMF.TipoOperacao;
                int tipoMercado = operacaoBMF.TipoMercado.Value;
                int pontaEstrategia = operacaoBMF.PontaEstrategia.Value;
                #endregion

                #region Valores Unitários da BMF
                decimal emolumentoUnitario;
                decimal emolumentoDTUnitario;
                decimal registroUnitario;
                decimal registroDTUnitario;

                if (pontaEstrategia == TipoEstrategia.FRC.DDI_LONGO) {
                    cdAtivoBMF = "FRC";
                }

                TabelaCustosBMF tabelaCustosBMF = new TabelaCustosBMF();
                tabelaCustosBMF.BuscaTabelaCustosBMF(cdAtivoBMF, serie, data);
                //tabelaCustosBMF.BuscaTabelaCustosBMF(cdAtivoBMF, serie, data);                
                //Se for sócio pego os valores unitários de Sócio Efetivo.
                //Hoje usa o mesmo % para sócio e Investidor Institucional.
                ClienteBMF clienteBMF = new ClienteBMF();
                clienteBMF.BuscaSocioInvestidor(idCliente);
                if (clienteBMF.IsSocio() || clienteBMF.IsInvestidorInstitucional()) {
                    emolumentoUnitario = tabelaCustosBMF.EmolumentoSocio.Value;
                    emolumentoDTUnitario = tabelaCustosBMF.EmolumentoDTSocio.Value;
                    registroUnitario = tabelaCustosBMF.RegistroSocio.Value;
                    registroDTUnitario = tabelaCustosBMF.RegistroDTSocio.Value;
                }
                else {
                    emolumentoUnitario = tabelaCustosBMF.Emolumento.Value;
                    emolumentoDTUnitario = tabelaCustosBMF.EmolumentoDT.Value;
                    registroUnitario = tabelaCustosBMF.Registro.Value;
                    registroDTUnitario = tabelaCustosBMF.RegistroDT.Value;
                }
                #endregion

                #region Cálculo dos valores de emolumento e registro
                decimal emolumento;
                decimal registro;
                if (tipoOperacao == TipoOperacaoBMF.Compra || tipoOperacao == TipoOperacaoBMF.Venda) {
                    emolumento = Utilitario.Truncate(quantidade * emolumentoUnitario, 2);
                    registro = Utilitario.Truncate(quantidade * registroUnitario, 2);
                }
                else {
                    emolumento = Utilitario.Truncate(quantidade * emolumentoDTUnitario, 2);
                    registro = Utilitario.Truncate(quantidade * registroDTUnitario, 2);
                }

                //Se for a ponta futuro de VOI, VTC, VTF, VID, ou a ponta curta do FRA, emolumento/registro = zero .
                if (pontaEstrategia == TipoEstrategia.FRC.DDI_CURTO || pontaEstrategia == TipoEstrategia.VOI.IND_FUT ||
                    pontaEstrategia == TipoEstrategia.VTC.DOL_FUT || pontaEstrategia == TipoEstrategia.VTF.DI1_FUT_CURTO ||
                    pontaEstrategia == TipoEstrategia.VTF.DI1_FUT_LONGO || pontaEstrategia == TipoEstrategia.VID.DI1_FUT) {
                    emolumento = 0;
                    registro = 0;
                }

                operacaoBMF.Emolumento = emolumento;
                operacaoBMF.Registro = registro;
                #endregion

                //O cálculo abaixo não tem custo unitário associado do TarPreg.
                //Precisa ser calculado "por fora".
                #region Cálculo do custo Wtr
                if (cdAtivoBMF == "WDL" || cdAtivoBMF == "WIN" || cdAtivoBMF == "WCF" || cdAtivoBMF == "WBG") {
                    TabelaCalculoBMF tabelaCalculoBMF = new TabelaCalculoBMF();
                    tabelaCalculoBMF.LoadByPrimaryKey(data, cdAtivoBMF, (byte)tipoMercado);

                    if (!tabelaCalculoBMF.es.HasData) {
                        throw new TabelaCalculoBMFNaoCadastradoException("Tabela Cálculo BMF não cadastrada para o ativo "
                                                                    + cdAtivoBMF + " na data " + data);
                    }

                    decimal formulaCustoWtr = Utilitario.Truncate(tabelaCalculoBMF.P.Value * 0.34M, 2);
                    decimal custoWtrUnitario;
                    decimal custoWtr;
                    if (tipoOperacao == TipoOperacaoBMF.CompraDaytrade || tipoOperacao == TipoOperacaoBMF.VendaDaytrade) {
                        custoWtrUnitario = Utilitario.Truncate(formulaCustoWtr * tabelaCalculoBMF.PercentualDayTrade.Value / 100, 2);
                    }
                    else {
                        custoWtrUnitario = Utilitario.Truncate(formulaCustoWtr, 2);
                    }
                    custoWtr = custoWtrUnitario * quantidade;
                    operacaoBMF.CustoWtr = custoWtr;
                }
                #endregion
            }

            operacaoBMFCollection.Save();
        }

        /// <summary>
        /// Calcula os ajustes sobre operações de futuros e opções com ajustes.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoVisualizacao">Analitico ou Consolidado (para jogar em Liquidacao, vale somente Analitico)</param>
        public void CalculaAjusteOperacao(int idCliente, DateTime data, byte tipoVisualizacao)
        {
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            operacaoBMFCollection.BuscaOperacaoBMFCalculaAjuste(idCliente, data);
            Hashtable hsContaAtivo = new Hashtable();

            for (int i = 0; i < operacaoBMFCollection.Count; i++) 
            {
                #region Valores OperacaoBMF
                OperacaoBMF operacaoBMF = (OperacaoBMF)operacaoBMFCollection[i];
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                string serie = operacaoBMF.Serie;
                int idAgente = operacaoBMF.IdAgenteLiquidacao.Value;
                decimal quantidade = operacaoBMF.Quantidade.Value;
                string tipoOperacao = operacaoBMF.TipoOperacao;
                decimal pu = operacaoBMF.Pu.Value;
                #endregion

                #region Busco a Ptax/IPCA e o PU de fechamento do dia do ativoBMF
                CotacaoBMF cotacaoBMF = new CotacaoBMF();

                decimal valorIndice;
                if (cdAtivoBMF == "DDI") 
                {
                    DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    valorIndice = cotacaoBMF.BuscaPtaxAtivo(cdAtivoBMF, dataAnterior);
                }
                else if (cdAtivoBMF == "DAP") {
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    if (!cotacaoIndice.LoadByPrimaryKey(data, ListaIndiceFixo.IPCA)) {
                        throw new CotacaoIndiceNaoCadastradoException("Cotação IPCA não cadastrada na data " + data);
                    }
                    valorIndice = cotacaoIndice.Valor.Value;
                }
                else {
                    valorIndice = cotacaoBMF.BuscaPtaxAtivo(cdAtivoBMF, data);
                }

                cotacaoBMF.BuscaCotacaoBMF(cdAtivoBMF, serie, data);
                decimal puFechamento = cotacaoBMF.PUFechamento.Value;
                #endregion

                #region Peso do Ativo
                AtivoBMF ativoBMF = new AtivoBMF();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBMF.Query.Peso);
                campos.Add(ativoBMF.Query.IdMoeda);
                ativoBMF.LoadByPrimaryKey(campos, operacaoBMF.CdAtivoBMF, operacaoBMF.Serie);
                decimal peso = ativoBMF.Peso.Value;
                #endregion

                decimal ajuste;
                #region Cálculo do Ajuste Diário
                if (tipoOperacao == TipoOperacaoBMF.Compra || tipoOperacao == TipoOperacaoBMF.CompraDaytrade) {
                    ajuste = Utilitario.Truncate(Utilitario.Truncate(puFechamento - pu, 7) * peso * valorIndice, 2);
                }
                else {
                    ajuste = -1 * Utilitario.Truncate(Utilitario.Truncate(puFechamento - pu, 7) * peso * valorIndice, 2);
                }

                ajuste = Utilitario.Truncate(ajuste * quantidade, 2);
                #endregion

                operacaoBMF.Ajuste = ajuste;

                if (tipoVisualizacao == (byte)VizualizacaoOperacaoBMF.Analitico)
                {
                    int idMoeda = ativoBMF.IdMoeda.Value;

                    int idContaDefault = 0;
                    #region Busca Conta Default
                    if (hsContaAtivo.Contains(cdAtivoBMF + "-" + serie))
                    {
                        idContaDefault = (int)hsContaAtivo[cdAtivoBMF + "-" + serie];
                    }
                    else
                    {
                        Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                        idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoeda);
                        hsContaAtivo.Add(cdAtivoBMF + "-" + serie, idContaDefault);
                    }
                    #endregion

                    DateTime dataLiquidacao = new DateTime();
                    if (idMoeda == (int)ListaMoedaFixo.Real && !AtivoBMF.IsAgricolaCambial(cdAtivoBMF))
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    }
                    else
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros);

                        Cliente cliente = new Cliente();
                        List<esQueryItem> camposCliente = new List<esQueryItem>();
                        camposCliente.Add(cliente.Query.IdMoeda);
                        cliente.LoadByPrimaryKey(camposCliente, idCliente);

                        if (cliente.IdMoeda.Value == (byte)ListaMoedaFixo.Real && !Calendario.IsDiaUtil(dataLiquidacao))
                        {
                            dataLiquidacao = Calendario.AdicionaDiaUtil(dataLiquidacao, 1);
                        }
                    }

                    byte situacaoLiquidacao = 0;
                    if (idMoeda == (int)ListaMoedaFixo.Real)
                    {
                        situacaoLiquidacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    }
                    else
                    {
                        situacaoLiquidacao = (byte)SituacaoLancamentoLiquidacao.Compensacao;
                    }

                    #region Lancamento em Liquidacao
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                                        
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = "Ajuste de Operação de " + cdAtivoBMF + serie;
                    liquidacao.Valor = ajuste;
                    liquidacao.Situacao = situacaoLiquidacao;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgente;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoBMF.IdConta.HasValue && operacaoBMF.IdConta.Value > 0 ? operacaoBMF.IdConta.Value : idContaDefault;
                    liquidacaoCollection.AttachEntity(liquidacao);
                    #endregion
                }
            }

            #region Save das Collections
            operacaoBMFCollection.Save();
            //
            liquidacaoCollection.Save();
            #endregion
        }

        /// <summary>
        /// Transfere as ordens já casadas (em normais e DT) da OrdemBMF para OperacaoBMF.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CarregaOrdemCasada(int idCliente, DateTime data) 
        {
            OperacaoBMFCollection operacaoBMFCollectionDeletar = new OperacaoBMFCollection();
            operacaoBMFCollectionDeletar.DeletaOperacaoOrdemCasada(idCliente, data);

            /* Para salvar a operacaoBMF no final */
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            //
            OrdemBMFCollection ordemBMFCollection = new OrdemBMFCollection();
            ordemBMFCollection.BuscaOrdemBMFCompleta(idCliente, data);
            for (int i = 0; i < ordemBMFCollection.Count; i++) {
                OrdemBMF ordemBMF = (OrdemBMF)ordemBMFCollection[i];

                #region OrdemBMF
                string cdAtivoBMF = ordemBMF.CdAtivoBMF;
                string serie = ordemBMF.Serie;
                // Hierarquico
                decimal peso = ordemBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value;
                int tipoMercado = ordemBMF.TipoMercado.Value;
                string tipoOrdem = ordemBMF.TipoOrdem.Trim().Substring(0,1);
                decimal pu = ordemBMF.Pu.Value;
                int quantidade = ordemBMF.Quantidade.Value;
                int quantidadeDayTrade = ordemBMF.QuantidadeDayTrade.Value;
                int quantidadeNormal = quantidade - quantidadeDayTrade;
                decimal valorNormal = Utilitario.Truncate(quantidadeNormal * pu * peso, 2);
                decimal valorDaytrade = Utilitario.Truncate(quantidadeDayTrade * pu * peso, 2);
                
                decimal corretagem = ordemBMF.Corretagem.Value;
                decimal emolumento = ordemBMF.Emolumento.Value;
                decimal registro = ordemBMF.Registro.Value;
                decimal custoWtr = ordemBMF.CustoWtr.Value;                
                decimal txClearingVlFixo = ordemBMF.TaxaClearingVlFixo.GetValueOrDefault(0);

                decimal corretagemNormal = Utilitario.Truncate(corretagem * quantidadeNormal / quantidade, 2);
                decimal emolumentoNormal = Utilitario.Truncate(emolumento * quantidadeNormal / quantidade, 2);
                decimal registroNormal = Utilitario.Truncate(registro * quantidadeNormal / quantidade, 2);
                decimal custoWtrNormal = Utilitario.Truncate(custoWtr * quantidadeNormal / quantidade, 2);
                decimal txClearingVlFixoNormal = Utilitario.Truncate(txClearingVlFixo * quantidadeNormal / quantidade, 2);

                decimal corretagemDayTrade = corretagem - corretagemNormal;
                decimal emolumentoDayTrade = emolumento - emolumentoNormal;
                decimal registroDayTrade = registro - registroNormal;
                decimal custoWtrDayTrade = custoWtr - custoWtrNormal;
                decimal txClearingVlFixoDayTrade = txClearingVlFixo - txClearingVlFixoNormal;

                int idAgenteCorretora = ordemBMF.IdAgenteCorretora.Value;
                int idAgenteLiquidacao = ordemBMF.IdAgenteLiquidacao.Value;
                int pontaEstrategia = ordemBMF.PontaEstrategia.Value;
                byte fonteOrdem = ordemBMF.Fonte.Value;
                int? idLocalNegociacao = ordemBMF.IdLocalNegociacao;
                int? idLocalCustodia = ordemBMF.IdLocalCustodia;
                int? idClearing = ordemBMF.IdClearing;

                #endregion
                //
                byte fonteOperacao = fonteOrdem;
                if (fonteOperacao == (int)FonteOrdemBMF.Manual)
                {
                    fonteOperacao = (int)FonteOperacaoBMF.OrdemBMF;
                }

                if (quantidadeDayTrade != 0) 
                {                    
                    /* Adiciono 'D' porque a operação é DT / C = CD, V = VD */                   
                    string tipoOperacao = tipoOrdem + "D";                                    

                    #region InsereOperacao
                    OperacaoBMF operacaoBMF = operacaoBMFCollection.AddNew();
                    operacaoBMF.TipoMercado = (byte)tipoMercado;
                    operacaoBMF.TipoOperacao = tipoOperacao;
                    operacaoBMF.Data = data;
                    operacaoBMF.Pu = pu;
                    operacaoBMF.Valor = valorDaytrade;
                    operacaoBMF.ValorLiquido = ordemBMF.IsTipoOrdemCompra()
                                                            ? valorDaytrade + (corretagemDayTrade + emolumentoDayTrade + registroDayTrade + custoWtrDayTrade)
                                                            : valorDaytrade - (corretagemDayTrade + emolumentoDayTrade + registroDayTrade + custoWtrDayTrade);

                    operacaoBMF.PULiquido = Utilitario.Truncate((operacaoBMF.ValorLiquido.Value / quantidadeDayTrade) / peso, 10);
                    /* Taxas */
                    operacaoBMF.Corretagem = corretagemDayTrade;
                    operacaoBMF.Emolumento = emolumentoDayTrade;
                    operacaoBMF.Registro = registroDayTrade;
                    operacaoBMF.CustoWtr = custoWtrDayTrade;
                    //
                    operacaoBMF.Quantidade = quantidadeDayTrade;
                    operacaoBMF.CdAtivoBMF = cdAtivoBMF;
                    operacaoBMF.Serie = serie;
                    operacaoBMF.IdAgenteCorretora = idAgenteCorretora;
                    operacaoBMF.IdAgenteLiquidacao = idAgenteLiquidacao;
                    operacaoBMF.Origem = ordemBMF.Origem;
                    operacaoBMF.Fonte = fonteOperacao;
                    operacaoBMF.IdCliente = idCliente;
                    operacaoBMF.PontaEstrategia = (short)pontaEstrategia;
                    operacaoBMF.IdentificadorEstrategia = ordemBMF.IdentificadorEstrategia.HasValue ? ordemBMF.IdentificadorEstrategia : null;
                    operacaoBMF.NumeroNegocioBMF = ordemBMF.NumeroNegocioBMF.HasValue ? ordemBMF.NumeroNegocioBMF : null;
                    operacaoBMF.Taxa = ordemBMF.Taxa.HasValue ? ordemBMF.Taxa : null;
                    operacaoBMF.CalculaDespesas = ordemBMF.CalculaDespesas;
                    operacaoBMF.PercentualDesconto = ordemBMF.PercentualDesconto;
                    operacaoBMF.IdTrader = ordemBMF.IdTrader;
                    operacaoBMF.IdMoeda = ordemBMF.IdMoeda.Value;
                    operacaoBMF.IdCategoriaMovimentacao = ordemBMF.IdCategoriaMovimentacao.HasValue ? ordemBMF.IdCategoriaMovimentacao : null;
                    operacaoBMF.IdLocalNegociacao = idLocalNegociacao;
                    operacaoBMF.IdLocalCustodia = idLocalCustodia;
                    operacaoBMF.IdClearing = idClearing;
                    operacaoBMF.DataOperacao = ordemBMF.DataOperacao.GetValueOrDefault(data);
                    operacaoBMF.TaxaClearingVlFixo = txClearingVlFixoDayTrade;
                    #endregion
                }

                if (quantidadeNormal != 0) 
                {
                    string tipoOperacao = tipoOrdem;

                    #region InsereOperacao
                    OperacaoBMF operacaoBMF = operacaoBMFCollection.AddNew();
                    operacaoBMF.TipoMercado = (byte)tipoMercado;
                    operacaoBMF.TipoOperacao = tipoOperacao;
                    operacaoBMF.Data = data;
                    operacaoBMF.Pu = pu;
                    operacaoBMF.Valor = valorNormal;
                    operacaoBMF.ValorLiquido = ordemBMF.IsTipoOrdemCompra()
                                                            ? valorNormal + (corretagemNormal + emolumentoNormal + registroNormal + custoWtrNormal)
                                                            : valorNormal - (corretagemNormal + emolumentoNormal + registroNormal + custoWtrNormal);


                    operacaoBMF.PULiquido = Utilitario.Truncate((operacaoBMF.ValorLiquido.Value / quantidadeNormal) / peso, 10);

                    /* Taxas */
                    operacaoBMF.Corretagem = corretagemNormal;
                    operacaoBMF.Emolumento = emolumentoNormal;
                    operacaoBMF.Registro = registroNormal;
                    operacaoBMF.CustoWtr = custoWtrNormal;
                    //
                    operacaoBMF.Quantidade = quantidadeNormal;
                    operacaoBMF.CdAtivoBMF = cdAtivoBMF;
                    operacaoBMF.Serie = serie;
                    operacaoBMF.IdAgenteCorretora = idAgenteCorretora;
                    operacaoBMF.IdAgenteLiquidacao = idAgenteLiquidacao;
                    operacaoBMF.Origem = ordemBMF.Origem;
                    operacaoBMF.Fonte = fonteOperacao;
                    operacaoBMF.IdCliente = idCliente;
                    operacaoBMF.PontaEstrategia = (short)pontaEstrategia;
                    operacaoBMF.IdentificadorEstrategia = ordemBMF.IdentificadorEstrategia.HasValue ? ordemBMF.IdentificadorEstrategia : null;
                    operacaoBMF.NumeroNegocioBMF = ordemBMF.NumeroNegocioBMF.HasValue ? ordemBMF.NumeroNegocioBMF : null;
                    operacaoBMF.Taxa = ordemBMF.Taxa.HasValue ? ordemBMF.Taxa : null;
                    operacaoBMF.Origem = (byte)OrigemOperacaoBMF.Primaria;
                    operacaoBMF.CalculaDespesas = ordemBMF.CalculaDespesas;
                    operacaoBMF.PercentualDesconto = ordemBMF.PercentualDesconto;
                    operacaoBMF.IdTrader = ordemBMF.IdTrader;
                    operacaoBMF.IdMoeda = ordemBMF.IdMoeda.Value;
                    operacaoBMF.IdLocalNegociacao = idLocalNegociacao;
                    operacaoBMF.IdCategoriaMovimentacao = ordemBMF.IdCategoriaMovimentacao.HasValue ? ordemBMF.IdCategoriaMovimentacao : null;
                    operacaoBMF.IdLocalCustodia = idLocalCustodia;
                    operacaoBMF.IdClearing = idClearing;
                    operacaoBMF.DataOperacao = ordemBMF.DataOperacao.GetValueOrDefault(data);
                    operacaoBMF.TaxaClearingVlFixo = txClearingVlFixoNormal;
                    #endregion
                }
            }

            operacaoBMFCollection.Save();
        }

        /// <summary>
        /// Busca a série da ponta curta de DDI referente à estratégia FRA identificada pelo identificadorEstrategia.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="identificadorEstrategia"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaSerieCurtaFRAOperacao(int idCliente, DateTime data, int identificadorEstrategia) 
        {        
            this.QueryReset();
            this.Query
                 .Select(this.query.Serie)
                 .Where(this.query.IdCliente == idCliente,
                        this.query.Data == data,
                        this.query.PontaEstrategia == TipoEstrategia.FRC.DDI_CURTO,
                        this.query.IdentificadorEstrategia == identificadorEstrategia);

            this.Query.Load();

            return this.es.HasData;
        }

        /// <summary>
        /// Calcula as taxas de operação (emolumento, registro, custoWtr) de cada ativo/serie operado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaTaxasOperacao(int idCliente, DateTime data)
        {
            #region Query Inicial para trazer o distinct dos ativos operados
            OperacaoBMFCollection operacaoBMFCollectionAtivos = new OperacaoBMFCollection();
            operacaoBMFCollectionAtivos.Query.Select(operacaoBMFCollectionAtivos.Query.CdAtivoBMF, operacaoBMFCollectionAtivos.Query.IdAgenteCorretora,
                                                     operacaoBMFCollectionAtivos.Query.TipoMercado, operacaoBMFCollectionAtivos.Query.IdTrader);
            operacaoBMFCollectionAtivos.Query.Where(operacaoBMFCollectionAtivos.Query.IdCliente.Equal(idCliente) &
                                                     operacaoBMFCollectionAtivos.Query.Data.Equal(data) &
                                                     operacaoBMFCollectionAtivos.Query.Fonte.NotEqual(FonteOperacaoBMF.Sinacor) &
                                                     operacaoBMFCollectionAtivos.Query.CalculaDespesas.Equal("S") &
                                                     operacaoBMFCollectionAtivos.Query.Origem.NotIn((byte)OrigemOperacaoBMF.AjusteFuturo,
                                                                                 (byte)OrigemOperacaoBMF.TransferenciaCorretora) &
                                                     operacaoBMFCollectionAtivos.Query.CdAtivoBMF.NotIn("DI1", "DOL", "WDL"));
            operacaoBMFCollectionAtivos.Query.GroupBy(operacaoBMFCollectionAtivos.Query.CdAtivoBMF, operacaoBMFCollectionAtivos.Query.IdAgenteCorretora,
                                                      operacaoBMFCollectionAtivos.Query.TipoMercado, operacaoBMFCollectionAtivos.Query.IdTrader);
            operacaoBMFCollectionAtivos.Query.Load();

            string groupBy = "" + OperacaoBMFMetadata.ColumnNames.CdAtivoBMF + "]+[" + OperacaoBMFMetadata.ColumnNames.Serie + "";           
            OperacaoBMFCollection operacaoBMFCollectionAtivosDI1 = new OperacaoBMFCollection();
            operacaoBMFCollectionAtivosDI1.Query.Select((operacaoBMFCollectionAtivosDI1.Query.CdAtivoBMF + operacaoBMFCollectionAtivosDI1.Query.Serie).As("CdAtivoBMF"), 
                                                     operacaoBMFCollectionAtivosDI1.Query.IdAgenteCorretora,
                                                     operacaoBMFCollectionAtivosDI1.Query.TipoMercado, operacaoBMFCollectionAtivosDI1.Query.IdTrader);
            operacaoBMFCollectionAtivosDI1.Query.Where(operacaoBMFCollectionAtivosDI1.Query.IdCliente.Equal(idCliente) &
                                                     operacaoBMFCollectionAtivosDI1.Query.Data.Equal(data) &
                                                     operacaoBMFCollectionAtivosDI1.Query.Fonte.NotEqual(FonteOperacaoBMF.Sinacor) &
                                                     operacaoBMFCollectionAtivosDI1.Query.CalculaDespesas.Equal("S") &
                                                     operacaoBMFCollectionAtivosDI1.Query.Origem.NotIn((byte)OrigemOperacaoBMF.AjusteFuturo,
                                                                                 (byte)OrigemOperacaoBMF.TransferenciaCorretora) &
                                                     operacaoBMFCollectionAtivosDI1.Query.CdAtivoBMF.In("DI1", "DOL", "WDL"));
            operacaoBMFCollectionAtivosDI1.Query.GroupBy(   groupBy, 
                                                            operacaoBMFCollectionAtivosDI1.Query.IdAgenteCorretora,
                                                            operacaoBMFCollectionAtivosDI1.Query.TipoMercado, operacaoBMFCollectionAtivosDI1.Query.IdTrader);
           
            operacaoBMFCollectionAtivosDI1.Query.Load();
            
            operacaoBMFCollectionAtivos.Combine(operacaoBMFCollectionAtivosDI1);
            #endregion

            #region Montagem da lista de custos de emolumentos
            List<EstruturaEmolumentos> listaEmolumento = new List<EstruturaEmolumentos>();
            if (!operacaoBMFCollectionAtivos.HasData)
            {
                return;
            }
            else
            {
                byte tipoPlataforma = (byte)TipoPlataformaOperacional.Normal;
                ClienteBMF clienteBMF = new ClienteBMF();                
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(clienteBMF.Query.TipoPlataforma);
                if (clienteBMF.LoadByPrimaryKey(campos, idCliente))
                {
                    tipoPlataforma = clienteBMF.TipoPlataforma.Value;
                }

                DateTime segundaFeira = Calendario.RetornaPrimeiraSegundaFeira(data);
                DateTime dataFim = Calendario.SubtraiDiaUtil(segundaFeira, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                DateTime dataInicio = Calendario.SubtraiDiaUtil(dataFim, 20, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                foreach (OperacaoBMF operacaoBMFAtivo in operacaoBMFCollectionAtivos)
                {
                    string cdAtivoBMF = operacaoBMFAtivo.CdAtivoBMF;

                    string serie = ""; //Só usado para o caso de DI1 e para rolagem de DOL/WDL
                    if (cdAtivoBMF.Contains("DI1") || cdAtivoBMF.Contains("DOL") || cdAtivoBMF.Contains("WDL"))
                    {
                        string cdAtivoBMFCompleto = cdAtivoBMF;
                        cdAtivoBMF = cdAtivoBMFCompleto.Substring(0, 3);
                        serie = cdAtivoBMFCompleto.Replace(cdAtivoBMF, "");                        
                    }

                    byte tipoMercado = operacaoBMFAtivo.TipoMercado.Value;
                    int idAgenteCorretora = operacaoBMFAtivo.IdAgenteCorretora.Value;
                    int? idTrader = operacaoBMFAtivo.IdTrader;

                    bool isAlgoTrader = false;

                    if (idTrader.HasValue)
                    {
                        Trader trader = new Trader();
                        campos = new List<esQueryItem>();
                        campos.Add(trader.Query.AlgoTrader);
                        trader.LoadByPrimaryKey(idTrader.Value);
                        if (trader.AlgoTrader == "S")
                        {
                            isAlgoTrader = true;
                        }                        
                    }
                    
                    decimal volume = 0;
                    #region Calcula o volume operado nos 21 pregões anteriores
                    if (!isAlgoTrader)
                    {
                        OperacaoBMFQuery operacaoBMFQuery = new OperacaoBMFQuery("O");
                        TraderQuery traderQuery = new TraderQuery("T");
                        operacaoBMFQuery.Select(operacaoBMFQuery.Quantidade.Sum());
                        operacaoBMFQuery.LeftJoin(traderQuery).On(traderQuery.IdTrader == operacaoBMFQuery.IdTrader);
                        operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente) &
                                               operacaoBMFQuery.CdAtivoBMF.Equal(cdAtivoBMF) &
                                               operacaoBMFQuery.TipoMercado.Equal(tipoMercado) &
                                               operacaoBMFQuery.IdAgenteCorretora.Equal(idAgenteCorretora) &
                                               operacaoBMFQuery.Data.LessThanOrEqual(dataFim) &
                                               operacaoBMFQuery.Data.GreaterThanOrEqual(dataInicio) &
                                               operacaoBMFQuery.CalculaDespesas.Equal("S") &
                                               operacaoBMFQuery.Origem.NotIn((byte)OrigemOperacaoBMF.AjusteFuturo,
                                                                             (byte)OrigemOperacaoBMF.TransferenciaCorretora)
                                                                             );
                        operacaoBMFQuery.Where(traderQuery.AlgoTrader == "N" | operacaoBMFQuery.IdTrader.IsNull());
                    
                        OperacaoBMF operacaoBMFVolume = new OperacaoBMF();
                        operacaoBMFVolume.Load(operacaoBMFQuery);

                        if (operacaoBMFVolume.Quantidade.HasValue)
                        {
                            volume = operacaoBMFVolume.Quantidade.Value;
                        }
                    }
                    #endregion

                    volume = Utilitario.Truncate(volume / 21, 0);

                    decimal custoUnitario = this.RetornaCustoUnitarioEmolumento(data, cdAtivoBMF, tipoMercado, volume);

                    #region Tratamento especial para DI1
                    if (cdAtivoBMF == "DI1")
                    {
                        custoUnitario = custoUnitario / 100M + 1M;
                        //Busca qtde de dias até o vcto
                        AtivoBMF ativoBMF = new AtivoBMF();                        
                        campos = new List<esQueryItem>();
                        campos.Add(ativoBMF.Query.DataVencimento);
                        ativoBMF.LoadByPrimaryKey(campos, cdAtivoBMF, serie);
                        DateTime dataVencimento = ativoBMF.DataVencimento.Value;
                        int numeroDiasVencimento = Calendario.NumeroDias(data, dataVencimento, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                        if (numeroDiasVencimento > 105)
                        {
                            numeroDiasVencimento = 105;
                        }
                        double potencia = (double)(numeroDiasVencimento / 252M);
                        custoUnitario = Convert.ToDecimal(Math.Pow((double)custoUnitario, potencia)) - 1M;
                        custoUnitario = custoUnitario * 100000M;
                    }
                    #endregion

                    if (cdAtivoBMF == "DOL" || cdAtivoBMF == "WDL")
                    {
                        AtivoBMF ativoBMF = new AtivoBMF();
                        campos = new List<esQueryItem>();
                        campos.Add(ativoBMF.Query.DataVencimento);
                        ativoBMF.LoadByPrimaryKey(campos, cdAtivoBMF, serie);
                        DateTime dataVencimento = ativoBMF.DataVencimento.Value;
                        DateTime dataVencimento2DiasAntes = Calendario.SubtraiDiaUtil(dataVencimento, 2, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                        
                        if (DateTime.Compare(data, dataVencimento2DiasAntes) == 0)
                        {
                            if (isAlgoTrader)
                            {
                                custoUnitario = custoUnitario * 0.5M;
                            }
                            else
                            {
                                custoUnitario = Utilitario.Truncate(custoUnitario * 0.5M, 2);
                            }
                        }
                    }

                    if (cdAtivoBMF == "DI1" && !isAlgoTrader && tipoPlataforma == (byte)TipoPlataformaOperacional.Normal)
                    {
                        custoUnitario = Utilitario.Truncate(custoUnitario, 2);
                    }

                    //Desconto para operações por Algorithmic Trader ou via DMA
                    if (isAlgoTrader)
                    {
                        if (cdAtivoBMF == "DI1")
                        {
                            decimal custoUnitarioDesconto = custoUnitario * 0.7M;
                            custoUnitario -= custoUnitarioDesconto;
                            custoUnitario = Utilitario.Truncate(custoUnitario, 2);
                        }
                        else
                        {
                            decimal custoUnitarioDesconto = custoUnitario * 0.7M;
                            custoUnitario = custoUnitario - custoUnitarioDesconto;
                            custoUnitario = Utilitario.Truncate(custoUnitario, 2);                            
                        }
                    } 
                    else if (tipoPlataforma == (byte)TipoPlataformaOperacional.DMA)
                    {
                        if (cdAtivoBMF == "DI1")
                        {
                            decimal custoUnitarioDesconto = custoUnitario * 0.1M;
                            custoUnitario -= custoUnitarioDesconto;
                            custoUnitario = Utilitario.Truncate(custoUnitario, 2);
                        }
                        else
                        {
                            decimal custoUnitarioDesconto = Utilitario.Truncate(custoUnitario * 0.1M, 2);
                            custoUnitario = custoUnitario - custoUnitarioDesconto;
                        }                        
                    }                    
                    //

                    decimal descontoDayTrade = this.RetornaDescontoDayTrade(data, cdAtivoBMF);
                    
                    EstruturaEmolumentos estruturaEmolumentos = new EstruturaEmolumentos();
                    estruturaEmolumentos.CdAtivoBMF = cdAtivoBMF;
                    estruturaEmolumentos.Serie = serie;
                    estruturaEmolumentos.IsAlgoTrader = isAlgoTrader;
                    estruturaEmolumentos.IdAgente = idAgenteCorretora;
                    estruturaEmolumentos.CustoUnitario = custoUnitario;
                    estruturaEmolumentos.CustoUnitarioDescontoDT = Utilitario.Truncate(custoUnitario * (1 - descontoDayTrade / 100M), 2);
                    listaEmolumento.Add(estruturaEmolumentos);
                }
            }
            #endregion

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();

            operacaoBMFCollection.BuscaOperacaoBMFCalculaTaxas(idCliente, data);

            decimal totalEmolumento = 0;
            decimal totalRegistro = 0;
            for (int i = 0; i < operacaoBMFCollection.Count; i++) 
            {
                #region Valores OperacaoBMF
                OperacaoBMF operacaoBMF = (OperacaoBMF)operacaoBMFCollection[i];
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                string serie = operacaoBMF.Serie;
                byte tipoMercado = operacaoBMF.TipoMercado.Value;
                decimal quantidade = operacaoBMF.Quantidade.Value;
                string tipoOperacao = operacaoBMF.TipoOperacao;
                int idAgenteCorretora = operacaoBMF.IdAgenteCorretora.Value;
                int? idTrader = operacaoBMF.IdTrader;
                #endregion

                bool isAlgoTrader = false;
                if (idTrader.HasValue)
                {
                    Trader trader = new Trader();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(trader.Query.AlgoTrader);
                    trader.LoadByPrimaryKey(idTrader.Value);
                    if (trader.AlgoTrader == "S")
                    {
                        isAlgoTrader = true;
                    }                        
                }

                EstruturaEmolumentos estruturaEmolumentos = new EstruturaEmolumentos();

                if (cdAtivoBMF == "DI1" || cdAtivoBMF == "DOL" || cdAtivoBMF == "WDL")
                {
                    estruturaEmolumentos = listaEmolumento.Find(delegate(EstruturaEmolumentos e)
                    {
                        return e.CdAtivoBMF == cdAtivoBMF && e.Serie == serie && e.IdAgente == idAgenteCorretora && e.IsAlgoTrader == isAlgoTrader;
                    });
                }
                else
                {
                    estruturaEmolumentos = listaEmolumento.Find(delegate(EstruturaEmolumentos e)
                    {
                        return e.CdAtivoBMF == cdAtivoBMF && e.IdAgente == idAgenteCorretora && e.IsAlgoTrader == isAlgoTrader;
                    });
                }

                decimal emolumento = 0;
                if (tipoOperacao == TipoOperacaoBMF.CompraDaytrade || tipoOperacao == TipoOperacaoBMF.VendaDaytrade)
                {
                    emolumento = Utilitario.Truncate(quantidade * estruturaEmolumentos.CustoUnitarioDescontoDT, 2);
                }
                else
                {
                    emolumento = Utilitario.Truncate(quantidade * estruturaEmolumentos.CustoUnitario, 2);
                }

                decimal registro = 0;
                if (cdAtivoBMF != "WIN" && cdAtivoBMF != "WDL") //Mini indice nao cobra registro
                {
                    registro = Utilitario.Truncate(quantidade * 0.10M, 2);
                }
            
                decimal custoWtr = 0;

                operacaoBMF.Emolumento = emolumento;
                operacaoBMF.Registro = registro;
                operacaoBMF.CustoWtr = custoWtr;

                totalEmolumento += emolumento;
                totalRegistro += registro;

                if (i == (operacaoBMFCollection.Count - 1))
                {
                    decimal outrosCustosEmolumento = Math.Round(totalEmolumento * percOutrosCustosEmolumento / 100, 2);
                    decimal outrosCustosRegistro = Math.Round(totalRegistro * percOutrosCustosRegistro / 100, 2);
                    operacaoBMF.OutrosCustos = outrosCustosEmolumento + outrosCustosRegistro;
                }
            }

            operacaoBMFCollection.Save();
        }

        /// <summary>
        /// Retorna o custo unitário baseado na tabela de faixas.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaCustoUnitarioEmolumento(DateTime data, string cdAtivoBMF, byte tipoMercado, decimal volume)
        {
            TabelaCustoUnitarioBMF tabelaCustoUnitarioBMF = new TabelaCustoUnitarioBMF();
            decimal custoUnitarioTabela = tabelaCustoUnitarioBMF.RetornaCustoUnitario(cdAtivoBMF, volume, data);

            return custoUnitarioTabela;
        }

        /// <summary>
        /// Retorna o desconto para DayTrade.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaDescontoDayTrade(DateTime data, string cdAtivoBMF)
        {
            decimal descontoDayTrade = 0;
            switch (cdAtivoBMF)
            {
                case "IND":
                    descontoDayTrade = 70M;
                    break;
                case "DI1":
                case "WIN":
                case "DOL":
                case "WDL":
                    descontoDayTrade = 50M;
                    break;
            }

            return descontoDayTrade;
        }

        /// <summary>
        /// Calcula as taxas de operação (emolumento, registro, custoWtr) de cada ativo/serie operado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaTaxasOperacaoAntigo(int idCliente, DateTime data)
        {
            #region Inicializa as variáveis principais
            decimal emolumentoUnitario = 0;
            decimal registroUnitario = 0;
            decimal custoWtrUnitario = 0;
            decimal emolumento = 0;
            decimal registro = 0;
            decimal custoWtr = 0;
            decimal formulaBase = 0;
            #endregion

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();

            operacaoBMFCollection.BuscaOperacaoBMFCalculaTaxas(idCliente, data);
            for (int i = 0; i < operacaoBMFCollection.Count; i++)
            {

                #region Valores OperacaoBMF
                OperacaoBMF operacaoBMF = (OperacaoBMF)operacaoBMFCollection[i];
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                string serie = operacaoBMF.Serie;
                int tipoMercado = operacaoBMF.TipoMercado.Value;
                decimal quantidade = operacaoBMF.Quantidade.Value;
                string tipoOperacao = operacaoBMF.TipoOperacao;
                int pontaEstrategia = operacaoBMF.PontaEstrategia.Value;
                int identificadorEstrategia = operacaoBMF.IdentificadorEstrategia.Value;
                #endregion

                TabelaCalculoBMF tabelaCalculoBMF = new TabelaCalculoBMF();
                if (!tabelaCalculoBMF.LoadByPrimaryKey(data, cdAtivoBMF, (byte)tipoMercado))
                {
                    throw new TabelaCalculoBMFNaoCadastradoException("Tabela de Cálculo BMF não cadastrada na data " + data + " para o ativo " + cdAtivoBMF);
                }

                #region Tratamento especifico para DDI em casos de estrategia (FRA)
                int diasCorridosDDICurto = 0;
                if (cdAtivoBMF == "DDI" && pontaEstrategia == TipoEstrategia.FRC.DDI_LONGO)
                {
                    OperacaoBMF operacaoBMFDDI = new OperacaoBMF();
                    if (!operacaoBMFDDI.BuscaSerieCurtaFRAOperacao(idCliente, data, identificadorEstrategia))
                    {
                        throw new OperacaoBMFNaoLancadaException("Ponta Curta da Estratégia FRA não encontrada.");
                    }
                    string serieDDICurta = operacaoBMFDDI.Serie;

                    TabelaVencimentosBMF tabelaVencimentosBMFDDICurto = new TabelaVencimentosBMF();
                    if (!tabelaVencimentosBMFDDICurto.LoadByPrimaryKey(data, cdAtivoBMF, serieDDICurta))
                    {
                        throw new TabelaVencimentosBMFNaoCadastradoException("Tabela de Vencimentos BMF não cadastrada na data " + data + " para o ativo " + cdAtivoBMF + serieDDICurta);
                    }
                    diasCorridosDDICurto = tabelaVencimentosBMFDDICurto.DiasCorridos.Value;

                    TabelaCalculoBMF tabelaCalculoBMFFRA = new TabelaCalculoBMF();
                    if (!tabelaCalculoBMFFRA.LoadByPrimaryKey(data, "FRC", (byte)tipoMercado))
                    {
                        throw new TabelaCalculoBMFNaoCadastradoException("Tabela de Cálculo BMF não cadastrada na data " + data + " para o ativo FRC");
                    }
                    tabelaCalculoBMF = (TabelaCalculoBMF)Utilitario.Clone(tabelaCalculoBMFFRA);
                }

                if (pontaEstrategia == TipoEstrategia.FRC.DDI_CURTO || pontaEstrategia == TipoEstrategia.FRC.DDI_LONGO)
                {
                    cdAtivoBMF = "FRC";
                }
                #endregion

                TabelaVencimentosBMF tabelaVencimentosBMF = new TabelaVencimentosBMF();
                if (!tabelaVencimentosBMF.LoadByPrimaryKey(data, cdAtivoBMF, serie))
                {
                    throw new TabelaVencimentosBMFNaoCadastradoException("Tabela de Vencimentos BMF não cadastrada na data " + data + " para o ativo " + cdAtivoBMF + serie);
                }

                int diasCorridos = tabelaVencimentosBMF.DiasCorridos.Value;
                int diasUteis = tabelaVencimentosBMF.DiasUteis.Value;

                #region Tratamento especifico para D11, D12, D13 (pega DI1 correspondente para diasUteis)
                if (cdAtivoBMF == "D11" || cdAtivoBMF == "D12" || cdAtivoBMF == "D13")
                {
                    AtivoBMFCollection ativoBMFDI1Collection = new AtivoBMFCollection();
                    AtivoBMF ativoBMFDI1 = new AtivoBMF();
                    ativoBMFDI1Collection.BuscaVencimentos("DI1", operacaoBMF.UpToAtivoBMFByCdAtivoBMF.DataVencimento.Value);

                    if (cdAtivoBMF == "D11")
                    {
                        ativoBMFDI1 = (AtivoBMF)ativoBMFDI1Collection[3];
                    }
                    else if (cdAtivoBMF == "D12")
                    {
                        ativoBMFDI1 = (AtivoBMF)ativoBMFDI1Collection[6];
                    }
                    else if (cdAtivoBMF == "D13")
                    {
                        ativoBMFDI1 = (AtivoBMF)ativoBMFDI1Collection[12];
                    }

                    string serieDI1 = ativoBMFDI1.Serie;

                    TabelaVencimentosBMF tabelaVencimentosBMFDI1 = new TabelaVencimentosBMF();
                    if (!tabelaVencimentosBMFDI1.LoadByPrimaryKey(data, "DI1", serieDI1))
                    {
                        throw new TabelaVencimentosBMFNaoCadastradoException("Tabela de Vencimentos BMF não cadastrada na data " + data + " para o ativo DI1" + serieDI1);
                    }
                    diasUteis = tabelaVencimentosBMFDI1.DiasUteis.Value;
                }
                #endregion

                #region Cálculo do Emolumento Unitário
                //Calcula a fórmula base de cada ativo prevista pela BMF.
                AtivoBMF ativoBMF = new AtivoBMF();
                ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);

                if (cdAtivoBMF == "FRC")
                {
                    formulaBase = ativoBMF.RetornaFormulaBaseEmolumentoFRC(data, ativoBMF, tabelaCalculoBMF,
                                                                           diasCorridos, diasCorridosDDICurto,
                                                                           pontaEstrategia, false);
                }
                else
                {
                    formulaBase = ativoBMF.RetornaFormulaBaseEmolumento(data, ativoBMF, tabelaCalculoBMF,
                                                                        diasUteis, diasCorridos,
                                                                        pontaEstrategia);
                }

                //Contratos mini usam custo fixo direto ditado pelo percentual P.
                if (cdAtivoBMF == "WDL" || cdAtivoBMF == "WBG" || cdAtivoBMF == "WIN")
                {
                    formulaBase = tabelaCalculoBMF.P.Value;
                }

                //Clientes sócios da BMF têm desconto no emolumento.
                //Hoje usa o mesmo % para sócio e Investidor Institucional.
                ClienteBMF clienteBMF = new ClienteBMF();
                clienteBMF.BuscaSocioInvestidor(idCliente);
                if (clienteBMF.IsSocio() || clienteBMF.IsInvestidorInstitucional())
                {
                    if (tipoOperacao == TipoOperacaoBMF.Compra || tipoOperacao == TipoOperacaoBMF.Venda)
                    {
                        emolumentoUnitario = Utilitario.Truncate(ParametrosConfiguracaoSistema.BMF.DescontoSocio / 100M * formulaBase, 2);
                    }
                    else
                    {
                        emolumentoUnitario = Utilitario.Truncate(ParametrosConfiguracaoSistema.BMF.DescontoSocio / 100M * formulaBase *
                                                                 tabelaCalculoBMF.PercentualDayTrade.Value / 100M, 2);
                    }
                }
                else
                {
                    if (tipoOperacao == TipoOperacaoBMF.Compra || tipoOperacao == TipoOperacaoBMF.Venda) { emolumentoUnitario = Utilitario.Truncate(formulaBase, 2); }
                    else { emolumentoUnitario = Utilitario.Truncate(formulaBase * tabelaCalculoBMF.PercentualDayTrade.Value / 100M, 2); }
                }

                //Teste de valor mínimo de emolumento unitário.
                emolumentoUnitario = Math.Max(emolumentoUnitario, 0.01M);
                #endregion

                #region Cálculo do Emolumento Final
                //Contratos mini têm redução por sem operados no WebTrading.
                if (cdAtivoBMF == "WDL" || cdAtivoBMF == "WBG" || cdAtivoBMF == "WIN")
                {
                    emolumento = Utilitario.Truncate(emolumentoUnitario * 0.07M, 2) * (quantidade);
                }
                else
                {
                    emolumento = emolumentoUnitario * quantidade;
                }

                //Se for a ponta futuro de VOI, VTC, VTF, VID, ou a ponta curta do FRA, emolumento = zero.
                if (pontaEstrategia == TipoEstrategia.FRC.DDI_CURTO || pontaEstrategia == TipoEstrategia.VOI.IND_FUT ||
                    pontaEstrategia == TipoEstrategia.VTC.DOL_FUT || pontaEstrategia == TipoEstrategia.VTF.DI1_FUT_CURTO ||
                    pontaEstrategia == TipoEstrategia.VTF.DI1_FUT_LONGO || pontaEstrategia == TipoEstrategia.VID.DI1_FUT)
                {
                    emolumento = 0;
                }
                #endregion

                #region Cálculo do Registro Unitário

                #region DI1, IDI, DIA, D11, D12, D13
                if (cdAtivoBMF == "DI1" || cdAtivoBMF == "IDI" || cdAtivoBMF == "DIA" || cdAtivoBMF == "D11" ||
                    cdAtivoBMF == "D12" || cdAtivoBMF == "D13")
                {
                    formulaBase = ativoBMF.RetornaFormulaBaseEmolumento(data, ativoBMF, tabelaCalculoBMF,
                                                                        42, 42, pontaEstrategia);

                    registroUnitario = Utilitario.Truncate(formulaBase * tabelaCalculoBMF.PercentualRegistro.Value / 100M, 2);

                    //Clientes sócios da BMF têm desconto no registro.
                    //Hoje usa o mesmo % para sócio e Investidor Institucional.
                    clienteBMF = new ClienteBMF();
                    clienteBMF.BuscaSocioInvestidor(idCliente);
                    if (clienteBMF.IsSocio() || clienteBMF.IsInvestidorInstitucional())
                    {
                        registroUnitario = Utilitario.Truncate(ParametrosConfiguracaoSistema.BMF.DescontoSocio / 100M * registroUnitario, 2);
                    }
                    else
                    {
                        registroUnitario = Utilitario.Truncate(registroUnitario, 2);
                    }
                }

                #endregion

                #region DDI, SCC, SC2, SC3
                // Atentar para o uso do ativo original >> operacaoBMF.CdAtivoBMF, no caso de DDI                
                else if (operacaoBMF.CdAtivoBMF == "DDI" || cdAtivoBMF == "SCC" || cdAtivoBMF == "SC2" || cdAtivoBMF == "SC3")
                {
                    bool calculaRegistroFRC;
                    if (cdAtivoBMF == "FRC")
                    {
                        calculaRegistroFRC = true;
                    }
                    else
                    {
                        calculaRegistroFRC = false;
                    }

                    formulaBase = ativoBMF.RetornaFormulaBaseEmolumentoFRC(data, ativoBMF, tabelaCalculoBMF,
                                                                           60, diasCorridosDDICurto, pontaEstrategia,
                                                                           calculaRegistroFRC);

                    registroUnitario = Utilitario.Truncate(formulaBase * tabelaCalculoBMF.PercentualRegistro.Value / 100M, 2);

                    //Clientes sócios da BMF têm desconto no registro.
                    //Hoje usa o mesmo % para sócio e Investidor Institucional.
                    clienteBMF = new ClienteBMF();
                    clienteBMF.BuscaSocioInvestidor(idCliente);
                    if (clienteBMF.IsSocio() || clienteBMF.IsInvestidorInstitucional())
                    {
                        registroUnitario = Utilitario.Truncate(ParametrosConfiguracaoSistema.BMF.DescontoSocio / 100M * registroUnitario, 2);
                    }
                    else
                    {
                        registroUnitario = Utilitario.Truncate(registroUnitario, 2);
                    }
                }

                #endregion

                #region DOL, DLA
                else if (cdAtivoBMF == "DOL" || cdAtivoBMF == "DLA")
                {
                    formulaBase = ativoBMF.RetornaFormulaBaseEmolumento(data, ativoBMF, tabelaCalculoBMF,
                                                                        diasUteis, diasCorridos, pontaEstrategia);
                    DateTime dataPosterior = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    DateTime dataPosterior2Dias = Calendario.AdicionaDiaUtil(dataPosterior, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    if (ativoBMF.DataVencimento == dataPosterior || ativoBMF.DataVencimento == dataPosterior2Dias)
                    {
                        registroUnitario = Utilitario.Truncate((0.2M) * formulaBase, 2);
                    }
                    else
                    {
                        registroUnitario = Utilitario.Truncate(formulaBase * tabelaCalculoBMF.PercentualRegistro.Value / 100M, 2);
                    }

                    //Clientes sócios da BMF têm desconto no registro.
                    //Hoje usa o mesmo % para sócio e Investidor Institucional.
                    clienteBMF = new ClienteBMF();
                    clienteBMF.BuscaSocioInvestidor(idCliente);
                    if (clienteBMF.IsSocio() || clienteBMF.IsInvestidorInstitucional())
                    {
                        registroUnitario = Utilitario.Truncate(ParametrosConfiguracaoSistema.BMF.DescontoSocio / 100M * registroUnitario, 2);
                    }
                    else
                    {
                        registroUnitario = Utilitario.Truncate(registroUnitario, 2);
                    }
                }

                #endregion

                #region Else
                else
                {
                    formulaBase = ativoBMF.RetornaFormulaBaseEmolumento(data, ativoBMF, tabelaCalculoBMF,
                                                                        diasUteis, diasCorridos, pontaEstrategia);

                    registroUnitario = Utilitario.Truncate(formulaBase * tabelaCalculoBMF.PercentualRegistro.Value / 100M, 2);

                    //Clientes sócios da BMF têm desconto no registro.
                    //Hoje usa o mesmo % para sócio e Investidor Institucional.
                    clienteBMF = new ClienteBMF();
                    clienteBMF.BuscaSocioInvestidor(idCliente);
                    if (clienteBMF.IsSocio() || clienteBMF.IsInvestidorInstitucional())
                    {
                        registroUnitario = Utilitario.Truncate(ParametrosConfiguracaoSistema.BMF.DescontoSocio / 100M * registroUnitario, 2);
                    }
                    else
                    {
                        registroUnitario = Utilitario.Truncate(registroUnitario, 2);
                    }
                }
                #endregion

                //Teste de valor mínimo de registro unitário.
                registroUnitario = Math.Max(registroUnitario, 0.01M);

                #endregion

                #region Cálculo do Registro Final
                registro = registroUnitario * quantidade;

                //Se for a ponta futuro de VOI, VTC, VTF, VID, ou a ponta curta do FRA, registro = zero.
                if (pontaEstrategia == TipoEstrategia.FRC.DDI_CURTO || pontaEstrategia == TipoEstrategia.VOI.IND_FUT ||
                    pontaEstrategia == TipoEstrategia.VTC.DOL_FUT || pontaEstrategia == TipoEstrategia.VTF.DI1_FUT_CURTO ||
                    pontaEstrategia == TipoEstrategia.VTF.DI1_FUT_LONGO || pontaEstrategia == TipoEstrategia.VID.DI1_FUT)
                {
                    registro = 0;
                }
                #endregion

                #region Cálculo do custo Wtr
                if (cdAtivoBMF == "WDL" || cdAtivoBMF == "WIN" || cdAtivoBMF == "WCF" || cdAtivoBMF == "WBG")
                {
                    decimal formulaCustoWtr = Utilitario.Truncate(tabelaCalculoBMF.P.Value * 0.34M, 2);
                    if (tipoOperacao == TipoOperacaoBMF.CompraDaytrade || tipoOperacao == TipoOperacaoBMF.VendaDaytrade)
                    {
                        custoWtrUnitario = Utilitario.Truncate(formulaCustoWtr * tabelaCalculoBMF.PercentualDayTrade.Value / 100, 2);
                    }
                    else
                    {
                        custoWtrUnitario = Utilitario.Truncate(formulaCustoWtr, 2);
                    }
                    custoWtr = custoWtrUnitario * quantidade;
                }
                #endregion

                operacaoBMF.Emolumento = emolumento;
                operacaoBMF.Registro = registro;
                operacaoBMF.CustoWtr = custoWtr;

            }

            operacaoBMFCollection.Save();
        }

        /// <summary>
        /// Processa as compras do dia, gerando nova custódia e apurando o resultado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>        
        public void ProcessaCompra(int idCliente, DateTime data)
        {
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            PosicaoBMF posicaoBMF = new PosicaoBMF();

            ClienteBMF clienteBMF = new ClienteBMF();
            clienteBMF.LoadByPrimaryKey(idCliente);

            int? idAgenteCentralizado = clienteBMF.IdCustodianteBmf;
            //

            operacaoBMFCollection.BuscaOperacaoBMFCompra(idCliente, data);
            for (int i = 0; i < operacaoBMFCollection.Count; i++) 
            {
                OperacaoBMF operacaoBMF = operacaoBMFCollection[i];
                 
                //Centralização de custódia
                if (operacaoBMF.Origem != (byte)OrigemOperacaoBMF.TransferenciaCorretora)
                    operacaoBMF.IdAgenteLiquidacao = idAgenteCentralizado ?? operacaoBMF.IdAgenteLiquidacao.Value;

                int idAgente = operacaoBMF.IdAgenteLiquidacao.Value;
                string cdAtivo = operacaoBMF.CdAtivoBMF;
                string serie = operacaoBMF.Serie;
                int quantidadeOperacao = operacaoBMF.Quantidade.Value;
                decimal puOperacao = operacaoBMF.Pu.Value;
                decimal puLiquidoOperacao = operacaoBMF.PULiquido.Value;
                decimal ajuste = operacaoBMF.Ajuste.Value;
                int? idConta = ParametrosConfiguracaoSistema.Outras.MultiConta ? operacaoBMF.IdConta : null;

                // Tem Posição para o ativo - Atualiza a posição
                if (posicaoBMF.BuscaPosicaoBMF(idCliente, idAgente, cdAtivo, serie, idConta))
                {
                    decimal quantidadePosicao = posicaoBMF.Quantidade.Value;
                    // Comprado
                    if (quantidadePosicao >= 0) {
                        // Ja possui posicao para o ativo - Atualizar quantidade e preco medio                  
                        posicaoBMF.AtualizaPosicaoCompraComprado(posicaoBMF.IdPosicao.Value, quantidadeOperacao,
                                                                 puOperacao, puLiquidoOperacao);
                    }
                    // Vendido
                    else 
                    {
                        // Se quantidade comprada for suficiente para passar de uma posicao negativa 
                        // para positivo
                        if (quantidadeOperacao > Math.Abs(quantidadePosicao)) 
                        {
                            // Baixa posicao - Atualizar quantidade                                                        
                            operacaoBMF.ProcessaResultadoNormal(operacaoBMF, posicaoBMF);                        
                            posicaoBMF.AtualizaPosicaoCompraVendido(posicaoBMF.IdPosicao.Value, quantidadeOperacao,
                                        puOperacao, puLiquidoOperacao);
                        }
                        else 
                        {
                            // Baixa posicao - Atualizar apenas a quantidade
                            operacaoBMF.ProcessaResultadoNormal(operacaoBMF, posicaoBMF);
                            posicaoBMF.AtualizaQuantidade(posicaoBMF.IdPosicao.Value, quantidadeOperacao);
                        }
                    }
                    
                }
                // Não tem posição para o Ativo - Inclusão da nova Posição
                else {
                    posicaoBMF.InserePosicaoBMF(operacaoBMF);
                }
            }
        }

        /// <summary>
        /// Processa as vendas do dia, gerando nova custódia e apurando o resultado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>        
        public void ProcessaVenda(int idCliente, DateTime data) 
        {
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            PosicaoBMF posicaoBMF = new PosicaoBMF();
            //

            ClienteBMF clienteBMF = new ClienteBMF();
            clienteBMF.LoadByPrimaryKey(idCliente);

            int? idAgenteCentralizado = clienteBMF.IdCustodianteBmf;
            //

            operacaoBMFCollection.BuscaOperacaoBMFVenda(idCliente, data);
            for (int i = 0; i < operacaoBMFCollection.Count; i++) 
            {
                OperacaoBMF operacaoBMF = operacaoBMFCollection[i];

                //Centralização de custódia
                if (operacaoBMF.Origem != (byte)OrigemOperacaoBMF.TransferenciaCorretora)
                    operacaoBMF.IdAgenteLiquidacao = idAgenteCentralizado ?? operacaoBMF.IdAgenteLiquidacao.Value;

                decimal puOperacao = operacaoBMF.Pu.Value;
                decimal puLiquidoOperacao = operacaoBMF.Pu.Value;
                int quantidadeOperacao = operacaoBMF.Quantidade.Value;
                string cdAtivo = operacaoBMF.CdAtivoBMF;
                string serie = operacaoBMF.Serie;
                int idAgente = operacaoBMF.IdAgenteLiquidacao.Value;
                decimal ajuste = operacaoBMF.Ajuste.Value;
                int? idConta = ParametrosConfiguracaoSistema.Outras.MultiConta ? operacaoBMF.IdConta : null;

                // Tem Posição para o ativo - Atualiza a posição
                if (posicaoBMF.BuscaPosicaoBMF(idCliente, idAgente, cdAtivo, serie, idConta))
                {
                    decimal quantidadePosicao = posicaoBMF.Quantidade.Value;
                    // Vendido
                    if (quantidadePosicao <= 0) {
                        // Ja possui posicao para o ativo - Atualizar quantidade e preco medio                  
                        posicaoBMF.AtualizaPosicaoVendaVendido(posicaoBMF.IdPosicao.Value, quantidadeOperacao,
                                                                   puOperacao, puLiquidoOperacao);
                    }
                    // Comprado
                    else 
                    {
                        // Se quantidade comprada for suficiente para passar de uma posicao positiva 
                        // para negativa
                        int quantidadeOperacaoNegativa = -1 * quantidadeOperacao;
                        if (quantidadeOperacao > quantidadePosicao) 
                        {
                            operacaoBMF.ProcessaResultadoNormal(operacaoBMF, posicaoBMF);
                            // Baixa posicao - Atualizar quantidade                                                        
                            posicaoBMF.AtualizaPosicaoVendaComprado(posicaoBMF.IdPosicao.Value,
                                                                    quantidadeOperacao, puOperacao, puLiquidoOperacao);
                        }
                        else 
                        {
                            operacaoBMF.ProcessaResultadoNormal(operacaoBMF, posicaoBMF);
                            // Baixa posicao - Atualizar apenas a quantidade                            
                            posicaoBMF.AtualizaQuantidade(posicaoBMF.IdPosicao.Value, quantidadeOperacaoNegativa);
                        }
                    }
                    
                }
                // Não tem posição para o Ativo - Inclusão da nova Posição
                else {
                    // Quantidade da operação é positiva, mas quantidade da  posição fica negativa
                    operacaoBMF.Quantidade = -1 * operacaoBMF.Quantidade;
                    posicaoBMF.InserePosicaoBMF(operacaoBMF);
                }
            }
        }


        /// <summary>
        /// Processa ajustes.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>        
        public void AtualizaAjuste(int idCliente, DateTime data)
        {
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            PosicaoBMF posicaoBMF = new PosicaoBMF();
            //
            operacaoBMFCollection.BuscaOperacaoBMFCompraVendaDayTrade(idCliente, data);
            for (int i = 0; i < operacaoBMFCollection.Count; i++)
            {
                OperacaoBMF operacaoBMF = operacaoBMFCollection[i];               
                string cdAtivo = operacaoBMF.CdAtivoBMF;
                string serie = operacaoBMF.Serie;
                int idAgente = operacaoBMF.IdAgenteLiquidacao.Value;
                decimal ajuste = operacaoBMF.Ajuste.Value;
                int? idConta = ParametrosConfiguracaoSistema.Outras.MultiConta ? operacaoBMF.IdConta : null;

                // Tem Posição para o ativo - Atualiza a posição
                if (posicaoBMF.BuscaPosicaoBMF(idCliente, idAgente, cdAtivo, serie, idConta))
                {
                    PosicaoBMF posicaoBMFAjuste = new PosicaoBMF();
                    posicaoBMFAjuste.LoadByPrimaryKey(posicaoBMF.IdPosicao.Value);
                    posicaoBMFAjuste.AjusteDiario += ajuste;
                    posicaoBMFAjuste.AjusteAcumulado += ajuste;
                    posicaoBMFAjuste.Save();
                }
            }
        }

        /// <summary>
        /// Apura o resultado de operações normais, confrontando o custo operado com o custo de estoque em posição.
        /// </summary>
        /// <param name="operacaoBMF">objeto com informações de OperacaoBMF</param>
        /// <param name="posicaoBMF">objeto com informações de PosicaoBMF</param>
        public void ProcessaResultadoNormal(OperacaoBMF operacaoBMF, PosicaoBMF posicaoBMF) 
        {
            #region ArgumentosNulos - throw ArgumentNullException
            if (String.IsNullOrEmpty(operacaoBMF.CdAtivoBMF) ||
                 !operacaoBMF.IdOperacao.HasValue ||
                 !operacaoBMF.IdCliente.HasValue ||
                 !operacaoBMF.Quantidade.HasValue ||
                 !operacaoBMF.PULiquido.HasValue ||
                 String.IsNullOrEmpty(operacaoBMF.TipoOperacao) ||
                 !posicaoBMF.PUCustoLiquido.HasValue ||
                 !posicaoBMF.Quantidade.HasValue) {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("operacaoBMF.CdAtivoBMF ou ")
                .Append("operacaoBMF.IdOperacao ou ")
                .Append("operacaoBMF.IdCliente ou ")
                .Append("operacaoBMF.Quantidade ou ")
                .Append("operacaoBMF.PULiquido ou ")
                .Append("operacaoBMF.TipoOperacao ou ")
                .Append("posicaoBMF.PUCustoLiquido ou ")
                .Append("posicaoBMF.Quantidade");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            decimal quantidadeBase;
            decimal resultado = 0;

            AtivoBMF ativoBMF = new AtivoBMF();            
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(ativoBMF.Query.Peso);
            ativoBMF.LoadByPrimaryKey(campos, operacaoBMF.CdAtivoBMF, operacaoBMF.Serie);
            decimal peso = ativoBMF.Peso.Value;

            if (Math.Abs(posicaoBMF.Quantidade.Value) < operacaoBMF.Quantidade) {
                quantidadeBase = Math.Abs(posicaoBMF.Quantidade.Value);
            }
            else {
                quantidadeBase = operacaoBMF.Quantidade.Value;
            }

            if (operacaoBMF.TipoMercado != TipoMercadoBMF.Futuro)
            {
                // Verifica se está vendido
                switch (operacaoBMF.TipoOperacao)
                {
                    case TipoOperacaoBMF.Compra:
                        resultado = quantidadeBase *
                                (posicaoBMF.PUCustoLiquido.Value - operacaoBMF.PULiquido.Value) * peso;
                        break;
                    case TipoOperacaoBMF.Venda:
                        resultado = quantidadeBase *
                                (operacaoBMF.PULiquido.Value - posicaoBMF.PUCustoLiquido.Value) * peso;
                        break;
                }
            }
            else
            {
                // Verifica se está vendido
                switch (operacaoBMF.TipoOperacao)
                {
                    case TipoOperacaoBMF.Compra:
                        resultado = quantidadeBase *
                                (posicaoBMF.PUCusto.Value - operacaoBMF.Pu.Value) * peso;
                        break;
                    case TipoOperacaoBMF.Venda:
                        resultado = quantidadeBase *
                                (operacaoBMF.Pu.Value - posicaoBMF.PUCusto.Value) * peso;
                        break;
                }
            }

            OperacaoBMF operacaoBMFAux = new OperacaoBMF();
            // Atualiza resultado realizado da Operação                                         
            operacaoBMFAux.IdOperacao = operacaoBMF.IdOperacao;
            operacaoBMFAux.AcceptChanges();
            operacaoBMFAux.ResultadoRealizado = resultado;
            operacaoBMFAux.Save();
        }

        /// <summary>
        /// Lança as liquidações de Compra/Venda do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoVisualizacao">Enum ContaCorrente.VizualizacaoOperacaoBMF. Indica se visualiza analítico ou consolidado.</param>        
        public void LancaCCOperacao(int idCliente, DateTime data, VizualizacaoOperacaoBMF tipoVisualizacao) 
        {        
            if (tipoVisualizacao == VizualizacaoOperacaoBMF.Analitico) {
                this.LancaCCOperacaoAnalitico(idCliente, data);
                this.LancaCCOperacaoDespesas(idCliente, data);
            }
            else if (tipoVisualizacao == VizualizacaoOperacaoBMF.Consolidado) {
                this.LancaCCOperacaoConsolidado(idCliente, data);
            }
        }

        /// <summary>
        /// Lança as liquidações de Compra/Venda do dia, de forma analítica.
        /// Este método se aplica APENAS às operações de opções s/ futuro/disponível e de disponível.
        /// Futuros e termos não são lançados em Liquidacao por este método e sim pelo CalculoAjusteOperacao,
        /// caso esteja marcado como VisualizacaoAnalitico!
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void LancaCCOperacaoAnalitico(int idCliente, DateTime data) 
        {
            // Este método se aplica APENAS às operações de opções s/ futuro/disponível e de disponível.
            // Futuros e termos não são lançados em Liquidacao por este método e sim pelo CalculoAjusteOperacao,
            // caso esteja marcado como VisualizacaoAnalitico!

            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            //
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.BuscaOperacaoBMFLancaOperacaoAnalitico(idCliente, data);

            for (int i = 0; i < operacaoBMFCollection.Count; i++) {
                OperacaoBMF operacaoBMF = operacaoBMFCollection[i];

                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                string serie = operacaoBMF.Serie;
                decimal valor = operacaoBMF.Valor.Value;
                
                int idAgenteLiquidacao = operacaoBMF.IdAgenteLiquidacao.Value;
                int idMoeda = operacaoBMF.IdMoeda.Value;

                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoeda);
                //

                Cliente clienteLocal = new Cliente();
                clienteLocal.LoadByPrimaryKey(idCliente);
                
                DateTime dataLiquidacao = new DateTime();
                if (clienteLocal.IdLocal != LocalFeriadoFixo.NovaYork && idMoeda == (int)ListaMoedaFixo.Real && !AtivoBMF.IsAgricolaCambial(cdAtivoBMF))
                {
                    dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                }
                else //Hoje tratando apenas local = NY (dolar)
                {
                    dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros);

                    Cliente cliente = new Cliente();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(cliente.Query.IdMoeda);
                    cliente.LoadByPrimaryKey(campos, idCliente);

                    if (cliente.IdMoeda.Value == (byte)ListaMoedaFixo.Real && !Calendario.IsDiaUtil(dataLiquidacao))
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(dataLiquidacao, 1);
                    }
                }

                // para calculo da origem Liquidacao                
                int tipoMercado = (int)operacaoBMF.TipoMercado;
                string tipoOperacao = operacaoBMF.TipoOperacao;
                int origemOperacao = operacaoBMF.Origem.Value;

                string descricao = "";
                if (operacaoBMF.IsTipoCompra() || operacaoBMF.IsTipoCompraDaytrade()) {
                    descricao = "Compra " + cdAtivoBMF + serie;
                    valor = valor * -1;
                }
                else if (operacaoBMF.IsTipoVenda() || operacaoBMF.IsTipoVendaDaytrade()) {
                    descricao = "Venda " + cdAtivoBMF + serie;
                }

                byte situacaoLiquidacao = 0;
                /*if (idMoeda == (int)ListaMoedaFixo.Real)
                {
                    situacaoLiquidacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                }
                else
                {
                    situacaoLiquidacao = (byte)SituacaoLancamentoLiquidacao.Compensacao;
                }*/
                situacaoLiquidacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                
                #region Nova Liquidacao
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = descricao;
                liquidacao.Valor = valor;
                liquidacao.Situacao = situacaoLiquidacao;
                liquidacao.Origem = operacaoBMF.GetOrigemLancamentoLiquidacao(tipoMercado, tipoOperacao, origemOperacao);
                liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdAgente = idAgenteLiquidacao;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoBMF.IdConta.HasValue && operacaoBMF.IdConta.Value > 0 ? operacaoBMF.IdConta.Value : idContaDefault;
                #endregion

                liquidacaoCollection.AttachEntity(liquidacao);
            }

            liquidacaoCollection.Save();
        }

        /// <summary>
        /// Lança as liquidações de despesas (emolumento, registro, custoWTR e corretagem) do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void LancaCCOperacaoDespesas(int idCliente, DateTime data)
        {
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            //
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.BuscaOperacaoBMFLancaOperacaoDespesas(idCliente, data);

            for (int i = 0; i < operacaoBMFCollection.Count; i++)
            {
                OperacaoBMF operacaoBMF = operacaoBMFCollection[i];
                // taxas
                decimal taxaClearing = operacaoBMF.TaxaClearingVlFixo.Value;
                decimal corretagem = operacaoBMF.Corretagem.Value;
                decimal emolumento = operacaoBMF.Emolumento.Value;
                decimal registro = operacaoBMF.Registro.Value;
                decimal custoWtr = operacaoBMF.CustoWtr.Value;
                decimal outrosCustos = operacaoBMF.OutrosCustos.Value;
                //                                             
                int idAgenteLiquidacao = operacaoBMF.IdAgenteLiquidacao.Value;
                int idMoeda = operacaoBMF.IdMoeda.Value;
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;

                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoeda);
                //

                Cliente clienteLocal = new Cliente();
                clienteLocal.LoadByPrimaryKey(idCliente);
                
                DateTime dataLiquidacao = new DateTime();
                if (clienteLocal.IdLocal != LocalFeriadoFixo.NovaYork && idMoeda == (int)ListaMoedaFixo.Real && !AtivoBMF.IsAgricolaCambial(cdAtivoBMF))
                {
                    dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                }
                else //Hoje tratando apenas local = NY (dolar)
                {
                    dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros);

                    Cliente cliente = new Cliente();
                    List<esQueryItem> camposCliente = new List<esQueryItem>();
                    camposCliente.Add(cliente.Query.IdMoeda);
                    cliente.LoadByPrimaryKey(camposCliente, idCliente);

                    if (cliente.IdMoeda.Value == (byte)ListaMoedaFixo.Real && !Calendario.IsDiaUtil(dataLiquidacao))
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(dataLiquidacao, 1);
                    }
                }

                AgenteMercado agenteMercado = new AgenteMercado();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(agenteMercado.Query.Nome);
                agenteMercado.LoadByPrimaryKey(idAgenteLiquidacao);
                string nomeAgente = agenteMercado.Nome;
                //
                #region Liquidacao -> Corretagem
                Liquidacao liquidacao = new Liquidacao();
                if (corretagem > 0)
                {
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = "Corretagem BMF " + cdAtivoBMF + " (" + nomeAgente + ")";
                    liquidacao.Valor = corretagem * -1;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.BMF.Corretagem;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgenteLiquidacao;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    //                
                    liquidacaoCollection.AttachEntity(liquidacao);
                }
                #endregion

                #region Liquidacao -> Taxa clearing (repasse)
                liquidacao = new Liquidacao();
                if (taxaClearing > 0)
                {
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = "Taxa clearing BMF " + cdAtivoBMF + " (" + nomeAgente + ")";
                    liquidacao.Valor = taxaClearing * -1;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.BMF.TaxaClearing;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgenteLiquidacao;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    //                
                    liquidacaoCollection.AttachEntity(liquidacao);
                }
                #endregion


                #region Liquidacao -> Emolumento
                liquidacao = new Liquidacao();

                if (emolumento > 0)
                {
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = "Taxas BMF - Emolumento " + cdAtivoBMF + " (" + nomeAgente + ")";
                    liquidacao.Valor = emolumento * -1;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.BMF.Emolumento;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgenteLiquidacao;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    //
                    liquidacaoCollection.AttachEntity(liquidacao);
                }
                #endregion

                #region Liquidacao -> Registro
                liquidacao = new Liquidacao();
                if (registro > 0)
                {
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = "Taxas BMF - Registro " + cdAtivoBMF + " (" + nomeAgente + ")"; ;
                    liquidacao.Valor = registro * -1;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.BMF.Registro;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgenteLiquidacao;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    //
                    liquidacaoCollection.AttachEntity(liquidacao);
                }
                #endregion

                #region Liquidacao -> Outros Custos
                liquidacao = new Liquidacao();
                if (outrosCustos > 0)
                {
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = "Taxas BMF - Outros Custos " + cdAtivoBMF + " (" + nomeAgente + ")"; ;
                    liquidacao.Valor = outrosCustos * -1;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.BMF.OutrosCustos;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgenteLiquidacao;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    //
                    liquidacaoCollection.AttachEntity(liquidacao);
                }
                #endregion

            }

            liquidacaoCollection.Save();
        }
        
        /// <summary>
        /// Lança as liquidações de Compra/Venda do dia, ajustes e despesas de forma consolidada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void LancaCCOperacaoConsolidado(int idCliente, DateTime data) 
        {
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            #region Futuros
            operacaoBMFCollection.BuscaOperacaoBMFLancaOperacaoConsolidadoFuturo(idCliente, data);            
            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                int idAgenteLiquidacao = operacaoBMF.IdAgenteLiquidacao.Value;
                int idMoeda = operacaoBMF.IdMoeda.Value;
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;

                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoeda);
                //

                Cliente clienteLocal = new Cliente();
                clienteLocal.LoadByPrimaryKey(idCliente);
                
                DateTime dataLiquidacao = new DateTime();
                if (clienteLocal.IdLocal != LocalFeriadoFixo.NovaYork && idMoeda == (int)ListaMoedaFixo.Real && !AtivoBMF.IsAgricolaCambial(cdAtivoBMF))
                {
                    dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                }
                else //Hoje tratando apenas local = NY (dolar)
                {
                    dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros);

                    Cliente cliente = new Cliente();
                    List<esQueryItem> camposCliente = new List<esQueryItem>();
                    camposCliente.Add(cliente.Query.IdMoeda);
                    cliente.LoadByPrimaryKey(camposCliente, idCliente);

                    if (cliente.IdMoeda.Value == (byte)ListaMoedaFixo.Real && !Calendario.IsDiaUtil(dataLiquidacao))
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(dataLiquidacao, 1);
                    }
                }

                AgenteMercado agenteMercado = new AgenteMercado();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(agenteMercado.Query.Nome);
                agenteMercado.LoadByPrimaryKey(idAgenteLiquidacao);
                string nomeAgente = agenteMercado.Nome;
                
                decimal valorLiquidar = operacaoBMF.Ajuste.Value - operacaoBMF.Corretagem.Value
                                - operacaoBMF.Emolumento.Value - operacaoBMF.Registro.Value - operacaoBMF.OutrosCustos.Value - operacaoBMF.TaxaClearingVlFixo.Value;
                    
                string descricao = "Valor Líquido Futuros (" + nomeAgente + ")";

                byte situacaoLiquidacao = (byte)SituacaoLancamentoLiquidacao.Normal;


                #region Nova Liquidacao
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = descricao;
                liquidacao.Valor = valorLiquidar;
                liquidacao.Situacao = situacaoLiquidacao;
                liquidacao.Origem = (int)OrigemLancamentoLiquidacao.BMF.Outros;
                liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdAgente = idAgenteLiquidacao;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoBMF.IdConta.HasValue && operacaoBMF.IdConta.Value > 0 ? operacaoBMF.IdConta.Value : idContaDefault;
                #endregion

                liquidacaoCollection.AttachEntity(liquidacao);
            }
            #endregion

            operacaoBMFCollection = new OperacaoBMFCollection();
            #region Opções (Liquido Venda)
            operacaoBMFCollection.BuscaOperacaoBMFLancaOperacaoConsolidadoVenda(idCliente, data);
            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                int idAgenteLiquidacao = operacaoBMF.IdAgenteLiquidacao.Value;
                int idMoeda = operacaoBMF.IdMoeda.Value;
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;

                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoeda);
                //

                DateTime dataLiquidacao = new DateTime();
                if (idMoeda == (int)ListaMoedaFixo.Real && !AtivoBMF.IsAgricolaCambial(cdAtivoBMF))
                {
                    dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                }
                else //Hoje tratando apenas local = NY (dolar)
                {
                    dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros);

                    Cliente cliente = new Cliente();
                    List<esQueryItem> camposCliente = new List<esQueryItem>();
                    camposCliente.Add(cliente.Query.IdMoeda);
                    cliente.LoadByPrimaryKey(camposCliente, idCliente);

                    if (cliente.IdMoeda.Value == (byte)ListaMoedaFixo.Real && !Calendario.IsDiaUtil(dataLiquidacao))
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(dataLiquidacao, 1);
                    }
                }
                                
                AgenteMercado agenteMercado = new AgenteMercado();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(agenteMercado.Query.Nome);
                agenteMercado.LoadByPrimaryKey(idAgenteLiquidacao);
                string nomeAgente = agenteMercado.Nome;

                decimal valorLiquidar = operacaoBMF.ValorLiquido.Value;

                string descricao = "Venda Líquida Opções BMF (" + nomeAgente + ")";

                #region Nova Liquidacao
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = descricao;
                liquidacao.Valor = valorLiquidar;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = (int)OrigemLancamentoLiquidacao.BMF.Outros;
                liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdAgente = idAgenteLiquidacao;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoBMF.IdConta.HasValue && operacaoBMF.IdConta.Value > 0 ? operacaoBMF.IdConta.Value : idContaDefault;
                #endregion

                liquidacaoCollection.AttachEntity(liquidacao);
            }
            #endregion

            operacaoBMFCollection = new OperacaoBMFCollection();
            #region Opções (Liquido Compra)
            operacaoBMFCollection.BuscaOperacaoBMFLancaOperacaoConsolidadoCompra(idCliente, data);
            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                int idAgenteLiquidacao = operacaoBMF.IdAgenteLiquidacao.Value;
                int idMoeda = operacaoBMF.IdMoeda.Value;
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;

                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoeda);
                //

                DateTime dataLiquidacao = new DateTime();
                if (idMoeda == (int)ListaMoedaFixo.Real && !AtivoBMF.IsAgricolaCambial(cdAtivoBMF))
                {
                    dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                }
                else //Hoje tratando apenas local = NY (dolar)
                {
                    dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros);

                    Cliente cliente = new Cliente();
                    List<esQueryItem> camposCliente = new List<esQueryItem>();
                    camposCliente.Add(cliente.Query.IdMoeda);
                    cliente.LoadByPrimaryKey(camposCliente, idCliente);

                    if (cliente.IdMoeda.Value == (byte)ListaMoedaFixo.Real && !Calendario.IsDiaUtil(dataLiquidacao))
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(dataLiquidacao, 1);
                    }
                }

                AgenteMercado agenteMercado = new AgenteMercado();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(agenteMercado.Query.Nome);
                agenteMercado.LoadByPrimaryKey(idAgenteLiquidacao);
                string nomeAgente = agenteMercado.Nome;

                decimal valorLiquidar = operacaoBMF.ValorLiquido.Value * -1;

                string descricao = "Compra Líquida Opções BMF (" + nomeAgente + ")";

                #region Nova Liquidacao
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = descricao;
                liquidacao.Valor = valorLiquidar;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = (int)OrigemLancamentoLiquidacao.BMF.Outros;
                liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdAgente = idAgenteLiquidacao;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoBMF.IdConta.HasValue && operacaoBMF.IdConta.Value > 0 ? operacaoBMF.IdConta.Value : idContaDefault;
                #endregion

                liquidacaoCollection.AttachEntity(liquidacao);
            }
            #endregion

            liquidacaoCollection.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoMercado"></param>
        /// <param name="tipoOperacao"></param>
        /// <param name="origemOperacao"></param>
        /// <returns>int representando a Origem do Lancamento da Liquidacao
        /// valores possiveis de retorno:
        /// OrigemLancamentoLiquidacao.BMF.CompraVista,
        /// OrigemLancamentoLiquidacao.BMF.VendaVista,
        /// OrigemLancamentoLiquidacao.BMF.CompraOpcoes,
        /// OrigemLancamentoLiquidacao.BMF.VendaOpcoes,
        /// OrigemLancamentoLiquidacao.BMF.ExercicioCompra,
        /// OrigemLancamentoLiquidacao.BMF.ExercicioVenda,
        /// </returns>
        /// throws ArgumentExcepition se 
        /// tipoMercado diferente dos valores do enum TipoMercadoBMF
        /// tipoOperacao diferente dos valores do enum TipoOperacaoBMF
        /// origemOperacao diferente dos valores do enum OrigemOperacaoBMF
        public int GetOrigemLancamentoLiquidacao(int tipoMercado, string tipoOperacao, int origemOperacao) {
            
            #region ArgumentosNulos - throw ArgumentException
            List<int> valoresPossiveisTipoMercado = TipoMercadoBMF.Values();
            if (!valoresPossiveisTipoMercado.Contains(tipoMercado)) {
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("tipoMercado incorreto. Os valores possiveis estão no enum TipoMercadoBMF");
                throw new ArgumentException(mensagem.ToString());
            }
            List<string> valoresPossiveisTipoOperacao = TipoOperacaoBMF.Values();
            if (!valoresPossiveisTipoOperacao.Contains(tipoOperacao)) {
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("tipoOperacao incorreto. Os valores possiveis estão no enum TipoOperacaoBMF");
                throw new ArgumentException(mensagem.ToString());
            }
            List<int> valoresPossiveisOrigem = OrigemOperacaoBMF.Values();
            if (!valoresPossiveisOrigem.Contains(origemOperacao)) {
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("origemOperacao incorreto. Os valores possiveis estão no enum OrigemOperacaoBMF");
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            int origemLiquidacao = (int)OrigemLancamentoLiquidacao.BMF.None;

            #region Define Origem da Liquidacao
            if (this.IsTipoCompra() || this.IsTipoCompraDaytrade() &&
                this.IsOrigemPrimaria() &&
                this.IsTipoMercadoVista()) {

                origemLiquidacao = (int)OrigemLancamentoLiquidacao.BMF.CompraVista;
            }
            else if (this.IsTipoVenda() || this.IsTipoVendaDaytrade() &&
                     this.IsOrigemPrimaria() &&
                     this.IsTipoMercadoVista()) {

                origemLiquidacao = (int)OrigemLancamentoLiquidacao.BMF.VendaVista;
            }
            else if (this.IsTipoCompra() || this.IsTipoCompraDaytrade() &&
                     this.IsOrigemPrimaria() &&
                     this.IsTipoMercadoOpcaoDisponivel() || this.IsTipoMercadoOpcaoFuturo()) {

                origemLiquidacao = (int)OrigemLancamentoLiquidacao.BMF.CompraOpcoes;
            }
            else if (this.IsTipoVenda() || this.IsTipoVendaDaytrade() &&
                     this.IsOrigemPrimaria() &&
                     this.IsTipoMercadoOpcaoDisponivel() || this.IsTipoMercadoOpcaoFuturo()) {

                origemLiquidacao = (int)OrigemLancamentoLiquidacao.BMF.VendaOpcoes;
            }
            else if (this.IsTipoCompra() &&
                     this.IsOrigemExercicioOpcao() &&
                     !this.IsTipoMercadoOpcaoDisponivel() && !this.IsTipoMercadoOpcaoFuturo()) {

                origemLiquidacao = (int)OrigemLancamentoLiquidacao.BMF.ExercicioCompra;
            }
            else if (this.IsTipoVenda() &&
                     this.IsOrigemExercicioOpcao() &&
                     !this.IsTipoMercadoOpcaoDisponivel() && !this.IsTipoMercadoOpcaoFuturo()) {

                origemLiquidacao = (int)OrigemLancamentoLiquidacao.BMF.ExercicioVenda;
            }
            #endregion

            return origemLiquidacao;
        }

        /// <summary>
        /// Calcula valores líquidos, descontando as despesas. Atualiza o valor líquido e o PU líquido.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaValoresLiquidos(int idCliente, DateTime data) 
        {
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.BuscaOperacaoBMFValores(idCliente, data);
            
            for (int i = 0; i < operacaoBMFCollection.Count; i++) 
            {
                OperacaoBMF operacaoBMF = (OperacaoBMF)operacaoBMFCollection[i];
                decimal valor = operacaoBMF.Valor.Value;
                decimal quantidade = operacaoBMF.Quantidade.Value;
                decimal pu = operacaoBMF.Pu.Value;
                decimal peso = operacaoBMF.UpToAtivoBMFByCdAtivoBMF.Peso.Value; //HIERARQUICO
                decimal puLiquido = operacaoBMF.PULiquido.Value;
                // taxas
                decimal corretagem = operacaoBMF.Corretagem.Value;
                decimal emolumento = operacaoBMF.Emolumento.Value;
                decimal registro = operacaoBMF.Registro.Value;
                decimal custoWtr = operacaoBMF.CustoWtr.Value;
                decimal taxaClearingVlFixo = operacaoBMF.TaxaClearingVlFixo.Value;
                decimal outrosCustos = operacaoBMF.OutrosCustos.Value;
                decimal taxas = corretagem + emolumento + registro + custoWtr + outrosCustos + taxaClearingVlFixo;
                decimal valorLiquido = 0;

                switch (operacaoBMF.TipoOperacao) {
                    case TipoOperacaoBMF.Compra:
                    case TipoOperacaoBMF.CompraDaytrade:
                        puLiquido = (valor + taxas) / (quantidade * peso);
                        valorLiquido = valor + taxas;
                        break;
                    case TipoOperacaoBMF.Venda:
                    case TipoOperacaoBMF.VendaDaytrade:
                        puLiquido = (valor - taxas) / (quantidade * peso);
                        valorLiquido = valor - taxas;
                        break;
                }

                operacaoBMF.ValorLiquido = valorLiquido;
                operacaoBMF.PULiquido = puLiquido;
            }

            // Salva a collection de operações
            operacaoBMFCollection.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Compra
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoCompra() {
            return this.TipoOperacao == TipoOperacaoBMF.Compra;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Venda
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoVenda() {
            return this.TipoOperacao == TipoOperacaoBMF.Venda;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Compra Daytrade
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoCompraDaytrade() {
            return this.TipoOperacao == TipoOperacaoBMF.CompraDaytrade;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Venda Daytrade
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoVendaDaytrade() {
            return this.TipoOperacao == TipoOperacaoBMF.VendaDaytrade;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoMercado da Operacao é Vista
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoMercadoVista() {
            return this.TipoMercado == TipoMercadoBMF.Disponivel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é Primaria
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemPrimaria() {
            return this.Origem == OrigemOperacaoBMF.Primaria;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoMercado da Operacao é OpcaoDisponivel
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoMercadoOpcaoDisponivel() {
            return this.TipoMercado == TipoMercadoBMF.OpcaoDisponivel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoMercado da Operacao é OpcaoFuturo
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoMercadoOpcaoFuturo() {
            return this.TipoMercado == TipoMercadoBMF.OpcaoFuturo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é ExercicioOpcaoDisponivel ou ExercicioOpcaoFuturo
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemExercicioOpcao() {
            return (this.Origem == OrigemOperacaoBMF.ExercicioOpcaoCompra ||
                    this.Origem == OrigemOperacaoBMF.ExercicioOpcaoVenda);
        }

        /// <summary>
        /// Calcula corretagem das operações do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaCorretagemOperacao(int idCliente, DateTime data) 
        {
            ClienteBMF clienteBMF = new ClienteBMF();
            int? custodianteBmfDefault = null;
            if (clienteBMF.LoadByPrimaryKey(idCliente) && clienteBMF.IdCustodianteBmf.HasValue)
            {
                custodianteBmfDefault = clienteBMF.IdCustodianteBmf;
            }

            DateTime dataInicioMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                        
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.BuscaOperacaoBMFCalculaCorretagem(idCliente, data);
            for (int i = 0; i < operacaoBMFCollection.Count; i++) {

                #region Valores OperacaoBMF
                OperacaoBMF operacaoBMF = operacaoBMFCollection[i];
                int idAgenteCorretora = operacaoBMF.IdAgenteCorretora.Value;
                int idAgenteLiquidacao = operacaoBMF.IdAgenteLiquidacao.Value;
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                string serie = operacaoBMF.Serie;
                int tipoMercado = operacaoBMF.TipoMercado.Value;
                int quantidade = operacaoBMF.Quantidade.Value;
                string tipoOperacao = operacaoBMF.TipoOperacao;
                decimal valor = operacaoBMF.Valor.Value;
                decimal percentualDesconto = 0;
                int idMoeda = operacaoBMF.IdMoeda.Value;
                int? idConta = ParametrosConfiguracaoSistema.Outras.MultiConta ? operacaoBMF.IdConta : null;

                if (operacaoBMF.PercentualDesconto.HasValue)
                {
                    percentualDesconto = operacaoBMF.PercentualDesconto.Value;
                }
                #endregion

                #region Verifica o número de vencimentos do ativo/serie
                AtivoBMF ativoBMF = new AtivoBMF();
                int numeroVencimento = 0;
                if (idMoeda == (byte)ListaMoedaFixo.Real)
                {
                    numeroVencimento = ativoBMF.RetornaNumeroVencimentos(cdAtivoBMF, serie, tipoMercado, data);
                }
                #endregion

                #region Busca o saldo corrente de nr de contratos operados no mês
                OperacaoBMF operacaoBMFOperado = new OperacaoBMF();
                int saldoAcumulado = 0;
                if (idMoeda == (byte)ListaMoedaFixo.Real)
                {
                    saldoAcumulado = operacaoBMFOperado.RetornaTotalOperado(idCliente, idAgenteCorretora, cdAtivoBMF,
                                                                        dataInicioMes, data, idConta);
                }
                #endregion

                #region Busca Perfil de Corretagem do idCliente/idAgente/cdAtivoBMF/tipoMercado
                int idPerfilCorretagem = 0;
                DateTime dataReferenciaPerfil = new DateTime();
                int tipoCorretagem;
                int tipoCorretagemDT;
                bool achouPerfil = false;

                string cdAtivoBMFPesquisa = cdAtivoBMF; //Específico apenas para buscar perfil de corretagem de ativos offshore
                if (idMoeda != (byte)ListaMoedaFixo.Real)
                {
                    cdAtivoBMFPesquisa = cdAtivoBMF.Substring(0, 2);
                }

                PerfilCorretagemBMF perfilCorretagemBMF = new PerfilCorretagemBMF();

                //Busca perfil por Cliente/Agente/Grupo 'Geral'
                if (perfilCorretagemBMF.BuscaPerfilCorretagemGeral(data, idCliente, idAgenteCorretora))
                {
                    idPerfilCorretagem = perfilCorretagemBMF.IdPerfilCorretagem.Value;
                    dataReferenciaPerfil = perfilCorretagemBMF.DataReferencia.Value;
                    tipoCorretagem = perfilCorretagemBMF.TipoCorretagem.Value;
                    tipoCorretagemDT = perfilCorretagemBMF.TipoCorretagemDT.Value;
                    achouPerfil = true;
                }
                //Busca perfil por Cliente/Agente/Ativo/TipoMercado
                else if (perfilCorretagemBMF.BuscaPerfilCorretagem(data, idCliente, idAgenteCorretora,
                                                              cdAtivoBMFPesquisa, tipoMercado))
                {
                    idPerfilCorretagem = perfilCorretagemBMF.IdPerfilCorretagem.Value;
                    dataReferenciaPerfil = perfilCorretagemBMF.DataReferencia.Value;
                    tipoCorretagem = perfilCorretagemBMF.TipoCorretagem.Value;
                    tipoCorretagemDT = perfilCorretagemBMF.TipoCorretagemDT.Value;
                    achouPerfil = true;
                }
                //Busca perfil por Cliente/Agente/Grupo de Ativos
                else if (perfilCorretagemBMF.BuscaPerfilCorretagemGrupo(data, idCliente, idAgenteCorretora,
                                                                  cdAtivoBMFPesquisa, tipoMercado))
                {
                    idPerfilCorretagem = perfilCorretagemBMF.IdPerfilCorretagem.Value;
                    dataReferenciaPerfil = perfilCorretagemBMF.DataReferencia.Value;
                    tipoCorretagem = perfilCorretagemBMF.TipoCorretagem.Value;
                    tipoCorretagemDT = perfilCorretagemBMF.TipoCorretagemDT.Value;
                    achouPerfil = true;
                }
                else //Se não existe perfil, cobra corretagem cheia pela TOB
                {
                    tipoCorretagem = (int)TipoCalculoCorretagem.TOB;
                    tipoCorretagemDT = (int)TipoCalculoCorretagem.TOB;
                }                
                #endregion

                #region Busca Taxas do cliente relativas à Faixa de Corretagem, associada ao perfil acima
                decimal taxaCliente;
                decimal taxaClienteDT;

                if (!achouPerfil) //Se não achou perfil, cobra corretagem cheia pela TOB
                {
                    tipoCorretagem = (int)TipoCalculoCorretagem.TOB;
                    tipoCorretagemDT = (int)TipoCalculoCorretagem.TOB;
                    taxaCliente = 100;
                    taxaClienteDT = 100;                    
                }
                else
                {
                    TabelaCorretagemBMF tabelaCorretagemBMF = new TabelaCorretagemBMF();
                    if (tabelaCorretagemBMF.BuscaTabelaCorretagem(idPerfilCorretagem, dataReferenciaPerfil,
                                                                  numeroVencimento, saldoAcumulado))
                    {
                        taxaCliente = tabelaCorretagemBMF.TaxaCorretagem.Value;
                        taxaClienteDT = tabelaCorretagemBMF.TaxaCorretagemDT.Value;
                    }
                    //Se não achou faixa, cobra corretagem cheia pela TOB
                    else if (tipoCorretagem == (int)TipoCalculoCorretagem.TOB)
                    {
                        taxaCliente = 100;
                        taxaClienteDT = 100;
                    }
                    else
                    {                        
                        AgenteMercado agenteMercado = new AgenteMercado();
                        agenteMercado.LoadByPrimaryKey(idAgenteCorretora);
                        string nomeAgente = agenteMercado.Nome;
                        throw new TabelaCorretagemBMFNaoCadastradoException("Valor fixo de corretagem não cadastrado para o cliente " + idCliente + ", agente " + nomeAgente + ", ativo " + cdAtivoBMF);
                    }
                }
                #endregion

                #region Cálculo apenas para quando for % da TOB
                decimal baseCalculoTOB = 0;
                decimal ptax = 1;
                decimal peso = 0;
                decimal tob = 0;
                decimal tobDayTrade = 0;
                decimal tobMinima = 0;
                decimal tobDayTradeMinima = 0;
                decimal tobExercicio = 0;
                decimal tobExercicioCasado = 0;
                decimal corretagemUnitaria = 0;
                decimal corretagemUnitariaDT = 0;
                if (tipoCorretagem == (int)TipoCalculoCorretagem.TOB)
                {
                    //Busca valor base da TOB
                    baseCalculoTOB = this.RetornaBaseCalculoTOB(data, cdAtivoBMF, serie, valor, tipoMercado);

                    #region Busca PTAX
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    if (AtivoBMF.IsAgricolaCambial(cdAtivoBMF))
                    {
                        DateTime ultimoDiaMesAnterior = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                        ultimoDiaMesAnterior = Calendario.SubtraiDiaUtil(ultimoDiaMesAnterior, 1,
                                                                         LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        ptax = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.PTAX_800VENDA, ultimoDiaMesAnterior);
                    }
                    else if (AtivoBMF.IsAtivoCambial(cdAtivoBMF))
                    {
                        DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        ptax = cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.PTAX_800VENDA, dataAnterior);
                    }
                    #endregion

                    #region Busca peso do ativo
                    ativoBMF = new AtivoBMF();
                    ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);
                    peso = ativoBMF.Peso.Value;
                    #endregion

                    //Corretagem Base a ser aplicada para operações normais e DT
                    decimal corretagemBase = Utilitario.Truncate(peso * ptax * baseCalculoTOB, 2);

                    #region Busca percentuais da TOB
                    if (tipoCorretagem == (int)TipoCalculoCorretagem.TOB ||
                        tipoCorretagemDT == (int)TipoCalculoCorretagem.TOB)
                    {
                        TabelaTOB tabelaTOB = new TabelaTOB();
                        tabelaTOB.BuscaTabelaTOB(data, cdAtivoBMF, tipoMercado);

                        tob = tabelaTOB.Tob.Value;
                        tobDayTrade = tabelaTOB.TOBDayTrade.Value;
                        tobMinima = tabelaTOB.TOBMinima.Value;
                        tobDayTradeMinima = tabelaTOB.TOBDayTradeMinima.Value;
                        tobExercicio = tabelaTOB.TOBExercicio.Value;
                        tobExercicioCasado = tabelaTOB.TOBExercicioCasado.Value;
                    }
                    #endregion

                    //Cálculo das corretagens unitárias (operações normais e DT)
                    corretagemUnitaria = Utilitario.Truncate((corretagemBase * tob) / 100, 2);
                    corretagemUnitariaDT = Utilitario.Truncate((corretagemBase * tobDayTrade) / 100, 2);

                    #region Agricolas cambiais têm tratamento especial na truncagem
                    if (AtivoBMF.IsAgricolaCambial(cdAtivoBMF))
                    {
                        corretagemUnitaria = Utilitario.Truncate(peso * baseCalculoTOB * tob / 100, 2);
                        corretagemUnitaria = Utilitario.Truncate(corretagemUnitaria * ptax, 2);

                        corretagemUnitariaDT = Utilitario.Truncate(peso * baseCalculoTOB * tobDayTrade / 100, 2);
                        corretagemUnitariaDT = Utilitario.Truncate(corretagemUnitariaDT * ptax, 2);
                    }
                    #endregion

                    #region Corretagens mínimas (operações normais e DT), se aplica apenas a % da TOB
                    if (tipoCorretagem == (int)TipoCalculoCorretagem.TOB)
                    {
                        if (tipoOperacao == TipoOperacaoBMF.Compra || tipoOperacao == TipoOperacaoBMF.Venda)
                        {
                            corretagemUnitaria = corretagemUnitaria > tobMinima ? corretagemUnitaria : tobMinima;
                        }
                        else
                        {
                            corretagemUnitaria = corretagemUnitariaDT > tobDayTradeMinima ? corretagemUnitariaDT : tobDayTradeMinima;
                        }
                    }
                    #endregion
                }
                #endregion

                #region Cálculo da corretagem final (pela TOB ou Valor Fixo)
                decimal corretagemFinal;
                if (tipoOperacao == TipoOperacaoBMF.Compra || tipoOperacao == TipoOperacaoBMF.Venda)
                {
                    if (tipoCorretagem == (int)TipoCalculoCorretagem.TOB)
                    {
                        corretagemFinal = Utilitario.Truncate(corretagemUnitaria * taxaCliente / 100, 2);
                        corretagemFinal = corretagemFinal * quantidade;                        
                    }
                    else //Corretagem Fixa (taxaCorretagem é o valor por contrato)
                    {
                        corretagemFinal = Utilitario.Truncate(taxaCliente * quantidade, 2);                        
                    }
                }
                else //Operação DayTrade
                {
                    if (tipoCorretagemDT == (int)TipoCalculoCorretagem.TOB)
                    {
                        corretagemFinal = Utilitario.Truncate(corretagemUnitaria * taxaClienteDT / 100, 2);
                        corretagemFinal = corretagemFinal * quantidade;                        
                    }
                    else //Corretagem Fixa (taxaCorretagem é o valor por contrato)
                    {
                        corretagemFinal = Utilitario.Truncate(taxaClienteDT * quantidade, 2);                
                    }
                }

                //Abate o % de desconto
                corretagemFinal -= Utilitario.Truncate(percentualDesconto / 100M * corretagemFinal, 2);
                //
                #endregion

                operacaoBMF.Corretagem = corretagemFinal;

                #region Calculo Taxa clearing (repasse corretagem)

                decimal valorFixoAgenteCustodia = 0;
                decimal valorFixoCorretora = 0;

                TabelaTaxaClearingBMF tabelaTaxaClearingBMF = new TabelaTaxaClearingBMF();
                if (tabelaTaxaClearingBMF.BuscaTabelaRepassaCorretagem(idCliente, tipoMercado, cdAtivoBMF, numeroVencimento))
                {
                    decimal vlTaxaClearing = custodianteBmfDefault == operacaoBMF.IdAgenteCorretora.Value ? tabelaTaxaClearingBMF.ValorFixoAgenteCustodia.Value : tabelaTaxaClearingBMF.ValorFixoCorretora.Value;
                    operacaoBMF.TaxaClearingVlFixo = vlTaxaClearing * quantidade;      
                }

                #endregion

            }

            operacaoBMFCollection.Save();
        }

        /// <summary>
        /// Retorna o valor da base de cálculo da TOB (Taxa Operacional Básica), para o cálculo da corretagem.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="serie"></param>
        /// <param name="valor"></param>
        /// <param name="tipoMercado"></param>
        /// <returns></returns>
        public decimal RetornaBaseCalculoTOB(DateTime data, string cdAtivoBMF, string serie, decimal valor, int tipoMercado) {

            AtivoBMF ativoBMFOriginal = new AtivoBMF();
            ativoBMFOriginal.LoadByPrimaryKey(cdAtivoBMF, serie);

            //Algumas opções sobre disponivel e futuro, assim como todos os termos e disponiveis 
            //usam o valor da operação como base da TOB
            if ((tipoMercado == TipoMercadoBMF.OpcaoDisponivel && cdAtivoBMF != "IDI") ||
                (tipoMercado == TipoMercadoBMF.OpcaoFuturo && cdAtivoBMF == "DOL") ||
                (tipoMercado == TipoMercadoBMF.Disponivel) ||
                (tipoMercado == TipoMercadoBMF.Termo))
            {
                return valor;
            }
            
            int tipoMercadoUsar = tipoMercado; //A ser efetivamente usado para buscar futuramente a cotação do ativo
            #region Muda o mercado e/ou o ativo
            //Opção sobre Disponível de DI - Mesma base do DI1 futuro
            if (cdAtivoBMF == "IDI") 
            {
                cdAtivoBMF = "DI1";
                tipoMercadoUsar = TipoMercadoBMF.Futuro;

                AtivoBMFCollection ativoBMFAuxColl = new AtivoBMFCollection();
                ativoBMFAuxColl.Query.Where(ativoBMFAuxColl.Query.DataVencimento.GreaterThanOrEqual(ativoBMFOriginal.DataVencimento.Value)
                                        & ativoBMFAuxColl.Query.CdAtivoBMF.Equal(cdAtivoBMF));
                ativoBMFAuxColl.Query.OrderBy(ativoBMFAuxColl.Query.DataVencimento.Ascending);

                if (ativoBMFAuxColl.Query.Load())
                {
                    serie = ativoBMFAuxColl[0].Serie;
                }
            }
            //Opções sobre futuro de DI1 - Mesma base do DI1 futuro
            if (cdAtivoBMF == "D11" || cdAtivoBMF == "D12" || cdAtivoBMF == "D13" || cdAtivoBMF == "D14") 
            {
                cdAtivoBMF = "DI1";
                tipoMercadoUsar = TipoMercadoBMF.Futuro;
            }
            //IND Opção sobre Futuro - Mesma base do IND futuro
            if (cdAtivoBMF == "IND") 
            {
                tipoMercadoUsar = TipoMercadoBMF.Futuro;
            }
            #endregion            

            #region Determina a série/vencimento de acordo com o ativo/mercado
            AtivoBMF ativoBMF = new AtivoBMF();

            string serieUsar = serie; //A ser efetivamente usada para buscar futuramente a cotação do ativo

            //Todas as agricolas - Futuro e Opção sobre Futuro (2o vencimento)
            if (AtivoBMF.IsAgricolaCambial(cdAtivoBMF) || AtivoBMF.IsAgricolaReais(cdAtivoBMF))
            {
                ativoBMF.BuscaSerieAberto(cdAtivoBMF, TipoMercadoBMF.Futuro, data, 2);
                serieUsar = ativoBMF.Serie;
            }
            // DI Futuro, Cupom Cambial, Futuro de IGPM, Futuro fracionário de IGPM, 
            // Cupom de IGPM, Cupom de IPCA (vencimento negociado)
            else if (cdAtivoBMF == "DI1" || cdAtivoBMF == "DDI" || cdAtivoBMF == "IGM" || cdAtivoBMF == "IGF" ||
                     cdAtivoBMF == "DDM" || cdAtivoBMF == "DAP")
            {
                serieUsar = serie;
            }
            //IND - Futuro e Opção sobre Futuro (1o vencimento) 
            else if (cdAtivoBMF == "IND")
            {
                ativoBMF.BuscaSerieAberto(cdAtivoBMF, TipoMercadoBMF.Futuro, data, 1);
                serieUsar = ativoBMF.Serie;
            }
            //Bonds - Globals e T10 (vencimento negociado)
            else if (AtivoBMF.IsAtivoBond(cdAtivoBMF))
            {
                serieUsar = serie;
            }
            //Todos os outros futuros (1o vencimento)
            else if (tipoMercado == (int)TipoMercadoBMF.Futuro)
            {
                ativoBMF.BuscaSerieAberto(cdAtivoBMF, TipoMercadoBMF.Futuro, data, 1);
                serieUsar = ativoBMF.Serie;
            }
            #endregion

            #region Busca a cotação do futuro
            CotacaoBMF cotacaoBMF = new CotacaoBMF();            
            decimal cotacaoBase;
            if (AtivoBMF.IsAtivoJuros(cdAtivoBMF))
            {
                cotacaoBMF.BuscaCotacaoBMF(cdAtivoBMF, serieUsar, data);
                cotacaoBase = cotacaoBMF.PUCorrigido.Value;
            }
            else 
            {
                DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                cotacaoBMF.BuscaCotacaoBMF(cdAtivoBMF, serieUsar, dataAnterior);

                cotacaoBase = cotacaoBMF.PUFechamento.Value;
            }
            #endregion

            #region Calcula a TOB
            if (cdAtivoBMF == "DI1" || cdAtivoBMF == "DDI" || cdAtivoBMF == "DDM" || cdAtivoBMF == "DAP")
            {
                return Utilitario.Truncate(100000 - cotacaoBase, 2);
            }
            else
            {
                return cotacaoBase;
            }
            #endregion

        }

        /// <summary>
        /// Retorna o total de resultado normal.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="tipoMercado">pode ser passado no formato IN(...)</param>
        public decimal RetornaTotalResultadoNormal(int idCliente, DateTime dataInicio, DateTime dataFim, int tipoMercado) 
        {        
            this.QueryReset();
            this.Query
                 .Select(this.Query.ResultadoRealizado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.In(tipoMercado),
                        this.Query.TipoOperacao.NotIn(TipoOperacaoBMF.CompraDaytrade,
                                                      TipoOperacaoBMF.VendaDaytrade)
                        );
            this.Query.Load();

            // Se valores dos campos são nulos retorna zero
            if (this.es.HasData)
            {
                return this.ResultadoRealizado.HasValue ? this.ResultadoRealizado.Value : 0;
            }
            else
            {
                return 0;
            }            
        }

        /// <summary>
        /// Retorna o total de resultado daytrade.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="tipoMercado">pode ser passado no formato IN(...)</param>
        public decimal RetornaTotalResultadoDayTrade(int idCliente, DateTime dataInicio, DateTime dataFim, int tipoMercado) 
        {        
            //Total de Venda DT
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.In(tipoMercado),
                        this.Query.TipoOperacao.In(TipoOperacaoBMF.VendaDaytrade)
                        );
            this.Query.Load();

            decimal totalVendaDT = 0;
            if (this.es.HasData) {
                totalVendaDT = this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
            }

            //Total de Compra DT
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.In(tipoMercado),
                        this.Query.TipoOperacao.In(TipoOperacaoBMF.CompraDaytrade)
                        );
            this.Query.Load();

            decimal totalCompraDT = 0;
            if (this.es.HasData)
            {
                totalCompraDT = this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
            }

            decimal totalResultadoDayTrade = totalVendaDT - totalCompraDT;

            return totalResultadoDayTrade;

        }

        /// <summary>
        /// Retorna o total de resultado normal em opções.        
        /// Filtra para Opcoes sobre Disponivel e Opcoes sobre Futuro.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        public decimal RetornaTotalResultadoNormalOpcoes(int idCliente, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ResultadoRealizado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo),
                        this.Query.TipoOperacao.NotIn(TipoOperacaoBMF.CompraDaytrade,
                                                      TipoOperacaoBMF.VendaDaytrade)
                        );
            this.Query.Load();

            // Se valores dos campos são nulos retorna zero
            if (this.es.HasData) {
                return this.ResultadoRealizado.HasValue ? this.ResultadoRealizado.Value : 0;
            }
            else
            {
                return 0;
            }            
        }

        /// <summary>
        /// Retorna o total de resultado daytrade em opções.
        /// Filtra para Opcoes sobre Disponivel e Opcoes sobre Futuro.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        public decimal RetornaTotalResultadoDayTradeOpcoes(int idCliente, DateTime dataInicio, DateTime dataFim)
        {
            //Total de Venda DT
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo),
                        this.Query.TipoOperacao.In(TipoOperacaoBMF.VendaDaytrade)
                        );
            this.Query.Load();

            decimal totalVendaDT = 0;
            if (this.es.HasData)
            {
                totalVendaDT = this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
            }

            //Total de Compra DT
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo),
                        this.Query.TipoOperacao.In(TipoOperacaoBMF.CompraDaytrade)
                        );
            this.Query.Load();

            decimal totalCompraDT = 0;
            if (this.es.HasData)
            {
                totalCompraDT = this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
            }

            decimal totalResultadoDayTrade = totalVendaDT - totalCompraDT;

            return totalResultadoDayTrade;
        }

        /// <summary>
        /// Retorna o total de ajuste normal.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="cdAtivoBMF"></param>
        public decimal RetornaTotalAjusteNormal(int idCliente, DateTime dataInicio, DateTime dataFim, string cdAtivoBMF)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Ajuste.Sum(), this.query.Corretagem.Sum(),
                         this.Query.Emolumento.Sum(), this.query.Registro.Sum(), this.query.CustoWtr.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.CdAtivoBMF.In(cdAtivoBMF),
                        this.Query.TipoOperacao.NotIn(TipoOperacaoBMF.CompraDaytrade,
                                                      TipoOperacaoBMF.VendaDaytrade)
                        );
            this.Query.Load();

            // Se valores dos campos são nulos retorna zero
            if (this.es.HasData)
            {
                decimal valor = this.Ajuste.HasValue ? this.Ajuste.Value : 0;

                decimal corretagem = this.Corretagem.HasValue ? this.Corretagem.Value : 0;
                valor -= corretagem;

                decimal emolumento = this.Emolumento.HasValue ? this.Emolumento.Value : 0;
                valor -= emolumento;

                decimal registro = this.Registro.HasValue ? this.Registro.Value : 0;
                valor -= registro;

                decimal custoWTR = this.CustoWtr.HasValue ? this.CustoWtr.Value : 0;
                valor -= custoWTR;

                return valor;
            }
            else
            {
                return 0;
            }            
        }

        /// <summary>
        /// Retorna o total de ajuste de operação.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="cdAtivoBMF"></param>
        public decimal RetornaTotalResultadoNormal(int idCliente, DateTime dataInicio, DateTime dataFim, string cdAtivoBMF)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ResultadoRealizado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.CdAtivoBMF.In(cdAtivoBMF),
                        this.Query.TipoOperacao.NotIn(TipoOperacaoBMF.CompraDaytrade,
                                                      TipoOperacaoBMF.VendaDaytrade),
                        this.Query.Origem.NotEqual((byte)OrigemOperacaoBMF.AjusteFuturo)
                        );
            this.Query.Load();

            // Se valores dos campos são nulos retorna zero
            if (this.es.HasData)
            {
                decimal valor = this.ResultadoRealizado.HasValue ? this.ResultadoRealizado.Value : 0;

                return valor;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Retorna o total de ajuste daytrade.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="cdAtivoBMF"></param>
        public decimal RetornaTotalAjusteDayTrade(int idCliente, DateTime dataInicio, DateTime dataFim, string cdAtivoBMF)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Ajuste.Sum(), this.query.Corretagem.Sum(),
                         this.Query.Emolumento.Sum(), this.query.Registro.Sum(), this.query.CustoWtr.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.CdAtivoBMF.In(cdAtivoBMF),
                        this.Query.TipoOperacao.In(TipoOperacaoBMF.CompraDaytrade,
                                                      TipoOperacaoBMF.VendaDaytrade)
                        );
            this.Query.Load();

            // Se valores dos campos são nulos retorna zero
            if (this.es.HasData)
            {
                decimal valor = this.Ajuste.HasValue ? this.Ajuste.Value : 0;

                decimal corretagem = this.Corretagem.HasValue ? this.Corretagem.Value : 0;
                valor -= corretagem;

                decimal emolumento = this.Emolumento.HasValue ? this.Emolumento.Value : 0;
                valor -= emolumento;

                decimal registro = this.Registro.HasValue ? this.Registro.Value : 0;
                valor -= registro;

                decimal custoWTR = this.CustoWtr.HasValue ? this.CustoWtr.Value : 0;
                valor -= custoWTR;

                return valor;
            }
            else
            {
                return 0;
            }            
        }

        /// <summary>
        /// Retorna o total de ajuste normal.        
        /// Filtra para não trazer "DOL", "IND", "DI1", "DDI", "WIN", "WDL".
        /// Filtra para trazer outros Futuros ou DLA.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        public decimal RetornaTotalAjusteNormalOutros(int idCliente, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Ajuste.Sum(), this.query.Corretagem.Sum(),
                         this.Query.Emolumento.Sum(), this.query.Registro.Sum(), this.query.CustoWtr.Sum())
                 .Where(this.Query.IdCliente == idCliente &
                        this.Query.Data.GreaterThanOrEqual(dataInicio) &
                        this.Query.Data.LessThanOrEqual(dataFim) &
                        this.Query.TipoOperacao.NotIn(TipoOperacaoBMF.CompraDaytrade, TipoOperacaoBMF.VendaDaytrade) &
                        this.Query.CdAtivoBMF.NotIn("DOL", "IND", "DI1", "DDI", "WIN", "WDL") &
                            (
                                this.Query.TipoMercado.Equal(TipoMercadoBMF.Futuro) |
                                this.Query.CdAtivoBMF.Equal("DLA")
                            )
                        );  
            this.Query.Load();

            // Se valores dos campos são nulos retorna zero
            if (this.es.HasData)
            {
                decimal valor = this.Ajuste.HasValue ? this.Ajuste.Value : 0;

                decimal corretagem = this.Corretagem.HasValue ? this.Corretagem.Value : 0;
                valor -= corretagem;

                decimal emolumento = this.Emolumento.HasValue ? this.Emolumento.Value : 0;
                valor -= emolumento;

                decimal registro = this.Registro.HasValue ? this.Registro.Value : 0;
                valor -= registro;

                decimal custoWTR = this.CustoWtr.HasValue ? this.CustoWtr.Value : 0;
                valor -= custoWTR;

                return valor;
            }
            else
            {
                return 0;
            }            
        }

        /// <summary>
        /// Retorna o total de ajuste normal.        
        /// Filtra para não trazer "DOL", "IND", "DI1", "DDI", "WIN", "WDL".
        /// Filtra para trazer outros Futuros ou DLA.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        public decimal RetornaTotalResultadoNormalOutros(int idCliente, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ResultadoRealizado.Sum())
                 .Where(this.Query.IdCliente == idCliente &
                        this.Query.Data.GreaterThanOrEqual(dataInicio) &
                        this.Query.Data.LessThanOrEqual(dataFim) &
                        this.Query.TipoOperacao.NotIn(TipoOperacaoBMF.CompraDaytrade, TipoOperacaoBMF.VendaDaytrade) &
                        this.Query.CdAtivoBMF.NotIn("DOL", "IND", "DI1", "DDI", "WIN", "WDL") &
                            (
                                this.Query.TipoMercado.Equal(TipoMercadoBMF.Futuro) |
                                this.Query.CdAtivoBMF.Equal("DLA")
                            ),
                        this.Query.Origem.NotEqual((byte)OrigemOperacaoBMF.AjusteFuturo)
                        );
            this.Query.Load();

            // Se valores dos campos são nulos retorna zero
            if (this.es.HasData)
            {
                decimal valor = this.ResultadoRealizado.HasValue ? this.ResultadoRealizado.Value : 0;
                                
                return valor;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Retorna o total de ajuste daytrade.        
        /// Filtra para não trazer "DOL", "IND", "DI1", "DDI", "WIN", "WDL".
        /// Filtra para trazer outros Futuros ou DLA.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>        
        public decimal RetornaTotalAjusteDayTradeOutros(int idCliente, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Ajuste.Sum(), this.Query.Corretagem.Sum(),
                         this.Query.Emolumento.Sum(), this.Query.Registro.Sum(), this.Query.CustoWtr.Sum())
                 .Where(this.Query.IdCliente == idCliente &                      
                        this.Query.Data.GreaterThanOrEqual(dataInicio) &
                        this.Query.Data.LessThanOrEqual(dataFim) &
                        this.Query.TipoOperacao.In(TipoOperacaoBMF.CompraDaytrade, TipoOperacaoBMF.VendaDaytrade) &
                        this.Query.CdAtivoBMF.NotIn("DOL", "IND", "DI1", "DDI", "WIN", "WDL") &
                            (                         
                                this.Query.TipoMercado.Equal(TipoMercadoBMF.Futuro) |
                                this.Query.CdAtivoBMF.Equal("DLA")
                            )
                        );                           
            this.Query.Load();

            // Se valores dos campos são nulos retorna zero
            if (this.es.HasData)
            {
                decimal valor = this.Ajuste.HasValue ? this.Ajuste.Value : 0;

                decimal corretagem = this.Corretagem.HasValue ? this.Corretagem.Value : 0;
                valor -= corretagem;

                decimal emolumento = this.Emolumento.HasValue ? this.Emolumento.Value : 0;
                valor -= emolumento;

                decimal registro = this.Registro.HasValue ? this.Registro.Value : 0;
                valor -= registro;

                decimal custoWTR = this.CustoWtr.HasValue ? this.CustoWtr.Value : 0;
                valor -= custoWTR;

                return valor;
            }
            else
            {
                return 0;
            }            
        }
                
        /// <summary>
        /// Retorna o total de ajustes daytrades líquidos (desconta as despesas) para o cliente no período.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        public decimal RetornaTotalAjusteDayTrade(int idCliente, int idAgenteCorretora,
                                                  DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Ajuste.Sum(), this.Query.Corretagem.Sum(),
                         this.Query.Emolumento.Sum(), this.Query.Registro.Sum(), this.Query.CustoWtr.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteCorretora == idAgenteCorretora,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.Equal(TipoMercadoBMF.Futuro),
                        this.Query.TipoOperacao.In(TipoOperacaoBMF.CompraDaytrade, TipoOperacaoBMF.VendaDaytrade)
                        );
            this.Query.Load();

            // Se valores dos campos são nulos retorna zero
            if (this.es.HasData)
            {
                decimal valor = this.Ajuste.HasValue ? this.Ajuste.Value : 0;
                
                decimal corretagem = this.Corretagem.HasValue ? this.Corretagem.Value : 0;
                valor -= corretagem;

                decimal emolumento = this.Emolumento.HasValue ? this.Emolumento.Value : 0;
                valor -= emolumento;

                decimal registro = this.Registro.HasValue ? this.Registro.Value : 0;
                valor -= registro;

                decimal custoWTR = this.CustoWtr.HasValue ? this.CustoWtr.Value : 0;
                valor -= custoWTR;

                return valor;
            }
            else
            {
                return 0;
            }            
        }

        /// <summary>
        /// Retorna o total de resultado daytrade.
        /// Filtra por TipoMercado.NotIn(TipoMercadoBMF.Futuro).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        public decimal RetornaTotalResultadoDayTrade(int idCliente, int idAgenteCorretora, 
                                                     DateTime dataInicio, DateTime dataFim)
        {
            //Total de Venda DT
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteCorretora == idAgenteCorretora,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.NotIn(TipoMercadoBMF.Futuro),
                        this.Query.TipoOperacao.In(TipoOperacaoBMF.VendaDaytrade)
                        );
            this.Query.Load();

            decimal totalVendaDT = 0;
            if (this.es.HasData)
            {
                totalVendaDT = this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
            }

            //Total de Compra DT
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteCorretora == idAgenteCorretora,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.NotIn(TipoMercadoBMF.Futuro),
                        this.Query.TipoOperacao.In(TipoOperacaoBMF.CompraDaytrade)
                        );
            this.Query.Load();

            decimal totalCompraDT = 0;
            if (this.es.HasData)
            {
                totalCompraDT = this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
            }

            decimal totalResultadoDayTrade = totalVendaDT - totalCompraDT;

            return totalResultadoDayTrade;            
        }

        /// <summary>
        /// Integra operações vindas do Sinacor, e joga na tabela OperacaoBMF.
        /// Caso o ativo não exista, é cadastrado direto em AtivoBMF.
        /// Processo NÃO transacionado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        public void IntegraOperacoesSinacor(int idCliente, DateTime dataOperacao)
        {
            ClienteBMF clienteBMF = new ClienteBMF();

            #region Busca código do cliente (codigoCliente na BMF)
            //Se não achar pelo menos o código do cliente associado, sai da função
            if (!clienteBMF.LoadByPrimaryKey(idCliente))
            {
                return;
            }
            
            //Se não tiver código associado, lança exception
            if (String.IsNullOrEmpty(clienteBMF.CodigoSinacor))
            {
                return;
            }

            //Se código não é número, lança exception
            if (!Utilitario.IsInteger(clienteBMF.CodigoSinacor))
            {
                return;
            }

            int codigoCliente = 0;
            if (!String.IsNullOrEmpty(clienteBMF.CodigoSinacor))
            {
                codigoCliente = Convert.ToInt32(clienteBMF.CodigoSinacor);
            }
            #endregion

            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);

            //Deleta a Custodia para Permanencia dado o Cliente/Data
            CustodiaBMFCollection custodiaBMFCollection = new CustodiaBMFCollection();
            custodiaBMFCollection.DeletaCustodiaPermanencia(idCliente, dataOperacao);

            //Deleta operações com Fonte = Sinacor
            OperacaoBMFCollection operacaoBMFCollectionDeletar = new OperacaoBMFCollection();
            operacaoBMFCollectionDeletar.DeletaOperacaoBMFSinacor(idCliente, dataOperacao);

            //Abre nova conexão para o Sinacor (ORACLE) e busca ordens do dia
            TmffinpreCollection tmffinpreCollection = new TmffinpreCollection();
            tmffinpreCollection.BuscaOperacoesSINACOR(codigoCliente, dataOperacao);

            TmffinpreHCollection tmffinpreHCollection = new TmffinpreHCollection();
            tmffinpreHCollection.BuscaOperacoesSINACOR(codigoCliente, dataOperacao);

            TmffinpreCollection tmffinpreCollectionAux = new TmffinpreCollection(tmffinpreHCollection);

            tmffinpreCollection.Combine(tmffinpreCollectionAux);            

            int idAgenteMercado = 0;
            if (tmffinpreCollection.Count > 0)
            {
                //Busca o IdAgenteMercado default (usa o de Bovespa)
                int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
                AgenteMercado agenteMercado = new AgenteMercado();
                idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);
            }

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();

            decimal totalEmolumento = 0;
            decimal totalRegistro = 0;
            for (int i = 0; i < tmffinpreCollection.Count; i++)
            {
                Tmffinpre tmffinpre = tmffinpreCollection[i];

                string cdAtivoBMF = tmffinpre.CdCommod.Trim();
                string serie = tmffinpre.CdSerie.Trim();

                decimal corretagem = tmffinpre.VlCorneg.Value;
                decimal emolumento = tmffinpre.VlEmoneg.Value;
                decimal registro = tmffinpre.VlTaxreg.Value;


                if (tmffinpre.TpNegocio == "TP")
                {
                    DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataOperacao, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    DateTime dataProxima = Calendario.AdicionaDiaUtil(dataOperacao, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    CustodiaBMF custodiaBMF = new CustodiaBMF();
                    CustodiaBMF custodiaBMFInserir = new CustodiaBMF();

                    #region Trata Taxa de Permanencia
                    if (!custodiaBMF.BuscaCustodiaBMF(idCliente, cdAtivoBMF, idAgenteMercado, dataAnterior, null))
                    {
                        #region Insere CustodiaBMF Nova (Custodia de 1 dia apenas)
                        custodiaBMFInserir.AddNew();
                        custodiaBMFInserir.CdAtivoBMF = cdAtivoBMF;
                        custodiaBMFInserir.Data = dataOperacao;
                        custodiaBMFInserir.IdAgente = idAgenteMercado;
                        custodiaBMFInserir.IdCliente = idCliente;
                        custodiaBMFInserir.ValorDiario = emolumento;
                        custodiaBMFInserir.ValorAcumulado = emolumento;
                        custodiaBMFInserir.ValorPagar = emolumento;
                        custodiaBMFInserir.DataPagamento = dataProxima;

                        if (ParametrosConfiguracaoSistema.Outras.MultiConta)
                        {
                            custodiaBMFInserir.IdConta = idContaDefault;
                        }
                        custodiaBMFInserir.Save();
                        #endregion
                    }
                    else
                    {
                        #region Insere CustodiaBMF Nova, pegando o valor acumulado em D-1
                        custodiaBMFInserir.AddNew();
                        custodiaBMFInserir.IdCliente = idCliente;
                        custodiaBMFInserir.IdAgente = idAgenteMercado;
                        custodiaBMFInserir.CdAtivoBMF = cdAtivoBMF;
                        custodiaBMFInserir.Data = dataOperacao;
                        custodiaBMFInserir.ValorDiario = emolumento;
                        custodiaBMFInserir.ValorAcumulado = custodiaBMF.ValorAcumulado + emolumento;
                        custodiaBMFInserir.ValorPagar = custodiaBMF.ValorAcumulado + emolumento;
                        custodiaBMFInserir.DataPagamento = dataProxima;
                        if (ParametrosConfiguracaoSistema.Outras.MultiConta)
                        {
                            custodiaBMFInserir.IdConta = idContaDefault;
                        }
                        custodiaBMFInserir.Save();
                        #endregion
                    }
                    #endregion

                    #region Nova Liquidacao (Provisão de tx permanencia)
                    if (custodiaBMFInserir.ValorDiario.Value > 0)
                    {
                        AgenteMercado agenteMercado = new AgenteMercado();
                        List<esQueryItem> campos = new List<esQueryItem>();
                        campos.Add(agenteMercado.Query.Nome);
                        agenteMercado.LoadByPrimaryKey(idAgenteMercado);
                        string nomeAgente = agenteMercado.Nome;

                        Liquidacao liquidacao = new Liquidacao();
                        liquidacao.DataLancamento = dataOperacao;
                        liquidacao.DataVencimento = custodiaBMFInserir.DataPagamento.Value;
                        liquidacao.Descricao = "Taxa de Permanência Futuros - " + cdAtivoBMF + " (" + nomeAgente + ")";
                        liquidacao.Valor = custodiaBMFInserir.ValorDiario.Value * -1;
                        liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = (int)OrigemLancamentoLiquidacao.BMF.TaxaPermanencia;
                        liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdAgente = idAgenteMercado;
                        liquidacao.IdCliente = idCliente;
                        liquidacao.IdConta = idContaDefault;

                        liquidacao.Save();
                    }
                    #endregion

                    continue;
                }

                //Busca ativo
                AtivoBMF ativoBMF = new AtivoBMF();
                DateTime? dataVencimento = new DateTime();
                decimal peso = 0;
                byte tipoMercado = 0;
                if (!ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie))
                {
                    //Importa do Sinacor o ativo
                    AtivoBMF ativoBMFImportar = new AtivoBMF();
                    if (ativoBMFImportar.IntegraAtivoSinacor(cdAtivoBMF, serie))
                    {
                        dataVencimento = ativoBMFImportar.DataVencimento;
                        peso = ativoBMFImportar.Peso.Value;
                        tipoMercado = ativoBMFImportar.TipoMercado.Value;
                    }
                    //
                }
                else
                {
                    dataVencimento = ativoBMF.DataVencimento;
                    peso = ativoBMF.Peso.Value;
                    tipoMercado = ativoBMF.TipoMercado.Value;
                }

                //Só permite importar se data do vencimento do ativo for maior ou igual q a data de movimento
                if ((!dataVencimento.HasValue) || (DateTime.Compare(dataVencimento.Value, dataOperacao) >= 0))
                {
                    string tipoOperacao = tmffinpre.CdNatope.Trim();
                    #region Trata tipo de operação (para DI1, DDI e caso de DayTrade)
                    if (tmffinpre.TpNegocio == "DT")
                    {
                        tipoOperacao = tipoOperacao + "D";
                    }
                    if (cdAtivoBMF == "DI1" || cdAtivoBMF == "DDI")
                    {
                        if (tipoOperacao == "C")
                        {
                            tipoOperacao = "V";
                        }
                        else if (tipoOperacao == "CD")
                        {
                            tipoOperacao = "VD";
                        }
                        else if (tipoOperacao == "V")
                        {
                            tipoOperacao = "C";
                        }
                        else if (tipoOperacao == "VD")
                        {
                            tipoOperacao = "CD";
                        }
                    }
                    #endregion

                    decimal pu = tmffinpre.PrNegocioAju.Value;
                    int quantidade = (int)Math.Abs(tmffinpre.QtQtdesp.Value); //Pode vir negativo para V e VD
                    decimal valor = Math.Round(pu * quantidade * peso, 2);

                    decimal valorLiquido = 0;
                    if (tipoOperacao == TipoOperacaoBMF.Compra || tipoOperacao == TipoOperacaoBMF.CompraDaytrade)
                    {
                        valorLiquido = valor + corretagem + emolumento + registro;
                    }
                    else
                    {
                        valorLiquido = valor - (corretagem + emolumento + registro);
                    }

                    decimal puLiquido = (valorLiquido / quantidade) / peso;

                    OperacaoBMF operacaoBMF = operacaoBMFCollection.AddNew();
                    operacaoBMF.CalculaDespesas = "N";
                    operacaoBMF.CdAtivoBMF = cdAtivoBMF;
                    operacaoBMF.Corretagem = corretagem;
                    operacaoBMF.Data = dataOperacao;
                    operacaoBMF.Emolumento = emolumento;
                    operacaoBMF.Fonte = (byte)FonteOperacaoBMF.Sinacor;
                    operacaoBMF.IdAgenteCorretora = idAgenteMercado;
                    operacaoBMF.IdAgenteLiquidacao = idAgenteMercado;
                    operacaoBMF.IdCliente = idCliente;
                    operacaoBMF.Origem = OrigemOperacaoBMF.Primaria;
                    operacaoBMF.Pu = pu;
                    operacaoBMF.PULiquido = puLiquido;
                    operacaoBMF.Quantidade = quantidade;
                    operacaoBMF.Registro = registro;
                    operacaoBMF.Serie = serie;
                    operacaoBMF.TipoMercado = tipoMercado;
                    operacaoBMF.TipoOperacao = tipoOperacao;
                    operacaoBMF.Valor = valor;
                    operacaoBMF.ValorLiquido = valorLiquido;
                    operacaoBMF.PercentualDesconto = 0;
                    operacaoBMF.IdMoeda = (int)ListaMoedaFixo.Real;

                    if (ParametrosConfiguracaoSistema.Outras.MultiConta)
                    {
                        operacaoBMF.IdConta = idContaDefault;
                    }

                    totalEmolumento += emolumento;
                    totalRegistro += registro;

                    //if (i == (tmffinpreCollection.Count - 1) && clienteBMF.InvestidorInstitucional == "N")
                    //{
                    //    decimal outrosCustosEmolumento = Math.Round(totalEmolumento * percOutrosCustosEmolumento / 100, 2);
                    //    decimal outrosCustosRegistro = Math.Round(totalRegistro * percOutrosCustosRegistro / 100, 2);
                    //    operacaoBMF.OutrosCustos = outrosCustosEmolumento + outrosCustosRegistro;
                    //}
                    //else
                    //{
                        operacaoBMF.OutrosCustos = 0;
                    //}
                }                

            }

            operacaoBMFCollection.Save();

        }

        /// <summary>
        /// Rateia corretagem e taxas a partir dos valores informados do dia.
        /// Somente para operações com CalculaDespesas = 'N'.
        /// As taxas são jogadas todas em Emolumento, o resto fica zerado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="corretagem"></param>
        /// <param name="taxas"></param>
        public void RateiaTaxasTudo(int idCliente, DateTime data, decimal totalCorretagem, decimal totalTaxas)
        {
            decimal corretagemResidual = totalCorretagem;
            decimal taxasResidual = totalTaxas;
            decimal totalValor = 0;

            OperacaoBMF operacaoBMF = new OperacaoBMF();
            operacaoBMF.Query.Select(operacaoBMF.Query.Valor.Sum());
            operacaoBMF.Query.Where(operacaoBMF.Query.IdCliente.Equal(idCliente),
                                    operacaoBMF.Query.Data.Equal(data),
                                    operacaoBMF.Query.Fonte.NotIn((byte)FonteOperacaoBMF.Sinacor,
                                                                  (byte)FonteOperacaoBMF.Interno));

            if (ParametrosConfiguracaoSistema.BMF.TipoCalculoTaxasBMF != (int)TipoCalculoTaxasBMF.NaoCalcula)
            {
                operacaoBMF.Query.Where(operacaoBMF.Query.CalculaDespesas.Equal("N"));
            }

            if (operacaoBMF.Query.Load())
            {
                if (operacaoBMF.Valor.HasValue)
                {
                    totalValor = operacaoBMF.Valor.Value;
                }
            }
            else
            {
                return;
            }

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoBMFCollection.Query.Data.Equal(data),
                                                operacaoBMFCollection.Query.Fonte.NotIn((byte)FonteOperacaoBMF.Sinacor,
                                                                  (byte)FonteOperacaoBMF.Interno));

            if (ParametrosConfiguracaoSistema.BMF.TipoCalculoTaxasBMF != (int)TipoCalculoTaxasBMF.NaoCalcula)
            {
                operacaoBMF.Query.Where(operacaoBMFCollection.Query.CalculaDespesas.Equal("N"));
            }

            operacaoBMFCollection.Query.Load();

            #region Rateia Taxas
            for (int i = 0; i < operacaoBMFCollection.Count; i++)
            {
                operacaoBMF = new OperacaoBMF();
                operacaoBMF = operacaoBMFCollection[i];
                decimal valor = operacaoBMF.Valor.Value;

                // calculo das proporções
                decimal corretagem = Utilitario.Truncate(((totalCorretagem * valor) / totalValor), 2);
                decimal taxas = Utilitario.Truncate(((totalTaxas * valor) / totalValor), 2);

                corretagemResidual -= corretagem;
                taxasResidual -= taxas;

                // Se é a ultima operacao da corretora, garante-se que 100% da corretagem e taxas
                // foram distribuidas, jogando todo o residual para a ultima operacao
                if (i == operacaoBMFCollection.Count - 1)
                {
                    corretagem += corretagemResidual;
                    taxas += taxasResidual;
                }


                if (totalCorretagem != 0) //Testa isso, já que posso ratear apenas os custos (emol e reg)
                {
                    operacaoBMF.Corretagem = corretagem;
                }

                operacaoBMF.Emolumento = taxas;
                operacaoBMF.Registro = 0;
                operacaoBMF.CustoWtr = 0;
                operacaoBMF.OutrosCustos = 0;
            }
            #endregion

            // Salva a collection de operações
            operacaoBMFCollection.Save();
        }

        /// <summary>
        /// Rateia corretagem e taxas a partir dos valores informados do dia.
        /// Somente para operações com CalculaDespesas = 'N'.
        /// As taxas são jogadas todas em Emolumento, o resto fica zerado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="corretagem"></param>
        /// <param name="taxas"></param>
        public void RateiaTaxasTudoPorIds(List<int> listaIds, decimal totalCorretagem, decimal totalTaxas)
        {
            decimal corretagemResidual = totalCorretagem;
            decimal taxasResidual = totalTaxas;
            decimal totalValor = 0;

            OperacaoBMF operacaoBMF = new OperacaoBMF();
            operacaoBMF.Query.Select(operacaoBMF.Query.Valor.Sum());
            operacaoBMF.Query.Where(operacaoBMF.Query.IdOperacao.In(listaIds));

            if (operacaoBMF.Query.Load())
            {
                if (operacaoBMF.Valor.HasValue)
                {
                    totalValor = operacaoBMF.Valor.Value;
                }
            }
            else
            {
                return;
            }

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.IdOperacao.In(listaIds));

            operacaoBMFCollection.Query.Load();

            #region Rateia Taxas
            for (int i = 0; i < operacaoBMFCollection.Count; i++)
            {
                operacaoBMF = new OperacaoBMF();
                operacaoBMF = operacaoBMFCollection[i];
                decimal valor = operacaoBMF.Valor.Value;

                // calculo das proporções
                decimal corretagem = Utilitario.Truncate(((totalCorretagem * valor) / totalValor), 2);
                decimal taxas = Utilitario.Truncate(((totalTaxas * valor) / totalValor), 2);

                corretagemResidual -= corretagem;
                taxasResidual -= taxas;

                // Se é a ultima operacao da corretora, garante-se que 100% da corretagem e taxas
                // foram distribuidas, jogando todo o residual para a ultima operacao
                if (i == operacaoBMFCollection.Count - 1)
                {
                    corretagem += corretagemResidual;
                    taxas += taxasResidual;
                }


                if (totalCorretagem != 0) //Testa isso, já que posso ratear apenas os custos (emol e reg)
                {
                    operacaoBMF.Corretagem = corretagem;
                }

                operacaoBMF.Emolumento = taxas;
                operacaoBMF.Registro = 0;
                operacaoBMF.CustoWtr = 0;
                operacaoBMF.OutrosCustos = 0;
                operacaoBMF.CalculaDespesas = "N";
            }
            #endregion

            // Salva a collection de operações
            operacaoBMFCollection.Save();
        }

        /// <summary>
        /// Retorna a quantidade de contratos operados de compra ou compra DT.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="serie"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public int RetornaTotalQuantidadeCompra(int idCliente, string cdAtivoBMF, string serie,
                                     DateTime dataInicio, DateTime dataFim)
        {
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Query.Select(operacaoBMFCollection.Query.Quantidade.Sum());
            operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.IdCliente == idCliente,
                                              operacaoBMFCollection.Query.CdAtivoBMF == cdAtivoBMF,
                                              operacaoBMFCollection.Query.Serie == serie,
                                              operacaoBMFCollection.Query.Data.GreaterThanOrEqual(dataInicio),
                                              operacaoBMFCollection.Query.Data.LessThanOrEqual(dataFim),
                                              operacaoBMFCollection.Query.TipoOperacao.In(TipoOperacaoBMF.Compra, TipoOperacaoBMF.CompraDaytrade));
            operacaoBMFCollection.Query.Load();

            if (this.es.HasData)
            {
                return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
            }
            else
            {
                return 0;
            }            
        }

        /// <summary>
        /// Retorna a quantidade de contratos operados de venda ou venda DT.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="serie"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public int RetornaTotalQuantidadeVenda(int idCliente, string cdAtivoBMF, string serie,
                                     DateTime dataInicio, DateTime dataFim)
        {
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Query.Select(operacaoBMFCollection.Query.Quantidade.Sum());
            operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.IdCliente == idCliente,
                                              operacaoBMFCollection.Query.CdAtivoBMF == cdAtivoBMF,
                                              operacaoBMFCollection.Query.Serie == serie,
                                              operacaoBMFCollection.Query.Data.GreaterThanOrEqual(dataInicio),
                                              operacaoBMFCollection.Query.Data.LessThanOrEqual(dataFim),
                                              operacaoBMFCollection.Query.TipoOperacao.In(TipoOperacaoBMF.Venda, TipoOperacaoBMF.VendaDaytrade));
            operacaoBMFCollection.Query.Load();

            if (this.es.HasData)
            {
                return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Carrega em OperacaoBMF a partir de nota de corretagem PDF.
        /// </summary>
        /// <param name="streamArquivoPDF">Stream de Bytes do Arquivo PDF</param>
        /// <exception cref="AtivoNaoCadastradoException">Se Código do Ativo não estiver Cadastrado na Base</exception>
        /// <exception cref="CodigoClienteBovespaInvalido">Se não existe idCliente no Sistema com o CodigoClienteBovespa informado no arquivo</exception>
        public void CarregaNotaCorretagemPDF(Stream streamArquivoPDF)
        {
            NotaCorretagemBMFPDFCollection notaCorretagemBMFPDFCollection = new NotaCorretagemBMFPDFCollection();
            List<NotaCorretagemBMFPDF> listaNotaCorretagem = notaCorretagemBMFPDFCollection.RetornaOperacoesBMF(streamArquivoPDF);

            if (listaNotaCorretagem.Count == 0)
            {
                return;
            }

            int codigoBovespaAgente = listaNotaCorretagem[0].CodigoBovespa;

            #region GetIdAgente
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            agenteMercado.BuscaIdAgenteMercadoBMF(codigoBovespaAgente);

            int idAgenteCorretora = 0;
            if (agenteMercado.IdAgente.HasValue)
            {
                idAgenteCorretora = agenteMercado.IdAgente.Value;
            }
            else
            {
                throw new IdAgenteNaoCadastradoException("Agente com código Bovespa não encontrado - " + codigoBovespaAgente.ToString());
            }
            #endregion

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            
            List<int> listaIdCliente = new List<int>();
            List<DateTime> listaDatas = new List<DateTime>();
            List<decimal> listaTaxas = new List<decimal>();
            foreach (NotaCorretagemBMFPDF notaCorretagem in listaNotaCorretagem)
            {
                #region Confere o IdCliente Interno
                int codigoClienteBovespa = notaCorretagem.CodigoCliente;
                CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespa);
                if (!idClienteExiste)
                {
                    throw new Exception("\nProcessando Arquivo Nota de Corretagem: Não Existe Conta por Agente Cadastrada para o Cliente " + codigoClienteBovespa + " na Corretora " + codigoBovespaAgente);
                }
                #endregion

                int idCliente = codigoClienteAgente.IdCliente.Value;

                this.CheckValidData(idCliente, notaCorretagem.DataPregao); //Este check vale tanto para Insert em OrdemBMF

                listaIdCliente.Add(idCliente);
                listaDatas.Add(notaCorretagem.DataPregao);

                listaTaxas.Add(notaCorretagem.TaxaRegistroBMF + notaCorretagem.TaxasBMF);
                                
                foreach (DetalheNotaBMFPDF detalheNotaBMFPDF in notaCorretagem.ListaDetalheNotaBMFPDF)
                {
                    AtivoBMF ativoBMF = new AtivoBMF();
                    string cdAtivo = detalheNotaBMFPDF.Mercadoria.Trim();

                    string cdAtivoBMF = cdAtivo.Substring(0, 3).Trim();
                    string serie = cdAtivo.Replace(cdAtivoBMF, "").Trim();

                    if (!ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie))
                    {
                        throw new AtivoBMFNaoCadastradoException("Ativo não encontrado - " + cdAtivo + serie);
                    }
                                        
                    byte tipoMercado = ativoBMF.TipoMercado.Value;
                    int quantidade = Convert.ToInt32(detalheNotaBMFPDF.Quantidade);
                    decimal pu = detalheNotaBMFPDF.PrecoAjuste;
                    decimal corretagem = detalheNotaBMFPDF.TaxaOperacional;

                    string tipoOperacao = detalheNotaBMFPDF.CV;
                    if (detalheNotaBMFPDF.TipoNegocio.Contains("DAY"))
                    {
                        tipoOperacao = tipoOperacao + "D";
                    }

                    decimal valor = Utilitario.Truncate(quantidade * pu * ativoBMF.Peso.Value, 2);
                    
                    #region Preenche OperacaoBMF
                    OperacaoBMF operacaoBMF = operacaoBMFCollection.AddNew();
                    operacaoBMF.Ajuste = 0;
                    operacaoBMF.CalculaDespesas = "N";
                    operacaoBMF.CdAtivoBMF = cdAtivoBMF;
                    operacaoBMF.Corretagem = corretagem;
                    operacaoBMF.CustoWtr = 0;
                    operacaoBMF.Data = notaCorretagem.DataPregao;
                    operacaoBMF.Emolumento = 0;
                    operacaoBMF.Fonte = (byte)FonteOperacaoBMF.NotaPDFSinacor;
                    operacaoBMF.IdAgenteCorretora = idAgenteCorretora;
                    operacaoBMF.IdAgenteLiquidacao = idAgenteCorretora;
                    operacaoBMF.IdCliente = idCliente;
                    operacaoBMF.IdMoeda = (int)ListaMoedaFixo.Real;
                    operacaoBMF.Origem = (byte)OrigemOperacaoBMF.Primaria;
                    operacaoBMF.OutrosCustos = 0;
                    operacaoBMF.Pu = pu;
                    operacaoBMF.PULiquido = pu;
                    operacaoBMF.Quantidade = quantidade;
                    operacaoBMF.Registro = 0;
                    operacaoBMF.ResultadoRealizado = 0;
                    operacaoBMF.Serie = serie;
                    operacaoBMF.TipoMercado = ativoBMF.TipoMercado.Value;
                    operacaoBMF.TipoOperacao = tipoOperacao;
                    operacaoBMF.Valor = valor;
                    operacaoBMF.ValorLiquido = valor;
                    #endregion

                }
            }

            using (esTransactionScope scope = new esTransactionScope())
            {
                #region Deleta antes de salvar as collections
                for (int i = 0; i < listaIdCliente.Count; i++)
                {
                    int idClienteLista = listaIdCliente[i];
                    DateTime dataLista = listaDatas[i];

                    OperacaoBMFCollection operacaoBMFCollectionDeletar = new OperacaoBMFCollection();
                    operacaoBMFCollectionDeletar.Query.Select(operacaoBMFCollectionDeletar.Query.IdOperacao);
                    operacaoBMFCollectionDeletar.Query.Where(operacaoBMFCollectionDeletar.Query.IdCliente.Equal(idClienteLista),
                                                             operacaoBMFCollectionDeletar.Query.Data.Equal(dataLista),
                                                             operacaoBMFCollectionDeletar.Query.IdAgenteCorretora.Equal(idAgenteCorretora),
                                                             operacaoBMFCollectionDeletar.Query.Fonte.Equal((byte)FonteOperacaoBMF.NotaPDFSinacor));
                    operacaoBMFCollectionDeletar.Query.Load();
                    operacaoBMFCollectionDeletar.MarkAllAsDeleted();
                    operacaoBMFCollectionDeletar.Save();
                }

                operacaoBMFCollection.Save();
                #endregion

                #region Rateio de Taxas
                for (int i = 0; i < listaIdCliente.Count; i++)
                {
                    int idClienteLista = listaIdCliente[i];
                    DateTime dataLista = listaDatas[i];

                    decimal taxas = listaTaxas[i];

                    if (taxas != 0)
                    {
                        OperacaoBMF operacaoBMF = new OperacaoBMF();
                        operacaoBMF.RateiaTaxasTudo(idClienteLista, dataLista, 0, taxas); //Rateia apenas as taxas, a corretagem mantem o que veio na linha detalhe
                    }
                }
                #endregion
                                
                scope.Complete();
            }

        }

        private void CheckValidData(int idCliente, DateTime data)
        {
            //Verifica se a Ordem pode ser salva

            //Criterio: DataOrdem nao pode ser anterior a data cliente ou entao é no mesmo dia e o cliente não está fechado

            Cliente clienteCheck = new Cliente();
            clienteCheck.LoadByPrimaryKey(idCliente);

            if ((data < clienteCheck.DataDia.Value) || (data == clienteCheck.DataDia.Value && clienteCheck.Status.Value == (byte)StatusCliente.Divulgado))
            {
                throw new Exception("\nOrdem não foi processada. Problema: Data informada anterior à data do cliente ou status do cliente fechado.");
            }
        }
    }
}
