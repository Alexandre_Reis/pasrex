using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.BMF.Enums;
using Financial.Common.Enums;

namespace Financial.BMF
{
	public partial class AtivoBMFCollection : esAtivoBMFCollection
	{
        /// <summary>
        /// Carrega o objeto AtivoBMFCollection com o campo DataVencimento.
        /// Ordena crescente pela dataVencimento.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="dataVencimento"></param>        
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaVencimentos(string cdAtivoBMF, DateTime dataVencimento)
        {   
            this.QueryReset();
            this.Query
                 .Select(this.Query.DataVencimento)
                 .Where(this.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                        this.Query.DataVencimento.GreaterThanOrEqual(dataVencimento))
                 .OrderBy(this.Query.DataVencimento.Ascending);

            this.Query.Load();
            
            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto AtivoBMFCollection com os campos Serie, DataVencimento.
        /// Ordena crescente pela dataVencimento.
        /// Busca futuros de Dolar com dataVencimento > data
        /// ou outros com dataVencimento >= data.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="dataVencimento"></param>        
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaSeriesCalculoTaxasVencimento(string cdAtivoBMF, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Serie, this.Query.DataVencimento)
                 .Where(this.Query.CdAtivoBMF.Equal(cdAtivoBMF),                        
                        this.Query.Or(
                            this.Query.TipoMercado == TipoMercadoBMF.Futuro,
                            this.Query.CdAtivoBMF.In("DOL","WDL","EUR"),
                            this.Query.DataVencimento.GreaterThan(data)
                            ),
                        this.Query.Or(
                            this.Query.TipoMercado == TipoMercadoBMF.Futuro,
                            this.Query.CdAtivoBMF.NotIn("DOL","WDL","EUR"),
                            this.Query.DataVencimento.GreaterThanOrEqual(data)
                            )                                   
                            )
                 .OrderBy(this.Query.DataVencimento.Ascending);

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto AtivoBMFCollection com os campos Serie, DataVencimento.
        /// Filtra pela DataVencimento > data.
        /// Ordena crescente pela dataVencimento.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="tipoMercado"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaSeriesVincendas(string cdAtivoBMF, int tipoMercado, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Serie, this.Query.DataVencimento)
                 .Where(this.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                        this.Query.TipoMercado.Equal(tipoMercado),
                        this.Query.DataVencimento.GreaterThan(data))
                 .OrderBy(this.Query.DataVencimento.Ascending);

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto AtivoBMFCollection com todos os campos de AtivoBMFCollection.
        /// Filtra IdMoeda = Dolar.
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="data"></param>
        /// <returns>bool indicando se foi encontrado registros</returns>
        public bool BuscaAtivoOffshore()
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdMoeda.NotEqual((short)ListaMoedaFixo.Real));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto AtivoBMFCollection com todos os campos de AtivoBMFCollection.
        /// Filtra IdMoeda = Real.
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="data"></param>
        /// <returns>bool indicando se foi encontrado registros</returns>
        public bool BuscaAtivoOnshore()
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdMoeda.Equal((short)ListaMoedaFixo.Real));

            bool retorno = this.Query.Load();

            return retorno;
        }
               
	}
}
