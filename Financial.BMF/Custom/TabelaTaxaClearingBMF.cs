﻿using System;
using System.Collections.Generic;
using System.Text;
using EntitySpaces.Interfaces;
namespace Financial.BMF
{
    public partial class TabelaTaxaClearingBMF : esTabelaTaxaClearingBMF
    {
        public bool BuscaTabelaRepassaCorretagem(int idCliente, int tipoMercado, string cdAtivoBMF, int faixaVencimento)
        {
            PerfilTaxaClearingBMFQuery perfilTaxaClearingBMFQuery = new PerfilTaxaClearingBMFQuery("P");
            PerfilTaxaClearingBMFClienteQuery perfilTaxaClearingBMFClienteQuery = new PerfilTaxaClearingBMFClienteQuery("C");
            TabelaTaxaClearingBMFQuery tabelaTaxaClearingBMFQuery = new TabelaTaxaClearingBMFQuery("T");
            TabelaTaxaClearingBMFCollection tabelaTaxaClearingBMFCollection = new TabelaTaxaClearingBMFCollection();

            tabelaTaxaClearingBMFQuery.Select(
                tabelaTaxaClearingBMFQuery.ValorFixoAgenteCustodia,
                tabelaTaxaClearingBMFQuery.ValorFixoCorretora);
            tabelaTaxaClearingBMFQuery.InnerJoin(perfilTaxaClearingBMFClienteQuery).On(perfilTaxaClearingBMFClienteQuery.IdPerfilTaxaClearingBMF == tabelaTaxaClearingBMFQuery.IdPerfilTaxaClearingBMF);
            tabelaTaxaClearingBMFQuery.Where(perfilTaxaClearingBMFClienteQuery.IdCliente == idCliente &&
                                            tabelaTaxaClearingBMFQuery.TipoMercado == tipoMercado &&
                                            tabelaTaxaClearingBMFQuery.CdAtivoBMF == cdAtivoBMF );

            tabelaTaxaClearingBMFQuery.Where((tabelaTaxaClearingBMFQuery.FaixaVencimento == faixaVencimento && tabelaTaxaClearingBMFQuery.UltimoVencimento == "0") | 
                                             (tabelaTaxaClearingBMFQuery.FaixaVencimento <= faixaVencimento && tabelaTaxaClearingBMFQuery.UltimoVencimento == "1") );

            tabelaTaxaClearingBMFCollection.Load(tabelaTaxaClearingBMFQuery);

            if (tabelaTaxaClearingBMFCollection.HasData)
            {
                List<TabelaTaxaClearingBMF> lstTabelaTaxaClearingBMF = (List<TabelaTaxaClearingBMF>)tabelaTaxaClearingBMFCollection;

                TabelaTaxaClearingBMF tabelaTaxaClearingBMF = lstTabelaTaxaClearingBMF.Find(delegate(TabelaTaxaClearingBMF x) { return x.TipoMercado == tipoMercado; });

                if (tabelaTaxaClearingBMF != null && tabelaTaxaClearingBMF.ValorFixoAgenteCustodia.HasValue)
                {
                    // Copia informações de perfilCorretagemBMFCollection para PerfilCorretagemBMF
                    this.AddNew();
                    this.ValorFixoAgenteCustodia = tabelaTaxaClearingBMF.ValorFixoAgenteCustodia.Value;
                    this.ValorFixoCorretora = tabelaTaxaClearingBMF.ValorFixoCorretora.Value;
                    return true;
                }

                tabelaTaxaClearingBMF = lstTabelaTaxaClearingBMF.Find(delegate(TabelaTaxaClearingBMF x) { return x.TipoMercado == null; });

                if (tabelaTaxaClearingBMF != null && tabelaTaxaClearingBMF.ValorFixoAgenteCustodia.HasValue)
                {
                    // Copia informações de perfilCorretagemBMFCollection para PerfilCorretagemBMF
                    this.AddNew();
                    this.ValorFixoAgenteCustodia = ((TabelaTaxaClearingBMF)tabelaTaxaClearingBMFCollection[0]).ValorFixoAgenteCustodia.Value;
                    this.ValorFixoCorretora = ((TabelaTaxaClearingBMF)tabelaTaxaClearingBMFCollection[0]).ValorFixoCorretora.Value;
                    return true;
                }
            }

            return false;

        }
    }
}
