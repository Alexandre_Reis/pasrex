using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.BMF {
    public partial class TransferenciaBMFCollection : esTransferenciaBMFCollection {
        private static readonly ILog log = LogManager.GetLogger(typeof(TransferenciaBMFCollection));

        /// <summary>
        /// Carrega o objeto TransferenciaBMFCollection com os campos IdTransferencia, CdAtivoBMF, IdAgenteOrigem,
        /// Serie, IdAgenteDestino, Quantidade, Pu, IdCliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaTransferenciaBMF(int idCliente, DateTime data) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdTransferencia, this.Query.CdAtivoBMF,
                        this.Query.IdAgenteOrigem, this.Query.Serie,
                        this.Query.IdAgenteDestino, this.Query.Quantidade, this.Query.Pu,
                        this.Query.IdCliente)
                 .Where(this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente);

            this.Query.Load();
        }

    }
}
