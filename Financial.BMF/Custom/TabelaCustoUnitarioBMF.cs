﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.BMF.Enums;
using log4net;
using Financial.Util;
using Financial.BMF.Exceptions;
using Financial.Common.Enums;
using Financial.Common;
using Financial.Interfaces.Import.BMF;
using Financial.Interfaces.Sinacor;

namespace Financial.BMF
{
    public class TabelaCustoUnitarioBMF
    {
        /// <summary>
        /// Retorna o custo unitário, já convertido pelo dólar se for o caso. Se não achar o ativo, retorna 0.
        /// </summary>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="quantidade"></param>
        /// <returns></returns>
        public decimal RetornaCustoUnitario(string cdAtivoBMF, decimal quantidade, DateTime data)
        {
            decimal custoUnitarioFinal = 0;

            bool semVolume = quantidade == 0;

            decimal custoUnitario = 0;
            decimal quantidadeAcumulada = 0;
            decimal quantidadeResidual = quantidade;
            decimal valorPonderado = 0;
            decimal quantidadeAdicional = 0;
            decimal quantidadeFaixa = 0;
            decimal quantidadeFaixaAnterior = 0;
            while (quantidadeResidual > 0 || semVolume)
            {
                quantidadeFaixaAnterior = quantidadeFaixa;

                #region DI1, DIL, IDI
                if (cdAtivoBMF == "DI1" || cdAtivoBMF == "DIL" || cdAtivoBMF == "IDI")
                {                    
                    if (quantidade <= 100M || semVolume)
                    {
                        custoUnitario = 0.0020M;                        
                    }
                    else if (quantidade <= 1260M)
                    {
                        custoUnitario = 0.0019M;                        
                    }
                    else if (quantidade <= 2800M)
                    {
                        custoUnitario = 0.0017M;                        
                    }
                    else if (quantidade <= 7300M)
                    {
                        custoUnitario = 0.0016M;                        
                    }
                    else if (quantidade <= 47900M)
                    {
                        custoUnitario = 0.0015M;                        
                    }
                    else
                    {
                        custoUnitario = 0.0013M;                        
                    }
                    break;
                }
                #endregion

                #region DDI, SCC, SC3 -> Cupom cambial e swap cambial
                else if (cdAtivoBMF == "DDI" || cdAtivoBMF == "SCC" || cdAtivoBMF == "SC3") //Cupom cambial e swap cambial
                {
                    if (quantidade <= 100M || semVolume)
                    {
                        custoUnitario = 0.0030M;
                    }
                    else if (quantidade <= 1000M)
                    {
                        custoUnitario = 0.0027M;
                    }
                    else if (quantidade <= 1400M)
                    {
                        custoUnitario = 0.0026M;
                    }
                    else if (quantidade <= 3400M)
                    {
                        custoUnitario = 0.0024M;
                    }
                    else if (quantidade <= 14850M)
                    {
                        custoUnitario = 0.0023M;
                    }
                    else
                    {
                        custoUnitario = 0.0021M;
                    }
                }
                #endregion

                #region IAP, IGM -> Taxa de juros indexada à inflação (IPCA e IGPM)
                else if (cdAtivoBMF == "IAP" || cdAtivoBMF == "IGM") //Taxa de juros indexada à inflação (IPCA e IGPM)
                {
                    if (quantidade <= 10M || semVolume)
                    {
                        custoUnitario = 0.0015M;
                    }
                    else if (quantidade <= 50M)
                    {
                        custoUnitario = 0.0014M;
                    }
                    else if (quantidade <= 130M)
                    {
                        custoUnitario = 0.0013M;
                    }
                    else if (quantidade <= 150M)
                    {
                        custoUnitario = 0.0012M;
                    }
                    else if (quantidade <= 300M)
                    {
                        custoUnitario = 0.0011M;
                    }
                    else
                    {
                        custoUnitario = 0.0010M;
                    }
                }
                #endregion

                #region OZ1
                else if (cdAtivoBMF == "OZ1")
                {
                    if (quantidade <= 10M || semVolume)
                    {
                        custoUnitario = 0.58M;
                    }
                    else if (quantidade <= 50M)
                    {
                        custoUnitario = 0.55M;
                    }
                    else if (quantidade <= 130M)
                    {
                        custoUnitario = 0.52M;
                    }
                    else if (quantidade <= 150M)
                    {
                        custoUnitario = 0.49M;
                    }
                    else if (quantidade <= 300M)
                    {
                        custoUnitario = 0.46M;
                    }
                    else
                    {
                        custoUnitario = 0.41M;
                    }
                }
                #endregion

                #region IND, WIN
                else if (cdAtivoBMF == "IND" || cdAtivoBMF == "WIN")
                {
                    if (quantidadeAcumulada <= 10M || semVolume)
                    {
                        custoUnitario = 1.75M;
                        quantidadeFaixa = 10M;
                    }
                    else if (quantidadeAcumulada <= 50M)
                    {
                        custoUnitario = 1.57M;
                        quantidadeFaixa = 50M;                        
                    }
                    else if (quantidadeAcumulada <= 100M)
                    {
                        custoUnitario = 1.49M;
                        quantidadeFaixa = 100M;                        
                    }
                    else if (quantidadeAcumulada <= 190M)
                    {
                        custoUnitario = 1.40M;
                        quantidadeFaixa = 190M;                        
                    }
                    else if (quantidadeAcumulada <= 300M)
                    {
                        custoUnitario = 1.31M;
                        quantidadeFaixa = 300M;                        
                    }
                    else
                    {
                        custoUnitario = 1.22M;
                        quantidadeFaixa = 999999999999M;
                    }                    
                }
                #endregion

                #region DOL, DLA, WDL
                else if (cdAtivoBMF == "DOL" || cdAtivoBMF == "DLA" || cdAtivoBMF == "WDL")
                {
                    if (quantidadeAcumulada <= 10M || semVolume)
                    {
                        custoUnitario = 1.03M;
                        quantidadeFaixa = 10M;
                    }
                    else if (quantidadeAcumulada <= 150M)
                    {
                        custoUnitario = 0.98M;
                        quantidadeFaixa = 150M;
                    }
                    else if (quantidadeAcumulada <= 360M)
                    {
                        custoUnitario = 0.88M;
                        quantidadeFaixa = 360M;
                    }
                    else if (quantidadeAcumulada <= 1500M)
                    {
                        custoUnitario = 0.82M;
                        quantidadeFaixa = 1500M;
                    }
                    else if (quantidadeAcumulada <= 12500M)
                    {
                        custoUnitario = 0.77M;
                        quantidadeFaixa = 12500M;
                    }
                    else
                    {
                        custoUnitario = 0.67M;
                        quantidadeFaixa = 999999999999M;
                    }
                }
                #endregion

                #region EUR, EBR
                else if (cdAtivoBMF == "EUR" || cdAtivoBMF == "EBR")
                {
                    if (quantidade <= 20M || semVolume)
                    {
                        custoUnitario = 1.03M;
                    }
                    else if (quantidade <= 50M)
                    {
                        custoUnitario = 0.98M;
                    }
                    else if (quantidade <= 130M)
                    {
                        custoUnitario = 0.93M;
                    }
                    else if (quantidade <= 150M)
                    {
                        custoUnitario = 0.88M;
                    }
                    else if (quantidade <= 1000M)
                    {
                        custoUnitario = 0.82M;
                    }
                    else
                    {
                        custoUnitario = 0.67M;
                    }
                }
                #endregion

                #region ISU (Acucar)
                else if (cdAtivoBMF == "ISU") //Acucar
                {
                    if (quantidade <= 5M || semVolume)
                    {
                        custoUnitario = 0.61M;
                    }
                    else if (quantidade <= 25M)
                    {
                        custoUnitario = 0.58M;
                    }
                    else if (quantidade <= 65M)
                    {
                        custoUnitario = 0.55M;
                    }
                    else if (quantidade <= 75M)
                    {
                        custoUnitario = 0.52M;
                    }
                    else if (quantidade <= 100M)
                    {
                        custoUnitario = 0.49M;
                    }
                    else
                    {
                        custoUnitario = 0.46M;
                    }
                }
                #endregion

                #region BGI (Boi Gordo)
                else if (cdAtivoBMF == "BGI") //Boi Gordo
                {
                    if (quantidade <= 5M || semVolume)
                    {
                        custoUnitario = 2.40M;
                    }
                    else if (quantidade <= 10M)
                    {
                        custoUnitario = 2.28M;
                    }
                    else if (quantidade <= 20M)
                    {
                        custoUnitario = 2.16M;
                    }
                    else if (quantidade <= 30M)
                    {
                        custoUnitario = 2.04M;
                    }
                    else if (quantidade <= 150M)
                    {
                        custoUnitario = 1.89M;
                    }
                    else
                    {
                        custoUnitario = 1.76M;
                    }
                }
                #endregion

                #region ICF (Cafe)
                else if (cdAtivoBMF == "ICF") //Cafe
                {
                    if (quantidadeAcumulada <= 5M || semVolume)
                    {
                        custoUnitario = 0.66M;
                        quantidadeFaixa = 5M;
                    }
                    else if (quantidadeAcumulada <= 10M)
                    {
                        custoUnitario = 0.63M;
                        quantidadeFaixa = 10M;
                    }
                    else if (quantidadeAcumulada <= 20M)
                    {
                        custoUnitario = 0.59M;
                        quantidadeFaixa = 20M;
                    }
                    else if (quantidadeAcumulada <= 100M)
                    {
                        custoUnitario = 0.56M;
                        quantidadeFaixa = 100M;
                    }
                    else if (quantidadeAcumulada <= 200M)
                    {
                        custoUnitario = 0.53M;
                        quantidadeFaixa = 200M;
                    }
                    else
                    {
                        custoUnitario = 0.46M;
                        quantidadeFaixa = 999999999999M;
                    }
                }
                #endregion

                #region ETN (Etanol)
                else if (cdAtivoBMF == "ETN") //Etanol
                {
                    if (quantidade <= 5M || semVolume)
                    {
                        custoUnitario = 1.68M;
                    }
                    else if (quantidade <= 25M)
                    {
                        custoUnitario = 1.60M;
                    }
                    else if (quantidade <= 65M)
                    {
                        custoUnitario = 1.51M;
                    }
                    else if (quantidade <= 75M)
                    {
                        custoUnitario = 1.43M;
                    }
                    else if (quantidade <= 100M)
                    {
                        custoUnitario = 1.34M;
                    }
                    else
                    {
                        custoUnitario = 1.26M;
                    }
                }
                #endregion

                #region CCM (Milho)
                else if (cdAtivoBMF == "CCM") //Milho
                {
                    if (quantidade <= 5M || semVolume)
                    {
                        custoUnitario = 0.15M;
                    }
                    else if (quantidade <= 20M)
                    {
                        custoUnitario = 0.14M;
                    }
                    else if (quantidade <= 30M)
                    {
                        custoUnitario = 0.13M;
                    }
                    else if (quantidade <= 60M)
                    {
                        custoUnitario = 0.12M;
                    }
                    else if (quantidade <= 200M)
                    {
                        custoUnitario = 0.11M;
                    }
                    else
                    {
                        custoUnitario = 0.10M;
                    }
                }
                #endregion

                #region SOJ (Soja)
                else if (cdAtivoBMF == "SOJ") //Soja
                {
                    if (quantidade <= 5M || semVolume)
                    {
                        custoUnitario = 0.13M;
                    }
                    else if (quantidade <= 20M)
                    {
                        custoUnitario = 0.12M;
                    }
                    else if (quantidade <= 30M)
                    {
                        custoUnitario = 0.11M;
                    }
                    else if (quantidade <= 60M)
                    {
                        custoUnitario = 0.10M;
                    }
                    else if (quantidade <= 200M)
                    {
                        custoUnitario = 0.09M;
                    }
                    else
                    {
                        custoUnitario = 0.08M;
                    }
                }
                #endregion

                if (semVolume)
                {
                    break;
                }

                quantidadeAdicional = quantidadeFaixa - quantidadeFaixaAnterior;

                if (quantidadeAdicional > quantidadeResidual)
                {
                    quantidadeAdicional = quantidadeResidual;
                }

                quantidadeResidual -= quantidadeAdicional;
                quantidadeAcumulada += quantidadeAdicional + 0.01M; //Soma o 0,01 para forçar a passagem de faixa
                valorPonderado += Math.Round(quantidadeAdicional * custoUnitario, 2);
            }
            
            decimal cotacaoMoeda = 1;
            #region Converte pela ptax ou pelo euro para ativos em moeda
            if (cdAtivoBMF == "DOL" || cdAtivoBMF == "DLA" || cdAtivoBMF == "WDL" ||
                cdAtivoBMF == "EUR" || cdAtivoBMF == "EBR")
            {
                DateTime primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                DateTime ultimoDiaMesAnterior = Calendario.SubtraiDiaUtil(primeiroDiaMes, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                CotacaoIndice cotacaoIndice = new CotacaoIndice();

                if (cdAtivoBMF == "DOL" || cdAtivoBMF == "DLA" || cdAtivoBMF == "WDL")
                {
                    cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.PTAX_800VENDA, ultimoDiaMesAnterior);
                }
                else
                {
                    cotacaoIndice.BuscaCotacaoIndice(ListaIndiceFixo.EURO, ultimoDiaMesAnterior);
                }

                cotacaoMoeda = cotacaoIndice.Valor.Value;
            }
            #endregion

            if (cdAtivoBMF == "DI1")
            {
                custoUnitarioFinal = custoUnitario; //Pega o custo unitário direto
            }
            else if (semVolume)
            {
                custoUnitarioFinal = Utilitario.Truncate(custoUnitario * cotacaoMoeda, 2); //Pega o custo da 1a faixa de cada ativo
            }
            else
            {
                if (cotacaoMoeda != 1)
                {
                    custoUnitarioFinal = Utilitario.Truncate(valorPonderado / quantidade, 2);
                    custoUnitarioFinal = Utilitario.Truncate(custoUnitarioFinal * cotacaoMoeda, 2);
                }
                else
                {
                    custoUnitarioFinal = Utilitario.Truncate(valorPonderado / quantidade * cotacaoMoeda, 2);
                }
            }

            #region Mini contratos têm redução sobre o contrato principal
            if (cdAtivoBMF == "WIN")
            {
                custoUnitarioFinal = Utilitario.Truncate(custoUnitarioFinal - custoUnitarioFinal * 0.88M, 2);
            }
            else if (cdAtivoBMF == "WDL")
            {
                custoUnitarioFinal = Utilitario.Truncate(custoUnitarioFinal - custoUnitarioFinal * 0.91M, 2);
            }
            #endregion

            if (cdAtivoBMF != "DI1")
            {
                custoUnitarioFinal = Utilitario.Truncate(custoUnitarioFinal, 2);
            }

            return custoUnitarioFinal;
        }
    }
}