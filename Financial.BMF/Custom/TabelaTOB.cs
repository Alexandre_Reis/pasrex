﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.BMF.Exceptions;
using log4net;
using Financial.BMF.Enums;

namespace Financial.BMF
{
	public partial class TabelaTOB : esTabelaTOB
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaTOB));

        /// <summary>
        /// Carrega o objeto TabelaTOB com os campos Tob, TOBDayTrade, TOBMinima, TOBDayTradeMinima, 
        /// TOBExercicio, TOBExercicioCasado.        
        /// Filtra DataReferencia menor ou igual a dataReferencia.
        /// Ordenado decrescente pela DataReferencia.
        /// TabelaTOBNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="dataReferencia"></param>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="tipoMercado"></param>
        public void BuscaTabelaTOB(DateTime dataReferencia, string cdAtivoBMF, int tipoMercado)
        {
            TabelaTOBCollection tabelaTOBCollection = new TabelaTOBCollection();

            tabelaTOBCollection.Query
                 .Select(tabelaTOBCollection.Query.Tob, tabelaTOBCollection.Query.TOBDayTrade,
                         tabelaTOBCollection.Query.TOBMinima, tabelaTOBCollection.Query.TOBDayTradeMinima,
                         tabelaTOBCollection.Query.TOBExercicio, tabelaTOBCollection.Query.TOBExercicioCasado)
                 .Where(tabelaTOBCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                        tabelaTOBCollection.Query.TipoMercado == tipoMercado,
                        tabelaTOBCollection.Query.DataReferencia.LessThanOrEqual(dataReferencia))
                 .OrderBy(tabelaTOBCollection.Query.DataReferencia.Descending);

            tabelaTOBCollection.Query.Load();

            if (tabelaTOBCollection.HasData)
            {
                // Copia informações de perfilCorretagemBMFCollection para PerfilCorretagemBMF
                this.AddNew();
                this.Tob = ((TabelaTOB)tabelaTOBCollection[0]).Tob;
                this.TOBDayTrade = ((TabelaTOB)tabelaTOBCollection[0]).TOBDayTrade;
                this.TOBMinima = ((TabelaTOB)tabelaTOBCollection[0]).TOBMinima;
                this.TOBDayTradeMinima = ((TabelaTOB)tabelaTOBCollection[0]).TOBDayTradeMinima;
                this.TOBExercicio = ((TabelaTOB)tabelaTOBCollection[0]).TOBExercicio;
                this.TOBExercicioCasado = ((TabelaTOB)tabelaTOBCollection[0]).TOBExercicioCasado;                
            }
            else
            {
                string tipoMercadoDescricao = "";
                switch (tipoMercado)
                {
                    case (byte)TipoMercadoBMF.Disponivel: tipoMercadoDescricao = "Disponível";
                        break;
                    case (byte)TipoMercadoBMF.Futuro: tipoMercadoDescricao = "Futuro";
                        break;
                    case (byte)TipoMercadoBMF.OpcaoDisponivel: tipoMercadoDescricao = "Opção s/ Disponível";
                        break;
                    case (byte)TipoMercadoBMF.OpcaoFuturo: tipoMercadoDescricao = "Opção s/ Futuro";
                        break;                    
                }

                throw new TabelaTOBNaoCadastradoException("Tabela TOB não cadastrada para o ativo " + cdAtivoBMF + ", mercado " + tipoMercadoDescricao);
            }
        }

	}
}
