﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.BMF.Enums;
using log4net;

namespace Financial.BMF
{
	public partial class OrdemBMFCollection : esOrdemBMFCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(OrdemBMFCollection));

        /// <summary>
        /// Atualiza a quantidade daytrade de todas as ordens filtradas por cliente e data,
        /// com a quantidade passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="quantidadeDaytrade"></param>
        public void AtualizaOrdemBMF(int idCliente, DateTime data, int quantidadeDaytrade)
        {

            #region logEntrada
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            OrdemBMF ordemBMF = new OrdemBMF();
            this.BuscaOrdemBMF(idCliente, data);

            for (int i = 0; i < this.Count; i++)
            {
                ordemBMF = (OrdemBMF)this[i];
                ordemBMF.QuantidadeDayTrade = quantidadeDaytrade;
            }
            this.Save();

            #region logSaida
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

        }

        /// <summary>
        /// Carrega o objeto OrdemBMFCollection com os campos IdOrdem, Quantidade, QuantidadeDayTrade, 
        /// IdAgenteCorretora, IdAgenteLiquidacao, CdAtivoBMF, Serie.
        /// Busca operações apenas de venda (ExercicioOpcaoCompra e ExercicioOpcaoVenda não entram)
        /// ordenadas por AgenteCorretora, AgenteLiquidacao, CdAtivoBMF, Serie, IdOrdem.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaOrdemBMFVenda(int idCliente, DateTime data)
        {
            this.QueryReset();            
            this.Query
                 .Select(this.Query.IdOrdem, this.Query.Quantidade, this.Query.QuantidadeDayTrade,
                        this.Query.IdAgenteCorretora, this.Query.IdAgenteLiquidacao, this.Query.CdAtivoBMF,
                        this.Query.Serie, this.Query.PontaEstrategia)
                 .Where(this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOrdem == TipoOrdemBMF.Venda,
                        this.Query.Origem.NotEqual(OrigemOrdemBMF.ExercicioOpcaoCompra),
                        this.Query.Origem.NotEqual(OrigemOrdemBMF.ExercicioOpcaoVenda))
                 .OrderBy(this.Query.IdAgenteCorretora.Ascending,
                          this.Query.IdAgenteLiquidacao.Ascending,
                          this.Query.CdAtivoBMF.Ascending,
                          this.Query.Serie.Ascending,
                          this.Query.PontaEstrategia.Ascending,
                          this.Query.IdOrdem.Ascending);

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto OrdemBMFCollection com os campos IdOrdem, Quantidade, QuantidadeDayTrade, 
        /// IdAgenteCorretora, IdAgenteLiquidacao, CdAtivoBMF, Serie.
        /// Busca operações apenas de compra (ExercicioOpcaoCompra e ExercicioOpcaoVenda não entram)
        /// ordenadas por IdOrdem.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="idAgenteLiquidacao"></param>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="serie"></param>
        /// <param name="consideraAgentes">Se true, filtra pelos agentes na query</param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaOrdemBMFCompra(int idCliente, DateTime data, int idAgenteCorretora,
                                        int idAgenteLiquidacao, string cdAtivoBMF, string serie,
                                        short pontaEstrategia, bool consideraAgentes)
        {
            this.QueryReset();
            this.Query.Select(this.Query.IdOrdem, this.Query.Quantidade, this.Query.QuantidadeDayTrade,
                              this.Query.IdAgenteCorretora, this.Query.IdAgenteLiquidacao, this.Query.CdAtivoBMF,
                              this.Query.Serie);
            this.Query.Where(this.Query.Data.Equal(data) &
                             this.Query.IdCliente == idCliente &
                             this.Query.TipoOrdem == TipoOrdemBMF.Compra &
                             this.Query.Origem.NotEqual(OrigemOrdemBMF.ExercicioOpcaoCompra) &
                             this.Query.Origem.NotEqual(OrigemOrdemBMF.ExercicioOpcaoVenda) &
                             this.Query.CdAtivoBMF == cdAtivoBMF &
                             this.Query.Serie == serie &
                             this.Query.PontaEstrategia == pontaEstrategia);

            if (consideraAgentes)
                this.Query.Where(this.Query.IdAgenteCorretora.Equal(idAgenteCorretora) & this.Query.IdAgenteLiquidacao.Equal(idAgenteLiquidacao));

            this.Query.OrderBy(this.Query.IdOrdem.Ascending);

            bool retorno = this.Query.Load();

            return retorno;            
        }

        /// <summary>
        /// Carrega o objeto OrdemBMFCollection com os campos IdOrdem, Quantidade, QuantidadeDayTrade, 
        /// IdTrader, CdAtivoBMF, Serie.
        /// Busca operações apenas de venda (ExercicioOpcaoCompra e ExercicioOpcaoVenda não entram)
        /// ordenadas por IdTrader, CdAtivoBMF, Serie, IdOrdem.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaOrdemBMFVendaGerencial(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOrdem, this.Query.Quantidade, this.Query.QuantidadeDayTrade,
                        this.Query.IdTrader, this.Query.CdAtivoBMF,
                        this.Query.Serie)
                 .Where(this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOrdem == TipoOrdemBMF.Venda,
                        this.Query.Origem.NotEqual(OrigemOrdemBMF.ExercicioOpcaoCompra),
                        this.Query.Origem.NotEqual(OrigemOrdemBMF.ExercicioOpcaoVenda),
                        this.Query.IdTrader.IsNotNull())
                 .OrderBy(this.Query.IdTrader.Ascending,
                          this.Query.CdAtivoBMF.Ascending,
                          this.Query.Serie.Ascending,
                          this.Query.IdOrdem.Ascending);

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto OrdemBMFCollection com os campos IdOrdem, Quantidade, QuantidadeDayTrade, 
        /// IdTrader, CdAtivoBMF, Serie.
        /// Busca operações apenas de compra (ExercicioOpcaoCompra e ExercicioOpcaoVenda não entram)
        /// ordenadas por IdOrdem.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="idTrader"></param>
        /// <param name="cdAtivoBMF"></param>
        /// <param name="serie"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaOrdemBMFCompraGerencial(int idCliente, DateTime data, int idTrader, string cdAtivoBMF, string serie)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOrdem, this.Query.Quantidade, this.Query.QuantidadeDayTrade,
                          this.Query.IdTrader, this.Query.CdAtivoBMF,
                          this.Query.Serie)
                 .Where(this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOrdem == TipoOrdemBMF.Compra,
                        this.Query.Origem.NotEqual(OrigemOrdemBMF.ExercicioOpcaoCompra),
                        this.Query.Origem.NotEqual(OrigemOrdemBMF.ExercicioOpcaoVenda),
                        this.Query.IdTrader == idTrader,
                        this.Query.CdAtivoBMF == cdAtivoBMF,
                        this.Query.Serie == serie)
                 .OrderBy(this.Query.IdOrdem.Ascending);

            bool retorno = this.Query.Load();
                       
            return retorno;            
        }

        /// <summary>
        /// Carrega o objeto OrdemBMFCollection com os campos IdOrdem, Quantidade, QuantidadeDayTrade,
        /// IdAgenteCorretora, IdAgenteLiquidacao, CdAtivoBMF, Serie.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaOrdemBMF(int idCliente, DateTime dataOperacao)
        {
            // TODO log da funcao

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOrdem, this.Query.Quantidade, this.Query.QuantidadeDayTrade,
                        this.Query.IdAgenteCorretora, this.Query.IdAgenteLiquidacao, this.Query.CdAtivoBMF,
                        this.Query.Serie)
                 .Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente);
                                 
            bool retorno = this.Query.Load();

            #region logSql

            if (log.IsInfoEnabled) {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@Data1", "'" + dataOperacao.ToString("yyyy-MM-dd") + "'");
                sql = sql.Replace("@IdCliente2", "'" + idCliente + "'");

                log.Info(sql);
            }
            #endregion	         

            return retorno;
            //TODO logar this.Query.es.LastQuery;
        }

        /// <summary>
        /// Carrega o objeto OrdemBMFCollection com todos os campos de OrdemBMF.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaOrdemBMFCompleta(int idCliente, DateTime dataOperacao)
        {
            // TODO log da funcao

            this.QueryReset();
            this.Query.Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente);

            bool retorno = this.Query.Load();
            
            #region logSql
            if (log.IsInfoEnabled) {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@Data1", "'" + dataOperacao.ToString("yyyy-MM-dd") + "'");
                sql = sql.Replace("@IdCliente2", "'" + idCliente + "'");

                log.Info(sql);
            }
            #endregion	         

            return retorno;
            //TODO logar this.Query.es.LastQuery;
        }

        /// <summary>
        /// Deleta todas as ordens de BMF do dia, que tenham sido integradas pelo arquivo RCOMIESP.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        /// <param name="fonte"></param>        
        public void DeletaOrdemBMFRComiesp(int idCliente, int idAgenteCorretora, DateTime data)
        {

            #region logEntrada
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
            
            this.QueryReset();
            this.Query
              .Select(this.Query.IdOrdem)
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.IdAgenteCorretora == idAgenteCorretora,
                     this.Query.Data.Equal(data),
                     this.Query.Fonte == FonteOrdemBMF.ArquivoRCOMIESP);

            this.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled) {
                string sql = "Delete: " +this.Query.es.LastQuery;
                sql = sql.Replace("@IdCliente1", "'" + idCliente + "'");
                sql = sql.Replace("@IdAgenteCorretora2", "'" + idAgenteCorretora + "'");
                sql = sql.Replace("@Data3", "'" + data + "'");
                log.Info(sql);
            }
            #endregion

            this.MarkAllAsDeleted();
            this.Save();

            #region logSaida
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
        }

        /// <summary>
        /// Carrega o objeto OrdemBMFCollection com todos os campos de OrdemBMF.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <param name="cdAtivoBMF"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaOrdemBMF(int idCliente, DateTime dataOperacao, string cdAtivoBMF)
        {
            // TODO log da funcao

            this.QueryReset();
            this.Query
                 .Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBMF.In(cdAtivoBMF));

            bool retorno = this.Query.Load();

            #region logSql

            if (log.IsInfoEnabled)
            {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@Data1", "'" + dataOperacao.ToString("yyyy-MM-dd") + "'");
                sql = sql.Replace("@IdCliente2", "'" + idCliente + "'");

                log.Info(sql);
            }
            #endregion

            return retorno;
            //TODO logar this.Query.es.LastQuery;
        }
	}
}
