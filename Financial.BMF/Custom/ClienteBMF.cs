﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.BMF.Exceptions;
using log4net;

namespace Financial.BMF
{
	public partial class ClienteBMF : esClienteBMF
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(ClienteBMF));

        /// <summary>
        /// Carrega o objeto ClienteBMF com os campos Socio, InvestidorInstitucional.
        /// ClienteBMFNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public void BuscaSocioInvestidor(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.query.Socio, this.query.InvestidorInstitucional)
                 .Where(this.query.IdCliente == idCliente);

            this.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled)
            {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@IdCliente1", "'" + idCliente + "'");                
                log.Info(sql);
            }
            #endregion

            if (!this.es.HasData)
            {
                throw new ClienteBMFNaoCadastradoException("ClienteBMF " + idCliente + " não cadastrado.");            
            }            
        }

        /// <summary>
        /// Retorna bool indicando se o clienteBMF é sócio da BMF.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public bool IsSocio()
        {
            return this.Socio == "S";
        }

        /// <summary>
        /// Retorna bool indicando se o clienteBMF é Investidor Institucional da BMF.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public bool IsInvestidorInstitucional()
        {
            return this.InvestidorInstitucional == "S";
        }

	}
}
