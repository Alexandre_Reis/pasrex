using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.BMF
{
	public partial class CustodiaBMF : esCustodiaBMF
	{
        /// <summary>
        /// Carrega o objeto CustodiaBMF com os campos ValorAcumulado, ValorDiario, DataPagamento.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBMF"></param>        
        /// <param name="idAgente"></param>        
        /// <param name="data"></param>        
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaCustodiaBMF(int idCliente, string cdAtivoBMF, int idAgente, DateTime data, int? idConta)
        {            
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorAcumulado, this.Query.ValorDiario, this.Query.DataPagamento)
                 .Where(this.Query.IdCliente.Equal(idCliente), 
                        this.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                        this.Query.IdAgente.Equal(idAgente), 
                        this.Query.Data.Equal(data));

            if (idConta.HasValue && idConta.Value > 0)
            {
                this.Query.Where(this.Query.IdConta == idConta.Value);
            }

            this.Query.Load();
            
            return this.es.HasData;            
        }
	}
}
