﻿// The following code was generated by Microsoft Visual Studio 2005.
// The test owner should check each test for validity.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using System.Collections.Generic;
using Financial.Common;
using System.Data;
namespace Test.Financial.Common {
    /// <summary>
    ///This is a test class for Financial.Common.AgenteMercadoCollection and is intended
    ///to contain all Financial.Common.AgenteMercadoCollection Unit Tests
    ///</summary>
    [TestClass()]
    public class AgenteMercadoCollectionTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }
        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        }

        //
        //Use ClassCleanup to run code after all tests in a class have run
        //
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for BuscaAgenteMercado ()
        ///</summary>
        [TestMethod()]
        public void BuscaAgenteMercadoTest() {
            AgenteMercadoCollection target = new AgenteMercadoCollection();

            DataSet expected = null;
            DataSet actual;

            actual = target.BuscaAgenteMercado();

            Assert.AreEqual(expected, actual, "Financial.Common.AgenteMercadoCollection.BuscaAgenteMercado did not return the ex" +
                    "pected value.");
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

    }


}
