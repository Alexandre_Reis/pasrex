﻿// The following code was generated by Microsoft Visual Studio 2005.
// The test owner should check each test for validity.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using System.Collections.Generic;
using Financial.CRM;
namespace Test.Financial.CRM {
    /// <summary>
    ///This is a test class for Financial.CRM.PessoaEndereco and is intended
    ///to contain all Financial.CRM.PessoaEndereco Unit Tests
    ///</summary>
    [TestClass()]
    public class PessoaEnderecoTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }
        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        }
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for EnderecoCompleto
        ///</summary>
        [TestMethod()]
        public void EnderecoCompletoTest1() {
            PessoaEndereco target = new PessoaEndereco();
            target.LoadByPrimaryKey(1, 1);
            string enderecoCompletoEsperado = "Rua Joao Duque, 555 Casa 3 - Campinas - SP";
            //
            Assert.AreEqual(enderecoCompletoEsperado, target.EnderecoCompleto, "Endereço Completo Incorreto");
        }
    }
}
