﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Configuration;
using RoboImportaTesouro.Properties;
using Financial.Util;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Common;
using EntitySpaces.Interfaces;
using log4net;
using log4net.Config;
using Financial.RendaFixa;
using Financial.Common.Enums;
using Financial.Interfaces.Import.RendaFixa;
using System.Net.Mail;
using System.Net.Configuration;


namespace RoboImportaTesouro
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();

            string timing = "D0";
            if (!String.IsNullOrEmpty(Settings.Default.Timing))
            {
                timing = Settings.Default.Timing;
            }

            DateTime dataAtual = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime data = dataAtual;
            
            string BDs = Settings.Default.BDs;
            string[] listaBDs = BDs.Split(new Char[] { ';' });
            string server = Settings.Default.Server;
            string login = Settings.Default.Login;
            string senha = Settings.Default.Senha;
                        
            bool erro = false;
            int i = 0;
            foreach (string BD in listaBDs)
            {

                string BDAjustado = BD.Replace("\r\n", string.Empty).Trim();
                                
                esConfigSettings.ConnectionInfo.Connections.Clear();

                esConnectionElement connSQL = new esConnectionElement();
                connSQL.Name = "Financial"+i;
                connSQL.ProviderMetadataKey = "esDefault";
                connSQL.SqlAccessType = esSqlAccessType.DynamicSQL;
                connSQL.Provider = "EntitySpaces.SqlClientProvider";
                connSQL.ProviderClass = "DataProvider";
                connSQL.ConnectionString = @"" + "Data Source=" + server + ";Initial Catalog=" + BDAjustado + ";User ID=" + login + ";Password=" + senha + ";" + "";
                connSQL.DatabaseVersion = "2005";
                esConfigSettings.ConnectionInfo.Connections.Add(connSQL);

                esConfigSettings.ConnectionInfo.Default = "Financial"+i;
                i++;

                DateTime dataReferencia = data;
                if (timing != "D0")
                {
                    int numeroDias = Convert.ToInt32(timing.Replace("D", "").Trim());
                    dataReferencia = Calendario.SubtraiDiaUtil(data, numeroDias);                    
                }

                if (!Calendario.IsDiaUtil(data))
                {
                    return;
                }

                if (BDAjustado != "")
                {
                    
                    if (!ProcessaCarga(BDAjustado, dataReferencia))
                    {
                        erro = true;
                    }

                    //se timing D1 fez D-menos-1 e agora faz D-zero
                    if (timing == "D1")
                    {
                        dataReferencia = Calendario.AdicionaDiaUtil(dataReferencia, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        if (!ProcessaCarga(BDAjustado, dataReferencia))
                        {
                            erro = true;
                        }
                    }

                }
            }            

            if (!erro)
            {
                EnviaEmail("Carga do robô executada com sucesso!");
            }
            
        }      
                

        private static bool ProcessaCarga(string BD, DateTime data)
        { 
            esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();

            string path = Settings.Default.Downloads;            

            bool isFeriadoBovespa = Calendario.IsFeriado(data, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            bool indicaErro = false;
            StringBuilder mensagemErro = new StringBuilder();
            
            if (Calendario.IsDiaUtil(data))
            {
                if (log.IsDebugEnabled)
                {
                    log.Debug("Status de Importação: " + data.ToShortDateString());
                    log.Debug("BD: " + BD + "\n");
                }
                                    
                mensagemErro.AppendLine();


                if (!isFeriadoBovespa)
                {

                    #region CotacaoTesouro

                    DateTime dataInicio = DateTime.Now;
                   
                    //
                    CotacaoMercadoTesouro cotacaoMercadoTesouro = new CotacaoMercadoTesouro();
                    // 
                    try
                    {
                        cotacaoMercadoTesouro.CarregaCotacaoTesouro(data, path);

                        if (log.IsDebugEnabled)
                        {
                            log.Debug("CotacaoTesouroDireto Importado: " + data.ToShortDateString());
                        }
                    }
                    catch (Exception e1)
                    {
                        indicaErro = true;
                        //
                        string msg = "CotacaoTesouroDireto com problemas \n";
                        msg += " - Dados não Importados: " + e1.Message;
                        //
                        mensagemErro.AppendLine("\t" + msg);
                    }

                    #region Log do Processo
                    if (!indicaErro)
                    {
                        DateTime dataFim = DateTime.Now;
                        //
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.Descricao = "Importação CotacaoTesouro - " + data.ToShortDateString();
                        historicoLog.Login = "RoboImportaTesouro";
                        historicoLog.Maquina = Utilitario.GetLocalIp();
                        historicoLog.Cultura = "";
                        historicoLog.DataInicio = dataInicio;
                        historicoLog.DataFim = dataFim;
                        historicoLog.Origem = (int)HistoricoLogOrigem.Outros;
                        historicoLog.Save();
                        //
                    }
                    #endregion

                    #endregion
                   

                }                
                
            }            
           
            if (indicaErro)
            {
                log.Debug(mensagemErro);
                EnviaEmail("BD: " + BD + "\r\n" + mensagemErro.ToString());
            }
            else
            {
                log.Debug("\n");
            }

            bool ok = !indicaErro;

            return ok;

        }

        private static void EnviaEmail(string mensagemErro)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

            string in_from = settings.Smtp.From.ToString();
            string stringEmail = Settings.Default.EmailTo;

            if (stringEmail != "")
            {
                stringEmail = stringEmail.Replace(";", ",");
                string[] emails = stringEmail.Split(new Char[] { ',' });

                foreach (string email in emails)
                {
                    MailAddress from = new MailAddress(in_from);
                    MailAddress to = new MailAddress(email);

                    MailMessage msg = new MailMessage(from, to);
                    msg.From = from;

                    msg.IsBodyHtml = false;

                    msg.Subject = "Carga diária Robô Financial";
                    msg.Body = mensagemErro;

                    SmtpClient smtp = new SmtpClient();

                    if (settings.Smtp.From.Contains("@gmail"))
                    {
                        smtp.EnableSsl = true;
                    }

                    try
                    {
                        smtp.Send(msg);    
                    }
                    catch (Exception e1)
                    {
                        string msgErro = "Erro ao enviar Email\n" + e1.Message;
                        //
                        mensagemErro += "\t" + msgErro;

                        log.Debug(mensagemErro);
                    }           
                    
                }
            }
        }
        
    }
}
