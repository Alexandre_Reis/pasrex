using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string cotistaParam = Request["cotista"];
        string usrcodParam = Request["usrcod"];
        if (string.IsNullOrEmpty(cotistaParam) && !string.IsNullOrEmpty(usrcodParam))
        {
            int usrcod = Convert.ToInt32(usrcodParam);
            cotistaParam = "F01" + usrcod;
        }

        string path = "~/Controllers/Application/LoadGlobals.ashx?cotista=" + cotistaParam;
        Page.ClientScript.RegisterClientScriptInclude("Registration", ResolveUrl(path));

        path = "Scripts/app/all-classes.js?v=1.1";
        Page.ClientScript.RegisterClientScriptInclude("Registration1", ResolveUrl(path));

        path = "Scripts/app/Application.js?v=1.1";
        Page.ClientScript.RegisterClientScriptInclude("Registration2", ResolveUrl(path));

    }
}
