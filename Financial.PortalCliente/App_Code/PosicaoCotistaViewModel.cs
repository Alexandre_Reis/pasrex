using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Fundo;
using Financial.CRM;
using Financial.Investidor;

public class PosicaoCotistaViewModel
{
    public int? IdPosicao;
    public int? IdOperacao;
    public int? IdCotista;
    public int? IdCarteira;
    public string CpfcnpjCarteira;
    public decimal? ValorAplicacao;
    public DateTime? DataAplicacao;
    public DateTime? DataConversao;
    public decimal? CotaAplicacao;
    public decimal? CotaDia;
    public decimal? ValorBruto;
    public decimal? ValorLiquido;
    public decimal? QuantidadeInicial;
    public decimal? Quantidade;
    public decimal? QuantidadeBloqueada;
    public DateTime? DataUltimaCobrancaIR;
    public decimal? ValorIR;
    public decimal? ValorIOF;
    public decimal? ValorPerformance;
    public decimal? ValorIOFVirtual;
    public decimal? QuantidadeAntesCortes;
    public decimal? ValorRendimento;
    public DateTime? DataUltimoCortePfee;
    public string PosicaoIncorporada;
    public string NomeCarteira;
    public string ApelidoCarteira;
    public decimal? ValorIRMaio;
    public decimal? ValorIRNovembro;
    public DateTime? DataDia;
    public decimal? ValorLiquidoResgate;

    public PosicaoCotistaViewModel()
    {
    }
    public PosicaoCotistaViewModel(PosicaoCotista posicaoCotista)
    {
        this.CotaAplicacao = posicaoCotista.CotaAplicacao;
        this.CotaDia = posicaoCotista.CotaDia;
        this.DataAplicacao = posicaoCotista.DataAplicacao;
        this.DataConversao = posicaoCotista.DataConversao;
        this.DataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR;
        this.DataUltimoCortePfee = posicaoCotista.DataUltimoCortePfee;
        this.IdCarteira = posicaoCotista.IdCarteira;
        this.IdCotista = posicaoCotista.IdCotista;
        this.IdOperacao = posicaoCotista.IdOperacao;
        this.IdPosicao = posicaoCotista.IdPosicao;
        this.PosicaoIncorporada = posicaoCotista.PosicaoIncorporada;
        this.Quantidade = posicaoCotista.Quantidade;
        this.QuantidadeAntesCortes = posicaoCotista.QuantidadeAntesCortes;
        this.QuantidadeBloqueada = posicaoCotista.QuantidadeBloqueada;
        this.QuantidadeInicial = posicaoCotista.QuantidadeInicial;
        this.ValorAplicacao = posicaoCotista.ValorAplicacao;
        this.ValorBruto = posicaoCotista.ValorBruto;
        this.ValorIOF = posicaoCotista.ValorIOF;
        this.ValorIOFVirtual = posicaoCotista.ValorIOFVirtual;
        this.ValorIR = posicaoCotista.ValorIR;
        this.ValorLiquido = posicaoCotista.ValorLiquido;
        this.ValorPerformance = posicaoCotista.ValorPerformance;
        this.ValorRendimento = posicaoCotista.ValorRendimento;

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(this.IdCarteira.Value);
        this.NomeCarteira = cliente.Nome;
        this.ApelidoCarteira = cliente.Apelido;
        this.DataDia = cliente.DataDia;
        
        Pessoa pessoa = new Pessoa();
        pessoa.LoadByPrimaryKey(this.IdCarteira.Value);
        this.CpfcnpjCarteira = pessoa.Cpfcnpj;

        this.ValorIRMaio = 0;
        this.ValorIRNovembro = 0;
    }

    public PosicaoCotistaViewModel(PosicaoCotistaHistorico posicaoCotista)
    {
        this.CotaAplicacao = posicaoCotista.CotaAplicacao;
        this.CotaDia = posicaoCotista.CotaDia;
        this.DataAplicacao = posicaoCotista.DataAplicacao;
        this.DataConversao = posicaoCotista.DataConversao;
        this.DataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR;
        this.DataUltimoCortePfee = posicaoCotista.DataUltimoCortePfee;
        this.IdCarteira = posicaoCotista.IdCarteira;
        this.IdCotista = posicaoCotista.IdCotista;
        this.IdOperacao = posicaoCotista.IdOperacao;
        this.IdPosicao = posicaoCotista.IdPosicao;
        this.PosicaoIncorporada = posicaoCotista.PosicaoIncorporada;
        this.Quantidade = posicaoCotista.Quantidade;
        this.QuantidadeAntesCortes = posicaoCotista.QuantidadeAntesCortes;
        this.QuantidadeBloqueada = posicaoCotista.QuantidadeBloqueada;
        this.QuantidadeInicial = posicaoCotista.QuantidadeInicial;
        this.ValorAplicacao = posicaoCotista.ValorAplicacao;
        this.ValorBruto = posicaoCotista.ValorBruto;
        this.ValorIOF = posicaoCotista.ValorIOF;
        this.ValorIOFVirtual = posicaoCotista.ValorIOFVirtual;
        this.ValorIR = posicaoCotista.ValorIR;
        this.ValorLiquido = posicaoCotista.ValorLiquido;
        this.ValorPerformance = posicaoCotista.ValorPerformance;
        this.ValorRendimento = posicaoCotista.ValorRendimento;

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(this.IdCarteira.Value);
        this.NomeCarteira = cliente.Nome;
        this.ApelidoCarteira = cliente.Apelido;
        this.DataDia = cliente.DataDia;

        Pessoa pessoa = new Pessoa();
        pessoa.LoadByPrimaryKey(this.IdCarteira.Value);
        this.CpfcnpjCarteira = pessoa.Cpfcnpj;

        this.ValorIRMaio = 0;
        this.ValorIRNovembro = 0;
    }

    public PosicaoCotistaViewModel(PosicaoCotistaAbertura posicaoCotista)
    {
        this.CotaAplicacao = posicaoCotista.CotaAplicacao;
        this.CotaDia = posicaoCotista.CotaDia;
        this.DataAplicacao = posicaoCotista.DataAplicacao;
        this.DataConversao = posicaoCotista.DataConversao;
        this.DataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR;
        this.DataUltimoCortePfee = posicaoCotista.DataUltimoCortePfee;
        this.IdCarteira = posicaoCotista.IdCarteira;
        this.IdCotista = posicaoCotista.IdCotista;
        this.IdOperacao = posicaoCotista.IdOperacao;
        this.IdPosicao = posicaoCotista.IdPosicao;
        this.PosicaoIncorporada = posicaoCotista.PosicaoIncorporada;
        this.Quantidade = posicaoCotista.Quantidade;
        this.QuantidadeAntesCortes = posicaoCotista.QuantidadeAntesCortes;
        this.QuantidadeBloqueada = posicaoCotista.QuantidadeBloqueada;
        this.QuantidadeInicial = posicaoCotista.QuantidadeInicial;
        this.ValorAplicacao = posicaoCotista.ValorAplicacao;
        this.ValorBruto = posicaoCotista.ValorBruto;
        this.ValorIOF = posicaoCotista.ValorIOF;
        this.ValorIOFVirtual = posicaoCotista.ValorIOFVirtual;
        this.ValorIR = posicaoCotista.ValorIR;
        this.ValorLiquido = posicaoCotista.ValorLiquido;
        this.ValorPerformance = posicaoCotista.ValorPerformance;
        this.ValorRendimento = posicaoCotista.ValorRendimento;
        this.ValorLiquidoResgate =this.ValorLiquido;

		
		Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(this.IdCarteira.Value);
        
        this.NomeCarteira = cliente.Nome;
        this.ApelidoCarteira = cliente.Apelido;
        this.DataDia = cliente.DataDia;

        OrdemCotistaQuery ordemQuery = new OrdemCotistaQuery("O");

        ordemQuery.Select(ordemQuery.ValorLiquido, ordemQuery.ValorBruto, ordemQuery.TipoOperacao);
        ordemQuery.Where(ordemQuery.DataOperacao.GreaterThanOrEqual(this.DataDia.Value),
            ordemQuery.TipoOperacao.In((byte)TipoOperacaoCotista.ResgateBruto, (byte)TipoOperacaoCotista.ResgateLiquido, (byte)TipoOperacaoCotista.ResgateTotal),
            ordemQuery.IdCotista.Equal(this.IdCotista.Value),
            ordemQuery.StatusIntegracao.NotIn((byte)StatusOrdemIntegracao.Cancelado, (byte)StatusOrdemIntegracao.OutroErro, (byte)StatusOrdemIntegracao.SemSaldo),
	        ordemQuery.IdCarteira.Equal(this.IdCarteira.Value));

        OrdemCotistaCollection resgatesPendentes = new OrdemCotistaCollection();

        resgatesPendentes.Load(ordemQuery);

        decimal totalResgatesPendentes = 0;

		foreach(OrdemCotista resgatePendente in resgatesPendentes){
			if(resgatePendente.TipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal){
				this.ValorLiquidoResgate = 0;
				break;
			}
			else if(resgatePendente.TipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido){
				this.ValorLiquidoResgate -= resgatePendente.ValorLiquido.Value;
			}
			else if(resgatePendente.TipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto){
				this.ValorLiquidoResgate -= resgatePendente.ValorBruto.Value;
			}
		}
        
        Pessoa pessoa = new Pessoa();
        pessoa.LoadByPrimaryKey(this.IdCarteira.Value);
        this.CpfcnpjCarteira = pessoa.Cpfcnpj;

        this.ValorIRMaio = 0;
        this.ValorIRNovembro = 0;
    }

    public decimal RetornaSaldoDisponivel(int idCarteira, int idCotista)
    {
        decimal saldoDisponivel = 0;

        PosicaoCotistaCollection posicoes = new PosicaoCotistaCollection();
        posicoes.Query.Select(posicoes.Query.Quantidade.Sum(),
                                posicoes.Query.IdCarteira,
                                posicoes.Query.ValorLiquido.Sum()
                              );

        posicoes.Query.Where(
                                           posicoes.Query.IdCotista.Equal(idCotista),
                                           posicoes.Query.IdCarteira.Equal(idCarteira),
                                           posicoes.Query.Quantidade.NotEqual(0));
        posicoes.Query.GroupBy(posicoes.Query.IdCarteira);

        posicoes.Load(posicoes.Query);

        if (posicoes.Count > 0)
        {
            saldoDisponivel = posicoes[0].ValorLiquido.Value;
        }
        return saldoDisponivel;
    }
}

