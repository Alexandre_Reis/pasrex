using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Fundo;
using Financial.Common;
using Financial.Fundo.Enums;
using Financial.Security;

public class CarteiraViewModel
{
    public string Nome;
    public string Apelido;
    public int IdCarteira;
    
    public string Categoria;
    public string GrauRisco;

    public decimal? RetornoMesCorrente;
    public decimal? RetornoUltimoMes;
    public decimal? RetornoAno;
    public decimal? Retorno12Meses;
    public decimal ValorMinimoInicial;
    public decimal ValorMinimoAplicacao;
    public decimal ValorMinimoResgate;
    public decimal ValorMinimoSaldo;
    public int DiasCotizacaoAplicacao;
    public int DiasCotizacaoResgate;
    public int DiasLiquidacaoAplicacao;
    public int DiasLiquidacaoResgate;
    public DateTime? HorarioFim;
    public bool? TemPermissaoAplicar;
    public string LinkLamina;
    public string LinkTermoAdesao;
    public string LinkProspecto;
    public string LinkRegulamento;

    public CarteiraViewModel()
    {
    }
    public CarteiraViewModel(Carteira carteira)
    {

        this.Apelido = carteira.Apelido.Trim();
        this.Nome = carteira.Nome;
        this.IdCarteira = carteira.IdCarteira.Value;

        if (carteira.IdGrauRisco.HasValue)
        {
            GrauRisco grauRisco = new GrauRisco();
            grauRisco.LoadByPrimaryKey(carteira.IdGrauRisco.Value);
            this.GrauRisco = grauRisco.Descricao;
        }

        CategoriaFundo categoriaFundo = new CategoriaFundo();
        categoriaFundo.LoadByPrimaryKey(carteira.IdCategoria.Value);
        this.Categoria = categoriaFundo.Descricao;

        //Patrimonio mais recente
        HistoricoCota historicoCotaMax = new HistoricoCota();
        DateTime? dataMaxCota = historicoCotaMax.BuscaDataMaxCota(carteira.IdCarteira.Value);
        DateTime dataFimCalculoMedida;
        if (dataMaxCota.HasValue)
        {
            historicoCotaMax = new HistoricoCota();
            historicoCotaMax.LoadByPrimaryKey(dataMaxCota.Value, carteira.IdCarteira.Value);
            
            dataFimCalculoMedida = dataMaxCota.Value;
        }
        else
        {
            dataFimCalculoMedida = DateTime.Today;
        }

        CalculoMedida calculoMedida = new CalculoMedida(carteira.IdCarteira.Value, carteira.IdIndiceBenchmark.Value, carteira.DataInicioCota.Value, dataFimCalculoMedida);
        calculoMedida.SetAjustaCota(true);

        try
        {
            this.RetornoMesCorrente = calculoMedida.CalculaRetornoMes(calculoMedida.DataFimJanela.Value);
        }
        catch { }

        try
        {
            this.RetornoUltimoMes = calculoMedida.CalculaRetornoMesFechado(DateTime.Today.AddMonths(-1));
        }
        catch { }

        try
        {
            this.RetornoAno = calculoMedida.CalculaRetornoAno(calculoMedida.DataFimJanela.Value);
        }
        catch { }

        try
        {
            this.Retorno12Meses = calculoMedida.CalculaRetornoPeriodoMes(calculoMedida.DataFimJanela.Value, 12);
        }
        catch { }

        this.ValorMinimoAplicacao = carteira.ValorMinimoAplicacao.Value;
        this.ValorMinimoInicial = carteira.ValorMinimoInicial.Value;
        this.ValorMinimoResgate = carteira.ValorMinimoResgate.Value;
        this.ValorMinimoSaldo = carteira.ValorMinimoSaldo.Value;
        this.DiasCotizacaoAplicacao = carteira.DiasCotizacaoAplicacao.Value;
        this.DiasCotizacaoResgate = carteira.DiasCotizacaoResgate.Value;
        this.DiasLiquidacaoAplicacao = carteira.DiasLiquidacaoAplicacao.Value;
        this.DiasLiquidacaoResgate = carteira.DiasLiquidacaoResgate.Value;
        if (carteira.HorarioFim.HasValue)
        {
            this.HorarioFim = carteira.HorarioFim.Value;
        }
        
        string diretorioFundo = String.Format(@"Fundo{0}", this.IdCarteira);
        this.LinkLamina = String.Format(@"\{0}\Lamina.pdf", diretorioFundo);
        this.LinkProspecto = String.Format(@"\{0}\Prospecto.pdf", diretorioFundo);
        this.LinkTermoAdesao = String.Format(@"\{0}\TermoAdesao.pdf", diretorioFundo);
        this.LinkRegulamento = String.Format(@"\{0}\Regulamento.pdf", diretorioFundo);

    }

    public void VerificaPermissaoAplicarFundo(int idCotista)
    {
        PermissaoOperacaoFundo permissaoOperacaoFundo = new PermissaoOperacaoFundo();
        this.TemPermissaoAplicar = permissaoOperacaoFundo.LoadByPrimaryKey(idCotista, this.IdCarteira);

    }
}