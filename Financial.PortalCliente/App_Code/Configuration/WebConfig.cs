﻿using System;
using System.Data;
using System.Web.Configuration;
using System.Web;
using System.Configuration;
using Financial.Util;

namespace Financial.WebConfigConfiguration {

    /// <summary>
    /// Summary description for WebConfig
    /// </summary>
    public static class WebConfig {
        public class AppSettings {

            /// <summary>
            /// Define se usa Formato de Número Negativo nos Relatórios
            /// </summary>
            public static string RelatoriosFormatoNumeroNegativo {
                get { return WebConfigurationManager.AppSettings["RelatoriosFormatoNumeroNegativo"]; }
            }

            /// <summary>
            /// Define se usa Cor Vermelha nos Números Negativos de Relatorio
            /// </summary>
            public static string RelatoriosCorVermelha {
                get { return WebConfigurationManager.AppSettings["RelatoriosCorVermelha"]; }
            }

            /// <summary>
            /// Define a Fonte usada no relatorio
            /// </summary>
            public static string TipoFonteRelatorio {
                get { return WebConfigurationManager.AppSettings["TipoFonteRelatorio"]; }
            }

            /// <summary>
            /// Usado em casos onde é necessário mostrar o nome do cliente
            /// </summary>
            public static string Cliente {
                get { return WebConfigurationManager.AppSettings["Cliente"]; }
            }

            /// <summary>
            ///  Usado para carregar as imagens Dinamicamente por Cliente
            /// </summary>
            public static string ImagensPersonalizadas {
                get { return WebConfigurationManager.AppSettings["Cliente"]; }
            }

            /// <summary>
            ///  Usado para carregar a Página Inicial Do Sistema
            /// </summary>
            public static string PaginaInicial {
                get { return WebConfigurationManager.AppSettings["PaginaInicial"]; }
            }

            /// <summary>
            ///  Informação Sobre Conta Corrente do Cliente
            /// </summary>
            public static bool MultiConta {
                get { return Convert.ToBoolean(WebConfigurationManager.AppSettings["MultiConta"]); }
            }

            /// <summary>
            ///  Informação Sobre Acesso Automático para internos na hora do cadastro do cotista
            /// </summary>
            public static bool PermissaoInternoAuto {
                get { return Convert.ToBoolean(WebConfigurationManager.AppSettings["PermissaoInternoAuto"]); }
            }

            /// <summary>
            ///  Usado para associação na criação automática de logins para clientes
            /// </summary>
            public static string GrupoCliente {
                get { return WebConfigurationManager.AppSettings["GrupoCliente"]; }
            }

            /// <summary>
            ///  Usado para associação na criação automática de logins para cotistas
            /// </summary>
            public static string GrupoCotista {
                get { return WebConfigurationManager.AppSettings["GrupoCotista"]; }
            }

            /// <summary>
            ///  Usado para mostrar o combo de agente liquidação na tela de operação de bolsa
            /// </summary>
            public static string AgenteLiquidacaoExclusivo {
                get { return WebConfigurationManager.AppSettings["AgenteLiquidacaoExclusivo"]; }
            }

            /// <summary>
            ///  Se true Mostra o Campo CalculaContabil no Cadastro de Cliente 
            ///  e os Campos IdEventoProvisao/IdEventoPagamento nos Cadastros de TabelaProvisão/TabelaTaxaAdministracao
            /// </summary>  
            public static bool CalculaContatil {
                get {
                    return WebConfigurationManager.AppSettings["CalculaContatil"] == ""
                           ? false
                           : Convert.ToBoolean(WebConfigurationManager.AppSettings["CalculaContatil"]);
                }
            }

            /// <summary>
            /// Define o Diretorio de Downloads
            /// </summary>
            public static string DiretorioDownloads {
                get { return WebConfigurationManager.AppSettings["DiretorioDownloads"]; }
            }

            /// <summary>
            ///  Usado para setar se anexa todos os pdfs em 1 único pdf na hora do envio do email
            /// </summary>
            public static string EnvioEmailPDFUnico {
                get { return WebConfigurationManager.AppSettings["EnvioEmailPDFUnico"]; }
            }

            /// <summary>
            ///  Usado para definir se trava ou não a data de lançamento
            /// </summary>
            public static bool LiberaDataLancamentoLiquidacao {
                get {
                    if (WebConfigurationManager.AppSettings["LiberaDataLancamentoLiquidacao"] == "") {
                        return false;
                    }
                    else {
                        return Convert.ToBoolean(WebConfigurationManager.AppSettings["LiberaDataLancamentoLiquidacao"]);
                    }
                }
            }

            /// <summary>
            ///  Se true Mostra o teclado virtual na página de login
            /// </summary>  
            public static bool TecladoVirtual {
                get {
                    return WebConfigurationManager.AppSettings["TecladoVirtual"] == ""
                           ? false
                           : Convert.ToBoolean(WebConfigurationManager.AppSettings["TecladoVirtual"]);
                }
            }

            /// <summary>
            ///  Se true utiliza o FinancialDesk para usuários de grupos do tipo Externo
            /// </summary>  
            public static bool FinancialDesk {
                get {
                    string settingFinancialDesk = WebConfigurationManager.AppSettings["FinancialDesk"];
                    return settingFinancialDesk == ""
                           ? false
                           : Convert.ToBoolean(settingFinancialDesk);
                }
            }

            /// <summary>
            ///  Se true seta para usar SSL no envio de email
            /// </summary>  
            public static bool SMTPEnableSSL
            {
                get {
                    string settingSMTPEnableSSL = WebConfigurationManager.AppSettings["SMTPEnableSSL"];
                    return settingSMTPEnableSSL == ""
                           ? false
                           : Convert.ToBoolean(settingSMTPEnableSSL);
                }
            }

            /// <summary>
            ///  Rolagem virtual do grid
            /// </summary>  
            public static bool RolagemVirtual
            {
                get
                {
                    string rolagemVirtual = WebConfigurationManager.AppSettings["RolagemVirtual"];
                    return rolagemVirtual == ""
                           ? false
                           : Convert.ToBoolean(rolagemVirtual);
                }
            }

            /// <summary>
            ///  Usado para Batimento Sinacor
            /// </summary>
            public static string ListaNumeros {
                get { return WebConfigurationManager.AppSettings["ListaNumeros"]; }
            }

            #region ESPECIFICO PORTOPAR
            /// <summary>
            /// Código do Usuário - XML 
            /// </summary>
            public static string ConfigUsuarioIntegracao
            {
                get { return ParametrosConfiguracaoSistema.Outras.UsuarioIntegracao; }
            }

            /// <summary>
            /// Senha - XML 
            /// </summary>
            public static string ConfigSenhaIntegracao
            {
                get { return ParametrosConfiguracaoSistema.Outras.SenhaIntegracao; }
            }

            /// <summary>
            /// Código do Gestor do Cliente no sistema FJ - XML 
            /// </summary>
            public static string ConfigCdBanCli
            {
                get { return WebConfigurationManager.AppSettings["ConfigCdBanCli"]; }
            }
            #endregion
        }
    }

    /// <summary>
    /// Fornece o Path Completo da Aplicação no servidor onde está rodando a aplicação
    /// </summary>
    public static class DiretorioAplicacao {
        public static string DiretorioBaseAplicacao {
            //get { return HttpContext.Current.Server.MapPath("/Financial.Web/"); }

            get {
                //string path = "/" + WebConfigurationManager.AppSettings["DiretorioBaseAplicacao"] + "/";

                string aux = WebConfigurationManager.AppSettings["DiretorioBaseAplicacao"];
                string path = String.IsNullOrEmpty(aux)
                                ? "~/"
                                : "/" + WebConfigurationManager.AppSettings["DiretorioBaseAplicacao"] + "/";

                string retorno = HttpContext.Current.Server.MapPath(path);
                return retorno;
            }
        }
    }
}