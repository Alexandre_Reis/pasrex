﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.InterfacesCustom;
using Financial.InterfacesCustom.PortoPar;

/// <summary>
/// Summary description for OrdemTesourariaViewModel
/// </summary>
public class OrdemTesourariaViewModel
{
   
    public int? IdOperacao;
    public int? IdOperacaoMae;
    public int? IdCarteira;
    public string NomeCarteira;
    public int IdCotista;
    public DateTime DataOperacao;
    public Decimal Valor;
    public string TipoOperacaoString;
    public string StatusProcessamento;
    public byte Status;
    public byte TipoOperacao;
    public int? IdConta;
    public byte IdFormaLiquidacao;
    public bool IsDistribuida;
    public string NomeCotista;
    public string ContaCorrente;
    public string FormaLiquidacao;
    public string Observacao;
    //public bool IsPrevidencia;

    public OrdemTesourariaViewModel()
    {
    }
    public OrdemTesourariaViewModel(Financial.InterfacesCustom.OrdemTesouraria ordemTesouraria)
    {
        this.IdOperacao = ordemTesouraria.IdOperacao;
        this.IdOperacaoMae = ordemTesouraria.IdOperacaoMae;
        this.IdCarteira = ordemTesouraria.IdCarteira;
        this.DataOperacao = ordemTesouraria.DataOperacao.Value;
        this.IdCotista = ordemTesouraria.IdCotista.Value;
        this.IdConta = ordemTesouraria.IdConta;
        this.IdFormaLiquidacao = ordemTesouraria.IdFormaLiquidacao.Value;

        Cotista cotista = new Cotista();
        cotista.LoadByPrimaryKey(this.IdCotista);
        this.NomeCotista = cotista.Nome;

        if (this.IdCarteira.HasValue)
        {
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(this.IdCarteira.Value);
            this.NomeCarteira = carteira.Nome;
            
            /*CategoriaFundo categoria = carteira.UpToCategoriaFundoByIdCategoria;
            string descricaoCategoria = categoria.Descricao.ToLower();
            this.IsPrevidencia = descricaoCategoria.Contains("previdência") || 
                descricaoCategoria.Contains("previdencia") || 
                descricaoCategoria.Contains("vgbl") || 
                descricaoCategoria.Contains("pgbl");*/
        }

        string tipoOperacao = "";
        if (ordemTesouraria.TipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
        {
            tipoOperacao = "Aplicação";
        }
        else if (ordemTesouraria.TipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
        {
            tipoOperacao = "Resgate Total";
        }
        else
        {
            tipoOperacao = "Resgate Parcial";
        }

        this.TipoOperacao = (byte)ordemTesouraria.TipoOperacao;
        this.TipoOperacaoString = tipoOperacao;

        this.Valor = ordemTesouraria.Valor.Value;
        this.Status = (byte)ordemTesouraria.Status;

        string statusDescricao = "";
       
        if (ordemTesouraria.Status == (byte)StatusOrdemTesourariaPorto.Aprovado)
        {
            statusDescricao = "Aprovada";
        }
        else if (ordemTesouraria.Status == (byte)StatusOrdemTesourariaPorto.Digitado)
        {
            statusDescricao = "Digitada";
        }
        else if (ordemTesouraria.Status == (byte)StatusOrdemTesourariaPorto.Rejeitado)
        {
            statusDescricao = "Rejeitada";
        }
        else if (ordemTesouraria.Status == (byte)StatusOrdemTesourariaPorto.Distribuido)
        {
            statusDescricao = "Distribuída";
        }
        else if (ordemTesouraria.Status == (byte)StatusOrdemTesourariaPorto.PGBLIntegrado)
        {
            statusDescricao = "Integrada";
        }
        else if (ordemTesouraria.Status == (byte)StatusOrdemTesourariaPorto.PGBLAprovado)
        {
            statusDescricao = "Aprovada";
        }
        else if (ordemTesouraria.Status == (byte)StatusOrdemTesourariaPorto.PGBLRejeitado)
        {
            statusDescricao = "Rejeitada";
        }

        Financial.ContaCorrente.FormaLiquidacao formaLiquidacao = new Financial.ContaCorrente.FormaLiquidacao();
        if (formaLiquidacao.LoadByPrimaryKey(this.IdFormaLiquidacao))
        {
            this.FormaLiquidacao = formaLiquidacao.Descricao;
        }

        ContaCorrente contaCorrente = new ContaCorrente();
        if (this.IdConta.HasValue && contaCorrente.LoadByPrimaryKey(this.IdConta.Value))
        {
            ContaCorrenteViewModel contaCorrenteViewModel = new ContaCorrenteViewModel(contaCorrente);
            this.ContaCorrente = contaCorrenteViewModel.Descricao;
        }
        this.StatusProcessamento = statusDescricao;

        Financial.InterfacesCustom.OrdemTesourariaCollection filhas = new Financial.InterfacesCustom.OrdemTesourariaCollection();
        filhas.Query.Select(filhas.Query.IdOperacao);
        filhas.Query.Where(filhas.Query.IdOperacaoMae.Equal(this.IdOperacao));
        filhas.Load(filhas.Query);
        this.IsDistribuida = filhas.Count > 0;
        this.Observacao = string.IsNullOrEmpty(ordemTesouraria.Observacao) ? "" : ordemTesouraria.Observacao.Replace("\n", "<br />");

    }
}