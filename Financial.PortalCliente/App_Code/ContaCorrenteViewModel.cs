using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Fundo;
using Financial.Common;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Investidor;

public class ContaCorrenteViewModel
{
    public string Descricao;
    public int IdConta;
    
    public ContaCorrenteViewModel()
    {

    }
    public ContaCorrenteViewModel(ContaCorrente contaCorrente)
    {
        this.IdConta = contaCorrente.IdConta.Value;

        string nomeBanco = "";
        if (contaCorrente.IdBanco.HasValue)
        {
            Banco banco = new Banco();
            banco.LoadByPrimaryKey(contaCorrente.IdBanco.Value);
            nomeBanco = banco.Nome;
        }

        string codigoAgencia = "";
        if (contaCorrente.IdAgencia.HasValue)
        {
            Agencia agencia = new Agencia();
            agencia.LoadByPrimaryKey(contaCorrente.IdAgencia.Value);
            codigoAgencia = agencia.Codigo;
        }

        this.Descricao = String.Format("{0} - {1} - {2}-{3}", nomeBanco, codigoAgencia, contaCorrente.Numero, contaCorrente.DigitoConta);
    }

}