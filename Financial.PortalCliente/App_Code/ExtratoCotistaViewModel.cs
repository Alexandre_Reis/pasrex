﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Fundo;
using Financial.CRM;

public class ExtratoCotistaViewModel
{
    public int IdCarteira;
    public DateTime DataHistorico;
    public string Historico;
    public decimal CotaDia;
    public decimal ValorBruto;
    public decimal ValorLiquido;
    public decimal Quantidade;
    public decimal ValorIR;
    public decimal ValorIOF;
    public string NomeCarteira;

    public ExtratoCotistaViewModel()
    {

    }
    public ExtratoCotistaViewModel(PosicaoCotistaHistorico posicaoCotista)
    {

        this.CotaDia = posicaoCotista.CotaDia.Value;
        this.DataHistorico = posicaoCotista.DataHistorico.Value;
        this.IdCarteira = posicaoCotista.IdCarteira.Value;
        this.Quantidade = posicaoCotista.Quantidade.Value;
        this.ValorBruto = posicaoCotista.ValorBruto.Value;
        this.ValorIOF = posicaoCotista.ValorIOF.Value;
        this.ValorIR = posicaoCotista.ValorIR.Value;
        this.ValorLiquido = posicaoCotista.ValorLiquido.Value;

        Carteira carteira = new Carteira();
        carteira.LoadByPrimaryKey(this.IdCarteira);
        this.NomeCarteira = carteira.Nome;

    }

    public ExtratoCotistaViewModel(PosicaoCotista posicaoCotista)
    {

        this.CotaDia = posicaoCotista.CotaDia.Value;
        this.DataHistorico = DateTime.Today;
        this.IdCarteira = posicaoCotista.IdCarteira.Value;
        this.Quantidade = posicaoCotista.Quantidade.Value;
        this.ValorBruto = posicaoCotista.ValorBruto.Value;
        this.ValorIOF = posicaoCotista.ValorIOF.Value;
        this.ValorIR = posicaoCotista.ValorIR.Value;
        this.ValorLiquido = posicaoCotista.ValorLiquido.Value;

        Carteira carteira = new Carteira();
        carteira.LoadByPrimaryKey(this.IdCarteira);
        this.NomeCarteira = carteira.Nome;

    }

    public ExtratoCotistaViewModel(OrdemCotista ordemCotista)
    {
        //this.CotaDia = 
        this.DataHistorico = ordemCotista.DataOperacao.Value;
        this.IdCarteira = ordemCotista.IdCarteira.Value;
        this.Quantidade = ordemCotista.Quantidade.Value;
        this.ValorBruto = ordemCotista.ValorBruto.Value;
        this.ValorIOF = 0;
        this.ValorIR = 0;
        this.ValorLiquido = ordemCotista.ValorLiquido.Value;

        Carteira carteira = new Carteira();
        carteira.LoadByPrimaryKey(this.IdCarteira);
        this.NomeCarteira = carteira.Nome;

    }

    public ExtratoCotistaViewModel(OperacaoCotista operacaoCotista)
    {
        this.CotaDia = operacaoCotista.CotaOperacao.Value;
        this.DataHistorico = operacaoCotista.DataOperacao.Value;
        this.IdCarteira = operacaoCotista.IdCarteira.Value;
        this.Quantidade = operacaoCotista.Quantidade.Value;
        this.ValorBruto = operacaoCotista.ValorBruto.Value;
        this.ValorIOF = operacaoCotista.ValorIOF.Value;
        this.ValorIR = operacaoCotista.ValorIR.Value;
        this.ValorLiquido = operacaoCotista.ValorLiquido.Value;

        string historico = "";
        if (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ComeCotas)
        {
            historico = "Resgate Come-Cotas";
        }
        else if (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
        {
            historico = "Aplicação";
        }
        else if (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
            operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateCotas ||
            operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateCotasEspecial ||
            operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido)
        {
            historico = "Resgate";
        }
        else if (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
        {
            historico = "Resgate Total";
        }
        else
        {
            historico = "Outros";
        }
        this.Historico = historico;

        Carteira carteira = new Carteira();
        carteira.LoadByPrimaryKey(this.IdCarteira);
        this.NomeCarteira = carteira.Nome;

    }

    public void AddPosicaoZerada(int idCarteira, DateTime dataHistorico, string historico)
    {
        this.Historico = historico;
        this.CotaDia = 0;
        this.DataHistorico = dataHistorico;
        this.IdCarteira = idCarteira;
        this.Quantidade = 0;
        this.ValorBruto = 0;
        this.ValorIOF = 0;
        this.ValorIR = 0;
        this.ValorLiquido = 0;

        Carteira carteira = new Carteira();
        carteira.LoadByPrimaryKey(this.IdCarteira);
        this.NomeCarteira = carteira.Nome;
    }
}

