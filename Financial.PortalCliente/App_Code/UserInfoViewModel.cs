using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Fundo;
using Financial.InterfacesCustom;

public class UserInfoViewModel
{
    //public IIdentity Identity { get; private set; }
    //  public bool IsInRole(string role) { return false; }

    // public CustomPrincipal(string login)
    // {
    //    this.Identity = new GenericIdentity(login);
    // }

    public int UserId;
    public int TenantId;
    public string FirstName;
    public string LastName;
    public string Login;
    public string CPFCNPJ;
    public bool MultiCotista;
    public bool CotistaNaSession;
    public string PerfilInvestidor;
    public int IdCotista;
    public bool CotistaPodeOperar;
    public string PendenciaCadastral;
    public UserInfoPermissoes Permissoes;
    
    public UserInfoViewModel()
    {
    }

    public void LoadPermissoes(int idGrupo)
    {
        this.Permissoes = new UserInfoPermissoes();

        this.Permissoes.AcessoSeguranca = false;
        this.Permissoes.PodeAprovar = false;
        this.Permissoes.PodeBoletar = false;
        this.Permissoes.PodeDistribuir = false;
        this.Permissoes.AcessoPGBL = false;

        PermissaoPortalCliente permissaoPortalCliente = new PermissaoPortalCliente();
        if (permissaoPortalCliente.LoadByPrimaryKey(idGrupo))
        {
            this.Permissoes.AcessoPGBL = permissaoPortalCliente.AcessoPGBL == "S";
            this.Permissoes.AcessoSeguranca = permissaoPortalCliente.AcessoSeguranca == "S";
            this.Permissoes.PodeAprovar = permissaoPortalCliente.PodeAprovar == "S";
            this.Permissoes.PodeBoletar = permissaoPortalCliente.PodeBoletar == "S";
            this.Permissoes.PodeDistribuir = permissaoPortalCliente.PodeDistribuir == "S";
        }
        
        Financial.Security.GrupoUsuario grupo = new Financial.Security.GrupoUsuario();
        if (grupo.LoadByPrimaryKey(idGrupo))
        {
            this.Permissoes.IsGestor = grupo.Descricao.ToUpper().Contains("GESTOR");
            this.Permissoes.IsNotExterno = grupo.TipoPerfil != (byte)Financial.Security.Enums.TipoPerfilGrupo.Externo;
        }

    }
}

public class UserInfoPermissoes
{
    public bool PodeBoletar;
    public bool PodeDistribuir;
    public bool PodeAprovar;
    public bool AcessoSeguranca;
    public bool AcessoPGBL;
    public bool IsGestor;
    public bool IsNotExterno;

    public UserInfoPermissoes(){

    }
}
