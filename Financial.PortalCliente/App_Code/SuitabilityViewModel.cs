﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Fundo;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.CRM;

public class SuitabilityViewModel
{
    public List<SuitabilityQuestaoViewModel> Questoes = new List<SuitabilityQuestaoViewModel>();
    public SuitabilityViewModel(int idTipoValidacao)
    {
        SuitabilityQuestaoCollection questaoCollection = new SuitabilityQuestaoCollection();
        questaoCollection.Query.Where(questaoCollection.Query.IdValidacao.Equal(idTipoValidacao));
        questaoCollection.Load(questaoCollection.Query);

        this.Questoes = new List<SuitabilityQuestaoViewModel>();
        foreach (SuitabilityQuestao questao in questaoCollection)
        {
            this.Questoes.Add(new SuitabilityQuestaoViewModel(questao));
        }
    }

    public void LoadRespostas(PessoaSuitability pessoaSuitability, int idCotista)
    {

        if (pessoaSuitability.IdValidacao.HasValue && pessoaSuitability.UltimaAlteracao.HasValue)
        {

            //Seleciona ultima data da resposta do questionário do cliente
            SuitabilityRespostaCotistaDetalheCollection respostas = new SuitabilityRespostaCotistaDetalheCollection();
            respostas.Query.Where(respostas.Query.IdCotista.Equal(idCotista),
                respostas.Query.Data.Equal(pessoaSuitability.UltimaAlteracao));
            respostas.Query.OrderBy(respostas.Query.Data.Descending);
            respostas.Load(respostas.Query);

            List<int> questoesRespondidas = new List<int>();
            foreach (SuitabilityRespostaCotistaDetalhe resposta in respostas)
            {
                foreach (SuitabilityQuestaoViewModel questao in this.Questoes)
                {
                    if (questao.IdQuestao == resposta.IdQuestao && !questoesRespondidas.Contains(questao.IdQuestao))
                    {
                        foreach (SuitabilityOpcaoViewModel opcao in questao.Opcoes)
                        {
                            if (resposta.IdOpcao == opcao.IdOpcao)
                            {
                                opcao.Escolhida = true;
                                break;
                            }
                        }
                        questoesRespondidas.Add(questao.IdQuestao);
                    }
                }
            }


        }
    }

}

public class SuitabilityQuestaoViewModel
{
    public int IdQuestao;
    public string Descricao;
    public int Fator;
    public List<SuitabilityOpcaoViewModel> Opcoes;

    public SuitabilityQuestaoViewModel(SuitabilityQuestao questao)
    {
        this.IdQuestao = questao.IdQuestao.Value;
        this.Descricao = questao.Descricao;
        this.Fator = questao.Fator.Value;
        this.Opcoes = new List<SuitabilityOpcaoViewModel>();
        SuitabilityOpcaoCollection opcaoCollection = questao.SuitabilityOpcaoCollectionByIdQuestao;

        foreach (SuitabilityOpcao opcao in opcaoCollection)
        {
            this.Opcoes.Add(new SuitabilityOpcaoViewModel(opcao));
        }
    }
}

public class SuitabilityOpcaoViewModel
{
    public int IdOpcao;
    public string Descricao;
    public bool Escolhida;

    public SuitabilityOpcaoViewModel(SuitabilityOpcao opcao)
    {
        this.IdOpcao = opcao.IdOpcao.Value;
        this.Descricao = opcao.Descricao;
        this.Escolhida = false;
    }
}
