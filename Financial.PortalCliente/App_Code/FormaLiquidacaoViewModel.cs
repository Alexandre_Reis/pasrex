using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Fundo;
using Financial.Common;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Investidor;
using Financial.ContaCorrente;

public class FormaLiquidacaoViewModel
{
    public string Descricao;
    public byte IdFormaLiquidacao;
    
    public FormaLiquidacaoViewModel()
    {

    }
    public FormaLiquidacaoViewModel(FormaLiquidacao formaLiquidacao)
    {
        this.IdFormaLiquidacao = formaLiquidacao.IdFormaLiquidacao.Value;
        this.Descricao = formaLiquidacao.Descricao;
   }

}