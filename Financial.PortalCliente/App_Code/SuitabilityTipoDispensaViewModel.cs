using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Fundo;
using Financial.Common;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Investidor;
using Financial.ContaCorrente;

public class SuitabilityTipoDispensaViewModel
{
    public string Descricao;
    public int IdDispensa;
    
    public SuitabilityTipoDispensaViewModel()
    {

    }

    public SuitabilityTipoDispensaViewModel(int idDispensa, string descricao)
    {
        this.IdDispensa = idDispensa;
        this.Descricao = descricao;
    }

    public SuitabilityTipoDispensaViewModel(SuitabilityTipoDispensa suitabilityTipoDispensa)
    {
        this.IdDispensa = suitabilityTipoDispensa.IdDispensa.Value;
        this.Descricao = suitabilityTipoDispensa.Descricao;
   }

}