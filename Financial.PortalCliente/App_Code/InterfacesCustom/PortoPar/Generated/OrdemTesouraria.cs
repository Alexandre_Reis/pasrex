/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 13:34:12
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;













































































































































































































namespace Financial.InterfacesCustom
{

    [Serializable]
    abstract public class esOrdemTesourariaCollection : esEntityCollection
    {
        public esOrdemTesourariaCollection()
        {

        }

        protected override string GetCollectionName()
        {
            return "OrdemTesourariaCollection";
        }

        #region Query Logic
        protected void InitQuery(esOrdemTesourariaQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntityCollection)this).Connection;
        }

        protected bool OnQueryLoaded(DataTable table)
        {
            this.PopulateCollection(table);
            return (this.RowCount > 0) ? true : false;
        }

        protected override void HookupQuery(esDynamicQuery query)
        {
            this.InitQuery(query as esOrdemTesourariaQuery);
        }
        #endregion

        virtual public OrdemTesouraria DetachEntity(OrdemTesouraria entity)
        {
            return base.DetachEntity(entity) as OrdemTesouraria;
        }

        virtual public OrdemTesouraria AttachEntity(OrdemTesouraria entity)
        {
            return base.AttachEntity(entity) as OrdemTesouraria;
        }

        virtual public void Combine(OrdemTesourariaCollection collection)
        {
            base.Combine(collection);
        }

        new public OrdemTesouraria this[int index]
        {
            get
            {
                return base[index] as OrdemTesouraria;
            }
        }

        public override Type GetEntityType()
        {
            return typeof(OrdemTesouraria);
        }
    }



    [Serializable]
    abstract public class esOrdemTesouraria : esEntity
    {
        /// <summary>
        /// Used internally by the entity's DynamicQuery mechanism.
        /// </summary>
        virtual protected esOrdemTesourariaQuery GetDynamicQuery()
        {
            return null;
        }

        public esOrdemTesouraria()
        {

        }

        public esOrdemTesouraria(DataRow row)
            : base(row)
        {

        }

        #region LoadByPrimaryKey
        public virtual bool LoadByPrimaryKey(System.Int32 idOperacao)
        {
            if (this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idOperacao);
            else
                return LoadByPrimaryKeyStoredProcedure(idOperacao);
        }

        /// <summary>
        /// Loads an entity by primary key
        /// </summary>
        /// <remarks>
        /// EntitySpaces requires primary keys be defined on all tables.
        /// If a table does not have a primary key set,
        /// this method will not compile.
        /// Does not support sqlAcessType. It only works with DynamicQuery
        /// </remarks>
        /// <param name="fieldsToReturn">Fields desired</param>
        public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idOperacao)
        {
            esQueryItem[] fields = fieldsToReturn.ToArray();
            esOrdemTesourariaQuery query = this.GetDynamicQuery();
            query
                .Select(fields)
                .Where(query.IdOperacao == idOperacao);

            return query.Load();
        }

        public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao)
        {
            if (sqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idOperacao);
            else
                return LoadByPrimaryKeyStoredProcedure(idOperacao);
        }

        private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao)
        {
            esOrdemTesourariaQuery query = this.GetDynamicQuery();
            query.Where(query.IdOperacao == idOperacao);
            return query.Load();
        }

        private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao)
        {
            esParameters parms = new esParameters();
            parms.Add("IdOperacao", idOperacao);
            return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
        }
        #endregion



        #region Properties


        public override void SetProperties(IDictionary values)
        {
            foreach (string propertyName in values.Keys)
            {
                this.SetProperty(propertyName, values[propertyName]);
            }
        }

        public override void SetProperty(string name, object value)
        {
            if (this.Row == null) this.AddNew();

            esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
            if (col != null)
            {
                if (value == null || value.GetType().ToString() == "System.String")
                {
                    // Use the strongly typed property
                    switch (name)
                    {
                        case "IdOperacao": this.str.IdOperacao = (string)value; break;
                        case "IdOperacaoMae": this.str.IdOperacaoMae = (string)value; break;
                        case "IdCarteira": this.str.IdCarteira = (string)value; break;
                        case "IdCotista": this.str.IdCotista = (string)value; break;
                        case "DataOperacao": this.str.DataOperacao = (string)value; break;
                        case "TipoOperacao": this.str.TipoOperacao = (string)value; break;
                        case "Status": this.str.Status = (string)value; break;
                        case "Valor": this.str.Valor = (string)value; break;
                        case "IdFormaLiquidacao": this.str.IdFormaLiquidacao = (string)value; break;
                        case "IdConta": this.str.IdConta = (string)value; break;
                        case "Observacao": this.str.Observacao = (string)value; break;
                    }
                }
                else
                {
                    switch (name)
                    {
                        case "IdOperacao":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdOperacao = (System.Int32?)value;
                            break;

                        case "IdOperacaoMae":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdOperacaoMae = (System.Int32?)value;
                            break;

                        case "IdCarteira":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdCarteira = (System.Int32?)value;
                            break;

                        case "IdCotista":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdCotista = (System.Int32?)value;
                            break;

                        case "DataOperacao":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataOperacao = (System.DateTime?)value;
                            break;

                        case "TipoOperacao":

                            if (value == null || value.GetType().ToString() == "System.Byte")
                                this.TipoOperacao = (System.Byte?)value;
                            break;

                        case "Status":

                            if (value == null || value.GetType().ToString() == "System.Byte")
                                this.Status = (System.Byte?)value;
                            break;

                        case "Valor":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.Valor = (System.Decimal?)value;
                            break;

                        case "IdFormaLiquidacao":

                            if (value == null || value.GetType().ToString() == "System.Byte")
                                this.IdFormaLiquidacao = (System.Byte?)value;
                            break;

                        case "IdConta":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdConta = (System.Int32?)value;
                            break;


                        default:
                            break;
                    }
                }
            }
            else if (this.Row.Table.Columns.Contains(name))
            {
                this.Row[name] = value;
            }
            else
            {
                throw new Exception("SetProperty Error: '" + name + "' not found");
            }
        }


        /// <summary>
        /// Maps to OrdemTesouraria.IdOperacao
        /// </summary>
        virtual public System.Int32? IdOperacao
        {
            get
            {
                return base.GetSystemInt32(OrdemTesourariaMetadata.ColumnNames.IdOperacao);
            }

            set
            {
                base.SetSystemInt32(OrdemTesourariaMetadata.ColumnNames.IdOperacao, value);
            }
        }

        /// <summary>
        /// Maps to OrdemTesouraria.IdOperacaoMae
        /// </summary>
        virtual public System.Int32? IdOperacaoMae
        {
            get
            {
                return base.GetSystemInt32(OrdemTesourariaMetadata.ColumnNames.IdOperacaoMae);
            }

            set
            {
                if (base.SetSystemInt32(OrdemTesourariaMetadata.ColumnNames.IdOperacaoMae, value))
                {
                    this._UpToOrdemTesourariaByIdOperacaoMae = null;
                }
            }
        }

        /// <summary>
        /// Maps to OrdemTesouraria.IdCarteira
        /// </summary>
        virtual public System.Int32? IdCarteira
        {
            get
            {
                return base.GetSystemInt32(OrdemTesourariaMetadata.ColumnNames.IdCarteira);
            }

            set
            {
                base.SetSystemInt32(OrdemTesourariaMetadata.ColumnNames.IdCarteira, value);
            }
        }

        /// <summary>
        /// Maps to OrdemTesouraria.IdCotista
        /// </summary>
        virtual public System.Int32? IdCotista
        {
            get
            {
                return base.GetSystemInt32(OrdemTesourariaMetadata.ColumnNames.IdCotista);
            }

            set
            {
                base.SetSystemInt32(OrdemTesourariaMetadata.ColumnNames.IdCotista, value);
            }
        }

        /// <summary>
        /// Maps to OrdemTesouraria.DataOperacao
        /// </summary>
        virtual public System.DateTime? DataOperacao
        {
            get
            {
                return base.GetSystemDateTime(OrdemTesourariaMetadata.ColumnNames.DataOperacao);
            }

            set
            {
                base.SetSystemDateTime(OrdemTesourariaMetadata.ColumnNames.DataOperacao, value);
            }
        }

        /// <summary>
        /// Maps to OrdemTesouraria.TipoOperacao
        /// </summary>
        virtual public System.Byte? TipoOperacao
        {
            get
            {
                return base.GetSystemByte(OrdemTesourariaMetadata.ColumnNames.TipoOperacao);
            }

            set
            {
                base.SetSystemByte(OrdemTesourariaMetadata.ColumnNames.TipoOperacao, value);
            }
        }

        /// <summary>
        /// Maps to OrdemTesouraria.Status
        /// </summary>
        virtual public System.Byte? Status
        {
            get
            {
                return base.GetSystemByte(OrdemTesourariaMetadata.ColumnNames.Status);
            }

            set
            {
                base.SetSystemByte(OrdemTesourariaMetadata.ColumnNames.Status, value);
            }
        }

        /// <summary>
        /// Maps to OrdemTesouraria.Valor
        /// </summary>
        virtual public System.Decimal? Valor
        {
            get
            {
                return base.GetSystemDecimal(OrdemTesourariaMetadata.ColumnNames.Valor);
            }

            set
            {
                base.SetSystemDecimal(OrdemTesourariaMetadata.ColumnNames.Valor, value);
            }
        }

        /// <summary>
        /// Maps to OrdemTesouraria.IdFormaLiquidacao
        /// </summary>
        virtual public System.Byte? IdFormaLiquidacao
        {
            get
            {
                return base.GetSystemByte(OrdemTesourariaMetadata.ColumnNames.IdFormaLiquidacao);
            }

            set
            {
                base.SetSystemByte(OrdemTesourariaMetadata.ColumnNames.IdFormaLiquidacao, value);
            }
        }

        /// <summary>
        /// Maps to OrdemTesouraria.IdConta
        /// </summary>
        virtual public System.Int32? IdConta
        {
            get
            {
                return base.GetSystemInt32(OrdemTesourariaMetadata.ColumnNames.IdConta);
            }

            set
            {
                base.SetSystemInt32(OrdemTesourariaMetadata.ColumnNames.IdConta, value);
            }
        }

        /// <summary>
        /// Maps to OrdemTesouraria.Observacao
        /// </summary>
        virtual public System.String Observacao
        {
            get
            {
                return base.GetSystemString(OrdemTesourariaMetadata.ColumnNames.Observacao);
            }

            set
            {
                base.SetSystemString(OrdemTesourariaMetadata.ColumnNames.Observacao, value);
            }
        }

        [CLSCompliant(false)]
        internal protected OrdemTesouraria _UpToOrdemTesourariaByIdOperacaoMae;
        #endregion

        #region String Properties


        [BrowsableAttribute(false)]
        public esStrings str
        {
            get
            {
                if (esstrings == null)
                {
                    esstrings = new esStrings(this);
                }
                return esstrings;
            }
        }


        [Serializable]
        sealed public class esStrings
        {
            public esStrings(esOrdemTesouraria entity)
            {
                this.entity = entity;
            }


            public System.String IdOperacao
            {
                get
                {
                    System.Int32? data = entity.IdOperacao;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdOperacao = null;
                    else entity.IdOperacao = Convert.ToInt32(value);
                }
            }

            public System.String IdOperacaoMae
            {
                get
                {
                    System.Int32? data = entity.IdOperacaoMae;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdOperacaoMae = null;
                    else entity.IdOperacaoMae = Convert.ToInt32(value);
                }
            }

            public System.String IdCarteira
            {
                get
                {
                    System.Int32? data = entity.IdCarteira;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdCarteira = null;
                    else entity.IdCarteira = Convert.ToInt32(value);
                }
            }

            public System.String IdCotista
            {
                get
                {
                    System.Int32? data = entity.IdCotista;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdCotista = null;
                    else entity.IdCotista = Convert.ToInt32(value);
                }
            }

            public System.String DataOperacao
            {
                get
                {
                    System.DateTime? data = entity.DataOperacao;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.DataOperacao = null;
                    else entity.DataOperacao = Convert.ToDateTime(value);
                }
            }

            public System.String TipoOperacao
            {
                get
                {
                    System.Byte? data = entity.TipoOperacao;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.TipoOperacao = null;
                    else entity.TipoOperacao = Convert.ToByte(value);
                }
            }

            public System.String Status
            {
                get
                {
                    System.Byte? data = entity.Status;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Status = null;
                    else entity.Status = Convert.ToByte(value);
                }
            }

            public System.String Valor
            {
                get
                {
                    System.Decimal? data = entity.Valor;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Valor = null;
                    else entity.Valor = Convert.ToDecimal(value);
                }
            }

            public System.String IdFormaLiquidacao
            {
                get
                {
                    System.Byte? data = entity.IdFormaLiquidacao;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdFormaLiquidacao = null;
                    else entity.IdFormaLiquidacao = Convert.ToByte(value);
                }
            }

            public System.String IdConta
            {
                get
                {
                    System.Int32? data = entity.IdConta;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdConta = null;
                    else entity.IdConta = Convert.ToInt32(value);
                }
            }

            public System.String Observacao
            {
                get
                {
                    System.String data = entity.Observacao;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Observacao = null;
                    else entity.Observacao = Convert.ToString(value);
                }
            }


            private esOrdemTesouraria entity;
        }
        #endregion

        #region Query Logic
        protected void InitQuery(esOrdemTesourariaQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntity)this).Connection;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        protected bool OnQueryLoaded(DataTable table)
        {
            bool dataFound = this.PopulateEntity(table);

            if (this.RowCount > 1)
            {
                throw new Exception("esOrdemTesouraria can only hold one record of data");
            }

            return dataFound;
        }
        #endregion

        [NonSerialized]
        private esStrings esstrings;
    }



    public partial class OrdemTesouraria : esOrdemTesouraria
    {


        #region OrdemTesourariaCollectionByIdOperacaoMae - Zero To Many
        /// <summary>
        /// Zero to Many
        /// Foreign Key Name - FK_OrdemTesouraria_OrdemTesouraria1
        /// </summary>

        [XmlIgnore]
        public OrdemTesourariaCollection OrdemTesourariaCollectionByIdOperacaoMae
        {
            get
            {
                if (this._OrdemTesourariaCollectionByIdOperacaoMae == null)
                {
                    this._OrdemTesourariaCollectionByIdOperacaoMae = new OrdemTesourariaCollection();
                    this._OrdemTesourariaCollectionByIdOperacaoMae.es.Connection.Name = this.es.Connection.Name;
                    this.SetPostSave("OrdemTesourariaCollectionByIdOperacaoMae", this._OrdemTesourariaCollectionByIdOperacaoMae);

                    if (this.IdOperacao != null)
                    {
                        this._OrdemTesourariaCollectionByIdOperacaoMae.Query.Where(this._OrdemTesourariaCollectionByIdOperacaoMae.Query.IdOperacaoMae == this.IdOperacao);
                        this._OrdemTesourariaCollectionByIdOperacaoMae.Query.Load();

                        // Auto-hookup Foreign Keys
                        this._OrdemTesourariaCollectionByIdOperacaoMae.fks.Add(OrdemTesourariaMetadata.ColumnNames.IdOperacaoMae, this.IdOperacao);
                    }
                }

                return this._OrdemTesourariaCollectionByIdOperacaoMae;
            }

            set
            {
                if (value != null) throw new Exception("'value' Must be null");

                if (this._OrdemTesourariaCollectionByIdOperacaoMae != null)
                {
                    this.RemovePostSave("OrdemTesourariaCollectionByIdOperacaoMae");
                    this._OrdemTesourariaCollectionByIdOperacaoMae = null;

                }
            }
        }

        private OrdemTesourariaCollection _OrdemTesourariaCollectionByIdOperacaoMae;
        #endregion


        #region UpToOrdemTesourariaByIdOperacaoMae - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - FK_OrdemTesouraria_OrdemTesouraria1
        /// </summary>

        [XmlIgnore]
        public OrdemTesouraria UpToOrdemTesourariaByIdOperacaoMae
        {
            get
            {
                if (this._UpToOrdemTesourariaByIdOperacaoMae == null
                    && IdOperacaoMae != null)
                {
                    this._UpToOrdemTesourariaByIdOperacaoMae = new OrdemTesouraria();
                    this._UpToOrdemTesourariaByIdOperacaoMae.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToOrdemTesourariaByIdOperacaoMae", this._UpToOrdemTesourariaByIdOperacaoMae);
                    this._UpToOrdemTesourariaByIdOperacaoMae.Query.Where(this._UpToOrdemTesourariaByIdOperacaoMae.Query.IdOperacao == this.IdOperacaoMae);
                    this._UpToOrdemTesourariaByIdOperacaoMae.Query.Load();
                }

                return this._UpToOrdemTesourariaByIdOperacaoMae;
            }

            set
            {
                this.RemovePreSave("UpToOrdemTesourariaByIdOperacaoMae");


                if (value == null)
                {
                    this.IdOperacaoMae = null;
                    this._UpToOrdemTesourariaByIdOperacaoMae = null;
                }
                else
                {
                    this.IdOperacaoMae = value.IdOperacao;
                    this._UpToOrdemTesourariaByIdOperacaoMae = value;
                    this.SetPreSave("UpToOrdemTesourariaByIdOperacaoMae", this._UpToOrdemTesourariaByIdOperacaoMae);
                }

            }
        }
        #endregion



        /// <summary>
        /// Used internally by the entity's hierarchical properties.
        /// </summary>
        protected override List<esPropertyDescriptor> GetHierarchicalProperties()
        {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();

            props.Add(new esPropertyDescriptor(this, "OrdemTesourariaCollectionByIdOperacaoMae", typeof(OrdemTesourariaCollection), new OrdemTesouraria()));

            return props;
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PreSave.
        /// </summary>
        protected override void ApplyPreSaveKeys()
        {
            if (!this.es.IsDeleted && this._UpToOrdemTesourariaByIdOperacaoMae != null)
            {
                this.IdOperacaoMae = this._UpToOrdemTesourariaByIdOperacaoMae.IdOperacao;
            }
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostSave.
        /// </summary>
        protected override void ApplyPostSaveKeys()
        {
            if (this._OrdemTesourariaCollectionByIdOperacaoMae != null)
            {
                foreach (OrdemTesouraria obj in this._OrdemTesourariaCollectionByIdOperacaoMae)
                {
                    if (obj.es.IsAdded)
                    {
                        obj.IdOperacaoMae = this.IdOperacao;
                    }
                }
            }
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostOneToOneSave.
        /// </summary>
        protected override void ApplyPostOneSaveKeys()
        {
        }

    }



    [Serializable]
    abstract public class esOrdemTesourariaQuery : esDynamicQuery
    {
        override protected IMetadata Meta
        {
            get
            {
                return OrdemTesourariaMetadata.Meta();
            }
        }


        public esQueryItem IdOperacao
        {
            get
            {
                return new esQueryItem(this, OrdemTesourariaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
            }
        }

        public esQueryItem IdOperacaoMae
        {
            get
            {
                return new esQueryItem(this, OrdemTesourariaMetadata.ColumnNames.IdOperacaoMae, esSystemType.Int32);
            }
        }

        public esQueryItem IdCarteira
        {
            get
            {
                return new esQueryItem(this, OrdemTesourariaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
            }
        }

        public esQueryItem IdCotista
        {
            get
            {
                return new esQueryItem(this, OrdemTesourariaMetadata.ColumnNames.IdCotista, esSystemType.Int32);
            }
        }

        public esQueryItem DataOperacao
        {
            get
            {
                return new esQueryItem(this, OrdemTesourariaMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
            }
        }

        public esQueryItem TipoOperacao
        {
            get
            {
                return new esQueryItem(this, OrdemTesourariaMetadata.ColumnNames.TipoOperacao, esSystemType.Byte);
            }
        }

        public esQueryItem Status
        {
            get
            {
                return new esQueryItem(this, OrdemTesourariaMetadata.ColumnNames.Status, esSystemType.Byte);
            }
        }

        public esQueryItem Valor
        {
            get
            {
                return new esQueryItem(this, OrdemTesourariaMetadata.ColumnNames.Valor, esSystemType.Decimal);
            }
        }

        public esQueryItem IdFormaLiquidacao
        {
            get
            {
                return new esQueryItem(this, OrdemTesourariaMetadata.ColumnNames.IdFormaLiquidacao, esSystemType.Byte);
            }
        }

        public esQueryItem IdConta
        {
            get
            {
                return new esQueryItem(this, OrdemTesourariaMetadata.ColumnNames.IdConta, esSystemType.Int32);
            }
        }

        public esQueryItem Observacao
        {
            get
            {
                return new esQueryItem(this, OrdemTesourariaMetadata.ColumnNames.Observacao, esSystemType.String);
            }
        }

    }



    [Serializable]
    [XmlType("OrdemTesourariaCollection")]
    public partial class OrdemTesourariaCollection : esOrdemTesourariaCollection, IEnumerable<OrdemTesouraria>
    {
        public OrdemTesourariaCollection()
        {

        }

        public static implicit operator List<OrdemTesouraria>(OrdemTesourariaCollection coll)
        {
            List<OrdemTesouraria> list = new List<OrdemTesouraria>();

            foreach (OrdemTesouraria emp in coll)
            {
                list.Add(emp);
            }

            return list;
        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return OrdemTesourariaMetadata.Meta();
            }
        }



        override protected esDynamicQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new OrdemTesourariaQuery();
                this.InitQuery(query);
            }
            return this.query;
        }

        override protected esEntity CreateEntityForCollection(DataRow row)
        {
            return new OrdemTesouraria(row);
        }

        override protected esEntity CreateEntity()
        {
            return new OrdemTesouraria();
        }


        #endregion


        [BrowsableAttribute(false)]
        public OrdemTesourariaQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new OrdemTesourariaQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }

        public bool Load(OrdemTesourariaQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        public OrdemTesouraria AddNew()
        {
            OrdemTesouraria entity = base.AddNewEntity() as OrdemTesouraria;

            return entity;
        }

        public OrdemTesouraria FindByPrimaryKey(System.Int32 idOperacao)
        {
            return base.FindByPrimaryKey(idOperacao) as OrdemTesouraria;
        }


        #region IEnumerable<OrdemTesouraria> Members

        IEnumerator<OrdemTesouraria> IEnumerable<OrdemTesouraria>.GetEnumerator()
        {
            System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
            System.Collections.IEnumerator iterator = enumer.GetEnumerator();

            while (iterator.MoveNext())
            {
                yield return iterator.Current as OrdemTesouraria;
            }
        }

        #endregion

        private OrdemTesourariaQuery query;
    }


    /// <summary>
    /// Encapsulates the 'OrdemTesouraria' table
    /// </summary>

    [Serializable]
    public partial class OrdemTesouraria : esOrdemTesouraria
    {
        public OrdemTesouraria()
        {

        }

        public OrdemTesouraria(DataRow row)
            : base(row)
        {

        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return OrdemTesourariaMetadata.Meta();
            }
        }



        override protected esOrdemTesourariaQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new OrdemTesourariaQuery();
                this.InitQuery(query);
            }
            return this.query;
        }
        #endregion




        [BrowsableAttribute(false)]
        public OrdemTesourariaQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new OrdemTesourariaQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }


        public bool Load(OrdemTesourariaQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        private OrdemTesourariaQuery query;
    }



    [Serializable]
    public partial class OrdemTesourariaQuery : esOrdemTesourariaQuery
    {
        public OrdemTesourariaQuery()
        {

        }

        public OrdemTesourariaQuery(string joinAlias)
        {
            this.es.JoinAlias = joinAlias;
        }


    }



    [Serializable]
    public partial class OrdemTesourariaMetadata : esMetadata, IMetadata
    {
        #region Protected Constructor
        protected OrdemTesourariaMetadata()
        {
            _columns = new esColumnMetadataCollection();
            esColumnMetadata c;

            c = new esColumnMetadata(OrdemTesourariaMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = OrdemTesourariaMetadata.PropertyNames.IdOperacao;
            c.IsInPrimaryKey = true;
            c.IsAutoIncrement = true;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(OrdemTesourariaMetadata.ColumnNames.IdOperacaoMae, 1, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = OrdemTesourariaMetadata.PropertyNames.IdOperacaoMae;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(OrdemTesourariaMetadata.ColumnNames.IdCarteira, 2, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = OrdemTesourariaMetadata.PropertyNames.IdCarteira;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(OrdemTesourariaMetadata.ColumnNames.IdCotista, 3, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = OrdemTesourariaMetadata.PropertyNames.IdCotista;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(OrdemTesourariaMetadata.ColumnNames.DataOperacao, 4, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = OrdemTesourariaMetadata.PropertyNames.DataOperacao;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(OrdemTesourariaMetadata.ColumnNames.TipoOperacao, 5, typeof(System.Byte), esSystemType.Byte);
            c.PropertyName = OrdemTesourariaMetadata.PropertyNames.TipoOperacao;
            c.NumericPrecision = 3;
            _columns.Add(c);


            c = new esColumnMetadata(OrdemTesourariaMetadata.ColumnNames.Status, 6, typeof(System.Byte), esSystemType.Byte);
            c.PropertyName = OrdemTesourariaMetadata.PropertyNames.Status;
            c.NumericPrecision = 3;
            _columns.Add(c);


            c = new esColumnMetadata(OrdemTesourariaMetadata.ColumnNames.Valor, 7, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = OrdemTesourariaMetadata.PropertyNames.Valor;
            c.NumericPrecision = 16;
            c.NumericScale = 2;
            _columns.Add(c);


            c = new esColumnMetadata(OrdemTesourariaMetadata.ColumnNames.IdFormaLiquidacao, 8, typeof(System.Byte), esSystemType.Byte);
            c.PropertyName = OrdemTesourariaMetadata.PropertyNames.IdFormaLiquidacao;
            c.NumericPrecision = 3;
            _columns.Add(c);


            c = new esColumnMetadata(OrdemTesourariaMetadata.ColumnNames.IdConta, 9, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = OrdemTesourariaMetadata.PropertyNames.IdConta;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(OrdemTesourariaMetadata.ColumnNames.Observacao, 10, typeof(System.String), esSystemType.String);
            c.PropertyName = OrdemTesourariaMetadata.PropertyNames.Observacao;
            c.CharacterMaxLength = 400;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


        }
        #endregion

        static public OrdemTesourariaMetadata Meta()
        {
            return meta;
        }

        public Guid DataID
        {
            get { return base._dataID; }
        }

        public bool MultiProviderMode
        {
            get { return false; }
        }

        public esColumnMetadataCollection Columns
        {
            get { return base._columns; }
        }

        #region ColumnNames
        public class ColumnNames
        {
            public const string IdOperacao = "IdOperacao";
            public const string IdOperacaoMae = "IdOperacaoMae";
            public const string IdCarteira = "IdCarteira";
            public const string IdCotista = "IdCotista";
            public const string DataOperacao = "DataOperacao";
            public const string TipoOperacao = "TipoOperacao";
            public const string Status = "Status";
            public const string Valor = "Valor";
            public const string IdFormaLiquidacao = "IdFormaLiquidacao";
            public const string IdConta = "IdConta";
            public const string Observacao = "Observacao";
        }
        #endregion

        #region PropertyNames
        public class PropertyNames
        {
            public const string IdOperacao = "IdOperacao";
            public const string IdOperacaoMae = "IdOperacaoMae";
            public const string IdCarteira = "IdCarteira";
            public const string IdCotista = "IdCotista";
            public const string DataOperacao = "DataOperacao";
            public const string TipoOperacao = "TipoOperacao";
            public const string Status = "Status";
            public const string Valor = "Valor";
            public const string IdFormaLiquidacao = "IdFormaLiquidacao";
            public const string IdConta = "IdConta";
            public const string Observacao = "Observacao";
        }
        #endregion

        public esProviderSpecificMetadata GetProviderMetadata(string mapName)
        {
            MapToMeta mapMethod = mapDelegates[mapName];

            if (mapMethod != null)
                return mapMethod(mapName);
            else
                return null;
        }

        #region MAP esDefault

        static private int RegisterDelegateesDefault()
        {
            // This is only executed once per the life of the application
            lock (typeof(OrdemTesourariaMetadata))
            {
                if (OrdemTesourariaMetadata.mapDelegates == null)
                {
                    OrdemTesourariaMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
                }

                if (OrdemTesourariaMetadata.meta == null)
                {
                    OrdemTesourariaMetadata.meta = new OrdemTesourariaMetadata();
                }

                MapToMeta mapMethod = new MapToMeta(meta.esDefault);
                mapDelegates.Add("esDefault", mapMethod);
                mapMethod("esDefault");
            }
            return 0;
        }

        private esProviderSpecificMetadata esDefault(string mapName)
        {
            if (!_providerMetadataMaps.ContainsKey(mapName))
            {
                esProviderSpecificMetadata meta = new esProviderSpecificMetadata();


                meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("IdOperacaoMae", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
                meta.AddTypeMap("TipoOperacao", new esTypeMap("tinyint", "System.Byte"));
                meta.AddTypeMap("Status", new esTypeMap("tinyint", "System.Byte"));
                meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("IdFormaLiquidacao", new esTypeMap("tinyint", "System.Byte"));
                meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("Observacao", new esTypeMap("varchar", "System.String"));



                meta.Source = "OrdemTesouraria";
                meta.Destination = "OrdemTesouraria";

                meta.spInsert = "proc_OrdemTesourariaInsert";
                meta.spUpdate = "proc_OrdemTesourariaUpdate";
                meta.spDelete = "proc_OrdemTesourariaDelete";
                meta.spLoadAll = "proc_OrdemTesourariaLoadAll";
                meta.spLoadByPrimaryKey = "proc_OrdemTesourariaLoadByPrimaryKey";

                this._providerMetadataMaps["esDefault"] = meta;
            }

            return this._providerMetadataMaps["esDefault"];
        }

        #endregion

        static private OrdemTesourariaMetadata meta;
        static protected Dictionary<string, MapToMeta> mapDelegates;
        static private int _esDefault = RegisterDelegateesDefault();
    }
}
