﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/04/2014 12:35:24
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using System.Web;
using Financial.Fundo;
using Financial.InvestidorCotista.Enums;
using Financial.CRM;
using Financial.InvestidorCotista;
using Financial.Security;
using System.Net.Mail;
using System.Configuration;
using Financial.Investidor;
using Financial.Security.Enums;
using Financial.ContaCorrente;
using Financial.Util;
using Financial.Fundo.Enums;

namespace Financial.InterfacesCustom
{
    public partial class OrdemTesouraria : esOrdemTesouraria
    {
        public void EnviarEmailInstitucional(HttpContext context, Carteira carteira, byte situacaoEnvio)
        {
            DateTime dataLiquidacao = this.CalculaDataLiquidacao(carteira);

            int idCotista = this.IdCotista.Value;
            int idCarteira = this.IdCarteira.Value;
            DateTime dataOperacao = this.DataOperacao.Value;
            byte tipoOperacao = this.TipoOperacao.Value;
            bool isAplicacao = tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao;
            bool isResgateTotal = tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal;
            string descricaoResgate = isResgateTotal ? "resgate total" : "resgate";
            string tipoOperacaoDescricao = isAplicacao ? "aplicação" : descricaoResgate;

            decimal valor = this.Valor.Value;

            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(idCotista);

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            #region Avisar por Email
            //Avisar por email se estiver configurado
            bool envieiEmail = false;
            string errosSendMail = "";
            PermissaoOperacaoFundo permissaoOperacaoFundo = new PermissaoOperacaoFundo();

            if (permissaoOperacaoFundo.LoadByPrimaryKey(idCotista, idCarteira) &&
                !String.IsNullOrEmpty(permissaoOperacaoFundo.Email) && 
                (permissaoOperacaoFundo.SituacaoEnvio.Value == (byte)SituacaoEnvio.Todas || 
                    permissaoOperacaoFundo.SituacaoEnvio.Value == situacaoEnvio))
            {

                envieiEmail = true;

                const string SUBJECT_DEFAULT_CADASTRO_OPERACAO = "Boleta - \"{NomeCotista}\"";
                const string BODY_DEFAULT_CADASTRO_OPERACAO = "Prezados,<br /><br />Segue movimento do cotista: {NomeCotista} - CNPJ: {CpfCnpjCotista}<br /><br />Data da Solicitação: {Now}<br />Cotista: {NomeCotista}<br />Movimento: {TipoOperacao}<br />Valor: {Valor}<br />{TipoLiquidacao}Fundo: {NomeFundo}<br />Data Liquidação: {DataLiquidacao}<br />{ContaCorrenteLiquidacao}";
                const string FROM_DEFAULT = "suporte@financialonline.com.br";

                string subject = ConfigurationManager.AppSettings["SubjectCadastroOperacao"];
                if (String.IsNullOrEmpty(subject))
                {
                    subject = SUBJECT_DEFAULT_CADASTRO_OPERACAO;
                }

                string body = ConfigurationManager.AppSettings["BodyCadastroOperacao"];
                if (String.IsNullOrEmpty(body))
                {
                    body = BODY_DEFAULT_CADASTRO_OPERACAO;
                }

                if (isResgateTotal)
                {
                    //Remover info de valor se for um Resgate Total
                    body = body.Replace("Valor: {Valor}<br />", "");
                }

                string from = ConfigurationManager.AppSettings["FromCadastroOperacao"];
                if (String.IsNullOrEmpty(from))
                {
                    from = FROM_DEFAULT;
                }

                MailMessage mail = new MailMessage();
                string sendTo = permissaoOperacaoFundo.Email.Replace(';', ',');
                mail.To.Add(sendTo);
                mail.From = new MailAddress(from);

                string tipoOperacaoDescricaoFormatada = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.
        ToTitleCase(tipoOperacaoDescricao);

                string tipoLiquidacao = "";
                if (this.IdFormaLiquidacao.HasValue)
                {
                    FormaLiquidacao formaLiquidacao = new FormaLiquidacao();
                    formaLiquidacao.LoadByPrimaryKey(this.IdFormaLiquidacao.Value);
                    tipoLiquidacao = String.Format("Tipo de Liquidação: {0}<br />", formaLiquidacao.Descricao);
                }



                string contaCorrenteLiquidacao = "";
                if (this.IdConta.HasValue)
                {
                    Financial.Investidor.ContaCorrente contaCorrente = new Financial.Investidor.ContaCorrente();
                    contaCorrente.LoadByPrimaryKey(this.IdConta.Value);

                    string bancoNome = "";
                    if (contaCorrente.IdBanco.HasValue)
                    {
                        Banco banco = new Banco();
                        banco.LoadByPrimaryKey(contaCorrente.IdBanco.Value);
                        bancoNome = banco.Nome;
                    }

                    string agenciaCodigo = "";
                    if (contaCorrente.IdAgencia.HasValue)
                    {
                        Agencia agencia = new Agencia();
                        agencia.LoadByPrimaryKey(contaCorrente.IdAgencia.Value);
                        agenciaCodigo = agencia.Codigo;
                    }

                    contaCorrenteLiquidacao = String.Format("<br />Banco: {0}<br />Ag.: {1}<br />C/C: {2}-{3}",
                        bancoNome, agenciaCodigo,
                        contaCorrente.Numero, contaCorrente.DigitoConta);
                }



                string[] placeholders = { "{TipoOperacao}", "{NomeFundo}", 
            "{DataOperacao}", "{Valor}", "{NomeCotista}", "{Now}", "{DataLiquidacao}",
                "{TipoLiquidacao}", "{ContaCorrenteLiquidacao}","{CpfCnpjCotista}"};


                string[] placeholdersValues = { tipoOperacaoDescricaoFormatada, carteira.Nome,
            dataOperacao.ToString("d/MM/yyyy"), valor.ToString("#,##0.00"), 
                cotista.Nome, DateTime.Today.ToString("d/MM/yyyy"), 
                dataLiquidacao.ToString("d/MM/yyyy"), tipoLiquidacao, contaCorrenteLiquidacao, 
                pessoa.Cpfcnpj};


                for (int i = 0; i < placeholders.Length; i++)
                {
                    subject = subject.Replace(placeholders[i], placeholdersValues[i]);
                    body = body.Replace(placeholders[i], placeholdersValues[i]);
                }

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient();

                /*if (from.Contains("@financialonline.com.br"))
                {
                    smtp = new SmtpClient("smtp.gmail.com", 587);
                    smtp.Credentials = new System.Net.NetworkCredential("efreitasrj@gmail.com", "Verbatim2");
                    smtp.EnableSsl = true;
                }*/

                string smtpEnableSSL = ConfigurationManager.AppSettings["SMTPEnableSSL"];

                if (smtpEnableSSL == "true")
                {
                    smtp.EnableSsl = true;
                }

                try
                {
                    smtp.Send(mail);
                }
                catch (Exception e)
                {
                    errosSendMail = e.Message;
                    envieiEmail = false;
                }

            }
            #endregion

            string envieiEmailString = envieiEmail ? "Email enviado" : "Email não enviado - erro: " + errosSendMail;
            string descricaoHistorico = String.Format("Portal Cliente: Envio de email: {0} (Cotista: {1}, Fundo: {2})", envieiEmailString, idCotista, idCarteira);

            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            descricaoHistorico,
                                            HttpContext.Current.User.Identity.Name,
                                            "",
                                            "",
                                            HistoricoLogOrigem.PortalInstitucional);

        }

        public void EnviarEmailBackOffice(HttpContext context, Carteira carteira, string descricaoTriggerEmail)
        {

            if (String.IsNullOrEmpty(Financial.Util.ParametrosConfiguracaoSistema.Outras.EmailBackOfficeInstitucional))
            {
                return;
            }

            string sendTo = Financial.Util.ParametrosConfiguracaoSistema.Outras.EmailBackOfficeInstitucional.Replace(';', ',');

            DateTime dataLiquidacao = this.CalculaDataLiquidacao(carteira);

            int idCotista = this.IdCotista.Value;
            int idCarteira = this.IdCarteira.Value;
            DateTime dataOperacao = this.DataOperacao.Value;
            byte tipoOperacao = this.TipoOperacao.Value;
            bool isAplicacao = tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao;
            bool isResgateTotal = tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal;
            string descricaoResgate = isResgateTotal ? "resgate total" : "resgate";
            string tipoOperacaoDescricao = isAplicacao ? "aplicação" : descricaoResgate;

            decimal valor = this.Valor.Value;

            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(idCotista);

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            #region Avisar por Email
            //Avisar por email se estiver configurado
            bool envieiEmail = false;
            string errosSendMail = "";

            envieiEmail = true;

            const string SUBJECT_DEFAULT_CADASTRO_OPERACAO = "{DescricaoTriggerEmail} - \"{NomeCotista}\"";
            const string BODY_DEFAULT_CADASTRO_OPERACAO = "Prezados,<br /><br />Segue movimento do cotista: {NomeCotista} - CNPJ: {CpfCnpjCotista}<br /><br />Data da Solicitação: {Now}<br />Cotista: {NomeCotista}<br />Movimento: {TipoOperacao}<br />Valor: {Valor}<br />{TipoLiquidacao}Fundo: {NomeFundo}<br />Data Liquidação: {DataLiquidacao}<br />{ContaCorrenteLiquidacao}";
            const string FROM_DEFAULT = "suporte@financialonline.com.br";

            string subject = ConfigurationManager.AppSettings["SubjectCadastroOperacao"];
            if (String.IsNullOrEmpty(subject))
            {
                subject = SUBJECT_DEFAULT_CADASTRO_OPERACAO;
            }

            string body = ConfigurationManager.AppSettings["BodyCadastroOperacao"];
            if (String.IsNullOrEmpty(body))
            {
                body = BODY_DEFAULT_CADASTRO_OPERACAO;
            }

            if (isResgateTotal)
            {
                //Remover info de valor se for um Resgate Total
                body = body.Replace("Valor: {Valor}<br />", "");
            }

            string from = ConfigurationManager.AppSettings["FromCadastroOperacao"];
            if (String.IsNullOrEmpty(from))
            {
                from = FROM_DEFAULT;
            }

            MailMessage mail = new MailMessage();
            
            mail.To.Add(sendTo);
            mail.From = new MailAddress(from);

            string tipoOperacaoDescricaoFormatada = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.
    ToTitleCase(tipoOperacaoDescricao);

            string tipoLiquidacao = "";
            if (this.IdFormaLiquidacao.HasValue)
            {
                FormaLiquidacao formaLiquidacao = new FormaLiquidacao();
                formaLiquidacao.LoadByPrimaryKey(this.IdFormaLiquidacao.Value);
                tipoLiquidacao = String.Format("Tipo de Liquidação: {0}<br />", formaLiquidacao.Descricao);
            }



            string contaCorrenteLiquidacao = "";
            if (this.IdConta.HasValue)
            {
                Financial.Investidor.ContaCorrente contaCorrente = new Financial.Investidor.ContaCorrente();
                contaCorrente.LoadByPrimaryKey(this.IdConta.Value);

                string bancoNome = "";
                if (contaCorrente.IdBanco.HasValue)
                {
                    Banco banco = new Banco();
                    banco.LoadByPrimaryKey(contaCorrente.IdBanco.Value);
                    bancoNome = banco.Nome;
                }

                string agenciaCodigo = "";
                if (contaCorrente.IdAgencia.HasValue)
                {
                    Agencia agencia = new Agencia();
                    agencia.LoadByPrimaryKey(contaCorrente.IdAgencia.Value);
                    agenciaCodigo = agencia.Codigo;
                }

                contaCorrenteLiquidacao = String.Format("<br />Banco: {0}<br />Ag.: {1}<br />C/C: {2}-{3}",
                    bancoNome, agenciaCodigo,
                    contaCorrente.Numero, contaCorrente.DigitoConta);
            }



            string[] placeholders = { "{TipoOperacao}", "{NomeFundo}", 
            "{DataOperacao}", "{Valor}", "{NomeCotista}", "{Now}", "{DataLiquidacao}",
                "{TipoLiquidacao}", "{ContaCorrenteLiquidacao}","{CpfCnpjCotista}", "{DescricaoTriggerEmail}"};


            string[] placeholdersValues = { tipoOperacaoDescricaoFormatada, carteira.Nome,
            dataOperacao.ToString("d/MM/yyyy"), valor.ToString("#,##0.00"), 
                cotista.Nome, DateTime.Today.ToString("d/MM/yyyy"), 
                dataLiquidacao.ToString("d/MM/yyyy"), tipoLiquidacao, contaCorrenteLiquidacao, 
                pessoa.Cpfcnpj, descricaoTriggerEmail};


            for (int i = 0; i < placeholders.Length; i++)
            {
                subject = subject.Replace(placeholders[i], placeholdersValues[i]);
                body = body.Replace(placeholders[i], placeholdersValues[i]);
            }

            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();

            /*if (from.Contains("@financialonline.com.br"))
            {
                smtp = new SmtpClient("smtp.gmail.com", 587);
                smtp.Credentials = new System.Net.NetworkCredential("efreitasrj@gmail.com", "Verbatim2");
                smtp.EnableSsl = true;
            }*/

            string smtpEnableSSL = ConfigurationManager.AppSettings["SMTPEnableSSL"];

            if (smtpEnableSSL == "true")
            {
                smtp.EnableSsl = true;
            }

            try
            {
                smtp.Send(mail);
            }
            catch (Exception e)
            {
                errosSendMail = e.Message;
                envieiEmail = false;
            }


            #endregion

        }

        public void EnviarEmailBackOffice(HttpContext context, string descricaoTriggerEmail)
        {

            if (String.IsNullOrEmpty(Financial.Util.ParametrosConfiguracaoSistema.Outras.EmailBackOfficeInstitucional))
            {
                return;
            }

            string sendTo = Financial.Util.ParametrosConfiguracaoSistema.Outras.EmailBackOfficeInstitucional.Replace(';', ',');

            int idCotista = this.IdCotista.Value;
            DateTime dataOperacao = this.DataOperacao.Value;
            byte tipoOperacao = this.TipoOperacao.Value;
            bool isAplicacao = tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao;
            bool isResgateTotal = tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal;
            string descricaoResgate = isResgateTotal ? "resgate total" : "resgate";
            string tipoOperacaoDescricao = isAplicacao ? "aplicação" : descricaoResgate;

            decimal valor = this.Valor.Value;

            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(idCotista);

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            #region Avisar por Email
            //Avisar por email se estiver configurado
            bool envieiEmail = false;
            string errosSendMail = "";

            envieiEmail = true;

            const string SUBJECT_DEFAULT_CADASTRO_OPERACAO = "{DescricaoTriggerEmail} - \"{NomeCotista}\"";
            const string BODY_DEFAULT_CADASTRO_OPERACAO = "Prezados,<br /><br />Segue movimento do cotista: {NomeCotista} - CNPJ: {CpfCnpjCotista}<br /><br />Data da Solicitação: {Now}<br />Cotista: {NomeCotista}<br />Movimento: {TipoOperacao}<br />Valor: {Valor}<br />{TipoLiquidacao}{ContaCorrenteLiquidacao}";
            const string FROM_DEFAULT = "suporte@financialonline.com.br";

            string subject = ConfigurationManager.AppSettings["SubjectCadastroOperacao"];
            if (String.IsNullOrEmpty(subject))
            {
                subject = SUBJECT_DEFAULT_CADASTRO_OPERACAO;
            }

            string body = ConfigurationManager.AppSettings["BodyCadastroOperacao"];
            if (String.IsNullOrEmpty(body))
            {
                body = BODY_DEFAULT_CADASTRO_OPERACAO;
            }

            if (isResgateTotal)
            {
                //Remover info de valor se for um Resgate Total
                body = body.Replace("Valor: {Valor}<br />", "");
            }

            string from = ConfigurationManager.AppSettings["FromCadastroOperacao"];
            if (String.IsNullOrEmpty(from))
            {
                from = FROM_DEFAULT;
            }

            MailMessage mail = new MailMessage();
            
            mail.To.Add(sendTo);
            mail.From = new MailAddress(from);

            string tipoOperacaoDescricaoFormatada = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.
    ToTitleCase(tipoOperacaoDescricao);

            string tipoLiquidacao = "";
            if (this.IdFormaLiquidacao.HasValue)
            {
                FormaLiquidacao formaLiquidacao = new FormaLiquidacao();
                formaLiquidacao.LoadByPrimaryKey(this.IdFormaLiquidacao.Value);
                tipoLiquidacao = String.Format("Tipo de Liquidação: {0}<br />", formaLiquidacao.Descricao);
            }



            string contaCorrenteLiquidacao = "";
            if (this.IdConta.HasValue)
            {
                Financial.Investidor.ContaCorrente contaCorrente = new Financial.Investidor.ContaCorrente();
                contaCorrente.LoadByPrimaryKey(this.IdConta.Value);

                string bancoNome = "";
                if (contaCorrente.IdBanco.HasValue)
                {
                    Banco banco = new Banco();
                    banco.LoadByPrimaryKey(contaCorrente.IdBanco.Value);
                    bancoNome = banco.Nome;
                }

                string agenciaCodigo = "";
                if (contaCorrente.IdAgencia.HasValue)
                {
                    Agencia agencia = new Agencia();
                    agencia.LoadByPrimaryKey(contaCorrente.IdAgencia.Value);
                    agenciaCodigo = agencia.Codigo;
                }

                contaCorrenteLiquidacao = String.Format("<br />Banco: {0}<br />Ag.: {1}<br />C/C: {2}-{3}",
                    bancoNome, agenciaCodigo,
                    contaCorrente.Numero, contaCorrente.DigitoConta);
            }



            string[] placeholders = { "{TipoOperacao}",  
            "{DataOperacao}", "{Valor}", "{NomeCotista}", "{Now}", 
                "{TipoLiquidacao}", "{ContaCorrenteLiquidacao}","{CpfCnpjCotista}", "{DescricaoTriggerEmail}"};


            string[] placeholdersValues = { tipoOperacaoDescricaoFormatada, 
            dataOperacao.ToString("d/MM/yyyy"), valor.ToString("#,##0.00"), 
                cotista.Nome, DateTime.Today.ToString("d/MM/yyyy"), 
                tipoLiquidacao, contaCorrenteLiquidacao, 
                pessoa.Cpfcnpj, descricaoTriggerEmail};


            for (int i = 0; i < placeholders.Length; i++)
            {
                subject = subject.Replace(placeholders[i], placeholdersValues[i]);
                body = body.Replace(placeholders[i], placeholdersValues[i]);
            }

            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();

            /*if (from.Contains("@financialonline.com.br"))
            {
                smtp = new SmtpClient("smtp.gmail.com", 587);
                smtp.Credentials = new System.Net.NetworkCredential("efreitasrj@gmail.com", "Verbatim2");
                smtp.EnableSsl = true;
            }*/

            string smtpEnableSSL = ConfigurationManager.AppSettings["SMTPEnableSSL"];

            if (smtpEnableSSL == "true")
            {
                smtp.EnableSsl = true;
            }

            try
            {
                smtp.Send(mail);
            }
            catch (Exception e)
            {
                errosSendMail = e.Message;
                envieiEmail = false;
            }


            #endregion

        }

        private DateTime CalculaDataLiquidacao(Carteira carteira)
        {

            DateTime dataConversao = new DateTime();
            if (this.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao ||
                this.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial)
            {
                dataConversao = Calendario.AdicionaDiaUtil(this.DataOperacao.Value, carteira.DiasCotizacaoAplicacao.Value);
            }
            else if (this.TipoOperacao.Value != (byte)TipoOperacaoCotista.AplicacaoCotasEspecial)
            {
                if (carteira.ContagemDiasConversaoResgate.Value == (byte)ContagemDiasLiquidacaoResgate.DiasUteis)
                {
                    dataConversao = Calendario.AdicionaDiaUtil(this.DataOperacao.Value, carteira.DiasCotizacaoResgate.Value);
                }
                else
                {
                    dataConversao = this.DataOperacao.Value.AddDays(carteira.DiasCotizacaoResgate.Value);

                    if (!Calendario.IsDiaUtil(dataConversao))
                    {
                        dataConversao = Calendario.AdicionaDiaUtil(dataConversao, 1);
                    }
                }
            }

            DateTime dataLiquidacao = new DateTime();
            if (this.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao ||
                this.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial)
            {
                dataLiquidacao = Calendario.AdicionaDiaUtil(this.DataOperacao.Value, carteira.DiasLiquidacaoAplicacao.Value);
            }
            else if (this.TipoOperacao.Value != (byte)TipoOperacaoCotista.AplicacaoCotasEspecial)
            {
                if (carteira.ContagemDiasConversaoResgate.Value == (byte)ContagemDiasLiquidacaoResgate.DiasUteis)
                {
                    dataLiquidacao = Calendario.AdicionaDiaUtil(this.DataOperacao.Value, carteira.DiasLiquidacaoResgate.Value);
                }
                else
                {
                    //Conta por Dias Úteis em cima da data de conversão
                    dataLiquidacao = Calendario.AdicionaDiaUtil(dataConversao, carteira.DiasLiquidacaoResgate.Value);
                }
            }
            return dataLiquidacao;
        }

    }

}
