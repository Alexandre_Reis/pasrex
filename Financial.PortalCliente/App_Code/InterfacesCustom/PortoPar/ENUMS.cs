using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for ENUMS
/// </summary>
namespace Financial.InterfacesCustom.PortoPar
{
    public enum StatusOrdemTesourariaPorto
    {
        Digitado = 1,
        Aprovado = 2,
        Rejeitado = 3,
        Distribuido = 4,
        PGBLIntegrado = 100,
        PGBLAprovado = 101,
        PGBLRejeitado = 102
    }

    /*public enum StatusOrdemIntegracao
    {
        Digitado = 10,
        Cancelado = 20,
        ItauOK = 30,
        SemSaldo = 40,
        Processado = 50 //Manter esta entrada apesar da Ordem nao assumir este status. Sinaliza que deve ser procurada a operacao correspondente
    }*/
}