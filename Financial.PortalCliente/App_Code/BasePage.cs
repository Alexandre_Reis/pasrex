﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Text;
using System.Drawing;
using System.Collections.Generic;

using Financial.Web.Util;

using Financial.Util;
using Financial.Security;
using System.Net.Mail;
using System.Web.Configuration;
using System.Net.Configuration;
using System.Globalization;
using Financial.Security.Enums;
using Financial.WebConfigConfiguration;

namespace Financial.Web.Common
{

    /// <summary>
    ///     Classe Base para Todas as Paginas Web
    /// </summary>
    public class BasePage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HtmlLink link = new HtmlLink();
            link.Href = "~/imagensPersonalizadas/favicon.ico";
            link.Attributes.Add("rel", "SHORTCUT ICON");
            link.Attributes.Add("type", "image/x-icon");
            this.Page.Header.Controls.Add(link);

            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/Login/LoginInit.aspx");
                return;
            }
            else
            {

                // Se login = master passa reto
                //if (new FinancialMembershipProvider().IsUserMaster(HttpContext.Current.User.Identity.Name)) {
                if (Convert.ToBoolean(Session["IsUserMaster"]) == true)
                {
                    string dirApp = DiretorioAplicacao.DiretorioBaseAplicacao.Replace("/", "");
                    Session["CurrentPage"] = dirApp == HttpContext.Current.Request.Url.Segments[1]
                                             ? HttpContext.Current.Request.Url.LocalPath.Replace(HttpContext.Current.Request.Url.Segments[0] + HttpContext.Current.Request.Url.Segments[1], "")
                                             : HttpContext.Current.Request.Url.LocalPath;
                }
                else
                {

                    Usuario usuario = new Usuario();
                    usuario.Query.Where(usuario.Query.Login.Equal(HttpContext.Current.User.Identity.Name));

                    if (!usuario.Query.Load())
                    {
                        Response.Redirect("~/Login/LoginInit.aspx");
                        return;
                    }
                    else if (usuario.DataLogout.Value > usuario.DataUltimoLogin.Value)
                    {
                        GrupoUsuario grupoUsuario = new GrupoUsuario();
                        grupoUsuario.LoadByPrimaryKey(usuario.IdGrupo.Value);

                        if (grupoUsuario.TipoPerfil.Value != (byte)TipoPerfilGrupo.Administrador)
                        {
                            Response.Redirect("~/Login/LoginInit.aspx");
                            return;
                        }
                    }
                    else
                    {
                        string dirApp = DiretorioAplicacao.DiretorioBaseAplicacao.Replace("/", "");
                        if (dirApp == HttpContext.Current.Request.Url.Segments[1])
                        {
                            Session["CurrentPage"] = HttpContext.Current.Request.Url.LocalPath.Replace(HttpContext.Current.Request.Url.Segments[0] + HttpContext.Current.Request.Url.Segments[1], "");
                        }
                        else
                        {
                            Session["CurrentPage"] = HttpContext.Current.Request.Url.LocalPath;
                        }
                    }
                }

            }

            InitializeCulture();
        }

        /// <summary>
        ///     Inicializa Configurações especificas de Lingua
        ///     Função Automaticamente chamada pelo FrameWork
        /// </summary>
        protected override void InitializeCulture()
        {
            PersonalizeCulture.InicializaCulturePersonalizada();
            base.InitializeCulture();
        }
    }

}