using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Fundo;
using Financial.CRM;

public class CotistaViewModel
{
    public int IdCotista;
    public string Nome;
    public string Cpfcnpj;

    public CotistaViewModel()
    {
    }
    public CotistaViewModel(Cotista cotista)
    {
        this.IdCotista = cotista.IdCotista.Value;
        this.Nome = cotista.Nome;
        this.Cpfcnpj = cotista.Cpfcnpj;

    }
}
