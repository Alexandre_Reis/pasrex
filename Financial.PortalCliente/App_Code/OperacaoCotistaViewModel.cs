﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.InterfacesCustom.PortoPar;

/// <summary>
/// Summary description for OperacaoCotistaViewModel
/// </summary>
public class OperacaoCotistaViewModel
{
    public int IdOperacao;
    public int IdCotista;
    public int IdCarteira;
    public DateTime DataOperacao;
    public DateTime DataAgendamento;
    public Decimal ValorBruto;
    public Decimal ValorLiquido;
    public Decimal Valor;
    public int TipoOperacao;
    public string TipoOperacaoString;
    public string StatusProcessamento;
    public string NomeCarteira;
    public string NomeCotista;
    public string ContaCorrente;
    public string FormaLiquidacao;
    public string StatusCotizacao;

    public OperacaoCotistaViewModel()
    {
    }
    public OperacaoCotistaViewModel(OrdemCotista ordemCotista)
    {
        this.IdCarteira = ordemCotista.IdCarteira.Value;
        this.IdCotista = ordemCotista.IdCotista.Value;
        this.IdOperacao = ordemCotista.IdOperacao.Value;
        this.DataOperacao = ordemCotista.DataOperacao.Value;
        this.DataAgendamento = ordemCotista.DataAgendamento.Value;
        this.ValorBruto = ordemCotista.ValorBruto.Value;
        this.ValorLiquido = ordemCotista.ValorLiquido.Value;
        this.TipoOperacao = ordemCotista.TipoOperacao.Value;

        string tipoOperacao = "";
        if (ordemCotista.TipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
        {
            tipoOperacao = "Aplicação";
            this.Valor = this.ValorBruto;
        }
        else if (ordemCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
        {
            tipoOperacao = "Resgate Total";
            this.Valor = this.ValorLiquido;
        }
        else
        {
            tipoOperacao = "Resgate Parcial";
            this.Valor = this.ValorLiquido;
        }

        this.TipoOperacaoString = tipoOperacao;

        Cotista cotista = new Cotista();
        cotista.LoadByPrimaryKey(this.IdCotista);
        this.NomeCotista = cotista.Nome;

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(this.IdCarteira);
        string nomeFundo = cliente.Nome;
        this.NomeCarteira = nomeFundo;

        this.StatusCotizacao = this.DataOperacao < cliente.DataDia.Value ? "Efetivada" : "Pendente";

        string statusDescricao = "";
        if (ordemCotista.StatusIntegracao == (byte)StatusOrdemIntegracao.Agendado)
        {
            statusDescricao = "Agendada";
        }
        else if (ordemCotista.StatusIntegracao == (byte)StatusOrdemIntegracao.Cancelado)
        {
            statusDescricao = "Cancelada";
        }
        else if (ordemCotista.StatusIntegracao == (byte)StatusOrdemIntegracao.ItauOK)
        {
            statusDescricao = "Processando";
        }
        else if (ordemCotista.StatusIntegracao == (byte)StatusOrdemIntegracao.SemSaldo)
        {
            statusDescricao = "Saldo Insuficiente";
        }
        else if (ordemCotista.StatusIntegracao == (byte)StatusOrdemIntegracao.OutroErro)
        {
            statusDescricao = "Erro no Envio";
        }
        else if (ordemCotista.StatusIntegracao == (byte)StatusOrdemIntegracao.Processado)
        {
            statusDescricao = "Efetivada";
        }

        this.StatusProcessamento = statusDescricao;

        if (ordemCotista.TipoOperacao != (byte)TipoOperacaoCotista.Aplicacao)
        {
            Financial.ContaCorrente.FormaLiquidacao formaLiquidacao = new Financial.ContaCorrente.FormaLiquidacao();
            if (formaLiquidacao.LoadByPrimaryKey(ordemCotista.IdFormaLiquidacao.Value))
            {
                this.FormaLiquidacao = formaLiquidacao.Descricao;
            }
        }

        ContaCorrente contaCorrente = new ContaCorrente();
        if (ordemCotista.IdConta.HasValue && contaCorrente.LoadByPrimaryKey(ordemCotista.IdConta.Value))
        {
            ContaCorrenteViewModel contaCorrenteViewModel = new ContaCorrenteViewModel(contaCorrente);
            this.ContaCorrente = contaCorrenteViewModel.Descricao;
        }
    }

    public OperacaoCotistaViewModel(OperacaoCotista operacaoCotista)
    {
        this.IdCarteira = operacaoCotista.IdCarteira.Value;
        this.IdCotista = operacaoCotista.IdCotista.Value;
        this.IdOperacao = operacaoCotista.IdOperacao.Value;
        this.DataOperacao = operacaoCotista.DataOperacao.Value;
        this.DataAgendamento = operacaoCotista.DataAgendamento.Value;
        this.ValorBruto = operacaoCotista.ValorBruto.Value;
        this.ValorLiquido = operacaoCotista.ValorLiquido.Value;

        string tipoOperacao = "";
        if (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
        {
            tipoOperacao = "Aplicação";
            this.Valor = this.ValorBruto;
        }
        else if (operacaoCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
        {
            tipoOperacao = "Resgate Total";
            this.Valor = this.ValorLiquido;
        }
        else
        {
            tipoOperacao = "Resgate Parcial";
            this.Valor = this.ValorLiquido;
        }
        this.TipoOperacaoString = tipoOperacao;

        Cotista cotista = new Cotista();
        cotista.LoadByPrimaryKey(this.IdCotista);
        this.NomeCotista = cotista.Nome;

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(this.IdCarteira);
        string nomeFundo = cliente.Nome;
        this.NomeCarteira = nomeFundo;

        this.StatusCotizacao = this.DataOperacao < cliente.DataDia.Value ? "Efetivada" : "Pendente";

        this.StatusProcessamento = "Efetivada";

        if (operacaoCotista.TipoOperacao != (byte)TipoOperacaoCotista.Aplicacao)
        {
            Financial.ContaCorrente.FormaLiquidacao formaLiquidacao = new Financial.ContaCorrente.FormaLiquidacao();
            if (formaLiquidacao.LoadByPrimaryKey(operacaoCotista.IdFormaLiquidacao.Value))
            {
                this.FormaLiquidacao = formaLiquidacao.Descricao;
            }
        }

        ContaCorrente contaCorrente = new ContaCorrente();
        if (operacaoCotista.IdConta.HasValue && contaCorrente.LoadByPrimaryKey(operacaoCotista.IdConta.Value))
        {
            ContaCorrenteViewModel contaCorrenteViewModel = new ContaCorrenteViewModel(contaCorrente);
            this.ContaCorrente = contaCorrenteViewModel.Descricao;
        }
    }

    public OrdemCotista BuscaResgateTotalPendente(int idCarteira, int idCotista)
    {
        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCarteira);
        DateTime dataDia = cliente.DataDia.Value;

        OrdemCotistaCollection resgatesTotais = new OrdemCotistaCollection();
        resgatesTotais.Query.Where(
                resgatesTotais.Query.StatusIntegracao.NotIn((byte)StatusOrdemIntegracao.Cancelado, (byte)StatusOrdemIntegracao.OutroErro, (byte)StatusOrdemIntegracao.SemSaldo),
                resgatesTotais.Query.DataOperacao.GreaterThanOrEqual(dataDia),
                resgatesTotais.Query.IdCarteira.Equal(idCarteira),
                resgatesTotais.Query.IdCotista.Equal(idCotista),
                resgatesTotais.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.ResgateTotal));

        resgatesTotais.Load(resgatesTotais.Query);
        if (resgatesTotais.Count > 0)
        {
            return resgatesTotais[0];
        }
        else
        {
            return null;
        }
    }


}

