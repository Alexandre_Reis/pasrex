using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Fundo;
using Financial.Investidor;
using System.Collections.Generic;
using System.IO;

public class DownloadExtratoViewModel
{
    public List<DocumentoViewModel> Documentos;

    public DownloadExtratoViewModel(int idCotista)
    {
        this.Documentos = new List<DocumentoViewModel>();

        string diretorioDownload = System.Configuration.ConfigurationManager.AppSettings["DiretorioDownload"];
        string diretorioCotista = String.Format(@"{0}\Cotista{1}", diretorioDownload, idCotista);
        
        string[] filesCotista;
                
        if (Directory.Exists(diretorioCotista))
        {
            filesCotista = Directory.GetFiles(diretorioCotista);
            foreach (string file in filesCotista)
            {
                string filename = Path.GetFileNameWithoutExtension(file).ToLower();
                if(filename.StartsWith("extrato")){
                    string[] dataExtratoParts = filename.Replace("extrato", "").Split('-');
                    int anoExtrato = Convert.ToInt32(dataExtratoParts[0]);
                    int mesExtrato = Convert.ToInt32(dataExtratoParts[1]);

                    DateTime dataExtrato = new DateTime(anoExtrato, mesExtrato, 1);
                    DocumentoViewModel documento = new DocumentoViewModel();
                    documento.Nome = dataExtrato.ToString("y");
                    documento.Tipo = "cotista";
                    documento.Path = file.Replace(diretorioDownload, "");
                    this.Documentos.Add(documento);
                }
            }
        }
        this.Documentos.Reverse();
    }

}