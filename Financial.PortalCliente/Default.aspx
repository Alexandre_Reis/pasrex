﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>Portal do Cliente</title>
    <link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />
    <link href="Content/Site.css?v=1" rel="stylesheet" type="text/css" />
    <link href="Content/site-iframe.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 8]>
		<link rel="stylesheet" type="text/css" href="Content/SiteIE8AndLower.css" />
	<![endif]-->
</head>
<body>
    <div id="body-loading-mask-wrapper" style="width: 100%">
        <div id="body-loading-mask" style='background: transparent url(Content/themes/base/images/loading_32.gif) no-repeat;
            font: 12px "Segoe UI", "Lucida Sans Unicode", "tahoma", Verdana, sans-serif;
            color: #3d3d3d; width: 100px; padding: 8px 0 0 40px; height: 30px; margin: 250px auto;'>
            Carregando...</div>
    </div>

    <script src="Scripts/ext-4.1.0/ext-all.js" type="text/javascript"></script>

    <script src="Scripts/ext-4.1.0/locale/ext-lang-pt_BR.js?v=1" type="text/javascript"></script>

    <script src="Scripts/plupload/plupload.full.js" type="text/javascript"></script>

    <!--script src="@Url.Content("~/Scripts/plupload/plupload.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/plupload/plupload.flash.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/plupload/plupload.html5.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/plupload/plupload.silverlight.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/plupload/plupload.html4.js")" type="text/javascript"></script-->

    <!--script src="Controllers/Application/LoadGlobals.ashx" type="text/javascript"></script-->

    <!--script src="@Url.Content("~/Scripts/app/app-all.js?v1.25")" type="text/javascript"></script-->

    <!--script src="Scripts/app/all-classes.js" type="text/javascript"></script>

    <script src="Scripts/app/Application.js" type="text/javascript"></script-->

    <!-- Fields required for history management -->
    <form id="Form1" runat="server">
    </form>
    <form id="history-form" style="display: none">
        <input type="hidden" id="x-history-field" />
        <iframe id="x-history-frame"></iframe>
    </form>
</body>
</html>
