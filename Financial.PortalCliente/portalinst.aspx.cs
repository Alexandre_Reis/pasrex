using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Tesouraria : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //URL: http://aplwebtst/portoparportalinstitucional/portalinst.aspx?usrtip=F&empcod=1&usrcod=016441&sesnum=488713&sissgl=PORTOPAR&prgsgl=mop&modsgl=portalinst&portal=3 
        //Gerar: F0116441 

        string usrcodParam = Request["usrcod"];
        string cotistaParam="";
        if(!string.IsNullOrEmpty(usrcodParam)){
            int usrcod = Convert.ToInt32(usrcodParam);
            cotistaParam = "F01" + usrcod;
        }
        string path = "~/Controllers/Application/LoadGlobals.ashx?isTesouraria=true&cotista=" + cotistaParam;
        Page.ClientScript.RegisterClientScriptInclude("Registration", ResolveUrl(path));

        path = "Scripts/app/all-classes.js";
        Page.ClientScript.RegisterClientScriptInclude("Registration1", ResolveUrl(path));

        path = "Scripts/app/Application.js";
        Page.ClientScript.RegisterClientScriptInclude("Registration2", ResolveUrl(path));
    }
}
