<%@ Application Language="C#" %>

<script RunAt="server">

    private string DecryptConnectionString(string encryptedConnectionString)
    {
        const string SHARED_SECRET = "financial@$";
        string[] connectionStringTokens = encryptedConnectionString.Split(';');
        System.Collections.Generic.List<string> decryptedConnectionStringList = new System.Collections.Generic.List<string>();

        foreach (string connectionStringToken in connectionStringTokens)
        {
            string token = connectionStringToken.Trim();
            if (token.ToLower().StartsWith("password"))
            {
                int passwordStart = token.IndexOf("=") + 1;
                string password = token.Substring(passwordStart);
                string decryptedPassword = Financial.Util.Crypto.DecryptStringAES(password, SHARED_SECRET);
                token = "Password=" + decryptedPassword;
            }

            decryptedConnectionStringList.Add(token);
        }

        string decryptedConnectionString = String.Join(";", decryptedConnectionStringList.ToArray());
        return decryptedConnectionString;
    }

    private void AddEntitySpacesSqlConnection(string connectionName, string encryptedConnectionString)
    {
        EntitySpaces.Interfaces.esConnectionElement conn = new EntitySpaces.Interfaces.esConnectionElement();

        //Decrypt connection string
        conn.ConnectionString = DecryptConnectionString(encryptedConnectionString);

        conn.Name = connectionName;
        conn.Provider = "EntitySpaces.SqlClientProvider";
        conn.ProviderClass = "DataProvider";
        conn.SqlAccessType = EntitySpaces.Interfaces.esSqlAccessType.DynamicSQL;
        conn.ProviderMetadataKey = "esDefault";
        conn.DatabaseVersion = "2005";
        EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Connections.Add(conn);
    }

    private void AddEntitySpacesOracleConnection(string connectionName, string encryptedConnectionString)
    {
        EntitySpaces.Interfaces.esConnectionElement conn = new EntitySpaces.Interfaces.esConnectionElement();

        //Decrypt connection string
        conn.ConnectionString = DecryptConnectionString(encryptedConnectionString);

        conn.Name = connectionName;
        conn.Provider = "EntitySpaces.OracleClientProvider";
        conn.ProviderClass = "DataProvider";
        conn.SqlAccessType = EntitySpaces.Interfaces.esSqlAccessType.DynamicSQL;
        conn.ProviderMetadataKey = "esDefault";
        EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Connections.Add(conn);
    }

    void Application_Start(object sender, EventArgs e)
    {
        bool webconfigEncrypted = ConfigurationManager.AppSettings["CriptografaSenhaConexao"] == "true";
        if (webconfigEncrypted)
        {
            const string FINANCIAL_CONNECTION_NAME = "Financial";
            const string SINACOR_CONNECTION_NAME = "Sinacor";

            ConnectionStringSettings connectionSettings = ConfigurationManager.ConnectionStrings[FINANCIAL_CONNECTION_NAME];
            if (connectionSettings == null)
            {
                throw new Exception("N�o foi poss�vel encontrar a conex�o Financial no Web.Config");
            }

            string encryptedConnectionString = connectionSettings.ConnectionString;

            //Hoje consideramos que se estamos encriptando estamos usando o Financial no SQL Server. 
            //Se mudar, precisamos rever esse IF e passar pela connection string o provider
            AddEntitySpacesSqlConnection(FINANCIAL_CONNECTION_NAME, encryptedConnectionString);
            EntitySpaces.Interfaces.esConfigSettings.ConnectionInfo.Default = FINANCIAL_CONNECTION_NAME;

            connectionSettings = ConfigurationManager.ConnectionStrings[SINACOR_CONNECTION_NAME];
            if (connectionSettings != null)
            {
                encryptedConnectionString = connectionSettings.ConnectionString;
                if (!string.IsNullOrEmpty(encryptedConnectionString))
                {
                    AddEntitySpacesOracleConnection(SINACOR_CONNECTION_NAME, encryptedConnectionString);
                }
            }
        }

        EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        //string arquivo = Financial.WebConfigConfiguration.DiretorioAplicacao.DiretorioBaseAplicacao + "web.Config";
        //log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(arquivo));

        //if (EntitySpaces.Interfaces.esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider")
        //{
        //    EntitySpaces.Core.esUtility u = new EntitySpaces.Core.esUtility();
        //    string sql = " set language english ";
        //    u.ExecuteNonQuery(EntitySpaces.Interfaces.esQueryType.Text, sql, "");
        //}
    }

    void Application_End(object sender, EventArgs e)
    {
        
    }

    void Application_Error(object sender, EventArgs e)
    {
       
    }

    void Session_Start(object sender, EventArgs e)
    {

    }

    void Session_End(object sender, EventArgs e)
    {
        
    }    
</script>

