<%@ WebHandler Language="C#" Class="CotistaSelect" %>

using System;
using System.Web;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.Security;

public class CotistaSelect : IHttpHandler, System.Web.SessionState.IRequiresSessionState {
    
    public void ProcessRequest (HttpContext context) {

        int idCotista = Convert.ToInt32(context.Request["IdCotista"]);

        string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        string login = infoCotistaSplitted[2];
        
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(login);

        PermissaoCotistaCollection permissoes = new PermissaoCotistaCollection();
        permissoes.BuscaCotistasAssociados(login);
                
        List<CotistaViewModel> cotistas = new List<CotistaViewModel>();

        bool acessaCotista = false;

        if (idCotista == -1)
        {
            context.Session["InfoCotista"] = idCotista + "|" + "Todos" + "|" + login;
        }
        else
        {
            foreach (PermissaoCotista permissao in permissoes)
            {
                if (permissao.IdCotista == idCotista)
                {
                    Cotista cotista = new Cotista();
                    cotista.LoadByPrimaryKey(idCotista);
                    context.Session["InfoCotista"] = cotista.IdCotista.Value + "|" + cotista.Nome + "|" + login;

                    acessaCotista = true;
                    break;
                }
            }
        }

        JSONResponseEntity jsonResponse = new JSONResponseEntity();
        jsonResponse.success = acessaCotista;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());
        
        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    public class JSONResponseEntity
    {
        public bool success;
        public JSONResponseEntity() { }
    }

   
}