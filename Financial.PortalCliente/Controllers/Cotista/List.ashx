<%@ WebHandler Language="C#" Class="CotistaList" %>

using System;
using System.Web;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.Security;

public class CotistaList : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {

        string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        string login = infoCotistaSplitted[2];

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(login);

        CotistaCollection cotistas = new CotistaCollection();
        CotistaQuery cotistaQuery = new CotistaQuery("C");
        PermissaoCotistaQuery permissaoQuery = new PermissaoCotistaQuery("Q");
        PessoaQuery pessoaQuery = new PessoaQuery("P");

        string filter = context.Request["filter"];
        cotistaQuery.Select(cotistaQuery.IdCotista, cotistaQuery.Nome, pessoaQuery.Cpfcnpj);
        cotistaQuery.InnerJoin(permissaoQuery).On(cotistaQuery.IdCotista.Equal(permissaoQuery.IdCotista));
        cotistaQuery.InnerJoin(pessoaQuery).On(cotistaQuery.IdPessoa.Equal(pessoaQuery.IdPessoa));
        
        if (!String.IsNullOrEmpty(filter))
        {
            filter = HttpUtility.UrlDecode(filter);
            string[] filterTokens = filter.Split(',');
            filterTokens = filterTokens[1].Split(':');
            filter = filterTokens[1].Replace("\"", "").Replace("}]", "");
            
            string searchQuery = "%" + filter + "%";
            cotistaQuery.Where(cotistaQuery.IdCotista.Like(searchQuery) | cotistaQuery.Nome.Like(searchQuery) | pessoaQuery.Cpfcnpj.Like(searchQuery));
        }
        
        cotistaQuery.Where(permissaoQuery.IdUsuario.Equal(usuario.IdUsuario.Value));
        cotistas.Load(cotistaQuery);
        
        JSONResponseCollection jsonResponse = new JSONResponseCollection();
        List<CotistaViewModel> cotistasViewModel = new List<CotistaViewModel>();
       
        string pageParam = context.Request["page"];
        if (!String.IsNullOrEmpty(pageParam))
        {
            int page = Convert.ToInt32(pageParam);
            int start = Convert.ToInt32(context.Request["start"]);
            int limit = Convert.ToInt32(context.Request["limit"]);
            int rowNumber = 0;
            foreach (Cotista cotista in cotistas)
            {
                if (rowNumber >= start)
                {
                    if (cotistasViewModel.Count < limit)
                    {
                        CotistaViewModel cotistaViewModel = new CotistaViewModel();
                        cotistaViewModel.Cpfcnpj = cotista.GetColumn("Cpfcnpj").ToString();
                        cotistaViewModel.IdCotista = cotista.IdCotista.Value;
                        cotistaViewModel.Nome = cotista.Nome;
                        cotistasViewModel.Add(cotistaViewModel);
                    }
                    else
                    {
                        break;
                    }
                }
                rowNumber++;
            }
        }

        jsonResponse.Collection = cotistasViewModel;
        jsonResponse.totalCount = cotistas.Count;
        
        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseCollection
    {
        public List<CotistaViewModel> Collection;
        public JSONResponseCollection() { }
        public int totalCount;
    }


}