<%@ WebHandler Language="C#" Class="ExtratoCotistaList" %>

using System;
using System.Web;
using System.Text;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;

public class ExtratoCotistaList : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        int idCotista = Convert.ToInt32((Convert.ToString(context.Session["InfoCotista"]).Split('|'))[0]);
        Cotista cotista = new Cotista();
        cotista.LoadByPrimaryKey(idCotista);

        string filterParam = context.Request.Params["filter"];
        List<FilterViewModel> filters = new List<FilterViewModel>();

        if (!String.IsNullOrEmpty(filterParam))
        {
            filters = (List<FilterViewModel>)JsonConvert.DeserializeObject(filterParam, typeof(List<FilterViewModel>));
        }

        DateTime dataInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
        DateTime dataFim = DateTime.Today;

        bool loadOperacoes = false;

        foreach (FilterViewModel filter in filters)
        {
            string property = filter.property.ToLower();
            string value = Convert.ToString(filter.value);

            if (property == "periodo")
            {
                if (value == "last7")
                {
                    //Default
                    dataInicio = DateTime.Today.AddDays(-7);
                }
                else if (value == "last30")
                {
                    dataInicio = DateTime.Today.AddDays(-30);
                }
                else if (value == "currentMonth")
                {
                    dataInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                }
                else if (value == "period")
                {
                    foreach (FilterViewModel filterData in filters)
                    {
                        property = filterData.property.ToLower();
                        if (property == "datainicio")
                        {
                            dataInicio = Convert.ToDateTime(filterData.value);
                        }
                        else if (property == "datafim")
                        {
                            dataFim = Convert.ToDateTime(filterData.value);
                        }
                    }
                }
            }
            else if (property == "tipoextrato")
            {
                if (value == "analitico")
                {
                    loadOperacoes = true;
                }
            }
        }

        if (!Calendario.IsDiaUtil(dataInicio))
        {
            dataInicio = Calendario.AdicionaDiaUtil(dataInicio, 1);
        }

        if (!Calendario.IsDiaUtil(dataFim))
        {
            dataFim = Calendario.SubtraiDiaUtil(dataInicio, 1);
        }

        JSONResponseCollection jsonResponse = new JSONResponseCollection();

        //Vamos ter que preencher com 0 os saldos iniciais e finais que n�o fazem par no per�odo
        List<ExtratoCotistaViewModel> extratoDataInicio = new List<ExtratoCotistaViewModel>();
        List<ExtratoCotistaViewModel> extratoDataFim = new List<ExtratoCotistaViewModel>();
        List<ExtratoCotistaViewModel> operacoesExtrato = new List<ExtratoCotistaViewModel>();
        //Usaremos dictionary pra busca ser mais rapida
        Dictionary<int, string> carteirasComPosicaoDataInicio = new Dictionary<int, string>();
        Dictionary<int, string> carteirasComPosicaoDataFim = new Dictionary<int, string>();

        PosicaoCotistaHistoricoCollection posicoesDataInicio = new PosicaoCotistaHistoricoCollection();
        posicoesDataInicio.Query.Select(posicoesDataInicio.Query.CotaDia.Avg(),
            posicoesDataInicio.Query.DataHistorico,
            posicoesDataInicio.Query.IdCarteira,
            posicoesDataInicio.Query.Quantidade.Sum(),
            posicoesDataInicio.Query.ValorBruto.Sum(),
            posicoesDataInicio.Query.ValorIR.Sum(),
            posicoesDataInicio.Query.ValorIOF.Sum(),
            posicoesDataInicio.Query.ValorLiquido.Sum()
            );

        posicoesDataInicio.Query.Where(posicoesDataInicio.Query.IdCotista.Equal(cotista.IdCotista.Value),
            posicoesDataInicio.Query.DataHistorico.Equal(dataInicio),
            posicoesDataInicio.Query.Quantidade.GreaterThan(0));

        posicoesDataInicio.Query.GroupBy(posicoesDataInicio.Query.DataHistorico, posicoesDataInicio.Query.IdCarteira);
        posicoesDataInicio.Load(posicoesDataInicio.Query);

        foreach (PosicaoCotistaHistorico posicaoDataInicio in posicoesDataInicio)
        {
            carteirasComPosicaoDataInicio.Add(posicaoDataInicio.IdCarteira.Value, "");
            ExtratoCotistaViewModel registroExtrato = new ExtratoCotistaViewModel(posicaoDataInicio);
            registroExtrato.Historico = "Saldo Inicial";
            extratoDataInicio.Add(registroExtrato);
        }

        if (dataFim.Date < DateTime.Today)
        {
            PosicaoCotistaHistoricoCollection posicoesDataFim = new PosicaoCotistaHistoricoCollection();
            posicoesDataFim.Query.Select(posicoesDataFim.Query.CotaDia.Avg(),
                posicoesDataFim.Query.DataHistorico,
                posicoesDataFim.Query.IdCarteira,
                posicoesDataFim.Query.Quantidade.Sum(),
                posicoesDataFim.Query.ValorBruto.Sum(),
                posicoesDataFim.Query.ValorIR.Sum(),
                posicoesDataFim.Query.ValorIOF.Sum(),
                posicoesDataFim.Query.ValorLiquido.Sum()
                );

            posicoesDataFim.Query.Where(posicoesDataFim.Query.IdCotista.Equal(cotista.IdCotista.Value),
                posicoesDataFim.Query.DataHistorico.Equal(dataFim),
                posicoesDataFim.Query.Quantidade.GreaterThan(0));

            posicoesDataFim.Query.GroupBy(posicoesDataFim.Query.DataHistorico, posicoesDataFim.Query.IdCarteira);
            posicoesDataFim.Load(posicoesDataFim.Query);

            foreach (PosicaoCotistaHistorico posicaoDataFim in posicoesDataFim)
            {
                carteirasComPosicaoDataFim.Add(posicaoDataFim.IdCarteira.Value, "");
                ExtratoCotistaViewModel registroExtrato = new ExtratoCotistaViewModel(posicaoDataFim);
                registroExtrato.Historico = "Saldo Final";
                extratoDataFim.Add(registroExtrato);
            }
        }
        else
        {
            PosicaoCotistaCollection posicoesDataFim = new PosicaoCotistaCollection();
            posicoesDataFim.Query.Select(posicoesDataFim.Query.CotaDia.Avg(),
                //posicoesDataFim.Query.DataHistorico,
                posicoesDataFim.Query.IdCarteira,
                posicoesDataFim.Query.Quantidade.Sum(),
                posicoesDataFim.Query.ValorBruto.Sum(),
                posicoesDataFim.Query.ValorIR.Sum(),
                posicoesDataFim.Query.ValorIOF.Sum(),
                posicoesDataFim.Query.ValorLiquido.Sum()
                );

            posicoesDataFim.Query.Where(posicoesDataFim.Query.IdCotista.Equal(cotista.IdCotista.Value),
                posicoesDataFim.Query.Quantidade.GreaterThan(0));

            posicoesDataFim.Query.GroupBy(posicoesDataFim.Query.IdCarteira);
            posicoesDataFim.Load(posicoesDataFim.Query);

            foreach (PosicaoCotista posicaoDataFim in posicoesDataFim)
            {
                carteirasComPosicaoDataFim.Add(posicaoDataFim.IdCarteira.Value, "");
                ExtratoCotistaViewModel registroExtrato = new ExtratoCotistaViewModel(posicaoDataFim);
                registroExtrato.Historico = "Saldo Final";
                extratoDataFim.Add(registroExtrato);
            }
        }

        //Pesquisar carteiras que soh tem saldo no inicio
        foreach (ExtratoCotistaViewModel extrato in extratoDataInicio)
        {
            if (!carteirasComPosicaoDataFim.ContainsKey(extrato.IdCarteira))
            {
                ExtratoCotistaViewModel registroExtrato = new ExtratoCotistaViewModel();
                registroExtrato.AddPosicaoZerada(extrato.IdCarteira, dataFim, "Saldo Final");
                extratoDataFim.Add(registroExtrato);
            }
        }

        //Pesquisar carteiras que soh tem saldo no fim
        foreach (ExtratoCotistaViewModel extrato in extratoDataFim)
        {
            if (!carteirasComPosicaoDataInicio.ContainsKey(extrato.IdCarteira))
            {
                ExtratoCotistaViewModel registroExtrato = new ExtratoCotistaViewModel();
                registroExtrato.AddPosicaoZerada(extrato.IdCarteira, dataInicio, "Saldo Inicial");
                extratoDataInicio.Add(registroExtrato);
            }
        }

        #region Pegar movimento
        if (loadOperacoes)
        {

            /*OrdemCotistaCollection ordemCotistaCollection = new OrdemCotistaCollection();
            ordemCotistaCollection.Query.Where(ordemCotistaCollection.Query.IdCotista.Equal(cotista.IdCotista.Value),
                                               ordemCotistaCollection.Query.DataOperacao.GreaterThanOrEqual(dataInicio),
                                               ordemCotistaCollection.Query.DataOperacao.LessThanOrEqual(dataFim),
                                               ordemCotistaCollection.Query.Status.NotEqual((byte)StatusOrdemCotista.Processado)); //Processadas estao em Operacao

            //if (idCarteira.HasValue)
            //{
            //    ordemCotistaCollection.Query.Where(ordemCotistaCollection.Query.IdCarteira.Equal(idCarteira.Value));
            //}
        
            ordemCotistaCollection.Query.OrderBy(ordemCotistaCollection.Query.DataOperacao.Ascending);
            ordemCotistaCollection.Query.Load();

            foreach (OrdemCotista ordemCotista in ordemCotistaCollection)
            {
                operacoesExtrato.Add(new ExtratoCotistaViewModel(ordemCotista));
            }*/

            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCotista.Equal(cotista.IdCotista.Value),
                                                operacaoCotistaCollection.Query.DataOperacao.GreaterThanOrEqual(dataInicio),
                                                operacaoCotistaCollection.Query.DataOperacao.LessThanOrEqual(dataFim)
                );


            /*if (idCarteira.HasValue)
            {
                operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteira.Value));
            }*/

            operacaoCotistaCollection.Query.OrderBy(operacaoCotistaCollection.Query.DataOperacao.Ascending);
            operacaoCotistaCollection.Query.Load();

            foreach (OperacaoCotista operacao in operacaoCotistaCollection)
            {
                operacoesExtrato.Add(new ExtratoCotistaViewModel(operacao));
            }
        }

        #endregion //FIM MOVIMENTO



        //Juntar extratos
        extratoDataInicio.AddRange(operacoesExtrato);
        extratoDataInicio.AddRange(extratoDataFim);
        jsonResponse.Collection = extratoDataInicio;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseCollection
    {
        public List<ExtratoCotistaViewModel> Collection;
        public JSONResponseCollection() { }
    }
}

