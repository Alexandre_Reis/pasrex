<%@ WebHandler Language="C#" Class="FundoCotistaOperacaoList" %>

using System;
using System.Web;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.Security;
using Financial.Investidor;
using Financial.Investidor.Enums;

public class FundoCotistaOperacaoList : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {

        string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        string login = infoCotistaSplitted[2];

        int idCotista = Convert.ToInt32((Convert.ToString(context.Session["InfoCotista"]).Split('|'))[0]);

        JSONResponseCollection jsonResponse = new JSONResponseCollection();
        List<CarteiraViewModel> fundos = new List<CarteiraViewModel>();
        Dictionary<int, int> dicFundos = new Dictionary<int, int>();

        PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
        posicaoCotistaCollection.Query.Select(posicaoCotistaCollection.Query.IdCarteira);
        posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCotista.Equal(idCotista));
        posicaoCotistaCollection.Query.es.Distinct = true;
        posicaoCotistaCollection.Load(posicaoCotistaCollection.Query);

        foreach (PosicaoCotista posicao in posicaoCotistaCollection)
        {
            int idFundo = posicao.IdCarteira.Value;
            if (!dicFundos.ContainsKey(idFundo))
            {
                Carteira fundo = new Carteira();
                if (fundo.LoadByPrimaryKey(idFundo))
                {
                    CarteiraViewModel carteiraViewModel = new CarteiraViewModel(fundo);
                    fundos.Add(carteiraViewModel);
                    dicFundos.Add(idFundo, idFundo);
                }
            }
        }

        OrdemCotistaCollection ordemCotistaCollection = new OrdemCotistaCollection();
        ordemCotistaCollection.Query.Select(ordemCotistaCollection.Query.IdCarteira);
        ordemCotistaCollection.Query.Where(ordemCotistaCollection.Query.IdCotista.Equal(idCotista));
        ordemCotistaCollection.Query.es.Distinct = true;
        ordemCotistaCollection.Load(ordemCotistaCollection.Query);

        foreach (OrdemCotista ordem in ordemCotistaCollection)
        {
            int idFundo = ordem.IdCarteira.Value;
            if (!dicFundos.ContainsKey(idFundo))
            {
                Carteira fundo = new Carteira();
                if (fundo.LoadByPrimaryKey(idFundo))
                {
                    CarteiraViewModel carteiraViewModel = new CarteiraViewModel(fundo);
                    fundos.Add(carteiraViewModel);
                    dicFundos.Add(idFundo, idFundo);
                }
            }
        }

        OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
        operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.IdCarteira);
        operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCotista.Equal(idCotista));
        operacaoCotistaCollection.Query.es.Distinct = true;
        operacaoCotistaCollection.Load(operacaoCotistaCollection.Query);

        foreach (OperacaoCotista operacao in operacaoCotistaCollection)
        {
            int idFundo = operacao.IdCarteira.Value;
            if (!dicFundos.ContainsKey(idFundo))
            {
                Carteira fundo = new Carteira();
                if (fundo.LoadByPrimaryKey(idFundo))
                {
                    CarteiraViewModel carteiraViewModel = new CarteiraViewModel(fundo);
                    fundos.Add(carteiraViewModel);
                    dicFundos.Add(idFundo, idFundo);
                }
            }
        }


        jsonResponse.Collection = fundos;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseCollection
    {
        public List<CarteiraViewModel> Collection;
        public JSONResponseCollection() { }
    }


}