<%@ WebHandler Language="C#" Class="OrdemTesourariaList" %>

using System;
using System.Web;
using System.Text;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.InterfacesCustom;
using Financial.InterfacesCustom.PortoPar;

public class OrdemTesourariaList : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {

        JSONResponseCollection jsonResponse = new JSONResponseCollection();

        //fake return

        /*List<OrdemTesourariaViewModel> operacoes = new List<OrdemTesourariaViewModel>();
        OrdemTesourariaViewModel ordem = new OrdemTesourariaViewModel();
        ordem.DataOperacao = DateTime.Today;
        ordem.IdCarteira = 1;
        ordem.IdCotista = 1;
        ordem.IdOperacao = 1;
        ordem.NomeCarteira = "Fundo A";
        ordem.StatusProcessamento = "Digitado";
        ordem.TipoOperacaoString = "Aplica��o";
        ordem.Valor = 5000;
        operacoes.Add(ordem);
        jsonResponse.Collection = operacoes;
        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());
        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
        return;*/

        string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        string login = infoCotistaSplitted[2];

        int idCotista = Convert.ToInt32(infoCotistaSplitted[0]);
        Cotista cotista = new Cotista();
        cotista.LoadByPrimaryKey(idCotista);

        string filterParam = context.Request.Params["filter"];
        List<FilterViewModel> filters = new List<FilterViewModel>();

        if (!String.IsNullOrEmpty(filterParam))
        {
            filters = (List<FilterViewModel>)JsonConvert.DeserializeObject(filterParam, typeof(List<FilterViewModel>));
        }

        DateTime dataInicio = DateTime.Today;
        DateTime dataFim = DateTime.Today.AddYears(10);

        foreach (FilterViewModel filter in filters)
        {
            string property = filter.property.ToLower();
            string value = Convert.ToString(filter.value);

            if (property == "periodo")
            {
                if (value == "last7")
                {
                    dataInicio = DateTime.Today.AddDays(-7);
                }
                else if (value == "last1")
                {
                    //Default
                }
                else if (value == "last30")
                {
                    dataInicio = DateTime.Today.AddDays(-30);
                }
                else if (value == "currentMonth")
                {
                    dataInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                }
                else if (value == "period")
                {
                    foreach (FilterViewModel filterData in filters)
                    {
                        property = filterData.property.ToLower();
                        if (property == "datainicio")
                        {
                            dataInicio = Convert.ToDateTime(filterData.value);
                        }
                        else if (property == "datafim")
                        {
                            dataFim = Convert.ToDateTime(filterData.value);
                        }
                    }
                }
            }
        }


        List<OrdemTesourariaViewModel> operacoes = new List<OrdemTesourariaViewModel>();

        Financial.InterfacesCustom.OrdemTesourariaCollection ordemTesourariaCollection = new Financial.InterfacesCustom.OrdemTesourariaCollection();

        if (idCotista == -1)
        {
            //Todos os cotistas aos quais o usuario tem acesso e apenas aprovacao
            int[] idCotistasPermitidos = this.GetIdCotistasPermitidos(login);
            ordemTesourariaCollection.Query.Where(ordemTesourariaCollection.Query.IdCotista.In(idCotistasPermitidos));
        }
        else
        {
            ordemTesourariaCollection.Query.Where(ordemTesourariaCollection.Query.IdCotista.Equal(cotista.IdCotista.Value));
        }

        
        

        ordemTesourariaCollection.Query.Where(
                                            ordemTesourariaCollection.Query.Status.In((byte)StatusOrdemTesourariaPorto.Aprovado,
                                                (byte)StatusOrdemTesourariaPorto.Digitado, (byte)StatusOrdemTesourariaPorto.Distribuido,
                                                (byte)StatusOrdemTesourariaPorto.Rejeitado),
                                                ordemTesourariaCollection.Query.DataOperacao.GreaterThanOrEqual(dataInicio),
                                            ordemTesourariaCollection.Query.DataOperacao.LessThanOrEqual(dataFim),
                                            ordemTesourariaCollection.Query.IdOperacaoMae.IsNull());
        ordemTesourariaCollection.Query.OrderBy(ordemTesourariaCollection.Query.DataOperacao.Descending);
        ordemTesourariaCollection.Query.Load();

        foreach (Financial.InterfacesCustom.OrdemTesouraria ordemTesouraria in ordemTesourariaCollection)
        {
            //Nao adicionar ordens maes distibuidas
            bool isOrdemTesourariaMae = ordemTesouraria.IdOperacaoMae == null;
            bool isDistribuida = ordemTesouraria.Status == (byte)Financial.InterfacesCustom.PortoPar.StatusOrdemTesourariaPorto.Distribuido;
            //if (!(isOrdemTesourariaMae && isDistribuida))
            {
                operacoes.Add(new OrdemTesourariaViewModel(ordemTesouraria));
            }
        }

        jsonResponse.Collection = operacoes;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);

    }

    public int[] GetIdCotistasPermitidos(string login)
    {

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(login);

        PermissaoCotistaCollection permissoes = new PermissaoCotistaCollection();
        permissoes.BuscaCotistasAssociados(login);

        JSONResponseCollection jsonResponse = new JSONResponseCollection();
        List<int> cotistas = new List<int>();

        foreach (PermissaoCotista permissao in permissoes)
        {
            cotistas.Add(permissao.IdCotista.Value);
        }

        return cotistas.ToArray();
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseCollection
    {
        public List<OrdemTesourariaViewModel> Collection;
        public JSONResponseCollection() { }
    }
}

