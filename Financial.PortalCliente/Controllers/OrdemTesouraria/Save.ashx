<%@ WebHandler Language="C#" Class="OrdemTesourariaSave" %>

using System;
using System.Web;
using System.Text;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.InterfacesCustom;

public class OrdemTesourariaSave : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    
    
    public void ProcessRequest(HttpContext context)
    {
        JSONResponseEntity jsonResponse = new JSONResponseEntity();

        int idCotista = Convert.ToInt32((Convert.ToString(context.Session["InfoCotista"]).Split('|'))[0]);
        int idCarteira = Convert.ToInt32(context.Request["IdCarteira"]);

        DateTime dataOperacao = Convert.ToDateTime(context.Request["DataOperacao"]);
        byte tipoOperacao = Convert.ToByte(context.Request["TipoOperacao"]);
        bool isResgateTotal = tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal;
        
        string valorParam = context.Request["Valor"];
        decimal valor = Convert.ToDecimal(valorParam, new System.Globalization.CultureInfo("en-US"));
        
        byte idFormaLiquidacao = Convert.ToByte(context.Request["IdFormaLiquidacao"]);
        int? idConta = null;
        string idContaParam = context.Request["IdConta"];
        if (!String.IsNullOrEmpty(idContaParam))
        {
            idConta = Convert.ToInt32(idContaParam);
        }

        //Valida��es
        string errorMessage = "";
        if (dataOperacao.Date < DateTime.Today)
        {
            errorMessage = "Data da opera��o deve ser futura.";
        }
        else if (!Calendario.IsDiaUtil(dataOperacao.Date))
        {
            errorMessage = "Data da opera��o deve ser dia �til.";
        }
        else if (valor <= 0 && !isResgateTotal)
        {
            errorMessage = "Valor de opera��o inv�lido.";
        }

        //Checar limites por usuario/operacao
        string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        string login = infoCotistaSplitted[2];
        
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(login);

        bool isAplicacao = tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao;
        
        LimiteOperacaoUsuario limiteOperacaoUsuario = new LimiteOperacaoUsuario();
        if (limiteOperacaoUsuario.LoadByPrimaryKey(usuario.IdUsuario.Value))
        {
            if (isAplicacao)
            {
                if (limiteOperacaoUsuario.MaximoAplicacao.HasValue && valor > limiteOperacaoUsuario.MaximoAplicacao.Value)
                {
                    errorMessage = "Valor de aplica��o � maior do que o m�ximo permitido para o usu�rio.";
                }
            }
            else
            {
                if (limiteOperacaoUsuario.MaximoResgate.HasValue && valor > limiteOperacaoUsuario.MaximoResgate.Value)
                {
                    errorMessage = "Valor de resgate � maior do que o m�ximo permitido para o usu�rio.";
                }
            }
        }
        
        if (errorMessage != "")
        {
            jsonResponse.success = false;
            jsonResponse.errorMessage = errorMessage;

            string jsonErro = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

            context.Response.ContentType = "text/javascript";
            context.Response.Write(jsonErro);
            return;
        }
        
        
        Financial.InterfacesCustom.OrdemTesouraria ordemTesouraria = new Financial.InterfacesCustom.OrdemTesouraria();
        ordemTesouraria.DataOperacao = dataOperacao;
        ordemTesouraria.IdCotista = idCotista;
        ordemTesouraria.TipoOperacao = tipoOperacao;
        ordemTesouraria.Valor = valor;
        ordemTesouraria.Status = (byte)Financial.InterfacesCustom.PortoPar.StatusOrdemTesourariaPorto.Digitado;
        ordemTesouraria.IdFormaLiquidacao = idFormaLiquidacao;
        ordemTesouraria.IdConta = idConta;

        ordemTesouraria.EnviarEmailBackOffice(context, "Inclus�o Opera��o Portal Institucional");
            
        ordemTesouraria.Save();
        
        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Portal Cliente: Cadastro de OrdemTesouraria - Operacao: Insert OrdemTesouraria: " + ordemTesouraria.IdOperacao + UtilitarioWeb.ToString(ordemTesouraria),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(context.Request),
                                        "",
                                        Convert.ToBoolean(context.Session["IsTesouraria"]) ?
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalInstitucional :
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalCotista);
        #endregion
        
        jsonResponse.Entity = new OrdemTesourariaViewModel(ordemTesouraria);
        jsonResponse.success = true;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseEntity
    {
        public OrdemTesourariaViewModel Entity;
        public bool success;
        public string errorMessage;
        public JSONResponseEntity() { }
    }
}

