<%@ WebHandler Language="C#" Class="OrdemTesourariaList" %>

using System;
using System.Web;
using System.Text;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.InterfacesCustom;

public class OrdemTesourariaList : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        int idCotista = Convert.ToInt32((Convert.ToString(context.Session["InfoCotista"]).Split('|'))[0]);
        List<FilterViewModel> filters = (List<FilterViewModel>)JsonConvert.DeserializeObject(context.Request.Params["filter"], typeof(List<FilterViewModel>));

        int idOrdemTesourariaMae = Convert.ToInt32(filters[0].value);

        //Cotista cotista = new Cotista();
        //cotista.LoadByPrimaryKey(idCotista);

        Financial.InterfacesCustom.OrdemTesourariaCollection ordens = new Financial.InterfacesCustom.OrdemTesourariaCollection();
        ordens.Query.Where(ordens.Query.IdOperacaoMae.Equal(idOrdemTesourariaMae));

        if (idCotista > 0)
        {
            ordens.Query.Where(ordens.Query.IdCotista.Equal(idCotista));
        }

        ordens.Load(ordens.Query);

        List<OrdemTesourariaViewModel> ordensViewModel = new List<OrdemTesourariaViewModel>();

        foreach (Financial.InterfacesCustom.OrdemTesouraria ordem in ordens)
        {
            ordensViewModel.Add(new OrdemTesourariaViewModel(ordem));
        }

        JSONResponseCollection jsonResponse = new JSONResponseCollection();
        jsonResponse.Collection = ordensViewModel;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseCollection
    {
        public List<OrdemTesourariaViewModel> Collection;
        public JSONResponseCollection() { }
    }
}

