<%@ WebHandler Language="C#" Class="OrdemCotistaSave" %>

using System;
using System.Web;
using System.Text;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.InterfacesCustom;
using System.Configuration;
using System.Net.Mail;
using Financial.Interfaces.Export;

public class OrdemCotistaSave : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        JSONResponseEntity jsonResponse = new JSONResponseEntity();
        int idOperacao = Convert.ToInt32(context.Request["IdOperacao"]);
        string action = context.Request["action"];
        string motivoCancelamento = context.Request["motivoCancelamento"];

        Financial.InterfacesCustom.OrdemTesouraria ordemTesourariaMae = new Financial.InterfacesCustom.OrdemTesouraria();
        ordemTesourariaMae.LoadByPrimaryKey(idOperacao);

        HistoricoLog historicoLog = new HistoricoLog();

        bool ordemIsPgbl = ordemTesourariaMae.Status == (byte)Financial.InterfacesCustom.PortoPar.StatusOrdemTesourariaPorto.PGBLIntegrado;

        bool success = true;
        int idOrdemCotistaCriada;
        string msgErro = "";

        if (action == "aprovar")
        {
            if (ordemTesourariaMae.Status != (byte)Financial.InterfacesCustom.PortoPar.StatusOrdemTesourariaPorto.Distribuido &&
                ordemTesourariaMae.Status != (byte)Financial.InterfacesCustom.PortoPar.StatusOrdemTesourariaPorto.PGBLIntegrado)
            {
                //Impedir duplo submission
                throw new Exception("Inconsist�ncia de status");
            }

            if (ordemIsPgbl)
            {
                ordemTesourariaMae.Status = (byte)Financial.InterfacesCustom.PortoPar.StatusOrdemTesourariaPorto.PGBLAprovado;
                CriarOrdemCotista(ordemTesourariaMae, context, out idOrdemCotistaCriada);
            }
            else
            {
                ordemTesourariaMae.Status = (byte)Financial.InterfacesCustom.PortoPar.StatusOrdemTesourariaPorto.Aprovado;

                Financial.InterfacesCustom.OrdemTesourariaCollection ordemTesourariaFilhas = new Financial.InterfacesCustom.OrdemTesourariaCollection();
                ordemTesourariaFilhas.Query.Where(ordemTesourariaFilhas.Query.IdOperacaoMae.Equal(ordemTesourariaMae.IdOperacao.Value));

                ordemTesourariaFilhas.Load(ordemTesourariaFilhas.Query);

                foreach (Financial.InterfacesCustom.OrdemTesouraria ordemTesourariaFilha in ordemTesourariaFilhas)
                {
                    ordemTesourariaFilha.Status = (byte)Financial.InterfacesCustom.PortoPar.StatusOrdemTesourariaPorto.Aprovado;

                    //Criar ordens Financial
                    if (!CriarOrdemCotista(ordemTesourariaFilha, context, out idOrdemCotistaCriada))
                    {
                        //Pegar erro do log:
                        HistoricoLogCollection logs = new HistoricoLogCollection();
                        string searchString = String.Format("Envio XmlMva para a ordem cotista {0}. Msg retorno:", idOrdemCotistaCriada);
                        logs.Query.Where(logs.Query.Descricao.Like(searchString + "%"),
                            logs.Query.DataInicio.GreaterThanOrEqual(DateTime.Today));

                        if (logs.Load(logs.Query) && logs.Count > 0)
                        {
                            msgErro = logs[0].Descricao.Replace(searchString, "").Replace("+", " ").Replace("|", "");
                        }
                        success = false;
                    }
                }

                ordemTesourariaFilhas.Save();
            }

            ordemTesourariaMae.Save();
            #region Logar
            historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Portal Cliente: Aprova��o de Ordem - Operacao: Update Ordem: " + ordemTesourariaMae.IdOperacao + UtilitarioWeb.ToString(ordemTesourariaMae),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(context.Request),
                                            "",
                                            Convert.ToBoolean(context.Session["IsTesouraria"]) ?
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalInstitucional :
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalCotista);
            #endregion
        }
        else
        {
            ordemTesourariaMae.Status = ordemIsPgbl ? (byte)Financial.InterfacesCustom.PortoPar.StatusOrdemTesourariaPorto.PGBLRejeitado :
                (byte)Financial.InterfacesCustom.PortoPar.StatusOrdemTesourariaPorto.Rejeitado;

            Financial.InterfacesCustom.OrdemTesourariaCollection ordemTesourariaFilhas = new Financial.InterfacesCustom.OrdemTesourariaCollection();
            ordemTesourariaFilhas.Query.Where(ordemTesourariaFilhas.Query.IdOperacaoMae.Equal(ordemTesourariaMae.IdOperacao.Value));

            ordemTesourariaFilhas.Load(ordemTesourariaFilhas.Query);

            foreach (Financial.InterfacesCustom.OrdemTesouraria ordemTesourariaFilha in ordemTesourariaFilhas)
            {
                ordemTesourariaFilha.Status = (byte)Financial.InterfacesCustom.PortoPar.StatusOrdemTesourariaPorto.Rejeitado;
                ordemTesourariaFilha.Observacao = motivoCancelamento;
            }

            ordemTesourariaFilhas.Save();

            ordemTesourariaMae.Observacao = motivoCancelamento;
            ordemTesourariaMae.Save();
            #region Logar
            historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Portal Cliente: Cancelamento de Ordem - Operacao: Update Ordem: " + ordemTesourariaMae.IdOperacao + UtilitarioWeb.ToString(ordemTesourariaMae),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(context.Request),
                                            "",
                                            Convert.ToBoolean(context.Session["IsTesouraria"]) ?
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalInstitucional :
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalCotista);
            #endregion

        }


        jsonResponse.success = success;

        if (!success)
        {
            string baseMsgErro = "Ocorreram erros durante o envio das ordens para o Ita�. ";
            jsonResponse.errorMessage = baseMsgErro;
            jsonResponse.errorMessage += (String.IsNullOrEmpty(msgErro) ? "Favor consultar o log." : "Detalhes do erro: " + msgErro);
        }

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }

    public bool CriarOrdemCotista(Financial.InterfacesCustom.OrdemTesouraria ordemTesouraria, HttpContext context, out int idOrdemCotistaCriada)
    {
        Carteira carteira = new Carteira();
        carteira.LoadByPrimaryKey(ordemTesouraria.IdCarteira.Value);

        OrdemCotista ordemCotista = new OrdemCotista();
        ordemCotista.IdCotista = ordemTesouraria.IdCotista.Value;
        ordemCotista.IdCarteira = ordemTesouraria.IdCarteira;
        ordemCotista.DataOperacao = ordemTesouraria.DataOperacao;

        ordemCotista.DataAgendamento = ordemTesouraria.DataOperacao; //Vamos setar DataAgendamento antes de remover eventual TIME-PART da opera��o - esta linha tem que preceder a pr�xima
        ordemCotista.DataOperacao = ordemTesouraria.DataOperacao;
        ordemCotista.TipoOperacao = ordemTesouraria.TipoOperacao;
        ordemCotista.Quantidade = 0;
        ordemCotista.ValorBruto = 0;
        ordemCotista.ValorLiquido = 0;

        DateTime dataConversao = new DateTime();
        if (ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao ||
            ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial)
        {
            dataConversao = Calendario.AdicionaDiaUtil(ordemCotista.DataOperacao.Value, carteira.DiasCotizacaoAplicacao.Value);
        }
        else if (ordemCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.AplicacaoCotasEspecial)
        {
            if (carteira.ContagemDiasConversaoResgate.Value == (byte)ContagemDiasLiquidacaoResgate.DiasUteis)
            {
                dataConversao = Calendario.AdicionaDiaUtil(ordemCotista.DataOperacao.Value, carteira.DiasCotizacaoResgate.Value);
            }
            else
            {
                dataConversao = ordemCotista.DataOperacao.Value.AddDays(carteira.DiasCotizacaoResgate.Value);

                if (!Calendario.IsDiaUtil(dataConversao))
                {
                    dataConversao = Calendario.AdicionaDiaUtil(dataConversao, 1);
                }
            }
        }
        ordemCotista.DataConversao = dataConversao;

        DateTime dataLiquidacao = new DateTime();
        if (ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao ||
            ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial)
        {
            dataLiquidacao = Calendario.AdicionaDiaUtil(ordemCotista.DataOperacao.Value, carteira.DiasLiquidacaoAplicacao.Value);
        }
        else if (ordemCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.AplicacaoCotasEspecial)
        {
            if (carteira.ContagemDiasConversaoResgate.Value == (byte)ContagemDiasLiquidacaoResgate.DiasUteis)
            {
                dataLiquidacao = Calendario.AdicionaDiaUtil(ordemCotista.DataOperacao.Value, carteira.DiasLiquidacaoResgate.Value);
            }
            else
            {
                //Conta por Dias �teis em cima da data de convers�o
                dataLiquidacao = Calendario.AdicionaDiaUtil(ordemCotista.DataConversao.Value, carteira.DiasLiquidacaoResgate.Value);
            }
        }
        ordemCotista.DataLiquidacao = dataLiquidacao;

        if (ordemCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
        {
            ordemCotista.ValorBruto = 0;
        }
        else if (ordemCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido)
        {
            ordemCotista.ValorLiquido = ordemTesouraria.Valor;
        }
        else
        {
            ordemCotista.ValorBruto = ordemTesouraria.Valor;
        }

        if (ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao)
        {
            ordemCotista.TipoResgate = null;
        }
        else
        {
            ordemCotista.TipoResgate = (byte)TipoResgateCotista.FIFO;
        }

        ordemCotista.IdFormaLiquidacao = ordemTesouraria.IdFormaLiquidacao;
        ordemCotista.IdConta = ordemTesouraria.IdConta;

        bool operacaoAgendada = ordemCotista.DataOperacao.Value.Date > DateTime.Today;
        bool success = true;

        if (!operacaoAgendada)
        {
            success = ordemCotista.IntegraItau(2);
        }

        ordemCotista.EnviarEmailBackOffice(ordemCotista, context, carteira, "Aprova��o Opera��o Portal Institucional", true);

        if (ordemCotista.IdOperacao.HasValue)
        {
            idOrdemCotistaCriada = ordemCotista.IdOperacao.Value;
        }
        else
        {
            idOrdemCotistaCriada = 0;
        }
        
        return success;
    }


    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseEntity
    {
        public bool success;
        public string errorMessage;
        public JSONResponseEntity() { }
    }
}

