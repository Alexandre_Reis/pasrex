<%@ WebHandler Language="C#" Class="OrdemTesourariaDistribute" %>

using System;
using System.Web;
using System.Text;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.InterfacesCustom;

public class OrdemTesourariaDistribute : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        JSONResponseEntity jsonResponse = new JSONResponseEntity();

        string ordensParam = context.Request.Params["ordens"];
        List<OrdemTesourariaViewModel> ordemTesourariaViewModelFilhas = new List<OrdemTesourariaViewModel>();

        ordemTesourariaViewModelFilhas = (List<OrdemTesourariaViewModel>)JsonConvert.DeserializeObject(ordensParam, typeof(List<OrdemTesourariaViewModel>));

        //[{"IdCarteira":205214,"NomeCarteira":"TRX REALTY I FIC DE FI MULTIMERCADO","ValorBruto":1}
        int idOrdemTesourariaMae = Convert.ToInt32(context.Request.Params["IdOperacao"]);
        Financial.InterfacesCustom.OrdemTesouraria ordemTesourariaMae = new Financial.InterfacesCustom.OrdemTesouraria();
        ordemTesourariaMae.LoadByPrimaryKey(idOrdemTesourariaMae);

        Financial.InterfacesCustom.OrdemTesourariaCollection ordemTesourariaCollectionFilhas = new Financial.InterfacesCustom.OrdemTesourariaCollection();

        foreach (OrdemTesourariaViewModel ordemTesourariaViewModelFilha in ordemTesourariaViewModelFilhas)
        {

            #region Valida��es
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(ordemTesourariaViewModelFilha.IdCarteira.Value);

            Financial.InterfacesCustom.OrdemTesouraria ordemTesourariaFilha = new Financial.InterfacesCustom.OrdemTesouraria();
            ordemTesourariaFilha.IdCotista = ordemTesourariaMae.IdCotista;
            ordemTesourariaFilha.IdCarteira = ordemTesourariaViewModelFilha.IdCarteira;
            ordemTesourariaFilha.DataOperacao = ordemTesourariaMae.DataOperacao;
            ordemTesourariaFilha.TipoOperacao = ordemTesourariaMae.TipoOperacao;
            ordemTesourariaFilha.Valor = ordemTesourariaViewModelFilha.Valor;
            ordemTesourariaFilha.IdConta = ordemTesourariaMae.IdConta;
            ordemTesourariaFilha.IdFormaLiquidacao = ordemTesourariaMae.IdFormaLiquidacao;
            ordemTesourariaFilha.Status = (byte)Financial.InterfacesCustom.PortoPar.StatusOrdemTesourariaPorto.Distribuido;
            ordemTesourariaFilha.IdOperacaoMae = ordemTesourariaMae.IdOperacao;
            
            //Valida��es
            string errorMessage = "";

            bool isAplicacao = ordemTesourariaFilha.TipoOperacao == (byte)TipoOperacaoCotista.Aplicacao;
            bool isResgateTotal = ordemTesourariaFilha.TipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal;

            //Saldo atual disponivel
            decimal saldoDisponivel = new PosicaoCotistaViewModel().RetornaSaldoDisponivel(ordemTesourariaFilha.IdCarteira.Value, ordemTesourariaFilha.IdCotista.Value);
 
            if (ordemTesourariaFilha.Valor <= 0 && !isResgateTotal)
            {
                errorMessage = "Valor de opera��o inv�lido.";
            }
            else if (isAplicacao)
            {
                bool isAplicacaoInicial = saldoDisponivel == 0;

                if (isAplicacaoInicial)
                {
                    if (carteira.ValorMinimoInicial.HasValue && ordemTesourariaFilha.Valor < carteira.ValorMinimoInicial)
                    {
                        errorMessage = "Valor de aplica��o � menor do que o m�nimo permitido para aplica��o inicial no fundo.";
                    }
                }
                else
                {
                    if (carteira.ValorMaximoAplicacao.HasValue &&
                        (carteira.ValorMaximoAplicacao.Value > 0) && (ordemTesourariaFilha.Valor > carteira.ValorMaximoAplicacao.Value))
                    {
                        errorMessage = "Valor de aplica��o � maior do que o m�ximo permitido para o fundo.";
                    }
                    else if (carteira.ValorMinimoAplicacao.HasValue && ordemTesourariaFilha.Valor < carteira.ValorMinimoAplicacao.Value)
                    {
                        errorMessage = "Valor de aplica��o � menor do que o m�nimo permitido para o fundo.";
                    }
                }
            }
            else if (!isResgateTotal)
            {
                if (carteira.ValorMinimoResgate.HasValue && ordemTesourariaFilha.Valor < carteira.ValorMinimoResgate.Value)
                {
                    errorMessage = "Valor de resgate � menor do que o m�nimo permitido para o fundo.";
                }


                decimal saldoAposResgate = saldoDisponivel - ordemTesourariaFilha.Valor.Value;

                if (saldoAposResgate < 0)
                {
                    errorMessage = "Valor de resgate � maior do que o saldo do fundo.";
                }

                if (carteira.ValorMinimoSaldo.HasValue)
                {
                    if (saldoAposResgate < carteira.ValorMinimoSaldo.Value)
                    {
                        errorMessage = "Valor de saldo ap�s o resgate � inferior ao saldo m�nimo permitido para o fundo.";
                    }
                }
            }

            if (errorMessage != "")
            {
                jsonResponse.success = false;
                jsonResponse.errorMessage = errorMessage;

                string jsonErro = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

                context.Response.ContentType = "text/javascript";
                context.Response.Write(jsonErro);
                return;
            }
            #endregion

            ordemTesourariaFilha.EnviarEmailBackOffice(context, carteira, "Distribui��o da Opera��o");
            ordemTesourariaCollectionFilhas.AttachEntity(ordemTesourariaFilha);
        }

        ordemTesourariaCollectionFilhas.Save();

        ordemTesourariaMae.Status = (byte)Financial.InterfacesCustom.PortoPar.StatusOrdemTesourariaPorto.Distribuido;
        ordemTesourariaMae.Save();

        #region Envio de email e log do processo
        HistoricoLog historicoLog = new HistoricoLog();
        foreach (Financial.InterfacesCustom.OrdemTesouraria ordemTesourariaFilha in ordemTesourariaCollectionFilhas)
        {
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(ordemTesourariaFilha.IdCarteira.Value);
            
            ordemTesourariaFilha.EnviarEmailInstitucional(context, carteira, (byte)SituacaoEnvio.Distribuicao);
            historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Portal Cliente: Distribui��o de OrdemTesouraria - Operacao: Insert OrdemTesourariaFilha: " + ordemTesourariaFilha.IdOperacao + UtilitarioWeb.ToString(ordemTesourariaFilha),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(context.Request),
                                            "",
                                            Convert.ToBoolean(context.Session["IsTesouraria"]) ?
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalInstitucional :
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalCotista);
        }
        
        historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Portal Cliente: Distribui��o de OrdemTesouraria - Operacao: Update OrdemTesourariaMae: " + ordemTesourariaMae.IdOperacao + UtilitarioWeb.ToString(ordemTesourariaMae),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(context.Request),
                                        "",
                                        Convert.ToBoolean(context.Session["IsTesouraria"]) ?
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalInstitucional :
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalCotista);
        #endregion

        
        jsonResponse.success = true;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }

    
    
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseEntity
    {
        public bool success;
        public string errorMessage;
        public JSONResponseEntity() { }
    }
}

