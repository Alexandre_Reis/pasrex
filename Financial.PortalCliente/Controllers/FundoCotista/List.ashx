<%@ WebHandler Language="C#" Class="FundoCotistaList" %>

using System;
using System.Web;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.Security;
using Financial.Investidor;
using Financial.Investidor.Enums;

public class FundoCotistaList : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {

        string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        string login = infoCotistaSplitted[2];

        int idCotista = Convert.ToInt32((Convert.ToString(context.Session["InfoCotista"]).Split('|'))[0]);

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(login);

        CarteiraCollection fundos = new CarteiraCollection();

        PermissaoClienteQuery permissaoQuery = new PermissaoClienteQuery("P");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        CategoriaFundoQuery categoriaQuery = new CategoriaFundoQuery("T");

        carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, carteiraQuery.Apelido, carteiraQuery.ValorMinimoInicial, carteiraQuery.HorarioFim, categoriaQuery.Descricao);
            
        carteiraQuery.Where(permissaoQuery.IdUsuario.Equal(usuario.IdUsuario.Value));
        carteiraQuery.Where(clienteQuery.IdTipo.In((int)TipoClienteFixo.Clube, (int)TipoClienteFixo.FDIC, (int)TipoClienteFixo.Fundo));
        carteiraQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira.Equal(clienteQuery.IdCliente));
        carteiraQuery.InnerJoin(permissaoQuery).On(clienteQuery.IdCliente.Equal(permissaoQuery.IdCliente));
        carteiraQuery.LeftJoin(categoriaQuery).On(carteiraQuery.IdCategoria.Equal(categoriaQuery.IdCategoria));
        fundos.Load(carteiraQuery);

        JSONResponseCollection jsonResponse = new JSONResponseCollection();
        List<CarteiraViewModel> fundosViewModel = new List<CarteiraViewModel>();

        PermissaoOperacaoFundoCollection permissaoOperacaoFundoCol = new PermissaoOperacaoFundoCollection();
        permissaoOperacaoFundoCol.Query.Where(permissaoOperacaoFundoCol.Query.IdCotista.Equal(idCotista));
        permissaoOperacaoFundoCol.Load(permissaoOperacaoFundoCol.Query);
        Dictionary<int, int> fundosPermitidos = new Dictionary<int, int>();
        foreach (PermissaoOperacaoFundo permissao in permissaoOperacaoFundoCol)
        {
            int idFundoPermitido = permissao.IdFundo.Value;
            if (!fundosPermitidos.ContainsKey(idFundoPermitido))
            {
                fundosPermitidos.Add(idFundoPermitido, idFundoPermitido);
            }
        }
        
        foreach (Carteira fundo in fundos)
        {
            CarteiraViewModel carteiraViewModel = new CarteiraViewModel();

            if (idCotista == -1)
            {
                carteiraViewModel.TemPermissaoAplicar = true;
            }
            else
            {
                carteiraViewModel.TemPermissaoAplicar = fundosPermitidos.ContainsKey(fundo.IdCarteira.Value);
            }

            
            carteiraViewModel.IdCarteira = fundo.IdCarteira.Value;
            carteiraViewModel.Nome = fundo.Nome;
            carteiraViewModel.ValorMinimoInicial = fundo.ValorMinimoInicial.Value; 
            carteiraViewModel.HorarioFim = fundo.HorarioFim;
            carteiraViewModel.Categoria = Convert.ToString(fundo.GetColumn("Descricao"));
            carteiraViewModel.Apelido = fundo.Apelido;
             
            fundosViewModel.Add(carteiraViewModel);
            
        }

        jsonResponse.Collection = fundosViewModel;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseCollection
    {
        public List<CarteiraViewModel> Collection;
        public JSONResponseCollection() { }
    }


}