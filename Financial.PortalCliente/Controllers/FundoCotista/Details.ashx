<%@ WebHandler Language="C#" Class="FundoCotistaList" %>

using System;
using System.Web;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.Security;
using Financial.Investidor;
using Financial.Investidor.Enums;

public class FundoCotistaList : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {

        string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        string login = infoCotistaSplitted[2];

        int idCarteira = Convert.ToInt32(context.Request["idcarteira"]);

        JSONResponseEntity jsonResponse = new JSONResponseEntity();

        Carteira carteira = new Carteira();
        carteira.LoadByPrimaryKey(idCarteira);

        CarteiraViewModel carteiraViewModel = new CarteiraViewModel(carteira);
        
        jsonResponse.carteira = carteiraViewModel;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseEntity
    {
        public CarteiraViewModel carteira;
        public JSONResponseEntity() { }
    }


}