<%@ WebHandler Language="C#" Class="PosicaoCotistaList" %>

using System;
using System.Web;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.Security;
using Financial.Investidor;
using Financial.Investidor.Enums;

public class PosicaoCotistaList : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        int idCotista = Convert.ToInt32((Convert.ToString(context.Session["InfoCotista"]).Split('|'))[0]);

        if (idCotista <= 0)
        {
            idCotista = Convert.ToInt32(context.Request["idCotista"]);
        }
        
        Cotista cotista = new Cotista();
        cotista.LoadByPrimaryKey(idCotista);


        ClienteQuery clienteQuery = new ClienteQuery("C");
        PosicaoCotistaAberturaQuery posicaoQuery = new PosicaoCotistaAberturaQuery("P");

        posicaoQuery.Select(posicaoQuery.Quantidade.Sum(),
                                posicaoQuery.IdCarteira,
                                posicaoQuery.IdCotista,
                                                    posicaoQuery.QuantidadeBloqueada.Sum(),
                                                    posicaoQuery.ValorBruto.Sum(),
                                                    posicaoQuery.ValorIR.Sum(),
                                                    posicaoQuery.ValorIOF.Sum(),
                                                    posicaoQuery.ValorPerformance.Sum(),
                                                    posicaoQuery.ValorLiquido.Sum(),
                                                    posicaoQuery.ValorRendimento.Sum(),
                                                    posicaoQuery.CotaDia.Avg());
        
        posicaoQuery.LeftJoin(clienteQuery).On(posicaoQuery.IdCarteira.Equal(clienteQuery.IdCliente));
        
        posicaoQuery.Where(posicaoQuery.DataHistorico.Equal(clienteQuery.DataDia),
            posicaoQuery.Quantidade.NotEqual(0),
            posicaoQuery.IdCotista.Equal(idCotista));

        posicaoQuery.GroupBy(posicaoQuery.IdCarteira, posicaoQuery.IdCotista);
                
        PosicaoCotistaAberturaCollection posicoes = new PosicaoCotistaAberturaCollection();
        
        posicoes.Load(posicaoQuery);

        List<PosicaoCotistaViewModel> posicoesViewModel = new List<PosicaoCotistaViewModel>();

        foreach (PosicaoCotistaAbertura posicao in posicoes)
        {
            posicoesViewModel.Add(new PosicaoCotistaViewModel(posicao));
        }

        JSONResponseCollection jsonResponse = new JSONResponseCollection();
        jsonResponse.Collection = posicoesViewModel;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseCollection
    {
        public List<PosicaoCotistaViewModel> Collection;
        public JSONResponseCollection() { }
    }



}