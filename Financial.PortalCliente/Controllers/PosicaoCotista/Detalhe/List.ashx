<%@ WebHandler Language="C#" Class="PosicaoCotistaDetalheList" %>

using System;
using System.Web;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.Security;

public class PosicaoCotistaDetalheList : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) {
        int idCotista = Convert.ToInt32((Convert.ToString(context.Session["InfoCotista"]).Split('|'))[0]);
        List<FilterViewModel> filters = (List<FilterViewModel>) JsonConvert.DeserializeObject(context.Request.Params["filter"],typeof(List<FilterViewModel>));
            
        int idCarteira = Convert.ToInt32(filters[0].value);
        
        Cotista cotista = new Cotista();
        cotista.LoadByPrimaryKey(idCotista);

        PosicaoCotistaCollection posicoes = new PosicaoCotistaCollection();
        posicoes.Query.Where(posicoes.Query.IdCotista.Equal(cotista.IdCotista.Value),
            posicoes.Query.IdCarteira.Equal(idCarteira));
        
        posicoes.Load(posicoes.Query);
        
        List<PosicaoCotistaViewModel> posicoesViewModel = new List<PosicaoCotistaViewModel>();

        foreach (PosicaoCotista posicao in posicoes)
        {
            posicoesViewModel.Add(new PosicaoCotistaViewModel(posicao));
        }
        
        JSONResponseCollection jsonResponse = new JSONResponseCollection();
        jsonResponse.Collection = posicoesViewModel;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());
        
        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    public class JSONResponseCollection
    {
        public List<PosicaoCotistaViewModel> Collection;
        public JSONResponseCollection() { }
    }

    

}