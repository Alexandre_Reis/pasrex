<%@ WebHandler Language="C#" Class="OrdemCotistaSave" %>

using System;
using System.Web;
using System.Text;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.InterfacesCustom;
using System.Configuration;
using System.Net.Mail;
using Financial.Interfaces.Export;

public class OrdemCotistaSave : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        JSONResponseEntity jsonResponse = new JSONResponseEntity();
        int idOperacao = Convert.ToInt32(context.Request["IdOperacao"]);
        string action = context.Request["action"];

        OrdemCotista ordem = new OrdemCotista();
        ordem.LoadByPrimaryKey(idOperacao);

        
        ordem.StatusIntegracao = (byte)StatusOrdemIntegracao.Cancelado;
        ordem.Status = (byte)StatusOrdemCotista.Cancelado;
        ordem.Save();

        #region Logar
        HistoricoLog historicoLog = new HistoricoLog();

        historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Portal Cliente: Cancelamento de Ordem - Operacao: Update Ordem: " + ordem.IdOperacao + UtilitarioWeb.ToString(ordem),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(context.Request),
                                        "",
                                        Convert.ToBoolean(context.Session["IsTesouraria"]) ?
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalInstitucional :
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalCotista);
        #endregion



        jsonResponse.success = true;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseEntity
    {
        public bool success;
        public string errorMessage;
        public JSONResponseEntity() { }
    }
}

