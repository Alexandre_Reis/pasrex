<%@ WebHandler Language="C#" Class="OrdemCotistaSave" %>

using System;
using System.Web;
using System.Text;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using System.Net.Mail;
using System.Configuration;
using Financial.Interfaces.Export;
using Financial.InvestidorCotista.Perfil;

public class OrdemCotistaSave : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        JSONResponseEntity jsonResponse = new JSONResponseEntity();

        int idCotista = Convert.ToInt32((Convert.ToString(context.Session["InfoCotista"]).Split('|'))[0]);
        int idCarteira = Convert.ToInt32(context.Request["IdCarteira"]);
        DateTime dataOperacao = Convert.ToDateTime(context.Request["DataOperacao"]);
        byte tipoOperacao = Convert.ToByte(context.Request["TipoOperacao"]);
        byte idFormaLiquidacao;
        if (String.IsNullOrEmpty(context.Request["IdFormaLiquidacao"]))
        {
            idFormaLiquidacao = 1;
        }
        else
        {
            idFormaLiquidacao = Convert.ToByte(context.Request["IdFormaLiquidacao"]);
        }

        string valorParam = context.Request["ValorBruto"];
        decimal valorBruto = Convert.ToDecimal(valorParam, new System.Globalization.CultureInfo("en-US"));

        int idConta = Convert.ToInt32(context.Request["IdConta"]);

        Carteira carteira = new Carteira();
        carteira.LoadByPrimaryKey(idCarteira);

        //Valida��es
        string errorMessage = "";

        bool isAplicacao = tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao;
        bool isResgateTotal = tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal;

        string tipoOperacaoDescricao = "aplica��o";
        string artigoTipoOperacao = "A";
        if (!isAplicacao)
        {
            tipoOperacaoDescricao = "resgate";
            artigoTipoOperacao = "O";
        }

        //Saldo atual disponivel na posicao mais recente
        decimal saldoDisponivel = new PosicaoCotistaViewModel().RetornaSaldoDisponivel(idCarteira, idCotista);

        //Encontrar resgates totais
        OrdemCotista resgateTotal = new OperacaoCotistaViewModel().BuscaResgateTotalPendente(idCarteira, idCotista);
        bool existeResgateTotalFuturo = resgateTotal != null;

        if (dataOperacao.Date < DateTime.Today)
        {
            errorMessage = "Data da opera��o deve ser futura.";
        }
        else if (!Calendario.IsDiaUtil(dataOperacao.Date))
        {
            errorMessage = "Data da opera��o deve ser dia �til.";
        }
        else if (valorBruto <= 0 && !isResgateTotal)
        {
            errorMessage = "Valor de opera��o inv�lido.";
        }
        else if (isAplicacao)
        {
            bool isAplicacaoInicial = saldoDisponivel == 0;

            if (isAplicacaoInicial)
            {
                if (carteira.ValorMinimoInicial.HasValue && valorBruto < carteira.ValorMinimoInicial)
                {
                    errorMessage = "Valor de aplica��o � menor do que o m�nimo permitido para aplica��o inicial no fundo.";
                }
            }
            else
            {
                if (carteira.ValorMaximoAplicacao.HasValue &&
                    (carteira.ValorMaximoAplicacao.Value > 0) && (valorBruto > carteira.ValorMaximoAplicacao.Value))
                {
                    errorMessage = "Valor de aplica��o � maior do que o m�ximo permitido para o fundo.";
                }
                else if (carteira.ValorMinimoAplicacao.HasValue && valorBruto < carteira.ValorMinimoAplicacao.Value)
                {
                    errorMessage = "Valor de aplica��o � menor do que o m�nimo permitido para o fundo.";
                }
            }
        }
        else if (!isResgateTotal)
        {
            if (carteira.ValorMinimoResgate.HasValue && valorBruto < carteira.ValorMinimoResgate.Value)
            {
                errorMessage = "Valor de resgate � menor do que o m�nimo permitido para o fundo.";
            }


            decimal saldoAposResgate = saldoDisponivel - valorBruto;

            if (saldoAposResgate < 0)
            {
                errorMessage = "Valor de resgate � maior do que o saldo do fundo.";
            }

            if (carteira.ValorMinimoSaldo.HasValue)
            {
                if (saldoAposResgate < carteira.ValorMinimoSaldo.Value)
                {
                    errorMessage = "Valor de saldo ap�s o resgate � inferior ao saldo m�nimo permitido para o fundo.";
                }
            }

            //Impedir resgate se for ap�s ou na mesma data de um resgate total
            if (existeResgateTotalFuturo && dataOperacao >= resgateTotal.DataOperacao.Value)
            {
                errorMessage = "N�o � poss�vel lan�ar este resgate pois existe um resgate total agendado";
            }
        }
        else if (isResgateTotal)
        {
            //Impedir lan�ar mais de um resgate total ativo
            if (existeResgateTotalFuturo)
            {
                errorMessage = "N�o � poss�vel lan�ar este resgate pois j� existe um resgate total agendado";
            }
        }

        //Checar limites por usuario/operacao
        string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        string login = infoCotistaSplitted[2];

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(login);

        LimiteOperacaoUsuario limiteOperacaoUsuario = new LimiteOperacaoUsuario();
        if (limiteOperacaoUsuario.LoadByPrimaryKey(usuario.IdUsuario.Value))
        {
            if (isAplicacao)
            {
                if (limiteOperacaoUsuario.MaximoAplicacao.HasValue && valorBruto > limiteOperacaoUsuario.MaximoAplicacao.Value)
                {
                    errorMessage = "Valor de aplica��o � maior do que o m�ximo permitido para o usu�rio.";
                }
            }
            else if (!isResgateTotal)
            {
                if (limiteOperacaoUsuario.MaximoResgate.HasValue && valorBruto > limiteOperacaoUsuario.MaximoResgate.Value)
                {
                    errorMessage = "Valor de resgate � maior do que o m�ximo permitido para o usu�rio.";
                }
            }

        }

        if (errorMessage != "")
        {
            jsonResponse.success = false;
            jsonResponse.errorMessage = errorMessage;

            string jsonErro = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

            context.Response.ContentType = "text/javascript";
            context.Response.Write(jsonErro);
            return;
        }

        #region Validacoes Suitability
        PessoaSuitability pessoaSuitability = new PessoaSuitability();
        Cotista cotista = new Cotista();
        cotista.LoadByPrimaryKey(idCotista);
        jsonResponse.perfilCotista = null;
        if (pessoaSuitability.LoadByPrimaryKey(cotista.IdPessoa.Value))
        {
            ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();
            if (!pessoaSuitability.Dispensado.Equals("S"))
            {
                if (isAplicacao)
                {
                    OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                    operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteira));
                    operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCotista.Equal(idCotista));
                    operacaoCotistaCollection.Query.Load();

                    bool primeiraAplicacao = operacaoCotistaCollection.Count == 0;
                    bool termoAdesaoAceito = Convert.ToBoolean(context.Request["TermoAdesaoAceito"]);

                    if (primeiraAplicacao && !termoAdesaoAceito)
                    {
                        errorMessage = controllerPerfilCotista.selecionaMensagemEvento(17, idCotista);
                        jsonResponse.erroSuitability = 17;
                    }
                }

                //Verificar enquadramento
                SuitabilityPerfilInvestidor suitabilityPerfilCotistaOut;
                SuitabilityPerfilInvestidor suitabilityPerfilPosicaoOut;
                bool cotistaDesenquadrado;

                controllerPerfilCotista.verificaEnquadramentoSuitability(idCotista, idCarteira, tipoOperacao, valorBruto, out suitabilityPerfilPosicaoOut, out suitabilityPerfilCotistaOut,
                    out cotistaDesenquadrado);

                bool desenquadramentoAceito = Convert.ToBoolean(context.Request["DesenquadramentoAceito"]);
                if (cotistaDesenquadrado && !desenquadramentoAceito)
                {
                    errorMessage = controllerPerfilCotista.selecionaMensagemEvento(14, idCotista);
                    jsonResponse.erroSuitability = 14;
                }

                if (errorMessage != "")
                {
                    jsonResponse.success = true; //vamos tratar como passos do workflow no sucesso
                    jsonResponse.errorMessage = errorMessage;

                    string jsonErro = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

                    context.Response.ContentType = "text/javascript";
                    context.Response.Write(jsonErro);
                    return;
                }
            }

            jsonResponse.perfilCotista = controllerPerfilCotista.carregaPerfil(cotista.IdCotista.Value);
        }
        #endregion

        //Validar horario de aplicacoes com data de hoje
        if (dataOperacao.Date == DateTime.Today.Date)
        {
            DateTime? horarioLimiteOperacao = null;
            if (isAplicacao)
            {
                if (carteira.HorarioFim.HasValue)
                {
                    DateTime horarioLimiteAplicacaoCarteira = carteira.HorarioFim.Value;

                    //Ajustar data de opera��o e mensagem de alerta quando o horario aplic/resgate tiver sido excedido
                    horarioLimiteOperacao = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day,
                        horarioLimiteAplicacaoCarteira.Hour, horarioLimiteAplicacaoCarteira.Minute, 0);
                }
            }
            else
            {
                if (carteira.HorarioFimResgate.HasValue)
                {
                    DateTime horarioLimiteResgateCarteira = carteira.HorarioFimResgate.Value;

                    //Ajustar data de opera��o e mensagem de alerta quando o horario aplic/resgate tiver sido excedido
                    horarioLimiteOperacao = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day,
                        horarioLimiteResgateCarteira.Hour, horarioLimiteResgateCarteira.Minute, 0);
                }
            }

            if (horarioLimiteOperacao.HasValue && DateTime.Now > horarioLimiteOperacao)
            {
                dataOperacao = Calendario.AdicionaDiaUtil(dataOperacao, 1);

                errorMessage = String.Format("Aten��o: O hor�rio de {0} deste fundo foi excedido. {1} {0} foi agendada para o pr�ximo dia �til. \n\n\n", tipoOperacaoDescricao, artigoTipoOperacao);
            }
        }

        OrdemCotista ordemCotista = new OrdemCotista();
        ordemCotista.IdCotista = idCotista;
        ordemCotista.IdCarteira = idCarteira;

        ordemCotista.DataAgendamento = DateTime.Now; //Vamos setar DataAgendamento antes de remover eventual TIME-PART da opera��o - esta linha tem que preceder a pr�xima
        ordemCotista.DataOperacao = dataOperacao.Date;
        ordemCotista.TipoOperacao = tipoOperacao;
        ordemCotista.Quantidade = 0;
        ordemCotista.ValorLiquido = 0;
        ordemCotista.ValorBruto = 0;
        ordemCotista.IdConta = idConta;

        //Para opera��es na data ou superiores � data dia, � poss�vel informar apenas a data da opera��o

        DateTime dataConversao = new DateTime();
        if (ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao ||
            ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial)
        {
            dataConversao = Calendario.AdicionaDiaUtil(ordemCotista.DataOperacao.Value, carteira.DiasCotizacaoAplicacao.Value);
        }
        else if (ordemCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.AplicacaoCotasEspecial)
        {
            if (carteira.ContagemDiasConversaoResgate.Value == (byte)ContagemDiasLiquidacaoResgate.DiasUteis)
            {
                dataConversao = Calendario.AdicionaDiaUtil(ordemCotista.DataOperacao.Value, carteira.DiasCotizacaoResgate.Value);
            }
            else
            {
                dataConversao = ordemCotista.DataOperacao.Value.AddDays(carteira.DiasCotizacaoResgate.Value);

                if (!Calendario.IsDiaUtil(dataConversao))
                {
                    dataConversao = Calendario.AdicionaDiaUtil(dataConversao, 1);
                }
            }
        }
        ordemCotista.DataConversao = dataConversao;

        DateTime dataLiquidacao = new DateTime();
        if (ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao ||
            ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial)
        {
            dataLiquidacao = Calendario.AdicionaDiaUtil(ordemCotista.DataOperacao.Value, carteira.DiasLiquidacaoAplicacao.Value);
        }
        else if (ordemCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.AplicacaoCotasEspecial)
        {
            if (carteira.ContagemDiasConversaoResgate.Value == (byte)ContagemDiasLiquidacaoResgate.DiasUteis)
            {
                dataLiquidacao = Calendario.AdicionaDiaUtil(ordemCotista.DataOperacao.Value, carteira.DiasLiquidacaoResgate.Value);
            }
            else
            {
                //Conta por Dias �teis em cima da data de convers�o
                dataLiquidacao = Calendario.AdicionaDiaUtil(ordemCotista.DataConversao.Value, carteira.DiasLiquidacaoResgate.Value);
            }
        }
        ordemCotista.DataLiquidacao = dataLiquidacao;

        if (ordemCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
        {
            /*PosicaoCotistaCollection posicoes = new PosicaoCotistaCollection();
            posicoes.Query.Select(posicoes.Query.ValorLiquido.Sum(), posicoes.Query.ValorBruto.Sum());
            posicoes.Query.Where(posicoes.Query.IdCotista.Equal(idCotista),
                    posicoes.Query.IdCarteira.Equal(idCarteira));*/

            ordemCotista.ValorLiquido = 0;
            ordemCotista.ValorBruto = 0;

            /*if (posicaoLivre != null) //Nao temos como calcular o valor do resgate total antes do mesmo ser processado
            {
                ordemCotista.ValorBruto = posicaoLivre.ValorBruto;
                ordemCotista.ValorLiquido = posicaoLivre.ValorLiquido;
            }*/

        }
        else if (ordemCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido)
        {
            ordemCotista.ValorLiquido = valorBruto;
        }
        else
        {
            ordemCotista.ValorBruto = valorBruto;
        }

        if (ordemCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao)
        {
            ordemCotista.TipoResgate = null;
        }
        else
        {
            ordemCotista.TipoResgate = (byte)TipoResgateCotista.FIFO;
        }

        if (isAplicacao)
        {
            if (idFormaLiquidacao == 0)
            {
                ordemCotista.IdFormaLiquidacao = 1;
            }
            else
            {
                ordemCotista.IdFormaLiquidacao = idFormaLiquidacao;
            }
        }
        else
        {
            const string CODIGO_COMPENSACAO_BANCO_ITAU = "341";
            //Resgate com regra maluca de forma liquidacao derivada da conta
            ContaCorrente contaCorrente = new ContaCorrente();
            contaCorrente.LoadByPrimaryKey(idConta);
            Banco banco = new Banco();
            banco.LoadByPrimaryKey(contaCorrente.IdBanco.Value);
            if (banco.CodigoCompensacao.Trim() == CODIGO_COMPENSACAO_BANCO_ITAU)
            {
                ordemCotista.IdFormaLiquidacao = 3; //conta corrente
            }
            else
            {
                ordemCotista.IdFormaLiquidacao = 2; //ted
            }
        }

        //ordemCotista.Fonte = (byte)FonteOrdemCotista.Manual;

        string mensagemDatas = isAplicacao ? "MensagemAplicacao" : "MensagemResgate";
        mensagemDatas = System.Configuration.ConfigurationManager.AppSettings[mensagemDatas];
        mensagemDatas = mensagemDatas.Replace("{DataLiquidacao}", ordemCotista.DataLiquidacao.Value.ToString("d/MM/yyyy"));
        mensagemDatas = mensagemDatas.Replace("{DataConversao}", ordemCotista.DataConversao.Value.ToString("d/MM/yyyy"));

        errorMessage += mensagemDatas;

        string json;

        bool operacaoAgendada = ordemCotista.DataOperacao.Value.Date > DateTime.Today;

        ordemCotista.Status = (byte)StatusOrdemCotista.Digitado;
        ordemCotista.StatusIntegracao = (byte)StatusOrdemIntegracao.Agendado;

        ordemCotista.Save();

        if (!operacaoAgendada)
        {
            string msgRetorno;
            if (!ordemCotista.IntegraItau(1, out msgRetorno))
            {
                errorMessage = TrataMensagemErroItau(msgRetorno);
            }
        }

        ordemCotista.EnviarEmailBackOffice(ordemCotista, context, carteira, "Inclus�o Opera��o Portal Varejo", false);

        string descricaoHistorico = String.Format("Cadastro de OrdemCotista - Operacao: Insert OrdemCotista: {0}", UtilitarioWeb.ToString(ordemCotista));
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        descricaoHistorico,
                                        HttpContext.Current.User.Identity.Name,
                                        "",
                                        "",
                                        Convert.ToBoolean(context.Session["IsTesouraria"]) ?
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalInstitucional :
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalCotista);

        jsonResponse.Entity = new OperacaoCotistaViewModel(ordemCotista);
        jsonResponse.success = true;
        jsonResponse.errorMessage = errorMessage;//Manter isso - estamos mandando avisos mesmo que tenha sido salva a opera��o !

        json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }

    public string TrataMensagemErroItau(string msgErroItau)
    {
        msgErroItau = msgErroItau.ToUpper();
        string appDataFolder = System.IO.Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "App_Data");
        string mensagensErroPath = appDataFolder + "\\" + "mensagens_erro_itau.csv";
        string[] mensagensErro = System.IO.File.ReadAllLines(mensagensErroPath);
        string mensagemPadrao = "";
        Dictionary<string, string> mappingMsgsErro = new Dictionary<string, string>();
        foreach (string mensagemErro in mensagensErro)
        {
            string[] splittedMsgErro = mensagemErro.Split(';');
            string msgItau = splittedMsgErro[0].ToUpper();
            string msgItauTratada = splittedMsgErro[1];
            if (msgItau == "PADRAO")
            {
                mensagemPadrao = msgItauTratada;
            }
            else if (!mappingMsgsErro.ContainsKey(msgItau))
            {
                mappingMsgsErro.Add(msgItau, splittedMsgErro[1]);
            }
        }

        string msgErroTratada = mensagemPadrao;

        foreach (KeyValuePair<string, string> mappingMsgErro in mappingMsgsErro)
        {
            if (msgErroItau.Contains(mappingMsgErro.Key))
            {
                msgErroTratada = mappingMsgErro.Value;
                break;
            }
        }

        return msgErroTratada;
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseEntity
    {
        public OperacaoCotistaViewModel Entity;
        public ControllerPerfilCotista.PerfilCotista perfilCotista;
        public bool success;
        public string errorMessage;
        public int erroSuitability;
        public JSONResponseEntity() {
            this.erroSuitability = 0;
        }
    }
}

