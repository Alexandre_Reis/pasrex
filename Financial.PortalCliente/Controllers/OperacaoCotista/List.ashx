<%@ WebHandler Language="C#" Class="OperacaoCotistaList" %>

using System;
using System.Web;
using System.Text;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.InterfacesCustom.PortoPar;

public class OperacaoCotistaList : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        int idCotista = Convert.ToInt32(infoCotistaSplitted[0]);
        string login = infoCotistaSplitted[2];

        Cotista cotista = new Cotista();
        cotista.LoadByPrimaryKey(idCotista);

        string filterParam = context.Request.Params["filter"];
        List<FilterViewModel> filters = new List<FilterViewModel>();

        if (!String.IsNullOrEmpty(filterParam))
        {
            filters = (List<FilterViewModel>)JsonConvert.DeserializeObject(filterParam, typeof(List<FilterViewModel>));
        }

        DateTime dataInicio = DateTime.Today.AddDays(-7);
        DateTime dataFim = DateTime.Today.AddYears(10);
        int? idCarteira = null;
        byte? tipoOperacao = null;
        byte? statusIntegracao = null;

        foreach (FilterViewModel filter in filters)
        {
            string property = filter.property.ToLower();
            string value = Convert.ToString(filter.value);

            if (property == "periodo")
            {
                if (value == "last7")
                {
                    //Default
                }
                else if (value == "last30")
                {
                    dataInicio = DateTime.Today.AddDays(-30);
                }
                else if (value == "currentMonth")
                {
                    dataInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                }
                else if (value == "period")
                {
                    foreach (FilterViewModel filterData in filters)
                    {
                        property = filterData.property.ToLower();
                        if (property == "datainicio")
                        {
                            dataInicio = Convert.ToDateTime(filterData.value);
                        }
                        else if (property == "datafim")
                        {
                            dataFim = Convert.ToDateTime(filterData.value);
                        }
                    }
                }
            }
            else if (property == "idcarteira")
            {
                int idCarteiraParsed;
                if (Int32.TryParse(value, out idCarteiraParsed))
                {
                    idCarteira = idCarteiraParsed;
                }
            }
            else if (property == "tipooperacao")
            {
                byte tipoOperacaoParsed;
                if (Byte.TryParse(value, out tipoOperacaoParsed))
                {
                    tipoOperacao = tipoOperacaoParsed;
                }
            }
            else if (property == "statusintegracao")
            {
                byte statusIntegracaoParsed;
                if (Byte.TryParse(value, out statusIntegracaoParsed))
                {
                    statusIntegracao = statusIntegracaoParsed;
                }
            }
        }


        JSONResponseCollection jsonResponse = new JSONResponseCollection();
        List<OperacaoCotistaViewModel> operacoes = new List<OperacaoCotistaViewModel>();


        bool loadOperacoes = true;
        bool loadOrdens = true;

        if (statusIntegracao.HasValue)
        {
            if (statusIntegracao.Value == (byte)StatusOrdemIntegracao.Processado)
            {
                //Se o status for efetivada, carregaremos apenas operacoes
                loadOrdens = false;
            }
            else
            {
                //Se for qualquer outro status, sao status de ordem, nao vamos carregar operacoes
                loadOperacoes = false;
            }
        }

        if (loadOrdens)
        {
            OrdemCotistaCollection ordemCotistaCollection = new OrdemCotistaCollection();

            if (idCotista == -1)
            {
                //Todos os cotistas aos quais o usuario tem acesso e apenas aprovacao
                int[] idCotistasPermitidos = this.GetIdCotistasPermitidos(login);
                ordemCotistaCollection.Query.Where(ordemCotistaCollection.Query.IdCotista.In(idCotistasPermitidos));
            }
            else
            {
                ordemCotistaCollection.Query.Where(ordemCotistaCollection.Query.IdCotista.Equal(cotista.IdCotista.Value));
            }

            ordemCotistaCollection.Query.Where(
                                               ordemCotistaCollection.Query.DataOperacao.GreaterThanOrEqual(dataInicio),
                                               ordemCotistaCollection.Query.DataOperacao.LessThanOrEqual(dataFim),
                                               ordemCotistaCollection.Query.Status.NotEqual((byte)StatusOrdemCotista.Processado)); //Processadas estao em Operacao

            if (idCarteira.HasValue)
            {
                ordemCotistaCollection.Query.Where(ordemCotistaCollection.Query.IdCarteira.Equal(idCarteira.Value));
            }

            if (tipoOperacao.HasValue)
            {
                if (tipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao)
                {
                    ordemCotistaCollection.Query.Where(ordemCotistaCollection.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.Aplicacao));
                }
                else
                {
                    ordemCotistaCollection.Query.Where(ordemCotistaCollection.Query.TipoOperacao.In((byte)TipoOperacaoCotista.ResgateBruto,
                        (byte)TipoOperacaoCotista.ResgateLiquido, (byte)TipoOperacaoCotista.ResgateTotal));
                }
            }

            if (statusIntegracao.HasValue)
            {
                /*if (statusIntegracao.Value == (byte)StatusOrdemIntegracao.Agendado)
                {
                    //S� existe um tipo de status "Agendado" na GUI que corresponde internamente a agendamentos que vao ser integrados e que nao serao integrados com o itau
                    ordemCotistaCollection.Query.Where(ordemCotistaCollection.Query.StatusIntegracao.In(
                        (byte)StatusOrdemIntegracao.Agendado, (byte)StatusOrdemIntegracao.AgendadoItau));
                }
                else
                {*/
                ordemCotistaCollection.Query.Where(ordemCotistaCollection.Query.StatusIntegracao.Equal(statusIntegracao.Value));
                //}
            }

            ordemCotistaCollection.Query.OrderBy(ordemCotistaCollection.Query.DataOperacao.Descending);
            ordemCotistaCollection.Query.Load();

            foreach (OrdemCotista ordemCotista in ordemCotistaCollection)
            {
                operacoes.Add(new OperacaoCotistaViewModel(ordemCotista));
            }
        }

        if (loadOperacoes)
        {
            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();

            if (idCotista == -1)
            {
                //Todos os cotistas aos quais o usuario tem acesso e apenas aprovacao
                int[] idCotistasPermitidos = this.GetIdCotistasPermitidos(login);
                operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCotista.In(idCotistasPermitidos));
            }
            else
            {
                operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCotista.Equal(cotista.IdCotista.Value));
            }

            operacaoCotistaCollection.Query.Where(
                                                operacaoCotistaCollection.Query.DataOperacao.GreaterThanOrEqual(dataInicio),
                                                operacaoCotistaCollection.Query.DataOperacao.LessThanOrEqual(dataFim)
                );


            if (idCarteira.HasValue)
            {
                operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteira.Value));
            }

            if (tipoOperacao.HasValue)
            {
                if (tipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao)
                {
                    operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.Aplicacao));
                }
                else
                {
                    operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.TipoOperacao.In((byte)TipoOperacaoCotista.ResgateBruto,
                        (byte)TipoOperacaoCotista.ResgateLiquido, (byte)TipoOperacaoCotista.ResgateTotal));
                }
            }

            operacaoCotistaCollection.Query.OrderBy(operacaoCotistaCollection.Query.DataOperacao.Descending);
            operacaoCotistaCollection.Query.Load();

            foreach (OperacaoCotista operacao in operacaoCotistaCollection)
            {
                operacoes.Add(new OperacaoCotistaViewModel(operacao));
            }
        }

        operacoes.Sort(delegate(OperacaoCotistaViewModel p1, OperacaoCotistaViewModel p2)
            {
                return p2.DataOperacao.CompareTo(p1.DataOperacao);
            }
        );

        jsonResponse.Collection = operacoes;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);

    }

    public int[] GetIdCotistasPermitidos(string login)
    {

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(login);

        PermissaoCotistaCollection permissoes = new PermissaoCotistaCollection();
        permissoes.BuscaCotistasAssociados(login);

        JSONResponseCollection jsonResponse = new JSONResponseCollection();
        List<int> cotistas = new List<int>();

        foreach (PermissaoCotista permissao in permissoes)
        {
            cotistas.Add(permissao.IdCotista.Value);
        }

        return cotistas.ToArray();
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseCollection
    {
        public List<OperacaoCotistaViewModel> Collection;
        public JSONResponseCollection() { }
    }
}

