<%@ WebHandler Language="C#" Class="OrdemPgblList" %>

using System;
using System.Web;
using System.Text;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.InterfacesCustom;
using Financial.InterfacesCustom.PortoPar;

public class OrdemPgblList : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {

        JSONResponseCollection jsonResponse = new JSONResponseCollection();
        
        //fake return
        
        /*List<OrdemPgblViewModel> operacoes = new List<OrdemPgblViewModel>();
        OrdemPgblViewModel ordem = new OrdemPgblViewModel();
        ordem.DataOperacao = DateTime.Today;
        ordem.IdCarteira = 1;
        ordem.IdCotista = 1;
        ordem.IdOperacao = 1;
        ordem.NomeCarteira = "Fundo A";
        ordem.StatusProcessamento = "Digitado";
        ordem.TipoOperacaoString = "Aplica��o";
        ordem.Valor = 5000;
        operacoes.Add(ordem);
        jsonResponse.Collection = operacoes;
        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());
        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
        return;*/

        string filterParam = context.Request.Params["filter"];
        List<FilterViewModel> filters = new List<FilterViewModel>();

        if (!String.IsNullOrEmpty(filterParam))
        {
            filters = (List<FilterViewModel>)JsonConvert.DeserializeObject(filterParam, typeof(List<FilterViewModel>));
        }

        DateTime dataInicio = DateTime.Today.AddDays(-7);
        DateTime dataFim = DateTime.Today.AddYears(10);
        int? idCotista = null;
        int? idCarteira = null;
        
        foreach (FilterViewModel filter in filters)
        {
            string property = filter.property.ToLower();
            string value = Convert.ToString(filter.value);
            
            if (property == "periodo")
            {
                if (value == "last7")
                {
                    //Default
                }
                else if (value == "last30")
                {
                    dataInicio = DateTime.Today.AddDays(-30);
                }
                else if (value == "currentMonth")
                {
                    dataInicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                }
                else if (value == "period")
                {
                    foreach (FilterViewModel filterData in filters)
                    {
                        property = filterData.property.ToLower();
                        if (property == "datainicio")
                        {
                            dataInicio = Convert.ToDateTime(filterData.value);
                        }else if (property == "datafim")
                        {
                            dataFim = Convert.ToDateTime(filterData.value);
                        }
                    }
                }
            }else if(property == "idcotista"){
                idCotista = Convert.ToInt32(value);
            }
            else if (property == "idcarteira")
            {
                idCarteira = Convert.ToInt32(value);
            }
        }
        
        
        List<OrdemTesourariaViewModel> operacoes = new List<OrdemTesourariaViewModel>();
   
        
        Financial.InterfacesCustom.OrdemTesourariaCollection ordemPgblCollection = new Financial.InterfacesCustom.OrdemTesourariaCollection();
        ordemPgblCollection.Query.Where(
                                            ordemPgblCollection.Query.IdOperacaoMae.IsNull(),
                                            ordemPgblCollection.Query.Status.In((byte)StatusOrdemTesourariaPorto.PGBLAprovado,
                                                (byte)StatusOrdemTesourariaPorto.PGBLIntegrado, (byte)StatusOrdemTesourariaPorto.PGBLRejeitado),
                                                ordemPgblCollection.Query.DataOperacao.GreaterThanOrEqual(dataInicio),
                                            ordemPgblCollection.Query.DataOperacao.LessThanOrEqual(dataFim));
        
        if(idCotista.HasValue){
            ordemPgblCollection.Query.Where(ordemPgblCollection.Query.IdCotista.Equal(idCotista.Value));
        }

        if (idCarteira.HasValue)
        {
            ordemPgblCollection.Query.Where(ordemPgblCollection.Query.IdCarteira.Equal(idCarteira.Value));
        }
        
        ordemPgblCollection.Query.OrderBy(ordemPgblCollection.Query.DataOperacao.Descending);
        ordemPgblCollection.Query.Load();

        string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        string login = infoCotistaSplitted[2];
        
        PermissaoCotistaCollection permissoes = new PermissaoCotistaCollection();
        permissoes.BuscaCotistasAssociados(login);
        Dictionary<int, int> dicCotistas = new Dictionary<int, int>();
        
        foreach (PermissaoCotista permissao in permissoes)
        {
            dicCotistas.Add(permissao.IdCotista.Value, 1);
        }
        
        foreach (Financial.InterfacesCustom.OrdemTesouraria ordemPgbl in ordemPgblCollection)
        {
            if (dicCotistas.ContainsKey(ordemPgbl.IdCotista.Value))
            {
                operacoes.Add(new OrdemTesourariaViewModel(ordemPgbl));
            }
        }

        jsonResponse.Collection = operacoes;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
        
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseCollection
    {
        public List<OrdemTesourariaViewModel> Collection;
        public JSONResponseCollection() { }
    }
}

