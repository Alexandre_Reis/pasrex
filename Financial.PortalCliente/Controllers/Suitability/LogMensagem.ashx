<%@ WebHandler Language="C#" Class="SuitabilityDispensa" %>

using System;
using System.Web;
using System.Text;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.InvestidorCotista.Perfil;

public class SuitabilityDispensa : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        JSONResponseEntity jsonResponse = new JSONResponseEntity();

        string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        string login = infoCotistaSplitted[2];

        int idEvento = Convert.ToInt32(context.Request["IdMensagem"]);
        SuitabilityEventos evento = new SuitabilityEventos();
        evento.LoadByPrimaryKey(idEvento);
        int idMensagem = evento.IdMensagem.Value;
        
        int idCotista = Convert.ToInt32((Convert.ToString(context.Session["InfoCotista"]).Split('|'))[0]);
        string mensagem = context.Request["Mensagem"];
        string resposta = context.Request["Resposta"];
        
        ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

        ControllerPerfilCotista.LogMensagem logMensagem = new ControllerPerfilCotista.LogMensagem();

        logMensagem.IdCotista = idCotista.ToString();
        logMensagem.IdMensagem = idMensagem.ToString();
        logMensagem.Mensagem = mensagem;
        logMensagem.Resposta = resposta;
        
        SuitabilityLogMensagens.createSuitabilityLogMensagens(HttpContext.Current.User.Identity.Name,
                                                              Convert.ToInt32(logMensagem.IdCotista),
                                                              Convert.ToInt32(logMensagem.IdMensagem),
                                                              logMensagem.Mensagem,
                                                              logMensagem.Resposta);        
                
        jsonResponse.success = true;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }
   
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseEntity
    {
        public bool success;
        public JSONResponseEntity() { }
    }
}

