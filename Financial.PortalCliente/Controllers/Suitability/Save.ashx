<%@ WebHandler Language="C#" Class="SuitabilitySave" %>

using System;
using System.Web;
using System.Text;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using System.Configuration;
using System.Web.Configuration;
using System.Net.Configuration;
using Financial.InvestidorCotista.Perfil;

public class SuitabilitySave : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        JSONResponseEntity jsonResponse = new JSONResponseEntity();

        string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        string login = infoCotistaSplitted[2];
        
        //Usuario usuario = new Usuario();
        //usuario.BuscaUsuario(login);
        
        int idCotista = Convert.ToInt32((Convert.ToString(context.Session["InfoCotista"]).Split('|'))[0]);

        string[] paramsKeys = context.Request.Params.AllKeys;

        List<ControllerPerfilCotista.Resposta> respostas = new List<ControllerPerfilCotista.Resposta>();
        foreach (string paramKey in paramsKeys)
        {
            if (paramKey.ToLower().StartsWith("opcaoquestao-"))
            {
                int idQuestao = Convert.ToInt32((paramKey.Split('-'))[1]);
                int idOpcao = Convert.ToInt32(context.Request[paramKey]);

                ControllerPerfilCotista.Resposta resposta = new ControllerPerfilCotista.Resposta();
                resposta.idCotista = idCotista.ToString();
                resposta.idOpcao = idOpcao.ToString();
                resposta.idQuestao = idQuestao.ToString();

                respostas.Add(resposta);
            }
        }

        ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

        controllerPerfilCotista.aceitePerfil(idCotista, respostas, login);

        jsonResponse.perfilCotista = controllerPerfilCotista.carregaPerfil(idCotista);
        
        jsonResponse.success = true;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }
    
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseEntity
    {
        public bool success;
        public ControllerPerfilCotista.PerfilCotista perfilCotista;
        public JSONResponseEntity() { }
    }

}