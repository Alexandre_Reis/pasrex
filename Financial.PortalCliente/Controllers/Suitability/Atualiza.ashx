<%@ WebHandler Language="C#" Class="SuitabilityAtualiza" %>

using System;
using System.Web;
using System.Text;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.InvestidorCotista.Perfil;

public class SuitabilityAtualiza : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        JSONResponseEntity jsonResponse = new JSONResponseEntity();

        string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        string login = infoCotistaSplitted[2];

        int idCotista = Convert.ToInt32((Convert.ToString(context.Session["InfoCotista"]).Split('|'))[0]);

        List<ControllerPerfilCotista.Resposta> respostas = new List<ControllerPerfilCotista.Resposta>();
        string[] paramsKeys = context.Request.Params.AllKeys;
        foreach (string paramKey in paramsKeys)
        {
            if (paramKey.ToLower().StartsWith("opcaoquestao-"))
            {
                int idQuestao = Convert.ToInt32((paramKey.Split('-'))[1]);
                int idOpcao = Convert.ToInt32(context.Request[paramKey]);

                ControllerPerfilCotista.Resposta resposta = new ControllerPerfilCotista.Resposta();
                resposta.idCotista = idCotista.ToString();
                resposta.idOpcao = idOpcao.ToString();
                resposta.idQuestao = idQuestao.ToString();

                respostas.Add(resposta);
            }
        }

        ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

        ControllerPerfilCotista.PerfilCotista oldPerfilCotista = controllerPerfilCotista.carregaPerfil(idCotista);
        ControllerPerfilCotista.PerfilCotista perfilCotista = controllerPerfilCotista.apuraPerfil(respostas);
        perfilCotista.PerfilInvestimentos = oldPerfilCotista.PerfilInvestimentos;
        
        jsonResponse.success = true;
        jsonResponse.perfilCotista = perfilCotista;
        
        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }
   
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseEntity
    {
        public bool success;
        public ControllerPerfilCotista.PerfilCotista perfilCotista;
        public JSONResponseEntity() { }
    }

   
}

