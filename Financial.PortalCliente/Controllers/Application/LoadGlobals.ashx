<%@ WebHandler Language="C#" Class="LoadGlobals" %>

using System;
using System.Web;
using System.Text;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.ContaCorrente;
using Financial.InvestidorCotista.Perfil;

public class LoadGlobals : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        JSONResponse jsonResponse = new JSONResponse();
        string json;

        string infoCotista = Convert.ToString(context.Session["InfoCotista"]);
        string login;
        string[] infoCotistaSplitted;

        //Se estamos chamando esse ashx novamente apos a aplicacao ja estar iniciada para dar refresh nos valores
        bool reload = HttpContext.Current.Request["reload"] == "true";

        if (String.IsNullOrEmpty(infoCotista))
        {
            string cotistaParam = HttpContext.Current.Request["cotista"];
            login = String.IsNullOrEmpty(cotistaParam) ? HttpContext.Current.User.Identity.Name : cotistaParam;

        }
        else
        {
            infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
            login = infoCotistaSplitted[2];
        }

        Usuario usuario = new Usuario();
        if (!usuario.BuscaUsuario(login))
        {
            json = "alert('Usu�rio n�o encontrado')";

            context.Response.ContentType = "text/javascript";
            context.Response.Write(json);
            return;
        }

        PermissaoCotistaCollection permissaoCotistas = new PermissaoCotistaCollection();
        permissaoCotistas.BuscaCotistasAssociados(login);

        bool cotistaNaSession;

        Cotista cotista = new Cotista();
        Pessoa pessoa = new Pessoa();
        if (String.IsNullOrEmpty(infoCotista))
        {
            cotista.LoadByPrimaryKey(permissaoCotistas[0].IdCotista.Value);
            context.Session["InfoCotista"] = cotista.IdCotista.Value + "|" + cotista.Nome + "|" + login;
            cotistaNaSession = false;
        }
        else
        {
            cotistaNaSession = true;
        }

        bool cotistaPodeOperar = true;
        infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        int idCotista = Convert.ToInt32(infoCotistaSplitted[0]);
        string nomeCotista = infoCotistaSplitted[1];

        bool cotistaLoaded = cotista.LoadByPrimaryKey(idCotista);

        string pendenciaCadastral = "";
        if (cotistaLoaded)
        {
            pendenciaCadastral = cotista.PendenciaCadastral;
            bool cadastroExpirado = (cotista.DataExpiracao.HasValue && (cotista.DataExpiracao.Value.Date < DateTime.Today));
            if (cadastroExpirado || !String.IsNullOrEmpty(cotista.PendenciaCadastral))
            {
                cotistaPodeOperar = false;
                if (cadastroExpirado)
                {
                    pendenciaCadastral = "Cadastro expirado";
                }
            }
            
            pessoa.LoadByPrimaryKey(cotista.IdPessoa.Value);
            
        }

        UserInfoViewModel userInfo = new UserInfoViewModel();
        userInfo.FirstName = usuario.Nome;
        userInfo.LastName = usuario.Nome;
        userInfo.Login = nomeCotista;
        userInfo.IdCotista = idCotista;
        userInfo.UserId = usuario.IdUsuario.Value;
        userInfo.MultiCotista = permissaoCotistas.Count > 1;
        userInfo.CotistaNaSession = cotistaNaSession;
        userInfo.CPFCNPJ = pessoa.Cpfcnpj;
        userInfo.CotistaPodeOperar = cotistaPodeOperar;
        userInfo.PendenciaCadastral = pendenciaCadastral;
        userInfo.LoadPermissoes(usuario.IdGrupo.Value);

        FormaLiquidacaoCollection formasLiquidacao = new FormaLiquidacaoCollection();
        formasLiquidacao.LoadAll();

        jsonResponse.formasLiquidacao = new List<FormaLiquidacaoViewModel>();
        foreach (FormaLiquidacao formaLiquidacao in formasLiquidacao)
        {
            if (formaLiquidacao.Descricao.Contains("TED") || formaLiquidacao.Descricao.Contains("Conta Corrente") ||
                formaLiquidacao.Descricao.Contains("Cetip"))
            {
                jsonResponse.formasLiquidacao.Add(new FormaLiquidacaoViewModel(formaLiquidacao));
            }
        }

        if (cotistaLoaded)
        {
            ContaCorrenteCollection contaCorrenteCollection = cotista.BuscaContaCorrenteCollection();
            jsonResponse.contasCorrentes = new List<ContaCorrenteViewModel>();

            foreach (ContaCorrente contaCorrente in contaCorrenteCollection)
            {
                jsonResponse.contasCorrentes.Add(new ContaCorrenteViewModel(contaCorrente));
            }
        }

        const int ID_AGENTE_MERCADO_PORTO = 10017;
        Financial.Common.AgenteMercado agentePorto = new Financial.Common.AgenteMercado();
        if (agentePorto.LoadByPrimaryKey(ID_AGENTE_MERCADO_PORTO))
        {
            jsonResponse.enderecoPorto = String.Format("{0} - {1} - {2} - cep: {3}", agentePorto.Endereco, agentePorto.Bairro, agentePorto.Cidade, agentePorto.Cep);
        }

        jsonResponse.baseDirectory = VirtualPathUtility.ToAbsolute("~/");
        jsonResponse.isTesouraria = context.Request["isTesouraria"] == "true";
        context.Session["IsTesouraria"] = jsonResponse.isTesouraria;
        jsonResponse.emailBackOffice = jsonResponse.isTesouraria ? Financial.Util.ParametrosConfiguracaoSistema.Outras.EmailBackOfficeInstitucional :
            Financial.Util.ParametrosConfiguracaoSistema.Outras.EmailBackOffice;
        jsonResponse.pathHistoricoLog = System.Configuration.ConfigurationManager.AppSettings["PathHistoricoLog"];
        jsonResponse.userInfo = userInfo;


        PessoaSuitability pessoaSuitability = new PessoaSuitability();
        if (cotistaLoaded)
        {
            
            pessoaSuitability.LoadByPrimaryKey(pessoa.IdPessoa.Value);

            jsonResponse.suitability = new SuitabilityViewModel(pessoaSuitability.IdValidacao.GetValueOrDefault(0));
            
            jsonResponse.suitability.LoadRespostas(pessoaSuitability, cotista.IdCotista.Value);
            ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();
            jsonResponse.perfilCotista = controllerPerfilCotista.carregaPerfil(cotista.IdCotista.Value);
            
            
            
            //jsonResponse.perfilCotistaDentroValidade = (pessoaSuitability.UltimaAlteracao.HasValue && (DateTime.Now <= controllerPerfilCotista.retornaValidade(pessoaSuitability.UltimaAlteracao.Value)));
        }

        jsonResponse.msgsSuitability = new Dictionary<string, string>();
        SuitabilityEventosCollection eventos = new SuitabilityEventosCollection();
        eventos.LoadAll();
        foreach (SuitabilityEventos evento in eventos)
        {
            if (evento.Descricao.ToLower().Contains("portal") && evento.IdMensagem.HasValue)
            {
                SuitabilityMensagens mensagem = evento.UpToSuitabilityMensagensByIdMensagem;
                string msgDescricao = mensagem.Descricao.Replace("\n", "<br />");
                if (msgDescricao.Contains("|"))
                {
                    msgDescricao = msgDescricao.Split('|')[0];
                }
                jsonResponse.msgsSuitability.Add("msg_" + evento.IdSuitabilityEventos.Value, msgDescricao);
            }
        }
        
        jsonResponse.definicaoPerfilInvestidor = new Dictionary<string,string>();

        if (pessoaSuitability.IdValidacao.HasValue)
        {
            SuitabilityParametroPerfilCollection parametros = new SuitabilityParametroPerfilCollection();
            parametros.LoadAll();
            foreach (SuitabilityParametroPerfil parametro in parametros)
            {
                SuitabilityPerfilInvestidor perfil = parametro.UpToSuitabilityPerfilInvestidorByPerfilInvestidor;
                if (parametro.IdValidacao.Value == pessoaSuitability.IdValidacao.Value)
                {
                    jsonResponse.definicaoPerfilInvestidor.Add(perfil.Perfil, parametro.Definicao);
                }
            }
        }

        SuitabilityTipoDispensaCollection tiposDispensa = new SuitabilityTipoDispensaCollection();
        tiposDispensa.LoadAll();
        jsonResponse.tiposDispensa = new List<SuitabilityTipoDispensaViewModel>();

        jsonResponse.tiposDispensa.Add(new SuitabilityTipoDispensaViewModel(0, "N�o aplic�vel"));
        foreach (SuitabilityTipoDispensa tipoDispensa in tiposDispensa)
        {
            jsonResponse.tiposDispensa.Add(new SuitabilityTipoDispensaViewModel(tipoDispensa));
        }

        jsonResponse.today = DateTime.Today;
        jsonResponse.aplicacoesPendentes = TotalizaAplicacoesPendentes(idCotista);
        jsonResponse.disclaimerPosicao = Financial.Util.ParametrosConfiguracaoSistema.Outras.DisclaimerPosicao;
        jsonResponse.disclaimerOperacao = Financial.Util.ParametrosConfiguracaoSistema.Outras.DisclaimerOperacao;

        jsonResponse.downloadExtrato = new DownloadExtratoViewModel(idCotista);


        string descricaoHistorico = String.Format("Cotista Selecionado: {0}", context.Session["InfoCotista"]);

        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        descricaoHistorico,
                                        HttpContext.Current.User.Identity.Name,
                                        "",
                                        "",
                                        Convert.ToBoolean(context.Session["IsTesouraria"]) ?
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalInstitucional :
                                            Financial.Security.Enums.HistoricoLogOrigem.PortalCotista);


        json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        if (!reload)
        {
            json = "CRMax = {}; CRMax.globals = {}; CRMax.globals = " + json;
        }

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }

    public decimal TotalizaAplicacoesPendentes(int idCotista)
    {

        ClienteQuery clienteQuery = new ClienteQuery("C");
        OrdemCotistaQuery ordemQuery = new OrdemCotistaQuery("O");

        ordemQuery.Select(ordemQuery.ValorBruto.Sum());
        ordemQuery.LeftJoin(clienteQuery).On(ordemQuery.IdCarteira.Equal(clienteQuery.IdCliente));
        ordemQuery.Where(ordemQuery.DataOperacao.GreaterThanOrEqual(clienteQuery.DataDia),
            ordemQuery.TipoOperacao.Equal((byte)TipoOperacaoCotista.Aplicacao),
            ordemQuery.StatusIntegracao.NotIn((byte)StatusOrdemIntegracao.Cancelado, (byte)StatusOrdemIntegracao.OutroErro, (byte)StatusOrdemIntegracao.SemSaldo),
            ordemQuery.IdCotista.Equal(idCotista));

        OrdemCotistaCollection ordens = new OrdemCotistaCollection();

        decimal valorTotalOrdens = 0;

        if (ordens.Load(ordemQuery))
        {
            valorTotalOrdens = ordens.Count > 0 && ordens[0] != null && ordens[0].ValorBruto.HasValue ?
                ordens[0].ValorBruto.Value : 0;
        }

        return valorTotalOrdens;

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponse
    {
        public UserInfoViewModel userInfo;
        public bool isTesouraria;
        public string pathHistoricoLog;
        public string baseDirectory;
        public string enderecoPorto;
        public string emailBackOffice;
        public SuitabilityViewModel suitability;
        public Dictionary<string, string> msgsSuitability;
        public Dictionary<string, string> definicaoPerfilInvestidor;
        public DownloadExtratoViewModel downloadExtrato;
        public JSONResponse() { }
        public List<SuitabilityTipoDispensaViewModel> tiposDispensa;
        public List<ContaCorrenteViewModel> contasCorrentes;
        public List<FormaLiquidacaoViewModel> formasLiquidacao;
        public DateTime today;
        public decimal aplicacoesPendentes;
        public string disclaimerPosicao;
        public string disclaimerOperacao;
        

        public ControllerPerfilCotista.PerfilCotista perfilCotista;

    }
}