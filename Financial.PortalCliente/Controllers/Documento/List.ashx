<%@ WebHandler Language="C#" Class="DocumentoList" %>

using System;
using System.Web;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.Security;
using System.IO;

public class DocumentoList : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {

        int idCotista = Convert.ToInt32((Convert.ToString(context.Session["InfoCotista"]).Split('|'))[0]);

        List<DocumentoViewModel> documentos = new List<DocumentoViewModel>();

        string diretorioDownload = System.Configuration.ConfigurationManager.AppSettings["DiretorioDownload"];
        string diretorioPublic = String.Format(@"{0}\Public", diretorioDownload);
        string diretorioCotista = String.Format(@"{0}\Cotista{1}", diretorioDownload, idCotista);
        string diretorioFundoTemplate = String.Format(@"{0}\{1}", diretorioDownload, "Fundo{0}");

        //string[] filesCotista;

        string[] filesPublic = Directory.GetFiles(diretorioPublic);
        List<string> files = new List<string>();
        foreach (string file in filesPublic)
        {
            DocumentoViewModel documento = new DocumentoViewModel();
            documento.Nome = Path.GetFileName(file);
            documento.Tipo = "public";
            documento.Path = file.Replace(diretorioDownload, "");
            documentos.Add(documento);
        }

        /*if (Directory.Exists(diretorioCotista))
        {
            filesCotista = Directory.GetFiles(diretorioCotista);
            foreach (string file in filesCotista)
            {
                DocumentoViewModel documento = new DocumentoViewModel();
                documento.Nome = Path.GetFileName(file);
                documento.Tipo = "cotista";
                documento.Path = file.Replace(diretorioDownload, "");
                documentos.Add(documento);
            }
        }*/

        //Pesquisar diretorios que contenham documentos de fundos que o cotista tenha acesso
        /*
         string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        string login = infoCotistaSplitted[2];
        
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(login);

        PermissaoClienteCollection permissoes = new PermissaoClienteCollection();
        permissoes.Query.Where(permissoes.Query.IdUsuario.Equal(usuario.IdUsuario.Value));
        permissoes.Load(permissoes.Query);

        foreach (PermissaoCliente permissao in permissoes)
        {
            int idFundo = permissao.IdCliente.Value;
            string diretorioFundo = String.Format(diretorioFundoTemplate, idFundo);
            if (Directory.Exists(diretorioFundo))
            {
                string[] filesFundo = Directory.GetFiles(diretorioFundo);
                foreach (string file in filesFundo)
                {
                    DocumentoViewModel documento = new DocumentoViewModel();
                    documento.Nome = Path.GetFileName(file);
                    documento.Tipo = "fundo";
                    documento.Path = file.Replace(diretorioDownload, "");
                    documentos.Add(documento); //Por enquanto nao adicionar documetos do fundo pois os links estao no contexto do site
                }
            }
            
        }*/

        JSONResponseCollection jsonResponse = new JSONResponseCollection();
        jsonResponse.Collection = documentos;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseCollection
    {
        public List<DocumentoViewModel> Collection;
        public JSONResponseCollection() { }
    }
}