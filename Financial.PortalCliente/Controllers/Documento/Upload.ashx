<%@ WebHandler Language="C#" Class="DocumentoUpload" %>

using System;
using System.IO;
using System.Web;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.Security;

public class DocumentoUpload : IHttpHandler, System.Web.SessionState.IRequiresSessionState {
    
    public void ProcessRequest (HttpContext context) {

        int idCotista = Convert.ToInt32((Convert.ToString(context.Session["InfoCotista"]).Split('|'))[0]);
        
        HttpPostedFile file = context.Request.Files[0];

        string diretorioUpload = System.Configuration.ConfigurationManager.AppSettings["DiretorioUpload"];
        string diretorioUploadCotista = String.Format(@"{0}\Cotista{1}", diretorioUpload, idCotista);
        if (!Directory.Exists(diretorioUploadCotista))
        {
            Directory.CreateDirectory(diretorioUploadCotista);
        }
        
        string filename = String.Format(@"{0}\{1}", diretorioUploadCotista, file.FileName);
        file.SaveAs(filename);
        
        JSONResponseEntity jsonResponse = new JSONResponseEntity();
        jsonResponse.success = true;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());
        
        context.Response.ContentType = "text/html";
        context.Response.Write(json);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    public class JSONResponseEntity
    {
        public bool success;
    }
}