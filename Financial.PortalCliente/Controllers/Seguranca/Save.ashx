<%@ WebHandler Language="C#" Class="SegurancaSave" %>

using System;
using System.Web;
using System.Text;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista.Enums;
using EntitySpaces.Interfaces;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;

public class SegurancaSave : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        string emailBackOffice;
        int idConfig;
        bool isTesouraria = context.Request["isTesouraria"] == "true";
        string operacao = context.Request["operacao"];
        JSONResponseEntity jsonResponse = new JSONResponseEntity();
        
        
        if (operacao == "update-disclaimer")
        {
            string disclaimerOperacao = context.Request["disclaimerOperacao"];
            string disclaimerPosicao = context.Request["disclaimerPosicao"];
            
            UpdateDisclaimer(disclaimerOperacao, (int)Financial.Util.Enums.ConfiguracaoRegrasNegocio.Outras.DisclaimerOperacao, "Operacao");
            UpdateDisclaimer(disclaimerPosicao, (int)Financial.Util.Enums.ConfiguracaoRegrasNegocio.Outras.DisclaimerPosicao, "Posicao");

            string descricaoHistorico = String.Format("Atualiza��o do Disclaimer: {0}", "Opera��o: " + disclaimerOperacao + "Posi��o: " + disclaimerPosicao);
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            descricaoHistorico,
                                            HttpContext.Current.User.Identity.Name,
                                            "",
                                            "",
                                            Convert.ToBoolean(context.Session["IsTesouraria"]) ?
                                                Financial.Security.Enums.HistoricoLogOrigem.PortalInstitucional :
                                                Financial.Security.Enums.HistoricoLogOrigem.PortalCotista);
        }
        else
        {
            if (isTesouraria)
            {
                emailBackOffice = context.Request["emailBackOfficeInstitucional"];
                idConfig = (int)Financial.Util.Enums.ConfiguracaoRegrasNegocio.Outras.EmailBackOfficeInstitucional;
            }
            else
            {
                emailBackOffice = context.Request["emailBackOffice"];
                idConfig = (int)Financial.Util.Enums.ConfiguracaoRegrasNegocio.Outras.EmailBackOffice;
            }

            Financial.Util.ConfiguracaoSistema.Configuracao config = new Financial.Util.ConfiguracaoSistema.Configuracao();

            if (!config.LoadByPrimaryKey(idConfig))
            {
                string institucional = isTesouraria ? " Institucional" : "";
                config.Descricao = "Email Back-office" + institucional;
                config.Id = idConfig;
            }
            config.ValorTexto = emailBackOffice;
            config.Save();

            string descricaoHistorico = String.Format("Atualiza��o do Email BackOffice: {0}", emailBackOffice);
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            descricaoHistorico,
                                            HttpContext.Current.User.Identity.Name,
                                            "",
                                            "",
                                            Convert.ToBoolean(context.Session["IsTesouraria"]) ?
                                                Financial.Security.Enums.HistoricoLogOrigem.PortalInstitucional :
                                                Financial.Security.Enums.HistoricoLogOrigem.PortalCotista);
        }
        
        jsonResponse.success = true;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());

        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }

    public void UpdateDisclaimer(string disclaimer, int idConfig, string tipoDisclaimer)
    {
        Financial.Util.ConfiguracaoSistema.Configuracao config = new Financial.Util.ConfiguracaoSistema.Configuracao();

        if (!config.LoadByPrimaryKey(idConfig))
        {
            config.Descricao = "Disclaimer " + tipoDisclaimer;
            config.Id = idConfig;
        }
        config.ValorTexto = disclaimer;
        config.Save();
    }
    
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseEntity
    {
        public bool success;
        public JSONResponseEntity() { }
    }
}

