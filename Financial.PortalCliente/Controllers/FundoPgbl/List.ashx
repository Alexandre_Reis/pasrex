<%@ WebHandler Language="C#" Class="FundoCotistaList" %>

using System;
using System.Web;
using Financial.InvestidorCotista;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using Financial.Fundo;
using Financial.CRM;
using Financial.Security;
using Financial.Investidor;
using Financial.Investidor.Enums;

public class FundoCotistaList : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context) {

        string[] infoCotistaSplitted = Convert.ToString(context.Session["InfoCotista"]).Split('|');
        string login = infoCotistaSplitted[2];
        
        int idCotista = Convert.ToInt32((Convert.ToString(context.Session["InfoCotista"]).Split('|'))[0]);
        
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(login);
        
       
        JSONResponseCollection jsonResponse = new JSONResponseCollection();
        List<CarteiraViewModel> fundos = new List<CarteiraViewModel>();
        
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        CategoriaFundoQuery categoriaFundoQuery = new CategoriaFundoQuery("F");

        carteiraQuery.Where(categoriaFundoQuery.Descricao.ToLower().Like("%previd%"));
        carteiraQuery.LeftJoin(categoriaFundoQuery).On(categoriaFundoQuery.IdCategoria.Equal(carteiraQuery.IdCategoria));
        CarteiraCollection carteiras = new CarteiraCollection();
        carteiras.Load(carteiraQuery);
        
        foreach (Carteira carteira in carteiras)
        {
            
                CarteiraViewModel carteiraViewModel = new CarteiraViewModel(carteira);
                
                fundos.Add(carteiraViewModel);
            
        }
        
        jsonResponse.Collection = fundos;

        string json = JsonConvert.SerializeObject(jsonResponse, Formatting.Indented, new JavaScriptDateTimeConverter());
        
        context.Response.ContentType = "text/javascript";
        context.Response.Write(json);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class JSONResponseCollection
    {
        public List<CarteiraViewModel> Collection;
        public JSONResponseCollection() { }
    }


}