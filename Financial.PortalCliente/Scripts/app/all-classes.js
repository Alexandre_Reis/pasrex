/*
Copyright(c) 2012 Company Name
*/
(function () {
    /**
    * @class Ext.ux.form.field.ClearButton
    *
    * Plugin for text components that shows a "clear" button over the text field.
    * When the button is clicked the text field is set empty.
    * Icon image and positioning can be controlled using CSS.
    * Works with Ext.form.field.Text, Ext.form.field.TextArea, Ext.form.field.ComboBox and Ext.form.field.Date.
    *
    * Plugin alias is 'clearbutton' (use "plugins: 'clearbutton'" in GridPanel config).
    *
    * @author <a href="mailto:stephen.friedrich@fortis-it.de">Stephen Friedrich</a>
    * @author <a href="mailto:fabian.urban@fortis-it.de">Fabian Urban</a>
    *
    * @copyright (c) 2011 Fortis IT Services GmbH
    * @license Ext.ux.form.field.ClearButton is released under the
    * <a target="_blank" href="http://www.apache.org/licenses/LICENSE-2.0">Apache License, Version 2.0</a>.
    *
    */
    Ext.define('Ext.ux.form.field.ClearButton', {
        alias: 'plugin.clearbutton',

        /**
        * @cfg {Boolean} Hide the clear button when the field is empty (default: true).
        */
        hideClearButtonWhenEmpty: true,

        /**
        * @cfg {Boolean} Hide the clear button until the mouse is over the field (default: true).
        */
        hideClearButtonWhenMouseOut: false,

        /**
        * @cfg {Boolean} When the clear buttons is hidden/shown, this will animate the button to its new state (using opacity) (default: true).
        */
        animateClearButton: true,

        /**
        * @cfg {Boolean} Empty the text field when ESC is pressed while the text field is focused.
        */
        clearOnEscape: true,

        /**
        * @cfg {String} CSS class used for the button div.
        * Also used as a prefix for other classes (suffixes: '-mouse-over-input', '-mouse-over-button', '-mouse-down', '-on', '-off')
        */
        clearButtonCls: 'ext-ux-clearbutton',

        /**
        * The text field (or text area, combo box, date field) that we are attached to
        */
        textField: null,

        /**
        * Will be set to true if animateClearButton is true and the browser supports CSS 3 transitions
        * @private
        */
        animateWithCss3: false,

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Set up and tear down
        //
        /////////////////////////////////////////////////////////////////////////////////////////////////////

        constructor: function (cfg) {
            Ext.apply(this, cfg);

            this.callParent(arguments);
        },

        /**
        * Called by plug-in system to initialize the plugin for a specific text field (or text area, combo box, date field).
        * Most all the setup is delayed until the component is rendered.
        */
        init: function (textField) {
            this.textField = textField;
            if (!textField.rendered) {
                textField.on('afterrender', this.handleAfterRender, this);
            }
            else {
                // probably an existing input element transformed to extjs field
                this.handleAfterRender();
            }
        },

        /**
        * After the field has been rendered sets up the plugin (create the Element for the clear button, attach listeners).
        * @private
        */
        handleAfterRender: function (textField) {
            this.isTextArea = (this.textField.inputEl.dom.type.toLowerCase() == 'textarea');

            this.createClearButtonEl();
            this.addListeners();

            this.repositionClearButton();
            this.updateClearButtonVisibility();

            this.addEscListener();
        },

        /**
        * Creates the Element and DOM for the clear button
        */
        createClearButtonEl: function () {
            var animateWithClass = this.animateClearButton && this.animateWithCss3;
            this.clearButtonEl = this.textField.bodyEl.createChild({
                tag: 'div',
                cls: this.clearButtonCls
            });
            if (this.animateClearButton) {
                this.animateWithCss3 = this.supportsCssTransition(this.clearButtonEl);
            }
            if (this.animateWithCss3) {
                this.clearButtonEl.addCls(this.clearButtonCls + '-off');
            }
            else {
                this.clearButtonEl.setStyle('visibility', 'hidden');
            }
        },

        /**
        * Returns true iff the browser supports CSS 3 transitions
        * @param el an element that is checked for support of the "transition" CSS property (considering any
        *           vendor prefixes)
        */
        supportsCssTransition: function (el) {
            var styles = ['transitionProperty', 'WebkitTransitionProperty', 'MozTransitionProperty',
                          'OTransitionProperty', 'msTransitionProperty', 'KhtmlTransitionProperty'];

            var style = el.dom.style;
            for (var i = 0, length = styles.length; i < length; ++i) {
                if (style[styles[i]] !== 'undefined') {
                    // Supported property will result in empty string
                    return true;
                }
            }
            return false;
        },

        /**
        * If config option "clearOnEscape" is true, then add a key listener that will clear this field
        */
        addEscListener: function () {
            if (!this.clearOnEscape) {
                return;
            }

            // Using a KeyMap did not work: ESC is swallowed by combo box and date field before it reaches our own KeyMap
            this.textField.inputEl.on('keydown',
                function (e) {
                    if (e.getKey() == Ext.EventObject.ESC) {
                        if (this.textField.isExpanded) {
                            // Let combo box or date field first remove the popup
                            return;
                        }
                        // No idea why the defer is necessary, but otherwise the call to setValue('') is ignored
                        Ext.Function.defer(this.textField.setValue, 1, this.textField, ['']);
                        e.stopEvent();
                    }
                },
                this);
        },

        /**
        * Adds listeners to the field, its input element and the clear button to handle resizing, mouse over/out events, click events etc.
        */
        addListeners: function () {
            // listeners on input element (DOM/El level)
            var textField = this.textField;
            var bodyEl = textField.bodyEl;
            bodyEl.on('mouseover', this.handleMouseOverInputField, this);
            bodyEl.on('mouseout', this.handleMouseOutOfInputField, this);

            // listeners on text field (component level)
            textField.on('destroy', this.handleDestroy, this);
            textField.on('resize', this.repositionClearButton, this);
            textField.on('change', function () {
                this.repositionClearButton();
                this.updateClearButtonVisibility();
            }, this);

            // listeners on clear button (DOM/El level)
            var clearButtonEl = this.clearButtonEl;
            clearButtonEl.on('mouseover', this.handleMouseOverClearButton, this);
            clearButtonEl.on('mouseout', this.handleMouseOutOfClearButton, this);
            clearButtonEl.on('mousedown', this.handleMouseDownOnClearButton, this);
            clearButtonEl.on('mouseup', this.handleMouseUpOnClearButton, this);
            clearButtonEl.on('click', this.handleMouseClickOnClearButton, this);
        },

        /**
        * When the field is destroyed, we also need to destroy the clear button Element to prevent memory leaks.
        */
        handleDestroy: function () {
            this.clearButtonEl.destroy();
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Mouse event handlers
        //
        /////////////////////////////////////////////////////////////////////////////////////////////////////

        /**
        * Tada - the real action: If user left clicked on the clear button, then empty the field
        */
        handleMouseClickOnClearButton: function (event, htmlElement, object) {
            if (!this.isLeftButton(event)) {
                return;
            }
            this.textField.setValue('');
            this.textField.focus();
        },

        handleMouseOverInputField: function (event, htmlElement, object) {
            this.clearButtonEl.addCls(this.clearButtonCls + '-mouse-over-input');
            if (event.getRelatedTarget() == this.clearButtonEl.dom) {
                // Moused moved to clear button and will generate another mouse event there.
                // Handle it here to avoid duplicate updates (else animation will break)
                this.clearButtonEl.removeCls(this.clearButtonCls + '-mouse-over-button');
                this.clearButtonEl.removeCls(this.clearButtonCls + '-mouse-down');
            }
            this.updateClearButtonVisibility();
        },

        handleMouseOutOfInputField: function (event, htmlElement, object) {
            this.clearButtonEl.removeCls(this.clearButtonCls + '-mouse-over-input');
            if (event.getRelatedTarget() == this.clearButtonEl.dom) {
                // Moused moved from clear button and will generate another mouse event there.
                // Handle it here to avoid duplicate updates (else animation will break)
                this.clearButtonEl.addCls(this.clearButtonCls + '-mouse-over-button');
            }
            this.updateClearButtonVisibility();
        },

        handleMouseOverClearButton: function (event, htmlElement, object) {
            event.stopEvent();
            if (this.textField.bodyEl.contains(event.getRelatedTarget())) {
                // has been handled in handleMouseOutOfInputField() to prevent double update
                return;
            }
            this.clearButtonEl.addCls(this.clearButtonCls + '-mouse-over-button');
            this.updateClearButtonVisibility();
        },

        handleMouseOutOfClearButton: function (event, htmlElement, object) {
            event.stopEvent();
            if (this.textField.bodyEl.contains(event.getRelatedTarget())) {
                // will be handled in handleMouseOverInputField() to prevent double update
                return;
            }
            this.clearButtonEl.removeCls(this.clearButtonCls + '-mouse-over-button');
            this.clearButtonEl.removeCls(this.clearButtonCls + '-mouse-down');
            this.updateClearButtonVisibility();
        },

        handleMouseDownOnClearButton: function (event, htmlElement, object) {
            if (!this.isLeftButton(event)) {
                return;
            }
            this.clearButtonEl.addCls(this.clearButtonCls + '-mouse-down');
        },

        handleMouseUpOnClearButton: function (event, htmlElement, object) {
            if (!this.isLeftButton(event)) {
                return;
            }
            this.clearButtonEl.removeCls(this.clearButtonCls + '-mouse-down');
        },

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // Utility methods
        //
        /////////////////////////////////////////////////////////////////////////////////////////////////////

        /**
        * Repositions the clear button element based on the textfield.inputEl element
        * @private
        */
        repositionClearButton: function () {
            var clearButtonEl = this.clearButtonEl;
            if (!clearButtonEl) {
                return;
            }
            var clearButtonPosition = this.calculateClearButtonPosition(this.textField);
            clearButtonEl.dom.style.right = clearButtonPosition.right + 'px';
            clearButtonEl.dom.style.top = clearButtonPosition.top + 'px';
        },

        /**
        * Calculates the position of the clear button based on the textfield.inputEl element
        * @private
        */
        calculateClearButtonPosition: function (textField) {
            var positions = textField.inputEl.getBox(true, true);
            var top = positions.y;
            var right = positions.x;
            if (this.fieldHasScrollBar()) {
                right += Ext.getScrollBarWidth();
            }
            if (this.textField.triggerWrap) {
                right += this.textField.getTriggerWidth();
            }
            return {
                right: right,
                top: top
            };
        },

        /**
        * Checks if the field we are attached to currently has a scrollbar
        */
        fieldHasScrollBar: function () {
            if (!this.isTextArea) {
                return false;
            }

            var inputEl = this.textField.inputEl;
            var overflowY = inputEl.getStyle('overflow-y');
            if (overflowY == 'hidden' || overflowY == 'visible') {
                return false;
            }
            if (overflowY == 'scroll') {
                return true;
            }
            //noinspection RedundantIfStatementJS
            if (inputEl.dom.scrollHeight <= inputEl.dom.clientHeight) {
                return false;
            }
            return true;
        },


        /**
        * Small wrapper around clearButtonEl.isVisible() to handle setVisible animation that may still be in progress.
        */
        isButtonCurrentlyVisible: function () {
            if (this.animateClearButton && this.animateWithCss3) {
                return this.clearButtonEl.hasCls(this.clearButtonCls + '-on');
            }

            // This should not be necessary (see Element.setVisible/isVisible), but else there is confusion about visibility
            // when moving the mouse out and _quickly_ over then input again.
            var cachedVisible = Ext.core.Element.data(this.clearButtonEl.dom, 'isVisible');
            if (typeof (cachedVisible) == 'boolean') {
                return cachedVisible;
            }
            return this.clearButtonEl.isVisible();
        },

        /**
        * Checks config options and current mouse status to determine if the clear button should be visible.
        */
        shouldButtonBeVisible: function () {
            if (this.hideClearButtonWhenEmpty && Ext.isEmpty(this.textField.getValue())) {
                return false;
            }

            var clearButtonEl = this.clearButtonEl;
            //noinspection RedundantIfStatementJS
            if (this.hideClearButtonWhenMouseOut
                && !clearButtonEl.hasCls(this.clearButtonCls + '-mouse-over-button')
                && !clearButtonEl.hasCls(this.clearButtonCls + '-mouse-over-input')) {
                return false;
            }

            return true;
        },

        /**
        * Called after any event that may influence the clear button visibility.
        */
        updateClearButtonVisibility: function () {
            var oldVisible = this.isButtonCurrentlyVisible();
            var newVisible = this.shouldButtonBeVisible();

            var clearButtonEl = this.clearButtonEl;
            if (oldVisible != newVisible) {
                if (this.animateClearButton && this.animateWithCss3) {
                    this.clearButtonEl.removeCls(this.clearButtonCls + (oldVisible ? '-on' : '-off'));
                    clearButtonEl.addCls(this.clearButtonCls + (newVisible ? '-on' : '-off'));
                }
                else {
                    clearButtonEl.stopAnimation();
                    clearButtonEl.setVisible(newVisible, this.animateClearButton);
                }

                // Set background-color of clearButton to same as field's background-color (for those browsers/cases
                // where the padding-right (see below) does not work)
                clearButtonEl.setStyle('background-color', this.textField.inputEl.getStyle('background-color'));

                // Adjust padding-right of the input tag to make room for the button
                // IE (up to v9) just ignores this and Gecko handles padding incorrectly with  textarea scrollbars
                if (!(this.isTextArea && Ext.isGecko) && !Ext.isIE) {
                    // See https://bugzilla.mozilla.org/show_bug.cgi?id=157846
                    var deltaPaddingRight = clearButtonEl.getWidth() - this.clearButtonEl.getMargin('l');
                    var currentPaddingRight = this.textField.inputEl.getPadding('r');
                    var factor = (newVisible ? +1 : -1);
                    this.textField.inputEl.dom.style.paddingRight = (currentPaddingRight + factor * deltaPaddingRight) + 'px';
                }
            }
        },

        isLeftButton: function (event) {
            return event.button === 0;
        }

    });

})();
/*
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as published by the Free Software Foundation and appearing in the file LICENSE included in the packaging of this file.  Please review the following information to ensure the GNU General Public License version 3.0 requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * http://www.gnu.org/licenses/lgpl.html
 *
 * @description: This class provide aditional format to numbers by extending Ext.form.field.Number
 *
 * @author: Greivin Britton
 * @email: brittongr@gmail.com
 * @version: 2 compatible with ExtJS 4
 */
Ext.define('Ext.ux.form.NumericField', 
{
    extend: 'Ext.form.field.Number',//Extending the NumberField
    alias: 'widget.numericfield',//Defining the xtype,
    currencySymbol: null,
    useThousandSeparator: true,
    thousandSeparator: ',',
    alwaysDisplayDecimals: false,
    fieldStyle: 'text-align: right;',
	initComponent: function(){
        if (this.useThousandSeparator && this.decimalSeparator == ',' && this.thousandSeparator == ',') 
            this.thousandSeparator = '.';
        else 
            if (this.allowDecimals && this.thousandSeparator == '.' && this.decimalSeparator == '.') 
                this.decimalSeparator = ',';
        
        this.callParent(arguments);
    },
    setValue: function(value){
        Ext.ux.form.NumericField.superclass.setValue.call(this, value != null ? value.toString().replace('.', this.decimalSeparator) : value);
        
        this.setRawValue(this.getFormattedValue(this.getValue()));
    },
    getFormattedValue: function(value){
        if (Ext.isEmpty(value) || !this.hasFormat()) 
            return value;
        else 
        {
            var neg = null;
            
            value = (neg = value < 0) ? value * -1 : value;
            value = this.allowDecimals && this.alwaysDisplayDecimals ? value.toFixed(this.decimalPrecision) : value;
            
            if (this.useThousandSeparator) 
            {
                if (this.useThousandSeparator && Ext.isEmpty(this.thousandSeparator)) 
                    throw ('NumberFormatException: invalid thousandSeparator, property must has a valid character.');
                
                if (this.thousandSeparator == this.decimalSeparator) 
                    throw ('NumberFormatException: invalid thousandSeparator, thousand separator must be different from decimalSeparator.');
                
                value = value.toString();
                
                var ps = value.split('.');
                ps[1] = ps[1] ? ps[1] : null;
                
                var whole = ps[0];
                
                var r = /(\d+)(\d{3})/;
                
                var ts = this.thousandSeparator;
                
                while (r.test(whole)) 
                    whole = whole.replace(r, '$1' + ts + '$2');
                
                value = whole + (ps[1] ? this.decimalSeparator + ps[1] : '');
            }
            
            return Ext.String.format('{0}{1}{2}', (neg ? '-' : ''), (Ext.isEmpty(this.currencySymbol) ? '' : this.currencySymbol + ' '), value);
        }
    },
    /**
     * overrides parseValue to remove the format applied by this class
     */
    parseValue: function(value){
        //Replace the currency symbol and thousand separator
        return Ext.ux.form.NumericField.superclass.parseValue.call(this, this.removeFormat(value));
    },
    /**
     * Remove only the format added by this class to let the superclass validate with it's rules.
     * @param {Object} value
     */
    removeFormat: function(value){
        if (Ext.isEmpty(value) || !this.hasFormat()) 
            return value;
        else 
        {
            value = value.toString().replace(this.currencySymbol + ' ', '');
            
            value = this.useThousandSeparator ? value.replace(new RegExp('[' + this.thousandSeparator + ']', 'g'), '') : value;
            
            return value;
        }
    },
    /**
     * Remove the format before validating the the value.
     * @param {Number} value
     */
    getErrors: function(value){
        return Ext.ux.form.NumericField.superclass.getErrors.call(this, this.removeFormat(value));
    },
    hasFormat: function(){
        return this.decimalSeparator != '.' || (this.useThousandSeparator == true && this.getRawValue() != null) || !Ext.isEmpty(this.currencySymbol) || this.alwaysDisplayDecimals;
    },
    /**
     * Display the numeric value with the fixed decimal precision and without the format using the setRawValue, don't need to do a setValue because we don't want a double
     * formatting and process of the value because beforeBlur perform a getRawValue and then a setValue.
     */
    onFocus: function(){
        this.setRawValue(this.removeFormat(this.getRawValue()));
        
        this.callParent(arguments);
    }
});

/**
* BoxSelect for ExtJS 4.1, a combo box improved for multiple value querying, selection and management.
*
* A friendlier combo box for multiple selections that creates easily individually
* removable labels for each selection, as seen on facebook and other sites. Querying
* and type-ahead support are also improved for multiple selections.
*
* Options and usage mostly remain consistent with the standard
* [ComboBox](http://docs.sencha.com/ext-js/4-1/#!/api/Ext.form.field.ComboBox) control.
* Some default configuration options have changed, but most should still work properly
* if overridden unless otherwise noted.
*
* Please note, this component does not support versions of ExtJS earlier than 4.1.
*
* Inspired by the [SuperBoxSelect component for ExtJS 3](http://technomedia.co.uk/SuperBoxSelect/examples3.html),
* which in turn was inspired by the [BoxSelect component for ExtJS 2](http://efattal.fr/en/extjs/extuxboxselect/).
*
* Various contributions and suggestions made by many members of the ExtJS community which can be seen
* in the [official user extension forum post](http://www.sencha.com/forum/showthread.php?134751-Ext.ux.form.field.BoxSelect).
*
* Many thanks go out to all of those who have contributed, this extension would not be
* possible without your help.
*
* See [AUTHORS.txt](../AUTHORS.TXT) for a list of major contributors
*
* @author kvee_iv http://www.sencha.com/forum/member.php?29437-kveeiv
* @version 2.0.3
* @requires BoxSelect.css
* @xtype boxselect
*
*/
Ext.define('Ext.ux.form.field.BoxSelect', {
    extend: 'Ext.form.field.ComboBox',
    alias: ['widget.comboboxselect', 'widget.boxselect'],
    requires: ['Ext.selection.Model', 'Ext.data.Store'],

    //
    // Begin configuration options related to selected values
    //

    /**
    * @cfg {Boolean}
    * If set to `true`, allows the combo field to hold more than one value at a time, and allows selecting multiple
    * items from the dropdown list. The combo's text field will show all selected values using the template
    * defined by {@link #labelTpl}.
    *

    */
    multiSelect: true,

    /**
    * @cfg {String/Ext.XTemplate} labelTpl
    * The [XTemplate](http://docs.sencha.com/ext-js/4-1/#!/api/Ext.XTemplate) to use for the inner
    * markup of the labelled items. Defaults to the configured {@link #displayField}
    */

    /**
    * @cfg
    * @inheritdoc
    *
    * When {@link #forceSelection} is `false`, new records can be created by the user as they
    * are typed. These records are **not** added to the combo's store. This creation
    * is triggered by typing the configured 'delimiter', and can be further configured using the
    * {@link #createNewOnEnter} and {@link #createNewOnBlur} configuration options.
    *
    * This functionality is primarily useful with BoxSelect components for things
    * such as an email address.
    */
    forceSelection: true,

    /**
    * @cfg {Boolean}
    * Has no effect if {@link #forceSelection} is `true`.
    *
    * With {@link #createNewOnEnter} set to `true`, the creation described in
    * {@link #forceSelection} will also be triggered by the 'enter' key.
    */
    createNewOnEnter: false,

    createNewOnSpace: false,
    createNewOnComma: false,


    /**
    * @cfg {Boolean}
    * Has no effect if {@link #forceSelection} is `true`.
    *
    * With {@link #createNewOnBlur} set to `true`, the creation described in
    * {@link #forceSelection} will also be triggered when the field loses focus.
    *
    * Please note that this behavior is also affected by the configuration options
    * {@link #autoSelect} and {@link #selectOnTab}. If those are true and an existing
    * item would have been selected as a result, the partial text the user has entered will
    * be discarded and the existing item will be added to the selection.
    */
    createNewOnBlur: false,

    /**
    * @cfg {Boolean}
    * Has no effect if {@link #multiSelect} is `false`.
    *
    * Controls the formatting of the form submit value of the field as returned by {@link #getSubmitValue}
    *
    * - `true` for the field value to submit as a json encoded array in a single GET/POST variable
    * - `false` for the field to submit as an array of GET/POST variables
    */
    encodeSubmitValue: false,

    //
    // End of configuration options related to selected values
    //



    //
    // Configuration options related to pick list behavior
    //

    /**
    * @cfg {Boolean}
    * `true` to activate the trigger when clicking in empty space in the field. Note that the
    * subsequent behavior of this is controlled by the field's {@link #triggerAction}.
    * This behavior is similar to that of a basic ComboBox with {@link #editable} `false`.
    */
    triggerOnClick: true,

    /**
    * @cfg {Boolean}
    * - `true` to have each selected value fill to the width of the form field
    * - `false to have each selected value size to its displayed contents
    */
    stacked: false,

    /**
    * @cfg {Boolean}
    * Has no effect if {@link #multiSelect} is `false`
    *
    * `true` to keep the pick list expanded after each selection from the pick list
    * `false` to automatically collapse the pick list after a selection is made
    */
    pinList: true,

    /**
    * @cfg {Boolean}
    * True to hide the currently selected values from the drop down list. These items are hidden via
    * css to maintain simplicity in store and filter management.
    *
    * - `true` to hide currently selected values from the drop down pick list
    * - `false` to keep the item in the pick list as a selected item
    */
    filterPickList: false,

    //
    // End of configuration options related to pick list behavior
    //



    //
    // Configuration options related to text field behavior
    //

    /**
    * @cfg
    * @inheritdoc
    */
    selectOnFocus: true,

    /**
    * @cfg {Boolean}
    *
    * `true` if this field should automatically grow and shrink vertically to its content.
    * Note that this overrides the natural trigger grow functionality, which is used to size
    * the field horizontally.
    */
    grow: true,

    /**
    * @cfg {Number/Boolean}
    * Has no effect if {@link #grow} is `false`
    *
    * The minimum height to allow when {@link #grow} is `true`, or `false` to allow for
    * natural vertical growth based on the current selected values. See also {@link #growMax}.
    */
    growMin: false,

    /**
    * @cfg {Number/Boolean}
    * Has no effect if {@link #grow} is `false`
    *
    * The maximum height to allow when {@link #grow} is `true`, or `false` to allow for
    * natural vertical growth based on the current selected values. See also {@link #growMin}.
    */
    growMax: false,

    /**
    * @cfg growAppend
    * @hide
    * Currently unsupported by BoxSelect since this is used for horizontal growth and
    * BoxSelect only supports vertical growth.
    */
    /**
    * @cfg growToLongestValue
    * @hide
    * Currently unsupported by BoxSelect since this is used for horizontal growth and
    * BoxSelect only supports vertical growth.
    */

    //
    // End of configuration options related to text field behavior
    //


    //
    // Event signatures
    //

    /**
    * @event autosize
    * Fires when the **{@link #autoSize}** function is triggered and the field is resized according to the
    * {@link #grow}/{@link #growMin}/{@link #growMax} configs as a result. This event provides a hook for the
    * developer to apply additional logic at runtime to resize the field if needed.
    * @param {Ext.ux.form.field.BoxSelect} this This BoxSelect field
    * @param {Number} height The new field height
    */

    //
    // End of event signatures
    //



    //
    // Configuration options that will break things if messed with
    //

    /**
    * @private
    */
    fieldSubTpl: [
        '<div id="{cmpId}-listWrapper" class="x-boxselect {fieldCls} {typeCls}">',
        '<ul id="{cmpId}-itemList" class="x-boxselect-list">',
        '<li id="{cmpId}-inputElCt" class="x-boxselect-input">',
        '<input id="{cmpId}-inputEl" type="{type}" ',
        '<tpl if="name">name="{name}" </tpl>',
        '<tpl if="value"> value="{[Ext.util.Format.htmlEncode(values.value)]}"</tpl>',
        '<tpl if="size">size="{size}" </tpl>',
        '<tpl if="tabIdx">tabIndex="{tabIdx}" </tpl>',
        '<tpl if="disabled"> disabled="disabled"</tpl>',
        'class="x-boxselect-input-field {inputElCls}" autocomplete="off">',
        '</li>',
        '</ul>',
        '</div>',
        {
            compiled: true,
            disableFormats: true
        }
    ],

    /**
    * @private
    */
    childEls: ['listWrapper', 'itemList', 'inputEl', 'inputElCt'],

    /**
    * @private
    */
    componentLayout: 'boxselectfield',

    /**
    * @inheritdoc
    *
    * Initialize additional settings and enable simultaneous typeAhead and multiSelect support
    * @protected
    */
    initComponent: function () {
        var me = this,
        typeAhead = me.typeAhead;

        if (typeAhead && !me.editable) {
            Ext.Error.raise('If typeAhead is enabled the combo must be editable: true -- please change one of those settings.');
        }

        Ext.apply(me, {
            typeAhead: false
        });

        me.callParent();

        me.typeAhead = typeAhead;

        me.selectionModel = new Ext.selection.Model({
            store: me.valueStore,
            mode: 'MULTI',
            lastFocused: null,
            onSelectChange: function (record, isSelected, suppressEvent, commitFn) {
                commitFn();
            }
        });

        if (!Ext.isEmpty(me.delimiter) && me.multiSelect) {
            me.delimiterRegexp = new RegExp(String(me.delimiter).replace(/[$%()*+.?\[\\\]{|}]/g, "\\$&"));
        }
    },

    /**
    * Register events for management controls of labelled items
    * @protected
    */
    initEvents: function () {
        var me = this;

        me.callParent(arguments);

        if (!me.enableKeyEvents) {
            me.mon(me.inputEl, 'keydown', me.onKeyDown, me);
        }
        me.mon(me.inputEl, 'paste', me.onPaste, me);
        me.mon(me.listWrapper, 'click', me.onItemListClick, me);

        // I would prefer to use relayEvents here to forward these events on, but I want
        // to pass the field instead of exposing the underlying selection model
        me.mon(me.selectionModel, {
            'selectionchange': function (selModel, selectedRecs) {
                me.applyMultiselectItemMarkup();
                me.fireEvent('valueselectionchange', me, selectedRecs);
            },
            'focuschange': function (selectionModel, oldFocused, newFocused) {
                me.fireEvent('valuefocuschange', me, oldFocused, newFocused);
            },
            scope: me
        });
    },

    /**
    * @inheritdoc
    *
    * Create a store for the records of our current value based on the main store's model
    * @protected
    */
    onBindStore: function (store, initial) {
        var me = this;

        if (store) {
            me.valueStore = new Ext.data.Store({
                model: store.model,
                proxy: {
                    type: 'memory'
                }
            });
            me.mon(me.valueStore, 'datachanged', me.applyMultiselectItemMarkup, me);
            if (me.selectionModel) {
                me.selectionModel.bindStore(me.valueStore);
            }
        }
    },

    /**
    * @inheritdoc
    *
    * Remove the selected value store and associated listeners
    * @protected
    */
    onUnbindStore: function (store) {
        var me = this,
        valueStore = me.valueStore;

        if (valueStore) {
            if (me.selectionModel) {
                me.selectionModel.setLastFocused(null);
                me.selectionModel.deselectAll();
                me.selectionModel.bindStore(null);
            }
            me.mun(valueStore, 'datachanged', me.applyMultiselectItemMarkup, me);
            valueStore.destroy();
            me.valueStore = null;
        }

        me.callParent(arguments);
    },

    /**
    * @inheritdoc
    *
    * Add refresh tracking to the picker for selection management
    * @protected
    */
    createPicker: function () {
        var me = this,
        picker = me.callParent(arguments);

        me.mon(picker, {
            'beforerefresh': me.onBeforeListRefresh,
            scope: me
        });

        if (me.filterPickList) {
            picker.addCls('x-boxselect-hideselections');
        }

        return picker;
    },

    /**
    * @inheritdoc
    *
    * Clean up selected values management controls
    * @protected
    */
    onDestroy: function () {
        var me = this;

        Ext.destroyMembers(me, 'valueStore', 'selectionModel');

        me.callParent(arguments);
    },

    /**
    * Add empty text support to initial render.
    * @protected
    */
    getSubTplData: function () {
        var me = this,
            data = me.callParent(),
            isEmpty = me.emptyText && data.value.length < 1;

        if (isEmpty) {
            data.value = me.emptyText;
        } else {
            data.value = '';
        }
        data.inputElCls = data.fieldCls.match(me.emptyCls) ? me.emptyCls : '';

        return data;
    },

    /**
    * @inheritdoc
    *
    * Overridden to avoid use of placeholder, as our main input field is often empty
    * @protected
    */
    afterRender: function () {
        var me = this;

        if (Ext.supports.Placeholder && me.inputEl && me.emptyText) {
            delete me.inputEl.dom.placeholder;
        }

        me.bodyEl.applyStyles('vertical-align:top');

        if (me.grow) {
            if (Ext.isNumber(me.growMin) && (me.growMin > 0)) {
                me.listWrapper.applyStyles('min-height:' + me.growMin + 'px');
            }
            if (Ext.isNumber(me.growMax) && (me.growMax > 0)) {
                me.listWrapper.applyStyles('max-height:' + me.growMax + 'px');
            }
        }

        if (me.stacked === true) {
            me.itemList.addCls('x-boxselect-stacked');
        }

        if (!me.multiSelect) {
            me.itemList.addCls('x-boxselect-singleselect');
        }

        me.applyMultiselectItemMarkup();

        me.callParent(arguments);
    },

    /**
    * Overridden to search entire unfiltered store since already selected values
    * can span across multiple store page loads and other filtering. Overlaps
    * some with {@link #isFilteredRecord}, but findRecord is used by the base component
    * for various logic so this logic is applied here as well.
    * @protected
    */
    findRecord: function (field, value) {
        var ds = this.store,
        matches;

        if (!ds) {
            return false;
        }

        matches = ds.queryBy(function (rec, id) {
            return rec.isEqual(rec.get(field), value);
        });

        return (matches.getCount() > 0) ? matches.first() : false;
    },

    /**
    * Overridden to map previously selected records to the "new" versions of the records
    * based on value field, if they are part of the new store load
    * @protected
    */
    onLoad: function () {
        var me = this,
        valueField = me.valueField,
        valueStore = me.valueStore,
        changed = false;

        if (valueStore) {
            if (!Ext.isEmpty(me.value) && (valueStore.getCount() == 0)) {
                me.setValue(me.value, false, true);
            }

            valueStore.suspendEvents();
            valueStore.each(function (rec) {
                var r = me.findRecord(valueField, rec.get(valueField)),
                i = r ? valueStore.indexOf(rec) : -1;
                if (i >= 0) {
                    valueStore.removeAt(i);
                    valueStore.insert(i, r);
                    changed = true;
                }
            });
            valueStore.resumeEvents();
            if (changed) {
                valueStore.fireEvent('datachanged', valueStore);
            }
        }

        me.callParent(arguments);
    },

    /**
    * Used to determine if a record is filtered out of the current store's data set,
    * for determining if a currently selected value should be retained.
    *
    * Slightly complicated logic. A record is considered filtered and should be retained if:
    *
    * - It is not in the combo store and the store has no filter or it is in the filtered data set
    *   (Happens when our selected value is just part of a different load, page or query)
    * - It is not in the combo store and forceSelection is false and it is in the value store
    *   (Happens when our selected value was created manually)
    *
    * @private
    */
    isFilteredRecord: function (record) {
        var me = this,
        store = me.store,
        valueField = me.valueField,
        storeRecord,
        filtered = false;

        storeRecord = store.findExact(valueField, record.get(valueField));

        filtered = ((storeRecord === -1) && (!store.snapshot || (me.findRecord(valueField, record.get(valueField)) !== false)));

        filtered = filtered || (!filtered && (storeRecord === -1) && (me.forceSelection !== true) &&
            (me.valueStore.findExact(valueField, record.get(valueField)) >= 0));

        return filtered;
    },

    /**
    * @inheritdoc
    *
    * Overridden to allow for continued querying with multiSelect selections already made
    * @protected
    */
    doRawQuery: function () {
        var me = this,
        rawValue = me.inputEl.dom.value;

        if (me.multiSelect) {
            rawValue = rawValue.split(me.delimiter).pop();
        }

        this.doQuery(rawValue, false, true);
    },

    /**
    * When the picker is refreshing, we should ignore selection changes. Otherwise
    * the value of our field will be changing just because our view of the choices is.
    * @protected
    */
    onBeforeListRefresh: function () {
        this.ignoreSelection++;
    },

    /**
    * When the picker is refreshing, we should ignore selection changes. Otherwise
    * the value of our field will be changing just because our view of the choices is.
    * @protected
    */
    onListRefresh: function () {
        this.callParent(arguments);
        if (this.ignoreSelection > 0) {
            --this.ignoreSelection;
        }
    },

    /**
    * Overridden to preserve current labelled items when list is filtered/paged/loaded
    * and does not include our current value. See {@link #isFilteredRecord}
    * @private
    */
    onListSelectionChange: function (list, selectedRecords) {
        var me = this,
        valueStore = me.valueStore,
        mergedRecords = [],
        i;

        // Only react to selection if it is not called from setValue, and if our list is
        // expanded (ignores changes to the selection model triggered elsewhere)
        if ((me.ignoreSelection <= 0) && me.isExpanded) {
            // Pull forward records that were already selected or are now filtered out of the store
            valueStore.each(function (rec) {
                if (Ext.Array.contains(selectedRecords, rec) || me.isFilteredRecord(rec)) {
                    mergedRecords.push(rec);
                }
            });
            mergedRecords = Ext.Array.merge(mergedRecords, selectedRecords);

            i = Ext.Array.intersect(mergedRecords, valueStore.getRange()).length;
            if ((i != mergedRecords.length) || (i != me.valueStore.getCount())) {
                me.setValue(mergedRecords, false);
                if (!me.multiSelect || !me.pinList) {
                    Ext.defer(me.collapse, 1, me);
                }
                if (valueStore.getCount() > 0) {
                    me.fireEvent('select', me, valueStore.getRange());
                }
            }
            me.inputEl.focus();
            if (!me.pinList) {
                me.inputEl.dom.value = '';
            }
            if (me.selectOnFocus) {
                me.inputEl.dom.select();
            }
        }
    },

    /**
    * Overridden to use valueStore instead of valueModels, for inclusion of
    * filtered records. See {@link #isFilteredRecord}
    * @private
    */
    syncSelection: function () {
        var me = this,
        picker = me.picker,
        valueField = me.valueField,
        pickStore, selection, selModel;

        if (picker) {
            pickStore = picker.store;

            // From the value, find the Models that are in the store's current data
            selection = [];
            if (me.valueStore) {
                me.valueStore.each(function (rec) {
                    var i = pickStore.findExact(valueField, rec.get(valueField));
                    if (i >= 0) {
                        selection.push(pickStore.getAt(i));
                    }
                });
            }

            // Update the selection to match
            me.ignoreSelection++;
            selModel = picker.getSelectionModel();
            selModel.deselectAll();
            if (selection.length > 0) {
                selModel.select(selection);
            }
            if (me.ignoreSelection > 0) {
                --me.ignoreSelection;
            }
        }
    },

    /**
    * Overridden to align to itemList size instead of inputEl
    */
    doAlign: function () {
        var me = this,
            picker = me.picker,
            aboveSfx = '-above',
            isAbove;

        me.picker.alignTo(me.listWrapper, me.pickerAlign, me.pickerOffset);
        // add the {openCls}-above class if the picker was aligned above
        // the field due to hitting the bottom of the viewport
        isAbove = picker.el.getY() < me.inputEl.getY();
        me.bodyEl[isAbove ? 'addCls' : 'removeCls'](me.openCls + aboveSfx);
        picker[isAbove ? 'addCls' : 'removeCls'](picker.baseCls + aboveSfx);
    },

    /**
    * Overridden to preserve scroll position of pick list when list is realigned
    */
    alignPicker: function () {
        var me = this,
            picker = me.picker,
            pickerScrollPos = picker.getTargetEl().dom.scrollTop;

        me.callParent(arguments);

        if (me.isExpanded) {
            if (me.matchFieldWidth) {
                // Auto the height (it will be constrained by min and max width) unless there are no records to display.
                picker.setWidth(me.listWrapper.getWidth());
            }

            picker.getTargetEl().dom.scrollTop = pickerScrollPos;
        }
    },

    /**
    * Get the current cursor position in the input field, for key-based navigation
    * @private
    */
    getCursorPosition: function () {
        var cursorPos;
        if (Ext.isIE) {
            cursorPos = document.selection.createRange();
            cursorPos.collapse(true);
            cursorPos.moveStart("character", -this.inputEl.dom.value.length);
            cursorPos = cursorPos.text.length;
        } else {
            cursorPos = this.inputEl.dom.selectionStart;
        }
        return cursorPos;
    },

    /**
    * Check to see if the input field has selected text, for key-based navigation
    * @private
    */
    hasSelectedText: function () {
        var sel, range;
        if (Ext.isIE) {
            sel = document.selection;
            range = sel.createRange();
            return (range.parentElement() == this.inputEl.dom);
        } else {
            return this.inputEl.dom.selectionStart != this.inputEl.dom.selectionEnd;
        }
    },

    /**
    * Handles keyDown processing of key-based selection of labelled items.
    * Supported keyboard controls:
    *
    * - If pick list is expanded
    *
    *     - `CTRL-A` will select all the items in the pick list
    *
    * - If the cursor is at the beginning of the input field and there are values present
    *
    *     - `CTRL-A` will highlight all the currently selected values
    *     - `BACKSPACE` and `DELETE` will remove any currently highlighted selected values
    *     - `RIGHT` and `LEFT` will move the current highlight in the appropriate direction
    *     - `SHIFT-RIGHT` and `SHIFT-LEFT` will add to the current highlight in the appropriate direction
    *
    * @protected
    */
    onKeyDown: function (e, t) {
        var me = this,
        key = e.getKey(),
        rawValue = me.inputEl.dom.value,
        valueStore = me.valueStore,
        selModel = me.selectionModel,
        stopEvent = false;

        if (me.readOnly || me.disabled || !me.editable) {
            return;
        }

        if (me.isExpanded && (key == e.A && e.ctrlKey)) {
            // CTRL-A when picker is expanded - add all items in current picker store page to current value
            me.select(me.getStore().getRange());
            selModel.setLastFocused(null);
            selModel.deselectAll();
            me.collapse();
            me.inputEl.focus();
            stopEvent = true;
        } else if ((valueStore.getCount() > 0) &&
                ((rawValue == '') || ((me.getCursorPosition() === 0) && !me.hasSelectedText()))) {
            // Keyboard navigation of current values
            var lastSelectionIndex = (selModel.getCount() > 0) ? valueStore.indexOf(selModel.getLastSelected() || selModel.getLastFocused()) : -1;

            if ((key == e.BACKSPACE) || (key == e.DELETE)) {
                if (lastSelectionIndex > -1) {
                    if (selModel.getCount() > 1) {
                        lastSelectionIndex = -1;
                    }
                    me.valueStore.remove(selModel.getSelection());
                } else {
                    me.valueStore.remove(me.valueStore.last());
                }
                selModel.clearSelections();
                me.setValue(me.valueStore.getRange());
                if (lastSelectionIndex > 0) {
                    selModel.select(lastSelectionIndex - 1);
                }
                stopEvent = true;
            } else if ((key == e.RIGHT) || (key == e.LEFT)) {
                if ((lastSelectionIndex == -1) && (key == e.LEFT)) {
                    selModel.select(valueStore.last());
                    stopEvent = true;
                } else if (lastSelectionIndex > -1) {
                    if (key == e.RIGHT) {
                        if (lastSelectionIndex < (valueStore.getCount() - 1)) {
                            selModel.select(lastSelectionIndex + 1, e.shiftKey);
                            stopEvent = true;
                        } else if (!e.shiftKey) {
                            selModel.setLastFocused(null);
                            selModel.deselectAll();
                            stopEvent = true;
                        }
                    } else if ((key == e.LEFT) && (lastSelectionIndex > 0)) {
                        selModel.select(lastSelectionIndex - 1, e.shiftKey);
                        stopEvent = true;
                    }
                }
            } else if (key == e.A && e.ctrlKey) {
                selModel.selectAll();
                stopEvent = e.A;
            }
            me.inputEl.focus();
        }

        if (stopEvent) {
            me.preventKeyUpEvent = stopEvent;
            e.stopEvent();
            return;
        }

        // Prevent key up processing for enter if it is being handled by the picker
        if (me.isExpanded && (key == e.ENTER) && me.picker.highlightedItem) {
            me.preventKeyUpEvent = true;
        }

        if (me.enableKeyEvents) {
            me.callParent(arguments);
        }

        if (!e.isSpecialKey() && !e.hasModifier()) {
            me.selectionModel.setLastFocused(null);
            me.selectionModel.deselectAll();
            me.inputEl.focus();
        }
    },

    /**
    * Handles auto-selection and creation of labelled items based on this field's
    * delimiter, as well as the keyUp processing of key-based selection of labelled items.
    * @protected
    */
    onKeyUp: function (e, t) {
        var me = this,
        rawValue = me.inputEl.dom.value;

        if (me.preventKeyUpEvent) {
            e.stopEvent();
            if ((me.preventKeyUpEvent === true) || (e.getKey() === me.preventKeyUpEvent)) {
                delete me.preventKeyUpEvent;
            }
            return;
        }

        if (me.multiSelect && (me.delimiterRegexp && me.delimiterRegexp.test(rawValue)) ||
                ((me.createNewOnEnter === true) && e.getKey() == e.ENTER) ||
                ((me.createNewOnSpace === true) && e.getKey() == e.SPACE) ||
                ((me.createNewOnComma === true) && e.getKey() == 188)
                ) {

            if (me.createNewOnComma) {
                rawValue = rawValue.replace(',', '');
            }

            if (me.createNewOnSpace === true) {
                if (rawValue == ' ') {
                    me.inputEl.dom.value = '';
                    return;
                }
                rawValue = Ext.String.trim(rawValue);
            }

            rawValue = Ext.Array.clean(rawValue.split(me.delimiterRegexp));

            me.inputEl.dom.value = '';
            me.setValue(me.valueStore.getRange().concat(rawValue));
            me.inputEl.focus();
        }

        me.callParent([e, t]);
    },

    /**
    * Handles auto-selection of labelled items based on this field's delimiter when pasting
    * a list of values in to the field (e.g., for email addresses)
    * @protected
    */
    onPaste: function (e, t) {
        var me = this,
            rawValue = me.inputEl.dom.value,
            clipboard = (e && e.browserEvent && e.browserEvent.clipboardData) ? e.browserEvent.clipboardData : false;

        if (me.multiSelect && (me.delimiterRegexp && me.delimiterRegexp.test(rawValue))) {
            if (clipboard && clipboard.getData) {
                if (/text\/plain/.test(clipboard.types)) {
                    rawValue = clipboard.getData('text/plain');
                } else if (/text\/html/.test(clipboard.types)) {
                    rawValue = clipboard.getData('text/html');
                }
            }

            rawValue = Ext.Array.clean(rawValue.split(me.delimiterRegexp));
            me.inputEl.dom.value = '';
            me.setValue(me.valueStore.getRange().concat(rawValue));
            me.inputEl.focus();
        }
    },

    /**
    * Overridden to handle key navigation of pick list when list is filtered. Because we
    * want to avoid complexity that could be introduced by modifying the store's contents,
    * (e.g., always having to search back through and remove values when they might
    * be re-sent by the server, adding the values back in their previous position when
    * they are removed from the current selection, etc.), we handle this filtering
    * via a simple css rule. However, for the moment since those DOM nodes still exist
    * in the list we have to hijack the highlighting methods for the picker's BoundListKeyNav
    * to appropriately skip over these hidden nodes. This is a less than ideal solution,
    * but it centralizes all of the complexity of this problem in to this one method.
    * @protected
    */
    onExpand: function () {
        var me = this,
            keyNav = me.listKeyNav;

        me.callParent(arguments);

        if (keyNav || !me.filterPickList) {
            return;
        }
        keyNav = me.listKeyNav;
        keyNav.highlightAt = function (index) {
            var boundList = this.boundList,
                item = boundList.all.item(index),
                len = boundList.all.getCount(),
                direction;

            if (item && item.hasCls('x-boundlist-selected')) {
                if ((index == 0) || !boundList.highlightedItem || (boundList.indexOf(boundList.highlightedItem) < index)) {
                    direction = 1;
                } else {
                    direction = -1;
                }
                do {
                    index = index + direction;
                    item = boundList.all.item(index);
                } while ((index > 0) && (index < len) && item.hasCls('x-boundlist-selected'));

                if (item.hasCls('x-boundlist-selected')) {
                    return;
                }
            }

            if (item) {
                item = item.dom;
                boundList.highlightItem(item);
                boundList.getTargetEl().scrollChildIntoView(item, false);
            }
        };
    },

    /**
    * Overridden to get and set the DOM value directly for type-ahead suggestion (bypassing get/setRawValue)
    * @protected
    */
    onTypeAhead: function () {
        var me = this,
        displayField = me.displayField,
        inputElDom = me.inputEl.dom,
        valueStore = me.valueStore,
        boundList = me.getPicker(),
        record, newValue, len, selStart;

        if (me.filterPickList) {
            var fn = this.createFilterFn(displayField, inputElDom.value);
            record = me.store.findBy(function (rec) {
                return ((valueStore.indexOfId(rec.getId()) === -1) && fn(rec));
            });
            record = (record === -1) ? false : me.store.getAt(record);
        } else {
            record = me.store.findRecord(displayField, inputElDom.value);
        }

        if (record) {
            newValue = record.get(displayField);
            len = newValue.length;
            selStart = inputElDom.value.length;
            boundList.highlightItem(boundList.getNode(record));
            if (selStart !== 0 && selStart !== len) {
                inputElDom.value = newValue;
                me.selectText(selStart, newValue.length);
            }
        }
    },

    /**
    * Delegation control for selecting and removing labelled items or triggering list collapse/expansion
    * @protected
    */
    onItemListClick: function (evt, el, o) {
        var me = this,
        itemEl = evt.getTarget('.x-boxselect-item'),
        closeEl = itemEl ? evt.getTarget('.x-boxselect-item-close') : false;

        if (me.readOnly || me.disabled) {
            return;
        }

        evt.stopPropagation();

        if (itemEl) {
            if (closeEl) {
                me.removeByListItemNode(itemEl);
                if (me.valueStore.getCount() > 0) {
                    me.fireEvent('select', me, me.valueStore.getRange());
                }
            } else {
                me.toggleSelectionByListItemNode(itemEl, evt.shiftKey);
            }
            me.inputEl.focus();
        } else {
            if (me.selectionModel.getCount() > 0) {
                me.selectionModel.setLastFocused(null);
                me.selectionModel.deselectAll();
            }
            if (me.triggerOnClick) {
                me.onTriggerClick();
            }
        }
    },

    /**
    * Build the markup for the labelled items. Template must be built on demand due to ComboBox initComponent
    * lifecycle for the creation of on-demand stores (to account for automatic valueField/displayField setting)
    * @private
    */
    getMultiSelectItemMarkup: function () {
        var me = this;

        if (!me.multiSelectItemTpl) {
            if (!me.labelTpl) {
                me.labelTpl = Ext.create('Ext.XTemplate',
                    '{[values.' + me.displayField + ']}'
                );
            } else if (Ext.isString(me.labelTpl) || Ext.isArray(me.labelTpl)) {
                me.labelTpl = Ext.create('Ext.XTemplate', me.labelTpl);
            }

            me.multiSelectItemTpl = [
            '<tpl for=".">',
            '<li class="x-boxselect-item ',
            '<tpl if="this.isSelected(values.' + me.valueField + ')">',
            ' selected',
            '</tpl>',
            '" qtip="{[typeof values === "string" ? values : values.' + me.displayField + ']}">',
            '<div class="x-boxselect-item-text">{[typeof values === "string" ? values : this.getItemLabel(values)]}</div>',
            '<div class="x-tab-close-btn x-boxselect-item-close"></div>',
            '</li>',
            '</tpl>',
            {
                compile: true,
                disableFormats: true,
                isSelected: function (value) {
                    var i = me.valueStore.findExact(me.valueField, value);
                    if (i >= 0) {
                        return me.selectionModel.isSelected(me.valueStore.getAt(i));
                    }
                    return false;
                },
                getItemLabel: function (values) {
                    return me.getTpl('labelTpl').apply(values);
                }
            }
            ];
        }

        return this.getTpl('multiSelectItemTpl').apply(Ext.Array.pluck(this.valueStore.getRange(), 'data'));
    },

    /**
    * Update the labelled items rendering
    * @private
    */
    applyMultiselectItemMarkup: function () {
        var me = this,
        itemList = me.itemList,
        item;

        if (itemList) {
            while ((item = me.inputElCt.prev()) != null) {
                item.remove();
            }
            me.inputElCt.insertHtml('beforeBegin', me.getMultiSelectItemMarkup());
        }

        Ext.Function.defer(function () {
            if (me.picker && me.isExpanded) {
                me.alignPicker();
            }
            if (me.hasFocus) {
                me.inputElCt.scrollIntoView(me.listWrapper);
            }
        }, 15);
    },

    /**
    * Returns the record from valueStore for the labelled item node
    */
    getRecordByListItemNode: function (itemEl) {
        var me = this,
        itemIdx = 0,
        searchEl = me.itemList.dom.firstChild;

        while (searchEl && searchEl.nextSibling) {
            if (searchEl == itemEl) {
                break;
            }
            itemIdx++;
            searchEl = searchEl.nextSibling;
        }
        itemIdx = (searchEl == itemEl) ? itemIdx : false;

        if (itemIdx === false) {
            return false;
        }

        return me.valueStore.getAt(itemIdx);
    },

    /**
    * Toggle of labelled item selection by node reference
    */
    toggleSelectionByListItemNode: function (itemEl, keepExisting) {
        var me = this,
        rec = me.getRecordByListItemNode(itemEl),
        selModel = me.selectionModel;

        if (rec) {
            if (selModel.isSelected(rec)) {
                if (selModel.isFocused(rec)) {
                    selModel.setLastFocused(null);
                }
                selModel.deselect(rec);
            } else {
                selModel.select(rec, keepExisting);
            }
        }
    },

    /**
    * Removal of labelled item by node reference
    */
    removeByListItemNode: function (itemEl) {
        var me = this,
        rec = me.getRecordByListItemNode(itemEl);

        if (rec) {
            me.valueStore.remove(rec);
            me.setValue(me.valueStore.getRange());
        }
    },

    /**
    * @inheritdoc
    * Intercept calls to getRawValue to pretend there is no inputEl for rawValue handling,
    * so that we can use inputEl for user input of just the current value.
    */
    getRawValue: function () {
        var me = this,
        inputEl = me.inputEl,
        result;
        me.inputEl = false;
        result = me.callParent(arguments);
        me.inputEl = inputEl;
        return result;
    },

    /**
    * @inheritdoc
    * Intercept calls to setRawValue to pretend there is no inputEl for rawValue handling,
    * so that we can use inputEl for user input of just the current value.
    */
    setRawValue: function (value) {
        var me = this,
        inputEl = me.inputEl,
        result;

        me.inputEl = false;
        result = me.callParent([value]);
        me.inputEl = inputEl;

        return result;
    },

    /**
    * Adds a value or values to the current value of the field
    * @param {Mixed} value The value or values to add to the current value, see {@link #setValue}
    */
    addValue: function (value) {
        var me = this;
        if (value) {
            me.setValue(Ext.Array.merge(me.value, Ext.Array.from(value)));
        }
    },

    /**
    * Removes a value or values from the current value of the field
    * @param {Mixed} value The value or values to remove from the current value, see {@link #setValue}
    */
    removeValue: function (value) {
        var me = this;

        if (value) {
            me.setValue(Ext.Array.difference(me.value, Ext.Array.from(value)));
        }
    },

    /**
    * Sets the specified value(s) into the field. The following value formats are recognised:
    *
    * - Single Values
    *
    *     - A string associated to this field's configured {@link #valueField}
    *     - A record containing at least this field's configured {@link #valueField} and {@link #displayField}
    *
    * - Multiple Values
    *
    *     - If {@link #multiSelect} is `true`, a string containing multiple strings as
    *       specified in the Single Values section above, concatenated in to one string
    *       with each entry separated by this field's configured {@link #delimiter}
    *     - An array of strings as specified in the Single Values section above
    *     - An array of records as specified in the Single Values section above
    *
    * In any of the string formats above, the following occurs if an associated record cannot be found:
    *
    * 1. If {@link #forceSelection} is `false`, a new record of the {@link #store}'s configured model type
    *    will be created using the given value as the {@link #displayField} and {@link #valueField}.
    *    This record will be added to the current value, but it will **not** be added to the store.
    * 2. If {@link #forceSelection} is `true` and {@link #queryMode} is `remote`, the list of unknown
    *    values will be submitted as a call to the {@link #store}'s load as a parameter named by
    *    the {@link #valueField} with values separated by the configured {@link #delimiter}.
    *    ** This process will cause setValue to asynchronously process. ** This will only be attempted
    *    once. Any unknown values that the server does not return records for will be removed.
    * 3. Otherwise, unknown values will be removed.
    *
    * @param {Mixed} value The value(s) to be set, see method documentation for details
    * @return {Ext.form.field.Field/Boolean} this, or `false` if asynchronously querying for unknown values
    */
    setValue: function (value, doSelect, skipLoad) {
        var me = this,
        valueStore = me.valueStore,
        valueField = me.valueField,
        record, len, i, valueRecord, h,
        unknownValues = [];

        if (Ext.isEmpty(value)) {
            value = null;
        }
        if (Ext.isString(value) && me.multiSelect) {
            value = value.split(me.delimiter);
        }
        value = Ext.Array.from(value, true);

        for (i = 0, len = value.length; i < len; i++) {
            record = value[i];
            if (!record || !record.isModel) {
                valueRecord = valueStore.findExact(valueField, record);
                if (valueRecord >= 0) {
                    value[i] = valueStore.getAt(valueRecord);
                } else {
                    valueRecord = me.findRecord(valueField, record);
                    if (!valueRecord) {
                        if (me.forceSelection) {
                            unknownValues.push(record);
                        } else {
                            valueRecord = {};
                            valueRecord[me.valueField] = record;
                            valueRecord[me.displayField] = record;
                            valueRecord = new me.valueStore.model(valueRecord);
                        }
                    }
                    if (valueRecord) {
                        value[i] = valueRecord;
                    }
                }
            }
        }

        if ((skipLoad !== true) && (unknownValues.length > 0) && (me.queryMode === 'remote')) {
            var params = {};
            params[me.valueField] = unknownValues.join(me.delimiter);
            me.store.load({
                params: params,
                callback: function () {
                    if (me.itemList) {
                        me.itemList.unmask();
                    }
                    me.setValue(value, doSelect, true);
                    me.autoSize();
                }
            });
            return false;
        }

        // For single-select boxes, use the last good (formal record) value if possible
        if (!me.multiSelect && (value.length > 0)) {
            for (i = value.length - 1; i >= 0; i--) {
                if (value[i].isModel) {
                    value = value[i];
                    break;
                }
            }
            if (Ext.isArray(value)) {
                value = value[value.length - 1];
            }
        }

        return me.callParent([value, doSelect]);
    },

    /**
    * Returns the records for the field's current value
    * @return {Array} The records for the field's current value
    */
    getValueRecords: function () {
        return this.valueStore.getRange();
    },

    /**
    * @inheritdoc
    * Overridden to optionally allow for submitting the field as a json encoded array.
    */
    getSubmitData: function () {
        var me = this,
        val = me.callParent(arguments);

        if (me.multiSelect && me.encodeSubmitValue && val && val[me.name]) {
            val[me.name] = Ext.encode(val[me.name]);
        }

        return val;
    },

    /**
    * Overridden to clear the input field if we are auto-setting a value as we blur.
    * @protected
    */
    mimicBlur: function () {
        var me = this;

        if (me.selectOnTab && me.picker && me.picker.highlightedItem) {
            me.inputEl.dom.value = '';
        }

        me.callParent(arguments);
    },

    /**
    * Overridden to handle partial-input selections more directly
    */
    assertValue: function () {
        var me = this,
        rawValue = me.inputEl.dom.value,
        rec = !Ext.isEmpty(rawValue) ? me.findRecordByDisplay(rawValue) : false,
        value = false;

        if (!rec && !me.forceSelection && me.createNewOnBlur && !Ext.isEmpty(rawValue)) {
            value = rawValue;
        } else if (rec) {
            value = rec;
        }

        if (value) {
            me.addValue(value);
        }

        me.inputEl.dom.value = '';

        me.collapse();
    },

    /**
    * Expand record values for evaluating change and fire change events for UI to respond to
    */
    checkChange: function () {
        if (!this.suspendCheckChange && !this.isDestroyed) {
            var me = this,
            valueStore = me.valueStore,
            lastValue = me.lastValue,
            valueField = me.valueField,
            newValue = Ext.Array.map(Ext.Array.from(me.value), function (val) {
                if (val.isModel) {
                    return val.get(valueField);
                }
                return val;
            }, this).join(this.delimiter),
            isEqual = me.isEqual(newValue, lastValue);

            if (!isEqual || ((newValue.length > 0 && valueStore.getCount() < newValue.length))) {
                valueStore.suspendEvents();
                valueStore.removeAll();
                if (Ext.isArray(me.valueModels)) {
                    valueStore.add(me.valueModels);
                }
                valueStore.resumeEvents();
                valueStore.fireEvent('datachanged', valueStore);

                if (!isEqual) {
                    me.lastValue = newValue;
                    me.fireEvent('change', me, newValue, lastValue);
                    me.onChange(newValue, lastValue);
                }
            }
        }
    },

    /**
    * Overridden to be more accepting of varied value types
    */
    isEqual: function (v1, v2) {
        var fromArray = Ext.Array.from,
            valueField = this.valueField,
            i, len, t1, t2;

        v1 = fromArray(v1);
        v2 = fromArray(v2);
        len = v1.length;

        if (len !== v2.length) {
            return false;
        }

        for (i = 0; i < len; i++) {
            t1 = v1[i].isModel ? v1[i].get(valueField) : v1[i];
            t2 = v2[i].isModel ? v2[i].get(valueField) : v2[i];
            if (t1 !== t2) {
                return false;
            }
        }

        return true;
    },

    /**
    * Overridden to use value (selection) instead of raw value and to avoid the use of placeholder
    */
    applyEmptyText: function () {
        var me = this,
        emptyText = me.emptyText,
        inputEl, isEmpty;

        if (me.rendered && emptyText) {
            isEmpty = Ext.isEmpty(me.value) && !me.hasFocus;
            inputEl = me.inputEl;
            if (isEmpty) {
                inputEl.dom.value = emptyText;
                inputEl.addCls(me.emptyCls);
                me.listWrapper.addCls(me.emptyCls);
            } else {
                if (inputEl.dom.value === emptyText) {
                    inputEl.dom.value = '';
                }
                me.listWrapper.removeCls(me.emptyCls);
                inputEl.removeCls(me.emptyCls);
            }
            me.autoSize();
        }
    },

    /**
    * Overridden to use inputEl instead of raw value and to avoid the use of placeholder
    */
    preFocus: function () {
        var me = this,
        inputEl = me.inputEl,
        emptyText = me.emptyText,
        isEmpty;

        if (emptyText && inputEl.dom.value === emptyText) {
            inputEl.dom.value = '';
            isEmpty = true;
            inputEl.removeCls(me.emptyCls);
            me.listWrapper.removeCls(me.emptyCls);
        }
        if (me.selectOnFocus || isEmpty) {
            inputEl.dom.select();
        }
    },

    /**
    * Intercept calls to onFocus to add focusCls, because the base field
    * classes assume this should be applied to inputEl
    */
    onFocus: function () {
        var me = this,
        focusCls = me.focusCls,
        itemList = me.itemList;

        if (focusCls && itemList) {
            itemList.addCls(focusCls);
        }

        me.callParent(arguments);
    },

    /**
    * Intercept calls to onBlur to remove focusCls, because the base field
    * classes assume this should be applied to inputEl
    */
    onBlur: function () {
        var me = this,
        focusCls = me.focusCls,
        itemList = me.itemList;

        if (focusCls && itemList) {
            itemList.removeCls(focusCls);
        }

        me.callParent(arguments);
    },

    /**
    * Intercept calls to renderActiveError to add invalidCls, because the base
    * field classes assume this should be applied to inputEl
    */
    renderActiveError: function () {
        var me = this,
        invalidCls = me.invalidCls,
        itemList = me.itemList,
        hasError = me.hasActiveError();

        if (invalidCls && itemList) {
            itemList[hasError ? 'addCls' : 'removeCls'](me.invalidCls + '-field');
        }

        me.callParent(arguments);
    },

    /**
    * Initiate auto-sizing for height based on {@link #grow}, if applicable.
    */
    autoSize: function () {
        var me = this,
        height;

        if (me.grow && me.rendered) {
            me.autoSizing = true;
            me.updateLayout();
        }

        return me;
    },

    /**
    * Track height change to fire {@link #event-autosize} event, when applicable.
    */
    afterComponentLayout: function () {
        var me = this,
            width;

        if (me.autoSizing) {
            height = me.getHeight();
            if (height !== me.lastInputHeight) {
                if (me.isExpanded) {
                    me.alignPicker();
                }
                me.fireEvent('autosize', me, height);
                me.lastInputHeight = height;
                delete me.autoSizing;
            }
        }
    }
});

/**
* Ensures the input element takes up the maximum amount of remaining list width,
* or the entirety of the list width if too little space remains. In this case,
* the list height will be automatically increased to accomodate the new line. This
* growth will not occur if {@link Ext.ux.form.field.BoxSelect#multiSelect} or
* {@link Ext.ux.form.field.BoxSelect#grow} is false.
*/
Ext.define('Ext.ux.layout.component.field.BoxSelectField', {
    /* Begin Definitions */
    alias: ['layout.boxselectfield'],
    extend: 'Ext.layout.component.field.Trigger',

    /* End Definitions */

    type: 'boxselectfield',

    /*For proper calculations we need our field to be sized.*/
    waitForOuterWidthInDom: true,

    beginLayout: function (ownerContext) {
        var me = this,
            owner = me.owner;

        me.callParent(arguments);

        ownerContext.inputElCtContext = ownerContext.getEl('inputElCt');
        owner.inputElCt.setStyle('width', '');

        me.skipInputGrowth = !owner.grow || !owner.multiSelect;
    },

    beginLayoutFixed: function (ownerContext, width, suffix) {
        var me = this,
            owner = ownerContext.target;

        owner.triggerEl.setStyle('height', '24px');

        me.callParent(arguments);

        if (ownerContext.heightModel.fixed && ownerContext.lastBox) {
            owner.listWrapper.setStyle('height', ownerContext.lastBox.height + 'px');
            owner.itemList.setStyle('height', '100%');
        }
        /*No inputElCt calculations here!*/
    },

    /*Calculate and cache value of input container.*/
    publishInnerWidth: function (ownerContext) {
        var me = this,
            owner = me.owner,
            width = owner.itemList.getWidth(true) - 10,
            lastEntry = owner.inputElCt.prev(null, true);

        if (lastEntry && !owner.stacked) {
            lastEntry = Ext.fly(lastEntry);
            width = width - lastEntry.getOffsetsTo(lastEntry.up(''))[0] - lastEntry.getWidth();
        }

        if (!me.skipInputGrowth && (width < 35)) {
            width = width - 10;
        } else if (width < 1) {
            width = 1;
        }
        ownerContext.inputElCtContext.setWidth(width);
    }
});

/* 
*    Notification / Toastwindow extension for Ext JS 4.x
*
*    Copyright (c) 2011 Eirik Lorentsen (http://www.eirik.net/)
*
*    Examples and documentation at: http://www.eirik.net/Ext/ux/window/Notification.html
*
*    Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) 
*    and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
*
*    Version: 2.1
*    Last changed date: 2012-08-12
*/

Ext.define('Ext.ux.window.Notification', {
    extend: 'Ext.window.Window',
    alias: 'widget.uxNotification',

    cls: 'ux-notification-window',
    autoClose: true,
    autoHeight: true,
    plain: false,
    draggable: false,
    shadow: false,
    focus: Ext.emptyFn,

    // For alignment and to store array of rendered notifications. Defaults to document if not set.
    manager: null,

    useXAxis: false,

    // Options: br, bl, tr, tl, t, l, b, r
    position: 'br',

    // Pixels between each notification
    spacing: 6,

    // Pixels from the managers borders to start the first notification
    paddingX: 30,
    paddingY: 10,

    slideInAnimation: 'easeIn',
    slideBackAnimation: 'bounceOut',
    slideInDuration: 1500,
    slideBackDuration: 1000,
    hideDuration: 500,
    autoCloseDelay: 7000,
    stickOnClick: true,
    stickWhileHover: true,

    // Private. Do not override!
    isHiding: false,
    readyToHide: false,
    destroyAfterHide: false,
    closeOnMouseOut: false,

    // Caching coordinates to be able to align to final position of siblings being animated
    xPos: 0,
    yPos: 0,

    statics: {
        defaultManager: {
            el: null
        }
    },

    initComponent: function () {
        var me = this;

        // Backwards compatibility
        if (Ext.isDefined(me.corner)) {
            me.position = me.corner;
        }
        if (Ext.isDefined(me.slideDownAnimation)) {
            me.slideBackAnimation = me.slideDownAnimation;
        }
        if (Ext.isDefined(me.autoDestroyDelay)) {
            me.autoCloseDelay = me.autoDestroyDelay;
        }
        if (Ext.isDefined(me.autoHideDelay)) {
            me.autoCloseDelay = me.autoHideDelay;
        }
        if (Ext.isDefined(me.autoHide)) {
            me.autoClose = me.autoHide;
        }
        if (Ext.isDefined(me.slideInDelay)) {
            me.slideInDuration = me.slideInDelay;
        }
        if (Ext.isDefined(me.slideDownDelay)) {
            me.slideBackDuration = me.slideDownDelay;
        }
        if (Ext.isDefined(me.fadeDelay)) {
            me.hideDuration = me.fadeDelay;
        }

        // 'bc', lc', 'rc', 'tc' compatibility
        me.position = me.position.replace(/c/, '');

        me.updateAlignment(me.position);

        me.setManager(me.manager);

        me.callParent(arguments);
    },

    onRender: function () {
        var me = this;

        me.el.hover(
            function () {
                me.mouseIsOver = true;
            },
            function () {
                me.mouseIsOver = false;
                if (me.closeOnMouseOut) {
                    me.closeOnMouseOut = false;
                    me.close();
                }
            },
            me
        );

        this.callParent(arguments);

    },

    updateAlignment: function (position) {
        var me = this;

        switch (position) {
            case 'br':
                me.paddingFactorX = -1;
                me.paddingFactorY = -1;
                me.siblingAlignment = "br-br";
                if (me.useXAxis) {
                    me.managerAlignment = "bl-br";
                } else {
                    me.managerAlignment = "tr-br";
                }
                break;
            case 'bl':
                me.paddingFactorX = 1;
                me.paddingFactorY = -1;
                me.siblingAlignment = "bl-bl";
                if (me.useXAxis) {
                    me.managerAlignment = "br-bl";
                } else {
                    me.managerAlignment = "tl-bl";
                }
                break;
            case 'tr':
                me.paddingFactorX = -1;
                me.paddingFactorY = 1;
                me.siblingAlignment = "tr-tr";
                if (me.useXAxis) {
                    me.managerAlignment = "tl-tr";
                } else {
                    me.managerAlignment = "br-tr";
                }
                break;
            case 'tl':
                me.paddingFactorX = 1;
                me.paddingFactorY = 1;
                me.siblingAlignment = "tl-tl";
                if (me.useXAxis) {
                    me.managerAlignment = "tr-tl";
                } else {
                    me.managerAlignment = "bl-tl";
                }
                break;
            case 'b':
                me.paddingFactorX = 0;
                me.paddingFactorY = -1;
                me.siblingAlignment = "b-b";
                me.useXAxis = 0;
                me.managerAlignment = "t-b";
                break;
            case 't':
                me.paddingFactorX = 0;
                me.paddingFactorY = 1;
                me.siblingAlignment = "t-t";
                me.useXAxis = 0;
                me.managerAlignment = "b-t";
                break;
            case 'l':
                me.paddingFactorX = 1;
                me.paddingFactorY = 0;
                me.siblingAlignment = "l-l";
                me.useXAxis = 1;
                me.managerAlignment = "r-l";
                break;
            case 'r':
                me.paddingFactorX = -1;
                me.paddingFactorY = 0;
                me.siblingAlignment = "r-r";
                me.useXAxis = 1;
                me.managerAlignment = "l-r";
                break;
        }
    },

    getXposAlignedToManager: function () {
        var me = this;

        var xPos = 0;

        // Avoid error messages if the manager does not have a dom element
        if (me.manager && me.manager.el && me.manager.el.dom) {
            if (!me.useXAxis) {
                // Element should already be aligned verticaly
                return me.el.getLeft();
            } else {
                // Using getAnchorXY instead of getTop/getBottom should give a correct placement when document is used
                // as the manager but is still 0 px high. Before rendering the viewport.
                if (me.position == 'br' || me.position == 'tr' || me.position == 'r') {
                    xPos += me.manager.el.getAnchorXY('r')[0];
                    xPos -= (me.el.getWidth() + me.paddingX);
                } else {
                    xPos += me.manager.el.getAnchorXY('l')[0];
                    xPos += me.paddingX;
                }
            }
        }

        return xPos;
    },

    getYposAlignedToManager: function () {
        var me = this;

        var yPos = 0;

        // Avoid error messages if the manager does not have a dom element
        if (me.manager && me.manager.el && me.manager.el.dom) {
            if (me.useXAxis) {
                // Element should already be aligned horizontaly
                return me.el.getTop();
            } else {
                // Using getAnchorXY instead of getTop/getBottom should give a correct placement when document is used
                // as the manager but is still 0 px high. Before rendering the viewport.
                if (me.position == 'br' || me.position == 'bl' || me.position == 'b') {
                    yPos += me.manager.el.getAnchorXY('b')[1];
                    yPos -= (me.el.getHeight() + me.paddingY);
                } else {
                    yPos += me.manager.el.getAnchorXY('t')[1];
                    yPos += me.paddingY;
                }
            }
        }

        return yPos;
    },

    getXposAlignedToSibling: function (sibling) {
        var me = this;

        if (me.useXAxis) {
            if (me.position == 'tl' || me.position == 'bl' || me.position == 'l') {
                // Using sibling's width when adding
                return (sibling.xPos + sibling.el.getWidth() + sibling.spacing);
            } else {
                // Using own width when subtracting
                return (sibling.xPos - me.el.getWidth() - me.spacing);
            }
        } else {
            return me.el.getLeft();
        }

    },

    getYposAlignedToSibling: function (sibling) {
        var me = this;

        if (me.useXAxis) {
            return me.el.getTop();
        } else {
            if (me.position == 'tr' || me.position == 'tl' || me.position == 't') {
                // Using sibling's width when adding
                return (sibling.yPos + sibling.el.getHeight() + sibling.spacing);
            } else {
                // Using own width when subtracting
                return (sibling.yPos - me.el.getHeight() - sibling.spacing);
            }
        }
    },

    getNotifications: function (alignment) {
        var me = this;

        if (!me.manager.notifications[alignment]) {
            me.manager.notifications[alignment] = [];
        }

        return me.manager.notifications[alignment];
    },

    setManager: function (manager) {
        var me = this;

        me.manager = manager;

        if (typeof me.manager == 'string') {
            me.manager = Ext.getCmp(me.manager);
        }

        // If no manager is provided or found, then the static object is used and the el property pointed to the body document.
        if (!me.manager) {
            me.manager = me.statics().defaultManager;

            if (!me.manager.el) {
                me.manager.el = Ext.getBody();
            }
        }

        if (typeof me.manager.notifications == 'undefined') {
            me.manager.notifications = {};
        }
    },

    update: function () {
        var me = this;

        if (me.autoClose) {
            me.task && me.task.delay(me.autoCloseDelay);
        }

        this.callParent(arguments);
    },

    beforeShow: function () {
        var me = this;

        if (me.stickOnClick) {
            if (me.body && me.body.dom) {
                Ext.fly(me.body.dom).on('click', function () {
                    me.cancelAutoClose();
                    me.addCls('notification-fixed');
                }, me);
            }
        }

        if (me.autoClose) {
            me.task = new Ext.util.DelayedTask(me.doAutoClose, me);
            me.task.delay(me.autoCloseDelay);
        }

        // Shunting offscreen to avoid flicker
        me.el.setX(-10000);
        me.el.setOpacity(1);

    },

    afterShow: function () {
        var me = this;

        var notifications = me.getNotifications(me.managerAlignment);

        if (notifications.length) {
            me.el.alignTo(notifications[notifications.length - 1].el, me.siblingAlignment, [0, 0]);
            me.xPos = me.getXposAlignedToSibling(notifications[notifications.length - 1]);
            me.yPos = me.getYposAlignedToSibling(notifications[notifications.length - 1]);
        } else {
            me.el.alignTo(me.manager.el, me.managerAlignment, [(me.paddingX * me.paddingFactorX), (me.paddingY * me.paddingFactorY)], false);
            me.xPos = me.getXposAlignedToManager();
            me.yPos = me.getYposAlignedToManager();
        }

        Ext.Array.include(notifications, me);

        me.el.animate({
            to: {
                x: me.xPos,
                y: me.yPos,
                opacity: 1
            },
            easing: me.slideInAnimation,
            duration: me.slideInDuration,
            dynamic: true
        });

        this.callParent(arguments);
    },

    slideBack: function () {
        var me = this;

        var notifications = me.getNotifications(me.managerAlignment);
        var index = Ext.Array.indexOf(notifications, me)

        // Not animating the element if it already started to hide itself or if the manager is not present in the dom
        if (!me.isHiding && me.el && me.manager && me.manager.el && me.manager.el.dom && me.manager.el.isVisible()) {

            if (index) {
                me.xPos = me.getXposAlignedToSibling(notifications[index - 1]);
                me.yPos = me.getYposAlignedToSibling(notifications[index - 1]);
            } else {
                me.xPos = me.getXposAlignedToManager();
                me.yPos = me.getYposAlignedToManager();
            }

            me.stopAnimation();

            me.el.animate({
                to: {
                    x: me.xPos,
                    y: me.yPos
                },
                easing: me.slideBackAnimation,
                duration: me.slideBackDuration,
                dynamic: true
            });
        }
    },

    cancelAutoClose: function () {
        var me = this;

        if (me.autoClose) {
            me.task.cancel();
        }
    },

    doAutoClose: function () {
        var me = this;

        if (!(me.stickWhileHover && me.mouseIsOver)) {
            // Close immediately
            me.close();
        } else {
            // Delayed closing when mouse leaves the component.
            me.closeOnMouseOut = true;
        }
    },

    removeFromManager: function () {
        var me = this;

        if (me.manager) {
            var notifications = me.getNotifications(me.managerAlignment);
            var index = Ext.Array.indexOf(notifications, me);
            if (index != -1) {
                Ext.Array.erase(notifications, index, 1);

                // Slide "down" all notifications "above" the hidden one
                for (; index < notifications.length; index++) {
                    notifications[index].slideBack();
                }
            }
        }
    },

    hide: function () {
        var me = this;

        // Avoids restarting the last animation on an element already underway with its hide animation
        if (!me.isHiding && me.el) {

            me.isHiding = true;

            me.cancelAutoClose();
            me.stopAnimation();

            me.el.animate({
                to: {
                    opacity: 0
                },
                easing: 'easeIn',
                duration: me.hideDuration,
                dynamic: false,
                listeners: {
                    afteranimate: function () {
                        me.removeFromManager();
                        me.readyToHide = true;
                        me.hide(me.animateTarget, me.doClose, me);
                    }
                }
            });
        }

        // Calling parent's hide function to complete hiding
        if (me.readyToHide) {
            me.isHiding = false;
            me.readyToHide = false;
            me.removeCls('notification-fixed');
            me.callParent(arguments);
            if (me.destroyAfterHide) {
                me.destroy();
            }
        }
    },

    destroy: function () {
        var me = this;

        if (!me.hidden) {
            me.destroyAfterHide = true;
            me.hide(me.animateTarget, me.doClose, me);
        } else {
            me.callParent(arguments);
        }
    }

});


/*    Changelog:
*
*    2011-09-01 - 1.1: Bugfix. Array.indexOf not universally implemented, causing errors in IE<=8. Replaced with Ext.Array.indexOf.
*    2011-09-12 - 1.2: Added config options: stickOnClick and stickWhileHover.
*    2011-09-13 - 1.3: Cleaned up component destruction.
*    2012-03-06 - 2.0: Renamed some properties ending with "Delay" to the more correct: "Duration".
*                    Moved the hiding animation out of destruction and into hide.
*                      Renamed the corresponding "destroy" properties to "hide".
*                    (Hpsam) Changed addClass to addCls.
*                    (Hpsam) Avoiding setting 'notification-fixed' when auto hiding.
*                    (Justmyhobby) Using separate arrays to enable managers to mix alignments.
*                    (Kreeve_ctisn) Removed default title.
*                      (Jmaia) Center of edges can be used for positioning. Renamed corner property to position.
*                    (Hpsam) Hiding or destroying manager does not cause errors.
*  2012-08-12 - 2.1: Renamed autoHide to autoClose
*                    (Dmurat) Enabled reuse of notifications (closeAction: 'hide')
*                    (Idonofrio) Destroying notification by default (closeAction: 'destroy')
*/  
Ext.define('Ext.ux.form.SearchField', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.searchfield',

    displayField: 'Name',
    typeAhead: false,
    hideLabel: true,
    hideTrigger: true,
    queryMode: 'remote',
    listeners: {
        // delete the previous query in the beforequery event or set
        // combo.lastQuery = null (this will reload the store the next time it expands)
        beforequery: function(qe){
            delete qe.combo.lastQuery;
        }
    },
    minChars: 1,
    autoSelect: true,
    triggerAction: 'query',

    listConfig: {
        loadingText: 'Pesquisando...',
        emptyText: 'Nenhum resultado encontrado'

        /*getInnerTpl: function() {
        return '<a class="search-item" href="http://www.sencha.com/forum/showthread.php?t={topicId}&p={id}">' +
        '<h3><span>{[Ext.Date.format(values.lastPost, "M j, Y")]}<br />by {author}</span>{title}</h3>' +
        '{excerpt}' +
        '</a>';
        }*/
    },

    initComponent: function () {
        this.store = Ext.create('Ext.data.Store', {
            fields: ['DocumentTypeId', 'Name', 'DocumentId'],
            autoLoad: false,

            proxy: {
                type: 'ajax',
                url: 'search/list',
                reader: {
                    type: 'json',
                    root: 'Collection',
                    successProperty: 'success'
                }
            }
        });

        this.on({
            select: { fn: 'onOptionSelect', scope: this }
        });

        this.callParent();
    },

    onOptionSelect: function (combo, records, eOpts) {
        if (!records || !records.length) {
            return;
        }

        var record = records[0];
        var id = record.data.DocumentId;

        if (record.data.DocumentTypeId == CRMax.globals.enums.documentType.Person.Id) {
            Ext.History.add("/person/" + id);
        } else if (record.data.DocumentTypeId == CRMax.globals.enums.documentType.Company.Id) {
            Ext.History.add("/company/" + id);
        } else {
            Ext.History.add("/deal/" + id);
        }

        combo.setValue(null);
    }

});
Ext.define('Ext.ux.form.HoverButton', {
    extend: 'Ext.Button',
    alias: 'widget.hoverButton',
    isOver: false,
    hideDelay: 250,
    showDelay: 0,

    applyListeners: function (menu, cfg) {
        Ext.apply(menu, cfg);
        Ext.each(menu.items, function (item, idx, allItems) {
            if (item.menu) this.applyListeners(item.menu, cfg);
        }, this);
    },

    initComponent: function () {
        var config = {},
            menuConfig = {},
            me = this;

        me.delayedShowMenu = new Ext.util.DelayedTask(function () {
            if (!me.isOver) return;
            me.showMenu();
        }, this);

        me.delayedHideMenu = new Ext.util.DelayedTask(function () {
            if (me.isOver) return;
            me.hideMenu();
        });

        if (Ext.isDefined(this.initialConfig.menu)) {
            config = {
                listeners: {
                    mouseover: {
                        scope: me,
                        fn: function (b) {
                            me.isOver = true;
                            me.delayedShowMenu.delay(me.showDelay);
                        }
                    },
                    mouseout: {
                        scope: me,
                        fn: function (b) {
                            me.isOver = false;
                            me.delayedHideMenu.delay(me.hideDelay);
                        }
                    }
                }
            };

            menuConfig = {
                listeners: {
                    mouseover: {
                        scope: me,
                        fn: function (menu, item, e) {
                            me.delayedHideMenu.cancel();
                        }
                    },
                    mouseenter: {
                        scope: me,
                        fn: function (menu, e) {
                            me.delayedHideMenu.cancel();
                        }
                    },
                    mouseleave: {
                        scope: me,
                        fn: function (menu, e) {
                            me.delayedHideMenu.delay(me.hideDelay);
                        }
                    }
                }
            };

            //apply mouseover/leave listeners to all submenus recursively
            me.applyListeners(me.menu, menuConfig);
        }

        Ext.apply(me, Ext.apply(me.initialConfig, config));
        //Ext.HoverButton.superclass.initComponent.apply(me, arguments);
        this.callParent();
    }
});
// Ext.ux.panel.UploadPanel for ExtJs 4
// Source: http://www.thomatechnik.de/webmaster-tools/extjs-plupload/
// Based on: http://www.sencha.com/forum/showthread.php?98033-Ext.ux.Plupload-Panel-Button-%28file-uploader%29
// Please link to this page if you find this extension usefull
// Version 0.1

Ext.define('Ext.ux.panel.UploadPanel',
{ extend: 'Ext.grid.Panel',
    alias: 'widget.xuploadpanel',

    // Configuration
    url: '/File/Upload', // URL to your server-side upload-script
    urlExtraParams: {}, //Object to be serialized as query string and appended to the URL dynamically, passing additional info like record Id, original File Name, etc.

    max_file_size: '100mb', // The max. allowed file-size
    multipart: false,

    border: false,

    chunk_size: '1mb',
    unique_names: true,
    multiple_queues: false,
    hideBottomBar: true,
    hideDeleteButton: true,
    hideEmptyBody: true,

    pluploadPath: '/scripts/plupload/', // Path to plupload
    pluploadRuntimes: 'html5,silverlight,flash,html4',  // All the runtimes you want to use

    cls: 'upload-panel',

    // Texts (language-dependent)

    texts:
	{ status: ['NA FILA', 'TRANSFERINDO', 'DESCONHECIDO', 'FALHOU', 'ENVIADO'],
	    DragDropAvailable: 'Arraste e solte arquivos aqui',
	    noDragDropAvailable: '',
	    emptyTextTpl: '<div style="color:#808080; margin:0 auto; text-align:center; top:48%; position:relative;">{0}</div>',
	    cols: ["Arquivo", "Tamanho", "Status", "Mensagem"],
	    addButtonText: 'Anexar arquivos...',
	    uploadButtonText: 'upload',
	    cancelButtonText: 'cancel',
	    deleteButtonText: 'remover',
	    deleteUploadedText: 'remover finalizados',
	    deleteAllText: 'remover todos',
	    deleteSelectedText: 'remover selecionados',
	    progressCurrentFile: 'arquivo atual:',
	    progressTotal: 'total:',
	    statusInvalidSizeText: 'Arquivo muito grande',
	    statusInvalidExtensionText: 'tipo de arquivo inválido'
	},

    // Internal (do not change)
    // Grid-View
    multiSelect: true,
    viewConfig:
	{ deferEmptyText: false // For showing emptyText
	},

    // Hack: loaded of the actual file (plupload is sometimes a step ahead)
    loadedFile: 0,

    constructor: function (config) {
        // List of files
        this.success = [];
        this.failed = [];

        // Column-Headers
        config.columns =
		[{ header: this.texts.cols[0], flex: 1, dataIndex: 'name' },
		 	{ header: this.texts.cols[1], flex: 1, align: 'right', dataIndex: 'size', renderer: Ext.util.Format.fileSize },
		 	{ header: this.texts.cols[2], flex: 1, dataIndex: 'status', renderer: this.renderStatus },
		 	{ header: this.texts.cols[3], flex: 1, dataIndex: 'msg' },
            { width: 50, renderer: this.renderDeleteGridRowButton }
		];

        config.listeners = {
            'itemclick': function (view, record, item, index, eventObj, eOpts) {
                var targetEl = Ext.get(eventObj.target);
                if (targetEl.hasCls('btn-delete-grid-row')) {
                    this.remove_file(record.get('id'));
                }

            },
            scope: this
        };

        // Model and Store
        if (!Ext.ModelManager.getModel('Plupload')) {
            Ext.define('Plupload',
				{ extend: 'Ext.data.Model',
				    fields: ['id', 'loaded', 'name', 'size', 'percent', 'status', 'msg']
				});
        };

        config.store =
		{ type: 'json',
		    model: 'Plupload',
		    listeners: {
		        load: this.onStoreLoad,
		        remove: this.onStoreRemove,
		        update: this.onStoreUpdate,
		        scope: this
		    },
		    proxy: 'memory'
		};

        // Top-Bar
        this.tbar =
		{ enableOverflow: true,
		    items: [
	                new Ext.Button({
	                    text: this.texts.addButtonText,
	                    itemId: 'addButton',
	                    iconCls: config.addButtonCls || 'pluploadAddCls',
	                    disabled: true
	                }),
	                new Ext.Button({
	                    text: this.texts.uploadButtonText,
	                    handler: this.onStart,
	                    scope: this,
	                    hidden: true,
	                    disabled: true,
	                    itemId: 'upload',
	                    iconCls: config.uploadButtonCls || 'pluploadUploadCls'
	                }),
	                new Ext.Button({
	                    text: this.texts.cancelButtonText,
	                    handler: this.onCancel,
	                    scope: this,
	                    hidden: true,
	                    disabled: true,
	                    itemId: 'cancel',
	                    iconCls: config.cancelButtonCls || 'pluploadCancelCls'
	                }),
	                new Ext.SplitButton({
	                    hidden: this.hideDeleteButton,
	                    text: this.texts.deleteButtonText,
	                    handler: this.onDeleteSelected,
	                    menu: new Ext.menu.Menu({
	                        items: [
	                            { text: this.texts.deleteUploadedText, hidden: true, handler: this.onDeleteUploaded, scope: this },
	                            '-',
	                            { text: this.texts.deleteAllText, handler: this.onDeleteAll, scope: this },
	                            '-',
	                            { text: this.texts.deleteSelectedText, handler: this.onDeleteSelected, scope: this }
	                        ]
	                    }),
	                    scope: this,
	                    //disabled: true,
	                    itemId: 'delete',
	                    iconCls: config.deleteButtonCls || 'pluploadDeleteCls'
	                })
	            ]
		};

        // Progress-Bar (bottom)
        this.progressBarSingle = new Ext.ProgressBar(
		{ flex: 1,
		    animate: true
		});
        this.progressBarAll = new Ext.ProgressBar(
		{ flex: 2,
		    animate: true
		});

        this.bbar =
		{ layout: 'hbox',
		    hidden: this.hideBottomBar,
		    style: { paddingLeft: '5px' },
		    items: [this.texts.progressCurrentFile,
	               	 	this.progressBarSingle,
	               	 	{ xtype: 'tbtext',
	               	 	    itemId: 'single',
	               	 	    style: 'text-align:right',
	               	 	    text: '',
	               	 	    width: 100
	               	 	},
	               	 	this.texts.progressTotal,
	               	 	this.progressBarAll,
	               	 	{ xtype: 'tbtext',
	               	 	    itemId: 'all',
	               	 	    style: 'text-align:right',
	               	 	    text: '',
	               	 	    width: 100
	               	 	},
	               	 	{ xtype: 'tbtext',
	               	 	    itemId: 'speed',
	               	 	    style: 'text-align:right',
	               	 	    text: '',
	               	 	    width: 100
	               	 	},
	               	 	{ xtype: 'tbtext',
	               	 	    itemId: 'remaining',
	               	 	    style: 'text-align:right',
	               	 	    text: '',
	               	 	    width: 100
	               	 	}
	                ]
		};

        this.callParent(arguments);
    },

    updateBodyVisibility: function () {
        //Hide Grid
        var hideCls = 'upload-panel-hidden-body';

        if (this.hideEmptyBody === true && this.store.getCount() === 0) {
            this.addCls(hideCls);
        } else {
            this.removeCls(hideCls);
        }

    },

    onDestroy: function () {
        this.store.destroyStore();
        this.callParent();
    },

    afterRender: function () {
        this.updateBodyVisibility();
        this.store.on('datachanged', this.updateBodyVisibility, this);

        this.callParent(arguments);
        this.initPlUpload();
    },

    getPendingFilesCount: function () {
        var pendingFilesCount = 0;
        this.store.each(function (record) {
            if (record.get("status") == 1) {
                pendingFilesCount++;
            }
        }, this);
        return pendingFilesCount;
    },

    renderDeleteGridRowButton: function (value, meta, record, rowIndex, colIndex, store, view) {
        return '<button class="btn-delete-grid-row"></button>';
    },

    renderStatus: function (value, meta, record, rowIndex, colIndex, store, view) {
        var s = this.texts.status[value - 1];
        if (value == 2) {
            s += " " + record.get("percent") + " %";
        }
        return s;
    },

    /*
    renderProgress: function(value, meta, record, rowIndex, colIndex, store, view)
    {	return value;
    console.log("Call renderer", value);
    var id;
    if (this.progressBars[rowIndex] === undefined)
    {	console.log("Create Bar");
    id = Ext.id();
    this.progressBars[rowIndex] = id;
    Ext.Function.defer(function(id, record)
    {	console.log("Create bar ", id, value, record);
    var bar = new Ext.ProgressBar(
    {	height: 15,
    renderTo: id,
    value: (value / 100)
    });
    this.progressBars[record.id] = bar;
    console.log("After create bar", id, value, record);
    },
    25,
    this,
    [id, record]);			
    }
    else
    {	if (Ext.isObject(this.progressBars[rowIndex]))
    {	var bar = this.progressBars[rowIndex];
    bar.setValue(value);
    id = bar.getEl().dom.id;
    console.log("Fetch bar ", id);
    }
    else
    console.log("Wait for creation");
    }
    return (String.format('<div id="{0}"></div>', id));
    },
    */

    getTopToolbar: function () {
        var bars = this.getDockedItems('toolbar[dock="top"]');
        return bars[0];
    },
    getBottomToolbar: function () {
        var bars = this.getDockedItems('toolbar[dock="bottom"]');
        return bars[0];
    },

    initPlUpload: function () {
        this.uploader = new plupload.Uploader(
        { url: this.url,
            runtimes: this.pluploadRuntimes,
            browse_button: this.getTopToolbar().getComponent('addButton').getEl().dom.id,
            container: this.getEl().dom.id,
            max_file_size: this.max_file_size || '',
            resize: this.resize || '',
            flash_swf_url: this.pluploadPath + 'plupload.flash.swf',
            silverlight_xap_url: this.pluploadPath + 'plupload.silverlight.xap',
            filters: this.filters || [],
            multiple_queues: this.multiple_queues,
            chunk_size: this.chunk_size,
            unique_names: this.unique_names,
            //multipart: this.multipart, //Asp.netMVC FileUpload controller doesn't work if these multipart properties are present
            //multipart_params: this.multipart_params || null, //Asp.netMVC FileUpload controller doesn't work if these multipart properties are present
            drop_element: this.getEl().dom.id,
            required_features: this.required_features || null
        });

        // Events
        Ext.each(['Init', 'ChunkUploaded', 'FilesAdded', 'FilesRemoved', 'FileUploaded', 'BeforeUpload', 'PostInit',
		                  'QueueChanged', 'Refresh', 'StateChanged', 'UploadFile', 'UploadProgress', 'Error'],
		                 function (v) {
		                     this.uploader.bind(v, eval("this.Plupload" + v), this);
		                 }, this);
        // Init Plupload
        this.uploader.init();
    },

    onDeleteSelected: function () {
        Ext.each(this.getView().getSelectionModel().getSelection(),
            function (record) {
                this.remove_file(record.get('id'));
            }, this
        );
    },
    onDeleteAll: function () {
        var ids = [];

        this.store.each(
            function (record) {
                ids.push(record.get('id'));
            }, this
        );

        Ext.each(ids,
            function (id) {
                this.remove_file(id);
            }, this);

    },
    onDeleteUploaded: function () {

        var ids = [];

        this.store.each(
            function (record) {
                if (record.get('status') == 5) {
                    ids.push(record.get('id'));
                }
            }, this
        );

        Ext.each(ids,
            function (id) {
                this.remove_file(id);
            }, this);


    },
    onCancel: function () {
        this.uploader.stop();
        this.updateProgress();
    },
    onStart: function () {
        this.fireEvent('beforestart', this);
        if (this.multipart_params) {
            this.uploader.settings.multipart_params = this.multipart_params;
        }

        this.uploader.start();
    },

    remove_file: function (id) {
        var fileObj = this.uploader.getFile(id);
        if (fileObj) {
            this.uploader.removeFile(fileObj);
        }
        else {
            this.store.remove(this.store.getById(id));
        }
    },
    updateStore: function (files) {
        Ext.each(files, function (data) {
            this.updateStoreFile(data);
        }, this);
    },
    updateStoreFile: function (data) {

        data.msg = data.msg || '';
        var record = this.store.getById(data.id);
        if (record) {
            record.set(data);
            record.commit();
        }
        else {
            this.store.add(data);
        }

    },
    onStoreLoad: function (store, record, operation) {
    },
    onStoreRemove: function (store, record, operation) {
        if (!store.data.length) {
            this.getTopToolbar().getComponent('delete').setDisabled(true);
            this.uploader.total.reset();
        }
        var id = record.get('id');

        Ext.each(this.success,
	        function (v) {
	            if (v && v.id == id) {
	                Ext.Array.remove(this.success, v);
	            }
	        }, this
	    );

        Ext.each(this.failed,
	        function (v) {
	            if (v && v.id == id) {
	                Ext.Array.remove(this.failed, v);
	            }
	        }, this
	    );
    },
    onStoreUpdate: function (store, record, operation) {
        var canUpload = false;
        if (this.uploader.state != 2) {
            this.store.each(function (record) {
                if (record.get("status") == 1) {
                    canUpload = true;
                    return false;
                }
            }, this);
        }
        this.getTopToolbar().getComponent('upload').setDisabled(!canUpload);
    },

    updateProgress: function (file) {
        var queueProgress = this.uploader.total;

        // All
        var total = queueProgress.size;
        var uploaded = queueProgress.loaded;
        this.getBottomToolbar().getComponent('all').setText(Ext.util.Format.fileSize(uploaded) + "/" + Ext.util.Format.fileSize(total));
        if (total > 0)
            this.progressBarAll.updateProgress(queueProgress.percent / 100, queueProgress.percent + " %");
        else this.progressBarAll.updateProgress(0, ' ');

        // Speed+Remaining
        var speed = queueProgress.bytesPerSec;
        if (speed > 0) {
            var totalSec = parseInt((total - uploaded) / speed);
            var hours = parseInt(totalSec / 3600) % 24;
            var minutes = parseInt(totalSec / 60) % 60;
            var seconds = totalSec % 60;
            var timeRemaining = result = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
            this.getBottomToolbar().getComponent('speed').setText(Ext.util.Format.fileSize(speed) + '/s');
            this.getBottomToolbar().getComponent('remaining').setText(timeRemaining);
        }
        else {
            this.getBottomToolbar().getComponent('speed').setText('');
            this.getBottomToolbar().getComponent('remaining').setText('');
        }

        // Single
        if (!file) {
            this.getBottomToolbar().getComponent('single').setText('');
            this.progressBarSingle.updateProgress(0, ' ');
        }
        else {
            total = file.size;
            //uploaded = file.loaded; // file.loaded sometimes is 1 step ahead, so we can not use it.
            //uploaded = 0; if (file.percent > 0) uploaded = file.size * file.percent / 100.0; // But this solution is imprecise as well since percent is only a hint
            uploaded = this.loadedFile; // So we use this Hack to store the value which is one step back
            this.getBottomToolbar().getComponent('single').setText(Ext.util.Format.fileSize(uploaded) + "/" + Ext.util.Format.fileSize(total));
            this.progressBarSingle.updateProgress(file.percent / 100, (file.percent).toFixed(0) + " %");
        }
    },

    PluploadInit: function (uploader, data) {
        this.getTopToolbar().getComponent('addButton').setDisabled(false);
        // console.log("Runtime: ", data.runtime);
        if (data.runtime == "flash" ||
    		data.runtime == "silverlight" ||
    		data.runtime == "html4") {
            this.view.emptyText = this.texts.noDragDropAvailable;
        }
        else {
            this.view.emptyText = this.texts.DragDropAvailable
        }
        this.view.emptyText = String.format(this.texts.emptyTextTpl, this.view.emptyText);
        this.view.refresh();

        this.updateProgress();
    },
    PluploadChunkUploaded: function () {
    },
    PluploadFilesAdded: function (uploader, files) {
        this.getTopToolbar().getComponent('delete').setDisabled(false);
        this.updateStore(files);
        this.updateProgress();
    },
    PluploadFilesRemoved: function (uploader, files) {
        Ext.each(files,
            function (file) {
                this.store.remove(this.store.getById(file.id));
            }, this
		);
        this.updateProgress();
    },

    PluploadBeforeUpload: function (uploader, file) {
        this.fireEvent('beforeupload', this);

        if (this.unique_names == true) {
            //append original file name to the submission url
            this.urlExtraParams.originalName = file.name;
        }

        uploader.settings.originalUrl = uploader.settings.originalUrl || uploader.settings.url;
        uploader.settings.url = uploader.settings.originalUrl + '?' + Ext.Object.toQueryString(this.urlExtraParams);

    },

    PluploadFileUploaded: function (uploader, file, status) {
        var response = Ext.JSON.decode(status.response);
        if (response.success == true) {
            file.server_error = 0;
            this.success.push(file);
        }
        else {
            if (response.message) {
                file.msg = '<span style="color: red">' + response.message + '</span>';
            }
            file.server_error = 1;
            this.failed.push(file);
        }
        this.updateStoreFile(file);
        this.updateProgress(file);
    },
    PluploadPostInit: function () {
    },
    PluploadQueueChanged: function (uploader) {
        this.updateProgress();
    },
    PluploadRefresh: function (uploader) {
        this.updateStore(uploader.files);
        this.updateProgress();
    },
    PluploadStateChanged: function (uploader) {
        if (uploader.state == 2) {
            this.fireEvent('uploadstarted', this);
            this.getTopToolbar().getComponent('cancel').setDisabled(false);
        }
        else {

            this.fireEvent('uploadcomplete', this, this.success, this.failed);
            var tbar = this.getTopToolbar();
            tbar && tbar.getComponent('cancel').setDisabled(true);
        }
    },
    PluploadUploadFile: function () {
        this.loadedFile = 0;
    },
    PluploadUploadProgress: function (uploader, file) {	// No chance to stop here - we get no response-text from the server. So just continue if something fails here. Will be fixed in next update, says plupload.
        if (file.server_error) {
            file.status = 4;
        }
        this.updateStoreFile(file);
        this.updateProgress(file);
        this.loadedFile = file.loaded;
    },
    PluploadError: function (uploader, data) {
        data.file.status = 4;
        if (data.code == -600) {
            data.file.msg = String.format('<span style="color: red">{0}</span>', this.texts.statusInvalidSizeText);
        }
        else if (data.code == -700) {
            data.file.msg = String.format('<span style="color: red">{0}</span>', this.texts.statusInvalidExtensionText);
        }
        else {
            data.file.msg = String.format('<span style="color: red">{2} ({0}: {1})</span>', data.code, data.details, data.message);
        }
        this.updateStoreFile(data.file);
        this.updateProgress();
    }
});


// Advance File-Size
Ext.util.Format.fileSize = function (value) {
    if (value > 1) {
        var s = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
        var e = Math.floor(Math.log(value) / Math.log(1024));
        if (e > 0)
            return (value / Math.pow(1024, Math.floor(e))).toFixed(2) + " " + s[e];
        else return value + " " + s[e];
    }
    else if (value == 1) {
        return "1 Byte";
    }
    return '-';
}

String.format = function () {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }
    return s;
}
Ext.define('Ext.ux.panel.FileInfoPanel', {
    extend: 'Ext.Component',
    alias: 'widget.fileinfopanel',

    initComponent: function () {
        this.tpl = new Ext.XTemplate(
            '<ul class="attachments">',
                '<tpl for="FileData">',
                    '<li>',
                        '<a href="{[this.buildFileLink(parent, xindex-1)]}" target="_blank" class="attachment" type="text/plain">',
                            '<span class="attachment-icon attachment-icon-{[this.getFileExtension(parent, xindex-1)]}"></span> {Name}',
                        '</a>',
                    '</li>',
                '</tpl>',
            '</ul>',
            {
                disableFormats: true,
                buildFileLink: function (record, index) {
                    return CRMax.globals.fileDownloadPath + '/' + record.FileData[index].FileId;
                },
                getFileExtension: function (record, index) {
                    var GENERIC_EXTENSION = "gen";
                    var supportedExtensions = {
                    /*    'pdf': true,
                        'doc': true,
                        'gif': true,*/
                        'png': true
                    };

                    var fileName = record.FileData[index].Name.toLowerCase();

                    var extension = (/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName) : undefined;

                    if (!extension || supportedExtensions[extension] !== true) {
                        return GENERIC_EXTENSION;
                    } else {
                        return extension;
                    }

                }
            }
          );


        if (this.fileData) {
            this.loadData(this.fileData);
        }

        this.callParent();
    },

    loadData: function (fileData) {
        this.fileData = fileData;
        this.update({ FileData: fileData });
    }

});
/**
 *   ComboView
 */
 Ext.define('Ext.ux.ComboView',
    {extend : 'Ext.view.View', 
         alias : 'widget.comboview', 
    /**
      * @cfg {Boolean} maxLength
      * maximum length for viewItems. If text is longer, it gets 'ellipsisied'.  
      */
    maxLength: 18,
    /**
      * @cfg {Boolean} removeOnDblClick
      * true to unselect viewItem on double click  
      */
    removeOnDblClick: true,
    /**
      * @cfg {Boolean} inputLength
      * length for the inputfield  
      */
    inputLength: 40,
    itemSelector: 'li.x-boxselect-item',
    overItemCls: 'selected',
    initComponent: function () {
        var me = this,
            field = me.field;
        me.closeCls = 'x-boxselect-item-close';
        if (!me.tpl) {
            var displayField = field.displayField,
                descField = field.descField,
                iconClsField = field.iconClsField;
            me.tpl = new Ext.XTemplate(
                '<ul class="x-boxselect-list {fieldCls} {typeCls}">',
                    '{[this.empty(values)]}',
                    '<tpl for=".">', 
                        '<li class="x-boxselect-item ', 
                        iconClsField ? ('x-boxselect-icon {' + iconClsField + '}"') : '"', 
                        descField ? ('data-qtitle="{' + displayField + '}" data-qtip="{' + descField + '}">') : '>', 
                        '<div class="x-tab-close-btn ', me.closeCls, '"></div>', 
                        '<div class="x-boxselect-item-text">{[this.ellipsis(values.', displayField, ')]}</div>', 
                        '<div class="x-tab-close-btn ', me.closeCls, '"></div>', 
                    '</li>', 
                '</tpl>', 
				'<li class="x-boxselect-input"><input style="width:'+( me.inputLength) +'px;"/></li>', // need this to manage focus; widh of input is larger in createNewOnEnter is set to true
            '</ul>', {
                compiled: true,
                disableFormats: true,
                length: me.maxLength,
                ellipsis: function (txt) {
                    return Ext.String.ellipsis(txt, this.length)
                },
                emptyText: me.emptyText,
                empty : function(values) {
                    return   '<span class="empty">' + (values.length  ? '' : this.emptyText )+ '</span>' 
                }
            })
        };
        delete me.emptyText;
        me.callParent(arguments)
    },
    renderSelectors: {
        inputEl: 'input',
        emptyEl: 'span.empty'
    },
    getFocusEl: function () {
        return this.inputEl
    },
    addFocusListener: function (force) {
        var me = this;
        if (!me.focusListenerAdded || force) {
            me.callParent(); // force argument only valid in ComboView
            var focusEl = me.getFocusEl();
            if (focusEl) {
                focusEl.on({
                    focus: me.field.onFocus,
                    blur: me.field.onBlur,
                    scope: me.field
                });
                me.getEl().on({
                    focus: me.field.onFocus,
                    scope: me.field
                })
            }
        }		
    },
    onItemClick: function (r, h, i, e, o) {
        if (e.getTarget('.' + this.closeCls)) {
            return this.onDataChange(r, 'remove')
        }
        this.highlightItem(h)
    },
    onItemDblClick: function (r, h, i, e, o) {
        if (this.removeOnDblClick) {
            this.onDataChange(r, 'remove')
        }
    },
    onDataChange: function (r, action) {
        var me = this;
        if(me.field.readOnly || me.field.disabled) {return}
        if (action == 'remove') {
            me.store.remove(r)
        }
        me.field.setStoreValues()
    },
    listeners: {
        refresh: {
            fn: function () {
                this.applyRenderSelectors()
                this.addFocusListener(true)
            }
        }
    },
    onDestroy: function () {
        var me = this,
            focusEl;
        if (focusEl = me.getFocusEl()) {
            focusEl.clearListeners()
        }
    }
/*::::*/
});




/**
 *   ComboFieldBox
 */
Ext.define('Ext.ux.ComboFieldBox',
    { extend: 'Ext.form.field.ComboBox',
        alias: 'widget.combofieldbox',
        requires: ['Ext.ux.ComboView'],
        multiSelect: true,
        /**
        * @cfg
        * maximum height for inputEl. 
        */
        maxHeight: 150,
        /**
        * @cfg
        * name of field used for description/tooltip
        */
        descField: null,
        /**
        * @cfg
        * config object passed to the view 
        * viewCfg: {},
        */
        /**
        * @cfg {String} iconClsField
        * The underlying iconCls field name to bind to this ComboBox.
        * iconClsField: '',
        */
        /**
        * @cfg {Boolean} createNewOnEnter
        * When forceSelection is false, new records can be created by the user. This configuration
        * option has no effect if forceSelection is true, which is the default.
        */
        createNewOnEnter: false,
        /**
        * @cfg {Boolean} forceSelection
        * override parent config. If force selection is set to false and    
        */
        forceSelection: true,
        /**
        * @cfg {Boolean} selectOnTab
        * Whether the Tab key should select the currently highlighted item.
        */
        selectOnTab: false,
        /**
        * @cfg {String} trigger1Cls
        * css class for the first trigger. To have just one trigger acting like in usual combo, set trigger1Cls to null. First trigger clears all values
        */
        trigger1Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
        /**
        * @cfg {String} trigger2Cls
        * css class for the second trigger. To have just one trigger, set trigger1Cls to null.
        */
        trigger2Cls: Ext.baseCSSPrefix + 'form-combo-trigger',

        /**
        * @cfg {String} listIconCls
        * css class to use when an iconClsField is set. This class is injected into getInnerTpl method when constructing the comboBox boundList
        */
        listIconCls: 'x-boundlist-icon',
        fieldSubTpl: [
        '<div class="{hiddenDataCls}" role="presentation"></div>',
        '<div id="{id}"',
            '<tpl if="readOnly"> readonly="readonly"</tpl>',
            '<tpl if="disabled"> disabled="disabled"</tpl>',
            '<tpl if="tabIdx"> tabIndex="{tabIdx}"</tpl>',
            '<tpl if="name"> name="{name}"</tpl>',
            '<tpl if="fieldStyle"> style="{fieldStyle}"</tpl>',
            '<tpl if="placeholder"> placeholder="{placeholder}"</tpl>',
            '<tpl if="size"> size="{size}"</tpl>',
            'class="{fieldCls} {typeCls} x-boxselect" autocomplete="off" />',
        '</div>',
    {
        compiled: true,
        disableFormats: true
    }
    ],
        getSubTplData: function () {
            var me = this,
            fieldStyle = me.getFieldStyle(),
            ret = me.callParent(arguments);
            ret.fieldStyle = (fieldStyle || '') + ';overflow:auto;height:' + (me.height ? (me.height + 'px;') : 'auto;') + (me.maxHeight ? ('max-height:' + me.maxHeight + 'px;') : '');
            delete me.height; //need to delete height for the correct component height to be recalculated on layout. 
            return ret;
        },
        alignPicker: function () {
            var me = this,
            picker = me.getPicker(),
            w = me.triggerWidth;
            me.callParent(arguments);
            if (me.isExpanded && me.matchFieldWidth) {
                picker.setWidth(me.bodyEl.getWidth() - (me.trigger2Cls ? (2 * w) : w));
            }
        },
        initComponent: function () {
            var me = this;
            if (!me.trigger1Cls) {
                me.onTrigger1Click = null;
                me.trigger2Cls = null;
            }
            me.getValueStore();
            me.listConfig = Ext.apply(me.listConfig || {}, { selModel: { mode: 'SIMPLE', enableKeyNav: false} });
            if (me.iconClsField || me.descField) {
                Ext.apply(me.listConfig, { getInnerTpl: function (displayField) {
                    return '<div data-qtip="{' + me.descField + '}" class="' + ((me.iconClsField && me.listIconCls) ? me.listIconCls : '') + ' {' + me.iconClsField + '}">{' + me.displayField + '}</div>';
                }
                })
            };
            me.callParent(arguments);
        },
        onTrigger1Click: function () {
            var me = this;
            me.setValue("");
            me.collapse();
        },
        setValueStore: function (store) {
            this.valueStore = store;
        },
        getValueStore: function () {
            var me = this;
            return me.valueStore || (me.valueStore = me.createValueStore());
        },
        createValueStore: function () {
            return this.valueStore = new Ext.data.Store({
                model: this.store.model
            });
        },
        /**
        * get all field values from value store and re-set combobox values
        */
        setStoreValues: function () {
            var me = this,
            st = me.getValueStore();
            me.setValue(st.data.extractValues(me.valueField || st.valueField, 'data'));
            me.syncSelection();
        },
        getValueModels: function () {
            return this.valueModels || [];
        },
        afterSetValue: function (action) {
            var me = this;
            me.valueStore.removeAll();
            me.valueStore.add(me.getValueModels());
            if (me.isExpanded) {
                me.alignPicker();
            }
            me.syncSelection();
            me.updateLayout();
        },
        setValue: function (value, action) {
            var me = this,
        	picker = me.getPicker(),
        	oldPr = picker.preserveScrollOnRefresh;
            if (me.tempValue) {
                value = Ext.Array.unique(value.concat(me.tempValue))
                var val = me.store.data.extractValues(me.valueField, 'data');
                if (me.typeAhead && (me.store.getCount() == 1)) {
                    value.push(me.store.getAt(0).get(me.valueField));
                    me._needCollapse = true;
                }
                me.store.data.addAll(Ext.Array.filter(me.valueStore.data.items, function (i) { return (val.indexOf(i.data[me.valueField]) < 0) }))
                picker.preserveScrollOnRefresh = true;
                picker.refresh();
                picker.preserveScrollOnRefresh = oldPr;
            }
            me.callParent([value, false]);
            me.afterSetValue(action)
        },
        getRawValue: function () {
            return Ext.value(this.rawValue, '');
        },
        doRawQuery: function () {
            var me = this,
			qe;
            if (me.view && me.typeAhead && (qe = me.view.inputEl.getValue())) {
                me.tempValue = me.value;
                this.doQuery(qe, false, true);
                delete me.tempValue;
                if (me._needCollapse) {
                    me.collapse();
                    delete me._needCollapse;
                }
                else {
                    me.onExpand();
                    me._preventClear = true;
                    me.view.inputEl.focus();
                    me.view.inputEl.dom.value = qe;
                    delete me._preventClear;
                }
            }
        },
        onBlur: function () {
            var me = this;
            me.view.inputEl.dom.value = '';
            if (me.view.emptyEl) { me.view.emptyEl.show() }
        },
        onFocus: function () {
            return;
            var me = this,
    		view = me.view;
            me.callParent(arguments);
            if (me._preventClear != true) {
                me.store.clearFilter();
                me.picker.refresh();
            }
            if (view.emptyEl) {
                view.emptyEl.setVisibilityMode(Ext.dom.AbstractElement.DISPLAY)
                view.emptyEl.hide()
            }
            me.view.focus();
        },
        buildKeyNav: function () {
            var me = this,
            selectOnTab = me.selectOnTab,
            picker = me.getPicker();
            return new Ext.view.BoundListKeyNav(picker.el, {
                boundList: picker,
                forceKeyDown: true,
                tab: function (e) {
                    if (selectOnTab || me.typeAhead) {
                        this.selectHighlighted(e);
                        me.triggerBlur();
                    }
                    else {
                        me.onTriggerClick()
                    }
                    return true
                },
                esc: function (e) {
                    me.onTriggerClick()
                }
            });
        },
        onExpand: function () {
            var me = this,
            keyNav = me.listKeyNav,
            selectOnTab = me.selectOnTab,
            picker = me.getPicker();
            // Handle BoundList navigation from the input field. Insert a tab listener specially to enable selectOnTab.
            if (!keyNav) { keyNav = me.listKeyNav = me.buildKeyNav() }
            // While list is expanded, stop tab monitoring from Ext.form.field.Trigger so it doesn't short-circuit selectOnTab
            if (selectOnTab) {
                me.ignoreMonitorTab = true;
            }

            Ext.defer(keyNav.enable, 1, keyNav); //wait a bit so it doesn't react to the down arrow opening the picker
            picker.focus();
            picker.highlightItem(picker.getNode(0));
        },
        onCollapse: function () {
            var me = this;
            me.callParent(arguments);
            me.view.focus();
        },
        createRecord: function (rawValue) {
            var me = this, rec = {};
            rec[me.valueField] = rawValue;
            rec[me.displayField] = rawValue;
            return rec
        },
        afterComponentLayout: function () {
            var me = this;
            me.callParent(arguments);
            if (!me.view) {
                var selectBoxOnTab = me.selectBoxOnTab,
        	move = function (index) {
        	    var nav = this,
                boundList = nav.boundList,
                allItems = boundList.all,
                oldItem = boundList.highlightedItem,
                oldItemIdx = oldItem ? boundList.indexOf(oldItem) : -1,
                newItemIdx = oldItemIdx < allItems.getCount() - 1 ? oldItemIdx + index : 0; //wraps around
        	    nav.highlightAt(newItemIdx);
        	    me.view.focus()
        	},
            del = function (e) {
                if (me.readOnly || me.disabled || !me.editable) { return }
                var nav = this,
                boundList = nav.boundList,
                item = boundList.highlightedItem,
                index;
                if (item) {
                    index = boundList.indexOf(item)
                    me.getValueStore().remove(boundList.getRecord(item))
                    me.setStoreValues();
                    nav.highlightAt(index);
                    me.view.focus()
                }
                return true;
            };
                me.view = new Ext.ux.ComboView(Ext.apply({
                    store: me.valueStore,
                    emptyText: me.emptyText || '',
                    field: me,
                    renderTo: me.inputEl
                }, me.viewCfg));
                var boxKeyNav = me.boxKeyNav = new Ext.view.BoundListKeyNav(me.view.el, {
                    boundList: me.view,
                    forceKeyDown: true,
                    down: function (e) {
                        if (me.isExpanded && me.view.inputEl.getValue()) { return me.picker.focus() }
                        me.onTriggerClick();
                    },
                    right: function (e) {
                        move.call(this, 1)
                    },
                    left: function (e) {
                        move.call(this, -1)
                    },
                    enter: function (e) {
                        if (me.readOnly || me.disabled || !me.editable) { return }
                        if (me.multiSelect && me.createNewOnEnter == true && e.getKey() == e.ENTER && (rawValue = e.target.value) && (!Ext.isEmpty(rawValue))) {
                            rec = me.store.findExact(me.valueField, rawValue);
                            if (rec < 0) {
                                rec = me.store.add(me.createRecord(rawValue))
                            }
                            me.getValueStore().add(rec)
                            me.setStoreValues()
                        }
                        me.view.focus()
                    },
                    tab: function (e) {
                        if (me.isExpanded && e.target.value) {
                            me.picker.focus()
                        }
                        return true
                    },
                    del: del,
                    space: del
                });
                Ext.defer(boxKeyNav.enable, 1, boxKeyNav);
            }
        },
        onDestroy: function () {
            var me = this;
            if (me.view) { Ext.destroy(me.view, me.boxKeyNav) }
            me.callParent(arguments);
        }
    });

Ext.define('CRMax.model.Cotista', {
    extend: 'Ext.data.Model',
    fields: ['IdCotista', 'Nome', 'Cpfcnpj'],
    idProperty: 'IdCotista'/*,

    proxy: {
        type: 'ajax',
        api: {
            create: 'user/create',
            read: 'user/read',
            update: 'user/update'
        },
        reader: {
            type: 'json',
            root: 'Entity',
            successProperty: 'success'
        }
    }*/
});
Ext.define('CRMax.store.Cotistas', {
	extend: 'Ext.data.Store',
	model: 'CRMax.model.Cotista',

	autoLoad: true,

    pageSize: 9,
    remoteFilter: true,
    
	proxy: {
	    type: 'ajax',
	    url: CRMax.globals.baseDirectory + 'Controllers/Cotista/List.ashx',
	    reader: {
	        type: 'json',
	        root: 'Collection',
	        successProperty: 'success',
	        totalProperty: 'totalCount'
	    }
	}

});
Ext.define('CRMax.store.Posicoes', {
	extend: 'Ext.data.Store',
	fields: ['IdPosicao', 'ApelidoCarteira', 'NomeCarteira', 'ValorBruto', 'ValorLiquido', 'ValorLiquidoResgate', 'IdCarteira', 'CotaDia', 'Quantidade', 
	    {name: 'DataDia', type: 'date', dateFormat: 'd/m/y'}],
	autoLoad: true,
	proxy: {
        type: 'ajax',
        url: CRMax.globals.baseDirectory + 'Controllers/PosicaoCotista/List.ashx',
        reader: {
            type: 'json',
            root: 'Collection',
            successProperty: 'success'
        }
    }
});
Ext.define('CRMax.store.PosicoesDetalhadas', {
	extend: 'Ext.data.Store',
	fields: ['IdPosicao', 'NomeCarteira', 'ValorBruto', 'ValorLiquido', 'IdCarteira', 'DataAplicacao', 'Quantidade', 'CotaDia', 'ValorIR', 'ValorIOF'],
	autoLoad: false,
	remoteFilter: true,
	proxy: {
        type: 'ajax',
        url: CRMax.globals.baseDirectory + 'Controllers/PosicaoCotista/Detalhe/List.ashx',
        reader: {
            type: 'json',
            root: 'Collection',
            successProperty: 'success'
        }
    }
});
Ext.define('CRMax.store.FundosPgbl', {
	extend: 'Ext.data.Store',
	fields: ['IdCarteira', 'Nome', 'Categoria', 'GrauRisco', 'RetornoMesCorrente', 'RetornoUltimoMes', 'Retorno12Meses', 'RetornoAno', 'ValorMinimoInicial', 'ValorMinimoAplicacao',
	'ValorMinimoResgate', 'ValorMinimoSaldo', 'DiasCotizacaoAplicacao', 'DiasCotizacaoResgate', 'DiasLiquidacaoAplicacao', 'DiasLiquidacaoResgate',
	'HorarioFim', 'TemPermissaoAplicar', 'LinkLamina', 'LinkTermoAdesao', 'LinkProspecto', 'LinkRegulamento'],
    alias: 'store.fundoscotista',
	autoLoad: false,

	proxy: {
	    type: 'ajax',
	    url: CRMax.globals.baseDirectory + 'Controllers/FundoPgbl/List.ashx',
	    reader: {
	        type: 'json',
	        root: 'Collection',
	        successProperty: 'success'
	    }
	}

});
Ext.define('CRMax.store.FundosCotista', {
	extend: 'Ext.data.Store',
	fields: ['IdCarteira', 'Apelido', 'Nome', 'Categoria', 'GrauRisco', 'RetornoMesCorrente', 'RetornoUltimoMes', 'Retorno12Meses', 'RetornoAno', 'ValorMinimoInicial', 'ValorMinimoAplicacao',
	'ValorMinimoResgate', 'ValorMinimoSaldo', 'DiasCotizacaoAplicacao', 'DiasCotizacaoResgate', 'DiasLiquidacaoAplicacao', 'DiasLiquidacaoResgate',
	'HorarioFim', 'TemPermissaoAplicar', 'LinkLamina', 'LinkTermoAdesao', 'LinkProspecto', 'LinkRegulamento'],
    alias: 'store.fundoscotista',
	autoLoad: true,

	proxy: {
	    type: 'ajax',
	    url: CRMax.globals.baseDirectory + 'Controllers/FundoCotista/List.ashx',
	    reader: {
	        type: 'json',
	        root: 'Collection',
	        successProperty: 'success'
	    }
	}

});
Ext.define('CRMax.store.FundosCotistaOperacao', {
	extend: 'Ext.data.Store',
	fields: ['IdCarteira', 'Nome', 'Categoria', 'GrauRisco', 'RetornoMesCorrente', 'RetornoUltimoMes', 'Retorno12Meses', 'RetornoAno', 'ValorMinimoInicial', 'ValorMinimoAplicacao',
	'ValorMinimoResgate', 'ValorMinimoSaldo', 'DiasCotizacaoAplicacao', 'DiasCotizacaoResgate', 'DiasLiquidacaoAplicacao', 'DiasLiquidacaoResgate',
	'HorarioFim', 'TemPermissaoAplicar', 'LinkLamina', 'LinkTermoAdesao', 'LinkProspecto', 'LinkRegulamento'],
    alias: 'store.fundoscotistaoperacao',
	autoLoad: true,

	proxy: {
	    type: 'ajax',
	    url: CRMax.globals.baseDirectory + 'Controllers/FundoCotistaOperacao/List.ashx',
	    reader: {
	        type: 'json',
	        root: 'Collection',
	        successProperty: 'success'
	    }
	}

});
Ext.define('CRMax.store.Extratos', {
	extend: 'Ext.data.Store',
	fields: ['NomeCarteira', 'ValorBruto', 'ValorLiquido', 'IdCarteira', 'DataHistorico', 'Quantidade', 'CotaDia', 'ValorIR', 'ValorIOF', 'Historico'],
	groupField: 'NomeCarteira',
    
    remoteFilter: true,
	autoLoad: true,
	proxy: {
        type: 'ajax',
        url: CRMax.globals.baseDirectory + 'Controllers/ExtratoCotista/List.ashx',
        reader: {
            type: 'json',
            root: 'Collection',
            successProperty: 'success'
        }
    }
	
});
Ext.define('CRMax.store.OrdensPgbl', {
	extend: 'Ext.data.Store',
	fields: ['IdOperacao', 'FormaLiquidacao', 'ContaCorrente', 'TipoOperacaoString', 'IdCotista', 'NomeCotista', 'NomeCarteira', 'IdOperacaoMae', 'IdCarteira', 'TipoOperacao', 'Status', 'StatusProcessamento', {name: 'DataOperacao', type: 'date', dateFormat: 'd/m/y'}, 'Valor', 'Saldo', 'IsDistribuida'],
	remoteFilter: true,
	autoLoad: true,
	proxy: {
        type: 'ajax',
        url: CRMax.globals.baseDirectory + 'Controllers/OrdemPgbl/List.ashx',
        reader: {
            type: 'json',
            root: 'Collection',
            successProperty: 'success'
        }
    }
});
Ext.define('CRMax.store.OrdensTesouraria', {
	extend: 'Ext.data.Store',
	fields: ['IdOperacao', 'FormaLiquidacao', 'NomeCotista', 'IdCotista', 'ContaCorrente', 'TipoOperacaoString', 'NomeCarteira', 'IdOperacaoMae', 'IdCarteira', 'TipoOperacao', 'Status', 'StatusProcessamento', {name: 'DataOperacao', type: 'date', dateFormat: 'd/m/y'}, 'Valor', 'Saldo', 'IsDistribuida', 'Observacao'],
	remoteFilter: true,
	autoLoad: true,
	proxy: {
        type: 'ajax',
        url: CRMax.globals.baseDirectory + 'Controllers/OrdemTesouraria/List.ashx',
        reader: {
            type: 'json',
            root: 'Collection',
            successProperty: 'success'
        }
    }
});
Ext.define('CRMax.store.OrdensDistribuidas', {
	extend: 'Ext.data.Store',
	fields: ['IdCarteira','NomeCarteira', 'Valor', 'TipoOperacaoString', 'FormaLiquidacao', 'ContaCorrente'],
	autoLoad: false,
	remoteFilter: true,
	proxy: {
        type: 'ajax',
        url: CRMax.globals.baseDirectory + 'Controllers/OrdemTesouraria/Distribute/List.ashx',
        reader: {
            type: 'json',
            root: 'Collection',
            successProperty: 'success'
        }
    }
});
Ext.define('CRMax.store.Operacoes', {
	extend: 'Ext.data.Store',
	fields: ['IdOperacao', 'TipoOperacaoString', 'NomeCarteira', 'StatusProcessamento', 'StatusCotizacao',
	{name: 'DataOperacao', type: 'date', dateFormat: 'd/m/y'}, 'ValorBruto', 'ValorLiquido', 'Valor', 'FormaLiquidacao', 'ContaCorrente', 'NomeCotista'],
	remoteFilter: true,
	autoLoad: true,
	proxy: {
        type: 'ajax',
        url: CRMax.globals.baseDirectory + 'Controllers/OperacaoCotista/List.ashx',
        reader: {
            type: 'json',
            root: 'Collection',
            successProperty: 'success'
        }
    }
	
});
/*Ext.define('CRMax.store.Documentos', {
	extend: 'Ext.data.Store',
	fields: ['Nome', 'Path'],
	data: [
	    {Nome: 'Documento 1'},
	    {Nome: 'Documento 2'}]
});*/


Ext.define('CRMax.store.Documentos', {
	extend: 'Ext.data.Store',
	fields: ['Nome', 'Tipo', 'Path'],

	autoLoad: true,

	proxy: {
	    type: 'ajax',
	    url: CRMax.globals.baseDirectory + 'Controllers/Documento/List.ashx',
	    reader: {
	        type: 'json',
	        root: 'Collection',
	        successProperty: 'success'
	    }
	}

});
CRMax.tmp = [];

if(CRMax.globals.userInfo.IdCotista == -1 && CRMax.globals.isTesouraria){
    if(CRMax.globals.userInfo.Permissoes.PodeDistribuir){
        CRMax.tmp.push(
            { description: "Distribuição", iconCls: 'navigator-icon-home', id: 'distribuicao', leaf: true }
        );
    }

    if(CRMax.globals.userInfo.Permissoes.PodeAprovar){
        CRMax.tmp.push(
            { description: "Aprovação", iconCls: 'navigator-icon-home', id: 'aprovacao', leaf: true }
        );
    }
}

else if (CRMax.globals.userInfo.IdCotista == -1 && !CRMax.globals.isTesouraria){
    CRMax.tmp.push(
            { description: "Movimentações", iconCls: 'navigator-icon-home', id: 'operacoes', leaf: true }
    );
}

else{

    var opcoesTesouraria = [];
    
    opcoesTesouraria.push({ description: "Saldos", iconCls: 'navigator-icon-home', id: 'home', leaf: true });
    opcoesTesouraria.push({ description: "Ordens", iconCls: 'navigator-icon-home', id: 'ordens', leaf: true });
    if(CRMax.globals.userInfo.Permissoes.AcessoPGBL){
        opcoesTesouraria.push({ description: "PGBL", iconCls: 'navigator-icon-home', id: 'ordenspgbl', leaf: true });
    }
    opcoesTesouraria.push({ description: "Operações", iconCls: 'navigator-icon-home', id: 'operacoes', leaf: true });

    CRMax.tmp = CRMax.globals.isTesouraria ? 
        opcoesTesouraria
    :
    [
            { description: "Meus Investimentos", iconCls: 'navigator-icon-home', id: 'home', leaf: true },
            { description: "Movimentações", iconCls: 'navigator-icon-home', id: 'operacoes', leaf: true },
            { description: "Extrato", iconCls: 'navigator-icon-home', id: 'extrato', leaf: true },
            { description: "Perfil de Investidor", iconCls: 'navigator-icon-contacts', id: 'suitability', leaf: true },
            { description: "Documentos", iconCls: 'navigator-icon-contacts', id: 'documentos', leaf: true }
            
        ];

    if(CRMax.globals.userInfo.Permissoes.AcessoSeguranca){
        CRMax.tmp.push({ description: "Segurança", iconCls: 'navigator-icon-contacts', id: 'seguranca', leaf: true });
    }
}


Ext.define('CRMax.store.MainNavigator', {
    extend: 'Ext.data.Store',
    fields: ['description', 'iconCls', 'id'],
    data: CRMax.tmp
        
    
});
Ext.define('CRMax.view.Settings', {
    extend: 'Ext.Panel',
    alias: 'widget.settings',
    border: false,
    layout: 'border',
    id: 'settings-panel',
    initComponent: function () {

        var settingsStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'caption'],
            data: [
                { id: 'users', caption: 'Usuários' },
                { id: 'deal-categories', caption: 'Categorias de Negócio' },
                { id: 'task-categories', caption: 'Categorias de Tarefa' },
                { id: 'custom-fields', caption: 'Campos Personalizados' }
            ]
        });

        var dataview = {
            xtype: 'dataview',
            id: 'settings-panel-dataview',
            store: settingsStore,
            region: 'west',
            border: false,
            width: 250,
            selectedItemCls: 'active-settings-entry',
            tpl: [
                '<h3>Gerenciar</h3>',
                '<ul>',
                    '<tpl for=".">',
                        '<li class="settings-entry">{caption}</li>',
                    '</tpl>',
                '</ul>'
            ],
            itemSelector: 'li.settings-entry'
        };

        this.items = [
                    dataview, {
                        itemId: 'cards',
                        layout: 'card',
                        region: 'center'
                    }
                ];

        this.callParent(arguments);
    }
});
Ext.define('CRMax.view.HomePanel', {
    extend: 'Ext.Panel',
    alias: 'widget.homepanel',
    border: false,
    autoScroll: true,
    //layout: 'border',
    //layout: 'column',
    cls: 'home-panel',
    tplAplicacoesPendentes: 'Aplicações pendentes de cotização:<span style="padding-left: 20px;">R$ {0}</span>',
    initComponent: function () {
       
       var fundoList = new CRMax.view.fundo.List();
        
       var rightColumn = {
            title: 'Fundos disponíveis para Aplicação',
            //columnWidth: 1,
            //width: 800,
            //minWidth: 200,
            //height: 500,
            //bodyStyle: 'padding: 20px 10px; background-color: #fff;',
            region: 'south',
            border: false,
            //layout: 'fit',
            autoScroll: true,
            items: fundoList
        };

        var posicaoList = new CRMax.view.posicao.List();
        
        var posicaoDetalheList = new CRMax.view.posicao.detalhe.List();

        var winPosicaoDetalhe = Ext.create('Ext.window.Window', {
            title: 'Posição Detalhada',
            height: 400,
            width: 900,
            y: CRMax.application.inIframe() ? 50 :undefined,
            modal: true,
            border: false,
            resizable: false,
            layout: 'fit',
            closeAction: 'hide',
            items: posicaoDetalheList,
            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'Fechar',
                handler: function(){
					winPosicaoDetalhe.close();}
            }]
        });

		
        
        var aplicacoesPendentesContainer = {
            id : 'aplicacoes-pendentes-container',
            html: Ext.String.format(this.tplAplicacoesPendentes, Ext.util.Format.number(CRMax.globals.aplicacoesPendentes,'0,000.00')),
            style: 'margin-top: 20px;',
            bodyStyle: 'padding: 7px;background-color: #efefef;'
        };
        
        var disclaimerContainer = {
            html: CRMax.globals.disclaimerPosicao,
            border: false,
            bodyStyle: 'padding: 10px'
        };
        
        var leftColumn = {
            //columnWidth: .3,
            //width: 300,
            //columnWidth: .5,
            //width: 500,
            //maxWidth: 500,
            title: 'Saldos Fundos',
            bodyStyle: 'padding-bottom: 30px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: [posicaoList, aplicacoesPendentesContainer, disclaimerContainer]
        };
        
        CRMax.application.on('detalheclick', function(record){
            var idCarteira = record.get('IdCarteira');
            var filters = [];
            
            filters.push({
                property: 'IdCarteira',
                value: idCarteira
            });
            
            posicaoDetalheList.filter(filters);
            var dataPosicao = Ext.Date.format(record.get('DataDia'), "j/n/Y");
            winPosicaoDetalhe.setTitle('<span style="font-weight: normal">Posição Detalhada em ' + dataPosicao + 
                ' :</span> ' + record.get('NomeCarteira'));
            winPosicaoDetalhe.show();
        });
        
        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
        
        CRMax.application.on('operacaoupdated', this.updateAplicacoesPendentes, this);
    },
    
    updateAplicacoesPendentes: function(){
        var container = Ext.getCmp('aplicacoes-pendentes-container');
        
        Ext.Ajax.request({
            scope: this,
            url: CRMax.globals.baseDirectory + 'controllers/application/loadglobals.ashx',
                success: function(response) {
                    //Exemplo texto parsear ->> "aplicacoesPendentes": 123379894652746.30,
                    var propertyName = "aplicacoesPendentes";
                    var posicaoInicial = response.responseText.indexOf(propertyName) + propertyName.length;
                    var posicaoFinal = response.responseText.indexOf(",", posicaoInicial);
                    var aplicacoesPendentes = parseFloat(response.responseText.substring((posicaoInicial + 3), (posicaoFinal)));
                    container.update(Ext.String.format(this.tplAplicacoesPendentes, Ext.util.Format.number(aplicacoesPendentes,'0,000.00')));
                }
        });
        
        
    }

});

Ext.define('CRMax.view.OperacoesPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.operacoespanel',
    border: false,
    layout: 'border',
    cls: 'home-panel',
    initComponent: function () {
        
       var operacaoFilter = new CRMax.view.operacao.Filter({listId: 'operacao-list'}); 
       
       var disclaimerContainer = {
            html: CRMax.globals.disclaimerOperacao,
            border: false,
            bodyStyle: 'margin-top: 10px; padding: 10px; background-color: transparent;'
        };
       
       var rightColumn = {
            //title: 'Pesquisar Operações',
            width: 360,
            bodyStyle: 'background-color: #fafafa;',
            region: 'east',
            border: false,
            autoScroll: true,
            items: [operacaoFilter, disclaimerContainer]
       };

        var operacaoList = new CRMax.view.operacao.List({id: 'operacao-list'});
        
        var leftColumn = {
            columnWidth: 1,
            title: 'Histórico de Movimentações',
            //bodyStyle: 'padding: 20px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: [operacaoList]
        };
        
        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
    }

});
Ext.define('CRMax.view.OrdensPgblPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.ordenspgblpanel',
    border: false,
    layout: 'border',
    cls: 'home-panel',
    initComponent: function () {
        
       var ordemFilter = new CRMax.view.ordem.pgbl.Filter({listId: 'ordem-pgbl-list'}); 
       var rightColumn = {
            width: 360,
            bodyStyle: 'background-color: #fafafa;',
            region: 'east',
            border: false,
            autoScroll: true,
            items: [ordemFilter]
       };


        var ordemList = new CRMax.view.ordem.pgbl.List({id: 'ordem-pgbl-list'});
        
        var leftColumn = {
            columnWidth: 1,
            title: 'Ordens',
            //bodyStyle: 'padding: 20px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: [ordemList]
        };
        
        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
    }

});
Ext.define('CRMax.view.DistribuicaoPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.distribuicaopanel',
    border: false,
    layout: 'border',
    cls: 'home-panel',
    initComponent: function () {
        
       var distribuicaoFilter = new CRMax.view.ordem.Filter({listId: 'distribuicao-list'}); 
       var rightColumn = {
            width: 360,
            bodyStyle: 'background-color: #fafafa;',
            region: 'east',
            border: false,
            autoScroll: true,
            items: [distribuicaoFilter]
       };


        var distribuicaoList = new CRMax.view.ordem.List({id: 'distribuicao-list', multiCotista: true, actionStatus: 'distribuicao'});
        
        var leftColumn = {
            columnWidth: 1,
            title: 'Distribuição',
            //bodyStyle: 'padding: 20px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: [distribuicaoList]
        };
        
        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
    }

});
Ext.define('CRMax.view.AprovacaoPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.aprovacaopanel',
    border: false,
    layout: 'border',
    cls: 'home-panel',
    initComponent: function () {
        
       var aprovacaoFilter = new CRMax.view.ordem.Filter({listId: 'aprovacao-list'}); 
       var rightColumn = {
            width: 360,
            bodyStyle: 'background-color: #fafafa;',
            region: 'east',
            border: false,
            autoScroll: true,
            items: [aprovacaoFilter]
       };


        var aprovacaoList = new CRMax.view.ordem.List({id: 'aprovacao-list', multiCotista: true, actionStatus: 'aprovacao'});
        
        var leftColumn = {
            columnWidth: 1,
            title: 'Aprovação',
            //bodyStyle: 'padding: 20px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: [aprovacaoList]
        };
        
        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
    }

});
Ext.define('CRMax.view.OrdensPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.ordenspanel',
    border: false,
    layout: 'border',
    cls: 'home-panel',
    initComponent: function () {
        
       var ordemFilter = new CRMax.view.ordem.Filter({listId: 'ordem-list'}); 
       var rightColumn = {
            width: 360,
            bodyStyle: 'background-color: #fafafa;',
            region: 'east',
            border: false,
            autoScroll: true,
            items: [ordemFilter]
       };


        var ordemList = new CRMax.view.ordem.List({id: 'ordem-list'});
        
        var leftColumn = {
            columnWidth: 1,
            title: 'Ordens',
            //bodyStyle: 'padding: 20px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: [ordemList]
        };
        
        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
    }

});
Ext.define('CRMax.view.SegurancaPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.segurancapanel',
    border: false,
    layout: 'border',
    cls: 'home-panel',
    initComponent: function() {
        this.gridIframe = new Ext.Component({
            autoEl: {
                tag: 'iframe',
                width: '100%',
                height: 300,
                style: 'border: 0',
                src: CRMax.globals.pathHistoricoLog,
                scrolling: 'no'
            }
        });

        this.tbar = [{
            text: 'Email BackOffice',
            baseCls: '',
            cls: 'btn-aplicacao',
            handler: function() {
                Ext.create('Ext.window.Window', {
                    title: 'Email Back-office',
                    height: 200,
                    width: 400,
                    layout: 'fit',
                    items: { // Let's put an empty grid in just to illustrate fit layout
                        xtype: 'form',
                        bodyStyle: 'padding: 10px',
                        border: false,
                        items: [{
                            xtype: 'textarea',
                            width: 370,
                            name: 'EmailBackOffice',
                            height: 60,
                            fieldLabel: 'Email Back-office',
                            value: CRMax.globals.emailBackOffice
                        }],
                        buttons: [{
                            baseCls: '',
                            cls: 'btn',
                            text: 'Cancelar',
                            scope: this,
                            handler: function(btn) {
                                btn.up('window').close();
                            }
                        }, {
                            text: 'Gravar',
                            baseCls: '',
                            scope: this,
                            cls: 'btn btn-inverse',
                            handler: function(btn) {
                                var emailBackOffice = btn.up('form').getValues().EmailBackOffice;
                                
                                var params = {};
                                
                                if(CRMax.globals.isTesouraria){
                                    params.emailBackOfficeInstitucional = emailBackOffice;
                                    params.isTesouraria = true;
                                }else{
                                    params.emailBackOffice = emailBackOffice;
                                    params.isTesouraria = false;
                                }
                                
                                Ext.Ajax.request({
                                    url: CRMax.globals.baseDirectory + 'controllers/seguranca/save.ashx',
                                    params: params,
                                    success: function(response) {
                                        var msgSucesso = 'Atualização realizada com sucesso';
                                        Ext.Msg.alert('Aviso',msgSucesso);    
                                        
//                                        CRMax.application.notificationWindow.update(msgSucesso);
//                                        CRMax.application.notificationWindow.show();
                                        btn.up('window').close();
                                        CRMax.globals.emailBackOffice = emailBackOffice;
                                    }
                                });
                            }
                        }]
                    }
                }).show();
            }
        },
        
        {
            text: 'Disclaimer',
            baseCls: '',
            cls: 'btn-aplicacao',
            handler: function() {
                Ext.create('Ext.window.Window', {
                    title: 'Disclaimer',
                    height: 400,
                    width: 500,
                    layout: 'fit',
                    items: { // Let's put an empty grid in just to illustrate fit layout
                        xtype: 'form',
                        bodyStyle: 'padding: 10px',
                        border: false,
                        items: [{
                            xtype: 'textarea',
                            width: 470,
                            name: 'DisclaimerPosicao',
                            height: 100,
                            fieldLabel: 'Texto Saldo',
                            value: CRMax.globals.disclaimerPosicao
                        },{
                            xtype: 'textarea',
                            width: 470,
                            name: 'DisclaimerOperacao',
                            height: 100,
                            fieldLabel: 'Texto Operações',
                            value: CRMax.globals.disclaimerOperacao
                        }],
                        buttons: [{
                            baseCls: '',
                            cls: 'btn',
                            text: 'Cancelar',
                            scope: this,
                            handler: function(btn) {
                                btn.up('window').close();
                            }
                        }, {
                            text: 'Gravar',
                            baseCls: '',
                            scope: this,
                            cls: 'btn btn-inverse',
                            handler: function(btn) {
                                var formValues = btn.up('form').getValues();
                                var disclaimerPosicao = formValues.DisclaimerPosicao;
                                var disclaimerOperacao =  formValues.DisclaimerOperacao;
                                
                                var params = {
                                    operacao: 'update-disclaimer',
                                    disclaimerPosicao: disclaimerPosicao,
                                    disclaimerOperacao: disclaimerOperacao
                                };
                                
                                Ext.Ajax.request({
                                    url: CRMax.globals.baseDirectory + 'controllers/seguranca/save.ashx',
                                    params: params,
                                    success: function(response) {
                                        var msgSucesso = 'Atualização realizada com sucesso';
                                        Ext.Msg.alert('Aviso',msgSucesso);    
                                        
                                        btn.up('window').close();
                                        CRMax.globals.disclaimerPosicao = disclaimerPosicao;
                                        CRMax.globals.disclaimerOperacao = disclaimerOperacao;
                                    }
                                });
                            }
                        }]
                    }
                }).show();
            }
        }
        ];

        this.items = this.gridIframe;

        this.callParent(arguments);
    }

});
Ext.define('CRMax.view.ExtratoPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.extratopanel',
    border: false,
    layout: 'border',
    cls: 'home-panel',
    initComponent: function () {
        
       var extratoFilter = new CRMax.view.extrato.Filter({listId: 'extrato-list'}); 
       var rightColumn = {
            //title: 'Pesquisar Operações',
            width: 200,
            bodyStyle: 'background-color: #fafafa;',
            region: 'east',
            border: false,
            autoScroll: true,
            items: [extratoFilter]
       };

        var extratoList = new CRMax.view.extrato.List({id: 'extrato-list'});
        
        var leftColumn = {
            columnWidth: 1,
            title: 'Extrato',
            //bodyStyle: 'padding: 20px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: [extratoList]
        };
        
        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
    }

});
Ext.define('CRMax.view.Documentos', {
    extend: 'Ext.Panel',
    alias: 'widget.documentoslist',
    border: false,
    title: 'Documentos',
    //cls: 'latest-activity-panel',
    initComponent: function () {
        
       /* var buttonEnviarDocumento = {
            xtype: 'button',
            text: 'Enviar documento',
            scope: this,
            handler: this.showUploadWindow
        };
        
        this.tbar = [
            buttonEnviarDocumento
        ];*/
        
        var documentoTpl = new Ext.XTemplate(
        '<tpl for=".">',
        
        '<div class="MediaItemContainer" index="0">',
            '<div class="MediaItem File" style="position: relative;">',
                '<div class="ThumbContainer" title="Download" style="height: 160px; width: 120px; background-color: rgb(210, 71, 38);">',
                    '<img class="Thumb" src="content/themes/base/images/xml_57.png" alt="Download" title="Download" style="padding-top: 52px;"> ',
                '</div>',
                '<div class="FileName" title="Download">',
                    '<span class="TextSizeSmall">{Nome?}</span>',
                '</div>',
                '<div class="Overlay" title="Download">',
                    '<span class="FloatRight TextSizeSmall">Download</span>',
                    '<a href="#" class="FloatRight">',
                        '<img src="content/themes/base/images/liveview_download.png">',
                    '</a>',
                '</div>',
            '</div>',
        '</div>',
            
        '</tpl>'
        );
        
        var dataview = Ext.create('Ext.view.View', {
            store: 'Documentos',
            tpl: documentoTpl,
            itemSelector: 'div.MediaItemContainer',
            emptyText: 'Nenhum documento encontrado',
            listeners: {
                itemclick: this.onItemClick,
                scope: this
            }
        });
        
        this.items = [dataview];
        
        this.callParent(arguments);
    },
    
    onItemClick: function(dataview, record, item){
        var path = encodeURIComponent(record.get('Path'));
        window.open(CRMax.globals.baseDirectory + 'controllers/documento/download.ashx?path=' + path);
    },
    showUploadWindow: function(){
        this.uploadWindow = Ext.create('Ext.window.Window', {
            title: 'Enviar documento',
            height: 200,
            width: 400,
            layout: 'fit',
            border: false,
            url: CRMax.globals.baseDirectory + 'controllers/documento/upload.ashx',
            items:{
                xtype: 'form',
                closable: true,
                header: false,
                border: false,
                bodyPadding: 10,
            
                items: {  // Let's put an empty grid in just to illustrate fit layout
                    xtype: 'filefield',
                    name: 'file',
                    fieldLabel: 'Arquivo',
                    labelWidth: 50,
                    msgTarget: 'side',
                    allowBlank: false,
                    anchor: '100%',
                    buttonText: 'Selecione o arquivo...'
                }
            },
            buttons: [
            {text: 'Enviar', scope: this, handler: this.submitUpload}, 
            
            {text: 'Cancelar', scope: this, handler: function(){this.uploadWindow.close();}}]
        });
        this.uploadWindow.show();
    },
    
   submitUpload: function(){
        var form = this.uploadWindow.down('form');
        var basicForm = form.getForm();
        
        basicForm.submit({
            url: CRMax.globals.baseDirectory + 'Controllers/Documento/Upload.ashx',
            waitMsg: 'Processando...',
            waitTitle: 'Aguarde',
            scope: this,
            success: function (fp, o) {
                
                Ext.Msg.alert('Aviso','Arquivo enviado com sucesso!'); 
                
                //CRMax.application.notificationWindow.update('Arquivo enviado com sucesso!');
                //CRMax.application.notificationWindow.show();
                this.uploadWindow.close();
            },
            failure: function(response){
                if (!(response && response.responseText && (response.responseText.indexOf('Convert.ToInt32((Convert.ToString(context.Session') > 0))) {
                    Ext.Msg.alert('Aviso','Ocorreu um erro durante a gravação. Por favor, tente novamente.');    
                }
            }
        });
   } 
});
Ext.define('CRMax.view.LatestActivityPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.latestactivitypanel',
    border: false,
    layout: 'border',
    cls: 'latest-activity-panel',
    initComponent: function () {

        var rightColumn = {
            title: 'Operações',
            width: 560,
            bodyStyle: 'padding: 20px 10px; background-color: #fafafa;',
            region: 'east',
            border: false,
            autoScroll: true,
            items: new CRMax.view.operacao.List()
        };

        var leftColumn = {
            columnWidth: 1,
            title: 'Extrato',
            bodyStyle: 'padding: 20px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: new CRMax.view.posicao.List()
        };


        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
    }
});
Ext.define('CRMax.view.WelcomePanel', {
    extend: 'Ext.Panel',
    alias: 'widget.welcomepanel',
    title: 'Bem vindo',
    border: false,
    cls: 'welcome-panel',
    autoScroll: true,
    bodyStyle: 'margin-top: 3px;',
    
    initComponent: function () {

        var html = [
            '<h2>Bem vindo ao Linkloud!</h2>',
            '<div class="step-one">',
                '<h3>Primeiro passo: <span class="link link-add-contacts">Adicione os seus contatos</span></h3>',
                '<p>O Linkloud é focado em pessoas, portanto esta é a melhor forma de começar a usá-lo</p>',
            '</div>',

            '<div>',
                '<h3>O Linkloud ajuda a manter o relacionamento do seu negócio em dia</h3>',
            '</div>',

            '<div class="features">',
                '<ul>',
                    '<li>',
                        '<img style="width: 102; height: 129px; margin-left: 30px;" src="' + CRMax.globals.imagesPath + '/address_book.png" />',
                        '<h4>Agenda Compartilhada</h2>',
                        '<p>Guarde e compartilhe informações sobre clientes, fornecedores, <i>leads</i> e outros</p>',
                    '</li>',

                    '<li>',
                        '<img style="width: 145; height: 129px; margin-left: 0px;" src="' + CRMax.globals.imagesPath + '/communication.png" />',
                        '<h4>Histórico de Contato</h2>',
                        '<p>Registre e recupere anotações sobre ligações, reuniões, emails, conversas, etc.</p>',
                    '</li>',

                    '<li>',
                        '<img style="width: 106; height: 129px; margin-left: 28px;" src="' + CRMax.globals.imagesPath + '/pencil_clipboard.png" />',
                        '<h4>Tarefas e Lembretes</h2>',
                        '<p>Organize lembretes por e-mail e SMS para garantir que o trabalho está sendo feito</p>',
                    '</li>',
                '</ul>',
                '<div class="clearfix"></div>',
            '</div>',

            '<div class="step-other">',
                '<h5>Você também pode...</h5>',
                '<h3><span class="link link-add-users">Convidar outras pessoas para usarem o Linkloud com você</span></h3>',
                '<p>Crie contas para seus colaboradores para que vocês possam usar o Linkloud juntos</p>',
            '</div>'
        ];

        this.items = [
                    {
                        border: false,
                        html: html.join('')
                    }
                ];

        this.callParent(arguments);
    },

    afterRender: function () {
        this.body.on('click', this.onBodyClick, this);
        this.callParent(arguments);
    },

    onBodyClick: function (eventObj, htmlElement, eventOptions) {
        var targetEl = Ext.get(eventObj.target);

        if (!targetEl.hasCls('link')) {
            return;
        }

        if (targetEl.hasCls('link-add-users')) {
            //CRMax.application.getController('People').onCreatePerson();
            //Ext.History.add('/settings');
            CRMax.application.getController('MainNavigator').selectNode('settings');
        } else if (targetEl.hasCls('link-add-contacts')) {
            CRMax.application.getController('People').onButtonCreatePersonClick();
        }

    }
});
Ext.define('CRMax.view.MainNavigator', {
    extend: 'Ext.view.View',
    alias: 'widget.mainnavigator',
    id: 'MainNavigator',
    //width: 60,
    //height: 150,
    lines: false,
    rootVisible: false,
    overItemCls: 'menu-nav-option-over',
    trackOver: true,

    initComponent: function () {
        this.store = Ext.create('CRMax.store.MainNavigator');
        
        this.tpl = new Ext.XTemplate(
            '<ul class="menu-nav">',
            '<tpl for=".">',
                '<li class="menu-nav-option">',
                    '{description}',
                '</li>',
            '</tpl>',
            '</ul>'
        );
        
        this.itemSelector = 'li.menu-nav-option';
        
        this.callParent(arguments);
    }
});
Ext.define('CRMax.view.MainContentPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.maincontentpanel',
    layout: 'card',

    addComponent: function (componentConfig, removeBeforeAdding) {

        var cards = this.getLayout().getLayoutItems();

        //try to find component
        var component = null;
        for (var cardCount = 0; cardCount < cards.length; cardCount++) {
            if (cards[cardCount].itemId == componentConfig.itemId) {
                component = cards[cardCount];

                if (removeBeforeAdding === true) {
                    component.destroy();
                    component = null;
                }
                break;
            }
        }

        if (!component) {
            component = this.add(componentConfig);
        }

        this.getLayout().setActiveItem(componentConfig.itemId);
        return component;
    },

    getActivePanel: function () {
        return this.getLayout().getActiveItem();
    }

});
Ext.define('CRMax.view.Viewport', {
    extend: 'Ext.container.Viewport',
    layout: 'border',
    requires: ['CRMax.view.MainNavigator', 'CRMax.view.MainContentPanel', 'Ext.ux.form.SearchField', 'Ext.ux.form.HoverButton'],
    mainContentPanelItemId: 'main-content-panel',
    mainNavigatorPanelItemId: 'MainNavigator',

    getMainNavigatorPanel: function () {
        return this.mainNavigatorPanel || (this.mainNavigatorPanel = this.getComponent(this.mainNavigatorPanelItemId));

    },

    getMainContentPanel: function () {
        return this.mainContentPanel || (this.mainContentPanel = this.getComponent(this.mainContentPanelItemId));

    },

    initComponent: function () {
    
        var navigationBar = {
            border: false,
            itemId: this.mainNavigatorPanelItemId,
            bodyCls: 'mainnavigator-panel-body',
            //region: 'west',
            xtype: 'mainnavigator'
        };
        
        var topBar = {
            region: 'north',
            cls: 'viewport-north-panel-body',
            xtype: 'panel',
            bodyStyle: 'background-color: transparent; margin-top: 15px;',
            height: 65,
            border: false,
            items: [
                {
                    xtype: 'component',
                    id: 'logo-cliente',
                    style: 'float: left; margin-right: 30px;',
                    html: '<img style="padding-top: 2px;" src="' + CRMax.globals.baseDirectory + 'content/themes/base/images/logo_cliente.gif" />'
                },
                navigationBar, 
                {
                    xtype: 'button',
                    id: 'button-logoff',
                    hidden: true,
                    iconCls: 'icon-logoff',
                    text: 'Sair'
                }, {
                    xtype: 'button',
                    iconCls: 'icon-user',
                    id: 'button-edit-user',
                    text: CRMax.globals.userInfo.Login
        }]};
        
        
    
        this.items = [
            topBar
            /*{
            region: 'north',
            height: 60,
            border: false,
            bodyCls: 'viewport-north-panel-body',
            items: [
                topBar
            ]
        }*/, {
            xtype: 'maincontentpanel',
            itemId: this.mainContentPanelItemId,
            region: 'center',
            border: false
        }
        ];

        this.callParent();
    }
});
/*MENSAGENS SUITABILITY PORTAL
IdSuitabilityEventos	Descricao
1	Apuração de novo perfil de Investidor (Portal)
2	Declaração sobre as informações prestadas (Portal)
4	Aceite do perfil apurado (Portal)
6	Aceite do tipo de dispensa (Portal)
8	Recusa do preenchimento do formulário (Portal)
9	Questionário não preenchido (Portal)
10	Atualizar perfil (Portal)
11	Inexistência de Perfil (Portal)
12	Perfil desatualizado (Portal)
14	Desenquadramento - Alteração de perfil (Portal)
15	Termo de Ciência (Portal)
17	Termo de Ciência de Risco e Adesão ao Fundo (Portal)
19	Termo de Inadequação (Portal)
*/
Ext.define('CRMax.view.suitability.Edit', {
    extend: 'Ext.form.Panel',
    alias: 'widget.suitabilityedit',
    title: 'Editar Perfil',
    autoScroll: true,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    border: false,
    cls: 'edit-form',

    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'top'
    },

    initComponent: function() {

        var formularioPreenchido = CRMax.globals.perfilCotista.Perfil !== null && CRMax.globals.perfilCotista.Perfil !== 'Indefinido.';

        this.messageFormularioPreenchido = '<p style="margin: 15px; 0">Formulário preenchido com Sucesso!</p>';
        this.messageInstrucoesPreenchimento = '<p style="margin: 15px; 0">Responda a cada uma das cinco questões de múltipla escolha ao lado, escolhendo a alternativa que melhor lhe represente, tendo em vista sua situação financeira, sua experiência em matéria de investimentos, seus objetivos, horizontes de investimento, tolerância a riscos e necessidade de liquidez.</p><p style="margin: 15px; 0;">Assinale apenas uma alternativa em cada questão.</p>';

        var mensagemQuestionario = formularioPreenchido ? this.messageFormularioPreenchido : this.messageInstrucoesPreenchimento;


        var buttonDispensar = {
            xtype: 'button',
            text: 'Dispensar',
            baseCls: '',
            itemId: 'btn-dispensar',
            scope: this,
            cls: 'btn btn-inverse',
            handler: this.dispensar
        };

        var buttonCancelarDispensa = {
            xtype: 'button',
            text: 'Canc. Dispensa',
            baseCls: '',
            itemId: 'btn-cancelar-dispensa',
            scope: this,
            cls: 'btn btn-inverse',
            handler: this.cancelarDispensa
        };

        var buttonAtualizar = {
            xtype: 'button',
            text: 'Apurar',
            baseCls: '',
            itemId: 'btn-atualizar',
            scope: this,
            cls: 'btn btn-inverse',
            handler: this.submitAtualiza
        };

        var buttonAceitar = {
            xtype: 'button',
            text: 'Aceito',
            baseCls: '',
            itemId: 'btn-aceitar',
            scope: this,
            cls: 'btn btn-inverse',
            handler: this.submitAceito
        };

        var buttonRecusar = {
            xtype: 'button',
            text: 'Recuso',
            baseCls: '',
            itemId: 'btn-recusar',
            scope: this,
            cls: 'btn btn-inverse',
            handler: this.submitRecusa
        };

        var buttons = [buttonDispensar, buttonCancelarDispensa, buttonAtualizar, buttonAceitar, buttonRecusar];

        var infoSuitability = {
            xtype: 'component',
            id: 'info_suitability'
        };

        var leftColumn = {
            border: false,
            cls: 'left-column',
            layout: 'anchor',
            defaults: {
                width: 345
            },
            autoScroll: true,
            width: 390,
            items: [{
                    xtype: 'component',
                    html: '<h1>QUESTIONÁRIO</h1><div id="message-questionario">' + mensagemQuestionario + '</div>'
                },
                infoSuitability, {
                    xtype: 'component',
                    html: '<p style="margin: 0 15px 30px 15px;">Para conhecer os perfis de investidor, clique <span id="link-definicao-perfil" style="cursor:pointer; font-weight: bold;">aqui</span>.</p>'
                }
            ]
        };

        var questoesFields = [];

        for (var countQuestao = 0; countQuestao < CRMax.globals.suitability.Questoes.length; countQuestao++) {
            var questao = CRMax.globals.suitability.Questoes[countQuestao];

            var opcoesField = [];

            for (var countOpcao = 0; countOpcao < questao.Opcoes.length; countOpcao++) {
                var opcao = questao.Opcoes[countOpcao];

                opcoesField.push({
                    boxLabel: opcao.Descricao,
                    name: 'OpcaoQuestao-' + questao.IdQuestao,
                    inputValue: opcao.IdOpcao,
                    checked: opcao.Escolhida
                });
            }

            var questaoField = {
                xtype: 'radiogroup',
                fieldLabel: questao.Descricao,
                columns: 1,
                vertical: true,
                items: opcoesField,
                allowBlank: false,
                listeners: {
                    change: function() {
                        this.up('form').checkFormFilled();
                    }
                }
            };

            questoesFields.push(questaoField);
        }

        var mensagemPreenchido = {
            xtype: 'component',
            itemId: 'mensagem-preenchido',
            border: false,
            hidden: true,
            html: ''
        };

        questoesFields.push(mensagemPreenchido);

        var rightColumn = {
            //columnWidth: 1,
            flex: 1,
            itemId: 'right-column',
            autoScroll: true,
            cls: 'right-column',
            border: false,
            items: questoesFields
        }

        this.items = [
            leftColumn, rightColumn
        ];

        this.buttons = buttons;

        this.callParent(arguments);

        this.down('#btn-atualizar').disable();
        this.down('#btn-aceitar').disable();

        if (formularioPreenchido || CRMax.globals.perfilCotista.Dispensado == 'Sim') {
            this.displayMensagemFormularioPreenchido();
        }
        
        this.on('show', this.checkNeedsEditForm, this);
    },

    afterRender: function() {
        this.callParent(arguments);

        this.buildLinks();
        if (Ext.get('link-definicao-perfil')) {
            Ext.get('link-definicao-perfil').on('click', this.displayDefinicaoPerfil, this);
        }

        this.updateInfoSuitability();
        
    },
    
    checkNeedsEditForm: function(){
         if(CRMax.application.editSuitability === true){
            CRMax.application.editSuitability = false;
            this.editForm();
        }
    },


    logMensagem: function(idMensagem, mensagem, resposta) {
        Ext.Ajax.request({
            scope: this,
            url: CRMax.globals.baseDirectory + 'controllers/suitability/logmensagem.ashx?idmensagem=' + idMensagem + '&mensagem=' + mensagem + '&resposta=' + resposta,
            success: function(response) {

            }
        });
    },

    displayDefinicaoPerfil: function() {

        var html = '<div id="holder-definicao-perfil">';
        for (perfil in CRMax.globals.definicaoPerfilInvestidor) {
            var definicao = CRMax.globals.definicaoPerfilInvestidor[perfil];
            html += '<p style="font-size: 14px; margin-bottom: 10px;"><b>' + perfil + '</b>: ' + definicao + '</p>';
        }

        html += '</div>';
        html += '<p style="cursor: pointer; margin: 30px 0 30px 0;"><img style="vertical-align:middle" src="Content/themes/base/images/print.png" /><span style="cursor: pointer;" id="link-print-definicao-perfil">Imprimir</span></p>';


        var component = {
            xtype: 'component',
            html: html
        };
        var form = this;
        var winDefinicao = Ext.create('Ext.window.Window', {
            title: 'Perfis de Investidor',
            height: 390,
            autoScroll: true,
            width: 600,
            modal: true,
            layout: 'fit',
            border: false,
            bodyBorder: false,
            bodyPadding: 20,
            items: [component],

            listeners: {
                afterrender: function() {
                    Ext.get('link-print-definicao-perfil').on('click', form.printDefinicaoPerfil, this);
                }
            },

            buttons: [{
                text: 'Ok',
                baseCls: '',
                scope: this,
                cls: 'btn btn-inverse',
                handler: function() {
                    winDefinicao.close();
                }
            }]
        }).show();

    },

    updateInfoSuitability: function(perfilCotista) {

        if (!perfilCotista) {
            perfilCotista = CRMax.globals.perfilCotista;
        }

        var html = '';

        if (perfilCotista.DtUltAtualizacao && perfilCotista.Perfil != "Indefinido.") {
            html += '<p>Última atualização: <b>' + perfilCotista.DtUltAtualizacao + '</b></p>';
        }
        if (perfilCotista.Validade) {
            html += '<p style="margin-top: 3px;">Validade: <b>' + perfilCotista.Validade + '</b></p>';
        }
        if (perfilCotista.Perfil) {
            html += '<p style="margin-top: 3px;">Perfil apurado: <b>' + perfilCotista.Perfil + '</b></p>';
        }
        if (perfilCotista.PerfilInvestimentos) {
            html += '<p style="margin-top: 3px;">Perfil Investimentos: <b>' + perfilCotista.PerfilInvestimentos + '</b></p>';
        }
        if (perfilCotista.Definicao) {
            html += '<p style="margin-top: 3px;">Descrição Perfil: <b>' + perfilCotista.Definicao + '</b></p>';
        }
        if (perfilCotista.Dispensado) {
            html += '<p style="margin-top: 3px;">Dispensado: <b>' + perfilCotista.Dispensado + '</b></p>';
        }
        if (perfilCotista.TipoDispensa) {
            html += '<p style="margin-top: 3px;">Tipo de Dispensa: <b>' + perfilCotista.TipoDispensa + '</b></p>';
        }
        if (perfilCotista.Alertas) {
            html += '<p style="margin-top: 3px;">Alertas: <b>' + perfilCotista.Alertas + '</b></p>';
        }
        if(perfilCotista.Perfil && perfilCotista.PerfilInvestimentos){
        
            var perfil = perfilCotista.Perfil.toUpperCase();
            var perfilInvestimentos = perfilCotista.PerfilInvestimentos.toUpperCase();
            
            if(perfilCotista.Perfil != "Indefinido." && perfilCotista.PerfilInvestimentos != "Indefinido."){
                var enquadramento = perfil === perfilInvestimentos ? 'ENQUADRADO' : 'DESENQUADRADO';
                html += '<p style="margin-top: 3px;">Perfil de Investidor <b>' + enquadramento + '</b> aos investimentos.</p>';
            }
        }


        if (html != '') {
            html = '<h1 style="margin-top: 50px; margin-bottom: 15px;">PERFIL DO INVESTIDOR</h1><div style="margin: 0 15px 20px 15px">' + html + '</div>';
        }

        var infoSuitability = Ext.get('info_suitability');
        infoSuitability.update(html);


        //Ajustar visibilidade dos botoes
        if (CRMax.globals.perfilCotista.Dispensado == 'Sim') {
            this.down('#btn-cancelar-dispensa').show();
            this.down('#btn-dispensar').hide();
            this.down('#btn-aceitar').hide();
            this.down('#btn-recusar').hide();
            this.down('#btn-atualizar').hide();
        } else {
            this.down('#btn-cancelar-dispensa').hide();
            this.down('#btn-dispensar').show();
            this.down('#btn-aceitar').show();
            this.down('#btn-recusar').show();
            this.down('#btn-atualizar').show();
        }

    },

    checkFormFilled: function(){
        var form = this;
        var basicForm = this.getForm();
        
        if (basicForm.isValid()) {
            form.down('#btn-atualizar').enable();
            form.down('#btn-aceitar').disable();
            form.down('#btn-dispensar').enable();
        }else{
            form.down('#btn-atualizar').disable();
            form.down('#btn-aceitar').disable();
        }
        
        basicForm.clearInvalid();
        
    },

    editForm: function() {
        this.checkFormFilled();
        
        this.displayQuestoes();
        if (Ext.get('message-questionario')) {
            Ext.get('message-questionario').update(this.messageInstrucoesPreenchimento);
        }
        //var btnGravar  =  this.down('#btn-gravar');
        //btnGravar.show();
    },

    buildLinks: function() {
        if (Ext.get('link-print')) {
            Ext.get('link-print').on('click', this.printForm, this);
        }

        if (Ext.get('link-edit')) {
            Ext.get('link-edit').on('click', this.editForm, this);
        }

        if (Ext.get('link-print-termo-recusa')) {
            Ext.get('link-print-termo-recusa').on('click', this.printTermoRecusa, this);
        }
    },

    printTermoRecusa: function() {
        var html = [
            '<!DOCTYPE html>',
            '<html>',
            '<head>',
            '<!--meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /-->',
            '<meta charset="utf-8" />',
            '<title>Termo de Recusa</title>',
            '<link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />',
            '<link href="Content/Site.css" rel="stylesheet" type="text/css" />',
            '<link href="Content/print.css" rel="stylesheet" type="text/css" />',
            '</head>',
            '<body class="x-body">',
            '<div class="header">',
            '<img src="Content/themes/base/images/logo_cliente.gif" alt="Logo" />',
            '<div id="form-title">Termo de Recusa</div>',
            '</div>',
            '<div class="termo" style="padding: 30px;">',
            '{0}',
            '</div>',
            '<script type="text/javascript">window.print()</script>',
            '</body>',
            '</html>'
        ];

        html = Ext.String.format(html.join(''), CRMax.globals.msgsSuitability.msg_8);

        //open up a new printing window, write to it, print it and close
        var win = window.open();

        //document must be open and closed
        win.document.open();
        win.document.write(html);
        win.document.close();
    },

    printDefinicaoPerfil: function() {
        var el = Ext.fly('holder-definicao-perfil'); //.getEl();

        var elInnerHtml = el.dom.innerHTML;

        var html = [
            '<!DOCTYPE html>',
            '<html>',
            '<head>',
            '<!--meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /-->',
            '<meta charset="utf-8" />',
            '<title>Perfis de Investidor</title>',
            '<link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />',
            '<link href="Content/Site.css" rel="stylesheet" type="text/css" />',
            '<link href="Content/print.css" rel="stylesheet" type="text/css" />',
            '</head>',
            '<body class="x-body">',
            '<div class="header">',
            '<img src="Content/themes/base/images/logo_cliente.gif" alt="Logo" />',
            '<div id="form-title">Perfis do Investidor</div>',
            '</div>',
            '<div class="perfis" style="padding: 30px;">',
            '{0}',
            '</div>',
            '<script type="text/javascript">window.print()</script>',
            '</body>',
            '</html>'
        ];

        html = Ext.String.format(html.join(''), elInnerHtml);

        //open up a new printing window, write to it, print it and close
        var win = window.open();

        //document must be open and closed
        win.document.open();
        win.document.write(html);
        win.document.close();

    },


    printForm: function() {
        this.displayQuestoes();

        var el = this.down('#right-column').getEl();

        var elInnerHtml = el.dom.innerHTML;

        var currChar;
        for (var currChar = 0; currChar < elInnerHtml.length; currChar++) {
            var nextChecked = elInnerHtml.toLowerCase().indexOf('x-form-cb-checked', currChar);
            if (nextChecked >= 0) {
                //encontrar button
                var inputTypeButtonString = 'type="button"';
                var nextButton = elInnerHtml.toLowerCase().indexOf(inputTypeButtonString, nextChecked);
                if (nextButton <= 0) {
                    inputTypeButtonString = 'type=button'; //IE 7 !!!!
                    nextButton = elInnerHtml.toLowerCase().indexOf(inputTypeButtonString, nextChecked);
                }


                if (nextButton >= 0) {
                    elInnerHtml = elInnerHtml.substr(0, nextButton + inputTypeButtonString.length) + ' value="X" ' + elInnerHtml.substr(nextButton + inputTypeButtonString.length);
                }

                currChar = nextChecked;

            } else {
                break;
            }
        }


        var html = [
            '<!DOCTYPE html>',
            '<html>',
            '<head>',
            '<!--meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /-->',
            '<meta charset="utf-8" />',
            '<title>Perfil do Investidor</title>',
            '<link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />',
            '<link href="Content/Site.css" rel="stylesheet" type="text/css" />',
            '<link href="Content/print.css" rel="stylesheet" type="text/css" />',
            '</head>',
            '<body class="x-body">',
            '<div class="header">',
            '<img src="Content/themes/base/images/logo_cliente.gif" alt="Logo" />',

            '<div id="form-title">Perfil do Investidor</div>',
            '<div style="padding: 40px 20px 0 20px;">',
            '<p style="width: 400px; float: left">Nome: <b>{1}</b></p>', '<p>CPF/CNPJ: <b>{3}</b></p>',
            '<p style="width: 400px; float: left">Última atualização: <b>{4}</b></p>', '<p>Validade: <b>{5}</b></p>',
            '<p style="margin-top: 10px;">Perfil Apurado: <b>{6}</b> - {7}</p>',
            '</div>',

            '</div>',
            '<div class="questoes">',
            '{0}',
            '</div>',
            '<div class="footer">',
            '<div id="perfil-calculado">',
            'Perfil do Investidor computado com base nas respostas acima: <span class="perfil">{2}</span>',
            '</div>',
            '<div id="assinatura"></div>',
            '<div id="nome-cotista">{1}</div>',
            '</div>',
            '<script type="text/javascript">window.print()</script>',
            '</body>',
            '</html>'
        ];

        var perfilCotista = CRMax.globals.perfilCotista;
        html = Ext.String.format(html.join(''), elInnerHtml, CRMax.globals.userInfo.Login, CRMax.globals.perfilCotista.Perfil, CRMax.globals.userInfo.CPFCNPJ,
            perfilCotista.DtUltAtualizacao, perfilCotista.Validade, perfilCotista.Perfil, perfilCotista.Definicao);

        //open up a new printing window, write to it, print it and close
        var win = window.open();

        //document must be open and closed
        win.document.open();
        win.document.write(html);
        win.document.close();

        this.displayMensagemFormularioPreenchido();
        //win.print();
        //setTimeout('win.print()', 1000);
        /*win.onload = function () {
            alert('print');
            window.print();
        }*/

        /*
        if (Ext.isIE) {
            window.close();
        } else {
            win.close();
        }*/
    },

    displayQuestoes: function() {
        var fields = this.getForm().getFields();
        fields.each(function(field) {
            field.show();
        }, this);
        var mensagemPreenchido = this.down('#mensagem-preenchido');
        mensagemPreenchido.hide();
    },

    displayMensagemFormularioPreenchido: function() {
        var fields = this.getForm().getFields();
        fields.each(function(field) {
            field.hide();
        }, this);
        var mensagemPreenchido = this.down('#mensagem-preenchido');

        var mioloMensagem;
        if (CRMax.globals.perfilCotista.Dispensado == 'Sim') {
            mioloMensagem = '<p style="padding: 20px; font-size: 15px;">Você foi dispensado do preenchimento do perfil de investidor. Para alterar esta condição, selecione o botão [Canc. Dispensa].</p>';
        } else if (CRMax.globals.perfilCotista.Status == 'Recusado') {
            mioloMensagem = '<p style="padding: 20px; font-size: 15px;">Cliente recusou realizar o preenchimento do formulário. </p><p style="margin: 30px 0 20px 0; padding-left: 20px;font-size: 15px;"><img style="vertical-align:middle" src="Content/themes/base/images/print.png" /> Para imprimir o termo de recusa, clique <span id="link-print-termo-recusa" style="cursor:pointer; font-weight: bold;">aqui</span>.</p><p style="padding-left: 20px;font-size: 15px;"><img style="vertical-align:middle" src="Content/themes/base/images/edit.png" /> Para editar suas respostas clique <span id="link-edit" style="cursor:pointer; font-weight: bold;">aqui</span>.</p>';
        } else {
            mioloMensagem = '<p style="padding: 20px; font-size: 15px;">Com base nas respostas preenchidas, seu Perfil de Investidor é: <span class="perfil">' +
                CRMax.globals.perfilCotista.Perfil + '</span>.</p><p style="margin: 30px 0 20px 0; padding-left: 20px;font-size: 15px;"><img style="vertical-align:middle" src="Content/themes/base/images/print.png" /> Para imprimir suas respostas, clique <span id="link-print" style="cursor:pointer; font-weight: bold;">aqui</span>.</p><p style="padding-left: 20px;font-size: 15px;"><img style="vertical-align:middle" src="Content/themes/base/images/edit.png" /> Para editar suas respostas clique <span id="link-edit" style="cursor:pointer; font-weight: bold;">aqui</span>.</p>';
        }

        mensagemPreenchido.update(mioloMensagem);

        mensagemPreenchido.show();

        if (Ext.get('message-questionario')) {
            Ext.get('message-questionario').update(this.messageFormularioPreenchido);
        }

        this.down('#btn-atualizar').disable();
        this.down('#btn-aceitar').disable();
        this.down('#btn-dispensar').enable();

        this.buildLinks();


        //var btnGravar  =  this.down('#btn-gravar');
        //btnGravar.hide();
    },

    dispensar: function() {

        var tiposDispensa = CRMax.globals.tiposDispensa;

        var tiposDispensaStore = Ext.create('Ext.data.Store', {
            fields: ['IdDispensa', 'Descricao'],
            data: tiposDispensa
        });

        var comboTiposDispensa = {
            xtype: 'combo',
            name: 'IdDispensa',
            fieldLabel: 'Tipos de Dispensa',
            allowBlank: false,
            itemId: 'IdDispensa',
            store: tiposDispensaStore,
            queryMode: 'local',
            displayField: 'Descricao',
            valueField: 'IdDispensa',
            editable: false,
            width: 350,
            forceSelection: true
        };

        var msg = {
            xtype: 'component',
            html: '<h3 style="margin-bottom: 20px;">Selecione o tipo de dispensa:</h3>'
        };

        var form = {
            xtype: 'form',
            bodyStyle: 'padding: 20px;',
            items: [msg, comboTiposDispensa]
        }

        var winConfirma = Ext.create('Ext.window.Window', {
            title: 'Tipos de Dispensa',
            height: 290,
            width: 600,
            modal: true,
            layout: 'fit',
            border: false,
            bodyBorder: false,
            items: [form],

            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'Cancelar',
                scope: this,
                handler: function() {
                    winConfirma.close();
                }
            }, {
                text: 'Ok',
                baseCls: '',
                scope: this,
                cls: 'btn btn-inverse',
                handler: function() {
                    var formRendered = winConfirma.down('form');
                    var basicForm = formRendered.getForm();
                    var values = basicForm.getValues();

                    if (values.IdDispensa === undefined) {
                        Ext.Msg.alert('Aviso', 'Selecione uma opção antes de prosseguir');
                        return;
                    }

                    if (values.IdDispensa === 0) {
                        //Se nao se aplica, submeter sem mensagem de aceite
                        this.submitDispensa(winConfirma, values);
                    } else {
                        Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_6, function(btn) {
                            if (btn === 'yes') {
                                this.logMensagem(6, CRMax.globals.msgsSuitability.msg_6, 'OK');
                                this.submitDispensa(winConfirma, values);
                            } else {
                                this.logMensagem(6, CRMax.globals.msgsSuitability.msg_6, 'Cancelar');
                                winConfirma.close();
                            }
                        }, this);
                    }
                }
            }]
        }).show();

    },

    submitDispensa: function(win, values) {
        var that = this;
        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'Controllers/Suitability/Dispensa.ashx',
            params: values,
            success: function(response) {

                Ext.Ajax.request({
                    scope: this,
                    url: CRMax.globals.baseDirectory + 'controllers/application/loadglobals.ashx?reload=true',
                    success: function(response) {

                        var responseObject = Ext.JSON.decode(response.responseText);
                        that.updateGlobals(responseObject.perfilCotista);

                        that.updateInfoSuitability();

                        if (CRMax.globals.perfilCotista.Dispensado == 'Sim') {
                            that.displayMensagemFormularioPreenchido();
                        } else {
                            that.editForm();
                        }

                        if (win) {
                            win.close();
                        }
                    }
                });
            },
            failure: function(response) {
                if (!(response && response.responseText && (response.responseText.indexOf('Convert.ToInt32((Convert.ToString(context.Session') > 0))) {
                    Ext.Msg.alert('Aviso', 'Ocorreu um erro durante a gravação. Por favor, tente novamente.');
                }

            }
        });

    },

    submitAtualiza: function() {

        var form = this;

        var basicForm = this.getForm();

        if (!basicForm.isValid()) {
            Ext.Msg.alert('Aviso', CRMax.globals.msgsSuitability.msg_9);
            return;
        }

        basicForm.submit({
            url: CRMax.globals.baseDirectory + 'Controllers/Suitability/Atualiza.ashx',
            waitMsg: 'Processando...',
            waitTitle: 'Aguarde',
            scope: this,
            success: function(fp, action) {
                var responseObject = Ext.JSON.decode(action.response.responseText);

                responseObject.perfilCotista.Alertas = CRMax.globals.msgsSuitability.msg_1;

                form.updateInfoSuitability(responseObject.perfilCotista);

                form.down('#btn-dispensar').disable();
                form.down('#btn-atualizar').disable();
                form.down('#btn-aceitar').enable();

                Ext.Msg.alert('Perfil do Investidor', 'Perfil apurado: ' + responseObject.perfilCotista.Perfil);

            },
            failure: function(response) {
                if (!(response && response.responseText && (response.responseText.indexOf('Convert.ToInt32((Convert.ToString(context.Session') > 0))) {
                    Ext.Msg.alert('Aviso', 'Ocorreu um erro durante a gravação. Por favor, tente novamente.');
                }
            }
        });



    },

    saveForm: function() {
        var basicForm = this.getForm();
        basicForm.submit({
            params: {
                action: 'save'
            },
            url: CRMax.globals.baseDirectory + 'Controllers/Suitability/Save.ashx',
            waitMsg: 'Processando...',
            waitTitle: 'Aguarde',
            scope: this,
            success: function(fp, action) {
                var responseObject = Ext.JSON.decode(action.response.responseText);

                this.updateGlobals(responseObject.perfilCotista);

                this.displayMensagemFormularioPreenchido();

                Ext.Msg.alert('Aviso', 'Perfil de investidor atualizado');

                //CRMax.application.notificationWindow.update('Perfil de investidor atualizado');
                //CRMax.application.notificationWindow.show();

                CRMax.application.fireEvent('suitabilityupdated');
            },
            failure: function(response) {
                if (!(response && response.responseText && (response.responseText.indexOf('Convert.ToInt32((Convert.ToString(context.Session') > 0))) {
                    Ext.Msg.alert('Aviso', 'Ocorreu um erro durante a gravação. Por favor, tente novamente.');
                }
            }
        });
    },

    submitAceito: function() {
        var basicForm = this.getForm();
        if (!basicForm.isValid()) {
            Ext.Msg.alert('Aviso', CRMax.globals.msgsSuitability.msg_9);
            return;
        }

        if (!this.down('#btn-atualizar').isDisabled()) {
            Ext.Msg.alert('Aviso', CRMax.globals.msgsSuitability.msg_10);
            return;
        }


        Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_2, function(btn) {
            if (btn === 'yes') {
                this.logMensagem(2, CRMax.globals.msgsSuitability.msg_2, 'OK');

                Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_4, function(btn) {
                    if (btn === 'yes') {
                        this.logMensagem(4, CRMax.globals.msgsSuitability.msg_4, 'OK');
                        Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_15, function(btn) {
                            if (btn === 'yes') {
                                this.logMensagem(15, CRMax.globals.msgsSuitability.msg_15, 'OK');
                                this.saveForm();
                            } else {
                                this.logMensagem(15, CRMax.globals.msgsSuitability.msg_15, 'Cancelar');
                            }
                        }, this);
                    } else {
                        this.logMensagem(4, CRMax.globals.msgsSuitability.msg_4, 'Cancelar');
                    }
                }, this);

            } else {
                this.logMensagem(2, CRMax.globals.msgsSuitability.msg_2, 'Cancelar');
            }
        }, this);

    },

    cancelarDispensa: function() {
        var values = {
            IdDispensa: 0
        };

        this.submitDispensa(null, values);
    },

    submitRecusa: function() {
        var basicForm = this.getForm();

        Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_8, function(btn) {
            if (btn === 'yes') {
                this.logMensagem(8, CRMax.globals.msgsSuitability.msg_8, 'OK');
                Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_15, function(btn) {
                    if (btn === 'yes') {
                        this.logMensagem(15, CRMax.globals.msgsSuitability.msg_15, 'OK');
                        this.saveRecusa();
                    } else {
                        this.logMensagem(15, CRMax.globals.msgsSuitability.msg_15, 'Cancelar');
                    }
                }, this);
            } else {
                this.logMensagem(8, CRMax.globals.msgsSuitability.msg_8, 'Cancelar');
            }
        }, this);
    },

    updateGlobals: function(perfilCotista) {

        Ext.apply(CRMax.globals.perfilCotista, perfilCotista);

    },

    saveRecusa: function() {
        var form = this;
        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'Controllers/Suitability/Recusa.ashx',
            success: function(response) {

                Ext.Ajax.request({
                    scope: this,
                    url: CRMax.globals.baseDirectory + 'controllers/application/loadglobals.ashx?reload=true',
                    success: function(response) {

                        var responseObject = Ext.JSON.decode(response.responseText);
                        form.updateGlobals(responseObject.perfilCotista);
                        form.updateInfoSuitability();
                        form.displayMensagemFormularioPreenchido();

                    }
                });
            },
            failure: function(response) {
                if (!(response && response.responseText && (response.responseText.indexOf('Convert.ToInt32((Convert.ToString(context.Session') > 0))) {
                    Ext.Msg.alert('Aviso', 'Ocorreu um erro durante a gravação. Por favor, tente novamente.');
                }

            }
        });
    }

});
Ext.define('CRMax.view.operacao.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.operacaoedit',
    title: 'Editar Operação',
    autoScroll: true,

    modal: true,
    shadow: false,
    resizable: false,
    border: false,
    draggable: false,
    width: 600,
    tipoResgateEnum: {
        parcial: 1,
        total: 2
    },
    tipoOperacaoEnum: {
        aplicacao: 1,
        resgateBruto: 2,
        resgateLiquido: 3,
        resgateTotal: 5
    },

    initComponent: function() {

        var contasCotista = CRMax.globals.contasCorrentes;

        var contasCotistaStore = Ext.create('Ext.data.Store', {
            fields: ['IdConta', 'Descricao'],
            data: contasCotista
        });
        var contaCotistaDefault = contasCotista.length > 0 ? contasCotista[0].IdConta : undefined;

        var formasLiquidacao = CRMax.globals.formasLiquidacao;
        var formasLiquidacaoStore = Ext.create('Ext.data.Store', {
            fields: ['IdFormaLiquidacao', 'Descricao'],
            data: formasLiquidacao
        });

        var isAplicacao = this.tipoOperacao === this.tipoOperacaoEnum.aplicacao;

        this.setTitle(isAplicacao ? 'Aplicar' : 'Resgatar');

        this.formPanel = new Ext.form.Panel({
            fieldDefaults: {
                msgTarget: 'under'
            },

            bodyStyle: 'padding: 20px;',
            itemId: 'operacao-form',
            items: [{
                    xtype: 'combo',
                    name: 'IdCarteira',
                    fieldLabel: 'Fundo',
                    allowBlank: false,
                    itemId: 'IdCarteira',
                    store: 'FundosCotista',
                    queryMode: 'local',
                    displayField: 'Nome',
                    valueField: 'IdCarteira',
                    editable: false,
                    readOnly: true,
                    value: this.idCarteira,
                    width: 500,
                    forceSelection: true
                }, {
                    xtype: 'datefield',
                    name: 'DataOperacao',
                    fieldLabel: 'Data',
                    itemId: 'DataOperacao',
                    value: new Date(),
                    allowBlank: false,
                    submitFormat: 'Y-m-d',
                    altFormats: 'jnY|dnY|dmY'
                }, {
                    xtype: 'radiogroup',
                    fieldLabel: 'Tipo de Resgate',
                    columns: 1,
                    hidden: isAplicacao,
                    itemId: 'tiporesgate-radio',
                    vertical: true,
                    listeners: {
                        change: function(radio, newValue) {
                            var valorBrutoField = this.formPanel.down('#ValorBruto');
                            if (newValue.TipoResgate === this.tipoResgateEnum.parcial) {
                                valorBrutoField.show();
                                this.tipoResgate = this.tipoResgateEnum.parcial;
                            } else {
                                valorBrutoField.hide();
                                valorBrutoField.setValue(0);
                                this.tipoResgate = this.tipoResgateEnum.total;
                            }
                        },
                        scope: this
                    },
                    items: [{
                        boxLabel: 'Parcial',
                        name: 'TipoResgate',
                        style: 'margin-bottom: 20px',
                        inputValue: this.tipoResgateEnum.parcial,
                        checked: false
                    }, {
                        boxLabel: 'Total',
                        name: 'TipoResgate',
                        inputValue: this.tipoResgateEnum.total,
                        checked: false
                    }]
                },
                /*{
            xtype: 'textfield',
			plugins: 'textmask',
			mask: 'R$ #9.999.999.990,00',
			money: true,

            fieldLabel: 'Valor',			
			hidden: !isAplicacao,
			allowBlank: false,
			itemId: 'ValorBruto',
            name: 'ValorBrutoMask'
              
        }*/
                ,

                {
                    xtype: 'numericfield',
                    itemId: 'ValorBruto',
                    name: 'ValorBrutoMask',
                    decimalSeparator: ',',
                    decimalPrecision: 2,
                    allowNegative: false,
                    alwaysDisplayDecimals: true,
                    hideTrigger: true,
                    useThousandSeparator: true,
                    hidden: !isAplicacao,
                    fieldLabel: 'Valor',
                    allowBlank: false,
                    currencySymbol: 'R$'
                }, {
                    xtype: 'combo',
                    name: 'IdConta',
                    fieldLabel: 'Conta para Crédito',
                    allowBlank: true,
                    itemId: 'IdConta',
                    store: contasCotistaStore,
                    queryMode: 'local',
                    hidden: isAplicacao,
                    displayField: 'Descricao',
                    valueField: 'IdConta',
                    editable: false,
                    value: contaCotistaDefault,
                    width: 350,
                    forceSelection: true
                }

            ],
            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'Cancelar',
                scope: this,
                handler: this.close
            }, {
                text: isAplicacao ? 'Aplicar' : 'Resgatar',
                baseCls: '',
                scope: this,
                cls: 'btn btn-inverse',
                handler: this.validateForm
            }]

        });

        this.items = [this.formPanel];

        this.callParent(arguments);
    },

    displayWinConfirma: function(isAplicacao, tipoOperacaoString, nomeFundo, dataOperacao, valorFormatado, contaParaCredito, tipoOperacao, valorBruto) {

        var detalheValor = (tipoOperacaoString.indexOf('total') >= 0) ? '' : Ext.String.format('<p>Valor: {0}</p><br/>', valorFormatado);

        var detalheContaParaCredito = isAplicacao ? '' : Ext.String.format('<p>Conta para Crédito: {0}</p><br/>', contaParaCredito);

        var msgConfirma = Ext.String.format('<p>Por favor, verifique os dados da operação antes de prosseguir:</p><br />' +
            '<p>Fundo: {3}</p><br/>' +
            '<p>Data da Operação: {0}</p><br/>' +
            '{2}' +
            '{4}' +
            '<br /><p><b>Deseja confirmar {1}?</b></p>', dataOperacao, tipoOperacaoString.toLowerCase().replace('aplicação', 'a aplicação').replace('resgate', 'o resgate'),
            detalheValor, nomeFundo, detalheContaParaCredito);

        var winConfirma = Ext.create('Ext.window.Window', {
            title: 'Confirmação',
            height: 290,
            width: 600,
            modal: true,
            layout: 'fit',
            border: false,
            bodyBorder: false,
            items: { // Let's put an empty grid in just to illustrate fit layout
                xtype: 'panel',
                border: false,
                bodyBorder: false,
                bodyStyle: 'padding: 20px',
                html: msgConfirma
            },
            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'Não',
                scope: this,
                handler: function() {
                    winConfirma.close();
                }
            }, {
                text: 'Sim',
                baseCls: '',
                scope: this,
                cls: 'btn btn-inverse',
                handler: function() {
                    this.saveForm(tipoOperacao, valorBruto);
                    winConfirma.close();
                }
            }]
        }).show();

    },

    validateForm: function() {
        var basicForm = this.formPanel.getForm();



        var tipoOperacao;
        var tipoOperacaoString;
        var isAplicacao = false;
        if (this.tipoOperacao == this.tipoOperacaoEnum.aplicacao) {
            isAplicacao = true;
            tipoOperacao = this.tipoOperacaoEnum.aplicacao;
            tipoOperacaoString = 'Aplicação';
        } else if (this.tipoResgate === this.tipoResgateEnum.parcial) {
            tipoOperacao = this.tipoOperacaoEnum.resgateLiquido;
            tipoOperacaoString = 'Resgate parcial';
        } else if (this.tipoResgate === this.tipoResgateEnum.total) {
            tipoOperacao = this.tipoOperacaoEnum.resgateTotal;
            tipoOperacaoString = 'Resgate total';
        } else {
            Ext.Msg.alert('Aviso', 'Por favor, selecione o tipo de resgate');
            return false;
        }

        if (!basicForm.isValid()) {
            return false;
        }

        var idContaField = this.formPanel.down('#IdConta');
        if (isAplicacao) {
            idContaField.setValue('');
        } else {
            if (idContaField.getValue() == '') {
                Ext.Msg.alert('Aviso', 'Por favor, selecione a conta para crédito');
                return false;
            }
        }

        var valorBruto = this.formPanel.down('#ValorBruto').getValue();
        //valorBruto = valorBruto * 100;

        var nomeFundo = this.formPanel.down('#IdCarteira').getRawValue();
        var dataOperacao = Ext.util.Format.date(this.formPanel.down('#DataOperacao').getValue(), 'd/m/Y');
        var valorFormatado = this.formPanel.down('#ValorBruto').getRawValue();
        var contaParaCredito = idContaField.getRawValue();

        this.displayWinConfirma(isAplicacao, tipoOperacaoString, nomeFundo, dataOperacao, valorFormatado, contaParaCredito, tipoOperacao, valorBruto);

    },


    trataErroTermoAdesaoAceito: function(errorCode, errorMessage, paramsWorkflowSuitability, tipoOperacao, valorBruto){
        Ext.MessageBox.confirm('', errorMessage, function(btn) {
            if (btn === 'yes') {
               this.logMensagem(errorCode, errorMessage, 'OK');
               paramsWorkflowSuitability.TermoAdesaoAceito = true;
               this.saveForm(tipoOperacao, valorBruto, paramsWorkflowSuitability);
            } else {
                this.logMensagem(errorCode, errorMessage, 'Cancelar');
                return false;
            }
        }, this);
    },
    
    trataErroDesenquadramento: function(errorCode, errorMessage, paramsWorkflowSuitability, tipoOperacao, valorBruto){
        Ext.MessageBox.confirm('', errorMessage, function(btn) {
            if (btn === 'yes') {
               //Se o usuario concordar, cancelar o registro da movimentacao para que ele possa preencher o questionario
               this.logMensagem(errorCode, errorMessage, 'OK');
               
               CRMax.application.editSuitability = true;
               CRMax.application.getController('MainNavigator').selectNodeSpecial('suitability');
               this.close();
               
            } else {
                this.logMensagem(errorCode, errorMessage, 'Cancelar');
                Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_19, function(btn) {
                    if (btn === 'yes') {
                        this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'OK');
                        paramsWorkflowSuitability.DesenquadramentoAceito = true;
                        this.saveForm(tipoOperacao, valorBruto, paramsWorkflowSuitability);
                    } else {
                        //cancelou
                        this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'Cancelar');
                    }
                }, this);
                
            }
        }, this);
    },
    

    logMensagem: function(idMensagem, mensagem, resposta){
        Ext.Ajax.request({
                    scope: this,
                    url: CRMax.globals.baseDirectory + 'controllers/suitability/logmensagem.ashx?idmensagem=' + idMensagem + '&mensagem=' + mensagem + '&resposta=' + resposta,
                    success: function(response) {

                    }
                });
    },

    trataErroSuitabilitySaveForm: function(errorCode, errorMessage, paramsWorkflowSuitability, tipoOperacao, valorBruto) {
        paramsWorkflowSuitability = paramsWorkflowSuitability || {};
        
        var propertySuitability;
        if(errorCode == 17){
            errorMessage = CRMax.globals.msgsSuitability.msg_17;
            this.trataErroTermoAdesaoAceito(errorCode, errorMessage, paramsWorkflowSuitability, tipoOperacao, valorBruto);
        }else if(errorCode == 14){
            errorMessage = CRMax.globals.msgsSuitability.msg_14;
            this.trataErroDesenquadramento(errorCode, errorMessage, paramsWorkflowSuitability, tipoOperacao, valorBruto);
        }
    },

    saveForm: function(tipoOperacao, valorBruto, paramsWorkflowSuitability) {

        var params = {
                TipoOperacao: tipoOperacao,
                action: 'save',
                ValorBruto: valorBruto
            };
            
        if(paramsWorkflowSuitability){
            Ext.apply(params, paramsWorkflowSuitability);
        }

        var basicForm = this.formPanel.getForm();
        basicForm.submit({
            params: params,
            url: CRMax.globals.baseDirectory + 'Controllers/OperacaoCotista/Save.ashx',
            waitMsg: 'Processando...',
            waitTitle: 'Aguarde',
            scope: this,
            success: function(fp, action) {
                var responseObject = Ext.JSON.decode(action.response.responseText);

                if (responseObject.errorMessage && responseObject.errorMessage.length > 0) {
                    if (responseObject.erroSuitability > 0) {
                        this.trataErroSuitabilitySaveForm(responseObject.erroSuitability, responseObject.errorMessage, paramsWorkflowSuitability, tipoOperacao, valorBruto);
                        return;
                    }

                    Ext.Msg.alert('Aviso', responseObject.errorMessage);
                } else {
                    Ext.Msg.alert('Aviso', 'Operação enviada com sucesso!');
                    //CRMax.application.notificationWindow.update('Operação enviada com sucesso');
                    //CRMax.application.notificationWindow.show();
                }
                
                if(responseObject.perfilCotista){
                    Ext.apply(CRMax.globals.perfilCotista, responseObject.perfilCotista);
                }
                
                CRMax.application.fireEvent('operacaoupdated');
                this.close();
            },
            failure: function(form, action) {
                var responseObject = Ext.JSON.decode(action.response.responseText);
                var msgAlerta = responseObject && responseObject.errorMessage && responseObject.errorMessage.length ?
                    responseObject.errorMessage : 'Ocorreu um erro na comunicação';
                Ext.Msg.alert('Aviso', msgAlerta);
            }
        });

    }
});
Ext.define('CRMax.view.ordem.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.ordemedit',
    title: 'Editar Ordem',
    autoScroll: true,

    modal: true,
    shadow: false,
    resizable: false,
    border: false,
    draggable: false,
    width: 600,
    tipoResgateEnum: {
        parcial: 1,
        total: 2
    },
    tipoOperacaoEnum: {
        aplicacao: 1,
        resgateBruto: 2,
        resgateLiquido: 3,
        resgateTotal: 5
    },
    
    initComponent: function () { 
    
        var contasCotista = CRMax.globals.contasCorrentes;
        
        var contasCotistaStore = Ext.create('Ext.data.Store', {
             fields: ['IdConta', 'Descricao'],
             data : contasCotista
        });
        var contaCotistaDefault = contasCotista.length > 0 ? contasCotista[0].IdConta : undefined;
      
        var formasLiquidacao = CRMax.globals.formasLiquidacao;
        var formasLiquidacaoStore = Ext.create('Ext.data.Store', {
             fields: ['IdFormaLiquidacao', 'Descricao'],
             data : formasLiquidacao
        });
    
        var isAplicacao = this.tipoOperacao === this.tipoOperacaoEnum.aplicacao;
       
        this.setTitle(isAplicacao ? 'Aplicar' : 'Resgatar');
       
        this.formPanel = new Ext.form.Panel(
        {
            fieldDefaults: {
                msgTarget: 'under'
            },

            bodyStyle: 'padding: 20px;',
            itemId: 'ordem-form',
            items: [
          {
              xtype: 'datefield',
              name: 'DataOperacao',
              fieldLabel: 'Data',
              value: new Date(),
              allowBlank: false,
              submitFormat: 'Y-m-d',
              altFormats: 'jnY|dnY|dmY'
          },
          {
            xtype: 'radiogroup',
            fieldLabel: 'Tipo de Resgate',
            columns: 1,
            hidden: isAplicacao,
            itemId: 'tiporesgate-radio',
            vertical: true,
            listeners: {
                change: function(radio, newValue){
                    var valorBrutoField = this.formPanel.down('#Valor');
                    if(newValue.TipoResgate === this.tipoResgateEnum.parcial){
                        valorBrutoField.show();
                        this.tipoResgate = this.tipoResgateEnum.parcial;
                    }else{
                        valorBrutoField.hide();
                        valorBrutoField.setValue(0);
                        this.tipoResgate = this.tipoResgateEnum.total;
                    }
                },
                scope: this
            },
            items: [{
                boxLabel: 'Parcial',
                name: 'TipoResgate',
                style: 'margin-bottom: 20px',
                inputValue: this.tipoResgateEnum.parcial,
                checked: false
            },{
                boxLabel: 'Total',
                name: 'TipoResgate',
                inputValue: this.tipoResgateEnum.total,
                checked: false
            }]
        },
          {
              xtype: 'numericfield',
              itemId: 'Valor',
              name: 'ValorMask',
              decimalSeparator: ',',
              decimalPrecision: 2,
              allowNegative: false,
              alwaysDisplayDecimals: true,
              hideTrigger: true,
              useThousandSeparator: true,
              fieldLabel: 'Valor',
              allowBlank: false,
              currencySymbol: 'R$'
          },
          {
              xtype: 'combo',
              name: 'IdFormaLiquidacao',
              fieldLabel: 'Forma Liquidacação',
              allowBlank: false,
              itemId: 'IdFormaLiquidacao',
              store: formasLiquidacaoStore,
              queryMode: 'local',
              displayField: 'Descricao',
              valueField: 'IdFormaLiquidacao',
              editable: false,
              allowBlank: false,
              width: 240,
              forceSelection: true,
              listeners: {
                change: function(combo, newValue){
                    var idContaField = this.formPanel.down('#IdConta');
                    var comboDisplayValue = combo.getRawValue();
                    comboDisplayValue = comboDisplayValue.toLowerCase();
                    this.idContaObrigatorio = comboDisplayValue === 'ted' || comboDisplayValue === 'cetip' ||comboDisplayValue === 'doc' || comboDisplayValue.indexOf('corrente') !== -1;
                    if(this.idContaObrigatorio === true){
                        idContaField.show();
                    }else{
                        idContaField.hide();
                    }
                },
                scope: this
            }
          },
          {
              xtype: 'combo',
              name: 'IdConta',
              fieldLabel: 'Conta Liquidação',
              allowBlank: true,
              itemId: 'IdConta',
              store: contasCotistaStore,
              queryMode: 'local',
              hidden: true,
              displayField: 'Descricao',
              valueField: 'IdConta',
              editable: false,
              value: contaCotistaDefault,
              width: 350,
              forceSelection: true
          }
        ], buttons: [
        {
            baseCls: '',
            cls: 'btn',
            text: 'Cancelar',
            scope: this,
            handler: this.close
        },
            {
                text: isAplicacao ? 'Aplicar' : 'Resgatar',
                baseCls: '',
                scope: this,
                cls: 'btn btn-inverse',
                handler: this.saveForm
            }
        ]

        });

        this.items = [this.formPanel];

        this.callParent(arguments);
    },
    
    saveForm: function(){
        var basicForm = this.formPanel.getForm();
        var tipoOperacao;
        var tipoOperacaoString;
        
        var fieldValues = basicForm.getValues();
        
        var isAplicacao = false;
        if(this.tipoOperacao == this.tipoOperacaoEnum.aplicacao){
            isAplicacao = true;
            tipoOperacao = this.tipoOperacaoEnum.aplicacao;
            tipoOperacaoString = 'Aplicação';
        }else if(this.tipoResgate === this.tipoResgateEnum.parcial){
            tipoOperacao = this.tipoOperacaoEnum.resgateLiquido;
            tipoOperacaoString = 'Resgate parcial';
        }else if(this.tipoResgate === this.tipoResgateEnum.total){
            tipoOperacao = this.tipoOperacaoEnum.resgateTotal;
            tipoOperacaoString = 'Resgate total';
        }else{
            Ext.Msg.alert('Aviso','Por favor, selecione o tipo de resgate');
            return false;
        }
        
        if(this.idContaObrigatorio === true){
            var idContaField = this.formPanel.down('#IdConta');
            if(idContaField.getValue() == ''){
                Ext.Msg.alert('Aviso','Por favor, selecione a conta para liquidação');
                return false;
            }
        }
        
        var valor = this.formPanel.down('#Valor').getValue();
        if(this.tipoResgate !== this.tipoResgateEnum.total){        
            if(valor <= 0){
                Ext.Msg.alert('Aviso','Valor digitado é inválido');
                return false;
            }
        }
                
        basicForm.submit({
            params: {
                TipoOperacao: tipoOperacao,
                action: 'save',
                Valor: valor
            },
            url: CRMax.globals.baseDirectory + 'Controllers/OrdemTesouraria/Save.ashx',
            waitMsg: 'Processando...',
            waitTitle: 'Aguarde',
            scope: this,
            success: function (fp, o) {
                Ext.Msg.alert('Aviso','Ordem enviada com sucesso'); 
                
                //CRMax.application.notificationWindow.update('Ordem enviada com sucesso');
                //CRMax.application.notificationWindow.show();
                CRMax.application.fireEvent('ordemupdated');
                this.close();
            },
            failure: function (form, action) {
                var responseObject = Ext.JSON.decode(action.response.responseText);
                var msgAlerta = responseObject && responseObject.errorMessage && responseObject.errorMessage.length ?
                    responseObject.errorMessage : 'Ocorreu um erro na comunicação';
                Ext.Msg.alert('Aviso', msgAlerta);
            }
        });
    }
});
Ext.define('CRMax.view.operacao.Filter', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.operacaofilter',
    itemId: 'operacao-filter',
    autoScroll: true,
    title: 'Pesquisar Operações',
    //requires: ['Ext.ux.form.field.ClearButton'],
    border: false,
    bodyStyle: 'background-color: #f8f8f8; ',
    //style: 'border-left: 1px solid #ddd;border-bottom: 1px solid #ddd; ',
    initComponent: function () {

        var periodoStore = [];
        periodoStore.push(['last7','Últimos 7 dias']);
        periodoStore.push(['last30','Últimos 30 dias']);
        periodoStore.push(['currentMonth','Este mês']);
        periodoStore.push(['period','Período Específico']);
        
        var comboPeriodo = {
              xtype: 'combo',
              fieldLabel: 'Período',
              name: 'Periodo',
              store: periodoStore,
              queryMode: 'local',
              value: 'last7',
              listeners: {
                change: function(combo, newValue){
                    var dataInicio = this.formPanel.down('#DataInicio');
                    var dataFim = this.formPanel.down('#DataFim');
                    
                    if(newValue == 'period'){
                        dataInicio.show();
                        dataFim.show();
                    }else{
                        dataInicio.hide();
                        dataFim.hide();
                    }
                },
                scope: this
            }
        };
        
        var dataInicio = {
            xtype: 'datefield',
            name: 'DataInicio',
            itemId: 'DataInicio',
            submitFormat: 'Y-m-d',
            width: 200,
            hidden: true,
            fieldLabel: 'Início',
            format: 'j/n/Y',
            altFormats: 'jnY|dnY|dmY'
        };
        
        var dataFim = {
            xtype: 'datefield',
            name: 'DataFim',
            itemId: 'DataFim',
            submitFormat: 'Y-m-d',
            width: 200,
            hidden: true,
            fieldLabel: 'Fim',
            format: 'j/n/Y',
            altFormats: 'jnY|dnY|dmY'
        };
                
        var comboFundosCotista = {
              xtype: 'combo',
              fieldLabel: 'Fundo',
              name: 'IdCarteira',
              store: 'FundosCotistaOperacao',
              queryMode: 'local',
              displayField: 'Nome',
              valueField: 'IdCarteira'
        };
        
        var tipoOperacaoStore = [];
        tipoOperacaoStore.push([1,'Aplicação']);
        tipoOperacaoStore.push([2,'Resgate']);
        
        var comboTipoOperacao = {
              xtype: 'combo',
              fieldLabel: 'Tipo de Operação',
              name: 'TipoOperacao',
              store: tipoOperacaoStore,
              queryMode: 'local'
        };
        
        var statusOrdemIntegracaoStore = [];
        
        statusOrdemIntegracaoStore.push([110,'Agendada']);
        statusOrdemIntegracaoStore.push([111,'Cancelada']);
        statusOrdemIntegracaoStore.push([112,'Processando']);
        if(CRMax.globals.userInfo.Permissoes.IsGestor || CRMax.globals.userInfo.Permissoes.IsNotExterno){
            statusOrdemIntegracaoStore.push([113,'Saldo Insuficiente']);
            statusOrdemIntegracaoStore.push([114,'Erro no Envio']);
        }
        statusOrdemIntegracaoStore.push([250,'Efetivada']);
        
        var comboStatusOrdem = {
              xtype: 'combo',
              fieldLabel: 'Status',
              name: 'StatusIntegracao',
              store: statusOrdemIntegracaoStore,
              queryMode: 'local'
        };
        
        var formFields = [comboPeriodo, dataInicio, dataFim, comboFundosCotista, comboTipoOperacao, comboStatusOrdem];

        this.formPanel = Ext.create('Ext.form.Panel', {
            //title: 'Campos',
            items: formFields,
            bodyStyle: 'padding: 10px; background-color: #f3f3f3;',
            border: false,
            defaults: {
            
                //plugins: ['clearbutton'],
                listeners: {
                    scope: this
                    /*change: function (field, newValue) {
                        //Handle clear button plugin... we want to refresh store if field was cleared
                        if (!newValue || !newValue.length) {
                            this.buildFilter();
                        }
                    },
                    select: function () {
                        this.buildFilter();
                    },
                    blur: function () {
                        this.buildFilter();
                    },
                    specialkey: function (field, e) {
                        if (e.getKey() == e.ENTER || e.getKey() == e.ESC || e.getKey() == e.TAB) {
                            this.buildFilter();
                        }
                    }*/
                }
            }
        });


        this.items = [
             this.formPanel
        ];

        this.buttons = [
                
                { text: 'Limpar Filtros',
                    style: 'margin-right: 3px;',
                    handler: this.clearFilter,
                    scope: this
                },'->',
                { text: 'Pesquisar',
                    handler: this.buildFilter,
                    scope: this
                }];


        this.callParent(arguments);
    },


    clearFilter: function () {
        this.clearFormFields();

    },

    clearFormFields: function () {
        //turn off fieltering
        this.filtering = false;

        var basicForm = this.formPanel.getForm();
        var formFields = basicForm.getFields();

        formFields.each(function (formField) {
            //formField.setValue('');
            formField.reset();
        });

        this.filtering = true;
        this.buildFilter();
    },

    onButtonFilterPaymentStatusClick: function (button) {
        var topButton = button.up('button');
        topButton.setText(button.text);
        topButton.filterValue = button.paymentStatusId;

        this.buildFilter();
    },

    getList: function () {
        return Ext.getCmp(this.listId);
    },

    buildFilter: function () {

        if (this.filtering === false) {
            //Sometimes we need to disable filtering to prevent unwanted requests, when clearing all form fields in batch, for example
            return;
        }

        //this.filtering = true;
        var basicForm = this.formPanel.getForm();
        var list = this.getList();

        var filters = [];
        
        var formFields = basicForm.getFields();

        formFields.each(function (formField) {
            var value = formField.getValue();
            if (value && Ext.isString(value)) {
                value = value.replace('\t', '');
            }

            if (formField.getXType() == 'textfield') {
                formField.setRawValue(value);
            }

            if (value) {
                filters.push({
                    property: formField.name,
                    value: value
                });
            }
        });

        list.setStoreFilter(filters);
    }
});


Ext.define('CRMax.view.ordem.Distribute', {
    extend: 'Ext.window.Window',
    alias: 'widget.ordemdistribute',
    itemId: 'ordem-distribute',
    autoScroll: true,
    modal: true,
    closable: true,
    title: 'Distribuir Ordens',
    border: false,
    bodyStyle: 'background-color: #f8f8f8; ',
    initComponent: function () {
        //Limpar store
        var storeFundosTemPermissao = new Ext.create('CRMax.store.FundosCotista');
        storeFundosTemPermissao.filter('TemPermissaoAplicar', true);
        
        var storePosicoes = Ext.data.StoreManager.lookup('Posicoes');
        storePosicoes.removeAll();
        var paramsLoadPosicoes = {idCotista: this.idCotista};
        storePosicoes.load({params: paramsLoadPosicoes});
    
        var storeDistribuicao = Ext.create('Ext.data.Store', {
            fields: ['IdCarteira','NomeCarteira', 'Valor']
        });
        
        var isResgateTotal = this.saldoTotal === 0;
                
        this.saldos = {
            saldoTotal: this.saldoTotal,
            saldoRestante: this.saldoTotal,
            subTotal: 0
        };

        var bbarDistribuicao = [
            { 
                xtype: 'component', 
                tpl: 'Sub-total: R${SubTotal:number("0,000.00"} - Saldo restante: R${Saldo:number("0,000.00"}', 
                data:{SubTotal: this.saldos.subTotal, Saldo: this.saldos.saldoRestante} 
            }
        ];

        var columns = [
            { header: 'Fundo',  dataIndex: 'NomeCarteira', flex: 1 }
        ];

        if(!isResgateTotal){
            columns.push({ header: 'Valor informado', dataIndex: 'Valor', xtype: 'numbercolumn', format:'R$ 0,000.00', align: 'right' });
        }

        columns.push({
                    xtype:'actioncolumn',
                    width:70,
                    renderer: function(v, meta){return '<div class="x-action-col-0 x-action-col x-action-col-resgatar">remover</div>'},
                    tooltip: 'remover',
                    scope: this,
                    handler: this.onClickButtonApagarDistribuicaoOrdem
        });

        this.gridDistribuicao = Ext.create('Ext.grid.Panel', {
            store: storeDistribuicao,
            enableColumnMove: false,
            enableColumnResize: false,
            sortableColumns: false,
            enableColumnHide: false,
            columns: columns,
             dockedItems: isResgateTotal ? undefined : [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    itemId: 'bottomtoolbar',
                    items: bbarDistribuicao
                }
            ],
            height: 200
        });
       
        var comboFundoConfig = {};
        if(this.tipoOperacao == 1){
            //Aplicacao mostrar todos os fundos que o cotista pode aplicar (termo adesao assinado)
            //comboFundoConfig.store = 'FundosCotista';
            //comboFundoConfig.displayField = 'Nome';
            comboFundoConfig.store = storeFundosTemPermissao;
            
            comboFundoConfig.tpl = Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '<div class="x-boundlist-item">{Apelido} - {Nome}</div>',
                '</tpl>'
            );
    
            comboFundoConfig.displayTpl = Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '{Apelido} - {Nome}',
                '</tpl>'
            );
            
        }else{
            //Resgate mostrar apenas fundos com posicao e exibir saldo
            comboFundoConfig.store = 'Posicoes';
            //comboFundoConfig.displayField = 'NomeCarteira';
            comboFundoConfig.tpl = Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '<div class="x-boundlist-item">{ApelidoCarteira} - {NomeCarteira} <span style="color:#999; padding-left: 10px;"> R$ {ValorLiquido:number("0,000.00"}</span></div>',
                '</tpl>'
            );
            
            comboFundoConfig.displayTpl = Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '{ApelidoCarteira} - {NomeCarteira}',
                '</tpl>'
            );
        }
        
        var comboFundo = {
              xtype: 'combo',
              name: 'IdCarteira',
              fieldLabel: 'Fundo',
              allowBlank: false,
              itemId: 'ComboFundo',
              store: comboFundoConfig.store,
              queryMode: 'local',
              displayField: comboFundoConfig.displayField,
              valueField: 'IdCarteira',
              value: this.idCarteira,
              width: 500,
              tpl: comboFundoConfig.tpl,
              displayTpl: comboFundoConfig.displayTpl,
              forceSelection: true
          };
        
        var valorField = {
              itemId: 'Valor',
              name: 'Valor',
              xtype: 'numericfield',
              decimalSeparator: ',',
              decimalPrecision: 2,
              allowNegative: false,
              alwaysDisplayDecimals: true,
              hideTrigger: true,
              useThousandSeparator: true,
              fieldLabel: 'Valor',
              allowBlank: false,
              hidden: isResgateTotal
          };
        
        var nomeCotista = {
            xtype: 'displayfield',
            fieldLabel: 'Cotista',
            name: 'Cotista',
            value: this.nomeCotista
        };
        
        var tipoOperacao = {
            xtype: 'displayfield',
            fieldLabel: 'Tipo Operação',
            name: 'TipoOperacaoString',
            value: this.tipoOperacaoString
        };
        
        var formaLiquidacao = {
            xtype: 'displayfield',
            fieldLabel: 'Forma Liquidação',
            name: 'FormaLiquidacao',
            value: this.formaLiquidacao
        };
        
        var contaCorrente = {
            xtype: 'displayfield',
            fieldLabel: 'Conta Corrente',
            name: 'ContaCorrente',
            value: this.contaCorrente
        };
        
        var btnInserir = { 
                    xtype: 'button',
                    text: 'Inserir',
                    handler: this.onInserirOrdem,
                    scope: this
                };
          
        var formFields = [comboFundo, valorField, nomeCotista, tipoOperacao, formaLiquidacao, contaCorrente, btnInserir];


        this.formPanel = Ext.create('Ext.form.Panel', {
            items: formFields,
            bodyStyle: 'padding: 10px; background-color: #f3f3f3;',
            border: false
        });


        this.items = [
             this.formPanel, this.gridDistribuicao
        ];

        this.buttons = [
                {
            baseCls: '',
            cls: 'btn',
            text: 'Cancelar',
            scope: this,
            handler: this.close
        },{ text: 'Gravar',
            baseCls: '',
                cls: 'btn btn-inverse',
                    handler: this.onSaveDistribuicao,
                    scope: this
                }];

        this.callParent(arguments);
    },
    
    atualizarSaldo: function(){
        var store = this.gridDistribuicao.getStore();
        this.saldos.saldoRestante = this.saldos.saldoTotal;
        this.saldos.subTotal = 0;
        store.each(function(record){
            var valorRecord = record.get('Valor');
            this.saldos.saldoRestante -= valorRecord;
            this.saldos.saldoRestante = +this.saldos.saldoRestante.toFixed(2); //Round Javascript
            this.saldos.subTotal += valorRecord;
            this.saldos.subTotal = +this.saldos.subTotal.toFixed(2); //Round Javascript
            
        }, this);
        
        //Atualizar bottom bar
        var bbar = this.gridDistribuicao.getDockedComponent('bottomtoolbar');
        var totalComponent = bbar.items.getAt(0);
        totalComponent.update({SubTotal: this.saldos.subTotal, Saldo: this.saldos.saldoRestante});
    },
    
    onInserirOrdem: function(){
        var store = this.gridDistribuicao.getStore();
        var valorField = this.formPanel.down('#Valor');
        var valor = valorField.getValue();
        
        var comboFundo = this.formPanel.down('#ComboFundo');
        var idCarteira = comboFundo.getValue();
        if(!idCarteira){
            return false;
        }
        
        var isResgateTotal = this.saldoTotal === 0;
        
        if(!isResgateTotal){
            if(!valor){
                return false;
            }
        
            if(valor <= 0){
                Ext.Msg.alert('Aviso','Valor inválido');
                return false;
            }
            
            if(this.tipoOperacao !== 1){
               //Se ResgateParcial, verificar se o valor lançado nao é maior do que o saldo na posição
                var comboFundoRecord = comboFundo.findRecord(comboFundo.valueField, idCarteira);
                var saldoPosicao = comboFundoRecord.get('ValorLiquido');
                if(valor > saldoPosicao){
                    Ext.Msg.alert('Aviso','O valor do resgate informado ultrapassa o saldo do fundo escolhido');
                    return false;
                }
            }
            
            if(valor > this.saldos.saldoRestante){
               Ext.Msg.alert('Aviso','O valor informado ultrapassa o saldo restante');
                return false;
            }
        }else{
            valor = 0;
        }

        //Não permitir inserir operações para carteiras repetidas
        var existeOperacaoCarteira = store.find('IdCarteira', idCarteira, 0, false, false, true) !== -1;
        if(existeOperacaoCarteira){
            Ext.Msg.alert('Aviso','Este fundo já se encontra selecionado na lista de distribuições');
            return false;
        }
        
        var newRecord = {
            IdCarteira: comboFundo.getValue(),
            NomeCarteira: comboFundo.getRawValue(),
            Valor: valor
        };
        
        store.add(newRecord);
        
        //Atualizar saldo
        this.atualizarSaldo();
        
        comboFundo.reset();
        valorField.reset();
        
    },
    
    onSaveDistribuicao: function(btn){
        if(this.saldos.saldoRestante !== 0){
            Ext.Msg.alert('Aviso','A distribuição só pode ser realizada após a utilização de todo o saldo da operação');
            return false;
        }

        if(btn && btn.disable){
            btn.disable();
        }
            
        var that = this;
        var store = this.gridDistribuicao.getStore();
        var ordens = Ext.encode(Ext.pluck(store.data.items, 'data'));
        
        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'controllers/ordemtesouraria/distribute.ashx',
            params: {
                ordens: ordens,
                idOperacao: this.idOperacao
            },
            success: function(response){
                if(btn && btn.enable){
                    btn.enable();
                }
            
                var responseObject = Ext.JSON.decode(response.responseText);
                if(responseObject.errorMessage && responseObject.errorMessage.length > 0){
                    Ext.Msg.alert('Aviso',responseObject.errorMessage);
                    return false;
                }
                
                Ext.Msg.alert('Aviso','Ordem distribuída com sucesso'); 
                
                //CRMax.application.notificationWindow.update('Ordem distribuída com sucesso');
                //CRMax.application.notificationWindow.show();
                
                CRMax.application.fireEvent('ordemupdated');
                that.close();
            },
            failure: function(response){
                if (!(response && response.responseText && (response.responseText.indexOf('Convert.ToInt32((Convert.ToString(context.Session') > 0))) {
                    Ext.Msg.alert('Aviso','Ocorreu um erro durante a gravação. Por favor, tente novamente.');    
                }
                if(btn && btn.enable){
                    btn.enable();
                }
            }
        });
    },
  
    
    onClickButtonApagarDistribuicaoOrdem: function(grid, rowIndex, colIndex, item, e, record, row){
        var store = grid.getStore();
        store.remove(record);
        
        var window = grid.up('window');
        window.atualizarSaldo();
    }

  });
Ext.define('CRMax.view.ordem.distribute.List', {
    extend: 'Ext.window.Window',
    alias: 'widget.ordemdistributelist',
    border: false,
    title: 'Distribuição da ordem',
    height: 400,
    width: 720,
    modal: true,
    border: false,
    resizable: false,
    layout: 'fit',
    closeAction: 'hide',
    actions: {
        aprovar: 'aprovar',
        cancelar: 'cancelar'
    },
    
    initComponent: function () {
        var gridColumns = [
            { header: 'Fundo',  dataIndex: 'NomeCarteira', flex: 1 },
            { header: 'Tipo Liquidação',  dataIndex: 'FormaLiquidacao', width: 100 },
            { header: 'Conta Corrente',  dataIndex: 'ContaCorrente', width: 150 },
            { header: 'Histórico',  dataIndex: 'TipoOperacaoString'},
            { header: 'Valor informado', dataIndex: 'Valor', xtype: 'numbercolumn', format:'R$ 0,000.00', align: 'right' }
                
        ];
        
        this.gridPanel = Ext.create('Ext.grid.Panel', {
            store: 'OrdensDistribuidas',
            columns: gridColumns,
            enableColumnMove: false,
            enableColumnResize: false,
            sortableColumns: false,
            enableColumnHide: false
        });
        
        this.buttons = [{
                baseCls: '',
                cls: 'btn btn-inverse',
                hidden: true,
                itemId: 'btn-aprovar',
                text: 'Aprovar Ordem',
                scope: this,
                handler: function(){this.updateStatusOrdem(this.actions.aprovar);}
            },{
                baseCls: '',
                cls: 'btn btn-inverse',
                itemId: 'btn-cancelar',
                hidden: true,
                text: 'Cancelar Ordem',
                scope: this,
                handler: function(){this.updateStatusOrdem(this.actions.cancelar);}
            },{
                baseCls: '',
                cls: 'btn',
                scope: this,
                text: 'Fechar',
                handler: function(){this.close();}
            }];
        
        this.items = [this.gridPanel];
        
        this.callParent(arguments);
    },

    loadStore: function () {
        this.gridPanel.store.load();
    },
    
    filter: function(filters){
        this.gridPanel.store.clearFilter(true);
        this.gridPanel.store.filter(filters);
    },
    
    updateFilterOperacaoMae: function(idOperacaoMae){
        this.idOperacaoMae = idOperacaoMae;
        var filters = [];
            
        filters.push({
            property: 'IdOperacaoMae',
            value: idOperacaoMae
        });
            
        this.filter(filters);
    },
    
    setActionAprovar: function(){
        this.updateAction(this.actions.aprovar);
    },
    
    setActionCancelar: function(){
        this.updateAction(this.actions.cancelar);
    },
    
    hideActionButtons: function(){
        var btnAprovar = this.down('#btn-aprovar');
        var btnCancelar = this.down('#btn-cancelar');
        btnAprovar.hide();
        btnCancelar.hide();
    },
   
    updateAction: function(action){
        this.action = action;
        
        this.hideActionButtons();     
        
        if(action == this.actions.aprovar){
            var btnAprovar = this.down('#btn-aprovar');
            btnAprovar.show();
        }else if(action == this.actions.cancelar){
            var btnCancelar = this.down('#btn-cancelar');
            btnCancelar.show();
        }
    },
    
    updateStatusOrdem: function(action){
        this.hideActionButtons();
        
        Ext.Ajax.request({
            scope: this,
            url: CRMax.globals.baseDirectory + 'controllers/ordemtesouraria/updateStatus.ashx',
            params: {
                idOperacao: this.idOperacaoMae,
                action: action
            },
            success: function(response){
                this.updateAction(action);
                var responseObject = Ext.JSON.decode(response.responseText);
                
                if(responseObject.errorMessage && responseObject.errorMessage.length > 0){
                    Ext.Msg.alert('Aviso',responseObject.errorMessage);
                }else{
                    Ext.Msg.alert('Aviso','Operação executada com sucesso'); 
                    
                    //CRMax.application.notificationWindow.update('Operação executada com sucesso');
                    //CRMax.application.notificationWindow.show();
                }
                
                CRMax.application.fireEvent('ordemupdated');
                this.close();
                
            },
            failure: function(response){
                this.updateAction(action);
                var responseObject = Ext.JSON.decode(action.response.responseText);
                var msgAlerta = responseObject && responseObject.errorMessage && responseObject.errorMessage.length ?
                    responseObject.errorMessage : 'Ocorreu um erro na comunicação';
                Ext.Msg.alert('Aviso', msgAlerta);
            }
        });
    }
});
Ext.define('CRMax.view.ordem.Filter', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ordemfilter',
    itemId: 'ordem-filter',
    autoScroll: true,
    title: 'Pesquisar Ordens',
    //requires: ['Ext.ux.form.field.ClearButton'],
    border: false,
    bodyStyle: 'background-color: #f8f8f8; ',
    //style: 'border-left: 1px solid #ddd;border-bottom: 1px solid #ddd; ',
    initComponent: function () {

        var periodoStore = [];
        periodoStore.push(['last1','Do dia']);
        periodoStore.push(['last7','Últimos 7 dias']);
        periodoStore.push(['last30','Últimos 30 dias']);
        periodoStore.push(['currentMonth','Este mês']);
        periodoStore.push(['period','Período Específico']);
        
        var comboPeriodo = {
              xtype: 'combo',
              fieldLabel: 'Período',
              name: 'Periodo',
              store: periodoStore,
              queryMode: 'local',
              value: 'last1',
              listeners: {
                change: function(combo, newValue){
                    var dataInicio = this.formPanel.down('#DataInicio');
                    var dataFim = this.formPanel.down('#DataFim');
                    
                    if(newValue == 'period'){
                        dataInicio.show();
                        dataFim.show();
                    }else{
                        dataInicio.hide();
                        dataFim.hide();
                    }
                },
                scope: this
            }
        };
        
        var dataInicio = {
            xtype: 'datefield',
            name: 'DataInicio',
            itemId: 'DataInicio',
            submitFormat: 'Y-m-d',
            width: 200,
            hidden: true,
            fieldLabel: 'Início',
            format: 'j/n/Y',
            altFormats: 'jnY|dnY|dmY'
        };
        
        var dataFim = {
            xtype: 'datefield',
            name: 'DataFim',
            itemId: 'DataFim',
            submitFormat: 'Y-m-d',
            width: 200,
            hidden: true,
            fieldLabel: 'Fim',
            format: 'j/n/Y',
            altFormats: 'jnY|dnY|dmY'
        };
        
        var formFields = [comboPeriodo, dataInicio, dataFim];


        this.formPanel = Ext.create('Ext.form.Panel', {
            //title: 'Campos',
            items: formFields,
            bodyStyle: 'padding: 10px; background-color: #f3f3f3;',
            border: false,
            defaults: {
                //plugins: ['clearbutton'],
                listeners: {
                    scope: this
                    /*change: function (field, newValue) {
                        //Handle clear button plugin... we want to refresh store if field was cleared
                        if (!newValue || !newValue.length) {
                            this.buildFilter();
                        }
                    },
                    select: function () {
                        this.buildFilter();
                    },
                    blur: function () {
                        this.buildFilter();
                    },
                    specialkey: function (field, e) {
                        if (e.getKey() == e.ENTER || e.getKey() == e.ESC || e.getKey() == e.TAB) {
                            this.buildFilter();
                        }
                    }*/
                }
            }
        });


        this.items = [
             this.formPanel
        ];

        this.buttons = [
                '->',
                /*{ text: 'Limpar Filtros',
                    style: 'margin-right: 33px;',
                    handler: this.clearFilter,
                    scope: this
                }*/,
                { text: 'Pesquisar',
                    handler: this.buildFilter,
                    scope: this
                }];


        this.callParent(arguments);
    },


    clearFilter: function () {
        this.clearFormFields();

        var financeList = this.getFinanceList();
        financeList.setStoreFilter([]);
    },

    clearFormFields: function () {
        //turn off fieltering
        this.filtering = false;

        var basicForm = this.formPanel.getForm();
        var formFields = basicForm.getFields();

        formFields.each(function (formField) {
            formField.setValue('');
        });

        this.filtering = true;
    },

    onButtonFilterPaymentStatusClick: function (button) {
        var topButton = button.up('button');
        topButton.setText(button.text);
        topButton.filterValue = button.paymentStatusId;

        this.buildFilter();
    },

    getList: function () {
        return Ext.getCmp(this.listId);
    },

    buildFilter: function () {

        if (this.filtering === false) {
            //Sometimes we need to disable filtering to prevent unwanted requests, when clearing all form fields in batch, for example
            return;
        }

        //this.filtering = true;
        var basicForm = this.formPanel.getForm();
        var list = this.getList();

        var filters = [];
        
        var formFields = basicForm.getFields();

        formFields.each(function (formField) {
            var value = formField.getValue();
            if (value && Ext.isString(value)) {
                value = value.replace('\t', '');
            }

            if (formField.getXType() == 'textfield') {
                formField.setRawValue(value);
            }

            if (value) {
                filters.push({
                    property: formField.name,
                    value: value
                });
            }
        });

        list.setStoreFilter(filters);
    }
});


Ext.define('CRMax.view.ordem.pgbl.Filter', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ordemfilter',
    itemId: 'ordem-pgbl-filter',
    autoScroll: true,
    title: 'Pesquisar Ordens',
    //requires: ['Ext.ux.form.field.ClearButton'],
    border: false,
    bodyStyle: 'background-color: #f8f8f8; ',
    //style: 'border-left: 1px solid #ddd;border-bottom: 1px solid #ddd; ',
    initComponent: function () {

        var periodoStore = [];
        periodoStore.push(['last7','Últimos 7 dias']);
        periodoStore.push(['last30','Últimos 30 dias']);
        periodoStore.push(['currentMonth','Este mês']);
        periodoStore.push(['period','Período Específico']);
        
        var comboPeriodo = {
              xtype: 'combo',
              fieldLabel: 'Período',
              name: 'Periodo',
              store: periodoStore,
              queryMode: 'local',
              value: 'last7',
              listeners: {
                change: function(combo, newValue){
                    var dataInicio = this.formPanel.down('#DataInicio');
                    var dataFim = this.formPanel.down('#DataFim');
                    
                    if(newValue == 'period'){
                        dataInicio.show();
                        dataFim.show();
                    }else{
                        dataInicio.hide();
                        dataFim.hide();
                    }
                },
                scope: this
            }
        };
        
        var comboCotista = {
              xtype: 'combo',
              fieldLabel: 'Cotista',
              name: 'IdCotista',
              store: 'Cotistas',
              queryMode: 'local',
              displayField: 'Nome',
              valueField: 'IdCotista',
              listeners: {
                change: function(combo, newValue){
                    this.buildFilter();
                },
                scope: this
            }
              
        };
        
        var dataInicio = {
            xtype: 'datefield',
            name: 'DataInicio',
            itemId: 'DataInicio',
            submitFormat: 'Y-m-d',
            width: 200,
            hidden: true,
            fieldLabel: 'Início',
            format: 'j/n/Y',
            altFormats: 'jnY|dnY|dmY'
        };
        
        var dataFim = {
            xtype: 'datefield',
            name: 'DataFim',
            itemId: 'DataFim',
            submitFormat: 'Y-m-d',
            width: 200,
            hidden: true,
            fieldLabel: 'Fim',
            format: 'j/n/Y',
            altFormats: 'jnY|dnY|dmY'
        };
        
      Ext.data.StoreManager.lookup('FundosPgbl').load();
      
        var comboFundosPgbl = {
              xtype: 'combo',
              fieldLabel: 'Fundo',
              name: 'IdCarteira',
              store: 'FundosPgbl',
              queryMode: 'local',
              displayField: 'Nome',
              valueField: 'IdCarteira'
        };
         
        var formFields = [comboPeriodo, dataInicio, dataFim, comboCotista, comboFundosPgbl];
        

        this.formPanel = Ext.create('Ext.form.Panel', {
            //title: 'Campos',
            items: formFields,
            bodyStyle: 'padding: 10px; background-color: #f3f3f3;',
            border: false,
            defaults: {
                //plugins: ['clearbutton'],
                listeners: {
                    scope: this
                    /*change: function (field, newValue) {
                        //Handle clear button plugin... we want to refresh store if field was cleared
                        if (!newValue || !newValue.length) {
                            this.buildFilter();
                        }
                    },
                    select: function () {
                        this.buildFilter();
                    },
                    blur: function () {
                        this.buildFilter();
                    },
                    specialkey: function (field, e) {
                        if (e.getKey() == e.ENTER || e.getKey() == e.ESC || e.getKey() == e.TAB) {
                            this.buildFilter();
                        }
                    }*/
                }
            }
        });


        this.items = [
             this.formPanel
        ];

        this.buttons = [
                '->',
                { text: 'Limpar Filtros',
                    style: 'margin-right: 33px;',
                    handler: this.clearFilter,
                    scope: this
                },
                { text: 'Pesquisar',
                    handler: this.buildFilter,
                    scope: this
                }];


        this.callParent(arguments);
    },


    clearFilter: function () {
        this.clearFormFields();
this.buildFilter();
      
    },

    clearFormFields: function () {
        //turn off fieltering
        this.filtering = false;

        var basicForm = this.formPanel.getForm();
        var formFields = basicForm.getFields();

        formFields.each(function (formField) {
            formField.setValue('');
        });

        this.filtering = true;
    },

    onButtonFilterPaymentStatusClick: function (button) {
        var topButton = button.up('button');
        topButton.setText(button.text);
        topButton.filterValue = button.paymentStatusId;

        this.buildFilter();
    },

    getList: function () {
        return Ext.getCmp(this.listId);
    },

    buildFilter: function () {

        if (this.filtering === false) {
            //Sometimes we need to disable filtering to prevent unwanted requests, when clearing all form fields in batch, for example
            return;
        }

        //this.filtering = true;
        var basicForm = this.formPanel.getForm();
        var list = this.getList();

        var filters = [];
        
        var formFields = basicForm.getFields();

        formFields.each(function (formField) {
            var value = formField.getValue();
            if (value && Ext.isString(value)) {
                value = value.replace('\t', '');
            }

            if (formField.getXType() == 'textfield') {
                formField.setRawValue(value);
            }

            if (value) {
                filters.push({
                    property: formField.name,
                    value: value
                });
            }
        });

        list.setStoreFilter(filters);
    }
});


Ext.define('CRMax.view.extrato.Filter', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.extratofilter',
    itemId: 'extrato-filter',
    autoScroll: true,
    title: 'Período do Extrato',
    //requires: ['Ext.ux.form.field.ClearButton'],
    border: false,
    bodyStyle: 'background-color: #f8f8f8; ',
    //style: 'border-left: 1px solid #ddd;border-bottom: 1px solid #ddd; ',
    initComponent: function () {

        var tipoExtratoStore = [];
        tipoExtratoStore.push(['sintetico', 'Consolidado']);
        tipoExtratoStore.push(['analitico', 'Detalhado']);
        tipoExtratoStore.push(['relatoriomensal', 'Relatório Mensal']);
        var comboTipoExtrato = {
              xtype: 'combo',
              fieldLabel: 'Tipo Extrato',
              name: 'TipoExtrato',
              store: tipoExtratoStore,
              queryMode: 'local',
              value: 'sintetico',
              width: 170,
              editable: false,
              forceSelection: true,
              listWidth: 50,
              listeners: {
                change: function(combo, newValue, oldValue){
                    var comboPeriodo = this.formPanel.getForm().findField('Periodo');
                    var comboPeriodoStore = comboPeriodo.getStore();
                    if(newValue == 'relatoriomensal'){
                        comboPeriodoStore.removeAll();
                        comboPeriodoStore.loadRawData(extratoStore);
                        comboPeriodo.setValue(undefined);
                    }else{
                        if(oldValue == 'relatoriomensal'){
                            comboPeriodoStore.removeAll();
                            comboPeriodoStore.loadRawData(periodoStore);
                            comboPeriodo.setValue('currentMonth');
                        }
                        this.buildFilter();
                    }
                },
                scope: this
            }
        };
        
        
        var periodoStore = [];
        periodoStore.push(['last7','Últimos 7 dias']);
        periodoStore.push(['last30','Últimos 30 dias']);
        periodoStore.push(['currentMonth','Este mês']);
        periodoStore.push(['period','Período Específico']);
        
        var extratoStore = [];
        
        for(var i=0; i < CRMax.globals.downloadExtrato.Documentos.length; i++){
            var documento = CRMax.globals.downloadExtrato.Documentos[i];
            extratoStore.push([documento.Path, documento.Nome]);
        }
        
        var comboPeriodo = {
              xtype: 'combo',
              fieldLabel: 'Período',
              name: 'Periodo',
              store: periodoStore,
              queryMode: 'local',
              value: 'currentMonth',
              width: 170,
              editable: false,
              forceSelection: true,
              listWidth: 50,
              listeners: {
                change: function(combo, newValue){
                    if(!newValue){
                        return;
                    }
                    
                    if(newValue != 'last7' && newValue != 'last30' && newValue != 'currentMonth' && newValue != 'period'){
                        var controllerLink = CRMax.globals.baseDirectory + 'controllers/documento/download.ashx?path=';
                        var linkExtrato = encodeURIComponent(newValue);
                        window.open(controllerLink + linkExtrato);
                        return;
                    }
                
                    var dataInicio = this.formPanel.down('#DataInicio');
                    var dataFim = this.formPanel.down('#DataFim');
                    
                    if(newValue == 'period'){
                        dataInicio.show();
                        dataFim.show();
                    }else{
                        dataInicio.hide();
                        dataFim.hide();
                    }
                
                    
                },
                scope: this
            }
        };
        
        var dataInicio = {
            xtype: 'datefield',
            name: 'DataInicio',
            itemId: 'DataInicio',
            submitFormat: 'Y-m-d',
            width: 150,
            hidden: true,
            fieldLabel: 'Início',
            format: 'j/n/Y',
            altFormats: 'jnY|dnY|dmY'
        };
        
        var dataFim = {
            xtype: 'datefield',
            name: 'DataFim',
            itemId: 'DataFim',
            submitFormat: 'Y-m-d',
            width: 150,
            hidden: true,
            fieldLabel: 'Fim',
            format: 'j/n/Y',
            altFormats: 'jnY|dnY|dmY'
        };
        
        var formFields = [comboTipoExtrato, comboPeriodo, dataInicio, dataFim];


        this.formPanel = Ext.create('Ext.form.Panel', {
            //title: 'Campos',
            items: formFields,
            bodyStyle: 'padding: 10px; background-color: #f3f3f3;',
            border: false,
            labelWidth: 50,
            defaults: {
            labelAlign: 'top',
                labelWidth: 50,
                //plugins: ['clearbutton'],
                listeners: {
                    scope: this
                    /*change: function (field, newValue) {
                        //Handle clear button plugin... we want to refresh store if field was cleared
                        if (!newValue || !newValue.length) {
                            this.buildFilter();
                        }
                    },
                    select: function () {
                        this.buildFilter();
                    },
                    blur: function () {
                        this.buildFilter();
                    },
                    specialkey: function (field, e) {
                        if (e.getKey() == e.ENTER || e.getKey() == e.ESC || e.getKey() == e.TAB) {
                            this.buildFilter();
                        }
                    }*/
                }
            }
        });


        this.items = [
             this.formPanel
        ];

        this.buttons = [
                '->',
                { text: 'Pesquisar',
                    handler: this.buildFilter,
                    scope: this
                }];


        this.callParent(arguments);
    },


    clearFilter: function () {
        this.clearFormFields();

        var financeList = this.getFinanceList();
        financeList.setStoreFilter([]);
    },

    clearFormFields: function () {
        //turn off fieltering
        this.filtering = false;

        var basicForm = this.formPanel.getForm();
        var formFields = basicForm.getFields();

        formFields.each(function (formField) {
            formField.setValue('');
        });

        this.filtering = true;
    },

    onButtonFilterPaymentStatusClick: function (button) {
        var topButton = button.up('button');
        topButton.setText(button.text);
        topButton.filterValue = button.paymentStatusId;

        this.buildFilter();
    },

    getList: function () {
        return Ext.getCmp(this.listId);
    },

    buildFilter: function () {

        
        
        if (this.filtering === false) {
            //Sometimes we need to disable filtering to prevent unwanted requests, when clearing all form fields in batch, for example
            return;
        }

        //this.filtering = true;
        var basicForm = this.formPanel.getForm();
        var list = this.getList();

        var filters = [];
        
        var formFields = basicForm.getFields();

        formFields.each(function (formField) {
            var value = formField.getValue();
            if (value && Ext.isString(value)) {
                value = value.replace('\t', '');
            }

            if (formField.getXType() == 'textfield') {
                formField.setRawValue(value);
            }

            if (value) {
                filters.push({
                    property: formField.name,
                    value: value
                });
            }
        });

        list.setStoreFilter(filters);
    }
});


Ext.define('CRMax.view.cotista.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.cotistalist',

    store: 'Cotistas',

    border: false,

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function() {

        this.columns = [{
            header: 'Código',
            dataIndex: 'IdCotista',
            width: 100
        }, {
            header: 'Nome',
            dataIndex: 'Nome',
            flex: 1
        }, {
            header: 'CPF/CNPJ',
            dataIndex: 'Cpfcnpj',
            flex: 1
        }];


        var filterTask = new Ext.util.DelayedTask();

        var filterChange = function(field, newValue) {
            filterTask.delay(500, filter, this, [field, newValue]);
        };

        var filter = function(field, newValue) {
            //Handle clear button plugin... we want to refresh store if field was cleared
            this.store.clearFilter(true);
            if (newValue && newValue.length > 0) {
                this.store.filter([{
                    property: "searchQuery",
                    value: newValue
                }]);
            } else {
                this.store.clearFilter();
            }

        };

        this.tbar = [{
            xtype: 'image',
            style: 'margin: 0 4px 0 6px;',
            src: 'content/themes/base/images/search.png',
            width: 16,
            height: 16
        }, {
            xtype: 'textfield',
            width: 300,
            listeners: {
                scope: this,
                change: filterChange
            }
            /*,
                                select: function () {
                                    this.buildFilter();
                                },
                                blur: function () {
                                    this.buildFilter();
                                },
                                specialkey: function (field, e) {
                                    if (e.getKey() == e.ENTER || e.getKey() == e.ESC || e.getKey() == e.TAB) {
                                        this.buildFilter();
                                    }
                                }*/
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            store: 'Cotistas',
            dock: 'bottom'
        }];

        this.callParent(arguments);

        this.on('select', this.onRowSelect, this);
    },
    afterRender: function() {
        var me = this;
        me.callParent(arguments);
        me.down('#refresh').hide();
    },

    onRowSelect: function(grid, record, index, options) {
        var idCotista = record.get('IdCotista');

        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'controllers/cotista/select.ashx',
            params: {
                idCotista: idCotista
            },
            success: function(response) {
                window.location.reload();
            }
        });

    },

    loadStore: function() {
        this.store.load();
    }
});
Ext.define('CRMax.view.fundo.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.fundolist',
    hideHeaders: false,
    store: {
        type: 'fundoscotista'
    },

    border: false,

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function () {
        this.columns = [
            {dataIndex: 'Nome', flex: 1, header: 'Fundo'},
            {dataIndex: 'ValorMinimoInicial', width: 100, header: 'Aplic. Inicial (R$)', xtype: 'numbercolumn', format:'0,000', align: 'right'},
            {dataIndex: 'HorarioFim', width: 110, header: 'Hora Limite', type: 'date', align: 'center', xtype: 'datecolumn',format: 'H:i'},
            {dataIndex: 'Categoria', width: 200, header: 'Categoria' },
            {
                xtype:'actioncolumn',
                renderer: function(v, meta){return '<div class="x-action-col-0 x-action-col x-action-col-aplicar">aplicar</div>'},
                width:50,
                hidden: CRMax.globals.isTesouraria,
                items: [{
                    scope: this,
                    handler: this.onClickButtonAplicar
                }]
            },
            {
                xtype:'actioncolumn',
                renderer: function(v, meta){return '<div class="x-action-col-0 x-action-col x-action-col-info">+info</div>'},
                width:45,
                items: [{
                    scope: this,
                    handler: this.onClickButtonDetalhes
                }]
            }
        ];

        this.callParent(arguments);
    },

    /*onClickButtonAplicar: function(grid, rowIndex, colIndex, item, e, record, row){
        if(CRMax.globals.userInfo.CotistaPodeOperar !== true){
            Ext.Msg.alert('Aviso','Não é possível realizar aplicações. Motivo: ' + CRMax.globals.userInfo.PendenciaCadastral);
            return false;
        }
        
        var temPermissaoAplicar = record.get('TemPermissaoAplicar');
        if(!temPermissaoAplicar){
            this.showWindowSemPermissaoAplicarFundo(record);
        }else{
            this.showWindowAvisoAplicacao(record);
            
        }
    },*/
    
    onClickButtonAplicar: function(grid, rowIndex, colIndex, item, e, record, row) {
         if (CRMax.globals.userInfo.CotistaPodeOperar !== true) {
            Ext.Msg.alert('Aviso', 'Não é possível realizar aplicações. Motivo: ' + CRMax.globals.userInfo.PendenciaCadastral);
            return false;
        }
        
        var temPermissaoAplicar = record.get('TemPermissaoAplicar');
        if(!temPermissaoAplicar){
            this.showWindowSemPermissaoAplicarFundo(record);
            return false;
        }

        var dispensado = CRMax.globals.perfilCotista.Dispensado == 'Sim';
        var recusou = CRMax.globals.perfilCotista.Status == 'Recusado';
        var temPerfil = CRMax.globals.perfilCotista.Perfil !== null && CRMax.globals.perfilCotista.Perfil !== "" && CRMax.globals.perfilCotista.Perfil != 'Indefinido.';

        var gridPanel = grid.up('gridpanel');
        if (dispensado || recusou) {
            gridPanel.showWindowAvisoAplicacao(record);
        } else {
            //Se cliente se recusou a preencher o perfil ou nao tem perfil preenchido, nao continuar
            if (!temPerfil) {
                Ext.Msg.alert('Aviso', CRMax.globals.msgsSuitability.msg_11);
                return false;
            }

            //Checar data de validade do perfil
            if (!CRMax.globals.perfilCotista.DentroValidade) {
                //Exibir mensagem de perfil desatualizado
                Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_12, function(btn) {
                    if (btn === 'yes') {
                        this.logMensagem(12, CRMax.globals.msgsSuitability.msg_12, 'OK');

                        //Exibir mensagem do termo de inadequacao
                        Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_19, function(btn) {
                            if (btn === 'yes') {
                                this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'OK');
                                gridPanel.showWindowAvisoAplicacao(record);
                            } else {
                                this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'Cancelar');
                                return false;
                            }
                        }, this);
                    } else {
                        this.logMensagem(12, CRMax.globals.msgsSuitability.msg_12, 'Cancelar');
                        return false;
                    }
                }, this);
            }else{
                gridPanel.showWindowAvisoAplicacao(record);
            }
        }
    },
    
    logMensagem: function(idMensagem, mensagem, resposta) {
        Ext.Ajax.request({
            scope: this,
            url: CRMax.globals.baseDirectory + 'controllers/suitability/logmensagem.ashx?idmensagem=' + idMensagem + '&mensagem=' + mensagem + '&resposta=' + resposta,
            success: function(response) {

            }
        });
    },
    
    showWindowAvisoAplicacao: function(record){
        var html = '<div class="info-aplic-fundo">';
        html += '<div class="box-intrucoes-aplicacao">';
        html += '<p>Para efetivação da aplicação é necessário providenciar a transferência do montante através de TED/DOC/TEF de acordo com seus dados previamente informados em seu cadastro.</p>';
        html += '<p>O horário limite para movimentações é 15h. Solicitações recebidas após este horário serão processadas automaticamente no dia útil subsequente.</p>';
        html += '<p>Dúvidas entre em contato com a Central de Atendimento: (11) 3366-3370 (dias úteis das 8h15 às 17h30).</p>';
        html += '<br /><p>Você já realizou a transferência?</p>';
        html += '</div>';
        html += '</div>';
        var that = this;
        var winAvisoAplicacao = Ext.create('Ext.window.Window', {
            title: 'Aplicação em Fundo de Investimento',
            height: 400,
            width: 800,
            y: CRMax.application.inIframe() ? 50 :undefined,
            modal: true,
            layout: 'fit',
            border: false,
            //items: component
            html: html,
            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'NÃO',
                handler: function(){winAvisoAplicacao.close();}
            },
            {
                baseCls: '',
                cls: 'btn btn-inverse',
                text: 'SIM',
                handler: function(){that.showWindowAplicacao(record); winAvisoAplicacao.close();}
            }
            ]
        });
        
        winAvisoAplicacao.show(); 
    },
    
    showWinDetalhesFundo: function(carteira){
        var controllerLink = CRMax.globals.baseDirectory + 'controllers/documento/download.ashx?path=';
        var linkLamina = controllerLink + encodeURIComponent(carteira.LinkLamina);
        var linkProspecto = controllerLink + encodeURIComponent(carteira.LinkProspecto);
        var linkTermoAdesao = controllerLink + encodeURIComponent(carteira.LinkTermoAdesao);
        var linkRegulamento = controllerLink + encodeURIComponent(carteira.LinkRegulamento);
        
        var html = '<div class="info-aplic-fundo">';
        html += '<h1>Fundo: ' + carteira.Nome + '</h1>';
        html += '<a target="_blank" href="' + linkLamina + '">Lâmina</a>';
        html += '<a target="_blank" href="' + linkTermoAdesao + '">Termo de Adesão</a>';
        html += '<a target="_blank" href="' + linkProspecto + '">Prospecto</a>';
        html += '<a target="_blank" href="' + linkRegulamento + '">Regulamento</a>';
        html += '</div>'

        var htmlComponent = {
            xtype: 'component',
            //layout: 'fit',
            html: html,
            height: 100
        };
        
        var storeRentabilidade = Ext.create('Ext.data.Store', {
            //storeId:'Rentabilidades',
            fields:['RetornoMesCorrente', 'RetornoUltimoMes', 'RetornoAno', 'Retorno12Meses'],
            data:{'items':[
                { 
                    'RetornoMesCorrente': carteira.RetornoMesCorrente,  
                    'RetornoUltimoMes': carteira.RetornoUltimoMes,
                    'RetornoAno': carteira.RetornoAno,
                    'Retorno12Meses': carteira.Retorno12Meses
                }
            ]},
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'items'
                }
            }
        });

        var gridRentabilidade = Ext.create('Ext.grid.Panel', {
            title: 'Rentabilidade',
            store: storeRentabilidade,
            //layout: 'fit',
            columns: [
                {dataIndex: 'RetornoMesCorrente', width: 85, header: 'No Mês', headerAlign: 'right', xtype: 'numbercolumn', format:'0,000.00%', align: 'right'},
                {dataIndex: 'RetornoUltimoMes', width: 125,header: 'Mês Anterior', xtype: 'numbercolumn', format:'0,000.00%', align: 'right'},
                {dataIndex: 'RetornoAno', width: 125,header: 'No Ano', xtype: 'numbercolumn', format:'0,000.00%', align: 'right'},
                {dataIndex: 'Retorno12Meses', width: 125,header: '12 Meses', xtype: 'numbercolumn', format:'0,000.00%', align: 'right'},
                {dataIndex: 'Spacer', width: 25,header: ''}
            ],
            height: 250,
            width: 500,
            enableColumnMove: false,
            enableColumnResize: false,
            sortableColumns: false,
            enableColumnHide: false
        });
               
        var winDetalhesFundo = Ext.create('Ext.window.Window', {
            title: 'Detalhes do Fundo de Investimento',
            height: 300,
            width: 500,
            cls: 'win-detalhes-fundo',
            y: CRMax.application.inIframe() ? 50 :undefined,
            modal: true,
            //layout: 'fit',
            items: [htmlComponent, gridRentabilidade],
            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'Fechar',
                handler: function(){winDetalhesFundo.close()}
            }]
        });
        
        winDetalhesFundo.show();  
    },
    
    onClickButtonDetalhes: function(grid, rowIndex, colIndex, item, e, recordCarteira, row){
        
        Ext.Ajax.request({
            scope: this,
            url: CRMax.globals.baseDirectory + 'controllers/fundocotista/details.ashx?idcarteira=' + recordCarteira.get('IdCarteira'),
            success: function(response) {
                var responseObject = Ext.JSON.decode(response.responseText);
                this.showWinDetalhesFundo(responseObject.carteira);
            }
        });
        
        
    },
    
    showWindowAplicacao: function(record){
        var idCarteira = record.get('IdCarteira');
        var window = Ext.create('CRMax.view.operacao.Edit', {
            tipoOperacao: 1,
            y: CRMax.application.inIframe() ? 50 :undefined,
            idCarteira: idCarteira
        });
        window.show();
    },
    
    showWindowSemPermissaoAplicarFundo: function(recordCarteira){
        Ext.Ajax.request({
            scope: this,
            url: CRMax.globals.baseDirectory + 'controllers/fundocotista/details.ashx?idcarteira=' + recordCarteira.get('IdCarteira'),
            success: function(response) {
                var responseObject = Ext.JSON.decode(response.responseText);
                
                recordCarteira = responseObject.carteira;
                
                var controllerLink = CRMax.globals.baseDirectory + 'controllers/documento/download.ashx?path=';
        var linkLamina = controllerLink + encodeURIComponent(recordCarteira.LinkLamina);
        var linkProspecto = controllerLink + encodeURIComponent(recordCarteira.LinkProspecto);
        var linkTermoAdesao = controllerLink + encodeURIComponent(recordCarteira.LinkTermoAdesao);
        var linkRegulamento = controllerLink + encodeURIComponent(recordCarteira.LinkRegulamento);
        
        var html = '<div class="info-aplic-fundo">';
        html += '<h1>Fundo: ' + recordCarteira.Nome + '</h1>';
        html += '<a target="_blank" href="' + linkLamina + '">Lâmina</a>';
        html += '<a target="_blank" href="' + linkTermoAdesao + '">Termo de Adesão</a>';
        html += '<a target="_blank" href="' + linkProspecto + '">Prospecto</a>';
        html += '<a target="_blank" href="' + linkRegulamento + '">Regulamento</a>';
        html += '<div class="box-intrucoes-aplicacao">';
        html += '<p>Esta é a sua primeira aplicação neste fundo. Por favor, leia a Lâmina, Prospecto, Regulamento e o Termo de Adesão do Fundo.</p>';
        html += '<p>Favor imprimir, preencher e assinar o Termo de Adesão e enviar para a Porto Seguro Investimentos.</p>';
        html += '<p>Endereço para envio do Termo de Adesão:<br /> ' + CRMax.globals.enderecoPorto + '</p>';
         html += '<p>Dúvidas entre em contato com a Central de Atendimento: (11) 3366-3370 (dias úteis das 8h15 às 17h30).</p>';
         html += '</div>';
         html += '</div>';
        
        var that = this;
        
        var winSemPermissaoAplicarFundo = Ext.create('Ext.window.Window', {
            title: 'Nova Aplicação em Fundo de Investimento',
            height: 400,
            width: 800,
            y: CRMax.application.inIframe() ? 50 :undefined,
            modal: true,
            layout: 'fit',
            border: false,
            //items: component
            html: html,
            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'Fechar',
                handler: function(){winSemPermissaoAplicarFundo.close()}
            }/*,{
                baseCls: '',
                cls: 'btn btn-inverse',
                text: 'Continuar',
                handler: function(){that.showWindowAvisoAplicacao(recordCarteira);winSemPermissaoAplicarFundo.close();}
            }*/]
        });
        
        winSemPermissaoAplicarFundo.show();  
                
            }
        });
    },

    loadStore: function () {
        this.store.load();
    }
});
Ext.define('CRMax.view.posicao.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.posicaolist',
    hideHeaders: false,
    store: 'Posicoes',

    border: false,

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function() {

        if (CRMax.globals.isTesouraria && CRMax.globals.userInfo.Permissoes.PodeBoletar) {
            this.tbar = [{
                text: 'Aplicar',
                handler: this.onClickButtonAplicarOrdem,
                baseCls: '',
                cls: 'btn-aplicacao'
            }, {
                text: 'Resgatar',
                handler: this.onClickButtonResgatarOrdem,
                baseCls: '',
                cls: 'btn-resgate'
            }];
        }

        this.columns = [{
            dataIndex: 'NomeCarteira',
            flex: 1,
            header: 'Fundo'
        }, {
            dataIndex: 'Quantidade',
            header: 'Qtde. Cotas',
            width: 120,
            xtype: 'numbercolumn',
            format: '0,000.000000',
            align: 'right'
        }, {
            dataIndex: 'CotaDia',
            header: 'Valor Cota (R$)',
            width: 100,
            xtype: 'numbercolumn',
            format: '0,000.000000',
            align: 'right'
        }, {
            dataIndex: 'ValorLiquido',
            header: 'Saldo Líquido (R$)',
            width: 120,
            xtype: 'numbercolumn',
            format: '0,000.00',
            align: 'right'
        }, {
            dataIndex: 'ValorLiquidoResgate',
            header: 'Saldo Líquido p/ Resgate (R$)',
            width: 170,
            xtype: 'numbercolumn',
            format: '0,000.00',
            align: 'right'
        }, {
            xtype: 'actioncolumn',
            width: 50,
            hidden: CRMax.globals.isTesouraria,
            renderer: function(v, meta) {
                return '<div class="x-action-col-0 x-action-col x-action-col-aplicar">aplicar</div>'
            },
            tooltip: 'aplicar',
            items: [{
                    scope: this,
                    handler: this.onClickButtonAplicar
                }]
        }, {
            xtype: 'actioncolumn',
            width: 60,
            hidden: CRMax.globals.isTesouraria,
            renderer: function(v, meta) {
                return '<div class="x-action-col-0 x-action-col x-action-col-resgatar">resgatar</div>'
            },
            items: [{
                scope: this,
                handler: this.onClickButtonResgatar
            }]
        }, {
            xtype: 'actioncolumn',
            width: 55,
            renderer: function(v, meta) {
                return '<div class="x-action-col-0 x-action-col x-action-col-info">posição</div>'
            },
            items: [{
                handler: this.onClickButtonDetalhe
            }]
        }];

        this.callParent(arguments);

        CRMax.application.on('operacaoupdated', this.loadStore, this);
    },

    onClickButtonAplicar: function(grid, rowIndex, colIndex, item, e, record, row) {

        if (CRMax.globals.userInfo.CotistaPodeOperar !== true) {
            Ext.Msg.alert('Aviso', 'Não é possível realizar aplicações. Motivo: ' + CRMax.globals.userInfo.PendenciaCadastral);
            return false;
        }

        var dispensado = CRMax.globals.perfilCotista.Dispensado == 'Sim';
        var recusou = CRMax.globals.perfilCotista.Status == 'Recusado';
        var temPerfil = CRMax.globals.perfilCotista.Perfil !== null && CRMax.globals.perfilCotista.Perfil !== "" && CRMax.globals.perfilCotista.Perfil != 'Indefinido.';

        var gridPanel = grid.up('gridpanel');
        if (dispensado || recusou) {
            gridPanel.showWindowAvisoAplicacao(record);
        } else {
            //Se cliente se recusou a preencher o perfil ou nao tem perfil preenchido, nao continuar
            if (!temPerfil) {
                Ext.Msg.alert('Aviso', CRMax.globals.msgsSuitability.msg_11);
                return false;
            }

            //Checar data de validade do perfil
            if (!CRMax.globals.perfilCotista.DentroValidade) {
                //Exibir mensagem de perfil desatualizado
                Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_12, function(btn) {
                    if (btn === 'yes') {
                        this.logMensagem(12, CRMax.globals.msgsSuitability.msg_12, 'OK');

                        //Exibir mensagem do termo de inadequacao
                        Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_19, function(btn) {
                            if (btn === 'yes') {
                                this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'OK');
                                gridPanel.showWindowAvisoAplicacao(record);
                            } else {
                                this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'Cancelar');
                                return false;
                            }
                        }, this);
                    } else {
                        this.logMensagem(12, CRMax.globals.msgsSuitability.msg_12, 'Cancelar');
                        return false;
                    }
                }, this);
            }else{
                gridPanel.showWindowAvisoAplicacao(record);
            }
        }
    },

    logMensagem: function(idMensagem, mensagem, resposta) {
        Ext.Ajax.request({
            scope: this,
            url: CRMax.globals.baseDirectory + 'controllers/suitability/logmensagem.ashx?idmensagem=' + idMensagem + '&mensagem=' + mensagem + '&resposta=' + resposta,
            success: function(response) {

            }
        });
    },

    showWindowAvisoAplicacao: function(record) {
        var html = '<div class="info-aplic-fundo">';
        html += '<div class="box-intrucoes-aplicacao">';
        html += '<p>Para efetivação da aplicação é necessário providenciar a transferência do montante através de TED/DOC/TEF de acordo com seus dados previamente informados em seu cadastro.</p>';
        html += '<p>O horário limite para movimentações é 15h. Solicitações recebidas após este horário serão processadas automaticamente no dia útil subsequente.</p>';
        html += '<p>Dúvidas entre em contato com a Central de Atendimento: (11) 3366-3370 (dias úteis das 8h15 às 17h30).</p>';
        html += '<br /><p>Você já realizou a transferência?</p>';
        html += '</div>';
        html += '</div>';
        var that = this;
        var winAvisoAplicacao = Ext.create('Ext.window.Window', {
            title: 'Aplicação em Fundo de Investimento',
            height: 400,
            width: 800,
            y: CRMax.application.inIframe() ? 50 : undefined,
            modal: true,
            layout: 'fit',
            border: false,
            //items: component
            html: html,
            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'NÃO',
                handler: function() {
                    winAvisoAplicacao.close();
                }
            }, {
                baseCls: '',
                cls: 'btn btn-inverse',
                text: 'SIM',
                handler: function() {
                    that.showWindowAplicacao(record);
                    winAvisoAplicacao.close();
                }
            }]
        });

        winAvisoAplicacao.show();
    },

    showWindowAplicacao: function(record) {
        var idCarteira = record.get('IdCarteira');
        var window = Ext.create('CRMax.view.operacao.Edit', {
            tipoOperacao: 1,
            y: CRMax.application.inIframe() ? 50 : undefined,
            idCarteira: idCarteira
        });
        window.show();
    },

    onClickButtonAplicarOrdem: function() {
        if (CRMax.globals.userInfo.CotistaPodeOperar !== true) {
            Ext.Msg.alert('Aviso', 'Não é possível realizar aplicações. Motivo: ' + CRMax.globals.userInfo.PendenciaCadastral);
            return false;
        }

        var window = Ext.create('CRMax.view.ordem.Edit', {
            tipoOperacao: 1,
            y: CRMax.application.inIframe() ? 50 : undefined
        });
        window.show();
    },

    onClickButtonResgatarOrdem: function() {

        var window = Ext.create('CRMax.view.ordem.Edit', {
            tipoOperacao: 2,
            y: CRMax.application.inIframe() ? 50 : undefined
        });
        window.show();
    },

    onClickButtonResgatar: function(grid, rowIndex, colIndex, item, e, record, row) {

        if (CRMax.globals.userInfo.CotistaPodeOperar !== true) {
            Ext.Msg.alert('Aviso', 'Não é possível realizar aplicações. Motivo: ' + CRMax.globals.userInfo.PendenciaCadastral);
            return false;
        }

        var dispensado = CRMax.globals.perfilCotista.Dispensado == 'Sim';
        var recusou = CRMax.globals.perfilCotista.Status == 'Recusado';
        var temPerfil = CRMax.globals.perfilCotista.Perfil !== null && CRMax.globals.perfilCotista.Perfil !== "" && CRMax.globals.perfilCotista.Perfil != 'Indefinido.';    

        var gridPanel = grid.up('gridpanel');
        if (!dispensado) {
            //Se cliente se recusou a preencher o perfil ou nao tem perfil preenchido, nao continuar
            if (recusou || !temPerfil) {
                Ext.Msg.alert('Aviso', CRMax.globals.msgsSuitability.msg_11);
                return false;
            }

            //Checar data de validade do perfil
            if (!CRMax.globals.perfilCotista.DentroValidade) {
                //Exibir mensagem de perfil desatualizado
                Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_12, function(btn) {
                    if (btn === 'yes') {
                        this.logMensagem(12, CRMax.globals.msgsSuitability.msg_12, 'OK');

                        //Exibir mensagem do termo de inadequacao
                        Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_19, function(btn) {
                            if (btn === 'yes') {
                                this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'OK');
                                gridPanel.showWindowResgate(record);
                            } else {
                                this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'Cancelar');
                                return false;
                            }
                        }, this);
                    } else {
                        this.logMensagem(12, CRMax.globals.msgsSuitability.msg_12, 'Cancelar');
                        return false;
                    }
                }, this);


            } else {
                gridPanel.showWindowResgate(record);
            }
        }
    },

    showWindowResgate: function(record) {
        var idCarteira = record.get('IdCarteira');
        var window = Ext.create('CRMax.view.operacao.Edit', {
            tipoOperacao: 2,
            y: CRMax.application.inIframe() ? 50 : undefined,
            idCarteira: idCarteira
        });
        window.show();
    },

    onClickButtonDetalhe: function(grid, rowIndex, colIndex, item, e, record, row) {
        CRMax.application.fireEvent('detalheclick', record);
    },

    loadStore: function() {
        this.store.load();
    }
});
Ext.define('CRMax.view.posicao.detalhe.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.posicaodetalhelist',
    hideHeaders: false,
    store: 'PosicoesDetalhadas',

    border: false,

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function () {
        
        this.columns = [
            {dataIndex: 'DataAplicacao', width: 80, header: 'Data Aplicação', type: 'date', xtype: 'datecolumn',format: 'd/m/Y' },
            {dataIndex: 'Quantidade', width: 150, header: 'Qtde. Cotas Atual', xtype: 'numbercolumn', format:'0,000.000000', align: 'right'},
            {dataIndex: 'CotaDia', width: 110, header: 'Valor Cota Atual (R$)', xtype: 'numbercolumn', format:'0,000.000000', align: 'right'},
            {dataIndex: 'ValorBruto', header: 'Saldo Bruto (R$)', width: 110, xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'ValorIR', header: 'IR (R$)', width: 90, xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'ValorIOF', header: 'IOF (R$)', width: 80, xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'ValorLiquido', header: 'Saldo Líquido (R$)', width: 100, xtype: 'numbercolumn', format:'0,000.00', align: 'right'}
        ];
        
        this.callParent(arguments);
    },

    loadStore: function () {
        this.store.load();
    },
    
    filter: function(filters){
        this.store.clearFilter(true);
        this.store.filter(filters);
    }
});
Ext.define('CRMax.view.extrato.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.extratolist',
    hideHeaders: false,
    store: 'Extratos',
    border: false,
    features: [{ftype:'grouping', groupHeaderTpl: '{name}'}],

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function () {
        
        this.columns = [
            {dataIndex: 'DataHistorico', width: 80, header: 'Data', type: 'date', xtype: 'datecolumn',format: 'd/m/Y' },
            {dataIndex: 'Historico', width: 80, header: 'Histórico'},
            {dataIndex: 'Quantidade', header: 'Qtde. Cotas', width: 90, xtype: 'numbercolumn', format:'0,000.0000', align: 'right'},
            {dataIndex: 'CotaDia', header: 'Valor Cota (R$)', width: 110, xtype: 'numbercolumn', format:'0,000.000000', align: 'right'},
            {dataIndex: 'ValorBruto', header: 'Valor Bruto (R$)', width: 100, xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'ValorIR', header: 'IR (R$)', width: 70, xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'ValorIOF', header: 'IOF (R$)', width: 60, xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'ValorLiquido', header: 'Valor Líquido (R$)', width: 110, xtype: 'numbercolumn', format:'0,000.00', align: 'right'}
        ];
        
        this.tbar = [{
             text: 'Imprimir',
             //iconCls: 'icon-print',
             scope: this,
             baseCls: '',
                   cls: 'btn-aplicacao',
                   
             handler: this.onButtonPrintWYSIWYGClick
         }];

        this.callParent(arguments);
    },

    loadStore: function () {
        this.store.load();
    },
    onButtonPrintWYSIWYGClick: function () {
        var el = this.getEl();
        var lastFilter = this.getStore().lastFilter;
        
        if(!lastFilter){
            lastFilter = [{property: 'periodo', value: 'currentMonth'}];
        }
        
        var dataFim = Ext.clone(CRMax.globals.today);
        var dataInicio = Ext.clone(dataFim);
        if(lastFilter[0].value == 'last7'){
            dataInicio.setDate(dataInicio.getDate() - 7);
        }else if(lastFilter[0].value == 'last30'){
            dataInicio.setDate(dataInicio.getDate() - 30);
        }else if(lastFilter[0].value == 'currentMonth'){
            dataInicio = new Date(dataInicio.getFullYear(), dataInicio.getMonth(), 1);
        }else if(lastFilter[0].value == 'period'){
            dataInicio = lastFilter[1].value;
			dataFim = lastFilter[2].value;
        }
        
        var html = [
                '<!DOCTYPE html>',
                '<html>',
                '<head>',
                    '<!--meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /-->',
                    '<meta charset="utf-8" />',
                    '<title>Extrato</title>',
                    '<link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />',
                    '<link href="Content/Site.css" rel="stylesheet" type="text/css" />',
                    '<link href="Content/print-grid.css" rel="stylesheet" type="text/css" />',
                    
                '</head>',
                '<body class="grid-print order-grid ">',
                '<div class="header">',
                    '<img src="Content/themes/base/images/logo_cliente.gif" alt="Logo" />',
                    '<div id="form-title">Extrato</div>',
                    '<div id="form-header-detail">Cliente: <b>{1}</b> <br />Período: <b>{2}</b> a <b>{3}</b></div>',
                '</div>',
                '<div class="div-wrapper" style="position: relative">',
                '{0}',
                '</div>',
                    '<script type="text/javascript">window.print()</script>',
                 '</body>',
                 '</html>'];

        html = Ext.String.format(html.join(''), el.dom.innerHTML, 
            CRMax.globals.userInfo.Login, Ext.Date.format(dataInicio, "j/M/Y"), Ext.Date.format(dataFim, "j/M/Y"));

        //open up a new printing window, write to it, print it and close
        var win = window.open('', 'printgrid');

        //document must be open and closed
        win.document.open();
        win.document.write(html);
        win.document.close();

        //win.print();

        /*if (Ext.isIE) {
            window.close();
        } else {
            win.close();
        }*/

    },
    setStoreFilter: function (filter) {

        var store = this.getStore();
        if (!store.lastFilter) {
            store.lastFilter = [];
        }

        if (Ext.encode(filter) === Ext.encode(store.lastFilter)) {
            //Prevent double submission if filter hasn't changed
            return;
        }

        store.clearFilter(true);
        store.lastFilter = Ext.clone(filter);
        store.filter(filter);

    }
});
Ext.define('CRMax.view.operacao.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.operacaolist',
    hideHeaders: false,
    store: 'Operacoes',
    border: false,

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function () {
        this.columns = [
            {dataIndex: 'DataOperacao', width: 80, header: 'Data', type: 'date', xtype: 'datecolumn',format: 'd/m/Y' },
            {dataIndex: 'NomeCarteira', width: 170, header: 'Fundo'},
            {dataIndex: 'TipoOperacaoString', width: 100, header: 'Tipo de Operação'},
            {dataIndex: 'Valor', width: 80, header: 'Valor (R$)', xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            { header: 'Tipo Liquidação',  dataIndex: 'FormaLiquidacao', width: 100 },
            { header: 'Conta Corrente',  dataIndex: 'ContaCorrente', width: 150 },
            {dataIndex: 'StatusProcessamento', 
                renderer: function(v, meta, record){
                    var status = v;
                    if(!(CRMax.globals.userInfo.Permissoes.IsGestor || CRMax.globals.userInfo.Permissoes.IsNotExterno)){
                        //Se usuario nao eh gestor ou eh externo, maquiar alguns dos status
                        if(v == 'Saldo Insuficiente' || v == 'Erro no Envio'){
                            status = 'Processando';
                        }
                    }
                    
                    return status;
                }, width: 100, header: 'Status'},
                
            {dataIndex: 'StatusCotizacao', width: 80, header: 'Cotização'},    
                
            {
                xtype:'actioncolumn',
                width:64,
                //hidden: !CRMax.globals.userInfo.Permissoes.PodeDistribuir,
                renderer: function(v, meta, record){
                    return (record.get('StatusProcessamento') == 'Agendada' || record.get('StatusProcessamento') == 'Saldo Insuficiente')? 
                        '<div class="x-action-col-0 x-action-col x-action-col-resgatar">cancelar</div>' : 
                        ''},
                tooltip: 'cancelar',
                scope: this,
                handler: this.onClickButtonRejeitar
            }
        ];
        
        if(CRMax.globals.userInfo.IdCotista == -1){
            this.columns[1].width = 125;
            this.columns.splice(1,0,{dataIndex: 'NomeCotista', width: 125, header: 'Cotista'});
        }

        this.tbar = [
                {
                   text: 'Imprimir',
                   //iconCls: 'icon-print',
                   scope: this,
                   baseCls: '',
                   cls: 'btn-aplicacao',
                   
                   handler: this.onButtonPrintWYSIWYGClick
                }
            ];

        this.callParent(arguments);
        
        CRMax.application.on('operacaoupdated', this.loadStore, this);
        CRMax.application.on('ordemupdated', this.loadStore, this);
        this.loadStore();
    },

    onClickButtonRejeitar: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var idOperacao = record.get('IdOperacao');
        gridPanel.updateStatusOrdem(idOperacao, 'rejeitar', 'Cancelamento efetuado com sucesso.');
    },

    updateStatusOrdem: function(idOperacao, action, msgSucesso){
        var gridPanel = this;
        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'controllers/operacaocotista/updateStatus.ashx',
            params: {
                idOperacao: idOperacao,
                action: action
            },
            success: function(response){
                Ext.Msg.alert('Aviso',msgSucesso); 
                //CRMax.application.notificationWindow.update(msgSucesso);
                //CRMax.application.notificationWindow.show();
                CRMax.application.fireEvent('operacaoupdated');
                gridPanel.getStore().load();
            }
        });
    },

    loadStore: function () {
        this.store.load();
    },
    
    onButtonPrintWYSIWYGClick: function () {
        var el = this.getEl();

        var html = [
                '<!DOCTYPE html>',
                '<html>',
                '<head>',
                    '<!--meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /-->',
                    '<meta charset="utf-8" />',
                    '<title>Extrato</title>',
                    '<link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />',
                    '<link href="Content/Site.css" rel="stylesheet" type="text/css" />',
                    '<link href="Content/print-grid.css" rel="stylesheet" type="text/css" />',
                    //'<link href="Content/print.css" rel="stylesheet" type="text/css" />',
                '</head>',
                '<body class="grid-print order-grid ">',
                /*'<div class="header">',
                    '<img src="Content/themes/base/images/logo_cliente.gif" alt="Logo" />',
                    '<div id="form-title">Extrato</div>',
                '</div>',*/
                '{0}',
                 '</body>',
                 '</html>'];

        html = Ext.String.format(html.join(''), el.dom.innerHTML);

        //open up a new printing window, write to it, print it and close
        var win = window.open('', 'printgrid');

        //document must be open and closed
        win.document.open();
        win.document.write(html);
        win.document.close();

        win.print();

        /*if (Ext.isIE) {
            window.close();
        } else {
            win.close();
        }*/

    },
    
    setStoreFilter: function (filter) {

        var store = this.getStore();
        if (!store.lastFilter) {
            store.lastFilter = [];
        }

        if (Ext.encode(filter) === Ext.encode(store.lastFilter)) {
            //Prevent double submission if filter hasn't changed
            return;
        }

        store.clearFilter(true);
        store.lastFilter = Ext.clone(filter);
        store.filter(filter);

    }
});
Ext.define('CRMax.view.ordem.pgbl.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ordempgbllist',
    hideHeaders: false,
    store: 'OrdensPgbl',
    border: false,

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function () {
           this.tbar = [
                {
                   text: 'Imprimir',
                   //iconCls: 'icon-print',
                   scope: this,
                   baseCls: '',
                   cls: 'btn-aplicacao',
                   handler: this.onButtonPrintWYSIWYGClick
                }
            ];
        
        this.columns = [
            {dataIndex: 'DataOperacao', width: 100, header: 'Data', type: 'date', xtype: 'datecolumn',format: 'd/m/Y' },
            {dataIndex: 'NomeCarteira', flex: 1, header: 'Fundo'},
            {dataIndex: 'IdCotista', width: 100, header: 'IdCotista'},
            {dataIndex: 'NomeCotista', flex: 1, header: 'Cotista'},
            {dataIndex: 'TipoOperacaoString', flex: 1, header: 'Tipo'},
            {dataIndex: 'Valor', width: 90, header: 'Valor', xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            
             { header: 'Tipo Liquidação',  dataIndex: 'FormaLiquidacao', width: 100 },
            { header: 'Conta Corrente',  dataIndex: 'ContaCorrente', width: 150 },
            
            {dataIndex: 'StatusProcessamento', width: 100, header: 'Status'},
            {
                xtype:'actioncolumn',
                width:55,
                hidden: !CRMax.globals.userInfo.Permissoes.PodeAprovar,
                renderer: function(v, meta, record){
                    return record.get('Status') !== CRMax.globals.StatusOrdemTesourariaPorto.PGBLIntegrado ? '' : 
                        '<div class="x-action-col-0 x-action-col x-action-col-aplicar">aprovar</div>';},
                tooltip: 'aprovar',
                scope: this,
                handler: this.onClickButtonAprovar
            },{
                xtype:'actioncolumn',
                width:64,
                hidden: !CRMax.globals.userInfo.Permissoes.PodeAprovar,
                renderer: function(v, meta, record){
                    return record.get('Status') !== CRMax.globals.StatusOrdemTesourariaPorto.PGBLIntegrado ? '' : 
                        '<div class="x-action-col-0 x-action-col x-action-col-resgatar">cancelar</div>'},
                tooltip: 'cancelar',
                scope: this,
                handler: this.onClickButtonRejeitar
            }
        ];

        this.callParent(arguments);
        
        CRMax.application.on('ordemupdated', function(){this.getStore().load()}, this);
        this.getStore().load();
    },


    
        onButtonPrintWYSIWYGClick: function () {
        var el = this.getEl();

        var html = [
                '<!DOCTYPE html>',
                '<html>',
                '<head>',
                    '<!--meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /-->',
                    '<meta charset="utf-8" />',
                    '<title>Extrato</title>',
                    '<link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />',
                    '<link href="Content/Site.css" rel="stylesheet" type="text/css" />',
                    '<link href="Content/print-grid.css" rel="stylesheet" type="text/css" />',
                    //'<link href="Content/print.css" rel="stylesheet" type="text/css" />',
                '</head>',
                '<body class="grid-print order-grid ">',
                /*'<div class="header">',
                    '<img src="Content/themes/base/images/logo_cliente.gif" alt="Logo" />',
                    '<div id="form-title">Extrato</div>',
                '</div>',*/
                '{0}',
                 '</body>',
                 '</html>'];

        html = Ext.String.format(html.join(''), el.dom.innerHTML);

        //open up a new printing window, write to it, print it and close
        var win = window.open('', 'printgrid');

        //document must be open and closed
        win.document.open();
        win.document.write(html);
        win.document.close();

        win.print();

        /*if (Ext.isIE) {
            window.close();
        } else {
            win.close();
        }*/

    },
    
    onClickButtonAprovar: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var idOperacao = record.get('IdOperacao');
        gridPanel.updateStatusOrdem(idOperacao, 'aprovar', 'Ordem Aprovada');
    },

    onClickButtonRejeitar: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var idOperacao = record.get('IdOperacao');
        gridPanel.updateStatusOrdem(idOperacao, 'rejeitar', 'Cancelamento efetuado com sucesso.');
    },
    
    setStoreFilter: function (filter) {

        var store = this.getStore();
        if (!store.lastFilter) {
            store.lastFilter = [];
        }

        if (Ext.encode(filter) === Ext.encode(store.lastFilter)) {
            //Prevent double submission if filter hasn't changed
            return;
        }

        store.clearFilter(true);
        store.lastFilter = Ext.clone(filter);
        store.filter(filter);

    },
    
    updateStatusOrdem: function(idOperacao, action, msgSucesso){
        var gridPanel = this;
        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'controllers/ordemtesouraria/updateStatus.ashx',
            params: {
                idOperacao: idOperacao,
                action: action
            },
            success: function(response){
                Ext.Msg.alert('Aviso',msgSucesso); 
                
                //CRMax.application.notificationWindow.update(msgSucesso);
                //CRMax.application.notificationWindow.show();
                CRMax.application.fireEvent('ordemupdated');
                gridPanel.getStore().load();
            }
        });
    }
});
Ext.define('CRMax.view.aprovacao.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ordemlist',
    hideHeaders: false,
    store: 'OrdensTesouraria',
    border: false,

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function () {
           this.tbar = [
                {
                   text: 'Imprimir',
                   //iconCls: 'icon-print',
                   scope: this,
                   baseCls: '',
                   cls: 'btn-aplicacao',
                   handler: this.onButtonPrintWYSIWYGClick
                }
            ];
        
        this.columns = [
            {dataIndex: 'DataOperacao', width: 80, header: 'Data', type: 'date', xtype: 'datecolumn',format: 'd/m/Y' },
            {dataIndex: 'NomeCotista', flex: 1, header: 'Cotista'},
            {dataIndex: 'TipoOperacaoString', width: 140, header: 'Tipo'},
            {dataIndex: 'Valor', width: 50, header: 'Valor (R$)', xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            { header: 'Tipo Liquidação',  dataIndex: 'FormaLiquidacao', width: 100 },
            { header: 'Conta Corrente',  dataIndex: 'ContaCorrente', width: 150 },
            {
                xtype:'actioncolumn',
                width:55,
                renderer: function(v, meta, record){
                    return '<div class="x-action-col-0 x-action-col x-action-col-aplicar">aprovar</div>';
                },
                tooltip: 'aprovar',
                scope: this,
                handler: this.onClickButtonAprovar
            },{
                xtype:'actioncolumn',
                width:64,
                renderer: function(v, meta, record){
                    return '<div class="x-action-col-0 x-action-col x-action-col-resgatar">cancelar</div>'
                },
                tooltip: 'cancelar',
                scope: this,
                handler: this.onClickButtonRejeitar
            }
        ];

        this.callParent(arguments);
        
        CRMax.application.on('ordemupdated', function(){this.getStore().load()}, this);
        this.getStore().load();
    },

        onButtonPrintWYSIWYGClick: function () {
        var el = this.getEl();

        var html = [
                '<!DOCTYPE html>',
                '<html>',
                '<head>',
                    '<!--meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /-->',
                    '<meta charset="utf-8" />',
                    '<title>Extrato</title>',
                    '<link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />',
                    '<link href="Content/Site.css" rel="stylesheet" type="text/css" />',
                    '<link href="Content/print-grid.css" rel="stylesheet" type="text/css" />',
                    //'<link href="Content/print.css" rel="stylesheet" type="text/css" />',
                '</head>',
                '<body class="grid-print order-grid ">',
                /*'<div class="header">',
                    '<img src="Content/themes/base/images/logo_cliente.gif" alt="Logo" />',
                    '<div id="form-title">Extrato</div>',
                '</div>',*/
                '{0}',
                 '</body>',
                 '</html>'];

        html = Ext.String.format(html.join(''), el.dom.innerHTML);

        //open up a new printing window, write to it, print it and close
        var win = window.open('', 'printgrid');

        //document must be open and closed
        win.document.open();
        win.document.write(html);
        win.document.close();

        win.print();

        /*if (Ext.isIE) {
            window.close();
        } else {
            win.close();
        }*/

    },
    
    onClickButtonAprovar: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var window = gridPanel.getWindowDistribuicao();
        
        var idOperacao = record.get('IdOperacao');
        window.updateFilterOperacaoMae(idOperacao);
        window.setActionAprovar();
        window.show();
    },

    onClickButtonRejeitar: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var idOperacao = record.get('IdOperacao');
        gridPanel.updateStatusOrdem(idOperacao, 'rejeitar', 'Cancelamento efetuado com sucesso.');
    },

    getWindowDistribuicao: function(){
        if(!this.windowDistribuicao){
            this.windowDistribuicao = Ext.create('CRMax.view.ordem.distribute.List');
        }
        
        return this.windowDistribuicao;
    },


    setStoreFilter: function (filter) {

        var store = this.getStore();
        if (!store.lastFilter) {
            store.lastFilter = [];
        }

        if (Ext.encode(filter) === Ext.encode(store.lastFilter)) {
            //Prevent double submission if filter hasn't changed
            return;
        }

        store.clearFilter(true);
        store.lastFilter = Ext.clone(filter);
        store.filter(filter);

    },
    
    updateStatusOrdem: function(idOperacao, action, msgSucesso){
        var gridPanel = this;
        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'controllers/ordemtesouraria/updateStatus.ashx',
            params: {
                idOperacao: idOperacao,
                action: action
            },
            success: function(response){
                Ext.Msg.alert('Aviso',msgSucesso); 
                
                //CRMax.application.notificationWindow.update(msgSucesso);
                //CRMax.application.notificationWindow.show();
                CRMax.application.fireEvent('ordemupdated');
                gridPanel.getStore().load();
            }
        });
    }
});
Ext.define('CRMax.view.ordem.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ordemlist',
    hideHeaders: false,
    border: false,

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function () {
    
           this.store = Ext.create(CRMax.store.OrdensTesouraria);
    
           this.tbar = [
                {
                   text: 'Imprimir',
                   //iconCls: 'icon-print',
                   scope: this,
                   baseCls: '',
                   cls: 'btn-aplicacao',
                   handler: this.onButtonPrintWYSIWYGClick
                }
            ];
        
        this.columns = [
                {dataIndex: 'DataOperacao', width: 80, header: 'Data', type: 'date', xtype: 'datecolumn',format: 'd/m/Y' }
            ];
        
        if(this.multiCotista === true){
            this.columns.push(
                {dataIndex: 'NomeCotista', flex: 1, header: 'Cotista'}
            );
        }
            
        this.columns.push(
            {dataIndex: 'TipoOperacaoString', width: 80, header: 'Tipo'},
            {dataIndex: 'Valor', width: 90, header: 'Valor (R$)', xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'StatusProcessamento', width: 100, header: 'Status',
                renderer: function(v, meta, record){
                    var observacao = record.get('Observacao');
                    if(observacao && observacao.length > 0){
                        v += ' <span class="cancel-info" data-qtip="' + observacao + '"> </span>';
                    }
                    return v;
                }
            },
            { header: 'Tipo Liquidação',  dataIndex: 'FormaLiquidacao' },
            { header: 'Conta Corrente',  dataIndex: 'ContaCorrente'}
            );
        
        
        if(!this.actionStatus){
            this.columns.push({
                xtype:'actioncolumn',
                width:64,
                hidden: !CRMax.globals.userInfo.Permissoes.PodeBoletar,
                renderer: function(v, meta, record){
                    var status = record.get('Status');
                    return status === CRMax.globals.StatusOrdemTesourariaPorto.Digitado && !CRMax.globals.userInfo.Permissoes.PodeDistribuir ?  
                        '<div class="x-action-col-0 x-action-col x-action-col-resgatar">cancelar</div>' : ''},
                tooltip: 'cancelar',
                scope: this,
                handler: this.onClickButtonRejeitar
            });
        }
        
        if(!this.actionStatus || this.actionStatus === 'aprovacao'){    
            this.columns.push({
                xtype:'actioncolumn',
                width:55,
                hidden: !CRMax.globals.userInfo.Permissoes.PodeAprovar,
                renderer: function(v, meta, record){
                    return record.get('Status') !== CRMax.globals.StatusOrdemTesourariaPorto.Distribuido ? '' : 
                        '<div class="x-action-col-0 x-action-col x-action-col-aplicar">aprovar</div>';},
                tooltip: 'aprovar',
                scope: this,
                handler: this.onClickButtonAprovar
            },{
                xtype:'actioncolumn',
                width:64,
                hidden: !CRMax.globals.userInfo.Permissoes.PodeAprovar,
                renderer: function(v, meta, record){
                    var status = record.get('Status');
                    
                    return record.get('Status') !== CRMax.globals.StatusOrdemTesourariaPorto.Distribuido ? '' : 
                        '<div class="x-action-col-0 x-action-col x-action-col-resgatar">cancelar</div>'},
                tooltip: 'cancelar',
                scope: this,
                handler: this.onClickButtonRejeitar
            }
            );    
        }
        
        if(!this.actionStatus || this.actionStatus === 'distribuicao'){    
            this.columns.push({
                xtype:'actioncolumn',
                width:64,
                hidden: !CRMax.globals.userInfo.Permissoes.PodeDistribuir,
                renderer: function(v, meta, record){
                    return record.get('Status') !== CRMax.globals.StatusOrdemTesourariaPorto.Digitado ? '' : 
                        '<div class="x-action-col-0 x-action-col x-action-col-info">distribuir</div>'},
                tooltip: 'distribuir',
                scope: this,
                handler: this.onClickButtonDistribuir
            },{
                xtype:'actioncolumn',
                width:64,
                hidden: !CRMax.globals.userInfo.Permissoes.PodeDistribuir,
                renderer: function(v, meta, record){
                    var status = record.get('Status');
                    
                    return record.get('Status') !== CRMax.globals.StatusOrdemTesourariaPorto.Digitado ? '' : 
                        '<div class="x-action-col-0 x-action-col x-action-col-resgatar">cancelar</div>'},
                tooltip: 'distribuir',
                scope: this,
                handler: this.onClickButtonRejeitarDistribuicao
            });
        }
        
        this.columns.push({
                xtype:'actioncolumn',
                width:64,
                renderer: function(v, meta, record){
                    return record.get('IsDistribuida') ? '<div class="x-action-col-0 x-action-col x-action-col-info">ordem</div>' : '' 
                        },
                tooltip: '+info',
                scope: this,
                handler: this.onClickButtonInfoDistribuicao
            }
        );

        this.callParent(arguments);
        
        CRMax.application.on('ordemupdated', function(){this.getStore().load()}, this);
        this.getStore().load();
    },


    
        onButtonPrintWYSIWYGClick: function () {
        var el = this.getEl();

        var html = [
                '<!DOCTYPE html>',
                '<html>',
                '<head>',
                    '<!--meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /-->',
                    '<meta charset="utf-8" />',
                    '<title>Extrato</title>',
                    '<link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />',
                    '<link href="Content/Site.css" rel="stylesheet" type="text/css" />',
                    '<link href="Content/print-grid.css" rel="stylesheet" type="text/css" />',
                    //'<link href="Content/print.css" rel="stylesheet" type="text/css" />',
                '</head>',
                '<body class="grid-print order-grid ">',
                /*'<div class="header">',
                    '<img src="Content/themes/base/images/logo_cliente.gif" alt="Logo" />',
                    '<div id="form-title">Extrato</div>',
                '</div>',*/
                '{0}',
                 '</body>',
                 '</html>'];

        html = Ext.String.format(html.join(''), el.dom.innerHTML);

        //open up a new printing window, write to it, print it and close
        var win = window.open('', 'printgrid');

        //document must be open and closed
        win.document.open();
        win.document.write(html);
        win.document.close();

        win.print();

        /*if (Ext.isIE) {
            window.close();
        } else {
            win.close();
        }*/

    },
    
    onClickButtonDistribuir: function(grid, rowIndex, colIndex, item, e, record, row){
        var window = Ext.create('CRMax.view.ordem.Distribute', {
            idOperacao: record.get('IdOperacao'),
            tipoOperacao: record.get('TipoOperacao'),
            saldoTotal: record.get('Valor'),
            formaLiquidacao:  record.get('FormaLiquidacao'),
            contaCorrente: record.get('ContaCorrente'),
            tipoOperacaoString: record.get('TipoOperacaoString'),
            idCotista: record.get('IdCotista'),
            nomeCotista: record.get('NomeCotista')
        });
        window.show();
    },
    
    onClickButtonInfoDistribuicao: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var window = gridPanel.getWindowDistribuicao();
        window.setTitle('Distribuição da Ordem');
        
        var idOperacao = record.get('IdOperacao');
        window.updateFilterOperacaoMae(idOperacao);
        window.hideActionButtons();
        window.show();
    },

    getWindowDistribuicao: function(){
        if(!this.windowDistribuicao){
            this.windowDistribuicao = Ext.create('CRMax.view.ordem.distribute.List');
        }
        
        return this.windowDistribuicao;
    },
    
    onClickButtonAprovar: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var window = gridPanel.getWindowDistribuicao();

        window.setTitle('Aprovar Ordem - ' + record.get('NomeCotista'));
        var idOperacao = record.get('IdOperacao');
        window.updateFilterOperacaoMae(idOperacao);
        window.setActionAprovar();
        window.show();
    },

    onClickButtonRejeitar: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var idOperacao = record.get('IdOperacao');
        //gridPanel.updateStatusOrdem(idOperacao, 'rejeitar', 'Cancelamento efetuado com sucesso.');
        
        Ext.Msg.prompt('Cancelamento', 'Por favor, informe o motivo do cancelamento desta ordem:', function(btn, motivoCancelamento){
            if (btn == 'ok'){
                gridPanel.updateStatusOrdem(idOperacao, 'rejeitar', 'Ordem Cancelada', motivoCancelamento);
            }
        }, this, true);
        
    },

    onClickButtonRejeitarDistribuicao: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var idOperacao = record.get('IdOperacao');
        
        Ext.Msg.prompt('Cancelamento', 'Por favor, informe o motivo do cancelamento desta ordem:', function(btn, motivoCancelamento){
            if (btn == 'ok'){
                gridPanel.updateStatusOrdem(idOperacao, 'rejeitar', 'Ordem Cancelada', motivoCancelamento);
            }
        }, this, true);
        
        
        
        /*var gridPanel = grid.up('gridpanel');
        var window = gridPanel.getWindowDistribuicao();
        
        var idOperacao = record.get('IdOperacao');
        window.updateFilterOperacaoMae(idOperacao);
        window.setActionCancelar();
        window.show();*/
    },
    
    setStoreFilter: function (filter) {

        var store = this.getStore();
        if (!store.lastFilter) {
            store.lastFilter = [];
        }

        if (Ext.encode(filter) === Ext.encode(store.lastFilter)) {
            //Prevent double submission if filter hasn't changed
            return;
        }

        store.clearFilter(true);
        store.lastFilter = Ext.clone(filter);
        store.filter(filter);

    },
    
    updateStatusOrdem: function(idOperacao, action, msgSucesso, motivoCancelamento){
        var gridPanel = this;
        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'controllers/ordemtesouraria/updateStatus.ashx',
            params: {
                idOperacao: idOperacao,
                action: action,
                motivoCancelamento: motivoCancelamento
            },
            success: function(response){
                Ext.Msg.alert('Aviso',msgSucesso); 
                
                //CRMax.application.notificationWindow.update(msgSucesso);
                //CRMax.application.notificationWindow.show();
                CRMax.application.fireEvent('ordemupdated');
                gridPanel.getStore().load();
            }
        });
    }
});
Ext.define('CRMax.view.cotista.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.cotistaedit',
    title: ' ',
    autoScroll: true,

    modal: true,
    shadow: false,
    resizable: false,
    border: false,
    draggable: false,
    closable: false,
    width: 500,
    layout: 'fit',
    height: 400,

    initComponent: function () {
        
        this.grid = Ext.create('CRMax.view.cotista.List', {title: 'Selecionar um Cotista na lista abaixo:'});
        
        this.items = [this.grid];
        
        /*this.tools = [{
            type: 'close',
            width: 200,
            tooltip: 'Sair',
            // hidden:true,
            handler: function(event, toolEl, panelHeader) {
                // refresh logic
            }
        }];*/
        
        this.header = {
            xtype: 'header',
            titlePosition: 0,
            defaults: {
                margin: '0 10px'
            },
            items: [
                {
                    xtype: 'component',
                    html: '<div style="color: #fff; font-size: 14px;">Cotistas</div>',
                    width: 400
                },
               
                {
                    xtype: 'button',
                    id: 'button-logoff-2',
                    iconCls: 'icon-logoff',
                    text: 'Sair'
                }
            ]
        }
        
        
        
        if( (CRMax.globals.userInfo.MultiCotista === true && CRMax.globals.isTesouraria !== true)||
            ((CRMax.globals.userInfo.Permissoes.PodeDistribuir === true || 
            CRMax.globals.userInfo.Permissoes.PodeAprovar === true) && CRMax.globals.isTesouraria === true)){
            
            var buttonLabel = CRMax.globals.isTesouraria === true ? 'Editar Ordens' : 'Todos';
            
            this.buttons = [{
                baseCls: '',
                cls: 'btn btn-inverse',
                text: buttonLabel,
                scope: this,
                handler: this.irParaAprovacao
            }]
         }

        this.callParent(arguments);
    },
    
    irParaAprovacao: function(){
        var idCotista = -1;
        
        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'controllers/cotista/select.ashx',
            params: {
                idCotista: idCotista
            },
            success: function(response){
                window.location.reload();
            }
        });
    }
});
Ext.define('CRMax.controller.MainNavigator', {
    extend: 'Ext.app.Controller',
    views: ['MainNavigator', 'HomePanel', 'Settings'],
    refs: [{ ref: 'mainNavigator', selector: 'mainnavigator'}],
    init: function () {
        this.control({
            '#MainNavigator': {
                itemclick: this.onNodeSelection/*,
                select: this.onNodeSelection*/
            }
        })
    },

    onNodeSelection: function (objParam, record) {
        Ext.History.add("/" + record.get('id'));
    },

    selectNode: function (nodeId) {
        /*Ext.History.add("/" + record.get('id'));
        var mainNavigator = this.getMainNavigator();
        var record = mainNavigator.getRootNode().findChild('id', nodeId, true);
        mainNavigator.getSelectionModel().select(record, false, false);*/
    },

    selectNodeSpecial: function (nodeId) {
        Ext.History.add("/" + nodeId);
    }
});
Ext.define('CRMax.controller.UserProfile', {
    extend: 'Ext.app.Controller',
    init: function () {
        this.control({
            '#button-logoff': {
                click: this.onUserLogOff
            },
            '#button-logoff-2': {
                click: this.onUserLogOff
            }
        });
    },

    onUserLogOff: function () {
        window.location = CRMax.globals.baseDirectory + 'login/LogOff.aspx?isTesouraria=' + (CRMax.globals.isTesouraria ? "true" : "false");
    }

});
Ext.define('CRMax.controller.Settings', {
    extend: 'Ext.app.Controller',
    views: ['Settings', 'cotista.List', 'cotista.Edit'],
    refs: [{ ref: 'settingsPanel', selector: 'settings'}],
    //stores: ['Users'],

    init: function () {
        this.control({
            '#settings > dataview': {
                itemclick: this.onDataViewItemClick,
                afterrender: this.onDataViewAfterRender
            },

            '#user-list': {
                itemclick: this.onUserListItemClick
            },

            '#button-create-user': {
                click: this.onButtonCreateUserClick
            },

            '#button-edit-user': {
                click: this.onButtonEditUserClick
            },

            '#user-form button[action=save]': {
                click: this.onUserFormButtonSaveClick
            },

            '#user-form button[action=cancel]': {
                click: this.onUserFormButtonCancelClick
            }
        });

        this.application.on({
            userupdated: this.onUserUpdated,
            scope: this
        });

    },

    onDataViewAfterRender: function (dataview) {
        dataview.select(0);
        this.addUsersGrid();
    },

    onButtonEditUserClick: function (button) {
        if(CRMax.globals.userInfo.MultiCotista === true){
            this.editCurrentUser();
        }
    },

    editCurrentUser: function () {
        var win = new CRMax.view.cotista.Edit();
        win.show();
    },

    loadUser: function (record, formPanel) {
        formPanel.loadRecord(record);

    },

    onUserUpdated: function () {
        this.getUsersStore().load();
    },

    closeUserWindow: function (window) {
        window.close();
    },

    onUserFormButtonCancelClick: function (button) {
        var window = button.up('window');
        this.closeUserWindow(window);
    },

    onButtonCreateUserClick: function () {
        this.onCreateUser();
    },

    onCreateUser: function () {
        var userWin = new CRMax.view.user.Edit();
        userWin.show();

    },

    onUserListItemClick: function (view, record, htmlElement, index, eventObject, eOpts) {
        var targetEl = Ext.get(eventObject.target);

        if (targetEl.hasCls('resend-invitation-link')) {
            this.resendInvitationLink(record.data.UserId);
        }
    },

    resendInvitationLink: function (userId) {
        var model = Ext.ModelManager.getModel('CRMax.model.User')
        model.load(userId, {
            scope: this,
            success: function (user) {
                user.data.UIAction = 'ResendInvitationLink';
                this.saveUser(user, null, 'Convite reenviado.');
            }
        });
    },

    onDataViewItemClick: function (view, record) {
        if (record.data.id == 'users') {
            this.addUsersGrid();
        } else if (record.data.id == 'deal-categories') {
            this.editDealCategories();
        } else if (record.data.id == 'task-categories') {
            this.editTaskCategories();
        } else if (record.data.id == 'custom-fields') {
            //this.addCustomFieldsGrid();
            this.editCustomFields();
        }
    },

    editDealCategories: function () {
        var winDealCategories = Ext.create('Ext.window.Window', {
            title: 'Categorias de negócio',
            //layout: 'fit',
            modal: true,
            shadow: false,
            resizable: false,
            border: false,
            draggable: false,
            width: 500,
            cls: 'crmax-grid-panel',


            items: {
                xtype: 'dealcategorylist',
                height: 400
            },

            buttons: [
        {
            baseCls: '',
            cls: 'btn',
            text: 'Finalizar edição',
            handler: function (btn) {
                winDealCategories.close();
            }
        }]

        });

        winDealCategories.show();

    },

    editTaskCategories: function () {
        var winTaskCategories = Ext.create('Ext.window.Window', {
            title: 'Categorias de tarefa',
            //layout: 'fit',
            modal: true,
            shadow: false,
            resizable: false,
            border: false,
            draggable: false,
            width: 500,
            cls: 'crmax-grid-panel',


            items: {
                xtype: 'taskcategorylist',
                height: 400
            },

            buttons: [
        {
            baseCls: '',
            cls: 'btn',
            text: 'Finalizar edição',
            handler: function (btn) {
                winTaskCategories.close();
            }
        }]

        });

        winTaskCategories.show();

    },

    editCustomFields: function () {
        this.addCardPanel({
            itemId: 'customfield-list',
            title: 'Campos Personalizados',
            xtype: 'customfieldlist',
            hideHeaders: true
        });


        /*var winCustomFields = Ext.create('Ext.window.Window', {
        title: 'Campos Personalizados',
        //layout: 'fit',
        modal: true,
        shadow: false,
        resizable: false,
        border: false,
        draggable: false,
        width: 500,
        cls: 'crmax-grid-panel',
            
        items: {
        xtype: 'customfieldlist',
        height: 400
        },

        buttons: [
        {
        baseCls: '',
        cls: 'btn',
        text: 'Finalizar edição',
        handler: function (btn) {
        winCustomFields.close();
        }
        }]

        });

        winCustomFields.show();*/

    },

    addUsersGrid: function () {
        this.addCardPanel({
            itemId: 'user-list',
            title: 'Usuários',
            xtype: 'userlist',
            hideHeaders: true
        });
    },

    addCardPanel: function (componentConfig, removeBeforeAdding) {
        var settingsPanel = this.getSettingsPanel();
        var cards = settingsPanel.down('#cards');
        var cardLayout = cards.getLayout();
        var cardItems = cardLayout.getLayoutItems();

        //try to find component
        var component = null;
        for (var cardCount = 0; cardCount < cardItems.length; cardCount++) {
            if (cardItems[cardCount].itemId == componentConfig.itemId) {
                component = cardItems[cardCount];
                if (removeBeforeAdding === true) {
                    component.destroy();
                    component = null;
                }
                break;
            }
        }

        if (!component) {
            component = cards.add(componentConfig);
        }

        cardLayout.setActiveItem(componentConfig.itemId);
        return component;
    },

    onUserFormButtonSaveClick: function (button) {
        var formPanel = button.up('form'),
        formWin = formPanel.up('window'),
        form = formPanel.getForm(),
        user = form.getRecord(),
        values = form.getValues();

        if (!user) {
            //creating a new task
            user = Ext.create('CRMax.model.User', values);
        } else {
            user.set(values);
        }

        if (!form.isValid()) {
            return false;
        }

        this.saveUser(user, formWin);
    },

    saveUser: function (user, formWin, customUpdateMessage) {
        user.save({
            scope: this,
            success: function () {
                this.application.fireEvent('userupdated');

                Ext.Msg.alert('Aviso',customUpdateMessage || 'Usuário atualizado.'); 

                //this.application.notificationWindow.update(customUpdateMessage || 'Usuário atualizado.');
                //this.application.notificationWindow.show();

                formWin && this.closeUserWindow(formWin);
            },
            failure: function (record, operation) {
                Ext.Msg.alert('Aviso',operation.request.scope.reader.jsonData.errorMessage);
            }
        });
    }
});
