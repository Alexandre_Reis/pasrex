Ext.define('Ext.ux.panel.FileInfoPanel', {
    extend: 'Ext.Component',
    alias: 'widget.fileinfopanel',

    initComponent: function () {
        this.tpl = new Ext.XTemplate(
            '<ul class="attachments">',
                '<tpl for="FileData">',
                    '<li>',
                        '<a href="{[this.buildFileLink(parent, xindex-1)]}" target="_blank" class="attachment" type="text/plain">',
                            '<span class="attachment-icon attachment-icon-{[this.getFileExtension(parent, xindex-1)]}"></span> {Name}',
                        '</a>',
                    '</li>',
                '</tpl>',
            '</ul>',
            {
                disableFormats: true,
                buildFileLink: function (record, index) {
                    return CRMax.globals.fileDownloadPath + '/' + record.FileData[index].FileId;
                },
                getFileExtension: function (record, index) {
                    var GENERIC_EXTENSION = "gen";
                    var supportedExtensions = {
                    /*    'pdf': true,
                        'doc': true,
                        'gif': true,*/
                        'png': true
                    };

                    var fileName = record.FileData[index].Name.toLowerCase();

                    var extension = (/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName) : undefined;

                    if (!extension || supportedExtensions[extension] !== true) {
                        return GENERIC_EXTENSION;
                    } else {
                        return extension;
                    }

                }
            }
          );


        if (this.fileData) {
            this.loadData(this.fileData);
        }

        this.callParent();
    },

    loadData: function (fileData) {
        this.fileData = fileData;
        this.update({ FileData: fileData });
    }

});