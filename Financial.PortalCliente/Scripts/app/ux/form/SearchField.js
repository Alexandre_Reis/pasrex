Ext.define('Ext.ux.form.SearchField', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.searchfield',

    displayField: 'Name',
    typeAhead: false,
    hideLabel: true,
    hideTrigger: true,
    queryMode: 'remote',
    listeners: {
        // delete the previous query in the beforequery event or set
        // combo.lastQuery = null (this will reload the store the next time it expands)
        beforequery: function(qe){
            delete qe.combo.lastQuery;
        }
    },
    minChars: 1,
    autoSelect: true,
    triggerAction: 'query',

    listConfig: {
        loadingText: 'Pesquisando...',
        emptyText: 'Nenhum resultado encontrado'

        /*getInnerTpl: function() {
        return '<a class="search-item" href="http://www.sencha.com/forum/showthread.php?t={topicId}&p={id}">' +
        '<h3><span>{[Ext.Date.format(values.lastPost, "M j, Y")]}<br />by {author}</span>{title}</h3>' +
        '{excerpt}' +
        '</a>';
        }*/
    },

    initComponent: function () {
        this.store = Ext.create('Ext.data.Store', {
            fields: ['DocumentTypeId', 'Name', 'DocumentId'],
            autoLoad: false,

            proxy: {
                type: 'ajax',
                url: 'search/list',
                reader: {
                    type: 'json',
                    root: 'Collection',
                    successProperty: 'success'
                }
            }
        });

        this.on({
            select: { fn: 'onOptionSelect', scope: this }
        });

        this.callParent();
    },

    onOptionSelect: function (combo, records, eOpts) {
        if (!records || !records.length) {
            return;
        }

        var record = records[0];
        var id = record.data.DocumentId;

        if (record.data.DocumentTypeId == CRMax.globals.enums.documentType.Person.Id) {
            Ext.History.add("/person/" + id);
        } else if (record.data.DocumentTypeId == CRMax.globals.enums.documentType.Company.Id) {
            Ext.History.add("/company/" + id);
        } else {
            Ext.History.add("/deal/" + id);
        }

        combo.setValue(null);
    }

});