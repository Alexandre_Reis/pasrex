﻿Ext.define('Ext.ux.form.HoverButton', {
    extend: 'Ext.Button',
    alias: 'widget.hoverButton',
    isOver: false,
    hideDelay: 250,
    showDelay: 0,

    applyListeners: function (menu, cfg) {
        Ext.apply(menu, cfg);
        Ext.each(menu.items, function (item, idx, allItems) {
            if (item.menu) this.applyListeners(item.menu, cfg);
        }, this);
    },

    initComponent: function () {
        var config = {},
            menuConfig = {},
            me = this;

        me.delayedShowMenu = new Ext.util.DelayedTask(function () {
            if (!me.isOver) return;
            me.showMenu();
        }, this);

        me.delayedHideMenu = new Ext.util.DelayedTask(function () {
            if (me.isOver) return;
            me.hideMenu();
        });

        if (Ext.isDefined(this.initialConfig.menu)) {
            config = {
                listeners: {
                    mouseover: {
                        scope: me,
                        fn: function (b) {
                            me.isOver = true;
                            me.delayedShowMenu.delay(me.showDelay);
                        }
                    },
                    mouseout: {
                        scope: me,
                        fn: function (b) {
                            me.isOver = false;
                            me.delayedHideMenu.delay(me.hideDelay);
                        }
                    }
                }
            };

            menuConfig = {
                listeners: {
                    mouseover: {
                        scope: me,
                        fn: function (menu, item, e) {
                            me.delayedHideMenu.cancel();
                        }
                    },
                    mouseenter: {
                        scope: me,
                        fn: function (menu, e) {
                            me.delayedHideMenu.cancel();
                        }
                    },
                    mouseleave: {
                        scope: me,
                        fn: function (menu, e) {
                            me.delayedHideMenu.delay(me.hideDelay);
                        }
                    }
                }
            };

            //apply mouseover/leave listeners to all submenus recursively
            me.applyListeners(me.menu, menuConfig);
        }

        Ext.apply(me, Ext.apply(me.initialConfig, config));
        //Ext.HoverButton.superclass.initComponent.apply(me, arguments);
        this.callParent();
    }
});