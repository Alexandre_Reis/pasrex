﻿Ext.define('CRMax.controller.UserProfile', {
    extend: 'Ext.app.Controller',
    init: function () {
        this.control({
            '#button-logoff': {
                click: this.onUserLogOff
            },
            '#button-logoff-2': {
                click: this.onUserLogOff
            }
        });
    },

    onUserLogOff: function () {
        window.location = CRMax.globals.baseDirectory + 'login/LogOff.aspx?isTesouraria=' + (CRMax.globals.isTesouraria ? "true" : "false");
    }

});