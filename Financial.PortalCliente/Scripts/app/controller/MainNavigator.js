﻿Ext.define('CRMax.controller.MainNavigator', {
    extend: 'Ext.app.Controller',
    views: ['MainNavigator', 'HomePanel', 'Settings'],
    refs: [{ ref: 'mainNavigator', selector: 'mainnavigator'}],
    init: function () {
        this.control({
            '#MainNavigator': {
                itemclick: this.onNodeSelection/*,
                select: this.onNodeSelection*/
            }
        })
    },

    onNodeSelection: function (objParam, record) {
        Ext.History.add("/" + record.get('id'));
    },

    selectNode: function (nodeId) {
        /*Ext.History.add("/" + record.get('id'));
        var mainNavigator = this.getMainNavigator();
        var record = mainNavigator.getRootNode().findChild('id', nodeId, true);
        mainNavigator.getSelectionModel().select(record, false, false);*/
    },

    selectNodeSpecial: function (nodeId) {
        Ext.History.add("/" + nodeId);
    }
});