﻿Ext.define('CRMax.controller.Settings', {
    extend: 'Ext.app.Controller',
    views: ['Settings', 'cotista.List', 'cotista.Edit'],
    refs: [{ ref: 'settingsPanel', selector: 'settings'}],
    //stores: ['Users'],

    init: function () {
        this.control({
            '#settings > dataview': {
                itemclick: this.onDataViewItemClick,
                afterrender: this.onDataViewAfterRender
            },

            '#user-list': {
                itemclick: this.onUserListItemClick
            },

            '#button-create-user': {
                click: this.onButtonCreateUserClick
            },

            '#button-edit-user': {
                click: this.onButtonEditUserClick
            },

            '#user-form button[action=save]': {
                click: this.onUserFormButtonSaveClick
            },

            '#user-form button[action=cancel]': {
                click: this.onUserFormButtonCancelClick
            }
        });

        this.application.on({
            userupdated: this.onUserUpdated,
            scope: this
        });

    },

    onDataViewAfterRender: function (dataview) {
        dataview.select(0);
        this.addUsersGrid();
    },

    onButtonEditUserClick: function (button) {
        if(CRMax.globals.userInfo.MultiCotista === true){
            this.editCurrentUser();
        }
    },

    editCurrentUser: function () {
        var win = new CRMax.view.cotista.Edit();
        win.show();
    },

    loadUser: function (record, formPanel) {
        formPanel.loadRecord(record);

    },

    onUserUpdated: function () {
        this.getUsersStore().load();
    },

    closeUserWindow: function (window) {
        window.close();
    },

    onUserFormButtonCancelClick: function (button) {
        var window = button.up('window');
        this.closeUserWindow(window);
    },

    onButtonCreateUserClick: function () {
        this.onCreateUser();
    },

    onCreateUser: function () {
        var userWin = new CRMax.view.user.Edit();
        userWin.show();

    },

    onUserListItemClick: function (view, record, htmlElement, index, eventObject, eOpts) {
        var targetEl = Ext.get(eventObject.target);

        if (targetEl.hasCls('resend-invitation-link')) {
            this.resendInvitationLink(record.data.UserId);
        }
    },

    resendInvitationLink: function (userId) {
        var model = Ext.ModelManager.getModel('CRMax.model.User')
        model.load(userId, {
            scope: this,
            success: function (user) {
                user.data.UIAction = 'ResendInvitationLink';
                this.saveUser(user, null, 'Convite reenviado.');
            }
        });
    },

    onDataViewItemClick: function (view, record) {
        if (record.data.id == 'users') {
            this.addUsersGrid();
        } else if (record.data.id == 'deal-categories') {
            this.editDealCategories();
        } else if (record.data.id == 'task-categories') {
            this.editTaskCategories();
        } else if (record.data.id == 'custom-fields') {
            //this.addCustomFieldsGrid();
            this.editCustomFields();
        }
    },

    editDealCategories: function () {
        var winDealCategories = Ext.create('Ext.window.Window', {
            title: 'Categorias de negócio',
            //layout: 'fit',
            modal: true,
            shadow: false,
            resizable: false,
            border: false,
            draggable: false,
            width: 500,
            cls: 'crmax-grid-panel',


            items: {
                xtype: 'dealcategorylist',
                height: 400
            },

            buttons: [
        {
            baseCls: '',
            cls: 'btn',
            text: 'Finalizar edição',
            handler: function (btn) {
                winDealCategories.close();
            }
        }]

        });

        winDealCategories.show();

    },

    editTaskCategories: function () {
        var winTaskCategories = Ext.create('Ext.window.Window', {
            title: 'Categorias de tarefa',
            //layout: 'fit',
            modal: true,
            shadow: false,
            resizable: false,
            border: false,
            draggable: false,
            width: 500,
            cls: 'crmax-grid-panel',


            items: {
                xtype: 'taskcategorylist',
                height: 400
            },

            buttons: [
        {
            baseCls: '',
            cls: 'btn',
            text: 'Finalizar edição',
            handler: function (btn) {
                winTaskCategories.close();
            }
        }]

        });

        winTaskCategories.show();

    },

    editCustomFields: function () {
        this.addCardPanel({
            itemId: 'customfield-list',
            title: 'Campos Personalizados',
            xtype: 'customfieldlist',
            hideHeaders: true
        });


        /*var winCustomFields = Ext.create('Ext.window.Window', {
        title: 'Campos Personalizados',
        //layout: 'fit',
        modal: true,
        shadow: false,
        resizable: false,
        border: false,
        draggable: false,
        width: 500,
        cls: 'crmax-grid-panel',
            
        items: {
        xtype: 'customfieldlist',
        height: 400
        },

        buttons: [
        {
        baseCls: '',
        cls: 'btn',
        text: 'Finalizar edição',
        handler: function (btn) {
        winCustomFields.close();
        }
        }]

        });

        winCustomFields.show();*/

    },

    addUsersGrid: function () {
        this.addCardPanel({
            itemId: 'user-list',
            title: 'Usuários',
            xtype: 'userlist',
            hideHeaders: true
        });
    },

    addCardPanel: function (componentConfig, removeBeforeAdding) {
        var settingsPanel = this.getSettingsPanel();
        var cards = settingsPanel.down('#cards');
        var cardLayout = cards.getLayout();
        var cardItems = cardLayout.getLayoutItems();

        //try to find component
        var component = null;
        for (var cardCount = 0; cardCount < cardItems.length; cardCount++) {
            if (cardItems[cardCount].itemId == componentConfig.itemId) {
                component = cardItems[cardCount];
                if (removeBeforeAdding === true) {
                    component.destroy();
                    component = null;
                }
                break;
            }
        }

        if (!component) {
            component = cards.add(componentConfig);
        }

        cardLayout.setActiveItem(componentConfig.itemId);
        return component;
    },

    onUserFormButtonSaveClick: function (button) {
        var formPanel = button.up('form'),
        formWin = formPanel.up('window'),
        form = formPanel.getForm(),
        user = form.getRecord(),
        values = form.getValues();

        if (!user) {
            //creating a new task
            user = Ext.create('CRMax.model.User', values);
        } else {
            user.set(values);
        }

        if (!form.isValid()) {
            return false;
        }

        this.saveUser(user, formWin);
    },

    saveUser: function (user, formWin, customUpdateMessage) {
        user.save({
            scope: this,
            success: function () {
                this.application.fireEvent('userupdated');

                Ext.Msg.alert('Aviso',customUpdateMessage || 'Usuário atualizado.'); 

                //this.application.notificationWindow.update(customUpdateMessage || 'Usuário atualizado.');
                //this.application.notificationWindow.show();

                formWin && this.closeUserWindow(formWin);
            },
            failure: function (record, operation) {
                Ext.Msg.alert('Aviso',operation.request.scope.reader.jsonData.errorMessage);
            }
        });
    }
});