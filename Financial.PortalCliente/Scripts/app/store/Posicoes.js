Ext.define('CRMax.store.Posicoes', {
	extend: 'Ext.data.Store',
	fields: ['IdPosicao', 'ApelidoCarteira', 'NomeCarteira', 'ValorBruto', 'ValorLiquido', 'ValorLiquidoResgate', 'IdCarteira', 'CotaDia', 'Quantidade', 
	    {name: 'DataDia', type: 'date', dateFormat: 'd/m/y'}],
	autoLoad: true,
	proxy: {
        type: 'ajax',
        url: CRMax.globals.baseDirectory + 'Controllers/PosicaoCotista/List.ashx',
        reader: {
            type: 'json',
            root: 'Collection',
            successProperty: 'success'
        }
    }
});