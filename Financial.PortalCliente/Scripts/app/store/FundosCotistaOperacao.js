Ext.define('CRMax.store.FundosCotistaOperacao', {
	extend: 'Ext.data.Store',
	fields: ['IdCarteira', 'Nome', 'Categoria', 'GrauRisco', 'RetornoMesCorrente', 'RetornoUltimoMes', 'Retorno12Meses', 'RetornoAno', 'ValorMinimoInicial', 'ValorMinimoAplicacao',
	'ValorMinimoResgate', 'ValorMinimoSaldo', 'DiasCotizacaoAplicacao', 'DiasCotizacaoResgate', 'DiasLiquidacaoAplicacao', 'DiasLiquidacaoResgate',
	'HorarioFim', 'TemPermissaoAplicar', 'LinkLamina', 'LinkTermoAdesao', 'LinkProspecto', 'LinkRegulamento'],
    alias: 'store.fundoscotistaoperacao',
	autoLoad: true,

	proxy: {
	    type: 'ajax',
	    url: CRMax.globals.baseDirectory + 'Controllers/FundoCotistaOperacao/List.ashx',
	    reader: {
	        type: 'json',
	        root: 'Collection',
	        successProperty: 'success'
	    }
	}

});