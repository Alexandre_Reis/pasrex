Ext.define('CRMax.store.OrdensPgbl', {
	extend: 'Ext.data.Store',
	fields: ['IdOperacao', 'FormaLiquidacao', 'ContaCorrente', 'TipoOperacaoString', 'IdCotista', 'NomeCotista', 'NomeCarteira', 'IdOperacaoMae', 'IdCarteira', 'TipoOperacao', 'Status', 'StatusProcessamento', {name: 'DataOperacao', type: 'date', dateFormat: 'd/m/y'}, 'Valor', 'Saldo', 'IsDistribuida'],
	remoteFilter: true,
	autoLoad: true,
	proxy: {
        type: 'ajax',
        url: CRMax.globals.baseDirectory + 'Controllers/OrdemPgbl/List.ashx',
        reader: {
            type: 'json',
            root: 'Collection',
            successProperty: 'success'
        }
    }
});