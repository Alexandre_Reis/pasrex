Ext.define('CRMax.store.PosicoesDetalhadas', {
	extend: 'Ext.data.Store',
	fields: ['IdPosicao', 'NomeCarteira', 'ValorBruto', 'ValorLiquido', 'IdCarteira', 'DataAplicacao', 'Quantidade', 'CotaDia', 'ValorIR', 'ValorIOF'],
	autoLoad: false,
	remoteFilter: true,
	proxy: {
        type: 'ajax',
        url: CRMax.globals.baseDirectory + 'Controllers/PosicaoCotista/Detalhe/List.ashx',
        reader: {
            type: 'json',
            root: 'Collection',
            successProperty: 'success'
        }
    }
});