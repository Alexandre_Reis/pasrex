/*Ext.define('CRMax.store.Documentos', {
	extend: 'Ext.data.Store',
	fields: ['Nome', 'Path'],
	data: [
	    {Nome: 'Documento 1'},
	    {Nome: 'Documento 2'}]
});*/


Ext.define('CRMax.store.Documentos', {
	extend: 'Ext.data.Store',
	fields: ['Nome', 'Tipo', 'Path'],

	autoLoad: true,

	proxy: {
	    type: 'ajax',
	    url: CRMax.globals.baseDirectory + 'Controllers/Documento/List.ashx',
	    reader: {
	        type: 'json',
	        root: 'Collection',
	        successProperty: 'success'
	    }
	}

});