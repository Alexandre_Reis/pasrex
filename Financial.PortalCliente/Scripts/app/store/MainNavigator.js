﻿CRMax.tmp = [];

if(CRMax.globals.userInfo.IdCotista == -1 && CRMax.globals.isTesouraria){
    if(CRMax.globals.userInfo.Permissoes.PodeDistribuir){
        CRMax.tmp.push(
            { description: "Distribuição", iconCls: 'navigator-icon-home', id: 'distribuicao', leaf: true }
        );
    }

    if(CRMax.globals.userInfo.Permissoes.PodeAprovar){
        CRMax.tmp.push(
            { description: "Aprovação", iconCls: 'navigator-icon-home', id: 'aprovacao', leaf: true }
        );
    }
}

else if (CRMax.globals.userInfo.IdCotista == -1 && !CRMax.globals.isTesouraria){
    CRMax.tmp.push(
            { description: "Movimentações", iconCls: 'navigator-icon-home', id: 'operacoes', leaf: true }
    );
}

else{

    var opcoesTesouraria = [];
    
    opcoesTesouraria.push({ description: "Saldos", iconCls: 'navigator-icon-home', id: 'home', leaf: true });
    opcoesTesouraria.push({ description: "Ordens", iconCls: 'navigator-icon-home', id: 'ordens', leaf: true });
    if(CRMax.globals.userInfo.Permissoes.AcessoPGBL){
        opcoesTesouraria.push({ description: "PGBL", iconCls: 'navigator-icon-home', id: 'ordenspgbl', leaf: true });
    }
    opcoesTesouraria.push({ description: "Operações", iconCls: 'navigator-icon-home', id: 'operacoes', leaf: true });

    CRMax.tmp = CRMax.globals.isTesouraria ? 
        opcoesTesouraria
    :
    [
            { description: "Meus Investimentos", iconCls: 'navigator-icon-home', id: 'home', leaf: true },
            { description: "Movimentações", iconCls: 'navigator-icon-home', id: 'operacoes', leaf: true },
            { description: "Extrato", iconCls: 'navigator-icon-home', id: 'extrato', leaf: true },
            { description: "Perfil de Investidor", iconCls: 'navigator-icon-contacts', id: 'suitability', leaf: true },
            { description: "Documentos", iconCls: 'navigator-icon-contacts', id: 'documentos', leaf: true }
            
        ];

    if(CRMax.globals.userInfo.Permissoes.AcessoSeguranca){
        CRMax.tmp.push({ description: "Segurança", iconCls: 'navigator-icon-contacts', id: 'seguranca', leaf: true });
    }
}


Ext.define('CRMax.store.MainNavigator', {
    extend: 'Ext.data.Store',
    fields: ['description', 'iconCls', 'id'],
    data: CRMax.tmp
        
    
});