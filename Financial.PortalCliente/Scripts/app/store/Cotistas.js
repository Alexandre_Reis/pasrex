Ext.define('CRMax.store.Cotistas', {
	extend: 'Ext.data.Store',
	model: 'CRMax.model.Cotista',

	autoLoad: true,

    pageSize: 9,
    remoteFilter: true,
    
	proxy: {
	    type: 'ajax',
	    url: CRMax.globals.baseDirectory + 'Controllers/Cotista/List.ashx',
	    reader: {
	        type: 'json',
	        root: 'Collection',
	        successProperty: 'success',
	        totalProperty: 'totalCount'
	    }
	}

});