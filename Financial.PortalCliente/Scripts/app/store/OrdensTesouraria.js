Ext.define('CRMax.store.OrdensTesouraria', {
	extend: 'Ext.data.Store',
	fields: ['IdOperacao', 'FormaLiquidacao', 'NomeCotista', 'IdCotista', 'ContaCorrente', 'TipoOperacaoString', 'NomeCarteira', 'IdOperacaoMae', 'IdCarteira', 'TipoOperacao', 'Status', 'StatusProcessamento', {name: 'DataOperacao', type: 'date', dateFormat: 'd/m/y'}, 'Valor', 'Saldo', 'IsDistribuida', 'Observacao'],
	remoteFilter: true,
	autoLoad: true,
	proxy: {
        type: 'ajax',
        url: CRMax.globals.baseDirectory + 'Controllers/OrdemTesouraria/List.ashx',
        reader: {
            type: 'json',
            root: 'Collection',
            successProperty: 'success'
        }
    }
});