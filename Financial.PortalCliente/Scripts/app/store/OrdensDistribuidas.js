Ext.define('CRMax.store.OrdensDistribuidas', {
	extend: 'Ext.data.Store',
	fields: ['IdCarteira','NomeCarteira', 'Valor', 'TipoOperacaoString', 'FormaLiquidacao', 'ContaCorrente'],
	autoLoad: false,
	remoteFilter: true,
	proxy: {
        type: 'ajax',
        url: CRMax.globals.baseDirectory + 'Controllers/OrdemTesouraria/Distribute/List.ashx',
        reader: {
            type: 'json',
            root: 'Collection',
            successProperty: 'success'
        }
    }
});