Ext.define('CRMax.store.Extratos', {
	extend: 'Ext.data.Store',
	fields: ['NomeCarteira', 'ValorBruto', 'ValorLiquido', 'IdCarteira', 'DataHistorico', 'Quantidade', 'CotaDia', 'ValorIR', 'ValorIOF', 'Historico'],
	groupField: 'NomeCarteira',
    
    remoteFilter: true,
	autoLoad: true,
	proxy: {
        type: 'ajax',
        url: CRMax.globals.baseDirectory + 'Controllers/ExtratoCotista/List.ashx',
        reader: {
            type: 'json',
            root: 'Collection',
            successProperty: 'success'
        }
    }
	
});