Ext.define('CRMax.store.FundosPgbl', {
	extend: 'Ext.data.Store',
	fields: ['IdCarteira', 'Nome', 'Categoria', 'GrauRisco', 'RetornoMesCorrente', 'RetornoUltimoMes', 'Retorno12Meses', 'RetornoAno', 'ValorMinimoInicial', 'ValorMinimoAplicacao',
	'ValorMinimoResgate', 'ValorMinimoSaldo', 'DiasCotizacaoAplicacao', 'DiasCotizacaoResgate', 'DiasLiquidacaoAplicacao', 'DiasLiquidacaoResgate',
	'HorarioFim', 'TemPermissaoAplicar', 'LinkLamina', 'LinkTermoAdesao', 'LinkProspecto', 'LinkRegulamento'],
    alias: 'store.fundoscotista',
	autoLoad: false,

	proxy: {
	    type: 'ajax',
	    url: CRMax.globals.baseDirectory + 'Controllers/FundoPgbl/List.ashx',
	    reader: {
	        type: 'json',
	        root: 'Collection',
	        successProperty: 'success'
	    }
	}

});