Ext.define('CRMax.store.Operacoes', {
	extend: 'Ext.data.Store',
	fields: ['IdOperacao', 'TipoOperacaoString', 'NomeCarteira', 'StatusProcessamento', 'StatusCotizacao',
	{name: 'DataOperacao', type: 'date', dateFormat: 'd/m/y'}, 'ValorBruto', 'ValorLiquido', 'Valor', 'FormaLiquidacao', 'ContaCorrente', 'NomeCotista'],
	remoteFilter: true,
	autoLoad: true,
	proxy: {
        type: 'ajax',
        url: CRMax.globals.baseDirectory + 'Controllers/OperacaoCotista/List.ashx',
        reader: {
            type: 'json',
            root: 'Collection',
            successProperty: 'success'
        }
    }
	
});