Ext.define('CRMax.store.FundosCotista', {
	extend: 'Ext.data.Store',
	fields: ['IdCarteira', 'Apelido', 'Nome', 'Categoria', 'GrauRisco', 'RetornoMesCorrente', 'RetornoUltimoMes', 'Retorno12Meses', 'RetornoAno', 'ValorMinimoInicial', 'ValorMinimoAplicacao',
	'ValorMinimoResgate', 'ValorMinimoSaldo', 'DiasCotizacaoAplicacao', 'DiasCotizacaoResgate', 'DiasLiquidacaoAplicacao', 'DiasLiquidacaoResgate',
	'HorarioFim', 'TemPermissaoAplicar', 'LinkLamina', 'LinkTermoAdesao', 'LinkProspecto', 'LinkRegulamento'],
    alias: 'store.fundoscotista',
	autoLoad: true,

	proxy: {
	    type: 'ajax',
	    url: CRMax.globals.baseDirectory + 'Controllers/FundoCotista/List.ashx',
	    reader: {
	        type: 'json',
	        root: 'Collection',
	        successProperty: 'success'
	    }
	}

});