//Codigo para ajustar bug do zindex do alert modal no form submit
Ext.override(Ext.window.Window, {
    setActive: function(active, newActive) {
        var me = this;


        if (active) {
            if (me.el.shadow && !me.maximized) {
                me.el.enableShadow(true);
            }
//            if (me.modal && !me.preventFocusOnActivate) {
//                me.focus(false, true);
//            }
            me.fireEvent('activate', me);
        } else {
            // Only the *Windows* in a zIndex stack share a shadow. All other types of floaters
            // can keep their shadows all the time
            if (me.isWindow && (newActive && newActive.isWindow)) {
                me.el.disableShadow();
            }
            me.fireEvent('deactivate', me);
        }
    }
});
//FIM

Ext.Ajax.timeout = 160000; // 60 seconds

//Use the mask function on the Ext.getBody() element to mask the body element during Ajax calls
/*Ext.Ajax.on('beforerequest', function () { Ext.getBody().mask('', 'x-mask-loading') }, Ext.getBody());
Ext.Ajax.on('requestcomplete', Ext.getBody().unmask, Ext.getBody());
Ext.Ajax.on('requestexception', Ext.getBody().unmask, Ext.getBody());*/


/*Ext.Loader.setConfig({ enabled: true, 
paths: {
'Ext': '/Scripts/ext-4.1.0/src',
'CRMax': '/Scripts/app',
'Ext.ux': '/Scripts/app/ux'
}
});*/

//We need to check if our session hasn't expired. If it did, ajax requests will have html with the LogOn string in them
/*Ext.Ajax.on('requestcomplete', function (connection, response) {
    if (response && response.responseText && (response.responseText.indexOf('form action="/Account/LogOn') > 0)) {
        alert('Sua sessão expirou. Clique OK para continuar');
        window.location.reload(true);
    }
});*/


Ext.Ajax.on('requestexception', function (conn, response, options) {
    if (response && response.responseText && (response.responseText.indexOf('Convert.ToInt32((Convert.ToString(context.Session') > 0)) {
            Ext.Msg.alert('Aviso','Sua sessão expirou. Clique OK para continuar');
            window.location.reload(true);
    }
});

//Tooltip enhancement


// Fix CheboxModel BUG -- Remove after fixed !!!
Ext.override(Ext.selection.CheckboxModel, {
    onRowMouseDown: function (view, record, item, index, e) {
        view.el.focus(200); // Focus the element in 0.2 seconds - time for the click to happen...
        var me = this,
            checker = e.getTarget('.' + Ext.baseCSSPrefix + 'grid-row-checker'),
            mode;

        if (!me.allowRightMouseSelection(e)) {
            return;
        }

        // checkOnly set, but we didn't click on a checker.
        if (me.checkOnly && !checker) {
            return;
        }

        if (checker) {
            mode = me.getSelectionMode();
            // dont change the mode if its single otherwise
            // we would get multiple selection
            if (mode !== 'SINGLE') {
                me.setSelectionMode('SIMPLE');
            }
            me.selectWithEvent(record, e);
            me.setSelectionMode(mode);
        } else {
            me.selectWithEvent(record, e);
        }

    }
});

Ext.application({
    name: 'CRMax',
    autoCreateViewport: true,

    appFolder: '/Scripts/app',

    stores: [
        'Extratos',
        'Operacoes',
        'Posicoes',
        'PosicoesDetalhadas',
        'Documentos',
        'FundosCotista',
        'FundosCotistaOperacao',
        'FundosPgbl',
        'Cotistas',
        'OrdensTesouraria',
        'OrdensPgbl',
        'OrdensDistribuidas'
    ],

    controllers: [
    'MainNavigator',
    'UserProfile',
    'Settings'
    ],

    buildStore: function (storeInfo) {
        var records = [];
        var enumKey = storeInfo[0];
        var storeName = storeInfo[1];

        for (var objectProperty in CRMax.globals.enums[enumKey]) {
            //Adjust multi-value DisplayName
            var enumObject = CRMax.globals.enums[enumKey][objectProperty];
            if (enumObject.DisplayName && enumObject.DisplayName.split('|').length > 0) {
                var displayNameInfo = enumObject.DisplayName.split('|');
                enumObject.DisplayName = displayNameInfo[0];
                for (var displayNameInfoCount = 1; displayNameInfoCount < displayNameInfo.length; displayNameInfoCount++) {
                    var keyValuePair = displayNameInfo[displayNameInfoCount].split(':');
                    enumObject[keyValuePair[0]] = keyValuePair[1];
                }
            }

            records.push(enumObject);
        }


        Ext.data.StoreManager.lookup(storeName).loadRawData(records);

    },

    buildHashFromStore: function (storeName, hashName, key) {
        var store = Ext.data.StoreManager.lookup(storeName);
        CRMax.globals.hashes[hashName] = {};
        store.each(function (record) {
            CRMax.globals.hashes[hashName][record.get(key).toString()] = record.data;

        }, this);
    },

    createCustomFieldListDomainStore: function (customFieldId) {
        var storeId = 'CFLD_' + customFieldId;
        return CRMax.store[storeId] =
                    Ext.create('Ext.data.Store', {
                        storeId: storeId,
                        sortOnLoad: true,
                        sorters: [{
                            property: 'DisplayText',
                            direction: 'ASC'
                        }],
                        model: 'CRMax.model.CustomFieldListDomain'
                    });
    },

    inIframe: function(){
       try {
            return window.self !== window.top;
       }catch (e) {
            return true;
       }
    },

    launch: function () {
        // Init the singleton.  Any tag-based quick tips will start working.
        Ext.tip.QuickTipManager.init();

        // Apply a set of config properties to the singleton
        Ext.apply(Ext.tip.QuickTipManager.getQuickTip(), {
            dismissDelay: 0
        });
    
        if(!CRMax.globals.userInfo){
            Ext.Msg.alert('Aviso','Usuário não encontrado');
        }
    
        if(this.inIframe()){
            Ext.util.History.useTopWindow = false;
            
            //Esconder botões de sair e logo
            Ext.get('logo-cliente').hide();
            Ext.get('button-logoff').hide();
        }
        
        Ext.History.init();

        CRMax.application = this; //keep application always accessible

        Ext.util.Format.decimalSeparator = ',';
        Ext.util.Format.thousandSeparator = '.';

        this.viewport = Ext.ComponentQuery.query('viewport')[0];

        
        var initialPage;
        if(CRMax.globals.isTesouraria && CRMax.globals.userInfo.IdCotista == -1){
            if(CRMax.globals.userInfo.Permissoes.PodeDistribuir){
                initialPage = '/distribuicao';
            }else{
                initialPage = '/aprovacao';
            }
        }
        else if(!CRMax.globals.isTesouraria && CRMax.globals.userInfo.IdCotista == -1)
            initialPage = '/operacoes';
        else{
            initialPage = '/home';
        }
        
        Ext.History.add(initialPage);
        Ext.History.on('change', this.handleHistoryChange, this);

        //Build Hashes
        CRMax.globals.hashes = {};

        CRMax.globals.imagesPath = CRMax.globals.applicationPath + 'Content/themes/base/images';
        CRMax.globals.helpPath = CRMax.globals.applicationPath + 'Help';
        CRMax.globals.fileDownloadPath = CRMax.globals.applicationPath + 'file/download';
        CRMax.globals.fileReadPath = CRMax.globals.applicationPath + 'file/read';

        CRMax.globals.StatusOrdemTesourariaPorto = {
            Digitado : 1,
            Aprovado : 2,
            Rejeitado : 3,
            Distribuido : 4,
            PGBLIntegrado : 100,
            PGBLAprovado : 101,
            PGBLRejeitado : 102
        };
        
        
        /*CRMax.globals.texts = {
            task: {
                nullCategoryFeminine: 'Não informada'
            }
        };*/

        //Create shorthand for company/person/deal info
        /*CRMax.globals.Person = CRMax.globals.enums.documentType.Person;
        CRMax.globals.Company = CRMax.globals.enums.documentType.Company;
        CRMax.globals.Deal = CRMax.globals.enums.documentType.Deal;*/

        // create password confirmation validation type
        Ext.apply(Ext.form.VTypes, {
            passwordConfirmation: function (value, field) {
                if (!(field.initialPasswordField instanceof Ext.form.Field)) {
                    field.initialPasswordField = field.up('form').getForm().findField(field.initialPasswordField);
                }
                return (value == field.initialPasswordField.getValue());
            },
            passwordConfirmationText: 'Confirmação deve ser igual à nova senha.'
        });

        this.getController('MainNavigator').selectNode('home');
        this.handleHistoryChange(initialPage);

        //Initialize notification popup
        this.notificationWindow = Ext.create('widget.uxNotification', {
            title: '',
            position: 't',
            autoCloseDelay: 7000,
            slideInDuration: 1,
            slideBackDuration: 1,
            closeAction: 'hide',
            autoClose: true,
            spacing: 20,
            closable: false
        });
        
        if(CRMax.globals.userInfo.MultiCotista === true && CRMax.globals.userInfo.CotistaNaSession === false){
            this.getController('Settings').editCurrentUser();
        }
        
        if(CRMax.globals.userInfo.MultiCotista === false){
            Ext.get('button-edit-user').dom.style.cursor = 'auto';
            Ext.get('button-edit-user-btnEl').dom.style.cursor = 'auto';
            Ext.get('button-edit-user-btnInnerEl').dom.style.cursor = 'auto';
            Ext.get('button-edit-user-btnIconEl').dom.style.cursor = 'auto';
        }

    },

    handleHistoryChange: function (token, eOpts) {
        var panelMainContent = this.viewport.getMainContentPanel();

        if (token == '/home') {
            this.getController('MainNavigator').selectNode('home');
            var homeItemId = 'home-panel';
            panelMainContent.addComponent({ itemId: homeItemId, xtype: 'homepanel' });
        }
        else if (token == '/contacts') {
            this.getController('MainNavigator').selectNode('contacts');
            panelMainContent.addComponent({ itemId: 'contact-list', xtype: 'contactlist' });
        }
        else if (token == '/operacoes') {
            this.getController('MainNavigator').selectNode('operacoes');
            panelMainContent.addComponent({ itemId: 'operacoes-panel', xtype: 'operacoespanel' });
        }
        else if (token == '/ordens') {
            this.getController('MainNavigator').selectNode('ordens');
            panelMainContent.addComponent({ itemId: 'ordens-panel', xtype: 'ordenspanel' });
        }
        else if (token == '/aprovacao') {
            this.getController('MainNavigator').selectNode('aprovacao');
            panelMainContent.addComponent({ itemId: 'apovacao-panel', xtype: 'aprovacaopanel' });
        }
        else if (token == '/distribuicao') {
            this.getController('MainNavigator').selectNode('distribuicao');
            panelMainContent.addComponent({ itemId: 'distribuicao-panel', xtype: 'distribuicaopanel' });
        }
        else if (token == '/seguranca') {
            this.getController('MainNavigator').selectNode('seguranca');
            panelMainContent.addComponent({ itemId: 'seguranca-panel', xtype: 'segurancapanel' });
        }
        else if (token == '/ordenspgbl') {
            this.getController('MainNavigator').selectNode('ordenspgbl');
            panelMainContent.addComponent({ itemId: 'ordens-pgbl-panel', xtype: 'ordenspgblpanel' });
        }
        else if (token == '/extrato') {
            this.getController('MainNavigator').selectNode('extrato');
            panelMainContent.addComponent({ itemId: 'extrato-panel', xtype: 'extratopanel' });
        }
        else if (token == '/documentos') {
            this.getController('MainNavigator').selectNode('documentos');
            panelMainContent.addComponent({ itemId: 'documentos-list', xtype: 'documentoslist' });
        }
        else if (token == '/suitability') {
            this.getController('MainNavigator').selectNode('suitability');
            panelMainContent.addComponent({ itemId: 'suitability-edit', xtype: 'suitabilityedit' });
        }
        else if (token == '/tasks') {
            this.getController('MainNavigator').selectNode('tasks');
            panelMainContent.addComponent({ itemId: 'task-list', xtype: 'tasklist' });
        }
        else if (token == '/deals') {
            this.getController('MainNavigator').selectNode('deals');
            panelMainContent.addComponent({ itemId: 'deal-list', xtype: 'deallist' });
        }
        else if (token == '/settings') {
            this.getController('MainNavigator').selectNode('settings');
            panelMainContent.addComponent({ itemId: 'settings', xtype: 'settings' });
        }
        else if (token.indexOf('/company/new') === 0) {
            this.getController('Companies').createCompany();
        }

        else if (token.indexOf('/company/edit/') === 0) {
            var splittedToken = token.split('/');
            if (splittedToken.length >= 4) {
                var companyId = parseInt(splittedToken[3], 10);
                this.getController('Companies').editCompany(companyId);
            }
        }

        else if (token.indexOf('/person/new') === 0) {
            this.getController('People').createPerson();
        }

        else if (token.indexOf('/person/edit/') === 0) {
            var splittedToken = token.split('/');
            if (splittedToken.length >= 4) {
                var personId = parseInt(splittedToken[3], 10);
                this.getController('People').editPerson(personId);
            }
        }

        else if (token.indexOf('/deal/new') === 0) {
            this.getController('Deals').createDeal();
        }

        else if (token.indexOf('/deal/edit/') === 0) {
            var splittedToken = token.split('/');
            if (splittedToken.length >= 4) {
                var dealId = parseInt(splittedToken[3], 10);
                this.getController('Deals').editDeal(dealId);
            }
        }

        else if (token.indexOf('/company/') === 0) {
            var splittedToken = token.split('/');
            if (splittedToken.length >= 3) {
                var companyId = parseInt(splittedToken[2], 10);
                this.getController('Companies').displayCompany(companyId);
            }
        }
        else if (token.indexOf('/person/') === 0) {
            var splittedToken = token.split('/');
            if (splittedToken.length >= 3) {
                var personId = parseInt(splittedToken[2], 10);
                this.getController('People').displayPerson(personId);
            }
        }
        else if (token.indexOf('/deal/') === 0) {
            var splittedToken = token.split('/');
            if (splittedToken.length >= 3) {
                var dealId = parseInt(splittedToken[2], 10);
                this.getController('Deals').displayDeal(dealId);
            }
        }
        else {
            //alert('Unsupported');
        }

    }
});



