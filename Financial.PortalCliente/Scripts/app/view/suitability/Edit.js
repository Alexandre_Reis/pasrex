﻿/*MENSAGENS SUITABILITY PORTAL
IdSuitabilityEventos	Descricao
1	Apuração de novo perfil de Investidor (Portal)
2	Declaração sobre as informações prestadas (Portal)
4	Aceite do perfil apurado (Portal)
6	Aceite do tipo de dispensa (Portal)
8	Recusa do preenchimento do formulário (Portal)
9	Questionário não preenchido (Portal)
10	Atualizar perfil (Portal)
11	Inexistência de Perfil (Portal)
12	Perfil desatualizado (Portal)
14	Desenquadramento - Alteração de perfil (Portal)
15	Termo de Ciência (Portal)
17	Termo de Ciência de Risco e Adesão ao Fundo (Portal)
19	Termo de Inadequação (Portal)
*/
Ext.define('CRMax.view.suitability.Edit', {
    extend: 'Ext.form.Panel',
    alias: 'widget.suitabilityedit',
    title: 'Editar Perfil',
    autoScroll: true,

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    border: false,
    cls: 'edit-form',

    fieldDefaults: {
        msgTarget: 'under',
        labelAlign: 'top'
    },

    initComponent: function() {

        var formularioPreenchido = CRMax.globals.perfilCotista.Perfil !== null && CRMax.globals.perfilCotista.Perfil !== 'Indefinido.';

        this.messageFormularioPreenchido = '<p style="margin: 15px; 0">Formulário preenchido com Sucesso!</p>';
        this.messageInstrucoesPreenchimento = '<p style="margin: 15px; 0">Responda a cada uma das cinco questões de múltipla escolha ao lado, escolhendo a alternativa que melhor lhe represente, tendo em vista sua situação financeira, sua experiência em matéria de investimentos, seus objetivos, horizontes de investimento, tolerância a riscos e necessidade de liquidez.</p><p style="margin: 15px; 0;">Assinale apenas uma alternativa em cada questão.</p>';

        var mensagemQuestionario = formularioPreenchido ? this.messageFormularioPreenchido : this.messageInstrucoesPreenchimento;


        var buttonDispensar = {
            xtype: 'button',
            text: 'Dispensar',
            baseCls: '',
            itemId: 'btn-dispensar',
            scope: this,
            cls: 'btn btn-inverse',
            handler: this.dispensar
        };

        var buttonCancelarDispensa = {
            xtype: 'button',
            text: 'Canc. Dispensa',
            baseCls: '',
            itemId: 'btn-cancelar-dispensa',
            scope: this,
            cls: 'btn btn-inverse',
            handler: this.cancelarDispensa
        };

        var buttonAtualizar = {
            xtype: 'button',
            text: 'Apurar',
            baseCls: '',
            itemId: 'btn-atualizar',
            scope: this,
            cls: 'btn btn-inverse',
            handler: this.submitAtualiza
        };

        var buttonAceitar = {
            xtype: 'button',
            text: 'Aceito',
            baseCls: '',
            itemId: 'btn-aceitar',
            scope: this,
            cls: 'btn btn-inverse',
            handler: this.submitAceito
        };

        var buttonRecusar = {
            xtype: 'button',
            text: 'Recuso',
            baseCls: '',
            itemId: 'btn-recusar',
            scope: this,
            cls: 'btn btn-inverse',
            handler: this.submitRecusa
        };

        var buttons = [buttonDispensar, buttonCancelarDispensa, buttonAtualizar, buttonAceitar, buttonRecusar];

        var infoSuitability = {
            xtype: 'component',
            id: 'info_suitability'
        };

        var leftColumn = {
            border: false,
            cls: 'left-column',
            layout: 'anchor',
            defaults: {
                width: 345
            },
            autoScroll: true,
            width: 390,
            items: [{
                    xtype: 'component',
                    html: '<h1>QUESTIONÁRIO</h1><div id="message-questionario">' + mensagemQuestionario + '</div>'
                },
                infoSuitability, {
                    xtype: 'component',
                    html: '<p style="margin: 0 15px 30px 15px;">Para conhecer os perfis de investidor, clique <span id="link-definicao-perfil" style="cursor:pointer; font-weight: bold;">aqui</span>.</p>'
                }
            ]
        };

        var questoesFields = [];

        for (var countQuestao = 0; countQuestao < CRMax.globals.suitability.Questoes.length; countQuestao++) {
            var questao = CRMax.globals.suitability.Questoes[countQuestao];

            var opcoesField = [];

            for (var countOpcao = 0; countOpcao < questao.Opcoes.length; countOpcao++) {
                var opcao = questao.Opcoes[countOpcao];

                opcoesField.push({
                    boxLabel: opcao.Descricao,
                    name: 'OpcaoQuestao-' + questao.IdQuestao,
                    inputValue: opcao.IdOpcao,
                    checked: opcao.Escolhida
                });
            }

            var questaoField = {
                xtype: 'radiogroup',
                fieldLabel: questao.Descricao,
                columns: 1,
                vertical: true,
                items: opcoesField,
                allowBlank: false,
                listeners: {
                    change: function() {
                        this.up('form').checkFormFilled();
                    }
                }
            };

            questoesFields.push(questaoField);
        }

        var mensagemPreenchido = {
            xtype: 'component',
            itemId: 'mensagem-preenchido',
            border: false,
            hidden: true,
            html: ''
        };

        questoesFields.push(mensagemPreenchido);

        var rightColumn = {
            //columnWidth: 1,
            flex: 1,
            itemId: 'right-column',
            autoScroll: true,
            cls: 'right-column',
            border: false,
            items: questoesFields
        }

        this.items = [
            leftColumn, rightColumn
        ];

        this.buttons = buttons;

        this.callParent(arguments);

        this.down('#btn-atualizar').disable();
        this.down('#btn-aceitar').disable();

        if (formularioPreenchido || CRMax.globals.perfilCotista.Dispensado == 'Sim') {
            this.displayMensagemFormularioPreenchido();
        }
        
        this.on('show', this.checkNeedsEditForm, this);
    },

    afterRender: function() {
        this.callParent(arguments);

        this.buildLinks();
        if (Ext.get('link-definicao-perfil')) {
            Ext.get('link-definicao-perfil').on('click', this.displayDefinicaoPerfil, this);
        }

        this.updateInfoSuitability();
        
    },
    
    checkNeedsEditForm: function(){
         if(CRMax.application.editSuitability === true){
            CRMax.application.editSuitability = false;
            this.editForm();
        }
    },


    logMensagem: function(idMensagem, mensagem, resposta) {
        Ext.Ajax.request({
            scope: this,
            url: CRMax.globals.baseDirectory + 'controllers/suitability/logmensagem.ashx?idmensagem=' + idMensagem + '&mensagem=' + mensagem + '&resposta=' + resposta,
            success: function(response) {

            }
        });
    },

    displayDefinicaoPerfil: function() {

        var html = '<div id="holder-definicao-perfil">';
        for (perfil in CRMax.globals.definicaoPerfilInvestidor) {
            var definicao = CRMax.globals.definicaoPerfilInvestidor[perfil];
            html += '<p style="font-size: 14px; margin-bottom: 10px;"><b>' + perfil + '</b>: ' + definicao + '</p>';
        }

        html += '</div>';
        html += '<p style="cursor: pointer; margin: 30px 0 30px 0;"><img style="vertical-align:middle" src="Content/themes/base/images/print.png" /><span style="cursor: pointer;" id="link-print-definicao-perfil">Imprimir</span></p>';


        var component = {
            xtype: 'component',
            html: html
        };
        var form = this;
        var winDefinicao = Ext.create('Ext.window.Window', {
            title: 'Perfis de Investidor',
            height: 390,
            autoScroll: true,
            width: 600,
            modal: true,
            layout: 'fit',
            border: false,
            bodyBorder: false,
            bodyPadding: 20,
            items: [component],

            listeners: {
                afterrender: function() {
                    Ext.get('link-print-definicao-perfil').on('click', form.printDefinicaoPerfil, this);
                }
            },

            buttons: [{
                text: 'Ok',
                baseCls: '',
                scope: this,
                cls: 'btn btn-inverse',
                handler: function() {
                    winDefinicao.close();
                }
            }]
        }).show();

    },

    updateInfoSuitability: function(perfilCotista) {

        if (!perfilCotista) {
            perfilCotista = CRMax.globals.perfilCotista;
        }

        var html = '';

        if (perfilCotista.DtUltAtualizacao && perfilCotista.Perfil != "Indefinido.") {
            html += '<p>Última atualização: <b>' + perfilCotista.DtUltAtualizacao + '</b></p>';
        }
        if (perfilCotista.Validade) {
            html += '<p style="margin-top: 3px;">Validade: <b>' + perfilCotista.Validade + '</b></p>';
        }
        if (perfilCotista.Perfil) {
            html += '<p style="margin-top: 3px;">Perfil apurado: <b>' + perfilCotista.Perfil + '</b></p>';
        }
        if (perfilCotista.PerfilInvestimentos) {
            html += '<p style="margin-top: 3px;">Perfil Investimentos: <b>' + perfilCotista.PerfilInvestimentos + '</b></p>';
        }
        if (perfilCotista.Definicao) {
            html += '<p style="margin-top: 3px;">Descrição Perfil: <b>' + perfilCotista.Definicao + '</b></p>';
        }
        if (perfilCotista.Dispensado) {
            html += '<p style="margin-top: 3px;">Dispensado: <b>' + perfilCotista.Dispensado + '</b></p>';
        }
        if (perfilCotista.TipoDispensa) {
            html += '<p style="margin-top: 3px;">Tipo de Dispensa: <b>' + perfilCotista.TipoDispensa + '</b></p>';
        }
        if (perfilCotista.Alertas) {
            html += '<p style="margin-top: 3px;">Alertas: <b>' + perfilCotista.Alertas + '</b></p>';
        }
        if(perfilCotista.Perfil && perfilCotista.PerfilInvestimentos){
        
            var perfil = perfilCotista.Perfil.toUpperCase();
            var perfilInvestimentos = perfilCotista.PerfilInvestimentos.toUpperCase();
            
            if(perfilCotista.Perfil != "Indefinido." && perfilCotista.PerfilInvestimentos != "Indefinido."){
                var enquadramento = perfil === perfilInvestimentos ? 'ENQUADRADO' : 'DESENQUADRADO';
                html += '<p style="margin-top: 3px;">Perfil de Investidor <b>' + enquadramento + '</b> aos investimentos.</p>';
            }
        }


        if (html != '') {
            html = '<h1 style="margin-top: 50px; margin-bottom: 15px;">PERFIL DO INVESTIDOR</h1><div style="margin: 0 15px 20px 15px">' + html + '</div>';
        }

        var infoSuitability = Ext.get('info_suitability');
        infoSuitability.update(html);


        //Ajustar visibilidade dos botoes
        if (CRMax.globals.perfilCotista.Dispensado == 'Sim') {
            this.down('#btn-cancelar-dispensa').show();
            this.down('#btn-dispensar').hide();
            this.down('#btn-aceitar').hide();
            this.down('#btn-recusar').hide();
            this.down('#btn-atualizar').hide();
        } else {
            this.down('#btn-cancelar-dispensa').hide();
            this.down('#btn-dispensar').show();
            this.down('#btn-aceitar').show();
            this.down('#btn-recusar').show();
            this.down('#btn-atualizar').show();
        }

    },

    checkFormFilled: function(){
        var form = this;
        var basicForm = this.getForm();
        
        if (basicForm.isValid()) {
            form.down('#btn-atualizar').enable();
            form.down('#btn-aceitar').disable();
            form.down('#btn-dispensar').enable();
        }else{
            form.down('#btn-atualizar').disable();
            form.down('#btn-aceitar').disable();
        }
        
        basicForm.clearInvalid();
        
    },

    editForm: function() {
        this.checkFormFilled();
        
        this.displayQuestoes();
        if (Ext.get('message-questionario')) {
            Ext.get('message-questionario').update(this.messageInstrucoesPreenchimento);
        }
        //var btnGravar  =  this.down('#btn-gravar');
        //btnGravar.show();
    },

    buildLinks: function() {
        if (Ext.get('link-print')) {
            Ext.get('link-print').on('click', this.printForm, this);
        }

        if (Ext.get('link-edit')) {
            Ext.get('link-edit').on('click', this.editForm, this);
        }

        if (Ext.get('link-print-termo-recusa')) {
            Ext.get('link-print-termo-recusa').on('click', this.printTermoRecusa, this);
        }
    },

    printTermoRecusa: function() {
        var html = [
            '<!DOCTYPE html>',
            '<html>',
            '<head>',
            '<!--meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /-->',
            '<meta charset="utf-8" />',
            '<title>Termo de Recusa</title>',
            '<link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />',
            '<link href="Content/Site.css" rel="stylesheet" type="text/css" />',
            '<link href="Content/print.css" rel="stylesheet" type="text/css" />',
            '</head>',
            '<body class="x-body">',
            '<div class="header">',
            '<img src="Content/themes/base/images/logo_cliente.gif" alt="Logo" />',
            '<div id="form-title">Termo de Recusa</div>',
            '</div>',
            '<div class="termo" style="padding: 30px;">',
            '{0}',
            '</div>',
            '<script type="text/javascript">window.print()</script>',
            '</body>',
            '</html>'
        ];

        html = Ext.String.format(html.join(''), CRMax.globals.msgsSuitability.msg_8);

        //open up a new printing window, write to it, print it and close
        var win = window.open();

        //document must be open and closed
        win.document.open();
        win.document.write(html);
        win.document.close();
    },

    printDefinicaoPerfil: function() {
        var el = Ext.fly('holder-definicao-perfil'); //.getEl();

        var elInnerHtml = el.dom.innerHTML;

        var html = [
            '<!DOCTYPE html>',
            '<html>',
            '<head>',
            '<!--meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /-->',
            '<meta charset="utf-8" />',
            '<title>Perfis de Investidor</title>',
            '<link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />',
            '<link href="Content/Site.css" rel="stylesheet" type="text/css" />',
            '<link href="Content/print.css" rel="stylesheet" type="text/css" />',
            '</head>',
            '<body class="x-body">',
            '<div class="header">',
            '<img src="Content/themes/base/images/logo_cliente.gif" alt="Logo" />',
            '<div id="form-title">Perfis do Investidor</div>',
            '</div>',
            '<div class="perfis" style="padding: 30px;">',
            '{0}',
            '</div>',
            '<script type="text/javascript">window.print()</script>',
            '</body>',
            '</html>'
        ];

        html = Ext.String.format(html.join(''), elInnerHtml);

        //open up a new printing window, write to it, print it and close
        var win = window.open();

        //document must be open and closed
        win.document.open();
        win.document.write(html);
        win.document.close();

    },


    printForm: function() {
        this.displayQuestoes();

        var el = this.down('#right-column').getEl();

        var elInnerHtml = el.dom.innerHTML;

        var currChar;
        for (var currChar = 0; currChar < elInnerHtml.length; currChar++) {
            var nextChecked = elInnerHtml.toLowerCase().indexOf('x-form-cb-checked', currChar);
            if (nextChecked >= 0) {
                //encontrar button
                var inputTypeButtonString = 'type="button"';
                var nextButton = elInnerHtml.toLowerCase().indexOf(inputTypeButtonString, nextChecked);
                if (nextButton <= 0) {
                    inputTypeButtonString = 'type=button'; //IE 7 !!!!
                    nextButton = elInnerHtml.toLowerCase().indexOf(inputTypeButtonString, nextChecked);
                }


                if (nextButton >= 0) {
                    elInnerHtml = elInnerHtml.substr(0, nextButton + inputTypeButtonString.length) + ' value="X" ' + elInnerHtml.substr(nextButton + inputTypeButtonString.length);
                }

                currChar = nextChecked;

            } else {
                break;
            }
        }


        var html = [
            '<!DOCTYPE html>',
            '<html>',
            '<head>',
            '<!--meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /-->',
            '<meta charset="utf-8" />',
            '<title>Perfil do Investidor</title>',
            '<link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />',
            '<link href="Content/Site.css" rel="stylesheet" type="text/css" />',
            '<link href="Content/print.css" rel="stylesheet" type="text/css" />',
            '</head>',
            '<body class="x-body">',
            '<div class="header">',
            '<img src="Content/themes/base/images/logo_cliente.gif" alt="Logo" />',

            '<div id="form-title">Perfil do Investidor</div>',
            '<div style="padding: 40px 20px 0 20px;">',
            '<p style="width: 400px; float: left">Nome: <b>{1}</b></p>', '<p>CPF/CNPJ: <b>{3}</b></p>',
            '<p style="width: 400px; float: left">Última atualização: <b>{4}</b></p>', '<p>Validade: <b>{5}</b></p>',
            '<p style="margin-top: 10px;">Perfil Apurado: <b>{6}</b> - {7}</p>',
            '</div>',

            '</div>',
            '<div class="questoes">',
            '{0}',
            '</div>',
            '<div class="footer">',
            '<div id="perfil-calculado">',
            'Perfil do Investidor computado com base nas respostas acima: <span class="perfil">{2}</span>',
            '</div>',
            '<div id="assinatura"></div>',
            '<div id="nome-cotista">{1}</div>',
            '</div>',
            '<script type="text/javascript">window.print()</script>',
            '</body>',
            '</html>'
        ];

        var perfilCotista = CRMax.globals.perfilCotista;
        html = Ext.String.format(html.join(''), elInnerHtml, CRMax.globals.userInfo.Login, CRMax.globals.perfilCotista.Perfil, CRMax.globals.userInfo.CPFCNPJ,
            perfilCotista.DtUltAtualizacao, perfilCotista.Validade, perfilCotista.Perfil, perfilCotista.Definicao);

        //open up a new printing window, write to it, print it and close
        var win = window.open();

        //document must be open and closed
        win.document.open();
        win.document.write(html);
        win.document.close();

        this.displayMensagemFormularioPreenchido();
        //win.print();
        //setTimeout('win.print()', 1000);
        /*win.onload = function () {
            alert('print');
            window.print();
        }*/

        /*
        if (Ext.isIE) {
            window.close();
        } else {
            win.close();
        }*/
    },

    displayQuestoes: function() {
        var fields = this.getForm().getFields();
        fields.each(function(field) {
            field.show();
        }, this);
        var mensagemPreenchido = this.down('#mensagem-preenchido');
        mensagemPreenchido.hide();
    },

    displayMensagemFormularioPreenchido: function() {
        var fields = this.getForm().getFields();
        fields.each(function(field) {
            field.hide();
        }, this);
        var mensagemPreenchido = this.down('#mensagem-preenchido');

        var mioloMensagem;
        if (CRMax.globals.perfilCotista.Dispensado == 'Sim') {
            mioloMensagem = '<p style="padding: 20px; font-size: 15px;">Você foi dispensado do preenchimento do perfil de investidor. Para alterar esta condição, selecione o botão [Canc. Dispensa].</p>';
        } else if (CRMax.globals.perfilCotista.Status == 'Recusado') {
            mioloMensagem = '<p style="padding: 20px; font-size: 15px;">Cliente recusou realizar o preenchimento do formulário. </p><p style="margin: 30px 0 20px 0; padding-left: 20px;font-size: 15px;"><img style="vertical-align:middle" src="Content/themes/base/images/print.png" /> Para imprimir o termo de recusa, clique <span id="link-print-termo-recusa" style="cursor:pointer; font-weight: bold;">aqui</span>.</p><p style="padding-left: 20px;font-size: 15px;"><img style="vertical-align:middle" src="Content/themes/base/images/edit.png" /> Para editar suas respostas clique <span id="link-edit" style="cursor:pointer; font-weight: bold;">aqui</span>.</p>';
        } else {
            mioloMensagem = '<p style="padding: 20px; font-size: 15px;">Com base nas respostas preenchidas, seu Perfil de Investidor é: <span class="perfil">' +
                CRMax.globals.perfilCotista.Perfil + '</span>.</p><p style="margin: 30px 0 20px 0; padding-left: 20px;font-size: 15px;"><img style="vertical-align:middle" src="Content/themes/base/images/print.png" /> Para imprimir suas respostas, clique <span id="link-print" style="cursor:pointer; font-weight: bold;">aqui</span>.</p><p style="padding-left: 20px;font-size: 15px;"><img style="vertical-align:middle" src="Content/themes/base/images/edit.png" /> Para editar suas respostas clique <span id="link-edit" style="cursor:pointer; font-weight: bold;">aqui</span>.</p>';
        }

        mensagemPreenchido.update(mioloMensagem);

        mensagemPreenchido.show();

        if (Ext.get('message-questionario')) {
            Ext.get('message-questionario').update(this.messageFormularioPreenchido);
        }

        this.down('#btn-atualizar').disable();
        this.down('#btn-aceitar').disable();
        this.down('#btn-dispensar').enable();

        this.buildLinks();


        //var btnGravar  =  this.down('#btn-gravar');
        //btnGravar.hide();
    },

    dispensar: function() {

        var tiposDispensa = CRMax.globals.tiposDispensa;

        var tiposDispensaStore = Ext.create('Ext.data.Store', {
            fields: ['IdDispensa', 'Descricao'],
            data: tiposDispensa
        });

        var comboTiposDispensa = {
            xtype: 'combo',
            name: 'IdDispensa',
            fieldLabel: 'Tipos de Dispensa',
            allowBlank: false,
            itemId: 'IdDispensa',
            store: tiposDispensaStore,
            queryMode: 'local',
            displayField: 'Descricao',
            valueField: 'IdDispensa',
            editable: false,
            width: 350,
            forceSelection: true
        };

        var msg = {
            xtype: 'component',
            html: '<h3 style="margin-bottom: 20px;">Selecione o tipo de dispensa:</h3>'
        };

        var form = {
            xtype: 'form',
            bodyStyle: 'padding: 20px;',
            items: [msg, comboTiposDispensa]
        }

        var winConfirma = Ext.create('Ext.window.Window', {
            title: 'Tipos de Dispensa',
            height: 290,
            width: 600,
            modal: true,
            layout: 'fit',
            border: false,
            bodyBorder: false,
            items: [form],

            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'Cancelar',
                scope: this,
                handler: function() {
                    winConfirma.close();
                }
            }, {
                text: 'Ok',
                baseCls: '',
                scope: this,
                cls: 'btn btn-inverse',
                handler: function() {
                    var formRendered = winConfirma.down('form');
                    var basicForm = formRendered.getForm();
                    var values = basicForm.getValues();

                    if (values.IdDispensa === undefined) {
                        Ext.Msg.alert('Aviso', 'Selecione uma opção antes de prosseguir');
                        return;
                    }

                    if (values.IdDispensa === 0) {
                        //Se nao se aplica, submeter sem mensagem de aceite
                        this.submitDispensa(winConfirma, values);
                    } else {
                        Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_6, function(btn) {
                            if (btn === 'yes') {
                                this.logMensagem(6, CRMax.globals.msgsSuitability.msg_6, 'OK');
                                this.submitDispensa(winConfirma, values);
                            } else {
                                this.logMensagem(6, CRMax.globals.msgsSuitability.msg_6, 'Cancelar');
                                winConfirma.close();
                            }
                        }, this);
                    }
                }
            }]
        }).show();

    },

    submitDispensa: function(win, values) {
        var that = this;
        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'Controllers/Suitability/Dispensa.ashx',
            params: values,
            success: function(response) {

                Ext.Ajax.request({
                    scope: this,
                    url: CRMax.globals.baseDirectory + 'controllers/application/loadglobals.ashx?reload=true',
                    success: function(response) {

                        var responseObject = Ext.JSON.decode(response.responseText);
                        that.updateGlobals(responseObject.perfilCotista);

                        that.updateInfoSuitability();

                        if (CRMax.globals.perfilCotista.Dispensado == 'Sim') {
                            that.displayMensagemFormularioPreenchido();
                        } else {
                            that.editForm();
                        }

                        if (win) {
                            win.close();
                        }
                    }
                });
            },
            failure: function(response) {
                if (!(response && response.responseText && (response.responseText.indexOf('Convert.ToInt32((Convert.ToString(context.Session') > 0))) {
                    Ext.Msg.alert('Aviso', 'Ocorreu um erro durante a gravação. Por favor, tente novamente.');
                }

            }
        });

    },

    submitAtualiza: function() {

        var form = this;

        var basicForm = this.getForm();

        if (!basicForm.isValid()) {
            Ext.Msg.alert('Aviso', CRMax.globals.msgsSuitability.msg_9);
            return;
        }

        basicForm.submit({
            url: CRMax.globals.baseDirectory + 'Controllers/Suitability/Atualiza.ashx',
            waitMsg: 'Processando...',
            waitTitle: 'Aguarde',
            scope: this,
            success: function(fp, action) {
                var responseObject = Ext.JSON.decode(action.response.responseText);

                responseObject.perfilCotista.Alertas = CRMax.globals.msgsSuitability.msg_1;

                form.updateInfoSuitability(responseObject.perfilCotista);

                form.down('#btn-dispensar').disable();
                form.down('#btn-atualizar').disable();
                form.down('#btn-aceitar').enable();

                Ext.Msg.alert('Perfil do Investidor', 'Perfil apurado: ' + responseObject.perfilCotista.Perfil);

            },
            failure: function(response) {
                if (!(response && response.responseText && (response.responseText.indexOf('Convert.ToInt32((Convert.ToString(context.Session') > 0))) {
                    Ext.Msg.alert('Aviso', 'Ocorreu um erro durante a gravação. Por favor, tente novamente.');
                }
            }
        });



    },

    saveForm: function() {
        var basicForm = this.getForm();
        basicForm.submit({
            params: {
                action: 'save'
            },
            url: CRMax.globals.baseDirectory + 'Controllers/Suitability/Save.ashx',
            waitMsg: 'Processando...',
            waitTitle: 'Aguarde',
            scope: this,
            success: function(fp, action) {
                var responseObject = Ext.JSON.decode(action.response.responseText);

                this.updateGlobals(responseObject.perfilCotista);

                this.displayMensagemFormularioPreenchido();

                Ext.Msg.alert('Aviso', 'Perfil de investidor atualizado');

                //CRMax.application.notificationWindow.update('Perfil de investidor atualizado');
                //CRMax.application.notificationWindow.show();

                CRMax.application.fireEvent('suitabilityupdated');
            },
            failure: function(response) {
                if (!(response && response.responseText && (response.responseText.indexOf('Convert.ToInt32((Convert.ToString(context.Session') > 0))) {
                    Ext.Msg.alert('Aviso', 'Ocorreu um erro durante a gravação. Por favor, tente novamente.');
                }
            }
        });
    },

    submitAceito: function() {
        var basicForm = this.getForm();
        if (!basicForm.isValid()) {
            Ext.Msg.alert('Aviso', CRMax.globals.msgsSuitability.msg_9);
            return;
        }

        if (!this.down('#btn-atualizar').isDisabled()) {
            Ext.Msg.alert('Aviso', CRMax.globals.msgsSuitability.msg_10);
            return;
        }


        Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_2, function(btn) {
            if (btn === 'yes') {
                this.logMensagem(2, CRMax.globals.msgsSuitability.msg_2, 'OK');

                Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_4, function(btn) {
                    if (btn === 'yes') {
                        this.logMensagem(4, CRMax.globals.msgsSuitability.msg_4, 'OK');
                        Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_15, function(btn) {
                            if (btn === 'yes') {
                                this.logMensagem(15, CRMax.globals.msgsSuitability.msg_15, 'OK');
                                this.saveForm();
                            } else {
                                this.logMensagem(15, CRMax.globals.msgsSuitability.msg_15, 'Cancelar');
                            }
                        }, this);
                    } else {
                        this.logMensagem(4, CRMax.globals.msgsSuitability.msg_4, 'Cancelar');
                    }
                }, this);

            } else {
                this.logMensagem(2, CRMax.globals.msgsSuitability.msg_2, 'Cancelar');
            }
        }, this);

    },

    cancelarDispensa: function() {
        var values = {
            IdDispensa: 0
        };

        this.submitDispensa(null, values);
    },

    submitRecusa: function() {
        var basicForm = this.getForm();

        Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_8, function(btn) {
            if (btn === 'yes') {
                this.logMensagem(8, CRMax.globals.msgsSuitability.msg_8, 'OK');
                Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_15, function(btn) {
                    if (btn === 'yes') {
                        this.logMensagem(15, CRMax.globals.msgsSuitability.msg_15, 'OK');
                        this.saveRecusa();
                    } else {
                        this.logMensagem(15, CRMax.globals.msgsSuitability.msg_15, 'Cancelar');
                    }
                }, this);
            } else {
                this.logMensagem(8, CRMax.globals.msgsSuitability.msg_8, 'Cancelar');
            }
        }, this);
    },

    updateGlobals: function(perfilCotista) {

        Ext.apply(CRMax.globals.perfilCotista, perfilCotista);

    },

    saveRecusa: function() {
        var form = this;
        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'Controllers/Suitability/Recusa.ashx',
            success: function(response) {

                Ext.Ajax.request({
                    scope: this,
                    url: CRMax.globals.baseDirectory + 'controllers/application/loadglobals.ashx?reload=true',
                    success: function(response) {

                        var responseObject = Ext.JSON.decode(response.responseText);
                        form.updateGlobals(responseObject.perfilCotista);
                        form.updateInfoSuitability();
                        form.displayMensagemFormularioPreenchido();

                    }
                });
            },
            failure: function(response) {
                if (!(response && response.responseText && (response.responseText.indexOf('Convert.ToInt32((Convert.ToString(context.Session') > 0))) {
                    Ext.Msg.alert('Aviso', 'Ocorreu um erro durante a gravação. Por favor, tente novamente.');
                }

            }
        });
    }

});