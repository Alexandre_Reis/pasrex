﻿Ext.define('CRMax.view.extrato.Filter', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.extratofilter',
    itemId: 'extrato-filter',
    autoScroll: true,
    title: 'Período do Extrato',
    //requires: ['Ext.ux.form.field.ClearButton'],
    border: false,
    bodyStyle: 'background-color: #f8f8f8; ',
    //style: 'border-left: 1px solid #ddd;border-bottom: 1px solid #ddd; ',
    initComponent: function () {

        var tipoExtratoStore = [];
        tipoExtratoStore.push(['sintetico', 'Consolidado']);
        tipoExtratoStore.push(['analitico', 'Detalhado']);
        tipoExtratoStore.push(['relatoriomensal', 'Relatório Mensal']);
        var comboTipoExtrato = {
              xtype: 'combo',
              fieldLabel: 'Tipo Extrato',
              name: 'TipoExtrato',
              store: tipoExtratoStore,
              queryMode: 'local',
              value: 'sintetico',
              width: 170,
              editable: false,
              forceSelection: true,
              listWidth: 50,
              listeners: {
                change: function(combo, newValue, oldValue){
                    var comboPeriodo = this.formPanel.getForm().findField('Periodo');
                    var comboPeriodoStore = comboPeriodo.getStore();
                    if(newValue == 'relatoriomensal'){
                        comboPeriodoStore.removeAll();
                        comboPeriodoStore.loadRawData(extratoStore);
                        comboPeriodo.setValue(undefined);
                    }else{
                        if(oldValue == 'relatoriomensal'){
                            comboPeriodoStore.removeAll();
                            comboPeriodoStore.loadRawData(periodoStore);
                            comboPeriodo.setValue('currentMonth');
                        }
                        this.buildFilter();
                    }
                },
                scope: this
            }
        };
        
        
        var periodoStore = [];
        periodoStore.push(['last7','Últimos 7 dias']);
        periodoStore.push(['last30','Últimos 30 dias']);
        periodoStore.push(['currentMonth','Este mês']);
        periodoStore.push(['period','Período Específico']);
        
        var extratoStore = [];
        
        for(var i=0; i < CRMax.globals.downloadExtrato.Documentos.length; i++){
            var documento = CRMax.globals.downloadExtrato.Documentos[i];
            extratoStore.push([documento.Path, documento.Nome]);
        }
        
        var comboPeriodo = {
              xtype: 'combo',
              fieldLabel: 'Período',
              name: 'Periodo',
              store: periodoStore,
              queryMode: 'local',
              value: 'currentMonth',
              width: 170,
              editable: false,
              forceSelection: true,
              listWidth: 50,
              listeners: {
                change: function(combo, newValue){
                    if(!newValue){
                        return;
                    }
                    
                    if(newValue != 'last7' && newValue != 'last30' && newValue != 'currentMonth' && newValue != 'period'){
                        var controllerLink = CRMax.globals.baseDirectory + 'controllers/documento/download.ashx?path=';
                        var linkExtrato = encodeURIComponent(newValue);
                        window.open(controllerLink + linkExtrato);
                        return;
                    }
                
                    var dataInicio = this.formPanel.down('#DataInicio');
                    var dataFim = this.formPanel.down('#DataFim');
                    
                    if(newValue == 'period'){
                        dataInicio.show();
                        dataFim.show();
                    }else{
                        dataInicio.hide();
                        dataFim.hide();
                    }
                
                    
                },
                scope: this
            }
        };
        
        var dataInicio = {
            xtype: 'datefield',
            name: 'DataInicio',
            itemId: 'DataInicio',
            submitFormat: 'Y-m-d',
            width: 150,
            hidden: true,
            fieldLabel: 'Início',
            format: 'j/n/Y',
            altFormats: 'jnY|dnY|dmY'
        };
        
        var dataFim = {
            xtype: 'datefield',
            name: 'DataFim',
            itemId: 'DataFim',
            submitFormat: 'Y-m-d',
            width: 150,
            hidden: true,
            fieldLabel: 'Fim',
            format: 'j/n/Y',
            altFormats: 'jnY|dnY|dmY'
        };
        
        var formFields = [comboTipoExtrato, comboPeriodo, dataInicio, dataFim];


        this.formPanel = Ext.create('Ext.form.Panel', {
            //title: 'Campos',
            items: formFields,
            bodyStyle: 'padding: 10px; background-color: #f3f3f3;',
            border: false,
            labelWidth: 50,
            defaults: {
            labelAlign: 'top',
                labelWidth: 50,
                //plugins: ['clearbutton'],
                listeners: {
                    scope: this
                    /*change: function (field, newValue) {
                        //Handle clear button plugin... we want to refresh store if field was cleared
                        if (!newValue || !newValue.length) {
                            this.buildFilter();
                        }
                    },
                    select: function () {
                        this.buildFilter();
                    },
                    blur: function () {
                        this.buildFilter();
                    },
                    specialkey: function (field, e) {
                        if (e.getKey() == e.ENTER || e.getKey() == e.ESC || e.getKey() == e.TAB) {
                            this.buildFilter();
                        }
                    }*/
                }
            }
        });


        this.items = [
             this.formPanel
        ];

        this.buttons = [
                '->',
                { text: 'Pesquisar',
                    handler: this.buildFilter,
                    scope: this
                }];


        this.callParent(arguments);
    },


    clearFilter: function () {
        this.clearFormFields();

        var financeList = this.getFinanceList();
        financeList.setStoreFilter([]);
    },

    clearFormFields: function () {
        //turn off fieltering
        this.filtering = false;

        var basicForm = this.formPanel.getForm();
        var formFields = basicForm.getFields();

        formFields.each(function (formField) {
            formField.setValue('');
        });

        this.filtering = true;
    },

    onButtonFilterPaymentStatusClick: function (button) {
        var topButton = button.up('button');
        topButton.setText(button.text);
        topButton.filterValue = button.paymentStatusId;

        this.buildFilter();
    },

    getList: function () {
        return Ext.getCmp(this.listId);
    },

    buildFilter: function () {

        
        
        if (this.filtering === false) {
            //Sometimes we need to disable filtering to prevent unwanted requests, when clearing all form fields in batch, for example
            return;
        }

        //this.filtering = true;
        var basicForm = this.formPanel.getForm();
        var list = this.getList();

        var filters = [];
        
        var formFields = basicForm.getFields();

        formFields.each(function (formField) {
            var value = formField.getValue();
            if (value && Ext.isString(value)) {
                value = value.replace('\t', '');
            }

            if (formField.getXType() == 'textfield') {
                formField.setRawValue(value);
            }

            if (value) {
                filters.push({
                    property: formField.name,
                    value: value
                });
            }
        });

        list.setStoreFilter(filters);
    }
});

