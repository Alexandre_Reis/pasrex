﻿Ext.define('CRMax.view.extrato.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.extratolist',
    hideHeaders: false,
    store: 'Extratos',
    border: false,
    features: [{ftype:'grouping', groupHeaderTpl: '{name}'}],

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function () {
        
        this.columns = [
            {dataIndex: 'DataHistorico', width: 80, header: 'Data', type: 'date', xtype: 'datecolumn',format: 'd/m/Y' },
            {dataIndex: 'Historico', width: 80, header: 'Histórico'},
            {dataIndex: 'Quantidade', header: 'Qtde. Cotas', width: 90, xtype: 'numbercolumn', format:'0,000.0000', align: 'right'},
            {dataIndex: 'CotaDia', header: 'Valor Cota (R$)', width: 110, xtype: 'numbercolumn', format:'0,000.000000', align: 'right'},
            {dataIndex: 'ValorBruto', header: 'Valor Bruto (R$)', width: 100, xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'ValorIR', header: 'IR (R$)', width: 70, xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'ValorIOF', header: 'IOF (R$)', width: 60, xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'ValorLiquido', header: 'Valor Líquido (R$)', width: 110, xtype: 'numbercolumn', format:'0,000.00', align: 'right'}
        ];
        
        this.tbar = [{
             text: 'Imprimir',
             //iconCls: 'icon-print',
             scope: this,
             baseCls: '',
                   cls: 'btn-aplicacao',
                   
             handler: this.onButtonPrintWYSIWYGClick
         }];

        this.callParent(arguments);
    },

    loadStore: function () {
        this.store.load();
    },
    onButtonPrintWYSIWYGClick: function () {
        var el = this.getEl();
        var lastFilter = this.getStore().lastFilter;
        
        if(!lastFilter){
            lastFilter = [{property: 'periodo', value: 'currentMonth'}];
        }
        
        var dataFim = Ext.clone(CRMax.globals.today);
        var dataInicio = Ext.clone(dataFim);
        if(lastFilter[0].value == 'last7'){
            dataInicio.setDate(dataInicio.getDate() - 7);
        }else if(lastFilter[0].value == 'last30'){
            dataInicio.setDate(dataInicio.getDate() - 30);
        }else if(lastFilter[0].value == 'currentMonth'){
            dataInicio = new Date(dataInicio.getFullYear(), dataInicio.getMonth(), 1);
        }else if(lastFilter[0].value == 'period'){
            dataInicio = lastFilter[1].value;
			dataFim = lastFilter[2].value;
        }
        
        var html = [
                '<!DOCTYPE html>',
                '<html>',
                '<head>',
                    '<!--meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /-->',
                    '<meta charset="utf-8" />',
                    '<title>Extrato</title>',
                    '<link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />',
                    '<link href="Content/Site.css" rel="stylesheet" type="text/css" />',
                    '<link href="Content/print-grid.css" rel="stylesheet" type="text/css" />',
                    
                '</head>',
                '<body class="grid-print order-grid ">',
                '<div class="header">',
                    '<img src="Content/themes/base/images/logo_cliente.gif" alt="Logo" />',
                    '<div id="form-title">Extrato</div>',
                    '<div id="form-header-detail">Cliente: <b>{1}</b> <br />Período: <b>{2}</b> a <b>{3}</b></div>',
                '</div>',
                '<div class="div-wrapper" style="position: relative">',
                '{0}',
                '</div>',
                    '<script type="text/javascript">window.print()</script>',
                 '</body>',
                 '</html>'];

        html = Ext.String.format(html.join(''), el.dom.innerHTML, 
            CRMax.globals.userInfo.Login, Ext.Date.format(dataInicio, "j/M/Y"), Ext.Date.format(dataFim, "j/M/Y"));

        //open up a new printing window, write to it, print it and close
        var win = window.open('', 'printgrid');

        //document must be open and closed
        win.document.open();
        win.document.write(html);
        win.document.close();

        //win.print();

        /*if (Ext.isIE) {
            window.close();
        } else {
            win.close();
        }*/

    },
    setStoreFilter: function (filter) {

        var store = this.getStore();
        if (!store.lastFilter) {
            store.lastFilter = [];
        }

        if (Ext.encode(filter) === Ext.encode(store.lastFilter)) {
            //Prevent double submission if filter hasn't changed
            return;
        }

        store.clearFilter(true);
        store.lastFilter = Ext.clone(filter);
        store.filter(filter);

    }
});