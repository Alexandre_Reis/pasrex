﻿Ext.define('CRMax.view.operacao.Filter', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.operacaofilter',
    itemId: 'operacao-filter',
    autoScroll: true,
    title: 'Pesquisar Operações',
    //requires: ['Ext.ux.form.field.ClearButton'],
    border: false,
    bodyStyle: 'background-color: #f8f8f8; ',
    //style: 'border-left: 1px solid #ddd;border-bottom: 1px solid #ddd; ',
    initComponent: function () {

        var periodoStore = [];
        periodoStore.push(['last7','Últimos 7 dias']);
        periodoStore.push(['last30','Últimos 30 dias']);
        periodoStore.push(['currentMonth','Este mês']);
        periodoStore.push(['period','Período Específico']);
        
        var comboPeriodo = {
              xtype: 'combo',
              fieldLabel: 'Período',
              name: 'Periodo',
              store: periodoStore,
              queryMode: 'local',
              value: 'last7',
              listeners: {
                change: function(combo, newValue){
                    var dataInicio = this.formPanel.down('#DataInicio');
                    var dataFim = this.formPanel.down('#DataFim');
                    
                    if(newValue == 'period'){
                        dataInicio.show();
                        dataFim.show();
                    }else{
                        dataInicio.hide();
                        dataFim.hide();
                    }
                },
                scope: this
            }
        };
        
        var dataInicio = {
            xtype: 'datefield',
            name: 'DataInicio',
            itemId: 'DataInicio',
            submitFormat: 'Y-m-d',
            width: 200,
            hidden: true,
            fieldLabel: 'Início',
            format: 'j/n/Y',
            altFormats: 'jnY|dnY|dmY'
        };
        
        var dataFim = {
            xtype: 'datefield',
            name: 'DataFim',
            itemId: 'DataFim',
            submitFormat: 'Y-m-d',
            width: 200,
            hidden: true,
            fieldLabel: 'Fim',
            format: 'j/n/Y',
            altFormats: 'jnY|dnY|dmY'
        };
                
        var comboFundosCotista = {
              xtype: 'combo',
              fieldLabel: 'Fundo',
              name: 'IdCarteira',
              store: 'FundosCotistaOperacao',
              queryMode: 'local',
              displayField: 'Nome',
              valueField: 'IdCarteira'
        };
        
        var tipoOperacaoStore = [];
        tipoOperacaoStore.push([1,'Aplicação']);
        tipoOperacaoStore.push([2,'Resgate']);
        
        var comboTipoOperacao = {
              xtype: 'combo',
              fieldLabel: 'Tipo de Operação',
              name: 'TipoOperacao',
              store: tipoOperacaoStore,
              queryMode: 'local'
        };
        
        var statusOrdemIntegracaoStore = [];
        
        statusOrdemIntegracaoStore.push([110,'Agendada']);
        statusOrdemIntegracaoStore.push([111,'Cancelada']);
        statusOrdemIntegracaoStore.push([112,'Processando']);
        if(CRMax.globals.userInfo.Permissoes.IsGestor || CRMax.globals.userInfo.Permissoes.IsNotExterno){
            statusOrdemIntegracaoStore.push([113,'Saldo Insuficiente']);
            statusOrdemIntegracaoStore.push([114,'Erro no Envio']);
        }
        statusOrdemIntegracaoStore.push([250,'Efetivada']);
        
        var comboStatusOrdem = {
              xtype: 'combo',
              fieldLabel: 'Status',
              name: 'StatusIntegracao',
              store: statusOrdemIntegracaoStore,
              queryMode: 'local'
        };
        
        var formFields = [comboPeriodo, dataInicio, dataFim, comboFundosCotista, comboTipoOperacao, comboStatusOrdem];

        this.formPanel = Ext.create('Ext.form.Panel', {
            //title: 'Campos',
            items: formFields,
            bodyStyle: 'padding: 10px; background-color: #f3f3f3;',
            border: false,
            defaults: {
            
                //plugins: ['clearbutton'],
                listeners: {
                    scope: this
                    /*change: function (field, newValue) {
                        //Handle clear button plugin... we want to refresh store if field was cleared
                        if (!newValue || !newValue.length) {
                            this.buildFilter();
                        }
                    },
                    select: function () {
                        this.buildFilter();
                    },
                    blur: function () {
                        this.buildFilter();
                    },
                    specialkey: function (field, e) {
                        if (e.getKey() == e.ENTER || e.getKey() == e.ESC || e.getKey() == e.TAB) {
                            this.buildFilter();
                        }
                    }*/
                }
            }
        });


        this.items = [
             this.formPanel
        ];

        this.buttons = [
                
                { text: 'Limpar Filtros',
                    style: 'margin-right: 3px;',
                    handler: this.clearFilter,
                    scope: this
                },'->',
                { text: 'Pesquisar',
                    handler: this.buildFilter,
                    scope: this
                }];


        this.callParent(arguments);
    },


    clearFilter: function () {
        this.clearFormFields();

    },

    clearFormFields: function () {
        //turn off fieltering
        this.filtering = false;

        var basicForm = this.formPanel.getForm();
        var formFields = basicForm.getFields();

        formFields.each(function (formField) {
            //formField.setValue('');
            formField.reset();
        });

        this.filtering = true;
        this.buildFilter();
    },

    onButtonFilterPaymentStatusClick: function (button) {
        var topButton = button.up('button');
        topButton.setText(button.text);
        topButton.filterValue = button.paymentStatusId;

        this.buildFilter();
    },

    getList: function () {
        return Ext.getCmp(this.listId);
    },

    buildFilter: function () {

        if (this.filtering === false) {
            //Sometimes we need to disable filtering to prevent unwanted requests, when clearing all form fields in batch, for example
            return;
        }

        //this.filtering = true;
        var basicForm = this.formPanel.getForm();
        var list = this.getList();

        var filters = [];
        
        var formFields = basicForm.getFields();

        formFields.each(function (formField) {
            var value = formField.getValue();
            if (value && Ext.isString(value)) {
                value = value.replace('\t', '');
            }

            if (formField.getXType() == 'textfield') {
                formField.setRawValue(value);
            }

            if (value) {
                filters.push({
                    property: formField.name,
                    value: value
                });
            }
        });

        list.setStoreFilter(filters);
    }
});

