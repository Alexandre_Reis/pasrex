﻿Ext.define('CRMax.view.operacao.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.operacaoedit',
    title: 'Editar Operação',
    autoScroll: true,

    modal: true,
    shadow: false,
    resizable: false,
    border: false,
    draggable: false,
    width: 600,
    tipoResgateEnum: {
        parcial: 1,
        total: 2
    },
    tipoOperacaoEnum: {
        aplicacao: 1,
        resgateBruto: 2,
        resgateLiquido: 3,
        resgateTotal: 5
    },

    initComponent: function() {

        var contasCotista = CRMax.globals.contasCorrentes;

        var contasCotistaStore = Ext.create('Ext.data.Store', {
            fields: ['IdConta', 'Descricao'],
            data: contasCotista
        });
        var contaCotistaDefault = contasCotista.length > 0 ? contasCotista[0].IdConta : undefined;

        var formasLiquidacao = CRMax.globals.formasLiquidacao;
        var formasLiquidacaoStore = Ext.create('Ext.data.Store', {
            fields: ['IdFormaLiquidacao', 'Descricao'],
            data: formasLiquidacao
        });

        var isAplicacao = this.tipoOperacao === this.tipoOperacaoEnum.aplicacao;

        this.setTitle(isAplicacao ? 'Aplicar' : 'Resgatar');

        this.formPanel = new Ext.form.Panel({
            fieldDefaults: {
                msgTarget: 'under'
            },

            bodyStyle: 'padding: 20px;',
            itemId: 'operacao-form',
            items: [{
                    xtype: 'combo',
                    name: 'IdCarteira',
                    fieldLabel: 'Fundo',
                    allowBlank: false,
                    itemId: 'IdCarteira',
                    store: 'FundosCotista',
                    queryMode: 'local',
                    displayField: 'Nome',
                    valueField: 'IdCarteira',
                    editable: false,
                    readOnly: true,
                    value: this.idCarteira,
                    width: 500,
                    forceSelection: true
                }, {
                    xtype: 'datefield',
                    name: 'DataOperacao',
                    fieldLabel: 'Data',
                    itemId: 'DataOperacao',
                    value: new Date(),
                    allowBlank: false,
                    submitFormat: 'Y-m-d',
                    altFormats: 'jnY|dnY|dmY'
                }, {
                    xtype: 'radiogroup',
                    fieldLabel: 'Tipo de Resgate',
                    columns: 1,
                    hidden: isAplicacao,
                    itemId: 'tiporesgate-radio',
                    vertical: true,
                    listeners: {
                        change: function(radio, newValue) {
                            var valorBrutoField = this.formPanel.down('#ValorBruto');
                            if (newValue.TipoResgate === this.tipoResgateEnum.parcial) {
                                valorBrutoField.show();
                                this.tipoResgate = this.tipoResgateEnum.parcial;
                            } else {
                                valorBrutoField.hide();
                                valorBrutoField.setValue(0);
                                this.tipoResgate = this.tipoResgateEnum.total;
                            }
                        },
                        scope: this
                    },
                    items: [{
                        boxLabel: 'Parcial',
                        name: 'TipoResgate',
                        style: 'margin-bottom: 20px',
                        inputValue: this.tipoResgateEnum.parcial,
                        checked: false
                    }, {
                        boxLabel: 'Total',
                        name: 'TipoResgate',
                        inputValue: this.tipoResgateEnum.total,
                        checked: false
                    }]
                },
                /*{
            xtype: 'textfield',
			plugins: 'textmask',
			mask: 'R$ #9.999.999.990,00',
			money: true,

            fieldLabel: 'Valor',			
			hidden: !isAplicacao,
			allowBlank: false,
			itemId: 'ValorBruto',
            name: 'ValorBrutoMask'
              
        }*/
                ,

                {
                    xtype: 'numericfield',
                    itemId: 'ValorBruto',
                    name: 'ValorBrutoMask',
                    decimalSeparator: ',',
                    decimalPrecision: 2,
                    allowNegative: false,
                    alwaysDisplayDecimals: true,
                    hideTrigger: true,
                    useThousandSeparator: true,
                    hidden: !isAplicacao,
                    fieldLabel: 'Valor',
                    allowBlank: false,
                    currencySymbol: 'R$'
                }, {
                    xtype: 'combo',
                    name: 'IdConta',
                    fieldLabel: 'Conta para Crédito',
                    allowBlank: true,
                    itemId: 'IdConta',
                    store: contasCotistaStore,
                    queryMode: 'local',
                    hidden: isAplicacao,
                    displayField: 'Descricao',
                    valueField: 'IdConta',
                    editable: false,
                    value: contaCotistaDefault,
                    width: 350,
                    forceSelection: true
                }

            ],
            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'Cancelar',
                scope: this,
                handler: this.close
            }, {
                text: isAplicacao ? 'Aplicar' : 'Resgatar',
                baseCls: '',
                scope: this,
                cls: 'btn btn-inverse',
                handler: this.validateForm
            }]

        });

        this.items = [this.formPanel];

        this.callParent(arguments);
    },

    displayWinConfirma: function(isAplicacao, tipoOperacaoString, nomeFundo, dataOperacao, valorFormatado, contaParaCredito, tipoOperacao, valorBruto) {

        var detalheValor = (tipoOperacaoString.indexOf('total') >= 0) ? '' : Ext.String.format('<p>Valor: {0}</p><br/>', valorFormatado);

        var detalheContaParaCredito = isAplicacao ? '' : Ext.String.format('<p>Conta para Crédito: {0}</p><br/>', contaParaCredito);

        var msgConfirma = Ext.String.format('<p>Por favor, verifique os dados da operação antes de prosseguir:</p><br />' +
            '<p>Fundo: {3}</p><br/>' +
            '<p>Data da Operação: {0}</p><br/>' +
            '{2}' +
            '{4}' +
            '<br /><p><b>Deseja confirmar {1}?</b></p>', dataOperacao, tipoOperacaoString.toLowerCase().replace('aplicação', 'a aplicação').replace('resgate', 'o resgate'),
            detalheValor, nomeFundo, detalheContaParaCredito);

        var winConfirma = Ext.create('Ext.window.Window', {
            title: 'Confirmação',
            height: 290,
            width: 600,
            modal: true,
            layout: 'fit',
            border: false,
            bodyBorder: false,
            items: { // Let's put an empty grid in just to illustrate fit layout
                xtype: 'panel',
                border: false,
                bodyBorder: false,
                bodyStyle: 'padding: 20px',
                html: msgConfirma
            },
            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'Não',
                scope: this,
                handler: function() {
                    winConfirma.close();
                }
            }, {
                text: 'Sim',
                baseCls: '',
                scope: this,
                cls: 'btn btn-inverse',
                handler: function() {
                    this.saveForm(tipoOperacao, valorBruto);
                    winConfirma.close();
                }
            }]
        }).show();

    },

    validateForm: function() {
        var basicForm = this.formPanel.getForm();



        var tipoOperacao;
        var tipoOperacaoString;
        var isAplicacao = false;
        if (this.tipoOperacao == this.tipoOperacaoEnum.aplicacao) {
            isAplicacao = true;
            tipoOperacao = this.tipoOperacaoEnum.aplicacao;
            tipoOperacaoString = 'Aplicação';
        } else if (this.tipoResgate === this.tipoResgateEnum.parcial) {
            tipoOperacao = this.tipoOperacaoEnum.resgateLiquido;
            tipoOperacaoString = 'Resgate parcial';
        } else if (this.tipoResgate === this.tipoResgateEnum.total) {
            tipoOperacao = this.tipoOperacaoEnum.resgateTotal;
            tipoOperacaoString = 'Resgate total';
        } else {
            Ext.Msg.alert('Aviso', 'Por favor, selecione o tipo de resgate');
            return false;
        }

        if (!basicForm.isValid()) {
            return false;
        }

        var idContaField = this.formPanel.down('#IdConta');
        if (isAplicacao) {
            idContaField.setValue('');
        } else {
            if (idContaField.getValue() == '') {
                Ext.Msg.alert('Aviso', 'Por favor, selecione a conta para crédito');
                return false;
            }
        }

        var valorBruto = this.formPanel.down('#ValorBruto').getValue();
        //valorBruto = valorBruto * 100;

        var nomeFundo = this.formPanel.down('#IdCarteira').getRawValue();
        var dataOperacao = Ext.util.Format.date(this.formPanel.down('#DataOperacao').getValue(), 'd/m/Y');
        var valorFormatado = this.formPanel.down('#ValorBruto').getRawValue();
        var contaParaCredito = idContaField.getRawValue();

        this.displayWinConfirma(isAplicacao, tipoOperacaoString, nomeFundo, dataOperacao, valorFormatado, contaParaCredito, tipoOperacao, valorBruto);

    },


    trataErroTermoAdesaoAceito: function(errorCode, errorMessage, paramsWorkflowSuitability, tipoOperacao, valorBruto){
        Ext.MessageBox.confirm('', errorMessage, function(btn) {
            if (btn === 'yes') {
               this.logMensagem(errorCode, errorMessage, 'OK');
               paramsWorkflowSuitability.TermoAdesaoAceito = true;
               this.saveForm(tipoOperacao, valorBruto, paramsWorkflowSuitability);
            } else {
                this.logMensagem(errorCode, errorMessage, 'Cancelar');
                return false;
            }
        }, this);
    },
    
    trataErroDesenquadramento: function(errorCode, errorMessage, paramsWorkflowSuitability, tipoOperacao, valorBruto){
        Ext.MessageBox.confirm('', errorMessage, function(btn) {
            if (btn === 'yes') {
               //Se o usuario concordar, cancelar o registro da movimentacao para que ele possa preencher o questionario
               this.logMensagem(errorCode, errorMessage, 'OK');
               
               CRMax.application.editSuitability = true;
               CRMax.application.getController('MainNavigator').selectNodeSpecial('suitability');
               this.close();
               
            } else {
                this.logMensagem(errorCode, errorMessage, 'Cancelar');
                Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_19, function(btn) {
                    if (btn === 'yes') {
                        this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'OK');
                        paramsWorkflowSuitability.DesenquadramentoAceito = true;
                        this.saveForm(tipoOperacao, valorBruto, paramsWorkflowSuitability);
                    } else {
                        //cancelou
                        this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'Cancelar');
                    }
                }, this);
                
            }
        }, this);
    },
    

    logMensagem: function(idMensagem, mensagem, resposta){
        Ext.Ajax.request({
                    scope: this,
                    url: CRMax.globals.baseDirectory + 'controllers/suitability/logmensagem.ashx?idmensagem=' + idMensagem + '&mensagem=' + mensagem + '&resposta=' + resposta,
                    success: function(response) {

                    }
                });
    },

    trataErroSuitabilitySaveForm: function(errorCode, errorMessage, paramsWorkflowSuitability, tipoOperacao, valorBruto) {
        paramsWorkflowSuitability = paramsWorkflowSuitability || {};
        
        var propertySuitability;
        if(errorCode == 17){
            errorMessage = CRMax.globals.msgsSuitability.msg_17;
            this.trataErroTermoAdesaoAceito(errorCode, errorMessage, paramsWorkflowSuitability, tipoOperacao, valorBruto);
        }else if(errorCode == 14){
            errorMessage = CRMax.globals.msgsSuitability.msg_14;
            this.trataErroDesenquadramento(errorCode, errorMessage, paramsWorkflowSuitability, tipoOperacao, valorBruto);
        }
    },

    saveForm: function(tipoOperacao, valorBruto, paramsWorkflowSuitability) {

        var params = {
                TipoOperacao: tipoOperacao,
                action: 'save',
                ValorBruto: valorBruto
            };
            
        if(paramsWorkflowSuitability){
            Ext.apply(params, paramsWorkflowSuitability);
        }

        var basicForm = this.formPanel.getForm();
        basicForm.submit({
            params: params,
            url: CRMax.globals.baseDirectory + 'Controllers/OperacaoCotista/Save.ashx',
            waitMsg: 'Processando...',
            waitTitle: 'Aguarde',
            scope: this,
            success: function(fp, action) {
                var responseObject = Ext.JSON.decode(action.response.responseText);

                if (responseObject.errorMessage && responseObject.errorMessage.length > 0) {
                    if (responseObject.erroSuitability > 0) {
                        this.trataErroSuitabilitySaveForm(responseObject.erroSuitability, responseObject.errorMessage, paramsWorkflowSuitability, tipoOperacao, valorBruto);
                        return;
                    }

                    Ext.Msg.alert('Aviso', responseObject.errorMessage);
                } else {
                    Ext.Msg.alert('Aviso', 'Operação enviada com sucesso!');
                    //CRMax.application.notificationWindow.update('Operação enviada com sucesso');
                    //CRMax.application.notificationWindow.show();
                }
                
                if(responseObject.perfilCotista){
                    Ext.apply(CRMax.globals.perfilCotista, responseObject.perfilCotista);
                }
                
                CRMax.application.fireEvent('operacaoupdated');
                this.close();
            },
            failure: function(form, action) {
                var responseObject = Ext.JSON.decode(action.response.responseText);
                var msgAlerta = responseObject && responseObject.errorMessage && responseObject.errorMessage.length ?
                    responseObject.errorMessage : 'Ocorreu um erro na comunicação';
                Ext.Msg.alert('Aviso', msgAlerta);
            }
        });

    }
});