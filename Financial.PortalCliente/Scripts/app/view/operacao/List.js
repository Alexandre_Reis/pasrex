﻿Ext.define('CRMax.view.operacao.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.operacaolist',
    hideHeaders: false,
    store: 'Operacoes',
    border: false,

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function () {
        this.columns = [
            {dataIndex: 'DataOperacao', width: 80, header: 'Data', type: 'date', xtype: 'datecolumn',format: 'd/m/Y' },
            {dataIndex: 'NomeCarteira', width: 170, header: 'Fundo'},
            {dataIndex: 'TipoOperacaoString', width: 100, header: 'Tipo de Operação'},
            {dataIndex: 'Valor', width: 80, header: 'Valor (R$)', xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            { header: 'Tipo Liquidação',  dataIndex: 'FormaLiquidacao', width: 100 },
            { header: 'Conta Corrente',  dataIndex: 'ContaCorrente', width: 150 },
            {dataIndex: 'StatusProcessamento', 
                renderer: function(v, meta, record){
                    var status = v;
                    if(!(CRMax.globals.userInfo.Permissoes.IsGestor || CRMax.globals.userInfo.Permissoes.IsNotExterno)){
                        //Se usuario nao eh gestor ou eh externo, maquiar alguns dos status
                        if(v == 'Saldo Insuficiente' || v == 'Erro no Envio'){
                            status = 'Processando';
                        }
                    }
                    
                    return status;
                }, width: 100, header: 'Status'},
                
            {dataIndex: 'StatusCotizacao', width: 80, header: 'Cotização'},    
                
            {
                xtype:'actioncolumn',
                width:64,
                //hidden: !CRMax.globals.userInfo.Permissoes.PodeDistribuir,
                renderer: function(v, meta, record){
                    return (record.get('StatusProcessamento') == 'Agendada' || record.get('StatusProcessamento') == 'Saldo Insuficiente')? 
                        '<div class="x-action-col-0 x-action-col x-action-col-resgatar">cancelar</div>' : 
                        ''},
                tooltip: 'cancelar',
                scope: this,
                handler: this.onClickButtonRejeitar
            }
        ];
        
        if(CRMax.globals.userInfo.IdCotista == -1){
            this.columns[1].width = 125;
            this.columns.splice(1,0,{dataIndex: 'NomeCotista', width: 125, header: 'Cotista'});
        }

        this.tbar = [
                {
                   text: 'Imprimir',
                   //iconCls: 'icon-print',
                   scope: this,
                   baseCls: '',
                   cls: 'btn-aplicacao',
                   
                   handler: this.onButtonPrintWYSIWYGClick
                }
            ];

        this.callParent(arguments);
        
        CRMax.application.on('operacaoupdated', this.loadStore, this);
        CRMax.application.on('ordemupdated', this.loadStore, this);
        this.loadStore();
    },

    onClickButtonRejeitar: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var idOperacao = record.get('IdOperacao');
        gridPanel.updateStatusOrdem(idOperacao, 'rejeitar', 'Cancelamento efetuado com sucesso.');
    },

    updateStatusOrdem: function(idOperacao, action, msgSucesso){
        var gridPanel = this;
        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'controllers/operacaocotista/updateStatus.ashx',
            params: {
                idOperacao: idOperacao,
                action: action
            },
            success: function(response){
                Ext.Msg.alert('Aviso',msgSucesso); 
                //CRMax.application.notificationWindow.update(msgSucesso);
                //CRMax.application.notificationWindow.show();
                CRMax.application.fireEvent('operacaoupdated');
                gridPanel.getStore().load();
            }
        });
    },

    loadStore: function () {
        this.store.load();
    },
    
    onButtonPrintWYSIWYGClick: function () {
        var el = this.getEl();

        var html = [
                '<!DOCTYPE html>',
                '<html>',
                '<head>',
                    '<!--meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /-->',
                    '<meta charset="utf-8" />',
                    '<title>Extrato</title>',
                    '<link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />',
                    '<link href="Content/Site.css" rel="stylesheet" type="text/css" />',
                    '<link href="Content/print-grid.css" rel="stylesheet" type="text/css" />',
                    //'<link href="Content/print.css" rel="stylesheet" type="text/css" />',
                '</head>',
                '<body class="grid-print order-grid ">',
                /*'<div class="header">',
                    '<img src="Content/themes/base/images/logo_cliente.gif" alt="Logo" />',
                    '<div id="form-title">Extrato</div>',
                '</div>',*/
                '{0}',
                 '</body>',
                 '</html>'];

        html = Ext.String.format(html.join(''), el.dom.innerHTML);

        //open up a new printing window, write to it, print it and close
        var win = window.open('', 'printgrid');

        //document must be open and closed
        win.document.open();
        win.document.write(html);
        win.document.close();

        win.print();

        /*if (Ext.isIE) {
            window.close();
        } else {
            win.close();
        }*/

    },
    
    setStoreFilter: function (filter) {

        var store = this.getStore();
        if (!store.lastFilter) {
            store.lastFilter = [];
        }

        if (Ext.encode(filter) === Ext.encode(store.lastFilter)) {
            //Prevent double submission if filter hasn't changed
            return;
        }

        store.clearFilter(true);
        store.lastFilter = Ext.clone(filter);
        store.filter(filter);

    }
});