﻿Ext.define('CRMax.view.fundo.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.fundolist',
    hideHeaders: false,
    store: {
        type: 'fundoscotista'
    },

    border: false,

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function () {
        this.columns = [
            {dataIndex: 'Nome', flex: 1, header: 'Fundo'},
            {dataIndex: 'ValorMinimoInicial', width: 100, header: 'Aplic. Inicial (R$)', xtype: 'numbercolumn', format:'0,000', align: 'right'},
            {dataIndex: 'HorarioFim', width: 110, header: 'Hora Limite', type: 'date', align: 'center', xtype: 'datecolumn',format: 'H:i'},
            {dataIndex: 'Categoria', width: 200, header: 'Categoria' },
            {
                xtype:'actioncolumn',
                renderer: function(v, meta){return '<div class="x-action-col-0 x-action-col x-action-col-aplicar">aplicar</div>'},
                width:50,
                hidden: CRMax.globals.isTesouraria,
                items: [{
                    scope: this,
                    handler: this.onClickButtonAplicar
                }]
            },
            {
                xtype:'actioncolumn',
                renderer: function(v, meta){return '<div class="x-action-col-0 x-action-col x-action-col-info">+info</div>'},
                width:45,
                items: [{
                    scope: this,
                    handler: this.onClickButtonDetalhes
                }]
            }
        ];

        this.callParent(arguments);
    },

    /*onClickButtonAplicar: function(grid, rowIndex, colIndex, item, e, record, row){
        if(CRMax.globals.userInfo.CotistaPodeOperar !== true){
            Ext.Msg.alert('Aviso','Não é possível realizar aplicações. Motivo: ' + CRMax.globals.userInfo.PendenciaCadastral);
            return false;
        }
        
        var temPermissaoAplicar = record.get('TemPermissaoAplicar');
        if(!temPermissaoAplicar){
            this.showWindowSemPermissaoAplicarFundo(record);
        }else{
            this.showWindowAvisoAplicacao(record);
            
        }
    },*/
    
    onClickButtonAplicar: function(grid, rowIndex, colIndex, item, e, record, row) {
         if (CRMax.globals.userInfo.CotistaPodeOperar !== true) {
            Ext.Msg.alert('Aviso', 'Não é possível realizar aplicações. Motivo: ' + CRMax.globals.userInfo.PendenciaCadastral);
            return false;
        }
        
        var temPermissaoAplicar = record.get('TemPermissaoAplicar');
        if(!temPermissaoAplicar){
            this.showWindowSemPermissaoAplicarFundo(record);
            return false;
        }

        var dispensado = CRMax.globals.perfilCotista.Dispensado == 'Sim';
        var recusou = CRMax.globals.perfilCotista.Status == 'Recusado';
        var temPerfil = CRMax.globals.perfilCotista.Perfil !== null && CRMax.globals.perfilCotista.Perfil !== "" && CRMax.globals.perfilCotista.Perfil != 'Indefinido.';

        var gridPanel = grid.up('gridpanel');
        if (dispensado || recusou) {
            gridPanel.showWindowAvisoAplicacao(record);
        } else {
            //Se cliente se recusou a preencher o perfil ou nao tem perfil preenchido, nao continuar
            if (!temPerfil) {
                Ext.Msg.alert('Aviso', CRMax.globals.msgsSuitability.msg_11);
                return false;
            }

            //Checar data de validade do perfil
            if (!CRMax.globals.perfilCotista.DentroValidade) {
                //Exibir mensagem de perfil desatualizado
                Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_12, function(btn) {
                    if (btn === 'yes') {
                        this.logMensagem(12, CRMax.globals.msgsSuitability.msg_12, 'OK');

                        //Exibir mensagem do termo de inadequacao
                        Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_19, function(btn) {
                            if (btn === 'yes') {
                                this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'OK');
                                gridPanel.showWindowAvisoAplicacao(record);
                            } else {
                                this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'Cancelar');
                                return false;
                            }
                        }, this);
                    } else {
                        this.logMensagem(12, CRMax.globals.msgsSuitability.msg_12, 'Cancelar');
                        return false;
                    }
                }, this);
            }else{
                gridPanel.showWindowAvisoAplicacao(record);
            }
        }
    },
    
    logMensagem: function(idMensagem, mensagem, resposta) {
        Ext.Ajax.request({
            scope: this,
            url: CRMax.globals.baseDirectory + 'controllers/suitability/logmensagem.ashx?idmensagem=' + idMensagem + '&mensagem=' + mensagem + '&resposta=' + resposta,
            success: function(response) {

            }
        });
    },
    
    showWindowAvisoAplicacao: function(record){
        var html = '<div class="info-aplic-fundo">';
        html += '<div class="box-intrucoes-aplicacao">';
        html += '<p>Para efetivação da aplicação é necessário providenciar a transferência do montante através de TED/DOC/TEF de acordo com seus dados previamente informados em seu cadastro.</p>';
        html += '<p>O horário limite para movimentações é 15h. Solicitações recebidas após este horário serão processadas automaticamente no dia útil subsequente.</p>';
        html += '<p>Dúvidas entre em contato com a Central de Atendimento: (11) 3366-3370 (dias úteis das 8h15 às 17h30).</p>';
        html += '<br /><p>Você já realizou a transferência?</p>';
        html += '</div>';
        html += '</div>';
        var that = this;
        var winAvisoAplicacao = Ext.create('Ext.window.Window', {
            title: 'Aplicação em Fundo de Investimento',
            height: 400,
            width: 800,
            y: CRMax.application.inIframe() ? 50 :undefined,
            modal: true,
            layout: 'fit',
            border: false,
            //items: component
            html: html,
            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'NÃO',
                handler: function(){winAvisoAplicacao.close();}
            },
            {
                baseCls: '',
                cls: 'btn btn-inverse',
                text: 'SIM',
                handler: function(){that.showWindowAplicacao(record); winAvisoAplicacao.close();}
            }
            ]
        });
        
        winAvisoAplicacao.show(); 
    },
    
    showWinDetalhesFundo: function(carteira){
        var controllerLink = CRMax.globals.baseDirectory + 'controllers/documento/download.ashx?path=';
        var linkLamina = controllerLink + encodeURIComponent(carteira.LinkLamina);
        var linkProspecto = controllerLink + encodeURIComponent(carteira.LinkProspecto);
        var linkTermoAdesao = controllerLink + encodeURIComponent(carteira.LinkTermoAdesao);
        var linkRegulamento = controllerLink + encodeURIComponent(carteira.LinkRegulamento);
        
        var html = '<div class="info-aplic-fundo">';
        html += '<h1>Fundo: ' + carteira.Nome + '</h1>';
        html += '<a target="_blank" href="' + linkLamina + '">Lâmina</a>';
        html += '<a target="_blank" href="' + linkTermoAdesao + '">Termo de Adesão</a>';
        html += '<a target="_blank" href="' + linkProspecto + '">Prospecto</a>';
        html += '<a target="_blank" href="' + linkRegulamento + '">Regulamento</a>';
        html += '</div>'

        var htmlComponent = {
            xtype: 'component',
            //layout: 'fit',
            html: html,
            height: 100
        };
        
        var storeRentabilidade = Ext.create('Ext.data.Store', {
            //storeId:'Rentabilidades',
            fields:['RetornoMesCorrente', 'RetornoUltimoMes', 'RetornoAno', 'Retorno12Meses'],
            data:{'items':[
                { 
                    'RetornoMesCorrente': carteira.RetornoMesCorrente,  
                    'RetornoUltimoMes': carteira.RetornoUltimoMes,
                    'RetornoAno': carteira.RetornoAno,
                    'Retorno12Meses': carteira.Retorno12Meses
                }
            ]},
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'items'
                }
            }
        });

        var gridRentabilidade = Ext.create('Ext.grid.Panel', {
            title: 'Rentabilidade',
            store: storeRentabilidade,
            //layout: 'fit',
            columns: [
                {dataIndex: 'RetornoMesCorrente', width: 85, header: 'No Mês', headerAlign: 'right', xtype: 'numbercolumn', format:'0,000.00%', align: 'right'},
                {dataIndex: 'RetornoUltimoMes', width: 125,header: 'Mês Anterior', xtype: 'numbercolumn', format:'0,000.00%', align: 'right'},
                {dataIndex: 'RetornoAno', width: 125,header: 'No Ano', xtype: 'numbercolumn', format:'0,000.00%', align: 'right'},
                {dataIndex: 'Retorno12Meses', width: 125,header: '12 Meses', xtype: 'numbercolumn', format:'0,000.00%', align: 'right'},
                {dataIndex: 'Spacer', width: 25,header: ''}
            ],
            height: 250,
            width: 500,
            enableColumnMove: false,
            enableColumnResize: false,
            sortableColumns: false,
            enableColumnHide: false
        });
               
        var winDetalhesFundo = Ext.create('Ext.window.Window', {
            title: 'Detalhes do Fundo de Investimento',
            height: 300,
            width: 500,
            cls: 'win-detalhes-fundo',
            y: CRMax.application.inIframe() ? 50 :undefined,
            modal: true,
            //layout: 'fit',
            items: [htmlComponent, gridRentabilidade],
            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'Fechar',
                handler: function(){winDetalhesFundo.close()}
            }]
        });
        
        winDetalhesFundo.show();  
    },
    
    onClickButtonDetalhes: function(grid, rowIndex, colIndex, item, e, recordCarteira, row){
        
        Ext.Ajax.request({
            scope: this,
            url: CRMax.globals.baseDirectory + 'controllers/fundocotista/details.ashx?idcarteira=' + recordCarteira.get('IdCarteira'),
            success: function(response) {
                var responseObject = Ext.JSON.decode(response.responseText);
                this.showWinDetalhesFundo(responseObject.carteira);
            }
        });
        
        
    },
    
    showWindowAplicacao: function(record){
        var idCarteira = record.get('IdCarteira');
        var window = Ext.create('CRMax.view.operacao.Edit', {
            tipoOperacao: 1,
            y: CRMax.application.inIframe() ? 50 :undefined,
            idCarteira: idCarteira
        });
        window.show();
    },
    
    showWindowSemPermissaoAplicarFundo: function(recordCarteira){
        Ext.Ajax.request({
            scope: this,
            url: CRMax.globals.baseDirectory + 'controllers/fundocotista/details.ashx?idcarteira=' + recordCarteira.get('IdCarteira'),
            success: function(response) {
                var responseObject = Ext.JSON.decode(response.responseText);
                
                recordCarteira = responseObject.carteira;
                
                var controllerLink = CRMax.globals.baseDirectory + 'controllers/documento/download.ashx?path=';
        var linkLamina = controllerLink + encodeURIComponent(recordCarteira.LinkLamina);
        var linkProspecto = controllerLink + encodeURIComponent(recordCarteira.LinkProspecto);
        var linkTermoAdesao = controllerLink + encodeURIComponent(recordCarteira.LinkTermoAdesao);
        var linkRegulamento = controllerLink + encodeURIComponent(recordCarteira.LinkRegulamento);
        
        var html = '<div class="info-aplic-fundo">';
        html += '<h1>Fundo: ' + recordCarteira.Nome + '</h1>';
        html += '<a target="_blank" href="' + linkLamina + '">Lâmina</a>';
        html += '<a target="_blank" href="' + linkTermoAdesao + '">Termo de Adesão</a>';
        html += '<a target="_blank" href="' + linkProspecto + '">Prospecto</a>';
        html += '<a target="_blank" href="' + linkRegulamento + '">Regulamento</a>';
        html += '<div class="box-intrucoes-aplicacao">';
        html += '<p>Esta é a sua primeira aplicação neste fundo. Por favor, leia a Lâmina, Prospecto, Regulamento e o Termo de Adesão do Fundo.</p>';
        html += '<p>Favor imprimir, preencher e assinar o Termo de Adesão e enviar para a Porto Seguro Investimentos.</p>';
        html += '<p>Endereço para envio do Termo de Adesão:<br /> ' + CRMax.globals.enderecoPorto + '</p>';
         html += '<p>Dúvidas entre em contato com a Central de Atendimento: (11) 3366-3370 (dias úteis das 8h15 às 17h30).</p>';
         html += '</div>';
         html += '</div>';
        
        var that = this;
        
        var winSemPermissaoAplicarFundo = Ext.create('Ext.window.Window', {
            title: 'Nova Aplicação em Fundo de Investimento',
            height: 400,
            width: 800,
            y: CRMax.application.inIframe() ? 50 :undefined,
            modal: true,
            layout: 'fit',
            border: false,
            //items: component
            html: html,
            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'Fechar',
                handler: function(){winSemPermissaoAplicarFundo.close()}
            }/*,{
                baseCls: '',
                cls: 'btn btn-inverse',
                text: 'Continuar',
                handler: function(){that.showWindowAvisoAplicacao(recordCarteira);winSemPermissaoAplicarFundo.close();}
            }*/]
        });
        
        winSemPermissaoAplicarFundo.show();  
                
            }
        });
    },

    loadStore: function () {
        this.store.load();
    }
});