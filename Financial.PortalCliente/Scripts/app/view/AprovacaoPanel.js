﻿Ext.define('CRMax.view.AprovacaoPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.aprovacaopanel',
    border: false,
    layout: 'border',
    cls: 'home-panel',
    initComponent: function () {
        
       var aprovacaoFilter = new CRMax.view.ordem.Filter({listId: 'aprovacao-list'}); 
       var rightColumn = {
            width: 360,
            bodyStyle: 'background-color: #fafafa;',
            region: 'east',
            border: false,
            autoScroll: true,
            items: [aprovacaoFilter]
       };


        var aprovacaoList = new CRMax.view.ordem.List({id: 'aprovacao-list', multiCotista: true, actionStatus: 'aprovacao'});
        
        var leftColumn = {
            columnWidth: 1,
            title: 'Aprovação',
            //bodyStyle: 'padding: 20px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: [aprovacaoList]
        };
        
        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
    }

});