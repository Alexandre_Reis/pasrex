﻿Ext.define('CRMax.view.SegurancaPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.segurancapanel',
    border: false,
    layout: 'border',
    cls: 'home-panel',
    initComponent: function() {
        this.gridIframe = new Ext.Component({
            autoEl: {
                tag: 'iframe',
                width: '100%',
                height: 300,
                style: 'border: 0',
                src: CRMax.globals.pathHistoricoLog,
                scrolling: 'no'
            }
        });

        this.tbar = [{
            text: 'Email BackOffice',
            baseCls: '',
            cls: 'btn-aplicacao',
            handler: function() {
                Ext.create('Ext.window.Window', {
                    title: 'Email Back-office',
                    height: 200,
                    width: 400,
                    layout: 'fit',
                    items: { // Let's put an empty grid in just to illustrate fit layout
                        xtype: 'form',
                        bodyStyle: 'padding: 10px',
                        border: false,
                        items: [{
                            xtype: 'textarea',
                            width: 370,
                            name: 'EmailBackOffice',
                            height: 60,
                            fieldLabel: 'Email Back-office',
                            value: CRMax.globals.emailBackOffice
                        }],
                        buttons: [{
                            baseCls: '',
                            cls: 'btn',
                            text: 'Cancelar',
                            scope: this,
                            handler: function(btn) {
                                btn.up('window').close();
                            }
                        }, {
                            text: 'Gravar',
                            baseCls: '',
                            scope: this,
                            cls: 'btn btn-inverse',
                            handler: function(btn) {
                                var emailBackOffice = btn.up('form').getValues().EmailBackOffice;
                                
                                var params = {};
                                
                                if(CRMax.globals.isTesouraria){
                                    params.emailBackOfficeInstitucional = emailBackOffice;
                                    params.isTesouraria = true;
                                }else{
                                    params.emailBackOffice = emailBackOffice;
                                    params.isTesouraria = false;
                                }
                                
                                Ext.Ajax.request({
                                    url: CRMax.globals.baseDirectory + 'controllers/seguranca/save.ashx',
                                    params: params,
                                    success: function(response) {
                                        var msgSucesso = 'Atualização realizada com sucesso';
                                        Ext.Msg.alert('Aviso',msgSucesso);    
                                        
//                                        CRMax.application.notificationWindow.update(msgSucesso);
//                                        CRMax.application.notificationWindow.show();
                                        btn.up('window').close();
                                        CRMax.globals.emailBackOffice = emailBackOffice;
                                    }
                                });
                            }
                        }]
                    }
                }).show();
            }
        },
        
        {
            text: 'Disclaimer',
            baseCls: '',
            cls: 'btn-aplicacao',
            handler: function() {
                Ext.create('Ext.window.Window', {
                    title: 'Disclaimer',
                    height: 400,
                    width: 500,
                    layout: 'fit',
                    items: { // Let's put an empty grid in just to illustrate fit layout
                        xtype: 'form',
                        bodyStyle: 'padding: 10px',
                        border: false,
                        items: [{
                            xtype: 'textarea',
                            width: 470,
                            name: 'DisclaimerPosicao',
                            height: 100,
                            fieldLabel: 'Texto Saldo',
                            value: CRMax.globals.disclaimerPosicao
                        },{
                            xtype: 'textarea',
                            width: 470,
                            name: 'DisclaimerOperacao',
                            height: 100,
                            fieldLabel: 'Texto Operações',
                            value: CRMax.globals.disclaimerOperacao
                        }],
                        buttons: [{
                            baseCls: '',
                            cls: 'btn',
                            text: 'Cancelar',
                            scope: this,
                            handler: function(btn) {
                                btn.up('window').close();
                            }
                        }, {
                            text: 'Gravar',
                            baseCls: '',
                            scope: this,
                            cls: 'btn btn-inverse',
                            handler: function(btn) {
                                var formValues = btn.up('form').getValues();
                                var disclaimerPosicao = formValues.DisclaimerPosicao;
                                var disclaimerOperacao =  formValues.DisclaimerOperacao;
                                
                                var params = {
                                    operacao: 'update-disclaimer',
                                    disclaimerPosicao: disclaimerPosicao,
                                    disclaimerOperacao: disclaimerOperacao
                                };
                                
                                Ext.Ajax.request({
                                    url: CRMax.globals.baseDirectory + 'controllers/seguranca/save.ashx',
                                    params: params,
                                    success: function(response) {
                                        var msgSucesso = 'Atualização realizada com sucesso';
                                        Ext.Msg.alert('Aviso',msgSucesso);    
                                        
                                        btn.up('window').close();
                                        CRMax.globals.disclaimerPosicao = disclaimerPosicao;
                                        CRMax.globals.disclaimerOperacao = disclaimerOperacao;
                                    }
                                });
                            }
                        }]
                    }
                }).show();
            }
        }
        ];

        this.items = this.gridIframe;

        this.callParent(arguments);
    }

});