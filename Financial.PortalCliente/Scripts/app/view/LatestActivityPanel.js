﻿Ext.define('CRMax.view.LatestActivityPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.latestactivitypanel',
    border: false,
    layout: 'border',
    cls: 'latest-activity-panel',
    initComponent: function () {

        var rightColumn = {
            title: 'Operações',
            width: 560,
            bodyStyle: 'padding: 20px 10px; background-color: #fafafa;',
            region: 'east',
            border: false,
            autoScroll: true,
            items: new CRMax.view.operacao.List()
        };

        var leftColumn = {
            columnWidth: 1,
            title: 'Extrato',
            bodyStyle: 'padding: 20px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: new CRMax.view.posicao.List()
        };


        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
    }
});