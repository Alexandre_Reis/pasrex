﻿Ext.define('CRMax.view.MainContentPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.maincontentpanel',
    layout: 'card',

    addComponent: function (componentConfig, removeBeforeAdding) {

        var cards = this.getLayout().getLayoutItems();

        //try to find component
        var component = null;
        for (var cardCount = 0; cardCount < cards.length; cardCount++) {
            if (cards[cardCount].itemId == componentConfig.itemId) {
                component = cards[cardCount];

                if (removeBeforeAdding === true) {
                    component.destroy();
                    component = null;
                }
                break;
            }
        }

        if (!component) {
            component = this.add(componentConfig);
        }

        this.getLayout().setActiveItem(componentConfig.itemId);
        return component;
    },

    getActivePanel: function () {
        return this.getLayout().getActiveItem();
    }

});