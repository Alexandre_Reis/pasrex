﻿Ext.define('CRMax.view.cotista.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.cotistalist',

    store: 'Cotistas',

    border: false,

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function() {

        this.columns = [{
            header: 'Código',
            dataIndex: 'IdCotista',
            width: 100
        }, {
            header: 'Nome',
            dataIndex: 'Nome',
            flex: 1
        }, {
            header: 'CPF/CNPJ',
            dataIndex: 'Cpfcnpj',
            flex: 1
        }];


        var filterTask = new Ext.util.DelayedTask();

        var filterChange = function(field, newValue) {
            filterTask.delay(500, filter, this, [field, newValue]);
        };

        var filter = function(field, newValue) {
            //Handle clear button plugin... we want to refresh store if field was cleared
            this.store.clearFilter(true);
            if (newValue && newValue.length > 0) {
                this.store.filter([{
                    property: "searchQuery",
                    value: newValue
                }]);
            } else {
                this.store.clearFilter();
            }

        };

        this.tbar = [{
            xtype: 'image',
            style: 'margin: 0 4px 0 6px;',
            src: 'content/themes/base/images/search.png',
            width: 16,
            height: 16
        }, {
            xtype: 'textfield',
            width: 300,
            listeners: {
                scope: this,
                change: filterChange
            }
            /*,
                                select: function () {
                                    this.buildFilter();
                                },
                                blur: function () {
                                    this.buildFilter();
                                },
                                specialkey: function (field, e) {
                                    if (e.getKey() == e.ENTER || e.getKey() == e.ESC || e.getKey() == e.TAB) {
                                        this.buildFilter();
                                    }
                                }*/
        }];

        this.dockedItems = [{
            xtype: 'pagingtoolbar',
            store: 'Cotistas',
            dock: 'bottom'
        }];

        this.callParent(arguments);

        this.on('select', this.onRowSelect, this);
    },
    afterRender: function() {
        var me = this;
        me.callParent(arguments);
        me.down('#refresh').hide();
    },

    onRowSelect: function(grid, record, index, options) {
        var idCotista = record.get('IdCotista');

        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'controllers/cotista/select.ashx',
            params: {
                idCotista: idCotista
            },
            success: function(response) {
                window.location.reload();
            }
        });

    },

    loadStore: function() {
        this.store.load();
    }
});