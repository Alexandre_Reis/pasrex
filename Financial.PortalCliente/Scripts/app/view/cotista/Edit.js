﻿Ext.define('CRMax.view.cotista.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.cotistaedit',
    title: ' ',
    autoScroll: true,

    modal: true,
    shadow: false,
    resizable: false,
    border: false,
    draggable: false,
    closable: false,
    width: 500,
    layout: 'fit',
    height: 400,

    initComponent: function () {
        
        this.grid = Ext.create('CRMax.view.cotista.List', {title: 'Selecionar um Cotista na lista abaixo:'});
        
        this.items = [this.grid];
        
        /*this.tools = [{
            type: 'close',
            width: 200,
            tooltip: 'Sair',
            // hidden:true,
            handler: function(event, toolEl, panelHeader) {
                // refresh logic
            }
        }];*/
        
        this.header = {
            xtype: 'header',
            titlePosition: 0,
            defaults: {
                margin: '0 10px'
            },
            items: [
                {
                    xtype: 'component',
                    html: '<div style="color: #fff; font-size: 14px;">Cotistas</div>',
                    width: 400
                },
               
                {
                    xtype: 'button',
                    id: 'button-logoff-2',
                    iconCls: 'icon-logoff',
                    text: 'Sair'
                }
            ]
        }
        
        
        
        if( (CRMax.globals.userInfo.MultiCotista === true && CRMax.globals.isTesouraria !== true)||
            ((CRMax.globals.userInfo.Permissoes.PodeDistribuir === true || 
            CRMax.globals.userInfo.Permissoes.PodeAprovar === true) && CRMax.globals.isTesouraria === true)){
            
            var buttonLabel = CRMax.globals.isTesouraria === true ? 'Editar Ordens' : 'Todos';
            
            this.buttons = [{
                baseCls: '',
                cls: 'btn btn-inverse',
                text: buttonLabel,
                scope: this,
                handler: this.irParaAprovacao
            }]
         }

        this.callParent(arguments);
    },
    
    irParaAprovacao: function(){
        var idCotista = -1;
        
        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'controllers/cotista/select.ashx',
            params: {
                idCotista: idCotista
            },
            success: function(response){
                window.location.reload();
            }
        });
    }
});