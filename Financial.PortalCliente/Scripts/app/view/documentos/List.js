﻿Ext.define('CRMax.view.Documentos', {
    extend: 'Ext.Panel',
    alias: 'widget.documentoslist',
    border: false,
    title: 'Documentos',
    //cls: 'latest-activity-panel',
    initComponent: function () {
        
       /* var buttonEnviarDocumento = {
            xtype: 'button',
            text: 'Enviar documento',
            scope: this,
            handler: this.showUploadWindow
        };
        
        this.tbar = [
            buttonEnviarDocumento
        ];*/
        
        var documentoTpl = new Ext.XTemplate(
        '<tpl for=".">',
        
        '<div class="MediaItemContainer" index="0">',
            '<div class="MediaItem File" style="position: relative;">',
                '<div class="ThumbContainer" title="Download" style="height: 160px; width: 120px; background-color: rgb(210, 71, 38);">',
                    '<img class="Thumb" src="content/themes/base/images/xml_57.png" alt="Download" title="Download" style="padding-top: 52px;"> ',
                '</div>',
                '<div class="FileName" title="Download">',
                    '<span class="TextSizeSmall">{Nome?}</span>',
                '</div>',
                '<div class="Overlay" title="Download">',
                    '<span class="FloatRight TextSizeSmall">Download</span>',
                    '<a href="#" class="FloatRight">',
                        '<img src="content/themes/base/images/liveview_download.png">',
                    '</a>',
                '</div>',
            '</div>',
        '</div>',
            
        '</tpl>'
        );
        
        var dataview = Ext.create('Ext.view.View', {
            store: 'Documentos',
            tpl: documentoTpl,
            itemSelector: 'div.MediaItemContainer',
            emptyText: 'Nenhum documento encontrado',
            listeners: {
                itemclick: this.onItemClick,
                scope: this
            }
        });
        
        this.items = [dataview];
        
        this.callParent(arguments);
    },
    
    onItemClick: function(dataview, record, item){
        var path = encodeURIComponent(record.get('Path'));
        window.open(CRMax.globals.baseDirectory + 'controllers/documento/download.ashx?path=' + path);
    },
    showUploadWindow: function(){
        this.uploadWindow = Ext.create('Ext.window.Window', {
            title: 'Enviar documento',
            height: 200,
            width: 400,
            layout: 'fit',
            border: false,
            url: CRMax.globals.baseDirectory + 'controllers/documento/upload.ashx',
            items:{
                xtype: 'form',
                closable: true,
                header: false,
                border: false,
                bodyPadding: 10,
            
                items: {  // Let's put an empty grid in just to illustrate fit layout
                    xtype: 'filefield',
                    name: 'file',
                    fieldLabel: 'Arquivo',
                    labelWidth: 50,
                    msgTarget: 'side',
                    allowBlank: false,
                    anchor: '100%',
                    buttonText: 'Selecione o arquivo...'
                }
            },
            buttons: [
            {text: 'Enviar', scope: this, handler: this.submitUpload}, 
            
            {text: 'Cancelar', scope: this, handler: function(){this.uploadWindow.close();}}]
        });
        this.uploadWindow.show();
    },
    
   submitUpload: function(){
        var form = this.uploadWindow.down('form');
        var basicForm = form.getForm();
        
        basicForm.submit({
            url: CRMax.globals.baseDirectory + 'Controllers/Documento/Upload.ashx',
            waitMsg: 'Processando...',
            waitTitle: 'Aguarde',
            scope: this,
            success: function (fp, o) {
                
                Ext.Msg.alert('Aviso','Arquivo enviado com sucesso!'); 
                
                //CRMax.application.notificationWindow.update('Arquivo enviado com sucesso!');
                //CRMax.application.notificationWindow.show();
                this.uploadWindow.close();
            },
            failure: function(response){
                if (!(response && response.responseText && (response.responseText.indexOf('Convert.ToInt32((Convert.ToString(context.Session') > 0))) {
                    Ext.Msg.alert('Aviso','Ocorreu um erro durante a gravação. Por favor, tente novamente.');    
                }
            }
        });
   } 
});