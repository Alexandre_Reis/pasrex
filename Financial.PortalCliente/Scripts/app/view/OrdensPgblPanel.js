﻿Ext.define('CRMax.view.OrdensPgblPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.ordenspgblpanel',
    border: false,
    layout: 'border',
    cls: 'home-panel',
    initComponent: function () {
        
       var ordemFilter = new CRMax.view.ordem.pgbl.Filter({listId: 'ordem-pgbl-list'}); 
       var rightColumn = {
            width: 360,
            bodyStyle: 'background-color: #fafafa;',
            region: 'east',
            border: false,
            autoScroll: true,
            items: [ordemFilter]
       };


        var ordemList = new CRMax.view.ordem.pgbl.List({id: 'ordem-pgbl-list'});
        
        var leftColumn = {
            columnWidth: 1,
            title: 'Ordens',
            //bodyStyle: 'padding: 20px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: [ordemList]
        };
        
        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
    }

});