﻿Ext.define('CRMax.view.Viewport', {
    extend: 'Ext.container.Viewport',
    layout: 'border',
    requires: ['CRMax.view.MainNavigator', 'CRMax.view.MainContentPanel', 'Ext.ux.form.SearchField', 'Ext.ux.form.HoverButton'],
    mainContentPanelItemId: 'main-content-panel',
    mainNavigatorPanelItemId: 'MainNavigator',

    getMainNavigatorPanel: function () {
        return this.mainNavigatorPanel || (this.mainNavigatorPanel = this.getComponent(this.mainNavigatorPanelItemId));

    },

    getMainContentPanel: function () {
        return this.mainContentPanel || (this.mainContentPanel = this.getComponent(this.mainContentPanelItemId));

    },

    initComponent: function () {
    
        var navigationBar = {
            border: false,
            itemId: this.mainNavigatorPanelItemId,
            bodyCls: 'mainnavigator-panel-body',
            //region: 'west',
            xtype: 'mainnavigator'
        };
        
        var topBar = {
            region: 'north',
            cls: 'viewport-north-panel-body',
            xtype: 'panel',
            bodyStyle: 'background-color: transparent; margin-top: 15px;',
            height: 65,
            border: false,
            items: [
                {
                    xtype: 'component',
                    id: 'logo-cliente',
                    style: 'float: left; margin-right: 30px;',
                    html: '<img style="padding-top: 2px;" src="' + CRMax.globals.baseDirectory + 'content/themes/base/images/logo_cliente.gif" />'
                },
                navigationBar, 
                {
                    xtype: 'button',
                    id: 'button-logoff',
                    hidden: true,
                    iconCls: 'icon-logoff',
                    text: 'Sair'
                }, {
                    xtype: 'button',
                    iconCls: 'icon-user',
                    id: 'button-edit-user',
                    text: CRMax.globals.userInfo.Login
        }]};
        
        
    
        this.items = [
            topBar
            /*{
            region: 'north',
            height: 60,
            border: false,
            bodyCls: 'viewport-north-panel-body',
            items: [
                topBar
            ]
        }*/, {
            xtype: 'maincontentpanel',
            itemId: this.mainContentPanelItemId,
            region: 'center',
            border: false
        }
        ];

        this.callParent();
    }
});