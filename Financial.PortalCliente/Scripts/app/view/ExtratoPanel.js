﻿Ext.define('CRMax.view.ExtratoPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.extratopanel',
    border: false,
    layout: 'border',
    cls: 'home-panel',
    initComponent: function () {
        
       var extratoFilter = new CRMax.view.extrato.Filter({listId: 'extrato-list'}); 
       var rightColumn = {
            //title: 'Pesquisar Operações',
            width: 200,
            bodyStyle: 'background-color: #fafafa;',
            region: 'east',
            border: false,
            autoScroll: true,
            items: [extratoFilter]
       };

        var extratoList = new CRMax.view.extrato.List({id: 'extrato-list'});
        
        var leftColumn = {
            columnWidth: 1,
            title: 'Extrato',
            //bodyStyle: 'padding: 20px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: [extratoList]
        };
        
        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
    }

});