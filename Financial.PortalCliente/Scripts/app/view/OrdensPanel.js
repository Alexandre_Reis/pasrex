﻿Ext.define('CRMax.view.OrdensPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.ordenspanel',
    border: false,
    layout: 'border',
    cls: 'home-panel',
    initComponent: function () {
        
       var ordemFilter = new CRMax.view.ordem.Filter({listId: 'ordem-list'}); 
       var rightColumn = {
            width: 360,
            bodyStyle: 'background-color: #fafafa;',
            region: 'east',
            border: false,
            autoScroll: true,
            items: [ordemFilter]
       };


        var ordemList = new CRMax.view.ordem.List({id: 'ordem-list'});
        
        var leftColumn = {
            columnWidth: 1,
            title: 'Ordens',
            //bodyStyle: 'padding: 20px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: [ordemList]
        };
        
        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
    }

});