﻿Ext.define('CRMax.view.posicao.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.posicaolist',
    hideHeaders: false,
    store: 'Posicoes',

    border: false,

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function() {

        if (CRMax.globals.isTesouraria && CRMax.globals.userInfo.Permissoes.PodeBoletar) {
            this.tbar = [{
                text: 'Aplicar',
                handler: this.onClickButtonAplicarOrdem,
                baseCls: '',
                cls: 'btn-aplicacao'
            }, {
                text: 'Resgatar',
                handler: this.onClickButtonResgatarOrdem,
                baseCls: '',
                cls: 'btn-resgate'
            }];
        }

        this.columns = [{
            dataIndex: 'NomeCarteira',
            flex: 1,
            header: 'Fundo'
        }, {
            dataIndex: 'Quantidade',
            header: 'Qtde. Cotas',
            width: 120,
            xtype: 'numbercolumn',
            format: '0,000.000000',
            align: 'right'
        }, {
            dataIndex: 'CotaDia',
            header: 'Valor Cota (R$)',
            width: 100,
            xtype: 'numbercolumn',
            format: '0,000.000000',
            align: 'right'
        }, {
            dataIndex: 'ValorLiquido',
            header: 'Saldo Líquido (R$)',
            width: 120,
            xtype: 'numbercolumn',
            format: '0,000.00',
            align: 'right'
        }, {
            dataIndex: 'ValorLiquidoResgate',
            header: 'Saldo Líquido p/ Resgate (R$)',
            width: 170,
            xtype: 'numbercolumn',
            format: '0,000.00',
            align: 'right'
        }, {
            xtype: 'actioncolumn',
            width: 50,
            hidden: CRMax.globals.isTesouraria,
            renderer: function(v, meta) {
                return '<div class="x-action-col-0 x-action-col x-action-col-aplicar">aplicar</div>'
            },
            tooltip: 'aplicar',
            items: [{
                    scope: this,
                    handler: this.onClickButtonAplicar
                }]
        }, {
            xtype: 'actioncolumn',
            width: 60,
            hidden: CRMax.globals.isTesouraria,
            renderer: function(v, meta) {
                return '<div class="x-action-col-0 x-action-col x-action-col-resgatar">resgatar</div>'
            },
            items: [{
                scope: this,
                handler: this.onClickButtonResgatar
            }]
        }, {
            xtype: 'actioncolumn',
            width: 55,
            renderer: function(v, meta) {
                return '<div class="x-action-col-0 x-action-col x-action-col-info">posição</div>'
            },
            items: [{
                handler: this.onClickButtonDetalhe
            }]
        }];

        this.callParent(arguments);

        CRMax.application.on('operacaoupdated', this.loadStore, this);
    },

    onClickButtonAplicar: function(grid, rowIndex, colIndex, item, e, record, row) {

        if (CRMax.globals.userInfo.CotistaPodeOperar !== true) {
            Ext.Msg.alert('Aviso', 'Não é possível realizar aplicações. Motivo: ' + CRMax.globals.userInfo.PendenciaCadastral);
            return false;
        }

        var dispensado = CRMax.globals.perfilCotista.Dispensado == 'Sim';
        var recusou = CRMax.globals.perfilCotista.Status == 'Recusado';
        var temPerfil = CRMax.globals.perfilCotista.Perfil !== null && CRMax.globals.perfilCotista.Perfil !== "" && CRMax.globals.perfilCotista.Perfil != 'Indefinido.';

        var gridPanel = grid.up('gridpanel');
        if (dispensado || recusou) {
            gridPanel.showWindowAvisoAplicacao(record);
        } else {
            //Se cliente se recusou a preencher o perfil ou nao tem perfil preenchido, nao continuar
            if (!temPerfil) {
                Ext.Msg.alert('Aviso', CRMax.globals.msgsSuitability.msg_11);
                return false;
            }

            //Checar data de validade do perfil
            if (!CRMax.globals.perfilCotista.DentroValidade) {
                //Exibir mensagem de perfil desatualizado
                Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_12, function(btn) {
                    if (btn === 'yes') {
                        this.logMensagem(12, CRMax.globals.msgsSuitability.msg_12, 'OK');

                        //Exibir mensagem do termo de inadequacao
                        Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_19, function(btn) {
                            if (btn === 'yes') {
                                this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'OK');
                                gridPanel.showWindowAvisoAplicacao(record);
                            } else {
                                this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'Cancelar');
                                return false;
                            }
                        }, this);
                    } else {
                        this.logMensagem(12, CRMax.globals.msgsSuitability.msg_12, 'Cancelar');
                        return false;
                    }
                }, this);
            }else{
                gridPanel.showWindowAvisoAplicacao(record);
            }
        }
    },

    logMensagem: function(idMensagem, mensagem, resposta) {
        Ext.Ajax.request({
            scope: this,
            url: CRMax.globals.baseDirectory + 'controllers/suitability/logmensagem.ashx?idmensagem=' + idMensagem + '&mensagem=' + mensagem + '&resposta=' + resposta,
            success: function(response) {

            }
        });
    },

    showWindowAvisoAplicacao: function(record) {
        var html = '<div class="info-aplic-fundo">';
        html += '<div class="box-intrucoes-aplicacao">';
        html += '<p>Para efetivação da aplicação é necessário providenciar a transferência do montante através de TED/DOC/TEF de acordo com seus dados previamente informados em seu cadastro.</p>';
        html += '<p>O horário limite para movimentações é 15h. Solicitações recebidas após este horário serão processadas automaticamente no dia útil subsequente.</p>';
        html += '<p>Dúvidas entre em contato com a Central de Atendimento: (11) 3366-3370 (dias úteis das 8h15 às 17h30).</p>';
        html += '<br /><p>Você já realizou a transferência?</p>';
        html += '</div>';
        html += '</div>';
        var that = this;
        var winAvisoAplicacao = Ext.create('Ext.window.Window', {
            title: 'Aplicação em Fundo de Investimento',
            height: 400,
            width: 800,
            y: CRMax.application.inIframe() ? 50 : undefined,
            modal: true,
            layout: 'fit',
            border: false,
            //items: component
            html: html,
            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'NÃO',
                handler: function() {
                    winAvisoAplicacao.close();
                }
            }, {
                baseCls: '',
                cls: 'btn btn-inverse',
                text: 'SIM',
                handler: function() {
                    that.showWindowAplicacao(record);
                    winAvisoAplicacao.close();
                }
            }]
        });

        winAvisoAplicacao.show();
    },

    showWindowAplicacao: function(record) {
        var idCarteira = record.get('IdCarteira');
        var window = Ext.create('CRMax.view.operacao.Edit', {
            tipoOperacao: 1,
            y: CRMax.application.inIframe() ? 50 : undefined,
            idCarteira: idCarteira
        });
        window.show();
    },

    onClickButtonAplicarOrdem: function() {
        if (CRMax.globals.userInfo.CotistaPodeOperar !== true) {
            Ext.Msg.alert('Aviso', 'Não é possível realizar aplicações. Motivo: ' + CRMax.globals.userInfo.PendenciaCadastral);
            return false;
        }

        var window = Ext.create('CRMax.view.ordem.Edit', {
            tipoOperacao: 1,
            y: CRMax.application.inIframe() ? 50 : undefined
        });
        window.show();
    },

    onClickButtonResgatarOrdem: function() {

        var window = Ext.create('CRMax.view.ordem.Edit', {
            tipoOperacao: 2,
            y: CRMax.application.inIframe() ? 50 : undefined
        });
        window.show();
    },

    onClickButtonResgatar: function(grid, rowIndex, colIndex, item, e, record, row) {

        if (CRMax.globals.userInfo.CotistaPodeOperar !== true) {
            Ext.Msg.alert('Aviso', 'Não é possível realizar aplicações. Motivo: ' + CRMax.globals.userInfo.PendenciaCadastral);
            return false;
        }

        var dispensado = CRMax.globals.perfilCotista.Dispensado == 'Sim';
        var recusou = CRMax.globals.perfilCotista.Status == 'Recusado';
        var temPerfil = CRMax.globals.perfilCotista.Perfil !== null && CRMax.globals.perfilCotista.Perfil !== "" && CRMax.globals.perfilCotista.Perfil != 'Indefinido.';    

        var gridPanel = grid.up('gridpanel');
        if (!dispensado) {
            //Se cliente se recusou a preencher o perfil ou nao tem perfil preenchido, nao continuar
            if (recusou || !temPerfil) {
                Ext.Msg.alert('Aviso', CRMax.globals.msgsSuitability.msg_11);
                return false;
            }

            //Checar data de validade do perfil
            if (!CRMax.globals.perfilCotista.DentroValidade) {
                //Exibir mensagem de perfil desatualizado
                Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_12, function(btn) {
                    if (btn === 'yes') {
                        this.logMensagem(12, CRMax.globals.msgsSuitability.msg_12, 'OK');

                        //Exibir mensagem do termo de inadequacao
                        Ext.MessageBox.confirm('', CRMax.globals.msgsSuitability.msg_19, function(btn) {
                            if (btn === 'yes') {
                                this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'OK');
                                gridPanel.showWindowResgate(record);
                            } else {
                                this.logMensagem(19, CRMax.globals.msgsSuitability.msg_19, 'Cancelar');
                                return false;
                            }
                        }, this);
                    } else {
                        this.logMensagem(12, CRMax.globals.msgsSuitability.msg_12, 'Cancelar');
                        return false;
                    }
                }, this);


            } else {
                gridPanel.showWindowResgate(record);
            }
        }
    },

    showWindowResgate: function(record) {
        var idCarteira = record.get('IdCarteira');
        var window = Ext.create('CRMax.view.operacao.Edit', {
            tipoOperacao: 2,
            y: CRMax.application.inIframe() ? 50 : undefined,
            idCarteira: idCarteira
        });
        window.show();
    },

    onClickButtonDetalhe: function(grid, rowIndex, colIndex, item, e, record, row) {
        CRMax.application.fireEvent('detalheclick', record);
    },

    loadStore: function() {
        this.store.load();
    }
});