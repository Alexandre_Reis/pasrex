﻿Ext.define('CRMax.view.posicao.detalhe.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.posicaodetalhelist',
    hideHeaders: false,
    store: 'PosicoesDetalhadas',

    border: false,

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function () {
        
        this.columns = [
            {dataIndex: 'DataAplicacao', width: 80, header: 'Data Aplicação', type: 'date', xtype: 'datecolumn',format: 'd/m/Y' },
            {dataIndex: 'Quantidade', width: 150, header: 'Qtde. Cotas Atual', xtype: 'numbercolumn', format:'0,000.000000', align: 'right'},
            {dataIndex: 'CotaDia', width: 110, header: 'Valor Cota Atual (R$)', xtype: 'numbercolumn', format:'0,000.000000', align: 'right'},
            {dataIndex: 'ValorBruto', header: 'Saldo Bruto (R$)', width: 110, xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'ValorIR', header: 'IR (R$)', width: 90, xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'ValorIOF', header: 'IOF (R$)', width: 80, xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'ValorLiquido', header: 'Saldo Líquido (R$)', width: 100, xtype: 'numbercolumn', format:'0,000.00', align: 'right'}
        ];
        
        this.callParent(arguments);
    },

    loadStore: function () {
        this.store.load();
    },
    
    filter: function(filters){
        this.store.clearFilter(true);
        this.store.filter(filters);
    }
});