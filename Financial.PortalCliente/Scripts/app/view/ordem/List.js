﻿Ext.define('CRMax.view.ordem.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ordemlist',
    hideHeaders: false,
    border: false,

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function () {
    
           this.store = Ext.create(CRMax.store.OrdensTesouraria);
    
           this.tbar = [
                {
                   text: 'Imprimir',
                   //iconCls: 'icon-print',
                   scope: this,
                   baseCls: '',
                   cls: 'btn-aplicacao',
                   handler: this.onButtonPrintWYSIWYGClick
                }
            ];
        
        this.columns = [
                {dataIndex: 'DataOperacao', width: 80, header: 'Data', type: 'date', xtype: 'datecolumn',format: 'd/m/Y' }
            ];
        
        if(this.multiCotista === true){
            this.columns.push(
                {dataIndex: 'NomeCotista', flex: 1, header: 'Cotista'}
            );
        }
            
        this.columns.push(
            {dataIndex: 'TipoOperacaoString', width: 80, header: 'Tipo'},
            {dataIndex: 'Valor', width: 90, header: 'Valor (R$)', xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'StatusProcessamento', width: 100, header: 'Status',
                renderer: function(v, meta, record){
                    var observacao = record.get('Observacao');
                    if(observacao && observacao.length > 0){
                        v += ' <span class="cancel-info" data-qtip="' + observacao + '"> </span>';
                    }
                    return v;
                }
            },
            { header: 'Tipo Liquidação',  dataIndex: 'FormaLiquidacao' },
            { header: 'Conta Corrente',  dataIndex: 'ContaCorrente'}
            );
        
        
        if(!this.actionStatus){
            this.columns.push({
                xtype:'actioncolumn',
                width:64,
                hidden: !CRMax.globals.userInfo.Permissoes.PodeBoletar,
                renderer: function(v, meta, record){
                    var status = record.get('Status');
                    return status === CRMax.globals.StatusOrdemTesourariaPorto.Digitado && !CRMax.globals.userInfo.Permissoes.PodeDistribuir ?  
                        '<div class="x-action-col-0 x-action-col x-action-col-resgatar">cancelar</div>' : ''},
                tooltip: 'cancelar',
                scope: this,
                handler: this.onClickButtonRejeitar
            });
        }
        
        if(!this.actionStatus || this.actionStatus === 'aprovacao'){    
            this.columns.push({
                xtype:'actioncolumn',
                width:55,
                hidden: !CRMax.globals.userInfo.Permissoes.PodeAprovar,
                renderer: function(v, meta, record){
                    return record.get('Status') !== CRMax.globals.StatusOrdemTesourariaPorto.Distribuido ? '' : 
                        '<div class="x-action-col-0 x-action-col x-action-col-aplicar">aprovar</div>';},
                tooltip: 'aprovar',
                scope: this,
                handler: this.onClickButtonAprovar
            },{
                xtype:'actioncolumn',
                width:64,
                hidden: !CRMax.globals.userInfo.Permissoes.PodeAprovar,
                renderer: function(v, meta, record){
                    var status = record.get('Status');
                    
                    return record.get('Status') !== CRMax.globals.StatusOrdemTesourariaPorto.Distribuido ? '' : 
                        '<div class="x-action-col-0 x-action-col x-action-col-resgatar">cancelar</div>'},
                tooltip: 'cancelar',
                scope: this,
                handler: this.onClickButtonRejeitar
            }
            );    
        }
        
        if(!this.actionStatus || this.actionStatus === 'distribuicao'){    
            this.columns.push({
                xtype:'actioncolumn',
                width:64,
                hidden: !CRMax.globals.userInfo.Permissoes.PodeDistribuir,
                renderer: function(v, meta, record){
                    return record.get('Status') !== CRMax.globals.StatusOrdemTesourariaPorto.Digitado ? '' : 
                        '<div class="x-action-col-0 x-action-col x-action-col-info">distribuir</div>'},
                tooltip: 'distribuir',
                scope: this,
                handler: this.onClickButtonDistribuir
            },{
                xtype:'actioncolumn',
                width:64,
                hidden: !CRMax.globals.userInfo.Permissoes.PodeDistribuir,
                renderer: function(v, meta, record){
                    var status = record.get('Status');
                    
                    return record.get('Status') !== CRMax.globals.StatusOrdemTesourariaPorto.Digitado ? '' : 
                        '<div class="x-action-col-0 x-action-col x-action-col-resgatar">cancelar</div>'},
                tooltip: 'distribuir',
                scope: this,
                handler: this.onClickButtonRejeitarDistribuicao
            });
        }
        
        this.columns.push({
                xtype:'actioncolumn',
                width:64,
                renderer: function(v, meta, record){
                    return record.get('IsDistribuida') ? '<div class="x-action-col-0 x-action-col x-action-col-info">ordem</div>' : '' 
                        },
                tooltip: '+info',
                scope: this,
                handler: this.onClickButtonInfoDistribuicao
            }
        );

        this.callParent(arguments);
        
        CRMax.application.on('ordemupdated', function(){this.getStore().load()}, this);
        this.getStore().load();
    },


    
        onButtonPrintWYSIWYGClick: function () {
        var el = this.getEl();

        var html = [
                '<!DOCTYPE html>',
                '<html>',
                '<head>',
                    '<!--meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /-->',
                    '<meta charset="utf-8" />',
                    '<title>Extrato</title>',
                    '<link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />',
                    '<link href="Content/Site.css" rel="stylesheet" type="text/css" />',
                    '<link href="Content/print-grid.css" rel="stylesheet" type="text/css" />',
                    //'<link href="Content/print.css" rel="stylesheet" type="text/css" />',
                '</head>',
                '<body class="grid-print order-grid ">',
                /*'<div class="header">',
                    '<img src="Content/themes/base/images/logo_cliente.gif" alt="Logo" />',
                    '<div id="form-title">Extrato</div>',
                '</div>',*/
                '{0}',
                 '</body>',
                 '</html>'];

        html = Ext.String.format(html.join(''), el.dom.innerHTML);

        //open up a new printing window, write to it, print it and close
        var win = window.open('', 'printgrid');

        //document must be open and closed
        win.document.open();
        win.document.write(html);
        win.document.close();

        win.print();

        /*if (Ext.isIE) {
            window.close();
        } else {
            win.close();
        }*/

    },
    
    onClickButtonDistribuir: function(grid, rowIndex, colIndex, item, e, record, row){
        var window = Ext.create('CRMax.view.ordem.Distribute', {
            idOperacao: record.get('IdOperacao'),
            tipoOperacao: record.get('TipoOperacao'),
            saldoTotal: record.get('Valor'),
            formaLiquidacao:  record.get('FormaLiquidacao'),
            contaCorrente: record.get('ContaCorrente'),
            tipoOperacaoString: record.get('TipoOperacaoString'),
            idCotista: record.get('IdCotista'),
            nomeCotista: record.get('NomeCotista')
        });
        window.show();
    },
    
    onClickButtonInfoDistribuicao: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var window = gridPanel.getWindowDistribuicao();
        window.setTitle('Distribuição da Ordem');
        
        var idOperacao = record.get('IdOperacao');
        window.updateFilterOperacaoMae(idOperacao);
        window.hideActionButtons();
        window.show();
    },

    getWindowDistribuicao: function(){
        if(!this.windowDistribuicao){
            this.windowDistribuicao = Ext.create('CRMax.view.ordem.distribute.List');
        }
        
        return this.windowDistribuicao;
    },
    
    onClickButtonAprovar: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var window = gridPanel.getWindowDistribuicao();

        window.setTitle('Aprovar Ordem - ' + record.get('NomeCotista'));
        var idOperacao = record.get('IdOperacao');
        window.updateFilterOperacaoMae(idOperacao);
        window.setActionAprovar();
        window.show();
    },

    onClickButtonRejeitar: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var idOperacao = record.get('IdOperacao');
        //gridPanel.updateStatusOrdem(idOperacao, 'rejeitar', 'Cancelamento efetuado com sucesso.');
        
        Ext.Msg.prompt('Cancelamento', 'Por favor, informe o motivo do cancelamento desta ordem:', function(btn, motivoCancelamento){
            if (btn == 'ok'){
                gridPanel.updateStatusOrdem(idOperacao, 'rejeitar', 'Ordem Cancelada', motivoCancelamento);
            }
        }, this, true);
        
    },

    onClickButtonRejeitarDistribuicao: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var idOperacao = record.get('IdOperacao');
        
        Ext.Msg.prompt('Cancelamento', 'Por favor, informe o motivo do cancelamento desta ordem:', function(btn, motivoCancelamento){
            if (btn == 'ok'){
                gridPanel.updateStatusOrdem(idOperacao, 'rejeitar', 'Ordem Cancelada', motivoCancelamento);
            }
        }, this, true);
        
        
        
        /*var gridPanel = grid.up('gridpanel');
        var window = gridPanel.getWindowDistribuicao();
        
        var idOperacao = record.get('IdOperacao');
        window.updateFilterOperacaoMae(idOperacao);
        window.setActionCancelar();
        window.show();*/
    },
    
    setStoreFilter: function (filter) {

        var store = this.getStore();
        if (!store.lastFilter) {
            store.lastFilter = [];
        }

        if (Ext.encode(filter) === Ext.encode(store.lastFilter)) {
            //Prevent double submission if filter hasn't changed
            return;
        }

        store.clearFilter(true);
        store.lastFilter = Ext.clone(filter);
        store.filter(filter);

    },
    
    updateStatusOrdem: function(idOperacao, action, msgSucesso, motivoCancelamento){
        var gridPanel = this;
        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'controllers/ordemtesouraria/updateStatus.ashx',
            params: {
                idOperacao: idOperacao,
                action: action,
                motivoCancelamento: motivoCancelamento
            },
            success: function(response){
                Ext.Msg.alert('Aviso',msgSucesso); 
                
                //CRMax.application.notificationWindow.update(msgSucesso);
                //CRMax.application.notificationWindow.show();
                CRMax.application.fireEvent('ordemupdated');
                gridPanel.getStore().load();
            }
        });
    }
});