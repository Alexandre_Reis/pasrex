﻿Ext.define('CRMax.view.ordem.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.ordemedit',
    title: 'Editar Ordem',
    autoScroll: true,

    modal: true,
    shadow: false,
    resizable: false,
    border: false,
    draggable: false,
    width: 600,
    tipoResgateEnum: {
        parcial: 1,
        total: 2
    },
    tipoOperacaoEnum: {
        aplicacao: 1,
        resgateBruto: 2,
        resgateLiquido: 3,
        resgateTotal: 5
    },
    
    initComponent: function () { 
    
        var contasCotista = CRMax.globals.contasCorrentes;
        
        var contasCotistaStore = Ext.create('Ext.data.Store', {
             fields: ['IdConta', 'Descricao'],
             data : contasCotista
        });
        var contaCotistaDefault = contasCotista.length > 0 ? contasCotista[0].IdConta : undefined;
      
        var formasLiquidacao = CRMax.globals.formasLiquidacao;
        var formasLiquidacaoStore = Ext.create('Ext.data.Store', {
             fields: ['IdFormaLiquidacao', 'Descricao'],
             data : formasLiquidacao
        });
    
        var isAplicacao = this.tipoOperacao === this.tipoOperacaoEnum.aplicacao;
       
        this.setTitle(isAplicacao ? 'Aplicar' : 'Resgatar');
       
        this.formPanel = new Ext.form.Panel(
        {
            fieldDefaults: {
                msgTarget: 'under'
            },

            bodyStyle: 'padding: 20px;',
            itemId: 'ordem-form',
            items: [
          {
              xtype: 'datefield',
              name: 'DataOperacao',
              fieldLabel: 'Data',
              value: new Date(),
              allowBlank: false,
              submitFormat: 'Y-m-d',
              altFormats: 'jnY|dnY|dmY'
          },
          {
            xtype: 'radiogroup',
            fieldLabel: 'Tipo de Resgate',
            columns: 1,
            hidden: isAplicacao,
            itemId: 'tiporesgate-radio',
            vertical: true,
            listeners: {
                change: function(radio, newValue){
                    var valorBrutoField = this.formPanel.down('#Valor');
                    if(newValue.TipoResgate === this.tipoResgateEnum.parcial){
                        valorBrutoField.show();
                        this.tipoResgate = this.tipoResgateEnum.parcial;
                    }else{
                        valorBrutoField.hide();
                        valorBrutoField.setValue(0);
                        this.tipoResgate = this.tipoResgateEnum.total;
                    }
                },
                scope: this
            },
            items: [{
                boxLabel: 'Parcial',
                name: 'TipoResgate',
                style: 'margin-bottom: 20px',
                inputValue: this.tipoResgateEnum.parcial,
                checked: false
            },{
                boxLabel: 'Total',
                name: 'TipoResgate',
                inputValue: this.tipoResgateEnum.total,
                checked: false
            }]
        },
          {
              xtype: 'numericfield',
              itemId: 'Valor',
              name: 'ValorMask',
              decimalSeparator: ',',
              decimalPrecision: 2,
              allowNegative: false,
              alwaysDisplayDecimals: true,
              hideTrigger: true,
              useThousandSeparator: true,
              fieldLabel: 'Valor',
              allowBlank: false,
              currencySymbol: 'R$'
          },
          {
              xtype: 'combo',
              name: 'IdFormaLiquidacao',
              fieldLabel: 'Forma Liquidacação',
              allowBlank: false,
              itemId: 'IdFormaLiquidacao',
              store: formasLiquidacaoStore,
              queryMode: 'local',
              displayField: 'Descricao',
              valueField: 'IdFormaLiquidacao',
              editable: false,
              allowBlank: false,
              width: 240,
              forceSelection: true,
              listeners: {
                change: function(combo, newValue){
                    var idContaField = this.formPanel.down('#IdConta');
                    var comboDisplayValue = combo.getRawValue();
                    comboDisplayValue = comboDisplayValue.toLowerCase();
                    this.idContaObrigatorio = comboDisplayValue === 'ted' || comboDisplayValue === 'cetip' ||comboDisplayValue === 'doc' || comboDisplayValue.indexOf('corrente') !== -1;
                    if(this.idContaObrigatorio === true){
                        idContaField.show();
                    }else{
                        idContaField.hide();
                    }
                },
                scope: this
            }
          },
          {
              xtype: 'combo',
              name: 'IdConta',
              fieldLabel: 'Conta Liquidação',
              allowBlank: true,
              itemId: 'IdConta',
              store: contasCotistaStore,
              queryMode: 'local',
              hidden: true,
              displayField: 'Descricao',
              valueField: 'IdConta',
              editable: false,
              value: contaCotistaDefault,
              width: 350,
              forceSelection: true
          }
        ], buttons: [
        {
            baseCls: '',
            cls: 'btn',
            text: 'Cancelar',
            scope: this,
            handler: this.close
        },
            {
                text: isAplicacao ? 'Aplicar' : 'Resgatar',
                baseCls: '',
                scope: this,
                cls: 'btn btn-inverse',
                handler: this.saveForm
            }
        ]

        });

        this.items = [this.formPanel];

        this.callParent(arguments);
    },
    
    saveForm: function(){
        var basicForm = this.formPanel.getForm();
        var tipoOperacao;
        var tipoOperacaoString;
        
        var fieldValues = basicForm.getValues();
        
        var isAplicacao = false;
        if(this.tipoOperacao == this.tipoOperacaoEnum.aplicacao){
            isAplicacao = true;
            tipoOperacao = this.tipoOperacaoEnum.aplicacao;
            tipoOperacaoString = 'Aplicação';
        }else if(this.tipoResgate === this.tipoResgateEnum.parcial){
            tipoOperacao = this.tipoOperacaoEnum.resgateLiquido;
            tipoOperacaoString = 'Resgate parcial';
        }else if(this.tipoResgate === this.tipoResgateEnum.total){
            tipoOperacao = this.tipoOperacaoEnum.resgateTotal;
            tipoOperacaoString = 'Resgate total';
        }else{
            Ext.Msg.alert('Aviso','Por favor, selecione o tipo de resgate');
            return false;
        }
        
        if(this.idContaObrigatorio === true){
            var idContaField = this.formPanel.down('#IdConta');
            if(idContaField.getValue() == ''){
                Ext.Msg.alert('Aviso','Por favor, selecione a conta para liquidação');
                return false;
            }
        }
        
        var valor = this.formPanel.down('#Valor').getValue();
        if(this.tipoResgate !== this.tipoResgateEnum.total){        
            if(valor <= 0){
                Ext.Msg.alert('Aviso','Valor digitado é inválido');
                return false;
            }
        }
                
        basicForm.submit({
            params: {
                TipoOperacao: tipoOperacao,
                action: 'save',
                Valor: valor
            },
            url: CRMax.globals.baseDirectory + 'Controllers/OrdemTesouraria/Save.ashx',
            waitMsg: 'Processando...',
            waitTitle: 'Aguarde',
            scope: this,
            success: function (fp, o) {
                Ext.Msg.alert('Aviso','Ordem enviada com sucesso'); 
                
                //CRMax.application.notificationWindow.update('Ordem enviada com sucesso');
                //CRMax.application.notificationWindow.show();
                CRMax.application.fireEvent('ordemupdated');
                this.close();
            },
            failure: function (form, action) {
                var responseObject = Ext.JSON.decode(action.response.responseText);
                var msgAlerta = responseObject && responseObject.errorMessage && responseObject.errorMessage.length ?
                    responseObject.errorMessage : 'Ocorreu um erro na comunicação';
                Ext.Msg.alert('Aviso', msgAlerta);
            }
        });
    }
});