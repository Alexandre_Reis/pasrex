﻿Ext.define('CRMax.view.ordem.distribute.List', {
    extend: 'Ext.window.Window',
    alias: 'widget.ordemdistributelist',
    border: false,
    title: 'Distribuição da ordem',
    height: 400,
    width: 720,
    modal: true,
    border: false,
    resizable: false,
    layout: 'fit',
    closeAction: 'hide',
    actions: {
        aprovar: 'aprovar',
        cancelar: 'cancelar'
    },
    
    initComponent: function () {
        var gridColumns = [
            { header: 'Fundo',  dataIndex: 'NomeCarteira', flex: 1 },
            { header: 'Tipo Liquidação',  dataIndex: 'FormaLiquidacao', width: 100 },
            { header: 'Conta Corrente',  dataIndex: 'ContaCorrente', width: 150 },
            { header: 'Histórico',  dataIndex: 'TipoOperacaoString'},
            { header: 'Valor informado', dataIndex: 'Valor', xtype: 'numbercolumn', format:'R$ 0,000.00', align: 'right' }
                
        ];
        
        this.gridPanel = Ext.create('Ext.grid.Panel', {
            store: 'OrdensDistribuidas',
            columns: gridColumns,
            enableColumnMove: false,
            enableColumnResize: false,
            sortableColumns: false,
            enableColumnHide: false
        });
        
        this.buttons = [{
                baseCls: '',
                cls: 'btn btn-inverse',
                hidden: true,
                itemId: 'btn-aprovar',
                text: 'Aprovar Ordem',
                scope: this,
                handler: function(){this.updateStatusOrdem(this.actions.aprovar);}
            },{
                baseCls: '',
                cls: 'btn btn-inverse',
                itemId: 'btn-cancelar',
                hidden: true,
                text: 'Cancelar Ordem',
                scope: this,
                handler: function(){this.updateStatusOrdem(this.actions.cancelar);}
            },{
                baseCls: '',
                cls: 'btn',
                scope: this,
                text: 'Fechar',
                handler: function(){this.close();}
            }];
        
        this.items = [this.gridPanel];
        
        this.callParent(arguments);
    },

    loadStore: function () {
        this.gridPanel.store.load();
    },
    
    filter: function(filters){
        this.gridPanel.store.clearFilter(true);
        this.gridPanel.store.filter(filters);
    },
    
    updateFilterOperacaoMae: function(idOperacaoMae){
        this.idOperacaoMae = idOperacaoMae;
        var filters = [];
            
        filters.push({
            property: 'IdOperacaoMae',
            value: idOperacaoMae
        });
            
        this.filter(filters);
    },
    
    setActionAprovar: function(){
        this.updateAction(this.actions.aprovar);
    },
    
    setActionCancelar: function(){
        this.updateAction(this.actions.cancelar);
    },
    
    hideActionButtons: function(){
        var btnAprovar = this.down('#btn-aprovar');
        var btnCancelar = this.down('#btn-cancelar');
        btnAprovar.hide();
        btnCancelar.hide();
    },
   
    updateAction: function(action){
        this.action = action;
        
        this.hideActionButtons();     
        
        if(action == this.actions.aprovar){
            var btnAprovar = this.down('#btn-aprovar');
            btnAprovar.show();
        }else if(action == this.actions.cancelar){
            var btnCancelar = this.down('#btn-cancelar');
            btnCancelar.show();
        }
    },
    
    updateStatusOrdem: function(action){
        this.hideActionButtons();
        
        Ext.Ajax.request({
            scope: this,
            url: CRMax.globals.baseDirectory + 'controllers/ordemtesouraria/updateStatus.ashx',
            params: {
                idOperacao: this.idOperacaoMae,
                action: action
            },
            success: function(response){
                this.updateAction(action);
                var responseObject = Ext.JSON.decode(response.responseText);
                
                if(responseObject.errorMessage && responseObject.errorMessage.length > 0){
                    Ext.Msg.alert('Aviso',responseObject.errorMessage);
                }else{
                    Ext.Msg.alert('Aviso','Operação executada com sucesso'); 
                    
                    //CRMax.application.notificationWindow.update('Operação executada com sucesso');
                    //CRMax.application.notificationWindow.show();
                }
                
                CRMax.application.fireEvent('ordemupdated');
                this.close();
                
            },
            failure: function(response){
                this.updateAction(action);
                var responseObject = Ext.JSON.decode(action.response.responseText);
                var msgAlerta = responseObject && responseObject.errorMessage && responseObject.errorMessage.length ?
                    responseObject.errorMessage : 'Ocorreu um erro na comunicação';
                Ext.Msg.alert('Aviso', msgAlerta);
            }
        });
    }
});