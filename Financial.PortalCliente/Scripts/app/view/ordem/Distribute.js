﻿Ext.define('CRMax.view.ordem.Distribute', {
    extend: 'Ext.window.Window',
    alias: 'widget.ordemdistribute',
    itemId: 'ordem-distribute',
    autoScroll: true,
    modal: true,
    closable: true,
    title: 'Distribuir Ordens',
    border: false,
    bodyStyle: 'background-color: #f8f8f8; ',
    initComponent: function () {
        //Limpar store
        var storeFundosTemPermissao = new Ext.create('CRMax.store.FundosCotista');
        storeFundosTemPermissao.filter('TemPermissaoAplicar', true);
        
        var storePosicoes = Ext.data.StoreManager.lookup('Posicoes');
        storePosicoes.removeAll();
        var paramsLoadPosicoes = {idCotista: this.idCotista};
        storePosicoes.load({params: paramsLoadPosicoes});
    
        var storeDistribuicao = Ext.create('Ext.data.Store', {
            fields: ['IdCarteira','NomeCarteira', 'Valor']
        });
        
        var isResgateTotal = this.saldoTotal === 0;
                
        this.saldos = {
            saldoTotal: this.saldoTotal,
            saldoRestante: this.saldoTotal,
            subTotal: 0
        };

        var bbarDistribuicao = [
            { 
                xtype: 'component', 
                tpl: 'Sub-total: R${SubTotal:number("0,000.00"} - Saldo restante: R${Saldo:number("0,000.00"}', 
                data:{SubTotal: this.saldos.subTotal, Saldo: this.saldos.saldoRestante} 
            }
        ];

        var columns = [
            { header: 'Fundo',  dataIndex: 'NomeCarteira', flex: 1 }
        ];

        if(!isResgateTotal){
            columns.push({ header: 'Valor informado', dataIndex: 'Valor', xtype: 'numbercolumn', format:'R$ 0,000.00', align: 'right' });
        }

        columns.push({
                    xtype:'actioncolumn',
                    width:70,
                    renderer: function(v, meta){return '<div class="x-action-col-0 x-action-col x-action-col-resgatar">remover</div>'},
                    tooltip: 'remover',
                    scope: this,
                    handler: this.onClickButtonApagarDistribuicaoOrdem
        });

        this.gridDistribuicao = Ext.create('Ext.grid.Panel', {
            store: storeDistribuicao,
            enableColumnMove: false,
            enableColumnResize: false,
            sortableColumns: false,
            enableColumnHide: false,
            columns: columns,
             dockedItems: isResgateTotal ? undefined : [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    itemId: 'bottomtoolbar',
                    items: bbarDistribuicao
                }
            ],
            height: 200
        });
       
        var comboFundoConfig = {};
        if(this.tipoOperacao == 1){
            //Aplicacao mostrar todos os fundos que o cotista pode aplicar (termo adesao assinado)
            //comboFundoConfig.store = 'FundosCotista';
            //comboFundoConfig.displayField = 'Nome';
            comboFundoConfig.store = storeFundosTemPermissao;
            
            comboFundoConfig.tpl = Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '<div class="x-boundlist-item">{Apelido} - {Nome}</div>',
                '</tpl>'
            );
    
            comboFundoConfig.displayTpl = Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '{Apelido} - {Nome}',
                '</tpl>'
            );
            
        }else{
            //Resgate mostrar apenas fundos com posicao e exibir saldo
            comboFundoConfig.store = 'Posicoes';
            //comboFundoConfig.displayField = 'NomeCarteira';
            comboFundoConfig.tpl = Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '<div class="x-boundlist-item">{ApelidoCarteira} - {NomeCarteira} <span style="color:#999; padding-left: 10px;"> R$ {ValorLiquido:number("0,000.00"}</span></div>',
                '</tpl>'
            );
            
            comboFundoConfig.displayTpl = Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '{ApelidoCarteira} - {NomeCarteira}',
                '</tpl>'
            );
        }
        
        var comboFundo = {
              xtype: 'combo',
              name: 'IdCarteira',
              fieldLabel: 'Fundo',
              allowBlank: false,
              itemId: 'ComboFundo',
              store: comboFundoConfig.store,
              queryMode: 'local',
              displayField: comboFundoConfig.displayField,
              valueField: 'IdCarteira',
              value: this.idCarteira,
              width: 500,
              tpl: comboFundoConfig.tpl,
              displayTpl: comboFundoConfig.displayTpl,
              forceSelection: true
          };
        
        var valorField = {
              itemId: 'Valor',
              name: 'Valor',
              xtype: 'numericfield',
              decimalSeparator: ',',
              decimalPrecision: 2,
              allowNegative: false,
              alwaysDisplayDecimals: true,
              hideTrigger: true,
              useThousandSeparator: true,
              fieldLabel: 'Valor',
              allowBlank: false,
              hidden: isResgateTotal
          };
        
        var nomeCotista = {
            xtype: 'displayfield',
            fieldLabel: 'Cotista',
            name: 'Cotista',
            value: this.nomeCotista
        };
        
        var tipoOperacao = {
            xtype: 'displayfield',
            fieldLabel: 'Tipo Operação',
            name: 'TipoOperacaoString',
            value: this.tipoOperacaoString
        };
        
        var formaLiquidacao = {
            xtype: 'displayfield',
            fieldLabel: 'Forma Liquidação',
            name: 'FormaLiquidacao',
            value: this.formaLiquidacao
        };
        
        var contaCorrente = {
            xtype: 'displayfield',
            fieldLabel: 'Conta Corrente',
            name: 'ContaCorrente',
            value: this.contaCorrente
        };
        
        var btnInserir = { 
                    xtype: 'button',
                    text: 'Inserir',
                    handler: this.onInserirOrdem,
                    scope: this
                };
          
        var formFields = [comboFundo, valorField, nomeCotista, tipoOperacao, formaLiquidacao, contaCorrente, btnInserir];


        this.formPanel = Ext.create('Ext.form.Panel', {
            items: formFields,
            bodyStyle: 'padding: 10px; background-color: #f3f3f3;',
            border: false
        });


        this.items = [
             this.formPanel, this.gridDistribuicao
        ];

        this.buttons = [
                {
            baseCls: '',
            cls: 'btn',
            text: 'Cancelar',
            scope: this,
            handler: this.close
        },{ text: 'Gravar',
            baseCls: '',
                cls: 'btn btn-inverse',
                    handler: this.onSaveDistribuicao,
                    scope: this
                }];

        this.callParent(arguments);
    },
    
    atualizarSaldo: function(){
        var store = this.gridDistribuicao.getStore();
        this.saldos.saldoRestante = this.saldos.saldoTotal;
        this.saldos.subTotal = 0;
        store.each(function(record){
            var valorRecord = record.get('Valor');
            this.saldos.saldoRestante -= valorRecord;
            this.saldos.saldoRestante = +this.saldos.saldoRestante.toFixed(2); //Round Javascript
            this.saldos.subTotal += valorRecord;
            this.saldos.subTotal = +this.saldos.subTotal.toFixed(2); //Round Javascript
            
        }, this);
        
        //Atualizar bottom bar
        var bbar = this.gridDistribuicao.getDockedComponent('bottomtoolbar');
        var totalComponent = bbar.items.getAt(0);
        totalComponent.update({SubTotal: this.saldos.subTotal, Saldo: this.saldos.saldoRestante});
    },
    
    onInserirOrdem: function(){
        var store = this.gridDistribuicao.getStore();
        var valorField = this.formPanel.down('#Valor');
        var valor = valorField.getValue();
        
        var comboFundo = this.formPanel.down('#ComboFundo');
        var idCarteira = comboFundo.getValue();
        if(!idCarteira){
            return false;
        }
        
        var isResgateTotal = this.saldoTotal === 0;
        
        if(!isResgateTotal){
            if(!valor){
                return false;
            }
        
            if(valor <= 0){
                Ext.Msg.alert('Aviso','Valor inválido');
                return false;
            }
            
            if(this.tipoOperacao !== 1){
               //Se ResgateParcial, verificar se o valor lançado nao é maior do que o saldo na posição
                var comboFundoRecord = comboFundo.findRecord(comboFundo.valueField, idCarteira);
                var saldoPosicao = comboFundoRecord.get('ValorLiquido');
                if(valor > saldoPosicao){
                    Ext.Msg.alert('Aviso','O valor do resgate informado ultrapassa o saldo do fundo escolhido');
                    return false;
                }
            }
            
            if(valor > this.saldos.saldoRestante){
               Ext.Msg.alert('Aviso','O valor informado ultrapassa o saldo restante');
                return false;
            }
        }else{
            valor = 0;
        }

        //Não permitir inserir operações para carteiras repetidas
        var existeOperacaoCarteira = store.find('IdCarteira', idCarteira, 0, false, false, true) !== -1;
        if(existeOperacaoCarteira){
            Ext.Msg.alert('Aviso','Este fundo já se encontra selecionado na lista de distribuições');
            return false;
        }
        
        var newRecord = {
            IdCarteira: comboFundo.getValue(),
            NomeCarteira: comboFundo.getRawValue(),
            Valor: valor
        };
        
        store.add(newRecord);
        
        //Atualizar saldo
        this.atualizarSaldo();
        
        comboFundo.reset();
        valorField.reset();
        
    },
    
    onSaveDistribuicao: function(btn){
        if(this.saldos.saldoRestante !== 0){
            Ext.Msg.alert('Aviso','A distribuição só pode ser realizada após a utilização de todo o saldo da operação');
            return false;
        }

        if(btn && btn.disable){
            btn.disable();
        }
            
        var that = this;
        var store = this.gridDistribuicao.getStore();
        var ordens = Ext.encode(Ext.pluck(store.data.items, 'data'));
        
        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'controllers/ordemtesouraria/distribute.ashx',
            params: {
                ordens: ordens,
                idOperacao: this.idOperacao
            },
            success: function(response){
                if(btn && btn.enable){
                    btn.enable();
                }
            
                var responseObject = Ext.JSON.decode(response.responseText);
                if(responseObject.errorMessage && responseObject.errorMessage.length > 0){
                    Ext.Msg.alert('Aviso',responseObject.errorMessage);
                    return false;
                }
                
                Ext.Msg.alert('Aviso','Ordem distribuída com sucesso'); 
                
                //CRMax.application.notificationWindow.update('Ordem distribuída com sucesso');
                //CRMax.application.notificationWindow.show();
                
                CRMax.application.fireEvent('ordemupdated');
                that.close();
            },
            failure: function(response){
                if (!(response && response.responseText && (response.responseText.indexOf('Convert.ToInt32((Convert.ToString(context.Session') > 0))) {
                    Ext.Msg.alert('Aviso','Ocorreu um erro durante a gravação. Por favor, tente novamente.');    
                }
                if(btn && btn.enable){
                    btn.enable();
                }
            }
        });
    },
  
    
    onClickButtonApagarDistribuicaoOrdem: function(grid, rowIndex, colIndex, item, e, record, row){
        var store = grid.getStore();
        store.remove(record);
        
        var window = grid.up('window');
        window.atualizarSaldo();
    }

  });