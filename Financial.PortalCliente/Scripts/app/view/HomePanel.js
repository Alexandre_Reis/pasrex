﻿Ext.define('CRMax.view.HomePanel', {
    extend: 'Ext.Panel',
    alias: 'widget.homepanel',
    border: false,
    autoScroll: true,
    //layout: 'border',
    //layout: 'column',
    cls: 'home-panel',
    tplAplicacoesPendentes: 'Aplicações pendentes de cotização:<span style="padding-left: 20px;">R$ {0}</span>',
    initComponent: function () {
       
       var fundoList = new CRMax.view.fundo.List();
        
       var rightColumn = {
            title: 'Fundos disponíveis para Aplicação',
            //columnWidth: 1,
            //width: 800,
            //minWidth: 200,
            //height: 500,
            //bodyStyle: 'padding: 20px 10px; background-color: #fff;',
            region: 'south',
            border: false,
            //layout: 'fit',
            autoScroll: true,
            items: fundoList
        };

        var posicaoList = new CRMax.view.posicao.List();
        
        var posicaoDetalheList = new CRMax.view.posicao.detalhe.List();

        var winPosicaoDetalhe = Ext.create('Ext.window.Window', {
            title: 'Posição Detalhada',
            height: 400,
            width: 900,
            y: CRMax.application.inIframe() ? 50 :undefined,
            modal: true,
            border: false,
            resizable: false,
            layout: 'fit',
            closeAction: 'hide',
            items: posicaoDetalheList,
            buttons: [{
                baseCls: '',
                cls: 'btn',
                text: 'Fechar',
                handler: function(){
					winPosicaoDetalhe.close();}
            }]
        });

		
        
        var aplicacoesPendentesContainer = {
            id : 'aplicacoes-pendentes-container',
            html: Ext.String.format(this.tplAplicacoesPendentes, Ext.util.Format.number(CRMax.globals.aplicacoesPendentes,'0,000.00')),
            style: 'margin-top: 20px;',
            bodyStyle: 'padding: 7px;background-color: #efefef;'
        };
        
        var disclaimerContainer = {
            html: CRMax.globals.disclaimerPosicao,
            border: false,
            bodyStyle: 'padding: 10px'
        };
        
        var leftColumn = {
            //columnWidth: .3,
            //width: 300,
            //columnWidth: .5,
            //width: 500,
            //maxWidth: 500,
            title: 'Saldos Fundos',
            bodyStyle: 'padding-bottom: 30px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: [posicaoList, aplicacoesPendentesContainer, disclaimerContainer]
        };
        
        CRMax.application.on('detalheclick', function(record){
            var idCarteira = record.get('IdCarteira');
            var filters = [];
            
            filters.push({
                property: 'IdCarteira',
                value: idCarteira
            });
            
            posicaoDetalheList.filter(filters);
            var dataPosicao = Ext.Date.format(record.get('DataDia'), "j/n/Y");
            winPosicaoDetalhe.setTitle('<span style="font-weight: normal">Posição Detalhada em ' + dataPosicao + 
                ' :</span> ' + record.get('NomeCarteira'));
            winPosicaoDetalhe.show();
        });
        
        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
        
        CRMax.application.on('operacaoupdated', this.updateAplicacoesPendentes, this);
    },
    
    updateAplicacoesPendentes: function(){
        var container = Ext.getCmp('aplicacoes-pendentes-container');
        
        Ext.Ajax.request({
            scope: this,
            url: CRMax.globals.baseDirectory + 'controllers/application/loadglobals.ashx',
                success: function(response) {
                    //Exemplo texto parsear ->> "aplicacoesPendentes": 123379894652746.30,
                    var propertyName = "aplicacoesPendentes";
                    var posicaoInicial = response.responseText.indexOf(propertyName) + propertyName.length;
                    var posicaoFinal = response.responseText.indexOf(",", posicaoInicial);
                    var aplicacoesPendentes = parseFloat(response.responseText.substring((posicaoInicial + 3), (posicaoFinal)));
                    container.update(Ext.String.format(this.tplAplicacoesPendentes, Ext.util.Format.number(aplicacoesPendentes,'0,000.00')));
                }
        });
        
        
    }

});
