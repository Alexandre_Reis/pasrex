﻿Ext.define('CRMax.view.posicaodetalhe.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.posicaodetalhelist',
    hideHeaders: false,
    store: 'PosicoesDetalhadas',

    border: false,

    initComponent: function () {
        
        this.columns = [
            //{dataIndex: 'DataAplicacao', width: 100, header: 'Data Aplicação', type: 'date', xtype: 'datecolumn',format: 'd/m/Y' },
            {dataIndex: 'Quantidade', header: 'Qtde. Cotas Atual', width: 100, xtype: 'numbercolumn', format:'0,000.000000', align: 'right'}
            /*{dataIndex: 'CotaDia', header: 'Valor Cota Atual', width: 100, xtype: 'numbercolumn', format:'0,000.000000', align: 'right'},
            {dataIndex: 'ValorBruto', header: 'Saldo Bruto', width: 100, xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'ValorIR', header: 'IR', width: 100, xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'ValorIOF', header: 'IOF', width: 100, xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            {dataIndex: 'ValorLiquido', header: 'Saldo Líquido', width: 100, xtype: 'numbercolumn', format:'0,000.00', align: 'right'}*/
        ];

        this.callParent(arguments);
    },

    loadStore: function () {
        this.store.load();
    }
});