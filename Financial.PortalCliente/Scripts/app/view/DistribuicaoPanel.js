﻿Ext.define('CRMax.view.DistribuicaoPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.distribuicaopanel',
    border: false,
    layout: 'border',
    cls: 'home-panel',
    initComponent: function () {
        
       var distribuicaoFilter = new CRMax.view.ordem.Filter({listId: 'distribuicao-list'}); 
       var rightColumn = {
            width: 360,
            bodyStyle: 'background-color: #fafafa;',
            region: 'east',
            border: false,
            autoScroll: true,
            items: [distribuicaoFilter]
       };


        var distribuicaoList = new CRMax.view.ordem.List({id: 'distribuicao-list', multiCotista: true, actionStatus: 'distribuicao'});
        
        var leftColumn = {
            columnWidth: 1,
            title: 'Distribuição',
            //bodyStyle: 'padding: 20px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: [distribuicaoList]
        };
        
        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
    }

});