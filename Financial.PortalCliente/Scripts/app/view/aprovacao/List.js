﻿Ext.define('CRMax.view.aprovacao.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ordemlist',
    hideHeaders: false,
    store: 'OrdensTesouraria',
    border: false,

    enableColumnMove: false,
    enableColumnResize: false,
    sortableColumns: false,
    enableColumnHide: false,

    initComponent: function () {
           this.tbar = [
                {
                   text: 'Imprimir',
                   //iconCls: 'icon-print',
                   scope: this,
                   baseCls: '',
                   cls: 'btn-aplicacao',
                   handler: this.onButtonPrintWYSIWYGClick
                }
            ];
        
        this.columns = [
            {dataIndex: 'DataOperacao', width: 80, header: 'Data', type: 'date', xtype: 'datecolumn',format: 'd/m/Y' },
            {dataIndex: 'NomeCotista', flex: 1, header: 'Cotista'},
            {dataIndex: 'TipoOperacaoString', width: 140, header: 'Tipo'},
            {dataIndex: 'Valor', width: 50, header: 'Valor (R$)', xtype: 'numbercolumn', format:'0,000.00', align: 'right'},
            { header: 'Tipo Liquidação',  dataIndex: 'FormaLiquidacao', width: 100 },
            { header: 'Conta Corrente',  dataIndex: 'ContaCorrente', width: 150 },
            {
                xtype:'actioncolumn',
                width:55,
                renderer: function(v, meta, record){
                    return '<div class="x-action-col-0 x-action-col x-action-col-aplicar">aprovar</div>';
                },
                tooltip: 'aprovar',
                scope: this,
                handler: this.onClickButtonAprovar
            },{
                xtype:'actioncolumn',
                width:64,
                renderer: function(v, meta, record){
                    return '<div class="x-action-col-0 x-action-col x-action-col-resgatar">cancelar</div>'
                },
                tooltip: 'cancelar',
                scope: this,
                handler: this.onClickButtonRejeitar
            }
        ];

        this.callParent(arguments);
        
        CRMax.application.on('ordemupdated', function(){this.getStore().load()}, this);
        this.getStore().load();
    },

        onButtonPrintWYSIWYGClick: function () {
        var el = this.getEl();

        var html = [
                '<!DOCTYPE html>',
                '<html>',
                '<head>',
                    '<!--meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /-->',
                    '<meta charset="utf-8" />',
                    '<title>Extrato</title>',
                    '<link rel="stylesheet" type="text/css" href="Scripts/ext-4.1.0/resources/css/ext-all.css" />',
                    '<link href="Content/Site.css" rel="stylesheet" type="text/css" />',
                    '<link href="Content/print-grid.css" rel="stylesheet" type="text/css" />',
                    //'<link href="Content/print.css" rel="stylesheet" type="text/css" />',
                '</head>',
                '<body class="grid-print order-grid ">',
                /*'<div class="header">',
                    '<img src="Content/themes/base/images/logo_cliente.gif" alt="Logo" />',
                    '<div id="form-title">Extrato</div>',
                '</div>',*/
                '{0}',
                 '</body>',
                 '</html>'];

        html = Ext.String.format(html.join(''), el.dom.innerHTML);

        //open up a new printing window, write to it, print it and close
        var win = window.open('', 'printgrid');

        //document must be open and closed
        win.document.open();
        win.document.write(html);
        win.document.close();

        win.print();

        /*if (Ext.isIE) {
            window.close();
        } else {
            win.close();
        }*/

    },
    
    onClickButtonAprovar: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var window = gridPanel.getWindowDistribuicao();
        
        var idOperacao = record.get('IdOperacao');
        window.updateFilterOperacaoMae(idOperacao);
        window.setActionAprovar();
        window.show();
    },

    onClickButtonRejeitar: function(grid, rowIndex, colIndex, item, e, record, row){
        var gridPanel = grid.up('gridpanel');
        var idOperacao = record.get('IdOperacao');
        gridPanel.updateStatusOrdem(idOperacao, 'rejeitar', 'Cancelamento efetuado com sucesso.');
    },

    getWindowDistribuicao: function(){
        if(!this.windowDistribuicao){
            this.windowDistribuicao = Ext.create('CRMax.view.ordem.distribute.List');
        }
        
        return this.windowDistribuicao;
    },


    setStoreFilter: function (filter) {

        var store = this.getStore();
        if (!store.lastFilter) {
            store.lastFilter = [];
        }

        if (Ext.encode(filter) === Ext.encode(store.lastFilter)) {
            //Prevent double submission if filter hasn't changed
            return;
        }

        store.clearFilter(true);
        store.lastFilter = Ext.clone(filter);
        store.filter(filter);

    },
    
    updateStatusOrdem: function(idOperacao, action, msgSucesso){
        var gridPanel = this;
        Ext.Ajax.request({
            url: CRMax.globals.baseDirectory + 'controllers/ordemtesouraria/updateStatus.ashx',
            params: {
                idOperacao: idOperacao,
                action: action
            },
            success: function(response){
                Ext.Msg.alert('Aviso',msgSucesso); 
                
                //CRMax.application.notificationWindow.update(msgSucesso);
                //CRMax.application.notificationWindow.show();
                CRMax.application.fireEvent('ordemupdated');
                gridPanel.getStore().load();
            }
        });
    }
});