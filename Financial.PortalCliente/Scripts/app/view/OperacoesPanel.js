﻿Ext.define('CRMax.view.OperacoesPanel', {
    extend: 'Ext.Panel',
    alias: 'widget.operacoespanel',
    border: false,
    layout: 'border',
    cls: 'home-panel',
    initComponent: function () {
        
       var operacaoFilter = new CRMax.view.operacao.Filter({listId: 'operacao-list'}); 
       
       var disclaimerContainer = {
            html: CRMax.globals.disclaimerOperacao,
            border: false,
            bodyStyle: 'margin-top: 10px; padding: 10px; background-color: transparent;'
        };
       
       var rightColumn = {
            //title: 'Pesquisar Operações',
            width: 360,
            bodyStyle: 'background-color: #fafafa;',
            region: 'east',
            border: false,
            autoScroll: true,
            items: [operacaoFilter, disclaimerContainer]
       };

        var operacaoList = new CRMax.view.operacao.List({id: 'operacao-list'});
        
        var leftColumn = {
            columnWidth: 1,
            title: 'Histórico de Movimentações',
            //bodyStyle: 'padding: 20px;',
            region: 'center',
            autoScroll: true,
            border: false,
            items: [operacaoList]
        };
        
        this.items = [leftColumn, rightColumn];

        this.callParent(arguments);
    }

});