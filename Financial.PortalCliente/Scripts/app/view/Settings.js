﻿Ext.define('CRMax.view.Settings', {
    extend: 'Ext.Panel',
    alias: 'widget.settings',
    border: false,
    layout: 'border',
    id: 'settings-panel',
    initComponent: function () {

        var settingsStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'caption'],
            data: [
                { id: 'users', caption: 'Usuários' },
                { id: 'deal-categories', caption: 'Categorias de Negócio' },
                { id: 'task-categories', caption: 'Categorias de Tarefa' },
                { id: 'custom-fields', caption: 'Campos Personalizados' }
            ]
        });

        var dataview = {
            xtype: 'dataview',
            id: 'settings-panel-dataview',
            store: settingsStore,
            region: 'west',
            border: false,
            width: 250,
            selectedItemCls: 'active-settings-entry',
            tpl: [
                '<h3>Gerenciar</h3>',
                '<ul>',
                    '<tpl for=".">',
                        '<li class="settings-entry">{caption}</li>',
                    '</tpl>',
                '</ul>'
            ],
            itemSelector: 'li.settings-entry'
        };

        this.items = [
                    dataview, {
                        itemId: 'cards',
                        layout: 'card',
                        region: 'center'
                    }
                ];

        this.callParent(arguments);
    }
});