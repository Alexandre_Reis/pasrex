﻿Ext.define('CRMax.view.WelcomePanel', {
    extend: 'Ext.Panel',
    alias: 'widget.welcomepanel',
    title: 'Bem vindo',
    border: false,
    cls: 'welcome-panel',
    autoScroll: true,
    bodyStyle: 'margin-top: 3px;',
    
    initComponent: function () {

        var html = [
            '<h2>Bem vindo ao Linkloud!</h2>',
            '<div class="step-one">',
                '<h3>Primeiro passo: <span class="link link-add-contacts">Adicione os seus contatos</span></h3>',
                '<p>O Linkloud é focado em pessoas, portanto esta é a melhor forma de começar a usá-lo</p>',
            '</div>',

            '<div>',
                '<h3>O Linkloud ajuda a manter o relacionamento do seu negócio em dia</h3>',
            '</div>',

            '<div class="features">',
                '<ul>',
                    '<li>',
                        '<img style="width: 102; height: 129px; margin-left: 30px;" src="' + CRMax.globals.imagesPath + '/address_book.png" />',
                        '<h4>Agenda Compartilhada</h2>',
                        '<p>Guarde e compartilhe informações sobre clientes, fornecedores, <i>leads</i> e outros</p>',
                    '</li>',

                    '<li>',
                        '<img style="width: 145; height: 129px; margin-left: 0px;" src="' + CRMax.globals.imagesPath + '/communication.png" />',
                        '<h4>Histórico de Contato</h2>',
                        '<p>Registre e recupere anotações sobre ligações, reuniões, emails, conversas, etc.</p>',
                    '</li>',

                    '<li>',
                        '<img style="width: 106; height: 129px; margin-left: 28px;" src="' + CRMax.globals.imagesPath + '/pencil_clipboard.png" />',
                        '<h4>Tarefas e Lembretes</h2>',
                        '<p>Organize lembretes por e-mail e SMS para garantir que o trabalho está sendo feito</p>',
                    '</li>',
                '</ul>',
                '<div class="clearfix"></div>',
            '</div>',

            '<div class="step-other">',
                '<h5>Você também pode...</h5>',
                '<h3><span class="link link-add-users">Convidar outras pessoas para usarem o Linkloud com você</span></h3>',
                '<p>Crie contas para seus colaboradores para que vocês possam usar o Linkloud juntos</p>',
            '</div>'
        ];

        this.items = [
                    {
                        border: false,
                        html: html.join('')
                    }
                ];

        this.callParent(arguments);
    },

    afterRender: function () {
        this.body.on('click', this.onBodyClick, this);
        this.callParent(arguments);
    },

    onBodyClick: function (eventObj, htmlElement, eventOptions) {
        var targetEl = Ext.get(eventObj.target);

        if (!targetEl.hasCls('link')) {
            return;
        }

        if (targetEl.hasCls('link-add-users')) {
            //CRMax.application.getController('People').onCreatePerson();
            //Ext.History.add('/settings');
            CRMax.application.getController('MainNavigator').selectNode('settings');
        } else if (targetEl.hasCls('link-add-contacts')) {
            CRMax.application.getController('People').onButtonCreatePersonClick();
        }

    }
});