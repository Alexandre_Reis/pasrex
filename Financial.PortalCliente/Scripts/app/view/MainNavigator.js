﻿Ext.define('CRMax.view.MainNavigator', {
    extend: 'Ext.view.View',
    alias: 'widget.mainnavigator',
    id: 'MainNavigator',
    //width: 60,
    //height: 150,
    lines: false,
    rootVisible: false,
    overItemCls: 'menu-nav-option-over',
    trackOver: true,

    initComponent: function () {
        this.store = Ext.create('CRMax.store.MainNavigator');
        
        this.tpl = new Ext.XTemplate(
            '<ul class="menu-nav">',
            '<tpl for=".">',
                '<li class="menu-nav-option">',
                    '{description}',
                '</li>',
            '</tpl>',
            '</ul>'
        );
        
        this.itemSelector = 'li.menu-nav-option';
        
        this.callParent(arguments);
    }
});