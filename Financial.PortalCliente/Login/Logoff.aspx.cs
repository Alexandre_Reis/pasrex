﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Financial.Security;
using System.Configuration;

public partial class login_Logoff : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        //Pagina LOGOFF utilizada pelo FDesk
        Usuario usuario = new Usuario();
        if (usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name))
        {
            usuario.DataLogout = DateTime.Now;
            usuario.Save();
        }

        System.Web.Security.FormsAuthentication.SignOut();

        Context.Session["InfoCotista"] = null;

        string customLogoffURL = ConfigurationManager.AppSettings["CustomLogoffURL"];
        if (!string.IsNullOrEmpty(customLogoffURL))
        {
            Response.Redirect(customLogoffURL);
        }
        else
        {
            bool isTesouraria = HttpContext.Current.Request["isTesouraria"] == "true";
            if (isTesouraria)
            {
                Response.Redirect("~/portalinst.aspx");
            }
            else
            {
                Response.Redirect("~/default.aspx");
            }
        }
    }
}
