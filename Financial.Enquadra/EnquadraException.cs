﻿using System;

namespace Financial.Enquadra.Exceptions
{
    /// <summary>
    /// Classe base de Exceção do componente de Enquadra
    /// </summary>
    public class EnquadraException : Exception
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public EnquadraException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public EnquadraException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public EnquadraException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    /// <summary>
    /// Exceção de EnquadraItemNaoCadastradoException
    /// </summary>
    public class EnquadraItemNaoCadastradoException : EnquadraException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public EnquadraItemNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public EnquadraItemNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de ValorBaseZeradoException
    /// </summary>
    public class ValorBaseZeradoException : EnquadraException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ValorBaseZeradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ValorBaseZeradoException(string mensagem) : base(mensagem) { }
    }
}


