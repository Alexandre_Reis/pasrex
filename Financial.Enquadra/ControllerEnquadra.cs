﻿using log4net;
using Financial.Enquadra.Enums;
using System.Collections.Generic;
using System;
using Financial.Common.Enums;
using System.Text;
using System.Data.SqlClient;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;

namespace Financial.Enquadra.Controller
{
    public class ControllerEnquadra
    {
        /// <summary>
        /// Realiza todos os cálculos de enquadramento da carteira, salvando os resultados ao final.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ExecutaFechamento(int idCarteira, DateTime data)
        {
            //Deleta os resultados para a carteira na data
            EnquadraResultadoDetalheCollection enquadraResultadoDetalheCollection = new EnquadraResultadoDetalheCollection();
            enquadraResultadoDetalheCollection.DeletaEnquadramentoResultadoDetalhe(idCarteira, data);

            EnquadraResultadoCollection enquadraResultadoCollection = new EnquadraResultadoCollection();
            enquadraResultadoCollection.DeletaEnquadramentoResultado(idCarteira, data);
            //

            //Avalia se a posição a ser buscada é a atual ou a histórica, baseado na data dia da carteira
            Cliente cliente = new Cliente();            
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(campos, idCarteira);
            DateTime dataCarteira = cliente.DataDia.Value;
            bool historico = false;
            if (DateTime.Compare(data, dataCarteira) > 0)
                historico = true;
            //

            //Busca as regras de enquadramento vigentes na data
            EnquadraRegraCollection enquadraRegraCollection = new EnquadraRegraCollection();
            enquadraRegraCollection.BuscaEnquadraRegrasVigentes(idCarteira, data);

            for (int i = 0; i < enquadraRegraCollection.Count; i++)
            {
                EnquadraRegra enquadraRegra = enquadraRegraCollection[i];
                int tipoRegra = enquadraRegra.TipoRegra.Value;

                //Processa as regras de enquadramento
                EnquadraRegra enquadraRegraCalculo = new EnquadraRegra();
                switch (tipoRegra)
                {
                    case (int)TipoRegraEnquadra.LimiteAbsoluto:
                        enquadraRegraCalculo.CalculaLimiteAbsoluto(enquadraRegra, idCarteira, historico, data);
                        break;
                    case (int)TipoRegraEnquadra.ConcentracaoCotista:
                        enquadraRegraCalculo.CalculaConcentracaoCotista(enquadraRegra, idCarteira, historico, data);
                        break;
                    case (int)TipoRegraEnquadra.NumeroCotista:
                        enquadraRegraCalculo.CalculaNumeroCotista(enquadraRegra, idCarteira, historico, data);
                        break;
                    case (int)TipoRegraEnquadra.PosicaoDescoberto:
                        enquadraRegraCalculo.CalculaPosicaoDescoberto(enquadraRegra, idCarteira, historico, data);
                        break;
                    case (int)TipoRegraEnquadra.MediaMovel:
                        enquadraRegraCalculo.CalculaMediaMovel(enquadraRegra, idCarteira, historico, data);
                        break;
                    case (int)TipoRegraEnquadra.Concentracao:
                        enquadraRegraCalculo.CalculaConcentracao(enquadraRegra, idCarteira, historico, data);
                        break;
                    case (int)TipoRegraEnquadra.LimiteOscilacao:
                        enquadraRegraCalculo.CalculaLimiteOscilacao(enquadraRegra, idCarteira, historico, data);
                        break;
                    case (int)TipoRegraEnquadra.LimiteDesvioMensal:
                        enquadraRegraCalculo.CalculaLimiteDesvio(enquadraRegra, idCarteira, historico, data, 21);
                        break;
                    case (int)TipoRegraEnquadra.LimiteDesvioAnual:
                        enquadraRegraCalculo.CalculaLimiteDesvio(enquadraRegra, idCarteira, historico, data, 252);
                        break;
                    case (int)TipoRegraEnquadra.ParticipacaoFundo:
                        enquadraRegraCalculo.CalculaParticipacaoFundo(enquadraRegra, idCarteira, historico, data);
                        break;
                }
                
            }
        }

    }
}
