/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 17/03/2014 16:46:09
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




using Financial.Common;
using Financial.Fundo;

				
				








				




		

		
		
		
		
		





namespace Financial.Enquadra
{

	[Serializable]
	abstract public class esEnquadraItemFundoCollection : esEntityCollection
	{
		public esEnquadraItemFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EnquadraItemFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esEnquadraItemFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEnquadraItemFundoQuery);
		}
		#endregion
		
		virtual public EnquadraItemFundo DetachEntity(EnquadraItemFundo entity)
		{
			return base.DetachEntity(entity) as EnquadraItemFundo;
		}
		
		virtual public EnquadraItemFundo AttachEntity(EnquadraItemFundo entity)
		{
			return base.AttachEntity(entity) as EnquadraItemFundo;
		}
		
		virtual public void Combine(EnquadraItemFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EnquadraItemFundo this[int index]
		{
			get
			{
				return base[index] as EnquadraItemFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EnquadraItemFundo);
		}
	}



	[Serializable]
	abstract public class esEnquadraItemFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEnquadraItemFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esEnquadraItemFundo()
		{

		}

		public esEnquadraItemFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idItem)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idItem);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idItem)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEnquadraItemFundoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdItem == idItem);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idItem)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idItem);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idItem)
		{
			esEnquadraItemFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdItem == idItem);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idItem)
		{
			esParameters parms = new esParameters();
			parms.Add("IdItem",idItem);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdItem": this.str.IdItem = (string)value; break;							
						case "TipoCarteira": this.str.TipoCarteira = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdAgenteAdministrador": this.str.IdAgenteAdministrador = (string)value; break;							
						case "IdAgenteGestor": this.str.IdAgenteGestor = (string)value; break;							
						case "IdCategoria": this.str.IdCategoria = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdItem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdItem = (System.Int32?)value;
							break;
						
						case "TipoCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCarteira = (System.Byte?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdAgenteAdministrador":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteAdministrador = (System.Int32?)value;
							break;
						
						case "IdAgenteGestor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteGestor = (System.Int32?)value;
							break;
						
						case "IdCategoria":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCategoria = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EnquadraItemFundo.IdItem
		/// </summary>
		virtual public System.Int32? IdItem
		{
			get
			{
				return base.GetSystemInt32(EnquadraItemFundoMetadata.ColumnNames.IdItem);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraItemFundoMetadata.ColumnNames.IdItem, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemFundo.TipoCarteira
		/// </summary>
		virtual public System.Byte? TipoCarteira
		{
			get
			{
				return base.GetSystemByte(EnquadraItemFundoMetadata.ColumnNames.TipoCarteira);
			}
			
			set
			{
				base.SetSystemByte(EnquadraItemFundoMetadata.ColumnNames.TipoCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemFundo.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(EnquadraItemFundoMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				if(base.SetSystemInt16(EnquadraItemFundoMetadata.ColumnNames.IdIndice, value))
				{
					this._UpToIndiceByIdIndice = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemFundo.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(EnquadraItemFundoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(EnquadraItemFundoMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemFundo.IdAgenteAdministrador
		/// </summary>
		virtual public System.Int32? IdAgenteAdministrador
		{
			get
			{
				return base.GetSystemInt32(EnquadraItemFundoMetadata.ColumnNames.IdAgenteAdministrador);
			}
			
			set
			{
				if(base.SetSystemInt32(EnquadraItemFundoMetadata.ColumnNames.IdAgenteAdministrador, value))
				{
					this._UpToAgenteMercadoByIdAgenteAdministrador = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemFundo.IdAgenteGestor
		/// </summary>
		virtual public System.Int32? IdAgenteGestor
		{
			get
			{
				return base.GetSystemInt32(EnquadraItemFundoMetadata.ColumnNames.IdAgenteGestor);
			}
			
			set
			{
				if(base.SetSystemInt32(EnquadraItemFundoMetadata.ColumnNames.IdAgenteGestor, value))
				{
					this._UpToAgenteMercadoByIdAgenteGestor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemFundo.IdCategoria
		/// </summary>
		virtual public System.Int32? IdCategoria
		{
			get
			{
				return base.GetSystemInt32(EnquadraItemFundoMetadata.ColumnNames.IdCategoria);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraItemFundoMetadata.ColumnNames.IdCategoria, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteAdministrador;
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteGestor;
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndice;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEnquadraItemFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdItem
			{
				get
				{
					System.Int32? data = entity.IdItem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdItem = null;
					else entity.IdItem = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoCarteira
			{
				get
				{
					System.Byte? data = entity.TipoCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCarteira = null;
					else entity.TipoCarteira = Convert.ToByte(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteAdministrador
			{
				get
				{
					System.Int32? data = entity.IdAgenteAdministrador;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteAdministrador = null;
					else entity.IdAgenteAdministrador = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteGestor
			{
				get
				{
					System.Int32? data = entity.IdAgenteGestor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteGestor = null;
					else entity.IdAgenteGestor = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCategoria
			{
				get
				{
					System.Int32? data = entity.IdCategoria;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCategoria = null;
					else entity.IdCategoria = Convert.ToInt32(value);
				}
			}
			

			private esEnquadraItemFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEnquadraItemFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEnquadraItemFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EnquadraItemFundo : esEnquadraItemFundo
	{

				
		#region UpToAgenteMercadoByIdAgenteAdministrador - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_EnquadraItemFundo_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteAdministrador
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteAdministrador == null
					&& IdAgenteAdministrador != null					)
				{
					this._UpToAgenteMercadoByIdAgenteAdministrador = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteAdministrador.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteAdministrador", this._UpToAgenteMercadoByIdAgenteAdministrador);
					this._UpToAgenteMercadoByIdAgenteAdministrador.Query.Where(this._UpToAgenteMercadoByIdAgenteAdministrador.Query.IdAgente == this.IdAgenteAdministrador);
					this._UpToAgenteMercadoByIdAgenteAdministrador.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteAdministrador;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteAdministrador");
				

				if(value == null)
				{
					this.IdAgenteAdministrador = null;
					this._UpToAgenteMercadoByIdAgenteAdministrador = null;
				}
				else
				{
					this.IdAgenteAdministrador = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteAdministrador = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteAdministrador", this._UpToAgenteMercadoByIdAgenteAdministrador);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAgenteMercadoByIdAgenteGestor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_EnquadraItemFundo_FK2
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteGestor
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteGestor == null
					&& IdAgenteGestor != null					)
				{
					this._UpToAgenteMercadoByIdAgenteGestor = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteGestor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteGestor", this._UpToAgenteMercadoByIdAgenteGestor);
					this._UpToAgenteMercadoByIdAgenteGestor.Query.Where(this._UpToAgenteMercadoByIdAgenteGestor.Query.IdAgente == this.IdAgenteGestor);
					this._UpToAgenteMercadoByIdAgenteGestor.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteGestor;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteGestor");
				

				if(value == null)
				{
					this.IdAgenteGestor = null;
					this._UpToAgenteMercadoByIdAgenteGestor = null;
				}
				else
				{
					this.IdAgenteGestor = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteGestor = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteGestor", this._UpToAgenteMercadoByIdAgenteGestor);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_EnquadraItemFundo_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		#region UpToEnquadraItem - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - EnquadraItem_EnquadraItemFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItem UpToEnquadraItem
		{
			get
			{
				if(this._UpToEnquadraItem == null
					&& IdItem != null					)
				{
					this._UpToEnquadraItem = new EnquadraItem();
					this._UpToEnquadraItem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEnquadraItem", this._UpToEnquadraItem);
					this._UpToEnquadraItem.Query.Where(this._UpToEnquadraItem.Query.IdItem == this.IdItem);
					this._UpToEnquadraItem.Query.Load();
				}

				return this._UpToEnquadraItem;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToEnquadraItem");

				if(value == null)
				{
					this._UpToEnquadraItem = null;
				}
				else
				{
					this._UpToEnquadraItem = value;
					this.SetPreSave("UpToEnquadraItem", this._UpToEnquadraItem);
				}
				
				
			} 
		}

		private EnquadraItem _UpToEnquadraItem;
		#endregion

				
		#region UpToIndiceByIdIndice - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Indice_EnquadraItemFundo_FK1
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndice
		{
			get
			{
				if(this._UpToIndiceByIdIndice == null
					&& IdIndice != null					)
				{
					this._UpToIndiceByIdIndice = new Indice();
					this._UpToIndiceByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
					this._UpToIndiceByIdIndice.Query.Where(this._UpToIndiceByIdIndice.Query.IdIndice == this.IdIndice);
					this._UpToIndiceByIdIndice.Query.Load();
				}

				return this._UpToIndiceByIdIndice;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndice");
				

				if(value == null)
				{
					this.IdIndice = null;
					this._UpToIndiceByIdIndice = null;
				}
				else
				{
					this.IdIndice = value.IdIndice;
					this._UpToIndiceByIdIndice = value;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteAdministrador != null)
			{
				this.IdAgenteAdministrador = this._UpToAgenteMercadoByIdAgenteAdministrador.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteGestor != null)
			{
				this.IdAgenteGestor = this._UpToAgenteMercadoByIdAgenteGestor.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToEnquadraItem != null)
			{
				this.IdItem = this._UpToEnquadraItem.IdItem;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEnquadraItemFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraItemFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdItem
		{
			get
			{
				return new esQueryItem(this, EnquadraItemFundoMetadata.ColumnNames.IdItem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoCarteira
		{
			get
			{
				return new esQueryItem(this, EnquadraItemFundoMetadata.ColumnNames.TipoCarteira, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, EnquadraItemFundoMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, EnquadraItemFundoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteAdministrador
		{
			get
			{
				return new esQueryItem(this, EnquadraItemFundoMetadata.ColumnNames.IdAgenteAdministrador, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteGestor
		{
			get
			{
				return new esQueryItem(this, EnquadraItemFundoMetadata.ColumnNames.IdAgenteGestor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCategoria
		{
			get
			{
				return new esQueryItem(this, EnquadraItemFundoMetadata.ColumnNames.IdCategoria, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EnquadraItemFundoCollection")]
	public partial class EnquadraItemFundoCollection : esEnquadraItemFundoCollection, IEnumerable<EnquadraItemFundo>
	{
		public EnquadraItemFundoCollection()
		{

		}
		
		public static implicit operator List<EnquadraItemFundo>(EnquadraItemFundoCollection coll)
		{
			List<EnquadraItemFundo> list = new List<EnquadraItemFundo>();
			
			foreach (EnquadraItemFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EnquadraItemFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraItemFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EnquadraItemFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EnquadraItemFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EnquadraItemFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraItemFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EnquadraItemFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EnquadraItemFundo AddNew()
		{
			EnquadraItemFundo entity = base.AddNewEntity() as EnquadraItemFundo;
			
			return entity;
		}

		public EnquadraItemFundo FindByPrimaryKey(System.Int32 idItem)
		{
			return base.FindByPrimaryKey(idItem) as EnquadraItemFundo;
		}


		#region IEnumerable<EnquadraItemFundo> Members

		IEnumerator<EnquadraItemFundo> IEnumerable<EnquadraItemFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EnquadraItemFundo;
			}
		}

		#endregion
		
		private EnquadraItemFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EnquadraItemFundo' table
	/// </summary>

	[Serializable]
	public partial class EnquadraItemFundo : esEnquadraItemFundo
	{
		public EnquadraItemFundo()
		{

		}
	
		public EnquadraItemFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraItemFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esEnquadraItemFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraItemFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EnquadraItemFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraItemFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EnquadraItemFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EnquadraItemFundoQuery query;
	}



	[Serializable]
	public partial class EnquadraItemFundoQuery : esEnquadraItemFundoQuery
	{
		public EnquadraItemFundoQuery()
		{

		}		
		
		public EnquadraItemFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EnquadraItemFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EnquadraItemFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EnquadraItemFundoMetadata.ColumnNames.IdItem, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraItemFundoMetadata.PropertyNames.IdItem;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemFundoMetadata.ColumnNames.TipoCarteira, 1, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EnquadraItemFundoMetadata.PropertyNames.TipoCarteira;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemFundoMetadata.ColumnNames.IdIndice, 2, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = EnquadraItemFundoMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemFundoMetadata.ColumnNames.IdCarteira, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraItemFundoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemFundoMetadata.ColumnNames.IdAgenteAdministrador, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraItemFundoMetadata.PropertyNames.IdAgenteAdministrador;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemFundoMetadata.ColumnNames.IdAgenteGestor, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraItemFundoMetadata.PropertyNames.IdAgenteGestor;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemFundoMetadata.ColumnNames.IdCategoria, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraItemFundoMetadata.PropertyNames.IdCategoria;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EnquadraItemFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdItem = "IdItem";
			 public const string TipoCarteira = "TipoCarteira";
			 public const string IdIndice = "IdIndice";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdAgenteAdministrador = "IdAgenteAdministrador";
			 public const string IdAgenteGestor = "IdAgenteGestor";
			 public const string IdCategoria = "IdCategoria";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdItem = "IdItem";
			 public const string TipoCarteira = "TipoCarteira";
			 public const string IdIndice = "IdIndice";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdAgenteAdministrador = "IdAgenteAdministrador";
			 public const string IdAgenteGestor = "IdAgenteGestor";
			 public const string IdCategoria = "IdCategoria";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EnquadraItemFundoMetadata))
			{
				if(EnquadraItemFundoMetadata.mapDelegates == null)
				{
					EnquadraItemFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EnquadraItemFundoMetadata.meta == null)
				{
					EnquadraItemFundoMetadata.meta = new EnquadraItemFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdItem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoCarteira", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteAdministrador", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteGestor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCategoria", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "EnquadraItemFundo";
				meta.Destination = "EnquadraItemFundo";
				
				meta.spInsert = "proc_EnquadraItemFundoInsert";				
				meta.spUpdate = "proc_EnquadraItemFundoUpdate";		
				meta.spDelete = "proc_EnquadraItemFundoDelete";
				meta.spLoadAll = "proc_EnquadraItemFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_EnquadraItemFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EnquadraItemFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
