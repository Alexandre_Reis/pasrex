/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 17/03/2014 16:46:08
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Enquadra
{

	[Serializable]
	abstract public class esEnquadraGrupoItemCollection : esEntityCollection
	{
		public esEnquadraGrupoItemCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EnquadraGrupoItemCollection";
		}

		#region Query Logic
		protected void InitQuery(esEnquadraGrupoItemQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEnquadraGrupoItemQuery);
		}
		#endregion
		
		virtual public EnquadraGrupoItem DetachEntity(EnquadraGrupoItem entity)
		{
			return base.DetachEntity(entity) as EnquadraGrupoItem;
		}
		
		virtual public EnquadraGrupoItem AttachEntity(EnquadraGrupoItem entity)
		{
			return base.AttachEntity(entity) as EnquadraGrupoItem;
		}
		
		virtual public void Combine(EnquadraGrupoItemCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EnquadraGrupoItem this[int index]
		{
			get
			{
				return base[index] as EnquadraGrupoItem;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EnquadraGrupoItem);
		}
	}



	[Serializable]
	abstract public class esEnquadraGrupoItem : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEnquadraGrupoItemQuery GetDynamicQuery()
		{
			return null;
		}

		public esEnquadraGrupoItem()
		{

		}

		public esEnquadraGrupoItem(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idGrupo, System.Int32 idItem)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupo, idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupo, idItem);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idGrupo, System.Int32 idItem)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEnquadraGrupoItemQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdGrupo == idGrupo, query.IdItem == idItem);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idGrupo, System.Int32 idItem)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupo, idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupo, idItem);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idGrupo, System.Int32 idItem)
		{
			esEnquadraGrupoItemQuery query = this.GetDynamicQuery();
			query.Where(query.IdGrupo == idGrupo, query.IdItem == idItem);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idGrupo, System.Int32 idItem)
		{
			esParameters parms = new esParameters();
			parms.Add("IdGrupo",idGrupo);			parms.Add("IdItem",idItem);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdGrupo": this.str.IdGrupo = (string)value; break;							
						case "IdItem": this.str.IdItem = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupo = (System.Int32?)value;
							break;
						
						case "IdItem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdItem = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EnquadraGrupoItem.IdGrupo
		/// </summary>
		virtual public System.Int32? IdGrupo
		{
			get
			{
				return base.GetSystemInt32(EnquadraGrupoItemMetadata.ColumnNames.IdGrupo);
			}
			
			set
			{
				if(base.SetSystemInt32(EnquadraGrupoItemMetadata.ColumnNames.IdGrupo, value))
				{
					this._UpToEnquadraGrupoByIdGrupo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraGrupoItem.IdItem
		/// </summary>
		virtual public System.Int32? IdItem
		{
			get
			{
				return base.GetSystemInt32(EnquadraGrupoItemMetadata.ColumnNames.IdItem);
			}
			
			set
			{
				if(base.SetSystemInt32(EnquadraGrupoItemMetadata.ColumnNames.IdItem, value))
				{
					this._UpToEnquadraItemByIdItem = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected EnquadraGrupo _UpToEnquadraGrupoByIdGrupo;
		[CLSCompliant(false)]
		internal protected EnquadraItem _UpToEnquadraItemByIdItem;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEnquadraGrupoItem entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdGrupo
			{
				get
				{
					System.Int32? data = entity.IdGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupo = null;
					else entity.IdGrupo = Convert.ToInt32(value);
				}
			}
				
			public System.String IdItem
			{
				get
				{
					System.Int32? data = entity.IdItem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdItem = null;
					else entity.IdItem = Convert.ToInt32(value);
				}
			}
			

			private esEnquadraGrupoItem entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEnquadraGrupoItemQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEnquadraGrupoItem can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EnquadraGrupoItem : esEnquadraGrupoItem
	{

				
		#region UpToEnquadraGrupoByIdGrupo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EnquadraGrupo_EnquadraGrupoItem_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraGrupo UpToEnquadraGrupoByIdGrupo
		{
			get
			{
				if(this._UpToEnquadraGrupoByIdGrupo == null
					&& IdGrupo != null					)
				{
					this._UpToEnquadraGrupoByIdGrupo = new EnquadraGrupo();
					this._UpToEnquadraGrupoByIdGrupo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEnquadraGrupoByIdGrupo", this._UpToEnquadraGrupoByIdGrupo);
					this._UpToEnquadraGrupoByIdGrupo.Query.Where(this._UpToEnquadraGrupoByIdGrupo.Query.IdGrupo == this.IdGrupo);
					this._UpToEnquadraGrupoByIdGrupo.Query.Load();
				}

				return this._UpToEnquadraGrupoByIdGrupo;
			}
			
			set
			{
				this.RemovePreSave("UpToEnquadraGrupoByIdGrupo");
				

				if(value == null)
				{
					this.IdGrupo = null;
					this._UpToEnquadraGrupoByIdGrupo = null;
				}
				else
				{
					this.IdGrupo = value.IdGrupo;
					this._UpToEnquadraGrupoByIdGrupo = value;
					this.SetPreSave("UpToEnquadraGrupoByIdGrupo", this._UpToEnquadraGrupoByIdGrupo);
				}
				
			}
		}
		#endregion
		

				
		#region UpToEnquadraItemByIdItem - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EnquadraItem_EnquadraGrupoItem_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItem UpToEnquadraItemByIdItem
		{
			get
			{
				if(this._UpToEnquadraItemByIdItem == null
					&& IdItem != null					)
				{
					this._UpToEnquadraItemByIdItem = new EnquadraItem();
					this._UpToEnquadraItemByIdItem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEnquadraItemByIdItem", this._UpToEnquadraItemByIdItem);
					this._UpToEnquadraItemByIdItem.Query.Where(this._UpToEnquadraItemByIdItem.Query.IdItem == this.IdItem);
					this._UpToEnquadraItemByIdItem.Query.Load();
				}

				return this._UpToEnquadraItemByIdItem;
			}
			
			set
			{
				this.RemovePreSave("UpToEnquadraItemByIdItem");
				

				if(value == null)
				{
					this.IdItem = null;
					this._UpToEnquadraItemByIdItem = null;
				}
				else
				{
					this.IdItem = value.IdItem;
					this._UpToEnquadraItemByIdItem = value;
					this.SetPreSave("UpToEnquadraItemByIdItem", this._UpToEnquadraItemByIdItem);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEnquadraGrupoByIdGrupo != null)
			{
				this.IdGrupo = this._UpToEnquadraGrupoByIdGrupo.IdGrupo;
			}
			if(!this.es.IsDeleted && this._UpToEnquadraItemByIdItem != null)
			{
				this.IdItem = this._UpToEnquadraItemByIdItem.IdItem;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEnquadraGrupoItemQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraGrupoItemMetadata.Meta();
			}
		}	
		

		public esQueryItem IdGrupo
		{
			get
			{
				return new esQueryItem(this, EnquadraGrupoItemMetadata.ColumnNames.IdGrupo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdItem
		{
			get
			{
				return new esQueryItem(this, EnquadraGrupoItemMetadata.ColumnNames.IdItem, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EnquadraGrupoItemCollection")]
	public partial class EnquadraGrupoItemCollection : esEnquadraGrupoItemCollection, IEnumerable<EnquadraGrupoItem>
	{
		public EnquadraGrupoItemCollection()
		{

		}
		
		public static implicit operator List<EnquadraGrupoItem>(EnquadraGrupoItemCollection coll)
		{
			List<EnquadraGrupoItem> list = new List<EnquadraGrupoItem>();
			
			foreach (EnquadraGrupoItem emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EnquadraGrupoItemMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraGrupoItemQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EnquadraGrupoItem(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EnquadraGrupoItem();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EnquadraGrupoItemQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraGrupoItemQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EnquadraGrupoItemQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EnquadraGrupoItem AddNew()
		{
			EnquadraGrupoItem entity = base.AddNewEntity() as EnquadraGrupoItem;
			
			return entity;
		}

		public EnquadraGrupoItem FindByPrimaryKey(System.Int32 idGrupo, System.Int32 idItem)
		{
			return base.FindByPrimaryKey(idGrupo, idItem) as EnquadraGrupoItem;
		}


		#region IEnumerable<EnquadraGrupoItem> Members

		IEnumerator<EnquadraGrupoItem> IEnumerable<EnquadraGrupoItem>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EnquadraGrupoItem;
			}
		}

		#endregion
		
		private EnquadraGrupoItemQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EnquadraGrupoItem' table
	/// </summary>

	[Serializable]
	public partial class EnquadraGrupoItem : esEnquadraGrupoItem
	{
		public EnquadraGrupoItem()
		{

		}
	
		public EnquadraGrupoItem(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraGrupoItemMetadata.Meta();
			}
		}
		
		
		
		override protected esEnquadraGrupoItemQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraGrupoItemQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EnquadraGrupoItemQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraGrupoItemQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EnquadraGrupoItemQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EnquadraGrupoItemQuery query;
	}



	[Serializable]
	public partial class EnquadraGrupoItemQuery : esEnquadraGrupoItemQuery
	{
		public EnquadraGrupoItemQuery()
		{

		}		
		
		public EnquadraGrupoItemQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EnquadraGrupoItemMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EnquadraGrupoItemMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EnquadraGrupoItemMetadata.ColumnNames.IdGrupo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraGrupoItemMetadata.PropertyNames.IdGrupo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraGrupoItemMetadata.ColumnNames.IdItem, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraGrupoItemMetadata.PropertyNames.IdItem;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EnquadraGrupoItemMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdGrupo = "IdGrupo";
			 public const string IdItem = "IdItem";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdGrupo = "IdGrupo";
			 public const string IdItem = "IdItem";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EnquadraGrupoItemMetadata))
			{
				if(EnquadraGrupoItemMetadata.mapDelegates == null)
				{
					EnquadraGrupoItemMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EnquadraGrupoItemMetadata.meta == null)
				{
					EnquadraGrupoItemMetadata.meta = new EnquadraGrupoItemMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdGrupo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdItem", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "EnquadraGrupoItem";
				meta.Destination = "EnquadraGrupoItem";
				
				meta.spInsert = "proc_EnquadraGrupoItemInsert";				
				meta.spUpdate = "proc_EnquadraGrupoItemUpdate";		
				meta.spDelete = "proc_EnquadraGrupoItemDelete";
				meta.spLoadAll = "proc_EnquadraGrupoItemLoadAll";
				meta.spLoadByPrimaryKey = "proc_EnquadraGrupoItemLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EnquadraGrupoItemMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
