/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 17/03/2014 16:46:09
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				


using Financial.Fundo;







				




		

		
		
		
		
		





namespace Financial.Enquadra
{

	[Serializable]
	abstract public class esEnquadraRegraCollection : esEntityCollection
	{
		public esEnquadraRegraCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EnquadraRegraCollection";
		}

		#region Query Logic
		protected void InitQuery(esEnquadraRegraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEnquadraRegraQuery);
		}
		#endregion
		
		virtual public EnquadraRegra DetachEntity(EnquadraRegra entity)
		{
			return base.DetachEntity(entity) as EnquadraRegra;
		}
		
		virtual public EnquadraRegra AttachEntity(EnquadraRegra entity)
		{
			return base.AttachEntity(entity) as EnquadraRegra;
		}
		
		virtual public void Combine(EnquadraRegraCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EnquadraRegra this[int index]
		{
			get
			{
				return base[index] as EnquadraRegra;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EnquadraRegra);
		}
	}



	[Serializable]
	abstract public class esEnquadraRegra : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEnquadraRegraQuery GetDynamicQuery()
		{
			return null;
		}

		public esEnquadraRegra()
		{

		}

		public esEnquadraRegra(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idRegra)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idRegra);
			else
				return LoadByPrimaryKeyStoredProcedure(idRegra);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idRegra)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEnquadraRegraQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdRegra == idRegra);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idRegra)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idRegra);
			else
				return LoadByPrimaryKeyStoredProcedure(idRegra);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idRegra)
		{
			esEnquadraRegraQuery query = this.GetDynamicQuery();
			query.Where(query.IdRegra == idRegra);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idRegra)
		{
			esParameters parms = new esParameters();
			parms.Add("IdRegra",idRegra);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdRegra": this.str.IdRegra = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "DataFim": this.str.DataFim = (string)value; break;							
						case "TipoEnquadramento": this.str.TipoEnquadramento = (string)value; break;							
						case "IdGrupoBase": this.str.IdGrupoBase = (string)value; break;							
						case "IdGrupoCriterio": this.str.IdGrupoCriterio = (string)value; break;							
						case "TipoRegra": this.str.TipoRegra = (string)value; break;							
						case "SubTipoRegra": this.str.SubTipoRegra = (string)value; break;							
						case "Minimo": this.str.Minimo = (string)value; break;							
						case "Maximo": this.str.Maximo = (string)value; break;							
						case "RealTime": this.str.RealTime = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdRegra":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdRegra = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "DataFim":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFim = (System.DateTime?)value;
							break;
						
						case "TipoEnquadramento":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoEnquadramento = (System.Byte?)value;
							break;
						
						case "IdGrupoBase":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupoBase = (System.Int32?)value;
							break;
						
						case "IdGrupoCriterio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupoCriterio = (System.Int32?)value;
							break;
						
						case "TipoRegra":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoRegra = (System.Int32?)value;
							break;
						
						case "SubTipoRegra":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.SubTipoRegra = (System.Int32?)value;
							break;
						
						case "Minimo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Minimo = (System.Decimal?)value;
							break;
						
						case "Maximo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Maximo = (System.Decimal?)value;
							break;
						
						case "RealTime":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.RealTime = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EnquadraRegra.IdRegra
		/// </summary>
		virtual public System.Int32? IdRegra
		{
			get
			{
				return base.GetSystemInt32(EnquadraRegraMetadata.ColumnNames.IdRegra);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraRegraMetadata.ColumnNames.IdRegra, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraRegra.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(EnquadraRegraMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(EnquadraRegraMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraRegra.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(EnquadraRegraMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(EnquadraRegraMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraRegra.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(EnquadraRegraMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(EnquadraRegraMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraRegra.DataFim
		/// </summary>
		virtual public System.DateTime? DataFim
		{
			get
			{
				return base.GetSystemDateTime(EnquadraRegraMetadata.ColumnNames.DataFim);
			}
			
			set
			{
				base.SetSystemDateTime(EnquadraRegraMetadata.ColumnNames.DataFim, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraRegra.TipoEnquadramento
		/// </summary>
		virtual public System.Byte? TipoEnquadramento
		{
			get
			{
				return base.GetSystemByte(EnquadraRegraMetadata.ColumnNames.TipoEnquadramento);
			}
			
			set
			{
				base.SetSystemByte(EnquadraRegraMetadata.ColumnNames.TipoEnquadramento, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraRegra.IdGrupoBase
		/// </summary>
		virtual public System.Int32? IdGrupoBase
		{
			get
			{
				return base.GetSystemInt32(EnquadraRegraMetadata.ColumnNames.IdGrupoBase);
			}
			
			set
			{
				if(base.SetSystemInt32(EnquadraRegraMetadata.ColumnNames.IdGrupoBase, value))
				{
					this._UpToEnquadraGrupoByIdGrupoBase = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraRegra.IdGrupoCriterio
		/// </summary>
		virtual public System.Int32? IdGrupoCriterio
		{
			get
			{
				return base.GetSystemInt32(EnquadraRegraMetadata.ColumnNames.IdGrupoCriterio);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraRegraMetadata.ColumnNames.IdGrupoCriterio, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraRegra.TipoRegra
		/// </summary>
		virtual public System.Int32? TipoRegra
		{
			get
			{
				return base.GetSystemInt32(EnquadraRegraMetadata.ColumnNames.TipoRegra);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraRegraMetadata.ColumnNames.TipoRegra, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraRegra.SubTipoRegra
		/// </summary>
		virtual public System.Int32? SubTipoRegra
		{
			get
			{
				return base.GetSystemInt32(EnquadraRegraMetadata.ColumnNames.SubTipoRegra);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraRegraMetadata.ColumnNames.SubTipoRegra, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraRegra.Minimo
		/// </summary>
		virtual public System.Decimal? Minimo
		{
			get
			{
				return base.GetSystemDecimal(EnquadraRegraMetadata.ColumnNames.Minimo);
			}
			
			set
			{
				base.SetSystemDecimal(EnquadraRegraMetadata.ColumnNames.Minimo, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraRegra.Maximo
		/// </summary>
		virtual public System.Decimal? Maximo
		{
			get
			{
				return base.GetSystemDecimal(EnquadraRegraMetadata.ColumnNames.Maximo);
			}
			
			set
			{
				base.SetSystemDecimal(EnquadraRegraMetadata.ColumnNames.Maximo, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraRegra.RealTime
		/// </summary>
		virtual public System.Byte? RealTime
		{
			get
			{
				return base.GetSystemByte(EnquadraRegraMetadata.ColumnNames.RealTime);
			}
			
			set
			{
				base.SetSystemByte(EnquadraRegraMetadata.ColumnNames.RealTime, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected EnquadraGrupo _UpToEnquadraGrupoByIdGrupoBase;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEnquadraRegra entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdRegra
			{
				get
				{
					System.Int32? data = entity.IdRegra;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdRegra = null;
					else entity.IdRegra = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataFim
			{
				get
				{
					System.DateTime? data = entity.DataFim;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFim = null;
					else entity.DataFim = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoEnquadramento
			{
				get
				{
					System.Byte? data = entity.TipoEnquadramento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoEnquadramento = null;
					else entity.TipoEnquadramento = Convert.ToByte(value);
				}
			}
				
			public System.String IdGrupoBase
			{
				get
				{
					System.Int32? data = entity.IdGrupoBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupoBase = null;
					else entity.IdGrupoBase = Convert.ToInt32(value);
				}
			}
				
			public System.String IdGrupoCriterio
			{
				get
				{
					System.Int32? data = entity.IdGrupoCriterio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupoCriterio = null;
					else entity.IdGrupoCriterio = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoRegra
			{
				get
				{
					System.Int32? data = entity.TipoRegra;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoRegra = null;
					else entity.TipoRegra = Convert.ToInt32(value);
				}
			}
				
			public System.String SubTipoRegra
			{
				get
				{
					System.Int32? data = entity.SubTipoRegra;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SubTipoRegra = null;
					else entity.SubTipoRegra = Convert.ToInt32(value);
				}
			}
				
			public System.String Minimo
			{
				get
				{
					System.Decimal? data = entity.Minimo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Minimo = null;
					else entity.Minimo = Convert.ToDecimal(value);
				}
			}
				
			public System.String Maximo
			{
				get
				{
					System.Decimal? data = entity.Maximo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Maximo = null;
					else entity.Maximo = Convert.ToDecimal(value);
				}
			}
				
			public System.String RealTime
			{
				get
				{
					System.Byte? data = entity.RealTime;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RealTime = null;
					else entity.RealTime = Convert.ToByte(value);
				}
			}
			

			private esEnquadraRegra entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEnquadraRegraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEnquadraRegra can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EnquadraRegra : esEnquadraRegra
	{

				
		#region EnquadraResultadoCollectionByIdRegra - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EnquadraRegra_EnquadraResultado_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraResultadoCollection EnquadraResultadoCollectionByIdRegra
		{
			get
			{
				if(this._EnquadraResultadoCollectionByIdRegra == null)
				{
					this._EnquadraResultadoCollectionByIdRegra = new EnquadraResultadoCollection();
					this._EnquadraResultadoCollectionByIdRegra.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraResultadoCollectionByIdRegra", this._EnquadraResultadoCollectionByIdRegra);
				
					if(this.IdRegra != null)
					{
						this._EnquadraResultadoCollectionByIdRegra.Query.Where(this._EnquadraResultadoCollectionByIdRegra.Query.IdRegra == this.IdRegra);
						this._EnquadraResultadoCollectionByIdRegra.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraResultadoCollectionByIdRegra.fks.Add(EnquadraResultadoMetadata.ColumnNames.IdRegra, this.IdRegra);
					}
				}

				return this._EnquadraResultadoCollectionByIdRegra;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraResultadoCollectionByIdRegra != null) 
				{ 
					this.RemovePostSave("EnquadraResultadoCollectionByIdRegra"); 
					this._EnquadraResultadoCollectionByIdRegra = null;
					
				} 
			} 			
		}

		private EnquadraResultadoCollection _EnquadraResultadoCollectionByIdRegra;
		#endregion

				
		#region EnquadraResultadoDetalheCollectionByIdRegra - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EnquadraRegra_EnquadraResultadoDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraResultadoDetalheCollection EnquadraResultadoDetalheCollectionByIdRegra
		{
			get
			{
				if(this._EnquadraResultadoDetalheCollectionByIdRegra == null)
				{
					this._EnquadraResultadoDetalheCollectionByIdRegra = new EnquadraResultadoDetalheCollection();
					this._EnquadraResultadoDetalheCollectionByIdRegra.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraResultadoDetalheCollectionByIdRegra", this._EnquadraResultadoDetalheCollectionByIdRegra);
				
					if(this.IdRegra != null)
					{
						this._EnquadraResultadoDetalheCollectionByIdRegra.Query.Where(this._EnquadraResultadoDetalheCollectionByIdRegra.Query.IdRegra == this.IdRegra);
						this._EnquadraResultadoDetalheCollectionByIdRegra.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraResultadoDetalheCollectionByIdRegra.fks.Add(EnquadraResultadoDetalheMetadata.ColumnNames.IdRegra, this.IdRegra);
					}
				}

				return this._EnquadraResultadoDetalheCollectionByIdRegra;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraResultadoDetalheCollectionByIdRegra != null) 
				{ 
					this.RemovePostSave("EnquadraResultadoDetalheCollectionByIdRegra"); 
					this._EnquadraResultadoDetalheCollectionByIdRegra = null;
					
				} 
			} 			
		}

		private EnquadraResultadoDetalheCollection _EnquadraResultadoDetalheCollectionByIdRegra;
		#endregion

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_EnquadraRegra_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToEnquadraGrupoByIdGrupoBase - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EnquadraGrupo_EnquadraRegra_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraGrupo UpToEnquadraGrupoByIdGrupoBase
		{
			get
			{
				if(this._UpToEnquadraGrupoByIdGrupoBase == null
					&& IdGrupoBase != null					)
				{
					this._UpToEnquadraGrupoByIdGrupoBase = new EnquadraGrupo();
					this._UpToEnquadraGrupoByIdGrupoBase.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEnquadraGrupoByIdGrupoBase", this._UpToEnquadraGrupoByIdGrupoBase);
					this._UpToEnquadraGrupoByIdGrupoBase.Query.Where(this._UpToEnquadraGrupoByIdGrupoBase.Query.IdGrupo == this.IdGrupoBase);
					this._UpToEnquadraGrupoByIdGrupoBase.Query.Load();
				}

				return this._UpToEnquadraGrupoByIdGrupoBase;
			}
			
			set
			{
				this.RemovePreSave("UpToEnquadraGrupoByIdGrupoBase");
				

				if(value == null)
				{
					this.IdGrupoBase = null;
					this._UpToEnquadraGrupoByIdGrupoBase = null;
				}
				else
				{
					this.IdGrupoBase = value.IdGrupo;
					this._UpToEnquadraGrupoByIdGrupoBase = value;
					this.SetPreSave("UpToEnquadraGrupoByIdGrupoBase", this._UpToEnquadraGrupoByIdGrupoBase);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "EnquadraResultadoCollectionByIdRegra", typeof(EnquadraResultadoCollection), new EnquadraResultado()));
			props.Add(new esPropertyDescriptor(this, "EnquadraResultadoDetalheCollectionByIdRegra", typeof(EnquadraResultadoDetalheCollection), new EnquadraResultadoDetalhe()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEnquadraGrupoByIdGrupoBase != null)
			{
				this.IdGrupoBase = this._UpToEnquadraGrupoByIdGrupoBase.IdGrupo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._EnquadraResultadoCollectionByIdRegra != null)
			{
				foreach(EnquadraResultado obj in this._EnquadraResultadoCollectionByIdRegra)
				{
					if(obj.es.IsAdded)
					{
						obj.IdRegra = this.IdRegra;
					}
				}
			}
			if(this._EnquadraResultadoDetalheCollectionByIdRegra != null)
			{
				foreach(EnquadraResultadoDetalhe obj in this._EnquadraResultadoDetalheCollectionByIdRegra)
				{
					if(obj.es.IsAdded)
					{
						obj.IdRegra = this.IdRegra;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEnquadraRegraQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraRegraMetadata.Meta();
			}
		}	
		

		public esQueryItem IdRegra
		{
			get
			{
				return new esQueryItem(this, EnquadraRegraMetadata.ColumnNames.IdRegra, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, EnquadraRegraMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, EnquadraRegraMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, EnquadraRegraMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataFim
		{
			get
			{
				return new esQueryItem(this, EnquadraRegraMetadata.ColumnNames.DataFim, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoEnquadramento
		{
			get
			{
				return new esQueryItem(this, EnquadraRegraMetadata.ColumnNames.TipoEnquadramento, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdGrupoBase
		{
			get
			{
				return new esQueryItem(this, EnquadraRegraMetadata.ColumnNames.IdGrupoBase, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdGrupoCriterio
		{
			get
			{
				return new esQueryItem(this, EnquadraRegraMetadata.ColumnNames.IdGrupoCriterio, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoRegra
		{
			get
			{
				return new esQueryItem(this, EnquadraRegraMetadata.ColumnNames.TipoRegra, esSystemType.Int32);
			}
		} 
		
		public esQueryItem SubTipoRegra
		{
			get
			{
				return new esQueryItem(this, EnquadraRegraMetadata.ColumnNames.SubTipoRegra, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Minimo
		{
			get
			{
				return new esQueryItem(this, EnquadraRegraMetadata.ColumnNames.Minimo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Maximo
		{
			get
			{
				return new esQueryItem(this, EnquadraRegraMetadata.ColumnNames.Maximo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RealTime
		{
			get
			{
				return new esQueryItem(this, EnquadraRegraMetadata.ColumnNames.RealTime, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EnquadraRegraCollection")]
	public partial class EnquadraRegraCollection : esEnquadraRegraCollection, IEnumerable<EnquadraRegra>
	{
		public EnquadraRegraCollection()
		{

		}
		
		public static implicit operator List<EnquadraRegra>(EnquadraRegraCollection coll)
		{
			List<EnquadraRegra> list = new List<EnquadraRegra>();
			
			foreach (EnquadraRegra emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EnquadraRegraMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraRegraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EnquadraRegra(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EnquadraRegra();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EnquadraRegraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraRegraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EnquadraRegraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EnquadraRegra AddNew()
		{
			EnquadraRegra entity = base.AddNewEntity() as EnquadraRegra;
			
			return entity;
		}

		public EnquadraRegra FindByPrimaryKey(System.Int32 idRegra)
		{
			return base.FindByPrimaryKey(idRegra) as EnquadraRegra;
		}


		#region IEnumerable<EnquadraRegra> Members

		IEnumerator<EnquadraRegra> IEnumerable<EnquadraRegra>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EnquadraRegra;
			}
		}

		#endregion
		
		private EnquadraRegraQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EnquadraRegra' table
	/// </summary>

	[Serializable]
	public partial class EnquadraRegra : esEnquadraRegra
	{
		public EnquadraRegra()
		{

		}
	
		public EnquadraRegra(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraRegraMetadata.Meta();
			}
		}
		
		
		
		override protected esEnquadraRegraQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraRegraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EnquadraRegraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraRegraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EnquadraRegraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EnquadraRegraQuery query;
	}



	[Serializable]
	public partial class EnquadraRegraQuery : esEnquadraRegraQuery
	{
		public EnquadraRegraQuery()
		{

		}		
		
		public EnquadraRegraQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EnquadraRegraMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EnquadraRegraMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EnquadraRegraMetadata.ColumnNames.IdRegra, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraRegraMetadata.PropertyNames.IdRegra;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraRegraMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = EnquadraRegraMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraRegraMetadata.ColumnNames.IdCarteira, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraRegraMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraRegraMetadata.ColumnNames.DataReferencia, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EnquadraRegraMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraRegraMetadata.ColumnNames.DataFim, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EnquadraRegraMetadata.PropertyNames.DataFim;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraRegraMetadata.ColumnNames.TipoEnquadramento, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EnquadraRegraMetadata.PropertyNames.TipoEnquadramento;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraRegraMetadata.ColumnNames.IdGrupoBase, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraRegraMetadata.PropertyNames.IdGrupoBase;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraRegraMetadata.ColumnNames.IdGrupoCriterio, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraRegraMetadata.PropertyNames.IdGrupoCriterio;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraRegraMetadata.ColumnNames.TipoRegra, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraRegraMetadata.PropertyNames.TipoRegra;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraRegraMetadata.ColumnNames.SubTipoRegra, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraRegraMetadata.PropertyNames.SubTipoRegra;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraRegraMetadata.ColumnNames.Minimo, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EnquadraRegraMetadata.PropertyNames.Minimo;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraRegraMetadata.ColumnNames.Maximo, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EnquadraRegraMetadata.PropertyNames.Maximo;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraRegraMetadata.ColumnNames.RealTime, 12, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EnquadraRegraMetadata.PropertyNames.RealTime;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EnquadraRegraMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdRegra = "IdRegra";
			 public const string Descricao = "Descricao";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataReferencia = "DataReferencia";
			 public const string DataFim = "DataFim";
			 public const string TipoEnquadramento = "TipoEnquadramento";
			 public const string IdGrupoBase = "IdGrupoBase";
			 public const string IdGrupoCriterio = "IdGrupoCriterio";
			 public const string TipoRegra = "TipoRegra";
			 public const string SubTipoRegra = "SubTipoRegra";
			 public const string Minimo = "Minimo";
			 public const string Maximo = "Maximo";
			 public const string RealTime = "RealTime";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdRegra = "IdRegra";
			 public const string Descricao = "Descricao";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataReferencia = "DataReferencia";
			 public const string DataFim = "DataFim";
			 public const string TipoEnquadramento = "TipoEnquadramento";
			 public const string IdGrupoBase = "IdGrupoBase";
			 public const string IdGrupoCriterio = "IdGrupoCriterio";
			 public const string TipoRegra = "TipoRegra";
			 public const string SubTipoRegra = "SubTipoRegra";
			 public const string Minimo = "Minimo";
			 public const string Maximo = "Maximo";
			 public const string RealTime = "RealTime";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EnquadraRegraMetadata))
			{
				if(EnquadraRegraMetadata.mapDelegates == null)
				{
					EnquadraRegraMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EnquadraRegraMetadata.meta == null)
				{
					EnquadraRegraMetadata.meta = new EnquadraRegraMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdRegra", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataFim", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoEnquadramento", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdGrupoBase", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdGrupoCriterio", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoRegra", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("SubTipoRegra", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Minimo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Maximo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RealTime", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "EnquadraRegra";
				meta.Destination = "EnquadraRegra";
				
				meta.spInsert = "proc_EnquadraRegraInsert";				
				meta.spUpdate = "proc_EnquadraRegraUpdate";		
				meta.spDelete = "proc_EnquadraRegraDelete";
				meta.spLoadAll = "proc_EnquadraRegraLoadAll";
				meta.spLoadByPrimaryKey = "proc_EnquadraRegraLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EnquadraRegraMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
