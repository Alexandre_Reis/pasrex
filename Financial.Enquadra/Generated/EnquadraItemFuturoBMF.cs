/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 17/03/2014 16:46:09
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		





using Financial.BMF;
				
				








				




		

		
		
		
		
		





namespace Financial.Enquadra
{

	[Serializable]
	abstract public class esEnquadraItemFuturoBMFCollection : esEntityCollection
	{
		public esEnquadraItemFuturoBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EnquadraItemFuturoBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esEnquadraItemFuturoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEnquadraItemFuturoBMFQuery);
		}
		#endregion
		
		virtual public EnquadraItemFuturoBMF DetachEntity(EnquadraItemFuturoBMF entity)
		{
			return base.DetachEntity(entity) as EnquadraItemFuturoBMF;
		}
		
		virtual public EnquadraItemFuturoBMF AttachEntity(EnquadraItemFuturoBMF entity)
		{
			return base.AttachEntity(entity) as EnquadraItemFuturoBMF;
		}
		
		virtual public void Combine(EnquadraItemFuturoBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EnquadraItemFuturoBMF this[int index]
		{
			get
			{
				return base[index] as EnquadraItemFuturoBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EnquadraItemFuturoBMF);
		}
	}



	[Serializable]
	abstract public class esEnquadraItemFuturoBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEnquadraItemFuturoBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esEnquadraItemFuturoBMF()
		{

		}

		public esEnquadraItemFuturoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idItem)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idItem);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idItem)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEnquadraItemFuturoBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdItem == idItem);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idItem)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idItem);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idItem)
		{
			esEnquadraItemFuturoBMFQuery query = this.GetDynamicQuery();
			query.Where(query.IdItem == idItem);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idItem)
		{
			esParameters parms = new esParameters();
			parms.Add("IdItem",idItem);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdItem": this.str.IdItem = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Posicao": this.str.Posicao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdItem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdItem = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EnquadraItemFuturoBMF.IdItem
		/// </summary>
		virtual public System.Int32? IdItem
		{
			get
			{
				return base.GetSystemInt32(EnquadraItemFuturoBMFMetadata.ColumnNames.IdItem);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraItemFuturoBMFMetadata.ColumnNames.IdItem, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemFuturoBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(EnquadraItemFuturoBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(EnquadraItemFuturoBMFMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToClasseBMFByCdAtivoBMF = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemFuturoBMF.Posicao
		/// </summary>
		virtual public System.String Posicao
		{
			get
			{
				return base.GetSystemString(EnquadraItemFuturoBMFMetadata.ColumnNames.Posicao);
			}
			
			set
			{
				base.SetSystemString(EnquadraItemFuturoBMFMetadata.ColumnNames.Posicao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected ClasseBMF _UpToClasseBMFByCdAtivoBMF;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEnquadraItemFuturoBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdItem
			{
				get
				{
					System.Int32? data = entity.IdItem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdItem = null;
					else entity.IdItem = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Posicao
			{
				get
				{
					System.String data = entity.Posicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Posicao = null;
					else entity.Posicao = Convert.ToString(value);
				}
			}
			

			private esEnquadraItemFuturoBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEnquadraItemFuturoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEnquadraItemFuturoBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EnquadraItemFuturoBMF : esEnquadraItemFuturoBMF
	{

				
		#region UpToClasseBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ClasseBMF_EnquadraItemFuturoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public ClasseBMF UpToClasseBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToClasseBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					)
				{
					this._UpToClasseBMFByCdAtivoBMF = new ClasseBMF();
					this._UpToClasseBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClasseBMFByCdAtivoBMF", this._UpToClasseBMFByCdAtivoBMF);
					this._UpToClasseBMFByCdAtivoBMF.Query.Where(this._UpToClasseBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToClasseBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToClasseBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToClasseBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this._UpToClasseBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this._UpToClasseBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToClasseBMFByCdAtivoBMF", this._UpToClasseBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

		#region UpToEnquadraItem - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - EnquadraItem_EnquadraItemFuturoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItem UpToEnquadraItem
		{
			get
			{
				if(this._UpToEnquadraItem == null
					&& IdItem != null					)
				{
					this._UpToEnquadraItem = new EnquadraItem();
					this._UpToEnquadraItem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEnquadraItem", this._UpToEnquadraItem);
					this._UpToEnquadraItem.Query.Where(this._UpToEnquadraItem.Query.IdItem == this.IdItem);
					this._UpToEnquadraItem.Query.Load();
				}

				return this._UpToEnquadraItem;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToEnquadraItem");

				if(value == null)
				{
					this._UpToEnquadraItem = null;
				}
				else
				{
					this._UpToEnquadraItem = value;
					this.SetPreSave("UpToEnquadraItem", this._UpToEnquadraItem);
				}
				
				
			} 
		}

		private EnquadraItem _UpToEnquadraItem;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEnquadraItem != null)
			{
				this.IdItem = this._UpToEnquadraItem.IdItem;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEnquadraItemFuturoBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraItemFuturoBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem IdItem
		{
			get
			{
				return new esQueryItem(this, EnquadraItemFuturoBMFMetadata.ColumnNames.IdItem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, EnquadraItemFuturoBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Posicao
		{
			get
			{
				return new esQueryItem(this, EnquadraItemFuturoBMFMetadata.ColumnNames.Posicao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EnquadraItemFuturoBMFCollection")]
	public partial class EnquadraItemFuturoBMFCollection : esEnquadraItemFuturoBMFCollection, IEnumerable<EnquadraItemFuturoBMF>
	{
		public EnquadraItemFuturoBMFCollection()
		{

		}
		
		public static implicit operator List<EnquadraItemFuturoBMF>(EnquadraItemFuturoBMFCollection coll)
		{
			List<EnquadraItemFuturoBMF> list = new List<EnquadraItemFuturoBMF>();
			
			foreach (EnquadraItemFuturoBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EnquadraItemFuturoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraItemFuturoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EnquadraItemFuturoBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EnquadraItemFuturoBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EnquadraItemFuturoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraItemFuturoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EnquadraItemFuturoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EnquadraItemFuturoBMF AddNew()
		{
			EnquadraItemFuturoBMF entity = base.AddNewEntity() as EnquadraItemFuturoBMF;
			
			return entity;
		}

		public EnquadraItemFuturoBMF FindByPrimaryKey(System.Int32 idItem)
		{
			return base.FindByPrimaryKey(idItem) as EnquadraItemFuturoBMF;
		}


		#region IEnumerable<EnquadraItemFuturoBMF> Members

		IEnumerator<EnquadraItemFuturoBMF> IEnumerable<EnquadraItemFuturoBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EnquadraItemFuturoBMF;
			}
		}

		#endregion
		
		private EnquadraItemFuturoBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EnquadraItemFuturoBMF' table
	/// </summary>

	[Serializable]
	public partial class EnquadraItemFuturoBMF : esEnquadraItemFuturoBMF
	{
		public EnquadraItemFuturoBMF()
		{

		}
	
		public EnquadraItemFuturoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraItemFuturoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esEnquadraItemFuturoBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraItemFuturoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EnquadraItemFuturoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraItemFuturoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EnquadraItemFuturoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EnquadraItemFuturoBMFQuery query;
	}



	[Serializable]
	public partial class EnquadraItemFuturoBMFQuery : esEnquadraItemFuturoBMFQuery
	{
		public EnquadraItemFuturoBMFQuery()
		{

		}		
		
		public EnquadraItemFuturoBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EnquadraItemFuturoBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EnquadraItemFuturoBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EnquadraItemFuturoBMFMetadata.ColumnNames.IdItem, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraItemFuturoBMFMetadata.PropertyNames.IdItem;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemFuturoBMFMetadata.ColumnNames.CdAtivoBMF, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = EnquadraItemFuturoBMFMetadata.PropertyNames.CdAtivoBMF;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemFuturoBMFMetadata.ColumnNames.Posicao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = EnquadraItemFuturoBMFMetadata.PropertyNames.Posicao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EnquadraItemFuturoBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdItem = "IdItem";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Posicao = "Posicao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdItem = "IdItem";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Posicao = "Posicao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EnquadraItemFuturoBMFMetadata))
			{
				if(EnquadraItemFuturoBMFMetadata.mapDelegates == null)
				{
					EnquadraItemFuturoBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EnquadraItemFuturoBMFMetadata.meta == null)
				{
					EnquadraItemFuturoBMFMetadata.meta = new EnquadraItemFuturoBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdItem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Posicao", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "EnquadraItemFuturoBMF";
				meta.Destination = "EnquadraItemFuturoBMF";
				
				meta.spInsert = "proc_EnquadraItemFuturoBMFInsert";				
				meta.spUpdate = "proc_EnquadraItemFuturoBMFUpdate";		
				meta.spDelete = "proc_EnquadraItemFuturoBMFDelete";
				meta.spLoadAll = "proc_EnquadraItemFuturoBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_EnquadraItemFuturoBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EnquadraItemFuturoBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
