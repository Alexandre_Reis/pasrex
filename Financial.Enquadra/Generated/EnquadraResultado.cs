/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 17/03/2014 16:46:09
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				



using Financial.Fundo;






				




		

		
		
		
		
		





namespace Financial.Enquadra
{

	[Serializable]
	abstract public class esEnquadraResultadoCollection : esEntityCollection
	{
		public esEnquadraResultadoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EnquadraResultadoCollection";
		}

		#region Query Logic
		protected void InitQuery(esEnquadraResultadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEnquadraResultadoQuery);
		}
		#endregion
		
		virtual public EnquadraResultado DetachEntity(EnquadraResultado entity)
		{
			return base.DetachEntity(entity) as EnquadraResultado;
		}
		
		virtual public EnquadraResultado AttachEntity(EnquadraResultado entity)
		{
			return base.AttachEntity(entity) as EnquadraResultado;
		}
		
		virtual public void Combine(EnquadraResultadoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EnquadraResultado this[int index]
		{
			get
			{
				return base[index] as EnquadraResultado;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EnquadraResultado);
		}
	}



	[Serializable]
	abstract public class esEnquadraResultado : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEnquadraResultadoQuery GetDynamicQuery()
		{
			return null;
		}

		public esEnquadraResultado()
		{

		}

		public esEnquadraResultado(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idResultado)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idResultado);
			else
				return LoadByPrimaryKeyStoredProcedure(idResultado);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idResultado)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEnquadraResultadoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdResultado == idResultado);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idResultado)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idResultado);
			else
				return LoadByPrimaryKeyStoredProcedure(idResultado);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idResultado)
		{
			esEnquadraResultadoQuery query = this.GetDynamicQuery();
			query.Where(query.IdResultado == idResultado);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idResultado)
		{
			esParameters parms = new esParameters();
			parms.Add("IdResultado",idResultado);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdResultado": this.str.IdResultado = (string)value; break;							
						case "IdRegra": this.str.IdRegra = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "ValorCriterio": this.str.ValorCriterio = (string)value; break;							
						case "ValorBase": this.str.ValorBase = (string)value; break;							
						case "Resultado": this.str.Resultado = (string)value; break;							
						case "Enquadrado": this.str.Enquadrado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdResultado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdResultado = (System.Int32?)value;
							break;
						
						case "IdRegra":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdRegra = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "ValorCriterio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCriterio = (System.Decimal?)value;
							break;
						
						case "ValorBase":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBase = (System.Decimal?)value;
							break;
						
						case "Resultado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Resultado = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EnquadraResultado.IdResultado
		/// </summary>
		virtual public System.Int32? IdResultado
		{
			get
			{
				return base.GetSystemInt32(EnquadraResultadoMetadata.ColumnNames.IdResultado);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraResultadoMetadata.ColumnNames.IdResultado, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultado.IdRegra
		/// </summary>
		virtual public System.Int32? IdRegra
		{
			get
			{
				return base.GetSystemInt32(EnquadraResultadoMetadata.ColumnNames.IdRegra);
			}
			
			set
			{
				if(base.SetSystemInt32(EnquadraResultadoMetadata.ColumnNames.IdRegra, value))
				{
					this._UpToEnquadraRegraByIdRegra = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultado.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(EnquadraResultadoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(EnquadraResultadoMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultado.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(EnquadraResultadoMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(EnquadraResultadoMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultado.ValorCriterio
		/// </summary>
		virtual public System.Decimal? ValorCriterio
		{
			get
			{
				return base.GetSystemDecimal(EnquadraResultadoMetadata.ColumnNames.ValorCriterio);
			}
			
			set
			{
				base.SetSystemDecimal(EnquadraResultadoMetadata.ColumnNames.ValorCriterio, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultado.ValorBase
		/// </summary>
		virtual public System.Decimal? ValorBase
		{
			get
			{
				return base.GetSystemDecimal(EnquadraResultadoMetadata.ColumnNames.ValorBase);
			}
			
			set
			{
				base.SetSystemDecimal(EnquadraResultadoMetadata.ColumnNames.ValorBase, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultado.Resultado
		/// </summary>
		virtual public System.Decimal? Resultado
		{
			get
			{
				return base.GetSystemDecimal(EnquadraResultadoMetadata.ColumnNames.Resultado);
			}
			
			set
			{
				base.SetSystemDecimal(EnquadraResultadoMetadata.ColumnNames.Resultado, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultado.Enquadrado
		/// </summary>
		virtual public System.String Enquadrado
		{
			get
			{
				return base.GetSystemString(EnquadraResultadoMetadata.ColumnNames.Enquadrado);
			}
			
			set
			{
				base.SetSystemString(EnquadraResultadoMetadata.ColumnNames.Enquadrado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected EnquadraRegra _UpToEnquadraRegraByIdRegra;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEnquadraResultado entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdResultado
			{
				get
				{
					System.Int32? data = entity.IdResultado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdResultado = null;
					else entity.IdResultado = Convert.ToInt32(value);
				}
			}
				
			public System.String IdRegra
			{
				get
				{
					System.Int32? data = entity.IdRegra;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdRegra = null;
					else entity.IdRegra = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorCriterio
			{
				get
				{
					System.Decimal? data = entity.ValorCriterio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCriterio = null;
					else entity.ValorCriterio = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBase
			{
				get
				{
					System.Decimal? data = entity.ValorBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBase = null;
					else entity.ValorBase = Convert.ToDecimal(value);
				}
			}
				
			public System.String Resultado
			{
				get
				{
					System.Decimal? data = entity.Resultado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Resultado = null;
					else entity.Resultado = Convert.ToDecimal(value);
				}
			}
				
			public System.String Enquadrado
			{
				get
				{
					System.String data = entity.Enquadrado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Enquadrado = null;
					else entity.Enquadrado = Convert.ToString(value);
				}
			}
			

			private esEnquadraResultado entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEnquadraResultadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEnquadraResultado can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EnquadraResultado : esEnquadraResultado
	{

				
		#region EnquadraResultadoDetalheCollectionByIdResultadoOrigem - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EnquadraResultado_EnquadraResultadoDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraResultadoDetalheCollection EnquadraResultadoDetalheCollectionByIdResultadoOrigem
		{
			get
			{
				if(this._EnquadraResultadoDetalheCollectionByIdResultadoOrigem == null)
				{
					this._EnquadraResultadoDetalheCollectionByIdResultadoOrigem = new EnquadraResultadoDetalheCollection();
					this._EnquadraResultadoDetalheCollectionByIdResultadoOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraResultadoDetalheCollectionByIdResultadoOrigem", this._EnquadraResultadoDetalheCollectionByIdResultadoOrigem);
				
					if(this.IdResultado != null)
					{
						this._EnquadraResultadoDetalheCollectionByIdResultadoOrigem.Query.Where(this._EnquadraResultadoDetalheCollectionByIdResultadoOrigem.Query.IdResultadoOrigem == this.IdResultado);
						this._EnquadraResultadoDetalheCollectionByIdResultadoOrigem.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraResultadoDetalheCollectionByIdResultadoOrigem.fks.Add(EnquadraResultadoDetalheMetadata.ColumnNames.IdResultadoOrigem, this.IdResultado);
					}
				}

				return this._EnquadraResultadoDetalheCollectionByIdResultadoOrigem;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraResultadoDetalheCollectionByIdResultadoOrigem != null) 
				{ 
					this.RemovePostSave("EnquadraResultadoDetalheCollectionByIdResultadoOrigem"); 
					this._EnquadraResultadoDetalheCollectionByIdResultadoOrigem = null;
					
				} 
			} 			
		}

		private EnquadraResultadoDetalheCollection _EnquadraResultadoDetalheCollectionByIdResultadoOrigem;
		#endregion

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_EnquadraResultado_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToEnquadraRegraByIdRegra - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EnquadraRegra_EnquadraResultado_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraRegra UpToEnquadraRegraByIdRegra
		{
			get
			{
				if(this._UpToEnquadraRegraByIdRegra == null
					&& IdRegra != null					)
				{
					this._UpToEnquadraRegraByIdRegra = new EnquadraRegra();
					this._UpToEnquadraRegraByIdRegra.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEnquadraRegraByIdRegra", this._UpToEnquadraRegraByIdRegra);
					this._UpToEnquadraRegraByIdRegra.Query.Where(this._UpToEnquadraRegraByIdRegra.Query.IdRegra == this.IdRegra);
					this._UpToEnquadraRegraByIdRegra.Query.Load();
				}

				return this._UpToEnquadraRegraByIdRegra;
			}
			
			set
			{
				this.RemovePreSave("UpToEnquadraRegraByIdRegra");
				

				if(value == null)
				{
					this.IdRegra = null;
					this._UpToEnquadraRegraByIdRegra = null;
				}
				else
				{
					this.IdRegra = value.IdRegra;
					this._UpToEnquadraRegraByIdRegra = value;
					this.SetPreSave("UpToEnquadraRegraByIdRegra", this._UpToEnquadraRegraByIdRegra);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "EnquadraResultadoDetalheCollectionByIdResultadoOrigem", typeof(EnquadraResultadoDetalheCollection), new EnquadraResultadoDetalhe()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEnquadraRegraByIdRegra != null)
			{
				this.IdRegra = this._UpToEnquadraRegraByIdRegra.IdRegra;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._EnquadraResultadoDetalheCollectionByIdResultadoOrigem != null)
			{
				foreach(EnquadraResultadoDetalhe obj in this._EnquadraResultadoDetalheCollectionByIdResultadoOrigem)
				{
					if(obj.es.IsAdded)
					{
						obj.IdResultadoOrigem = this.IdResultado;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEnquadraResultadoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraResultadoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdResultado
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoMetadata.ColumnNames.IdResultado, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdRegra
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoMetadata.ColumnNames.IdRegra, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorCriterio
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoMetadata.ColumnNames.ValorCriterio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBase
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoMetadata.ColumnNames.ValorBase, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Resultado
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoMetadata.ColumnNames.Resultado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Enquadrado
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoMetadata.ColumnNames.Enquadrado, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EnquadraResultadoCollection")]
	public partial class EnquadraResultadoCollection : esEnquadraResultadoCollection, IEnumerable<EnquadraResultado>
	{
		public EnquadraResultadoCollection()
		{

		}
		
		public static implicit operator List<EnquadraResultado>(EnquadraResultadoCollection coll)
		{
			List<EnquadraResultado> list = new List<EnquadraResultado>();
			
			foreach (EnquadraResultado emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EnquadraResultadoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraResultadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EnquadraResultado(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EnquadraResultado();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EnquadraResultadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraResultadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EnquadraResultadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EnquadraResultado AddNew()
		{
			EnquadraResultado entity = base.AddNewEntity() as EnquadraResultado;
			
			return entity;
		}

		public EnquadraResultado FindByPrimaryKey(System.Int32 idResultado)
		{
			return base.FindByPrimaryKey(idResultado) as EnquadraResultado;
		}


		#region IEnumerable<EnquadraResultado> Members

		IEnumerator<EnquadraResultado> IEnumerable<EnquadraResultado>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EnquadraResultado;
			}
		}

		#endregion
		
		private EnquadraResultadoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EnquadraResultado' table
	/// </summary>

	[Serializable]
	public partial class EnquadraResultado : esEnquadraResultado
	{
		public EnquadraResultado()
		{

		}
	
		public EnquadraResultado(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraResultadoMetadata.Meta();
			}
		}
		
		
		
		override protected esEnquadraResultadoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraResultadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EnquadraResultadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraResultadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EnquadraResultadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EnquadraResultadoQuery query;
	}



	[Serializable]
	public partial class EnquadraResultadoQuery : esEnquadraResultadoQuery
	{
		public EnquadraResultadoQuery()
		{

		}		
		
		public EnquadraResultadoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EnquadraResultadoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EnquadraResultadoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EnquadraResultadoMetadata.ColumnNames.IdResultado, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraResultadoMetadata.PropertyNames.IdResultado;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoMetadata.ColumnNames.IdRegra, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraResultadoMetadata.PropertyNames.IdRegra;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoMetadata.ColumnNames.IdCarteira, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraResultadoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoMetadata.ColumnNames.Data, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EnquadraResultadoMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoMetadata.ColumnNames.ValorCriterio, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EnquadraResultadoMetadata.PropertyNames.ValorCriterio;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoMetadata.ColumnNames.ValorBase, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EnquadraResultadoMetadata.PropertyNames.ValorBase;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoMetadata.ColumnNames.Resultado, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EnquadraResultadoMetadata.PropertyNames.Resultado;	
			c.NumericPrecision = 12;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoMetadata.ColumnNames.Enquadrado, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = EnquadraResultadoMetadata.PropertyNames.Enquadrado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EnquadraResultadoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdResultado = "IdResultado";
			 public const string IdRegra = "IdRegra";
			 public const string IdCarteira = "IdCarteira";
			 public const string Data = "Data";
			 public const string ValorCriterio = "ValorCriterio";
			 public const string ValorBase = "ValorBase";
			 public const string Resultado = "Resultado";
			 public const string Enquadrado = "Enquadrado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdResultado = "IdResultado";
			 public const string IdRegra = "IdRegra";
			 public const string IdCarteira = "IdCarteira";
			 public const string Data = "Data";
			 public const string ValorCriterio = "ValorCriterio";
			 public const string ValorBase = "ValorBase";
			 public const string Resultado = "Resultado";
			 public const string Enquadrado = "Enquadrado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EnquadraResultadoMetadata))
			{
				if(EnquadraResultadoMetadata.mapDelegates == null)
				{
					EnquadraResultadoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EnquadraResultadoMetadata.meta == null)
				{
					EnquadraResultadoMetadata.meta = new EnquadraResultadoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdResultado", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdRegra", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorCriterio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBase", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Resultado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Enquadrado", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "EnquadraResultado";
				meta.Destination = "EnquadraResultado";
				
				meta.spInsert = "proc_EnquadraResultadoInsert";				
				meta.spUpdate = "proc_EnquadraResultadoUpdate";		
				meta.spDelete = "proc_EnquadraResultadoDelete";
				meta.spLoadAll = "proc_EnquadraResultadoLoadAll";
				meta.spLoadByPrimaryKey = "proc_EnquadraResultadoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EnquadraResultadoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
