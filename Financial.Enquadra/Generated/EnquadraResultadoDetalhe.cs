/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 17/03/2014 16:46:10
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				




using Financial.Fundo;





				




		

		
		
		
		
		





namespace Financial.Enquadra
{

	[Serializable]
	abstract public class esEnquadraResultadoDetalheCollection : esEntityCollection
	{
		public esEnquadraResultadoDetalheCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EnquadraResultadoDetalheCollection";
		}

		#region Query Logic
		protected void InitQuery(esEnquadraResultadoDetalheQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEnquadraResultadoDetalheQuery);
		}
		#endregion
		
		virtual public EnquadraResultadoDetalhe DetachEntity(EnquadraResultadoDetalhe entity)
		{
			return base.DetachEntity(entity) as EnquadraResultadoDetalhe;
		}
		
		virtual public EnquadraResultadoDetalhe AttachEntity(EnquadraResultadoDetalhe entity)
		{
			return base.AttachEntity(entity) as EnquadraResultadoDetalhe;
		}
		
		virtual public void Combine(EnquadraResultadoDetalheCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EnquadraResultadoDetalhe this[int index]
		{
			get
			{
				return base[index] as EnquadraResultadoDetalhe;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EnquadraResultadoDetalhe);
		}
	}



	[Serializable]
	abstract public class esEnquadraResultadoDetalhe : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEnquadraResultadoDetalheQuery GetDynamicQuery()
		{
			return null;
		}

		public esEnquadraResultadoDetalhe()
		{

		}

		public esEnquadraResultadoDetalhe(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idResultado)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idResultado);
			else
				return LoadByPrimaryKeyStoredProcedure(idResultado);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idResultado)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEnquadraResultadoDetalheQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdResultado == idResultado);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idResultado)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idResultado);
			else
				return LoadByPrimaryKeyStoredProcedure(idResultado);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idResultado)
		{
			esEnquadraResultadoDetalheQuery query = this.GetDynamicQuery();
			query.Where(query.IdResultado == idResultado);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idResultado)
		{
			esParameters parms = new esParameters();
			parms.Add("IdResultado",idResultado);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdResultado": this.str.IdResultado = (string)value; break;							
						case "IdRegra": this.str.IdRegra = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Item": this.str.Item = (string)value; break;							
						case "ValorCriterio": this.str.ValorCriterio = (string)value; break;							
						case "ValorBase": this.str.ValorBase = (string)value; break;							
						case "Resultado": this.str.Resultado = (string)value; break;							
						case "Enquadrado": this.str.Enquadrado = (string)value; break;							
						case "IdResultadoOrigem": this.str.IdResultadoOrigem = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdResultado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdResultado = (System.Int32?)value;
							break;
						
						case "IdRegra":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdRegra = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "ValorCriterio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCriterio = (System.Decimal?)value;
							break;
						
						case "ValorBase":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBase = (System.Decimal?)value;
							break;
						
						case "Resultado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Resultado = (System.Decimal?)value;
							break;
						
						case "IdResultadoOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdResultadoOrigem = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EnquadraResultadoDetalhe.IdResultado
		/// </summary>
		virtual public System.Int32? IdResultado
		{
			get
			{
				return base.GetSystemInt32(EnquadraResultadoDetalheMetadata.ColumnNames.IdResultado);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraResultadoDetalheMetadata.ColumnNames.IdResultado, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultadoDetalhe.IdRegra
		/// </summary>
		virtual public System.Int32? IdRegra
		{
			get
			{
				return base.GetSystemInt32(EnquadraResultadoDetalheMetadata.ColumnNames.IdRegra);
			}
			
			set
			{
				if(base.SetSystemInt32(EnquadraResultadoDetalheMetadata.ColumnNames.IdRegra, value))
				{
					this._UpToEnquadraRegraByIdRegra = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultadoDetalhe.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(EnquadraResultadoDetalheMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(EnquadraResultadoDetalheMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultadoDetalhe.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(EnquadraResultadoDetalheMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(EnquadraResultadoDetalheMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultadoDetalhe.Item
		/// </summary>
		virtual public System.String Item
		{
			get
			{
				return base.GetSystemString(EnquadraResultadoDetalheMetadata.ColumnNames.Item);
			}
			
			set
			{
				base.SetSystemString(EnquadraResultadoDetalheMetadata.ColumnNames.Item, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultadoDetalhe.ValorCriterio
		/// </summary>
		virtual public System.Decimal? ValorCriterio
		{
			get
			{
				return base.GetSystemDecimal(EnquadraResultadoDetalheMetadata.ColumnNames.ValorCriterio);
			}
			
			set
			{
				base.SetSystemDecimal(EnquadraResultadoDetalheMetadata.ColumnNames.ValorCriterio, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultadoDetalhe.ValorBase
		/// </summary>
		virtual public System.Decimal? ValorBase
		{
			get
			{
				return base.GetSystemDecimal(EnquadraResultadoDetalheMetadata.ColumnNames.ValorBase);
			}
			
			set
			{
				base.SetSystemDecimal(EnquadraResultadoDetalheMetadata.ColumnNames.ValorBase, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultadoDetalhe.Resultado
		/// </summary>
		virtual public System.Decimal? Resultado
		{
			get
			{
				return base.GetSystemDecimal(EnquadraResultadoDetalheMetadata.ColumnNames.Resultado);
			}
			
			set
			{
				base.SetSystemDecimal(EnquadraResultadoDetalheMetadata.ColumnNames.Resultado, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultadoDetalhe.Enquadrado
		/// </summary>
		virtual public System.String Enquadrado
		{
			get
			{
				return base.GetSystemString(EnquadraResultadoDetalheMetadata.ColumnNames.Enquadrado);
			}
			
			set
			{
				base.SetSystemString(EnquadraResultadoDetalheMetadata.ColumnNames.Enquadrado, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraResultadoDetalhe.IdResultadoOrigem
		/// </summary>
		virtual public System.Int32? IdResultadoOrigem
		{
			get
			{
				return base.GetSystemInt32(EnquadraResultadoDetalheMetadata.ColumnNames.IdResultadoOrigem);
			}
			
			set
			{
				if(base.SetSystemInt32(EnquadraResultadoDetalheMetadata.ColumnNames.IdResultadoOrigem, value))
				{
					this._UpToEnquadraResultadoByIdResultadoOrigem = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected EnquadraRegra _UpToEnquadraRegraByIdRegra;
		[CLSCompliant(false)]
		internal protected EnquadraResultado _UpToEnquadraResultadoByIdResultadoOrigem;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEnquadraResultadoDetalhe entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdResultado
			{
				get
				{
					System.Int32? data = entity.IdResultado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdResultado = null;
					else entity.IdResultado = Convert.ToInt32(value);
				}
			}
				
			public System.String IdRegra
			{
				get
				{
					System.Int32? data = entity.IdRegra;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdRegra = null;
					else entity.IdRegra = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Item
			{
				get
				{
					System.String data = entity.Item;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Item = null;
					else entity.Item = Convert.ToString(value);
				}
			}
				
			public System.String ValorCriterio
			{
				get
				{
					System.Decimal? data = entity.ValorCriterio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCriterio = null;
					else entity.ValorCriterio = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBase
			{
				get
				{
					System.Decimal? data = entity.ValorBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBase = null;
					else entity.ValorBase = Convert.ToDecimal(value);
				}
			}
				
			public System.String Resultado
			{
				get
				{
					System.Decimal? data = entity.Resultado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Resultado = null;
					else entity.Resultado = Convert.ToDecimal(value);
				}
			}
				
			public System.String Enquadrado
			{
				get
				{
					System.String data = entity.Enquadrado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Enquadrado = null;
					else entity.Enquadrado = Convert.ToString(value);
				}
			}
				
			public System.String IdResultadoOrigem
			{
				get
				{
					System.Int32? data = entity.IdResultadoOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdResultadoOrigem = null;
					else entity.IdResultadoOrigem = Convert.ToInt32(value);
				}
			}
			

			private esEnquadraResultadoDetalhe entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEnquadraResultadoDetalheQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEnquadraResultadoDetalhe can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EnquadraResultadoDetalhe : esEnquadraResultadoDetalhe
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_EnquadraResultadoDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToEnquadraRegraByIdRegra - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EnquadraRegra_EnquadraResultadoDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraRegra UpToEnquadraRegraByIdRegra
		{
			get
			{
				if(this._UpToEnquadraRegraByIdRegra == null
					&& IdRegra != null					)
				{
					this._UpToEnquadraRegraByIdRegra = new EnquadraRegra();
					this._UpToEnquadraRegraByIdRegra.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEnquadraRegraByIdRegra", this._UpToEnquadraRegraByIdRegra);
					this._UpToEnquadraRegraByIdRegra.Query.Where(this._UpToEnquadraRegraByIdRegra.Query.IdRegra == this.IdRegra);
					this._UpToEnquadraRegraByIdRegra.Query.Load();
				}

				return this._UpToEnquadraRegraByIdRegra;
			}
			
			set
			{
				this.RemovePreSave("UpToEnquadraRegraByIdRegra");
				

				if(value == null)
				{
					this.IdRegra = null;
					this._UpToEnquadraRegraByIdRegra = null;
				}
				else
				{
					this.IdRegra = value.IdRegra;
					this._UpToEnquadraRegraByIdRegra = value;
					this.SetPreSave("UpToEnquadraRegraByIdRegra", this._UpToEnquadraRegraByIdRegra);
				}
				
			}
		}
		#endregion
		

				
		#region UpToEnquadraResultadoByIdResultadoOrigem - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EnquadraResultado_EnquadraResultadoDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraResultado UpToEnquadraResultadoByIdResultadoOrigem
		{
			get
			{
				if(this._UpToEnquadraResultadoByIdResultadoOrigem == null
					&& IdResultadoOrigem != null					)
				{
					this._UpToEnquadraResultadoByIdResultadoOrigem = new EnquadraResultado();
					this._UpToEnquadraResultadoByIdResultadoOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEnquadraResultadoByIdResultadoOrigem", this._UpToEnquadraResultadoByIdResultadoOrigem);
					this._UpToEnquadraResultadoByIdResultadoOrigem.Query.Where(this._UpToEnquadraResultadoByIdResultadoOrigem.Query.IdResultado == this.IdResultadoOrigem);
					this._UpToEnquadraResultadoByIdResultadoOrigem.Query.Load();
				}

				return this._UpToEnquadraResultadoByIdResultadoOrigem;
			}
			
			set
			{
				this.RemovePreSave("UpToEnquadraResultadoByIdResultadoOrigem");
				

				if(value == null)
				{
					this.IdResultadoOrigem = null;
					this._UpToEnquadraResultadoByIdResultadoOrigem = null;
				}
				else
				{
					this.IdResultadoOrigem = value.IdResultado;
					this._UpToEnquadraResultadoByIdResultadoOrigem = value;
					this.SetPreSave("UpToEnquadraResultadoByIdResultadoOrigem", this._UpToEnquadraResultadoByIdResultadoOrigem);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEnquadraRegraByIdRegra != null)
			{
				this.IdRegra = this._UpToEnquadraRegraByIdRegra.IdRegra;
			}
			if(!this.es.IsDeleted && this._UpToEnquadraResultadoByIdResultadoOrigem != null)
			{
				this.IdResultadoOrigem = this._UpToEnquadraResultadoByIdResultadoOrigem.IdResultado;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEnquadraResultadoDetalheQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraResultadoDetalheMetadata.Meta();
			}
		}	
		

		public esQueryItem IdResultado
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoDetalheMetadata.ColumnNames.IdResultado, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdRegra
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoDetalheMetadata.ColumnNames.IdRegra, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoDetalheMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoDetalheMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Item
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoDetalheMetadata.ColumnNames.Item, esSystemType.String);
			}
		} 
		
		public esQueryItem ValorCriterio
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoDetalheMetadata.ColumnNames.ValorCriterio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBase
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoDetalheMetadata.ColumnNames.ValorBase, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Resultado
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoDetalheMetadata.ColumnNames.Resultado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Enquadrado
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoDetalheMetadata.ColumnNames.Enquadrado, esSystemType.String);
			}
		} 
		
		public esQueryItem IdResultadoOrigem
		{
			get
			{
				return new esQueryItem(this, EnquadraResultadoDetalheMetadata.ColumnNames.IdResultadoOrigem, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EnquadraResultadoDetalheCollection")]
	public partial class EnquadraResultadoDetalheCollection : esEnquadraResultadoDetalheCollection, IEnumerable<EnquadraResultadoDetalhe>
	{
		public EnquadraResultadoDetalheCollection()
		{

		}
		
		public static implicit operator List<EnquadraResultadoDetalhe>(EnquadraResultadoDetalheCollection coll)
		{
			List<EnquadraResultadoDetalhe> list = new List<EnquadraResultadoDetalhe>();
			
			foreach (EnquadraResultadoDetalhe emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EnquadraResultadoDetalheMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraResultadoDetalheQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EnquadraResultadoDetalhe(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EnquadraResultadoDetalhe();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EnquadraResultadoDetalheQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraResultadoDetalheQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EnquadraResultadoDetalheQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EnquadraResultadoDetalhe AddNew()
		{
			EnquadraResultadoDetalhe entity = base.AddNewEntity() as EnquadraResultadoDetalhe;
			
			return entity;
		}

		public EnquadraResultadoDetalhe FindByPrimaryKey(System.Int32 idResultado)
		{
			return base.FindByPrimaryKey(idResultado) as EnquadraResultadoDetalhe;
		}


		#region IEnumerable<EnquadraResultadoDetalhe> Members

		IEnumerator<EnquadraResultadoDetalhe> IEnumerable<EnquadraResultadoDetalhe>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EnquadraResultadoDetalhe;
			}
		}

		#endregion
		
		private EnquadraResultadoDetalheQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EnquadraResultadoDetalhe' table
	/// </summary>

	[Serializable]
	public partial class EnquadraResultadoDetalhe : esEnquadraResultadoDetalhe
	{
		public EnquadraResultadoDetalhe()
		{

		}
	
		public EnquadraResultadoDetalhe(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraResultadoDetalheMetadata.Meta();
			}
		}
		
		
		
		override protected esEnquadraResultadoDetalheQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraResultadoDetalheQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EnquadraResultadoDetalheQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraResultadoDetalheQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EnquadraResultadoDetalheQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EnquadraResultadoDetalheQuery query;
	}



	[Serializable]
	public partial class EnquadraResultadoDetalheQuery : esEnquadraResultadoDetalheQuery
	{
		public EnquadraResultadoDetalheQuery()
		{

		}		
		
		public EnquadraResultadoDetalheQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EnquadraResultadoDetalheMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EnquadraResultadoDetalheMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EnquadraResultadoDetalheMetadata.ColumnNames.IdResultado, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraResultadoDetalheMetadata.PropertyNames.IdResultado;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoDetalheMetadata.ColumnNames.IdRegra, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraResultadoDetalheMetadata.PropertyNames.IdRegra;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoDetalheMetadata.ColumnNames.IdCarteira, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraResultadoDetalheMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoDetalheMetadata.ColumnNames.Data, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EnquadraResultadoDetalheMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoDetalheMetadata.ColumnNames.Item, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = EnquadraResultadoDetalheMetadata.PropertyNames.Item;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoDetalheMetadata.ColumnNames.ValorCriterio, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EnquadraResultadoDetalheMetadata.PropertyNames.ValorCriterio;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoDetalheMetadata.ColumnNames.ValorBase, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EnquadraResultadoDetalheMetadata.PropertyNames.ValorBase;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoDetalheMetadata.ColumnNames.Resultado, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EnquadraResultadoDetalheMetadata.PropertyNames.Resultado;	
			c.NumericPrecision = 12;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoDetalheMetadata.ColumnNames.Enquadrado, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = EnquadraResultadoDetalheMetadata.PropertyNames.Enquadrado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraResultadoDetalheMetadata.ColumnNames.IdResultadoOrigem, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraResultadoDetalheMetadata.PropertyNames.IdResultadoOrigem;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EnquadraResultadoDetalheMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdResultado = "IdResultado";
			 public const string IdRegra = "IdRegra";
			 public const string IdCarteira = "IdCarteira";
			 public const string Data = "Data";
			 public const string Item = "Item";
			 public const string ValorCriterio = "ValorCriterio";
			 public const string ValorBase = "ValorBase";
			 public const string Resultado = "Resultado";
			 public const string Enquadrado = "Enquadrado";
			 public const string IdResultadoOrigem = "IdResultadoOrigem";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdResultado = "IdResultado";
			 public const string IdRegra = "IdRegra";
			 public const string IdCarteira = "IdCarteira";
			 public const string Data = "Data";
			 public const string Item = "Item";
			 public const string ValorCriterio = "ValorCriterio";
			 public const string ValorBase = "ValorBase";
			 public const string Resultado = "Resultado";
			 public const string Enquadrado = "Enquadrado";
			 public const string IdResultadoOrigem = "IdResultadoOrigem";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EnquadraResultadoDetalheMetadata))
			{
				if(EnquadraResultadoDetalheMetadata.mapDelegates == null)
				{
					EnquadraResultadoDetalheMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EnquadraResultadoDetalheMetadata.meta == null)
				{
					EnquadraResultadoDetalheMetadata.meta = new EnquadraResultadoDetalheMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdResultado", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdRegra", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Item", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ValorCriterio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBase", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Resultado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Enquadrado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdResultadoOrigem", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "EnquadraResultadoDetalhe";
				meta.Destination = "EnquadraResultadoDetalhe";
				
				meta.spInsert = "proc_EnquadraResultadoDetalheInsert";				
				meta.spUpdate = "proc_EnquadraResultadoDetalheUpdate";		
				meta.spDelete = "proc_EnquadraResultadoDetalheDelete";
				meta.spLoadAll = "proc_EnquadraResultadoDetalheLoadAll";
				meta.spLoadByPrimaryKey = "proc_EnquadraResultadoDetalheLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EnquadraResultadoDetalheMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
