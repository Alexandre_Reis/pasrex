/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 17/03/2014 16:46:08
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Enquadra
{

	[Serializable]
	abstract public class esEnquadraItemCollection : esEntityCollection
	{
		public esEnquadraItemCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EnquadraItemCollection";
		}

		#region Query Logic
		protected void InitQuery(esEnquadraItemQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEnquadraItemQuery);
		}
		#endregion
		
		virtual public EnquadraItem DetachEntity(EnquadraItem entity)
		{
			return base.DetachEntity(entity) as EnquadraItem;
		}
		
		virtual public EnquadraItem AttachEntity(EnquadraItem entity)
		{
			return base.AttachEntity(entity) as EnquadraItem;
		}
		
		virtual public void Combine(EnquadraItemCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EnquadraItem this[int index]
		{
			get
			{
				return base[index] as EnquadraItem;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EnquadraItem);
		}
	}



	[Serializable]
	abstract public class esEnquadraItem : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEnquadraItemQuery GetDynamicQuery()
		{
			return null;
		}

		public esEnquadraItem()
		{

		}

		public esEnquadraItem(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idItem)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idItem);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idItem)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEnquadraItemQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdItem == idItem);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idItem)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idItem);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idItem)
		{
			esEnquadraItemQuery query = this.GetDynamicQuery();
			query.Where(query.IdItem == idItem);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idItem)
		{
			esParameters parms = new esParameters();
			parms.Add("IdItem",idItem);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdItem": this.str.IdItem = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdItem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdItem = (System.Int32?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Tipo = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EnquadraItem.IdItem
		/// </summary>
		virtual public System.Int32? IdItem
		{
			get
			{
				return base.GetSystemInt32(EnquadraItemMetadata.ColumnNames.IdItem);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraItemMetadata.ColumnNames.IdItem, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItem.Tipo
		/// </summary>
		virtual public System.Int32? Tipo
		{
			get
			{
				return base.GetSystemInt32(EnquadraItemMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraItemMetadata.ColumnNames.Tipo, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItem.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(EnquadraItemMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(EnquadraItemMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEnquadraItem entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdItem
			{
				get
				{
					System.Int32? data = entity.IdItem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdItem = null;
					else entity.IdItem = Convert.ToInt32(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Int32? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esEnquadraItem entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEnquadraItemQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEnquadraItem can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EnquadraItem : esEnquadraItem
	{

		#region UpToEnquadraGrupoCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - EnquadraItem_EnquadraGrupoItem_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraGrupoCollection UpToEnquadraGrupoCollection
		{
			get
			{
				if(this._UpToEnquadraGrupoCollection == null)
				{
					this._UpToEnquadraGrupoCollection = new EnquadraGrupoCollection();
					this._UpToEnquadraGrupoCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToEnquadraGrupoCollection", this._UpToEnquadraGrupoCollection);
					this._UpToEnquadraGrupoCollection.ManyToManyEnquadraItemCollection(this.IdItem);
				}

				return this._UpToEnquadraGrupoCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToEnquadraGrupoCollection != null) 
				{ 
					this.RemovePostSave("UpToEnquadraGrupoCollection"); 
					this._UpToEnquadraGrupoCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - EnquadraItem_EnquadraGrupoItem_FK1
		/// </summary>
		public void AssociateEnquadraGrupoCollection(EnquadraGrupo entity)
		{
			if (this._EnquadraGrupoItemCollection == null)
			{
				this._EnquadraGrupoItemCollection = new EnquadraGrupoItemCollection();
				this._EnquadraGrupoItemCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("EnquadraGrupoItemCollection", this._EnquadraGrupoItemCollection);
			}

			EnquadraGrupoItem obj = this._EnquadraGrupoItemCollection.AddNew();
			obj.IdItem = this.IdItem;
			obj.IdGrupo = entity.IdGrupo;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - EnquadraItem_EnquadraGrupoItem_FK1
		/// </summary>
		public void DissociateEnquadraGrupoCollection(EnquadraGrupo entity)
		{
			if (this._EnquadraGrupoItemCollection == null)
			{
				this._EnquadraGrupoItemCollection = new EnquadraGrupoItemCollection();
				this._EnquadraGrupoItemCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("EnquadraGrupoItemCollection", this._EnquadraGrupoItemCollection);
			}

			EnquadraGrupoItem obj = this._EnquadraGrupoItemCollection.AddNew();
			obj.IdItem = this.IdItem;
			obj.IdGrupo = entity.IdGrupo;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private EnquadraGrupoCollection _UpToEnquadraGrupoCollection;
		private EnquadraGrupoItemCollection _EnquadraGrupoItemCollection;
		#endregion

				
		#region EnquadraGrupoItemCollectionByIdItem - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EnquadraItem_EnquadraGrupoItem_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraGrupoItemCollection EnquadraGrupoItemCollectionByIdItem
		{
			get
			{
				if(this._EnquadraGrupoItemCollectionByIdItem == null)
				{
					this._EnquadraGrupoItemCollectionByIdItem = new EnquadraGrupoItemCollection();
					this._EnquadraGrupoItemCollectionByIdItem.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraGrupoItemCollectionByIdItem", this._EnquadraGrupoItemCollectionByIdItem);
				
					if(this.IdItem != null)
					{
						this._EnquadraGrupoItemCollectionByIdItem.Query.Where(this._EnquadraGrupoItemCollectionByIdItem.Query.IdItem == this.IdItem);
						this._EnquadraGrupoItemCollectionByIdItem.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraGrupoItemCollectionByIdItem.fks.Add(EnquadraGrupoItemMetadata.ColumnNames.IdItem, this.IdItem);
					}
				}

				return this._EnquadraGrupoItemCollectionByIdItem;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraGrupoItemCollectionByIdItem != null) 
				{ 
					this.RemovePostSave("EnquadraGrupoItemCollectionByIdItem"); 
					this._EnquadraGrupoItemCollectionByIdItem = null;
					
				} 
			} 			
		}

		private EnquadraGrupoItemCollection _EnquadraGrupoItemCollectionByIdItem;
		#endregion

				
		#region EnquadraItemAcao - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - EnquadraItem_EnquadraItemAcao_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemAcao EnquadraItemAcao
		{
			get
			{
				if(this._EnquadraItemAcao == null)
				{
					this._EnquadraItemAcao = new EnquadraItemAcao();
					this._EnquadraItemAcao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("EnquadraItemAcao", this._EnquadraItemAcao);
				
					if(this.IdItem != null)
					{
						this._EnquadraItemAcao.Query.Where(this._EnquadraItemAcao.Query.IdItem == this.IdItem);
						this._EnquadraItemAcao.Query.Load();
					}
				}

				return this._EnquadraItemAcao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemAcao != null) 
				{ 
					this.RemovePostOneSave("EnquadraItemAcao"); 
					this._EnquadraItemAcao = null;
					
				} 
			}          			
		}

		private EnquadraItemAcao _EnquadraItemAcao;
		#endregion

				
		#region EnquadraItemFundo - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - EnquadraItem_EnquadraItemFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemFundo EnquadraItemFundo
		{
			get
			{
				if(this._EnquadraItemFundo == null)
				{
					this._EnquadraItemFundo = new EnquadraItemFundo();
					this._EnquadraItemFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("EnquadraItemFundo", this._EnquadraItemFundo);
				
					if(this.IdItem != null)
					{
						this._EnquadraItemFundo.Query.Where(this._EnquadraItemFundo.Query.IdItem == this.IdItem);
						this._EnquadraItemFundo.Query.Load();
					}
				}

				return this._EnquadraItemFundo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemFundo != null) 
				{ 
					this.RemovePostOneSave("EnquadraItemFundo"); 
					this._EnquadraItemFundo = null;
					
				} 
			}          			
		}

		private EnquadraItemFundo _EnquadraItemFundo;
		#endregion

				
		#region EnquadraItemFuturoBMF - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - EnquadraItem_EnquadraItemFuturoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemFuturoBMF EnquadraItemFuturoBMF
		{
			get
			{
				if(this._EnquadraItemFuturoBMF == null)
				{
					this._EnquadraItemFuturoBMF = new EnquadraItemFuturoBMF();
					this._EnquadraItemFuturoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("EnquadraItemFuturoBMF", this._EnquadraItemFuturoBMF);
				
					if(this.IdItem != null)
					{
						this._EnquadraItemFuturoBMF.Query.Where(this._EnquadraItemFuturoBMF.Query.IdItem == this.IdItem);
						this._EnquadraItemFuturoBMF.Query.Load();
					}
				}

				return this._EnquadraItemFuturoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemFuturoBMF != null) 
				{ 
					this.RemovePostOneSave("EnquadraItemFuturoBMF"); 
					this._EnquadraItemFuturoBMF = null;
					
				} 
			}          			
		}

		private EnquadraItemFuturoBMF _EnquadraItemFuturoBMF;
		#endregion

				
		#region EnquadraItemOpcaoBMF - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - EnquadraItem_EnquadraItemOpcaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemOpcaoBMF EnquadraItemOpcaoBMF
		{
			get
			{
				if(this._EnquadraItemOpcaoBMF == null)
				{
					this._EnquadraItemOpcaoBMF = new EnquadraItemOpcaoBMF();
					this._EnquadraItemOpcaoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("EnquadraItemOpcaoBMF", this._EnquadraItemOpcaoBMF);
				
					if(this.IdItem != null)
					{
						this._EnquadraItemOpcaoBMF.Query.Where(this._EnquadraItemOpcaoBMF.Query.IdItem == this.IdItem);
						this._EnquadraItemOpcaoBMF.Query.Load();
					}
				}

				return this._EnquadraItemOpcaoBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemOpcaoBMF != null) 
				{ 
					this.RemovePostOneSave("EnquadraItemOpcaoBMF"); 
					this._EnquadraItemOpcaoBMF = null;
					
				} 
			}          			
		}

		private EnquadraItemOpcaoBMF _EnquadraItemOpcaoBMF;
		#endregion

				
		#region EnquadraItemOpcaoBolsa - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - EnquadraItem_EnquadraItemOpcaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemOpcaoBolsa EnquadraItemOpcaoBolsa
		{
			get
			{
				if(this._EnquadraItemOpcaoBolsa == null)
				{
					this._EnquadraItemOpcaoBolsa = new EnquadraItemOpcaoBolsa();
					this._EnquadraItemOpcaoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("EnquadraItemOpcaoBolsa", this._EnquadraItemOpcaoBolsa);
				
					if(this.IdItem != null)
					{
						this._EnquadraItemOpcaoBolsa.Query.Where(this._EnquadraItemOpcaoBolsa.Query.IdItem == this.IdItem);
						this._EnquadraItemOpcaoBolsa.Query.Load();
					}
				}

				return this._EnquadraItemOpcaoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemOpcaoBolsa != null) 
				{ 
					this.RemovePostOneSave("EnquadraItemOpcaoBolsa"); 
					this._EnquadraItemOpcaoBolsa = null;
					
				} 
			}          			
		}

		private EnquadraItemOpcaoBolsa _EnquadraItemOpcaoBolsa;
		#endregion

				
		#region EnquadraItemRendaFixa - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - EnquadraItem_EnquadraItemRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemRendaFixa EnquadraItemRendaFixa
		{
			get
			{
				if(this._EnquadraItemRendaFixa == null)
				{
					this._EnquadraItemRendaFixa = new EnquadraItemRendaFixa();
					this._EnquadraItemRendaFixa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("EnquadraItemRendaFixa", this._EnquadraItemRendaFixa);
				
					if(this.IdItem != null)
					{
						this._EnquadraItemRendaFixa.Query.Where(this._EnquadraItemRendaFixa.Query.IdItem == this.IdItem);
						this._EnquadraItemRendaFixa.Query.Load();
					}
				}

				return this._EnquadraItemRendaFixa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemRendaFixa != null) 
				{ 
					this.RemovePostOneSave("EnquadraItemRendaFixa"); 
					this._EnquadraItemRendaFixa = null;
					
				} 
			}          			
		}

		private EnquadraItemRendaFixa _EnquadraItemRendaFixa;
		#endregion

				
		#region EnquadraItemSwap - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - EnquadraItem_EnquadraItemSwap_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemSwap EnquadraItemSwap
		{
			get
			{
				if(this._EnquadraItemSwap == null)
				{
					this._EnquadraItemSwap = new EnquadraItemSwap();
					this._EnquadraItemSwap.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("EnquadraItemSwap", this._EnquadraItemSwap);
				
					if(this.IdItem != null)
					{
						this._EnquadraItemSwap.Query.Where(this._EnquadraItemSwap.Query.IdItem == this.IdItem);
						this._EnquadraItemSwap.Query.Load();
					}
				}

				return this._EnquadraItemSwap;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemSwap != null) 
				{ 
					this.RemovePostOneSave("EnquadraItemSwap"); 
					this._EnquadraItemSwap = null;
					
				} 
			}          			
		}

		private EnquadraItemSwap _EnquadraItemSwap;
		#endregion

				
		#region EnquadraItemTermoBolsa - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - EnquadraItem_EnquadraItemTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemTermoBolsa EnquadraItemTermoBolsa
		{
			get
			{
				if(this._EnquadraItemTermoBolsa == null)
				{
					this._EnquadraItemTermoBolsa = new EnquadraItemTermoBolsa();
					this._EnquadraItemTermoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("EnquadraItemTermoBolsa", this._EnquadraItemTermoBolsa);
				
					if(this.IdItem != null)
					{
						this._EnquadraItemTermoBolsa.Query.Where(this._EnquadraItemTermoBolsa.Query.IdItem == this.IdItem);
						this._EnquadraItemTermoBolsa.Query.Load();
					}
				}

				return this._EnquadraItemTermoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemTermoBolsa != null) 
				{ 
					this.RemovePostOneSave("EnquadraItemTermoBolsa"); 
					this._EnquadraItemTermoBolsa = null;
					
				} 
			}          			
		}

		private EnquadraItemTermoBolsa _EnquadraItemTermoBolsa;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "EnquadraGrupoItemCollectionByIdItem", typeof(EnquadraGrupoItemCollection), new EnquadraGrupoItem()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._EnquadraGrupoItemCollection != null)
			{
				foreach(EnquadraGrupoItem obj in this._EnquadraGrupoItemCollection)
				{
					if(obj.es.IsAdded)
					{
						obj.IdItem = this.IdItem;
					}
				}
			}
			if(this._EnquadraGrupoItemCollectionByIdItem != null)
			{
				foreach(EnquadraGrupoItem obj in this._EnquadraGrupoItemCollectionByIdItem)
				{
					if(obj.es.IsAdded)
					{
						obj.IdItem = this.IdItem;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
			if(this._EnquadraItemAcao != null)
			{
				if(this._EnquadraItemAcao.es.IsAdded)
				{
					this._EnquadraItemAcao.IdItem = this.IdItem;
				}
			}
			if(this._EnquadraItemFundo != null)
			{
				if(this._EnquadraItemFundo.es.IsAdded)
				{
					this._EnquadraItemFundo.IdItem = this.IdItem;
				}
			}
			if(this._EnquadraItemFuturoBMF != null)
			{
				if(this._EnquadraItemFuturoBMF.es.IsAdded)
				{
					this._EnquadraItemFuturoBMF.IdItem = this.IdItem;
				}
			}
			if(this._EnquadraItemOpcaoBMF != null)
			{
				if(this._EnquadraItemOpcaoBMF.es.IsAdded)
				{
					this._EnquadraItemOpcaoBMF.IdItem = this.IdItem;
				}
			}
			if(this._EnquadraItemOpcaoBolsa != null)
			{
				if(this._EnquadraItemOpcaoBolsa.es.IsAdded)
				{
					this._EnquadraItemOpcaoBolsa.IdItem = this.IdItem;
				}
			}
			if(this._EnquadraItemRendaFixa != null)
			{
				if(this._EnquadraItemRendaFixa.es.IsAdded)
				{
					this._EnquadraItemRendaFixa.IdItem = this.IdItem;
				}
			}
			if(this._EnquadraItemSwap != null)
			{
				if(this._EnquadraItemSwap.es.IsAdded)
				{
					this._EnquadraItemSwap.IdItem = this.IdItem;
				}
			}
			if(this._EnquadraItemTermoBolsa != null)
			{
				if(this._EnquadraItemTermoBolsa.es.IsAdded)
				{
					this._EnquadraItemTermoBolsa.IdItem = this.IdItem;
				}
			}
		}
		
	}
	
	public partial class EnquadraItemCollection : esEnquadraItemCollection
	{
		#region ManyToManyEnquadraGrupoCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyEnquadraGrupoCollection(System.Int32? IdGrupo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdGrupo", IdGrupo);
	
			return base.Load( esQueryType.ManyToMany, 
				"EnquadraItem,EnquadraGrupoItem|IdItem,IdItem|IdGrupo",	parms);
		}
		#endregion
	}




	[Serializable]
	abstract public class esEnquadraItemQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraItemMetadata.Meta();
			}
		}	
		

		public esQueryItem IdItem
		{
			get
			{
				return new esQueryItem(this, EnquadraItemMetadata.ColumnNames.IdItem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, EnquadraItemMetadata.ColumnNames.Tipo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, EnquadraItemMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EnquadraItemCollection")]
	public partial class EnquadraItemCollection : esEnquadraItemCollection, IEnumerable<EnquadraItem>
	{
		public EnquadraItemCollection()
		{

		}
		
		public static implicit operator List<EnquadraItem>(EnquadraItemCollection coll)
		{
			List<EnquadraItem> list = new List<EnquadraItem>();
			
			foreach (EnquadraItem emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EnquadraItemMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraItemQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EnquadraItem(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EnquadraItem();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EnquadraItemQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraItemQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EnquadraItemQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EnquadraItem AddNew()
		{
			EnquadraItem entity = base.AddNewEntity() as EnquadraItem;
			
			return entity;
		}

		public EnquadraItem FindByPrimaryKey(System.Int32 idItem)
		{
			return base.FindByPrimaryKey(idItem) as EnquadraItem;
		}


		#region IEnumerable<EnquadraItem> Members

		IEnumerator<EnquadraItem> IEnumerable<EnquadraItem>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EnquadraItem;
			}
		}

		#endregion
		
		private EnquadraItemQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EnquadraItem' table
	/// </summary>

	[Serializable]
	public partial class EnquadraItem : esEnquadraItem
	{
		public EnquadraItem()
		{

		}
	
		public EnquadraItem(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraItemMetadata.Meta();
			}
		}
		
		
		
		override protected esEnquadraItemQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraItemQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EnquadraItemQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraItemQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EnquadraItemQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EnquadraItemQuery query;
	}



	[Serializable]
	public partial class EnquadraItemQuery : esEnquadraItemQuery
	{
		public EnquadraItemQuery()
		{

		}		
		
		public EnquadraItemQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EnquadraItemMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EnquadraItemMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EnquadraItemMetadata.ColumnNames.IdItem, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraItemMetadata.PropertyNames.IdItem;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemMetadata.ColumnNames.Tipo, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraItemMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = EnquadraItemMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EnquadraItemMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdItem = "IdItem";
			 public const string Tipo = "Tipo";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdItem = "IdItem";
			 public const string Tipo = "Tipo";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EnquadraItemMetadata))
			{
				if(EnquadraItemMetadata.mapDelegates == null)
				{
					EnquadraItemMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EnquadraItemMetadata.meta == null)
				{
					EnquadraItemMetadata.meta = new EnquadraItemMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdItem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Tipo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "EnquadraItem";
				meta.Destination = "EnquadraItem";
				
				meta.spInsert = "proc_EnquadraItemInsert";				
				meta.spUpdate = "proc_EnquadraItemUpdate";		
				meta.spDelete = "proc_EnquadraItemDelete";
				meta.spLoadAll = "proc_EnquadraItemLoadAll";
				meta.spLoadByPrimaryKey = "proc_EnquadraItemLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EnquadraItemMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
