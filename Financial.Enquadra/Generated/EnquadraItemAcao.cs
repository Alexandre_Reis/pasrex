/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 17/03/2014 16:46:08
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		



using Financial.Common;


				
				








				




		

		
		
		
		
		





namespace Financial.Enquadra
{

	[Serializable]
	abstract public class esEnquadraItemAcaoCollection : esEntityCollection
	{
		public esEnquadraItemAcaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EnquadraItemAcaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esEnquadraItemAcaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEnquadraItemAcaoQuery);
		}
		#endregion
		
		virtual public EnquadraItemAcao DetachEntity(EnquadraItemAcao entity)
		{
			return base.DetachEntity(entity) as EnquadraItemAcao;
		}
		
		virtual public EnquadraItemAcao AttachEntity(EnquadraItemAcao entity)
		{
			return base.AttachEntity(entity) as EnquadraItemAcao;
		}
		
		virtual public void Combine(EnquadraItemAcaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EnquadraItemAcao this[int index]
		{
			get
			{
				return base[index] as EnquadraItemAcao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EnquadraItemAcao);
		}
	}



	[Serializable]
	abstract public class esEnquadraItemAcao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEnquadraItemAcaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esEnquadraItemAcao()
		{

		}

		public esEnquadraItemAcao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idItem)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idItem);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idItem)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEnquadraItemAcaoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdItem == idItem);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idItem)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idItem);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idItem)
		{
			esEnquadraItemAcaoQuery query = this.GetDynamicQuery();
			query.Where(query.IdItem == idItem);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idItem)
		{
			esParameters parms = new esParameters();
			parms.Add("IdItem",idItem);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdItem": this.str.IdItem = (string)value; break;							
						case "IdSetor": this.str.IdSetor = (string)value; break;							
						case "IdEmissor": this.str.IdEmissor = (string)value; break;							
						case "TipoPapel": this.str.TipoPapel = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "Bloqueado": this.str.Bloqueado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdItem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdItem = (System.Int32?)value;
							break;
						
						case "IdSetor":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdSetor = (System.Int16?)value;
							break;
						
						case "IdEmissor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEmissor = (System.Int32?)value;
							break;
						
						case "TipoPapel":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoPapel = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EnquadraItemAcao.IdItem
		/// </summary>
		virtual public System.Int32? IdItem
		{
			get
			{
				return base.GetSystemInt32(EnquadraItemAcaoMetadata.ColumnNames.IdItem);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraItemAcaoMetadata.ColumnNames.IdItem, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemAcao.IdSetor
		/// </summary>
		virtual public System.Int16? IdSetor
		{
			get
			{
				return base.GetSystemInt16(EnquadraItemAcaoMetadata.ColumnNames.IdSetor);
			}
			
			set
			{
				if(base.SetSystemInt16(EnquadraItemAcaoMetadata.ColumnNames.IdSetor, value))
				{
					this._UpToSetorByIdSetor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemAcao.IdEmissor
		/// </summary>
		virtual public System.Int32? IdEmissor
		{
			get
			{
				return base.GetSystemInt32(EnquadraItemAcaoMetadata.ColumnNames.IdEmissor);
			}
			
			set
			{
				if(base.SetSystemInt32(EnquadraItemAcaoMetadata.ColumnNames.IdEmissor, value))
				{
					this._UpToEmissorByIdEmissor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemAcao.TipoPapel
		/// </summary>
		virtual public System.Byte? TipoPapel
		{
			get
			{
				return base.GetSystemByte(EnquadraItemAcaoMetadata.ColumnNames.TipoPapel);
			}
			
			set
			{
				base.SetSystemByte(EnquadraItemAcaoMetadata.ColumnNames.TipoPapel, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemAcao.TipoMercado
		/// </summary>
		virtual public System.String TipoMercado
		{
			get
			{
				return base.GetSystemString(EnquadraItemAcaoMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemString(EnquadraItemAcaoMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemAcao.Bloqueado
		/// </summary>
		virtual public System.String Bloqueado
		{
			get
			{
				return base.GetSystemString(EnquadraItemAcaoMetadata.ColumnNames.Bloqueado);
			}
			
			set
			{
				base.SetSystemString(EnquadraItemAcaoMetadata.ColumnNames.Bloqueado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Emissor _UpToEmissorByIdEmissor;
		[CLSCompliant(false)]
		internal protected Setor _UpToSetorByIdSetor;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEnquadraItemAcao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdItem
			{
				get
				{
					System.Int32? data = entity.IdItem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdItem = null;
					else entity.IdItem = Convert.ToInt32(value);
				}
			}
				
			public System.String IdSetor
			{
				get
				{
					System.Int16? data = entity.IdSetor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSetor = null;
					else entity.IdSetor = Convert.ToInt16(value);
				}
			}
				
			public System.String IdEmissor
			{
				get
				{
					System.Int32? data = entity.IdEmissor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEmissor = null;
					else entity.IdEmissor = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoPapel
			{
				get
				{
					System.Byte? data = entity.TipoPapel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPapel = null;
					else entity.TipoPapel = Convert.ToByte(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.String data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToString(value);
				}
			}
				
			public System.String Bloqueado
			{
				get
				{
					System.String data = entity.Bloqueado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Bloqueado = null;
					else entity.Bloqueado = Convert.ToString(value);
				}
			}
			

			private esEnquadraItemAcao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEnquadraItemAcaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEnquadraItemAcao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EnquadraItemAcao : esEnquadraItemAcao
	{

				
		#region UpToEmissorByIdEmissor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Emissor_EnquadraItemAcao_FK1
		/// </summary>

		[XmlIgnore]
		public Emissor UpToEmissorByIdEmissor
		{
			get
			{
				if(this._UpToEmissorByIdEmissor == null
					&& IdEmissor != null					)
				{
					this._UpToEmissorByIdEmissor = new Emissor();
					this._UpToEmissorByIdEmissor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEmissorByIdEmissor", this._UpToEmissorByIdEmissor);
					this._UpToEmissorByIdEmissor.Query.Where(this._UpToEmissorByIdEmissor.Query.IdEmissor == this.IdEmissor);
					this._UpToEmissorByIdEmissor.Query.Load();
				}

				return this._UpToEmissorByIdEmissor;
			}
			
			set
			{
				this.RemovePreSave("UpToEmissorByIdEmissor");
				

				if(value == null)
				{
					this.IdEmissor = null;
					this._UpToEmissorByIdEmissor = null;
				}
				else
				{
					this.IdEmissor = value.IdEmissor;
					this._UpToEmissorByIdEmissor = value;
					this.SetPreSave("UpToEmissorByIdEmissor", this._UpToEmissorByIdEmissor);
				}
				
			}
		}
		#endregion
		

		#region UpToEnquadraItem - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - EnquadraItem_EnquadraItemAcao_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItem UpToEnquadraItem
		{
			get
			{
				if(this._UpToEnquadraItem == null
					&& IdItem != null					)
				{
					this._UpToEnquadraItem = new EnquadraItem();
					this._UpToEnquadraItem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEnquadraItem", this._UpToEnquadraItem);
					this._UpToEnquadraItem.Query.Where(this._UpToEnquadraItem.Query.IdItem == this.IdItem);
					this._UpToEnquadraItem.Query.Load();
				}

				return this._UpToEnquadraItem;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToEnquadraItem");

				if(value == null)
				{
					this._UpToEnquadraItem = null;
				}
				else
				{
					this._UpToEnquadraItem = value;
					this.SetPreSave("UpToEnquadraItem", this._UpToEnquadraItem);
				}
				
				
			} 
		}

		private EnquadraItem _UpToEnquadraItem;
		#endregion

				
		#region UpToSetorByIdSetor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Setor_EnquadraItemAcao_FK1
		/// </summary>

		[XmlIgnore]
		public Setor UpToSetorByIdSetor
		{
			get
			{
				if(this._UpToSetorByIdSetor == null
					&& IdSetor != null					)
				{
					this._UpToSetorByIdSetor = new Setor();
					this._UpToSetorByIdSetor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSetorByIdSetor", this._UpToSetorByIdSetor);
					this._UpToSetorByIdSetor.Query.Where(this._UpToSetorByIdSetor.Query.IdSetor == this.IdSetor);
					this._UpToSetorByIdSetor.Query.Load();
				}

				return this._UpToSetorByIdSetor;
			}
			
			set
			{
				this.RemovePreSave("UpToSetorByIdSetor");
				

				if(value == null)
				{
					this.IdSetor = null;
					this._UpToSetorByIdSetor = null;
				}
				else
				{
					this.IdSetor = value.IdSetor;
					this._UpToSetorByIdSetor = value;
					this.SetPreSave("UpToSetorByIdSetor", this._UpToSetorByIdSetor);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEmissorByIdEmissor != null)
			{
				this.IdEmissor = this._UpToEmissorByIdEmissor.IdEmissor;
			}
			if(!this.es.IsDeleted && this._UpToEnquadraItem != null)
			{
				this.IdItem = this._UpToEnquadraItem.IdItem;
			}
			if(!this.es.IsDeleted && this._UpToSetorByIdSetor != null)
			{
				this.IdSetor = this._UpToSetorByIdSetor.IdSetor;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEnquadraItemAcaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraItemAcaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdItem
		{
			get
			{
				return new esQueryItem(this, EnquadraItemAcaoMetadata.ColumnNames.IdItem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdSetor
		{
			get
			{
				return new esQueryItem(this, EnquadraItemAcaoMetadata.ColumnNames.IdSetor, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdEmissor
		{
			get
			{
				return new esQueryItem(this, EnquadraItemAcaoMetadata.ColumnNames.IdEmissor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoPapel
		{
			get
			{
				return new esQueryItem(this, EnquadraItemAcaoMetadata.ColumnNames.TipoPapel, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, EnquadraItemAcaoMetadata.ColumnNames.TipoMercado, esSystemType.String);
			}
		} 
		
		public esQueryItem Bloqueado
		{
			get
			{
				return new esQueryItem(this, EnquadraItemAcaoMetadata.ColumnNames.Bloqueado, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EnquadraItemAcaoCollection")]
	public partial class EnquadraItemAcaoCollection : esEnquadraItemAcaoCollection, IEnumerable<EnquadraItemAcao>
	{
		public EnquadraItemAcaoCollection()
		{

		}
		
		public static implicit operator List<EnquadraItemAcao>(EnquadraItemAcaoCollection coll)
		{
			List<EnquadraItemAcao> list = new List<EnquadraItemAcao>();
			
			foreach (EnquadraItemAcao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EnquadraItemAcaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraItemAcaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EnquadraItemAcao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EnquadraItemAcao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EnquadraItemAcaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraItemAcaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EnquadraItemAcaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EnquadraItemAcao AddNew()
		{
			EnquadraItemAcao entity = base.AddNewEntity() as EnquadraItemAcao;
			
			return entity;
		}

		public EnquadraItemAcao FindByPrimaryKey(System.Int32 idItem)
		{
			return base.FindByPrimaryKey(idItem) as EnquadraItemAcao;
		}


		#region IEnumerable<EnquadraItemAcao> Members

		IEnumerator<EnquadraItemAcao> IEnumerable<EnquadraItemAcao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EnquadraItemAcao;
			}
		}

		#endregion
		
		private EnquadraItemAcaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EnquadraItemAcao' table
	/// </summary>

	[Serializable]
	public partial class EnquadraItemAcao : esEnquadraItemAcao
	{
		public EnquadraItemAcao()
		{

		}
	
		public EnquadraItemAcao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraItemAcaoMetadata.Meta();
			}
		}
		
		
		
		override protected esEnquadraItemAcaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraItemAcaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EnquadraItemAcaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraItemAcaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EnquadraItemAcaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EnquadraItemAcaoQuery query;
	}



	[Serializable]
	public partial class EnquadraItemAcaoQuery : esEnquadraItemAcaoQuery
	{
		public EnquadraItemAcaoQuery()
		{

		}		
		
		public EnquadraItemAcaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EnquadraItemAcaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EnquadraItemAcaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EnquadraItemAcaoMetadata.ColumnNames.IdItem, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraItemAcaoMetadata.PropertyNames.IdItem;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemAcaoMetadata.ColumnNames.IdSetor, 1, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = EnquadraItemAcaoMetadata.PropertyNames.IdSetor;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemAcaoMetadata.ColumnNames.IdEmissor, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraItemAcaoMetadata.PropertyNames.IdEmissor;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemAcaoMetadata.ColumnNames.TipoPapel, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EnquadraItemAcaoMetadata.PropertyNames.TipoPapel;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemAcaoMetadata.ColumnNames.TipoMercado, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = EnquadraItemAcaoMetadata.PropertyNames.TipoMercado;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemAcaoMetadata.ColumnNames.Bloqueado, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = EnquadraItemAcaoMetadata.PropertyNames.Bloqueado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EnquadraItemAcaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdItem = "IdItem";
			 public const string IdSetor = "IdSetor";
			 public const string IdEmissor = "IdEmissor";
			 public const string TipoPapel = "TipoPapel";
			 public const string TipoMercado = "TipoMercado";
			 public const string Bloqueado = "Bloqueado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdItem = "IdItem";
			 public const string IdSetor = "IdSetor";
			 public const string IdEmissor = "IdEmissor";
			 public const string TipoPapel = "TipoPapel";
			 public const string TipoMercado = "TipoMercado";
			 public const string Bloqueado = "Bloqueado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EnquadraItemAcaoMetadata))
			{
				if(EnquadraItemAcaoMetadata.mapDelegates == null)
				{
					EnquadraItemAcaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EnquadraItemAcaoMetadata.meta == null)
				{
					EnquadraItemAcaoMetadata.meta = new EnquadraItemAcaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdItem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdSetor", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdEmissor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoPapel", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Bloqueado", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "EnquadraItemAcao";
				meta.Destination = "EnquadraItemAcao";
				
				meta.spInsert = "proc_EnquadraItemAcaoInsert";				
				meta.spUpdate = "proc_EnquadraItemAcaoUpdate";		
				meta.spDelete = "proc_EnquadraItemAcaoDelete";
				meta.spLoadAll = "proc_EnquadraItemAcaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_EnquadraItemAcaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EnquadraItemAcaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
