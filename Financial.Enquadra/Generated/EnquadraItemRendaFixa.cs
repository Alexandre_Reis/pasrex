/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 17/03/2014 16:46:09
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				

using Financial.Common;
using Financial.RendaFixa;








				




		

		
		
		
		
		





namespace Financial.Enquadra
{

	[Serializable]
	abstract public class esEnquadraItemRendaFixaCollection : esEntityCollection
	{
		public esEnquadraItemRendaFixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EnquadraItemRendaFixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esEnquadraItemRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEnquadraItemRendaFixaQuery);
		}
		#endregion
		
		virtual public EnquadraItemRendaFixa DetachEntity(EnquadraItemRendaFixa entity)
		{
			return base.DetachEntity(entity) as EnquadraItemRendaFixa;
		}
		
		virtual public EnquadraItemRendaFixa AttachEntity(EnquadraItemRendaFixa entity)
		{
			return base.AttachEntity(entity) as EnquadraItemRendaFixa;
		}
		
		virtual public void Combine(EnquadraItemRendaFixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EnquadraItemRendaFixa this[int index]
		{
			get
			{
				return base[index] as EnquadraItemRendaFixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EnquadraItemRendaFixa);
		}
	}



	[Serializable]
	abstract public class esEnquadraItemRendaFixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEnquadraItemRendaFixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esEnquadraItemRendaFixa()
		{

		}

		public esEnquadraItemRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idItem)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idItem);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idItem)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEnquadraItemRendaFixaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdItem == idItem);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idItem)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idItem);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idItem)
		{
			esEnquadraItemRendaFixaQuery query = this.GetDynamicQuery();
			query.Where(query.IdItem == idItem);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idItem)
		{
			esParameters parms = new esParameters();
			parms.Add("IdItem",idItem);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdItem": this.str.IdItem = (string)value; break;							
						case "OperacaoCompromissada": this.str.OperacaoCompromissada = (string)value; break;							
						case "TipoPapel": this.str.TipoPapel = (string)value; break;							
						case "TipoRentabilidade": this.str.TipoRentabilidade = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;							
						case "IdPapel": this.str.IdPapel = (string)value; break;							
						case "IdEmissor": this.str.IdEmissor = (string)value; break;							
						case "IdSetor": this.str.IdSetor = (string)value; break;							
						case "TipoEmissor": this.str.TipoEmissor = (string)value; break;							
						case "Bloqueado": this.str.Bloqueado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdItem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdItem = (System.Int32?)value;
							break;
						
						case "TipoPapel":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoPapel = (System.Byte?)value;
							break;
						
						case "TipoRentabilidade":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoRentabilidade = (System.Byte?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
						
						case "IdPapel":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPapel = (System.Int32?)value;
							break;
						
						case "IdEmissor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEmissor = (System.Int32?)value;
							break;
						
						case "IdSetor":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdSetor = (System.Int16?)value;
							break;
						
						case "TipoEmissor":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoEmissor = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EnquadraItemRendaFixa.IdItem
		/// </summary>
		virtual public System.Int32? IdItem
		{
			get
			{
				return base.GetSystemInt32(EnquadraItemRendaFixaMetadata.ColumnNames.IdItem);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraItemRendaFixaMetadata.ColumnNames.IdItem, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemRendaFixa.OperacaoCompromissada
		/// </summary>
		virtual public System.String OperacaoCompromissada
		{
			get
			{
				return base.GetSystemString(EnquadraItemRendaFixaMetadata.ColumnNames.OperacaoCompromissada);
			}
			
			set
			{
				base.SetSystemString(EnquadraItemRendaFixaMetadata.ColumnNames.OperacaoCompromissada, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemRendaFixa.TipoPapel
		/// </summary>
		virtual public System.Byte? TipoPapel
		{
			get
			{
				return base.GetSystemByte(EnquadraItemRendaFixaMetadata.ColumnNames.TipoPapel);
			}
			
			set
			{
				base.SetSystemByte(EnquadraItemRendaFixaMetadata.ColumnNames.TipoPapel, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemRendaFixa.TipoRentabilidade
		/// </summary>
		virtual public System.Byte? TipoRentabilidade
		{
			get
			{
				return base.GetSystemByte(EnquadraItemRendaFixaMetadata.ColumnNames.TipoRentabilidade);
			}
			
			set
			{
				base.SetSystemByte(EnquadraItemRendaFixaMetadata.ColumnNames.TipoRentabilidade, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemRendaFixa.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(EnquadraItemRendaFixaMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				if(base.SetSystemInt16(EnquadraItemRendaFixaMetadata.ColumnNames.IdIndice, value))
				{
					this._UpToIndiceByIdIndice = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemRendaFixa.IdPapel
		/// </summary>
		virtual public System.Int32? IdPapel
		{
			get
			{
				return base.GetSystemInt32(EnquadraItemRendaFixaMetadata.ColumnNames.IdPapel);
			}
			
			set
			{
				if(base.SetSystemInt32(EnquadraItemRendaFixaMetadata.ColumnNames.IdPapel, value))
				{
					this._UpToPapelRendaFixaByIdPapel = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemRendaFixa.IdEmissor
		/// </summary>
		virtual public System.Int32? IdEmissor
		{
			get
			{
				return base.GetSystemInt32(EnquadraItemRendaFixaMetadata.ColumnNames.IdEmissor);
			}
			
			set
			{
				if(base.SetSystemInt32(EnquadraItemRendaFixaMetadata.ColumnNames.IdEmissor, value))
				{
					this._UpToEmissorByIdEmissor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemRendaFixa.IdSetor
		/// </summary>
		virtual public System.Int16? IdSetor
		{
			get
			{
				return base.GetSystemInt16(EnquadraItemRendaFixaMetadata.ColumnNames.IdSetor);
			}
			
			set
			{
				base.SetSystemInt16(EnquadraItemRendaFixaMetadata.ColumnNames.IdSetor, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemRendaFixa.TipoEmissor
		/// </summary>
		virtual public System.Byte? TipoEmissor
		{
			get
			{
				return base.GetSystemByte(EnquadraItemRendaFixaMetadata.ColumnNames.TipoEmissor);
			}
			
			set
			{
				base.SetSystemByte(EnquadraItemRendaFixaMetadata.ColumnNames.TipoEmissor, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemRendaFixa.Bloqueado
		/// </summary>
		virtual public System.String Bloqueado
		{
			get
			{
				return base.GetSystemString(EnquadraItemRendaFixaMetadata.ColumnNames.Bloqueado);
			}
			
			set
			{
				base.SetSystemString(EnquadraItemRendaFixaMetadata.ColumnNames.Bloqueado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Emissor _UpToEmissorByIdEmissor;
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndice;
		[CLSCompliant(false)]
		internal protected PapelRendaFixa _UpToPapelRendaFixaByIdPapel;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEnquadraItemRendaFixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdItem
			{
				get
				{
					System.Int32? data = entity.IdItem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdItem = null;
					else entity.IdItem = Convert.ToInt32(value);
				}
			}
				
			public System.String OperacaoCompromissada
			{
				get
				{
					System.String data = entity.OperacaoCompromissada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OperacaoCompromissada = null;
					else entity.OperacaoCompromissada = Convert.ToString(value);
				}
			}
				
			public System.String TipoPapel
			{
				get
				{
					System.Byte? data = entity.TipoPapel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPapel = null;
					else entity.TipoPapel = Convert.ToByte(value);
				}
			}
				
			public System.String TipoRentabilidade
			{
				get
				{
					System.Byte? data = entity.TipoRentabilidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoRentabilidade = null;
					else entity.TipoRentabilidade = Convert.ToByte(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String IdPapel
			{
				get
				{
					System.Int32? data = entity.IdPapel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPapel = null;
					else entity.IdPapel = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEmissor
			{
				get
				{
					System.Int32? data = entity.IdEmissor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEmissor = null;
					else entity.IdEmissor = Convert.ToInt32(value);
				}
			}
				
			public System.String IdSetor
			{
				get
				{
					System.Int16? data = entity.IdSetor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSetor = null;
					else entity.IdSetor = Convert.ToInt16(value);
				}
			}
				
			public System.String TipoEmissor
			{
				get
				{
					System.Byte? data = entity.TipoEmissor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoEmissor = null;
					else entity.TipoEmissor = Convert.ToByte(value);
				}
			}
				
			public System.String Bloqueado
			{
				get
				{
					System.String data = entity.Bloqueado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Bloqueado = null;
					else entity.Bloqueado = Convert.ToString(value);
				}
			}
			

			private esEnquadraItemRendaFixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEnquadraItemRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEnquadraItemRendaFixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EnquadraItemRendaFixa : esEnquadraItemRendaFixa
	{

				
		#region UpToEmissorByIdEmissor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Emissor_EnquadraItemRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public Emissor UpToEmissorByIdEmissor
		{
			get
			{
				if(this._UpToEmissorByIdEmissor == null
					&& IdEmissor != null					)
				{
					this._UpToEmissorByIdEmissor = new Emissor();
					this._UpToEmissorByIdEmissor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEmissorByIdEmissor", this._UpToEmissorByIdEmissor);
					this._UpToEmissorByIdEmissor.Query.Where(this._UpToEmissorByIdEmissor.Query.IdEmissor == this.IdEmissor);
					this._UpToEmissorByIdEmissor.Query.Load();
				}

				return this._UpToEmissorByIdEmissor;
			}
			
			set
			{
				this.RemovePreSave("UpToEmissorByIdEmissor");
				

				if(value == null)
				{
					this.IdEmissor = null;
					this._UpToEmissorByIdEmissor = null;
				}
				else
				{
					this.IdEmissor = value.IdEmissor;
					this._UpToEmissorByIdEmissor = value;
					this.SetPreSave("UpToEmissorByIdEmissor", this._UpToEmissorByIdEmissor);
				}
				
			}
		}
		#endregion
		

		#region UpToEnquadraItem - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - EnquadraItem_EnquadraItemRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItem UpToEnquadraItem
		{
			get
			{
				if(this._UpToEnquadraItem == null
					&& IdItem != null					)
				{
					this._UpToEnquadraItem = new EnquadraItem();
					this._UpToEnquadraItem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEnquadraItem", this._UpToEnquadraItem);
					this._UpToEnquadraItem.Query.Where(this._UpToEnquadraItem.Query.IdItem == this.IdItem);
					this._UpToEnquadraItem.Query.Load();
				}

				return this._UpToEnquadraItem;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToEnquadraItem");

				if(value == null)
				{
					this._UpToEnquadraItem = null;
				}
				else
				{
					this._UpToEnquadraItem = value;
					this.SetPreSave("UpToEnquadraItem", this._UpToEnquadraItem);
				}
				
				
			} 
		}

		private EnquadraItem _UpToEnquadraItem;
		#endregion

				
		#region UpToIndiceByIdIndice - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Indice_EnquadraItemRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndice
		{
			get
			{
				if(this._UpToIndiceByIdIndice == null
					&& IdIndice != null					)
				{
					this._UpToIndiceByIdIndice = new Indice();
					this._UpToIndiceByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
					this._UpToIndiceByIdIndice.Query.Where(this._UpToIndiceByIdIndice.Query.IdIndice == this.IdIndice);
					this._UpToIndiceByIdIndice.Query.Load();
				}

				return this._UpToIndiceByIdIndice;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndice");
				

				if(value == null)
				{
					this.IdIndice = null;
					this._UpToIndiceByIdIndice = null;
				}
				else
				{
					this.IdIndice = value.IdIndice;
					this._UpToIndiceByIdIndice = value;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPapelRendaFixaByIdPapel - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PapelRendaFixa_EnquadraItemRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public PapelRendaFixa UpToPapelRendaFixaByIdPapel
		{
			get
			{
				if(this._UpToPapelRendaFixaByIdPapel == null
					&& IdPapel != null					)
				{
					this._UpToPapelRendaFixaByIdPapel = new PapelRendaFixa();
					this._UpToPapelRendaFixaByIdPapel.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPapelRendaFixaByIdPapel", this._UpToPapelRendaFixaByIdPapel);
					this._UpToPapelRendaFixaByIdPapel.Query.Where(this._UpToPapelRendaFixaByIdPapel.Query.IdPapel == this.IdPapel);
					this._UpToPapelRendaFixaByIdPapel.Query.Load();
				}

				return this._UpToPapelRendaFixaByIdPapel;
			}
			
			set
			{
				this.RemovePreSave("UpToPapelRendaFixaByIdPapel");
				

				if(value == null)
				{
					this.IdPapel = null;
					this._UpToPapelRendaFixaByIdPapel = null;
				}
				else
				{
					this.IdPapel = value.IdPapel;
					this._UpToPapelRendaFixaByIdPapel = value;
					this.SetPreSave("UpToPapelRendaFixaByIdPapel", this._UpToPapelRendaFixaByIdPapel);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEmissorByIdEmissor != null)
			{
				this.IdEmissor = this._UpToEmissorByIdEmissor.IdEmissor;
			}
			if(!this.es.IsDeleted && this._UpToEnquadraItem != null)
			{
				this.IdItem = this._UpToEnquadraItem.IdItem;
			}
			if(!this.es.IsDeleted && this._UpToPapelRendaFixaByIdPapel != null)
			{
				this.IdPapel = this._UpToPapelRendaFixaByIdPapel.IdPapel;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEnquadraItemRendaFixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraItemRendaFixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdItem
		{
			get
			{
				return new esQueryItem(this, EnquadraItemRendaFixaMetadata.ColumnNames.IdItem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem OperacaoCompromissada
		{
			get
			{
				return new esQueryItem(this, EnquadraItemRendaFixaMetadata.ColumnNames.OperacaoCompromissada, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoPapel
		{
			get
			{
				return new esQueryItem(this, EnquadraItemRendaFixaMetadata.ColumnNames.TipoPapel, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoRentabilidade
		{
			get
			{
				return new esQueryItem(this, EnquadraItemRendaFixaMetadata.ColumnNames.TipoRentabilidade, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, EnquadraItemRendaFixaMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdPapel
		{
			get
			{
				return new esQueryItem(this, EnquadraItemRendaFixaMetadata.ColumnNames.IdPapel, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEmissor
		{
			get
			{
				return new esQueryItem(this, EnquadraItemRendaFixaMetadata.ColumnNames.IdEmissor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdSetor
		{
			get
			{
				return new esQueryItem(this, EnquadraItemRendaFixaMetadata.ColumnNames.IdSetor, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TipoEmissor
		{
			get
			{
				return new esQueryItem(this, EnquadraItemRendaFixaMetadata.ColumnNames.TipoEmissor, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Bloqueado
		{
			get
			{
				return new esQueryItem(this, EnquadraItemRendaFixaMetadata.ColumnNames.Bloqueado, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EnquadraItemRendaFixaCollection")]
	public partial class EnquadraItemRendaFixaCollection : esEnquadraItemRendaFixaCollection, IEnumerable<EnquadraItemRendaFixa>
	{
		public EnquadraItemRendaFixaCollection()
		{

		}
		
		public static implicit operator List<EnquadraItemRendaFixa>(EnquadraItemRendaFixaCollection coll)
		{
			List<EnquadraItemRendaFixa> list = new List<EnquadraItemRendaFixa>();
			
			foreach (EnquadraItemRendaFixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EnquadraItemRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraItemRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EnquadraItemRendaFixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EnquadraItemRendaFixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EnquadraItemRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraItemRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EnquadraItemRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EnquadraItemRendaFixa AddNew()
		{
			EnquadraItemRendaFixa entity = base.AddNewEntity() as EnquadraItemRendaFixa;
			
			return entity;
		}

		public EnquadraItemRendaFixa FindByPrimaryKey(System.Int32 idItem)
		{
			return base.FindByPrimaryKey(idItem) as EnquadraItemRendaFixa;
		}


		#region IEnumerable<EnquadraItemRendaFixa> Members

		IEnumerator<EnquadraItemRendaFixa> IEnumerable<EnquadraItemRendaFixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EnquadraItemRendaFixa;
			}
		}

		#endregion
		
		private EnquadraItemRendaFixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EnquadraItemRendaFixa' table
	/// </summary>

	[Serializable]
	public partial class EnquadraItemRendaFixa : esEnquadraItemRendaFixa
	{
		public EnquadraItemRendaFixa()
		{

		}
	
		public EnquadraItemRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraItemRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esEnquadraItemRendaFixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraItemRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EnquadraItemRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraItemRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EnquadraItemRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EnquadraItemRendaFixaQuery query;
	}



	[Serializable]
	public partial class EnquadraItemRendaFixaQuery : esEnquadraItemRendaFixaQuery
	{
		public EnquadraItemRendaFixaQuery()
		{

		}		
		
		public EnquadraItemRendaFixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EnquadraItemRendaFixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EnquadraItemRendaFixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EnquadraItemRendaFixaMetadata.ColumnNames.IdItem, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraItemRendaFixaMetadata.PropertyNames.IdItem;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemRendaFixaMetadata.ColumnNames.OperacaoCompromissada, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = EnquadraItemRendaFixaMetadata.PropertyNames.OperacaoCompromissada;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemRendaFixaMetadata.ColumnNames.TipoPapel, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EnquadraItemRendaFixaMetadata.PropertyNames.TipoPapel;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemRendaFixaMetadata.ColumnNames.TipoRentabilidade, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EnquadraItemRendaFixaMetadata.PropertyNames.TipoRentabilidade;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemRendaFixaMetadata.ColumnNames.IdIndice, 4, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = EnquadraItemRendaFixaMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemRendaFixaMetadata.ColumnNames.IdPapel, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraItemRendaFixaMetadata.PropertyNames.IdPapel;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemRendaFixaMetadata.ColumnNames.IdEmissor, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraItemRendaFixaMetadata.PropertyNames.IdEmissor;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemRendaFixaMetadata.ColumnNames.IdSetor, 7, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = EnquadraItemRendaFixaMetadata.PropertyNames.IdSetor;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemRendaFixaMetadata.ColumnNames.TipoEmissor, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EnquadraItemRendaFixaMetadata.PropertyNames.TipoEmissor;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemRendaFixaMetadata.ColumnNames.Bloqueado, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = EnquadraItemRendaFixaMetadata.PropertyNames.Bloqueado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EnquadraItemRendaFixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdItem = "IdItem";
			 public const string OperacaoCompromissada = "OperacaoCompromissada";
			 public const string TipoPapel = "TipoPapel";
			 public const string TipoRentabilidade = "TipoRentabilidade";
			 public const string IdIndice = "IdIndice";
			 public const string IdPapel = "IdPapel";
			 public const string IdEmissor = "IdEmissor";
			 public const string IdSetor = "IdSetor";
			 public const string TipoEmissor = "TipoEmissor";
			 public const string Bloqueado = "Bloqueado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdItem = "IdItem";
			 public const string OperacaoCompromissada = "OperacaoCompromissada";
			 public const string TipoPapel = "TipoPapel";
			 public const string TipoRentabilidade = "TipoRentabilidade";
			 public const string IdIndice = "IdIndice";
			 public const string IdPapel = "IdPapel";
			 public const string IdEmissor = "IdEmissor";
			 public const string IdSetor = "IdSetor";
			 public const string TipoEmissor = "TipoEmissor";
			 public const string Bloqueado = "Bloqueado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EnquadraItemRendaFixaMetadata))
			{
				if(EnquadraItemRendaFixaMetadata.mapDelegates == null)
				{
					EnquadraItemRendaFixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EnquadraItemRendaFixaMetadata.meta == null)
				{
					EnquadraItemRendaFixaMetadata.meta = new EnquadraItemRendaFixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdItem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("OperacaoCompromissada", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TipoPapel", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoRentabilidade", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdPapel", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEmissor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdSetor", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TipoEmissor", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Bloqueado", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "EnquadraItemRendaFixa";
				meta.Destination = "EnquadraItemRendaFixa";
				
				meta.spInsert = "proc_EnquadraItemRendaFixaInsert";				
				meta.spUpdate = "proc_EnquadraItemRendaFixaUpdate";		
				meta.spDelete = "proc_EnquadraItemRendaFixaDelete";
				meta.spLoadAll = "proc_EnquadraItemRendaFixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_EnquadraItemRendaFixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EnquadraItemRendaFixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
