/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 17/03/2014 16:46:09
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				

using Financial.BMF;
				








				




		

		
		
		
		
		





namespace Financial.Enquadra
{

	[Serializable]
	abstract public class esEnquadraItemOpcaoBMFCollection : esEntityCollection
	{
		public esEnquadraItemOpcaoBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EnquadraItemOpcaoBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esEnquadraItemOpcaoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEnquadraItemOpcaoBMFQuery);
		}
		#endregion
		
		virtual public EnquadraItemOpcaoBMF DetachEntity(EnquadraItemOpcaoBMF entity)
		{
			return base.DetachEntity(entity) as EnquadraItemOpcaoBMF;
		}
		
		virtual public EnquadraItemOpcaoBMF AttachEntity(EnquadraItemOpcaoBMF entity)
		{
			return base.AttachEntity(entity) as EnquadraItemOpcaoBMF;
		}
		
		virtual public void Combine(EnquadraItemOpcaoBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EnquadraItemOpcaoBMF this[int index]
		{
			get
			{
				return base[index] as EnquadraItemOpcaoBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EnquadraItemOpcaoBMF);
		}
	}



	[Serializable]
	abstract public class esEnquadraItemOpcaoBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEnquadraItemOpcaoBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esEnquadraItemOpcaoBMF()
		{

		}

		public esEnquadraItemOpcaoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idItem)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idItem);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idItem)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEnquadraItemOpcaoBMFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdItem == idItem);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idItem)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idItem);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idItem)
		{
			esEnquadraItemOpcaoBMFQuery query = this.GetDynamicQuery();
			query.Where(query.IdItem == idItem);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idItem)
		{
			esParameters parms = new esParameters();
			parms.Add("IdItem",idItem);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdItem": this.str.IdItem = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdItem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdItem = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EnquadraItemOpcaoBMF.IdItem
		/// </summary>
		virtual public System.Int32? IdItem
		{
			get
			{
				return base.GetSystemInt32(EnquadraItemOpcaoBMFMetadata.ColumnNames.IdItem);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraItemOpcaoBMFMetadata.ColumnNames.IdItem, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemOpcaoBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(EnquadraItemOpcaoBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				if(base.SetSystemString(EnquadraItemOpcaoBMFMetadata.ColumnNames.CdAtivoBMF, value))
				{
					this._UpToClasseBMFByCdAtivoBMF = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected ClasseBMF _UpToClasseBMFByCdAtivoBMF;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEnquadraItemOpcaoBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdItem
			{
				get
				{
					System.Int32? data = entity.IdItem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdItem = null;
					else entity.IdItem = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
			

			private esEnquadraItemOpcaoBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEnquadraItemOpcaoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEnquadraItemOpcaoBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EnquadraItemOpcaoBMF : esEnquadraItemOpcaoBMF
	{

				
		#region UpToClasseBMFByCdAtivoBMF - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ClasseBMF_EnquadraItemOpcaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public ClasseBMF UpToClasseBMFByCdAtivoBMF
		{
			get
			{
				if(this._UpToClasseBMFByCdAtivoBMF == null
					&& CdAtivoBMF != null					)
				{
					this._UpToClasseBMFByCdAtivoBMF = new ClasseBMF();
					this._UpToClasseBMFByCdAtivoBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClasseBMFByCdAtivoBMF", this._UpToClasseBMFByCdAtivoBMF);
					this._UpToClasseBMFByCdAtivoBMF.Query.Where(this._UpToClasseBMFByCdAtivoBMF.Query.CdAtivoBMF == this.CdAtivoBMF);
					this._UpToClasseBMFByCdAtivoBMF.Query.Load();
				}

				return this._UpToClasseBMFByCdAtivoBMF;
			}
			
			set
			{
				this.RemovePreSave("UpToClasseBMFByCdAtivoBMF");
				

				if(value == null)
				{
					this.CdAtivoBMF = null;
					this._UpToClasseBMFByCdAtivoBMF = null;
				}
				else
				{
					this.CdAtivoBMF = value.CdAtivoBMF;
					this._UpToClasseBMFByCdAtivoBMF = value;
					this.SetPreSave("UpToClasseBMFByCdAtivoBMF", this._UpToClasseBMFByCdAtivoBMF);
				}
				
			}
		}
		#endregion
		

		#region UpToEnquadraItem - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - EnquadraItem_EnquadraItemOpcaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItem UpToEnquadraItem
		{
			get
			{
				if(this._UpToEnquadraItem == null
					&& IdItem != null					)
				{
					this._UpToEnquadraItem = new EnquadraItem();
					this._UpToEnquadraItem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEnquadraItem", this._UpToEnquadraItem);
					this._UpToEnquadraItem.Query.Where(this._UpToEnquadraItem.Query.IdItem == this.IdItem);
					this._UpToEnquadraItem.Query.Load();
				}

				return this._UpToEnquadraItem;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToEnquadraItem");

				if(value == null)
				{
					this._UpToEnquadraItem = null;
				}
				else
				{
					this._UpToEnquadraItem = value;
					this.SetPreSave("UpToEnquadraItem", this._UpToEnquadraItem);
				}
				
				
			} 
		}

		private EnquadraItem _UpToEnquadraItem;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEnquadraItem != null)
			{
				this.IdItem = this._UpToEnquadraItem.IdItem;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEnquadraItemOpcaoBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraItemOpcaoBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem IdItem
		{
			get
			{
				return new esQueryItem(this, EnquadraItemOpcaoBMFMetadata.ColumnNames.IdItem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, EnquadraItemOpcaoBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EnquadraItemOpcaoBMFCollection")]
	public partial class EnquadraItemOpcaoBMFCollection : esEnquadraItemOpcaoBMFCollection, IEnumerable<EnquadraItemOpcaoBMF>
	{
		public EnquadraItemOpcaoBMFCollection()
		{

		}
		
		public static implicit operator List<EnquadraItemOpcaoBMF>(EnquadraItemOpcaoBMFCollection coll)
		{
			List<EnquadraItemOpcaoBMF> list = new List<EnquadraItemOpcaoBMF>();
			
			foreach (EnquadraItemOpcaoBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EnquadraItemOpcaoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraItemOpcaoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EnquadraItemOpcaoBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EnquadraItemOpcaoBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EnquadraItemOpcaoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraItemOpcaoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EnquadraItemOpcaoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EnquadraItemOpcaoBMF AddNew()
		{
			EnquadraItemOpcaoBMF entity = base.AddNewEntity() as EnquadraItemOpcaoBMF;
			
			return entity;
		}

		public EnquadraItemOpcaoBMF FindByPrimaryKey(System.Int32 idItem)
		{
			return base.FindByPrimaryKey(idItem) as EnquadraItemOpcaoBMF;
		}


		#region IEnumerable<EnquadraItemOpcaoBMF> Members

		IEnumerator<EnquadraItemOpcaoBMF> IEnumerable<EnquadraItemOpcaoBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EnquadraItemOpcaoBMF;
			}
		}

		#endregion
		
		private EnquadraItemOpcaoBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EnquadraItemOpcaoBMF' table
	/// </summary>

	[Serializable]
	public partial class EnquadraItemOpcaoBMF : esEnquadraItemOpcaoBMF
	{
		public EnquadraItemOpcaoBMF()
		{

		}
	
		public EnquadraItemOpcaoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraItemOpcaoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esEnquadraItemOpcaoBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraItemOpcaoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EnquadraItemOpcaoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraItemOpcaoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EnquadraItemOpcaoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EnquadraItemOpcaoBMFQuery query;
	}



	[Serializable]
	public partial class EnquadraItemOpcaoBMFQuery : esEnquadraItemOpcaoBMFQuery
	{
		public EnquadraItemOpcaoBMFQuery()
		{

		}		
		
		public EnquadraItemOpcaoBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EnquadraItemOpcaoBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EnquadraItemOpcaoBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EnquadraItemOpcaoBMFMetadata.ColumnNames.IdItem, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraItemOpcaoBMFMetadata.PropertyNames.IdItem;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemOpcaoBMFMetadata.ColumnNames.CdAtivoBMF, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = EnquadraItemOpcaoBMFMetadata.PropertyNames.CdAtivoBMF;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EnquadraItemOpcaoBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdItem = "IdItem";
			 public const string CdAtivoBMF = "CdAtivoBMF";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdItem = "IdItem";
			 public const string CdAtivoBMF = "CdAtivoBMF";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EnquadraItemOpcaoBMFMetadata))
			{
				if(EnquadraItemOpcaoBMFMetadata.mapDelegates == null)
				{
					EnquadraItemOpcaoBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EnquadraItemOpcaoBMFMetadata.meta == null)
				{
					EnquadraItemOpcaoBMFMetadata.meta = new EnquadraItemOpcaoBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdItem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "EnquadraItemOpcaoBMF";
				meta.Destination = "EnquadraItemOpcaoBMF";
				
				meta.spInsert = "proc_EnquadraItemOpcaoBMFInsert";				
				meta.spUpdate = "proc_EnquadraItemOpcaoBMFUpdate";		
				meta.spDelete = "proc_EnquadraItemOpcaoBMFDelete";
				meta.spLoadAll = "proc_EnquadraItemOpcaoBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_EnquadraItemOpcaoBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EnquadraItemOpcaoBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
