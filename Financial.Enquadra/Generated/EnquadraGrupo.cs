/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 17/03/2014 16:46:06
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Enquadra
{

	[Serializable]
	abstract public class esEnquadraGrupoCollection : esEntityCollection
	{
		public esEnquadraGrupoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EnquadraGrupoCollection";
		}

		#region Query Logic
		protected void InitQuery(esEnquadraGrupoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEnquadraGrupoQuery);
		}
		#endregion
		
		virtual public EnquadraGrupo DetachEntity(EnquadraGrupo entity)
		{
			return base.DetachEntity(entity) as EnquadraGrupo;
		}
		
		virtual public EnquadraGrupo AttachEntity(EnquadraGrupo entity)
		{
			return base.AttachEntity(entity) as EnquadraGrupo;
		}
		
		virtual public void Combine(EnquadraGrupoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EnquadraGrupo this[int index]
		{
			get
			{
				return base[index] as EnquadraGrupo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EnquadraGrupo);
		}
	}



	[Serializable]
	abstract public class esEnquadraGrupo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEnquadraGrupoQuery GetDynamicQuery()
		{
			return null;
		}

		public esEnquadraGrupo()
		{

		}

		public esEnquadraGrupo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idGrupo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupo);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idGrupo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEnquadraGrupoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdGrupo == idGrupo);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idGrupo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupo);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idGrupo)
		{
			esEnquadraGrupoQuery query = this.GetDynamicQuery();
			query.Where(query.IdGrupo == idGrupo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idGrupo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdGrupo",idGrupo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdGrupo": this.str.IdGrupo = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupo = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EnquadraGrupo.IdGrupo
		/// </summary>
		virtual public System.Int32? IdGrupo
		{
			get
			{
				return base.GetSystemInt32(EnquadraGrupoMetadata.ColumnNames.IdGrupo);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraGrupoMetadata.ColumnNames.IdGrupo, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraGrupo.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(EnquadraGrupoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(EnquadraGrupoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEnquadraGrupo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdGrupo
			{
				get
				{
					System.Int32? data = entity.IdGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupo = null;
					else entity.IdGrupo = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esEnquadraGrupo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEnquadraGrupoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEnquadraGrupo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EnquadraGrupo : esEnquadraGrupo
	{

		#region UpToEnquadraItemCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - EnquadraGrupo_EnquadraGrupoItem_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemCollection UpToEnquadraItemCollection
		{
			get
			{
				if(this._UpToEnquadraItemCollection == null)
				{
					this._UpToEnquadraItemCollection = new EnquadraItemCollection();
					this._UpToEnquadraItemCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToEnquadraItemCollection", this._UpToEnquadraItemCollection);
					this._UpToEnquadraItemCollection.ManyToManyEnquadraGrupoCollection(this.IdGrupo);
				}

				return this._UpToEnquadraItemCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToEnquadraItemCollection != null) 
				{ 
					this.RemovePostSave("UpToEnquadraItemCollection"); 
					this._UpToEnquadraItemCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - EnquadraGrupo_EnquadraGrupoItem_FK1
		/// </summary>
		public void AssociateEnquadraItemCollection(EnquadraItem entity)
		{
			if (this._EnquadraGrupoItemCollection == null)
			{
				this._EnquadraGrupoItemCollection = new EnquadraGrupoItemCollection();
				this._EnquadraGrupoItemCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("EnquadraGrupoItemCollection", this._EnquadraGrupoItemCollection);
			}

			EnquadraGrupoItem obj = this._EnquadraGrupoItemCollection.AddNew();
			obj.IdGrupo = this.IdGrupo;
			obj.IdItem = entity.IdItem;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - EnquadraGrupo_EnquadraGrupoItem_FK1
		/// </summary>
		public void DissociateEnquadraItemCollection(EnquadraItem entity)
		{
			if (this._EnquadraGrupoItemCollection == null)
			{
				this._EnquadraGrupoItemCollection = new EnquadraGrupoItemCollection();
				this._EnquadraGrupoItemCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("EnquadraGrupoItemCollection", this._EnquadraGrupoItemCollection);
			}

			EnquadraGrupoItem obj = this._EnquadraGrupoItemCollection.AddNew();
			obj.IdGrupo = this.IdGrupo;
			obj.IdItem = entity.IdItem;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private EnquadraItemCollection _UpToEnquadraItemCollection;
		private EnquadraGrupoItemCollection _EnquadraGrupoItemCollection;
		#endregion

				
		#region EnquadraGrupoItemCollectionByIdGrupo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EnquadraGrupo_EnquadraGrupoItem_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraGrupoItemCollection EnquadraGrupoItemCollectionByIdGrupo
		{
			get
			{
				if(this._EnquadraGrupoItemCollectionByIdGrupo == null)
				{
					this._EnquadraGrupoItemCollectionByIdGrupo = new EnquadraGrupoItemCollection();
					this._EnquadraGrupoItemCollectionByIdGrupo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraGrupoItemCollectionByIdGrupo", this._EnquadraGrupoItemCollectionByIdGrupo);
				
					if(this.IdGrupo != null)
					{
						this._EnquadraGrupoItemCollectionByIdGrupo.Query.Where(this._EnquadraGrupoItemCollectionByIdGrupo.Query.IdGrupo == this.IdGrupo);
						this._EnquadraGrupoItemCollectionByIdGrupo.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraGrupoItemCollectionByIdGrupo.fks.Add(EnquadraGrupoItemMetadata.ColumnNames.IdGrupo, this.IdGrupo);
					}
				}

				return this._EnquadraGrupoItemCollectionByIdGrupo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraGrupoItemCollectionByIdGrupo != null) 
				{ 
					this.RemovePostSave("EnquadraGrupoItemCollectionByIdGrupo"); 
					this._EnquadraGrupoItemCollectionByIdGrupo = null;
					
				} 
			} 			
		}

		private EnquadraGrupoItemCollection _EnquadraGrupoItemCollectionByIdGrupo;
		#endregion

				
		#region EnquadraRegraCollectionByIdGrupoBase - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EnquadraGrupo_EnquadraRegra_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraRegraCollection EnquadraRegraCollectionByIdGrupoBase
		{
			get
			{
				if(this._EnquadraRegraCollectionByIdGrupoBase == null)
				{
					this._EnquadraRegraCollectionByIdGrupoBase = new EnquadraRegraCollection();
					this._EnquadraRegraCollectionByIdGrupoBase.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraRegraCollectionByIdGrupoBase", this._EnquadraRegraCollectionByIdGrupoBase);
				
					if(this.IdGrupo != null)
					{
						this._EnquadraRegraCollectionByIdGrupoBase.Query.Where(this._EnquadraRegraCollectionByIdGrupoBase.Query.IdGrupoBase == this.IdGrupo);
						this._EnquadraRegraCollectionByIdGrupoBase.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraRegraCollectionByIdGrupoBase.fks.Add(EnquadraRegraMetadata.ColumnNames.IdGrupoBase, this.IdGrupo);
					}
				}

				return this._EnquadraRegraCollectionByIdGrupoBase;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraRegraCollectionByIdGrupoBase != null) 
				{ 
					this.RemovePostSave("EnquadraRegraCollectionByIdGrupoBase"); 
					this._EnquadraRegraCollectionByIdGrupoBase = null;
					
				} 
			} 			
		}

		private EnquadraRegraCollection _EnquadraRegraCollectionByIdGrupoBase;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "EnquadraGrupoItemCollectionByIdGrupo", typeof(EnquadraGrupoItemCollection), new EnquadraGrupoItem()));
			props.Add(new esPropertyDescriptor(this, "EnquadraRegraCollectionByIdGrupoBase", typeof(EnquadraRegraCollection), new EnquadraRegra()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._EnquadraGrupoItemCollection != null)
			{
				foreach(EnquadraGrupoItem obj in this._EnquadraGrupoItemCollection)
				{
					if(obj.es.IsAdded)
					{
						obj.IdGrupo = this.IdGrupo;
					}
				}
			}
			if(this._EnquadraGrupoItemCollectionByIdGrupo != null)
			{
				foreach(EnquadraGrupoItem obj in this._EnquadraGrupoItemCollectionByIdGrupo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdGrupo = this.IdGrupo;
					}
				}
			}
			if(this._EnquadraRegraCollectionByIdGrupoBase != null)
			{
				foreach(EnquadraRegra obj in this._EnquadraRegraCollectionByIdGrupoBase)
				{
					if(obj.es.IsAdded)
					{
						obj.IdGrupoBase = this.IdGrupo;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}
	
	public partial class EnquadraGrupoCollection : esEnquadraGrupoCollection
	{
		#region ManyToManyEnquadraItemCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyEnquadraItemCollection(System.Int32? IdItem)
		{
			esParameters parms = new esParameters();
			parms.Add("IdItem", IdItem);
	
			return base.Load( esQueryType.ManyToMany, 
				"EnquadraGrupo,EnquadraGrupoItem|IdGrupo,IdGrupo|IdItem",	parms);
		}
		#endregion
	}




	[Serializable]
	abstract public class esEnquadraGrupoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraGrupoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdGrupo
		{
			get
			{
				return new esQueryItem(this, EnquadraGrupoMetadata.ColumnNames.IdGrupo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, EnquadraGrupoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EnquadraGrupoCollection")]
	public partial class EnquadraGrupoCollection : esEnquadraGrupoCollection, IEnumerable<EnquadraGrupo>
	{
		public EnquadraGrupoCollection()
		{

		}
		
		public static implicit operator List<EnquadraGrupo>(EnquadraGrupoCollection coll)
		{
			List<EnquadraGrupo> list = new List<EnquadraGrupo>();
			
			foreach (EnquadraGrupo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EnquadraGrupoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraGrupoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EnquadraGrupo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EnquadraGrupo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EnquadraGrupoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraGrupoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EnquadraGrupoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EnquadraGrupo AddNew()
		{
			EnquadraGrupo entity = base.AddNewEntity() as EnquadraGrupo;
			
			return entity;
		}

		public EnquadraGrupo FindByPrimaryKey(System.Int32 idGrupo)
		{
			return base.FindByPrimaryKey(idGrupo) as EnquadraGrupo;
		}


		#region IEnumerable<EnquadraGrupo> Members

		IEnumerator<EnquadraGrupo> IEnumerable<EnquadraGrupo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EnquadraGrupo;
			}
		}

		#endregion
		
		private EnquadraGrupoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EnquadraGrupo' table
	/// </summary>

	[Serializable]
	public partial class EnquadraGrupo : esEnquadraGrupo
	{
		public EnquadraGrupo()
		{

		}
	
		public EnquadraGrupo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraGrupoMetadata.Meta();
			}
		}
		
		
		
		override protected esEnquadraGrupoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraGrupoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EnquadraGrupoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraGrupoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EnquadraGrupoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EnquadraGrupoQuery query;
	}



	[Serializable]
	public partial class EnquadraGrupoQuery : esEnquadraGrupoQuery
	{
		public EnquadraGrupoQuery()
		{

		}		
		
		public EnquadraGrupoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EnquadraGrupoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EnquadraGrupoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EnquadraGrupoMetadata.ColumnNames.IdGrupo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraGrupoMetadata.PropertyNames.IdGrupo;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraGrupoMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = EnquadraGrupoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EnquadraGrupoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdGrupo = "IdGrupo";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdGrupo = "IdGrupo";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EnquadraGrupoMetadata))
			{
				if(EnquadraGrupoMetadata.mapDelegates == null)
				{
					EnquadraGrupoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EnquadraGrupoMetadata.meta == null)
				{
					EnquadraGrupoMetadata.meta = new EnquadraGrupoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdGrupo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "EnquadraGrupo";
				meta.Destination = "EnquadraGrupo";
				
				meta.spInsert = "proc_EnquadraGrupoInsert";				
				meta.spUpdate = "proc_EnquadraGrupoUpdate";		
				meta.spDelete = "proc_EnquadraGrupoDelete";
				meta.spLoadAll = "proc_EnquadraGrupoLoadAll";
				meta.spLoadByPrimaryKey = "proc_EnquadraGrupoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EnquadraGrupoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
