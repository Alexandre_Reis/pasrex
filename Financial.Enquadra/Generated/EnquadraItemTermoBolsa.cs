/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 17/03/2014 16:46:09
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Enquadra
{

	[Serializable]
	abstract public class esEnquadraItemTermoBolsaCollection : esEntityCollection
	{
		public esEnquadraItemTermoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EnquadraItemTermoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esEnquadraItemTermoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEnquadraItemTermoBolsaQuery);
		}
		#endregion
		
		virtual public EnquadraItemTermoBolsa DetachEntity(EnquadraItemTermoBolsa entity)
		{
			return base.DetachEntity(entity) as EnquadraItemTermoBolsa;
		}
		
		virtual public EnquadraItemTermoBolsa AttachEntity(EnquadraItemTermoBolsa entity)
		{
			return base.AttachEntity(entity) as EnquadraItemTermoBolsa;
		}
		
		virtual public void Combine(EnquadraItemTermoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EnquadraItemTermoBolsa this[int index]
		{
			get
			{
				return base[index] as EnquadraItemTermoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EnquadraItemTermoBolsa);
		}
	}



	[Serializable]
	abstract public class esEnquadraItemTermoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEnquadraItemTermoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esEnquadraItemTermoBolsa()
		{

		}

		public esEnquadraItemTermoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idItem)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idItem);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idItem)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEnquadraItemTermoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdItem == idItem);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idItem)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idItem);
			else
				return LoadByPrimaryKeyStoredProcedure(idItem);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idItem)
		{
			esEnquadraItemTermoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdItem == idItem);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idItem)
		{
			esParameters parms = new esParameters();
			parms.Add("IdItem",idItem);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdItem": this.str.IdItem = (string)value; break;							
						case "Posicao": this.str.Posicao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdItem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdItem = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EnquadraItemTermoBolsa.IdItem
		/// </summary>
		virtual public System.Int32? IdItem
		{
			get
			{
				return base.GetSystemInt32(EnquadraItemTermoBolsaMetadata.ColumnNames.IdItem);
			}
			
			set
			{
				base.SetSystemInt32(EnquadraItemTermoBolsaMetadata.ColumnNames.IdItem, value);
			}
		}
		
		/// <summary>
		/// Maps to EnquadraItemTermoBolsa.Posicao
		/// </summary>
		virtual public System.String Posicao
		{
			get
			{
				return base.GetSystemString(EnquadraItemTermoBolsaMetadata.ColumnNames.Posicao);
			}
			
			set
			{
				base.SetSystemString(EnquadraItemTermoBolsaMetadata.ColumnNames.Posicao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEnquadraItemTermoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdItem
			{
				get
				{
					System.Int32? data = entity.IdItem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdItem = null;
					else entity.IdItem = Convert.ToInt32(value);
				}
			}
				
			public System.String Posicao
			{
				get
				{
					System.String data = entity.Posicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Posicao = null;
					else entity.Posicao = Convert.ToString(value);
				}
			}
			

			private esEnquadraItemTermoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEnquadraItemTermoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEnquadraItemTermoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EnquadraItemTermoBolsa : esEnquadraItemTermoBolsa
	{

		#region UpToEnquadraItem - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - EnquadraItem_EnquadraItemTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItem UpToEnquadraItem
		{
			get
			{
				if(this._UpToEnquadraItem == null
					&& IdItem != null					)
				{
					this._UpToEnquadraItem = new EnquadraItem();
					this._UpToEnquadraItem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEnquadraItem", this._UpToEnquadraItem);
					this._UpToEnquadraItem.Query.Where(this._UpToEnquadraItem.Query.IdItem == this.IdItem);
					this._UpToEnquadraItem.Query.Load();
				}

				return this._UpToEnquadraItem;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToEnquadraItem");

				if(value == null)
				{
					this._UpToEnquadraItem = null;
				}
				else
				{
					this._UpToEnquadraItem = value;
					this.SetPreSave("UpToEnquadraItem", this._UpToEnquadraItem);
				}
				
				
			} 
		}

		private EnquadraItem _UpToEnquadraItem;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEnquadraItem != null)
			{
				this.IdItem = this._UpToEnquadraItem.IdItem;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEnquadraItemTermoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraItemTermoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdItem
		{
			get
			{
				return new esQueryItem(this, EnquadraItemTermoBolsaMetadata.ColumnNames.IdItem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Posicao
		{
			get
			{
				return new esQueryItem(this, EnquadraItemTermoBolsaMetadata.ColumnNames.Posicao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EnquadraItemTermoBolsaCollection")]
	public partial class EnquadraItemTermoBolsaCollection : esEnquadraItemTermoBolsaCollection, IEnumerable<EnquadraItemTermoBolsa>
	{
		public EnquadraItemTermoBolsaCollection()
		{

		}
		
		public static implicit operator List<EnquadraItemTermoBolsa>(EnquadraItemTermoBolsaCollection coll)
		{
			List<EnquadraItemTermoBolsa> list = new List<EnquadraItemTermoBolsa>();
			
			foreach (EnquadraItemTermoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EnquadraItemTermoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraItemTermoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EnquadraItemTermoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EnquadraItemTermoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EnquadraItemTermoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraItemTermoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EnquadraItemTermoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EnquadraItemTermoBolsa AddNew()
		{
			EnquadraItemTermoBolsa entity = base.AddNewEntity() as EnquadraItemTermoBolsa;
			
			return entity;
		}

		public EnquadraItemTermoBolsa FindByPrimaryKey(System.Int32 idItem)
		{
			return base.FindByPrimaryKey(idItem) as EnquadraItemTermoBolsa;
		}


		#region IEnumerable<EnquadraItemTermoBolsa> Members

		IEnumerator<EnquadraItemTermoBolsa> IEnumerable<EnquadraItemTermoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EnquadraItemTermoBolsa;
			}
		}

		#endregion
		
		private EnquadraItemTermoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EnquadraItemTermoBolsa' table
	/// </summary>

	[Serializable]
	public partial class EnquadraItemTermoBolsa : esEnquadraItemTermoBolsa
	{
		public EnquadraItemTermoBolsa()
		{

		}
	
		public EnquadraItemTermoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EnquadraItemTermoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esEnquadraItemTermoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EnquadraItemTermoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EnquadraItemTermoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EnquadraItemTermoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EnquadraItemTermoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EnquadraItemTermoBolsaQuery query;
	}



	[Serializable]
	public partial class EnquadraItemTermoBolsaQuery : esEnquadraItemTermoBolsaQuery
	{
		public EnquadraItemTermoBolsaQuery()
		{

		}		
		
		public EnquadraItemTermoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EnquadraItemTermoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EnquadraItemTermoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EnquadraItemTermoBolsaMetadata.ColumnNames.IdItem, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EnquadraItemTermoBolsaMetadata.PropertyNames.IdItem;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EnquadraItemTermoBolsaMetadata.ColumnNames.Posicao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = EnquadraItemTermoBolsaMetadata.PropertyNames.Posicao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EnquadraItemTermoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdItem = "IdItem";
			 public const string Posicao = "Posicao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdItem = "IdItem";
			 public const string Posicao = "Posicao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EnquadraItemTermoBolsaMetadata))
			{
				if(EnquadraItemTermoBolsaMetadata.mapDelegates == null)
				{
					EnquadraItemTermoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EnquadraItemTermoBolsaMetadata.meta == null)
				{
					EnquadraItemTermoBolsaMetadata.meta = new EnquadraItemTermoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdItem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Posicao", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "EnquadraItemTermoBolsa";
				meta.Destination = "EnquadraItemTermoBolsa";
				
				meta.spInsert = "proc_EnquadraItemTermoBolsaInsert";				
				meta.spUpdate = "proc_EnquadraItemTermoBolsaUpdate";		
				meta.spDelete = "proc_EnquadraItemTermoBolsaDelete";
				meta.spLoadAll = "proc_EnquadraItemTermoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_EnquadraItemTermoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EnquadraItemTermoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
