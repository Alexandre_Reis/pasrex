﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;
using Financial.Enquadra.Exceptions;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Enquadra.Enums;
using Financial.Bolsa;
using Financial.Investidor;
using Financial.Common;
using Financial.RendaFixa;
using Financial.Bolsa.Enums;
using Financial.Bolsa.Exceptions;

namespace Financial.Enquadra
{
	public partial class EnquadraRegra : esEnquadraRegra
	{
        public void CalculaLimiteAbsoluto(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data)
        {
            this.CalculaLimiteAbsoluto(enquadraRegra, idCarteira, historico, data, false);
        }

        public void CalculaConcentracaoCotista(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data)
        {
            this.CalculaConcentracaoCotista(enquadraRegra, idCarteira, historico, data, false);
        }

        public void CalculaNumeroCotista(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data)
        {
            this.CalculaNumeroCotista(enquadraRegra, idCarteira, historico, data, false);
        }

        public void CalculaPosicaoDescoberto(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data)
        {
            this.CalculaPosicaoDescoberto(enquadraRegra, idCarteira, historico, data, false);
        }

        public void CalculaConcentracao(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data)
        {
            this.CalculaConcentracao(enquadraRegra, idCarteira, historico, data, false);
        }
                
        /// <summary>
        /// Calcula limite absoluto de posições em ativos versus um valor base.
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        public EnquadraResultado CalculaLimiteAbsoluto(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data, bool realTime)
        {
            int idRegra = enquadraRegra.IdRegra.Value;
            int idGrupoCriterio = enquadraRegra.IdGrupoCriterio.Value;
            int idGrupoBase = enquadraRegra.IdGrupoBase.Value;

            EnquadraGrupo enquadraGrupo = new EnquadraGrupo();
            decimal valorCriterio = enquadraGrupo.RetornaValorGrupoItem(idCarteira, idGrupoCriterio, historico, data, realTime);

            enquadraGrupo = new EnquadraGrupo();
            decimal valorBase = enquadraGrupo.RetornaValorGrupoItem(idCarteira, idGrupoBase, historico, data, realTime);

            string enquadrado = "S";

            decimal resultado;
            if (valorBase == 0)
            {
                if (idGrupoBase == (int)ListaGrupoFixoEnquadra.PL_D0 || idGrupoBase == (int)ListaGrupoFixoEnquadra.PL_D1)
                {
                    EnquadraGrupo enquadraGrupoDescricao = new EnquadraGrupo();
                    enquadraGrupoDescricao.LoadByPrimaryKey(idGrupoBase);
                    throw new ValorBaseZeradoException("Valor base está zerado para a carteira " + idCarteira + ", grupo " +
                                        enquadraGrupoDescricao.Descricao + ". Para evitar esta mensagem, pode ser desligado o enquadramento na carteira.");
                }
                else
                {
                    resultado = 0;
                    
                    if (enquadraRegra.Minimo.HasValue)
                    {
                        decimal minimo = enquadraRegra.Minimo.Value;
                        if (valorCriterio < minimo)
                            enquadrado = "N";
                    }
                    if (enquadraRegra.Maximo.HasValue)
                    {
                        if (valorCriterio > 0)
                        {
                            enquadrado = "N";
                        }
                    }                    
                }
            }
            else
            {
                resultado = Utilitario.Truncate(valorCriterio / valorBase, 4) * 100;

                if (enquadraRegra.Minimo.HasValue)
                {
                    decimal minimo = enquadraRegra.Minimo.Value;
                    if (resultado < minimo)
                        enquadrado = "N";
                }
                if (enquadraRegra.Maximo.HasValue)
                {
                    decimal maximo = enquadraRegra.Maximo.Value;
                    if (resultado > maximo)
                        enquadrado = "N";
                }
            }


            EnquadraResultado enquadraResultado = new EnquadraResultado();
            enquadraResultado.IdRegra = idRegra;
            enquadraResultado.IdCarteira = idCarteira;
            enquadraResultado.Data = data;
            enquadraResultado.ValorCriterio = valorCriterio;
            enquadraResultado.ValorBase = valorBase;
            enquadraResultado.Resultado = resultado;
            enquadraResultado.Enquadrado = enquadrado;

            if (!realTime)
            {
                enquadraResultado.Save();            
            }

            return enquadraResultado;
        }

        /// <summary>
        /// Calcula concentração % de saldos por cotista em relação ao todo (PL).
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        public EnquadraResultado CalculaConcentracaoCotista(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data, bool realTime) 
        {
            EnquadraResultadoDetalheCollection enquadraResultadoDetalheCollection = new EnquadraResultadoDetalheCollection();
            
            #region Busca o PL de D0
            //Busca o tipo de cota (abert ou fech)
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TipoCota);
            carteira.LoadByPrimaryKey(campos, idCarteira);
            int tipoCota = carteira.TipoCota.Value;
            //

            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.BuscaValorPatrimonioDia(idCarteira, data);

            decimal valorPL;
            if (tipoCota == (int)TipoCotaFundo.Abertura)
                valorPL = historicoCota.PLAbertura.Value;
            else
                valorPL = historicoCota.PLFechamento.Value;
            #endregion        

            int idRegra = enquadraRegra.IdRegra.Value;

            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.BuscaPosicaoCotistaAgrupadoEnquadra(idCarteira);

            string enquadradoCotista = "S";
            string enquadrado = "S";
            decimal resultadoFinal = 0;
            decimal valorFinal = 0;
            decimal menorPercentual = 999999;
            decimal maiorPercentual = 0;
            decimal menorValor = 999999999999;
            decimal maiorValor = 0;
            for (int i = 0; i < posicaoCotistaCollection.Count; i++)
            {
                enquadradoCotista = "S";
                PosicaoCotista posicaoCotista = posicaoCotistaCollection[i];
                int idCotista = posicaoCotista.IdCotista.Value;
                decimal valor = posicaoCotista.ValorBruto.Value;

                decimal resultado = Utilitario.Truncate(valor / valorPL, 4) * 100;

                if (enquadraRegra.Minimo.HasValue)
                {
                    if (resultado < menorPercentual)
                    {
                        menorPercentual = resultado;
                        menorValor = valor;
                    }
                    
                    decimal minimo = enquadraRegra.Minimo.Value;
                    if (resultado < minimo)
                    {
                        enquadradoCotista = "N";
                        enquadrado = "N";
                    }                    
                }
                if (enquadraRegra.Maximo.HasValue)
                {
                    if (resultado > maiorPercentual)
                    {
                        maiorPercentual = resultado;
                        maiorValor = valor;
                    }
                    
                    decimal maximo = enquadraRegra.Maximo.Value;
                    if (resultado > maximo)
                    {
                        enquadradoCotista = "N";
                        enquadrado = "N";
                    }                    
                }

                //Busca o nome do cotista
                Cotista cotista = new Cotista();                	      
			    campos = new List<esQueryItem>();
	      	    campos.Add(cotista.Query.Nome);
                cotista.LoadByPrimaryKey(campos, idCotista);
                string nomeCotista = cotista.Nome;
                //

                if (!realTime)
                {
                    //Salva o resultado em EnquadraResultadoDetalhe
                    EnquadraResultadoDetalhe enquadraResultadoDetalhe = new EnquadraResultadoDetalhe();
                    enquadraResultadoDetalhe.IdRegra = idRegra;
                    enquadraResultadoDetalhe.IdCarteira = idCarteira;
                    enquadraResultadoDetalhe.Data = data;
                    enquadraResultadoDetalhe.Item = nomeCotista;
                    enquadraResultadoDetalhe.ValorCriterio = valor;
                    enquadraResultadoDetalhe.ValorBase = valorPL;
                    enquadraResultadoDetalhe.Resultado = resultado;
                    enquadraResultadoDetalhe.Enquadrado = enquadradoCotista;
                    enquadraResultadoDetalheCollection.AttachEntity(enquadraResultadoDetalhe);
                    //
                }
            }

            if (enquadraRegra.Maximo.HasValue)
            {
                valorFinal = maiorValor;
                resultadoFinal = maiorPercentual;
            }
            else
            {
                valorFinal = menorValor;
                resultadoFinal = menorPercentual;
            }

            //Salva o resultado Final em EnquadraResultado
            EnquadraResultado enquadraResultado = new EnquadraResultado();
            enquadraResultado.IdRegra = idRegra;
            enquadraResultado.IdCarteira = idCarteira;
            enquadraResultado.Data = data;
            enquadraResultado.ValorCriterio = valorFinal;
            enquadraResultado.ValorBase = valorPL;
            enquadraResultado.Resultado = resultadoFinal;
            enquadraResultado.Enquadrado = enquadrado;

            if (!realTime)
            {                
                enquadraResultado.Save();
                
                //Atualiza a collection de resultado detalhe
                foreach (EnquadraResultadoDetalhe enquadraResultadoDetalhe in enquadraResultadoDetalheCollection)
                {
                    enquadraResultadoDetalhe.IdResultadoOrigem = enquadraResultado.IdResultado.Value;
                }
                enquadraResultadoDetalheCollection.Save();
            }

            return enquadraResultado;
        }

        /// <summary>
        /// Calcula número de cotistas total.
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        public EnquadraResultado CalculaNumeroCotista(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data, bool realTime)
        {
            int idRegra = enquadraRegra.IdRegra.Value;

            PosicaoCotista posicaoCotista = new PosicaoCotista();
            decimal resultado = posicaoCotista.RetornaCountNumeroCotistas(idCarteira);

            string enquadrado = "S";
            if (enquadraRegra.Minimo.HasValue) {
                decimal minimo = enquadraRegra.Minimo.Value;
                if (resultado < minimo)
                    enquadrado = "N";
            }
            if (enquadraRegra.Maximo.HasValue) {
                decimal maximo = enquadraRegra.Maximo.Value;
                if (resultado > maximo)
                    enquadrado = "N";
            }

            EnquadraResultado enquadraResultado = new EnquadraResultado();
            enquadraResultado.IdRegra = idRegra;
            enquadraResultado.IdCarteira = idCarteira;
            enquadraResultado.Data = data;
            enquadraResultado.ValorCriterio = resultado;
            enquadraResultado.ValorBase = 0;
            enquadraResultado.Resultado = 0;
            enquadraResultado.Enquadrado = enquadrado;

            if (!realTime)
            {                
                enquadraResultado.Save();             
            }

            return enquadraResultado;
        }

        /// <summary>
        /// Checa possíveis posições a descoberto.
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        public EnquadraResultado CalculaPosicaoDescoberto(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data, bool realTime)
        {
            int subTipoRegra = enquadraRegra.SubTipoRegra.Value;

            EnquadraResultado enquadraResultado = new EnquadraResultado();

            switch (subTipoRegra)
            {
                case SubTipoRegraEnquadra.PosicaoDescoberto.Opcao:
                    enquadraResultado = CalculaPosicaoDescobertoOpcao(enquadraRegra, idCarteira, historico, data, realTime);
                    break;
                case SubTipoRegraEnquadra.PosicaoDescoberto.TermoAcao:
                    enquadraResultado = CalculaPosicaoDescobertoTermo(enquadraRegra, idCarteira, historico, data, realTime);
                    break;                
            }

            return enquadraResultado;
        }

        /// <summary>
        /// Calcula a regra de posição a descoberto de opções (somente opções vendidas sem cobertura de a vista equivalente).
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        private EnquadraResultado CalculaPosicaoDescobertoOpcao(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data, bool realTime)
        {
            EnquadraResultadoDetalheCollection enquadraResultadoDetalheCollection = new EnquadraResultadoDetalheCollection();

            int idRegra = enquadraRegra.IdRegra.Value;
            
            PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoBolsaQuery.Select(ativoBolsaQuery.CdAtivoBolsaObjeto,
                                     posicaoBolsaQuery.CdAtivoBolsa,
                                     posicaoBolsaQuery.Quantidade.Sum(),
                                     posicaoBolsaQuery.ValorMercado.Sum());
            posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idCarteira),
                                   posicaoBolsaQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra,
                                                                    TipoMercadoBolsa.OpcaoVenda),
                                   posicaoBolsaQuery.Quantidade.LessThan(0));
            posicaoBolsaQuery.GroupBy(ativoBolsaQuery.CdAtivoBolsaObjeto,
                                      posicaoBolsaQuery.CdAtivoBolsa);

            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            posicaoBolsaCollection.Load(posicaoBolsaQuery);

            decimal quantidadeOpcoes = 0;
            decimal quantidadeAcoes = 0;
            string enquadrado = "S";
            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = Convert.ToString(posicaoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsaObjeto));
                string cdAtivoBolsaOpcao = posicaoBolsa.CdAtivoBolsa;
                decimal quantidadeOpcoesAbs = Math.Abs(posicaoBolsa.Quantidade.Value);

                decimal quantidadeOpcoesDetalhe = 0;
                decimal quantidadeAcoesDetalhe = 0;

                PosicaoBolsaCollection collVista = new PosicaoBolsaCollection();
                collVista.Query.Select(collVista.Query.Quantidade);
                collVista.Query.Where(collVista.Query.IdCliente.Equal(idCarteira),
                                      collVista.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                      collVista.Query.Quantidade.GreaterThan(0));
                if (collVista.Query.Load())
                {
                    if (collVista[0].Quantidade < quantidadeOpcoesAbs)
                    {
                        enquadrado = "N";

                        quantidadeOpcoesDetalhe = posicaoBolsa.Quantidade.Value;
                        quantidadeAcoesDetalhe = collVista[0].Quantidade.Value;
                        if (Math.Abs(quantidadeOpcoes) < Math.Abs(quantidadeOpcoesDetalhe))
                        {
                            quantidadeOpcoes = quantidadeOpcoesDetalhe;
                            quantidadeAcoes = quantidadeAcoesDetalhe;
                        }
                    }
                }
                else
                {
                    enquadrado = "N";
                }

                if (!realTime)
                {
                    //Salva o resultado em EnquadraResultadoDetalhe
                    EnquadraResultadoDetalhe enquadraResultadoDetalhe = new EnquadraResultadoDetalhe();
                    enquadraResultadoDetalhe.IdRegra = idRegra;
                    enquadraResultadoDetalhe.IdCarteira = idCarteira;
                    enquadraResultadoDetalhe.Data = data;
                    enquadraResultadoDetalhe.Item = cdAtivoBolsaOpcao;
                    enquadraResultadoDetalhe.ValorCriterio = quantidadeOpcoesDetalhe;
                    enquadraResultadoDetalhe.ValorBase = quantidadeAcoesDetalhe;
                    enquadraResultadoDetalhe.Resultado = 0;
                    enquadraResultadoDetalhe.Enquadrado = enquadrado;
                    enquadraResultadoDetalheCollection.AttachEntity(enquadraResultadoDetalhe);
                    //
                }
            }

            EnquadraResultado enquadraResultado = new EnquadraResultado();
            enquadraResultado.IdRegra = idRegra;
            enquadraResultado.IdCarteira = idCarteira;
            enquadraResultado.Data = data;
            enquadraResultado.ValorCriterio = quantidadeOpcoes;
            enquadraResultado.ValorBase = quantidadeAcoes;
            enquadraResultado.Resultado = 0;
            enquadraResultado.Enquadrado = enquadrado;

            if (!realTime)
            {
                enquadraResultado.Save();
                
                //Atualiza a collection de resultado detalhe
                foreach (EnquadraResultadoDetalhe enquadraResultadoDetalhe in enquadraResultadoDetalheCollection)
                {
                    enquadraResultadoDetalhe.IdResultadoOrigem = enquadraResultado.IdResultado.Value;
                }
                enquadraResultadoDetalheCollection.Save();
            }

            return enquadraResultado;
        }

        /// <summary>
        /// Calcula a regra de posição a descoberto de termos (somente termos vendidos sem cobertura de a vista equivalente).
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        private EnquadraResultado CalculaPosicaoDescobertoTermo(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data, bool realTime)
        {
            EnquadraResultadoDetalheCollection enquadraResultadoDetalheCollection = new EnquadraResultadoDetalheCollection();

            int idRegra = enquadraRegra.IdRegra.Value;

            PosicaoTermoBolsaQuery posicaoTermoBolsaQuery = new PosicaoTermoBolsaQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            posicaoTermoBolsaQuery.Select(ativoBolsaQuery.CdAtivoBolsaObjeto,
                                          posicaoTermoBolsaQuery.CdAtivoBolsa,  
                                          posicaoTermoBolsaQuery.Quantidade.Sum());
            posicaoTermoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoTermoBolsaQuery.CdAtivoBolsa);
            posicaoTermoBolsaQuery.Where(posicaoTermoBolsaQuery.IdCliente.Equal(idCarteira),
                                   posicaoTermoBolsaQuery.Quantidade.LessThan(0));
            posicaoTermoBolsaQuery.GroupBy(ativoBolsaQuery.CdAtivoBolsaObjeto,
                                           posicaoTermoBolsaQuery.CdAtivoBolsa);

            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            posicaoTermoBolsaCollection.Load(posicaoTermoBolsaQuery);
            
            decimal quantidadeTermo = 0;
            decimal quantidadeAcoes = 0;
            string enquadrado = "S";
            foreach (PosicaoTermoBolsa posicaoTermoBolsa in posicaoTermoBolsaCollection)
            {
                string cdAtivoBolsa = Convert.ToString(posicaoTermoBolsa.GetColumn(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsaObjeto));
                string cdAtivoBolsaTermo = AtivoBolsa.RetornaCdAtivoBolsaAcao(posicaoTermoBolsa.CdAtivoBolsa) + "-" + 
                                           AtivoBolsa.RetornaDataVencimentoTermo(posicaoTermoBolsa.CdAtivoBolsa).ToShortDateString();
                decimal quantidadeTermoAbs = Math.Abs(posicaoTermoBolsa.Quantidade.Value);

                decimal quantidadeTermoDetalhe = 0;
                decimal quantidadeAcoesDetalhe = 0;

                PosicaoBolsaCollection collVista = new PosicaoBolsaCollection();
                collVista.Query.Select(collVista.Query.Quantidade);
                collVista.Query.Where(collVista.Query.IdCliente.Equal(idCarteira),
                                      collVista.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                      collVista.Query.Quantidade.GreaterThan(0));
                if (collVista.Query.Load())
                {
                    if (collVista[0].Quantidade < quantidadeTermoAbs)
                    {
                        enquadrado = "N";

                        quantidadeTermoDetalhe = posicaoTermoBolsa.Quantidade.Value;
                        quantidadeAcoesDetalhe = collVista[0].Quantidade.Value;
                        if (Math.Abs(quantidadeTermoDetalhe) < quantidadeTermoAbs)
                        {
                            quantidadeTermo = quantidadeTermoDetalhe;
                            quantidadeAcoes = quantidadeAcoesDetalhe;
                        }
                    }
                }
                else
                {
                    enquadrado = "N";
                }

                if (!realTime)
                {
                    //Salva o resultado em EnquadraResultadoDetalhe
                    EnquadraResultadoDetalhe enquadraResultadoDetalhe = new EnquadraResultadoDetalhe();
                    enquadraResultadoDetalhe.IdRegra = idRegra;
                    enquadraResultadoDetalhe.IdCarteira = idCarteira;
                    enquadraResultadoDetalhe.Data = data;
                    enquadraResultadoDetalhe.Item = cdAtivoBolsaTermo;
                    enquadraResultadoDetalhe.ValorCriterio = quantidadeTermoDetalhe;
                    enquadraResultadoDetalhe.ValorBase = quantidadeAcoesDetalhe;
                    enquadraResultadoDetalhe.Resultado = 0;
                    enquadraResultadoDetalhe.Enquadrado = enquadrado;
                    enquadraResultadoDetalheCollection.AttachEntity(enquadraResultadoDetalhe);
                    //
                }
            }

            EnquadraResultado enquadraResultado = new EnquadraResultado();
            enquadraResultado.IdRegra = idRegra;
            enquadraResultado.IdCarteira = idCarteira;
            enquadraResultado.Data = data;
            enquadraResultado.ValorCriterio = quantidadeTermo;
            enquadraResultado.ValorBase = quantidadeAcoes;
            enquadraResultado.Resultado = 0;
            enquadraResultado.Enquadrado = enquadrado;

            if (!realTime)
            {
                enquadraResultado.Save();

                //Atualiza a collection de resultado detalhe
                foreach (EnquadraResultadoDetalhe enquadraResultadoDetalhe in enquadraResultadoDetalheCollection)
                {
                    enquadraResultadoDetalhe.IdResultadoOrigem = enquadraResultado.IdResultado.Value;
                }
                enquadraResultadoDetalheCollection.Save();
            }

            return enquadraResultado;
            
        }

        /// <summary>
        /// Calcula média móvel de ações em relação ao PL.
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        public void CalculaMediaMovel(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data) {
            int idRegra = enquadraRegra.IdRegra.Value;

            //Busca a data de implantação da carteira/cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataImplantacao);
            cliente.LoadByPrimaryKey(campos, idCarteira);
            DateTime dataImplantacao = cliente.DataImplantacao.Value;
            //
            
            DateTime dataInicio = Calendario.SubtraiDiaUtil(data, 40);

            if (DateTime.Compare(dataImplantacao, dataInicio) > 0)
                dataInicio = dataImplantacao;

            DateTime dataFim = Calendario.SubtraiDiaUtil(data, 5);

            //Se a data inicio é > que a data fim, é pq não há dados para o cálculo, neste caso sai sem fazer nada
            if (DateTime.Compare(dataInicio, dataFim) > 0)
                return;

            DateTime dataAux = dataInicio;
            
            decimal totalPercentual = 0;            
            //Janela móvel de 40 dias para trás, com defasagem de 5 dias úteis
            int cont = 0;
            while (DateTime.Compare(dataAux, dataFim) <= 0)
	        {
                PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                decimal valorBolsa = posicaoBolsaHistorico.RetornaValorMercadoSum(idCarteira, dataAux);

                PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico = new PosicaoEmprestimoBolsaHistorico();
                valorBolsa += posicaoEmprestimoBolsaHistorico.RetornaValorMercadoNet(idCarteira, dataAux);

                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.Query.Select(historicoCota.Query.PLFechamento);
                historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idCarteira),
                                          historicoCota.Query.Data.Equal(dataAux));
                if (historicoCota.Query.Load() && historicoCota.PLFechamento.HasValue)
                {                
                    decimal valorPL = historicoCota.PLFechamento.Value;

                    decimal percentual = 0;

                    if (valorBolsa != 0 && valorPL == 0)
                    {
                        percentual = 0;
                    }
                    else if (valorBolsa == 0 && valorPL == 0)
                    {
                        percentual = 100;
                    }
                    else
                    {
                        percentual = valorBolsa / valorPL * 100;
                    }

                    totalPercentual += percentual;
                    
                    dataAux = Calendario.AdicionaDiaUtil(dataAux, 1);

                    cont += 1;
                }
            }

            decimal mediaMovel = 0;
            if (cont > 0)
            {
                mediaMovel = Utilitario.Truncate(totalPercentual / cont, 4);
            }

            string enquadrado = "S";
            if (enquadraRegra.Minimo.HasValue) {
                decimal minimo = enquadraRegra.Minimo.Value;
                if (mediaMovel < minimo)
                    enquadrado = "N";
            }
            if (enquadraRegra.Maximo.HasValue) {
                decimal maximo = enquadraRegra.Maximo.Value;
                if (mediaMovel > maximo)
                    enquadrado = "N";
            }

            //Salva o resultado em EnquadraResultado
            EnquadraResultado enquadraResultado = new EnquadraResultado();
            enquadraResultado.IdRegra = idRegra;
            enquadraResultado.IdCarteira = idCarteira;
            enquadraResultado.Data = data;
            enquadraResultado.ValorCriterio = 0;
            enquadraResultado.ValorBase = 0;
            enquadraResultado.Resultado = mediaMovel;
            enquadraResultado.Enquadrado = enquadrado;
            enquadraResultado.Save();
            //
        }

        /// <summary>
        /// Calcula concentração de ativos (fundos, ações, renda fixa).
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        public EnquadraResultado CalculaConcentracao(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data, bool realTime)
        {
            int subTipoRegra = enquadraRegra.SubTipoRegra.Value;

            EnquadraResultado enquadraResultado = new EnquadraResultado();

            switch (subTipoRegra)
            {
                case SubTipoRegraEnquadra.Concentracao.AcaoAtivo:
                    enquadraResultado = CalculaConcentracaoAcao(enquadraRegra, idCarteira, historico, data, realTime);                    
                    break;
                case SubTipoRegraEnquadra.Concentracao.CotaInvestimentoAtivo:
                    enquadraResultado = CalculaConcentracaoFundo(enquadraRegra, idCarteira, historico, data, realTime);
                    break;
                case SubTipoRegraEnquadra.Concentracao.CotaInvestimentoAdministrador:
                    enquadraResultado = CalculaConcentracaoAdministrador(enquadraRegra, idCarteira, historico, data, realTime);
                    break;
                case SubTipoRegraEnquadra.Concentracao.CotaInvestimentoGestor:
                    enquadraResultado = CalculaConcentracaoGestor(enquadraRegra, idCarteira, historico, data, realTime);
                    break;
                case SubTipoRegraEnquadra.Concentracao.RendaFixaTitulo:
                    enquadraResultado = CalculaConcentracaoTituloRendaFixa(enquadraRegra, idCarteira, historico, data, realTime);
                    break;
                case SubTipoRegraEnquadra.Concentracao.Emissor:
                    enquadraResultado = CalculaConcentracaoEmissor(enquadraRegra, idCarteira, historico, data, realTime);
                    break;
            }

            return enquadraResultado;
        }

        /// <summary>
        /// Método private para cálculo exclusivo de concentração por ação.
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        private EnquadraResultado CalculaConcentracaoAcao(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data, bool realTime)
        {
            EnquadraResultadoDetalheCollection enquadraResultadoDetalheCollection = new EnquadraResultadoDetalheCollection();

            int idRegra = enquadraRegra.IdRegra.Value;
            int idGrupoCriterio = enquadraRegra.IdGrupoCriterio.Value;
            int idGrupoBase = enquadraRegra.IdGrupoBase.Value;

            EnquadraGrupo enquadraGrupo = new EnquadraGrupo();
            decimal valorBase = enquadraGrupo.RetornaValorGrupoItem(idCarteira, idGrupoBase, historico, data, realTime);

            if (valorBase == 0)
                throw new ValorBaseZeradoException("Valor base está zerado para a carteira " + idCarteira + ", grupo " + idGrupoBase);

            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            posicaoBolsaCollection.BuscaPosicaoBolsaAgrupadoEnquadra(idCarteira);

            string enquadrado = "S";
            string enquadradoAcao = "S";
            decimal valorFinal = 0;
            decimal resultadoFinal = 0;
            for (int i = 0; i < posicaoBolsaCollection.Count; i++)
            {
                enquadradoAcao = "S";
                PosicaoBolsa posicaoBolsa = posicaoBolsaCollection[i];
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                decimal valorCriterio = posicaoBolsa.ValorMercado.Value;

                decimal resultado = Utilitario.Truncate(valorCriterio / valorBase, 4) * 100;

                if (enquadraRegra.Minimo.HasValue) {
                    decimal minimo = enquadraRegra.Minimo.Value;
                    if (resultado < minimo) {
                        enquadrado = "N";
                        enquadradoAcao = "N";
                        valorFinal = valorCriterio;
                        resultadoFinal = resultado;
                    }
                }
                if (enquadraRegra.Maximo.HasValue) {
                    decimal maximo = enquadraRegra.Maximo.Value;
                    if (resultado > maximo)
                    {
                        enquadrado = "N";
                        enquadradoAcao = "N";
                        valorFinal = valorCriterio;
                        resultadoFinal = resultado;
                    }
                }

                if (!realTime)
                {
                    //Salva o resultado em EnquadraResultadoDetalhe
                    EnquadraResultadoDetalhe enquadraResultadoDetalhe = new EnquadraResultadoDetalhe();
                    enquadraResultadoDetalhe.IdRegra = idRegra;
                    enquadraResultadoDetalhe.IdCarteira = idCarteira;
                    enquadraResultadoDetalhe.Data = data;
                    enquadraResultadoDetalhe.Item = cdAtivoBolsa;
                    enquadraResultadoDetalhe.ValorCriterio = valorCriterio;
                    enquadraResultadoDetalhe.ValorBase = valorBase;
                    enquadraResultadoDetalhe.Resultado = resultado;
                    enquadraResultadoDetalhe.Enquadrado = enquadradoAcao;

                    enquadraResultadoDetalheCollection.AttachEntity(enquadraResultadoDetalhe);
                    //
                }            
            }

            
            EnquadraResultado enquadraResultado = new EnquadraResultado();
            enquadraResultado.IdRegra = idRegra;
            enquadraResultado.IdCarteira = idCarteira;
            enquadraResultado.Data = data;
            enquadraResultado.ValorCriterio = valorFinal;
            enquadraResultado.ValorBase = valorBase;
            enquadraResultado.Resultado = resultadoFinal;
            enquadraResultado.Enquadrado = enquadrado;

            if (!realTime)
            {
                enquadraResultado.Save();
             
                //Atualiza a collection de resultado detalhe
                foreach (EnquadraResultadoDetalhe enquadraResultadoDetalhe in enquadraResultadoDetalheCollection)
                {
                    enquadraResultadoDetalhe.IdResultadoOrigem = enquadraResultado.IdResultado.Value;
                }
                enquadraResultadoDetalheCollection.Save();
            }

            return enquadraResultado;
        }

        /// <summary>
        /// Método private para cálculo exclusivo de concentração por ativo do tipo fundo/clube.
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        private EnquadraResultado CalculaConcentracaoFundo(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data, bool realTime)
        {
            EnquadraResultadoDetalheCollection enquadraResultadoDetalheCollection = new EnquadraResultadoDetalheCollection();

            int idRegra = enquadraRegra.IdRegra.Value;
            int idGrupoCriterio = enquadraRegra.IdGrupoCriterio.Value;
            int idGrupoBase = enquadraRegra.IdGrupoBase.Value;

            EnquadraGrupo enquadraGrupo = new EnquadraGrupo();
            decimal valorBase = enquadraGrupo.RetornaValorGrupoItem(idCarteira, idGrupoBase, historico, data, realTime);

            if (valorBase == 0)
                throw new ValorBaseZeradoException("Valor base está zerado para a carteira " + idCarteira + ", grupo " + idGrupoBase);

            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            posicaoFundoCollection.BuscaPosicaoFundoAgrupadoEnquadra(idCarteira);

            string enquadrado = "S";
            string enquadradoFundo = "S";
            decimal valorFinal = 0;
            decimal resultadoFinal = 0;
            for (int i = 0; i < posicaoFundoCollection.Count; i++)
            {
                enquadradoFundo = "S";
                PosicaoFundo posicaoFundo = posicaoFundoCollection[i];
                int idFundo = posicaoFundo.IdCarteira.Value;

                //Busca o nome do fundo
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idFundo);
                string nomeFundo = carteira.Nome;
                //

                EnquadraItemFundoCollection enquadraItemFundoCollection = new EnquadraItemFundoCollection();
                EnquadraItemFundoQuery enquadraItemFundoQuery = new EnquadraItemFundoQuery("I");
                EnquadraGrupoItemQuery enquadraGrupoItemQuery = new EnquadraGrupoItemQuery("G");
                enquadraItemFundoQuery.Select(enquadraItemFundoQuery.IdAgenteAdministrador,
                                              enquadraItemFundoQuery.IdAgenteGestor,
                                              enquadraItemFundoQuery.IdCarteira,
                                              enquadraItemFundoQuery.IdCategoria,
                                              enquadraItemFundoQuery.IdIndice,
                                              enquadraItemFundoQuery.TipoCarteira);
                enquadraItemFundoQuery.InnerJoin(enquadraGrupoItemQuery).On(enquadraGrupoItemQuery.IdItem == enquadraItemFundoQuery.IdItem);
                enquadraItemFundoQuery.Where(enquadraGrupoItemQuery.IdGrupo.Equal(idGrupoCriterio));
                enquadraItemFundoCollection.Load(enquadraItemFundoQuery);

                bool computar = false;
                foreach (EnquadraItemFundo enquadraItemFundo in enquadraItemFundoCollection)
                {
                    if (enquadraItemFundo.IdAgenteAdministrador.HasValue && enquadraItemFundo.IdAgenteAdministrador.Value == carteira.IdAgenteAdministrador.Value)
                    {
                        computar = true;
                        break;
                    }

                    if (enquadraItemFundo.IdAgenteGestor.HasValue && enquadraItemFundo.IdAgenteGestor.Value == carteira.IdAgenteGestor.Value)
                    {
                        computar = true;
                        break;
                    }

                    if (enquadraItemFundo.IdCarteira.HasValue && enquadraItemFundo.IdCarteira.Value == carteira.IdCarteira.Value)
                    {
                        computar = true;
                        break;
                    }

                    if (enquadraItemFundo.IdCategoria.HasValue && enquadraItemFundo.IdCategoria.Value == carteira.IdCategoria.Value)
                    {
                        computar = true;
                        break;
                    }

                    if (enquadraItemFundo.IdIndice.HasValue && enquadraItemFundo.IdIndice.Value == carteira.IdIndiceBenchmark.Value)
                    {
                        computar = true;
                        break;
                    }

                    if (enquadraItemFundo.TipoCarteira.HasValue && enquadraItemFundo.TipoCarteira.Value == carteira.TipoCarteira.Value)
                    {
                        computar = true;
                        break;
                    }
                }

                if (!computar)
                    continue; //se não atende os criterios do grupo item não soma

                decimal valorCriterio = posicaoFundo.ValorBruto.Value;

                decimal resultado = Utilitario.Truncate(valorCriterio / valorBase, 4) * 100;

                if (enquadraRegra.Minimo.HasValue) {
                    decimal minimo = enquadraRegra.Minimo.Value;
                    if (resultado < minimo) {
                        enquadrado = "N";
                        enquadradoFundo = "N";
                        valorFinal = valorCriterio;
                        resultadoFinal = resultado;
                    }
                }
                if (enquadraRegra.Maximo.HasValue) {
                    decimal maximo = enquadraRegra.Maximo.Value;
                    if (resultado > maximo) {
                        enquadrado = "N";
                        enquadradoFundo = "N";
                        valorFinal = valorCriterio;
                        resultadoFinal = resultado;
                    }
                }

                if (!realTime)
                {
                    //Salva o resultado em EnquadraResultadoDetalhe
                    EnquadraResultadoDetalhe enquadraResultadoDetalhe = new EnquadraResultadoDetalhe();
                    enquadraResultadoDetalhe.IdRegra = idRegra;
                    enquadraResultadoDetalhe.IdCarteira = idCarteira;
                    enquadraResultadoDetalhe.Data = data;
                    enquadraResultadoDetalhe.Item = nomeFundo;
                    enquadraResultadoDetalhe.ValorCriterio = valorCriterio;
                    enquadraResultadoDetalhe.ValorBase = valorBase;
                    enquadraResultadoDetalhe.Resultado = resultado;
                    enquadraResultadoDetalhe.Enquadrado = enquadradoFundo;

                    enquadraResultadoDetalheCollection.AttachEntity(enquadraResultadoDetalhe);
                    //
                }            
            }

            EnquadraResultado enquadraResultado = new EnquadraResultado();
            enquadraResultado.IdRegra = idRegra;
            enquadraResultado.IdCarteira = idCarteira;
            enquadraResultado.Data = data;
            enquadraResultado.ValorCriterio = valorFinal;
            enquadraResultado.ValorBase = valorBase;
            enquadraResultado.Resultado = resultadoFinal;
            enquadraResultado.Enquadrado = enquadrado;

            if (!realTime)
            {
                enquadraResultado.Save();

                //Atualiza a collection de resultado detalhe
                foreach (EnquadraResultadoDetalhe enquadraResultadoDetalhe in enquadraResultadoDetalheCollection)
                {
                    enquadraResultadoDetalhe.IdResultadoOrigem = enquadraResultado.IdResultado.Value;
                }
                enquadraResultadoDetalheCollection.Save();
            }

            return enquadraResultado;
        }

        /// <summary>
        /// Método private para cálculo exclusivo de concentração por títulos de renda fixa.
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        private EnquadraResultado CalculaConcentracaoTituloRendaFixa(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data, bool realTime)
        {
            EnquadraResultadoDetalheCollection enquadraResultadoDetalheCollection = new EnquadraResultadoDetalheCollection();

            int idRegra = enquadraRegra.IdRegra.Value;
            int idGrupoCriterio = enquadraRegra.IdGrupoCriterio.Value;
            int idGrupoBase = enquadraRegra.IdGrupoBase.Value;

            EnquadraGrupo enquadraGrupo = new EnquadraGrupo();
            decimal valorBase = enquadraGrupo.RetornaValorGrupoItem(idCarteira, idGrupoBase, historico, data, realTime);

            if (valorBase == 0)
                throw new ValorBaseZeradoException("Valor base está zerado para a carteira " + idCarteira + ", grupo " + idGrupoBase);

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.BuscaPosicaoRendaFixaAgrupadoEnquadra(idCarteira);

            string enquadrado = "S";
            string enquadradoTitulo = "S";
            decimal valorFinal = 0;
            decimal resultadoFinal = 0;
            for (int i = 0; i < posicaoRendaFixaCollection.Count; i++)
            {
                enquadradoTitulo = "S";
                PosicaoRendaFixa posicaoRendaFixa = posicaoRendaFixaCollection[i];
                int idTitulo = posicaoRendaFixa.IdTitulo.Value;

                //Busca a descricao do titulo
                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(tituloRendaFixa.Query.Descricao);
                tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);
                string descricaoTitulo = tituloRendaFixa.Descricao;
                //

                decimal valorCriterio = posicaoRendaFixa.ValorMercado.Value;

                if (valorCriterio != 0)
                {
                    decimal resultado = Utilitario.Truncate(valorCriterio / valorBase, 4) * 100;

                    if (enquadraRegra.Minimo.HasValue)
                    {
                        decimal minimo = enquadraRegra.Minimo.Value;
                        if (resultado < minimo)
                        {
                            enquadrado = "N";
                            enquadradoTitulo = "N";
                            valorFinal = valorCriterio;
                            resultadoFinal = resultado;
                        }
                    }
                    if (enquadraRegra.Maximo.HasValue)
                    {
                        decimal maximo = enquadraRegra.Maximo.Value;
                        if (resultado > maximo)
                        {
                            enquadrado = "N";
                            enquadradoTitulo = "N";
                            valorFinal = valorCriterio;
                            resultadoFinal = resultado;
                        }
                    }

                    if (!realTime)
                    {
                        //Salva o resultado em EnquadraResultadoDetalhe
                        EnquadraResultadoDetalhe enquadraResultadoDetalhe = new EnquadraResultadoDetalhe();
                        enquadraResultadoDetalhe.IdRegra = idRegra;
                        enquadraResultadoDetalhe.IdCarteira = idCarteira;
                        enquadraResultadoDetalhe.Data = data;
                        enquadraResultadoDetalhe.Item = descricaoTitulo;
                        enquadraResultadoDetalhe.ValorCriterio = valorCriterio;
                        enquadraResultadoDetalhe.ValorBase = valorBase;
                        enquadraResultadoDetalhe.Resultado = resultado;
                        enquadraResultadoDetalhe.Enquadrado = enquadradoTitulo;

                        enquadraResultadoDetalheCollection.AttachEntity(enquadraResultadoDetalhe);
                        //
                    }
                }
            }

            EnquadraResultado enquadraResultado = new EnquadraResultado();
            enquadraResultado.IdRegra = idRegra;
            enquadraResultado.IdCarteira = idCarteira;
            enquadraResultado.Data = data;
            enquadraResultado.ValorCriterio = valorFinal;
            enquadraResultado.ValorBase = valorBase;
            enquadraResultado.Resultado = resultadoFinal;
            enquadraResultado.Enquadrado = enquadrado;

            if (!realTime)
            {
                enquadraResultado.Save();

                //Atualiza a collection de resultado detalhe
                foreach (EnquadraResultadoDetalhe enquadraResultadoDetalhe in enquadraResultadoDetalheCollection)
                {
                    enquadraResultadoDetalhe.IdResultadoOrigem = enquadraResultado.IdResultado.Value;
                }
                enquadraResultadoDetalheCollection.Save();
            }

            return enquadraResultado;
        }

        /// <summary>
        /// Método private para cálculo exclusivo para cálculo de concentração total por administrador de fundos.
        /// Trabalha com grupos de ativos (somente do tipo = Cota de Investimentos),
        /// permitindo filtros associados ao grupo.
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        private EnquadraResultado CalculaConcentracaoAdministrador(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data, bool realTime)
        {
            EnquadraResultadoDetalheCollection enquadraResultadoDetalheCollection = new EnquadraResultadoDetalheCollection();

            int idRegra = enquadraRegra.IdRegra.Value;
            int idGrupoCriterio = enquadraRegra.IdGrupoCriterio.Value;
            int idGrupoBase = enquadraRegra.IdGrupoBase.Value;

            EnquadraGrupo enquadraGrupo = new EnquadraGrupo();
            decimal valorBase = enquadraGrupo.RetornaValorGrupoItem(idCarteira, idGrupoBase, historico, data, realTime);

            if (valorBase == 0)
                throw new ValorBaseZeradoException("Valor base está zerado para a carteira " + idCarteira + ", grupo " + idGrupoBase);

            AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();
            agenteMercadoCollection.BuscaAgenteMercadoAdministrador();

            string enquadrado = "S";
            string enquadradoAdministrador = "S";
            decimal valorFinal = 0;
            decimal resultadoFinal = 0;
            for (int i = 0; i < agenteMercadoCollection.Count; i++)
            {
                enquadradoAdministrador = "S";
                AgenteMercado agenteMercado = agenteMercadoCollection[i];
                int idAgente = agenteMercado.IdAgente.Value;
                string nomeAgente = agenteMercado.Nome;

                enquadraGrupo = new EnquadraGrupo();
                decimal valorCriterio = enquadraGrupo.RetornaValorGrupoItemPorAdministrador(idCarteira, idGrupoCriterio,
                                                                                      historico, data, idAgente, realTime);

                if (valorCriterio != 0)
                {
                    decimal resultado = Utilitario.Truncate(valorCriterio / valorBase, 4) * 100;

                    if (enquadraRegra.Minimo.HasValue)
                    {
                        decimal minimo = enquadraRegra.Minimo.Value;
                        if (resultado < minimo)
                        {
                            enquadrado = "N";
                            enquadradoAdministrador = "N";
                            valorFinal = valorCriterio;
                            resultadoFinal = resultado;
                        }
                    }
                    if (enquadraRegra.Maximo.HasValue)
                    {
                        decimal maximo = enquadraRegra.Maximo.Value;
                        if (resultado > maximo)
                        {
                            enquadrado = "N";
                            enquadradoAdministrador = "N";
                            valorFinal = valorCriterio;
                            resultadoFinal = resultado;
                        }
                    }

                    if (!realTime)
                    {
                        //Salva o resultado em EnquadraResultadoDetalhe
                        EnquadraResultadoDetalhe enquadraResultadoDetalhe = new EnquadraResultadoDetalhe();
                        enquadraResultadoDetalhe.IdRegra = idRegra;
                        enquadraResultadoDetalhe.IdCarteira = idCarteira;
                        enquadraResultadoDetalhe.Data = data;
                        enquadraResultadoDetalhe.Item = nomeAgente;
                        enquadraResultadoDetalhe.ValorCriterio = valorCriterio;
                        enquadraResultadoDetalhe.ValorBase = valorBase;
                        enquadraResultadoDetalhe.Resultado = resultado;
                        enquadraResultadoDetalhe.Enquadrado = enquadradoAdministrador;

                        enquadraResultadoDetalheCollection.AttachEntity(enquadraResultadoDetalhe);
                        //
                    }
                }
            }

            //Salva o resultado Final em EnquadraResultado
            EnquadraResultado enquadraResultado = new EnquadraResultado();
            enquadraResultado.IdRegra = idRegra;
            enquadraResultado.IdCarteira = idCarteira;
            enquadraResultado.Data = data;
            enquadraResultado.ValorCriterio = valorFinal;
            enquadraResultado.ValorBase = valorBase;
            enquadraResultado.Resultado = resultadoFinal;
            enquadraResultado.Enquadrado = enquadrado;

            if (!realTime)
            {
                enquadraResultado.Save();
                //

                //Atualiza a collection de resultado detalhe
                foreach (EnquadraResultadoDetalhe enquadraResultadoDetalhe in enquadraResultadoDetalheCollection)
                {
                    enquadraResultadoDetalhe.IdResultadoOrigem = enquadraResultado.IdResultado.Value;
                }
                enquadraResultadoDetalheCollection.Save();
            }

            return enquadraResultado;
        }

        /// <summary>
        /// Método private para cálculo exclusivo para cálculo de concentração total por gestor de fundos.
        /// Trabalha com grupos de ativos (somente do tipo = Cota de Investimentos),
        /// permitindo filtros associados ao grupo.
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        private EnquadraResultado CalculaConcentracaoGestor(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data, bool realTime)
        {
            EnquadraResultadoDetalheCollection enquadraResultadoDetalheCollection = new EnquadraResultadoDetalheCollection();

            int idRegra = enquadraRegra.IdRegra.Value;
            int idGrupoCriterio = enquadraRegra.IdGrupoCriterio.Value;
            int idGrupoBase = enquadraRegra.IdGrupoBase.Value;

            EnquadraGrupo enquadraGrupo = new EnquadraGrupo();
            decimal valorBase = enquadraGrupo.RetornaValorGrupoItem(idCarteira, idGrupoBase, historico, data, realTime);

            if (valorBase == 0)
                throw new ValorBaseZeradoException("Valor base está zerado para a carteira " + idCarteira + ", grupo " + idGrupoBase);

            AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();
            agenteMercadoCollection.BuscaAgenteMercadoGestor();

            string enquadrado = "S";
            string enquadradoGestor = "S";
            decimal valorFinal = 0;
            decimal resultadoFinal = 0;
            for (int i = 0; i < agenteMercadoCollection.Count; i++)
            {
                enquadradoGestor = "S";
                AgenteMercado agenteMercado = agenteMercadoCollection[i];
                int idAgente = agenteMercado.IdAgente.Value;
                string nomeAgente = agenteMercado.Nome;

                enquadraGrupo = new EnquadraGrupo();
                decimal valorCriterio = enquadraGrupo.RetornaValorGrupoItemPorGestor(idCarteira, idGrupoCriterio,
                                                                                      historico, data, idAgente, realTime);

                if (valorCriterio != 0)
                {
                    decimal resultado = Utilitario.Truncate(valorCriterio / valorBase, 4) * 100;

                    if (enquadraRegra.Minimo.HasValue)
                    {
                        decimal minimo = enquadraRegra.Minimo.Value;
                        if (resultado < minimo)
                        {
                            enquadrado = "N";
                            enquadradoGestor = "N";
                            valorFinal = valorCriterio;
                            resultadoFinal = resultado;
                        }
                    }
                    if (enquadraRegra.Maximo.HasValue)
                    {
                        decimal maximo = enquadraRegra.Maximo.Value;
                        if (resultado > maximo)
                        {
                            enquadrado = "N";
                            enquadradoGestor = "N";
                            valorFinal = valorCriterio;
                            resultadoFinal = resultado;
                        }
                    }

                    if (!realTime)
                    {
                        //Salva o resultado em EnquadraResultadoDetalhe
                        EnquadraResultadoDetalhe enquadraResultadoDetalhe = new EnquadraResultadoDetalhe();
                        enquadraResultadoDetalhe.IdRegra = idRegra;
                        enquadraResultadoDetalhe.IdCarteira = idCarteira;
                        enquadraResultadoDetalhe.Data = data;
                        enquadraResultadoDetalhe.Item = nomeAgente;
                        enquadraResultadoDetalhe.ValorCriterio = valorCriterio;
                        enquadraResultadoDetalhe.ValorBase = valorBase;
                        enquadraResultadoDetalhe.Resultado = resultado;
                        enquadraResultadoDetalhe.Enquadrado = enquadradoGestor;

                        enquadraResultadoDetalheCollection.AttachEntity(enquadraResultadoDetalhe);
                        //
                    }
                }
            }

            EnquadraResultado enquadraResultado = new EnquadraResultado();
            enquadraResultado.IdRegra = idRegra;
            enquadraResultado.IdCarteira = idCarteira;
            enquadraResultado.Data = data;
            enquadraResultado.ValorCriterio = valorFinal;
            enquadraResultado.ValorBase = valorBase;
            enquadraResultado.Resultado = resultadoFinal;
            enquadraResultado.Enquadrado = enquadrado;

            if (!realTime)
            {
                enquadraResultado.Save();

                //Atualiza a collection de resultado detalhe
                foreach (EnquadraResultadoDetalhe enquadraResultadoDetalhe in enquadraResultadoDetalheCollection)
                {
                    enquadraResultadoDetalhe.IdResultadoOrigem = enquadraResultado.IdResultado.Value;
                }
                enquadraResultadoDetalheCollection.Save();
            }

            return enquadraResultado;

        }
       
        /// <summary>
        /// Calcula concentração total por emissor em ações, fundos, renda fixa.
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        public EnquadraResultado CalculaConcentracaoEmissor(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data, bool realTime)
        {
            EnquadraResultadoDetalheCollection enquadraResultadoDetalheCollection = new EnquadraResultadoDetalheCollection();

            int idRegra = enquadraRegra.IdRegra.Value;
            int idGrupoCriterio = enquadraRegra.IdGrupoCriterio.Value;
            int idGrupoBase = enquadraRegra.IdGrupoBase.Value;

            EnquadraGrupo enquadraGrupo = new EnquadraGrupo();
            decimal valorBase = enquadraGrupo.RetornaValorGrupoItem(idCarteira, idGrupoBase, historico, data, realTime);

            if (valorBase == 0)
                throw new ValorBaseZeradoException("Valor base está zerado para a carteira " + idCarteira + ", grupo " + idGrupoBase);


            EmissorCollection emissorCollection = new EmissorCollection();
            #region Monta lista de ids de emissores na EmissorCollection
            List<int> listaIdEmissores = new List<int>();

            EmissorQuery emissorQuery = new EmissorQuery("E");
            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            emissorQuery.Select(emissorQuery.IdEmissor, emissorQuery.Nome);
            emissorQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdEmissor == emissorQuery.IdEmissor);
            emissorQuery.InnerJoin(posicaoRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);            
            emissorQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCarteira));
            emissorQuery.es.Distinct = true;

            emissorCollection.Load(emissorQuery);

            foreach (Emissor emissor in emissorCollection)
            {
                listaIdEmissores.Add(emissor.IdEmissor.Value);
            }

            emissorQuery = new EmissorQuery("E");
            PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            emissorQuery.Select(emissorQuery.IdEmissor, emissorQuery.Nome);
            emissorQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.IdEmissor == emissorQuery.IdEmissor);
            emissorQuery.InnerJoin(posicaoBolsaQuery).On(posicaoBolsaQuery.CdAtivoBolsa == ativoBolsaQuery.CdAtivoBolsa);            
            emissorQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idCarteira));

            if (listaIdEmissores.Count != 0)
            {
                emissorQuery.Where(emissorQuery.IdEmissor.NotIn(listaIdEmissores));
            }

            emissorQuery.es.Distinct = true;

            EmissorCollection emissorCollectionAux = new EmissorCollection();
            emissorCollectionAux.Load(emissorQuery);

            emissorCollection.Combine(emissorCollectionAux);

            foreach (Emissor emissor in emissorCollectionAux)
            {
                listaIdEmissores.Add(emissor.IdEmissor.Value);
            }

            emissorQuery = new EmissorQuery("E");
            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            emissorQuery.Select(emissorQuery.IdEmissor, emissorQuery.Nome);
            emissorQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdEmissor == emissorQuery.IdEmissor);
            emissorQuery.InnerJoin(operacaoRendaFixaQuery).On(operacaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);            
            emissorQuery.Where(operacaoRendaFixaQuery.IdCliente.Equal(idCarteira));

            if (listaIdEmissores.Count != 0)
            {
                emissorQuery.Where(emissorQuery.IdEmissor.NotIn(listaIdEmissores));
            }

            emissorQuery.es.Distinct = true;

            emissorCollectionAux = new EmissorCollection();
            emissorCollectionAux.Load(emissorQuery);

            emissorCollection.Combine(emissorCollectionAux);

            foreach (Emissor emissor in emissorCollectionAux)
            {
                listaIdEmissores.Add(emissor.IdEmissor.Value);
            }

            emissorQuery = new EmissorQuery("E");
            OperacaoBolsaQuery operacaoBolsaQuery = new OperacaoBolsaQuery("O");
            ativoBolsaQuery  = new AtivoBolsaQuery("A");
            emissorQuery.Select(emissorQuery.IdEmissor, emissorQuery.Nome);
            emissorQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.IdEmissor == emissorQuery.IdEmissor);
            emissorQuery.InnerJoin(operacaoBolsaQuery).On(operacaoBolsaQuery.CdAtivoBolsa == ativoBolsaQuery.CdAtivoBolsa);            
            emissorQuery.Where(operacaoBolsaQuery.IdCliente.Equal(idCarteira));

            if (listaIdEmissores.Count != 0)
            {
                emissorQuery.Where(emissorQuery.IdEmissor.NotIn(listaIdEmissores));
            }

            emissorQuery.es.Distinct = true;

            emissorCollectionAux = new EmissorCollection();
            emissorCollectionAux.Load(emissorQuery);

            emissorCollection.Combine(emissorCollectionAux);

            foreach (Emissor emissor in emissorCollectionAux)
            {
                listaIdEmissores.Add(emissor.IdEmissor.Value);
            }
            #endregion

            string enquadrado = "S";
            string enquadradoEmissor = "S";
            decimal valorFinal = 0;
            decimal resultadoFinal = 0;
            for (int i = 0; i < emissorCollection.Count; i++)
            {
                Emissor emissor = emissorCollection[i];
                                
                int idEmissor = emissor.IdEmissor.Value;
                string nomeEmissor = emissor.Nome;

                enquadraGrupo = new EnquadraGrupo();
                decimal valorCriterio = enquadraGrupo.RetornaValorGrupoItemPorEmissor(idCarteira, idGrupoCriterio,
                                                                                      historico, data, idEmissor, realTime);

                if (valorCriterio != 0)
                {
                    decimal resultado = Utilitario.Truncate(valorCriterio / valorBase, 4) * 100;

                    if (enquadraRegra.Minimo.HasValue)
                    {
                        decimal minimo = enquadraRegra.Minimo.Value;
                        if (resultado < minimo)
                        {
                            enquadrado = "N";
                            enquadradoEmissor = "N";
                            valorFinal = valorCriterio;
                            resultadoFinal = resultado;
                        }
                    }
                    if (enquadraRegra.Maximo.HasValue)
                    {
                        decimal maximo = enquadraRegra.Maximo.Value;
                        if (resultado > maximo)
                        {
                            enquadrado = "N";
                            enquadradoEmissor = "N";
                            valorFinal = valorCriterio;
                            resultadoFinal = resultado;
                        }
                    }

                    if (!realTime)
                    {
                        //Salva o resultado em EnquadraResultadoDetalhe
                        EnquadraResultadoDetalhe enquadraResultadoDetalhe = new EnquadraResultadoDetalhe();
                        enquadraResultadoDetalhe.IdRegra = idRegra;
                        enquadraResultadoDetalhe.IdCarteira = idCarteira;
                        enquadraResultadoDetalhe.Data = data;
                        enquadraResultadoDetalhe.Item = nomeEmissor;
                        enquadraResultadoDetalhe.ValorCriterio = valorCriterio;
                        enquadraResultadoDetalhe.ValorBase = valorBase;
                        enquadraResultadoDetalhe.Resultado = resultado;
                        enquadraResultadoDetalhe.Enquadrado = enquadradoEmissor;

                        enquadraResultadoDetalheCollection.AttachEntity(enquadraResultadoDetalhe);
                        //
                    }
                }
            }

            EnquadraResultado enquadraResultado = new EnquadraResultado();
            enquadraResultado.IdRegra = idRegra;
            enquadraResultado.IdCarteira = idCarteira;
            enquadraResultado.Data = data;
            enquadraResultado.ValorCriterio = valorFinal;
            enquadraResultado.ValorBase = valorBase;
            enquadraResultado.Resultado = resultadoFinal;
            enquadraResultado.Enquadrado = enquadrado;

            if (!realTime)
            {
                enquadraResultado.Save();

                //Atualiza a collection de resultado detalhe
                foreach (EnquadraResultadoDetalhe enquadraResultadoDetalhe in enquadraResultadoDetalheCollection)
                {
                    enquadraResultadoDetalhe.IdResultadoOrigem = enquadraResultado.IdResultado.Value;
                }
                enquadraResultadoDetalheCollection.Save();
            }

            return enquadraResultado;

        }

        /// <summary>
        /// Calcula % de oscilação na cota da própria carteira ou em ativos do tipo cotas de investimentos.
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        public void CalculaLimiteOscilacao(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data)
        {
            int idRegra = enquadraRegra.IdRegra.Value;
            int subTipoRegra = enquadraRegra.SubTipoRegra.Value;

            #region Análise de oscilação da cota da própria carteira
            if (subTipoRegra == SubTipoRegraEnquadra.LimiteOscilacao.CotaCarteira)
            {
                decimal retorno = 0;
                try
                {
                    CalculoMedida calculoMedida = new CalculoMedida(idCarteira);
                    retorno = calculoMedida.CalculaRetornoDia(data);
                }
                catch
                {                    
                }                

                string enquadrado = "S";
                if (enquadraRegra.Minimo.HasValue)
                {
                    decimal minimo = enquadraRegra.Minimo.Value;
                    if (retorno < minimo)
                        enquadrado = "N";
                }
                if (enquadraRegra.Maximo.HasValue)
                {
                    decimal maximo = enquadraRegra.Maximo.Value;
                    if (retorno > maximo)
                        enquadrado = "N";
                }

                //Salva o resultado em EnquadraResultado
                EnquadraResultado enquadraResultado = new EnquadraResultado();
                enquadraResultado.IdRegra = idRegra;
                enquadraResultado.IdCarteira = idCarteira;
                enquadraResultado.Data = data;
                enquadraResultado.ValorCriterio = retorno;
                enquadraResultado.ValorBase = 0;
                enquadraResultado.Resultado = 0;
                enquadraResultado.Enquadrado = enquadrado;
                enquadraResultado.Save();
                //
            }
            #endregion

            #region Análise de oscilação da cota de cada um dos fundos que compõem a carteira
            else if (subTipoRegra == SubTipoRegraEnquadra.LimiteOscilacao.CotaInvestimento)
            {
                CalculoMedida calculoMedida = new CalculoMedida(idCarteira);
                decimal retorno = calculoMedida.CalculaRetornoDia(data);

                PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                posicaoFundoCollection.BuscaPosicaoDistinctFundo(idCarteira);

                string enquadrado = "S";
                decimal retornoFinal = 0;                
                for (int i = 0; i < posicaoFundoCollection.Count; i++)
                {
                    PosicaoFundo posicaoFundo = posicaoFundoCollection[i];
                    int idFundo = posicaoFundo.IdCarteira.Value;

                    calculoMedida = new CalculoMedida(idFundo);
                    retorno = calculoMedida.CalculaRetornoDia(data);
                                        
                    if (enquadraRegra.Minimo.HasValue)
                    {
                        decimal minimo = enquadraRegra.Minimo.Value;
                        if (retorno < minimo)
                        {
                            enquadrado = "N";
                            retornoFinal = retorno;
                        }
                    }
                    if (enquadraRegra.Maximo.HasValue)
                    {
                        decimal maximo = enquadraRegra.Maximo.Value;
                        if (retorno > maximo)
                        {
                            enquadrado = "N";
                            retornoFinal = retorno;
                        }
                    }

                    //Busca o nome do fundo/clube
                    Carteira carteira = new Carteira();                    
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(carteira.Query.Nome);
                    carteira.LoadByPrimaryKey(campos, idFundo);
                    string nomeFundo = carteira.Nome;
                    //

                    //Salva o resultado em EnquadraResultadoDetalhe
                    EnquadraResultadoDetalhe enquadraResultadoDetalhe = new EnquadraResultadoDetalhe();
                    enquadraResultadoDetalhe.IdRegra = idRegra;
                    enquadraResultadoDetalhe.IdCarteira = idCarteira;
                    enquadraResultadoDetalhe.Data = data;
                    enquadraResultadoDetalhe.Item = nomeFundo;
                    enquadraResultadoDetalhe.ValorCriterio = retorno;
                    enquadraResultadoDetalhe.ValorBase = 0;
                    enquadraResultadoDetalhe.Resultado = 0;
                    enquadraResultadoDetalhe.Enquadrado = enquadrado;
                    enquadraResultadoDetalhe.Save();
                    //
                }

                //Salva o resultado em EnquadraResultado
                EnquadraResultado enquadraResultado = new EnquadraResultado();
                enquadraResultado.IdRegra = idRegra;
                enquadraResultado.IdCarteira = idCarteira;
                enquadraResultado.Data = data;
                enquadraResultado.ValorCriterio = retornoFinal;
                enquadraResultado.ValorBase = 0;
                enquadraResultado.Resultado = 0;
                enquadraResultado.Enquadrado = enquadrado;
                enquadraResultado.Save();
                //

            }
            #endregion

        }

        /// <summary>
        /// Calcula % de oscilação em relação ao parâmetro de descvio médio na cota da própria carteira ou em ativos do tipo cotas de investimentos.
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        /// <param name="numeroDias"></param>
        public void CalculaLimiteDesvio(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data, int numeroDias)
        {
            int idRegra = enquadraRegra.IdRegra.Value;
            int subTipoRegra = enquadraRegra.SubTipoRegra.Value;

            #region Análise de oscilação da cota da própria carteira
            if (subTipoRegra == SubTipoRegraEnquadra.LimiteOscilacao.CotaCarteira)
            {
                List<decimal> listaValores = new List<decimal>();
                DateTime dataAux = data;
                CalculoMedida calculoMedida = new CalculoMedida(idCarteira);
                decimal retornoReferencia = 0;
                decimal somaRetornos = 0;

                int i = 0;
                while (i < numeroDias)
                {                    
                    try
                    {
                        decimal retornoDia = calculoMedida.CalculaRetornoDia(dataAux);
                        listaValores.Add(retornoDia);

                        if (i == 0)
                        {
                            retornoReferencia = retornoDia;
                        }

                        somaRetornos += retornoDia;
                    }
                    catch (Exception)
                    {
                        break;
                    }

                    dataAux = Calendario.SubtraiDiaUtil(dataAux, 1);

                    i++;
                }

                string enquadrado = "S";
                decimal retornoCriterio = 0;
                decimal desvio = 9999999999;
                decimal media = 9999999999;
                if (i != 0)
                {
                    media = somaRetornos / i;
                    desvio = calculoMedida.CalculaDesvioPadrao(listaValores);
                                        
                    if (enquadraRegra.Minimo.HasValue)
                    {
                        decimal minimo = enquadraRegra.Minimo.Value;
                        retornoCriterio = media - (minimo * desvio);

                        if (retornoReferencia < retornoCriterio)
                        {
                            enquadrado = "N";
                        }
                    }
                    if (enquadraRegra.Maximo.HasValue)
                    {
                        decimal maximo = enquadraRegra.Maximo.Value;
                        retornoCriterio = media + (maximo * desvio);

                        if (retornoReferencia > retornoCriterio)
                        {
                            enquadrado = "N";
                        }
                    }
                }

                //Salva o resultado em EnquadraResultado
                EnquadraResultado enquadraResultado = new EnquadraResultado();
                enquadraResultado.IdRegra = idRegra;
                enquadraResultado.IdCarteira = idCarteira;
                enquadraResultado.Data = data;
                enquadraResultado.ValorCriterio = desvio;
                enquadraResultado.ValorBase = media;
                enquadraResultado.Resultado = retornoReferencia;
                enquadraResultado.Enquadrado = enquadrado;
                enquadraResultado.Save();
                //
            }
            #endregion

            #region Análise de oscilação da cota de cada um dos fundos que compõem a carteira
            else if (subTipoRegra == SubTipoRegraEnquadra.LimiteOscilacao.CotaInvestimento)
            {
                PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                posicaoFundoCollection.BuscaPosicaoDistinctFundo(idCarteira);

                string enquadrado = "S";
                decimal retornoReferenciaFinal = 0;
                decimal retornoCriterioFinal = 0;

                EnquadraResultadoDetalheCollection enquadraResultadoDetalheCollection = new EnquadraResultadoDetalheCollection();
                for (int i = 0; i < posicaoFundoCollection.Count; i++)
                {
                    PosicaoFundo posicaoFundo = posicaoFundoCollection[i];
                    int idFundo = posicaoFundo.IdCarteira.Value;

                    List<decimal> listaValores = new List<decimal>();
                    DateTime dataAux = data;
                    CalculoMedida calculoMedida = new CalculoMedida(idCarteira);
                    decimal retornoReferencia = 0;
                    decimal somaRetornos = 0;

                    int j = 0;
                    while (j < numeroDias)
                    {                    
                        try
                        {
                            decimal retornoDia = calculoMedida.CalculaRetornoDia(dataAux);
                            listaValores.Add(retornoDia);

                            if (j == 0)
                            {
                                retornoReferencia = retornoDia;
                            }

                            somaRetornos += retornoDia;
                        }
                        catch (Exception)
                        {
                            break;
                        }

                        dataAux = Calendario.SubtraiDiaUtil(dataAux, 1);

                        j++;
                    }
                                  
                    decimal retornoCriterio = 0;
                    if (j != 0)
                    {
                        decimal media = somaRetornos / j;

                        decimal desvio = calculoMedida.CalculaDesvioPadrao(listaValores);

                        if (enquadraRegra.Minimo.HasValue)
                        {
                            decimal minimo = enquadraRegra.Minimo.Value;
                            retornoCriterio = media - (minimo * desvio);

                            if (retornoReferencia < retornoCriterio)
                            {
                                enquadrado = "N";
                                retornoReferenciaFinal = retornoReferencia;
                                retornoCriterioFinal = retornoCriterio;
                            }
                        }
                        if (enquadraRegra.Maximo.HasValue)
                        {
                            decimal maximo = enquadraRegra.Maximo.Value;
                            retornoCriterio = media + (maximo * desvio);

                            if (retornoReferencia > retornoCriterio)
                            {
                                enquadrado = "N";
                                retornoReferenciaFinal = retornoReferencia;
                                retornoCriterioFinal = retornoCriterio;
                            }
                        }
                    }

                    //Busca o nome do fundo/clube
                    Carteira carteira = new Carteira();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(carteira.Query.Nome);
                    carteira.LoadByPrimaryKey(campos, idFundo);
                    string nomeFundo = carteira.Nome;
                    //

                    //Salva o resultado em EnquadraResultadoDetalhe
                    EnquadraResultadoDetalhe enquadraResultadoDetalhe = new EnquadraResultadoDetalhe();
                    enquadraResultadoDetalhe.IdRegra = idRegra;
                    enquadraResultadoDetalhe.IdCarteira = idCarteira;
                    enquadraResultadoDetalhe.Data = data;
                    enquadraResultadoDetalhe.Item = nomeFundo;
                    enquadraResultadoDetalhe.ValorCriterio = retornoCriterio;
                    enquadraResultadoDetalhe.ValorBase = 0;
                    enquadraResultadoDetalhe.Resultado = retornoReferencia;
                    enquadraResultadoDetalhe.Enquadrado = enquadrado;
                    enquadraResultadoDetalheCollection.AttachEntity(enquadraResultadoDetalhe);
                    //
                }

                //Salva o resultado em EnquadraResultado
                EnquadraResultado enquadraResultado = new EnquadraResultado();
                enquadraResultado.IdRegra = idRegra;
                enquadraResultado.IdCarteira = idCarteira;
                enquadraResultado.Data = data;
                enquadraResultado.ValorCriterio = retornoCriterioFinal;
                enquadraResultado.ValorBase = 0;
                enquadraResultado.Resultado = retornoReferenciaFinal;
                enquadraResultado.Enquadrado = enquadrado;
                enquadraResultado.Save();
                //

                foreach (EnquadraResultadoDetalhe enquadraResultadoDetalhe in enquadraResultadoDetalheCollection)
                {
                    enquadraResultadoDetalhe.IdResultadoOrigem = enquadraResultado.IdResultado;
                }
                enquadraResultadoDetalheCollection.Save();
            }
            #endregion

        }

        /// <summary>
        /// Método private para cálculo exclusivo de participacao em PL de fundo de investimento.
        /// </summary>
        /// <param name="enquadraRegra"></param>
        /// <param name="idCarteira"></param>
        /// <param name="historico"></param>
        /// <param name="data"></param>
        public void CalculaParticipacaoFundo(EnquadraRegra enquadraRegra, int idCarteira, bool historico, DateTime data)
        {
            EnquadraResultadoDetalheCollection enquadraResultadoDetalheCollection = new EnquadraResultadoDetalheCollection();

            int idRegra = enquadraRegra.IdRegra.Value;
                        
            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            posicaoFundoCollection.BuscaPosicaoFundoAgrupadoEnquadra(idCarteira);

            string enquadrado = "S";
            string enquadradoFundo = "S";
            decimal valorFinal = 0;
            decimal resultadoFinal = 0;
            for (int i = 0; i < posicaoFundoCollection.Count; i++)
            {
                enquadradoFundo = "S";
                PosicaoFundo posicaoFundo = posicaoFundoCollection[i];
                int idFundo = posicaoFundo.IdCarteira.Value;

                //Busca o nome do fundo
                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.Nome);
                carteira.LoadByPrimaryKey(campos, idFundo);
                string nomeFundo = carteira.Nome;
                //

                decimal valorCriterio = posicaoFundo.ValorBruto.Value;

                decimal plFundo = 0;
                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.BuscaValorPatrimonioDia(idFundo, data);
                
                if (historicoCota.PLFechamento.HasValue)
                {
                    plFundo = historicoCota.PLFechamento.Value;
                }
                else
                {
                    continue;
                }
                
                decimal resultado = Utilitario.Truncate(valorCriterio / plFundo, 4) * 100;

                if (enquadraRegra.Minimo.HasValue)
                {
                    decimal minimo = enquadraRegra.Minimo.Value;
                    if (resultado < minimo)
                    {
                        enquadrado = "N";
                        enquadradoFundo = "N";
                        valorFinal = valorCriterio;
                        resultadoFinal = resultado;
                    }
                }
                
                if (enquadraRegra.Maximo.HasValue)
                {
                    decimal maximo = enquadraRegra.Maximo.Value;
                    if (resultado > maximo)
                    {
                        enquadrado = "N";
                        enquadradoFundo = "N";
                        valorFinal = valorCriterio;
                        resultadoFinal = resultado;
                    }
                }

                //Salva o resultado Final em EnquadraResultado
                EnquadraResultado enquadraResultado = new EnquadraResultado();
                enquadraResultado.IdRegra = idRegra;
                enquadraResultado.IdCarteira = idCarteira;
                enquadraResultado.Data = data;
                enquadraResultado.ValorCriterio = valorFinal;
                enquadraResultado.ValorBase = plFundo;
                enquadraResultado.Resultado = resultadoFinal;
                enquadraResultado.Enquadrado = enquadrado;
                enquadraResultado.Save();
                //

                //Salva o resultado em EnquadraResultadoDetalhe
                EnquadraResultadoDetalhe enquadraResultadoDetalhe = new EnquadraResultadoDetalhe();
                enquadraResultadoDetalhe.IdRegra = idRegra;
                enquadraResultadoDetalhe.IdCarteira = idCarteira;
                enquadraResultadoDetalhe.Data = data;
                enquadraResultadoDetalhe.Item = nomeFundo;
                enquadraResultadoDetalhe.ValorCriterio = valorCriterio;
                enquadraResultadoDetalhe.ValorBase = plFundo;
                enquadraResultadoDetalhe.Resultado = resultado;
                enquadraResultadoDetalhe.Enquadrado = enquadradoFundo;
                enquadraResultadoDetalhe.IdResultadoOrigem = enquadraResultado.IdResultado.Value;

                enquadraResultadoDetalheCollection.AttachEntity(enquadraResultadoDetalhe);                
                
            }
            
            enquadraResultadoDetalheCollection.Save();
        }
                

	}
}
