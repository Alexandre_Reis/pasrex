﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Enquadra.Enums;
using Financial.Bolsa;
using Financial.Enquadra.Exceptions;
using Financial.BMF;
using Financial.Swap;
using Financial.Fundo;
using Financial.RendaFixa;
using Financial.Fundo.Enums;
using Financial.Util;

namespace Financial.Enquadra
{
	public partial class EnquadraGrupo : esEnquadraGrupo
	{
        /// <summary>
        /// Busca todos os itens (ações, opções, futuros...) referentes ao grupo, 
        /// e calcula o somatório de todos os itens.
        /// Trata itens especiais, buscando o valor direto.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idGrupo"></param>
        /// <param name="historico">Indica se deve buscar na posição atual ou histórica</param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaValorGrupoItem(int idCarteira, int idGrupo, bool historico, DateTime data, bool realTime) {            

            #region Trata itens especiais, retornando direto o valor se for o caso
            //PL de D0
            if (idGrupo == (int)ListaGrupoFixoEnquadra.PL_D0)
            {
                //Busca o tipo de cota (abert ou fech)
                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.TipoCota);
                carteira.LoadByPrimaryKey(campos, idCarteira);
                int tipoCota = carteira.TipoCota.Value;
                //

                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.BuscaValorPatrimonioDia(idCarteira, data);

                if (tipoCota == (int)TipoCotaFundo.Abertura)
                    return historicoCota.PLAbertura.Value;
                else
                    return historicoCota.PLFechamento.Value;

            }
            //

            //PL de D - 1
            if (idGrupo == (int)ListaGrupoFixoEnquadra.PL_D1)
            {
                //Busca o tipo de cota (abert ou fech)
                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.TipoCota);
                carteira.LoadByPrimaryKey(campos, idCarteira);
                int tipoCota = carteira.TipoCota.Value;
                //

                DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);

                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.BuscaValorPatrimonioDia(idCarteira, dataAnterior);

                if (tipoCota == (int)TipoCotaFundo.Abertura)
                    return historicoCota.PLAbertura.Value;
                else
                    return historicoCota.PLFechamento.Value;

            }
            //
            #endregion

            decimal totalValorGrupo = 0;
            EnquadraGrupoItemCollection enquadraGrupoItemCollection = new EnquadraGrupoItemCollection();
            if (enquadraGrupoItemCollection.BuscaGrupoItem(idGrupo)) {
                for (int i = 0; i < enquadraGrupoItemCollection.Count; i++) {
                    EnquadraGrupoItem enquadraGrupoItem = enquadraGrupoItemCollection[i];
                    int idItem = enquadraGrupoItem.IdItem.Value;
                    int tipoItem = enquadraGrupoItem.UpToEnquadraItemByIdItem.Tipo.Value; //HIERARQUICO

                    List<esQueryItem> campos = new List<esQueryItem>();

                    decimal valorItem = 0;
                    switch (tipoItem) {
                        #region Ação (leva em conta posições doadas e tomadas de aluguel de ações - BTC)
                        case (int)TipoItemEnquadra.Acao:
                            EnquadraItemAcao enquadraItemAcao = new EnquadraItemAcao();
			                campos.Add(enquadraItemAcao.Query.IdEmissor);
                            campos.Add(enquadraItemAcao.Query.IdSetor);
                            campos.Add(enquadraItemAcao.Query.TipoPapel);
                            campos.Add(enquadraItemAcao.Query.TipoMercado);
                            campos.Add(enquadraItemAcao.Query.Bloqueado);
                            if (!enquadraItemAcao.LoadByPrimaryKey(campos, idItem))
                                throw new EnquadraItemNaoCadastradoException("Item de enquadramento (ação) não está devidamente cadastrado, nr do item = " + idItem);

                            int? idEmissor = enquadraItemAcao.IdEmissor;
                            int? idSetor = enquadraItemAcao.IdSetor;
                            int? tipoPapelAcao = enquadraItemAcao.TipoPapel;
                            string tipoMercado = String.IsNullOrEmpty(enquadraItemAcao.TipoMercado) ? "" : enquadraItemAcao.TipoMercado;
                            string bloqueado = enquadraItemAcao.Bloqueado;

                            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                            valorItem = posicaoBolsa.RetornaValorMercadoAcaoEnquadra(idCarteira, idEmissor, idSetor, tipoPapelAcao, tipoMercado, bloqueado);
                            break;
                        #endregion

                        #region Opção Bolsa
                        case (int)TipoItemEnquadra.OpcaoBolsa:
                            EnquadraItemOpcaoBolsa enquadraItemOpcaoBolsa = new EnquadraItemOpcaoBolsa();
                            campos.Add(enquadraItemOpcaoBolsa.Query.Posicao);
                            if (!enquadraItemOpcaoBolsa.LoadByPrimaryKey(campos, idItem))
                                throw new EnquadraItemNaoCadastradoException("Item de enquadramento (opção) não está devidamente cadastrado, nr do item = " + idItem);

                            string posicaoOpcao = String.IsNullOrEmpty(enquadraItemOpcaoBolsa.Posicao) ? "" : enquadraItemOpcaoBolsa.Posicao;
                            
                            posicaoBolsa = new PosicaoBolsa();
                            valorItem = posicaoBolsa.RetornaValorMercadoOpcaoEnquadra(idCarteira, posicaoOpcao);
                            break;
                        #endregion

                        #region Termo Bolsa
                        case (int)TipoItemEnquadra.TermoBolsa:
                            EnquadraItemTermoBolsa enquadraItemTermoBolsa = new EnquadraItemTermoBolsa();
                            campos.Add(enquadraItemTermoBolsa.Query.Posicao);
                            if (!enquadraItemTermoBolsa.LoadByPrimaryKey(campos, idItem))
                                throw new EnquadraItemNaoCadastradoException("Item de enquadramento (Termo) não está devidamente cadastrado, nr do item = " + idItem);

                            string posicaoTermo = String.IsNullOrEmpty(enquadraItemTermoBolsa.Posicao) ? "" : enquadraItemTermoBolsa.Posicao;

                            PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();
                            valorItem = posicaoTermoBolsa.RetornaValorMercadoEnquadra(idCarteira, posicaoTermo);
                            break;
                        #endregion

                        #region Futuro BMF
                        case (int)TipoItemEnquadra.FuturoBMF:
                            EnquadraItemFuturoBMF enquadraItemFuturoBMF = new EnquadraItemFuturoBMF();
                            campos.Add(enquadraItemFuturoBMF.Query.CdAtivoBMF);
                            campos.Add(enquadraItemFuturoBMF.Query.Posicao);
                            if (!enquadraItemFuturoBMF.LoadByPrimaryKey(campos, idItem))
                                throw new EnquadraItemNaoCadastradoException("Item de enquadramento (Futuro BMF) não está devidamente cadastrado, nr do item = " + idItem);

                            string cdAtivoBMF = enquadraItemFuturoBMF.CdAtivoBMF;

                            string posicaoFuturo = String.IsNullOrEmpty(enquadraItemFuturoBMF.Posicao) ? "" : enquadraItemFuturoBMF.Posicao;

                            PosicaoBMF posicaoBMF = new PosicaoBMF();
                            valorItem = posicaoBMF.RetornaValorMercadoFuturoEnquadra(idCarteira, cdAtivoBMF, posicaoFuturo);
                            break;
                        #endregion

                        #region Opção BMF
                        case (int)TipoItemEnquadra.OpcaoBMF:
                            EnquadraItemOpcaoBMF enquadraItemOpcaoBMF = new EnquadraItemOpcaoBMF();
                            campos = new List<esQueryItem>();
                            campos.Add(enquadraItemOpcaoBMF.Query.CdAtivoBMF);
                            if (!enquadraItemOpcaoBMF.LoadByPrimaryKey(campos, idItem))
                                throw new EnquadraItemNaoCadastradoException("Item de enquadramento (Opção BMF) não está devidamente cadastrado, nr do item = " + idItem);

                            cdAtivoBMF = enquadraItemOpcaoBMF.CdAtivoBMF;

                            posicaoBMF = new PosicaoBMF();
                            valorItem = posicaoBMF.RetornaValorMercadoOpcaoEnquadra(idCarteira, cdAtivoBMF);
                            break;
                        #endregion

                        #region Renda Fixa
                        case (int)TipoItemEnquadra.RendaFixa:
                            EnquadraItemRendaFixa enquadraItemRendaFixa = new EnquadraItemRendaFixa();
                            campos = new List<esQueryItem>();
                            campos.Add(enquadraItemRendaFixa.Query.OperacaoCompromissada);
                            campos.Add(enquadraItemRendaFixa.Query.IdIndice);
                            campos.Add(enquadraItemRendaFixa.Query.IdPapel);
                            campos.Add(enquadraItemRendaFixa.Query.IdEmissor);
                            campos.Add(enquadraItemRendaFixa.Query.TipoEmissor);
                            campos.Add(enquadraItemRendaFixa.Query.IdSetor);
                            campos.Add(enquadraItemRendaFixa.Query.TipoPapel);
                            if (!enquadraItemRendaFixa.LoadByPrimaryKey(campos, idItem))
                                throw new EnquadraItemNaoCadastradoException("Item de enquadramento (RendaFixa) não está devidamente cadastrado, nr do item = " + idItem);

                            string operacaoCompromissada = enquadraItemRendaFixa.OperacaoCompromissada;
                            int? idIndice = enquadraItemRendaFixa.IdIndice;
                            int? idPapel = enquadraItemRendaFixa.IdPapel;
                            idEmissor = enquadraItemRendaFixa.IdEmissor;
                            int? tipoEmissor = enquadraItemRendaFixa.TipoEmissor;
                            idSetor = enquadraItemRendaFixa.IdSetor;
                            int? tipoPapel = enquadraItemRendaFixa.TipoPapel;

                            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                            valorItem = posicaoRendaFixa.RetornaValorMercadoEnquadra(idCarteira, operacaoCompromissada,
                                                                                     idIndice, idPapel, idEmissor,
                                                                                     tipoEmissor, idSetor, tipoPapel);
                            break;
                        #endregion

                        #region Swap
                        case (int)TipoItemEnquadra.Swap:
                            PosicaoSwap posicaoSwap = new PosicaoSwap();
                            posicaoSwap.RetornaValorMercadoEnquadra(idCarteira);
                            break;
                        #endregion

                        #region Cotas de Investimentos
                        case (int)TipoItemEnquadra.CotaInvestimento:
                            EnquadraItemFundo enquadraItemFundo = new EnquadraItemFundo();
                            campos = new List<esQueryItem>();
                            campos.Add(enquadraItemFundo.Query.TipoCarteira);
                            campos.Add(enquadraItemFundo.Query.IdAgenteAdministrador);
                            campos.Add(enquadraItemFundo.Query.IdAgenteGestor);
                            campos.Add(enquadraItemFundo.Query.IdIndice);
                            campos.Add(enquadraItemFundo.Query.IdCarteira);
                            campos.Add(enquadraItemFundo.Query.IdCategoria);
                            if (!enquadraItemFundo.LoadByPrimaryKey(campos, idItem))
                                throw new EnquadraItemNaoCadastradoException("Item de enquadramento (Fundo) não está devidamente cadastrado, nr do item = " + idItem);

                            int? tipoCarteira = enquadraItemFundo.TipoCarteira;
                            int? idAgenteAdministrador = enquadraItemFundo.IdAgenteAdministrador;
                            int? idAgenteGestor = enquadraItemFundo.IdAgenteGestor;
                            idIndice = enquadraItemFundo.IdIndice;
                            int? idFundo = enquadraItemFundo.IdCarteira; //Este é o id do fundo específico em posição
                            int? idCategoria = enquadraItemFundo.IdCategoria;

                            PosicaoFundo posicaoFundo = new PosicaoFundo();
                            valorItem = posicaoFundo.RetornaValorMercadoEnquadra(idCarteira, tipoCarteira, 
                                                                                 idAgenteAdministrador, idAgenteGestor,
                                                                                 idIndice, idFundo, idCategoria);
                            break;
                        #endregion                        
                    }

                    totalValorGrupo += valorItem;
                }
            }

            return totalValorGrupo;
        }

        /// <summary>
        /// Calcula o valor de mercado do grupo (ações / renda fixa), dado o emissor informado.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idGrupo"></param>
        /// <param name="historico">Indica se deve buscar na posição atual ou histórica</param>
        /// <param name="data"></param>
        /// <param name="idEmissor"></param>
        /// <returns></returns>
        public decimal RetornaValorGrupoItemPorEmissor(int idCarteira, int idGrupo, bool historico, DateTime data,
                                                       int idEmissor, bool realTime)
        {

            decimal totalValorGrupo = 0;
            EnquadraGrupoItemCollection enquadraGrupoItemCollection = new EnquadraGrupoItemCollection();
            if (enquadraGrupoItemCollection.BuscaGrupoItem(idGrupo)) {
                for (int i = 0; i < enquadraGrupoItemCollection.Count; i++) {
                    EnquadraGrupoItem enquadraGrupoItem = enquadraGrupoItemCollection[i];
                    int idItem = enquadraGrupoItem.IdItem.Value;
                    int tipoItem = enquadraGrupoItem.UpToEnquadraItemByIdItem.Tipo.Value; //HIERARQUICO

                    decimal valorItem = 0;
                    switch (tipoItem) {
                        #region Ação
                        case (int)TipoItemEnquadra.Acao:
                            EnquadraItemAcao enquadraItemAcao = new EnquadraItemAcao();
                            List<esQueryItem> campos = new List<esQueryItem>();
                            campos.Add(enquadraItemAcao.Query.IdSetor);
                            if (!enquadraItemAcao.LoadByPrimaryKey(campos, idItem))
                                throw new EnquadraItemNaoCadastradoException("Item de enquadramento (Ações) não está devidamente cadastrado, nr do item = " + idItem);
                            int? idSetorAcao = enquadraItemAcao.IdSetor;
                            int? tipoPapelAcao = enquadraItemAcao.TipoPapel;
                            string tipoMercado = enquadraItemAcao.TipoMercado;
                            string bloqueado = enquadraItemAcao.Bloqueado;

                            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                            valorItem = posicaoBolsa.RetornaValorMercadoAcaoEnquadra(idCarteira, idEmissor, idSetorAcao, tipoPapelAcao, tipoMercado, bloqueado);
                            break;
                        #endregion
                                                                    
                        #region Renda Fixa
                        case (int)TipoItemEnquadra.RendaFixa:
                            EnquadraItemRendaFixa enquadraItemRendaFixa = new EnquadraItemRendaFixa();
                            campos = new List<esQueryItem>();
                            campos.Add(enquadraItemRendaFixa.Query.OperacaoCompromissada);
                            campos.Add(enquadraItemRendaFixa.Query.IdIndice);
                            campos.Add(enquadraItemRendaFixa.Query.IdPapel);
                            campos.Add(enquadraItemRendaFixa.Query.IdSetor);                            
                            campos.Add(enquadraItemRendaFixa.Query.TipoEmissor);
                            campos.Add(enquadraItemRendaFixa.Query.TipoPapel);
                            if (!enquadraItemRendaFixa.LoadByPrimaryKey(campos, idItem))
                                throw new EnquadraItemNaoCadastradoException("Item de enquadramento (RendaFixa) não está devidamente cadastrado, nr do item = " + idItem);

                            string operacaoCompromissada = String.IsNullOrEmpty(enquadraItemRendaFixa.OperacaoCompromissada) ? "" : enquadraItemRendaFixa.OperacaoCompromissada;
                            int? idIndice = enquadraItemRendaFixa.IdIndice;
                            int? idPapel = enquadraItemRendaFixa.IdPapel;
                            short? idSetorRF = enquadraItemRendaFixa.IdSetor;
                            byte? tipoEmissor = enquadraItemRendaFixa.TipoEmissor;
                            int? tipoPapel = enquadraItemRendaFixa.TipoPapel;

                            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                            valorItem = posicaoRendaFixa.RetornaValorMercadoEnquadra(idCarteira, operacaoCompromissada,
                                                                                     idIndice, idPapel, idEmissor, tipoEmissor, idSetorRF, tipoPapel);
                            break;
                        #endregion                                                    
                    }

                    totalValorGrupo += valorItem;
                }
            }

            return totalValorGrupo;
        }

        /// <summary>
        /// Calcula o valor de mercado os fundos associados ao idAdministrador informado.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idGrupo"></param>
        /// <param name="historico">Indica se deve buscar na posição atual ou histórica</param>
        /// <param name="data"></param>
        /// <param name="idAdministrador"></param>
        /// <returns></returns>
        public decimal RetornaValorGrupoItemPorAdministrador(int idCarteira, int idGrupo, bool historico, DateTime data,
                                                             int idAdministrador, bool realTime)
        {
            decimal totalValorGrupo = 0;
            EnquadraGrupoItemCollection enquadraGrupoItemCollection = new EnquadraGrupoItemCollection();
            if (enquadraGrupoItemCollection.BuscaGrupoItem(idGrupo))
            {
                for (int i = 0; i < enquadraGrupoItemCollection.Count; i++)
                {
                    EnquadraGrupoItem enquadraGrupoItem = enquadraGrupoItemCollection[i];
                    int idItem = enquadraGrupoItem.IdItem.Value;
                    
                    decimal valorItem = 0;
                        
                    EnquadraItemFundo enquadraItemFundo = new EnquadraItemFundo();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(enquadraItemFundo.Query.TipoCarteira);
                    campos.Add(enquadraItemFundo.Query.IdAgenteAdministrador);
                    campos.Add(enquadraItemFundo.Query.IdIndice);
                    campos.Add(enquadraItemFundo.Query.IdCarteira);
                    campos.Add(enquadraItemFundo.Query.IdCategoria);
                    if (!enquadraItemFundo.LoadByPrimaryKey(campos, idItem))
                        throw new EnquadraItemNaoCadastradoException("Item de enquadramento (Fundo) não está devidamente cadastrado, nr do item = " + idItem);

                    int? tipoCarteira = enquadraItemFundo.TipoCarteira;
                    int? idIndice = enquadraItemFundo.IdIndice;
                    int? idFundo = enquadraItemFundo.IdCarteira; //Este é o id do fundo específico em posição
                    int? idCategoria = enquadraItemFundo.IdCategoria;

                    PosicaoFundo posicaoFundo = new PosicaoFundo();
                    valorItem = posicaoFundo.RetornaValorMercadoEnquadra(idCarteira, tipoCarteira,
                                                                         idAdministrador, null,
                                                                         idIndice, idFundo, idCategoria);
                    
                    totalValorGrupo += valorItem;
                }
            }

            return totalValorGrupo;
        }

        /// <summary>
        /// Calcula o valor de mercado os fundos associados ao idGestor informado.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idGrupo"></param>
        /// <param name="historico">Indica se deve buscar na posição atual ou histórica</param>
        /// <param name="data"></param>
        /// <param name="idGestor"></param>
        /// <returns></returns>
        public decimal RetornaValorGrupoItemPorGestor(int idCarteira, int idGrupo, bool historico, DateTime data,
                                                      int idGestor, bool realTime)
        {
            decimal totalValorGrupo = 0;
            EnquadraGrupoItemCollection enquadraGrupoItemCollection = new EnquadraGrupoItemCollection();
            if (enquadraGrupoItemCollection.BuscaGrupoItem(idGrupo))
            {
                for (int i = 0; i < enquadraGrupoItemCollection.Count; i++)
                {
                    EnquadraGrupoItem enquadraGrupoItem = enquadraGrupoItemCollection[i];
                    int idItem = enquadraGrupoItem.IdItem.Value;

                    decimal valorItem = 0;

                    EnquadraItemFundo enquadraItemFundo = new EnquadraItemFundo();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(enquadraItemFundo.Query.TipoCarteira);
                    campos.Add(enquadraItemFundo.Query.IdIndice);
                    campos.Add(enquadraItemFundo.Query.IdCarteira);
                    if (!enquadraItemFundo.LoadByPrimaryKey(campos, idItem))
                        throw new EnquadraItemNaoCadastradoException("Item de enquadramento (Fundo) não está devidamente cadastrado, nr do item = " + idItem);

                    int? tipoCarteira = enquadraItemFundo.TipoCarteira;
                    int? idIndice = enquadraItemFundo.IdIndice;
                    int? idFundo = enquadraItemFundo.IdCarteira; //Este é o id do fundo específico em posição
                    int? idCategoria = enquadraItemFundo.IdCategoria;

                    PosicaoFundo posicaoFundo = new PosicaoFundo();
                    valorItem = posicaoFundo.RetornaValorMercadoEnquadra(idCarteira, tipoCarteira,
                                                                         null, idGestor,
                                                                         idIndice, idFundo, idCategoria);

                    totalValorGrupo += valorItem;
                }
            }

            return totalValorGrupo;
        }

	}
}
