using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Enquadra
{
	public partial class EnquadraResultadoCollection : esEnquadraResultadoCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(EnquadraResultadoCollection));

        /// <summary>
        /// Deleta todos os resultados de enquadramento da carteira na data.
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="data"></param>  
        public void DeletaEnquadramentoResultado(int idCarteira, DateTime data)
        {
            // TODO log da funcao                              

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdResultado)
                 .Where(this.Query.IdCarteira.Equal(idCarteira),
                        this.Query.Data.GreaterThanOrEqual(data));

            this.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled)
            {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@IdCarteira1", "'" + idCarteira + "'");
                sql = sql.Replace("@Data2", "'" + data + "'");
                log.Info(sql);
            }
            #endregion

            #region logSaida
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            this.MarkAllAsDeleted();
            this.Save();
        }

	}
}
