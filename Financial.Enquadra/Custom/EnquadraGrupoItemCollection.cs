using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Enquadra
{
	public partial class EnquadraGrupoItemCollection : esEnquadraGrupoItemCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(EnquadraGrupoItemCollection));

        /// <summary>
        /// Carrega o objeto EnquadraGrupoItemCollection com todos os campos de EnquadraGrupoItem.
        /// </summary>
        /// <param name="idGrupo"></param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaGrupoItem(int idGrupo)
        {
            // TODO log da funcao                              

            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdGrupo == idGrupo);

            bool retorno = this.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled)
            {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@IdGrupo1", "'" + idGrupo + "'");
                log.Info(sql);
            }
            #endregion

            #region logSaida
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            return retorno;
        }

	}
}
