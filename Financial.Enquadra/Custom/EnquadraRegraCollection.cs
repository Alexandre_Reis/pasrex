using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Enquadra
{
	public partial class EnquadraRegraCollection : esEnquadraRegraCollection
	{        
        /// <summary>
        /// Carrega o objeto EnquadraRegraCollection com todos os campos de EnquadraRegra.
        /// Filtra para DataReferencia.LessThanOrEqual(data),
        /// (DataFim.IsNull() OR DataFim.GreaterThanOrEqual(data))
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="data"></param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaEnquadraRegrasVigentes(int idCarteira, DateTime data)
        {     
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.DataReferencia.LessThanOrEqual(data),
                        this.Query.Or(
                            this.Query.DataFim.IsNull(),
                            this.Query.DataFim.GreaterThanOrEqual(data))
                        )
                 .OrderBy(this.Query.DataReferencia.Descending);

            bool retorno = this.Query.Load();                

            return retorno;
        }

	}
}
