﻿using System;
using System.Text;
using System.Resources;
using Financial.Util;
using System.Collections.Generic;
using Financial.Enquadra.Properties;
using System.Globalization;
using Financial.Util.Enums;

namespace Financial.Enquadra.Enums
{
    /// <summary>
    /// Todos os valores do Enum devem estar cadastrados no Resource do Componente
    /// </summary>
    public enum TipoEnquadramentoEnquadra {
        
        [StringValue("Legal")]
        Legal = 1,
        
        [StringValue("Gerencial")]
        Gerencial = 2,
    }

    public enum ListaGrupoFixoEnquadra
    {
        PL_D0 = 1, //PL de D0
        PL_D1 = 2 //PL de D-1
    }

    public enum TipoItemEnquadra
    {
        Acao = 1,
        OpcaoBolsa = 2,
        TermoBolsa = 3,
        FuturoBMF = 4,
        OpcaoBMF = 5,
        RendaFixa = 6,
        Swap = 7,
        CotaInvestimento = 8
    }

    public enum RealTimeEnquadra
    {
        Calcula = 1,
        NaoCalcula = 2
    }
    
    /// <summary>
    /// Todos os valores do Enum devem estar cadastrados no Resource do Componente
    /// </summary>
    public enum TipoRegraEnquadra
    {
        [StringValue("LimiteAbsoluto")]
        LimiteAbsoluto = 100,

        [StringValue("ConcentracaoCotista")]
        ConcentracaoCotista = 200,

        [StringValue("NumeroCotista")]
        NumeroCotista = 300,

        [StringValue("PosicaoDescoberto")]
        PosicaoDescoberto = 400,

        [StringValue("MediaMovel")]
        MediaMovel = 500,

        [StringValue("Concentracao")]
        Concentracao = 600,

        [StringValue("LimiteOscilacao")]
        LimiteOscilacao = 800,

        [StringValue("LimiteDesvioMensal")]
        LimiteDesvioMensal = 810,

        [StringValue("LimiteDesvioAnual")]
        LimiteDesvioAnual = 811,

        [StringValue("ParticipacaoFundo")]
        ParticipacaoFundo = 900
    }

    public static class SubTipoRegraEnquadra
    {
        public static class PosicaoDescoberto
        {
            public const int TermoAcao = 401;
            public const int Opcao = 402;
        }

        public static class Concentracao
        {
            public const int CotaInvestimentoAtivo = 601;
            public const int CotaInvestimentoAdministrador = 602;
            public const int CotaInvestimentoGestor = 603;
            public const int AcaoAtivo = 610;
            public const int RendaFixaTitulo = 620;
            public const int Emissor = 630;
        }

        public static class LimiteOscilacao
        {
            public const int CotaCarteira = 801; //Cota da própria carteira em análise
            public const int CotaInvestimento = 802; //Cota de qq fundo pertencente à carteira
        }

    }

    public static class TraducaoEnumsEnquadra {

        #region Funções privates para fazer Pesquisa dentro dos Enums
        /// <summary>
        /// Dado um codigo retorna um Enum do tipo TipoRegraEnquadra
        /// </summary>
        /// <exception>throws ArgumentException se não existir um enum com o valor do código</exception> 
        /// <returns></returns>
        private static TipoRegraEnquadra SearchEnumTipoRegraEnquadra(int codigo) {
            int[] tipoRegraEnquadraValues = (int[])Enum.GetValues(typeof(TipoRegraEnquadra));

            int? posicao = null;
            for (int i = 0; i < tipoRegraEnquadraValues.Length; i++) {
                if (tipoRegraEnquadraValues[i] == codigo) {
                    posicao = i;
                    break;
                }
            }

            if (posicao.HasValue) {
                string tipoRegraEnquadraString = Enum.GetNames(typeof(TipoRegraEnquadra))[posicao.Value];
                // Monta o Enum de acordo com a string
                TipoRegraEnquadra tipoRegraEnquadra = (TipoRegraEnquadra)Enum.Parse(typeof(TipoRegraEnquadra), tipoRegraEnquadraString);
                return tipoRegraEnquadra;
            }
            else {
                throw new ArgumentException("TipoRegraEnquadra não possue Constante com valor " + codigo);
            }            
        }

        /// <summary>
        /// Dado um codigo retorna um Enum do tipo TipoEnquadramentoEnquadra
        /// </summary>
        /// <exception>throws ArgumentException se não existir um enum com o valor do código</exception> 
        /// <returns></returns>
        private static TipoEnquadramentoEnquadra SearchEnumTipoEnquadramentoEnquadra(int codigo) {
            int[] tipoEnquadramentoEnquadraValues = (int[])Enum.GetValues(typeof(TipoEnquadramentoEnquadra));

            int? posicao = null;
            for (int i = 0; i < tipoEnquadramentoEnquadraValues.Length; i++) {
                if (tipoEnquadramentoEnquadraValues[i] == codigo) {
                    posicao = i;
                    break;
                }
            }

            if (posicao.HasValue) {
                string tipoEnquadramentoEnquadraString = Enum.GetNames(typeof(TipoEnquadramentoEnquadra))[posicao.Value];
                // Monta o Enum de acordo com a string
                TipoEnquadramentoEnquadra tipoEnquadramentoEnquadra = (TipoEnquadramentoEnquadra)Enum.Parse(typeof(TipoEnquadramentoEnquadra), tipoEnquadramentoEnquadraString);
                return tipoEnquadramentoEnquadra;
            }
            else {
                throw new ArgumentException("TipoEnquadramentoEnquadra não possue Constante com valor " + codigo);
            }
        }

        #endregion

        public static class EnumTipoRegraEnquadra {
            /// <summary>
            /// Realiza a tradução do Enum TipoRegraEnquadra
            /// </summary>
            /// <param name="codigo"></param>
            /// <returns></returns>
            public static string TraduzEnum(int codigo) {                
                TipoRegraEnquadra t = TraducaoEnumsEnquadra.SearchEnumTipoRegraEnquadra(codigo);
                string chave = "#" + StringEnum.GetStringValue(t);
                return Resources.ResourceManager.GetString(chave);
            }
        }

        public static class EnumTipoEnquadramentoEnquadra {
            /// <summary>
            /// Realiza a tradução do Enum TipoEnquadramentoEnquadra
            /// </summary>
            /// <param name="codigo"></param>
            /// <returns></returns>
            public static string TraduzEnum(int codigo) {
                TipoEnquadramentoEnquadra t = TraducaoEnumsEnquadra.SearchEnumTipoEnquadramentoEnquadra(codigo);
                string chave = "#" + StringEnum.GetStringValue(t);
                return Resources.ResourceManager.GetString(chave);
            }
        }
    }
}
