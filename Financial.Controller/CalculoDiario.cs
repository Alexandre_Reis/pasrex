﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Investidor.Controller;
using EntitySpaces.Interfaces;
using Financial.Controller.Properties;
using ATK.Scheduler.Contracts;
using System.Diagnostics;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Fundo;
using Financial.Common;
using Financial.RendaFixa;
using Financial.Util;
using NDde.Client;

namespace Financial.Controller {
    public class CalculoDiario
    {
        public CalculoDiario()
        {
            this.RegistraConexoes();
        }

        /// <summary>
        /// Registra a Conexão
        /// </summary>
        private void RegistraConexoes() {
            // --- Manually register a connection 
            esConnectionElement conn = new esConnectionElement();
            conn.Name = "SQL";
            conn.ConnectionString = @""+Settings.Default.ConnectionStringCalculoDiarioJob+";";
            conn.Provider = "EntitySpaces.SqlClientProvider";
            conn.ProviderClass = "DataProvider";
            conn.SqlAccessType = esSqlAccessType.DynamicSQL;
            conn.ProviderMetadataKey = "esDefault";
            conn.DatabaseVersion = "2005";
            // --- Assign the Default Connection --- 
            esConfigSettings.ConnectionInfo.Connections.Add(conn);
            esConfigSettings.ConnectionInfo.Default = "SQL";
            // Register the Loader 
            esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        }

        /// <summary>
        /// Calcula o dia de todos os clientes com StatusRealTime = Executar.
        /// </summary>
        public void ExecutaFechamento(bool somenteComStatusExecutar)
        {
            DateTime dataHoje = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            ClienteCollection clienteCollection = new ClienteCollection();            
            clienteCollection.Query.Select(clienteCollection.Query.IdCliente,
                                           clienteCollection.Query.DataDia,
                                           clienteCollection.Query.IsProcessando);
            clienteCollection.Query.Where(clienteCollection.Query.StatusAtivo.Equal(StatusAtivoCliente.Ativo),
                                          clienteCollection.Query.Status.NotEqual((byte)StatusCliente.Divulgado),
                                          clienteCollection.Query.IsProcessando.Equal("N"),
                                          clienteCollection.Query.DataDia.Equal(dataHoje),
                                          clienteCollection.Query.CalculaRealTime.Equal("S"));

            if (somenteComStatusExecutar)
            {
                clienteCollection.Query.Where(clienteCollection.Query.StatusRealTime.Equal((byte)StatusRealTimeCliente.Executar));
            }

            clienteCollection.Query.Load();

            foreach (Cliente cliente in clienteCollection)
            {
                int idCliente = cliente.IdCliente.Value;
                DateTime dataDia = cliente.DataDia.Value;
                
                ParametrosProcessamento parametrosProcessamento = new ParametrosProcessamento();
                parametrosProcessamento.IntegraBolsa = false;
                parametrosProcessamento.IntegraBMF = false;
                parametrosProcessamento.ProcessaBolsa = true;
                parametrosProcessamento.ProcessaBMF = true;
                parametrosProcessamento.ProcessaRendaFixa = true;
                parametrosProcessamento.ProcessaSwap = true;

                Cliente clienteUpdate = new Cliente();

                try
                {
                    //Inicia o processamento mudando o status IsProcessando                    
                    clienteUpdate.LoadByPrimaryKey(idCliente);
                    clienteUpdate.IsProcessando = "S";
                    clienteUpdate.Save();
                    //

                    ControllerInvestidor controllerInvestidor = new ControllerInvestidor();
                    controllerInvestidor.ExecutaFechamento(idCliente, dataDia, parametrosProcessamento);
                }
                catch (Exception ex)
                {
                    string sSourceErro;
                    string sLogErro;
                    string sEventErro;
                    sSourceErro = "Financial.Controller";
                    sLogErro = "Error";
                    sEventErro = "Execução cliente " + idCliente.ToString() + " na data " + DateTime.Now.ToString() +
                        " Erro: " + ex.Message;
                    if (!EventLog.SourceExists(sSourceErro))
                        EventLog.CreateEventSource(sSourceErro, sLogErro);

                    EventLog.WriteEntry(sSourceErro, sEventErro, EventLogEntryType.Error);
                }
                finally
                {
                    clienteUpdate.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
                    clienteUpdate.IsProcessando = "N";
                    clienteUpdate.Save();
                }
            }
        }

        /// <summary>
        /// Atualiza a cotação de todos os clientes com CalculaRealTime = S.
        /// </summary>
        public void AtualizaCotacoes(Dictionary<string, decimal> listaCotacao)
        {
            DateTime data = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            foreach (KeyValuePair<string, decimal> cotacaoItem in listaCotacao)
            {
                string cdAtivo = cotacaoItem.Key.Trim();
                decimal cotacao = cotacaoItem.Value;

                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                if (cotacaoBolsa.LoadByPrimaryKey(data, cdAtivo))
                {
                    cotacaoBolsa.PUFechamento = cotacao;
                    cotacaoBolsa.Save();
                }
                else
                {
                    string cdAtivoBMF = cdAtivo.Substring(0, 3);
                    string serie = cdAtivo.Replace(cdAtivoBMF, "");
                    CotacaoBMF cotacaoBMF = new CotacaoBMF();
                    if (cotacaoBMF.LoadByPrimaryKey(data, cdAtivoBMF, serie))
                    {
                        cotacaoBMF.PUFechamento = cotacao;
                        cotacaoBMF.Save();
                    }
                }
            }            
        }                
    }

    public class AberturaDiaAtual
    {
        /// <summary>
        /// Copia cotações de Bolsa, BMF, Fundo (somente cotação), Andima, Series e Indices de D-1 para a data de hoje (real).
        /// </summary>
        public void CopiaCotacoes()
        {
            DateTime dataHoje = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataHoje, 1);

            #region Deleta todas as cotações na data de hoje!
            CotacaoBolsaCollection cotacaoBolsaCollection = new CotacaoBolsaCollection();
            cotacaoBolsaCollection.Query.Where(cotacaoBolsaCollection.Query.Data.Equal(dataHoje));
            cotacaoBolsaCollection.Query.Load();
            cotacaoBolsaCollection.MarkAllAsDeleted();
            cotacaoBolsaCollection.Save();

            CotacaoBMFCollection cotacaoBMFCollection = new CotacaoBMFCollection();
            cotacaoBMFCollection.Query.Where(cotacaoBMFCollection.Query.Data.Equal(dataHoje));
            cotacaoBMFCollection.Query.Load();
            cotacaoBMFCollection.MarkAllAsDeleted();
            cotacaoBMFCollection.Save();

            HistoricoCotaQuery historicoCotaQuery = new HistoricoCotaQuery("H");
            ClienteQuery clienteQuery = new ClienteQuery("L");
            historicoCotaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == historicoCotaQuery.IdCarteira);
            historicoCotaQuery.Where(historicoCotaQuery.Data.Equal(dataHoje),
                                     clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao));
            HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();
            historicoCotaCollection.Load(historicoCotaQuery);
            historicoCotaCollection.MarkAllAsDeleted();
            historicoCotaCollection.Save();

            CotacaoMercadoAndimaCollection cotacaoMercadoAndimaCollection = new CotacaoMercadoAndimaCollection();
            cotacaoMercadoAndimaCollection.Query.Where(cotacaoMercadoAndimaCollection.Query.DataReferencia.Equal(dataHoje));
            cotacaoMercadoAndimaCollection.Query.Load();
            cotacaoMercadoAndimaCollection.MarkAllAsDeleted();
            cotacaoMercadoAndimaCollection.Save();

            CotacaoSerieCollection cotacaoSerieCollection = new CotacaoSerieCollection();
            cotacaoSerieCollection.Query.Where(cotacaoSerieCollection.Query.Data.Equal(dataHoje));
            cotacaoSerieCollection.Query.Load();
            cotacaoSerieCollection.MarkAllAsDeleted();
            cotacaoSerieCollection.Save();

            CotacaoIndiceCollection cotacaoIndiceCollection = new CotacaoIndiceCollection();
            cotacaoIndiceCollection.Query.Where(cotacaoIndiceCollection.Query.Data.Equal(dataHoje));
            cotacaoIndiceCollection.Query.Load();
            cotacaoIndiceCollection.MarkAllAsDeleted();
            cotacaoIndiceCollection.Save();
            #endregion

            //COTACAO BOLSA
            cotacaoBolsaCollection = new CotacaoBolsaCollection();
            cotacaoBolsaCollection.Query.Where(cotacaoBolsaCollection.Query.Data.Equal(dataAnterior));
            cotacaoBolsaCollection.Query.Load();
            foreach (CotacaoBolsa cotacaoBolsa in cotacaoBolsaCollection)
            {
                CotacaoBolsa cotacaoBolsaInsert = new CotacaoBolsa();
                cotacaoBolsaInsert.CdAtivoBolsa = cotacaoBolsa.CdAtivoBolsa;
                cotacaoBolsaInsert.Data = dataHoje;
                cotacaoBolsaInsert.PUAbertura = cotacaoBolsa.PUAbertura;
                cotacaoBolsaInsert.PUFechamento = cotacaoBolsa.PUFechamento;
                cotacaoBolsaInsert.PUMedio = cotacaoBolsa.PUMedio;
                cotacaoBolsaInsert.Save();
            }

            //COTACAO BMF
            cotacaoBMFCollection = new CotacaoBMFCollection();
            cotacaoBMFCollection.Query.Where(cotacaoBMFCollection.Query.Data.Equal(dataAnterior));
            cotacaoBMFCollection.Query.Load();
            foreach (CotacaoBMF cotacaoBMF in cotacaoBMFCollection)
            {
                CotacaoBMF cotacaoBMFInsert = new CotacaoBMF();
                cotacaoBMFInsert.CdAtivoBMF = cotacaoBMF.CdAtivoBMF;
                cotacaoBMFInsert.Serie = cotacaoBMF.Serie;
                cotacaoBMFInsert.Data = dataHoje;
                cotacaoBMFInsert.PUCorrigido = cotacaoBMF.PUCorrigido;
                cotacaoBMFInsert.PUFechamento = cotacaoBMF.PUFechamento;
                cotacaoBMFInsert.PUMedio = cotacaoBMF.PUMedio;
                cotacaoBMFInsert.Save();
            }

            //COTACAO FUNDO
            historicoCotaQuery = new HistoricoCotaQuery("H");
            clienteQuery = new ClienteQuery("L");
            historicoCotaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == historicoCotaQuery.IdCarteira);
            historicoCotaQuery.Where(historicoCotaQuery.Data.Equal(dataAnterior),
                                     clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao));
            historicoCotaCollection = new HistoricoCotaCollection();
            historicoCotaCollection.Load(historicoCotaQuery);

            foreach (HistoricoCota historicoCota in historicoCotaCollection)
            {
                HistoricoCota historicoCotaInsert = new HistoricoCota();
                historicoCotaInsert.AjustePL = 0;
                historicoCotaInsert.CotaAbertura = historicoCota.CotaAbertura;
                historicoCotaInsert.CotaBruta = historicoCota.CotaBruta;
                historicoCotaInsert.CotaFechamento = historicoCota.CotaFechamento;
                historicoCotaInsert.Data = dataHoje;
                historicoCotaInsert.IdCarteira = historicoCota.IdCarteira;
                historicoCotaInsert.PatrimonioBruto = historicoCota.PatrimonioBruto;
                historicoCotaInsert.PLAbertura = historicoCota.PLAbertura;
                historicoCotaInsert.PLFechamento = historicoCota.PLFechamento;
                historicoCotaInsert.QuantidadeFechamento = historicoCota.QuantidadeFechamento;
                historicoCotaInsert.Save();
            }

            //COTACAO ANDIMA
            cotacaoMercadoAndimaCollection = new CotacaoMercadoAndimaCollection();
            cotacaoMercadoAndimaCollection.Query.Where(cotacaoMercadoAndimaCollection.Query.DataReferencia.Equal(dataAnterior));
            cotacaoMercadoAndimaCollection.Query.Load();
            foreach (CotacaoMercadoAndima cotacaoMercadoAndima in cotacaoMercadoAndimaCollection)
            {
                CotacaoMercadoAndima cotacaoMercadoAndimaInsert = new CotacaoMercadoAndima();
                cotacaoMercadoAndimaInsert.CodigoSELIC = cotacaoMercadoAndima.CodigoSELIC;
                cotacaoMercadoAndimaInsert.DataEmissao = cotacaoMercadoAndima.DataEmissao;
                cotacaoMercadoAndimaInsert.DataReferencia = dataHoje;
                cotacaoMercadoAndimaInsert.DataVencimento = cotacaoMercadoAndima.DataVencimento;
                cotacaoMercadoAndimaInsert.Descricao = cotacaoMercadoAndima.Descricao;
                cotacaoMercadoAndimaInsert.Pu = cotacaoMercadoAndima.Pu;
                cotacaoMercadoAndimaInsert.TaxaIndicativa = cotacaoMercadoAndima.TaxaIndicativa;
            }

            //COTACAO SERIE
            cotacaoSerieCollection = new CotacaoSerieCollection();
            cotacaoSerieCollection.Query.Where(cotacaoSerieCollection.Query.Data.Equal(dataAnterior));
            cotacaoSerieCollection.Query.Load();
            foreach (CotacaoSerie cotacaoSerie in cotacaoSerieCollection)
            {
                CotacaoSerie cotacaoSerieInsert = new CotacaoSerie();
                cotacaoSerieInsert.Data = dataHoje;
                cotacaoSerieInsert.Cotacao = cotacaoSerie.Cotacao;
                cotacaoSerieInsert.IdSerie = cotacaoSerie.IdSerie;
                cotacaoSerieInsert.Save();
            }

            //COTACAO INDICE
            cotacaoIndiceCollection = new CotacaoIndiceCollection();
            cotacaoIndiceCollection.Query.Where(cotacaoIndiceCollection.Query.Data.Equal(dataAnterior));
            cotacaoIndiceCollection.Query.Load();
            foreach (CotacaoIndice cotacaoIndice in cotacaoIndiceCollection)
            {
                CotacaoIndice cotacaoIndiceInsert = new CotacaoIndice();
                cotacaoIndiceInsert.Data = dataHoje;
                cotacaoIndiceInsert.IdIndice = cotacaoIndice.IdIndice;
                cotacaoIndiceInsert.Valor = cotacaoIndice.Valor;
                cotacaoIndiceInsert.Save();
            }
        }

        /// <summary>
        /// Abre o dia de todos os clientes q estejam fechados, com CalculaRealTime = S 
        /// e que estejam em D-1 da data atual.
        /// </summary>
        public void ExecutaAbertura(ParametrosProcessamento parametrosProcessamento)
        {
            DateTime dataHoje = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataHoje, 1);

            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Select(clienteCollection.Query.IdCliente,
                                           clienteCollection.Query.DataDia,
                                           clienteCollection.Query.IsProcessando);
            clienteCollection.Query.Where(clienteCollection.Query.StatusAtivo.Equal(StatusAtivoCliente.Ativo),
                                          clienteCollection.Query.Status.Equal((byte)StatusCliente.Divulgado),
                                          clienteCollection.Query.DataDia.Equal(dataAnterior),
                                          clienteCollection.Query.CalculaRealTime.Equal("S"));

            clienteCollection.Query.Load();

            foreach (Cliente cliente in clienteCollection)
            {
                int idCliente = cliente.IdCliente.Value;
                DateTime dataDia = cliente.DataDia.Value;

                //Inicia o processamento mudando o status IsProcessando
                Cliente clienteUpdate = new Cliente();
                clienteUpdate.LoadByPrimaryKey(idCliente);
                clienteUpdate.IsProcessando = "S";
                clienteUpdate.Save();
                //

                try
                {
                    ControllerInvestidor controllerInvestidor = new ControllerInvestidor();
                    controllerInvestidor.ExecutaAbertura(idCliente, dataDia, parametrosProcessamento);
                }
                catch (Exception ex)
                {
                    string sSourceErro;
                    string sLogErro;
                    string sEventErro;
                    sSourceErro = "Financial.Controller";
                    sLogErro = "Error";
                    sEventErro = "Abertura cliente " + idCliente.ToString() + " na data " + dataHoje +
                        " Erro: " + ex.Message;
                    if (!EventLog.SourceExists(sSourceErro))
                        EventLog.CreateEventSource(sSourceErro, sLogErro);

                    EventLog.WriteEntry(sSourceErro, sEventErro, EventLogEntryType.Error);
                }                
            }
        }
    }

    public class FinancialJob : IJob
    {
        #region IJob Members

        public void Initialize()
        {
            //Console.WriteLine("TestJob::Initialize");  

            string sSource;
            string sLog;
            string sEvent;
            sSource = "Financial.Controller";
            sLog = "Application";
            //sEvent = "Execução cliente " + idCliente.ToString() + " na data " + DateTime.Now.ToString();
            sEvent = "Inicialização do Financial.Controller (FinancialJob)";
            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);

            EventLog.WriteEntry(sSource, sEvent);
        }

        public void Run()
        {
            //Console.WriteLine(string.Format("TestJob::Run {0}", DateTime.Now.TimeOfDay));
            CalculoDiario calculoDiario = new CalculoDiario();
            calculoDiario.ExecutaFechamento(true);
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            //Console.WriteLine("TestJob::Dispose");
        }

        #endregion
    }

    public class FinancialQuote : IJob
    {
        private DdeClient client;
        Dictionary<string, decimal> listaCotacao;

        #region IJob Members

        public void Initialize()
        {
            //Console.WriteLine("TestJob::Initialize");                        
            string service = Settings.Default.QuoteService.Trim();
            string topic = Settings.Default.QuoteTopic.Trim();
            client = new DdeClient(service, topic);
            client.Advise += client_Advise;
            client.Disconnected += client_Disconnected;
            listaCotacao = new Dictionary<string, decimal>();
            CriaEntradaQuotes();

            string sSource;
            string sLog;
            string sEvent;
            sSource = "Financial.Controller";
            sLog = "Application";
            //sEvent = "Execução cliente " + idCliente.ToString() + " na data " + DateTime.Now.ToString();
            sEvent = "Inicialização do Financial.Controller (FinancialQuote)";
            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);

            EventLog.WriteEntry(sSource, sEvent);
        }

        public void Run()
        {
            //Console.WriteLine(string.Format("TestJob::Run {0}", DateTime.Now.TimeOfDay));
            CalculoDiario calculoDiario = new CalculoDiario();
            calculoDiario.AtualizaCotacoes(listaCotacao);
            calculoDiario.ExecutaFechamento(false);            

            listaCotacao.Clear();
        }

        /// <summary>
        /// Cria a lista de ativos de Bolsa e BMF a serem mapeados no DDE.
        /// </summary>
        private void CriaEntradaQuotes()
        {
            AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
            ativoBolsaCollection.LoadAll();

            foreach (AtivoBolsa ativoBolsa in ativoBolsaCollection)
            {
                client.StartAdvise(ativoBolsa.CdAtivoBolsa.Trim() + ".ult", 1, true, 60000);
            }

            AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();
            ativoBMFCollection.LoadAll();

            foreach (AtivoBMF ativoBMF in ativoBMFCollection)
            {
                client.StartAdvise(ativoBMF.CdAtivoBMF.Trim() + ativoBMF.Serie.Trim() + ".ult", 1, true, 60000);
            }
        }

        /// <summary>
        /// Evento para advise disparar a chamada de cotação.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void client_Advise(object sender, DdeAdviseEventArgs args)
        {
            string cdAtivo = args.Item.Trim();
            decimal cotacao = Convert.ToDecimal(args.Text);
            listaCotacao.Add(cdAtivo, cotacao);
        }

        /// <summary>
        /// Evento para advise qdo disconectar.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void client_Disconnected(object sender, DdeDisconnectedEventArgs args)
        {
            //Inócuo até o momento...

            //displayTextBox.Text =
            //    "OnDisconnected: " +
            //    "IsServerInitiated=" + args.IsServerInitiated.ToString() + " " +
            //    "IsDisposed=" + args.IsDisposed.ToString();
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            //Console.WriteLine("TestJob::Dispose");
        }

        #endregion
    }
}
