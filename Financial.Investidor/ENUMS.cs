﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.Util;
using Financial.Investidor.Properties;
using System.Reflection;

namespace Financial.Investidor.Enums
{
    /// <summary>
    /// /// Todos os valores do Enum devem estar cadastrados no Resource do Componente
    /// </summary>
    public enum StatusCliente
    {
        [StringValue("Aberto")]//Aberto
        Aberto = 1,

        [StringValue("Fechado")]//Fechado
        Fechado = 2,

        [StringValue("Divulgado")]//Divulgado
        Divulgado = 3,

        [StringValue("FechadoComErro")]//Fechado Com Erro
        FechadoComErro = 4,

        [StringValue("AbertoComErro")]//Aberto Com Erro
        AbertoComErro = 5
    }

    public enum SuitabilityCriterio
    {
        Igual = 1,
        MaiorIgual = 2,
        MenorIgual = 3
    }

    public enum SuitabilityStatusResposta
    {
        Recusado = 1,
        Efetuado = 2
    }

    public enum SuitabilityGrandezaTempo
    {
        Dia = 1,
        Mes = 2,
        Ano = 3
    }

    public enum StatusAtivoCliente
    {
        Ativo = 1,
        Inativo = 2
    }

    public enum SuitabilityVerificaEnquadramento
    {
        Exato = 1,
        RiscoMenor = 2
    }

    public enum SuitabilityGrandezaPeriodo
    {
        Dia = 1,
        Mes = 2,
        Ano = 3
    }

    public enum SuitabilityAlertas
    {
        PerfilExpiraAte30dias = 1,
        PerfilExpirado = 2,
        PerfilNaoInformado = 3,
        Desenquadramento = 4
    }

    public enum TipoControleCliente
    {
        /// <summary>
        /// Para fundos e clubes com cota somente informada.
        /// </summary>
        ApenasCotacao = 1,
                
        /// <summary>
        // Cliente para controle Dos Mercados(Bolsa, BMF...), mas não calcula Cota, Rentabilidade
        /// </summary>
        Carteira = 2,

        /// <summary>
        // Cliente para controle Dos Mercados(Bolsa, BMF...), e tba com cálculo de Cota, Rentabilidade. Mas não tem passivo (cotista).
        /// </summary>
        CarteiraRentabil = 3,
        
        /// <summary>
        /// Somente passivo (cotista). A cota deve ser informada na mão ou por importação.
        /// </summary>
        Cotista = 4,
        
        /// <summary>
        /// Ativo e passivo, tudo controlado.
        /// </summary>
        Completo = 5,

        /// <summary>
        /// Caso particular para controle somente da apuração de IR sobre ganhos em renda variável (bolsa e BMF).
        /// </summary>
        IRRendaVariavel = 6,

        /// <summary>
        /// Ativo importado de fontes externas (XML Anbid). Usa cota e PL importados.
        /// </summary>
        CarteiraImportada = 7,

        /// <summary>
        /// Usada para realizar simulações com ativos
        /// </summary>
        CarteiraSimulada = 8
    }

    public static class TipoClienteFixo {
        public const int ClientePessoaFisica = 1;
        public const int ClientePessoaJuridica = 10;
        public const int CarteiraPropria = 100;
        public const int Clube = 200;
        public const int CTVM = 300;
        public const int DTVM = 310;
        public const int Banco = 320;        
        public const int Fundo = 500;
        public const int FDIC = 510;
        public const int InvestidorEstrangeiro = 800;
        public const int OffShore_PF = 801;
        public const int OffShore_PJ = 802;

        /// <summary>
        /// Valores possiveis para o Enum
        /// </summary>
        /// <returns></returns>
        public static List<int> Values() {
            List<int> valoresPossiveis = new List<int>();
            valoresPossiveis.Add(ClientePessoaFisica);
            valoresPossiveis.Add(ClientePessoaJuridica);
            valoresPossiveis.Add(CarteiraPropria);
            valoresPossiveis.Add(Clube);
            valoresPossiveis.Add(CTVM);
            valoresPossiveis.Add(DTVM);
            valoresPossiveis.Add(Banco);
            valoresPossiveis.Add(Fundo);
            valoresPossiveis.Add(FDIC);
            valoresPossiveis.Add(InvestidorEstrangeiro);
            valoresPossiveis.Add(OffShore_PF);
            valoresPossiveis.Add(OffShore_PJ);
            return valoresPossiveis;
        }
    }

    public enum TipoContaCorrente {
        Deposito = 1,
        Investimento = 2
    }
  
    public enum TipoReprocessamento
    {
        /* Saldos de Fechamento do dia em questão */
        Fechamento = 1,
        /* Saldos de Fechamento de D-1, remunerados pela abertura */
        Abertura = 2,
        /* Saldos de Fechamento de D-1, remunerados pela abertura, não se preocupa com histórico */
        CalculoDiario = 3
    }

    public static class TraducaoEnumsInvestidor {

        #region Funções privates para fazer Pesquisa dentro dos Enums
        /// <summary>
        /// Dado um codigo retorna um Enum do tipo StatusCliente
        /// </summary>
        /// <exception>throws ArgumentException se não existir um enum com o valor do código</exception> 
        /// <returns></returns>
        private static StatusCliente SearchEnumStatusCliente(int codigo) {
            int[] statusClienteValues = (int[])Enum.GetValues(typeof(StatusCliente));

            int? posicao = null;
            for (int i = 0; i < statusClienteValues.Length; i++) {
                if (statusClienteValues[i] == codigo) {
                    posicao = i;
                    break;
                }
            }

            if (posicao.HasValue) {
                string statusClienteString = Enum.GetNames(typeof(StatusCliente))[posicao.Value];
                // Monta o Enum de acordo com a string
                StatusCliente statusCliente = (StatusCliente)Enum.Parse(typeof(StatusCliente), statusClienteString);
                return statusCliente;
            }
            else {
                throw new ArgumentException("StatusCliente não possue Constante com valor " + codigo);
            }
        }

        #endregion

        public static class EnumStatusCliente {
            /// <summary>
            /// Realiza a tradução do Enum StatusCliente
            /// </summary>
            /// <param name="codigo"></param>
            /// <returns></returns>
            public static string TraduzEnum(int codigo) {
                StatusCliente s = TraducaoEnumsInvestidor.SearchEnumStatusCliente(codigo);
                string chave = "#" + StringEnum.GetStringValue(s);
                return Resources.ResourceManager.GetString(chave);
            }
        }
    }

    public enum TipoBook
    {
        BookPorCarteira = 1,
        BookGeral = 2
    }

    public enum GrossupCliente
    {
        [StringValue("Não Faz Grossup")]
        NaoFaz = 1,

        [StringValue("Grossup sempre")]
        SempreFaz = 2,

        [StringValue("Apenas em resgate")]
        ApenasResgate = 3,

        [StringValue("Gross UP sem IOF")]
        GrossUpSemIOF = 4
    }

    public static class GrossupClienteDescricao
    {
        public static string RetornaDescricao(int idGrossupCliente)
        {
            return Enum.GetName(typeof(GrossupCliente), idGrossupCliente);
        }

        public static string RetornaStringValue(int idGrossupCliente)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(GrossupCliente), RetornaDescricao(idGrossupCliente)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum DescontoPLCliente
    {
        PLComDesconto = 1,
        BrutoSemImpacto = 2,
        BrutoComImpacto = 3
    }

    public enum StatusRealTimeCliente
    {
        NaoExecutar = 1,
        Executar = 2
    }

    #region enums para Processamento
    public enum TipoProcessamento
    {
        [StringValue("Fechamento")]
        Fechamento = 0,

        [StringValue("Divulgação")]
        Divulgacao = 1,

        [StringValue("Abertura")]
        Abertura = 2,

        [StringValue("Retroação")]
        Retroacao = 3,

        [StringValue("Avanço Período")]
        AvancoPeriodo = 4
    }

    public static class TipoProcessamentoDescricao
    {
        public static string RetornaDescricao(int idTipoProcessamento)
        {
            return Enum.GetName(typeof(TipoProcessamento), idTipoProcessamento);
        }

        public static string RetornaStringValue(int idTipoProcessamento)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoProcessamento), RetornaDescricao(idTipoProcessamento)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum StatusFinal
    {
        Aberto = 0,
        Fechado = 1
    }

    public enum IntegracaoBolsa
    {
        NaoIntegra = 0,
        Sinacor = 1,
        Sinacor_BTC = 2,        
    }

    public enum IntegracaoBMF
    {
        NaoIntegra = 0,
        Sinacor = 1
    }

    public enum IntegracaoRendaFixa
    {
        NaoIntegra = 0,
        Sinacor = 1,
        Virtual = 2
    }

    public enum IntegracaoCC
    {
        NaoIntegra = 0,
        Sinacor = 1,
        Sinacor_IRFonte = 2
    }

    public static class ListaInterfaceCliente
    {
        //de 1 a 100, reservado para cotista
        public static class Fundo
        {
            public const int NaoImporta = 1;
            public const int FinancialFull = 2; //Importa cotista (operacao e posicao) para fundo
        }

        //de 101 a 200, reservado para bolsa
        public static class Bolsa
        {
            public const int NaoImporta = 101;
            public const int SinacorPosicaoInicialBolsa = 102;
            public const int SinacorPosicaoInicialBTC = 103;
            public const int SinacorOperacaoAcoes = 110;
            public const int SinacorOperacaoFII = 111;
        }

        //de 201 a 300, reservado para BMF
        public static class BMF
        {
            public const int NaoImporta = 201;
            public const int SinacorPosicaoInicial = 202;
        }

        //de 301 a 400, reservado para renda fixa
        public static class RendaFixa
        {
            public const int NaoImporta = 301;
            public const int VirtualOperacao = 302; //Importa operacao
            public const int VirtualFull = 303; //Importa operacao e posicao
            public const int Sinacor = 310; //Importa operacao
        }

        //de 501 a 600, reservado para conta corrente
        public static class ContaCorrente
        {
            public const int NaoImporta = 501;
            public const int SinacorFull = 502; //Importa todos os lancamentos de c/c feitos no dia
        }
    }    
    #endregion

    public enum TipoPeriodoDatas
    {
        Mes = 1,
        Ano = 2,
        Meses3 = 3,
        Meses6 = 4,
        Meses12 = 5,
        Meses24 = 6,
        Ano_1 = 7,
        Ano_2 = 8,
        Ano_3 = 9,
        Inicio = 10
    }

    
}
