/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 2/11/2015 10:55:57 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Investidor
{

	[Serializable]
	abstract public class esBookCollection : esEntityCollection
	{
		public esBookCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "BookCollection";
		}

		#region Query Logic
		protected void InitQuery(esBookQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esBookQuery);
		}
		#endregion
		
		virtual public Book DetachEntity(Book entity)
		{
			return base.DetachEntity(entity) as Book;
		}
		
		virtual public Book AttachEntity(Book entity)
		{
			return base.AttachEntity(entity) as Book;
		}
		
		virtual public void Combine(BookCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Book this[int index]
		{
			get
			{
				return base[index] as Book;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Book);
		}
	}



	[Serializable]
	abstract public class esBook : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esBookQuery GetDynamicQuery()
		{
			return null;
		}

		public esBook()
		{

		}

		public esBook(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idBook)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBook);
			else
				return LoadByPrimaryKeyStoredProcedure(idBook);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idBook)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esBookQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdBook == idBook);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idBook)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBook);
			else
				return LoadByPrimaryKeyStoredProcedure(idBook);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idBook)
		{
			esBookQuery query = this.GetDynamicQuery();
			query.Where(query.IdBook == idBook);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idBook)
		{
			esParameters parms = new esParameters();
			parms.Add("IdBook",idBook);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdBook": this.str.IdBook = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;							
						case "Email": this.str.Email = (string)value; break;							
						case "Assunto": this.str.Assunto = (string)value; break;							
						case "Mensagem": this.str.Mensagem = (string)value; break;							
						case "EmailFrom": this.str.EmailFrom = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdBook":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdBook = (System.Int32?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Tipo = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Book.IdBook
		/// </summary>
		virtual public System.Int32? IdBook
		{
			get
			{
				return base.GetSystemInt32(BookMetadata.ColumnNames.IdBook);
			}
			
			set
			{
				base.SetSystemInt32(BookMetadata.ColumnNames.IdBook, value);
			}
		}
		
		/// <summary>
		/// Maps to Book.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(BookMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(BookMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to Book.Tipo
		/// </summary>
		virtual public System.Byte? Tipo
		{
			get
			{
				return base.GetSystemByte(BookMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemByte(BookMetadata.ColumnNames.Tipo, value);
			}
		}
		
		/// <summary>
		/// Maps to Book.Email
		/// </summary>
		virtual public System.String Email
		{
			get
			{
				return base.GetSystemString(BookMetadata.ColumnNames.Email);
			}
			
			set
			{
				base.SetSystemString(BookMetadata.ColumnNames.Email, value);
			}
		}
		
		/// <summary>
		/// Maps to Book.Assunto
		/// </summary>
		virtual public System.String Assunto
		{
			get
			{
				return base.GetSystemString(BookMetadata.ColumnNames.Assunto);
			}
			
			set
			{
				base.SetSystemString(BookMetadata.ColumnNames.Assunto, value);
			}
		}
		
		/// <summary>
		/// Maps to Book.Mensagem
		/// </summary>
		virtual public System.String Mensagem
		{
			get
			{
				return base.GetSystemString(BookMetadata.ColumnNames.Mensagem);
			}
			
			set
			{
				base.SetSystemString(BookMetadata.ColumnNames.Mensagem, value);
			}
		}
		
		/// <summary>
		/// Maps to Book.EmailFrom
		/// </summary>
		virtual public System.String EmailFrom
		{
			get
			{
				return base.GetSystemString(BookMetadata.ColumnNames.EmailFrom);
			}
			
			set
			{
				base.SetSystemString(BookMetadata.ColumnNames.EmailFrom, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esBook entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdBook
			{
				get
				{
					System.Int32? data = entity.IdBook;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdBook = null;
					else entity.IdBook = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Byte? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToByte(value);
				}
			}
				
			public System.String Email
			{
				get
				{
					System.String data = entity.Email;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Email = null;
					else entity.Email = Convert.ToString(value);
				}
			}
				
			public System.String Assunto
			{
				get
				{
					System.String data = entity.Assunto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Assunto = null;
					else entity.Assunto = Convert.ToString(value);
				}
			}
				
			public System.String Mensagem
			{
				get
				{
					System.String data = entity.Mensagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Mensagem = null;
					else entity.Mensagem = Convert.ToString(value);
				}
			}
				
			public System.String EmailFrom
			{
				get
				{
					System.String data = entity.EmailFrom;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmailFrom = null;
					else entity.EmailFrom = Convert.ToString(value);
				}
			}
			

			private esBook entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esBookQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esBook can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Book : esBook
	{

		#region UpToClienteCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - Book_BookCliente_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteCollection UpToClienteCollection
		{
			get
			{
				if(this._UpToClienteCollection == null)
				{
					this._UpToClienteCollection = new ClienteCollection();
					this._UpToClienteCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToClienteCollection", this._UpToClienteCollection);
					this._UpToClienteCollection.ManyToManyBookCollection(this.IdBook);
				}

				return this._UpToClienteCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToClienteCollection != null) 
				{ 
					this.RemovePostSave("UpToClienteCollection"); 
					this._UpToClienteCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - Book_BookCliente_FK1
		/// </summary>
		public void AssociateClienteCollection(Cliente entity)
		{
			if (this._BookClienteCollection == null)
			{
				this._BookClienteCollection = new BookClienteCollection();
				this._BookClienteCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("BookClienteCollection", this._BookClienteCollection);
			}

			BookCliente obj = this._BookClienteCollection.AddNew();
			obj.IdBook = this.IdBook;
			obj.IdCliente = entity.IdCliente;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - Book_BookCliente_FK1
		/// </summary>
		public void DissociateClienteCollection(Cliente entity)
		{
			if (this._BookClienteCollection == null)
			{
				this._BookClienteCollection = new BookClienteCollection();
				this._BookClienteCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("BookClienteCollection", this._BookClienteCollection);
			}

			BookCliente obj = this._BookClienteCollection.AddNew();
			obj.IdBook = this.IdBook;
			obj.IdCliente = entity.IdCliente;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private ClienteCollection _UpToClienteCollection;
		private BookClienteCollection _BookClienteCollection;
		#endregion

				
		#region BookClienteCollectionByIdBook - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Book_BookCliente_FK1
		/// </summary>

		[XmlIgnore]
		public BookClienteCollection BookClienteCollectionByIdBook
		{
			get
			{
				if(this._BookClienteCollectionByIdBook == null)
				{
					this._BookClienteCollectionByIdBook = new BookClienteCollection();
					this._BookClienteCollectionByIdBook.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("BookClienteCollectionByIdBook", this._BookClienteCollectionByIdBook);
				
					if(this.IdBook != null)
					{
						this._BookClienteCollectionByIdBook.Query.Where(this._BookClienteCollectionByIdBook.Query.IdBook == this.IdBook);
						this._BookClienteCollectionByIdBook.Query.Load();

						// Auto-hookup Foreign Keys
						this._BookClienteCollectionByIdBook.fks.Add(BookClienteMetadata.ColumnNames.IdBook, this.IdBook);
					}
				}

				return this._BookClienteCollectionByIdBook;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._BookClienteCollectionByIdBook != null) 
				{ 
					this.RemovePostSave("BookClienteCollectionByIdBook"); 
					this._BookClienteCollectionByIdBook = null;
					
				} 
			} 			
		}

		private BookClienteCollection _BookClienteCollectionByIdBook;
		#endregion

				
		#region BookRelatorioCollectionByIdBook - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Book_BookRelatorio_FK1
		/// </summary>

		[XmlIgnore]
		public BookRelatorioCollection BookRelatorioCollectionByIdBook
		{
			get
			{
				if(this._BookRelatorioCollectionByIdBook == null)
				{
					this._BookRelatorioCollectionByIdBook = new BookRelatorioCollection();
					this._BookRelatorioCollectionByIdBook.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("BookRelatorioCollectionByIdBook", this._BookRelatorioCollectionByIdBook);
				
					if(this.IdBook != null)
					{
						this._BookRelatorioCollectionByIdBook.Query.Where(this._BookRelatorioCollectionByIdBook.Query.IdBook == this.IdBook);
						this._BookRelatorioCollectionByIdBook.Query.Load();

						// Auto-hookup Foreign Keys
						this._BookRelatorioCollectionByIdBook.fks.Add(BookRelatorioMetadata.ColumnNames.IdBook, this.IdBook);
					}
				}

				return this._BookRelatorioCollectionByIdBook;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._BookRelatorioCollectionByIdBook != null) 
				{ 
					this.RemovePostSave("BookRelatorioCollectionByIdBook"); 
					this._BookRelatorioCollectionByIdBook = null;
					
				} 
			} 			
		}

		private BookRelatorioCollection _BookRelatorioCollectionByIdBook;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "BookClienteCollectionByIdBook", typeof(BookClienteCollection), new BookCliente()));
			props.Add(new esPropertyDescriptor(this, "BookRelatorioCollectionByIdBook", typeof(BookRelatorioCollection), new BookRelatorio()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._BookClienteCollection != null)
			{
				foreach(BookCliente obj in this._BookClienteCollection)
				{
					if(obj.es.IsAdded)
					{
						obj.IdBook = this.IdBook;
					}
				}
			}
			if(this._BookClienteCollectionByIdBook != null)
			{
				foreach(BookCliente obj in this._BookClienteCollectionByIdBook)
				{
					if(obj.es.IsAdded)
					{
						obj.IdBook = this.IdBook;
					}
				}
			}
			if(this._BookRelatorioCollectionByIdBook != null)
			{
				foreach(BookRelatorio obj in this._BookRelatorioCollectionByIdBook)
				{
					if(obj.es.IsAdded)
					{
						obj.IdBook = this.IdBook;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}
	
	public partial class BookCollection : esBookCollection
	{
		#region ManyToManyClienteCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyClienteCollection(System.Int32? IdCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente", IdCliente);
	
			return base.Load( esQueryType.ManyToMany, 
				"Book,BookCliente|IdBook,IdBook|IdCliente",	parms);
		}
		#endregion
	}




	[Serializable]
	abstract public class esBookQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return BookMetadata.Meta();
			}
		}	
		

		public esQueryItem IdBook
		{
			get
			{
				return new esQueryItem(this, BookMetadata.ColumnNames.IdBook, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, BookMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, BookMetadata.ColumnNames.Tipo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Email
		{
			get
			{
				return new esQueryItem(this, BookMetadata.ColumnNames.Email, esSystemType.String);
			}
		} 
		
		public esQueryItem Assunto
		{
			get
			{
				return new esQueryItem(this, BookMetadata.ColumnNames.Assunto, esSystemType.String);
			}
		} 
		
		public esQueryItem Mensagem
		{
			get
			{
				return new esQueryItem(this, BookMetadata.ColumnNames.Mensagem, esSystemType.String);
			}
		} 
		
		public esQueryItem EmailFrom
		{
			get
			{
				return new esQueryItem(this, BookMetadata.ColumnNames.EmailFrom, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("BookCollection")]
	public partial class BookCollection : esBookCollection, IEnumerable<Book>
	{
		public BookCollection()
		{

		}
		
		public static implicit operator List<Book>(BookCollection coll)
		{
			List<Book> list = new List<Book>();
			
			foreach (Book emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  BookMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BookQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Book(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Book();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public BookQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BookQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(BookQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Book AddNew()
		{
			Book entity = base.AddNewEntity() as Book;
			
			return entity;
		}

		public Book FindByPrimaryKey(System.Int32 idBook)
		{
			return base.FindByPrimaryKey(idBook) as Book;
		}


		#region IEnumerable<Book> Members

		IEnumerator<Book> IEnumerable<Book>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Book;
			}
		}

		#endregion
		
		private BookQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Book' table
	/// </summary>

	[Serializable]
	public partial class Book : esBook
	{
		public Book()
		{

		}
	
		public Book(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return BookMetadata.Meta();
			}
		}
		
		
		
		override protected esBookQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BookQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public BookQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BookQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(BookQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private BookQuery query;
	}



	[Serializable]
	public partial class BookQuery : esBookQuery
	{
		public BookQuery()
		{

		}		
		
		public BookQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class BookMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected BookMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(BookMetadata.ColumnNames.IdBook, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BookMetadata.PropertyNames.IdBook;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BookMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = BookMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BookMetadata.ColumnNames.Tipo, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = BookMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BookMetadata.ColumnNames.Email, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = BookMetadata.PropertyNames.Email;
			c.CharacterMaxLength = 1000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BookMetadata.ColumnNames.Assunto, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = BookMetadata.PropertyNames.Assunto;
			c.CharacterMaxLength = 1000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BookMetadata.ColumnNames.Mensagem, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = BookMetadata.PropertyNames.Mensagem;
			c.CharacterMaxLength = 2000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BookMetadata.ColumnNames.EmailFrom, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = BookMetadata.PropertyNames.EmailFrom;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public BookMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdBook = "IdBook";
			 public const string Descricao = "Descricao";
			 public const string Tipo = "Tipo";
			 public const string Email = "Email";
			 public const string Assunto = "Assunto";
			 public const string Mensagem = "Mensagem";
			 public const string EmailFrom = "EmailFrom";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdBook = "IdBook";
			 public const string Descricao = "Descricao";
			 public const string Tipo = "Tipo";
			 public const string Email = "Email";
			 public const string Assunto = "Assunto";
			 public const string Mensagem = "Mensagem";
			 public const string EmailFrom = "EmailFrom";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(BookMetadata))
			{
				if(BookMetadata.mapDelegates == null)
				{
					BookMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (BookMetadata.meta == null)
				{
					BookMetadata.meta = new BookMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdBook", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Email", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Assunto", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Mensagem", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("EmailFrom", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "Book";
				meta.Destination = "Book";
				
				meta.spInsert = "proc_BookInsert";				
				meta.spUpdate = "proc_BookUpdate";		
				meta.spDelete = "proc_BookDelete";
				meta.spLoadAll = "proc_BookLoadAll";
				meta.spLoadByPrimaryKey = "proc_BookLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private BookMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
