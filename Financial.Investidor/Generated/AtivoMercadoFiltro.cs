/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/12/2014 14:37:01
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Investidor
{

	[Serializable]
	abstract public class esAtivoMercadoFiltroCollection : esEntityCollection
	{
		public esAtivoMercadoFiltroCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AtivoMercadoFiltroCollection";
		}

		#region Query Logic
		protected void InitQuery(esAtivoMercadoFiltroQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAtivoMercadoFiltroQuery);
		}
		#endregion
		
		virtual public AtivoMercadoFiltro DetachEntity(AtivoMercadoFiltro entity)
		{
			return base.DetachEntity(entity) as AtivoMercadoFiltro;
		}
		
		virtual public AtivoMercadoFiltro AttachEntity(AtivoMercadoFiltro entity)
		{
			return base.AttachEntity(entity) as AtivoMercadoFiltro;
		}
		
		virtual public void Combine(AtivoMercadoFiltroCollection collection)
		{
			base.Combine(collection);
		}
		
		new public AtivoMercadoFiltro this[int index]
		{
			get
			{
				return base[index] as AtivoMercadoFiltro;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(AtivoMercadoFiltro);
		}
	}



	[Serializable]
	abstract public class esAtivoMercadoFiltro : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAtivoMercadoFiltroQuery GetDynamicQuery()
		{
			return null;
		}

		public esAtivoMercadoFiltro()
		{

		}

		public esAtivoMercadoFiltro(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idAtivoMercadoFiltro)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAtivoMercadoFiltro);
			else
				return LoadByPrimaryKeyStoredProcedure(idAtivoMercadoFiltro);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idAtivoMercadoFiltro)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAtivoMercadoFiltro);
			else
				return LoadByPrimaryKeyStoredProcedure(idAtivoMercadoFiltro);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idAtivoMercadoFiltro)
		{
			esAtivoMercadoFiltroQuery query = this.GetDynamicQuery();
			query.Where(query.IdAtivoMercadoFiltro == idAtivoMercadoFiltro);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idAtivoMercadoFiltro)
		{
			esParameters parms = new esParameters();
			parms.Add("IdAtivoMercadoFiltro",idAtivoMercadoFiltro);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdAtivoMercadoFiltro": this.str.IdAtivoMercadoFiltro = (string)value; break;							
						case "CompositeKey": this.str.CompositeKey = (string)value; break;							
						case "IdAtivo": this.str.IdAtivo = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdAtivoMercadoFiltro":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAtivoMercadoFiltro = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoMercado = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to AtivoMercadoFiltro.IdAtivoMercadoFiltro
		/// </summary>
		virtual public System.Int32? IdAtivoMercadoFiltro
		{
			get
			{
				return base.GetSystemInt32(AtivoMercadoFiltroMetadata.ColumnNames.IdAtivoMercadoFiltro);
			}
			
			set
			{
				base.SetSystemInt32(AtivoMercadoFiltroMetadata.ColumnNames.IdAtivoMercadoFiltro, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoMercadoFiltro.CompositeKey
		/// </summary>
		virtual public System.String CompositeKey
		{
			get
			{
				return base.GetSystemString(AtivoMercadoFiltroMetadata.ColumnNames.CompositeKey);
			}
			
			set
			{
				base.SetSystemString(AtivoMercadoFiltroMetadata.ColumnNames.CompositeKey, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoMercadoFiltro.IdAtivo
		/// </summary>
		virtual public System.String IdAtivo
		{
			get
			{
				return base.GetSystemString(AtivoMercadoFiltroMetadata.ColumnNames.IdAtivo);
			}
			
			set
			{
				base.SetSystemString(AtivoMercadoFiltroMetadata.ColumnNames.IdAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoMercadoFiltro.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(AtivoMercadoFiltroMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(AtivoMercadoFiltroMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoMercadoFiltro.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(AtivoMercadoFiltroMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(AtivoMercadoFiltroMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoMercadoFiltro.TipoMercado
		/// </summary>
		virtual public System.Int32? TipoMercado
		{
			get
			{
				return base.GetSystemInt32(AtivoMercadoFiltroMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemInt32(AtivoMercadoFiltroMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAtivoMercadoFiltro entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdAtivoMercadoFiltro
			{
				get
				{
					System.Int32? data = entity.IdAtivoMercadoFiltro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAtivoMercadoFiltro = null;
					else entity.IdAtivoMercadoFiltro = Convert.ToInt32(value);
				}
			}
				
			public System.String CompositeKey
			{
				get
				{
					System.String data = entity.CompositeKey;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CompositeKey = null;
					else entity.CompositeKey = Convert.ToString(value);
				}
			}
				
			public System.String IdAtivo
			{
				get
				{
					System.String data = entity.IdAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAtivo = null;
					else entity.IdAtivo = Convert.ToString(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Int32? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToInt32(value);
				}
			}
			

			private esAtivoMercadoFiltro entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAtivoMercadoFiltroQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAtivoMercadoFiltro can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class AtivoMercadoFiltro : esAtivoMercadoFiltro
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAtivoMercadoFiltroQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AtivoMercadoFiltroMetadata.Meta();
			}
		}	
		

		public esQueryItem IdAtivoMercadoFiltro
		{
			get
			{
				return new esQueryItem(this, AtivoMercadoFiltroMetadata.ColumnNames.IdAtivoMercadoFiltro, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CompositeKey
		{
			get
			{
				return new esQueryItem(this, AtivoMercadoFiltroMetadata.ColumnNames.CompositeKey, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAtivo
		{
			get
			{
				return new esQueryItem(this, AtivoMercadoFiltroMetadata.ColumnNames.IdAtivo, esSystemType.String);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, AtivoMercadoFiltroMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, AtivoMercadoFiltroMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, AtivoMercadoFiltroMetadata.ColumnNames.TipoMercado, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("AtivoMercadoFiltroCollection")]
	public partial class AtivoMercadoFiltroCollection : esAtivoMercadoFiltroCollection, IEnumerable<AtivoMercadoFiltro>
	{
		public AtivoMercadoFiltroCollection()
		{

		}
		
		public static implicit operator List<AtivoMercadoFiltro>(AtivoMercadoFiltroCollection coll)
		{
			List<AtivoMercadoFiltro> list = new List<AtivoMercadoFiltro>();
			
			foreach (AtivoMercadoFiltro emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AtivoMercadoFiltroMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AtivoMercadoFiltroQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new AtivoMercadoFiltro(row);
		}

		override protected esEntity CreateEntity()
		{
			return new AtivoMercadoFiltro();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AtivoMercadoFiltroQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AtivoMercadoFiltroQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AtivoMercadoFiltroQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public AtivoMercadoFiltro AddNew()
		{
			AtivoMercadoFiltro entity = base.AddNewEntity() as AtivoMercadoFiltro;
			
			return entity;
		}

		public AtivoMercadoFiltro FindByPrimaryKey(System.Int32 idAtivoMercadoFiltro)
		{
			return base.FindByPrimaryKey(idAtivoMercadoFiltro) as AtivoMercadoFiltro;
		}


		#region IEnumerable<AtivoMercadoFiltro> Members

		IEnumerator<AtivoMercadoFiltro> IEnumerable<AtivoMercadoFiltro>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as AtivoMercadoFiltro;
			}
		}

		#endregion
		
		private AtivoMercadoFiltroQuery query;
	}


	/// <summary>
	/// Encapsulates the 'AtivoMercadoFiltro' table
	/// </summary>

	[Serializable]
	public partial class AtivoMercadoFiltro : esAtivoMercadoFiltro
	{
		public AtivoMercadoFiltro()
		{

		}
	
		public AtivoMercadoFiltro(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AtivoMercadoFiltroMetadata.Meta();
			}
		}
		
		
		
		override protected esAtivoMercadoFiltroQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AtivoMercadoFiltroQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AtivoMercadoFiltroQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AtivoMercadoFiltroQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AtivoMercadoFiltroQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AtivoMercadoFiltroQuery query;
	}



	[Serializable]
	public partial class AtivoMercadoFiltroQuery : esAtivoMercadoFiltroQuery
	{
		public AtivoMercadoFiltroQuery()
		{

		}		
		
		public AtivoMercadoFiltroQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AtivoMercadoFiltroMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AtivoMercadoFiltroMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AtivoMercadoFiltroMetadata.ColumnNames.IdAtivoMercadoFiltro, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoMercadoFiltroMetadata.PropertyNames.IdAtivoMercadoFiltro;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoMercadoFiltroMetadata.ColumnNames.CompositeKey, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoMercadoFiltroMetadata.PropertyNames.CompositeKey;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoMercadoFiltroMetadata.ColumnNames.IdAtivo, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoMercadoFiltroMetadata.PropertyNames.IdAtivo;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoMercadoFiltroMetadata.ColumnNames.Descricao, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoMercadoFiltroMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoMercadoFiltroMetadata.ColumnNames.IdCliente, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoMercadoFiltroMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoMercadoFiltroMetadata.ColumnNames.TipoMercado, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoMercadoFiltroMetadata.PropertyNames.TipoMercado;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public AtivoMercadoFiltroMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdAtivoMercadoFiltro = "IdAtivoMercadoFiltro";
			 public const string CompositeKey = "CompositeKey";
			 public const string IdAtivo = "IdAtivo";
			 public const string Descricao = "Descricao";
			 public const string IdCliente = "IdCliente";
			 public const string TipoMercado = "TipoMercado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdAtivoMercadoFiltro = "IdAtivoMercadoFiltro";
			 public const string CompositeKey = "CompositeKey";
			 public const string IdAtivo = "IdAtivo";
			 public const string Descricao = "Descricao";
			 public const string IdCliente = "IdCliente";
			 public const string TipoMercado = "TipoMercado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AtivoMercadoFiltroMetadata))
			{
				if(AtivoMercadoFiltroMetadata.mapDelegates == null)
				{
					AtivoMercadoFiltroMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AtivoMercadoFiltroMetadata.meta == null)
				{
					AtivoMercadoFiltroMetadata.meta = new AtivoMercadoFiltroMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdAtivoMercadoFiltro", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CompositeKey", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAtivo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "AtivoMercadoFiltro";
				meta.Destination = "AtivoMercadoFiltro";
				
				meta.spInsert = "proc_AtivoMercadoFiltroInsert";				
				meta.spUpdate = "proc_AtivoMercadoFiltroUpdate";		
				meta.spDelete = "proc_AtivoMercadoFiltroDelete";
				meta.spLoadAll = "proc_AtivoMercadoFiltroLoadAll";
				meta.spLoadByPrimaryKey = "proc_AtivoMercadoFiltroLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AtivoMercadoFiltroMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
