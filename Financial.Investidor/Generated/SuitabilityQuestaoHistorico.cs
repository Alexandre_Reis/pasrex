/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 22/09/2015 17:35:55
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityQuestaoHistoricoCollection : esEntityCollection
	{
		public esSuitabilityQuestaoHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityQuestaoHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityQuestaoHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityQuestaoHistoricoQuery);
		}
		#endregion
		
		virtual public SuitabilityQuestaoHistorico DetachEntity(SuitabilityQuestaoHistorico entity)
		{
			return base.DetachEntity(entity) as SuitabilityQuestaoHistorico;
		}
		
		virtual public SuitabilityQuestaoHistorico AttachEntity(SuitabilityQuestaoHistorico entity)
		{
			return base.AttachEntity(entity) as SuitabilityQuestaoHistorico;
		}
		
		virtual public void Combine(SuitabilityQuestaoHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityQuestaoHistorico this[int index]
		{
			get
			{
				return base[index] as SuitabilityQuestaoHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityQuestaoHistorico);
		}
	}



	[Serializable]
	abstract public class esSuitabilityQuestaoHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityQuestaoHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityQuestaoHistorico()
		{

		}

		public esSuitabilityQuestaoHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idQuestao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idQuestao);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idQuestao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idQuestao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idQuestao);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idQuestao);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idQuestao)
		{
			esSuitabilityQuestaoHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdQuestao == idQuestao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idQuestao)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdQuestao",idQuestao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdQuestao": this.str.IdQuestao = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Fator": this.str.Fator = (string)value; break;							
						case "IdValidacao": this.str.IdValidacao = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;							
						case "Validacao": this.str.Validacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdQuestao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdQuestao = (System.Int32?)value;
							break;
						
						case "Fator":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Fator = (System.Int32?)value;
							break;
						
						case "IdValidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdValidacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityQuestaoHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(SuitabilityQuestaoHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(SuitabilityQuestaoHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityQuestaoHistorico.IdQuestao
		/// </summary>
		virtual public System.Int32? IdQuestao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityQuestaoHistoricoMetadata.ColumnNames.IdQuestao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityQuestaoHistoricoMetadata.ColumnNames.IdQuestao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityQuestaoHistorico.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(SuitabilityQuestaoHistoricoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityQuestaoHistoricoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityQuestaoHistorico.Fator
		/// </summary>
		virtual public System.Int32? Fator
		{
			get
			{
				return base.GetSystemInt32(SuitabilityQuestaoHistoricoMetadata.ColumnNames.Fator);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityQuestaoHistoricoMetadata.ColumnNames.Fator, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityQuestaoHistorico.IdValidacao
		/// </summary>
		virtual public System.Int32? IdValidacao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityQuestaoHistoricoMetadata.ColumnNames.IdValidacao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityQuestaoHistoricoMetadata.ColumnNames.IdValidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityQuestaoHistorico.Tipo
		/// </summary>
		virtual public System.String Tipo
		{
			get
			{
				return base.GetSystemString(SuitabilityQuestaoHistoricoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemString(SuitabilityQuestaoHistoricoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityQuestaoHistorico.Validacao
		/// </summary>
		virtual public System.String Validacao
		{
			get
			{
				return base.GetSystemString(SuitabilityQuestaoHistoricoMetadata.ColumnNames.Validacao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityQuestaoHistoricoMetadata.ColumnNames.Validacao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityQuestaoHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdQuestao
			{
				get
				{
					System.Int32? data = entity.IdQuestao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdQuestao = null;
					else entity.IdQuestao = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Fator
			{
				get
				{
					System.Int32? data = entity.Fator;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fator = null;
					else entity.Fator = Convert.ToInt32(value);
				}
			}
				
			public System.String IdValidacao
			{
				get
				{
					System.Int32? data = entity.IdValidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdValidacao = null;
					else entity.IdValidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.String data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToString(value);
				}
			}
				
			public System.String Validacao
			{
				get
				{
					System.String data = entity.Validacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Validacao = null;
					else entity.Validacao = Convert.ToString(value);
				}
			}
			

			private esSuitabilityQuestaoHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityQuestaoHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityQuestaoHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityQuestaoHistorico : esSuitabilityQuestaoHistorico
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityQuestaoHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityQuestaoHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, SuitabilityQuestaoHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdQuestao
		{
			get
			{
				return new esQueryItem(this, SuitabilityQuestaoHistoricoMetadata.ColumnNames.IdQuestao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, SuitabilityQuestaoHistoricoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Fator
		{
			get
			{
				return new esQueryItem(this, SuitabilityQuestaoHistoricoMetadata.ColumnNames.Fator, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdValidacao
		{
			get
			{
				return new esQueryItem(this, SuitabilityQuestaoHistoricoMetadata.ColumnNames.IdValidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, SuitabilityQuestaoHistoricoMetadata.ColumnNames.Tipo, esSystemType.String);
			}
		} 
		
		public esQueryItem Validacao
		{
			get
			{
				return new esQueryItem(this, SuitabilityQuestaoHistoricoMetadata.ColumnNames.Validacao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityQuestaoHistoricoCollection")]
	public partial class SuitabilityQuestaoHistoricoCollection : esSuitabilityQuestaoHistoricoCollection, IEnumerable<SuitabilityQuestaoHistorico>
	{
		public SuitabilityQuestaoHistoricoCollection()
		{

		}
		
		public static implicit operator List<SuitabilityQuestaoHistorico>(SuitabilityQuestaoHistoricoCollection coll)
		{
			List<SuitabilityQuestaoHistorico> list = new List<SuitabilityQuestaoHistorico>();
			
			foreach (SuitabilityQuestaoHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityQuestaoHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityQuestaoHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityQuestaoHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityQuestaoHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityQuestaoHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityQuestaoHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityQuestaoHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityQuestaoHistorico AddNew()
		{
			SuitabilityQuestaoHistorico entity = base.AddNewEntity() as SuitabilityQuestaoHistorico;
			
			return entity;
		}

		public SuitabilityQuestaoHistorico FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idQuestao)
		{
			return base.FindByPrimaryKey(dataHistorico, idQuestao) as SuitabilityQuestaoHistorico;
		}


		#region IEnumerable<SuitabilityQuestaoHistorico> Members

		IEnumerator<SuitabilityQuestaoHistorico> IEnumerable<SuitabilityQuestaoHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityQuestaoHistorico;
			}
		}

		#endregion
		
		private SuitabilityQuestaoHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityQuestaoHistorico' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityQuestaoHistorico : esSuitabilityQuestaoHistorico
	{
		public SuitabilityQuestaoHistorico()
		{

		}
	
		public SuitabilityQuestaoHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityQuestaoHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityQuestaoHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityQuestaoHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityQuestaoHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityQuestaoHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityQuestaoHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityQuestaoHistoricoQuery query;
	}



	[Serializable]
	public partial class SuitabilityQuestaoHistoricoQuery : esSuitabilityQuestaoHistoricoQuery
	{
		public SuitabilityQuestaoHistoricoQuery()
		{

		}		
		
		public SuitabilityQuestaoHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityQuestaoHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityQuestaoHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityQuestaoHistoricoMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SuitabilityQuestaoHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityQuestaoHistoricoMetadata.ColumnNames.IdQuestao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityQuestaoHistoricoMetadata.PropertyNames.IdQuestao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityQuestaoHistoricoMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityQuestaoHistoricoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 2000;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityQuestaoHistoricoMetadata.ColumnNames.Fator, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityQuestaoHistoricoMetadata.PropertyNames.Fator;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityQuestaoHistoricoMetadata.ColumnNames.IdValidacao, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityQuestaoHistoricoMetadata.PropertyNames.IdValidacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityQuestaoHistoricoMetadata.ColumnNames.Tipo, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityQuestaoHistoricoMetadata.PropertyNames.Tipo;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityQuestaoHistoricoMetadata.ColumnNames.Validacao, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityQuestaoHistoricoMetadata.PropertyNames.Validacao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityQuestaoHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdQuestao = "IdQuestao";
			 public const string Descricao = "Descricao";
			 public const string Fator = "Fator";
			 public const string IdValidacao = "IdValidacao";
			 public const string Tipo = "Tipo";
			 public const string Validacao = "Validacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdQuestao = "IdQuestao";
			 public const string Descricao = "Descricao";
			 public const string Fator = "Fator";
			 public const string IdValidacao = "IdValidacao";
			 public const string Tipo = "Tipo";
			 public const string Validacao = "Validacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityQuestaoHistoricoMetadata))
			{
				if(SuitabilityQuestaoHistoricoMetadata.mapDelegates == null)
				{
					SuitabilityQuestaoHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityQuestaoHistoricoMetadata.meta == null)
				{
					SuitabilityQuestaoHistoricoMetadata.meta = new SuitabilityQuestaoHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdQuestao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Fator", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdValidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Tipo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Validacao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "SuitabilityQuestaoHistorico";
				meta.Destination = "SuitabilityQuestaoHistorico";
				
				meta.spInsert = "proc_SuitabilityQuestaoHistoricoInsert";				
				meta.spUpdate = "proc_SuitabilityQuestaoHistoricoUpdate";		
				meta.spDelete = "proc_SuitabilityQuestaoHistoricoDelete";
				meta.spLoadAll = "proc_SuitabilityQuestaoHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityQuestaoHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityQuestaoHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
