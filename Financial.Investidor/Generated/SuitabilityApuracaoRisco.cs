/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/09/2015 14:44:52
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityApuracaoRiscoCollection : esEntityCollection
	{
		public esSuitabilityApuracaoRiscoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityApuracaoRiscoCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityApuracaoRiscoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityApuracaoRiscoQuery);
		}
		#endregion
		
		virtual public SuitabilityApuracaoRisco DetachEntity(SuitabilityApuracaoRisco entity)
		{
			return base.DetachEntity(entity) as SuitabilityApuracaoRisco;
		}
		
		virtual public SuitabilityApuracaoRisco AttachEntity(SuitabilityApuracaoRisco entity)
		{
			return base.AttachEntity(entity) as SuitabilityApuracaoRisco;
		}
		
		virtual public void Combine(SuitabilityApuracaoRiscoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityApuracaoRisco this[int index]
		{
			get
			{
				return base[index] as SuitabilityApuracaoRisco;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityApuracaoRisco);
		}
	}



	[Serializable]
	abstract public class esSuitabilityApuracaoRisco : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityApuracaoRiscoQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityApuracaoRisco()
		{

		}

		public esSuitabilityApuracaoRisco(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idApuracaoRisco)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idApuracaoRisco);
			else
				return LoadByPrimaryKeyStoredProcedure(idApuracaoRisco);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idApuracaoRisco)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idApuracaoRisco);
			else
				return LoadByPrimaryKeyStoredProcedure(idApuracaoRisco);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idApuracaoRisco)
		{
			esSuitabilityApuracaoRiscoQuery query = this.GetDynamicQuery();
			query.Where(query.IdApuracaoRisco == idApuracaoRisco);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idApuracaoRisco)
		{
			esParameters parms = new esParameters();
			parms.Add("IdApuracaoRisco",idApuracaoRisco);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdApuracaoRisco": this.str.IdApuracaoRisco = (string)value; break;							
						case "IdValidacao": this.str.IdValidacao = (string)value; break;							
						case "PerfilCarteira": this.str.PerfilCarteira = (string)value; break;							
						case "PerfilRiscoFundo": this.str.PerfilRiscoFundo = (string)value; break;							
						case "Criterio": this.str.Criterio = (string)value; break;							
						case "Percentual": this.str.Percentual = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdApuracaoRisco":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdApuracaoRisco = (System.Int32?)value;
							break;
						
						case "IdValidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdValidacao = (System.Int32?)value;
							break;
						
						case "PerfilCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PerfilCarteira = (System.Int32?)value;
							break;
						
						case "Criterio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Criterio = (System.Int32?)value;
							break;
						
						case "Percentual":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Percentual = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRisco.IdApuracaoRisco
		/// </summary>
		virtual public System.Int32? IdApuracaoRisco
		{
			get
			{
				return base.GetSystemInt32(SuitabilityApuracaoRiscoMetadata.ColumnNames.IdApuracaoRisco);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityApuracaoRiscoMetadata.ColumnNames.IdApuracaoRisco, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRisco.IdValidacao
		/// </summary>
		virtual public System.Int32? IdValidacao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityApuracaoRiscoMetadata.ColumnNames.IdValidacao);
			}
			
			set
			{
				if(base.SetSystemInt32(SuitabilityApuracaoRiscoMetadata.ColumnNames.IdValidacao, value))
				{
					this._UpToSuitabilityValidacaoByIdValidacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRisco.PerfilCarteira
		/// </summary>
		virtual public System.Int32? PerfilCarteira
		{
			get
			{
				return base.GetSystemInt32(SuitabilityApuracaoRiscoMetadata.ColumnNames.PerfilCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(SuitabilityApuracaoRiscoMetadata.ColumnNames.PerfilCarteira, value))
				{
					this._UpToSuitabilityPerfilInvestidorByPerfilCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRisco.PerfilRiscoFundo
		/// </summary>
		virtual public System.String PerfilRiscoFundo
		{
			get
			{
				return base.GetSystemString(SuitabilityApuracaoRiscoMetadata.ColumnNames.PerfilRiscoFundo);
			}
			
			set
			{
				base.SetSystemString(SuitabilityApuracaoRiscoMetadata.ColumnNames.PerfilRiscoFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRisco.Criterio
		/// </summary>
		virtual public System.Int32? Criterio
		{
			get
			{
				return base.GetSystemInt32(SuitabilityApuracaoRiscoMetadata.ColumnNames.Criterio);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityApuracaoRiscoMetadata.ColumnNames.Criterio, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRisco.Percentual
		/// </summary>
		virtual public System.Int32? Percentual
		{
			get
			{
				return base.GetSystemInt32(SuitabilityApuracaoRiscoMetadata.ColumnNames.Percentual);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityApuracaoRiscoMetadata.ColumnNames.Percentual, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected SuitabilityPerfilInvestidor _UpToSuitabilityPerfilInvestidorByPerfilCarteira;
		[CLSCompliant(false)]
		internal protected SuitabilityValidacao _UpToSuitabilityValidacaoByIdValidacao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityApuracaoRisco entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdApuracaoRisco
			{
				get
				{
					System.Int32? data = entity.IdApuracaoRisco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdApuracaoRisco = null;
					else entity.IdApuracaoRisco = Convert.ToInt32(value);
				}
			}
				
			public System.String IdValidacao
			{
				get
				{
					System.Int32? data = entity.IdValidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdValidacao = null;
					else entity.IdValidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String PerfilCarteira
			{
				get
				{
					System.Int32? data = entity.PerfilCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerfilCarteira = null;
					else entity.PerfilCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String PerfilRiscoFundo
			{
				get
				{
					System.String data = entity.PerfilRiscoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerfilRiscoFundo = null;
					else entity.PerfilRiscoFundo = Convert.ToString(value);
				}
			}
				
			public System.String Criterio
			{
				get
				{
					System.Int32? data = entity.Criterio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Criterio = null;
					else entity.Criterio = Convert.ToInt32(value);
				}
			}
				
			public System.String Percentual
			{
				get
				{
					System.Int32? data = entity.Percentual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Percentual = null;
					else entity.Percentual = Convert.ToInt32(value);
				}
			}
			

			private esSuitabilityApuracaoRisco entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityApuracaoRiscoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityApuracaoRisco can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityApuracaoRisco : esSuitabilityApuracaoRisco
	{

				
		#region UpToSuitabilityPerfilInvestidorByPerfilCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - SuitabilityApuracaoRisco_SuitabilityPerfilInvestidor_FK
		/// </summary>

		[XmlIgnore]
		public SuitabilityPerfilInvestidor UpToSuitabilityPerfilInvestidorByPerfilCarteira
		{
			get
			{
				if(this._UpToSuitabilityPerfilInvestidorByPerfilCarteira == null
					&& PerfilCarteira != null					)
				{
					this._UpToSuitabilityPerfilInvestidorByPerfilCarteira = new SuitabilityPerfilInvestidor();
					this._UpToSuitabilityPerfilInvestidorByPerfilCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSuitabilityPerfilInvestidorByPerfilCarteira", this._UpToSuitabilityPerfilInvestidorByPerfilCarteira);
					this._UpToSuitabilityPerfilInvestidorByPerfilCarteira.Query.Where(this._UpToSuitabilityPerfilInvestidorByPerfilCarteira.Query.IdPerfilInvestidor == this.PerfilCarteira);
					this._UpToSuitabilityPerfilInvestidorByPerfilCarteira.Query.Load();
				}

				return this._UpToSuitabilityPerfilInvestidorByPerfilCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToSuitabilityPerfilInvestidorByPerfilCarteira");
				

				if(value == null)
				{
					this.PerfilCarteira = null;
					this._UpToSuitabilityPerfilInvestidorByPerfilCarteira = null;
				}
				else
				{
					this.PerfilCarteira = value.IdPerfilInvestidor;
					this._UpToSuitabilityPerfilInvestidorByPerfilCarteira = value;
					this.SetPreSave("UpToSuitabilityPerfilInvestidorByPerfilCarteira", this._UpToSuitabilityPerfilInvestidorByPerfilCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToSuitabilityValidacaoByIdValidacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - SuitabilityApuracaoRisco_SuitabilityValidacao_FK
		/// </summary>

		[XmlIgnore]
		public SuitabilityValidacao UpToSuitabilityValidacaoByIdValidacao
		{
			get
			{
				if(this._UpToSuitabilityValidacaoByIdValidacao == null
					&& IdValidacao != null					)
				{
					this._UpToSuitabilityValidacaoByIdValidacao = new SuitabilityValidacao();
					this._UpToSuitabilityValidacaoByIdValidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSuitabilityValidacaoByIdValidacao", this._UpToSuitabilityValidacaoByIdValidacao);
					this._UpToSuitabilityValidacaoByIdValidacao.Query.Where(this._UpToSuitabilityValidacaoByIdValidacao.Query.IdValidacao == this.IdValidacao);
					this._UpToSuitabilityValidacaoByIdValidacao.Query.Load();
				}

				return this._UpToSuitabilityValidacaoByIdValidacao;
			}
			
			set
			{
				this.RemovePreSave("UpToSuitabilityValidacaoByIdValidacao");
				

				if(value == null)
				{
					this.IdValidacao = null;
					this._UpToSuitabilityValidacaoByIdValidacao = null;
				}
				else
				{
					this.IdValidacao = value.IdValidacao;
					this._UpToSuitabilityValidacaoByIdValidacao = value;
					this.SetPreSave("UpToSuitabilityValidacaoByIdValidacao", this._UpToSuitabilityValidacaoByIdValidacao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToSuitabilityPerfilInvestidorByPerfilCarteira != null)
			{
				this.PerfilCarteira = this._UpToSuitabilityPerfilInvestidorByPerfilCarteira.IdPerfilInvestidor;
			}
			if(!this.es.IsDeleted && this._UpToSuitabilityValidacaoByIdValidacao != null)
			{
				this.IdValidacao = this._UpToSuitabilityValidacaoByIdValidacao.IdValidacao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityApuracaoRiscoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityApuracaoRiscoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdApuracaoRisco
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoMetadata.ColumnNames.IdApuracaoRisco, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdValidacao
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoMetadata.ColumnNames.IdValidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PerfilCarteira
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoMetadata.ColumnNames.PerfilCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PerfilRiscoFundo
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoMetadata.ColumnNames.PerfilRiscoFundo, esSystemType.String);
			}
		} 
		
		public esQueryItem Criterio
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoMetadata.ColumnNames.Criterio, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Percentual
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoMetadata.ColumnNames.Percentual, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityApuracaoRiscoCollection")]
	public partial class SuitabilityApuracaoRiscoCollection : esSuitabilityApuracaoRiscoCollection, IEnumerable<SuitabilityApuracaoRisco>
	{
		public SuitabilityApuracaoRiscoCollection()
		{

		}
		
		public static implicit operator List<SuitabilityApuracaoRisco>(SuitabilityApuracaoRiscoCollection coll)
		{
			List<SuitabilityApuracaoRisco> list = new List<SuitabilityApuracaoRisco>();
			
			foreach (SuitabilityApuracaoRisco emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityApuracaoRiscoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityApuracaoRiscoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityApuracaoRisco(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityApuracaoRisco();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityApuracaoRiscoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityApuracaoRiscoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityApuracaoRiscoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityApuracaoRisco AddNew()
		{
			SuitabilityApuracaoRisco entity = base.AddNewEntity() as SuitabilityApuracaoRisco;
			
			return entity;
		}

		public SuitabilityApuracaoRisco FindByPrimaryKey(System.Int32 idApuracaoRisco)
		{
			return base.FindByPrimaryKey(idApuracaoRisco) as SuitabilityApuracaoRisco;
		}


		#region IEnumerable<SuitabilityApuracaoRisco> Members

		IEnumerator<SuitabilityApuracaoRisco> IEnumerable<SuitabilityApuracaoRisco>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityApuracaoRisco;
			}
		}

		#endregion
		
		private SuitabilityApuracaoRiscoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityApuracaoRisco' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityApuracaoRisco : esSuitabilityApuracaoRisco
	{
		public SuitabilityApuracaoRisco()
		{

		}
	
		public SuitabilityApuracaoRisco(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityApuracaoRiscoMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityApuracaoRiscoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityApuracaoRiscoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityApuracaoRiscoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityApuracaoRiscoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityApuracaoRiscoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityApuracaoRiscoQuery query;
	}



	[Serializable]
	public partial class SuitabilityApuracaoRiscoQuery : esSuitabilityApuracaoRiscoQuery
	{
		public SuitabilityApuracaoRiscoQuery()
		{

		}		
		
		public SuitabilityApuracaoRiscoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityApuracaoRiscoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityApuracaoRiscoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityApuracaoRiscoMetadata.ColumnNames.IdApuracaoRisco, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityApuracaoRiscoMetadata.PropertyNames.IdApuracaoRisco;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityApuracaoRiscoMetadata.ColumnNames.IdValidacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityApuracaoRiscoMetadata.PropertyNames.IdValidacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityApuracaoRiscoMetadata.ColumnNames.PerfilCarteira, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityApuracaoRiscoMetadata.PropertyNames.PerfilCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityApuracaoRiscoMetadata.ColumnNames.PerfilRiscoFundo, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityApuracaoRiscoMetadata.PropertyNames.PerfilRiscoFundo;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityApuracaoRiscoMetadata.ColumnNames.Criterio, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityApuracaoRiscoMetadata.PropertyNames.Criterio;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityApuracaoRiscoMetadata.ColumnNames.Percentual, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityApuracaoRiscoMetadata.PropertyNames.Percentual;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityApuracaoRiscoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdApuracaoRisco = "IdApuracaoRisco";
			 public const string IdValidacao = "IdValidacao";
			 public const string PerfilCarteira = "PerfilCarteira";
			 public const string PerfilRiscoFundo = "PerfilRiscoFundo";
			 public const string Criterio = "Criterio";
			 public const string Percentual = "Percentual";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdApuracaoRisco = "IdApuracaoRisco";
			 public const string IdValidacao = "IdValidacao";
			 public const string PerfilCarteira = "PerfilCarteira";
			 public const string PerfilRiscoFundo = "PerfilRiscoFundo";
			 public const string Criterio = "Criterio";
			 public const string Percentual = "Percentual";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityApuracaoRiscoMetadata))
			{
				if(SuitabilityApuracaoRiscoMetadata.mapDelegates == null)
				{
					SuitabilityApuracaoRiscoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityApuracaoRiscoMetadata.meta == null)
				{
					SuitabilityApuracaoRiscoMetadata.meta = new SuitabilityApuracaoRiscoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdApuracaoRisco", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdValidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PerfilCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PerfilRiscoFundo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Criterio", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Percentual", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "SuitabilityApuracaoRisco";
				meta.Destination = "SuitabilityApuracaoRisco";
				
				meta.spInsert = "proc_SuitabilityApuracaoRiscoInsert";				
				meta.spUpdate = "proc_SuitabilityApuracaoRiscoUpdate";		
				meta.spDelete = "proc_SuitabilityApuracaoRiscoDelete";
				meta.spLoadAll = "proc_SuitabilityApuracaoRiscoLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityApuracaoRiscoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityApuracaoRiscoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
