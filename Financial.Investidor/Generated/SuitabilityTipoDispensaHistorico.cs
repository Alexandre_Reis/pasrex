/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 27/08/2015 10:37:04
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityTipoDispensaHistoricoCollection : esEntityCollection
	{
		public esSuitabilityTipoDispensaHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityTipoDispensaHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityTipoDispensaHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityTipoDispensaHistoricoQuery);
		}
		#endregion
		
		virtual public SuitabilityTipoDispensaHistorico DetachEntity(SuitabilityTipoDispensaHistorico entity)
		{
			return base.DetachEntity(entity) as SuitabilityTipoDispensaHistorico;
		}
		
		virtual public SuitabilityTipoDispensaHistorico AttachEntity(SuitabilityTipoDispensaHistorico entity)
		{
			return base.AttachEntity(entity) as SuitabilityTipoDispensaHistorico;
		}
		
		virtual public void Combine(SuitabilityTipoDispensaHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityTipoDispensaHistorico this[int index]
		{
			get
			{
				return base[index] as SuitabilityTipoDispensaHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityTipoDispensaHistorico);
		}
	}



	[Serializable]
	abstract public class esSuitabilityTipoDispensaHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityTipoDispensaHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityTipoDispensaHistorico()
		{

		}

		public esSuitabilityTipoDispensaHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idDispensa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idDispensa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idDispensa);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idDispensa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idDispensa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idDispensa);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idDispensa)
		{
			esSuitabilityTipoDispensaHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdDispensa == idDispensa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idDispensa)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdDispensa",idDispensa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdDispensa": this.str.IdDispensa = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdDispensa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdDispensa = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityTipoDispensaHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityTipoDispensaHistorico.IdDispensa
		/// </summary>
		virtual public System.Int32? IdDispensa
		{
			get
			{
				return base.GetSystemInt32(SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.IdDispensa);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.IdDispensa, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityTipoDispensaHistorico.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityTipoDispensaHistorico.Tipo
		/// </summary>
		virtual public System.String Tipo
		{
			get
			{
				return base.GetSystemString(SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemString(SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityTipoDispensaHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdDispensa
			{
				get
				{
					System.Int32? data = entity.IdDispensa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdDispensa = null;
					else entity.IdDispensa = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.String data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToString(value);
				}
			}
			

			private esSuitabilityTipoDispensaHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityTipoDispensaHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityTipoDispensaHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityTipoDispensaHistorico : esSuitabilityTipoDispensaHistorico
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityTipoDispensaHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityTipoDispensaHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdDispensa
		{
			get
			{
				return new esQueryItem(this, SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.IdDispensa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.Tipo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityTipoDispensaHistoricoCollection")]
	public partial class SuitabilityTipoDispensaHistoricoCollection : esSuitabilityTipoDispensaHistoricoCollection, IEnumerable<SuitabilityTipoDispensaHistorico>
	{
		public SuitabilityTipoDispensaHistoricoCollection()
		{

		}
		
		public static implicit operator List<SuitabilityTipoDispensaHistorico>(SuitabilityTipoDispensaHistoricoCollection coll)
		{
			List<SuitabilityTipoDispensaHistorico> list = new List<SuitabilityTipoDispensaHistorico>();
			
			foreach (SuitabilityTipoDispensaHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityTipoDispensaHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityTipoDispensaHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityTipoDispensaHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityTipoDispensaHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityTipoDispensaHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityTipoDispensaHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityTipoDispensaHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityTipoDispensaHistorico AddNew()
		{
			SuitabilityTipoDispensaHistorico entity = base.AddNewEntity() as SuitabilityTipoDispensaHistorico;
			
			return entity;
		}

		public SuitabilityTipoDispensaHistorico FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idDispensa)
		{
			return base.FindByPrimaryKey(dataHistorico, idDispensa) as SuitabilityTipoDispensaHistorico;
		}


		#region IEnumerable<SuitabilityTipoDispensaHistorico> Members

		IEnumerator<SuitabilityTipoDispensaHistorico> IEnumerable<SuitabilityTipoDispensaHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityTipoDispensaHistorico;
			}
		}

		#endregion
		
		private SuitabilityTipoDispensaHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityTipoDispensaHistorico' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityTipoDispensaHistorico : esSuitabilityTipoDispensaHistorico
	{
		public SuitabilityTipoDispensaHistorico()
		{

		}
	
		public SuitabilityTipoDispensaHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityTipoDispensaHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityTipoDispensaHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityTipoDispensaHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityTipoDispensaHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityTipoDispensaHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityTipoDispensaHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityTipoDispensaHistoricoQuery query;
	}



	[Serializable]
	public partial class SuitabilityTipoDispensaHistoricoQuery : esSuitabilityTipoDispensaHistoricoQuery
	{
		public SuitabilityTipoDispensaHistoricoQuery()
		{

		}		
		
		public SuitabilityTipoDispensaHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityTipoDispensaHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityTipoDispensaHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SuitabilityTipoDispensaHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.IdDispensa, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityTipoDispensaHistoricoMetadata.PropertyNames.IdDispensa;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityTipoDispensaHistoricoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityTipoDispensaHistoricoMetadata.ColumnNames.Tipo, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityTipoDispensaHistoricoMetadata.PropertyNames.Tipo;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityTipoDispensaHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdDispensa = "IdDispensa";
			 public const string Descricao = "Descricao";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdDispensa = "IdDispensa";
			 public const string Descricao = "Descricao";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityTipoDispensaHistoricoMetadata))
			{
				if(SuitabilityTipoDispensaHistoricoMetadata.mapDelegates == null)
				{
					SuitabilityTipoDispensaHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityTipoDispensaHistoricoMetadata.meta == null)
				{
					SuitabilityTipoDispensaHistoricoMetadata.meta = new SuitabilityTipoDispensaHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdDispensa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "SuitabilityTipoDispensaHistorico";
				meta.Destination = "SuitabilityTipoDispensaHistorico";
				
				meta.spInsert = "proc_SuitabilityTipoDispensaHistoricoInsert";				
				meta.spUpdate = "proc_SuitabilityTipoDispensaHistoricoUpdate";		
				meta.spDelete = "proc_SuitabilityTipoDispensaHistoricoDelete";
				meta.spLoadAll = "proc_SuitabilityTipoDispensaHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityTipoDispensaHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityTipoDispensaHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
