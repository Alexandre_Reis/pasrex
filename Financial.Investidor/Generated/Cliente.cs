/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 07/01/2016 17:11:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Contabil;
using Financial.Common;
using Financial.Captacao;
using Financial.CRM;
using Financial.BMF;
using Financial.Tributo;
using Financial.Bolsa;
using Financial.Gerencial;
using Financial.Fundo;
using Financial.Util;
using Financial.RendaFixa;
using Financial.ContaCorrente;
using Financial.Swap;
using Financial.Security;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esClienteCollection : esEntityCollection
	{
		public esClienteCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ClienteCollection";
		}

		#region Query Logic
		protected void InitQuery(esClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esClienteQuery);
		}
		#endregion
		
		virtual public Cliente DetachEntity(Cliente entity)
		{
			return base.DetachEntity(entity) as Cliente;
		}
		
		virtual public Cliente AttachEntity(Cliente entity)
		{
			return base.AttachEntity(entity) as Cliente;
		}
		
		virtual public void Combine(ClienteCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Cliente this[int index]
		{
			get
			{
				return base[index] as Cliente;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Cliente);
		}
	}



	[Serializable]
	abstract public class esCliente : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esClienteQuery GetDynamicQuery()
		{
			return null;
		}

		public esCliente()
		{

		}

		public esCliente(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esClienteQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente)
		{
			esClienteQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "Apelido": this.str.Apelido = (string)value; break;							
						case "IdTipo": this.str.IdTipo = (string)value; break;							
						case "StatusAtivo": this.str.StatusAtivo = (string)value; break;							
						case "Status": this.str.Status = (string)value; break;							
						case "DataDia": this.str.DataDia = (string)value; break;							
						case "DataInicio": this.str.DataInicio = (string)value; break;							
						case "DataImplantacao": this.str.DataImplantacao = (string)value; break;							
						case "ApuraGanhoRV": this.str.ApuraGanhoRV = (string)value; break;							
						case "TipoControle": this.str.TipoControle = (string)value; break;							
						case "IsentoIR": this.str.IsentoIR = (string)value; break;							
						case "IsentoIOF": this.str.IsentoIOF = (string)value; break;							
						case "IdMoeda": this.str.IdMoeda = (string)value; break;							
						case "IdGrupoProcessamento": this.str.IdGrupoProcessamento = (string)value; break;							
						case "IsProcessando": this.str.IsProcessando = (string)value; break;							
						case "BookEmail": this.str.BookEmail = (string)value; break;							
						case "BookAssunto": this.str.BookAssunto = (string)value; break;							
						case "BookMensagem": this.str.BookMensagem = (string)value; break;							
						case "StatusRealTime": this.str.StatusRealTime = (string)value; break;							
						case "CalculaRealTime": this.str.CalculaRealTime = (string)value; break;							
						case "CalculaGerencial": this.str.CalculaGerencial = (string)value; break;							
						case "CalculaContabil": this.str.CalculaContabil = (string)value; break;							
						case "IdPlano": this.str.IdPlano = (string)value; break;							
						case "ContabilAcoes": this.str.ContabilAcoes = (string)value; break;							
						case "ContabilFundos": this.str.ContabilFundos = (string)value; break;							
						case "IdOfficer": this.str.IdOfficer = (string)value; break;							
						case "ZeraCaixa": this.str.ZeraCaixa = (string)value; break;							
						case "DescontaTributoPL": this.str.DescontaTributoPL = (string)value; break;							
						case "GrossUP": this.str.GrossUP = (string)value; break;							
						case "ContabilBMF": this.str.ContabilBMF = (string)value; break;							
						case "ContabilLiquidacao": this.str.ContabilLiquidacao = (string)value; break;							
						case "ContabilDaytrade": this.str.ContabilDaytrade = (string)value; break;							
						case "IdGrupoAfinidade": this.str.IdGrupoAfinidade = (string)value; break;							
						case "Logotipo": this.str.Logotipo = (string)value; break;							
						case "IdLocal": this.str.IdLocal = (string)value; break;							
						case "ContabilDataTrava": this.str.ContabilDataTrava = (string)value; break;							
						case "DataBloqueioProcessamento": this.str.DataBloqueioProcessamento = (string)value; break;							
						case "MultiMoeda": this.str.MultiMoeda = (string)value; break;							
						case "IdLocalNegociacao": this.str.IdLocalNegociacao = (string)value; break;							
						case "RegimeEspecialTributacao": this.str.RegimeEspecialTributacao = (string)value; break;							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;							
						case "AberturaIndexada": this.str.AberturaIndexada = (string)value; break;							
						case "IdIndiceAbertura": this.str.IdIndiceAbertura = (string)value; break;
						case "InvestidorProfissional": this.str.InvestidorProfissional = (string)value; break;							
						case "InvestidorQualificado": this.str.InvestidorQualificado = (string)value; break;							
						case "AmortizacaoRendimentoJuros": this.str.AmortizacaoRendimentoJuros = (string)value; break;
						case "DataAtualizacao": this.str.DataAtualizacao = (string)value; break;							
						case "DataEnvioCetip": this.str.DataEnvioCetip = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdTipo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTipo = (System.Int32?)value;
							break;
						
						case "StatusAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.StatusAtivo = (System.Byte?)value;
							break;
						
						case "Status":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Status = (System.Byte?)value;
							break;
						
						case "DataDia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataDia = (System.DateTime?)value;
							break;
						
						case "DataInicio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicio = (System.DateTime?)value;
							break;
						
						case "DataImplantacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataImplantacao = (System.DateTime?)value;
							break;
						
						case "TipoControle":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoControle = (System.Byte?)value;
							break;
						
						case "IdMoeda":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdMoeda = (System.Int16?)value;
							break;
						
						case "IdGrupoProcessamento":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdGrupoProcessamento = (System.Int16?)value;
							break;
						
						case "StatusRealTime":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.StatusRealTime = (System.Byte?)value;
							break;
						
						case "IdPlano":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPlano = (System.Int32?)value;
							break;
						
						case "ContabilAcoes":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContabilAcoes = (System.Byte?)value;
							break;
						
						case "ContabilFundos":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContabilFundos = (System.Byte?)value;
							break;
						
						case "IdOfficer":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOfficer = (System.Int32?)value;
							break;
						
						case "DescontaTributoPL":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.DescontaTributoPL = (System.Byte?)value;
							break;
						
						case "GrossUP":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.GrossUP = (System.Byte?)value;
							break;
						
						case "ContabilBMF":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContabilBMF = (System.Byte?)value;
							break;
						
						case "ContabilLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContabilLiquidacao = (System.Byte?)value;
							break;
						
						case "ContabilDaytrade":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContabilDaytrade = (System.Byte?)value;
							break;
						
						case "IdGrupoAfinidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupoAfinidade = (System.Int32?)value;
							break;
						
						case "IdLocal":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdLocal = (System.Int16?)value;
							break;
						
						case "ContabilDataTrava":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.ContabilDataTrava = (System.DateTime?)value;
							break;
						
						case "DataBloqueioProcessamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataBloqueioProcessamento = (System.DateTime?)value;
							break;
						
						case "IdLocalNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalNegociacao = (System.Int32?)value;
							break;
						
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
						
						case "AberturaIndexada":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.AberturaIndexada = (System.Int16?)value;
							break;
						
						case "IdIndiceAbertura":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndiceAbertura = (System.Int16?)value;
							break;
						
						case "DataAtualizacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAtualizacao = (System.DateTime?)value;
							break;
						
						case "DataEnvioCetip":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEnvioCetip = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Cliente.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ClienteMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(ClienteMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.Apelido
		/// </summary>
		virtual public System.String Apelido
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.Apelido);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.Apelido, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.IdTipo
		/// </summary>
		virtual public System.Int32? IdTipo
		{
			get
			{
				return base.GetSystemInt32(ClienteMetadata.ColumnNames.IdTipo);
			}
			
			set
			{
				if(base.SetSystemInt32(ClienteMetadata.ColumnNames.IdTipo, value))
				{
					this._UpToTipoClienteByIdTipo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Cliente.StatusAtivo
		/// </summary>
		virtual public System.Byte? StatusAtivo
		{
			get
			{
				return base.GetSystemByte(ClienteMetadata.ColumnNames.StatusAtivo);
			}
			
			set
			{
				base.SetSystemByte(ClienteMetadata.ColumnNames.StatusAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.Status
		/// </summary>
		virtual public System.Byte? Status
		{
			get
			{
				return base.GetSystemByte(ClienteMetadata.ColumnNames.Status);
			}
			
			set
			{
				base.SetSystemByte(ClienteMetadata.ColumnNames.Status, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.DataDia
		/// </summary>
		virtual public System.DateTime? DataDia
		{
			get
			{
				return base.GetSystemDateTime(ClienteMetadata.ColumnNames.DataDia);
			}
			
			set
			{
				base.SetSystemDateTime(ClienteMetadata.ColumnNames.DataDia, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.DataInicio
		/// </summary>
		virtual public System.DateTime? DataInicio
		{
			get
			{
				return base.GetSystemDateTime(ClienteMetadata.ColumnNames.DataInicio);
			}
			
			set
			{
				base.SetSystemDateTime(ClienteMetadata.ColumnNames.DataInicio, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.DataImplantacao
		/// </summary>
		virtual public System.DateTime? DataImplantacao
		{
			get
			{
				return base.GetSystemDateTime(ClienteMetadata.ColumnNames.DataImplantacao);
			}
			
			set
			{
				base.SetSystemDateTime(ClienteMetadata.ColumnNames.DataImplantacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.ApuraGanhoRV
		/// </summary>
		virtual public System.String ApuraGanhoRV
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.ApuraGanhoRV);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.ApuraGanhoRV, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.TipoControle
		/// </summary>
		virtual public System.Byte? TipoControle
		{
			get
			{
				return base.GetSystemByte(ClienteMetadata.ColumnNames.TipoControle);
			}
			
			set
			{
				base.SetSystemByte(ClienteMetadata.ColumnNames.TipoControle, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.IsentoIR
		/// </summary>
		virtual public System.String IsentoIR
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.IsentoIR);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.IsentoIR, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.IsentoIOF
		/// </summary>
		virtual public System.String IsentoIOF
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.IsentoIOF);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.IsentoIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.IdMoeda
		/// </summary>
		virtual public System.Int16? IdMoeda
		{
			get
			{
				return base.GetSystemInt16(ClienteMetadata.ColumnNames.IdMoeda);
			}
			
			set
			{
				if(base.SetSystemInt16(ClienteMetadata.ColumnNames.IdMoeda, value))
				{
					this._UpToMoedaByIdMoeda = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Cliente.IdGrupoProcessamento
		/// </summary>
		virtual public System.Int16? IdGrupoProcessamento
		{
			get
			{
				return base.GetSystemInt16(ClienteMetadata.ColumnNames.IdGrupoProcessamento);
			}
			
			set
			{
				if(base.SetSystemInt16(ClienteMetadata.ColumnNames.IdGrupoProcessamento, value))
				{
					this._UpToGrupoProcessamentoByIdGrupoProcessamento = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Cliente.IsProcessando
		/// </summary>
		virtual public System.String IsProcessando
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.IsProcessando);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.IsProcessando, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.BookEmail
		/// </summary>
		virtual public System.String BookEmail
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.BookEmail);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.BookEmail, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.BookAssunto
		/// </summary>
		virtual public System.String BookAssunto
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.BookAssunto);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.BookAssunto, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.BookMensagem
		/// </summary>
		virtual public System.String BookMensagem
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.BookMensagem);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.BookMensagem, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.StatusRealTime
		/// </summary>
		virtual public System.Byte? StatusRealTime
		{
			get
			{
				return base.GetSystemByte(ClienteMetadata.ColumnNames.StatusRealTime);
			}
			
			set
			{
				base.SetSystemByte(ClienteMetadata.ColumnNames.StatusRealTime, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.CalculaRealTime
		/// </summary>
		virtual public System.String CalculaRealTime
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.CalculaRealTime);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.CalculaRealTime, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.CalculaGerencial
		/// </summary>
		virtual public System.String CalculaGerencial
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.CalculaGerencial);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.CalculaGerencial, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.CalculaContabil
		/// </summary>
		virtual public System.String CalculaContabil
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.CalculaContabil);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.CalculaContabil, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.IdPlano
		/// </summary>
		virtual public System.Int32? IdPlano
		{
			get
			{
				return base.GetSystemInt32(ClienteMetadata.ColumnNames.IdPlano);
			}
			
			set
			{
				if(base.SetSystemInt32(ClienteMetadata.ColumnNames.IdPlano, value))
				{
					this._UpToContabPlanoByIdPlano = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Cliente.ContabilAcoes
		/// </summary>
		virtual public System.Byte? ContabilAcoes
		{
			get
			{
				return base.GetSystemByte(ClienteMetadata.ColumnNames.ContabilAcoes);
			}
			
			set
			{
				base.SetSystemByte(ClienteMetadata.ColumnNames.ContabilAcoes, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.ContabilFundos
		/// </summary>
		virtual public System.Byte? ContabilFundos
		{
			get
			{
				return base.GetSystemByte(ClienteMetadata.ColumnNames.ContabilFundos);
			}
			
			set
			{
				base.SetSystemByte(ClienteMetadata.ColumnNames.ContabilFundos, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.IdOfficer
		/// </summary>
		virtual public System.Int32? IdOfficer
		{
			get
			{
				return base.GetSystemInt32(ClienteMetadata.ColumnNames.IdOfficer);
			}
			
			set
			{
				if(base.SetSystemInt32(ClienteMetadata.ColumnNames.IdOfficer, value))
				{
					this._UpToOfficerByIdOfficer = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Cliente.ZeraCaixa
		/// </summary>
		virtual public System.String ZeraCaixa
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.ZeraCaixa);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.ZeraCaixa, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.DescontaTributoPL
		/// </summary>
		virtual public System.Byte? DescontaTributoPL
		{
			get
			{
				return base.GetSystemByte(ClienteMetadata.ColumnNames.DescontaTributoPL);
			}
			
			set
			{
				base.SetSystemByte(ClienteMetadata.ColumnNames.DescontaTributoPL, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.GrossUP
		/// </summary>
		virtual public System.Byte? GrossUP
		{
			get
			{
				return base.GetSystemByte(ClienteMetadata.ColumnNames.GrossUP);
			}
			
			set
			{
				base.SetSystemByte(ClienteMetadata.ColumnNames.GrossUP, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.ContabilBMF
		/// </summary>
		virtual public System.Byte? ContabilBMF
		{
			get
			{
				return base.GetSystemByte(ClienteMetadata.ColumnNames.ContabilBMF);
			}
			
			set
			{
				base.SetSystemByte(ClienteMetadata.ColumnNames.ContabilBMF, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.ContabilLiquidacao
		/// </summary>
		virtual public System.Byte? ContabilLiquidacao
		{
			get
			{
				return base.GetSystemByte(ClienteMetadata.ColumnNames.ContabilLiquidacao);
			}
			
			set
			{
				base.SetSystemByte(ClienteMetadata.ColumnNames.ContabilLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.ContabilDaytrade
		/// </summary>
		virtual public System.Byte? ContabilDaytrade
		{
			get
			{
				return base.GetSystemByte(ClienteMetadata.ColumnNames.ContabilDaytrade);
			}
			
			set
			{
				base.SetSystemByte(ClienteMetadata.ColumnNames.ContabilDaytrade, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.IdGrupoAfinidade
		/// </summary>
		virtual public System.Int32? IdGrupoAfinidade
		{
			get
			{
				return base.GetSystemInt32(ClienteMetadata.ColumnNames.IdGrupoAfinidade);
			}
			
			set
			{
				if(base.SetSystemInt32(ClienteMetadata.ColumnNames.IdGrupoAfinidade, value))
				{
					this._UpToGrupoAfinidadeByIdGrupoAfinidade = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Cliente.Logotipo
		/// </summary>
		virtual public System.String Logotipo
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.Logotipo);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.Logotipo, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.IdLocal
		/// </summary>
		virtual public System.Int16? IdLocal
		{
			get
			{
				return base.GetSystemInt16(ClienteMetadata.ColumnNames.IdLocal);
			}
			
			set
			{
				base.SetSystemInt16(ClienteMetadata.ColumnNames.IdLocal, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.ContabilDataTrava
		/// </summary>
		virtual public System.DateTime? ContabilDataTrava
		{
			get
			{
				return base.GetSystemDateTime(ClienteMetadata.ColumnNames.ContabilDataTrava);
			}
			
			set
			{
				base.SetSystemDateTime(ClienteMetadata.ColumnNames.ContabilDataTrava, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.DataBloqueioProcessamento
		/// </summary>
		virtual public System.DateTime? DataBloqueioProcessamento
		{
			get
			{
				return base.GetSystemDateTime(ClienteMetadata.ColumnNames.DataBloqueioProcessamento);
			}
			
			set
			{
				base.SetSystemDateTime(ClienteMetadata.ColumnNames.DataBloqueioProcessamento, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.MultiMoeda
		/// </summary>
		virtual public System.String MultiMoeda
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.MultiMoeda);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.MultiMoeda, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.IdLocalNegociacao
		/// </summary>
		virtual public System.Int32? IdLocalNegociacao
		{
			get
			{
				return base.GetSystemInt32(ClienteMetadata.ColumnNames.IdLocalNegociacao);
			}
			
			set
			{
				base.SetSystemInt32(ClienteMetadata.ColumnNames.IdLocalNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.RegimeEspecialTributacao
		/// </summary>
		virtual public System.String RegimeEspecialTributacao
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.RegimeEspecialTributacao);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.RegimeEspecialTributacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(ClienteMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				if(base.SetSystemInt32(ClienteMetadata.ColumnNames.IdPessoa, value))
				{
					this._UpToPessoaByIdPessoa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Cliente.AberturaIndexada
		/// </summary>
		virtual public System.Int16? AberturaIndexada
		{
			get
			{
				return base.GetSystemInt16(ClienteMetadata.ColumnNames.AberturaIndexada);
			}
			
			set
			{
				base.SetSystemInt16(ClienteMetadata.ColumnNames.AberturaIndexada, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.IdIndiceAbertura
		/// </summary>
		virtual public System.Int16? IdIndiceAbertura
		{
			get
			{
				return base.GetSystemInt16(ClienteMetadata.ColumnNames.IdIndiceAbertura);
			}
			
			set
			{
				if(base.SetSystemInt16(ClienteMetadata.ColumnNames.IdIndiceAbertura, value))
				{
					this._UpToIndiceByIdIndiceAbertura = null;
				}
			}
		}

		/// <summary>
		/// Maps to Cliente.InvestidorProfissional
		/// </summary>
		virtual public System.String InvestidorProfissional
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.InvestidorProfissional);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.InvestidorProfissional, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.InvestidorQualificado
		/// </summary>
		virtual public System.String InvestidorQualificado
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.InvestidorQualificado);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.InvestidorQualificado, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.AmortizacaoRendimentoJuros
		/// </summary>
		virtual public System.String AmortizacaoRendimentoJuros
		{
			get
			{
				return base.GetSystemString(ClienteMetadata.ColumnNames.AmortizacaoRendimentoJuros);
			}
			
			set
			{
				base.SetSystemString(ClienteMetadata.ColumnNames.AmortizacaoRendimentoJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.DataAtualizacao
		/// </summary>
		virtual public System.DateTime? DataAtualizacao
		{
			get
			{
				return base.GetSystemDateTime(ClienteMetadata.ColumnNames.DataAtualizacao);
			}
			
			set
			{
				base.SetSystemDateTime(ClienteMetadata.ColumnNames.DataAtualizacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Cliente.DataEnvioCetip
		/// </summary>
		virtual public System.DateTime? DataEnvioCetip
		{
			get
			{
				return base.GetSystemDateTime(ClienteMetadata.ColumnNames.DataEnvioCetip);
			}
			
			set
			{
				base.SetSystemDateTime(ClienteMetadata.ColumnNames.DataEnvioCetip, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected ContabPlano _UpToContabPlanoByIdPlano;
		[CLSCompliant(false)]
		internal protected GrupoAfinidade _UpToGrupoAfinidadeByIdGrupoAfinidade;
		[CLSCompliant(false)]
		internal protected GrupoProcessamento _UpToGrupoProcessamentoByIdGrupoProcessamento;
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndiceAbertura;
		[CLSCompliant(false)]
		internal protected Moeda _UpToMoedaByIdMoeda;
		[CLSCompliant(false)]
		internal protected Officer _UpToOfficerByIdOfficer;
		[CLSCompliant(false)]
		internal protected Pessoa _UpToPessoaByIdPessoa;
		[CLSCompliant(false)]
		internal protected TipoCliente _UpToTipoClienteByIdTipo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCliente entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String Apelido
			{
				get
				{
					System.String data = entity.Apelido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Apelido = null;
					else entity.Apelido = Convert.ToString(value);
				}
			}
				
			public System.String IdTipo
			{
				get
				{
					System.Int32? data = entity.IdTipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTipo = null;
					else entity.IdTipo = Convert.ToInt32(value);
				}
			}
				
			public System.String StatusAtivo
			{
				get
				{
					System.Byte? data = entity.StatusAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.StatusAtivo = null;
					else entity.StatusAtivo = Convert.ToByte(value);
				}
			}
				
			public System.String Status
			{
				get
				{
					System.Byte? data = entity.Status;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Status = null;
					else entity.Status = Convert.ToByte(value);
				}
			}
				
			public System.String DataDia
			{
				get
				{
					System.DateTime? data = entity.DataDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataDia = null;
					else entity.DataDia = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataInicio
			{
				get
				{
					System.DateTime? data = entity.DataInicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicio = null;
					else entity.DataInicio = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataImplantacao
			{
				get
				{
					System.DateTime? data = entity.DataImplantacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataImplantacao = null;
					else entity.DataImplantacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String ApuraGanhoRV
			{
				get
				{
					System.String data = entity.ApuraGanhoRV;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ApuraGanhoRV = null;
					else entity.ApuraGanhoRV = Convert.ToString(value);
				}
			}
				
			public System.String TipoControle
			{
				get
				{
					System.Byte? data = entity.TipoControle;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoControle = null;
					else entity.TipoControle = Convert.ToByte(value);
				}
			}
				
			public System.String IsentoIR
			{
				get
				{
					System.String data = entity.IsentoIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IsentoIR = null;
					else entity.IsentoIR = Convert.ToString(value);
				}
			}
				
			public System.String IsentoIOF
			{
				get
				{
					System.String data = entity.IsentoIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IsentoIOF = null;
					else entity.IsentoIOF = Convert.ToString(value);
				}
			}
				
			public System.String IdMoeda
			{
				get
				{
					System.Int16? data = entity.IdMoeda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoeda = null;
					else entity.IdMoeda = Convert.ToInt16(value);
				}
			}
				
			public System.String IdGrupoProcessamento
			{
				get
				{
					System.Int16? data = entity.IdGrupoProcessamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupoProcessamento = null;
					else entity.IdGrupoProcessamento = Convert.ToInt16(value);
				}
			}
				
			public System.String IsProcessando
			{
				get
				{
					System.String data = entity.IsProcessando;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IsProcessando = null;
					else entity.IsProcessando = Convert.ToString(value);
				}
			}
				
			public System.String BookEmail
			{
				get
				{
					System.String data = entity.BookEmail;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BookEmail = null;
					else entity.BookEmail = Convert.ToString(value);
				}
			}
				
			public System.String BookAssunto
			{
				get
				{
					System.String data = entity.BookAssunto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BookAssunto = null;
					else entity.BookAssunto = Convert.ToString(value);
				}
			}
				
			public System.String BookMensagem
			{
				get
				{
					System.String data = entity.BookMensagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BookMensagem = null;
					else entity.BookMensagem = Convert.ToString(value);
				}
			}
				
			public System.String StatusRealTime
			{
				get
				{
					System.Byte? data = entity.StatusRealTime;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.StatusRealTime = null;
					else entity.StatusRealTime = Convert.ToByte(value);
				}
			}
				
			public System.String CalculaRealTime
			{
				get
				{
					System.String data = entity.CalculaRealTime;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CalculaRealTime = null;
					else entity.CalculaRealTime = Convert.ToString(value);
				}
			}
				
			public System.String CalculaGerencial
			{
				get
				{
					System.String data = entity.CalculaGerencial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CalculaGerencial = null;
					else entity.CalculaGerencial = Convert.ToString(value);
				}
			}
				
			public System.String CalculaContabil
			{
				get
				{
					System.String data = entity.CalculaContabil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CalculaContabil = null;
					else entity.CalculaContabil = Convert.ToString(value);
				}
			}
				
			public System.String IdPlano
			{
				get
				{
					System.Int32? data = entity.IdPlano;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPlano = null;
					else entity.IdPlano = Convert.ToInt32(value);
				}
			}
				
			public System.String ContabilAcoes
			{
				get
				{
					System.Byte? data = entity.ContabilAcoes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContabilAcoes = null;
					else entity.ContabilAcoes = Convert.ToByte(value);
				}
			}
				
			public System.String ContabilFundos
			{
				get
				{
					System.Byte? data = entity.ContabilFundos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContabilFundos = null;
					else entity.ContabilFundos = Convert.ToByte(value);
				}
			}
				
			public System.String IdOfficer
			{
				get
				{
					System.Int32? data = entity.IdOfficer;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOfficer = null;
					else entity.IdOfficer = Convert.ToInt32(value);
				}
			}
				
			public System.String ZeraCaixa
			{
				get
				{
					System.String data = entity.ZeraCaixa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ZeraCaixa = null;
					else entity.ZeraCaixa = Convert.ToString(value);
				}
			}
				
			public System.String DescontaTributoPL
			{
				get
				{
					System.Byte? data = entity.DescontaTributoPL;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescontaTributoPL = null;
					else entity.DescontaTributoPL = Convert.ToByte(value);
				}
			}
				
			public System.String GrossUP
			{
				get
				{
					System.Byte? data = entity.GrossUP;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GrossUP = null;
					else entity.GrossUP = Convert.ToByte(value);
				}
			}
				
			public System.String ContabilBMF
			{
				get
				{
					System.Byte? data = entity.ContabilBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContabilBMF = null;
					else entity.ContabilBMF = Convert.ToByte(value);
				}
			}
				
			public System.String ContabilLiquidacao
			{
				get
				{
					System.Byte? data = entity.ContabilLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContabilLiquidacao = null;
					else entity.ContabilLiquidacao = Convert.ToByte(value);
				}
			}
				
			public System.String ContabilDaytrade
			{
				get
				{
					System.Byte? data = entity.ContabilDaytrade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContabilDaytrade = null;
					else entity.ContabilDaytrade = Convert.ToByte(value);
				}
			}
				
			public System.String IdGrupoAfinidade
			{
				get
				{
					System.Int32? data = entity.IdGrupoAfinidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupoAfinidade = null;
					else entity.IdGrupoAfinidade = Convert.ToInt32(value);
				}
			}
				
			public System.String Logotipo
			{
				get
				{
					System.String data = entity.Logotipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Logotipo = null;
					else entity.Logotipo = Convert.ToString(value);
				}
			}
				
			public System.String IdLocal
			{
				get
				{
					System.Int16? data = entity.IdLocal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocal = null;
					else entity.IdLocal = Convert.ToInt16(value);
				}
			}
				
			public System.String ContabilDataTrava
			{
				get
				{
					System.DateTime? data = entity.ContabilDataTrava;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContabilDataTrava = null;
					else entity.ContabilDataTrava = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataBloqueioProcessamento
			{
				get
				{
					System.DateTime? data = entity.DataBloqueioProcessamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataBloqueioProcessamento = null;
					else entity.DataBloqueioProcessamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String MultiMoeda
			{
				get
				{
					System.String data = entity.MultiMoeda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MultiMoeda = null;
					else entity.MultiMoeda = Convert.ToString(value);
				}
			}
				
			public System.String IdLocalNegociacao
			{
				get
				{
					System.Int32? data = entity.IdLocalNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalNegociacao = null;
					else entity.IdLocalNegociacao = Convert.ToInt32(value);
				}
			}
				
			public System.String RegimeEspecialTributacao
			{
				get
				{
					System.String data = entity.RegimeEspecialTributacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RegimeEspecialTributacao = null;
					else entity.RegimeEspecialTributacao = Convert.ToString(value);
				}
			}
				
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
				
			public System.String AberturaIndexada
			{
				get
				{
					System.Int16? data = entity.AberturaIndexada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AberturaIndexada = null;
					else entity.AberturaIndexada = Convert.ToInt16(value);
				}
			}
				
			public System.String IdIndiceAbertura
			{
				get
				{
					System.Int16? data = entity.IdIndiceAbertura;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndiceAbertura = null;
					else entity.IdIndiceAbertura = Convert.ToInt16(value);
				}
			}

			public System.String InvestidorProfissional
			{
				get
				{
					System.String data = entity.InvestidorProfissional;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InvestidorProfissional = null;
					else entity.InvestidorProfissional = Convert.ToString(value);
				}
			}
				
			public System.String InvestidorQualificado
			{
				get
				{
					System.String data = entity.InvestidorQualificado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InvestidorQualificado = null;
					else entity.InvestidorQualificado = Convert.ToString(value);
				}
			}
				
			public System.String AmortizacaoRendimentoJuros
			{
				get
				{
					System.String data = entity.AmortizacaoRendimentoJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AmortizacaoRendimentoJuros = null;
					else entity.AmortizacaoRendimentoJuros = Convert.ToString(value);
				}
			}
			
			public System.String DataAtualizacao
			{
				get
				{
					System.DateTime? data = entity.DataAtualizacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAtualizacao = null;
					else entity.DataAtualizacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataEnvioCetip
			{
				get
				{
					System.DateTime? data = entity.DataEnvioCetip;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEnvioCetip = null;
					else entity.DataEnvioCetip = Convert.ToDateTime(value);
				}
			}
			

			private esCliente entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCliente can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Cliente : esCliente
	{

				
		#region AcumuladoCorretagemBMFCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_AcumuladoCorretagemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AcumuladoCorretagemBMFCollection AcumuladoCorretagemBMFCollectionByIdCliente
		{
			get
			{
				if(this._AcumuladoCorretagemBMFCollectionByIdCliente == null)
				{
					this._AcumuladoCorretagemBMFCollectionByIdCliente = new AcumuladoCorretagemBMFCollection();
					this._AcumuladoCorretagemBMFCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AcumuladoCorretagemBMFCollectionByIdCliente", this._AcumuladoCorretagemBMFCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._AcumuladoCorretagemBMFCollectionByIdCliente.Query.Where(this._AcumuladoCorretagemBMFCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._AcumuladoCorretagemBMFCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._AcumuladoCorretagemBMFCollectionByIdCliente.fks.Add(AcumuladoCorretagemBMFMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._AcumuladoCorretagemBMFCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AcumuladoCorretagemBMFCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("AcumuladoCorretagemBMFCollectionByIdCliente"); 
					this._AcumuladoCorretagemBMFCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private AcumuladoCorretagemBMFCollection _AcumuladoCorretagemBMFCollectionByIdCliente;
		#endregion

				
		#region ApuracaoIRImobiliarioCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_ApuracaoIRImobiliario_FK1
		/// </summary>

		[XmlIgnore]
		public ApuracaoIRImobiliarioCollection ApuracaoIRImobiliarioCollectionByIdCliente
		{
			get
			{
				if(this._ApuracaoIRImobiliarioCollectionByIdCliente == null)
				{
					this._ApuracaoIRImobiliarioCollectionByIdCliente = new ApuracaoIRImobiliarioCollection();
					this._ApuracaoIRImobiliarioCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ApuracaoIRImobiliarioCollectionByIdCliente", this._ApuracaoIRImobiliarioCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._ApuracaoIRImobiliarioCollectionByIdCliente.Query.Where(this._ApuracaoIRImobiliarioCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._ApuracaoIRImobiliarioCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._ApuracaoIRImobiliarioCollectionByIdCliente.fks.Add(ApuracaoIRImobiliarioMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._ApuracaoIRImobiliarioCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ApuracaoIRImobiliarioCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("ApuracaoIRImobiliarioCollectionByIdCliente"); 
					this._ApuracaoIRImobiliarioCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private ApuracaoIRImobiliarioCollection _ApuracaoIRImobiliarioCollectionByIdCliente;
		#endregion

				
		#region ApuracaoRendaVariavelIRCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_ApuracaoRendaVariavelIR_FK1
		/// </summary>

		[XmlIgnore]
		public ApuracaoRendaVariavelIRCollection ApuracaoRendaVariavelIRCollectionByIdCliente
		{
			get
			{
				if(this._ApuracaoRendaVariavelIRCollectionByIdCliente == null)
				{
					this._ApuracaoRendaVariavelIRCollectionByIdCliente = new ApuracaoRendaVariavelIRCollection();
					this._ApuracaoRendaVariavelIRCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ApuracaoRendaVariavelIRCollectionByIdCliente", this._ApuracaoRendaVariavelIRCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._ApuracaoRendaVariavelIRCollectionByIdCliente.Query.Where(this._ApuracaoRendaVariavelIRCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._ApuracaoRendaVariavelIRCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._ApuracaoRendaVariavelIRCollectionByIdCliente.fks.Add(ApuracaoRendaVariavelIRMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._ApuracaoRendaVariavelIRCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ApuracaoRendaVariavelIRCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("ApuracaoRendaVariavelIRCollectionByIdCliente"); 
					this._ApuracaoRendaVariavelIRCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private ApuracaoRendaVariavelIRCollection _ApuracaoRendaVariavelIRCollectionByIdCliente;
		#endregion

				
		#region BloqueioBolsaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_BloqueioBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public BloqueioBolsaCollection BloqueioBolsaCollectionByIdCliente
		{
			get
			{
				if(this._BloqueioBolsaCollectionByIdCliente == null)
				{
					this._BloqueioBolsaCollectionByIdCliente = new BloqueioBolsaCollection();
					this._BloqueioBolsaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("BloqueioBolsaCollectionByIdCliente", this._BloqueioBolsaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._BloqueioBolsaCollectionByIdCliente.Query.Where(this._BloqueioBolsaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._BloqueioBolsaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._BloqueioBolsaCollectionByIdCliente.fks.Add(BloqueioBolsaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._BloqueioBolsaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._BloqueioBolsaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("BloqueioBolsaCollectionByIdCliente"); 
					this._BloqueioBolsaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private BloqueioBolsaCollection _BloqueioBolsaCollectionByIdCliente;
		#endregion

				
		#region BoletoRetroativo - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Cliente_BoletoRetroativo_FK1
		/// </summary>

		[XmlIgnore]
		public BoletoRetroativo BoletoRetroativo
		{
			get
			{
				if(this._BoletoRetroativo == null)
				{
					this._BoletoRetroativo = new BoletoRetroativo();
					this._BoletoRetroativo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("BoletoRetroativo", this._BoletoRetroativo);
				
					if(this.IdCliente != null)
					{
						this._BoletoRetroativo.Query.Where(this._BoletoRetroativo.Query.IdCliente == this.IdCliente);
						this._BoletoRetroativo.Query.Load();
					}
				}

				return this._BoletoRetroativo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._BoletoRetroativo != null) 
				{ 
					this.RemovePostOneSave("BoletoRetroativo"); 
					this._BoletoRetroativo = null;
					
				} 
			}          			
		}

		private BoletoRetroativo _BoletoRetroativo;
		#endregion

		#region UpToBookCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - Cliente_BookCliente_FK1
		/// </summary>

		[XmlIgnore]
		public BookCollection UpToBookCollection
		{
			get
			{
				if(this._UpToBookCollection == null)
				{
					this._UpToBookCollection = new BookCollection();
					this._UpToBookCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToBookCollection", this._UpToBookCollection);
					this._UpToBookCollection.ManyToManyClienteCollection(this.IdCliente);
				}

				return this._UpToBookCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToBookCollection != null) 
				{ 
					this.RemovePostSave("UpToBookCollection"); 
					this._UpToBookCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - Cliente_BookCliente_FK1
		/// </summary>
		public void AssociateBookCollection(Book entity)
		{
			if (this._BookClienteCollection == null)
			{
				this._BookClienteCollection = new BookClienteCollection();
				this._BookClienteCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("BookClienteCollection", this._BookClienteCollection);
			}

			BookCliente obj = this._BookClienteCollection.AddNew();
			obj.IdCliente = this.IdCliente;
			obj.IdBook = entity.IdBook;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - Cliente_BookCliente_FK1
		/// </summary>
		public void DissociateBookCollection(Book entity)
		{
			if (this._BookClienteCollection == null)
			{
				this._BookClienteCollection = new BookClienteCollection();
				this._BookClienteCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("BookClienteCollection", this._BookClienteCollection);
			}

			BookCliente obj = this._BookClienteCollection.AddNew();
			obj.IdCliente = this.IdCliente;
			obj.IdBook = entity.IdBook;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private BookCollection _UpToBookCollection;
		private BookClienteCollection _BookClienteCollection;
		#endregion

				
		#region BookClienteCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_BookCliente_FK1
		/// </summary>

		[XmlIgnore]
		public BookClienteCollection BookClienteCollectionByIdCliente
		{
			get
			{
				if(this._BookClienteCollectionByIdCliente == null)
				{
					this._BookClienteCollectionByIdCliente = new BookClienteCollection();
					this._BookClienteCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("BookClienteCollectionByIdCliente", this._BookClienteCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._BookClienteCollectionByIdCliente.Query.Where(this._BookClienteCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._BookClienteCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._BookClienteCollectionByIdCliente.fks.Add(BookClienteMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._BookClienteCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._BookClienteCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("BookClienteCollectionByIdCliente"); 
					this._BookClienteCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private BookClienteCollection _BookClienteCollectionByIdCliente;
		#endregion

				
		#region CalculoGerencialCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_CalculoGerencial_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoGerencialCollection CalculoGerencialCollectionByIdCliente
		{
			get
			{
				if(this._CalculoGerencialCollectionByIdCliente == null)
				{
					this._CalculoGerencialCollectionByIdCliente = new CalculoGerencialCollection();
					this._CalculoGerencialCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoGerencialCollectionByIdCliente", this._CalculoGerencialCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._CalculoGerencialCollectionByIdCliente.Query.Where(this._CalculoGerencialCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._CalculoGerencialCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoGerencialCollectionByIdCliente.fks.Add(CalculoGerencialMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._CalculoGerencialCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoGerencialCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("CalculoGerencialCollectionByIdCliente"); 
					this._CalculoGerencialCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private CalculoGerencialCollection _CalculoGerencialCollectionByIdCliente;
		#endregion

				
		#region CalculoRebateImpactaPLCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - CalculoRebateImpactaPL_Cliente_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoRebateImpactaPLCollection CalculoRebateImpactaPLCollectionByIdCliente
		{
			get
			{
				if(this._CalculoRebateImpactaPLCollectionByIdCliente == null)
				{
					this._CalculoRebateImpactaPLCollectionByIdCliente = new CalculoRebateImpactaPLCollection();
					this._CalculoRebateImpactaPLCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoRebateImpactaPLCollectionByIdCliente", this._CalculoRebateImpactaPLCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._CalculoRebateImpactaPLCollectionByIdCliente.Query.Where(this._CalculoRebateImpactaPLCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._CalculoRebateImpactaPLCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoRebateImpactaPLCollectionByIdCliente.fks.Add(CalculoRebateImpactaPLMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._CalculoRebateImpactaPLCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoRebateImpactaPLCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("CalculoRebateImpactaPLCollectionByIdCliente"); 
					this._CalculoRebateImpactaPLCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private CalculoRebateImpactaPLCollection _CalculoRebateImpactaPLCollectionByIdCliente;
		#endregion

				
		#region Carteira - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Cliente_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira Carteira
		{
			get
			{
				if(this._Carteira == null)
				{
					this._Carteira = new Carteira();
					this._Carteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("Carteira", this._Carteira);
				
					if(this.IdCliente != null)
					{
						this._Carteira.Query.Where(this._Carteira.Query.IdCarteira == this.IdCliente);
						this._Carteira.Query.Load();
					}
				}

				return this._Carteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._Carteira != null) 
				{ 
					this.RemovePostOneSave("Carteira"); 
					this._Carteira = null;
					
				} 
			}          			
		}

		private Carteira _Carteira;
		#endregion

				
		#region ClienteBMF - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Cliente_ClienteBMF_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteBMF ClienteBMF
		{
			get
			{
				if(this._ClienteBMF == null)
				{
					this._ClienteBMF = new ClienteBMF();
					this._ClienteBMF.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("ClienteBMF", this._ClienteBMF);
				
					if(this.IdCliente != null)
					{
						this._ClienteBMF.Query.Where(this._ClienteBMF.Query.IdCliente == this.IdCliente);
						this._ClienteBMF.Query.Load();
					}
				}

				return this._ClienteBMF;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClienteBMF != null) 
				{ 
					this.RemovePostOneSave("ClienteBMF"); 
					this._ClienteBMF = null;
					
				} 
			}          			
		}

		private ClienteBMF _ClienteBMF;
		#endregion

				
		#region ClienteBolsa - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Cliente_ClienteBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteBolsa ClienteBolsa
		{
			get
			{
				if(this._ClienteBolsa == null)
				{
					this._ClienteBolsa = new ClienteBolsa();
					this._ClienteBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("ClienteBolsa", this._ClienteBolsa);
				
					if(this.IdCliente != null)
					{
						this._ClienteBolsa.Query.Where(this._ClienteBolsa.Query.IdCliente == this.IdCliente);
						this._ClienteBolsa.Query.Load();
					}
				}

				return this._ClienteBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClienteBolsa != null) 
				{ 
					this.RemovePostOneSave("ClienteBolsa"); 
					this._ClienteBolsa = null;
					
				} 
			}          			
		}

		private ClienteBolsa _ClienteBolsa;
		#endregion

				
		#region ClienteInterface - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Cliente_ClienteInterface_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteInterface ClienteInterface
		{
			get
			{
				if(this._ClienteInterface == null)
				{
					this._ClienteInterface = new ClienteInterface();
					this._ClienteInterface.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("ClienteInterface", this._ClienteInterface);
				
					if(this.IdCliente != null)
					{
						this._ClienteInterface.Query.Where(this._ClienteInterface.Query.IdCliente == this.IdCliente);
						this._ClienteInterface.Query.Load();
					}
				}

				return this._ClienteInterface;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClienteInterface != null) 
				{ 
					this.RemovePostOneSave("ClienteInterface"); 
					this._ClienteInterface = null;
					
				} 
			}          			
		}

		private ClienteInterface _ClienteInterface;
		#endregion

		#region UpToPerfilProcessamentoCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - Cliente_ClientePerfil_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilProcessamentoCollection UpToPerfilProcessamentoCollection
		{
			get
			{
				if(this._UpToPerfilProcessamentoCollection == null)
				{
					this._UpToPerfilProcessamentoCollection = new PerfilProcessamentoCollection();
					this._UpToPerfilProcessamentoCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToPerfilProcessamentoCollection", this._UpToPerfilProcessamentoCollection);
					this._UpToPerfilProcessamentoCollection.ManyToManyClienteCollection(this.IdCliente);
				}

				return this._UpToPerfilProcessamentoCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToPerfilProcessamentoCollection != null) 
				{ 
					this.RemovePostSave("UpToPerfilProcessamentoCollection"); 
					this._UpToPerfilProcessamentoCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - Cliente_ClientePerfil_FK1
		/// </summary>
		public void AssociatePerfilProcessamentoCollection(PerfilProcessamento entity)
		{
			if (this._ClientePerfilCollection == null)
			{
				this._ClientePerfilCollection = new ClientePerfilCollection();
				this._ClientePerfilCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("ClientePerfilCollection", this._ClientePerfilCollection);
			}

			ClientePerfil obj = this._ClientePerfilCollection.AddNew();
			obj.IdCliente = this.IdCliente;
			obj.IdPerfil = entity.IdPerfil;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - Cliente_ClientePerfil_FK1
		/// </summary>
		public void DissociatePerfilProcessamentoCollection(PerfilProcessamento entity)
		{
			if (this._ClientePerfilCollection == null)
			{
				this._ClientePerfilCollection = new ClientePerfilCollection();
				this._ClientePerfilCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("ClientePerfilCollection", this._ClientePerfilCollection);
			}

			ClientePerfil obj = this._ClientePerfilCollection.AddNew();
			obj.IdCliente = this.IdCliente;
			obj.IdPerfil = entity.IdPerfil;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private PerfilProcessamentoCollection _UpToPerfilProcessamentoCollection;
		private ClientePerfilCollection _ClientePerfilCollection;
		#endregion

				
		#region ClientePerfilCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_ClientePerfil_FK1
		/// </summary>

		[XmlIgnore]
		public ClientePerfilCollection ClientePerfilCollectionByIdCliente
		{
			get
			{
				if(this._ClientePerfilCollectionByIdCliente == null)
				{
					this._ClientePerfilCollectionByIdCliente = new ClientePerfilCollection();
					this._ClientePerfilCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ClientePerfilCollectionByIdCliente", this._ClientePerfilCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._ClientePerfilCollectionByIdCliente.Query.Where(this._ClientePerfilCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._ClientePerfilCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._ClientePerfilCollectionByIdCliente.fks.Add(ClientePerfilMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._ClientePerfilCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClientePerfilCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("ClientePerfilCollectionByIdCliente"); 
					this._ClientePerfilCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private ClientePerfilCollection _ClientePerfilCollectionByIdCliente;
		#endregion

				
		#region ClienteRendaFixa - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Cliente_ClienteRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteRendaFixa ClienteRendaFixa
		{
			get
			{
				if(this._ClienteRendaFixa == null)
				{
					this._ClienteRendaFixa = new ClienteRendaFixa();
					this._ClienteRendaFixa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("ClienteRendaFixa", this._ClienteRendaFixa);
				
					if(this.IdCliente != null)
					{
						this._ClienteRendaFixa.Query.Where(this._ClienteRendaFixa.Query.IdCliente == this.IdCliente);
						this._ClienteRendaFixa.Query.Load();
					}
				}

				return this._ClienteRendaFixa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClienteRendaFixa != null) 
				{ 
					this.RemovePostOneSave("ClienteRendaFixa"); 
					this._ClienteRendaFixa = null;
					
				} 
			}          			
		}

		private ClienteRendaFixa _ClienteRendaFixa;
		#endregion

				
		#region CodigoClienteAgenteCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_CodigoClienteAgente_FK1
		/// </summary>

		[XmlIgnore]
		public CodigoClienteAgenteCollection CodigoClienteAgenteCollectionByIdCliente
		{
			get
			{
				if(this._CodigoClienteAgenteCollectionByIdCliente == null)
				{
					this._CodigoClienteAgenteCollectionByIdCliente = new CodigoClienteAgenteCollection();
					this._CodigoClienteAgenteCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CodigoClienteAgenteCollectionByIdCliente", this._CodigoClienteAgenteCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._CodigoClienteAgenteCollectionByIdCliente.Query.Where(this._CodigoClienteAgenteCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._CodigoClienteAgenteCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._CodigoClienteAgenteCollectionByIdCliente.fks.Add(CodigoClienteAgenteMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._CodigoClienteAgenteCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CodigoClienteAgenteCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("CodigoClienteAgenteCollectionByIdCliente"); 
					this._CodigoClienteAgenteCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private CodigoClienteAgenteCollection _CodigoClienteAgenteCollectionByIdCliente;
		#endregion

				
		#region ContabLancamentoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_ContabLancamento_FK1
		/// </summary>

		[XmlIgnore]
		public ContabLancamentoCollection ContabLancamentoCollectionByIdCliente
		{
			get
			{
				if(this._ContabLancamentoCollectionByIdCliente == null)
				{
					this._ContabLancamentoCollectionByIdCliente = new ContabLancamentoCollection();
					this._ContabLancamentoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ContabLancamentoCollectionByIdCliente", this._ContabLancamentoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._ContabLancamentoCollectionByIdCliente.Query.Where(this._ContabLancamentoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._ContabLancamentoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._ContabLancamentoCollectionByIdCliente.fks.Add(ContabLancamentoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._ContabLancamentoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ContabLancamentoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("ContabLancamentoCollectionByIdCliente"); 
					this._ContabLancamentoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private ContabLancamentoCollection _ContabLancamentoCollectionByIdCliente;
		#endregion

				
		#region ContabSaldoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ContabSaldo_ContabLancamento_FK1
		/// </summary>

		[XmlIgnore]
		public ContabSaldoCollection ContabSaldoCollectionByIdCliente
		{
			get
			{
				if(this._ContabSaldoCollectionByIdCliente == null)
				{
					this._ContabSaldoCollectionByIdCliente = new ContabSaldoCollection();
					this._ContabSaldoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ContabSaldoCollectionByIdCliente", this._ContabSaldoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._ContabSaldoCollectionByIdCliente.Query.Where(this._ContabSaldoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._ContabSaldoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._ContabSaldoCollectionByIdCliente.fks.Add(ContabSaldoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._ContabSaldoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ContabSaldoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("ContabSaldoCollectionByIdCliente"); 
					this._ContabSaldoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private ContabSaldoCollection _ContabSaldoCollectionByIdCliente;
		#endregion

				
		#region CustodiaBMFCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_CustodiaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public CustodiaBMFCollection CustodiaBMFCollectionByIdCliente
		{
			get
			{
				if(this._CustodiaBMFCollectionByIdCliente == null)
				{
					this._CustodiaBMFCollectionByIdCliente = new CustodiaBMFCollection();
					this._CustodiaBMFCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CustodiaBMFCollectionByIdCliente", this._CustodiaBMFCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._CustodiaBMFCollectionByIdCliente.Query.Where(this._CustodiaBMFCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._CustodiaBMFCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._CustodiaBMFCollectionByIdCliente.fks.Add(CustodiaBMFMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._CustodiaBMFCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CustodiaBMFCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("CustodiaBMFCollectionByIdCliente"); 
					this._CustodiaBMFCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private CustodiaBMFCollection _CustodiaBMFCollectionByIdCliente;
		#endregion

				
		#region DetalheResgateFundoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_DetalheResgateFundo_FK1
		/// </summary>

		[XmlIgnore]
		public DetalheResgateFundoCollection DetalheResgateFundoCollectionByIdCliente
		{
			get
			{
				if(this._DetalheResgateFundoCollectionByIdCliente == null)
				{
					this._DetalheResgateFundoCollectionByIdCliente = new DetalheResgateFundoCollection();
					this._DetalheResgateFundoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("DetalheResgateFundoCollectionByIdCliente", this._DetalheResgateFundoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._DetalheResgateFundoCollectionByIdCliente.Query.Where(this._DetalheResgateFundoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._DetalheResgateFundoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._DetalheResgateFundoCollectionByIdCliente.fks.Add(DetalheResgateFundoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._DetalheResgateFundoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._DetalheResgateFundoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("DetalheResgateFundoCollectionByIdCliente"); 
					this._DetalheResgateFundoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private DetalheResgateFundoCollection _DetalheResgateFundoCollectionByIdCliente;
		#endregion

				
		#region EventoFisicoBolsaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_EventoFisicoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFisicoBolsaCollection EventoFisicoBolsaCollectionByIdCliente
		{
			get
			{
				if(this._EventoFisicoBolsaCollectionByIdCliente == null)
				{
					this._EventoFisicoBolsaCollectionByIdCliente = new EventoFisicoBolsaCollection();
					this._EventoFisicoBolsaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoFisicoBolsaCollectionByIdCliente", this._EventoFisicoBolsaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._EventoFisicoBolsaCollectionByIdCliente.Query.Where(this._EventoFisicoBolsaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._EventoFisicoBolsaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoFisicoBolsaCollectionByIdCliente.fks.Add(EventoFisicoBolsaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._EventoFisicoBolsaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoFisicoBolsaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("EventoFisicoBolsaCollectionByIdCliente"); 
					this._EventoFisicoBolsaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private EventoFisicoBolsaCollection _EventoFisicoBolsaCollectionByIdCliente;
		#endregion

				
		#region EventoRendaFixaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_EventoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public EventoRendaFixaCollection EventoRendaFixaCollectionByIdCliente
		{
			get
			{
				if(this._EventoRendaFixaCollectionByIdCliente == null)
				{
					this._EventoRendaFixaCollectionByIdCliente = new EventoRendaFixaCollection();
					this._EventoRendaFixaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoRendaFixaCollectionByIdCliente", this._EventoRendaFixaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._EventoRendaFixaCollectionByIdCliente.Query.Where(this._EventoRendaFixaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._EventoRendaFixaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoRendaFixaCollectionByIdCliente.fks.Add(EventoRendaFixaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._EventoRendaFixaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoRendaFixaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("EventoRendaFixaCollectionByIdCliente"); 
					this._EventoRendaFixaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private EventoRendaFixaCollection _EventoRendaFixaCollectionByIdCliente;
		#endregion

				
		#region GerOperacaoBMFCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_GerOperacaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public GerOperacaoBMFCollection GerOperacaoBMFCollectionByIdCliente
		{
			get
			{
				if(this._GerOperacaoBMFCollectionByIdCliente == null)
				{
					this._GerOperacaoBMFCollectionByIdCliente = new GerOperacaoBMFCollection();
					this._GerOperacaoBMFCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerOperacaoBMFCollectionByIdCliente", this._GerOperacaoBMFCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._GerOperacaoBMFCollectionByIdCliente.Query.Where(this._GerOperacaoBMFCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._GerOperacaoBMFCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerOperacaoBMFCollectionByIdCliente.fks.Add(GerOperacaoBMFMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._GerOperacaoBMFCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerOperacaoBMFCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("GerOperacaoBMFCollectionByIdCliente"); 
					this._GerOperacaoBMFCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private GerOperacaoBMFCollection _GerOperacaoBMFCollectionByIdCliente;
		#endregion

				
		#region GerOperacaoBolsaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_GerOperacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public GerOperacaoBolsaCollection GerOperacaoBolsaCollectionByIdCliente
		{
			get
			{
				if(this._GerOperacaoBolsaCollectionByIdCliente == null)
				{
					this._GerOperacaoBolsaCollectionByIdCliente = new GerOperacaoBolsaCollection();
					this._GerOperacaoBolsaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerOperacaoBolsaCollectionByIdCliente", this._GerOperacaoBolsaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._GerOperacaoBolsaCollectionByIdCliente.Query.Where(this._GerOperacaoBolsaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._GerOperacaoBolsaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerOperacaoBolsaCollectionByIdCliente.fks.Add(GerOperacaoBolsaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._GerOperacaoBolsaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerOperacaoBolsaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("GerOperacaoBolsaCollectionByIdCliente"); 
					this._GerOperacaoBolsaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private GerOperacaoBolsaCollection _GerOperacaoBolsaCollectionByIdCliente;
		#endregion

				
		#region GerPosicaoBMFCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_GerPosicaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBMFCollection GerPosicaoBMFCollectionByIdCliente
		{
			get
			{
				if(this._GerPosicaoBMFCollectionByIdCliente == null)
				{
					this._GerPosicaoBMFCollectionByIdCliente = new GerPosicaoBMFCollection();
					this._GerPosicaoBMFCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBMFCollectionByIdCliente", this._GerPosicaoBMFCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._GerPosicaoBMFCollectionByIdCliente.Query.Where(this._GerPosicaoBMFCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._GerPosicaoBMFCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBMFCollectionByIdCliente.fks.Add(GerPosicaoBMFMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._GerPosicaoBMFCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBMFCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("GerPosicaoBMFCollectionByIdCliente"); 
					this._GerPosicaoBMFCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private GerPosicaoBMFCollection _GerPosicaoBMFCollectionByIdCliente;
		#endregion

				
		#region GerPosicaoBMFAberturaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_GerPosicaoBMFAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBMFAberturaCollection GerPosicaoBMFAberturaCollectionByIdCliente
		{
			get
			{
				if(this._GerPosicaoBMFAberturaCollectionByIdCliente == null)
				{
					this._GerPosicaoBMFAberturaCollectionByIdCliente = new GerPosicaoBMFAberturaCollection();
					this._GerPosicaoBMFAberturaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBMFAberturaCollectionByIdCliente", this._GerPosicaoBMFAberturaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._GerPosicaoBMFAberturaCollectionByIdCliente.Query.Where(this._GerPosicaoBMFAberturaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._GerPosicaoBMFAberturaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBMFAberturaCollectionByIdCliente.fks.Add(GerPosicaoBMFAberturaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._GerPosicaoBMFAberturaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBMFAberturaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("GerPosicaoBMFAberturaCollectionByIdCliente"); 
					this._GerPosicaoBMFAberturaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private GerPosicaoBMFAberturaCollection _GerPosicaoBMFAberturaCollectionByIdCliente;
		#endregion

				
		#region GerPosicaoBMFHistoricoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_GerPosicaoBMFHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBMFHistoricoCollection GerPosicaoBMFHistoricoCollectionByIdCliente
		{
			get
			{
				if(this._GerPosicaoBMFHistoricoCollectionByIdCliente == null)
				{
					this._GerPosicaoBMFHistoricoCollectionByIdCliente = new GerPosicaoBMFHistoricoCollection();
					this._GerPosicaoBMFHistoricoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBMFHistoricoCollectionByIdCliente", this._GerPosicaoBMFHistoricoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._GerPosicaoBMFHistoricoCollectionByIdCliente.Query.Where(this._GerPosicaoBMFHistoricoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._GerPosicaoBMFHistoricoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBMFHistoricoCollectionByIdCliente.fks.Add(GerPosicaoBMFHistoricoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._GerPosicaoBMFHistoricoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBMFHistoricoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("GerPosicaoBMFHistoricoCollectionByIdCliente"); 
					this._GerPosicaoBMFHistoricoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private GerPosicaoBMFHistoricoCollection _GerPosicaoBMFHistoricoCollectionByIdCliente;
		#endregion

				
		#region GerPosicaoBolsaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_GerPosicaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBolsaCollection GerPosicaoBolsaCollectionByIdCliente
		{
			get
			{
				if(this._GerPosicaoBolsaCollectionByIdCliente == null)
				{
					this._GerPosicaoBolsaCollectionByIdCliente = new GerPosicaoBolsaCollection();
					this._GerPosicaoBolsaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBolsaCollectionByIdCliente", this._GerPosicaoBolsaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._GerPosicaoBolsaCollectionByIdCliente.Query.Where(this._GerPosicaoBolsaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._GerPosicaoBolsaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBolsaCollectionByIdCliente.fks.Add(GerPosicaoBolsaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._GerPosicaoBolsaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBolsaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("GerPosicaoBolsaCollectionByIdCliente"); 
					this._GerPosicaoBolsaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private GerPosicaoBolsaCollection _GerPosicaoBolsaCollectionByIdCliente;
		#endregion

				
		#region GerPosicaoBolsaAberturaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_GerPosicaoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBolsaAberturaCollection GerPosicaoBolsaAberturaCollectionByIdCliente
		{
			get
			{
				if(this._GerPosicaoBolsaAberturaCollectionByIdCliente == null)
				{
					this._GerPosicaoBolsaAberturaCollectionByIdCliente = new GerPosicaoBolsaAberturaCollection();
					this._GerPosicaoBolsaAberturaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBolsaAberturaCollectionByIdCliente", this._GerPosicaoBolsaAberturaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._GerPosicaoBolsaAberturaCollectionByIdCliente.Query.Where(this._GerPosicaoBolsaAberturaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._GerPosicaoBolsaAberturaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBolsaAberturaCollectionByIdCliente.fks.Add(GerPosicaoBolsaAberturaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._GerPosicaoBolsaAberturaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBolsaAberturaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("GerPosicaoBolsaAberturaCollectionByIdCliente"); 
					this._GerPosicaoBolsaAberturaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private GerPosicaoBolsaAberturaCollection _GerPosicaoBolsaAberturaCollectionByIdCliente;
		#endregion

				
		#region GerPosicaoBolsaHistoricoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_GerPosicaoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBolsaHistoricoCollection GerPosicaoBolsaHistoricoCollectionByIdCliente
		{
			get
			{
				if(this._GerPosicaoBolsaHistoricoCollectionByIdCliente == null)
				{
					this._GerPosicaoBolsaHistoricoCollectionByIdCliente = new GerPosicaoBolsaHistoricoCollection();
					this._GerPosicaoBolsaHistoricoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBolsaHistoricoCollectionByIdCliente", this._GerPosicaoBolsaHistoricoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._GerPosicaoBolsaHistoricoCollectionByIdCliente.Query.Where(this._GerPosicaoBolsaHistoricoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._GerPosicaoBolsaHistoricoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBolsaHistoricoCollectionByIdCliente.fks.Add(GerPosicaoBolsaHistoricoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._GerPosicaoBolsaHistoricoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBolsaHistoricoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("GerPosicaoBolsaHistoricoCollectionByIdCliente"); 
					this._GerPosicaoBolsaHistoricoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private GerPosicaoBolsaHistoricoCollection _GerPosicaoBolsaHistoricoCollectionByIdCliente;
		#endregion

				
		#region IRFonteCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_IRFonte_FK1
		/// </summary>

		[XmlIgnore]
		public IRFonteCollection IRFonteCollectionByIdCliente
		{
			get
			{
				if(this._IRFonteCollectionByIdCliente == null)
				{
					this._IRFonteCollectionByIdCliente = new IRFonteCollection();
					this._IRFonteCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("IRFonteCollectionByIdCliente", this._IRFonteCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._IRFonteCollectionByIdCliente.Query.Where(this._IRFonteCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._IRFonteCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._IRFonteCollectionByIdCliente.fks.Add(IRFonteMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._IRFonteCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._IRFonteCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("IRFonteCollectionByIdCliente"); 
					this._IRFonteCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private IRFonteCollection _IRFonteCollectionByIdCliente;
		#endregion

				
		#region LiquidacaoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_Liquidacao_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoCollection LiquidacaoCollectionByIdCliente
		{
			get
			{
				if(this._LiquidacaoCollectionByIdCliente == null)
				{
					this._LiquidacaoCollectionByIdCliente = new LiquidacaoCollection();
					this._LiquidacaoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoCollectionByIdCliente", this._LiquidacaoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._LiquidacaoCollectionByIdCliente.Query.Where(this._LiquidacaoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._LiquidacaoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoCollectionByIdCliente.fks.Add(LiquidacaoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._LiquidacaoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("LiquidacaoCollectionByIdCliente"); 
					this._LiquidacaoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private LiquidacaoCollection _LiquidacaoCollectionByIdCliente;
		#endregion

				
		#region LiquidacaoAberturaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_LiquidacaoAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoAberturaCollection LiquidacaoAberturaCollectionByIdCliente
		{
			get
			{
				if(this._LiquidacaoAberturaCollectionByIdCliente == null)
				{
					this._LiquidacaoAberturaCollectionByIdCliente = new LiquidacaoAberturaCollection();
					this._LiquidacaoAberturaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoAberturaCollectionByIdCliente", this._LiquidacaoAberturaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._LiquidacaoAberturaCollectionByIdCliente.Query.Where(this._LiquidacaoAberturaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._LiquidacaoAberturaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoAberturaCollectionByIdCliente.fks.Add(LiquidacaoAberturaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._LiquidacaoAberturaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoAberturaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("LiquidacaoAberturaCollectionByIdCliente"); 
					this._LiquidacaoAberturaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private LiquidacaoAberturaCollection _LiquidacaoAberturaCollectionByIdCliente;
		#endregion

				
		#region LiquidacaoEmprestimoBolsaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_AntecipacaoEmprestimoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoEmprestimoBolsaCollection LiquidacaoEmprestimoBolsaCollectionByIdCliente
		{
			get
			{
				if(this._LiquidacaoEmprestimoBolsaCollectionByIdCliente == null)
				{
					this._LiquidacaoEmprestimoBolsaCollectionByIdCliente = new LiquidacaoEmprestimoBolsaCollection();
					this._LiquidacaoEmprestimoBolsaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoEmprestimoBolsaCollectionByIdCliente", this._LiquidacaoEmprestimoBolsaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._LiquidacaoEmprestimoBolsaCollectionByIdCliente.Query.Where(this._LiquidacaoEmprestimoBolsaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._LiquidacaoEmprestimoBolsaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoEmprestimoBolsaCollectionByIdCliente.fks.Add(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._LiquidacaoEmprestimoBolsaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoEmprestimoBolsaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("LiquidacaoEmprestimoBolsaCollectionByIdCliente"); 
					this._LiquidacaoEmprestimoBolsaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private LiquidacaoEmprestimoBolsaCollection _LiquidacaoEmprestimoBolsaCollectionByIdCliente;
		#endregion

				
		#region LiquidacaoFuturoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_LiquidacaoFuturo_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoFuturoCollection LiquidacaoFuturoCollectionByIdCliente
		{
			get
			{
				if(this._LiquidacaoFuturoCollectionByIdCliente == null)
				{
					this._LiquidacaoFuturoCollectionByIdCliente = new LiquidacaoFuturoCollection();
					this._LiquidacaoFuturoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoFuturoCollectionByIdCliente", this._LiquidacaoFuturoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._LiquidacaoFuturoCollectionByIdCliente.Query.Where(this._LiquidacaoFuturoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._LiquidacaoFuturoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoFuturoCollectionByIdCliente.fks.Add(LiquidacaoFuturoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._LiquidacaoFuturoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoFuturoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("LiquidacaoFuturoCollectionByIdCliente"); 
					this._LiquidacaoFuturoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private LiquidacaoFuturoCollection _LiquidacaoFuturoCollectionByIdCliente;
		#endregion

				
		#region LiquidacaoHistoricoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_LiquidacaoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoHistoricoCollection LiquidacaoHistoricoCollectionByIdCliente
		{
			get
			{
				if(this._LiquidacaoHistoricoCollectionByIdCliente == null)
				{
					this._LiquidacaoHistoricoCollectionByIdCliente = new LiquidacaoHistoricoCollection();
					this._LiquidacaoHistoricoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoHistoricoCollectionByIdCliente", this._LiquidacaoHistoricoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._LiquidacaoHistoricoCollectionByIdCliente.Query.Where(this._LiquidacaoHistoricoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._LiquidacaoHistoricoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoHistoricoCollectionByIdCliente.fks.Add(LiquidacaoHistoricoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._LiquidacaoHistoricoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoHistoricoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("LiquidacaoHistoricoCollectionByIdCliente"); 
					this._LiquidacaoHistoricoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private LiquidacaoHistoricoCollection _LiquidacaoHistoricoCollectionByIdCliente;
		#endregion

				
		#region LiquidacaoRendaFixaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_LiquidacaoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoRendaFixaCollection LiquidacaoRendaFixaCollectionByIdCliente
		{
			get
			{
				if(this._LiquidacaoRendaFixaCollectionByIdCliente == null)
				{
					this._LiquidacaoRendaFixaCollectionByIdCliente = new LiquidacaoRendaFixaCollection();
					this._LiquidacaoRendaFixaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoRendaFixaCollectionByIdCliente", this._LiquidacaoRendaFixaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._LiquidacaoRendaFixaCollectionByIdCliente.Query.Where(this._LiquidacaoRendaFixaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._LiquidacaoRendaFixaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoRendaFixaCollectionByIdCliente.fks.Add(LiquidacaoRendaFixaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._LiquidacaoRendaFixaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoRendaFixaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("LiquidacaoRendaFixaCollectionByIdCliente"); 
					this._LiquidacaoRendaFixaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private LiquidacaoRendaFixaCollection _LiquidacaoRendaFixaCollectionByIdCliente;
		#endregion

				
		#region LiquidacaoSwapCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_LiquidacaoSwap_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoSwapCollection LiquidacaoSwapCollectionByIdCliente
		{
			get
			{
				if(this._LiquidacaoSwapCollectionByIdCliente == null)
				{
					this._LiquidacaoSwapCollectionByIdCliente = new LiquidacaoSwapCollection();
					this._LiquidacaoSwapCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoSwapCollectionByIdCliente", this._LiquidacaoSwapCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._LiquidacaoSwapCollectionByIdCliente.Query.Where(this._LiquidacaoSwapCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._LiquidacaoSwapCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoSwapCollectionByIdCliente.fks.Add(LiquidacaoSwapMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._LiquidacaoSwapCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoSwapCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("LiquidacaoSwapCollectionByIdCliente"); 
					this._LiquidacaoSwapCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private LiquidacaoSwapCollection _LiquidacaoSwapCollectionByIdCliente;
		#endregion

				
		#region LiquidacaoTermoBolsaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_LiquidacaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoTermoBolsaCollection LiquidacaoTermoBolsaCollectionByIdCliente
		{
			get
			{
				if(this._LiquidacaoTermoBolsaCollectionByIdCliente == null)
				{
					this._LiquidacaoTermoBolsaCollectionByIdCliente = new LiquidacaoTermoBolsaCollection();
					this._LiquidacaoTermoBolsaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoTermoBolsaCollectionByIdCliente", this._LiquidacaoTermoBolsaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._LiquidacaoTermoBolsaCollectionByIdCliente.Query.Where(this._LiquidacaoTermoBolsaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._LiquidacaoTermoBolsaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoTermoBolsaCollectionByIdCliente.fks.Add(LiquidacaoTermoBolsaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._LiquidacaoTermoBolsaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoTermoBolsaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("LiquidacaoTermoBolsaCollectionByIdCliente"); 
					this._LiquidacaoTermoBolsaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private LiquidacaoTermoBolsaCollection _LiquidacaoTermoBolsaCollectionByIdCliente;
		#endregion

				
		#region LogProcessamentoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_LogProcessamento_FK1
		/// </summary>

		[XmlIgnore]
		public LogProcessamentoCollection LogProcessamentoCollectionByIdCliente
		{
			get
			{
				if(this._LogProcessamentoCollectionByIdCliente == null)
				{
					this._LogProcessamentoCollectionByIdCliente = new LogProcessamentoCollection();
					this._LogProcessamentoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LogProcessamentoCollectionByIdCliente", this._LogProcessamentoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._LogProcessamentoCollectionByIdCliente.Query.Where(this._LogProcessamentoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._LogProcessamentoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._LogProcessamentoCollectionByIdCliente.fks.Add(LogProcessamentoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._LogProcessamentoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LogProcessamentoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("LogProcessamentoCollectionByIdCliente"); 
					this._LogProcessamentoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private LogProcessamentoCollection _LogProcessamentoCollectionByIdCliente;
		#endregion

				
		#region OperacaoBMFCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_OperacaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoBMFCollection OperacaoBMFCollectionByIdCliente
		{
			get
			{
				if(this._OperacaoBMFCollectionByIdCliente == null)
				{
					this._OperacaoBMFCollectionByIdCliente = new OperacaoBMFCollection();
					this._OperacaoBMFCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoBMFCollectionByIdCliente", this._OperacaoBMFCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._OperacaoBMFCollectionByIdCliente.Query.Where(this._OperacaoBMFCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._OperacaoBMFCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoBMFCollectionByIdCliente.fks.Add(OperacaoBMFMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._OperacaoBMFCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoBMFCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("OperacaoBMFCollectionByIdCliente"); 
					this._OperacaoBMFCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private OperacaoBMFCollection _OperacaoBMFCollectionByIdCliente;
		#endregion

				
		#region OperacaoBolsaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_OperacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoBolsaCollection OperacaoBolsaCollectionByIdCliente
		{
			get
			{
				if(this._OperacaoBolsaCollectionByIdCliente == null)
				{
					this._OperacaoBolsaCollectionByIdCliente = new OperacaoBolsaCollection();
					this._OperacaoBolsaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoBolsaCollectionByIdCliente", this._OperacaoBolsaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._OperacaoBolsaCollectionByIdCliente.Query.Where(this._OperacaoBolsaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._OperacaoBolsaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoBolsaCollectionByIdCliente.fks.Add(OperacaoBolsaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._OperacaoBolsaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoBolsaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("OperacaoBolsaCollectionByIdCliente"); 
					this._OperacaoBolsaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private OperacaoBolsaCollection _OperacaoBolsaCollectionByIdCliente;
		#endregion

				
		#region OperacaoCambioCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_OperacaoCambio_Cliente
		/// </summary>

		[XmlIgnore]
		public OperacaoCambioCollection OperacaoCambioCollectionByIdCliente
		{
			get
			{
				if(this._OperacaoCambioCollectionByIdCliente == null)
				{
					this._OperacaoCambioCollectionByIdCliente = new OperacaoCambioCollection();
					this._OperacaoCambioCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoCambioCollectionByIdCliente", this._OperacaoCambioCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._OperacaoCambioCollectionByIdCliente.Query.Where(this._OperacaoCambioCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._OperacaoCambioCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoCambioCollectionByIdCliente.fks.Add(OperacaoCambioMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._OperacaoCambioCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoCambioCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("OperacaoCambioCollectionByIdCliente"); 
					this._OperacaoCambioCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private OperacaoCambioCollection _OperacaoCambioCollectionByIdCliente;
		#endregion

				
		#region OperacaoFundoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_OperacaoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoFundoCollection OperacaoFundoCollectionByIdCliente
		{
			get
			{
				if(this._OperacaoFundoCollectionByIdCliente == null)
				{
					this._OperacaoFundoCollectionByIdCliente = new OperacaoFundoCollection();
					this._OperacaoFundoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoFundoCollectionByIdCliente", this._OperacaoFundoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._OperacaoFundoCollectionByIdCliente.Query.Where(this._OperacaoFundoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._OperacaoFundoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoFundoCollectionByIdCliente.fks.Add(OperacaoFundoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._OperacaoFundoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoFundoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("OperacaoFundoCollectionByIdCliente"); 
					this._OperacaoFundoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private OperacaoFundoCollection _OperacaoFundoCollectionByIdCliente;
		#endregion

				
		#region OperacaoRendaFixaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_OperacaoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoRendaFixaCollection OperacaoRendaFixaCollectionByIdCliente
		{
			get
			{
				if(this._OperacaoRendaFixaCollectionByIdCliente == null)
				{
					this._OperacaoRendaFixaCollectionByIdCliente = new OperacaoRendaFixaCollection();
					this._OperacaoRendaFixaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoRendaFixaCollectionByIdCliente", this._OperacaoRendaFixaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._OperacaoRendaFixaCollectionByIdCliente.Query.Where(this._OperacaoRendaFixaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._OperacaoRendaFixaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoRendaFixaCollectionByIdCliente.fks.Add(OperacaoRendaFixaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._OperacaoRendaFixaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoRendaFixaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("OperacaoRendaFixaCollectionByIdCliente"); 
					this._OperacaoRendaFixaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private OperacaoRendaFixaCollection _OperacaoRendaFixaCollectionByIdCliente;
		#endregion

				
		#region OrdemBMFCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_OrdemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemBMFCollection OrdemBMFCollectionByIdCliente
		{
			get
			{
				if(this._OrdemBMFCollectionByIdCliente == null)
				{
					this._OrdemBMFCollectionByIdCliente = new OrdemBMFCollection();
					this._OrdemBMFCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemBMFCollectionByIdCliente", this._OrdemBMFCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._OrdemBMFCollectionByIdCliente.Query.Where(this._OrdemBMFCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._OrdemBMFCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemBMFCollectionByIdCliente.fks.Add(OrdemBMFMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._OrdemBMFCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemBMFCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("OrdemBMFCollectionByIdCliente"); 
					this._OrdemBMFCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private OrdemBMFCollection _OrdemBMFCollectionByIdCliente;
		#endregion

				
		#region OrdemBolsaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_OrdemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemBolsaCollection OrdemBolsaCollectionByIdCliente
		{
			get
			{
				if(this._OrdemBolsaCollectionByIdCliente == null)
				{
					this._OrdemBolsaCollectionByIdCliente = new OrdemBolsaCollection();
					this._OrdemBolsaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemBolsaCollectionByIdCliente", this._OrdemBolsaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._OrdemBolsaCollectionByIdCliente.Query.Where(this._OrdemBolsaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._OrdemBolsaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemBolsaCollectionByIdCliente.fks.Add(OrdemBolsaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._OrdemBolsaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemBolsaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("OrdemBolsaCollectionByIdCliente"); 
					this._OrdemBolsaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private OrdemBolsaCollection _OrdemBolsaCollectionByIdCliente;
		#endregion

				
		#region OrdemFundoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_OrdemFundo_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemFundoCollection OrdemFundoCollectionByIdCliente
		{
			get
			{
				if(this._OrdemFundoCollectionByIdCliente == null)
				{
					this._OrdemFundoCollectionByIdCliente = new OrdemFundoCollection();
					this._OrdemFundoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemFundoCollectionByIdCliente", this._OrdemFundoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._OrdemFundoCollectionByIdCliente.Query.Where(this._OrdemFundoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._OrdemFundoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemFundoCollectionByIdCliente.fks.Add(OrdemFundoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._OrdemFundoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemFundoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("OrdemFundoCollectionByIdCliente"); 
					this._OrdemFundoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private OrdemFundoCollection _OrdemFundoCollectionByIdCliente;
		#endregion

				
		#region OrdemRendaFixaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_OrdemRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemRendaFixaCollection OrdemRendaFixaCollectionByIdCliente
		{
			get
			{
				if(this._OrdemRendaFixaCollectionByIdCliente == null)
				{
					this._OrdemRendaFixaCollectionByIdCliente = new OrdemRendaFixaCollection();
					this._OrdemRendaFixaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemRendaFixaCollectionByIdCliente", this._OrdemRendaFixaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._OrdemRendaFixaCollectionByIdCliente.Query.Where(this._OrdemRendaFixaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._OrdemRendaFixaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemRendaFixaCollectionByIdCliente.fks.Add(OrdemRendaFixaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._OrdemRendaFixaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemRendaFixaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("OrdemRendaFixaCollectionByIdCliente"); 
					this._OrdemRendaFixaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private OrdemRendaFixaCollection _OrdemRendaFixaCollectionByIdCliente;
		#endregion

				
		#region PerfilCorretagemBMFCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PerfilCorretagemBMF_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilCorretagemBMFCollection PerfilCorretagemBMFCollectionByIdCliente
		{
			get
			{
				if(this._PerfilCorretagemBMFCollectionByIdCliente == null)
				{
					this._PerfilCorretagemBMFCollectionByIdCliente = new PerfilCorretagemBMFCollection();
					this._PerfilCorretagemBMFCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PerfilCorretagemBMFCollectionByIdCliente", this._PerfilCorretagemBMFCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PerfilCorretagemBMFCollectionByIdCliente.Query.Where(this._PerfilCorretagemBMFCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PerfilCorretagemBMFCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PerfilCorretagemBMFCollectionByIdCliente.fks.Add(PerfilCorretagemBMFMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PerfilCorretagemBMFCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PerfilCorretagemBMFCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PerfilCorretagemBMFCollectionByIdCliente"); 
					this._PerfilCorretagemBMFCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PerfilCorretagemBMFCollection _PerfilCorretagemBMFCollectionByIdCliente;
		#endregion

				
		#region PerfilCorretagemBolsaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PerfilCorretagemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilCorretagemBolsaCollection PerfilCorretagemBolsaCollectionByIdCliente
		{
			get
			{
				if(this._PerfilCorretagemBolsaCollectionByIdCliente == null)
				{
					this._PerfilCorretagemBolsaCollectionByIdCliente = new PerfilCorretagemBolsaCollection();
					this._PerfilCorretagemBolsaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PerfilCorretagemBolsaCollectionByIdCliente", this._PerfilCorretagemBolsaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PerfilCorretagemBolsaCollectionByIdCliente.Query.Where(this._PerfilCorretagemBolsaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PerfilCorretagemBolsaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PerfilCorretagemBolsaCollectionByIdCliente.fks.Add(PerfilCorretagemBolsaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PerfilCorretagemBolsaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PerfilCorretagemBolsaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PerfilCorretagemBolsaCollectionByIdCliente"); 
					this._PerfilCorretagemBolsaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PerfilCorretagemBolsaCollection _PerfilCorretagemBolsaCollectionByIdCliente;
		#endregion

		#region UpToUsuarioCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - Cliente_PermissaoCliente_FK1
		/// </summary>

		[XmlIgnore]
		public UsuarioCollection UpToUsuarioCollection
		{
			get
			{
				if(this._UpToUsuarioCollection == null)
				{
					this._UpToUsuarioCollection = new UsuarioCollection();
					this._UpToUsuarioCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToUsuarioCollection", this._UpToUsuarioCollection);
					this._UpToUsuarioCollection.ManyToManyClienteCollection(this.IdCliente);
				}

				return this._UpToUsuarioCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToUsuarioCollection != null) 
				{ 
					this.RemovePostSave("UpToUsuarioCollection"); 
					this._UpToUsuarioCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - Cliente_PermissaoCliente_FK1
		/// </summary>
		public void AssociateUsuarioCollection(Usuario entity)
		{
			if (this._PermissaoClienteCollection == null)
			{
				this._PermissaoClienteCollection = new PermissaoClienteCollection();
				this._PermissaoClienteCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("PermissaoClienteCollection", this._PermissaoClienteCollection);
			}

			PermissaoCliente obj = this._PermissaoClienteCollection.AddNew();
			obj.IdCliente = this.IdCliente;
			obj.IdUsuario = entity.IdUsuario;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - Cliente_PermissaoCliente_FK1
		/// </summary>
		public void DissociateUsuarioCollection(Usuario entity)
		{
			if (this._PermissaoClienteCollection == null)
			{
				this._PermissaoClienteCollection = new PermissaoClienteCollection();
				this._PermissaoClienteCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("PermissaoClienteCollection", this._PermissaoClienteCollection);
			}

			PermissaoCliente obj = this._PermissaoClienteCollection.AddNew();
			obj.IdCliente = this.IdCliente;
			obj.IdUsuario = entity.IdUsuario;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private UsuarioCollection _UpToUsuarioCollection;
		private PermissaoClienteCollection _PermissaoClienteCollection;
		#endregion

				
		#region PermissaoClienteCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PermissaoCliente_FK1
		/// </summary>

		[XmlIgnore]
		public PermissaoClienteCollection PermissaoClienteCollectionByIdCliente
		{
			get
			{
				if(this._PermissaoClienteCollectionByIdCliente == null)
				{
					this._PermissaoClienteCollectionByIdCliente = new PermissaoClienteCollection();
					this._PermissaoClienteCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PermissaoClienteCollectionByIdCliente", this._PermissaoClienteCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PermissaoClienteCollectionByIdCliente.Query.Where(this._PermissaoClienteCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PermissaoClienteCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PermissaoClienteCollectionByIdCliente.fks.Add(PermissaoClienteMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PermissaoClienteCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PermissaoClienteCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PermissaoClienteCollectionByIdCliente"); 
					this._PermissaoClienteCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PermissaoClienteCollection _PermissaoClienteCollectionByIdCliente;
		#endregion

				
		#region PermissaoOperacaoFundoCollectionByIdFundo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PermissaoOperacaoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public PermissaoOperacaoFundoCollection PermissaoOperacaoFundoCollectionByIdFundo
		{
			get
			{
				if(this._PermissaoOperacaoFundoCollectionByIdFundo == null)
				{
					this._PermissaoOperacaoFundoCollectionByIdFundo = new PermissaoOperacaoFundoCollection();
					this._PermissaoOperacaoFundoCollectionByIdFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PermissaoOperacaoFundoCollectionByIdFundo", this._PermissaoOperacaoFundoCollectionByIdFundo);
				
					if(this.IdCliente != null)
					{
						this._PermissaoOperacaoFundoCollectionByIdFundo.Query.Where(this._PermissaoOperacaoFundoCollectionByIdFundo.Query.IdFundo == this.IdCliente);
						this._PermissaoOperacaoFundoCollectionByIdFundo.Query.Load();

						// Auto-hookup Foreign Keys
						this._PermissaoOperacaoFundoCollectionByIdFundo.fks.Add(PermissaoOperacaoFundoMetadata.ColumnNames.IdFundo, this.IdCliente);
					}
				}

				return this._PermissaoOperacaoFundoCollectionByIdFundo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PermissaoOperacaoFundoCollectionByIdFundo != null) 
				{ 
					this.RemovePostSave("PermissaoOperacaoFundoCollectionByIdFundo"); 
					this._PermissaoOperacaoFundoCollectionByIdFundo = null;
					
				} 
			} 			
		}

		private PermissaoOperacaoFundoCollection _PermissaoOperacaoFundoCollectionByIdFundo;
		#endregion

				
		#region PosicaoBMFCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBMFCollection PosicaoBMFCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoBMFCollectionByIdCliente == null)
				{
					this._PosicaoBMFCollectionByIdCliente = new PosicaoBMFCollection();
					this._PosicaoBMFCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBMFCollectionByIdCliente", this._PosicaoBMFCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoBMFCollectionByIdCliente.Query.Where(this._PosicaoBMFCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoBMFCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBMFCollectionByIdCliente.fks.Add(PosicaoBMFMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoBMFCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBMFCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoBMFCollectionByIdCliente"); 
					this._PosicaoBMFCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoBMFCollection _PosicaoBMFCollectionByIdCliente;
		#endregion

				
		#region PosicaoBMFAberturaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoBMFAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBMFAberturaCollection PosicaoBMFAberturaCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoBMFAberturaCollectionByIdCliente == null)
				{
					this._PosicaoBMFAberturaCollectionByIdCliente = new PosicaoBMFAberturaCollection();
					this._PosicaoBMFAberturaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBMFAberturaCollectionByIdCliente", this._PosicaoBMFAberturaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoBMFAberturaCollectionByIdCliente.Query.Where(this._PosicaoBMFAberturaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoBMFAberturaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBMFAberturaCollectionByIdCliente.fks.Add(PosicaoBMFAberturaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoBMFAberturaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBMFAberturaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoBMFAberturaCollectionByIdCliente"); 
					this._PosicaoBMFAberturaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoBMFAberturaCollection _PosicaoBMFAberturaCollectionByIdCliente;
		#endregion

				
		#region PosicaoBMFHistoricoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoBMFHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBMFHistoricoCollection PosicaoBMFHistoricoCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoBMFHistoricoCollectionByIdCliente == null)
				{
					this._PosicaoBMFHistoricoCollectionByIdCliente = new PosicaoBMFHistoricoCollection();
					this._PosicaoBMFHistoricoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBMFHistoricoCollectionByIdCliente", this._PosicaoBMFHistoricoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoBMFHistoricoCollectionByIdCliente.Query.Where(this._PosicaoBMFHistoricoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoBMFHistoricoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBMFHistoricoCollectionByIdCliente.fks.Add(PosicaoBMFHistoricoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoBMFHistoricoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBMFHistoricoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoBMFHistoricoCollectionByIdCliente"); 
					this._PosicaoBMFHistoricoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoBMFHistoricoCollection _PosicaoBMFHistoricoCollectionByIdCliente;
		#endregion

				
		#region PosicaoBolsaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBolsaCollection PosicaoBolsaCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoBolsaCollectionByIdCliente == null)
				{
					this._PosicaoBolsaCollectionByIdCliente = new PosicaoBolsaCollection();
					this._PosicaoBolsaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBolsaCollectionByIdCliente", this._PosicaoBolsaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoBolsaCollectionByIdCliente.Query.Where(this._PosicaoBolsaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoBolsaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBolsaCollectionByIdCliente.fks.Add(PosicaoBolsaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoBolsaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBolsaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoBolsaCollectionByIdCliente"); 
					this._PosicaoBolsaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoBolsaCollection _PosicaoBolsaCollectionByIdCliente;
		#endregion

				
		#region PosicaoBolsaAberturaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBolsaAberturaCollection PosicaoBolsaAberturaCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoBolsaAberturaCollectionByIdCliente == null)
				{
					this._PosicaoBolsaAberturaCollectionByIdCliente = new PosicaoBolsaAberturaCollection();
					this._PosicaoBolsaAberturaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBolsaAberturaCollectionByIdCliente", this._PosicaoBolsaAberturaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoBolsaAberturaCollectionByIdCliente.Query.Where(this._PosicaoBolsaAberturaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoBolsaAberturaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBolsaAberturaCollectionByIdCliente.fks.Add(PosicaoBolsaAberturaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoBolsaAberturaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBolsaAberturaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoBolsaAberturaCollectionByIdCliente"); 
					this._PosicaoBolsaAberturaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoBolsaAberturaCollection _PosicaoBolsaAberturaCollectionByIdCliente;
		#endregion

				
		#region PosicaoBolsaDetalheCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoBolsaDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBolsaDetalheCollection PosicaoBolsaDetalheCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoBolsaDetalheCollectionByIdCliente == null)
				{
					this._PosicaoBolsaDetalheCollectionByIdCliente = new PosicaoBolsaDetalheCollection();
					this._PosicaoBolsaDetalheCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBolsaDetalheCollectionByIdCliente", this._PosicaoBolsaDetalheCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoBolsaDetalheCollectionByIdCliente.Query.Where(this._PosicaoBolsaDetalheCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoBolsaDetalheCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBolsaDetalheCollectionByIdCliente.fks.Add(PosicaoBolsaDetalheMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoBolsaDetalheCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBolsaDetalheCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoBolsaDetalheCollectionByIdCliente"); 
					this._PosicaoBolsaDetalheCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoBolsaDetalheCollection _PosicaoBolsaDetalheCollectionByIdCliente;
		#endregion

				
		#region PosicaoBolsaHistoricoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBolsaHistoricoCollection PosicaoBolsaHistoricoCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoBolsaHistoricoCollectionByIdCliente == null)
				{
					this._PosicaoBolsaHistoricoCollectionByIdCliente = new PosicaoBolsaHistoricoCollection();
					this._PosicaoBolsaHistoricoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBolsaHistoricoCollectionByIdCliente", this._PosicaoBolsaHistoricoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoBolsaHistoricoCollectionByIdCliente.Query.Where(this._PosicaoBolsaHistoricoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoBolsaHistoricoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBolsaHistoricoCollectionByIdCliente.fks.Add(PosicaoBolsaHistoricoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoBolsaHistoricoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBolsaHistoricoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoBolsaHistoricoCollectionByIdCliente"); 
					this._PosicaoBolsaHistoricoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoBolsaHistoricoCollection _PosicaoBolsaHistoricoCollectionByIdCliente;
		#endregion

				
		#region PosicaoEmprestimoBolsaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoEmprestimoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoEmprestimoBolsaCollection PosicaoEmprestimoBolsaCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoEmprestimoBolsaCollectionByIdCliente == null)
				{
					this._PosicaoEmprestimoBolsaCollectionByIdCliente = new PosicaoEmprestimoBolsaCollection();
					this._PosicaoEmprestimoBolsaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoEmprestimoBolsaCollectionByIdCliente", this._PosicaoEmprestimoBolsaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoEmprestimoBolsaCollectionByIdCliente.Query.Where(this._PosicaoEmprestimoBolsaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoEmprestimoBolsaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoEmprestimoBolsaCollectionByIdCliente.fks.Add(PosicaoEmprestimoBolsaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoEmprestimoBolsaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoEmprestimoBolsaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoEmprestimoBolsaCollectionByIdCliente"); 
					this._PosicaoEmprestimoBolsaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoEmprestimoBolsaCollection _PosicaoEmprestimoBolsaCollectionByIdCliente;
		#endregion

				
		#region PosicaoEmprestimoBolsaAberturaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoEmprestimoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoEmprestimoBolsaAberturaCollection PosicaoEmprestimoBolsaAberturaCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoEmprestimoBolsaAberturaCollectionByIdCliente == null)
				{
					this._PosicaoEmprestimoBolsaAberturaCollectionByIdCliente = new PosicaoEmprestimoBolsaAberturaCollection();
					this._PosicaoEmprestimoBolsaAberturaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoEmprestimoBolsaAberturaCollectionByIdCliente", this._PosicaoEmprestimoBolsaAberturaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoEmprestimoBolsaAberturaCollectionByIdCliente.Query.Where(this._PosicaoEmprestimoBolsaAberturaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoEmprestimoBolsaAberturaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoEmprestimoBolsaAberturaCollectionByIdCliente.fks.Add(PosicaoEmprestimoBolsaAberturaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoEmprestimoBolsaAberturaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoEmprestimoBolsaAberturaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoEmprestimoBolsaAberturaCollectionByIdCliente"); 
					this._PosicaoEmprestimoBolsaAberturaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoEmprestimoBolsaAberturaCollection _PosicaoEmprestimoBolsaAberturaCollectionByIdCliente;
		#endregion

				
		#region PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoEmprestimoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoEmprestimoBolsaHistoricoCollection PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente == null)
				{
					this._PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente = new PosicaoEmprestimoBolsaHistoricoCollection();
					this._PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente", this._PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente.Query.Where(this._PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente.fks.Add(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente"); 
					this._PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoEmprestimoBolsaHistoricoCollection _PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente;
		#endregion

				
		#region PosicaoFundoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoFundoCollection PosicaoFundoCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoFundoCollectionByIdCliente == null)
				{
					this._PosicaoFundoCollectionByIdCliente = new PosicaoFundoCollection();
					this._PosicaoFundoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoFundoCollectionByIdCliente", this._PosicaoFundoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoFundoCollectionByIdCliente.Query.Where(this._PosicaoFundoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoFundoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoFundoCollectionByIdCliente.fks.Add(PosicaoFundoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoFundoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoFundoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoFundoCollectionByIdCliente"); 
					this._PosicaoFundoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoFundoCollection _PosicaoFundoCollectionByIdCliente;
		#endregion

				
		#region PosicaoFundoAberturaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoFundoAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoFundoAberturaCollection PosicaoFundoAberturaCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoFundoAberturaCollectionByIdCliente == null)
				{
					this._PosicaoFundoAberturaCollectionByIdCliente = new PosicaoFundoAberturaCollection();
					this._PosicaoFundoAberturaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoFundoAberturaCollectionByIdCliente", this._PosicaoFundoAberturaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoFundoAberturaCollectionByIdCliente.Query.Where(this._PosicaoFundoAberturaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoFundoAberturaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoFundoAberturaCollectionByIdCliente.fks.Add(PosicaoFundoAberturaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoFundoAberturaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoFundoAberturaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoFundoAberturaCollectionByIdCliente"); 
					this._PosicaoFundoAberturaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoFundoAberturaCollection _PosicaoFundoAberturaCollectionByIdCliente;
		#endregion

				
		#region PosicaoFundoHistoricoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoFundoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoFundoHistoricoCollection PosicaoFundoHistoricoCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoFundoHistoricoCollectionByIdCliente == null)
				{
					this._PosicaoFundoHistoricoCollectionByIdCliente = new PosicaoFundoHistoricoCollection();
					this._PosicaoFundoHistoricoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoFundoHistoricoCollectionByIdCliente", this._PosicaoFundoHistoricoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoFundoHistoricoCollectionByIdCliente.Query.Where(this._PosicaoFundoHistoricoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoFundoHistoricoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoFundoHistoricoCollectionByIdCliente.fks.Add(PosicaoFundoHistoricoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoFundoHistoricoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoFundoHistoricoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoFundoHistoricoCollectionByIdCliente"); 
					this._PosicaoFundoHistoricoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoFundoHistoricoCollection _PosicaoFundoHistoricoCollectionByIdCliente;
		#endregion

				
		#region PosicaoRendaFixaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoRendaFixaCollection PosicaoRendaFixaCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoRendaFixaCollectionByIdCliente == null)
				{
					this._PosicaoRendaFixaCollectionByIdCliente = new PosicaoRendaFixaCollection();
					this._PosicaoRendaFixaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoRendaFixaCollectionByIdCliente", this._PosicaoRendaFixaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoRendaFixaCollectionByIdCliente.Query.Where(this._PosicaoRendaFixaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoRendaFixaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoRendaFixaCollectionByIdCliente.fks.Add(PosicaoRendaFixaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoRendaFixaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoRendaFixaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoRendaFixaCollectionByIdCliente"); 
					this._PosicaoRendaFixaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoRendaFixaCollection _PosicaoRendaFixaCollectionByIdCliente;
		#endregion

				
		#region PosicaoRendaFixaAberturaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoRendaFixaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoRendaFixaAberturaCollection PosicaoRendaFixaAberturaCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoRendaFixaAberturaCollectionByIdCliente == null)
				{
					this._PosicaoRendaFixaAberturaCollectionByIdCliente = new PosicaoRendaFixaAberturaCollection();
					this._PosicaoRendaFixaAberturaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoRendaFixaAberturaCollectionByIdCliente", this._PosicaoRendaFixaAberturaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoRendaFixaAberturaCollectionByIdCliente.Query.Where(this._PosicaoRendaFixaAberturaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoRendaFixaAberturaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoRendaFixaAberturaCollectionByIdCliente.fks.Add(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoRendaFixaAberturaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoRendaFixaAberturaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoRendaFixaAberturaCollectionByIdCliente"); 
					this._PosicaoRendaFixaAberturaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoRendaFixaAberturaCollection _PosicaoRendaFixaAberturaCollectionByIdCliente;
		#endregion

				
		#region PosicaoRendaFixaDetalheCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoRendaFixaDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoRendaFixaDetalheCollection PosicaoRendaFixaDetalheCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoRendaFixaDetalheCollectionByIdCliente == null)
				{
					this._PosicaoRendaFixaDetalheCollectionByIdCliente = new PosicaoRendaFixaDetalheCollection();
					this._PosicaoRendaFixaDetalheCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoRendaFixaDetalheCollectionByIdCliente", this._PosicaoRendaFixaDetalheCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoRendaFixaDetalheCollectionByIdCliente.Query.Where(this._PosicaoRendaFixaDetalheCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoRendaFixaDetalheCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoRendaFixaDetalheCollectionByIdCliente.fks.Add(PosicaoRendaFixaDetalheMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoRendaFixaDetalheCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoRendaFixaDetalheCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoRendaFixaDetalheCollectionByIdCliente"); 
					this._PosicaoRendaFixaDetalheCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoRendaFixaDetalheCollection _PosicaoRendaFixaDetalheCollectionByIdCliente;
		#endregion

				
		#region PosicaoRendaFixaHistoricoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoRendaFixaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoRendaFixaHistoricoCollection PosicaoRendaFixaHistoricoCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoRendaFixaHistoricoCollectionByIdCliente == null)
				{
					this._PosicaoRendaFixaHistoricoCollectionByIdCliente = new PosicaoRendaFixaHistoricoCollection();
					this._PosicaoRendaFixaHistoricoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoRendaFixaHistoricoCollectionByIdCliente", this._PosicaoRendaFixaHistoricoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoRendaFixaHistoricoCollectionByIdCliente.Query.Where(this._PosicaoRendaFixaHistoricoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoRendaFixaHistoricoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoRendaFixaHistoricoCollectionByIdCliente.fks.Add(PosicaoRendaFixaHistoricoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoRendaFixaHistoricoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoRendaFixaHistoricoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoRendaFixaHistoricoCollectionByIdCliente"); 
					this._PosicaoRendaFixaHistoricoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoRendaFixaHistoricoCollection _PosicaoRendaFixaHistoricoCollectionByIdCliente;
		#endregion

				
		#region PosicaoSwapAberturaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoSwapAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapAberturaCollection PosicaoSwapAberturaCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoSwapAberturaCollectionByIdCliente == null)
				{
					this._PosicaoSwapAberturaCollectionByIdCliente = new PosicaoSwapAberturaCollection();
					this._PosicaoSwapAberturaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapAberturaCollectionByIdCliente", this._PosicaoSwapAberturaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoSwapAberturaCollectionByIdCliente.Query.Where(this._PosicaoSwapAberturaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoSwapAberturaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapAberturaCollectionByIdCliente.fks.Add(PosicaoSwapAberturaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoSwapAberturaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapAberturaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoSwapAberturaCollectionByIdCliente"); 
					this._PosicaoSwapAberturaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoSwapAberturaCollection _PosicaoSwapAberturaCollectionByIdCliente;
		#endregion

				
		#region PosicaoSwapHistoricoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoSwapHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapHistoricoCollection PosicaoSwapHistoricoCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoSwapHistoricoCollectionByIdCliente == null)
				{
					this._PosicaoSwapHistoricoCollectionByIdCliente = new PosicaoSwapHistoricoCollection();
					this._PosicaoSwapHistoricoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapHistoricoCollectionByIdCliente", this._PosicaoSwapHistoricoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoSwapHistoricoCollectionByIdCliente.Query.Where(this._PosicaoSwapHistoricoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoSwapHistoricoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapHistoricoCollectionByIdCliente.fks.Add(PosicaoSwapHistoricoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoSwapHistoricoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapHistoricoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoSwapHistoricoCollectionByIdCliente"); 
					this._PosicaoSwapHistoricoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoSwapHistoricoCollection _PosicaoSwapHistoricoCollectionByIdCliente;
		#endregion

				
		#region PosicaoTermoBolsaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoTermoBolsaCollection PosicaoTermoBolsaCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoTermoBolsaCollectionByIdCliente == null)
				{
					this._PosicaoTermoBolsaCollectionByIdCliente = new PosicaoTermoBolsaCollection();
					this._PosicaoTermoBolsaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoTermoBolsaCollectionByIdCliente", this._PosicaoTermoBolsaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoTermoBolsaCollectionByIdCliente.Query.Where(this._PosicaoTermoBolsaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoTermoBolsaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoTermoBolsaCollectionByIdCliente.fks.Add(PosicaoTermoBolsaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoTermoBolsaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoTermoBolsaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoTermoBolsaCollectionByIdCliente"); 
					this._PosicaoTermoBolsaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoTermoBolsaCollection _PosicaoTermoBolsaCollectionByIdCliente;
		#endregion

				
		#region PosicaoTermoBolsaAberturaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoTermoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoTermoBolsaAberturaCollection PosicaoTermoBolsaAberturaCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoTermoBolsaAberturaCollectionByIdCliente == null)
				{
					this._PosicaoTermoBolsaAberturaCollectionByIdCliente = new PosicaoTermoBolsaAberturaCollection();
					this._PosicaoTermoBolsaAberturaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoTermoBolsaAberturaCollectionByIdCliente", this._PosicaoTermoBolsaAberturaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoTermoBolsaAberturaCollectionByIdCliente.Query.Where(this._PosicaoTermoBolsaAberturaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoTermoBolsaAberturaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoTermoBolsaAberturaCollectionByIdCliente.fks.Add(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoTermoBolsaAberturaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoTermoBolsaAberturaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoTermoBolsaAberturaCollectionByIdCliente"); 
					this._PosicaoTermoBolsaAberturaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoTermoBolsaAberturaCollection _PosicaoTermoBolsaAberturaCollectionByIdCliente;
		#endregion

				
		#region PosicaoTermoBolsaHistoricoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PosicaoTermoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoTermoBolsaHistoricoCollection PosicaoTermoBolsaHistoricoCollectionByIdCliente
		{
			get
			{
				if(this._PosicaoTermoBolsaHistoricoCollectionByIdCliente == null)
				{
					this._PosicaoTermoBolsaHistoricoCollectionByIdCliente = new PosicaoTermoBolsaHistoricoCollection();
					this._PosicaoTermoBolsaHistoricoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoTermoBolsaHistoricoCollectionByIdCliente", this._PosicaoTermoBolsaHistoricoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PosicaoTermoBolsaHistoricoCollectionByIdCliente.Query.Where(this._PosicaoTermoBolsaHistoricoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PosicaoTermoBolsaHistoricoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoTermoBolsaHistoricoCollectionByIdCliente.fks.Add(PosicaoTermoBolsaHistoricoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PosicaoTermoBolsaHistoricoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoTermoBolsaHistoricoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PosicaoTermoBolsaHistoricoCollectionByIdCliente"); 
					this._PosicaoTermoBolsaHistoricoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PosicaoTermoBolsaHistoricoCollection _PosicaoTermoBolsaHistoricoCollectionByIdCliente;
		#endregion

				
		#region PrejuizoFundoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PrejuizoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public PrejuizoFundoCollection PrejuizoFundoCollectionByIdCliente
		{
			get
			{
				if(this._PrejuizoFundoCollectionByIdCliente == null)
				{
					this._PrejuizoFundoCollectionByIdCliente = new PrejuizoFundoCollection();
					this._PrejuizoFundoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PrejuizoFundoCollectionByIdCliente", this._PrejuizoFundoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PrejuizoFundoCollectionByIdCliente.Query.Where(this._PrejuizoFundoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PrejuizoFundoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PrejuizoFundoCollectionByIdCliente.fks.Add(PrejuizoFundoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PrejuizoFundoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PrejuizoFundoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PrejuizoFundoCollectionByIdCliente"); 
					this._PrejuizoFundoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PrejuizoFundoCollection _PrejuizoFundoCollectionByIdCliente;
		#endregion

				
		#region PrejuizoFundoHistoricoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_PrejuizoFundoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PrejuizoFundoHistoricoCollection PrejuizoFundoHistoricoCollectionByIdCliente
		{
			get
			{
				if(this._PrejuizoFundoHistoricoCollectionByIdCliente == null)
				{
					this._PrejuizoFundoHistoricoCollectionByIdCliente = new PrejuizoFundoHistoricoCollection();
					this._PrejuizoFundoHistoricoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PrejuizoFundoHistoricoCollectionByIdCliente", this._PrejuizoFundoHistoricoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._PrejuizoFundoHistoricoCollectionByIdCliente.Query.Where(this._PrejuizoFundoHistoricoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._PrejuizoFundoHistoricoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._PrejuizoFundoHistoricoCollectionByIdCliente.fks.Add(PrejuizoFundoHistoricoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._PrejuizoFundoHistoricoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PrejuizoFundoHistoricoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("PrejuizoFundoHistoricoCollectionByIdCliente"); 
					this._PrejuizoFundoHistoricoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private PrejuizoFundoHistoricoCollection _PrejuizoFundoHistoricoCollectionByIdCliente;
		#endregion

				
		#region ProventoBolsaClienteCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_ProventoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public ProventoBolsaClienteCollection ProventoBolsaClienteCollectionByIdCliente
		{
			get
			{
				if(this._ProventoBolsaClienteCollectionByIdCliente == null)
				{
					this._ProventoBolsaClienteCollectionByIdCliente = new ProventoBolsaClienteCollection();
					this._ProventoBolsaClienteCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ProventoBolsaClienteCollectionByIdCliente", this._ProventoBolsaClienteCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._ProventoBolsaClienteCollectionByIdCliente.Query.Where(this._ProventoBolsaClienteCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._ProventoBolsaClienteCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._ProventoBolsaClienteCollectionByIdCliente.fks.Add(ProventoBolsaClienteMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._ProventoBolsaClienteCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ProventoBolsaClienteCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("ProventoBolsaClienteCollectionByIdCliente"); 
					this._ProventoBolsaClienteCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private ProventoBolsaClienteCollection _ProventoBolsaClienteCollectionByIdCliente;
		#endregion

				
		#region SaldoCaixaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_SaldoCaixa_FK1
		/// </summary>

		[XmlIgnore]
		public SaldoCaixaCollection SaldoCaixaCollectionByIdCliente
		{
			get
			{
				if(this._SaldoCaixaCollectionByIdCliente == null)
				{
					this._SaldoCaixaCollectionByIdCliente = new SaldoCaixaCollection();
					this._SaldoCaixaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("SaldoCaixaCollectionByIdCliente", this._SaldoCaixaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._SaldoCaixaCollectionByIdCliente.Query.Where(this._SaldoCaixaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._SaldoCaixaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._SaldoCaixaCollectionByIdCliente.fks.Add(SaldoCaixaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._SaldoCaixaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._SaldoCaixaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("SaldoCaixaCollectionByIdCliente"); 
					this._SaldoCaixaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private SaldoCaixaCollection _SaldoCaixaCollectionByIdCliente;
		#endregion

				
		#region TabelaCONRCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_TabelaCONR_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaCONRCollection TabelaCONRCollectionByIdCliente
		{
			get
			{
				if(this._TabelaCONRCollectionByIdCliente == null)
				{
					this._TabelaCONRCollectionByIdCliente = new TabelaCONRCollection();
					this._TabelaCONRCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaCONRCollectionByIdCliente", this._TabelaCONRCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._TabelaCONRCollectionByIdCliente.Query.Where(this._TabelaCONRCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._TabelaCONRCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaCONRCollectionByIdCliente.fks.Add(TabelaCONRMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._TabelaCONRCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaCONRCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("TabelaCONRCollectionByIdCliente"); 
					this._TabelaCONRCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private TabelaCONRCollection _TabelaCONRCollectionByIdCliente;
		#endregion

				
		#region TabelaCustosRendaFixaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_TabelaCustosRendaFixa_Cliente
		/// </summary>

		[XmlIgnore]
		public TabelaCustosRendaFixaCollection TabelaCustosRendaFixaCollectionByIdCliente
		{
			get
			{
				if(this._TabelaCustosRendaFixaCollectionByIdCliente == null)
				{
					this._TabelaCustosRendaFixaCollectionByIdCliente = new TabelaCustosRendaFixaCollection();
					this._TabelaCustosRendaFixaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaCustosRendaFixaCollectionByIdCliente", this._TabelaCustosRendaFixaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._TabelaCustosRendaFixaCollectionByIdCliente.Query.Where(this._TabelaCustosRendaFixaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._TabelaCustosRendaFixaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaCustosRendaFixaCollectionByIdCliente.fks.Add(TabelaCustosRendaFixaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._TabelaCustosRendaFixaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaCustosRendaFixaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("TabelaCustosRendaFixaCollectionByIdCliente"); 
					this._TabelaCustosRendaFixaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private TabelaCustosRendaFixaCollection _TabelaCustosRendaFixaCollectionByIdCliente;
		#endregion
				
		#region TabelaInterfaceClienteCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_TabelaInterfaceCliente_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaInterfaceClienteCollection TabelaInterfaceClienteCollectionByIdCliente
		{
			get
			{
				if(this._TabelaInterfaceClienteCollectionByIdCliente == null)
				{
					this._TabelaInterfaceClienteCollectionByIdCliente = new TabelaInterfaceClienteCollection();
					this._TabelaInterfaceClienteCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaInterfaceClienteCollectionByIdCliente", this._TabelaInterfaceClienteCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._TabelaInterfaceClienteCollectionByIdCliente.Query.Where(this._TabelaInterfaceClienteCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._TabelaInterfaceClienteCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaInterfaceClienteCollectionByIdCliente.fks.Add(TabelaInterfaceClienteMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._TabelaInterfaceClienteCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaInterfaceClienteCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("TabelaInterfaceClienteCollectionByIdCliente"); 
					this._TabelaInterfaceClienteCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private TabelaInterfaceClienteCollection _TabelaInterfaceClienteCollectionByIdCliente;
		#endregion

		#region UpToGrupoProcessamentoCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - FK_TabelaProcessamento_Cliente
		/// </summary>

		[XmlIgnore]
		public GrupoProcessamentoCollection UpToGrupoProcessamentoCollection
		{
			get
			{
				if(this._UpToGrupoProcessamentoCollection == null)
				{
					this._UpToGrupoProcessamentoCollection = new GrupoProcessamentoCollection();
					this._UpToGrupoProcessamentoCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToGrupoProcessamentoCollection", this._UpToGrupoProcessamentoCollection);
					this._UpToGrupoProcessamentoCollection.ManyToManyClienteCollection(this.IdCliente);
				}

				return this._UpToGrupoProcessamentoCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToGrupoProcessamentoCollection != null) 
				{ 
					this.RemovePostSave("UpToGrupoProcessamentoCollection"); 
					this._UpToGrupoProcessamentoCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - FK_TabelaProcessamento_Cliente
		/// </summary>
		public void AssociateGrupoProcessamentoCollection(GrupoProcessamento entity)
		{
			if (this._TabelaProcessamentoCollection == null)
			{
				this._TabelaProcessamentoCollection = new TabelaProcessamentoCollection();
				this._TabelaProcessamentoCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("TabelaProcessamentoCollection", this._TabelaProcessamentoCollection);
			}

			TabelaProcessamento obj = this._TabelaProcessamentoCollection.AddNew();
			obj.IdCliente = this.IdCliente;
			obj.IdGrupoProcessamento = entity.IdGrupoProcessamento;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - FK_TabelaProcessamento_Cliente
		/// </summary>
		public void DissociateGrupoProcessamentoCollection(GrupoProcessamento entity)
		{
			if (this._TabelaProcessamentoCollection == null)
			{
				this._TabelaProcessamentoCollection = new TabelaProcessamentoCollection();
				this._TabelaProcessamentoCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("TabelaProcessamentoCollection", this._TabelaProcessamentoCollection);
			}

			TabelaProcessamento obj = this._TabelaProcessamentoCollection.AddNew();
			obj.IdCliente = this.IdCliente;
			obj.IdGrupoProcessamento = entity.IdGrupoProcessamento;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private GrupoProcessamentoCollection _UpToGrupoProcessamentoCollection;
		private TabelaProcessamentoCollection _TabelaProcessamentoCollection;
		#endregion

				
		#region TabelaProcessamentoCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_TabelaProcessamento_Cliente
		/// </summary>

		[XmlIgnore]
		public TabelaProcessamentoCollection TabelaProcessamentoCollectionByIdCliente
		{
			get
			{
				if(this._TabelaProcessamentoCollectionByIdCliente == null)
				{
					this._TabelaProcessamentoCollectionByIdCliente = new TabelaProcessamentoCollection();
					this._TabelaProcessamentoCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaProcessamentoCollectionByIdCliente", this._TabelaProcessamentoCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._TabelaProcessamentoCollectionByIdCliente.Query.Where(this._TabelaProcessamentoCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._TabelaProcessamentoCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaProcessamentoCollectionByIdCliente.fks.Add(TabelaProcessamentoMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._TabelaProcessamentoCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaProcessamentoCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("TabelaProcessamentoCollectionByIdCliente"); 
					this._TabelaProcessamentoCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private TabelaProcessamentoCollection _TabelaProcessamentoCollectionByIdCliente;
		#endregion

				
		#region TabelaRebateCorretagemCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_TabelaRebateCorretagem_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaRebateCorretagemCollection TabelaRebateCorretagemCollectionByIdCliente
		{
			get
			{
				if(this._TabelaRebateCorretagemCollectionByIdCliente == null)
				{
					this._TabelaRebateCorretagemCollectionByIdCliente = new TabelaRebateCorretagemCollection();
					this._TabelaRebateCorretagemCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaRebateCorretagemCollectionByIdCliente", this._TabelaRebateCorretagemCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._TabelaRebateCorretagemCollectionByIdCliente.Query.Where(this._TabelaRebateCorretagemCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._TabelaRebateCorretagemCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaRebateCorretagemCollectionByIdCliente.fks.Add(TabelaRebateCorretagemMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._TabelaRebateCorretagemCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaRebateCorretagemCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("TabelaRebateCorretagemCollectionByIdCliente"); 
					this._TabelaRebateCorretagemCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private TabelaRebateCorretagemCollection _TabelaRebateCorretagemCollectionByIdCliente;
		#endregion

				
		#region TabelaTaxaCustodiaBolsaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_TabelaTaxaCustodiaBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaCustodiaBolsaCollection TabelaTaxaCustodiaBolsaCollectionByIdCliente
		{
			get
			{
				if(this._TabelaTaxaCustodiaBolsaCollectionByIdCliente == null)
				{
					this._TabelaTaxaCustodiaBolsaCollectionByIdCliente = new TabelaTaxaCustodiaBolsaCollection();
					this._TabelaTaxaCustodiaBolsaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaTaxaCustodiaBolsaCollectionByIdCliente", this._TabelaTaxaCustodiaBolsaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._TabelaTaxaCustodiaBolsaCollectionByIdCliente.Query.Where(this._TabelaTaxaCustodiaBolsaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._TabelaTaxaCustodiaBolsaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaTaxaCustodiaBolsaCollectionByIdCliente.fks.Add(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._TabelaTaxaCustodiaBolsaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaTaxaCustodiaBolsaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("TabelaTaxaCustodiaBolsaCollectionByIdCliente"); 
					this._TabelaTaxaCustodiaBolsaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private TabelaTaxaCustodiaBolsaCollection _TabelaTaxaCustodiaBolsaCollectionByIdCliente;
		#endregion

				
		#region TransferenciaBMFCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_TransferenciaBMF_FK1
		/// </summary>

		[XmlIgnore]
		public TransferenciaBMFCollection TransferenciaBMFCollectionByIdCliente
		{
			get
			{
				if(this._TransferenciaBMFCollectionByIdCliente == null)
				{
					this._TransferenciaBMFCollectionByIdCliente = new TransferenciaBMFCollection();
					this._TransferenciaBMFCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaBMFCollectionByIdCliente", this._TransferenciaBMFCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._TransferenciaBMFCollectionByIdCliente.Query.Where(this._TransferenciaBMFCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._TransferenciaBMFCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaBMFCollectionByIdCliente.fks.Add(TransferenciaBMFMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._TransferenciaBMFCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaBMFCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("TransferenciaBMFCollectionByIdCliente"); 
					this._TransferenciaBMFCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private TransferenciaBMFCollection _TransferenciaBMFCollectionByIdCliente;
		#endregion

				
		#region TransferenciaBolsaCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_TransferenciaBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public TransferenciaBolsaCollection TransferenciaBolsaCollectionByIdCliente
		{
			get
			{
				if(this._TransferenciaBolsaCollectionByIdCliente == null)
				{
					this._TransferenciaBolsaCollectionByIdCliente = new TransferenciaBolsaCollection();
					this._TransferenciaBolsaCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaBolsaCollectionByIdCliente", this._TransferenciaBolsaCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._TransferenciaBolsaCollectionByIdCliente.Query.Where(this._TransferenciaBolsaCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._TransferenciaBolsaCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaBolsaCollectionByIdCliente.fks.Add(TransferenciaBolsaMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._TransferenciaBolsaCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaBolsaCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("TransferenciaBolsaCollectionByIdCliente"); 
					this._TransferenciaBolsaCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private TransferenciaBolsaCollection _TransferenciaBolsaCollectionByIdCliente;
		#endregion

				
		#region UsuarioCollectionByIdCliente - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cliente_Usuario_FK1
		/// </summary>

		[XmlIgnore]
		public UsuarioCollection UsuarioCollectionByIdCliente
		{
			get
			{
				if(this._UsuarioCollectionByIdCliente == null)
				{
					this._UsuarioCollectionByIdCliente = new UsuarioCollection();
					this._UsuarioCollectionByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UsuarioCollectionByIdCliente", this._UsuarioCollectionByIdCliente);
				
					if(this.IdCliente != null)
					{
						this._UsuarioCollectionByIdCliente.Query.Where(this._UsuarioCollectionByIdCliente.Query.IdCliente == this.IdCliente);
						this._UsuarioCollectionByIdCliente.Query.Load();

						// Auto-hookup Foreign Keys
						this._UsuarioCollectionByIdCliente.fks.Add(UsuarioMetadata.ColumnNames.IdCliente, this.IdCliente);
					}
				}

				return this._UsuarioCollectionByIdCliente;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UsuarioCollectionByIdCliente != null) 
				{ 
					this.RemovePostSave("UsuarioCollectionByIdCliente"); 
					this._UsuarioCollectionByIdCliente = null;
					
				} 
			} 			
		}

		private UsuarioCollection _UsuarioCollectionByIdCliente;
		#endregion

				
		#region UpToContabPlanoByIdPlano - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabPlano_Cliente_FK1
		/// </summary>

		[XmlIgnore]
		public ContabPlano UpToContabPlanoByIdPlano
		{
			get
			{
				if(this._UpToContabPlanoByIdPlano == null
					&& IdPlano != null					)
				{
					this._UpToContabPlanoByIdPlano = new ContabPlano();
					this._UpToContabPlanoByIdPlano.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabPlanoByIdPlano", this._UpToContabPlanoByIdPlano);
					this._UpToContabPlanoByIdPlano.Query.Where(this._UpToContabPlanoByIdPlano.Query.IdPlano == this.IdPlano);
					this._UpToContabPlanoByIdPlano.Query.Load();
				}

				return this._UpToContabPlanoByIdPlano;
			}
			
			set
			{
				this.RemovePreSave("UpToContabPlanoByIdPlano");
				

				if(value == null)
				{
					this.IdPlano = null;
					this._UpToContabPlanoByIdPlano = null;
				}
				else
				{
					this.IdPlano = value.IdPlano;
					this._UpToContabPlanoByIdPlano = value;
					this.SetPreSave("UpToContabPlanoByIdPlano", this._UpToContabPlanoByIdPlano);
				}
				
			}
		}
		#endregion
		

				
		#region UpToGrupoAfinidadeByIdGrupoAfinidade - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - GrupoAfinidade_Cliente_FK1
		/// </summary>

		[XmlIgnore]
		public GrupoAfinidade UpToGrupoAfinidadeByIdGrupoAfinidade
		{
			get
			{
				if(this._UpToGrupoAfinidadeByIdGrupoAfinidade == null
					&& IdGrupoAfinidade != null					)
				{
					this._UpToGrupoAfinidadeByIdGrupoAfinidade = new GrupoAfinidade();
					this._UpToGrupoAfinidadeByIdGrupoAfinidade.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToGrupoAfinidadeByIdGrupoAfinidade", this._UpToGrupoAfinidadeByIdGrupoAfinidade);
					this._UpToGrupoAfinidadeByIdGrupoAfinidade.Query.Where(this._UpToGrupoAfinidadeByIdGrupoAfinidade.Query.IdGrupo == this.IdGrupoAfinidade);
					this._UpToGrupoAfinidadeByIdGrupoAfinidade.Query.Load();
				}

				return this._UpToGrupoAfinidadeByIdGrupoAfinidade;
			}
			
			set
			{
				this.RemovePreSave("UpToGrupoAfinidadeByIdGrupoAfinidade");
				

				if(value == null)
				{
					this.IdGrupoAfinidade = null;
					this._UpToGrupoAfinidadeByIdGrupoAfinidade = null;
				}
				else
				{
					this.IdGrupoAfinidade = value.IdGrupo;
					this._UpToGrupoAfinidadeByIdGrupoAfinidade = value;
					this.SetPreSave("UpToGrupoAfinidadeByIdGrupoAfinidade", this._UpToGrupoAfinidadeByIdGrupoAfinidade);
				}
				
			}
		}
		#endregion
		

				
		#region UpToGrupoProcessamentoByIdGrupoProcessamento - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - GrupoProcessamento_Cliente_FK1
		/// </summary>

		[XmlIgnore]
		public GrupoProcessamento UpToGrupoProcessamentoByIdGrupoProcessamento
		{
			get
			{
				if(this._UpToGrupoProcessamentoByIdGrupoProcessamento == null
					&& IdGrupoProcessamento != null					)
				{
					this._UpToGrupoProcessamentoByIdGrupoProcessamento = new GrupoProcessamento();
					this._UpToGrupoProcessamentoByIdGrupoProcessamento.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToGrupoProcessamentoByIdGrupoProcessamento", this._UpToGrupoProcessamentoByIdGrupoProcessamento);
					this._UpToGrupoProcessamentoByIdGrupoProcessamento.Query.Where(this._UpToGrupoProcessamentoByIdGrupoProcessamento.Query.IdGrupoProcessamento == this.IdGrupoProcessamento);
					this._UpToGrupoProcessamentoByIdGrupoProcessamento.Query.Load();
				}

				return this._UpToGrupoProcessamentoByIdGrupoProcessamento;
			}
			
			set
			{
				this.RemovePreSave("UpToGrupoProcessamentoByIdGrupoProcessamento");
				

				if(value == null)
				{
					this.IdGrupoProcessamento = null;
					this._UpToGrupoProcessamentoByIdGrupoProcessamento = null;
				}
				else
				{
					this.IdGrupoProcessamento = value.IdGrupoProcessamento;
					this._UpToGrupoProcessamentoByIdGrupoProcessamento = value;
					this.SetPreSave("UpToGrupoProcessamentoByIdGrupoProcessamento", this._UpToGrupoProcessamentoByIdGrupoProcessamento);
				}
				
			}
		}
		#endregion
		

				
		#region UpToIndiceByIdIndiceAbertura - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_Indice_FK1
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndiceAbertura
		{
			get
			{
				if(this._UpToIndiceByIdIndiceAbertura == null
					&& IdIndiceAbertura != null					)
				{
					this._UpToIndiceByIdIndiceAbertura = new Indice();
					this._UpToIndiceByIdIndiceAbertura.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndiceAbertura", this._UpToIndiceByIdIndiceAbertura);
					this._UpToIndiceByIdIndiceAbertura.Query.Where(this._UpToIndiceByIdIndiceAbertura.Query.IdIndice == this.IdIndiceAbertura);
					this._UpToIndiceByIdIndiceAbertura.Query.Load();
				}

				return this._UpToIndiceByIdIndiceAbertura;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndiceAbertura");
				

				if(value == null)
				{
					this.IdIndiceAbertura = null;
					this._UpToIndiceByIdIndiceAbertura = null;
				}
				else
				{
					this.IdIndiceAbertura = value.IdIndice;
					this._UpToIndiceByIdIndiceAbertura = value;
					this.SetPreSave("UpToIndiceByIdIndiceAbertura", this._UpToIndiceByIdIndiceAbertura);
				}
				
			}
		}
		#endregion
		

				
		#region UpToMoedaByIdMoeda - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Moeda_Cliente_FK1
		/// </summary>

		[XmlIgnore]
		public Moeda UpToMoedaByIdMoeda
		{
			get
			{
				if(this._UpToMoedaByIdMoeda == null
					&& IdMoeda != null					)
				{
					this._UpToMoedaByIdMoeda = new Moeda();
					this._UpToMoedaByIdMoeda.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToMoedaByIdMoeda", this._UpToMoedaByIdMoeda);
					this._UpToMoedaByIdMoeda.Query.Where(this._UpToMoedaByIdMoeda.Query.IdMoeda == this.IdMoeda);
					this._UpToMoedaByIdMoeda.Query.Load();
				}

				return this._UpToMoedaByIdMoeda;
			}
			
			set
			{
				this.RemovePreSave("UpToMoedaByIdMoeda");
				

				if(value == null)
				{
					this.IdMoeda = null;
					this._UpToMoedaByIdMoeda = null;
				}
				else
				{
					this.IdMoeda = value.IdMoeda;
					this._UpToMoedaByIdMoeda = value;
					this.SetPreSave("UpToMoedaByIdMoeda", this._UpToMoedaByIdMoeda);
				}
				
			}
		}
		#endregion
		

				
		#region UpToOfficerByIdOfficer - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Officer_Cliente_FK1
		/// </summary>

		[XmlIgnore]
		public Officer UpToOfficerByIdOfficer
		{
			get
			{
				if(this._UpToOfficerByIdOfficer == null
					&& IdOfficer != null					)
				{
					this._UpToOfficerByIdOfficer = new Officer();
					this._UpToOfficerByIdOfficer.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOfficerByIdOfficer", this._UpToOfficerByIdOfficer);
					this._UpToOfficerByIdOfficer.Query.Where(this._UpToOfficerByIdOfficer.Query.IdOfficer == this.IdOfficer);
					this._UpToOfficerByIdOfficer.Query.Load();
				}

				return this._UpToOfficerByIdOfficer;
			}
			
			set
			{
				this.RemovePreSave("UpToOfficerByIdOfficer");
				

				if(value == null)
				{
					this.IdOfficer = null;
					this._UpToOfficerByIdOfficer = null;
				}
				else
				{
					this.IdOfficer = value.IdOfficer;
					this._UpToOfficerByIdOfficer = value;
					this.SetPreSave("UpToOfficerByIdOfficer", this._UpToOfficerByIdOfficer);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPessoaByIdPessoa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_IdPessoa_FK1
		/// </summary>

		[XmlIgnore]
		public Pessoa UpToPessoaByIdPessoa
		{
			get
			{
				if(this._UpToPessoaByIdPessoa == null
					&& IdPessoa != null					)
				{
					this._UpToPessoaByIdPessoa = new Pessoa();
					this._UpToPessoaByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
					this._UpToPessoaByIdPessoa.Query.Where(this._UpToPessoaByIdPessoa.Query.IdPessoa == this.IdPessoa);
					this._UpToPessoaByIdPessoa.Query.Load();
				}

				return this._UpToPessoaByIdPessoa;
			}
			
			set
			{
				this.RemovePreSave("UpToPessoaByIdPessoa");
				

				if(value == null)
				{
					this.IdPessoa = null;
					this._UpToPessoaByIdPessoa = null;
				}
				else
				{
					this.IdPessoa = value.IdPessoa;
					this._UpToPessoaByIdPessoa = value;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTipoClienteByIdTipo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TipoCliente_Cliente_FK1
		/// </summary>

		[XmlIgnore]
		public TipoCliente UpToTipoClienteByIdTipo
		{
			get
			{
				if(this._UpToTipoClienteByIdTipo == null
					&& IdTipo != null					)
				{
					this._UpToTipoClienteByIdTipo = new TipoCliente();
					this._UpToTipoClienteByIdTipo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTipoClienteByIdTipo", this._UpToTipoClienteByIdTipo);
					this._UpToTipoClienteByIdTipo.Query.Where(this._UpToTipoClienteByIdTipo.Query.IdTipo == this.IdTipo);
					this._UpToTipoClienteByIdTipo.Query.Load();
				}

				return this._UpToTipoClienteByIdTipo;
			}
			
			set
			{
				this.RemovePreSave("UpToTipoClienteByIdTipo");
				

				if(value == null)
				{
					this.IdTipo = null;
					this._UpToTipoClienteByIdTipo = null;
				}
				else
				{
					this.IdTipo = value.IdTipo;
					this._UpToTipoClienteByIdTipo = value;
					this.SetPreSave("UpToTipoClienteByIdTipo", this._UpToTipoClienteByIdTipo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "AcumuladoCorretagemBMFCollectionByIdCliente", typeof(AcumuladoCorretagemBMFCollection), new AcumuladoCorretagemBMF()));
			props.Add(new esPropertyDescriptor(this, "ApuracaoIRImobiliarioCollectionByIdCliente", typeof(ApuracaoIRImobiliarioCollection), new ApuracaoIRImobiliario()));
			props.Add(new esPropertyDescriptor(this, "ApuracaoRendaVariavelIRCollectionByIdCliente", typeof(ApuracaoRendaVariavelIRCollection), new ApuracaoRendaVariavelIR()));
			props.Add(new esPropertyDescriptor(this, "BloqueioBolsaCollectionByIdCliente", typeof(BloqueioBolsaCollection), new BloqueioBolsa()));
			props.Add(new esPropertyDescriptor(this, "BookClienteCollectionByIdCliente", typeof(BookClienteCollection), new BookCliente()));
			props.Add(new esPropertyDescriptor(this, "CalculoGerencialCollectionByIdCliente", typeof(CalculoGerencialCollection), new CalculoGerencial()));
			props.Add(new esPropertyDescriptor(this, "CalculoRebateImpactaPLCollectionByIdCliente", typeof(CalculoRebateImpactaPLCollection), new CalculoRebateImpactaPL()));
			props.Add(new esPropertyDescriptor(this, "ClientePerfilCollectionByIdCliente", typeof(ClientePerfilCollection), new ClientePerfil()));
			props.Add(new esPropertyDescriptor(this, "CodigoClienteAgenteCollectionByIdCliente", typeof(CodigoClienteAgenteCollection), new CodigoClienteAgente()));
			props.Add(new esPropertyDescriptor(this, "ContabLancamentoCollectionByIdCliente", typeof(ContabLancamentoCollection), new ContabLancamento()));
			props.Add(new esPropertyDescriptor(this, "ContabSaldoCollectionByIdCliente", typeof(ContabSaldoCollection), new ContabSaldo()));
			props.Add(new esPropertyDescriptor(this, "CustodiaBMFCollectionByIdCliente", typeof(CustodiaBMFCollection), new CustodiaBMF()));
			props.Add(new esPropertyDescriptor(this, "DetalheResgateFundoCollectionByIdCliente", typeof(DetalheResgateFundoCollection), new DetalheResgateFundo()));
			props.Add(new esPropertyDescriptor(this, "EventoFisicoBolsaCollectionByIdCliente", typeof(EventoFisicoBolsaCollection), new EventoFisicoBolsa()));
			props.Add(new esPropertyDescriptor(this, "EventoRendaFixaCollectionByIdCliente", typeof(EventoRendaFixaCollection), new EventoRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "GerOperacaoBMFCollectionByIdCliente", typeof(GerOperacaoBMFCollection), new GerOperacaoBMF()));
			props.Add(new esPropertyDescriptor(this, "GerOperacaoBolsaCollectionByIdCliente", typeof(GerOperacaoBolsaCollection), new GerOperacaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBMFCollectionByIdCliente", typeof(GerPosicaoBMFCollection), new GerPosicaoBMF()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBMFAberturaCollectionByIdCliente", typeof(GerPosicaoBMFAberturaCollection), new GerPosicaoBMFAbertura()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBMFHistoricoCollectionByIdCliente", typeof(GerPosicaoBMFHistoricoCollection), new GerPosicaoBMFHistorico()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBolsaCollectionByIdCliente", typeof(GerPosicaoBolsaCollection), new GerPosicaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBolsaAberturaCollectionByIdCliente", typeof(GerPosicaoBolsaAberturaCollection), new GerPosicaoBolsaAbertura()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBolsaHistoricoCollectionByIdCliente", typeof(GerPosicaoBolsaHistoricoCollection), new GerPosicaoBolsaHistorico()));
			props.Add(new esPropertyDescriptor(this, "IRFonteCollectionByIdCliente", typeof(IRFonteCollection), new IRFonte()));
			props.Add(new esPropertyDescriptor(this, "LiquidacaoCollectionByIdCliente", typeof(LiquidacaoCollection), new Liquidacao()));
			props.Add(new esPropertyDescriptor(this, "LiquidacaoAberturaCollectionByIdCliente", typeof(LiquidacaoAberturaCollection), new LiquidacaoAbertura()));
			props.Add(new esPropertyDescriptor(this, "LiquidacaoEmprestimoBolsaCollectionByIdCliente", typeof(LiquidacaoEmprestimoBolsaCollection), new LiquidacaoEmprestimoBolsa()));
			props.Add(new esPropertyDescriptor(this, "LiquidacaoFuturoCollectionByIdCliente", typeof(LiquidacaoFuturoCollection), new LiquidacaoFuturo()));
			props.Add(new esPropertyDescriptor(this, "LiquidacaoHistoricoCollectionByIdCliente", typeof(LiquidacaoHistoricoCollection), new LiquidacaoHistorico()));
			props.Add(new esPropertyDescriptor(this, "LiquidacaoRendaFixaCollectionByIdCliente", typeof(LiquidacaoRendaFixaCollection), new LiquidacaoRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "LiquidacaoSwapCollectionByIdCliente", typeof(LiquidacaoSwapCollection), new LiquidacaoSwap()));
			props.Add(new esPropertyDescriptor(this, "LiquidacaoTermoBolsaCollectionByIdCliente", typeof(LiquidacaoTermoBolsaCollection), new LiquidacaoTermoBolsa()));
			props.Add(new esPropertyDescriptor(this, "LogProcessamentoCollectionByIdCliente", typeof(LogProcessamentoCollection), new LogProcessamento()));
			props.Add(new esPropertyDescriptor(this, "OperacaoBMFCollectionByIdCliente", typeof(OperacaoBMFCollection), new OperacaoBMF()));
			props.Add(new esPropertyDescriptor(this, "OperacaoBolsaCollectionByIdCliente", typeof(OperacaoBolsaCollection), new OperacaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "OperacaoCambioCollectionByIdCliente", typeof(OperacaoCambioCollection), new OperacaoCambio()));
			props.Add(new esPropertyDescriptor(this, "OperacaoFundoCollectionByIdCliente", typeof(OperacaoFundoCollection), new OperacaoFundo()));
			props.Add(new esPropertyDescriptor(this, "OperacaoRendaFixaCollectionByIdCliente", typeof(OperacaoRendaFixaCollection), new OperacaoRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "OrdemBMFCollectionByIdCliente", typeof(OrdemBMFCollection), new OrdemBMF()));
			props.Add(new esPropertyDescriptor(this, "OrdemBolsaCollectionByIdCliente", typeof(OrdemBolsaCollection), new OrdemBolsa()));
			props.Add(new esPropertyDescriptor(this, "OrdemFundoCollectionByIdCliente", typeof(OrdemFundoCollection), new OrdemFundo()));
			props.Add(new esPropertyDescriptor(this, "OrdemRendaFixaCollectionByIdCliente", typeof(OrdemRendaFixaCollection), new OrdemRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "PerfilCorretagemBMFCollectionByIdCliente", typeof(PerfilCorretagemBMFCollection), new PerfilCorretagemBMF()));
			props.Add(new esPropertyDescriptor(this, "PerfilCorretagemBolsaCollectionByIdCliente", typeof(PerfilCorretagemBolsaCollection), new PerfilCorretagemBolsa()));
			props.Add(new esPropertyDescriptor(this, "PermissaoClienteCollectionByIdCliente", typeof(PermissaoClienteCollection), new PermissaoCliente()));
			props.Add(new esPropertyDescriptor(this, "PermissaoOperacaoFundoCollectionByIdFundo", typeof(PermissaoOperacaoFundoCollection), new PermissaoOperacaoFundo()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBMFCollectionByIdCliente", typeof(PosicaoBMFCollection), new PosicaoBMF()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBMFAberturaCollectionByIdCliente", typeof(PosicaoBMFAberturaCollection), new PosicaoBMFAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBMFHistoricoCollectionByIdCliente", typeof(PosicaoBMFHistoricoCollection), new PosicaoBMFHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBolsaCollectionByIdCliente", typeof(PosicaoBolsaCollection), new PosicaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBolsaAberturaCollectionByIdCliente", typeof(PosicaoBolsaAberturaCollection), new PosicaoBolsaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBolsaDetalheCollectionByIdCliente", typeof(PosicaoBolsaDetalheCollection), new PosicaoBolsaDetalhe()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBolsaHistoricoCollectionByIdCliente", typeof(PosicaoBolsaHistoricoCollection), new PosicaoBolsaHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoEmprestimoBolsaCollectionByIdCliente", typeof(PosicaoEmprestimoBolsaCollection), new PosicaoEmprestimoBolsa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoEmprestimoBolsaAberturaCollectionByIdCliente", typeof(PosicaoEmprestimoBolsaAberturaCollection), new PosicaoEmprestimoBolsaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoEmprestimoBolsaHistoricoCollectionByIdCliente", typeof(PosicaoEmprestimoBolsaHistoricoCollection), new PosicaoEmprestimoBolsaHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoFundoCollectionByIdCliente", typeof(PosicaoFundoCollection), new PosicaoFundo()));
			props.Add(new esPropertyDescriptor(this, "PosicaoFundoAberturaCollectionByIdCliente", typeof(PosicaoFundoAberturaCollection), new PosicaoFundoAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoFundoHistoricoCollectionByIdCliente", typeof(PosicaoFundoHistoricoCollection), new PosicaoFundoHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoRendaFixaCollectionByIdCliente", typeof(PosicaoRendaFixaCollection), new PosicaoRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoRendaFixaAberturaCollectionByIdCliente", typeof(PosicaoRendaFixaAberturaCollection), new PosicaoRendaFixaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoRendaFixaDetalheCollectionByIdCliente", typeof(PosicaoRendaFixaDetalheCollection), new PosicaoRendaFixaDetalhe()));
			props.Add(new esPropertyDescriptor(this, "PosicaoRendaFixaHistoricoCollectionByIdCliente", typeof(PosicaoRendaFixaHistoricoCollection), new PosicaoRendaFixaHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapAberturaCollectionByIdCliente", typeof(PosicaoSwapAberturaCollection), new PosicaoSwapAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapHistoricoCollectionByIdCliente", typeof(PosicaoSwapHistoricoCollection), new PosicaoSwapHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoTermoBolsaCollectionByIdCliente", typeof(PosicaoTermoBolsaCollection), new PosicaoTermoBolsa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoTermoBolsaAberturaCollectionByIdCliente", typeof(PosicaoTermoBolsaAberturaCollection), new PosicaoTermoBolsaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoTermoBolsaHistoricoCollectionByIdCliente", typeof(PosicaoTermoBolsaHistoricoCollection), new PosicaoTermoBolsaHistorico()));
			props.Add(new esPropertyDescriptor(this, "PrejuizoFundoCollectionByIdCliente", typeof(PrejuizoFundoCollection), new PrejuizoFundo()));
			props.Add(new esPropertyDescriptor(this, "PrejuizoFundoHistoricoCollectionByIdCliente", typeof(PrejuizoFundoHistoricoCollection), new PrejuizoFundoHistorico()));
			props.Add(new esPropertyDescriptor(this, "ProventoBolsaClienteCollectionByIdCliente", typeof(ProventoBolsaClienteCollection), new ProventoBolsaCliente()));
			props.Add(new esPropertyDescriptor(this, "SaldoCaixaCollectionByIdCliente", typeof(SaldoCaixaCollection), new SaldoCaixa()));
			props.Add(new esPropertyDescriptor(this, "TabelaCONRCollectionByIdCliente", typeof(TabelaCONRCollection), new TabelaCONR()));
			props.Add(new esPropertyDescriptor(this, "TabelaCustosRendaFixaCollectionByIdCliente", typeof(TabelaCustosRendaFixaCollection), new TabelaCustosRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "TabelaInterfaceClienteCollectionByIdCliente", typeof(TabelaInterfaceClienteCollection), new TabelaInterfaceCliente()));
			props.Add(new esPropertyDescriptor(this, "TabelaProcessamentoCollectionByIdCliente", typeof(TabelaProcessamentoCollection), new TabelaProcessamento()));
			props.Add(new esPropertyDescriptor(this, "TabelaRebateCorretagemCollectionByIdCliente", typeof(TabelaRebateCorretagemCollection), new TabelaRebateCorretagem()));
			props.Add(new esPropertyDescriptor(this, "TabelaTaxaCustodiaBolsaCollectionByIdCliente", typeof(TabelaTaxaCustodiaBolsaCollection), new TabelaTaxaCustodiaBolsa()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaBMFCollectionByIdCliente", typeof(TransferenciaBMFCollection), new TransferenciaBMF()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaBolsaCollectionByIdCliente", typeof(TransferenciaBolsaCollection), new TransferenciaBolsa()));
			props.Add(new esPropertyDescriptor(this, "UsuarioCollectionByIdCliente", typeof(UsuarioCollection), new Usuario()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToContabPlanoByIdPlano != null)
			{
				this.IdPlano = this._UpToContabPlanoByIdPlano.IdPlano;
			}
			if(!this.es.IsDeleted && this._UpToGrupoAfinidadeByIdGrupoAfinidade != null)
			{
				this.IdGrupoAfinidade = this._UpToGrupoAfinidadeByIdGrupoAfinidade.IdGrupo;
			}
			if(!this.es.IsDeleted && this._UpToGrupoProcessamentoByIdGrupoProcessamento != null)
			{
				this.IdGrupoProcessamento = this._UpToGrupoProcessamentoByIdGrupoProcessamento.IdGrupoProcessamento;
			}
			if(!this.es.IsDeleted && this._UpToMoedaByIdMoeda != null)
			{
				this.IdMoeda = this._UpToMoedaByIdMoeda.IdMoeda;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}
	
	public partial class ClienteCollection : esClienteCollection
	{
		#region ManyToManyBookCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyBookCollection(System.Int32? IdBook)
		{
			esParameters parms = new esParameters();
			parms.Add("IdBook", IdBook);
	
			return base.Load( esQueryType.ManyToMany, 
				"Cliente,BookCliente|IdCliente,IdCliente|IdBook",	parms);
		}
		#endregion
	}

	
	public partial class ClienteCollection : esClienteCollection
	{
		#region ManyToManyPerfilProcessamentoCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyPerfilProcessamentoCollection(System.Int32? IdPerfil)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPerfil", IdPerfil);
	
			return base.Load( esQueryType.ManyToMany, 
				"Cliente,ClientePerfil|IdCliente,IdCliente|IdPerfil",	parms);
		}
		#endregion
	}

	
	public partial class ClienteCollection : esClienteCollection
	{
		#region ManyToManyUsuarioCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyUsuarioCollection(System.Int32? IdUsuario)
		{
			esParameters parms = new esParameters();
			parms.Add("IdUsuario", IdUsuario);
	
			return base.Load( esQueryType.ManyToMany, 
				"Cliente,PermissaoCliente|IdCliente,IdCliente|IdUsuario",	parms);
		}
		#endregion
	}

	
	public partial class ClienteCollection : esClienteCollection
	{
		#region ManyToManyGrupoProcessamentoCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyGrupoProcessamentoCollection(System.Int16? IdGrupoProcessamento)
		{
			esParameters parms = new esParameters();
			parms.Add("IdGrupoProcessamento", IdGrupoProcessamento);
	
			return base.Load( esQueryType.ManyToMany, 
				"Cliente,TabelaProcessamento|IdCliente,IdCliente|IdGrupoProcessamento",	parms);
		}
		#endregion
	}




	[Serializable]
	abstract public class esClienteQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ClienteMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem Apelido
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.Apelido, esSystemType.String);
			}
		} 
		
		public esQueryItem IdTipo
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.IdTipo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem StatusAtivo
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.StatusAtivo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Status
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.Status, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataDia
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.DataDia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataInicio
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.DataInicio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataImplantacao
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.DataImplantacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ApuraGanhoRV
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.ApuraGanhoRV, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoControle
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.TipoControle, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IsentoIR
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.IsentoIR, esSystemType.String);
			}
		} 
		
		public esQueryItem IsentoIOF
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.IsentoIOF, esSystemType.String);
			}
		} 
		
		public esQueryItem IdMoeda
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.IdMoeda, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdGrupoProcessamento
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.IdGrupoProcessamento, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IsProcessando
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.IsProcessando, esSystemType.String);
			}
		} 
		
		public esQueryItem BookEmail
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.BookEmail, esSystemType.String);
			}
		} 
		
		public esQueryItem BookAssunto
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.BookAssunto, esSystemType.String);
			}
		} 
		
		public esQueryItem BookMensagem
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.BookMensagem, esSystemType.String);
			}
		} 
		
		public esQueryItem StatusRealTime
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.StatusRealTime, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CalculaRealTime
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.CalculaRealTime, esSystemType.String);
			}
		} 
		
		public esQueryItem CalculaGerencial
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.CalculaGerencial, esSystemType.String);
			}
		} 
		
		public esQueryItem CalculaContabil
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.CalculaContabil, esSystemType.String);
			}
		} 
		
		public esQueryItem IdPlano
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.IdPlano, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ContabilAcoes
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.ContabilAcoes, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ContabilFundos
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.ContabilFundos, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdOfficer
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.IdOfficer, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ZeraCaixa
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.ZeraCaixa, esSystemType.String);
			}
		} 
		
		public esQueryItem DescontaTributoPL
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.DescontaTributoPL, esSystemType.Byte);
			}
		} 
		
		public esQueryItem GrossUP
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.GrossUP, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ContabilBMF
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.ContabilBMF, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ContabilLiquidacao
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.ContabilLiquidacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ContabilDaytrade
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.ContabilDaytrade, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdGrupoAfinidade
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.IdGrupoAfinidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Logotipo
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.Logotipo, esSystemType.String);
			}
		} 
		
		public esQueryItem IdLocal
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.IdLocal, esSystemType.Int16);
			}
		} 
		
		public esQueryItem ContabilDataTrava
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.ContabilDataTrava, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataBloqueioProcessamento
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.DataBloqueioProcessamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem MultiMoeda
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.MultiMoeda, esSystemType.String);
			}
		} 
		
		public esQueryItem IdLocalNegociacao
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.IdLocalNegociacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem RegimeEspecialTributacao
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.RegimeEspecialTributacao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem AberturaIndexada
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.AberturaIndexada, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdIndiceAbertura
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.IdIndiceAbertura, esSystemType.Int16);
			}
		}

		public esQueryItem InvestidorProfissional
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.InvestidorProfissional, esSystemType.String);
			}
		} 
		
		public esQueryItem InvestidorQualificado
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.InvestidorQualificado, esSystemType.String);
			}
		} 
		
		public esQueryItem AmortizacaoRendimentoJuros
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.AmortizacaoRendimentoJuros, esSystemType.String);
			}
		} 
		
        
		public esQueryItem DataAtualizacao
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.DataAtualizacao, esSystemType.DateTime);
			}
		} 
       
		
		public esQueryItem DataEnvioCetip
		{
			get
			{
				return new esQueryItem(this, ClienteMetadata.ColumnNames.DataEnvioCetip, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ClienteCollection")]
	public partial class ClienteCollection : esClienteCollection, IEnumerable<Cliente>
	{
		public ClienteCollection()
		{

		}
		
		public static implicit operator List<Cliente>(ClienteCollection coll)
		{
			List<Cliente> list = new List<Cliente>();
			
			foreach (Cliente emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Cliente(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Cliente();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Cliente AddNew()
		{
			Cliente entity = base.AddNewEntity() as Cliente;
			
			return entity;
		}

		public Cliente FindByPrimaryKey(System.Int32 idCliente)
		{
			return base.FindByPrimaryKey(idCliente) as Cliente;
		}


		#region IEnumerable<Cliente> Members

		IEnumerator<Cliente> IEnumerable<Cliente>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Cliente;
			}
		}

		#endregion
		
		private ClienteQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Cliente' table
	/// </summary>

	[Serializable]
	public partial class Cliente : esCliente
	{
		public Cliente()
		{

		}
	
		public Cliente(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esClienteQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ClienteQuery query;
	}



	[Serializable]
	public partial class ClienteQuery : esClienteQuery
	{
		public ClienteQuery()
		{

		}		
		
		public ClienteQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ClienteMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ClienteMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ClienteMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.Apelido, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.Apelido;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.IdTipo, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteMetadata.PropertyNames.IdTipo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.StatusAtivo, 4, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClienteMetadata.PropertyNames.StatusAtivo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.Status, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClienteMetadata.PropertyNames.Status;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.DataDia, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ClienteMetadata.PropertyNames.DataDia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.DataInicio, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ClienteMetadata.PropertyNames.DataInicio;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.DataImplantacao, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ClienteMetadata.PropertyNames.DataImplantacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.ApuraGanhoRV, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.ApuraGanhoRV;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.TipoControle, 10, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClienteMetadata.PropertyNames.TipoControle;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.IsentoIR, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.IsentoIR;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.IsentoIOF, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.IsentoIOF;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.IdMoeda, 13, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ClienteMetadata.PropertyNames.IdMoeda;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.IdGrupoProcessamento, 14, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ClienteMetadata.PropertyNames.IdGrupoProcessamento;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.IsProcessando, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.IsProcessando;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.BookEmail, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.BookEmail;
			c.CharacterMaxLength = 400;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.BookAssunto, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.BookAssunto;
			c.CharacterMaxLength = 1000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.BookMensagem, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.BookMensagem;
			c.CharacterMaxLength = 2000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.StatusRealTime, 19, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClienteMetadata.PropertyNames.StatusRealTime;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.CalculaRealTime, 20, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.CalculaRealTime;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.CalculaGerencial, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.CalculaGerencial;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.CalculaContabil, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.CalculaContabil;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.IdPlano, 23, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteMetadata.PropertyNames.IdPlano;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.ContabilAcoes, 24, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClienteMetadata.PropertyNames.ContabilAcoes;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.ContabilFundos, 25, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClienteMetadata.PropertyNames.ContabilFundos;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.IdOfficer, 26, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteMetadata.PropertyNames.IdOfficer;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.ZeraCaixa, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.ZeraCaixa;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.DescontaTributoPL, 28, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClienteMetadata.PropertyNames.DescontaTributoPL;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.GrossUP, 29, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClienteMetadata.PropertyNames.GrossUP;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.ContabilBMF, 30, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClienteMetadata.PropertyNames.ContabilBMF;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.ContabilLiquidacao, 31, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClienteMetadata.PropertyNames.ContabilLiquidacao;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.ContabilDaytrade, 32, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClienteMetadata.PropertyNames.ContabilDaytrade;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.IdGrupoAfinidade, 33, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteMetadata.PropertyNames.IdGrupoAfinidade;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.Logotipo, 34, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.Logotipo;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.IdLocal, 35, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ClienteMetadata.PropertyNames.IdLocal;	
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((1))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.ContabilDataTrava, 36, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ClienteMetadata.PropertyNames.ContabilDataTrava;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.DataBloqueioProcessamento, 37, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ClienteMetadata.PropertyNames.DataBloqueioProcessamento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.MultiMoeda, 38, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.MultiMoeda;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.IdLocalNegociacao, 39, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteMetadata.PropertyNames.IdLocalNegociacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.RegimeEspecialTributacao, 40, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.RegimeEspecialTributacao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.IdPessoa, 41, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteMetadata.PropertyNames.IdPessoa;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.AberturaIndexada, 42, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ClienteMetadata.PropertyNames.AberturaIndexada;	
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.IdIndiceAbertura, 43, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ClienteMetadata.PropertyNames.IdIndiceAbertura;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			

			c = new esColumnMetadata(ClienteMetadata.ColumnNames.InvestidorProfissional, 44, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.InvestidorProfissional;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.InvestidorQualificado, 45, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.InvestidorQualificado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 

			c = new esColumnMetadata(ClienteMetadata.ColumnNames.AmortizacaoRendimentoJuros, 47, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteMetadata.PropertyNames.AmortizacaoRendimentoJuros;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.DataAtualizacao, 42, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ClienteMetadata.PropertyNames.DataAtualizacao;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"(getdate())";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteMetadata.ColumnNames.DataEnvioCetip, 43, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ClienteMetadata.PropertyNames.DataEnvioCetip;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ClienteMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string Nome = "Nome";
			 public const string Apelido = "Apelido";
			 public const string IdTipo = "IdTipo";
			 public const string StatusAtivo = "StatusAtivo";
			 public const string Status = "Status";
			 public const string DataDia = "DataDia";
			 public const string DataInicio = "DataInicio";
			 public const string DataImplantacao = "DataImplantacao";
			 public const string ApuraGanhoRV = "ApuraGanhoRV";
			 public const string TipoControle = "TipoControle";
			 public const string IsentoIR = "IsentoIR";
			 public const string IsentoIOF = "IsentoIOF";
			 public const string IdMoeda = "IdMoeda";
			 public const string IdGrupoProcessamento = "IdGrupoProcessamento";
			 public const string IsProcessando = "IsProcessando";
			 public const string BookEmail = "BookEmail";
			 public const string BookAssunto = "BookAssunto";
			 public const string BookMensagem = "BookMensagem";
			 public const string StatusRealTime = "StatusRealTime";
			 public const string CalculaRealTime = "CalculaRealTime";
			 public const string CalculaGerencial = "CalculaGerencial";
			 public const string CalculaContabil = "CalculaContabil";
			 public const string IdPlano = "IdPlano";
			 public const string ContabilAcoes = "ContabilAcoes";
			 public const string ContabilFundos = "ContabilFundos";
			 public const string IdOfficer = "IdOfficer";
			 public const string ZeraCaixa = "ZeraCaixa";
			 public const string DescontaTributoPL = "DescontaTributoPL";
			 public const string GrossUP = "GrossUP";
			 public const string ContabilBMF = "ContabilBMF";
			 public const string ContabilLiquidacao = "ContabilLiquidacao";
			 public const string ContabilDaytrade = "ContabilDaytrade";
			 public const string IdGrupoAfinidade = "IdGrupoAfinidade";
			 public const string Logotipo = "Logotipo";
			 public const string IdLocal = "IdLocal";
			 public const string ContabilDataTrava = "ContabilDataTrava";
			 public const string DataBloqueioProcessamento = "DataBloqueioProcessamento";
			 public const string MultiMoeda = "MultiMoeda";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string RegimeEspecialTributacao = "RegimeEspecialTributacao";
			 public const string IdPessoa = "IdPessoa";
			 public const string AberturaIndexada = "AberturaIndexada";
			 public const string IdIndiceAbertura = "IdIndiceAbertura";
			 public const string InvestidorProfissional = "InvestidorProfissional";
			 public const string InvestidorQualificado = "InvestidorQualificado";
			 public const string AmortizacaoRendimentoJuros = "AmortizacaoRendimentoJuros";
			 public const string DataAtualizacao = "DataAtualizacao";
			 public const string DataEnvioCetip = "DataEnvioCetip";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string Nome = "Nome";
			 public const string Apelido = "Apelido";
			 public const string IdTipo = "IdTipo";
			 public const string StatusAtivo = "StatusAtivo";
			 public const string Status = "Status";
			 public const string DataDia = "DataDia";
			 public const string DataInicio = "DataInicio";
			 public const string DataImplantacao = "DataImplantacao";
			 public const string ApuraGanhoRV = "ApuraGanhoRV";
			 public const string TipoControle = "TipoControle";
			 public const string IsentoIR = "IsentoIR";
			 public const string IsentoIOF = "IsentoIOF";
			 public const string IdMoeda = "IdMoeda";
			 public const string IdGrupoProcessamento = "IdGrupoProcessamento";
			 public const string IsProcessando = "IsProcessando";
			 public const string BookEmail = "BookEmail";
			 public const string BookAssunto = "BookAssunto";
			 public const string BookMensagem = "BookMensagem";
			 public const string StatusRealTime = "StatusRealTime";
			 public const string CalculaRealTime = "CalculaRealTime";
			 public const string CalculaGerencial = "CalculaGerencial";
			 public const string CalculaContabil = "CalculaContabil";
			 public const string IdPlano = "IdPlano";
			 public const string ContabilAcoes = "ContabilAcoes";
			 public const string ContabilFundos = "ContabilFundos";
			 public const string IdOfficer = "IdOfficer";
			 public const string ZeraCaixa = "ZeraCaixa";
			 public const string DescontaTributoPL = "DescontaTributoPL";
			 public const string GrossUP = "GrossUP";
			 public const string ContabilBMF = "ContabilBMF";
			 public const string ContabilLiquidacao = "ContabilLiquidacao";
			 public const string ContabilDaytrade = "ContabilDaytrade";
			 public const string IdGrupoAfinidade = "IdGrupoAfinidade";
			 public const string Logotipo = "Logotipo";
			 public const string IdLocal = "IdLocal";
			 public const string ContabilDataTrava = "ContabilDataTrava";
			 public const string DataBloqueioProcessamento = "DataBloqueioProcessamento";
			 public const string MultiMoeda = "MultiMoeda";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string RegimeEspecialTributacao = "RegimeEspecialTributacao";
			 public const string IdPessoa = "IdPessoa";
			 public const string AberturaIndexada = "AberturaIndexada";
			 public const string IdIndiceAbertura = "IdIndiceAbertura";
			 public const string InvestidorProfissional = "InvestidorProfissional";
			 public const string InvestidorQualificado = "InvestidorQualificado";
			 public const string AmortizacaoRendimentoJuros = "AmortizacaoRendimentoJuros";
			 public const string DataAtualizacao = "DataAtualizacao";
			 public const string DataEnvioCetip = "DataEnvioCetip";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ClienteMetadata))
			{
				if(ClienteMetadata.mapDelegates == null)
				{
					ClienteMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ClienteMetadata.meta == null)
				{
					ClienteMetadata.meta = new ClienteMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Apelido", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdTipo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("StatusAtivo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Status", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataDia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataInicio", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataImplantacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ApuraGanhoRV", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TipoControle", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IsentoIR", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IsentoIOF", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdMoeda", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdGrupoProcessamento", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IsProcessando", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("BookEmail", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("BookAssunto", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("BookMensagem", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("StatusRealTime", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CalculaRealTime", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("CalculaGerencial", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("CalculaContabil", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdPlano", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ContabilAcoes", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ContabilFundos", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdOfficer", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ZeraCaixa", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DescontaTributoPL", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("GrossUP", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ContabilBMF", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ContabilLiquidacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ContabilDaytrade", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdGrupoAfinidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Logotipo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdLocal", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ContabilDataTrava", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataBloqueioProcessamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("MultiMoeda", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdLocalNegociacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RegimeEspecialTributacao", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AberturaIndexada", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdIndiceAbertura", new esTypeMap("smallint", "System.Int16"));			
				meta.AddTypeMap("InvestidorProfissional", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("InvestidorQualificado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("AmortizacaoRendimentoJuros", new esTypeMap("char", "System.String"));			
				meta.AddTypeMap("DataAtualizacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataEnvioCetip", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "Cliente";
				meta.Destination = "Cliente";
				
				meta.spInsert = "proc_ClienteInsert";				
				meta.spUpdate = "proc_ClienteUpdate";		
				meta.spDelete = "proc_ClienteDelete";
				meta.spLoadAll = "proc_ClienteLoadAll";
				meta.spLoadByPrimaryKey = "proc_ClienteLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ClienteMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
