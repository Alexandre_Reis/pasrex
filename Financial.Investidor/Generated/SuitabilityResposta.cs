/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 2/11/2015 10:56:13 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.CRM;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityRespostaCollection : esEntityCollection
	{
		public esSuitabilityRespostaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityRespostaCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityRespostaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityRespostaQuery);
		}
		#endregion
		
		virtual public SuitabilityResposta DetachEntity(SuitabilityResposta entity)
		{
			return base.DetachEntity(entity) as SuitabilityResposta;
		}
		
		virtual public SuitabilityResposta AttachEntity(SuitabilityResposta entity)
		{
			return base.AttachEntity(entity) as SuitabilityResposta;
		}
		
		virtual public void Combine(SuitabilityRespostaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityResposta this[int index]
		{
			get
			{
				return base[index] as SuitabilityResposta;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityResposta);
		}
	}



	[Serializable]
	abstract public class esSuitabilityResposta : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityRespostaQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityResposta()
		{

		}

		public esSuitabilityResposta(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idResposta)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idResposta);
			else
				return LoadByPrimaryKeyStoredProcedure(idResposta);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idResposta)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esSuitabilityRespostaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdResposta == idResposta);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idResposta)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idResposta);
			else
				return LoadByPrimaryKeyStoredProcedure(idResposta);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idResposta)
		{
			esSuitabilityRespostaQuery query = this.GetDynamicQuery();
			query.Where(query.IdResposta == idResposta);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idResposta)
		{
			esParameters parms = new esParameters();
			parms.Add("IdResposta",idResposta);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdResposta": this.str.IdResposta = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;							
						case "IdQuestao": this.str.IdQuestao = (string)value; break;							
						case "IdOpcaoEscolhida": this.str.IdOpcaoEscolhida = (string)value; break;							
						case "RiskPoints": this.str.RiskPoints = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdResposta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdResposta = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
						
						case "IdQuestao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdQuestao = (System.Int32?)value;
							break;
						
						case "IdOpcaoEscolhida":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOpcaoEscolhida = (System.Int32?)value;
							break;
						
						case "RiskPoints":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.RiskPoints = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityResposta.IdResposta
		/// </summary>
		virtual public System.Int32? IdResposta
		{
			get
			{
				return base.GetSystemInt32(SuitabilityRespostaMetadata.ColumnNames.IdResposta);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityRespostaMetadata.ColumnNames.IdResposta, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityResposta.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(SuitabilityRespostaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(SuitabilityRespostaMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityResposta.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(SuitabilityRespostaMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				if(base.SetSystemInt32(SuitabilityRespostaMetadata.ColumnNames.IdPessoa, value))
				{
					this._UpToPessoaByIdPessoa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityResposta.IdQuestao
		/// </summary>
		virtual public System.Int32? IdQuestao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityRespostaMetadata.ColumnNames.IdQuestao);
			}
			
			set
			{
				if(base.SetSystemInt32(SuitabilityRespostaMetadata.ColumnNames.IdQuestao, value))
				{
					this._UpToSuitabilityQuestaoByIdQuestao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityResposta.IdOpcaoEscolhida
		/// </summary>
		virtual public System.Int32? IdOpcaoEscolhida
		{
			get
			{
				return base.GetSystemInt32(SuitabilityRespostaMetadata.ColumnNames.IdOpcaoEscolhida);
			}
			
			set
			{
				if(base.SetSystemInt32(SuitabilityRespostaMetadata.ColumnNames.IdOpcaoEscolhida, value))
				{
					this._UpToSuitabilityOpcaoByIdOpcaoEscolhida = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityResposta.RiskPoints
		/// </summary>
		virtual public System.Int32? RiskPoints
		{
			get
			{
				return base.GetSystemInt32(SuitabilityRespostaMetadata.ColumnNames.RiskPoints);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityRespostaMetadata.ColumnNames.RiskPoints, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Pessoa _UpToPessoaByIdPessoa;
		[CLSCompliant(false)]
		internal protected SuitabilityOpcao _UpToSuitabilityOpcaoByIdOpcaoEscolhida;
		[CLSCompliant(false)]
		internal protected SuitabilityQuestao _UpToSuitabilityQuestaoByIdQuestao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityResposta entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdResposta
			{
				get
				{
					System.Int32? data = entity.IdResposta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdResposta = null;
					else entity.IdResposta = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
				
			public System.String IdQuestao
			{
				get
				{
					System.Int32? data = entity.IdQuestao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdQuestao = null;
					else entity.IdQuestao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOpcaoEscolhida
			{
				get
				{
					System.Int32? data = entity.IdOpcaoEscolhida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOpcaoEscolhida = null;
					else entity.IdOpcaoEscolhida = Convert.ToInt32(value);
				}
			}
				
			public System.String RiskPoints
			{
				get
				{
					System.Int32? data = entity.RiskPoints;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RiskPoints = null;
					else entity.RiskPoints = Convert.ToInt32(value);
				}
			}
			

			private esSuitabilityResposta entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityRespostaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityResposta can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityResposta : esSuitabilityResposta
	{

				
		#region UpToPessoaByIdPessoa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_SuitabilityResposta_Pessoa
		/// </summary>

		[XmlIgnore]
		public Pessoa UpToPessoaByIdPessoa
		{
			get
			{
				if(this._UpToPessoaByIdPessoa == null
					&& IdPessoa != null					)
				{
					this._UpToPessoaByIdPessoa = new Pessoa();
					this._UpToPessoaByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
					this._UpToPessoaByIdPessoa.Query.Where(this._UpToPessoaByIdPessoa.Query.IdPessoa == this.IdPessoa);
					this._UpToPessoaByIdPessoa.Query.Load();
				}

				return this._UpToPessoaByIdPessoa;
			}
			
			set
			{
				this.RemovePreSave("UpToPessoaByIdPessoa");
				

				if(value == null)
				{
					this.IdPessoa = null;
					this._UpToPessoaByIdPessoa = null;
				}
				else
				{
					this.IdPessoa = value.IdPessoa;
					this._UpToPessoaByIdPessoa = value;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToSuitabilityOpcaoByIdOpcaoEscolhida - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_SuitabilityResposta_SuitabilityOpcao
		/// </summary>

		[XmlIgnore]
		public SuitabilityOpcao UpToSuitabilityOpcaoByIdOpcaoEscolhida
		{
			get
			{
				if(this._UpToSuitabilityOpcaoByIdOpcaoEscolhida == null
					&& IdOpcaoEscolhida != null					)
				{
					this._UpToSuitabilityOpcaoByIdOpcaoEscolhida = new SuitabilityOpcao();
					this._UpToSuitabilityOpcaoByIdOpcaoEscolhida.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSuitabilityOpcaoByIdOpcaoEscolhida", this._UpToSuitabilityOpcaoByIdOpcaoEscolhida);
					this._UpToSuitabilityOpcaoByIdOpcaoEscolhida.Query.Where(this._UpToSuitabilityOpcaoByIdOpcaoEscolhida.Query.IdOpcao == this.IdOpcaoEscolhida);
					this._UpToSuitabilityOpcaoByIdOpcaoEscolhida.Query.Load();
				}

				return this._UpToSuitabilityOpcaoByIdOpcaoEscolhida;
			}
			
			set
			{
				this.RemovePreSave("UpToSuitabilityOpcaoByIdOpcaoEscolhida");
				

				if(value == null)
				{
					this.IdOpcaoEscolhida = null;
					this._UpToSuitabilityOpcaoByIdOpcaoEscolhida = null;
				}
				else
				{
					this.IdOpcaoEscolhida = value.IdOpcao;
					this._UpToSuitabilityOpcaoByIdOpcaoEscolhida = value;
					this.SetPreSave("UpToSuitabilityOpcaoByIdOpcaoEscolhida", this._UpToSuitabilityOpcaoByIdOpcaoEscolhida);
				}
				
			}
		}
		#endregion
		

				
		#region UpToSuitabilityQuestaoByIdQuestao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_SuitabilityResposta_SuitabilityQuestao
		/// </summary>

		[XmlIgnore]
		public SuitabilityQuestao UpToSuitabilityQuestaoByIdQuestao
		{
			get
			{
				if(this._UpToSuitabilityQuestaoByIdQuestao == null
					&& IdQuestao != null					)
				{
					this._UpToSuitabilityQuestaoByIdQuestao = new SuitabilityQuestao();
					this._UpToSuitabilityQuestaoByIdQuestao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSuitabilityQuestaoByIdQuestao", this._UpToSuitabilityQuestaoByIdQuestao);
					this._UpToSuitabilityQuestaoByIdQuestao.Query.Where(this._UpToSuitabilityQuestaoByIdQuestao.Query.IdQuestao == this.IdQuestao);
					this._UpToSuitabilityQuestaoByIdQuestao.Query.Load();
				}

				return this._UpToSuitabilityQuestaoByIdQuestao;
			}
			
			set
			{
				this.RemovePreSave("UpToSuitabilityQuestaoByIdQuestao");
				

				if(value == null)
				{
					this.IdQuestao = null;
					this._UpToSuitabilityQuestaoByIdQuestao = null;
				}
				else
				{
					this.IdQuestao = value.IdQuestao;
					this._UpToSuitabilityQuestaoByIdQuestao = value;
					this.SetPreSave("UpToSuitabilityQuestaoByIdQuestao", this._UpToSuitabilityQuestaoByIdQuestao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToSuitabilityOpcaoByIdOpcaoEscolhida != null)
			{
				this.IdOpcaoEscolhida = this._UpToSuitabilityOpcaoByIdOpcaoEscolhida.IdOpcao;
			}
			if(!this.es.IsDeleted && this._UpToSuitabilityQuestaoByIdQuestao != null)
			{
				this.IdQuestao = this._UpToSuitabilityQuestaoByIdQuestao.IdQuestao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityRespostaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityRespostaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdResposta
		{
			get
			{
				return new esQueryItem(this, SuitabilityRespostaMetadata.ColumnNames.IdResposta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, SuitabilityRespostaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, SuitabilityRespostaMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdQuestao
		{
			get
			{
				return new esQueryItem(this, SuitabilityRespostaMetadata.ColumnNames.IdQuestao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOpcaoEscolhida
		{
			get
			{
				return new esQueryItem(this, SuitabilityRespostaMetadata.ColumnNames.IdOpcaoEscolhida, esSystemType.Int32);
			}
		} 
		
		public esQueryItem RiskPoints
		{
			get
			{
				return new esQueryItem(this, SuitabilityRespostaMetadata.ColumnNames.RiskPoints, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityRespostaCollection")]
	public partial class SuitabilityRespostaCollection : esSuitabilityRespostaCollection, IEnumerable<SuitabilityResposta>
	{
		public SuitabilityRespostaCollection()
		{

		}
		
		public static implicit operator List<SuitabilityResposta>(SuitabilityRespostaCollection coll)
		{
			List<SuitabilityResposta> list = new List<SuitabilityResposta>();
			
			foreach (SuitabilityResposta emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityRespostaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityRespostaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityResposta(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityResposta();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityRespostaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityRespostaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityRespostaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityResposta AddNew()
		{
			SuitabilityResposta entity = base.AddNewEntity() as SuitabilityResposta;
			
			return entity;
		}

		public SuitabilityResposta FindByPrimaryKey(System.Int32 idResposta)
		{
			return base.FindByPrimaryKey(idResposta) as SuitabilityResposta;
		}


		#region IEnumerable<SuitabilityResposta> Members

		IEnumerator<SuitabilityResposta> IEnumerable<SuitabilityResposta>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityResposta;
			}
		}

		#endregion
		
		private SuitabilityRespostaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityResposta' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityResposta : esSuitabilityResposta
	{
		public SuitabilityResposta()
		{

		}
	
		public SuitabilityResposta(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityRespostaMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityRespostaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityRespostaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityRespostaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityRespostaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityRespostaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityRespostaQuery query;
	}



	[Serializable]
	public partial class SuitabilityRespostaQuery : esSuitabilityRespostaQuery
	{
		public SuitabilityRespostaQuery()
		{

		}		
		
		public SuitabilityRespostaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityRespostaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityRespostaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityRespostaMetadata.ColumnNames.IdResposta, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityRespostaMetadata.PropertyNames.IdResposta;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityRespostaMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SuitabilityRespostaMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityRespostaMetadata.ColumnNames.IdPessoa, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityRespostaMetadata.PropertyNames.IdPessoa;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityRespostaMetadata.ColumnNames.IdQuestao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityRespostaMetadata.PropertyNames.IdQuestao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityRespostaMetadata.ColumnNames.IdOpcaoEscolhida, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityRespostaMetadata.PropertyNames.IdOpcaoEscolhida;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityRespostaMetadata.ColumnNames.RiskPoints, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityRespostaMetadata.PropertyNames.RiskPoints;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityRespostaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdResposta = "IdResposta";
			 public const string Data = "Data";
			 public const string IdPessoa = "IdPessoa";
			 public const string IdQuestao = "IdQuestao";
			 public const string IdOpcaoEscolhida = "IdOpcaoEscolhida";
			 public const string RiskPoints = "RiskPoints";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdResposta = "IdResposta";
			 public const string Data = "Data";
			 public const string IdPessoa = "IdPessoa";
			 public const string IdQuestao = "IdQuestao";
			 public const string IdOpcaoEscolhida = "IdOpcaoEscolhida";
			 public const string RiskPoints = "RiskPoints";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityRespostaMetadata))
			{
				if(SuitabilityRespostaMetadata.mapDelegates == null)
				{
					SuitabilityRespostaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityRespostaMetadata.meta == null)
				{
					SuitabilityRespostaMetadata.meta = new SuitabilityRespostaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdResposta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdQuestao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOpcaoEscolhida", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RiskPoints", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "SuitabilityResposta";
				meta.Destination = "SuitabilityResposta";
				
				meta.spInsert = "proc_SuitabilityRespostaInsert";				
				meta.spUpdate = "proc_SuitabilityRespostaUpdate";		
				meta.spDelete = "proc_SuitabilityRespostaDelete";
				meta.spLoadAll = "proc_SuitabilityRespostaLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityRespostaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityRespostaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
