/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/04/2015 15:39:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esViewGPSAgendaFluxoAtivosCreditosCollection : esEntityCollection
	{
		public esViewGPSAgendaFluxoAtivosCreditosCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ViewGPSAgendaFluxoAtivosCreditosCollection";
		}

		#region Query Logic
		protected void InitQuery(esViewGPSAgendaFluxoAtivosCreditosQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esViewGPSAgendaFluxoAtivosCreditosQuery);
		}
		#endregion
		
		virtual public ViewGPSAgendaFluxoAtivosCreditos DetachEntity(ViewGPSAgendaFluxoAtivosCreditos entity)
		{
			return base.DetachEntity(entity) as ViewGPSAgendaFluxoAtivosCreditos;
		}
		
		virtual public ViewGPSAgendaFluxoAtivosCreditos AttachEntity(ViewGPSAgendaFluxoAtivosCreditos entity)
		{
			return base.AttachEntity(entity) as ViewGPSAgendaFluxoAtivosCreditos;
		}
		
		virtual public void Combine(ViewGPSAgendaFluxoAtivosCreditosCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ViewGPSAgendaFluxoAtivosCreditos this[int index]
		{
			get
			{
				return base[index] as ViewGPSAgendaFluxoAtivosCreditos;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ViewGPSAgendaFluxoAtivosCreditos);
		}
	}



	[Serializable]
	abstract public class esViewGPSAgendaFluxoAtivosCreditos : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esViewGPSAgendaFluxoAtivosCreditosQuery GetDynamicQuery()
		{
			return null;
		}

		public esViewGPSAgendaFluxoAtivosCreditos()
		{

		}

		public esViewGPSAgendaFluxoAtivosCreditos(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Apelido": this.str.Apelido = (string)value; break;							
						case "DataDia": this.str.DataDia = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "PLFechamento": this.str.PLFechamento = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "RiscoFinal": this.str.RiscoFinal = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "ValorMercado": this.str.ValorMercado = (string)value; break;							
						case "DataEvento": this.str.DataEvento = (string)value; break;							
						case "TipoEvento": this.str.TipoEvento = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "Comentario": this.str.Comentario = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataDia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataDia = (System.DateTime?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "PLFechamento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PLFechamento = (System.Decimal?)value;
							break;
						
						case "RiscoFinal":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.RiscoFinal = (System.Int32?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "ValorMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMercado = (System.Decimal?)value;
							break;
						
						case "DataEvento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEvento = (System.DateTime?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.Apelido
		/// </summary>
		virtual public System.String Apelido
		{
			get
			{
				return base.GetSystemString(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Apelido);
			}
			
			set
			{
				base.SetSystemString(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Apelido, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.DataDia
		/// </summary>
		virtual public System.DateTime? DataDia
		{
			get
			{
				return base.GetSystemDateTime(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.DataDia);
			}
			
			set
			{
				base.SetSystemDateTime(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.DataDia, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.PLFechamento
		/// </summary>
		virtual public System.Decimal? PLFechamento
		{
			get
			{
				return base.GetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.PLFechamento);
			}
			
			set
			{
				base.SetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.PLFechamento, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.RiscoFinal
		/// </summary>
		virtual public System.Int32? RiscoFinal
		{
			get
			{
				return base.GetSystemInt32(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.RiscoFinal);
			}
			
			set
			{
				base.SetSystemInt32(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.RiscoFinal, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.ValorMercado
		/// </summary>
		virtual public System.Decimal? ValorMercado
		{
			get
			{
				return base.GetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorMercado);
			}
			
			set
			{
				base.SetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.DataEvento
		/// </summary>
		virtual public System.DateTime? DataEvento
		{
			get
			{
				return base.GetSystemDateTime(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.DataEvento);
			}
			
			set
			{
				base.SetSystemDateTime(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.DataEvento, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.TipoEvento
		/// </summary>
		virtual public System.String TipoEvento
		{
			get
			{
				return base.GetSystemString(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.TipoEvento);
			}
			
			set
			{
				base.SetSystemString(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.TipoEvento, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Taxa, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.Comentario
		/// </summary>
		virtual public System.String Comentario
		{
			get
			{
				return base.GetSystemString(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Comentario);
			}
			
			set
			{
				base.SetSystemString(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Comentario, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to view__GPS__Agenda_Fluxo_Ativos_Creditos.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esViewGPSAgendaFluxoAtivosCreditos entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Apelido
			{
				get
				{
					System.String data = entity.Apelido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Apelido = null;
					else entity.Apelido = Convert.ToString(value);
				}
			}
				
			public System.String DataDia
			{
				get
				{
					System.DateTime? data = entity.DataDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataDia = null;
					else entity.DataDia = Convert.ToDateTime(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String PLFechamento
			{
				get
				{
					System.Decimal? data = entity.PLFechamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PLFechamento = null;
					else entity.PLFechamento = Convert.ToDecimal(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String RiscoFinal
			{
				get
				{
					System.Int32? data = entity.RiscoFinal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RiscoFinal = null;
					else entity.RiscoFinal = Convert.ToInt32(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorMercado
			{
				get
				{
					System.Decimal? data = entity.ValorMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMercado = null;
					else entity.ValorMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataEvento
			{
				get
				{
					System.DateTime? data = entity.DataEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEvento = null;
					else entity.DataEvento = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoEvento
			{
				get
				{
					System.String data = entity.TipoEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoEvento = null;
					else entity.TipoEvento = Convert.ToString(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String Comentario
			{
				get
				{
					System.String data = entity.Comentario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Comentario = null;
					else entity.Comentario = Convert.ToString(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
			

			private esViewGPSAgendaFluxoAtivosCreditos entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esViewGPSAgendaFluxoAtivosCreditosQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esViewGPSAgendaFluxoAtivosCreditos can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}



	[Serializable]
	abstract public class esViewGPSAgendaFluxoAtivosCreditosQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ViewGPSAgendaFluxoAtivosCreditosMetadata.Meta();
			}
		}	
		

		public esQueryItem Apelido
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Apelido, esSystemType.String);
			}
		} 
		
		public esQueryItem DataDia
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.DataDia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PLFechamento
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.PLFechamento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem RiscoFinal
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.RiscoFinal, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorMercado
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataEvento
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.DataEvento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoEvento
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.TipoEvento, esSystemType.String);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Comentario
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Comentario, esSystemType.String);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ViewGPSAgendaFluxoAtivosCreditosCollection")]
	public partial class ViewGPSAgendaFluxoAtivosCreditosCollection : esViewGPSAgendaFluxoAtivosCreditosCollection, IEnumerable<ViewGPSAgendaFluxoAtivosCreditos>
	{
		public ViewGPSAgendaFluxoAtivosCreditosCollection()
		{

		}
		
		public static implicit operator List<ViewGPSAgendaFluxoAtivosCreditos>(ViewGPSAgendaFluxoAtivosCreditosCollection coll)
		{
			List<ViewGPSAgendaFluxoAtivosCreditos> list = new List<ViewGPSAgendaFluxoAtivosCreditos>();
			
			foreach (ViewGPSAgendaFluxoAtivosCreditos emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ViewGPSAgendaFluxoAtivosCreditosMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ViewGPSAgendaFluxoAtivosCreditosQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ViewGPSAgendaFluxoAtivosCreditos(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ViewGPSAgendaFluxoAtivosCreditos();
		}
		
		
		override public bool LoadAll()
		{
			return base.LoadAll(esSqlAccessType.DynamicSQL);
		}	
		
		#endregion


		[BrowsableAttribute( false )]
		public ViewGPSAgendaFluxoAtivosCreditosQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ViewGPSAgendaFluxoAtivosCreditosQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ViewGPSAgendaFluxoAtivosCreditosQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ViewGPSAgendaFluxoAtivosCreditos AddNew()
		{
			ViewGPSAgendaFluxoAtivosCreditos entity = base.AddNewEntity() as ViewGPSAgendaFluxoAtivosCreditos;
			
			return entity;
		}


		#region IEnumerable<ViewGPSAgendaFluxoAtivosCreditos> Members

		IEnumerator<ViewGPSAgendaFluxoAtivosCreditos> IEnumerable<ViewGPSAgendaFluxoAtivosCreditos>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ViewGPSAgendaFluxoAtivosCreditos;
			}
		}

		#endregion
		
		private ViewGPSAgendaFluxoAtivosCreditosQuery query;
	}


	/// <summary>
	/// Encapsulates the 'view__GPS__Agenda_Fluxo_Ativos_Creditos' view
	/// </summary>

	[Serializable]
	public partial class ViewGPSAgendaFluxoAtivosCreditos : esViewGPSAgendaFluxoAtivosCreditos
	{
		public ViewGPSAgendaFluxoAtivosCreditos()
		{

		}
	
		public ViewGPSAgendaFluxoAtivosCreditos(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ViewGPSAgendaFluxoAtivosCreditosMetadata.Meta();
			}
		}
		
		
		
		override protected esViewGPSAgendaFluxoAtivosCreditosQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ViewGPSAgendaFluxoAtivosCreditosQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ViewGPSAgendaFluxoAtivosCreditosQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ViewGPSAgendaFluxoAtivosCreditosQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ViewGPSAgendaFluxoAtivosCreditosQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ViewGPSAgendaFluxoAtivosCreditosQuery query;
	}



	[Serializable]
	public partial class ViewGPSAgendaFluxoAtivosCreditosQuery : esViewGPSAgendaFluxoAtivosCreditosQuery
	{
		public ViewGPSAgendaFluxoAtivosCreditosQuery()
		{

		}		
		
		public ViewGPSAgendaFluxoAtivosCreditosQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ViewGPSAgendaFluxoAtivosCreditosMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ViewGPSAgendaFluxoAtivosCreditosMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Apelido, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.Apelido;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.DataDia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.DataDia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Data, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.PLFechamento, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.PLFechamento;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Descricao, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Nome, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.RiscoFinal, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.RiscoFinal;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Quantidade, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 26;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorMercado, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.ValorMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.DataEvento, 9, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.DataEvento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.TipoEvento, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.TipoEvento;
			c.CharacterMaxLength = 22;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Taxa, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 26;
			c.NumericScale = 20;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorBruto, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.Comentario, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.Comentario;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorIR, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 25;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorIOF, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.ValorLiquido, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 26;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ViewGPSAgendaFluxoAtivosCreditosMetadata.ColumnNames.IdCliente, 17, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ViewGPSAgendaFluxoAtivosCreditosMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ViewGPSAgendaFluxoAtivosCreditosMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Apelido = "Apelido";
			 public const string DataDia = "DataDia";
			 public const string Data = "Data";
			 public const string PLFechamento = "PLFechamento";
			 public const string Descricao = "Descricao";
			 public const string Nome = "Nome";
			 public const string RiscoFinal = "RiscoFinal";
			 public const string Quantidade = "Quantidade";
			 public const string ValorMercado = "ValorMercado";
			 public const string DataEvento = "DataEvento";
			 public const string TipoEvento = "TipoEvento";
			 public const string Taxa = "Taxa";
			 public const string ValorBruto = "ValorBruto";
			 public const string Comentario = "Comentario";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string IdCliente = "IdCliente";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Apelido = "Apelido";
			 public const string DataDia = "DataDia";
			 public const string Data = "Data";
			 public const string PLFechamento = "PLFechamento";
			 public const string Descricao = "Descricao";
			 public const string Nome = "Nome";
			 public const string RiscoFinal = "RiscoFinal";
			 public const string Quantidade = "Quantidade";
			 public const string ValorMercado = "ValorMercado";
			 public const string DataEvento = "DataEvento";
			 public const string TipoEvento = "TipoEvento";
			 public const string Taxa = "Taxa";
			 public const string ValorBruto = "ValorBruto";
			 public const string Comentario = "Comentario";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string IdCliente = "IdCliente";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ViewGPSAgendaFluxoAtivosCreditosMetadata))
			{
				if(ViewGPSAgendaFluxoAtivosCreditosMetadata.mapDelegates == null)
				{
					ViewGPSAgendaFluxoAtivosCreditosMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ViewGPSAgendaFluxoAtivosCreditosMetadata.meta == null)
				{
					ViewGPSAgendaFluxoAtivosCreditosMetadata.meta = new ViewGPSAgendaFluxoAtivosCreditosMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Apelido", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataDia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PLFechamento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("RiscoFinal", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataEvento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoEvento", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Comentario", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "view__GPS__Agenda_Fluxo_Ativos_Creditos";
				meta.Destination = "view__GPS__Agenda_Fluxo_Ativos_Creditos";
				
				meta.spInsert = "proc_view__GPS__Agenda_Fluxo_Ativos_CreditosInsert";				
				meta.spUpdate = "proc_view__GPS__Agenda_Fluxo_Ativos_CreditosUpdate";		
				meta.spDelete = "proc_view__GPS__Agenda_Fluxo_Ativos_CreditosDelete";
				meta.spLoadAll = "proc_view__GPS__Agenda_Fluxo_Ativos_CreditosLoadAll";
				meta.spLoadByPrimaryKey = "proc_view__GPS__Agenda_Fluxo_Ativos_CreditosLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ViewGPSAgendaFluxoAtivosCreditosMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
