/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 2/11/2015 10:56:13 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Investidor
{

	[Serializable]
	abstract public class esTipoClienteCollection : esEntityCollection
	{
		public esTipoClienteCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TipoClienteCollection";
		}

		#region Query Logic
		protected void InitQuery(esTipoClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTipoClienteQuery);
		}
		#endregion
		
		virtual public TipoCliente DetachEntity(TipoCliente entity)
		{
			return base.DetachEntity(entity) as TipoCliente;
		}
		
		virtual public TipoCliente AttachEntity(TipoCliente entity)
		{
			return base.AttachEntity(entity) as TipoCliente;
		}
		
		virtual public void Combine(TipoClienteCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TipoCliente this[int index]
		{
			get
			{
				return base[index] as TipoCliente;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TipoCliente);
		}
	}



	[Serializable]
	abstract public class esTipoCliente : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTipoClienteQuery GetDynamicQuery()
		{
			return null;
		}

		public esTipoCliente()
		{

		}

		public esTipoCliente(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTipo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTipo);
			else
				return LoadByPrimaryKeyStoredProcedure(idTipo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTipo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTipoClienteQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTipo == idTipo);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTipo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTipo);
			else
				return LoadByPrimaryKeyStoredProcedure(idTipo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTipo)
		{
			esTipoClienteQuery query = this.GetDynamicQuery();
			query.Where(query.IdTipo == idTipo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTipo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTipo",idTipo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTipo": this.str.IdTipo = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTipo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTipo = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TipoCliente.IdTipo
		/// </summary>
		virtual public System.Int32? IdTipo
		{
			get
			{
				return base.GetSystemInt32(TipoClienteMetadata.ColumnNames.IdTipo);
			}
			
			set
			{
				base.SetSystemInt32(TipoClienteMetadata.ColumnNames.IdTipo, value);
			}
		}
		
		/// <summary>
		/// Maps to TipoCliente.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(TipoClienteMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(TipoClienteMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTipoCliente entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTipo
			{
				get
				{
					System.Int32? data = entity.IdTipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTipo = null;
					else entity.IdTipo = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esTipoCliente entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTipoClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTipoCliente can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TipoCliente : esTipoCliente
	{

				
		#region ClienteCollectionByIdTipo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TipoCliente_Cliente_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteCollection ClienteCollectionByIdTipo
		{
			get
			{
				if(this._ClienteCollectionByIdTipo == null)
				{
					this._ClienteCollectionByIdTipo = new ClienteCollection();
					this._ClienteCollectionByIdTipo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ClienteCollectionByIdTipo", this._ClienteCollectionByIdTipo);
				
					if(this.IdTipo != null)
					{
						this._ClienteCollectionByIdTipo.Query.Where(this._ClienteCollectionByIdTipo.Query.IdTipo == this.IdTipo);
						this._ClienteCollectionByIdTipo.Query.Load();

						// Auto-hookup Foreign Keys
						this._ClienteCollectionByIdTipo.fks.Add(ClienteMetadata.ColumnNames.IdTipo, this.IdTipo);
					}
				}

				return this._ClienteCollectionByIdTipo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClienteCollectionByIdTipo != null) 
				{ 
					this.RemovePostSave("ClienteCollectionByIdTipo"); 
					this._ClienteCollectionByIdTipo = null;
					
				} 
			} 			
		}

		private ClienteCollection _ClienteCollectionByIdTipo;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "ClienteCollectionByIdTipo", typeof(ClienteCollection), new Cliente()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTipoClienteQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TipoClienteMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTipo
		{
			get
			{
				return new esQueryItem(this, TipoClienteMetadata.ColumnNames.IdTipo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, TipoClienteMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TipoClienteCollection")]
	public partial class TipoClienteCollection : esTipoClienteCollection, IEnumerable<TipoCliente>
	{
		public TipoClienteCollection()
		{

		}
		
		public static implicit operator List<TipoCliente>(TipoClienteCollection coll)
		{
			List<TipoCliente> list = new List<TipoCliente>();
			
			foreach (TipoCliente emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TipoClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TipoClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TipoCliente(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TipoCliente();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TipoClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TipoClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TipoClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TipoCliente AddNew()
		{
			TipoCliente entity = base.AddNewEntity() as TipoCliente;
			
			return entity;
		}

		public TipoCliente FindByPrimaryKey(System.Int32 idTipo)
		{
			return base.FindByPrimaryKey(idTipo) as TipoCliente;
		}


		#region IEnumerable<TipoCliente> Members

		IEnumerator<TipoCliente> IEnumerable<TipoCliente>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TipoCliente;
			}
		}

		#endregion
		
		private TipoClienteQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TipoCliente' table
	/// </summary>

	[Serializable]
	public partial class TipoCliente : esTipoCliente
	{
		public TipoCliente()
		{

		}
	
		public TipoCliente(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TipoClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esTipoClienteQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TipoClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TipoClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TipoClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TipoClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TipoClienteQuery query;
	}



	[Serializable]
	public partial class TipoClienteQuery : esTipoClienteQuery
	{
		public TipoClienteQuery()
		{

		}		
		
		public TipoClienteQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TipoClienteMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TipoClienteMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TipoClienteMetadata.ColumnNames.IdTipo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TipoClienteMetadata.PropertyNames.IdTipo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TipoClienteMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TipoClienteMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TipoClienteMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTipo = "IdTipo";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTipo = "IdTipo";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TipoClienteMetadata))
			{
				if(TipoClienteMetadata.mapDelegates == null)
				{
					TipoClienteMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TipoClienteMetadata.meta == null)
				{
					TipoClienteMetadata.meta = new TipoClienteMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTipo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "TipoCliente";
				meta.Destination = "TipoCliente";
				
				meta.spInsert = "proc_TipoClienteInsert";				
				meta.spUpdate = "proc_TipoClienteUpdate";		
				meta.spDelete = "proc_TipoClienteDelete";
				meta.spLoadAll = "proc_TipoClienteLoadAll";
				meta.spLoadByPrimaryKey = "proc_TipoClienteLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TipoClienteMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
