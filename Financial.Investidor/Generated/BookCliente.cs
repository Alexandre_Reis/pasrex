/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 2/11/2015 10:55:58 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Investidor
{

	[Serializable]
	abstract public class esBookClienteCollection : esEntityCollection
	{
		public esBookClienteCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "BookClienteCollection";
		}

		#region Query Logic
		protected void InitQuery(esBookClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esBookClienteQuery);
		}
		#endregion
		
		virtual public BookCliente DetachEntity(BookCliente entity)
		{
			return base.DetachEntity(entity) as BookCliente;
		}
		
		virtual public BookCliente AttachEntity(BookCliente entity)
		{
			return base.AttachEntity(entity) as BookCliente;
		}
		
		virtual public void Combine(BookClienteCollection collection)
		{
			base.Combine(collection);
		}
		
		new public BookCliente this[int index]
		{
			get
			{
				return base[index] as BookCliente;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(BookCliente);
		}
	}



	[Serializable]
	abstract public class esBookCliente : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esBookClienteQuery GetDynamicQuery()
		{
			return null;
		}

		public esBookCliente()
		{

		}

		public esBookCliente(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idBook, System.Int32 idCliente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBook, idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idBook, idCliente);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idBook, System.Int32 idCliente)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esBookClienteQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdBook == idBook, query.IdCliente == idCliente);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idBook, System.Int32 idCliente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBook, idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idBook, idCliente);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idBook, System.Int32 idCliente)
		{
			esBookClienteQuery query = this.GetDynamicQuery();
			query.Where(query.IdBook == idBook, query.IdCliente == idCliente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idBook, System.Int32 idCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdBook",idBook);			parms.Add("IdCliente",idCliente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdBook": this.str.IdBook = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "Imprime": this.str.Imprime = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdBook":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdBook = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to BookCliente.IdBook
		/// </summary>
		virtual public System.Int32? IdBook
		{
			get
			{
				return base.GetSystemInt32(BookClienteMetadata.ColumnNames.IdBook);
			}
			
			set
			{
				if(base.SetSystemInt32(BookClienteMetadata.ColumnNames.IdBook, value))
				{
					this._UpToBookByIdBook = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to BookCliente.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(BookClienteMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(BookClienteMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to BookCliente.Imprime
		/// </summary>
		virtual public System.String Imprime
		{
			get
			{
				return base.GetSystemString(BookClienteMetadata.ColumnNames.Imprime);
			}
			
			set
			{
				base.SetSystemString(BookClienteMetadata.ColumnNames.Imprime, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Book _UpToBookByIdBook;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esBookCliente entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdBook
			{
				get
				{
					System.Int32? data = entity.IdBook;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdBook = null;
					else entity.IdBook = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String Imprime
			{
				get
				{
					System.String data = entity.Imprime;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Imprime = null;
					else entity.Imprime = Convert.ToString(value);
				}
			}
			

			private esBookCliente entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esBookClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esBookCliente can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class BookCliente : esBookCliente
	{

				
		#region UpToBookByIdBook - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Book_BookCliente_FK1
		/// </summary>

		[XmlIgnore]
		public Book UpToBookByIdBook
		{
			get
			{
				if(this._UpToBookByIdBook == null
					&& IdBook != null					)
				{
					this._UpToBookByIdBook = new Book();
					this._UpToBookByIdBook.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToBookByIdBook", this._UpToBookByIdBook);
					this._UpToBookByIdBook.Query.Where(this._UpToBookByIdBook.Query.IdBook == this.IdBook);
					this._UpToBookByIdBook.Query.Load();
				}

				return this._UpToBookByIdBook;
			}
			
			set
			{
				this.RemovePreSave("UpToBookByIdBook");
				

				if(value == null)
				{
					this.IdBook = null;
					this._UpToBookByIdBook = null;
				}
				else
				{
					this.IdBook = value.IdBook;
					this._UpToBookByIdBook = value;
					this.SetPreSave("UpToBookByIdBook", this._UpToBookByIdBook);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_BookCliente_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToBookByIdBook != null)
			{
				this.IdBook = this._UpToBookByIdBook.IdBook;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esBookClienteQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return BookClienteMetadata.Meta();
			}
		}	
		

		public esQueryItem IdBook
		{
			get
			{
				return new esQueryItem(this, BookClienteMetadata.ColumnNames.IdBook, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, BookClienteMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Imprime
		{
			get
			{
				return new esQueryItem(this, BookClienteMetadata.ColumnNames.Imprime, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("BookClienteCollection")]
	public partial class BookClienteCollection : esBookClienteCollection, IEnumerable<BookCliente>
	{
		public BookClienteCollection()
		{

		}
		
		public static implicit operator List<BookCliente>(BookClienteCollection coll)
		{
			List<BookCliente> list = new List<BookCliente>();
			
			foreach (BookCliente emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  BookClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BookClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new BookCliente(row);
		}

		override protected esEntity CreateEntity()
		{
			return new BookCliente();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public BookClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BookClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(BookClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public BookCliente AddNew()
		{
			BookCliente entity = base.AddNewEntity() as BookCliente;
			
			return entity;
		}

		public BookCliente FindByPrimaryKey(System.Int32 idBook, System.Int32 idCliente)
		{
			return base.FindByPrimaryKey(idBook, idCliente) as BookCliente;
		}


		#region IEnumerable<BookCliente> Members

		IEnumerator<BookCliente> IEnumerable<BookCliente>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as BookCliente;
			}
		}

		#endregion
		
		private BookClienteQuery query;
	}


	/// <summary>
	/// Encapsulates the 'BookCliente' table
	/// </summary>

	[Serializable]
	public partial class BookCliente : esBookCliente
	{
		public BookCliente()
		{

		}
	
		public BookCliente(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return BookClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esBookClienteQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BookClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public BookClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BookClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(BookClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private BookClienteQuery query;
	}



	[Serializable]
	public partial class BookClienteQuery : esBookClienteQuery
	{
		public BookClienteQuery()
		{

		}		
		
		public BookClienteQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class BookClienteMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected BookClienteMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(BookClienteMetadata.ColumnNames.IdBook, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BookClienteMetadata.PropertyNames.IdBook;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BookClienteMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BookClienteMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BookClienteMetadata.ColumnNames.Imprime, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = BookClienteMetadata.PropertyNames.Imprime;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public BookClienteMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdBook = "IdBook";
			 public const string IdCliente = "IdCliente";
			 public const string Imprime = "Imprime";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdBook = "IdBook";
			 public const string IdCliente = "IdCliente";
			 public const string Imprime = "Imprime";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(BookClienteMetadata))
			{
				if(BookClienteMetadata.mapDelegates == null)
				{
					BookClienteMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (BookClienteMetadata.meta == null)
				{
					BookClienteMetadata.meta = new BookClienteMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdBook", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Imprime", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "BookCliente";
				meta.Destination = "BookCliente";
				
				meta.spInsert = "proc_BookClienteInsert";				
				meta.spUpdate = "proc_BookClienteUpdate";		
				meta.spDelete = "proc_BookClienteDelete";
				meta.spLoadAll = "proc_BookClienteLoadAll";
				meta.spLoadByPrimaryKey = "proc_BookClienteLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private BookClienteMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
