/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 2/11/2015 10:56:12 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityOpcaoCollection : esEntityCollection
	{
		public esSuitabilityOpcaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityOpcaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityOpcaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityOpcaoQuery);
		}
		#endregion
		
		virtual public SuitabilityOpcao DetachEntity(SuitabilityOpcao entity)
		{
			return base.DetachEntity(entity) as SuitabilityOpcao;
		}
		
		virtual public SuitabilityOpcao AttachEntity(SuitabilityOpcao entity)
		{
			return base.AttachEntity(entity) as SuitabilityOpcao;
		}
		
		virtual public void Combine(SuitabilityOpcaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityOpcao this[int index]
		{
			get
			{
				return base[index] as SuitabilityOpcao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityOpcao);
		}
	}



	[Serializable]
	abstract public class esSuitabilityOpcao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityOpcaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityOpcao()
		{

		}

		public esSuitabilityOpcao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOpcao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOpcao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOpcao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idOpcao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esSuitabilityOpcaoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdOpcao == idOpcao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOpcao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOpcao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOpcao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOpcao)
		{
			esSuitabilityOpcaoQuery query = this.GetDynamicQuery();
			query.Where(query.IdOpcao == idOpcao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOpcao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOpcao",idOpcao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOpcao": this.str.IdOpcao = (string)value; break;							
						case "IdQuestao": this.str.IdQuestao = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Pontos": this.str.Pontos = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOpcao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOpcao = (System.Int32?)value;
							break;
						
						case "IdQuestao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdQuestao = (System.Int32?)value;
							break;
						
						case "Pontos":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Pontos = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityOpcao.IdOpcao
		/// </summary>
		virtual public System.Int32? IdOpcao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityOpcaoMetadata.ColumnNames.IdOpcao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityOpcaoMetadata.ColumnNames.IdOpcao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityOpcao.IdQuestao
		/// </summary>
		virtual public System.Int32? IdQuestao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityOpcaoMetadata.ColumnNames.IdQuestao);
			}
			
			set
			{
				if(base.SetSystemInt32(SuitabilityOpcaoMetadata.ColumnNames.IdQuestao, value))
				{
					this._UpToSuitabilityQuestaoByIdQuestao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityOpcao.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(SuitabilityOpcaoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityOpcaoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityOpcao.Pontos
		/// </summary>
		virtual public System.Int32? Pontos
		{
			get
			{
				return base.GetSystemInt32(SuitabilityOpcaoMetadata.ColumnNames.Pontos);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityOpcaoMetadata.ColumnNames.Pontos, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected SuitabilityQuestao _UpToSuitabilityQuestaoByIdQuestao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityOpcao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOpcao
			{
				get
				{
					System.Int32? data = entity.IdOpcao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOpcao = null;
					else entity.IdOpcao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdQuestao
			{
				get
				{
					System.Int32? data = entity.IdQuestao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdQuestao = null;
					else entity.IdQuestao = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Pontos
			{
				get
				{
					System.Int32? data = entity.Pontos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pontos = null;
					else entity.Pontos = Convert.ToInt32(value);
				}
			}
			

			private esSuitabilityOpcao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityOpcaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityOpcao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityOpcao : esSuitabilityOpcao
	{

				
		#region SuitabilityRespostaCollectionByIdOpcaoEscolhida - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_SuitabilityResposta_SuitabilityOpcao
		/// </summary>

		[XmlIgnore]
		public SuitabilityRespostaCollection SuitabilityRespostaCollectionByIdOpcaoEscolhida
		{
			get
			{
				if(this._SuitabilityRespostaCollectionByIdOpcaoEscolhida == null)
				{
					this._SuitabilityRespostaCollectionByIdOpcaoEscolhida = new SuitabilityRespostaCollection();
					this._SuitabilityRespostaCollectionByIdOpcaoEscolhida.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("SuitabilityRespostaCollectionByIdOpcaoEscolhida", this._SuitabilityRespostaCollectionByIdOpcaoEscolhida);
				
					if(this.IdOpcao != null)
					{
						this._SuitabilityRespostaCollectionByIdOpcaoEscolhida.Query.Where(this._SuitabilityRespostaCollectionByIdOpcaoEscolhida.Query.IdOpcaoEscolhida == this.IdOpcao);
						this._SuitabilityRespostaCollectionByIdOpcaoEscolhida.Query.Load();

						// Auto-hookup Foreign Keys
						this._SuitabilityRespostaCollectionByIdOpcaoEscolhida.fks.Add(SuitabilityRespostaMetadata.ColumnNames.IdOpcaoEscolhida, this.IdOpcao);
					}
				}

				return this._SuitabilityRespostaCollectionByIdOpcaoEscolhida;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._SuitabilityRespostaCollectionByIdOpcaoEscolhida != null) 
				{ 
					this.RemovePostSave("SuitabilityRespostaCollectionByIdOpcaoEscolhida"); 
					this._SuitabilityRespostaCollectionByIdOpcaoEscolhida = null;
					
				} 
			} 			
		}

		private SuitabilityRespostaCollection _SuitabilityRespostaCollectionByIdOpcaoEscolhida;
		#endregion

				
		#region UpToSuitabilityQuestaoByIdQuestao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_SuitabilityOpcao_SuitabilityQuestao
		/// </summary>

		[XmlIgnore]
		public SuitabilityQuestao UpToSuitabilityQuestaoByIdQuestao
		{
			get
			{
				if(this._UpToSuitabilityQuestaoByIdQuestao == null
					&& IdQuestao != null					)
				{
					this._UpToSuitabilityQuestaoByIdQuestao = new SuitabilityQuestao();
					this._UpToSuitabilityQuestaoByIdQuestao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSuitabilityQuestaoByIdQuestao", this._UpToSuitabilityQuestaoByIdQuestao);
					this._UpToSuitabilityQuestaoByIdQuestao.Query.Where(this._UpToSuitabilityQuestaoByIdQuestao.Query.IdQuestao == this.IdQuestao);
					this._UpToSuitabilityQuestaoByIdQuestao.Query.Load();
				}

				return this._UpToSuitabilityQuestaoByIdQuestao;
			}
			
			set
			{
				this.RemovePreSave("UpToSuitabilityQuestaoByIdQuestao");
				

				if(value == null)
				{
					this.IdQuestao = null;
					this._UpToSuitabilityQuestaoByIdQuestao = null;
				}
				else
				{
					this.IdQuestao = value.IdQuestao;
					this._UpToSuitabilityQuestaoByIdQuestao = value;
					this.SetPreSave("UpToSuitabilityQuestaoByIdQuestao", this._UpToSuitabilityQuestaoByIdQuestao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "SuitabilityRespostaCollectionByIdOpcaoEscolhida", typeof(SuitabilityRespostaCollection), new SuitabilityResposta()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToSuitabilityQuestaoByIdQuestao != null)
			{
				this.IdQuestao = this._UpToSuitabilityQuestaoByIdQuestao.IdQuestao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._SuitabilityRespostaCollectionByIdOpcaoEscolhida != null)
			{
				foreach(SuitabilityResposta obj in this._SuitabilityRespostaCollectionByIdOpcaoEscolhida)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOpcaoEscolhida = this.IdOpcao;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityOpcaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityOpcaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOpcao
		{
			get
			{
				return new esQueryItem(this, SuitabilityOpcaoMetadata.ColumnNames.IdOpcao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdQuestao
		{
			get
			{
				return new esQueryItem(this, SuitabilityOpcaoMetadata.ColumnNames.IdQuestao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, SuitabilityOpcaoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Pontos
		{
			get
			{
				return new esQueryItem(this, SuitabilityOpcaoMetadata.ColumnNames.Pontos, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityOpcaoCollection")]
	public partial class SuitabilityOpcaoCollection : esSuitabilityOpcaoCollection, IEnumerable<SuitabilityOpcao>
	{
		public SuitabilityOpcaoCollection()
		{

		}
		
		public static implicit operator List<SuitabilityOpcao>(SuitabilityOpcaoCollection coll)
		{
			List<SuitabilityOpcao> list = new List<SuitabilityOpcao>();
			
			foreach (SuitabilityOpcao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityOpcaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityOpcaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityOpcao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityOpcao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityOpcaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityOpcaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityOpcaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityOpcao AddNew()
		{
			SuitabilityOpcao entity = base.AddNewEntity() as SuitabilityOpcao;
			
			return entity;
		}

		public SuitabilityOpcao FindByPrimaryKey(System.Int32 idOpcao)
		{
			return base.FindByPrimaryKey(idOpcao) as SuitabilityOpcao;
		}


		#region IEnumerable<SuitabilityOpcao> Members

		IEnumerator<SuitabilityOpcao> IEnumerable<SuitabilityOpcao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityOpcao;
			}
		}

		#endregion
		
		private SuitabilityOpcaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityOpcao' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityOpcao : esSuitabilityOpcao
	{
		public SuitabilityOpcao()
		{

		}
	
		public SuitabilityOpcao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityOpcaoMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityOpcaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityOpcaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityOpcaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityOpcaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityOpcaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityOpcaoQuery query;
	}



	[Serializable]
	public partial class SuitabilityOpcaoQuery : esSuitabilityOpcaoQuery
	{
		public SuitabilityOpcaoQuery()
		{

		}		
		
		public SuitabilityOpcaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityOpcaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityOpcaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityOpcaoMetadata.ColumnNames.IdOpcao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityOpcaoMetadata.PropertyNames.IdOpcao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityOpcaoMetadata.ColumnNames.IdQuestao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityOpcaoMetadata.PropertyNames.IdQuestao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityOpcaoMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityOpcaoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 2000;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityOpcaoMetadata.ColumnNames.Pontos, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityOpcaoMetadata.PropertyNames.Pontos;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityOpcaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOpcao = "IdOpcao";
			 public const string IdQuestao = "IdQuestao";
			 public const string Descricao = "Descricao";
			 public const string Pontos = "Pontos";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOpcao = "IdOpcao";
			 public const string IdQuestao = "IdQuestao";
			 public const string Descricao = "Descricao";
			 public const string Pontos = "Pontos";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityOpcaoMetadata))
			{
				if(SuitabilityOpcaoMetadata.mapDelegates == null)
				{
					SuitabilityOpcaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityOpcaoMetadata.meta == null)
				{
					SuitabilityOpcaoMetadata.meta = new SuitabilityOpcaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOpcao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdQuestao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Pontos", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "SuitabilityOpcao";
				meta.Destination = "SuitabilityOpcao";
				
				meta.spInsert = "proc_SuitabilityOpcaoInsert";				
				meta.spUpdate = "proc_SuitabilityOpcaoUpdate";		
				meta.spDelete = "proc_SuitabilityOpcaoDelete";
				meta.spLoadAll = "proc_SuitabilityOpcaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityOpcaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityOpcaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
