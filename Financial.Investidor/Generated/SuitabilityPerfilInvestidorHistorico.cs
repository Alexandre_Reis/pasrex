/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 27/08/2015 11:01:53
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityPerfilInvestidorHistoricoCollection : esEntityCollection
	{
		public esSuitabilityPerfilInvestidorHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityPerfilInvestidorHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityPerfilInvestidorHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityPerfilInvestidorHistoricoQuery);
		}
		#endregion
		
		virtual public SuitabilityPerfilInvestidorHistorico DetachEntity(SuitabilityPerfilInvestidorHistorico entity)
		{
			return base.DetachEntity(entity) as SuitabilityPerfilInvestidorHistorico;
		}
		
		virtual public SuitabilityPerfilInvestidorHistorico AttachEntity(SuitabilityPerfilInvestidorHistorico entity)
		{
			return base.AttachEntity(entity) as SuitabilityPerfilInvestidorHistorico;
		}
		
		virtual public void Combine(SuitabilityPerfilInvestidorHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityPerfilInvestidorHistorico this[int index]
		{
			get
			{
				return base[index] as SuitabilityPerfilInvestidorHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityPerfilInvestidorHistorico);
		}
	}



	[Serializable]
	abstract public class esSuitabilityPerfilInvestidorHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityPerfilInvestidorHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityPerfilInvestidorHistorico()
		{

		}

		public esSuitabilityPerfilInvestidorHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idPerfilInvestidor)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idPerfilInvestidor);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idPerfilInvestidor);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idPerfilInvestidor)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idPerfilInvestidor);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idPerfilInvestidor);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idPerfilInvestidor)
		{
			esSuitabilityPerfilInvestidorHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdPerfilInvestidor == idPerfilInvestidor);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idPerfilInvestidor)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdPerfilInvestidor",idPerfilInvestidor);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdPerfilInvestidor": this.str.IdPerfilInvestidor = (string)value; break;							
						case "Nivel": this.str.Nivel = (string)value; break;							
						case "Perfil": this.str.Perfil = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdPerfilInvestidor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilInvestidor = (System.Int32?)value;
							break;
						
						case "Nivel":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Nivel = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityPerfilInvestidorHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfilInvestidorHistorico.IdPerfilInvestidor
		/// </summary>
		virtual public System.Int32? IdPerfilInvestidor
		{
			get
			{
				return base.GetSystemInt32(SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.IdPerfilInvestidor);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.IdPerfilInvestidor, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfilInvestidorHistorico.Nivel
		/// </summary>
		virtual public System.Int32? Nivel
		{
			get
			{
				return base.GetSystemInt32(SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.Nivel);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.Nivel, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfilInvestidorHistorico.Perfil
		/// </summary>
		virtual public System.String Perfil
		{
			get
			{
				return base.GetSystemString(SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.Perfil);
			}
			
			set
			{
				base.SetSystemString(SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.Perfil, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfilInvestidorHistorico.Tipo
		/// </summary>
		virtual public System.String Tipo
		{
			get
			{
				return base.GetSystemString(SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemString(SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityPerfilInvestidorHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdPerfilInvestidor
			{
				get
				{
					System.Int32? data = entity.IdPerfilInvestidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilInvestidor = null;
					else entity.IdPerfilInvestidor = Convert.ToInt32(value);
				}
			}
				
			public System.String Nivel
			{
				get
				{
					System.Int32? data = entity.Nivel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nivel = null;
					else entity.Nivel = Convert.ToInt32(value);
				}
			}
				
			public System.String Perfil
			{
				get
				{
					System.String data = entity.Perfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Perfil = null;
					else entity.Perfil = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.String data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToString(value);
				}
			}
			

			private esSuitabilityPerfilInvestidorHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityPerfilInvestidorHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityPerfilInvestidorHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityPerfilInvestidorHistorico : esSuitabilityPerfilInvestidorHistorico
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityPerfilInvestidorHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityPerfilInvestidorHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdPerfilInvestidor
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.IdPerfilInvestidor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nivel
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.Nivel, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Perfil
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.Perfil, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.Tipo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityPerfilInvestidorHistoricoCollection")]
	public partial class SuitabilityPerfilInvestidorHistoricoCollection : esSuitabilityPerfilInvestidorHistoricoCollection, IEnumerable<SuitabilityPerfilInvestidorHistorico>
	{
		public SuitabilityPerfilInvestidorHistoricoCollection()
		{

		}
		
		public static implicit operator List<SuitabilityPerfilInvestidorHistorico>(SuitabilityPerfilInvestidorHistoricoCollection coll)
		{
			List<SuitabilityPerfilInvestidorHistorico> list = new List<SuitabilityPerfilInvestidorHistorico>();
			
			foreach (SuitabilityPerfilInvestidorHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityPerfilInvestidorHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityPerfilInvestidorHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityPerfilInvestidorHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityPerfilInvestidorHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityPerfilInvestidorHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityPerfilInvestidorHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityPerfilInvestidorHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityPerfilInvestidorHistorico AddNew()
		{
			SuitabilityPerfilInvestidorHistorico entity = base.AddNewEntity() as SuitabilityPerfilInvestidorHistorico;
			
			return entity;
		}

		public SuitabilityPerfilInvestidorHistorico FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idPerfilInvestidor)
		{
			return base.FindByPrimaryKey(dataHistorico, idPerfilInvestidor) as SuitabilityPerfilInvestidorHistorico;
		}


		#region IEnumerable<SuitabilityPerfilInvestidorHistorico> Members

		IEnumerator<SuitabilityPerfilInvestidorHistorico> IEnumerable<SuitabilityPerfilInvestidorHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityPerfilInvestidorHistorico;
			}
		}

		#endregion
		
		private SuitabilityPerfilInvestidorHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityPerfilInvestidorHistorico' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityPerfilInvestidorHistorico : esSuitabilityPerfilInvestidorHistorico
	{
		public SuitabilityPerfilInvestidorHistorico()
		{

		}
	
		public SuitabilityPerfilInvestidorHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityPerfilInvestidorHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityPerfilInvestidorHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityPerfilInvestidorHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityPerfilInvestidorHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityPerfilInvestidorHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityPerfilInvestidorHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityPerfilInvestidorHistoricoQuery query;
	}



	[Serializable]
	public partial class SuitabilityPerfilInvestidorHistoricoQuery : esSuitabilityPerfilInvestidorHistoricoQuery
	{
		public SuitabilityPerfilInvestidorHistoricoQuery()
		{

		}		
		
		public SuitabilityPerfilInvestidorHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityPerfilInvestidorHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityPerfilInvestidorHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SuitabilityPerfilInvestidorHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.IdPerfilInvestidor, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityPerfilInvestidorHistoricoMetadata.PropertyNames.IdPerfilInvestidor;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.Nivel, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityPerfilInvestidorHistoricoMetadata.PropertyNames.Nivel;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.Perfil, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityPerfilInvestidorHistoricoMetadata.PropertyNames.Perfil;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilInvestidorHistoricoMetadata.ColumnNames.Tipo, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityPerfilInvestidorHistoricoMetadata.PropertyNames.Tipo;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityPerfilInvestidorHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdPerfilInvestidor = "IdPerfilInvestidor";
			 public const string Nivel = "Nivel";
			 public const string Perfil = "Perfil";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdPerfilInvestidor = "IdPerfilInvestidor";
			 public const string Nivel = "Nivel";
			 public const string Perfil = "Perfil";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityPerfilInvestidorHistoricoMetadata))
			{
				if(SuitabilityPerfilInvestidorHistoricoMetadata.mapDelegates == null)
				{
					SuitabilityPerfilInvestidorHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityPerfilInvestidorHistoricoMetadata.meta == null)
				{
					SuitabilityPerfilInvestidorHistoricoMetadata.meta = new SuitabilityPerfilInvestidorHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdPerfilInvestidor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nivel", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Perfil", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "SuitabilityPerfilInvestidorHistorico";
				meta.Destination = "SuitabilityPerfilInvestidorHistorico";
				
				meta.spInsert = "proc_SuitabilityPerfilInvestidorHistoricoInsert";				
				meta.spUpdate = "proc_SuitabilityPerfilInvestidorHistoricoUpdate";		
				meta.spDelete = "proc_SuitabilityPerfilInvestidorHistoricoDelete";
				meta.spLoadAll = "proc_SuitabilityPerfilInvestidorHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityPerfilInvestidorHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityPerfilInvestidorHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
