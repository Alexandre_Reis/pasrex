/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 09/09/2015 12:26:54
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityMensagensCollection : esEntityCollection
	{
		public esSuitabilityMensagensCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityMensagensCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityMensagensQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityMensagensQuery);
		}
		#endregion
		
		virtual public SuitabilityMensagens DetachEntity(SuitabilityMensagens entity)
		{
			return base.DetachEntity(entity) as SuitabilityMensagens;
		}
		
		virtual public SuitabilityMensagens AttachEntity(SuitabilityMensagens entity)
		{
			return base.AttachEntity(entity) as SuitabilityMensagens;
		}
		
		virtual public void Combine(SuitabilityMensagensCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityMensagens this[int index]
		{
			get
			{
				return base[index] as SuitabilityMensagens;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityMensagens);
		}
	}



	[Serializable]
	abstract public class esSuitabilityMensagens : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityMensagensQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityMensagens()
		{

		}

		public esSuitabilityMensagens(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idMensagem)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idMensagem);
			else
				return LoadByPrimaryKeyStoredProcedure(idMensagem);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idMensagem)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idMensagem);
			else
				return LoadByPrimaryKeyStoredProcedure(idMensagem);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idMensagem)
		{
			esSuitabilityMensagensQuery query = this.GetDynamicQuery();
			query.Where(query.IdMensagem == idMensagem);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idMensagem)
		{
			esParameters parms = new esParameters();
			parms.Add("IdMensagem",idMensagem);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdMensagem": this.str.IdMensagem = (string)value; break;							
						case "Assunto": this.str.Assunto = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "ControlaConcordancia": this.str.ControlaConcordancia = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdMensagem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMensagem = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityMensagens.IdMensagem
		/// </summary>
		virtual public System.Int32? IdMensagem
		{
			get
			{
				return base.GetSystemInt32(SuitabilityMensagensMetadata.ColumnNames.IdMensagem);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityMensagensMetadata.ColumnNames.IdMensagem, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityMensagens.Assunto
		/// </summary>
		virtual public System.String Assunto
		{
			get
			{
				return base.GetSystemString(SuitabilityMensagensMetadata.ColumnNames.Assunto);
			}
			
			set
			{
				base.SetSystemString(SuitabilityMensagensMetadata.ColumnNames.Assunto, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityMensagens.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(SuitabilityMensagensMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityMensagensMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityMensagens.ControlaConcordancia
		/// </summary>
		virtual public System.String ControlaConcordancia
		{
			get
			{
				return base.GetSystemString(SuitabilityMensagensMetadata.ColumnNames.ControlaConcordancia);
			}
			
			set
			{
				base.SetSystemString(SuitabilityMensagensMetadata.ColumnNames.ControlaConcordancia, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityMensagens entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdMensagem
			{
				get
				{
					System.Int32? data = entity.IdMensagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMensagem = null;
					else entity.IdMensagem = Convert.ToInt32(value);
				}
			}
				
			public System.String Assunto
			{
				get
				{
					System.String data = entity.Assunto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Assunto = null;
					else entity.Assunto = Convert.ToString(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String ControlaConcordancia
			{
				get
				{
					System.String data = entity.ControlaConcordancia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ControlaConcordancia = null;
					else entity.ControlaConcordancia = Convert.ToString(value);
				}
			}
			

			private esSuitabilityMensagens entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityMensagensQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityMensagens can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityMensagens : esSuitabilityMensagens
	{

				
		#region SuitabilityEventosCollectionByIdMensagem - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - SuitabilityEventos_SuitabilityMensagens_FK
		/// </summary>

		[XmlIgnore]
		public SuitabilityEventosCollection SuitabilityEventosCollectionByIdMensagem
		{
			get
			{
				if(this._SuitabilityEventosCollectionByIdMensagem == null)
				{
					this._SuitabilityEventosCollectionByIdMensagem = new SuitabilityEventosCollection();
					this._SuitabilityEventosCollectionByIdMensagem.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("SuitabilityEventosCollectionByIdMensagem", this._SuitabilityEventosCollectionByIdMensagem);
				
					if(this.IdMensagem != null)
					{
						this._SuitabilityEventosCollectionByIdMensagem.Query.Where(this._SuitabilityEventosCollectionByIdMensagem.Query.IdMensagem == this.IdMensagem);
						this._SuitabilityEventosCollectionByIdMensagem.Query.Load();

						// Auto-hookup Foreign Keys
						this._SuitabilityEventosCollectionByIdMensagem.fks.Add(SuitabilityEventosMetadata.ColumnNames.IdMensagem, this.IdMensagem);
					}
				}

				return this._SuitabilityEventosCollectionByIdMensagem;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._SuitabilityEventosCollectionByIdMensagem != null) 
				{ 
					this.RemovePostSave("SuitabilityEventosCollectionByIdMensagem"); 
					this._SuitabilityEventosCollectionByIdMensagem = null;
					
				} 
			} 			
		}

		private SuitabilityEventosCollection _SuitabilityEventosCollectionByIdMensagem;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "SuitabilityEventosCollectionByIdMensagem", typeof(SuitabilityEventosCollection), new SuitabilityEventos()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._SuitabilityEventosCollectionByIdMensagem != null)
			{
				foreach(SuitabilityEventos obj in this._SuitabilityEventosCollectionByIdMensagem)
				{
					if(obj.es.IsAdded)
					{
						obj.IdMensagem = this.IdMensagem;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityMensagensQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityMensagensMetadata.Meta();
			}
		}	
		

		public esQueryItem IdMensagem
		{
			get
			{
				return new esQueryItem(this, SuitabilityMensagensMetadata.ColumnNames.IdMensagem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Assunto
		{
			get
			{
				return new esQueryItem(this, SuitabilityMensagensMetadata.ColumnNames.Assunto, esSystemType.String);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, SuitabilityMensagensMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem ControlaConcordancia
		{
			get
			{
				return new esQueryItem(this, SuitabilityMensagensMetadata.ColumnNames.ControlaConcordancia, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityMensagensCollection")]
	public partial class SuitabilityMensagensCollection : esSuitabilityMensagensCollection, IEnumerable<SuitabilityMensagens>
	{
		public SuitabilityMensagensCollection()
		{

		}
		
		public static implicit operator List<SuitabilityMensagens>(SuitabilityMensagensCollection coll)
		{
			List<SuitabilityMensagens> list = new List<SuitabilityMensagens>();
			
			foreach (SuitabilityMensagens emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityMensagensMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityMensagensQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityMensagens(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityMensagens();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityMensagensQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityMensagensQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityMensagensQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityMensagens AddNew()
		{
			SuitabilityMensagens entity = base.AddNewEntity() as SuitabilityMensagens;
			
			return entity;
		}

		public SuitabilityMensagens FindByPrimaryKey(System.Int32 idMensagem)
		{
			return base.FindByPrimaryKey(idMensagem) as SuitabilityMensagens;
		}


		#region IEnumerable<SuitabilityMensagens> Members

		IEnumerator<SuitabilityMensagens> IEnumerable<SuitabilityMensagens>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityMensagens;
			}
		}

		#endregion
		
		private SuitabilityMensagensQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityMensagens' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityMensagens : esSuitabilityMensagens
	{
		public SuitabilityMensagens()
		{

		}
	
		public SuitabilityMensagens(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityMensagensMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityMensagensQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityMensagensQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityMensagensQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityMensagensQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityMensagensQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityMensagensQuery query;
	}



	[Serializable]
	public partial class SuitabilityMensagensQuery : esSuitabilityMensagensQuery
	{
		public SuitabilityMensagensQuery()
		{

		}		
		
		public SuitabilityMensagensQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityMensagensMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityMensagensMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityMensagensMetadata.ColumnNames.IdMensagem, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityMensagensMetadata.PropertyNames.IdMensagem;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityMensagensMetadata.ColumnNames.Assunto, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityMensagensMetadata.PropertyNames.Assunto;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityMensagensMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityMensagensMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityMensagensMetadata.ColumnNames.ControlaConcordancia, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityMensagensMetadata.PropertyNames.ControlaConcordancia;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityMensagensMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdMensagem = "IdMensagem";
			 public const string Assunto = "Assunto";
			 public const string Descricao = "Descricao";
			 public const string ControlaConcordancia = "ControlaConcordancia";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdMensagem = "IdMensagem";
			 public const string Assunto = "Assunto";
			 public const string Descricao = "Descricao";
			 public const string ControlaConcordancia = "ControlaConcordancia";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityMensagensMetadata))
			{
				if(SuitabilityMensagensMetadata.mapDelegates == null)
				{
					SuitabilityMensagensMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityMensagensMetadata.meta == null)
				{
					SuitabilityMensagensMetadata.meta = new SuitabilityMensagensMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdMensagem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Assunto", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Descricao", new esTypeMap("text", "System.String"));
				meta.AddTypeMap("ControlaConcordancia", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "SuitabilityMensagens";
				meta.Destination = "SuitabilityMensagens";
				
				meta.spInsert = "proc_SuitabilityMensagensInsert";				
				meta.spUpdate = "proc_SuitabilityMensagensUpdate";		
				meta.spDelete = "proc_SuitabilityMensagensDelete";
				meta.spLoadAll = "proc_SuitabilityMensagensLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityMensagensLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityMensagensMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
