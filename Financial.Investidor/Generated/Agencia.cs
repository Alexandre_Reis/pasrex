/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 2/11/2015 10:55:52 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Investidor
{

	[Serializable]
	abstract public class esAgenciaCollection : esEntityCollection
	{
		public esAgenciaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AgenciaCollection";
		}

		#region Query Logic
		protected void InitQuery(esAgenciaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAgenciaQuery);
		}
		#endregion
		
		virtual public Agencia DetachEntity(Agencia entity)
		{
			return base.DetachEntity(entity) as Agencia;
		}
		
		virtual public Agencia AttachEntity(Agencia entity)
		{
			return base.AttachEntity(entity) as Agencia;
		}
		
		virtual public void Combine(AgenciaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Agencia this[int index]
		{
			get
			{
				return base[index] as Agencia;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Agencia);
		}
	}



	[Serializable]
	abstract public class esAgencia : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAgenciaQuery GetDynamicQuery()
		{
			return null;
		}

		public esAgencia()
		{

		}

		public esAgencia(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idAgencia)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgencia);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idAgencia)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esAgenciaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdAgencia == idAgencia);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idAgencia)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgencia);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idAgencia)
		{
			esAgenciaQuery query = this.GetDynamicQuery();
			query.Where(query.IdAgencia == idAgencia);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idAgencia)
		{
			esParameters parms = new esParameters();
			parms.Add("IdAgencia",idAgencia);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdAgencia": this.str.IdAgencia = (string)value; break;							
						case "IdBanco": this.str.IdBanco = (string)value; break;							
						case "Codigo": this.str.Codigo = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "DigitoAgencia": this.str.DigitoAgencia = (string)value; break;							
						case "IdLocal": this.str.IdLocal = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdAgencia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgencia = (System.Int32?)value;
							break;
						
						case "IdBanco":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdBanco = (System.Int32?)value;
							break;
						
						case "IdLocal":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdLocal = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Agencia.IdAgencia
		/// </summary>
		virtual public System.Int32? IdAgencia
		{
			get
			{
				return base.GetSystemInt32(AgenciaMetadata.ColumnNames.IdAgencia);
			}
			
			set
			{
				base.SetSystemInt32(AgenciaMetadata.ColumnNames.IdAgencia, value);
			}
		}
		
		/// <summary>
		/// Maps to Agencia.IdBanco
		/// </summary>
		virtual public System.Int32? IdBanco
		{
			get
			{
				return base.GetSystemInt32(AgenciaMetadata.ColumnNames.IdBanco);
			}
			
			set
			{
				if(base.SetSystemInt32(AgenciaMetadata.ColumnNames.IdBanco, value))
				{
					this._UpToBancoByIdBanco = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Agencia.Codigo
		/// </summary>
		virtual public System.String Codigo
		{
			get
			{
				return base.GetSystemString(AgenciaMetadata.ColumnNames.Codigo);
			}
			
			set
			{
				base.SetSystemString(AgenciaMetadata.ColumnNames.Codigo, value);
			}
		}
		
		/// <summary>
		/// Maps to Agencia.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(AgenciaMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(AgenciaMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to Agencia.DigitoAgencia
		/// </summary>
		virtual public System.String DigitoAgencia
		{
			get
			{
				return base.GetSystemString(AgenciaMetadata.ColumnNames.DigitoAgencia);
			}
			
			set
			{
				base.SetSystemString(AgenciaMetadata.ColumnNames.DigitoAgencia, value);
			}
		}
		
		/// <summary>
		/// Maps to Agencia.IdLocal
		/// </summary>
		virtual public System.Int16? IdLocal
		{
			get
			{
				return base.GetSystemInt16(AgenciaMetadata.ColumnNames.IdLocal);
			}
			
			set
			{
				base.SetSystemInt16(AgenciaMetadata.ColumnNames.IdLocal, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Banco _UpToBancoByIdBanco;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAgencia entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdAgencia
			{
				get
				{
					System.Int32? data = entity.IdAgencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgencia = null;
					else entity.IdAgencia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdBanco
			{
				get
				{
					System.Int32? data = entity.IdBanco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdBanco = null;
					else entity.IdBanco = Convert.ToInt32(value);
				}
			}
				
			public System.String Codigo
			{
				get
				{
					System.String data = entity.Codigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Codigo = null;
					else entity.Codigo = Convert.ToString(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String DigitoAgencia
			{
				get
				{
					System.String data = entity.DigitoAgencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DigitoAgencia = null;
					else entity.DigitoAgencia = Convert.ToString(value);
				}
			}
				
			public System.String IdLocal
			{
				get
				{
					System.Int16? data = entity.IdLocal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocal = null;
					else entity.IdLocal = Convert.ToInt16(value);
				}
			}
			

			private esAgencia entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAgenciaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAgencia can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Agencia : esAgencia
	{

				
		#region ContaCorrenteCollectionByIdAgencia - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Agencia_ContaCorrente_FK1
		/// </summary>

		[XmlIgnore]
		public ContaCorrenteCollection ContaCorrenteCollectionByIdAgencia
		{
			get
			{
				if(this._ContaCorrenteCollectionByIdAgencia == null)
				{
					this._ContaCorrenteCollectionByIdAgencia = new ContaCorrenteCollection();
					this._ContaCorrenteCollectionByIdAgencia.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ContaCorrenteCollectionByIdAgencia", this._ContaCorrenteCollectionByIdAgencia);
				
					if(this.IdAgencia != null)
					{
						this._ContaCorrenteCollectionByIdAgencia.Query.Where(this._ContaCorrenteCollectionByIdAgencia.Query.IdAgencia == this.IdAgencia);
						this._ContaCorrenteCollectionByIdAgencia.Query.Load();

						// Auto-hookup Foreign Keys
						this._ContaCorrenteCollectionByIdAgencia.fks.Add(ContaCorrenteMetadata.ColumnNames.IdAgencia, this.IdAgencia);
					}
				}

				return this._ContaCorrenteCollectionByIdAgencia;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ContaCorrenteCollectionByIdAgencia != null) 
				{ 
					this.RemovePostSave("ContaCorrenteCollectionByIdAgencia"); 
					this._ContaCorrenteCollectionByIdAgencia = null;
					
				} 
			} 			
		}

		private ContaCorrenteCollection _ContaCorrenteCollectionByIdAgencia;
		#endregion

				
		#region UpToBancoByIdBanco - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Banco_Agencia_FK1
		/// </summary>

		[XmlIgnore]
		public Banco UpToBancoByIdBanco
		{
			get
			{
				if(this._UpToBancoByIdBanco == null
					&& IdBanco != null					)
				{
					this._UpToBancoByIdBanco = new Banco();
					this._UpToBancoByIdBanco.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToBancoByIdBanco", this._UpToBancoByIdBanco);
					this._UpToBancoByIdBanco.Query.Where(this._UpToBancoByIdBanco.Query.IdBanco == this.IdBanco);
					this._UpToBancoByIdBanco.Query.Load();
				}

				return this._UpToBancoByIdBanco;
			}
			
			set
			{
				this.RemovePreSave("UpToBancoByIdBanco");
				

				if(value == null)
				{
					this.IdBanco = null;
					this._UpToBancoByIdBanco = null;
				}
				else
				{
					this.IdBanco = value.IdBanco;
					this._UpToBancoByIdBanco = value;
					this.SetPreSave("UpToBancoByIdBanco", this._UpToBancoByIdBanco);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "ContaCorrenteCollectionByIdAgencia", typeof(ContaCorrenteCollection), new ContaCorrente()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToBancoByIdBanco != null)
			{
				this.IdBanco = this._UpToBancoByIdBanco.IdBanco;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._ContaCorrenteCollectionByIdAgencia != null)
			{
				foreach(ContaCorrente obj in this._ContaCorrenteCollectionByIdAgencia)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgencia = this.IdAgencia;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAgenciaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AgenciaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdAgencia
		{
			get
			{
				return new esQueryItem(this, AgenciaMetadata.ColumnNames.IdAgencia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdBanco
		{
			get
			{
				return new esQueryItem(this, AgenciaMetadata.ColumnNames.IdBanco, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Codigo
		{
			get
			{
				return new esQueryItem(this, AgenciaMetadata.ColumnNames.Codigo, esSystemType.String);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, AgenciaMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem DigitoAgencia
		{
			get
			{
				return new esQueryItem(this, AgenciaMetadata.ColumnNames.DigitoAgencia, esSystemType.String);
			}
		} 
		
		public esQueryItem IdLocal
		{
			get
			{
				return new esQueryItem(this, AgenciaMetadata.ColumnNames.IdLocal, esSystemType.Int16);
			}
		} 
		
	}



	[Serializable]
	[XmlType("AgenciaCollection")]
	public partial class AgenciaCollection : esAgenciaCollection, IEnumerable<Agencia>
	{
		public AgenciaCollection()
		{

		}
		
		public static implicit operator List<Agencia>(AgenciaCollection coll)
		{
			List<Agencia> list = new List<Agencia>();
			
			foreach (Agencia emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AgenciaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgenciaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Agencia(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Agencia();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AgenciaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgenciaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AgenciaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Agencia AddNew()
		{
			Agencia entity = base.AddNewEntity() as Agencia;
			
			return entity;
		}

		public Agencia FindByPrimaryKey(System.Int32 idAgencia)
		{
			return base.FindByPrimaryKey(idAgencia) as Agencia;
		}


		#region IEnumerable<Agencia> Members

		IEnumerator<Agencia> IEnumerable<Agencia>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Agencia;
			}
		}

		#endregion
		
		private AgenciaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Agencia' table
	/// </summary>

	[Serializable]
	public partial class Agencia : esAgencia
	{
		public Agencia()
		{

		}
	
		public Agencia(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AgenciaMetadata.Meta();
			}
		}
		
		
		
		override protected esAgenciaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgenciaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AgenciaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgenciaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AgenciaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AgenciaQuery query;
	}



	[Serializable]
	public partial class AgenciaQuery : esAgenciaQuery
	{
		public AgenciaQuery()
		{

		}		
		
		public AgenciaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AgenciaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AgenciaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AgenciaMetadata.ColumnNames.IdAgencia, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgenciaMetadata.PropertyNames.IdAgencia;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenciaMetadata.ColumnNames.IdBanco, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgenciaMetadata.PropertyNames.IdBanco;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenciaMetadata.ColumnNames.Codigo, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenciaMetadata.PropertyNames.Codigo;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenciaMetadata.ColumnNames.Nome, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenciaMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenciaMetadata.ColumnNames.DigitoAgencia, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = AgenciaMetadata.PropertyNames.DigitoAgencia;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgenciaMetadata.ColumnNames.IdLocal, 5, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = AgenciaMetadata.PropertyNames.IdLocal;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public AgenciaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdAgencia = "IdAgencia";
			 public const string IdBanco = "IdBanco";
			 public const string Codigo = "Codigo";
			 public const string Nome = "Nome";
			 public const string DigitoAgencia = "DigitoAgencia";
			 public const string IdLocal = "IdLocal";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdAgencia = "IdAgencia";
			 public const string IdBanco = "IdBanco";
			 public const string Codigo = "Codigo";
			 public const string Nome = "Nome";
			 public const string DigitoAgencia = "DigitoAgencia";
			 public const string IdLocal = "IdLocal";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AgenciaMetadata))
			{
				if(AgenciaMetadata.mapDelegates == null)
				{
					AgenciaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AgenciaMetadata.meta == null)
				{
					AgenciaMetadata.meta = new AgenciaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdAgencia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdBanco", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Codigo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DigitoAgencia", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdLocal", new esTypeMap("smallint", "System.Int16"));			
				
				
				
				meta.Source = "Agencia";
				meta.Destination = "Agencia";
				
				meta.spInsert = "proc_AgenciaInsert";				
				meta.spUpdate = "proc_AgenciaUpdate";		
				meta.spDelete = "proc_AgenciaDelete";
				meta.spLoadAll = "proc_AgenciaLoadAll";
				meta.spLoadByPrimaryKey = "proc_AgenciaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AgenciaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
