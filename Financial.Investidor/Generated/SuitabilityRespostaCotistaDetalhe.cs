/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/09/2015 10:43:20
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityRespostaCotistaDetalheCollection : esEntityCollection
	{
		public esSuitabilityRespostaCotistaDetalheCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityRespostaCotistaDetalheCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityRespostaCotistaDetalheQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityRespostaCotistaDetalheQuery);
		}
		#endregion
		
		virtual public SuitabilityRespostaCotistaDetalhe DetachEntity(SuitabilityRespostaCotistaDetalhe entity)
		{
			return base.DetachEntity(entity) as SuitabilityRespostaCotistaDetalhe;
		}
		
		virtual public SuitabilityRespostaCotistaDetalhe AttachEntity(SuitabilityRespostaCotistaDetalhe entity)
		{
			return base.AttachEntity(entity) as SuitabilityRespostaCotistaDetalhe;
		}
		
		virtual public void Combine(SuitabilityRespostaCotistaDetalheCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityRespostaCotistaDetalhe this[int index]
		{
			get
			{
				return base[index] as SuitabilityRespostaCotistaDetalhe;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityRespostaCotistaDetalhe);
		}
	}



	[Serializable]
	abstract public class esSuitabilityRespostaCotistaDetalhe : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityRespostaCotistaDetalheQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityRespostaCotistaDetalhe()
		{

		}

		public esSuitabilityRespostaCotistaDetalhe(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int32 idCotista, System.Int32 idQuestao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idCotista, idQuestao);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idCotista, idQuestao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int32 idCotista, System.Int32 idQuestao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idCotista, idQuestao);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idCotista, idQuestao);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int32 idCotista, System.Int32 idQuestao)
		{
			esSuitabilityRespostaCotistaDetalheQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.IdCotista == idCotista, query.IdQuestao == idQuestao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int32 idCotista, System.Int32 idQuestao)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("IdCotista",idCotista);			parms.Add("IdQuestao",idQuestao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "IdQuestao": this.str.IdQuestao = (string)value; break;							
						case "IdOpcao": this.str.IdOpcao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
						
						case "IdQuestao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdQuestao = (System.Int32?)value;
							break;
						
						case "IdOpcao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOpcao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityRespostaCotistaDetalhe.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityRespostaCotistaDetalhe.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.IdCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityRespostaCotistaDetalhe.IdQuestao
		/// </summary>
		virtual public System.Int32? IdQuestao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.IdQuestao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.IdQuestao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityRespostaCotistaDetalhe.IdOpcao
		/// </summary>
		virtual public System.Int32? IdOpcao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.IdOpcao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.IdOpcao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityRespostaCotistaDetalhe entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String IdQuestao
			{
				get
				{
					System.Int32? data = entity.IdQuestao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdQuestao = null;
					else entity.IdQuestao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOpcao
			{
				get
				{
					System.Int32? data = entity.IdOpcao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOpcao = null;
					else entity.IdOpcao = Convert.ToInt32(value);
				}
			}
			

			private esSuitabilityRespostaCotistaDetalhe entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityRespostaCotistaDetalheQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityRespostaCotistaDetalhe can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityRespostaCotistaDetalhe : esSuitabilityRespostaCotistaDetalhe
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityRespostaCotistaDetalheQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityRespostaCotistaDetalheMetadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdQuestao
		{
			get
			{
				return new esQueryItem(this, SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.IdQuestao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOpcao
		{
			get
			{
				return new esQueryItem(this, SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.IdOpcao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityRespostaCotistaDetalheCollection")]
	public partial class SuitabilityRespostaCotistaDetalheCollection : esSuitabilityRespostaCotistaDetalheCollection, IEnumerable<SuitabilityRespostaCotistaDetalhe>
	{
		public SuitabilityRespostaCotistaDetalheCollection()
		{

		}
		
		public static implicit operator List<SuitabilityRespostaCotistaDetalhe>(SuitabilityRespostaCotistaDetalheCollection coll)
		{
			List<SuitabilityRespostaCotistaDetalhe> list = new List<SuitabilityRespostaCotistaDetalhe>();
			
			foreach (SuitabilityRespostaCotistaDetalhe emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityRespostaCotistaDetalheMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityRespostaCotistaDetalheQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityRespostaCotistaDetalhe(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityRespostaCotistaDetalhe();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityRespostaCotistaDetalheQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityRespostaCotistaDetalheQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityRespostaCotistaDetalheQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityRespostaCotistaDetalhe AddNew()
		{
			SuitabilityRespostaCotistaDetalhe entity = base.AddNewEntity() as SuitabilityRespostaCotistaDetalhe;
			
			return entity;
		}

		public SuitabilityRespostaCotistaDetalhe FindByPrimaryKey(System.DateTime data, System.Int32 idCotista, System.Int32 idQuestao)
		{
			return base.FindByPrimaryKey(data, idCotista, idQuestao) as SuitabilityRespostaCotistaDetalhe;
		}


		#region IEnumerable<SuitabilityRespostaCotistaDetalhe> Members

		IEnumerator<SuitabilityRespostaCotistaDetalhe> IEnumerable<SuitabilityRespostaCotistaDetalhe>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityRespostaCotistaDetalhe;
			}
		}

		#endregion
		
		private SuitabilityRespostaCotistaDetalheQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityRespostaCotistaDetalhe' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityRespostaCotistaDetalhe : esSuitabilityRespostaCotistaDetalhe
	{
		public SuitabilityRespostaCotistaDetalhe()
		{

		}
	
		public SuitabilityRespostaCotistaDetalhe(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityRespostaCotistaDetalheMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityRespostaCotistaDetalheQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityRespostaCotistaDetalheQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityRespostaCotistaDetalheQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityRespostaCotistaDetalheQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityRespostaCotistaDetalheQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityRespostaCotistaDetalheQuery query;
	}



	[Serializable]
	public partial class SuitabilityRespostaCotistaDetalheQuery : esSuitabilityRespostaCotistaDetalheQuery
	{
		public SuitabilityRespostaCotistaDetalheQuery()
		{

		}		
		
		public SuitabilityRespostaCotistaDetalheQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityRespostaCotistaDetalheMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityRespostaCotistaDetalheMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SuitabilityRespostaCotistaDetalheMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.IdCotista, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityRespostaCotistaDetalheMetadata.PropertyNames.IdCotista;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.IdQuestao, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityRespostaCotistaDetalheMetadata.PropertyNames.IdQuestao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityRespostaCotistaDetalheMetadata.ColumnNames.IdOpcao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityRespostaCotistaDetalheMetadata.PropertyNames.IdOpcao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityRespostaCotistaDetalheMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string IdCotista = "IdCotista";
			 public const string IdQuestao = "IdQuestao";
			 public const string IdOpcao = "IdOpcao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string IdCotista = "IdCotista";
			 public const string IdQuestao = "IdQuestao";
			 public const string IdOpcao = "IdOpcao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityRespostaCotistaDetalheMetadata))
			{
				if(SuitabilityRespostaCotistaDetalheMetadata.mapDelegates == null)
				{
					SuitabilityRespostaCotistaDetalheMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityRespostaCotistaDetalheMetadata.meta == null)
				{
					SuitabilityRespostaCotistaDetalheMetadata.meta = new SuitabilityRespostaCotistaDetalheMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdQuestao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOpcao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "SuitabilityRespostaCotistaDetalhe";
				meta.Destination = "SuitabilityRespostaCotistaDetalhe";
				
				meta.spInsert = "proc_SuitabilityRespostaCotistaDetalheInsert";				
				meta.spUpdate = "proc_SuitabilityRespostaCotistaDetalheUpdate";		
				meta.spDelete = "proc_SuitabilityRespostaCotistaDetalheDelete";
				meta.spLoadAll = "proc_SuitabilityRespostaCotistaDetalheLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityRespostaCotistaDetalheLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityRespostaCotistaDetalheMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
