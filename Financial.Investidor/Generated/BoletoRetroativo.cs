/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 14/10/2014 16:59:26
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esBoletoRetroativoCollection : esEntityCollection
	{
		public esBoletoRetroativoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "BoletoRetroativoCollection";
		}

		#region Query Logic
		protected void InitQuery(esBoletoRetroativoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esBoletoRetroativoQuery);
		}
		#endregion
		
		virtual public BoletoRetroativo DetachEntity(BoletoRetroativo entity)
		{
			return base.DetachEntity(entity) as BoletoRetroativo;
		}
		
		virtual public BoletoRetroativo AttachEntity(BoletoRetroativo entity)
		{
			return base.AttachEntity(entity) as BoletoRetroativo;
		}
		
		virtual public void Combine(BoletoRetroativoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public BoletoRetroativo this[int index]
		{
			get
			{
				return base[index] as BoletoRetroativo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(BoletoRetroativo);
		}
	}



	[Serializable]
	abstract public class esBoletoRetroativo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esBoletoRetroativoQuery GetDynamicQuery()
		{
			return null;
		}

		public esBoletoRetroativo()
		{

		}

		public esBoletoRetroativo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente)
		{
			esBoletoRetroativoQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "DataBoleto": this.str.DataBoleto = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoMercado = (System.Int32?)value;
							break;
						
						case "DataBoleto":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataBoleto = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to BoletoRetroativo.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(BoletoRetroativoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(BoletoRetroativoMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to BoletoRetroativo.TipoMercado
		/// </summary>
		virtual public System.Int32? TipoMercado
		{
			get
			{
				return base.GetSystemInt32(BoletoRetroativoMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemInt32(BoletoRetroativoMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to BoletoRetroativo.DataBoleto
		/// </summary>
		virtual public System.DateTime? DataBoleto
		{
			get
			{
				return base.GetSystemDateTime(BoletoRetroativoMetadata.ColumnNames.DataBoleto);
			}
			
			set
			{
				base.SetSystemDateTime(BoletoRetroativoMetadata.ColumnNames.DataBoleto, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esBoletoRetroativo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Int32? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToInt32(value);
				}
			}
				
			public System.String DataBoleto
			{
				get
				{
					System.DateTime? data = entity.DataBoleto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataBoleto = null;
					else entity.DataBoleto = Convert.ToDateTime(value);
				}
			}
			

			private esBoletoRetroativo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esBoletoRetroativoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esBoletoRetroativo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class BoletoRetroativo : esBoletoRetroativo
	{

		#region UpToCliente - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Cliente_BoletoRetroativo_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToCliente
		{
			get
			{
				if(this._UpToCliente == null
					&& IdCliente != null					)
				{
					this._UpToCliente = new Cliente();
					this._UpToCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCliente", this._UpToCliente);
					this._UpToCliente.Query.Where(this._UpToCliente.Query.IdCliente == this.IdCliente);
					this._UpToCliente.Query.Load();
				}

				return this._UpToCliente;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToCliente");

				if(value == null)
				{
					this._UpToCliente = null;
				}
				else
				{
					this._UpToCliente = value;
					this.SetPreSave("UpToCliente", this._UpToCliente);
				}
				
				
			} 
		}

		private Cliente _UpToCliente;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esBoletoRetroativoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return BoletoRetroativoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, BoletoRetroativoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, BoletoRetroativoMetadata.ColumnNames.TipoMercado, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataBoleto
		{
			get
			{
				return new esQueryItem(this, BoletoRetroativoMetadata.ColumnNames.DataBoleto, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("BoletoRetroativoCollection")]
	public partial class BoletoRetroativoCollection : esBoletoRetroativoCollection, IEnumerable<BoletoRetroativo>
	{
		public BoletoRetroativoCollection()
		{

		}
		
		public static implicit operator List<BoletoRetroativo>(BoletoRetroativoCollection coll)
		{
			List<BoletoRetroativo> list = new List<BoletoRetroativo>();
			
			foreach (BoletoRetroativo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  BoletoRetroativoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BoletoRetroativoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new BoletoRetroativo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new BoletoRetroativo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public BoletoRetroativoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BoletoRetroativoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(BoletoRetroativoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public BoletoRetroativo AddNew()
		{
			BoletoRetroativo entity = base.AddNewEntity() as BoletoRetroativo;
			
			return entity;
		}

		public BoletoRetroativo FindByPrimaryKey(System.Int32 idCliente)
		{
			return base.FindByPrimaryKey(idCliente) as BoletoRetroativo;
		}


		#region IEnumerable<BoletoRetroativo> Members

		IEnumerator<BoletoRetroativo> IEnumerable<BoletoRetroativo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as BoletoRetroativo;
			}
		}

		#endregion
		
		private BoletoRetroativoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'BoletoRetroativo' table
	/// </summary>

	[Serializable]
	public partial class BoletoRetroativo : esBoletoRetroativo
	{
		public BoletoRetroativo()
		{

		}
	
		public BoletoRetroativo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return BoletoRetroativoMetadata.Meta();
			}
		}
		
		
		
		override protected esBoletoRetroativoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BoletoRetroativoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public BoletoRetroativoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BoletoRetroativoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(BoletoRetroativoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private BoletoRetroativoQuery query;
	}



	[Serializable]
	public partial class BoletoRetroativoQuery : esBoletoRetroativoQuery
	{
		public BoletoRetroativoQuery()
		{

		}		
		
		public BoletoRetroativoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class BoletoRetroativoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected BoletoRetroativoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(BoletoRetroativoMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BoletoRetroativoMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BoletoRetroativoMetadata.ColumnNames.TipoMercado, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BoletoRetroativoMetadata.PropertyNames.TipoMercado;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BoletoRetroativoMetadata.ColumnNames.DataBoleto, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = BoletoRetroativoMetadata.PropertyNames.DataBoleto;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public BoletoRetroativoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataBoleto = "DataBoleto";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataBoleto = "DataBoleto";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(BoletoRetroativoMetadata))
			{
				if(BoletoRetroativoMetadata.mapDelegates == null)
				{
					BoletoRetroativoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (BoletoRetroativoMetadata.meta == null)
				{
					BoletoRetroativoMetadata.meta = new BoletoRetroativoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataBoleto", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "BoletoRetroativo";
				meta.Destination = "BoletoRetroativo";
				
				meta.spInsert = "proc_BoletoRetroativoInsert";				
				meta.spUpdate = "proc_BoletoRetroativoUpdate";		
				meta.spDelete = "proc_BoletoRetroativoDelete";
				meta.spLoadAll = "proc_BoletoRetroativoLoadAll";
				meta.spLoadByPrimaryKey = "proc_BoletoRetroativoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private BoletoRetroativoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
