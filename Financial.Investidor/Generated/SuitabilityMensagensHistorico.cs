/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 09/09/2015 12:26:54
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityMensagensHistoricoCollection : esEntityCollection
	{
		public esSuitabilityMensagensHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityMensagensHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityMensagensHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityMensagensHistoricoQuery);
		}
		#endregion
		
		virtual public SuitabilityMensagensHistorico DetachEntity(SuitabilityMensagensHistorico entity)
		{
			return base.DetachEntity(entity) as SuitabilityMensagensHistorico;
		}
		
		virtual public SuitabilityMensagensHistorico AttachEntity(SuitabilityMensagensHistorico entity)
		{
			return base.AttachEntity(entity) as SuitabilityMensagensHistorico;
		}
		
		virtual public void Combine(SuitabilityMensagensHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityMensagensHistorico this[int index]
		{
			get
			{
				return base[index] as SuitabilityMensagensHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityMensagensHistorico);
		}
	}



	[Serializable]
	abstract public class esSuitabilityMensagensHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityMensagensHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityMensagensHistorico()
		{

		}

		public esSuitabilityMensagensHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idMensagem)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idMensagem);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idMensagem);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idMensagem)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idMensagem);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idMensagem);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idMensagem)
		{
			esSuitabilityMensagensHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdMensagem == idMensagem);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idMensagem)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdMensagem",idMensagem);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdMensagem": this.str.IdMensagem = (string)value; break;							
						case "Assunto": this.str.Assunto = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "ControlaConcordancia": this.str.ControlaConcordancia = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdMensagem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMensagem = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityMensagensHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(SuitabilityMensagensHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(SuitabilityMensagensHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityMensagensHistorico.IdMensagem
		/// </summary>
		virtual public System.Int32? IdMensagem
		{
			get
			{
				return base.GetSystemInt32(SuitabilityMensagensHistoricoMetadata.ColumnNames.IdMensagem);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityMensagensHistoricoMetadata.ColumnNames.IdMensagem, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityMensagensHistorico.Assunto
		/// </summary>
		virtual public System.String Assunto
		{
			get
			{
				return base.GetSystemString(SuitabilityMensagensHistoricoMetadata.ColumnNames.Assunto);
			}
			
			set
			{
				base.SetSystemString(SuitabilityMensagensHistoricoMetadata.ColumnNames.Assunto, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityMensagensHistorico.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(SuitabilityMensagensHistoricoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityMensagensHistoricoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityMensagensHistorico.ControlaConcordancia
		/// </summary>
		virtual public System.String ControlaConcordancia
		{
			get
			{
				return base.GetSystemString(SuitabilityMensagensHistoricoMetadata.ColumnNames.ControlaConcordancia);
			}
			
			set
			{
				base.SetSystemString(SuitabilityMensagensHistoricoMetadata.ColumnNames.ControlaConcordancia, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityMensagensHistorico.Tipo
		/// </summary>
		virtual public System.String Tipo
		{
			get
			{
				return base.GetSystemString(SuitabilityMensagensHistoricoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemString(SuitabilityMensagensHistoricoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityMensagensHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdMensagem
			{
				get
				{
					System.Int32? data = entity.IdMensagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMensagem = null;
					else entity.IdMensagem = Convert.ToInt32(value);
				}
			}
				
			public System.String Assunto
			{
				get
				{
					System.String data = entity.Assunto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Assunto = null;
					else entity.Assunto = Convert.ToString(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String ControlaConcordancia
			{
				get
				{
					System.String data = entity.ControlaConcordancia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ControlaConcordancia = null;
					else entity.ControlaConcordancia = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.String data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToString(value);
				}
			}
			

			private esSuitabilityMensagensHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityMensagensHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityMensagensHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityMensagensHistorico : esSuitabilityMensagensHistorico
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityMensagensHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityMensagensHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, SuitabilityMensagensHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdMensagem
		{
			get
			{
				return new esQueryItem(this, SuitabilityMensagensHistoricoMetadata.ColumnNames.IdMensagem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Assunto
		{
			get
			{
				return new esQueryItem(this, SuitabilityMensagensHistoricoMetadata.ColumnNames.Assunto, esSystemType.String);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, SuitabilityMensagensHistoricoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem ControlaConcordancia
		{
			get
			{
				return new esQueryItem(this, SuitabilityMensagensHistoricoMetadata.ColumnNames.ControlaConcordancia, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, SuitabilityMensagensHistoricoMetadata.ColumnNames.Tipo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityMensagensHistoricoCollection")]
	public partial class SuitabilityMensagensHistoricoCollection : esSuitabilityMensagensHistoricoCollection, IEnumerable<SuitabilityMensagensHistorico>
	{
		public SuitabilityMensagensHistoricoCollection()
		{

		}
		
		public static implicit operator List<SuitabilityMensagensHistorico>(SuitabilityMensagensHistoricoCollection coll)
		{
			List<SuitabilityMensagensHistorico> list = new List<SuitabilityMensagensHistorico>();
			
			foreach (SuitabilityMensagensHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityMensagensHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityMensagensHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityMensagensHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityMensagensHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityMensagensHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityMensagensHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityMensagensHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityMensagensHistorico AddNew()
		{
			SuitabilityMensagensHistorico entity = base.AddNewEntity() as SuitabilityMensagensHistorico;
			
			return entity;
		}

		public SuitabilityMensagensHistorico FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idMensagem)
		{
			return base.FindByPrimaryKey(dataHistorico, idMensagem) as SuitabilityMensagensHistorico;
		}


		#region IEnumerable<SuitabilityMensagensHistorico> Members

		IEnumerator<SuitabilityMensagensHistorico> IEnumerable<SuitabilityMensagensHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityMensagensHistorico;
			}
		}

		#endregion
		
		private SuitabilityMensagensHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityMensagensHistorico' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityMensagensHistorico : esSuitabilityMensagensHistorico
	{
		public SuitabilityMensagensHistorico()
		{

		}
	
		public SuitabilityMensagensHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityMensagensHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityMensagensHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityMensagensHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityMensagensHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityMensagensHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityMensagensHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityMensagensHistoricoQuery query;
	}



	[Serializable]
	public partial class SuitabilityMensagensHistoricoQuery : esSuitabilityMensagensHistoricoQuery
	{
		public SuitabilityMensagensHistoricoQuery()
		{

		}		
		
		public SuitabilityMensagensHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityMensagensHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityMensagensHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityMensagensHistoricoMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SuitabilityMensagensHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityMensagensHistoricoMetadata.ColumnNames.IdMensagem, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityMensagensHistoricoMetadata.PropertyNames.IdMensagem;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityMensagensHistoricoMetadata.ColumnNames.Assunto, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityMensagensHistoricoMetadata.PropertyNames.Assunto;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityMensagensHistoricoMetadata.ColumnNames.Descricao, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityMensagensHistoricoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityMensagensHistoricoMetadata.ColumnNames.ControlaConcordancia, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityMensagensHistoricoMetadata.PropertyNames.ControlaConcordancia;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityMensagensHistoricoMetadata.ColumnNames.Tipo, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityMensagensHistoricoMetadata.PropertyNames.Tipo;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityMensagensHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdMensagem = "IdMensagem";
			 public const string Assunto = "Assunto";
			 public const string Descricao = "Descricao";
			 public const string ControlaConcordancia = "ControlaConcordancia";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdMensagem = "IdMensagem";
			 public const string Assunto = "Assunto";
			 public const string Descricao = "Descricao";
			 public const string ControlaConcordancia = "ControlaConcordancia";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityMensagensHistoricoMetadata))
			{
				if(SuitabilityMensagensHistoricoMetadata.mapDelegates == null)
				{
					SuitabilityMensagensHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityMensagensHistoricoMetadata.meta == null)
				{
					SuitabilityMensagensHistoricoMetadata.meta = new SuitabilityMensagensHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdMensagem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Assunto", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Descricao", new esTypeMap("text", "System.String"));
				meta.AddTypeMap("ControlaConcordancia", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "SuitabilityMensagensHistorico";
				meta.Destination = "SuitabilityMensagensHistorico";
				
				meta.spInsert = "proc_SuitabilityMensagensHistoricoInsert";				
				meta.spUpdate = "proc_SuitabilityMensagensHistoricoUpdate";		
				meta.spDelete = "proc_SuitabilityMensagensHistoricoDelete";
				meta.spLoadAll = "proc_SuitabilityMensagensHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityMensagensHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityMensagensHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
