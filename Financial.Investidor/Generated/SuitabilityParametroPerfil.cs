/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/09/2015 14:44:54
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityParametroPerfilCollection : esEntityCollection
	{
		public esSuitabilityParametroPerfilCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityParametroPerfilCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityParametroPerfilQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityParametroPerfilQuery);
		}
		#endregion
		
		virtual public SuitabilityParametroPerfil DetachEntity(SuitabilityParametroPerfil entity)
		{
			return base.DetachEntity(entity) as SuitabilityParametroPerfil;
		}
		
		virtual public SuitabilityParametroPerfil AttachEntity(SuitabilityParametroPerfil entity)
		{
			return base.AttachEntity(entity) as SuitabilityParametroPerfil;
		}
		
		virtual public void Combine(SuitabilityParametroPerfilCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityParametroPerfil this[int index]
		{
			get
			{
				return base[index] as SuitabilityParametroPerfil;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityParametroPerfil);
		}
	}



	[Serializable]
	abstract public class esSuitabilityParametroPerfil : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityParametroPerfilQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityParametroPerfil()
		{

		}

		public esSuitabilityParametroPerfil(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idParametroPerfil)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idParametroPerfil);
			else
				return LoadByPrimaryKeyStoredProcedure(idParametroPerfil);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idParametroPerfil)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idParametroPerfil);
			else
				return LoadByPrimaryKeyStoredProcedure(idParametroPerfil);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idParametroPerfil)
		{
			esSuitabilityParametroPerfilQuery query = this.GetDynamicQuery();
			query.Where(query.IdParametroPerfil == idParametroPerfil);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idParametroPerfil)
		{
			esParameters parms = new esParameters();
			parms.Add("IdParametroPerfil",idParametroPerfil);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdParametroPerfil": this.str.IdParametroPerfil = (string)value; break;							
						case "IdValidacao": this.str.IdValidacao = (string)value; break;							
						case "PerfilInvestidor": this.str.PerfilInvestidor = (string)value; break;							
						case "Definicao": this.str.Definicao = (string)value; break;							
						case "FaixaPontuacao": this.str.FaixaPontuacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdParametroPerfil":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdParametroPerfil = (System.Int32?)value;
							break;
						
						case "IdValidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdValidacao = (System.Int32?)value;
							break;
						
						case "PerfilInvestidor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PerfilInvestidor = (System.Int32?)value;
							break;
						
						case "FaixaPontuacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FaixaPontuacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityParametroPerfil.IdParametroPerfil
		/// </summary>
		virtual public System.Int32? IdParametroPerfil
		{
			get
			{
				return base.GetSystemInt32(SuitabilityParametroPerfilMetadata.ColumnNames.IdParametroPerfil);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityParametroPerfilMetadata.ColumnNames.IdParametroPerfil, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametroPerfil.IdValidacao
		/// </summary>
		virtual public System.Int32? IdValidacao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityParametroPerfilMetadata.ColumnNames.IdValidacao);
			}
			
			set
			{
				if(base.SetSystemInt32(SuitabilityParametroPerfilMetadata.ColumnNames.IdValidacao, value))
				{
					this._UpToSuitabilityValidacaoByIdValidacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametroPerfil.PerfilInvestidor
		/// </summary>
		virtual public System.Int32? PerfilInvestidor
		{
			get
			{
				return base.GetSystemInt32(SuitabilityParametroPerfilMetadata.ColumnNames.PerfilInvestidor);
			}
			
			set
			{
				if(base.SetSystemInt32(SuitabilityParametroPerfilMetadata.ColumnNames.PerfilInvestidor, value))
				{
					this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametroPerfil.Definicao
		/// </summary>
		virtual public System.String Definicao
		{
			get
			{
				return base.GetSystemString(SuitabilityParametroPerfilMetadata.ColumnNames.Definicao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityParametroPerfilMetadata.ColumnNames.Definicao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametroPerfil.FaixaPontuacao
		/// </summary>
		virtual public System.Int32? FaixaPontuacao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityParametroPerfilMetadata.ColumnNames.FaixaPontuacao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityParametroPerfilMetadata.ColumnNames.FaixaPontuacao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected SuitabilityPerfilInvestidor _UpToSuitabilityPerfilInvestidorByPerfilInvestidor;
		[CLSCompliant(false)]
		internal protected SuitabilityValidacao _UpToSuitabilityValidacaoByIdValidacao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityParametroPerfil entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdParametroPerfil
			{
				get
				{
					System.Int32? data = entity.IdParametroPerfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdParametroPerfil = null;
					else entity.IdParametroPerfil = Convert.ToInt32(value);
				}
			}
				
			public System.String IdValidacao
			{
				get
				{
					System.Int32? data = entity.IdValidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdValidacao = null;
					else entity.IdValidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String PerfilInvestidor
			{
				get
				{
					System.Int32? data = entity.PerfilInvestidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerfilInvestidor = null;
					else entity.PerfilInvestidor = Convert.ToInt32(value);
				}
			}
				
			public System.String Definicao
			{
				get
				{
					System.String data = entity.Definicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Definicao = null;
					else entity.Definicao = Convert.ToString(value);
				}
			}
				
			public System.String FaixaPontuacao
			{
				get
				{
					System.Int32? data = entity.FaixaPontuacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FaixaPontuacao = null;
					else entity.FaixaPontuacao = Convert.ToInt32(value);
				}
			}
			

			private esSuitabilityParametroPerfil entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityParametroPerfilQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityParametroPerfil can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityParametroPerfil : esSuitabilityParametroPerfil
	{

				
		#region UpToSuitabilityPerfilInvestidorByPerfilInvestidor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - SuitabilityParametroPerfil_SuitabilityPerfilInvestidor_FK
		/// </summary>

		[XmlIgnore]
		public SuitabilityPerfilInvestidor UpToSuitabilityPerfilInvestidorByPerfilInvestidor
		{
			get
			{
				if(this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor == null
					&& PerfilInvestidor != null					)
				{
					this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor = new SuitabilityPerfilInvestidor();
					this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSuitabilityPerfilInvestidorByPerfilInvestidor", this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor);
					this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor.Query.Where(this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor.Query.IdPerfilInvestidor == this.PerfilInvestidor);
					this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor.Query.Load();
				}

				return this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor;
			}
			
			set
			{
				this.RemovePreSave("UpToSuitabilityPerfilInvestidorByPerfilInvestidor");
				

				if(value == null)
				{
					this.PerfilInvestidor = null;
					this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor = null;
				}
				else
				{
					this.PerfilInvestidor = value.IdPerfilInvestidor;
					this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor = value;
					this.SetPreSave("UpToSuitabilityPerfilInvestidorByPerfilInvestidor", this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor);
				}
				
			}
		}
		#endregion
		

				
		#region UpToSuitabilityValidacaoByIdValidacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - SuitabilityParametroPerfil_SuitabilityValidacao_FK
		/// </summary>

		[XmlIgnore]
		public SuitabilityValidacao UpToSuitabilityValidacaoByIdValidacao
		{
			get
			{
				if(this._UpToSuitabilityValidacaoByIdValidacao == null
					&& IdValidacao != null					)
				{
					this._UpToSuitabilityValidacaoByIdValidacao = new SuitabilityValidacao();
					this._UpToSuitabilityValidacaoByIdValidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSuitabilityValidacaoByIdValidacao", this._UpToSuitabilityValidacaoByIdValidacao);
					this._UpToSuitabilityValidacaoByIdValidacao.Query.Where(this._UpToSuitabilityValidacaoByIdValidacao.Query.IdValidacao == this.IdValidacao);
					this._UpToSuitabilityValidacaoByIdValidacao.Query.Load();
				}

				return this._UpToSuitabilityValidacaoByIdValidacao;
			}
			
			set
			{
				this.RemovePreSave("UpToSuitabilityValidacaoByIdValidacao");
				

				if(value == null)
				{
					this.IdValidacao = null;
					this._UpToSuitabilityValidacaoByIdValidacao = null;
				}
				else
				{
					this.IdValidacao = value.IdValidacao;
					this._UpToSuitabilityValidacaoByIdValidacao = value;
					this.SetPreSave("UpToSuitabilityValidacaoByIdValidacao", this._UpToSuitabilityValidacaoByIdValidacao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor != null)
			{
				this.PerfilInvestidor = this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor.IdPerfilInvestidor;
			}
			if(!this.es.IsDeleted && this._UpToSuitabilityValidacaoByIdValidacao != null)
			{
				this.IdValidacao = this._UpToSuitabilityValidacaoByIdValidacao.IdValidacao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityParametroPerfilQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityParametroPerfilMetadata.Meta();
			}
		}	
		

		public esQueryItem IdParametroPerfil
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametroPerfilMetadata.ColumnNames.IdParametroPerfil, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdValidacao
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametroPerfilMetadata.ColumnNames.IdValidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PerfilInvestidor
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametroPerfilMetadata.ColumnNames.PerfilInvestidor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Definicao
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametroPerfilMetadata.ColumnNames.Definicao, esSystemType.String);
			}
		} 
		
		public esQueryItem FaixaPontuacao
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametroPerfilMetadata.ColumnNames.FaixaPontuacao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityParametroPerfilCollection")]
	public partial class SuitabilityParametroPerfilCollection : esSuitabilityParametroPerfilCollection, IEnumerable<SuitabilityParametroPerfil>
	{
		public SuitabilityParametroPerfilCollection()
		{

		}
		
		public static implicit operator List<SuitabilityParametroPerfil>(SuitabilityParametroPerfilCollection coll)
		{
			List<SuitabilityParametroPerfil> list = new List<SuitabilityParametroPerfil>();
			
			foreach (SuitabilityParametroPerfil emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityParametroPerfilMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityParametroPerfilQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityParametroPerfil(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityParametroPerfil();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityParametroPerfilQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityParametroPerfilQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityParametroPerfilQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityParametroPerfil AddNew()
		{
			SuitabilityParametroPerfil entity = base.AddNewEntity() as SuitabilityParametroPerfil;
			
			return entity;
		}

		public SuitabilityParametroPerfil FindByPrimaryKey(System.Int32 idParametroPerfil)
		{
			return base.FindByPrimaryKey(idParametroPerfil) as SuitabilityParametroPerfil;
		}


		#region IEnumerable<SuitabilityParametroPerfil> Members

		IEnumerator<SuitabilityParametroPerfil> IEnumerable<SuitabilityParametroPerfil>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityParametroPerfil;
			}
		}

		#endregion
		
		private SuitabilityParametroPerfilQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityParametroPerfil' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityParametroPerfil : esSuitabilityParametroPerfil
	{
		public SuitabilityParametroPerfil()
		{

		}
	
		public SuitabilityParametroPerfil(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityParametroPerfilMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityParametroPerfilQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityParametroPerfilQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityParametroPerfilQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityParametroPerfilQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityParametroPerfilQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityParametroPerfilQuery query;
	}



	[Serializable]
	public partial class SuitabilityParametroPerfilQuery : esSuitabilityParametroPerfilQuery
	{
		public SuitabilityParametroPerfilQuery()
		{

		}		
		
		public SuitabilityParametroPerfilQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityParametroPerfilMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityParametroPerfilMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityParametroPerfilMetadata.ColumnNames.IdParametroPerfil, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityParametroPerfilMetadata.PropertyNames.IdParametroPerfil;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametroPerfilMetadata.ColumnNames.IdValidacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityParametroPerfilMetadata.PropertyNames.IdValidacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametroPerfilMetadata.ColumnNames.PerfilInvestidor, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityParametroPerfilMetadata.PropertyNames.PerfilInvestidor;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametroPerfilMetadata.ColumnNames.Definicao, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityParametroPerfilMetadata.PropertyNames.Definicao;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametroPerfilMetadata.ColumnNames.FaixaPontuacao, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityParametroPerfilMetadata.PropertyNames.FaixaPontuacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityParametroPerfilMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdParametroPerfil = "IdParametroPerfil";
			 public const string IdValidacao = "IdValidacao";
			 public const string PerfilInvestidor = "PerfilInvestidor";
			 public const string Definicao = "Definicao";
			 public const string FaixaPontuacao = "FaixaPontuacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdParametroPerfil = "IdParametroPerfil";
			 public const string IdValidacao = "IdValidacao";
			 public const string PerfilInvestidor = "PerfilInvestidor";
			 public const string Definicao = "Definicao";
			 public const string FaixaPontuacao = "FaixaPontuacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityParametroPerfilMetadata))
			{
				if(SuitabilityParametroPerfilMetadata.mapDelegates == null)
				{
					SuitabilityParametroPerfilMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityParametroPerfilMetadata.meta == null)
				{
					SuitabilityParametroPerfilMetadata.meta = new SuitabilityParametroPerfilMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdParametroPerfil", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdValidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PerfilInvestidor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Definicao", new esTypeMap("text", "System.String"));
				meta.AddTypeMap("FaixaPontuacao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "SuitabilityParametroPerfil";
				meta.Destination = "SuitabilityParametroPerfil";
				
				meta.spInsert = "proc_SuitabilityParametroPerfilInsert";				
				meta.spUpdate = "proc_SuitabilityParametroPerfilUpdate";		
				meta.spDelete = "proc_SuitabilityParametroPerfilDelete";
				meta.spLoadAll = "proc_SuitabilityParametroPerfilLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityParametroPerfilLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityParametroPerfilMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
