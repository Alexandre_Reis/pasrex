/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 2/11/2015 10:56:13 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Investidor
{

	[Serializable]
	abstract public class esTabelaProcessamentoCollection : esEntityCollection
	{
		public esTabelaProcessamentoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaProcessamentoCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaProcessamentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaProcessamentoQuery);
		}
		#endregion
		
		virtual public TabelaProcessamento DetachEntity(TabelaProcessamento entity)
		{
			return base.DetachEntity(entity) as TabelaProcessamento;
		}
		
		virtual public TabelaProcessamento AttachEntity(TabelaProcessamento entity)
		{
			return base.AttachEntity(entity) as TabelaProcessamento;
		}
		
		virtual public void Combine(TabelaProcessamentoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaProcessamento this[int index]
		{
			get
			{
				return base[index] as TabelaProcessamento;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaProcessamento);
		}
	}



	[Serializable]
	abstract public class esTabelaProcessamento : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaProcessamentoQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaProcessamento()
		{

		}

		public esTabelaProcessamento(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente, System.Int16 idGrupoProcessamento)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idGrupoProcessamento);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idGrupoProcessamento);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente, System.Int16 idGrupoProcessamento)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaProcessamentoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente, query.IdGrupoProcessamento == idGrupoProcessamento);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente, System.Int16 idGrupoProcessamento)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idGrupoProcessamento);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idGrupoProcessamento);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente, System.Int16 idGrupoProcessamento)
		{
			esTabelaProcessamentoQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente, query.IdGrupoProcessamento == idGrupoProcessamento);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente, System.Int16 idGrupoProcessamento)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);			parms.Add("IdGrupoProcessamento",idGrupoProcessamento);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdGrupoProcessamento": this.str.IdGrupoProcessamento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdGrupoProcessamento":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdGrupoProcessamento = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaProcessamento.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(TabelaProcessamentoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaProcessamentoMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaProcessamento.IdGrupoProcessamento
		/// </summary>
		virtual public System.Int16? IdGrupoProcessamento
		{
			get
			{
				return base.GetSystemInt16(TabelaProcessamentoMetadata.ColumnNames.IdGrupoProcessamento);
			}
			
			set
			{
				if(base.SetSystemInt16(TabelaProcessamentoMetadata.ColumnNames.IdGrupoProcessamento, value))
				{
					this._UpToGrupoProcessamentoByIdGrupoProcessamento = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected GrupoProcessamento _UpToGrupoProcessamentoByIdGrupoProcessamento;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaProcessamento entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdGrupoProcessamento
			{
				get
				{
					System.Int16? data = entity.IdGrupoProcessamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupoProcessamento = null;
					else entity.IdGrupoProcessamento = Convert.ToInt16(value);
				}
			}
			

			private esTabelaProcessamento entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaProcessamentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaProcessamento can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaProcessamento : esTabelaProcessamento
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_TabelaProcessamento_Cliente
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToGrupoProcessamentoByIdGrupoProcessamento - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_TabelaProcessamento_GrupoProcessamento
		/// </summary>

		[XmlIgnore]
		public GrupoProcessamento UpToGrupoProcessamentoByIdGrupoProcessamento
		{
			get
			{
				if(this._UpToGrupoProcessamentoByIdGrupoProcessamento == null
					&& IdGrupoProcessamento != null					)
				{
					this._UpToGrupoProcessamentoByIdGrupoProcessamento = new GrupoProcessamento();
					this._UpToGrupoProcessamentoByIdGrupoProcessamento.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToGrupoProcessamentoByIdGrupoProcessamento", this._UpToGrupoProcessamentoByIdGrupoProcessamento);
					this._UpToGrupoProcessamentoByIdGrupoProcessamento.Query.Where(this._UpToGrupoProcessamentoByIdGrupoProcessamento.Query.IdGrupoProcessamento == this.IdGrupoProcessamento);
					this._UpToGrupoProcessamentoByIdGrupoProcessamento.Query.Load();
				}

				return this._UpToGrupoProcessamentoByIdGrupoProcessamento;
			}
			
			set
			{
				this.RemovePreSave("UpToGrupoProcessamentoByIdGrupoProcessamento");
				

				if(value == null)
				{
					this.IdGrupoProcessamento = null;
					this._UpToGrupoProcessamentoByIdGrupoProcessamento = null;
				}
				else
				{
					this.IdGrupoProcessamento = value.IdGrupoProcessamento;
					this._UpToGrupoProcessamentoByIdGrupoProcessamento = value;
					this.SetPreSave("UpToGrupoProcessamentoByIdGrupoProcessamento", this._UpToGrupoProcessamentoByIdGrupoProcessamento);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToGrupoProcessamentoByIdGrupoProcessamento != null)
			{
				this.IdGrupoProcessamento = this._UpToGrupoProcessamentoByIdGrupoProcessamento.IdGrupoProcessamento;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaProcessamentoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaProcessamentoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, TabelaProcessamentoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdGrupoProcessamento
		{
			get
			{
				return new esQueryItem(this, TabelaProcessamentoMetadata.ColumnNames.IdGrupoProcessamento, esSystemType.Int16);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaProcessamentoCollection")]
	public partial class TabelaProcessamentoCollection : esTabelaProcessamentoCollection, IEnumerable<TabelaProcessamento>
	{
		public TabelaProcessamentoCollection()
		{

		}
		
		public static implicit operator List<TabelaProcessamento>(TabelaProcessamentoCollection coll)
		{
			List<TabelaProcessamento> list = new List<TabelaProcessamento>();
			
			foreach (TabelaProcessamento emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaProcessamentoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaProcessamentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaProcessamento(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaProcessamento();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaProcessamentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaProcessamentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaProcessamentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaProcessamento AddNew()
		{
			TabelaProcessamento entity = base.AddNewEntity() as TabelaProcessamento;
			
			return entity;
		}

		public TabelaProcessamento FindByPrimaryKey(System.Int32 idCliente, System.Int16 idGrupoProcessamento)
		{
			return base.FindByPrimaryKey(idCliente, idGrupoProcessamento) as TabelaProcessamento;
		}


		#region IEnumerable<TabelaProcessamento> Members

		IEnumerator<TabelaProcessamento> IEnumerable<TabelaProcessamento>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaProcessamento;
			}
		}

		#endregion
		
		private TabelaProcessamentoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaProcessamento' table
	/// </summary>

	[Serializable]
	public partial class TabelaProcessamento : esTabelaProcessamento
	{
		public TabelaProcessamento()
		{

		}
	
		public TabelaProcessamento(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaProcessamentoMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaProcessamentoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaProcessamentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaProcessamentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaProcessamentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaProcessamentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaProcessamentoQuery query;
	}



	[Serializable]
	public partial class TabelaProcessamentoQuery : esTabelaProcessamentoQuery
	{
		public TabelaProcessamentoQuery()
		{

		}		
		
		public TabelaProcessamentoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaProcessamentoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaProcessamentoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaProcessamentoMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaProcessamentoMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProcessamentoMetadata.ColumnNames.IdGrupoProcessamento, 1, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TabelaProcessamentoMetadata.PropertyNames.IdGrupoProcessamento;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaProcessamentoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string IdGrupoProcessamento = "IdGrupoProcessamento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string IdGrupoProcessamento = "IdGrupoProcessamento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaProcessamentoMetadata))
			{
				if(TabelaProcessamentoMetadata.mapDelegates == null)
				{
					TabelaProcessamentoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaProcessamentoMetadata.meta == null)
				{
					TabelaProcessamentoMetadata.meta = new TabelaProcessamentoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdGrupoProcessamento", new esTypeMap("smallint", "System.Int16"));			
				
				
				
				meta.Source = "TabelaProcessamento";
				meta.Destination = "TabelaProcessamento";
				
				meta.spInsert = "proc_TabelaProcessamentoInsert";				
				meta.spUpdate = "proc_TabelaProcessamentoUpdate";		
				meta.spDelete = "proc_TabelaProcessamentoDelete";
				meta.spLoadAll = "proc_TabelaProcessamentoLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaProcessamentoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaProcessamentoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
