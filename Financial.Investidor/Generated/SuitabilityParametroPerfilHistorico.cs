/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/09/2015 14:40:55
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityParametroPerfilHistoricoCollection : esEntityCollection
	{
		public esSuitabilityParametroPerfilHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityParametroPerfilHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityParametroPerfilHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityParametroPerfilHistoricoQuery);
		}
		#endregion
		
		virtual public SuitabilityParametroPerfilHistorico DetachEntity(SuitabilityParametroPerfilHistorico entity)
		{
			return base.DetachEntity(entity) as SuitabilityParametroPerfilHistorico;
		}
		
		virtual public SuitabilityParametroPerfilHistorico AttachEntity(SuitabilityParametroPerfilHistorico entity)
		{
			return base.AttachEntity(entity) as SuitabilityParametroPerfilHistorico;
		}
		
		virtual public void Combine(SuitabilityParametroPerfilHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityParametroPerfilHistorico this[int index]
		{
			get
			{
				return base[index] as SuitabilityParametroPerfilHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityParametroPerfilHistorico);
		}
	}



	[Serializable]
	abstract public class esSuitabilityParametroPerfilHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityParametroPerfilHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityParametroPerfilHistorico()
		{

		}

		public esSuitabilityParametroPerfilHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idParametroPerfil)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idParametroPerfil);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idParametroPerfil);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idParametroPerfil)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idParametroPerfil);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idParametroPerfil);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idParametroPerfil)
		{
			esSuitabilityParametroPerfilHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdParametroPerfil == idParametroPerfil);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idParametroPerfil)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdParametroPerfil",idParametroPerfil);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdParametroPerfil": this.str.IdParametroPerfil = (string)value; break;							
						case "IdValidacao": this.str.IdValidacao = (string)value; break;							
						case "Validacao": this.str.Validacao = (string)value; break;							
						case "IdPerfilInvestidor": this.str.IdPerfilInvestidor = (string)value; break;							
						case "PerfilInvestidor": this.str.PerfilInvestidor = (string)value; break;							
						case "Definicao": this.str.Definicao = (string)value; break;							
						case "FaixaPontuacao": this.str.FaixaPontuacao = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdParametroPerfil":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdParametroPerfil = (System.Int32?)value;
							break;
						
						case "IdValidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdValidacao = (System.Int32?)value;
							break;
						
						case "IdPerfilInvestidor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilInvestidor = (System.Int32?)value;
							break;
						
						case "FaixaPontuacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FaixaPontuacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityParametroPerfilHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametroPerfilHistorico.IdParametroPerfil
		/// </summary>
		virtual public System.Int32? IdParametroPerfil
		{
			get
			{
				return base.GetSystemInt32(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.IdParametroPerfil);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.IdParametroPerfil, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametroPerfilHistorico.IdValidacao
		/// </summary>
		virtual public System.Int32? IdValidacao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.IdValidacao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.IdValidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametroPerfilHistorico.Validacao
		/// </summary>
		virtual public System.String Validacao
		{
			get
			{
				return base.GetSystemString(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.Validacao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.Validacao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametroPerfilHistorico.IdPerfilInvestidor
		/// </summary>
		virtual public System.Int32? IdPerfilInvestidor
		{
			get
			{
				return base.GetSystemInt32(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.IdPerfilInvestidor);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.IdPerfilInvestidor, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametroPerfilHistorico.PerfilInvestidor
		/// </summary>
		virtual public System.String PerfilInvestidor
		{
			get
			{
				return base.GetSystemString(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.PerfilInvestidor);
			}
			
			set
			{
				base.SetSystemString(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.PerfilInvestidor, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametroPerfilHistorico.Definicao
		/// </summary>
		virtual public System.String Definicao
		{
			get
			{
				return base.GetSystemString(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.Definicao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.Definicao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametroPerfilHistorico.FaixaPontuacao
		/// </summary>
		virtual public System.Int32? FaixaPontuacao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.FaixaPontuacao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.FaixaPontuacao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametroPerfilHistorico.Tipo
		/// </summary>
		virtual public System.String Tipo
		{
			get
			{
				return base.GetSystemString(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemString(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityParametroPerfilHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdParametroPerfil
			{
				get
				{
					System.Int32? data = entity.IdParametroPerfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdParametroPerfil = null;
					else entity.IdParametroPerfil = Convert.ToInt32(value);
				}
			}
				
			public System.String IdValidacao
			{
				get
				{
					System.Int32? data = entity.IdValidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdValidacao = null;
					else entity.IdValidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String Validacao
			{
				get
				{
					System.String data = entity.Validacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Validacao = null;
					else entity.Validacao = Convert.ToString(value);
				}
			}
				
			public System.String IdPerfilInvestidor
			{
				get
				{
					System.Int32? data = entity.IdPerfilInvestidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilInvestidor = null;
					else entity.IdPerfilInvestidor = Convert.ToInt32(value);
				}
			}
				
			public System.String PerfilInvestidor
			{
				get
				{
					System.String data = entity.PerfilInvestidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerfilInvestidor = null;
					else entity.PerfilInvestidor = Convert.ToString(value);
				}
			}
				
			public System.String Definicao
			{
				get
				{
					System.String data = entity.Definicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Definicao = null;
					else entity.Definicao = Convert.ToString(value);
				}
			}
				
			public System.String FaixaPontuacao
			{
				get
				{
					System.Int32? data = entity.FaixaPontuacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FaixaPontuacao = null;
					else entity.FaixaPontuacao = Convert.ToInt32(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.String data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToString(value);
				}
			}
			

			private esSuitabilityParametroPerfilHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityParametroPerfilHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityParametroPerfilHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityParametroPerfilHistorico : esSuitabilityParametroPerfilHistorico
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityParametroPerfilHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityParametroPerfilHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdParametroPerfil
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.IdParametroPerfil, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdValidacao
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.IdValidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Validacao
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.Validacao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdPerfilInvestidor
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.IdPerfilInvestidor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PerfilInvestidor
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.PerfilInvestidor, esSystemType.String);
			}
		} 
		
		public esQueryItem Definicao
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.Definicao, esSystemType.String);
			}
		} 
		
		public esQueryItem FaixaPontuacao
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.FaixaPontuacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.Tipo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityParametroPerfilHistoricoCollection")]
	public partial class SuitabilityParametroPerfilHistoricoCollection : esSuitabilityParametroPerfilHistoricoCollection, IEnumerable<SuitabilityParametroPerfilHistorico>
	{
		public SuitabilityParametroPerfilHistoricoCollection()
		{

		}
		
		public static implicit operator List<SuitabilityParametroPerfilHistorico>(SuitabilityParametroPerfilHistoricoCollection coll)
		{
			List<SuitabilityParametroPerfilHistorico> list = new List<SuitabilityParametroPerfilHistorico>();
			
			foreach (SuitabilityParametroPerfilHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityParametroPerfilHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityParametroPerfilHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityParametroPerfilHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityParametroPerfilHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityParametroPerfilHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityParametroPerfilHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityParametroPerfilHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityParametroPerfilHistorico AddNew()
		{
			SuitabilityParametroPerfilHistorico entity = base.AddNewEntity() as SuitabilityParametroPerfilHistorico;
			
			return entity;
		}

		public SuitabilityParametroPerfilHistorico FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idParametroPerfil)
		{
			return base.FindByPrimaryKey(dataHistorico, idParametroPerfil) as SuitabilityParametroPerfilHistorico;
		}


		#region IEnumerable<SuitabilityParametroPerfilHistorico> Members

		IEnumerator<SuitabilityParametroPerfilHistorico> IEnumerable<SuitabilityParametroPerfilHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityParametroPerfilHistorico;
			}
		}

		#endregion
		
		private SuitabilityParametroPerfilHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityParametroPerfilHistorico' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityParametroPerfilHistorico : esSuitabilityParametroPerfilHistorico
	{
		public SuitabilityParametroPerfilHistorico()
		{

		}
	
		public SuitabilityParametroPerfilHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityParametroPerfilHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityParametroPerfilHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityParametroPerfilHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityParametroPerfilHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityParametroPerfilHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityParametroPerfilHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityParametroPerfilHistoricoQuery query;
	}



	[Serializable]
	public partial class SuitabilityParametroPerfilHistoricoQuery : esSuitabilityParametroPerfilHistoricoQuery
	{
		public SuitabilityParametroPerfilHistoricoQuery()
		{

		}		
		
		public SuitabilityParametroPerfilHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityParametroPerfilHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityParametroPerfilHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SuitabilityParametroPerfilHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.IdParametroPerfil, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityParametroPerfilHistoricoMetadata.PropertyNames.IdParametroPerfil;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.IdValidacao, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityParametroPerfilHistoricoMetadata.PropertyNames.IdValidacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.Validacao, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityParametroPerfilHistoricoMetadata.PropertyNames.Validacao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.IdPerfilInvestidor, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityParametroPerfilHistoricoMetadata.PropertyNames.IdPerfilInvestidor;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.PerfilInvestidor, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityParametroPerfilHistoricoMetadata.PropertyNames.PerfilInvestidor;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.Definicao, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityParametroPerfilHistoricoMetadata.PropertyNames.Definicao;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.FaixaPontuacao, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityParametroPerfilHistoricoMetadata.PropertyNames.FaixaPontuacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametroPerfilHistoricoMetadata.ColumnNames.Tipo, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityParametroPerfilHistoricoMetadata.PropertyNames.Tipo;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityParametroPerfilHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdParametroPerfil = "IdParametroPerfil";
			 public const string IdValidacao = "IdValidacao";
			 public const string Validacao = "Validacao";
			 public const string IdPerfilInvestidor = "IdPerfilInvestidor";
			 public const string PerfilInvestidor = "PerfilInvestidor";
			 public const string Definicao = "Definicao";
			 public const string FaixaPontuacao = "FaixaPontuacao";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdParametroPerfil = "IdParametroPerfil";
			 public const string IdValidacao = "IdValidacao";
			 public const string Validacao = "Validacao";
			 public const string IdPerfilInvestidor = "IdPerfilInvestidor";
			 public const string PerfilInvestidor = "PerfilInvestidor";
			 public const string Definicao = "Definicao";
			 public const string FaixaPontuacao = "FaixaPontuacao";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityParametroPerfilHistoricoMetadata))
			{
				if(SuitabilityParametroPerfilHistoricoMetadata.mapDelegates == null)
				{
					SuitabilityParametroPerfilHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityParametroPerfilHistoricoMetadata.meta == null)
				{
					SuitabilityParametroPerfilHistoricoMetadata.meta = new SuitabilityParametroPerfilHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdParametroPerfil", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdValidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Validacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdPerfilInvestidor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PerfilInvestidor", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Definicao", new esTypeMap("text", "System.String"));
				meta.AddTypeMap("FaixaPontuacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Tipo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "SuitabilityParametroPerfilHistorico";
				meta.Destination = "SuitabilityParametroPerfilHistorico";
				
				meta.spInsert = "proc_SuitabilityParametroPerfilHistoricoInsert";				
				meta.spUpdate = "proc_SuitabilityParametroPerfilHistoricoUpdate";		
				meta.spDelete = "proc_SuitabilityParametroPerfilHistoricoDelete";
				meta.spLoadAll = "proc_SuitabilityParametroPerfilHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityParametroPerfilHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityParametroPerfilHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
