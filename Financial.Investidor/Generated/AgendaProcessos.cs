/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/05/2015 12:56:44
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esAgendaProcessosCollection : esEntityCollection
	{
		public esAgendaProcessosCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AgendaProcessosCollection";
		}

		#region Query Logic
		protected void InitQuery(esAgendaProcessosQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAgendaProcessosQuery);
		}
		#endregion
		
		virtual public AgendaProcessos DetachEntity(AgendaProcessos entity)
		{
			return base.DetachEntity(entity) as AgendaProcessos;
		}
		
		virtual public AgendaProcessos AttachEntity(AgendaProcessos entity)
		{
			return base.AttachEntity(entity) as AgendaProcessos;
		}
		
		virtual public void Combine(AgendaProcessosCollection collection)
		{
			base.Combine(collection);
		}
		
		new public AgendaProcessos this[int index]
		{
			get
			{
				return base[index] as AgendaProcessos;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(AgendaProcessos);
		}
	}



	[Serializable]
	abstract public class esAgendaProcessos : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAgendaProcessosQuery GetDynamicQuery()
		{
			return null;
		}

		public esAgendaProcessos()
		{

		}

		public esAgendaProcessos(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idAgendaProcesso)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgendaProcesso);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgendaProcesso);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idAgendaProcesso)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgendaProcesso);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgendaProcesso);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idAgendaProcesso)
		{
			esAgendaProcessosQuery query = this.GetDynamicQuery();
			query.Where(query.IdAgendaProcesso == idAgendaProcesso);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idAgendaProcesso)
		{
			esParameters parms = new esParameters();
			parms.Add("IdAgendaProcesso",idAgendaProcesso);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdAgendaProcesso": this.str.IdAgendaProcesso = (string)value; break;							
						case "IdPerfil": this.str.IdPerfil = (string)value; break;							
						case "HoraExecucao": this.str.HoraExecucao = (string)value; break;							
						case "DataFinal": this.str.DataFinal = (string)value; break;							
						case "Ativo": this.str.Ativo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdAgendaProcesso":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgendaProcesso = (System.Int32?)value;
							break;
						
						case "IdPerfil":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfil = (System.Int32?)value;
							break;
						
						case "HoraExecucao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.HoraExecucao = (System.DateTime?)value;
							break;
						
						case "DataFinal":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFinal = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to AgendaProcessos.IdAgendaProcesso
		/// </summary>
		virtual public System.Int32? IdAgendaProcesso
		{
			get
			{
				return base.GetSystemInt32(AgendaProcessosMetadata.ColumnNames.IdAgendaProcesso);
			}
			
			set
			{
				base.SetSystemInt32(AgendaProcessosMetadata.ColumnNames.IdAgendaProcesso, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaProcessos.IdPerfil
		/// </summary>
		virtual public System.Int32? IdPerfil
		{
			get
			{
				return base.GetSystemInt32(AgendaProcessosMetadata.ColumnNames.IdPerfil);
			}
			
			set
			{
				if(base.SetSystemInt32(AgendaProcessosMetadata.ColumnNames.IdPerfil, value))
				{
					this._UpToPerfilProcessamentoByIdPerfil = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AgendaProcessos.HoraExecucao
		/// </summary>
		virtual public System.DateTime? HoraExecucao
		{
			get
			{
				return base.GetSystemDateTime(AgendaProcessosMetadata.ColumnNames.HoraExecucao);
			}
			
			set
			{
				base.SetSystemDateTime(AgendaProcessosMetadata.ColumnNames.HoraExecucao, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaProcessos.DataFinal
		/// </summary>
		virtual public System.DateTime? DataFinal
		{
			get
			{
				return base.GetSystemDateTime(AgendaProcessosMetadata.ColumnNames.DataFinal);
			}
			
			set
			{
				base.SetSystemDateTime(AgendaProcessosMetadata.ColumnNames.DataFinal, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaProcessos.Ativo
		/// </summary>
		virtual public System.String Ativo
		{
			get
			{
				return base.GetSystemString(AgendaProcessosMetadata.ColumnNames.Ativo);
			}
			
			set
			{
				base.SetSystemString(AgendaProcessosMetadata.ColumnNames.Ativo, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected PerfilProcessamento _UpToPerfilProcessamentoByIdPerfil;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAgendaProcessos entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdAgendaProcesso
			{
				get
				{
					System.Int32? data = entity.IdAgendaProcesso;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgendaProcesso = null;
					else entity.IdAgendaProcesso = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPerfil
			{
				get
				{
					System.Int32? data = entity.IdPerfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfil = null;
					else entity.IdPerfil = Convert.ToInt32(value);
				}
			}
				
			public System.String HoraExecucao
			{
				get
				{
					System.DateTime? data = entity.HoraExecucao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.HoraExecucao = null;
					else entity.HoraExecucao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataFinal
			{
				get
				{
					System.DateTime? data = entity.DataFinal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFinal = null;
					else entity.DataFinal = Convert.ToDateTime(value);
				}
			}
				
			public System.String Ativo
			{
				get
				{
					System.String data = entity.Ativo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Ativo = null;
					else entity.Ativo = Convert.ToString(value);
				}
			}
			

			private esAgendaProcessos entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAgendaProcessosQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAgendaProcessos can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class AgendaProcessos : esAgendaProcessos
	{

				
		#region LogProcessamentoAutomaticoCollectionByIdAgendaProcesso - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AgendaProcessos_LogProcessamentoAutomatico_FK1
		/// </summary>

		[XmlIgnore]
		public LogProcessamentoAutomaticoCollection LogProcessamentoAutomaticoCollectionByIdAgendaProcesso
		{
			get
			{
				if(this._LogProcessamentoAutomaticoCollectionByIdAgendaProcesso == null)
				{
					this._LogProcessamentoAutomaticoCollectionByIdAgendaProcesso = new LogProcessamentoAutomaticoCollection();
					this._LogProcessamentoAutomaticoCollectionByIdAgendaProcesso.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LogProcessamentoAutomaticoCollectionByIdAgendaProcesso", this._LogProcessamentoAutomaticoCollectionByIdAgendaProcesso);
				
					if(this.IdAgendaProcesso != null)
					{
						this._LogProcessamentoAutomaticoCollectionByIdAgendaProcesso.Query.Where(this._LogProcessamentoAutomaticoCollectionByIdAgendaProcesso.Query.IdAgendaProcesso == this.IdAgendaProcesso);
						this._LogProcessamentoAutomaticoCollectionByIdAgendaProcesso.Query.Load();

						// Auto-hookup Foreign Keys
						this._LogProcessamentoAutomaticoCollectionByIdAgendaProcesso.fks.Add(LogProcessamentoAutomaticoMetadata.ColumnNames.IdAgendaProcesso, this.IdAgendaProcesso);
					}
				}

				return this._LogProcessamentoAutomaticoCollectionByIdAgendaProcesso;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LogProcessamentoAutomaticoCollectionByIdAgendaProcesso != null) 
				{ 
					this.RemovePostSave("LogProcessamentoAutomaticoCollectionByIdAgendaProcesso"); 
					this._LogProcessamentoAutomaticoCollectionByIdAgendaProcesso = null;
					
				} 
			} 			
		}

		private LogProcessamentoAutomaticoCollection _LogProcessamentoAutomaticoCollectionByIdAgendaProcesso;
		#endregion

				
		#region UpToPerfilProcessamentoByIdPerfil - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PerfilProcessamento_AgendaProcessos_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilProcessamento UpToPerfilProcessamentoByIdPerfil
		{
			get
			{
				if(this._UpToPerfilProcessamentoByIdPerfil == null
					&& IdPerfil != null					)
				{
					this._UpToPerfilProcessamentoByIdPerfil = new PerfilProcessamento();
					this._UpToPerfilProcessamentoByIdPerfil.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPerfilProcessamentoByIdPerfil", this._UpToPerfilProcessamentoByIdPerfil);
					this._UpToPerfilProcessamentoByIdPerfil.Query.Where(this._UpToPerfilProcessamentoByIdPerfil.Query.IdPerfil == this.IdPerfil);
					this._UpToPerfilProcessamentoByIdPerfil.Query.Load();
				}

				return this._UpToPerfilProcessamentoByIdPerfil;
			}
			
			set
			{
				this.RemovePreSave("UpToPerfilProcessamentoByIdPerfil");
				

				if(value == null)
				{
					this.IdPerfil = null;
					this._UpToPerfilProcessamentoByIdPerfil = null;
				}
				else
				{
					this.IdPerfil = value.IdPerfil;
					this._UpToPerfilProcessamentoByIdPerfil = value;
					this.SetPreSave("UpToPerfilProcessamentoByIdPerfil", this._UpToPerfilProcessamentoByIdPerfil);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "LogProcessamentoAutomaticoCollectionByIdAgendaProcesso", typeof(LogProcessamentoAutomaticoCollection), new LogProcessamentoAutomatico()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToPerfilProcessamentoByIdPerfil != null)
			{
				this.IdPerfil = this._UpToPerfilProcessamentoByIdPerfil.IdPerfil;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._LogProcessamentoAutomaticoCollectionByIdAgendaProcesso != null)
			{
				foreach(LogProcessamentoAutomatico obj in this._LogProcessamentoAutomaticoCollectionByIdAgendaProcesso)
				{
					if(obj.es.IsAdded)
					{
						obj.IdAgendaProcesso = this.IdAgendaProcesso;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAgendaProcessosQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AgendaProcessosMetadata.Meta();
			}
		}	
		

		public esQueryItem IdAgendaProcesso
		{
			get
			{
				return new esQueryItem(this, AgendaProcessosMetadata.ColumnNames.IdAgendaProcesso, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPerfil
		{
			get
			{
				return new esQueryItem(this, AgendaProcessosMetadata.ColumnNames.IdPerfil, esSystemType.Int32);
			}
		} 
		
		public esQueryItem HoraExecucao
		{
			get
			{
				return new esQueryItem(this, AgendaProcessosMetadata.ColumnNames.HoraExecucao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataFinal
		{
			get
			{
				return new esQueryItem(this, AgendaProcessosMetadata.ColumnNames.DataFinal, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Ativo
		{
			get
			{
				return new esQueryItem(this, AgendaProcessosMetadata.ColumnNames.Ativo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("AgendaProcessosCollection")]
	public partial class AgendaProcessosCollection : esAgendaProcessosCollection, IEnumerable<AgendaProcessos>
	{
		public AgendaProcessosCollection()
		{

		}
		
		public static implicit operator List<AgendaProcessos>(AgendaProcessosCollection coll)
		{
			List<AgendaProcessos> list = new List<AgendaProcessos>();
			
			foreach (AgendaProcessos emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AgendaProcessosMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgendaProcessosQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new AgendaProcessos(row);
		}

		override protected esEntity CreateEntity()
		{
			return new AgendaProcessos();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AgendaProcessosQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgendaProcessosQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AgendaProcessosQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public AgendaProcessos AddNew()
		{
			AgendaProcessos entity = base.AddNewEntity() as AgendaProcessos;
			
			return entity;
		}

		public AgendaProcessos FindByPrimaryKey(System.Int32 idAgendaProcesso)
		{
			return base.FindByPrimaryKey(idAgendaProcesso) as AgendaProcessos;
		}


		#region IEnumerable<AgendaProcessos> Members

		IEnumerator<AgendaProcessos> IEnumerable<AgendaProcessos>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as AgendaProcessos;
			}
		}

		#endregion
		
		private AgendaProcessosQuery query;
	}


	/// <summary>
	/// Encapsulates the 'AgendaProcessos' table
	/// </summary>

	[Serializable]
	public partial class AgendaProcessos : esAgendaProcessos
	{
		public AgendaProcessos()
		{

		}
	
		public AgendaProcessos(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AgendaProcessosMetadata.Meta();
			}
		}
		
		
		
		override protected esAgendaProcessosQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgendaProcessosQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AgendaProcessosQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgendaProcessosQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AgendaProcessosQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AgendaProcessosQuery query;
	}



	[Serializable]
	public partial class AgendaProcessosQuery : esAgendaProcessosQuery
	{
		public AgendaProcessosQuery()
		{

		}		
		
		public AgendaProcessosQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AgendaProcessosMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AgendaProcessosMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AgendaProcessosMetadata.ColumnNames.IdAgendaProcesso, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaProcessosMetadata.PropertyNames.IdAgendaProcesso;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaProcessosMetadata.ColumnNames.IdPerfil, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaProcessosMetadata.PropertyNames.IdPerfil;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaProcessosMetadata.ColumnNames.HoraExecucao, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AgendaProcessosMetadata.PropertyNames.HoraExecucao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaProcessosMetadata.ColumnNames.DataFinal, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AgendaProcessosMetadata.PropertyNames.DataFinal;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaProcessosMetadata.ColumnNames.Ativo, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = AgendaProcessosMetadata.PropertyNames.Ativo;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public AgendaProcessosMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdAgendaProcesso = "IdAgendaProcesso";
			 public const string IdPerfil = "IdPerfil";
			 public const string HoraExecucao = "HoraExecucao";
			 public const string DataFinal = "DataFinal";
			 public const string Ativo = "Ativo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdAgendaProcesso = "IdAgendaProcesso";
			 public const string IdPerfil = "IdPerfil";
			 public const string HoraExecucao = "HoraExecucao";
			 public const string DataFinal = "DataFinal";
			 public const string Ativo = "Ativo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AgendaProcessosMetadata))
			{
				if(AgendaProcessosMetadata.mapDelegates == null)
				{
					AgendaProcessosMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AgendaProcessosMetadata.meta == null)
				{
					AgendaProcessosMetadata.meta = new AgendaProcessosMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdAgendaProcesso", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPerfil", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("HoraExecucao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataFinal", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Ativo", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "AgendaProcessos";
				meta.Destination = "AgendaProcessos";
				
				meta.spInsert = "proc_AgendaProcessosInsert";				
				meta.spUpdate = "proc_AgendaProcessosUpdate";		
				meta.spDelete = "proc_AgendaProcessosDelete";
				meta.spLoadAll = "proc_AgendaProcessosLoadAll";
				meta.spLoadByPrimaryKey = "proc_AgendaProcessosLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AgendaProcessosMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
