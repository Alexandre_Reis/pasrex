/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/09/2015 14:19:41
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityOpcaoHistoricoCollection : esEntityCollection
	{
		public esSuitabilityOpcaoHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityOpcaoHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityOpcaoHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityOpcaoHistoricoQuery);
		}
		#endregion
		
		virtual public SuitabilityOpcaoHistorico DetachEntity(SuitabilityOpcaoHistorico entity)
		{
			return base.DetachEntity(entity) as SuitabilityOpcaoHistorico;
		}
		
		virtual public SuitabilityOpcaoHistorico AttachEntity(SuitabilityOpcaoHistorico entity)
		{
			return base.AttachEntity(entity) as SuitabilityOpcaoHistorico;
		}
		
		virtual public void Combine(SuitabilityOpcaoHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityOpcaoHistorico this[int index]
		{
			get
			{
				return base[index] as SuitabilityOpcaoHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityOpcaoHistorico);
		}
	}



	[Serializable]
	abstract public class esSuitabilityOpcaoHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityOpcaoHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityOpcaoHistorico()
		{

		}

		public esSuitabilityOpcaoHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idOpcao, System.Int32 idQuestao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idOpcao, idQuestao);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idOpcao, idQuestao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idOpcao, System.Int32 idQuestao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idOpcao, idQuestao);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idOpcao, idQuestao);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idOpcao, System.Int32 idQuestao)
		{
			esSuitabilityOpcaoHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdOpcao == idOpcao, query.IdQuestao == idQuestao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idOpcao, System.Int32 idQuestao)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdOpcao",idOpcao);			parms.Add("IdQuestao",idQuestao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdQuestao": this.str.IdQuestao = (string)value; break;							
						case "Questao": this.str.Questao = (string)value; break;							
						case "IdOpcao": this.str.IdOpcao = (string)value; break;							
						case "Opcao": this.str.Opcao = (string)value; break;							
						case "Pontos": this.str.Pontos = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdQuestao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdQuestao = (System.Int32?)value;
							break;
						
						case "IdOpcao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOpcao = (System.Int32?)value;
							break;
						
						case "Pontos":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Pontos = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityOpcaoHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(SuitabilityOpcaoHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(SuitabilityOpcaoHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityOpcaoHistorico.IdQuestao
		/// </summary>
		virtual public System.Int32? IdQuestao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityOpcaoHistoricoMetadata.ColumnNames.IdQuestao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityOpcaoHistoricoMetadata.ColumnNames.IdQuestao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityOpcaoHistorico.Questao
		/// </summary>
		virtual public System.String Questao
		{
			get
			{
				return base.GetSystemString(SuitabilityOpcaoHistoricoMetadata.ColumnNames.Questao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityOpcaoHistoricoMetadata.ColumnNames.Questao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityOpcaoHistorico.IdOpcao
		/// </summary>
		virtual public System.Int32? IdOpcao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityOpcaoHistoricoMetadata.ColumnNames.IdOpcao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityOpcaoHistoricoMetadata.ColumnNames.IdOpcao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityOpcaoHistorico.Opcao
		/// </summary>
		virtual public System.String Opcao
		{
			get
			{
				return base.GetSystemString(SuitabilityOpcaoHistoricoMetadata.ColumnNames.Opcao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityOpcaoHistoricoMetadata.ColumnNames.Opcao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityOpcaoHistorico.Pontos
		/// </summary>
		virtual public System.Int32? Pontos
		{
			get
			{
				return base.GetSystemInt32(SuitabilityOpcaoHistoricoMetadata.ColumnNames.Pontos);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityOpcaoHistoricoMetadata.ColumnNames.Pontos, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityOpcaoHistorico.Tipo
		/// </summary>
		virtual public System.String Tipo
		{
			get
			{
				return base.GetSystemString(SuitabilityOpcaoHistoricoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemString(SuitabilityOpcaoHistoricoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityOpcaoHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdQuestao
			{
				get
				{
					System.Int32? data = entity.IdQuestao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdQuestao = null;
					else entity.IdQuestao = Convert.ToInt32(value);
				}
			}
				
			public System.String Questao
			{
				get
				{
					System.String data = entity.Questao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Questao = null;
					else entity.Questao = Convert.ToString(value);
				}
			}
				
			public System.String IdOpcao
			{
				get
				{
					System.Int32? data = entity.IdOpcao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOpcao = null;
					else entity.IdOpcao = Convert.ToInt32(value);
				}
			}
				
			public System.String Opcao
			{
				get
				{
					System.String data = entity.Opcao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Opcao = null;
					else entity.Opcao = Convert.ToString(value);
				}
			}
				
			public System.String Pontos
			{
				get
				{
					System.Int32? data = entity.Pontos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pontos = null;
					else entity.Pontos = Convert.ToInt32(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.String data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToString(value);
				}
			}
			

			private esSuitabilityOpcaoHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityOpcaoHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityOpcaoHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityOpcaoHistorico : esSuitabilityOpcaoHistorico
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityOpcaoHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityOpcaoHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, SuitabilityOpcaoHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdQuestao
		{
			get
			{
				return new esQueryItem(this, SuitabilityOpcaoHistoricoMetadata.ColumnNames.IdQuestao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Questao
		{
			get
			{
				return new esQueryItem(this, SuitabilityOpcaoHistoricoMetadata.ColumnNames.Questao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdOpcao
		{
			get
			{
				return new esQueryItem(this, SuitabilityOpcaoHistoricoMetadata.ColumnNames.IdOpcao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Opcao
		{
			get
			{
				return new esQueryItem(this, SuitabilityOpcaoHistoricoMetadata.ColumnNames.Opcao, esSystemType.String);
			}
		} 
		
		public esQueryItem Pontos
		{
			get
			{
				return new esQueryItem(this, SuitabilityOpcaoHistoricoMetadata.ColumnNames.Pontos, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, SuitabilityOpcaoHistoricoMetadata.ColumnNames.Tipo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityOpcaoHistoricoCollection")]
	public partial class SuitabilityOpcaoHistoricoCollection : esSuitabilityOpcaoHistoricoCollection, IEnumerable<SuitabilityOpcaoHistorico>
	{
		public SuitabilityOpcaoHistoricoCollection()
		{

		}
		
		public static implicit operator List<SuitabilityOpcaoHistorico>(SuitabilityOpcaoHistoricoCollection coll)
		{
			List<SuitabilityOpcaoHistorico> list = new List<SuitabilityOpcaoHistorico>();
			
			foreach (SuitabilityOpcaoHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityOpcaoHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityOpcaoHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityOpcaoHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityOpcaoHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityOpcaoHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityOpcaoHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityOpcaoHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityOpcaoHistorico AddNew()
		{
			SuitabilityOpcaoHistorico entity = base.AddNewEntity() as SuitabilityOpcaoHistorico;
			
			return entity;
		}

		public SuitabilityOpcaoHistorico FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idOpcao, System.Int32 idQuestao)
		{
			return base.FindByPrimaryKey(dataHistorico, idOpcao, idQuestao) as SuitabilityOpcaoHistorico;
		}


		#region IEnumerable<SuitabilityOpcaoHistorico> Members

		IEnumerator<SuitabilityOpcaoHistorico> IEnumerable<SuitabilityOpcaoHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityOpcaoHistorico;
			}
		}

		#endregion
		
		private SuitabilityOpcaoHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityOpcaoHistorico' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityOpcaoHistorico : esSuitabilityOpcaoHistorico
	{
		public SuitabilityOpcaoHistorico()
		{

		}
	
		public SuitabilityOpcaoHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityOpcaoHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityOpcaoHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityOpcaoHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityOpcaoHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityOpcaoHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityOpcaoHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityOpcaoHistoricoQuery query;
	}



	[Serializable]
	public partial class SuitabilityOpcaoHistoricoQuery : esSuitabilityOpcaoHistoricoQuery
	{
		public SuitabilityOpcaoHistoricoQuery()
		{

		}		
		
		public SuitabilityOpcaoHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityOpcaoHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityOpcaoHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityOpcaoHistoricoMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SuitabilityOpcaoHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityOpcaoHistoricoMetadata.ColumnNames.IdQuestao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityOpcaoHistoricoMetadata.PropertyNames.IdQuestao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityOpcaoHistoricoMetadata.ColumnNames.Questao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityOpcaoHistoricoMetadata.PropertyNames.Questao;
			c.CharacterMaxLength = 2000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityOpcaoHistoricoMetadata.ColumnNames.IdOpcao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityOpcaoHistoricoMetadata.PropertyNames.IdOpcao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityOpcaoHistoricoMetadata.ColumnNames.Opcao, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityOpcaoHistoricoMetadata.PropertyNames.Opcao;
			c.CharacterMaxLength = 2000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityOpcaoHistoricoMetadata.ColumnNames.Pontos, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityOpcaoHistoricoMetadata.PropertyNames.Pontos;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityOpcaoHistoricoMetadata.ColumnNames.Tipo, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityOpcaoHistoricoMetadata.PropertyNames.Tipo;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityOpcaoHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdQuestao = "IdQuestao";
			 public const string Questao = "Questao";
			 public const string IdOpcao = "IdOpcao";
			 public const string Opcao = "Opcao";
			 public const string Pontos = "Pontos";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdQuestao = "IdQuestao";
			 public const string Questao = "Questao";
			 public const string IdOpcao = "IdOpcao";
			 public const string Opcao = "Opcao";
			 public const string Pontos = "Pontos";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityOpcaoHistoricoMetadata))
			{
				if(SuitabilityOpcaoHistoricoMetadata.mapDelegates == null)
				{
					SuitabilityOpcaoHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityOpcaoHistoricoMetadata.meta == null)
				{
					SuitabilityOpcaoHistoricoMetadata.meta = new SuitabilityOpcaoHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdQuestao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Questao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdOpcao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Opcao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Pontos", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Tipo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "SuitabilityOpcaoHistorico";
				meta.Destination = "SuitabilityOpcaoHistorico";
				
				meta.spInsert = "proc_SuitabilityOpcaoHistoricoInsert";				
				meta.spUpdate = "proc_SuitabilityOpcaoHistoricoUpdate";		
				meta.spDelete = "proc_SuitabilityOpcaoHistoricoDelete";
				meta.spLoadAll = "proc_SuitabilityOpcaoHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityOpcaoHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityOpcaoHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
