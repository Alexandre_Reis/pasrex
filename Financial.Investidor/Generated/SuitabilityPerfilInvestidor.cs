/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 27/08/2015 11:01:53
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityPerfilInvestidorCollection : esEntityCollection
	{
		public esSuitabilityPerfilInvestidorCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityPerfilInvestidorCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityPerfilInvestidorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityPerfilInvestidorQuery);
		}
		#endregion
		
		virtual public SuitabilityPerfilInvestidor DetachEntity(SuitabilityPerfilInvestidor entity)
		{
			return base.DetachEntity(entity) as SuitabilityPerfilInvestidor;
		}
		
		virtual public SuitabilityPerfilInvestidor AttachEntity(SuitabilityPerfilInvestidor entity)
		{
			return base.AttachEntity(entity) as SuitabilityPerfilInvestidor;
		}
		
		virtual public void Combine(SuitabilityPerfilInvestidorCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityPerfilInvestidor this[int index]
		{
			get
			{
				return base[index] as SuitabilityPerfilInvestidor;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityPerfilInvestidor);
		}
	}



	[Serializable]
	abstract public class esSuitabilityPerfilInvestidor : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityPerfilInvestidorQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityPerfilInvestidor()
		{

		}

		public esSuitabilityPerfilInvestidor(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPerfilInvestidor)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilInvestidor);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilInvestidor);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPerfilInvestidor)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilInvestidor);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilInvestidor);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPerfilInvestidor)
		{
			esSuitabilityPerfilInvestidorQuery query = this.GetDynamicQuery();
			query.Where(query.IdPerfilInvestidor == idPerfilInvestidor);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPerfilInvestidor)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPerfilInvestidor",idPerfilInvestidor);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPerfilInvestidor": this.str.IdPerfilInvestidor = (string)value; break;							
						case "Nivel": this.str.Nivel = (string)value; break;							
						case "Perfil": this.str.Perfil = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPerfilInvestidor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilInvestidor = (System.Int32?)value;
							break;
						
						case "Nivel":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Nivel = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityPerfilInvestidor.IdPerfilInvestidor
		/// </summary>
		virtual public System.Int32? IdPerfilInvestidor
		{
			get
			{
				return base.GetSystemInt32(SuitabilityPerfilInvestidorMetadata.ColumnNames.IdPerfilInvestidor);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityPerfilInvestidorMetadata.ColumnNames.IdPerfilInvestidor, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfilInvestidor.Nivel
		/// </summary>
		virtual public System.Int32? Nivel
		{
			get
			{
				return base.GetSystemInt32(SuitabilityPerfilInvestidorMetadata.ColumnNames.Nivel);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityPerfilInvestidorMetadata.ColumnNames.Nivel, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfilInvestidor.Perfil
		/// </summary>
		virtual public System.String Perfil
		{
			get
			{
				return base.GetSystemString(SuitabilityPerfilInvestidorMetadata.ColumnNames.Perfil);
			}
			
			set
			{
				base.SetSystemString(SuitabilityPerfilInvestidorMetadata.ColumnNames.Perfil, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityPerfilInvestidor entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPerfilInvestidor
			{
				get
				{
					System.Int32? data = entity.IdPerfilInvestidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilInvestidor = null;
					else entity.IdPerfilInvestidor = Convert.ToInt32(value);
				}
			}
				
			public System.String Nivel
			{
				get
				{
					System.Int32? data = entity.Nivel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nivel = null;
					else entity.Nivel = Convert.ToInt32(value);
				}
			}
				
			public System.String Perfil
			{
				get
				{
					System.String data = entity.Perfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Perfil = null;
					else entity.Perfil = Convert.ToString(value);
				}
			}
			

			private esSuitabilityPerfilInvestidor entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityPerfilInvestidorQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityPerfilInvestidor can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityPerfilInvestidor : esSuitabilityPerfilInvestidor
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityPerfilInvestidorQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityPerfilInvestidorMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPerfilInvestidor
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilInvestidorMetadata.ColumnNames.IdPerfilInvestidor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nivel
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilInvestidorMetadata.ColumnNames.Nivel, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Perfil
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilInvestidorMetadata.ColumnNames.Perfil, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityPerfilInvestidorCollection")]
	public partial class SuitabilityPerfilInvestidorCollection : esSuitabilityPerfilInvestidorCollection, IEnumerable<SuitabilityPerfilInvestidor>
	{
		public SuitabilityPerfilInvestidorCollection()
		{

		}
		
		public static implicit operator List<SuitabilityPerfilInvestidor>(SuitabilityPerfilInvestidorCollection coll)
		{
			List<SuitabilityPerfilInvestidor> list = new List<SuitabilityPerfilInvestidor>();
			
			foreach (SuitabilityPerfilInvestidor emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityPerfilInvestidorMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityPerfilInvestidorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityPerfilInvestidor(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityPerfilInvestidor();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityPerfilInvestidorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityPerfilInvestidorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityPerfilInvestidorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityPerfilInvestidor AddNew()
		{
			SuitabilityPerfilInvestidor entity = base.AddNewEntity() as SuitabilityPerfilInvestidor;
			
			return entity;
		}

		public SuitabilityPerfilInvestidor FindByPrimaryKey(System.Int32 idPerfilInvestidor)
		{
			return base.FindByPrimaryKey(idPerfilInvestidor) as SuitabilityPerfilInvestidor;
		}


		#region IEnumerable<SuitabilityPerfilInvestidor> Members

		IEnumerator<SuitabilityPerfilInvestidor> IEnumerable<SuitabilityPerfilInvestidor>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityPerfilInvestidor;
			}
		}

		#endregion
		
		private SuitabilityPerfilInvestidorQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityPerfilInvestidor' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityPerfilInvestidor : esSuitabilityPerfilInvestidor
	{
		public SuitabilityPerfilInvestidor()
		{

		}
	
		public SuitabilityPerfilInvestidor(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityPerfilInvestidorMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityPerfilInvestidorQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityPerfilInvestidorQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityPerfilInvestidorQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityPerfilInvestidorQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityPerfilInvestidorQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityPerfilInvestidorQuery query;
	}



	[Serializable]
	public partial class SuitabilityPerfilInvestidorQuery : esSuitabilityPerfilInvestidorQuery
	{
		public SuitabilityPerfilInvestidorQuery()
		{

		}		
		
		public SuitabilityPerfilInvestidorQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityPerfilInvestidorMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityPerfilInvestidorMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityPerfilInvestidorMetadata.ColumnNames.IdPerfilInvestidor, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityPerfilInvestidorMetadata.PropertyNames.IdPerfilInvestidor;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilInvestidorMetadata.ColumnNames.Nivel, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityPerfilInvestidorMetadata.PropertyNames.Nivel;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilInvestidorMetadata.ColumnNames.Perfil, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityPerfilInvestidorMetadata.PropertyNames.Perfil;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityPerfilInvestidorMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPerfilInvestidor = "IdPerfilInvestidor";
			 public const string Nivel = "Nivel";
			 public const string Perfil = "Perfil";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPerfilInvestidor = "IdPerfilInvestidor";
			 public const string Nivel = "Nivel";
			 public const string Perfil = "Perfil";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityPerfilInvestidorMetadata))
			{
				if(SuitabilityPerfilInvestidorMetadata.mapDelegates == null)
				{
					SuitabilityPerfilInvestidorMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityPerfilInvestidorMetadata.meta == null)
				{
					SuitabilityPerfilInvestidorMetadata.meta = new SuitabilityPerfilInvestidorMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPerfilInvestidor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nivel", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Perfil", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "SuitabilityPerfilInvestidor";
				meta.Destination = "SuitabilityPerfilInvestidor";
				
				meta.spInsert = "proc_SuitabilityPerfilInvestidorInsert";				
				meta.spUpdate = "proc_SuitabilityPerfilInvestidorUpdate";		
				meta.spDelete = "proc_SuitabilityPerfilInvestidorDelete";
				meta.spLoadAll = "proc_SuitabilityPerfilInvestidorLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityPerfilInvestidorLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityPerfilInvestidorMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
