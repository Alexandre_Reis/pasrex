/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 2/11/2015 10:56:12 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Investidor
{

	[Serializable]
	abstract public class esGrupoProcessamentoCollection : esEntityCollection
	{
		public esGrupoProcessamentoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "GrupoProcessamentoCollection";
		}

		#region Query Logic
		protected void InitQuery(esGrupoProcessamentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esGrupoProcessamentoQuery);
		}
		#endregion
		
		virtual public GrupoProcessamento DetachEntity(GrupoProcessamento entity)
		{
			return base.DetachEntity(entity) as GrupoProcessamento;
		}
		
		virtual public GrupoProcessamento AttachEntity(GrupoProcessamento entity)
		{
			return base.AttachEntity(entity) as GrupoProcessamento;
		}
		
		virtual public void Combine(GrupoProcessamentoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public GrupoProcessamento this[int index]
		{
			get
			{
				return base[index] as GrupoProcessamento;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(GrupoProcessamento);
		}
	}



	[Serializable]
	abstract public class esGrupoProcessamento : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esGrupoProcessamentoQuery GetDynamicQuery()
		{
			return null;
		}

		public esGrupoProcessamento()
		{

		}

		public esGrupoProcessamento(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int16 idGrupoProcessamento)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupoProcessamento);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupoProcessamento);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int16 idGrupoProcessamento)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esGrupoProcessamentoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdGrupoProcessamento == idGrupoProcessamento);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int16 idGrupoProcessamento)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupoProcessamento);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupoProcessamento);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int16 idGrupoProcessamento)
		{
			esGrupoProcessamentoQuery query = this.GetDynamicQuery();
			query.Where(query.IdGrupoProcessamento == idGrupoProcessamento);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int16 idGrupoProcessamento)
		{
			esParameters parms = new esParameters();
			parms.Add("IdGrupoProcessamento",idGrupoProcessamento);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdGrupoProcessamento": this.str.IdGrupoProcessamento = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdGrupoProcessamento":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdGrupoProcessamento = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to GrupoProcessamento.IdGrupoProcessamento
		/// </summary>
		virtual public System.Int16? IdGrupoProcessamento
		{
			get
			{
				return base.GetSystemInt16(GrupoProcessamentoMetadata.ColumnNames.IdGrupoProcessamento);
			}
			
			set
			{
				base.SetSystemInt16(GrupoProcessamentoMetadata.ColumnNames.IdGrupoProcessamento, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupoProcessamento.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(GrupoProcessamentoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(GrupoProcessamentoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esGrupoProcessamento entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdGrupoProcessamento
			{
				get
				{
					System.Int16? data = entity.IdGrupoProcessamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupoProcessamento = null;
					else entity.IdGrupoProcessamento = Convert.ToInt16(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esGrupoProcessamento entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esGrupoProcessamentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esGrupoProcessamento can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class GrupoProcessamento : esGrupoProcessamento
	{

				
		#region ClienteCollectionByIdGrupoProcessamento - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - GrupoProcessamento_Cliente_FK1
		/// </summary>

		[XmlIgnore]
		public ClienteCollection ClienteCollectionByIdGrupoProcessamento
		{
			get
			{
				if(this._ClienteCollectionByIdGrupoProcessamento == null)
				{
					this._ClienteCollectionByIdGrupoProcessamento = new ClienteCollection();
					this._ClienteCollectionByIdGrupoProcessamento.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ClienteCollectionByIdGrupoProcessamento", this._ClienteCollectionByIdGrupoProcessamento);
				
					if(this.IdGrupoProcessamento != null)
					{
						this._ClienteCollectionByIdGrupoProcessamento.Query.Where(this._ClienteCollectionByIdGrupoProcessamento.Query.IdGrupoProcessamento == this.IdGrupoProcessamento);
						this._ClienteCollectionByIdGrupoProcessamento.Query.Load();

						// Auto-hookup Foreign Keys
						this._ClienteCollectionByIdGrupoProcessamento.fks.Add(ClienteMetadata.ColumnNames.IdGrupoProcessamento, this.IdGrupoProcessamento);
					}
				}

				return this._ClienteCollectionByIdGrupoProcessamento;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClienteCollectionByIdGrupoProcessamento != null) 
				{ 
					this.RemovePostSave("ClienteCollectionByIdGrupoProcessamento"); 
					this._ClienteCollectionByIdGrupoProcessamento = null;
					
				} 
			} 			
		}

		private ClienteCollection _ClienteCollectionByIdGrupoProcessamento;
		#endregion

		#region UpToClienteCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - FK_TabelaProcessamento_GrupoProcessamento
		/// </summary>

		[XmlIgnore]
		public ClienteCollection UpToClienteCollection
		{
			get
			{
				if(this._UpToClienteCollection == null)
				{
					this._UpToClienteCollection = new ClienteCollection();
					this._UpToClienteCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToClienteCollection", this._UpToClienteCollection);
					this._UpToClienteCollection.ManyToManyGrupoProcessamentoCollection(this.IdGrupoProcessamento);
				}

				return this._UpToClienteCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToClienteCollection != null) 
				{ 
					this.RemovePostSave("UpToClienteCollection"); 
					this._UpToClienteCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - FK_TabelaProcessamento_GrupoProcessamento
		/// </summary>
		public void AssociateClienteCollection(Cliente entity)
		{
			if (this._TabelaProcessamentoCollection == null)
			{
				this._TabelaProcessamentoCollection = new TabelaProcessamentoCollection();
				this._TabelaProcessamentoCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("TabelaProcessamentoCollection", this._TabelaProcessamentoCollection);
			}

			TabelaProcessamento obj = this._TabelaProcessamentoCollection.AddNew();
			obj.IdGrupoProcessamento = this.IdGrupoProcessamento;
			obj.IdCliente = entity.IdCliente;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - FK_TabelaProcessamento_GrupoProcessamento
		/// </summary>
		public void DissociateClienteCollection(Cliente entity)
		{
			if (this._TabelaProcessamentoCollection == null)
			{
				this._TabelaProcessamentoCollection = new TabelaProcessamentoCollection();
				this._TabelaProcessamentoCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("TabelaProcessamentoCollection", this._TabelaProcessamentoCollection);
			}

			TabelaProcessamento obj = this._TabelaProcessamentoCollection.AddNew();
			obj.IdGrupoProcessamento = this.IdGrupoProcessamento;
			obj.IdCliente = entity.IdCliente;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private ClienteCollection _UpToClienteCollection;
		private TabelaProcessamentoCollection _TabelaProcessamentoCollection;
		#endregion

				
		#region TabelaProcessamentoCollectionByIdGrupoProcessamento - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_TabelaProcessamento_GrupoProcessamento
		/// </summary>

		[XmlIgnore]
		public TabelaProcessamentoCollection TabelaProcessamentoCollectionByIdGrupoProcessamento
		{
			get
			{
				if(this._TabelaProcessamentoCollectionByIdGrupoProcessamento == null)
				{
					this._TabelaProcessamentoCollectionByIdGrupoProcessamento = new TabelaProcessamentoCollection();
					this._TabelaProcessamentoCollectionByIdGrupoProcessamento.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaProcessamentoCollectionByIdGrupoProcessamento", this._TabelaProcessamentoCollectionByIdGrupoProcessamento);
				
					if(this.IdGrupoProcessamento != null)
					{
						this._TabelaProcessamentoCollectionByIdGrupoProcessamento.Query.Where(this._TabelaProcessamentoCollectionByIdGrupoProcessamento.Query.IdGrupoProcessamento == this.IdGrupoProcessamento);
						this._TabelaProcessamentoCollectionByIdGrupoProcessamento.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaProcessamentoCollectionByIdGrupoProcessamento.fks.Add(TabelaProcessamentoMetadata.ColumnNames.IdGrupoProcessamento, this.IdGrupoProcessamento);
					}
				}

				return this._TabelaProcessamentoCollectionByIdGrupoProcessamento;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaProcessamentoCollectionByIdGrupoProcessamento != null) 
				{ 
					this.RemovePostSave("TabelaProcessamentoCollectionByIdGrupoProcessamento"); 
					this._TabelaProcessamentoCollectionByIdGrupoProcessamento = null;
					
				} 
			} 			
		}

		private TabelaProcessamentoCollection _TabelaProcessamentoCollectionByIdGrupoProcessamento;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "ClienteCollectionByIdGrupoProcessamento", typeof(ClienteCollection), new Cliente()));
			props.Add(new esPropertyDescriptor(this, "TabelaProcessamentoCollectionByIdGrupoProcessamento", typeof(TabelaProcessamentoCollection), new TabelaProcessamento()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._ClienteCollectionByIdGrupoProcessamento != null)
			{
				foreach(Cliente obj in this._ClienteCollectionByIdGrupoProcessamento)
				{
					if(obj.es.IsAdded)
					{
						obj.IdGrupoProcessamento = this.IdGrupoProcessamento;
					}
				}
			}
			if(this._TabelaProcessamentoCollection != null)
			{
				foreach(TabelaProcessamento obj in this._TabelaProcessamentoCollection)
				{
					if(obj.es.IsAdded)
					{
						obj.IdGrupoProcessamento = this.IdGrupoProcessamento;
					}
				}
			}
			if(this._TabelaProcessamentoCollectionByIdGrupoProcessamento != null)
			{
				foreach(TabelaProcessamento obj in this._TabelaProcessamentoCollectionByIdGrupoProcessamento)
				{
					if(obj.es.IsAdded)
					{
						obj.IdGrupoProcessamento = this.IdGrupoProcessamento;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}
	
	public partial class GrupoProcessamentoCollection : esGrupoProcessamentoCollection
	{
		#region ManyToManyClienteCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyClienteCollection(System.Int32? IdCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente", IdCliente);
	
			return base.Load( esQueryType.ManyToMany, 
				"GrupoProcessamento,TabelaProcessamento|IdGrupoProcessamento,IdGrupoProcessamento|IdCliente",	parms);
		}
		#endregion
	}




	[Serializable]
	abstract public class esGrupoProcessamentoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return GrupoProcessamentoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdGrupoProcessamento
		{
			get
			{
				return new esQueryItem(this, GrupoProcessamentoMetadata.ColumnNames.IdGrupoProcessamento, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, GrupoProcessamentoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("GrupoProcessamentoCollection")]
	public partial class GrupoProcessamentoCollection : esGrupoProcessamentoCollection, IEnumerable<GrupoProcessamento>
	{
		public GrupoProcessamentoCollection()
		{

		}
		
		public static implicit operator List<GrupoProcessamento>(GrupoProcessamentoCollection coll)
		{
			List<GrupoProcessamento> list = new List<GrupoProcessamento>();
			
			foreach (GrupoProcessamento emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  GrupoProcessamentoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrupoProcessamentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new GrupoProcessamento(row);
		}

		override protected esEntity CreateEntity()
		{
			return new GrupoProcessamento();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public GrupoProcessamentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupoProcessamentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(GrupoProcessamentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public GrupoProcessamento AddNew()
		{
			GrupoProcessamento entity = base.AddNewEntity() as GrupoProcessamento;
			
			return entity;
		}

		public GrupoProcessamento FindByPrimaryKey(System.Int16 idGrupoProcessamento)
		{
			return base.FindByPrimaryKey(idGrupoProcessamento) as GrupoProcessamento;
		}


		#region IEnumerable<GrupoProcessamento> Members

		IEnumerator<GrupoProcessamento> IEnumerable<GrupoProcessamento>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as GrupoProcessamento;
			}
		}

		#endregion
		
		private GrupoProcessamentoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'GrupoProcessamento' table
	/// </summary>

	[Serializable]
	public partial class GrupoProcessamento : esGrupoProcessamento
	{
		public GrupoProcessamento()
		{

		}
	
		public GrupoProcessamento(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GrupoProcessamentoMetadata.Meta();
			}
		}
		
		
		
		override protected esGrupoProcessamentoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrupoProcessamentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public GrupoProcessamentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupoProcessamentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(GrupoProcessamentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private GrupoProcessamentoQuery query;
	}



	[Serializable]
	public partial class GrupoProcessamentoQuery : esGrupoProcessamentoQuery
	{
		public GrupoProcessamentoQuery()
		{

		}		
		
		public GrupoProcessamentoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class GrupoProcessamentoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GrupoProcessamentoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GrupoProcessamentoMetadata.ColumnNames.IdGrupoProcessamento, 0, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = GrupoProcessamentoMetadata.PropertyNames.IdGrupoProcessamento;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupoProcessamentoMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = GrupoProcessamentoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 300;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public GrupoProcessamentoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdGrupoProcessamento = "IdGrupoProcessamento";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdGrupoProcessamento = "IdGrupoProcessamento";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GrupoProcessamentoMetadata))
			{
				if(GrupoProcessamentoMetadata.mapDelegates == null)
				{
					GrupoProcessamentoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GrupoProcessamentoMetadata.meta == null)
				{
					GrupoProcessamentoMetadata.meta = new GrupoProcessamentoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdGrupoProcessamento", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "GrupoProcessamento";
				meta.Destination = "GrupoProcessamento";
				
				meta.spInsert = "proc_GrupoProcessamentoInsert";				
				meta.spUpdate = "proc_GrupoProcessamentoUpdate";		
				meta.spDelete = "proc_GrupoProcessamentoDelete";
				meta.spLoadAll = "proc_GrupoProcessamentoLoadAll";
				meta.spLoadByPrimaryKey = "proc_GrupoProcessamentoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GrupoProcessamentoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
