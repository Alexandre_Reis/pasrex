/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 2/11/2015 10:56:13 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Investidor
{

	[Serializable]
	abstract public class esTipoContaCollection : esEntityCollection
	{
		public esTipoContaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TipoContaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTipoContaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTipoContaQuery);
		}
		#endregion
		
		virtual public TipoConta DetachEntity(TipoConta entity)
		{
			return base.DetachEntity(entity) as TipoConta;
		}
		
		virtual public TipoConta AttachEntity(TipoConta entity)
		{
			return base.AttachEntity(entity) as TipoConta;
		}
		
		virtual public void Combine(TipoContaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TipoConta this[int index]
		{
			get
			{
				return base[index] as TipoConta;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TipoConta);
		}
	}



	[Serializable]
	abstract public class esTipoConta : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTipoContaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTipoConta()
		{

		}

		public esTipoConta(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTipoConta)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTipoConta);
			else
				return LoadByPrimaryKeyStoredProcedure(idTipoConta);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTipoConta)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTipoContaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTipoConta == idTipoConta);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTipoConta)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTipoConta);
			else
				return LoadByPrimaryKeyStoredProcedure(idTipoConta);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTipoConta)
		{
			esTipoContaQuery query = this.GetDynamicQuery();
			query.Where(query.IdTipoConta == idTipoConta);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTipoConta)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTipoConta",idTipoConta);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTipoConta": this.str.IdTipoConta = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTipoConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTipoConta = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TipoConta.IdTipoConta
		/// </summary>
		virtual public System.Int32? IdTipoConta
		{
			get
			{
				return base.GetSystemInt32(TipoContaMetadata.ColumnNames.IdTipoConta);
			}
			
			set
			{
				base.SetSystemInt32(TipoContaMetadata.ColumnNames.IdTipoConta, value);
			}
		}
		
		/// <summary>
		/// Maps to TipoConta.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(TipoContaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(TipoContaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTipoConta entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTipoConta
			{
				get
				{
					System.Int32? data = entity.IdTipoConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTipoConta = null;
					else entity.IdTipoConta = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esTipoConta entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTipoContaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTipoConta can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TipoConta : esTipoConta
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTipoContaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TipoContaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTipoConta
		{
			get
			{
				return new esQueryItem(this, TipoContaMetadata.ColumnNames.IdTipoConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, TipoContaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TipoContaCollection")]
	public partial class TipoContaCollection : esTipoContaCollection, IEnumerable<TipoConta>
	{
		public TipoContaCollection()
		{

		}
		
		public static implicit operator List<TipoConta>(TipoContaCollection coll)
		{
			List<TipoConta> list = new List<TipoConta>();
			
			foreach (TipoConta emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TipoContaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TipoContaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TipoConta(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TipoConta();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TipoContaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TipoContaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TipoContaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TipoConta AddNew()
		{
			TipoConta entity = base.AddNewEntity() as TipoConta;
			
			return entity;
		}

		public TipoConta FindByPrimaryKey(System.Int32 idTipoConta)
		{
			return base.FindByPrimaryKey(idTipoConta) as TipoConta;
		}


		#region IEnumerable<TipoConta> Members

		IEnumerator<TipoConta> IEnumerable<TipoConta>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TipoConta;
			}
		}

		#endregion
		
		private TipoContaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TipoConta' table
	/// </summary>

	[Serializable]
	public partial class TipoConta : esTipoConta
	{
		public TipoConta()
		{

		}
	
		public TipoConta(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TipoContaMetadata.Meta();
			}
		}
		
		
		
		override protected esTipoContaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TipoContaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TipoContaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TipoContaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TipoContaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TipoContaQuery query;
	}



	[Serializable]
	public partial class TipoContaQuery : esTipoContaQuery
	{
		public TipoContaQuery()
		{

		}		
		
		public TipoContaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TipoContaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TipoContaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TipoContaMetadata.ColumnNames.IdTipoConta, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TipoContaMetadata.PropertyNames.IdTipoConta;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TipoContaMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TipoContaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TipoContaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTipoConta = "IdTipoConta";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTipoConta = "IdTipoConta";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TipoContaMetadata))
			{
				if(TipoContaMetadata.mapDelegates == null)
				{
					TipoContaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TipoContaMetadata.meta == null)
				{
					TipoContaMetadata.meta = new TipoContaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTipoConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "TipoConta";
				meta.Destination = "TipoConta";
				
				meta.spInsert = "proc_TipoContaInsert";				
				meta.spUpdate = "proc_TipoContaUpdate";		
				meta.spDelete = "proc_TipoContaDelete";
				meta.spLoadAll = "proc_TipoContaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TipoContaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TipoContaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
