/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 09/09/2015 12:14:56
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityEventosCollection : esEntityCollection
	{
		public esSuitabilityEventosCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityEventosCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityEventosQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityEventosQuery);
		}
		#endregion
		
		virtual public SuitabilityEventos DetachEntity(SuitabilityEventos entity)
		{
			return base.DetachEntity(entity) as SuitabilityEventos;
		}
		
		virtual public SuitabilityEventos AttachEntity(SuitabilityEventos entity)
		{
			return base.AttachEntity(entity) as SuitabilityEventos;
		}
		
		virtual public void Combine(SuitabilityEventosCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityEventos this[int index]
		{
			get
			{
				return base[index] as SuitabilityEventos;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityEventos);
		}
	}



	[Serializable]
	abstract public class esSuitabilityEventos : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityEventosQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityEventos()
		{

		}

		public esSuitabilityEventos(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idSuitabilityEventos)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idSuitabilityEventos);
			else
				return LoadByPrimaryKeyStoredProcedure(idSuitabilityEventos);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idSuitabilityEventos)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idSuitabilityEventos);
			else
				return LoadByPrimaryKeyStoredProcedure(idSuitabilityEventos);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idSuitabilityEventos)
		{
			esSuitabilityEventosQuery query = this.GetDynamicQuery();
			query.Where(query.IdSuitabilityEventos == idSuitabilityEventos);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idSuitabilityEventos)
		{
			esParameters parms = new esParameters();
			parms.Add("IdSuitabilityEventos",idSuitabilityEventos);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdSuitabilityEventos": this.str.IdSuitabilityEventos = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "IdMensagem": this.str.IdMensagem = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdSuitabilityEventos":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSuitabilityEventos = (System.Int32?)value;
							break;
						
						case "IdMensagem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMensagem = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityEventos.IdSuitabilityEventos
		/// </summary>
		virtual public System.Int32? IdSuitabilityEventos
		{
			get
			{
				return base.GetSystemInt32(SuitabilityEventosMetadata.ColumnNames.IdSuitabilityEventos);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityEventosMetadata.ColumnNames.IdSuitabilityEventos, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityEventos.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(SuitabilityEventosMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityEventosMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityEventos.IdMensagem
		/// </summary>
		virtual public System.Int32? IdMensagem
		{
			get
			{
				return base.GetSystemInt32(SuitabilityEventosMetadata.ColumnNames.IdMensagem);
			}
			
			set
			{
				if(base.SetSystemInt32(SuitabilityEventosMetadata.ColumnNames.IdMensagem, value))
				{
					this._UpToSuitabilityMensagensByIdMensagem = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected SuitabilityMensagens _UpToSuitabilityMensagensByIdMensagem;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityEventos entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdSuitabilityEventos
			{
				get
				{
					System.Int32? data = entity.IdSuitabilityEventos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSuitabilityEventos = null;
					else entity.IdSuitabilityEventos = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String IdMensagem
			{
				get
				{
					System.Int32? data = entity.IdMensagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMensagem = null;
					else entity.IdMensagem = Convert.ToInt32(value);
				}
			}
			

			private esSuitabilityEventos entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityEventosQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityEventos can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityEventos : esSuitabilityEventos
	{

				
		#region UpToSuitabilityMensagensByIdMensagem - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - SuitabilityEventos_SuitabilityMensagens_FK
		/// </summary>

		[XmlIgnore]
		public SuitabilityMensagens UpToSuitabilityMensagensByIdMensagem
		{
			get
			{
				if(this._UpToSuitabilityMensagensByIdMensagem == null
					&& IdMensagem != null					)
				{
					this._UpToSuitabilityMensagensByIdMensagem = new SuitabilityMensagens();
					this._UpToSuitabilityMensagensByIdMensagem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSuitabilityMensagensByIdMensagem", this._UpToSuitabilityMensagensByIdMensagem);
					this._UpToSuitabilityMensagensByIdMensagem.Query.Where(this._UpToSuitabilityMensagensByIdMensagem.Query.IdMensagem == this.IdMensagem);
					this._UpToSuitabilityMensagensByIdMensagem.Query.Load();
				}

				return this._UpToSuitabilityMensagensByIdMensagem;
			}
			
			set
			{
				this.RemovePreSave("UpToSuitabilityMensagensByIdMensagem");
				

				if(value == null)
				{
					this.IdMensagem = null;
					this._UpToSuitabilityMensagensByIdMensagem = null;
				}
				else
				{
					this.IdMensagem = value.IdMensagem;
					this._UpToSuitabilityMensagensByIdMensagem = value;
					this.SetPreSave("UpToSuitabilityMensagensByIdMensagem", this._UpToSuitabilityMensagensByIdMensagem);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToSuitabilityMensagensByIdMensagem != null)
			{
				this.IdMensagem = this._UpToSuitabilityMensagensByIdMensagem.IdMensagem;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityEventosQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityEventosMetadata.Meta();
			}
		}	
		

		public esQueryItem IdSuitabilityEventos
		{
			get
			{
				return new esQueryItem(this, SuitabilityEventosMetadata.ColumnNames.IdSuitabilityEventos, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, SuitabilityEventosMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdMensagem
		{
			get
			{
				return new esQueryItem(this, SuitabilityEventosMetadata.ColumnNames.IdMensagem, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityEventosCollection")]
	public partial class SuitabilityEventosCollection : esSuitabilityEventosCollection, IEnumerable<SuitabilityEventos>
	{
		public SuitabilityEventosCollection()
		{

		}
		
		public static implicit operator List<SuitabilityEventos>(SuitabilityEventosCollection coll)
		{
			List<SuitabilityEventos> list = new List<SuitabilityEventos>();
			
			foreach (SuitabilityEventos emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityEventosMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityEventosQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityEventos(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityEventos();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityEventosQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityEventosQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityEventosQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityEventos AddNew()
		{
			SuitabilityEventos entity = base.AddNewEntity() as SuitabilityEventos;
			
			return entity;
		}

		public SuitabilityEventos FindByPrimaryKey(System.Int32 idSuitabilityEventos)
		{
			return base.FindByPrimaryKey(idSuitabilityEventos) as SuitabilityEventos;
		}


		#region IEnumerable<SuitabilityEventos> Members

		IEnumerator<SuitabilityEventos> IEnumerable<SuitabilityEventos>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityEventos;
			}
		}

		#endregion
		
		private SuitabilityEventosQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityEventos' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityEventos : esSuitabilityEventos
	{
		public SuitabilityEventos()
		{

		}
	
		public SuitabilityEventos(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityEventosMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityEventosQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityEventosQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityEventosQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityEventosQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityEventosQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityEventosQuery query;
	}



	[Serializable]
	public partial class SuitabilityEventosQuery : esSuitabilityEventosQuery
	{
		public SuitabilityEventosQuery()
		{

		}		
		
		public SuitabilityEventosQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityEventosMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityEventosMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityEventosMetadata.ColumnNames.IdSuitabilityEventos, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityEventosMetadata.PropertyNames.IdSuitabilityEventos;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityEventosMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityEventosMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityEventosMetadata.ColumnNames.IdMensagem, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityEventosMetadata.PropertyNames.IdMensagem;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityEventosMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdSuitabilityEventos = "IdSuitabilityEventos";
			 public const string Descricao = "Descricao";
			 public const string IdMensagem = "IdMensagem";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdSuitabilityEventos = "IdSuitabilityEventos";
			 public const string Descricao = "Descricao";
			 public const string IdMensagem = "IdMensagem";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityEventosMetadata))
			{
				if(SuitabilityEventosMetadata.mapDelegates == null)
				{
					SuitabilityEventosMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityEventosMetadata.meta == null)
				{
					SuitabilityEventosMetadata.meta = new SuitabilityEventosMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdSuitabilityEventos", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdMensagem", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "SuitabilityEventos";
				meta.Destination = "SuitabilityEventos";
				
				meta.spInsert = "proc_SuitabilityEventosInsert";				
				meta.spUpdate = "proc_SuitabilityEventosUpdate";		
				meta.spDelete = "proc_SuitabilityEventosDelete";
				meta.spLoadAll = "proc_SuitabilityEventosLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityEventosLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityEventosMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
