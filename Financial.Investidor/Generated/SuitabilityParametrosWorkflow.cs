/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/09/2015 17:12:33
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityParametrosWorkflowCollection : esEntityCollection
	{
		public esSuitabilityParametrosWorkflowCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityParametrosWorkflowCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityParametrosWorkflowQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityParametrosWorkflowQuery);
		}
		#endregion
		
		virtual public SuitabilityParametrosWorkflow DetachEntity(SuitabilityParametrosWorkflow entity)
		{
			return base.DetachEntity(entity) as SuitabilityParametrosWorkflow;
		}
		
		virtual public SuitabilityParametrosWorkflow AttachEntity(SuitabilityParametrosWorkflow entity)
		{
			return base.AttachEntity(entity) as SuitabilityParametrosWorkflow;
		}
		
		virtual public void Combine(SuitabilityParametrosWorkflowCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityParametrosWorkflow this[int index]
		{
			get
			{
				return base[index] as SuitabilityParametrosWorkflow;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityParametrosWorkflow);
		}
	}



	[Serializable]
	abstract public class esSuitabilityParametrosWorkflow : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityParametrosWorkflowQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityParametrosWorkflow()
		{

		}

		public esSuitabilityParametrosWorkflow(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idParametrosWorkflow)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idParametrosWorkflow);
			else
				return LoadByPrimaryKeyStoredProcedure(idParametrosWorkflow);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idParametrosWorkflow)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idParametrosWorkflow);
			else
				return LoadByPrimaryKeyStoredProcedure(idParametrosWorkflow);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idParametrosWorkflow)
		{
			esSuitabilityParametrosWorkflowQuery query = this.GetDynamicQuery();
			query.Where(query.IdParametrosWorkflow == idParametrosWorkflow);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idParametrosWorkflow)
		{
			esParameters parms = new esParameters();
			parms.Add("IdParametrosWorkflow",idParametrosWorkflow);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdParametrosWorkflow": this.str.IdParametrosWorkflow = (string)value; break;							
						case "AtivaWorkFlow": this.str.AtivaWorkFlow = (string)value; break;							
						case "VerificaEnquadramento": this.str.VerificaEnquadramento = (string)value; break;							
						case "ResponsavelNome": this.str.ResponsavelNome = (string)value; break;							
						case "ResponsavelEmail": this.str.ResponsavelEmail = (string)value; break;							
						case "ValorPeriodo": this.str.ValorPeriodo = (string)value; break;							
						case "GrandezaPeriodo": this.str.GrandezaPeriodo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdParametrosWorkflow":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdParametrosWorkflow = (System.Int32?)value;
							break;
						
						case "VerificaEnquadramento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.VerificaEnquadramento = (System.Int32?)value;
							break;
						
						case "ValorPeriodo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.ValorPeriodo = (System.Int32?)value;
							break;
						
						case "GrandezaPeriodo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.GrandezaPeriodo = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflow.IdParametrosWorkflow
		/// </summary>
		virtual public System.Int32? IdParametrosWorkflow
		{
			get
			{
				return base.GetSystemInt32(SuitabilityParametrosWorkflowMetadata.ColumnNames.IdParametrosWorkflow);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityParametrosWorkflowMetadata.ColumnNames.IdParametrosWorkflow, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflow.AtivaWorkFlow
		/// </summary>
		virtual public System.String AtivaWorkFlow
		{
			get
			{
				return base.GetSystemString(SuitabilityParametrosWorkflowMetadata.ColumnNames.AtivaWorkFlow);
			}
			
			set
			{
				base.SetSystemString(SuitabilityParametrosWorkflowMetadata.ColumnNames.AtivaWorkFlow, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflow.VerificaEnquadramento
		/// </summary>
		virtual public System.Int32? VerificaEnquadramento
		{
			get
			{
				return base.GetSystemInt32(SuitabilityParametrosWorkflowMetadata.ColumnNames.VerificaEnquadramento);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityParametrosWorkflowMetadata.ColumnNames.VerificaEnquadramento, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflow.ResponsavelNome
		/// </summary>
		virtual public System.String ResponsavelNome
		{
			get
			{
				return base.GetSystemString(SuitabilityParametrosWorkflowMetadata.ColumnNames.ResponsavelNome);
			}
			
			set
			{
				base.SetSystemString(SuitabilityParametrosWorkflowMetadata.ColumnNames.ResponsavelNome, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflow.ResponsavelEmail
		/// </summary>
		virtual public System.String ResponsavelEmail
		{
			get
			{
				return base.GetSystemString(SuitabilityParametrosWorkflowMetadata.ColumnNames.ResponsavelEmail);
			}
			
			set
			{
				base.SetSystemString(SuitabilityParametrosWorkflowMetadata.ColumnNames.ResponsavelEmail, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflow.ValorPeriodo
		/// </summary>
		virtual public System.Int32? ValorPeriodo
		{
			get
			{
				return base.GetSystemInt32(SuitabilityParametrosWorkflowMetadata.ColumnNames.ValorPeriodo);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityParametrosWorkflowMetadata.ColumnNames.ValorPeriodo, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflow.GrandezaPeriodo
		/// </summary>
		virtual public System.Int32? GrandezaPeriodo
		{
			get
			{
				return base.GetSystemInt32(SuitabilityParametrosWorkflowMetadata.ColumnNames.GrandezaPeriodo);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityParametrosWorkflowMetadata.ColumnNames.GrandezaPeriodo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityParametrosWorkflow entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdParametrosWorkflow
			{
				get
				{
					System.Int32? data = entity.IdParametrosWorkflow;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdParametrosWorkflow = null;
					else entity.IdParametrosWorkflow = Convert.ToInt32(value);
				}
			}
				
			public System.String AtivaWorkFlow
			{
				get
				{
					System.String data = entity.AtivaWorkFlow;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AtivaWorkFlow = null;
					else entity.AtivaWorkFlow = Convert.ToString(value);
				}
			}
				
			public System.String VerificaEnquadramento
			{
				get
				{
					System.Int32? data = entity.VerificaEnquadramento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VerificaEnquadramento = null;
					else entity.VerificaEnquadramento = Convert.ToInt32(value);
				}
			}
				
			public System.String ResponsavelNome
			{
				get
				{
					System.String data = entity.ResponsavelNome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResponsavelNome = null;
					else entity.ResponsavelNome = Convert.ToString(value);
				}
			}
				
			public System.String ResponsavelEmail
			{
				get
				{
					System.String data = entity.ResponsavelEmail;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResponsavelEmail = null;
					else entity.ResponsavelEmail = Convert.ToString(value);
				}
			}
				
			public System.String ValorPeriodo
			{
				get
				{
					System.Int32? data = entity.ValorPeriodo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPeriodo = null;
					else entity.ValorPeriodo = Convert.ToInt32(value);
				}
			}
				
			public System.String GrandezaPeriodo
			{
				get
				{
					System.Int32? data = entity.GrandezaPeriodo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GrandezaPeriodo = null;
					else entity.GrandezaPeriodo = Convert.ToInt32(value);
				}
			}
			

			private esSuitabilityParametrosWorkflow entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityParametrosWorkflowQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityParametrosWorkflow can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityParametrosWorkflow : esSuitabilityParametrosWorkflow
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityParametrosWorkflowQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityParametrosWorkflowMetadata.Meta();
			}
		}	
		

		public esQueryItem IdParametrosWorkflow
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowMetadata.ColumnNames.IdParametrosWorkflow, esSystemType.Int32);
			}
		} 
		
		public esQueryItem AtivaWorkFlow
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowMetadata.ColumnNames.AtivaWorkFlow, esSystemType.String);
			}
		} 
		
		public esQueryItem VerificaEnquadramento
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowMetadata.ColumnNames.VerificaEnquadramento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ResponsavelNome
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowMetadata.ColumnNames.ResponsavelNome, esSystemType.String);
			}
		} 
		
		public esQueryItem ResponsavelEmail
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowMetadata.ColumnNames.ResponsavelEmail, esSystemType.String);
			}
		} 
		
		public esQueryItem ValorPeriodo
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowMetadata.ColumnNames.ValorPeriodo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem GrandezaPeriodo
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowMetadata.ColumnNames.GrandezaPeriodo, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityParametrosWorkflowCollection")]
	public partial class SuitabilityParametrosWorkflowCollection : esSuitabilityParametrosWorkflowCollection, IEnumerable<SuitabilityParametrosWorkflow>
	{
		public SuitabilityParametrosWorkflowCollection()
		{

		}
		
		public static implicit operator List<SuitabilityParametrosWorkflow>(SuitabilityParametrosWorkflowCollection coll)
		{
			List<SuitabilityParametrosWorkflow> list = new List<SuitabilityParametrosWorkflow>();
			
			foreach (SuitabilityParametrosWorkflow emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityParametrosWorkflowMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityParametrosWorkflowQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityParametrosWorkflow(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityParametrosWorkflow();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityParametrosWorkflowQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityParametrosWorkflowQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityParametrosWorkflowQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityParametrosWorkflow AddNew()
		{
			SuitabilityParametrosWorkflow entity = base.AddNewEntity() as SuitabilityParametrosWorkflow;
			
			return entity;
		}

		public SuitabilityParametrosWorkflow FindByPrimaryKey(System.Int32 idParametrosWorkflow)
		{
			return base.FindByPrimaryKey(idParametrosWorkflow) as SuitabilityParametrosWorkflow;
		}


		#region IEnumerable<SuitabilityParametrosWorkflow> Members

		IEnumerator<SuitabilityParametrosWorkflow> IEnumerable<SuitabilityParametrosWorkflow>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityParametrosWorkflow;
			}
		}

		#endregion
		
		private SuitabilityParametrosWorkflowQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityParametrosWorkflow' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityParametrosWorkflow : esSuitabilityParametrosWorkflow
	{
		public SuitabilityParametrosWorkflow()
		{

		}
	
		public SuitabilityParametrosWorkflow(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityParametrosWorkflowMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityParametrosWorkflowQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityParametrosWorkflowQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityParametrosWorkflowQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityParametrosWorkflowQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityParametrosWorkflowQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityParametrosWorkflowQuery query;
	}



	[Serializable]
	public partial class SuitabilityParametrosWorkflowQuery : esSuitabilityParametrosWorkflowQuery
	{
		public SuitabilityParametrosWorkflowQuery()
		{

		}		
		
		public SuitabilityParametrosWorkflowQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityParametrosWorkflowMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityParametrosWorkflowMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityParametrosWorkflowMetadata.ColumnNames.IdParametrosWorkflow, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityParametrosWorkflowMetadata.PropertyNames.IdParametrosWorkflow;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((1))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametrosWorkflowMetadata.ColumnNames.AtivaWorkFlow, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityParametrosWorkflowMetadata.PropertyNames.AtivaWorkFlow;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametrosWorkflowMetadata.ColumnNames.VerificaEnquadramento, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityParametrosWorkflowMetadata.PropertyNames.VerificaEnquadramento;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametrosWorkflowMetadata.ColumnNames.ResponsavelNome, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityParametrosWorkflowMetadata.PropertyNames.ResponsavelNome;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametrosWorkflowMetadata.ColumnNames.ResponsavelEmail, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityParametrosWorkflowMetadata.PropertyNames.ResponsavelEmail;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametrosWorkflowMetadata.ColumnNames.ValorPeriodo, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityParametrosWorkflowMetadata.PropertyNames.ValorPeriodo;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametrosWorkflowMetadata.ColumnNames.GrandezaPeriodo, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityParametrosWorkflowMetadata.PropertyNames.GrandezaPeriodo;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityParametrosWorkflowMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdParametrosWorkflow = "IdParametrosWorkflow";
			 public const string AtivaWorkFlow = "AtivaWorkFlow";
			 public const string VerificaEnquadramento = "VerificaEnquadramento";
			 public const string ResponsavelNome = "ResponsavelNome";
			 public const string ResponsavelEmail = "ResponsavelEmail";
			 public const string ValorPeriodo = "ValorPeriodo";
			 public const string GrandezaPeriodo = "GrandezaPeriodo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdParametrosWorkflow = "IdParametrosWorkflow";
			 public const string AtivaWorkFlow = "AtivaWorkFlow";
			 public const string VerificaEnquadramento = "VerificaEnquadramento";
			 public const string ResponsavelNome = "ResponsavelNome";
			 public const string ResponsavelEmail = "ResponsavelEmail";
			 public const string ValorPeriodo = "ValorPeriodo";
			 public const string GrandezaPeriodo = "GrandezaPeriodo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityParametrosWorkflowMetadata))
			{
				if(SuitabilityParametrosWorkflowMetadata.mapDelegates == null)
				{
					SuitabilityParametrosWorkflowMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityParametrosWorkflowMetadata.meta == null)
				{
					SuitabilityParametrosWorkflowMetadata.meta = new SuitabilityParametrosWorkflowMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdParametrosWorkflow", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AtivaWorkFlow", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("VerificaEnquadramento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ResponsavelNome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ResponsavelEmail", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ValorPeriodo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("GrandezaPeriodo", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "SuitabilityParametrosWorkflow";
				meta.Destination = "SuitabilityParametrosWorkflow";
				
				meta.spInsert = "proc_SuitabilityParametrosWorkflowInsert";				
				meta.spUpdate = "proc_SuitabilityParametrosWorkflowUpdate";		
				meta.spDelete = "proc_SuitabilityParametrosWorkflowDelete";
				meta.spLoadAll = "proc_SuitabilityParametrosWorkflowLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityParametrosWorkflowLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityParametrosWorkflowMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
