/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 13/10/2014 18:08:13
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esLogProcessamentoCollection : esEntityCollection
	{
		public esLogProcessamentoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "LogProcessamentoCollection";
		}

		#region Query Logic
		protected void InitQuery(esLogProcessamentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esLogProcessamentoQuery);
		}
		#endregion
		
		virtual public LogProcessamento DetachEntity(LogProcessamento entity)
		{
			return base.DetachEntity(entity) as LogProcessamento;
		}
		
		virtual public LogProcessamento AttachEntity(LogProcessamento entity)
		{
			return base.AttachEntity(entity) as LogProcessamento;
		}
		
		virtual public void Combine(LogProcessamentoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public LogProcessamento this[int index]
		{
			get
			{
				return base[index] as LogProcessamento;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(LogProcessamento);
		}
	}



	[Serializable]
	abstract public class esLogProcessamento : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esLogProcessamentoQuery GetDynamicQuery()
		{
			return null;
		}

		public esLogProcessamento()
		{

		}

		public esLogProcessamento(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int32 idCliente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idCliente);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int32 idCliente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idCliente);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int32 idCliente)
		{
			esLogProcessamentoQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.IdCliente == idCliente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int32 idCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("IdCliente",idCliente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Login": this.str.Login = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;							
						case "DataInicio": this.str.DataInicio = (string)value; break;							
						case "DataFim": this.str.DataFim = (string)value; break;							
						case "DataInicialPeriodo": this.str.DataInicialPeriodo = (string)value; break;							
						case "DataFinalPeriodo": this.str.DataFinalPeriodo = (string)value; break;							
						case "Erro": this.str.Erro = (string)value; break;							
						case "Mensagem": this.str.Mensagem = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Tipo = (System.Int32?)value;
							break;
						
						case "DataInicio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicio = (System.DateTime?)value;
							break;
						
						case "DataFim":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFim = (System.DateTime?)value;
							break;
						
						case "DataInicialPeriodo":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicialPeriodo = (System.DateTime?)value;
							break;
						
						case "DataFinalPeriodo":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFinalPeriodo = (System.DateTime?)value;
							break;
						
						case "Erro":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Erro = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to LogProcessamento.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(LogProcessamentoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(LogProcessamentoMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LogProcessamento.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(LogProcessamentoMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(LogProcessamentoMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to LogProcessamento.Login
		/// </summary>
		virtual public System.String Login
		{
			get
			{
				return base.GetSystemString(LogProcessamentoMetadata.ColumnNames.Login);
			}
			
			set
			{
				base.SetSystemString(LogProcessamentoMetadata.ColumnNames.Login, value);
			}
		}
		
		/// <summary>
		/// Maps to LogProcessamento.Tipo
		/// </summary>
		virtual public System.Int32? Tipo
		{
			get
			{
				return base.GetSystemInt32(LogProcessamentoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemInt32(LogProcessamentoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		/// <summary>
		/// Maps to LogProcessamento.DataInicio
		/// </summary>
		virtual public System.DateTime? DataInicio
		{
			get
			{
				return base.GetSystemDateTime(LogProcessamentoMetadata.ColumnNames.DataInicio);
			}
			
			set
			{
				base.SetSystemDateTime(LogProcessamentoMetadata.ColumnNames.DataInicio, value);
			}
		}
		
		/// <summary>
		/// Maps to LogProcessamento.DataFim
		/// </summary>
		virtual public System.DateTime? DataFim
		{
			get
			{
				return base.GetSystemDateTime(LogProcessamentoMetadata.ColumnNames.DataFim);
			}
			
			set
			{
				base.SetSystemDateTime(LogProcessamentoMetadata.ColumnNames.DataFim, value);
			}
		}
		
		/// <summary>
		/// Maps to LogProcessamento.DataInicialPeriodo
		/// </summary>
		virtual public System.DateTime? DataInicialPeriodo
		{
			get
			{
				return base.GetSystemDateTime(LogProcessamentoMetadata.ColumnNames.DataInicialPeriodo);
			}
			
			set
			{
				base.SetSystemDateTime(LogProcessamentoMetadata.ColumnNames.DataInicialPeriodo, value);
			}
		}
		
		/// <summary>
		/// Maps to LogProcessamento.DataFinalPeriodo
		/// </summary>
		virtual public System.DateTime? DataFinalPeriodo
		{
			get
			{
				return base.GetSystemDateTime(LogProcessamentoMetadata.ColumnNames.DataFinalPeriodo);
			}
			
			set
			{
				base.SetSystemDateTime(LogProcessamentoMetadata.ColumnNames.DataFinalPeriodo, value);
			}
		}
		
		/// <summary>
		/// Maps to LogProcessamento.Erro
		/// </summary>
		virtual public System.Int32? Erro
		{
			get
			{
				return base.GetSystemInt32(LogProcessamentoMetadata.ColumnNames.Erro);
			}
			
			set
			{
				base.SetSystemInt32(LogProcessamentoMetadata.ColumnNames.Erro, value);
			}
		}
		
		/// <summary>
		/// Maps to LogProcessamento.Mensagem
		/// </summary>
		virtual public System.String Mensagem
		{
			get
			{
				return base.GetSystemString(LogProcessamentoMetadata.ColumnNames.Mensagem);
			}
			
			set
			{
				base.SetSystemString(LogProcessamentoMetadata.ColumnNames.Mensagem, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esLogProcessamento entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Login
			{
				get
				{
					System.String data = entity.Login;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Login = null;
					else entity.Login = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Int32? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToInt32(value);
				}
			}
				
			public System.String DataInicio
			{
				get
				{
					System.DateTime? data = entity.DataInicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicio = null;
					else entity.DataInicio = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataFim
			{
				get
				{
					System.DateTime? data = entity.DataFim;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFim = null;
					else entity.DataFim = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataInicialPeriodo
			{
				get
				{
					System.DateTime? data = entity.DataInicialPeriodo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicialPeriodo = null;
					else entity.DataInicialPeriodo = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataFinalPeriodo
			{
				get
				{
					System.DateTime? data = entity.DataFinalPeriodo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFinalPeriodo = null;
					else entity.DataFinalPeriodo = Convert.ToDateTime(value);
				}
			}
				
			public System.String Erro
			{
				get
				{
					System.Int32? data = entity.Erro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Erro = null;
					else entity.Erro = Convert.ToInt32(value);
				}
			}
				
			public System.String Mensagem
			{
				get
				{
					System.String data = entity.Mensagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Mensagem = null;
					else entity.Mensagem = Convert.ToString(value);
				}
			}
			

			private esLogProcessamento entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esLogProcessamentoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esLogProcessamento can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class LogProcessamento : esLogProcessamento
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_LogProcessamento_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esLogProcessamentoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return LogProcessamentoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Login
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoMetadata.ColumnNames.Login, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoMetadata.ColumnNames.Tipo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataInicio
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoMetadata.ColumnNames.DataInicio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataFim
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoMetadata.ColumnNames.DataFim, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataInicialPeriodo
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoMetadata.ColumnNames.DataInicialPeriodo, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataFinalPeriodo
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoMetadata.ColumnNames.DataFinalPeriodo, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Erro
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoMetadata.ColumnNames.Erro, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Mensagem
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoMetadata.ColumnNames.Mensagem, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("LogProcessamentoCollection")]
	public partial class LogProcessamentoCollection : esLogProcessamentoCollection, IEnumerable<LogProcessamento>
	{
		public LogProcessamentoCollection()
		{

		}
		
		public static implicit operator List<LogProcessamento>(LogProcessamentoCollection coll)
		{
			List<LogProcessamento> list = new List<LogProcessamento>();
			
			foreach (LogProcessamento emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  LogProcessamentoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LogProcessamentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new LogProcessamento(row);
		}

		override protected esEntity CreateEntity()
		{
			return new LogProcessamento();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public LogProcessamentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LogProcessamentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(LogProcessamentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public LogProcessamento AddNew()
		{
			LogProcessamento entity = base.AddNewEntity() as LogProcessamento;
			
			return entity;
		}

		public LogProcessamento FindByPrimaryKey(System.DateTime data, System.Int32 idCliente)
		{
			return base.FindByPrimaryKey(data, idCliente) as LogProcessamento;
		}


		#region IEnumerable<LogProcessamento> Members

		IEnumerator<LogProcessamento> IEnumerable<LogProcessamento>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as LogProcessamento;
			}
		}

		#endregion
		
		private LogProcessamentoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'LogProcessamento' table
	/// </summary>

	[Serializable]
	public partial class LogProcessamento : esLogProcessamento
	{
		public LogProcessamento()
		{

		}
	
		public LogProcessamento(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return LogProcessamentoMetadata.Meta();
			}
		}
		
		
		
		override protected esLogProcessamentoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LogProcessamentoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public LogProcessamentoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LogProcessamentoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(LogProcessamentoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private LogProcessamentoQuery query;
	}



	[Serializable]
	public partial class LogProcessamentoQuery : esLogProcessamentoQuery
	{
		public LogProcessamentoQuery()
		{

		}		
		
		public LogProcessamentoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class LogProcessamentoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected LogProcessamentoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(LogProcessamentoMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LogProcessamentoMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LogProcessamentoMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LogProcessamentoMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LogProcessamentoMetadata.ColumnNames.Login, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = LogProcessamentoMetadata.PropertyNames.Login;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LogProcessamentoMetadata.ColumnNames.Tipo, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LogProcessamentoMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LogProcessamentoMetadata.ColumnNames.DataInicio, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LogProcessamentoMetadata.PropertyNames.DataInicio;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LogProcessamentoMetadata.ColumnNames.DataFim, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LogProcessamentoMetadata.PropertyNames.DataFim;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LogProcessamentoMetadata.ColumnNames.DataInicialPeriodo, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LogProcessamentoMetadata.PropertyNames.DataInicialPeriodo;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LogProcessamentoMetadata.ColumnNames.DataFinalPeriodo, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LogProcessamentoMetadata.PropertyNames.DataFinalPeriodo;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LogProcessamentoMetadata.ColumnNames.Erro, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LogProcessamentoMetadata.PropertyNames.Erro;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LogProcessamentoMetadata.ColumnNames.Mensagem, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = LogProcessamentoMetadata.PropertyNames.Mensagem;
			c.CharacterMaxLength = 8000;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public LogProcessamentoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string Data = "Data";
			 public const string Login = "Login";
			 public const string Tipo = "Tipo";
			 public const string DataInicio = "DataInicio";
			 public const string DataFim = "DataFim";
			 public const string DataInicialPeriodo = "DataInicialPeriodo";
			 public const string DataFinalPeriodo = "DataFinalPeriodo";
			 public const string Erro = "Erro";
			 public const string Mensagem = "Mensagem";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string Data = "Data";
			 public const string Login = "Login";
			 public const string Tipo = "Tipo";
			 public const string DataInicio = "DataInicio";
			 public const string DataFim = "DataFim";
			 public const string DataInicialPeriodo = "DataInicialPeriodo";
			 public const string DataFinalPeriodo = "DataFinalPeriodo";
			 public const string Erro = "Erro";
			 public const string Mensagem = "Mensagem";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(LogProcessamentoMetadata))
			{
				if(LogProcessamentoMetadata.mapDelegates == null)
				{
					LogProcessamentoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (LogProcessamentoMetadata.meta == null)
				{
					LogProcessamentoMetadata.meta = new LogProcessamentoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Login", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataInicio", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataFim", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataInicialPeriodo", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataFinalPeriodo", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Erro", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Mensagem", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "LogProcessamento";
				meta.Destination = "LogProcessamento";
				
				meta.spInsert = "proc_LogProcessamentoInsert";				
				meta.spUpdate = "proc_LogProcessamentoUpdate";		
				meta.spDelete = "proc_LogProcessamentoDelete";
				meta.spLoadAll = "proc_LogProcessamentoLoadAll";
				meta.spLoadByPrimaryKey = "proc_LogProcessamentoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private LogProcessamentoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
