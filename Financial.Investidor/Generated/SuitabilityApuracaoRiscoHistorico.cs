/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/09/2015 15:02:03
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityApuracaoRiscoHistoricoCollection : esEntityCollection
	{
		public esSuitabilityApuracaoRiscoHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityApuracaoRiscoHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityApuracaoRiscoHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityApuracaoRiscoHistoricoQuery);
		}
		#endregion
		
		virtual public SuitabilityApuracaoRiscoHistorico DetachEntity(SuitabilityApuracaoRiscoHistorico entity)
		{
			return base.DetachEntity(entity) as SuitabilityApuracaoRiscoHistorico;
		}
		
		virtual public SuitabilityApuracaoRiscoHistorico AttachEntity(SuitabilityApuracaoRiscoHistorico entity)
		{
			return base.AttachEntity(entity) as SuitabilityApuracaoRiscoHistorico;
		}
		
		virtual public void Combine(SuitabilityApuracaoRiscoHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityApuracaoRiscoHistorico this[int index]
		{
			get
			{
				return base[index] as SuitabilityApuracaoRiscoHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityApuracaoRiscoHistorico);
		}
	}



	[Serializable]
	abstract public class esSuitabilityApuracaoRiscoHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityApuracaoRiscoHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityApuracaoRiscoHistorico()
		{

		}

		public esSuitabilityApuracaoRiscoHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idApuracaoRisco)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idApuracaoRisco);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idApuracaoRisco);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idApuracaoRisco)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idApuracaoRisco);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idApuracaoRisco);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idApuracaoRisco)
		{
			esSuitabilityApuracaoRiscoHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdApuracaoRisco == idApuracaoRisco);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idApuracaoRisco)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdApuracaoRisco",idApuracaoRisco);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdApuracaoRisco": this.str.IdApuracaoRisco = (string)value; break;							
						case "IdValidacao": this.str.IdValidacao = (string)value; break;							
						case "Validacao": this.str.Validacao = (string)value; break;							
						case "IdPerfilCarteira": this.str.IdPerfilCarteira = (string)value; break;							
						case "PerfilCarteira": this.str.PerfilCarteira = (string)value; break;							
						case "PerfilRiscoFundo": this.str.PerfilRiscoFundo = (string)value; break;							
						case "Criterio": this.str.Criterio = (string)value; break;							
						case "Percentual": this.str.Percentual = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdApuracaoRisco":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdApuracaoRisco = (System.Int32?)value;
							break;
						
						case "IdValidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdValidacao = (System.Int32?)value;
							break;
						
						case "IdPerfilCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilCarteira = (System.Int32?)value;
							break;
						
						case "Percentual":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Percentual = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRiscoHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRiscoHistorico.IdApuracaoRisco
		/// </summary>
		virtual public System.Int32? IdApuracaoRisco
		{
			get
			{
				return base.GetSystemInt32(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.IdApuracaoRisco);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.IdApuracaoRisco, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRiscoHistorico.IdValidacao
		/// </summary>
		virtual public System.Int32? IdValidacao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.IdValidacao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.IdValidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRiscoHistorico.Validacao
		/// </summary>
		virtual public System.String Validacao
		{
			get
			{
				return base.GetSystemString(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Validacao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Validacao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRiscoHistorico.IdPerfilCarteira
		/// </summary>
		virtual public System.Int32? IdPerfilCarteira
		{
			get
			{
				return base.GetSystemInt32(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.IdPerfilCarteira);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.IdPerfilCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRiscoHistorico.PerfilCarteira
		/// </summary>
		virtual public System.String PerfilCarteira
		{
			get
			{
				return base.GetSystemString(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.PerfilCarteira);
			}
			
			set
			{
				base.SetSystemString(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.PerfilCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRiscoHistorico.PerfilRiscoFundo
		/// </summary>
		virtual public System.String PerfilRiscoFundo
		{
			get
			{
				return base.GetSystemString(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.PerfilRiscoFundo);
			}
			
			set
			{
				base.SetSystemString(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.PerfilRiscoFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRiscoHistorico.Criterio
		/// </summary>
		virtual public System.String Criterio
		{
			get
			{
				return base.GetSystemString(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Criterio);
			}
			
			set
			{
				base.SetSystemString(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Criterio, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRiscoHistorico.Percentual
		/// </summary>
		virtual public System.Int32? Percentual
		{
			get
			{
				return base.GetSystemInt32(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Percentual);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Percentual, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityApuracaoRiscoHistorico.Tipo
		/// </summary>
		virtual public System.String Tipo
		{
			get
			{
				return base.GetSystemString(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemString(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityApuracaoRiscoHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdApuracaoRisco
			{
				get
				{
					System.Int32? data = entity.IdApuracaoRisco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdApuracaoRisco = null;
					else entity.IdApuracaoRisco = Convert.ToInt32(value);
				}
			}
				
			public System.String IdValidacao
			{
				get
				{
					System.Int32? data = entity.IdValidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdValidacao = null;
					else entity.IdValidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String Validacao
			{
				get
				{
					System.String data = entity.Validacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Validacao = null;
					else entity.Validacao = Convert.ToString(value);
				}
			}
				
			public System.String IdPerfilCarteira
			{
				get
				{
					System.Int32? data = entity.IdPerfilCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilCarteira = null;
					else entity.IdPerfilCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String PerfilCarteira
			{
				get
				{
					System.String data = entity.PerfilCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerfilCarteira = null;
					else entity.PerfilCarteira = Convert.ToString(value);
				}
			}
				
			public System.String PerfilRiscoFundo
			{
				get
				{
					System.String data = entity.PerfilRiscoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerfilRiscoFundo = null;
					else entity.PerfilRiscoFundo = Convert.ToString(value);
				}
			}
				
			public System.String Criterio
			{
				get
				{
					System.String data = entity.Criterio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Criterio = null;
					else entity.Criterio = Convert.ToString(value);
				}
			}
				
			public System.String Percentual
			{
				get
				{
					System.Int32? data = entity.Percentual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Percentual = null;
					else entity.Percentual = Convert.ToInt32(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.String data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToString(value);
				}
			}
			

			private esSuitabilityApuracaoRiscoHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityApuracaoRiscoHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityApuracaoRiscoHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityApuracaoRiscoHistorico : esSuitabilityApuracaoRiscoHistorico
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityApuracaoRiscoHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityApuracaoRiscoHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdApuracaoRisco
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.IdApuracaoRisco, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdValidacao
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.IdValidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Validacao
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Validacao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdPerfilCarteira
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.IdPerfilCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PerfilCarteira
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.PerfilCarteira, esSystemType.String);
			}
		} 
		
		public esQueryItem PerfilRiscoFundo
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.PerfilRiscoFundo, esSystemType.String);
			}
		} 
		
		public esQueryItem Criterio
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Criterio, esSystemType.String);
			}
		} 
		
		public esQueryItem Percentual
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Percentual, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Tipo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityApuracaoRiscoHistoricoCollection")]
	public partial class SuitabilityApuracaoRiscoHistoricoCollection : esSuitabilityApuracaoRiscoHistoricoCollection, IEnumerable<SuitabilityApuracaoRiscoHistorico>
	{
		public SuitabilityApuracaoRiscoHistoricoCollection()
		{

		}
		
		public static implicit operator List<SuitabilityApuracaoRiscoHistorico>(SuitabilityApuracaoRiscoHistoricoCollection coll)
		{
			List<SuitabilityApuracaoRiscoHistorico> list = new List<SuitabilityApuracaoRiscoHistorico>();
			
			foreach (SuitabilityApuracaoRiscoHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityApuracaoRiscoHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityApuracaoRiscoHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityApuracaoRiscoHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityApuracaoRiscoHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityApuracaoRiscoHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityApuracaoRiscoHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityApuracaoRiscoHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityApuracaoRiscoHistorico AddNew()
		{
			SuitabilityApuracaoRiscoHistorico entity = base.AddNewEntity() as SuitabilityApuracaoRiscoHistorico;
			
			return entity;
		}

		public SuitabilityApuracaoRiscoHistorico FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idApuracaoRisco)
		{
			return base.FindByPrimaryKey(dataHistorico, idApuracaoRisco) as SuitabilityApuracaoRiscoHistorico;
		}


		#region IEnumerable<SuitabilityApuracaoRiscoHistorico> Members

		IEnumerator<SuitabilityApuracaoRiscoHistorico> IEnumerable<SuitabilityApuracaoRiscoHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityApuracaoRiscoHistorico;
			}
		}

		#endregion
		
		private SuitabilityApuracaoRiscoHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityApuracaoRiscoHistorico' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityApuracaoRiscoHistorico : esSuitabilityApuracaoRiscoHistorico
	{
		public SuitabilityApuracaoRiscoHistorico()
		{

		}
	
		public SuitabilityApuracaoRiscoHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityApuracaoRiscoHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityApuracaoRiscoHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityApuracaoRiscoHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityApuracaoRiscoHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityApuracaoRiscoHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityApuracaoRiscoHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityApuracaoRiscoHistoricoQuery query;
	}



	[Serializable]
	public partial class SuitabilityApuracaoRiscoHistoricoQuery : esSuitabilityApuracaoRiscoHistoricoQuery
	{
		public SuitabilityApuracaoRiscoHistoricoQuery()
		{

		}		
		
		public SuitabilityApuracaoRiscoHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityApuracaoRiscoHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityApuracaoRiscoHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SuitabilityApuracaoRiscoHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.IdApuracaoRisco, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityApuracaoRiscoHistoricoMetadata.PropertyNames.IdApuracaoRisco;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.IdValidacao, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityApuracaoRiscoHistoricoMetadata.PropertyNames.IdValidacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Validacao, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityApuracaoRiscoHistoricoMetadata.PropertyNames.Validacao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.IdPerfilCarteira, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityApuracaoRiscoHistoricoMetadata.PropertyNames.IdPerfilCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.PerfilCarteira, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityApuracaoRiscoHistoricoMetadata.PropertyNames.PerfilCarteira;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.PerfilRiscoFundo, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityApuracaoRiscoHistoricoMetadata.PropertyNames.PerfilRiscoFundo;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Criterio, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityApuracaoRiscoHistoricoMetadata.PropertyNames.Criterio;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Percentual, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityApuracaoRiscoHistoricoMetadata.PropertyNames.Percentual;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityApuracaoRiscoHistoricoMetadata.ColumnNames.Tipo, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityApuracaoRiscoHistoricoMetadata.PropertyNames.Tipo;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityApuracaoRiscoHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdApuracaoRisco = "IdApuracaoRisco";
			 public const string IdValidacao = "IdValidacao";
			 public const string Validacao = "Validacao";
			 public const string IdPerfilCarteira = "IdPerfilCarteira";
			 public const string PerfilCarteira = "PerfilCarteira";
			 public const string PerfilRiscoFundo = "PerfilRiscoFundo";
			 public const string Criterio = "Criterio";
			 public const string Percentual = "Percentual";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdApuracaoRisco = "IdApuracaoRisco";
			 public const string IdValidacao = "IdValidacao";
			 public const string Validacao = "Validacao";
			 public const string IdPerfilCarteira = "IdPerfilCarteira";
			 public const string PerfilCarteira = "PerfilCarteira";
			 public const string PerfilRiscoFundo = "PerfilRiscoFundo";
			 public const string Criterio = "Criterio";
			 public const string Percentual = "Percentual";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityApuracaoRiscoHistoricoMetadata))
			{
				if(SuitabilityApuracaoRiscoHistoricoMetadata.mapDelegates == null)
				{
					SuitabilityApuracaoRiscoHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityApuracaoRiscoHistoricoMetadata.meta == null)
				{
					SuitabilityApuracaoRiscoHistoricoMetadata.meta = new SuitabilityApuracaoRiscoHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdApuracaoRisco", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdValidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Validacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdPerfilCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PerfilCarteira", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PerfilRiscoFundo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Criterio", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Percentual", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Tipo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "SuitabilityApuracaoRiscoHistorico";
				meta.Destination = "SuitabilityApuracaoRiscoHistorico";
				
				meta.spInsert = "proc_SuitabilityApuracaoRiscoHistoricoInsert";				
				meta.spUpdate = "proc_SuitabilityApuracaoRiscoHistoricoUpdate";		
				meta.spDelete = "proc_SuitabilityApuracaoRiscoHistoricoDelete";
				meta.spLoadAll = "proc_SuitabilityApuracaoRiscoHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityApuracaoRiscoHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityApuracaoRiscoHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
