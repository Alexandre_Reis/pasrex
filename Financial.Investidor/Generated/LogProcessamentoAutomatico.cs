/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/10/2014 18:27:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esLogProcessamentoAutomaticoCollection : esEntityCollection
	{
		public esLogProcessamentoAutomaticoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "LogProcessamentoAutomaticoCollection";
		}

		#region Query Logic
		protected void InitQuery(esLogProcessamentoAutomaticoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esLogProcessamentoAutomaticoQuery);
		}
		#endregion
		
		virtual public LogProcessamentoAutomatico DetachEntity(LogProcessamentoAutomatico entity)
		{
			return base.DetachEntity(entity) as LogProcessamentoAutomatico;
		}
		
		virtual public LogProcessamentoAutomatico AttachEntity(LogProcessamentoAutomatico entity)
		{
			return base.AttachEntity(entity) as LogProcessamentoAutomatico;
		}
		
		virtual public void Combine(LogProcessamentoAutomaticoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public LogProcessamentoAutomatico this[int index]
		{
			get
			{
				return base[index] as LogProcessamentoAutomatico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(LogProcessamentoAutomatico);
		}
	}



	[Serializable]
	abstract public class esLogProcessamentoAutomatico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esLogProcessamentoAutomaticoQuery GetDynamicQuery()
		{
			return null;
		}

		public esLogProcessamentoAutomatico()
		{

		}

		public esLogProcessamentoAutomatico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idLog)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLog);
			else
				return LoadByPrimaryKeyStoredProcedure(idLog);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLog)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLog);
			else
				return LoadByPrimaryKeyStoredProcedure(idLog);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idLog)
		{
			esLogProcessamentoAutomaticoQuery query = this.GetDynamicQuery();
			query.Where(query.IdLog == idLog);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLog)
		{
			esParameters parms = new esParameters();
			parms.Add("idLog",idLog);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLog": this.str.IdLog = (string)value; break;							
						case "IdAgendaProcesso": this.str.IdAgendaProcesso = (string)value; break;							
						case "DataInicio": this.str.DataInicio = (string)value; break;							
						case "DataFim": this.str.DataFim = (string)value; break;							
						case "Erro": this.str.Erro = (string)value; break;							
						case "Mensagem": this.str.Mensagem = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLog":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLog = (System.Int32?)value;
							break;
						
						case "IdAgendaProcesso":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgendaProcesso = (System.Int32?)value;
							break;
						
						case "DataInicio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicio = (System.DateTime?)value;
							break;
						
						case "DataFim":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFim = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to LogProcessamentoAutomatico.idLog
		/// </summary>
		virtual public System.Int32? IdLog
		{
			get
			{
				return base.GetSystemInt32(LogProcessamentoAutomaticoMetadata.ColumnNames.IdLog);
			}
			
			set
			{
				base.SetSystemInt32(LogProcessamentoAutomaticoMetadata.ColumnNames.IdLog, value);
			}
		}
		
		/// <summary>
		/// Maps to LogProcessamentoAutomatico.idAgendaProcesso
		/// </summary>
		virtual public System.Int32? IdAgendaProcesso
		{
			get
			{
				return base.GetSystemInt32(LogProcessamentoAutomaticoMetadata.ColumnNames.IdAgendaProcesso);
			}
			
			set
			{
				if(base.SetSystemInt32(LogProcessamentoAutomaticoMetadata.ColumnNames.IdAgendaProcesso, value))
				{
					this._UpToAgendaProcessosByIdAgendaProcesso = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LogProcessamentoAutomatico.DataInicio
		/// </summary>
		virtual public System.DateTime? DataInicio
		{
			get
			{
				return base.GetSystemDateTime(LogProcessamentoAutomaticoMetadata.ColumnNames.DataInicio);
			}
			
			set
			{
				base.SetSystemDateTime(LogProcessamentoAutomaticoMetadata.ColumnNames.DataInicio, value);
			}
		}
		
		/// <summary>
		/// Maps to LogProcessamentoAutomatico.DataFim
		/// </summary>
		virtual public System.DateTime? DataFim
		{
			get
			{
				return base.GetSystemDateTime(LogProcessamentoAutomaticoMetadata.ColumnNames.DataFim);
			}
			
			set
			{
				base.SetSystemDateTime(LogProcessamentoAutomaticoMetadata.ColumnNames.DataFim, value);
			}
		}
		
		/// <summary>
		/// Maps to LogProcessamentoAutomatico.Erro
		/// </summary>
		virtual public System.String Erro
		{
			get
			{
				return base.GetSystemString(LogProcessamentoAutomaticoMetadata.ColumnNames.Erro);
			}
			
			set
			{
				base.SetSystemString(LogProcessamentoAutomaticoMetadata.ColumnNames.Erro, value);
			}
		}
		
		/// <summary>
		/// Maps to LogProcessamentoAutomatico.Mensagem
		/// </summary>
		virtual public System.String Mensagem
		{
			get
			{
				return base.GetSystemString(LogProcessamentoAutomaticoMetadata.ColumnNames.Mensagem);
			}
			
			set
			{
				base.SetSystemString(LogProcessamentoAutomaticoMetadata.ColumnNames.Mensagem, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgendaProcessos _UpToAgendaProcessosByIdAgendaProcesso;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esLogProcessamentoAutomatico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLog
			{
				get
				{
					System.Int32? data = entity.IdLog;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLog = null;
					else entity.IdLog = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgendaProcesso
			{
				get
				{
					System.Int32? data = entity.IdAgendaProcesso;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgendaProcesso = null;
					else entity.IdAgendaProcesso = Convert.ToInt32(value);
				}
			}
				
			public System.String DataInicio
			{
				get
				{
					System.DateTime? data = entity.DataInicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicio = null;
					else entity.DataInicio = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataFim
			{
				get
				{
					System.DateTime? data = entity.DataFim;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFim = null;
					else entity.DataFim = Convert.ToDateTime(value);
				}
			}
				
			public System.String Erro
			{
				get
				{
					System.String data = entity.Erro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Erro = null;
					else entity.Erro = Convert.ToString(value);
				}
			}
				
			public System.String Mensagem
			{
				get
				{
					System.String data = entity.Mensagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Mensagem = null;
					else entity.Mensagem = Convert.ToString(value);
				}
			}
			

			private esLogProcessamentoAutomatico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esLogProcessamentoAutomaticoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esLogProcessamentoAutomatico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class LogProcessamentoAutomatico : esLogProcessamentoAutomatico
	{

				
		#region UpToAgendaProcessosByIdAgendaProcesso - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgendaProcessos_LogProcessamentoAutomatico_FK1
		/// </summary>

		[XmlIgnore]
		public AgendaProcessos UpToAgendaProcessosByIdAgendaProcesso
		{
			get
			{
				if(this._UpToAgendaProcessosByIdAgendaProcesso == null
					&& IdAgendaProcesso != null					)
				{
					this._UpToAgendaProcessosByIdAgendaProcesso = new AgendaProcessos();
					this._UpToAgendaProcessosByIdAgendaProcesso.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgendaProcessosByIdAgendaProcesso", this._UpToAgendaProcessosByIdAgendaProcesso);
					this._UpToAgendaProcessosByIdAgendaProcesso.Query.Where(this._UpToAgendaProcessosByIdAgendaProcesso.Query.IdAgendaProcesso == this.IdAgendaProcesso);
					this._UpToAgendaProcessosByIdAgendaProcesso.Query.Load();
				}

				return this._UpToAgendaProcessosByIdAgendaProcesso;
			}
			
			set
			{
				this.RemovePreSave("UpToAgendaProcessosByIdAgendaProcesso");
				

				if(value == null)
				{
					this.IdAgendaProcesso = null;
					this._UpToAgendaProcessosByIdAgendaProcesso = null;
				}
				else
				{
					this.IdAgendaProcesso = value.IdAgendaProcesso;
					this._UpToAgendaProcessosByIdAgendaProcesso = value;
					this.SetPreSave("UpToAgendaProcessosByIdAgendaProcesso", this._UpToAgendaProcessosByIdAgendaProcesso);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgendaProcessosByIdAgendaProcesso != null)
			{
				this.IdAgendaProcesso = this._UpToAgendaProcessosByIdAgendaProcesso.IdAgendaProcesso;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esLogProcessamentoAutomaticoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return LogProcessamentoAutomaticoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLog
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoAutomaticoMetadata.ColumnNames.IdLog, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgendaProcesso
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoAutomaticoMetadata.ColumnNames.IdAgendaProcesso, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataInicio
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoAutomaticoMetadata.ColumnNames.DataInicio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataFim
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoAutomaticoMetadata.ColumnNames.DataFim, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Erro
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoAutomaticoMetadata.ColumnNames.Erro, esSystemType.String);
			}
		} 
		
		public esQueryItem Mensagem
		{
			get
			{
				return new esQueryItem(this, LogProcessamentoAutomaticoMetadata.ColumnNames.Mensagem, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("LogProcessamentoAutomaticoCollection")]
	public partial class LogProcessamentoAutomaticoCollection : esLogProcessamentoAutomaticoCollection, IEnumerable<LogProcessamentoAutomatico>
	{
		public LogProcessamentoAutomaticoCollection()
		{

		}
		
		public static implicit operator List<LogProcessamentoAutomatico>(LogProcessamentoAutomaticoCollection coll)
		{
			List<LogProcessamentoAutomatico> list = new List<LogProcessamentoAutomatico>();
			
			foreach (LogProcessamentoAutomatico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  LogProcessamentoAutomaticoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LogProcessamentoAutomaticoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new LogProcessamentoAutomatico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new LogProcessamentoAutomatico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public LogProcessamentoAutomaticoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LogProcessamentoAutomaticoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(LogProcessamentoAutomaticoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public LogProcessamentoAutomatico AddNew()
		{
			LogProcessamentoAutomatico entity = base.AddNewEntity() as LogProcessamentoAutomatico;
			
			return entity;
		}

		public LogProcessamentoAutomatico FindByPrimaryKey(System.Int32 idLog)
		{
			return base.FindByPrimaryKey(idLog) as LogProcessamentoAutomatico;
		}


		#region IEnumerable<LogProcessamentoAutomatico> Members

		IEnumerator<LogProcessamentoAutomatico> IEnumerable<LogProcessamentoAutomatico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as LogProcessamentoAutomatico;
			}
		}

		#endregion
		
		private LogProcessamentoAutomaticoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'LogProcessamentoAutomatico' table
	/// </summary>

	[Serializable]
	public partial class LogProcessamentoAutomatico : esLogProcessamentoAutomatico
	{
		public LogProcessamentoAutomatico()
		{

		}
	
		public LogProcessamentoAutomatico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return LogProcessamentoAutomaticoMetadata.Meta();
			}
		}
		
		
		
		override protected esLogProcessamentoAutomaticoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LogProcessamentoAutomaticoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public LogProcessamentoAutomaticoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LogProcessamentoAutomaticoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(LogProcessamentoAutomaticoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private LogProcessamentoAutomaticoQuery query;
	}



	[Serializable]
	public partial class LogProcessamentoAutomaticoQuery : esLogProcessamentoAutomaticoQuery
	{
		public LogProcessamentoAutomaticoQuery()
		{

		}		
		
		public LogProcessamentoAutomaticoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class LogProcessamentoAutomaticoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected LogProcessamentoAutomaticoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(LogProcessamentoAutomaticoMetadata.ColumnNames.IdLog, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LogProcessamentoAutomaticoMetadata.PropertyNames.IdLog;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LogProcessamentoAutomaticoMetadata.ColumnNames.IdAgendaProcesso, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LogProcessamentoAutomaticoMetadata.PropertyNames.IdAgendaProcesso;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LogProcessamentoAutomaticoMetadata.ColumnNames.DataInicio, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LogProcessamentoAutomaticoMetadata.PropertyNames.DataInicio;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LogProcessamentoAutomaticoMetadata.ColumnNames.DataFim, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LogProcessamentoAutomaticoMetadata.PropertyNames.DataFim;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LogProcessamentoAutomaticoMetadata.ColumnNames.Erro, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = LogProcessamentoAutomaticoMetadata.PropertyNames.Erro;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LogProcessamentoAutomaticoMetadata.ColumnNames.Mensagem, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = LogProcessamentoAutomaticoMetadata.PropertyNames.Mensagem;
			c.CharacterMaxLength = 8000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public LogProcessamentoAutomaticoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLog = "idLog";
			 public const string IdAgendaProcesso = "idAgendaProcesso";
			 public const string DataInicio = "DataInicio";
			 public const string DataFim = "DataFim";
			 public const string Erro = "Erro";
			 public const string Mensagem = "Mensagem";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLog = "IdLog";
			 public const string IdAgendaProcesso = "IdAgendaProcesso";
			 public const string DataInicio = "DataInicio";
			 public const string DataFim = "DataFim";
			 public const string Erro = "Erro";
			 public const string Mensagem = "Mensagem";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(LogProcessamentoAutomaticoMetadata))
			{
				if(LogProcessamentoAutomaticoMetadata.mapDelegates == null)
				{
					LogProcessamentoAutomaticoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (LogProcessamentoAutomaticoMetadata.meta == null)
				{
					LogProcessamentoAutomaticoMetadata.meta = new LogProcessamentoAutomaticoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("idLog", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("idAgendaProcesso", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataInicio", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataFim", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Erro", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Mensagem", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "LogProcessamentoAutomatico";
				meta.Destination = "LogProcessamentoAutomatico";
				
				meta.spInsert = "proc_LogProcessamentoAutomaticoInsert";				
				meta.spUpdate = "proc_LogProcessamentoAutomaticoUpdate";		
				meta.spDelete = "proc_LogProcessamentoAutomaticoDelete";
				meta.spLoadAll = "proc_LogProcessamentoAutomaticoLoadAll";
				meta.spLoadByPrimaryKey = "proc_LogProcessamentoAutomaticoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private LogProcessamentoAutomaticoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
