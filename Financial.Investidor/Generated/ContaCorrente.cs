/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 07/08/2015 17:18:56
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	
using Financial.ContaCorrente;
using Financial.Fundo;
using Financial.CRM;
using Financial.InvestidorCotista;
	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Investidor
{

	[Serializable]
	abstract public class esContaCorrenteCollection : esEntityCollection
	{
		public esContaCorrenteCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ContaCorrenteCollection";
		}

		#region Query Logic
		protected void InitQuery(esContaCorrenteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esContaCorrenteQuery);
		}
		#endregion
		
		virtual public ContaCorrente DetachEntity(ContaCorrente entity)
		{
			return base.DetachEntity(entity) as ContaCorrente;
		}
		
		virtual public ContaCorrente AttachEntity(ContaCorrente entity)
		{
			return base.AttachEntity(entity) as ContaCorrente;
		}
		
		virtual public void Combine(ContaCorrenteCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ContaCorrente this[int index]
		{
			get
			{
				return base[index] as ContaCorrente;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ContaCorrente);
		}
	}



	[Serializable]
	abstract public class esContaCorrente : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esContaCorrenteQuery GetDynamicQuery()
		{
			return null;
		}

		public esContaCorrente()
		{

		}

		public esContaCorrente(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idConta)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idConta);
			else
				return LoadByPrimaryKeyStoredProcedure(idConta);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idConta)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esContaCorrenteQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdConta == idConta);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idConta)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idConta);
			else
				return LoadByPrimaryKeyStoredProcedure(idConta);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idConta)
		{
			esContaCorrenteQuery query = this.GetDynamicQuery();
			query.Where(query.IdConta == idConta);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idConta)
		{
			esParameters parms = new esParameters();
			parms.Add("IdConta",idConta);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdConta": this.str.IdConta = (string)value; break;							
						case "IdBanco": this.str.IdBanco = (string)value; break;							
						case "IdAgencia": this.str.IdAgencia = (string)value; break;							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;							
						case "Numero": this.str.Numero = (string)value; break;							
						case "ContaDefault": this.str.ContaDefault = (string)value; break;							
						case "IdMoeda": this.str.IdMoeda = (string)value; break;							
						case "DigitoConta": this.str.DigitoConta = (string)value; break;							
						case "IdLocal": this.str.IdLocal = (string)value; break;							
						case "DescricaoCodigo": this.str.DescricaoCodigo = (string)value; break;							
						case "IdTipoConta": this.str.IdTipoConta = (string)value; break;							
						case "IdGrupo": this.str.IdGrupo = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "ContaDefaultCliente": this.str.ContaDefaultCliente = (string)value; break;							
						case "ContaDefaultCotista": this.str.ContaDefaultCotista = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConta = (System.Int32?)value;
							break;
						
						case "IdBanco":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdBanco = (System.Int32?)value;
							break;
						
						case "IdAgencia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgencia = (System.Int32?)value;
							break;
						
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
						
						case "IdMoeda":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMoeda = (System.Int32?)value;
							break;
						
						case "IdLocal":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdLocal = (System.Int16?)value;
							break;
						
						case "IdTipoConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTipoConta = (System.Int32?)value;
							break;
						
						case "IdGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupo = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ContaCorrente.IdConta
		/// </summary>
		virtual public System.Int32? IdConta
		{
			get
			{
				return base.GetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdConta);
			}
			
			set
			{
				base.SetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdConta, value);
			}
		}
		
		/// <summary>
		/// Maps to ContaCorrente.IdBanco
		/// </summary>
		virtual public System.Int32? IdBanco
		{
			get
			{
				return base.GetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdBanco);
			}
			
			set
			{
				if(base.SetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdBanco, value))
				{
					this._UpToBancoByIdBanco = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ContaCorrente.IdAgencia
		/// </summary>
		virtual public System.Int32? IdAgencia
		{
			get
			{
				return base.GetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdAgencia);
			}
			
			set
			{
				if(base.SetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdAgencia, value))
				{
					this._UpToAgenciaByIdAgencia = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ContaCorrente.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				if(base.SetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdPessoa, value))
				{
					this._UpToPessoaByIdPessoa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ContaCorrente.Numero
		/// </summary>
		virtual public System.String Numero
		{
			get
			{
				return base.GetSystemString(ContaCorrenteMetadata.ColumnNames.Numero);
			}
			
			set
			{
				base.SetSystemString(ContaCorrenteMetadata.ColumnNames.Numero, value);
			}
		}
		
		/// <summary>
		/// Maps to ContaCorrente.ContaDefault
		/// </summary>
		virtual public System.String ContaDefault
		{
			get
			{
				return base.GetSystemString(ContaCorrenteMetadata.ColumnNames.ContaDefault);
			}
			
			set
			{
				base.SetSystemString(ContaCorrenteMetadata.ColumnNames.ContaDefault, value);
			}
		}
		
		/// <summary>
		/// Maps to ContaCorrente.IdMoeda
		/// </summary>
		virtual public System.Int32? IdMoeda
		{
			get
			{
				return base.GetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdMoeda);
			}
			
			set
			{
				base.SetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdMoeda, value);
			}
		}
		
		/// <summary>
		/// Maps to ContaCorrente.DigitoConta
		/// </summary>
		virtual public System.String DigitoConta
		{
			get
			{
				return base.GetSystemString(ContaCorrenteMetadata.ColumnNames.DigitoConta);
			}
			
			set
			{
				base.SetSystemString(ContaCorrenteMetadata.ColumnNames.DigitoConta, value);
			}
		}
		
		/// <summary>
		/// Maps to ContaCorrente.IdLocal
		/// </summary>
		virtual public System.Int16? IdLocal
		{
			get
			{
				return base.GetSystemInt16(ContaCorrenteMetadata.ColumnNames.IdLocal);
			}
			
			set
			{
				base.SetSystemInt16(ContaCorrenteMetadata.ColumnNames.IdLocal, value);
			}
		}
		
		/// <summary>
		/// Maps to ContaCorrente.DescricaoCodigo
		/// </summary>
		virtual public System.String DescricaoCodigo
		{
			get
			{
				return base.GetSystemString(ContaCorrenteMetadata.ColumnNames.DescricaoCodigo);
			}
			
			set
			{
				base.SetSystemString(ContaCorrenteMetadata.ColumnNames.DescricaoCodigo, value);
			}
		}
		
		/// <summary>
		/// Maps to ContaCorrente.IdTipoConta
		/// </summary>
		virtual public System.Int32? IdTipoConta
		{
			get
			{
				return base.GetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdTipoConta);
			}
			
			set
			{
				base.SetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdTipoConta, value);
			}
		}
		
		/// <summary>
		/// Maps to ContaCorrente.IdGrupo
		/// </summary>
		virtual public System.Int32? IdGrupo
		{
			get
			{
				return base.GetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdGrupo);
			}
			
			set
			{
				base.SetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdGrupo, value);
			}
		}
		
		/// <summary>
		/// Maps to ContaCorrente.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ContaCorrente.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				if(base.SetSystemInt32(ContaCorrenteMetadata.ColumnNames.IdCotista, value))
				{
					this._UpToCotistaByIdCotista = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ContaCorrente.ContaDefaultCliente
		/// </summary>
		virtual public System.String ContaDefaultCliente
		{
			get
			{
				return base.GetSystemString(ContaCorrenteMetadata.ColumnNames.ContaDefaultCliente);
			}
			
			set
			{
				base.SetSystemString(ContaCorrenteMetadata.ColumnNames.ContaDefaultCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to ContaCorrente.ContaDefaultCotista
		/// </summary>
		virtual public System.String ContaDefaultCotista
		{
			get
			{
				return base.GetSystemString(ContaCorrenteMetadata.ColumnNames.ContaDefaultCotista);
			}
			
			set
			{
				base.SetSystemString(ContaCorrenteMetadata.ColumnNames.ContaDefaultCotista, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Agencia _UpToAgenciaByIdAgencia;
		[CLSCompliant(false)]
		internal protected Banco _UpToBancoByIdBanco;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Cotista _UpToCotistaByIdCotista;
		[CLSCompliant(false)]
		internal protected Pessoa _UpToPessoaByIdPessoa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esContaCorrente entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdConta
			{
				get
				{
					System.Int32? data = entity.IdConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConta = null;
					else entity.IdConta = Convert.ToInt32(value);
				}
			}
				
			public System.String IdBanco
			{
				get
				{
					System.Int32? data = entity.IdBanco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdBanco = null;
					else entity.IdBanco = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgencia
			{
				get
				{
					System.Int32? data = entity.IdAgencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgencia = null;
					else entity.IdAgencia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
				
			public System.String Numero
			{
				get
				{
					System.String data = entity.Numero;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Numero = null;
					else entity.Numero = Convert.ToString(value);
				}
			}
				
			public System.String ContaDefault
			{
				get
				{
					System.String data = entity.ContaDefault;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContaDefault = null;
					else entity.ContaDefault = Convert.ToString(value);
				}
			}
				
			public System.String IdMoeda
			{
				get
				{
					System.Int32? data = entity.IdMoeda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoeda = null;
					else entity.IdMoeda = Convert.ToInt32(value);
				}
			}
				
			public System.String DigitoConta
			{
				get
				{
					System.String data = entity.DigitoConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DigitoConta = null;
					else entity.DigitoConta = Convert.ToString(value);
				}
			}
				
			public System.String IdLocal
			{
				get
				{
					System.Int16? data = entity.IdLocal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocal = null;
					else entity.IdLocal = Convert.ToInt16(value);
				}
			}
				
			public System.String DescricaoCodigo
			{
				get
				{
					System.String data = entity.DescricaoCodigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescricaoCodigo = null;
					else entity.DescricaoCodigo = Convert.ToString(value);
				}
			}
				
			public System.String IdTipoConta
			{
				get
				{
					System.Int32? data = entity.IdTipoConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTipoConta = null;
					else entity.IdTipoConta = Convert.ToInt32(value);
				}
			}
				
			public System.String IdGrupo
			{
				get
				{
					System.Int32? data = entity.IdGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupo = null;
					else entity.IdGrupo = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String ContaDefaultCliente
			{
				get
				{
					System.String data = entity.ContaDefaultCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContaDefaultCliente = null;
					else entity.ContaDefaultCliente = Convert.ToString(value);
				}
			}
				
			public System.String ContaDefaultCotista
			{
				get
				{
					System.String data = entity.ContaDefaultCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContaDefaultCotista = null;
					else entity.ContaDefaultCotista = Convert.ToString(value);
				}
			}
			

			private esContaCorrente entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esContaCorrenteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esContaCorrente can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ContaCorrente : esContaCorrente
	{

				
		#region SaldoCaixaCollectionByIdConta - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ContaCorrente_SaldoCaixa_FK1
		/// </summary>

		[XmlIgnore]
		public SaldoCaixaCollection SaldoCaixaCollectionByIdConta
		{
			get
			{
				if(this._SaldoCaixaCollectionByIdConta == null)
				{
					this._SaldoCaixaCollectionByIdConta = new SaldoCaixaCollection();
					this._SaldoCaixaCollectionByIdConta.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("SaldoCaixaCollectionByIdConta", this._SaldoCaixaCollectionByIdConta);
				
					if(this.IdConta != null)
					{
						this._SaldoCaixaCollectionByIdConta.Query.Where(this._SaldoCaixaCollectionByIdConta.Query.IdConta == this.IdConta);
						this._SaldoCaixaCollectionByIdConta.Query.Load();

						// Auto-hookup Foreign Keys
						this._SaldoCaixaCollectionByIdConta.fks.Add(SaldoCaixaMetadata.ColumnNames.IdConta, this.IdConta);
					}
				}

				return this._SaldoCaixaCollectionByIdConta;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._SaldoCaixaCollectionByIdConta != null) 
				{ 
					this.RemovePostSave("SaldoCaixaCollectionByIdConta"); 
					this._SaldoCaixaCollectionByIdConta = null;
					
				} 
			} 			
		}

		private SaldoCaixaCollection _SaldoCaixaCollectionByIdConta;
		#endregion

				
		#region UpToAgenciaByIdAgencia - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Agencia_ContaCorrente_FK1
		/// </summary>

		[XmlIgnore]
		public Agencia UpToAgenciaByIdAgencia
		{
			get
			{
				if(this._UpToAgenciaByIdAgencia == null
					&& IdAgencia != null					)
				{
					this._UpToAgenciaByIdAgencia = new Agencia();
					this._UpToAgenciaByIdAgencia.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenciaByIdAgencia", this._UpToAgenciaByIdAgencia);
					this._UpToAgenciaByIdAgencia.Query.Where(this._UpToAgenciaByIdAgencia.Query.IdAgencia == this.IdAgencia);
					this._UpToAgenciaByIdAgencia.Query.Load();
				}

				return this._UpToAgenciaByIdAgencia;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenciaByIdAgencia");
				

				if(value == null)
				{
					this.IdAgencia = null;
					this._UpToAgenciaByIdAgencia = null;
				}
				else
				{
					this.IdAgencia = value.IdAgencia;
					this._UpToAgenciaByIdAgencia = value;
					this.SetPreSave("UpToAgenciaByIdAgencia", this._UpToAgenciaByIdAgencia);
				}
				
			}
		}
		#endregion
		

				
		#region UpToBancoByIdBanco - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Banco_ContaCorrente_FK1
		/// </summary>

		[XmlIgnore]
		public Banco UpToBancoByIdBanco
		{
			get
			{
				if(this._UpToBancoByIdBanco == null
					&& IdBanco != null					)
				{
					this._UpToBancoByIdBanco = new Banco();
					this._UpToBancoByIdBanco.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToBancoByIdBanco", this._UpToBancoByIdBanco);
					this._UpToBancoByIdBanco.Query.Where(this._UpToBancoByIdBanco.Query.IdBanco == this.IdBanco);
					this._UpToBancoByIdBanco.Query.Load();
				}

				return this._UpToBancoByIdBanco;
			}
			
			set
			{
				this.RemovePreSave("UpToBancoByIdBanco");
				

				if(value == null)
				{
					this.IdBanco = null;
					this._UpToBancoByIdBanco = null;
				}
				else
				{
					this.IdBanco = value.IdBanco;
					this._UpToBancoByIdBanco = value;
					this.SetPreSave("UpToBancoByIdBanco", this._UpToBancoByIdBanco);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Pessoa_Cliente_FK
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCotistaByIdCotista - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Pessoa_Cotista_FK
		/// </summary>

		[XmlIgnore]
		public Cotista UpToCotistaByIdCotista
		{
			get
			{
				if(this._UpToCotistaByIdCotista == null
					&& IdCotista != null					)
				{
					this._UpToCotistaByIdCotista = new Cotista();
					this._UpToCotistaByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
					this._UpToCotistaByIdCotista.Query.Where(this._UpToCotistaByIdCotista.Query.IdCotista == this.IdCotista);
					this._UpToCotistaByIdCotista.Query.Load();
				}

				return this._UpToCotistaByIdCotista;
			}
			
			set
			{
				this.RemovePreSave("UpToCotistaByIdCotista");
				

				if(value == null)
				{
					this.IdCotista = null;
					this._UpToCotistaByIdCotista = null;
				}
				else
				{
					this.IdCotista = value.IdCotista;
					this._UpToCotistaByIdCotista = value;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPessoaByIdPessoa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Pessoa_ContaCorrente_FK1
		/// </summary>

		[XmlIgnore]
		public Pessoa UpToPessoaByIdPessoa
		{
			get
			{
				if(this._UpToPessoaByIdPessoa == null
					&& IdPessoa != null					)
				{
					this._UpToPessoaByIdPessoa = new Pessoa();
					this._UpToPessoaByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
					this._UpToPessoaByIdPessoa.Query.Where(this._UpToPessoaByIdPessoa.Query.IdPessoa == this.IdPessoa);
					this._UpToPessoaByIdPessoa.Query.Load();
				}

				return this._UpToPessoaByIdPessoa;
			}
			
			set
			{
				this.RemovePreSave("UpToPessoaByIdPessoa");
				

				if(value == null)
				{
					this.IdPessoa = null;
					this._UpToPessoaByIdPessoa = null;
				}
				else
				{
					this.IdPessoa = value.IdPessoa;
					this._UpToPessoaByIdPessoa = value;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "SaldoCaixaCollectionByIdConta", typeof(SaldoCaixaCollection), new SaldoCaixa()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenciaByIdAgencia != null)
			{
				this.IdAgencia = this._UpToAgenciaByIdAgencia.IdAgencia;
			}
			if(!this.es.IsDeleted && this._UpToBancoByIdBanco != null)
			{
				this.IdBanco = this._UpToBancoByIdBanco.IdBanco;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._SaldoCaixaCollectionByIdConta != null)
			{
				foreach(SaldoCaixa obj in this._SaldoCaixaCollectionByIdConta)
				{
					if(obj.es.IsAdded)
					{
						obj.IdConta = this.IdConta;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esContaCorrenteQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ContaCorrenteMetadata.Meta();
			}
		}	
		

		public esQueryItem IdConta
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.IdConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdBanco
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.IdBanco, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgencia
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.IdAgencia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Numero
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.Numero, esSystemType.String);
			}
		} 
		
		public esQueryItem ContaDefault
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.ContaDefault, esSystemType.String);
			}
		} 
		
		public esQueryItem IdMoeda
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.IdMoeda, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DigitoConta
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.DigitoConta, esSystemType.String);
			}
		} 
		
		public esQueryItem IdLocal
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.IdLocal, esSystemType.Int16);
			}
		} 
		
		public esQueryItem DescricaoCodigo
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.DescricaoCodigo, esSystemType.String);
			}
		} 
		
		public esQueryItem IdTipoConta
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.IdTipoConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdGrupo
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.IdGrupo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ContaDefaultCliente
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.ContaDefaultCliente, esSystemType.String);
			}
		} 
		
		public esQueryItem ContaDefaultCotista
		{
			get
			{
				return new esQueryItem(this, ContaCorrenteMetadata.ColumnNames.ContaDefaultCotista, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ContaCorrenteCollection")]
	public partial class ContaCorrenteCollection : esContaCorrenteCollection, IEnumerable<ContaCorrente>
	{
		public ContaCorrenteCollection()
		{

		}
		
		public static implicit operator List<ContaCorrente>(ContaCorrenteCollection coll)
		{
			List<ContaCorrente> list = new List<ContaCorrente>();
			
			foreach (ContaCorrente emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ContaCorrenteMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ContaCorrenteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ContaCorrente(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ContaCorrente();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ContaCorrenteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ContaCorrenteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ContaCorrenteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ContaCorrente AddNew()
		{
			ContaCorrente entity = base.AddNewEntity() as ContaCorrente;
			
			return entity;
		}

		public ContaCorrente FindByPrimaryKey(System.Int32 idConta)
		{
			return base.FindByPrimaryKey(idConta) as ContaCorrente;
		}


		#region IEnumerable<ContaCorrente> Members

		IEnumerator<ContaCorrente> IEnumerable<ContaCorrente>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ContaCorrente;
			}
		}

		#endregion
		
		private ContaCorrenteQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ContaCorrente' table
	/// </summary>

	[Serializable]
	public partial class ContaCorrente : esContaCorrente
	{
		public ContaCorrente()
		{

		}
	
		public ContaCorrente(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ContaCorrenteMetadata.Meta();
			}
		}
		
		
		
		override protected esContaCorrenteQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ContaCorrenteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ContaCorrenteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ContaCorrenteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ContaCorrenteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ContaCorrenteQuery query;
	}



	[Serializable]
	public partial class ContaCorrenteQuery : esContaCorrenteQuery
	{
		public ContaCorrenteQuery()
		{

		}		
		
		public ContaCorrenteQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ContaCorrenteMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ContaCorrenteMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.IdConta, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.IdConta;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.IdBanco, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.IdBanco;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.IdAgencia, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.IdAgencia;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.IdPessoa, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.IdPessoa;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.Numero, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.Numero;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.ContaDefault, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.ContaDefault;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.IdMoeda, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.IdMoeda;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.DigitoConta, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.DigitoConta;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.IdLocal, 8, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.IdLocal;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.DescricaoCodigo, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.DescricaoCodigo;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.IdTipoConta, 10, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.IdTipoConta;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.IdGrupo, 11, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.IdGrupo;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.IdCliente, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.IdCotista, 13, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.IdCotista;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.ContaDefaultCliente, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.ContaDefaultCliente;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContaCorrenteMetadata.ColumnNames.ContaDefaultCotista, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = ContaCorrenteMetadata.PropertyNames.ContaDefaultCotista;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ContaCorrenteMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdConta = "IdConta";
			 public const string IdBanco = "IdBanco";
			 public const string IdAgencia = "IdAgencia";
			 public const string IdPessoa = "IdPessoa";
			 public const string Numero = "Numero";
			 public const string ContaDefault = "ContaDefault";
			 public const string IdMoeda = "IdMoeda";
			 public const string DigitoConta = "DigitoConta";
			 public const string IdLocal = "IdLocal";
			 public const string DescricaoCodigo = "DescricaoCodigo";
			 public const string IdTipoConta = "IdTipoConta";
			 public const string IdGrupo = "IdGrupo";
			 public const string IdCliente = "IdCliente";
			 public const string IdCotista = "IdCotista";
			 public const string ContaDefaultCliente = "ContaDefaultCliente";
			 public const string ContaDefaultCotista = "ContaDefaultCotista";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdConta = "IdConta";
			 public const string IdBanco = "IdBanco";
			 public const string IdAgencia = "IdAgencia";
			 public const string IdPessoa = "IdPessoa";
			 public const string Numero = "Numero";
			 public const string ContaDefault = "ContaDefault";
			 public const string IdMoeda = "IdMoeda";
			 public const string DigitoConta = "DigitoConta";
			 public const string IdLocal = "IdLocal";
			 public const string DescricaoCodigo = "DescricaoCodigo";
			 public const string IdTipoConta = "IdTipoConta";
			 public const string IdGrupo = "IdGrupo";
			 public const string IdCliente = "IdCliente";
			 public const string IdCotista = "IdCotista";
			 public const string ContaDefaultCliente = "ContaDefaultCliente";
			 public const string ContaDefaultCotista = "ContaDefaultCotista";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ContaCorrenteMetadata))
			{
				if(ContaCorrenteMetadata.mapDelegates == null)
				{
					ContaCorrenteMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ContaCorrenteMetadata.meta == null)
				{
					ContaCorrenteMetadata.meta = new ContaCorrenteMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdBanco", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgencia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Numero", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ContaDefault", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdMoeda", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DigitoConta", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdLocal", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("DescricaoCodigo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdTipoConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdGrupo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ContaDefaultCliente", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ContaDefaultCotista", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "ContaCorrente";
				meta.Destination = "ContaCorrente";
				
				meta.spInsert = "proc_ContaCorrenteInsert";				
				meta.spUpdate = "proc_ContaCorrenteUpdate";		
				meta.spDelete = "proc_ContaCorrenteDelete";
				meta.spLoadAll = "proc_ContaCorrenteLoadAll";
				meta.spLoadByPrimaryKey = "proc_ContaCorrenteLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ContaCorrenteMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
