/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 2/11/2015 10:56:12 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				











	
using Financial.Common;

	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Investidor
{

	[Serializable]
	abstract public class esCodigoClienteAgenteCollection : esEntityCollection
	{
		public esCodigoClienteAgenteCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CodigoClienteAgenteCollection";
		}

		#region Query Logic
		protected void InitQuery(esCodigoClienteAgenteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCodigoClienteAgenteQuery);
		}
		#endregion
		
		virtual public CodigoClienteAgente DetachEntity(CodigoClienteAgente entity)
		{
			return base.DetachEntity(entity) as CodigoClienteAgente;
		}
		
		virtual public CodigoClienteAgente AttachEntity(CodigoClienteAgente entity)
		{
			return base.AttachEntity(entity) as CodigoClienteAgente;
		}
		
		virtual public void Combine(CodigoClienteAgenteCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CodigoClienteAgente this[int index]
		{
			get
			{
				return base[index] as CodigoClienteAgente;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CodigoClienteAgente);
		}
	}



	[Serializable]
	abstract public class esCodigoClienteAgente : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCodigoClienteAgenteQuery GetDynamicQuery()
		{
			return null;
		}

		public esCodigoClienteAgente()
		{

		}

		public esCodigoClienteAgente(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 identificador)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(identificador);
			else
				return LoadByPrimaryKeyStoredProcedure(identificador);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 identificador)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCodigoClienteAgenteQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Identificador == identificador);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 identificador)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(identificador);
			else
				return LoadByPrimaryKeyStoredProcedure(identificador);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 identificador)
		{
			esCodigoClienteAgenteQuery query = this.GetDynamicQuery();
			query.Where(query.Identificador == identificador);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 identificador)
		{
			esParameters parms = new esParameters();
			parms.Add("Identificador",identificador);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Identificador": this.str.Identificador = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "CodigoClienteBovespa": this.str.CodigoClienteBovespa = (string)value; break;							
						case "CodigoClienteBMF": this.str.CodigoClienteBMF = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Identificador":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Identificador = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "CodigoClienteBovespa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoClienteBovespa = (System.Int32?)value;
							break;
						
						case "CodigoClienteBMF":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoClienteBMF = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CodigoClienteAgente.Identificador
		/// </summary>
		virtual public System.Int32? Identificador
		{
			get
			{
				return base.GetSystemInt32(CodigoClienteAgenteMetadata.ColumnNames.Identificador);
			}
			
			set
			{
				base.SetSystemInt32(CodigoClienteAgenteMetadata.ColumnNames.Identificador, value);
			}
		}
		
		/// <summary>
		/// Maps to CodigoClienteAgente.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(CodigoClienteAgenteMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(CodigoClienteAgenteMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CodigoClienteAgente.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(CodigoClienteAgenteMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(CodigoClienteAgenteMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CodigoClienteAgente.CodigoClienteBovespa
		/// </summary>
		virtual public System.Int32? CodigoClienteBovespa
		{
			get
			{
				return base.GetSystemInt32(CodigoClienteAgenteMetadata.ColumnNames.CodigoClienteBovespa);
			}
			
			set
			{
				base.SetSystemInt32(CodigoClienteAgenteMetadata.ColumnNames.CodigoClienteBovespa, value);
			}
		}
		
		/// <summary>
		/// Maps to CodigoClienteAgente.CodigoClienteBMF
		/// </summary>
		virtual public System.Int32? CodigoClienteBMF
		{
			get
			{
				return base.GetSystemInt32(CodigoClienteAgenteMetadata.ColumnNames.CodigoClienteBMF);
			}
			
			set
			{
				base.SetSystemInt32(CodigoClienteAgenteMetadata.ColumnNames.CodigoClienteBMF, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCodigoClienteAgente entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Identificador
			{
				get
				{
					System.Int32? data = entity.Identificador;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Identificador = null;
					else entity.Identificador = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoClienteBovespa
			{
				get
				{
					System.Int32? data = entity.CodigoClienteBovespa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoClienteBovespa = null;
					else entity.CodigoClienteBovespa = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoClienteBMF
			{
				get
				{
					System.Int32? data = entity.CodigoClienteBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoClienteBMF = null;
					else entity.CodigoClienteBMF = Convert.ToInt32(value);
				}
			}
			

			private esCodigoClienteAgente entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCodigoClienteAgenteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCodigoClienteAgente can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CodigoClienteAgente : esCodigoClienteAgente
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_CodigoClienteAgente_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_CodigoClienteAgente_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCodigoClienteAgenteQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CodigoClienteAgenteMetadata.Meta();
			}
		}	
		

		public esQueryItem Identificador
		{
			get
			{
				return new esQueryItem(this, CodigoClienteAgenteMetadata.ColumnNames.Identificador, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, CodigoClienteAgenteMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, CodigoClienteAgenteMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoClienteBovespa
		{
			get
			{
				return new esQueryItem(this, CodigoClienteAgenteMetadata.ColumnNames.CodigoClienteBovespa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoClienteBMF
		{
			get
			{
				return new esQueryItem(this, CodigoClienteAgenteMetadata.ColumnNames.CodigoClienteBMF, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CodigoClienteAgenteCollection")]
	public partial class CodigoClienteAgenteCollection : esCodigoClienteAgenteCollection, IEnumerable<CodigoClienteAgente>
	{
		public CodigoClienteAgenteCollection()
		{

		}
		
		public static implicit operator List<CodigoClienteAgente>(CodigoClienteAgenteCollection coll)
		{
			List<CodigoClienteAgente> list = new List<CodigoClienteAgente>();
			
			foreach (CodigoClienteAgente emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CodigoClienteAgenteMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CodigoClienteAgenteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CodigoClienteAgente(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CodigoClienteAgente();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CodigoClienteAgenteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CodigoClienteAgenteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CodigoClienteAgenteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CodigoClienteAgente AddNew()
		{
			CodigoClienteAgente entity = base.AddNewEntity() as CodigoClienteAgente;
			
			return entity;
		}

		public CodigoClienteAgente FindByPrimaryKey(System.Int32 identificador)
		{
			return base.FindByPrimaryKey(identificador) as CodigoClienteAgente;
		}


		#region IEnumerable<CodigoClienteAgente> Members

		IEnumerator<CodigoClienteAgente> IEnumerable<CodigoClienteAgente>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CodigoClienteAgente;
			}
		}

		#endregion
		
		private CodigoClienteAgenteQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CodigoClienteAgente' table
	/// </summary>

	[Serializable]
	public partial class CodigoClienteAgente : esCodigoClienteAgente
	{
		public CodigoClienteAgente()
		{

		}
	
		public CodigoClienteAgente(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CodigoClienteAgenteMetadata.Meta();
			}
		}
		
		
		
		override protected esCodigoClienteAgenteQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CodigoClienteAgenteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CodigoClienteAgenteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CodigoClienteAgenteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CodigoClienteAgenteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CodigoClienteAgenteQuery query;
	}



	[Serializable]
	public partial class CodigoClienteAgenteQuery : esCodigoClienteAgenteQuery
	{
		public CodigoClienteAgenteQuery()
		{

		}		
		
		public CodigoClienteAgenteQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CodigoClienteAgenteMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CodigoClienteAgenteMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CodigoClienteAgenteMetadata.ColumnNames.Identificador, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CodigoClienteAgenteMetadata.PropertyNames.Identificador;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CodigoClienteAgenteMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CodigoClienteAgenteMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CodigoClienteAgenteMetadata.ColumnNames.IdAgente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CodigoClienteAgenteMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CodigoClienteAgenteMetadata.ColumnNames.CodigoClienteBovespa, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CodigoClienteAgenteMetadata.PropertyNames.CodigoClienteBovespa;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CodigoClienteAgenteMetadata.ColumnNames.CodigoClienteBMF, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CodigoClienteAgenteMetadata.PropertyNames.CodigoClienteBMF;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CodigoClienteAgenteMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Identificador = "Identificador";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CodigoClienteBovespa = "CodigoClienteBovespa";
			 public const string CodigoClienteBMF = "CodigoClienteBMF";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Identificador = "Identificador";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CodigoClienteBovespa = "CodigoClienteBovespa";
			 public const string CodigoClienteBMF = "CodigoClienteBMF";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CodigoClienteAgenteMetadata))
			{
				if(CodigoClienteAgenteMetadata.mapDelegates == null)
				{
					CodigoClienteAgenteMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CodigoClienteAgenteMetadata.meta == null)
				{
					CodigoClienteAgenteMetadata.meta = new CodigoClienteAgenteMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Identificador", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoClienteBovespa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoClienteBMF", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "CodigoClienteAgente";
				meta.Destination = "CodigoClienteAgente";
				
				meta.spInsert = "proc_CodigoClienteAgenteInsert";				
				meta.spUpdate = "proc_CodigoClienteAgenteUpdate";		
				meta.spDelete = "proc_CodigoClienteAgenteDelete";
				meta.spLoadAll = "proc_CodigoClienteAgenteLoadAll";
				meta.spLoadByPrimaryKey = "proc_CodigoClienteAgenteLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CodigoClienteAgenteMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
