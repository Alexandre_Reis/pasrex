/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 2/11/2015 10:56:13 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Investidor
{

	[Serializable]
	abstract public class esTabelaInterfaceClienteCollection : esEntityCollection
	{
		public esTabelaInterfaceClienteCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaInterfaceClienteCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaInterfaceClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaInterfaceClienteQuery);
		}
		#endregion
		
		virtual public TabelaInterfaceCliente DetachEntity(TabelaInterfaceCliente entity)
		{
			return base.DetachEntity(entity) as TabelaInterfaceCliente;
		}
		
		virtual public TabelaInterfaceCliente AttachEntity(TabelaInterfaceCliente entity)
		{
			return base.AttachEntity(entity) as TabelaInterfaceCliente;
		}
		
		virtual public void Combine(TabelaInterfaceClienteCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaInterfaceCliente this[int index]
		{
			get
			{
				return base[index] as TabelaInterfaceCliente;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaInterfaceCliente);
		}
	}



	[Serializable]
	abstract public class esTabelaInterfaceCliente : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaInterfaceClienteQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaInterfaceCliente()
		{

		}

		public esTabelaInterfaceCliente(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente, System.Int32 tipoInterface)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, tipoInterface);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, tipoInterface);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente, System.Int32 tipoInterface)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaInterfaceClienteQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente, query.TipoInterface == tipoInterface);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente, System.Int32 tipoInterface)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, tipoInterface);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, tipoInterface);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente, System.Int32 tipoInterface)
		{
			esTabelaInterfaceClienteQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente, query.TipoInterface == tipoInterface);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente, System.Int32 tipoInterface)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);			parms.Add("TipoInterface",tipoInterface);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "TipoInterface": this.str.TipoInterface = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "TipoInterface":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoInterface = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaInterfaceCliente.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(TabelaInterfaceClienteMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaInterfaceClienteMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaInterfaceCliente.TipoInterface
		/// </summary>
		virtual public System.Int32? TipoInterface
		{
			get
			{
				return base.GetSystemInt32(TabelaInterfaceClienteMetadata.ColumnNames.TipoInterface);
			}
			
			set
			{
				base.SetSystemInt32(TabelaInterfaceClienteMetadata.ColumnNames.TipoInterface, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaInterfaceCliente entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoInterface
			{
				get
				{
					System.Int32? data = entity.TipoInterface;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoInterface = null;
					else entity.TipoInterface = Convert.ToInt32(value);
				}
			}
			

			private esTabelaInterfaceCliente entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaInterfaceClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaInterfaceCliente can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaInterfaceCliente : esTabelaInterfaceCliente
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_TabelaInterfaceCliente_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaInterfaceClienteQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaInterfaceClienteMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, TabelaInterfaceClienteMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoInterface
		{
			get
			{
				return new esQueryItem(this, TabelaInterfaceClienteMetadata.ColumnNames.TipoInterface, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaInterfaceClienteCollection")]
	public partial class TabelaInterfaceClienteCollection : esTabelaInterfaceClienteCollection, IEnumerable<TabelaInterfaceCliente>
	{
		public TabelaInterfaceClienteCollection()
		{

		}
		
		public static implicit operator List<TabelaInterfaceCliente>(TabelaInterfaceClienteCollection coll)
		{
			List<TabelaInterfaceCliente> list = new List<TabelaInterfaceCliente>();
			
			foreach (TabelaInterfaceCliente emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaInterfaceClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaInterfaceClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaInterfaceCliente(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaInterfaceCliente();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaInterfaceClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaInterfaceClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaInterfaceClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaInterfaceCliente AddNew()
		{
			TabelaInterfaceCliente entity = base.AddNewEntity() as TabelaInterfaceCliente;
			
			return entity;
		}

		public TabelaInterfaceCliente FindByPrimaryKey(System.Int32 idCliente, System.Int32 tipoInterface)
		{
			return base.FindByPrimaryKey(idCliente, tipoInterface) as TabelaInterfaceCliente;
		}


		#region IEnumerable<TabelaInterfaceCliente> Members

		IEnumerator<TabelaInterfaceCliente> IEnumerable<TabelaInterfaceCliente>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaInterfaceCliente;
			}
		}

		#endregion
		
		private TabelaInterfaceClienteQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaInterfaceCliente' table
	/// </summary>

	[Serializable]
	public partial class TabelaInterfaceCliente : esTabelaInterfaceCliente
	{
		public TabelaInterfaceCliente()
		{

		}
	
		public TabelaInterfaceCliente(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaInterfaceClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaInterfaceClienteQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaInterfaceClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaInterfaceClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaInterfaceClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaInterfaceClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaInterfaceClienteQuery query;
	}



	[Serializable]
	public partial class TabelaInterfaceClienteQuery : esTabelaInterfaceClienteQuery
	{
		public TabelaInterfaceClienteQuery()
		{

		}		
		
		public TabelaInterfaceClienteQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaInterfaceClienteMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaInterfaceClienteMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaInterfaceClienteMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaInterfaceClienteMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInterfaceClienteMetadata.ColumnNames.TipoInterface, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaInterfaceClienteMetadata.PropertyNames.TipoInterface;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaInterfaceClienteMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string TipoInterface = "TipoInterface";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string TipoInterface = "TipoInterface";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaInterfaceClienteMetadata))
			{
				if(TabelaInterfaceClienteMetadata.mapDelegates == null)
				{
					TabelaInterfaceClienteMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaInterfaceClienteMetadata.meta == null)
				{
					TabelaInterfaceClienteMetadata.meta = new TabelaInterfaceClienteMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoInterface", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "TabelaInterfaceCliente";
				meta.Destination = "TabelaInterfaceCliente";
				
				meta.spInsert = "proc_TabelaInterfaceClienteInsert";				
				meta.spUpdate = "proc_TabelaInterfaceClienteUpdate";		
				meta.spDelete = "proc_TabelaInterfaceClienteDelete";
				meta.spLoadAll = "proc_TabelaInterfaceClienteLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaInterfaceClienteLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaInterfaceClienteMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
