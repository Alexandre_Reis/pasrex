/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 2/11/2015 10:56:13 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.CRM;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityPerfilCollection : esEntityCollection
	{
		public esSuitabilityPerfilCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityPerfilCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityPerfilQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityPerfilQuery);
		}
		#endregion
		
		virtual public SuitabilityPerfil DetachEntity(SuitabilityPerfil entity)
		{
			return base.DetachEntity(entity) as SuitabilityPerfil;
		}
		
		virtual public SuitabilityPerfil AttachEntity(SuitabilityPerfil entity)
		{
			return base.AttachEntity(entity) as SuitabilityPerfil;
		}
		
		virtual public void Combine(SuitabilityPerfilCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityPerfil this[int index]
		{
			get
			{
				return base[index] as SuitabilityPerfil;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityPerfil);
		}
	}



	[Serializable]
	abstract public class esSuitabilityPerfil : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityPerfilQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityPerfil()
		{

		}

		public esSuitabilityPerfil(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Byte idPerfilInvestidor, System.DateTime data)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilInvestidor, data);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilInvestidor, data);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Byte idPerfilInvestidor, System.DateTime data)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esSuitabilityPerfilQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPerfilInvestidor == idPerfilInvestidor, query.Data == data);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Byte idPerfilInvestidor, System.DateTime data)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilInvestidor, data);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilInvestidor, data);
		}

		private bool LoadByPrimaryKeyDynamic(System.Byte idPerfilInvestidor, System.DateTime data)
		{
			esSuitabilityPerfilQuery query = this.GetDynamicQuery();
			query.Where(query.IdPerfilInvestidor == idPerfilInvestidor, query.Data == data);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Byte idPerfilInvestidor, System.DateTime data)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPerfilInvestidor",idPerfilInvestidor);			parms.Add("Data",data);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPerfilInvestidor": this.str.IdPerfilInvestidor = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Pontos": this.str.Pontos = (string)value; break;							
						case "TipoCliente": this.str.TipoCliente = (string)value; break;							
						case "PerfilInvestidor": this.str.PerfilInvestidor = (string)value; break;							
						case "Definicao": this.str.Definicao = (string)value; break;							
						case "FaixaPontuacao": this.str.FaixaPontuacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPerfilInvestidor":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdPerfilInvestidor = (System.Byte?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Pontos":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Pontos = (System.Int32?)value;
							break;
						
						case "TipoCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoCliente = (System.Int32?)value;
							break;
						
						case "PerfilInvestidor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PerfilInvestidor = (System.Int32?)value;
							break;
						
						case "FaixaPontuacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FaixaPontuacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityPerfil.IdPerfilInvestidor
		/// </summary>
		virtual public System.Byte? IdPerfilInvestidor
		{
			get
			{
				return base.GetSystemByte(SuitabilityPerfilMetadata.ColumnNames.IdPerfilInvestidor);
			}
			
			set
			{
				if(base.SetSystemByte(SuitabilityPerfilMetadata.ColumnNames.IdPerfilInvestidor, value))
				{
					this._UpToPerfilInvestidorByIdPerfilInvestidor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfil.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(SuitabilityPerfilMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(SuitabilityPerfilMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfil.Pontos
		/// </summary>
		virtual public System.Int32? Pontos
		{
			get
			{
				return base.GetSystemInt32(SuitabilityPerfilMetadata.ColumnNames.Pontos);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityPerfilMetadata.ColumnNames.Pontos, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfil.TipoCliente
		/// </summary>
		virtual public System.Int32? TipoCliente
		{
			get
			{
				return base.GetSystemInt32(SuitabilityPerfilMetadata.ColumnNames.TipoCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(SuitabilityPerfilMetadata.ColumnNames.TipoCliente, value))
				{
					this._UpToTipoClienteByTipoCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfil.PerfilInvestidor
		/// </summary>
		virtual public System.Int32? PerfilInvestidor
		{
			get
			{
				return base.GetSystemInt32(SuitabilityPerfilMetadata.ColumnNames.PerfilInvestidor);
			}
			
			set
			{
				if(base.SetSystemInt32(SuitabilityPerfilMetadata.ColumnNames.PerfilInvestidor, value))
				{
					this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfil.Definicao
		/// </summary>
		virtual public System.String Definicao
		{
			get
			{
				return base.GetSystemString(SuitabilityPerfilMetadata.ColumnNames.Definicao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityPerfilMetadata.ColumnNames.Definicao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfil.FaixaPontuacao
		/// </summary>
		virtual public System.Int32? FaixaPontuacao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityPerfilMetadata.ColumnNames.FaixaPontuacao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityPerfilMetadata.ColumnNames.FaixaPontuacao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected PerfilInvestidor _UpToPerfilInvestidorByIdPerfilInvestidor;
		[CLSCompliant(false)]
		internal protected SuitabilityPerfilInvestidor _UpToSuitabilityPerfilInvestidorByPerfilInvestidor;
		[CLSCompliant(false)]
		internal protected TipoCliente _UpToTipoClienteByTipoCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityPerfil entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPerfilInvestidor
			{
				get
				{
					System.Byte? data = entity.IdPerfilInvestidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilInvestidor = null;
					else entity.IdPerfilInvestidor = Convert.ToByte(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Pontos
			{
				get
				{
					System.Int32? data = entity.Pontos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pontos = null;
					else entity.Pontos = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoCliente
			{
				get
				{
					System.Int32? data = entity.TipoCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCliente = null;
					else entity.TipoCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String PerfilInvestidor
			{
				get
				{
					System.Int32? data = entity.PerfilInvestidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerfilInvestidor = null;
					else entity.PerfilInvestidor = Convert.ToInt32(value);
				}
			}
				
			public System.String Definicao
			{
				get
				{
					System.String data = entity.Definicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Definicao = null;
					else entity.Definicao = Convert.ToString(value);
				}
			}
				
			public System.String FaixaPontuacao
			{
				get
				{
					System.Int32? data = entity.FaixaPontuacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FaixaPontuacao = null;
					else entity.FaixaPontuacao = Convert.ToInt32(value);
				}
			}
			

			private esSuitabilityPerfil entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityPerfilQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityPerfil can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityPerfil : esSuitabilityPerfil
	{

				
		#region UpToPerfilInvestidorByIdPerfilInvestidor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_SuitabilityPerfil_PerfilInvestidor
		/// </summary>

		[XmlIgnore]
		public PerfilInvestidor UpToPerfilInvestidorByIdPerfilInvestidor
		{
			get
			{
				if(this._UpToPerfilInvestidorByIdPerfilInvestidor == null
					&& IdPerfilInvestidor != null					)
				{
					this._UpToPerfilInvestidorByIdPerfilInvestidor = new PerfilInvestidor();
					this._UpToPerfilInvestidorByIdPerfilInvestidor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPerfilInvestidorByIdPerfilInvestidor", this._UpToPerfilInvestidorByIdPerfilInvestidor);
					this._UpToPerfilInvestidorByIdPerfilInvestidor.Query.Where(this._UpToPerfilInvestidorByIdPerfilInvestidor.Query.IdPerfilInvestidor == this.IdPerfilInvestidor);
					this._UpToPerfilInvestidorByIdPerfilInvestidor.Query.Load();
				}

				return this._UpToPerfilInvestidorByIdPerfilInvestidor;
			}
			
			set
			{
				this.RemovePreSave("UpToPerfilInvestidorByIdPerfilInvestidor");
				

				if(value == null)
				{
					this.IdPerfilInvestidor = null;
					this._UpToPerfilInvestidorByIdPerfilInvestidor = null;
				}
				else
				{
					this.IdPerfilInvestidor = value.IdPerfilInvestidor;
					this._UpToPerfilInvestidorByIdPerfilInvestidor = value;
					this.SetPreSave("UpToPerfilInvestidorByIdPerfilInvestidor", this._UpToPerfilInvestidorByIdPerfilInvestidor);
				}
				
			}
		}
		#endregion
		

				
		#region UpToSuitabilityPerfilInvestidorByPerfilInvestidor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - SuitabilityPerfil_SuitabilityPerfilInvestidor_FK
		/// </summary>

		[XmlIgnore]
		public SuitabilityPerfilInvestidor UpToSuitabilityPerfilInvestidorByPerfilInvestidor
		{
			get
			{
				if(this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor == null
					&& PerfilInvestidor != null					)
				{
					this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor = new SuitabilityPerfilInvestidor();
					this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSuitabilityPerfilInvestidorByPerfilInvestidor", this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor);
					this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor.Query.Where(this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor.Query.IdPerfilInvestidor == this.PerfilInvestidor);
					this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor.Query.Load();
				}

				return this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor;
			}
			
			set
			{
				this.RemovePreSave("UpToSuitabilityPerfilInvestidorByPerfilInvestidor");
				

				if(value == null)
				{
					this.PerfilInvestidor = null;
					this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor = null;
				}
				else
				{
					this.PerfilInvestidor = value.IdPerfilInvestidor;
					this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor = value;
					this.SetPreSave("UpToSuitabilityPerfilInvestidorByPerfilInvestidor", this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTipoClienteByTipoCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - SuitabilityPerfil_TipoCliente_FK
		/// </summary>

		[XmlIgnore]
		public TipoCliente UpToTipoClienteByTipoCliente
		{
			get
			{
				if(this._UpToTipoClienteByTipoCliente == null
					&& TipoCliente != null					)
				{
					this._UpToTipoClienteByTipoCliente = new TipoCliente();
					this._UpToTipoClienteByTipoCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTipoClienteByTipoCliente", this._UpToTipoClienteByTipoCliente);
					this._UpToTipoClienteByTipoCliente.Query.Where(this._UpToTipoClienteByTipoCliente.Query.IdTipo == this.TipoCliente);
					this._UpToTipoClienteByTipoCliente.Query.Load();
				}

				return this._UpToTipoClienteByTipoCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToTipoClienteByTipoCliente");
				

				if(value == null)
				{
					this.TipoCliente = null;
					this._UpToTipoClienteByTipoCliente = null;
				}
				else
				{
					this.TipoCliente = value.IdTipo;
					this._UpToTipoClienteByTipoCliente = value;
					this.SetPreSave("UpToTipoClienteByTipoCliente", this._UpToTipoClienteByTipoCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor != null)
			{
				this.PerfilInvestidor = this._UpToSuitabilityPerfilInvestidorByPerfilInvestidor.IdPerfilInvestidor;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityPerfilQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityPerfilMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPerfilInvestidor
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilMetadata.ColumnNames.IdPerfilInvestidor, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Pontos
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilMetadata.ColumnNames.Pontos, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoCliente
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilMetadata.ColumnNames.TipoCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PerfilInvestidor
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilMetadata.ColumnNames.PerfilInvestidor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Definicao
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilMetadata.ColumnNames.Definicao, esSystemType.String);
			}
		} 
		
		public esQueryItem FaixaPontuacao
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilMetadata.ColumnNames.FaixaPontuacao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityPerfilCollection")]
	public partial class SuitabilityPerfilCollection : esSuitabilityPerfilCollection, IEnumerable<SuitabilityPerfil>
	{
		public SuitabilityPerfilCollection()
		{

		}
		
		public static implicit operator List<SuitabilityPerfil>(SuitabilityPerfilCollection coll)
		{
			List<SuitabilityPerfil> list = new List<SuitabilityPerfil>();
			
			foreach (SuitabilityPerfil emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityPerfilMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityPerfilQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityPerfil(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityPerfil();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityPerfilQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityPerfilQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityPerfilQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityPerfil AddNew()
		{
			SuitabilityPerfil entity = base.AddNewEntity() as SuitabilityPerfil;
			
			return entity;
		}

		public SuitabilityPerfil FindByPrimaryKey(System.Byte idPerfilInvestidor, System.DateTime data)
		{
			return base.FindByPrimaryKey(idPerfilInvestidor, data) as SuitabilityPerfil;
		}


		#region IEnumerable<SuitabilityPerfil> Members

		IEnumerator<SuitabilityPerfil> IEnumerable<SuitabilityPerfil>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityPerfil;
			}
		}

		#endregion
		
		private SuitabilityPerfilQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityPerfil' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityPerfil : esSuitabilityPerfil
	{
		public SuitabilityPerfil()
		{

		}
	
		public SuitabilityPerfil(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityPerfilMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityPerfilQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityPerfilQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityPerfilQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityPerfilQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityPerfilQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityPerfilQuery query;
	}



	[Serializable]
	public partial class SuitabilityPerfilQuery : esSuitabilityPerfilQuery
	{
		public SuitabilityPerfilQuery()
		{

		}		
		
		public SuitabilityPerfilQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityPerfilMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityPerfilMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityPerfilMetadata.ColumnNames.IdPerfilInvestidor, 0, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = SuitabilityPerfilMetadata.PropertyNames.IdPerfilInvestidor;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SuitabilityPerfilMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilMetadata.ColumnNames.Pontos, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityPerfilMetadata.PropertyNames.Pontos;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilMetadata.ColumnNames.TipoCliente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityPerfilMetadata.PropertyNames.TipoCliente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilMetadata.ColumnNames.PerfilInvestidor, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityPerfilMetadata.PropertyNames.PerfilInvestidor;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilMetadata.ColumnNames.Definicao, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityPerfilMetadata.PropertyNames.Definicao;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilMetadata.ColumnNames.FaixaPontuacao, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityPerfilMetadata.PropertyNames.FaixaPontuacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityPerfilMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPerfilInvestidor = "IdPerfilInvestidor";
			 public const string Data = "Data";
			 public const string Pontos = "Pontos";
			 public const string TipoCliente = "TipoCliente";
			 public const string PerfilInvestidor = "PerfilInvestidor";
			 public const string Definicao = "Definicao";
			 public const string FaixaPontuacao = "FaixaPontuacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPerfilInvestidor = "IdPerfilInvestidor";
			 public const string Data = "Data";
			 public const string Pontos = "Pontos";
			 public const string TipoCliente = "TipoCliente";
			 public const string PerfilInvestidor = "PerfilInvestidor";
			 public const string Definicao = "Definicao";
			 public const string FaixaPontuacao = "FaixaPontuacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityPerfilMetadata))
			{
				if(SuitabilityPerfilMetadata.mapDelegates == null)
				{
					SuitabilityPerfilMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityPerfilMetadata.meta == null)
				{
					SuitabilityPerfilMetadata.meta = new SuitabilityPerfilMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPerfilInvestidor", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Pontos", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PerfilInvestidor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Definicao", new esTypeMap("text", "System.String"));
				meta.AddTypeMap("FaixaPontuacao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "SuitabilityPerfil";
				meta.Destination = "SuitabilityPerfil";
				
				meta.spInsert = "proc_SuitabilityPerfilInsert";				
				meta.spUpdate = "proc_SuitabilityPerfilUpdate";		
				meta.spDelete = "proc_SuitabilityPerfilDelete";
				meta.spLoadAll = "proc_SuitabilityPerfilLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityPerfilLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityPerfilMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
