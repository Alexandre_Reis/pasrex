/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/09/2015 14:44:54
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityQuestaoCollection : esEntityCollection
	{
		public esSuitabilityQuestaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityQuestaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityQuestaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityQuestaoQuery);
		}
		#endregion
		
		virtual public SuitabilityQuestao DetachEntity(SuitabilityQuestao entity)
		{
			return base.DetachEntity(entity) as SuitabilityQuestao;
		}
		
		virtual public SuitabilityQuestao AttachEntity(SuitabilityQuestao entity)
		{
			return base.AttachEntity(entity) as SuitabilityQuestao;
		}
		
		virtual public void Combine(SuitabilityQuestaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityQuestao this[int index]
		{
			get
			{
				return base[index] as SuitabilityQuestao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityQuestao);
		}
	}



	[Serializable]
	abstract public class esSuitabilityQuestao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityQuestaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityQuestao()
		{

		}

		public esSuitabilityQuestao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idQuestao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idQuestao);
			else
				return LoadByPrimaryKeyStoredProcedure(idQuestao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idQuestao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idQuestao);
			else
				return LoadByPrimaryKeyStoredProcedure(idQuestao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idQuestao)
		{
			esSuitabilityQuestaoQuery query = this.GetDynamicQuery();
			query.Where(query.IdQuestao == idQuestao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idQuestao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdQuestao",idQuestao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdQuestao": this.str.IdQuestao = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Fator": this.str.Fator = (string)value; break;							
						case "IdTipoValidacao": this.str.IdTipoValidacao = (string)value; break;							
						case "IdValidacao": this.str.IdValidacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdQuestao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdQuestao = (System.Int32?)value;
							break;
						
						case "Fator":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Fator = (System.Int32?)value;
							break;
						
						case "IdTipoValidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTipoValidacao = (System.Int32?)value;
							break;
						
						case "IdValidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdValidacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityQuestao.IdQuestao
		/// </summary>
		virtual public System.Int32? IdQuestao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityQuestaoMetadata.ColumnNames.IdQuestao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityQuestaoMetadata.ColumnNames.IdQuestao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityQuestao.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(SuitabilityQuestaoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityQuestaoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityQuestao.Fator
		/// </summary>
		virtual public System.Int32? Fator
		{
			get
			{
				return base.GetSystemInt32(SuitabilityQuestaoMetadata.ColumnNames.Fator);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityQuestaoMetadata.ColumnNames.Fator, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityQuestao.IdTipoValidacao
		/// </summary>
		virtual public System.Int32? IdTipoValidacao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityQuestaoMetadata.ColumnNames.IdTipoValidacao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityQuestaoMetadata.ColumnNames.IdTipoValidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityQuestao.IdValidacao
		/// </summary>
		virtual public System.Int32? IdValidacao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityQuestaoMetadata.ColumnNames.IdValidacao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityQuestaoMetadata.ColumnNames.IdValidacao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityQuestao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdQuestao
			{
				get
				{
					System.Int32? data = entity.IdQuestao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdQuestao = null;
					else entity.IdQuestao = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Fator
			{
				get
				{
					System.Int32? data = entity.Fator;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fator = null;
					else entity.Fator = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTipoValidacao
			{
				get
				{
					System.Int32? data = entity.IdTipoValidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTipoValidacao = null;
					else entity.IdTipoValidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdValidacao
			{
				get
				{
					System.Int32? data = entity.IdValidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdValidacao = null;
					else entity.IdValidacao = Convert.ToInt32(value);
				}
			}
			

			private esSuitabilityQuestao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityQuestaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityQuestao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityQuestao : esSuitabilityQuestao
	{

				
		#region SuitabilityOpcaoCollectionByIdQuestao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_SuitabilityOpcao_SuitabilityQuestao
		/// </summary>

		[XmlIgnore]
		public SuitabilityOpcaoCollection SuitabilityOpcaoCollectionByIdQuestao
		{
			get
			{
				if(this._SuitabilityOpcaoCollectionByIdQuestao == null)
				{
					this._SuitabilityOpcaoCollectionByIdQuestao = new SuitabilityOpcaoCollection();
					this._SuitabilityOpcaoCollectionByIdQuestao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("SuitabilityOpcaoCollectionByIdQuestao", this._SuitabilityOpcaoCollectionByIdQuestao);
				
					if(this.IdQuestao != null)
					{
						this._SuitabilityOpcaoCollectionByIdQuestao.Query.Where(this._SuitabilityOpcaoCollectionByIdQuestao.Query.IdQuestao == this.IdQuestao);
						this._SuitabilityOpcaoCollectionByIdQuestao.Query.Load();

						// Auto-hookup Foreign Keys
						this._SuitabilityOpcaoCollectionByIdQuestao.fks.Add(SuitabilityOpcaoMetadata.ColumnNames.IdQuestao, this.IdQuestao);
					}
				}

				return this._SuitabilityOpcaoCollectionByIdQuestao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._SuitabilityOpcaoCollectionByIdQuestao != null) 
				{ 
					this.RemovePostSave("SuitabilityOpcaoCollectionByIdQuestao"); 
					this._SuitabilityOpcaoCollectionByIdQuestao = null;
					
				} 
			} 			
		}

		private SuitabilityOpcaoCollection _SuitabilityOpcaoCollectionByIdQuestao;
		#endregion

				
		#region SuitabilityRespostaCollectionByIdQuestao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_SuitabilityResposta_SuitabilityQuestao
		/// </summary>

		[XmlIgnore]
		public SuitabilityRespostaCollection SuitabilityRespostaCollectionByIdQuestao
		{
			get
			{
				if(this._SuitabilityRespostaCollectionByIdQuestao == null)
				{
					this._SuitabilityRespostaCollectionByIdQuestao = new SuitabilityRespostaCollection();
					this._SuitabilityRespostaCollectionByIdQuestao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("SuitabilityRespostaCollectionByIdQuestao", this._SuitabilityRespostaCollectionByIdQuestao);
				
					if(this.IdQuestao != null)
					{
						this._SuitabilityRespostaCollectionByIdQuestao.Query.Where(this._SuitabilityRespostaCollectionByIdQuestao.Query.IdQuestao == this.IdQuestao);
						this._SuitabilityRespostaCollectionByIdQuestao.Query.Load();

						// Auto-hookup Foreign Keys
						this._SuitabilityRespostaCollectionByIdQuestao.fks.Add(SuitabilityRespostaMetadata.ColumnNames.IdQuestao, this.IdQuestao);
					}
				}

				return this._SuitabilityRespostaCollectionByIdQuestao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._SuitabilityRespostaCollectionByIdQuestao != null) 
				{ 
					this.RemovePostSave("SuitabilityRespostaCollectionByIdQuestao"); 
					this._SuitabilityRespostaCollectionByIdQuestao = null;
					
				} 
			} 			
		}

		private SuitabilityRespostaCollection _SuitabilityRespostaCollectionByIdQuestao;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "SuitabilityOpcaoCollectionByIdQuestao", typeof(SuitabilityOpcaoCollection), new SuitabilityOpcao()));
			props.Add(new esPropertyDescriptor(this, "SuitabilityRespostaCollectionByIdQuestao", typeof(SuitabilityRespostaCollection), new SuitabilityResposta()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._SuitabilityOpcaoCollectionByIdQuestao != null)
			{
				foreach(SuitabilityOpcao obj in this._SuitabilityOpcaoCollectionByIdQuestao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdQuestao = this.IdQuestao;
					}
				}
			}
			if(this._SuitabilityRespostaCollectionByIdQuestao != null)
			{
				foreach(SuitabilityResposta obj in this._SuitabilityRespostaCollectionByIdQuestao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdQuestao = this.IdQuestao;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityQuestaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityQuestaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdQuestao
		{
			get
			{
				return new esQueryItem(this, SuitabilityQuestaoMetadata.ColumnNames.IdQuestao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, SuitabilityQuestaoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Fator
		{
			get
			{
				return new esQueryItem(this, SuitabilityQuestaoMetadata.ColumnNames.Fator, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTipoValidacao
		{
			get
			{
				return new esQueryItem(this, SuitabilityQuestaoMetadata.ColumnNames.IdTipoValidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdValidacao
		{
			get
			{
				return new esQueryItem(this, SuitabilityQuestaoMetadata.ColumnNames.IdValidacao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityQuestaoCollection")]
	public partial class SuitabilityQuestaoCollection : esSuitabilityQuestaoCollection, IEnumerable<SuitabilityQuestao>
	{
		public SuitabilityQuestaoCollection()
		{

		}
		
		public static implicit operator List<SuitabilityQuestao>(SuitabilityQuestaoCollection coll)
		{
			List<SuitabilityQuestao> list = new List<SuitabilityQuestao>();
			
			foreach (SuitabilityQuestao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityQuestaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityQuestaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityQuestao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityQuestao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityQuestaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityQuestaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityQuestaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityQuestao AddNew()
		{
			SuitabilityQuestao entity = base.AddNewEntity() as SuitabilityQuestao;
			
			return entity;
		}

		public SuitabilityQuestao FindByPrimaryKey(System.Int32 idQuestao)
		{
			return base.FindByPrimaryKey(idQuestao) as SuitabilityQuestao;
		}


		#region IEnumerable<SuitabilityQuestao> Members

		IEnumerator<SuitabilityQuestao> IEnumerable<SuitabilityQuestao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityQuestao;
			}
		}

		#endregion
		
		private SuitabilityQuestaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityQuestao' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityQuestao : esSuitabilityQuestao
	{
		public SuitabilityQuestao()
		{

		}
	
		public SuitabilityQuestao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityQuestaoMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityQuestaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityQuestaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityQuestaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityQuestaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityQuestaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityQuestaoQuery query;
	}



	[Serializable]
	public partial class SuitabilityQuestaoQuery : esSuitabilityQuestaoQuery
	{
		public SuitabilityQuestaoQuery()
		{

		}		
		
		public SuitabilityQuestaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityQuestaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityQuestaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityQuestaoMetadata.ColumnNames.IdQuestao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityQuestaoMetadata.PropertyNames.IdQuestao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityQuestaoMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityQuestaoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 2000;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityQuestaoMetadata.ColumnNames.Fator, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityQuestaoMetadata.PropertyNames.Fator;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityQuestaoMetadata.ColumnNames.IdTipoValidacao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityQuestaoMetadata.PropertyNames.IdTipoValidacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityQuestaoMetadata.ColumnNames.IdValidacao, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityQuestaoMetadata.PropertyNames.IdValidacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityQuestaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdQuestao = "IdQuestao";
			 public const string Descricao = "Descricao";
			 public const string Fator = "Fator";
			 public const string IdTipoValidacao = "IdTipoValidacao";
			 public const string IdValidacao = "IdValidacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdQuestao = "IdQuestao";
			 public const string Descricao = "Descricao";
			 public const string Fator = "Fator";
			 public const string IdTipoValidacao = "IdTipoValidacao";
			 public const string IdValidacao = "IdValidacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityQuestaoMetadata))
			{
				if(SuitabilityQuestaoMetadata.mapDelegates == null)
				{
					SuitabilityQuestaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityQuestaoMetadata.meta == null)
				{
					SuitabilityQuestaoMetadata.meta = new SuitabilityQuestaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdQuestao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Fator", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTipoValidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdValidacao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "SuitabilityQuestao";
				meta.Destination = "SuitabilityQuestao";
				
				meta.spInsert = "proc_SuitabilityQuestaoInsert";				
				meta.spUpdate = "proc_SuitabilityQuestaoUpdate";		
				meta.spDelete = "proc_SuitabilityQuestaoDelete";
				meta.spLoadAll = "proc_SuitabilityQuestaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityQuestaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityQuestaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
