/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 07/08/2015 16:37:52
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.CRM;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Investidor
{

	[Serializable]
	abstract public class esBancoCollection : esEntityCollection
	{
		public esBancoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "BancoCollection";
		}

		#region Query Logic
		protected void InitQuery(esBancoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esBancoQuery);
		}
		#endregion
		
		virtual public Banco DetachEntity(Banco entity)
		{
			return base.DetachEntity(entity) as Banco;
		}
		
		virtual public Banco AttachEntity(Banco entity)
		{
			return base.AttachEntity(entity) as Banco;
		}
		
		virtual public void Combine(BancoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Banco this[int index]
		{
			get
			{
				return base[index] as Banco;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Banco);
		}
	}



	[Serializable]
	abstract public class esBanco : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esBancoQuery GetDynamicQuery()
		{
			return null;
		}

		public esBanco()
		{

		}

		public esBanco(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idBanco)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBanco);
			else
				return LoadByPrimaryKeyStoredProcedure(idBanco);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idBanco)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esBancoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdBanco == idBanco);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idBanco)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBanco);
			else
				return LoadByPrimaryKeyStoredProcedure(idBanco);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idBanco)
		{
			esBancoQuery query = this.GetDynamicQuery();
			query.Where(query.IdBanco == idBanco);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idBanco)
		{
			esParameters parms = new esParameters();
			parms.Add("IdBanco",idBanco);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdBanco": this.str.IdBanco = (string)value; break;							
						case "CodigoCompensacao": this.str.CodigoCompensacao = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "CodigoExterno": this.str.CodigoExterno = (string)value; break;							
						case "DescricaoCodigoExterno": this.str.DescricaoCodigoExterno = (string)value; break;							
						case "IdPessoa": this.str.IdPessoa = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdBanco":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdBanco = (System.Int32?)value;
							break;
						
						case "IdPessoa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPessoa = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Banco.IdBanco
		/// </summary>
		virtual public System.Int32? IdBanco
		{
			get
			{
				return base.GetSystemInt32(BancoMetadata.ColumnNames.IdBanco);
			}
			
			set
			{
				base.SetSystemInt32(BancoMetadata.ColumnNames.IdBanco, value);
			}
		}
		
		/// <summary>
		/// Maps to Banco.CodigoCompensacao
		/// </summary>
		virtual public System.String CodigoCompensacao
		{
			get
			{
				return base.GetSystemString(BancoMetadata.ColumnNames.CodigoCompensacao);
			}
			
			set
			{
				base.SetSystemString(BancoMetadata.ColumnNames.CodigoCompensacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Banco.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(BancoMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(BancoMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to Banco.CodigoExterno
		/// </summary>
		virtual public System.String CodigoExterno
		{
			get
			{
				return base.GetSystemString(BancoMetadata.ColumnNames.CodigoExterno);
			}
			
			set
			{
				base.SetSystemString(BancoMetadata.ColumnNames.CodigoExterno, value);
			}
		}
		
		/// <summary>
		/// Maps to Banco.DescricaoCodigoExterno
		/// </summary>
		virtual public System.String DescricaoCodigoExterno
		{
			get
			{
				return base.GetSystemString(BancoMetadata.ColumnNames.DescricaoCodigoExterno);
			}
			
			set
			{
				base.SetSystemString(BancoMetadata.ColumnNames.DescricaoCodigoExterno, value);
			}
		}
		
		/// <summary>
		/// Maps to Banco.IdPessoa
		/// </summary>
		virtual public System.Int32? IdPessoa
		{
			get
			{
				return base.GetSystemInt32(BancoMetadata.ColumnNames.IdPessoa);
			}
			
			set
			{
				if(base.SetSystemInt32(BancoMetadata.ColumnNames.IdPessoa, value))
				{
					this._UpToPessoaByIdPessoa = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Pessoa _UpToPessoaByIdPessoa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esBanco entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdBanco
			{
				get
				{
					System.Int32? data = entity.IdBanco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdBanco = null;
					else entity.IdBanco = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoCompensacao
			{
				get
				{
					System.String data = entity.CodigoCompensacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCompensacao = null;
					else entity.CodigoCompensacao = Convert.ToString(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String CodigoExterno
			{
				get
				{
					System.String data = entity.CodigoExterno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoExterno = null;
					else entity.CodigoExterno = Convert.ToString(value);
				}
			}
				
			public System.String DescricaoCodigoExterno
			{
				get
				{
					System.String data = entity.DescricaoCodigoExterno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescricaoCodigoExterno = null;
					else entity.DescricaoCodigoExterno = Convert.ToString(value);
				}
			}
				
			public System.String IdPessoa
			{
				get
				{
					System.Int32? data = entity.IdPessoa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPessoa = null;
					else entity.IdPessoa = Convert.ToInt32(value);
				}
			}
			

			private esBanco entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esBancoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esBanco can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Banco : esBanco
	{

				
		#region AgenciaCollectionByIdBanco - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Banco_Agencia_FK1
		/// </summary>

		[XmlIgnore]
		public AgenciaCollection AgenciaCollectionByIdBanco
		{
			get
			{
				if(this._AgenciaCollectionByIdBanco == null)
				{
					this._AgenciaCollectionByIdBanco = new AgenciaCollection();
					this._AgenciaCollectionByIdBanco.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AgenciaCollectionByIdBanco", this._AgenciaCollectionByIdBanco);
				
					if(this.IdBanco != null)
					{
						this._AgenciaCollectionByIdBanco.Query.Where(this._AgenciaCollectionByIdBanco.Query.IdBanco == this.IdBanco);
						this._AgenciaCollectionByIdBanco.Query.Load();

						// Auto-hookup Foreign Keys
						this._AgenciaCollectionByIdBanco.fks.Add(AgenciaMetadata.ColumnNames.IdBanco, this.IdBanco);
					}
				}

				return this._AgenciaCollectionByIdBanco;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AgenciaCollectionByIdBanco != null) 
				{ 
					this.RemovePostSave("AgenciaCollectionByIdBanco"); 
					this._AgenciaCollectionByIdBanco = null;
					
				} 
			} 			
		}

		private AgenciaCollection _AgenciaCollectionByIdBanco;
		#endregion

				
		#region ContaCorrenteCollectionByIdBanco - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Banco_ContaCorrente_FK1
		/// </summary>

		[XmlIgnore]
		public ContaCorrenteCollection ContaCorrenteCollectionByIdBanco
		{
			get
			{
				if(this._ContaCorrenteCollectionByIdBanco == null)
				{
					this._ContaCorrenteCollectionByIdBanco = new ContaCorrenteCollection();
					this._ContaCorrenteCollectionByIdBanco.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ContaCorrenteCollectionByIdBanco", this._ContaCorrenteCollectionByIdBanco);
				
					if(this.IdBanco != null)
					{
						this._ContaCorrenteCollectionByIdBanco.Query.Where(this._ContaCorrenteCollectionByIdBanco.Query.IdBanco == this.IdBanco);
						this._ContaCorrenteCollectionByIdBanco.Query.Load();

						// Auto-hookup Foreign Keys
						this._ContaCorrenteCollectionByIdBanco.fks.Add(ContaCorrenteMetadata.ColumnNames.IdBanco, this.IdBanco);
					}
				}

				return this._ContaCorrenteCollectionByIdBanco;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ContaCorrenteCollectionByIdBanco != null) 
				{ 
					this.RemovePostSave("ContaCorrenteCollectionByIdBanco"); 
					this._ContaCorrenteCollectionByIdBanco = null;
					
				} 
			} 			
		}

		private ContaCorrenteCollection _ContaCorrenteCollectionByIdBanco;
		#endregion

				
		#region UpToPessoaByIdPessoa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Banco_Pessoa_FK
		/// </summary>

		[XmlIgnore]
		public Pessoa UpToPessoaByIdPessoa
		{
			get
			{
				if(this._UpToPessoaByIdPessoa == null
					&& IdPessoa != null					)
				{
					this._UpToPessoaByIdPessoa = new Pessoa();
					this._UpToPessoaByIdPessoa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
					this._UpToPessoaByIdPessoa.Query.Where(this._UpToPessoaByIdPessoa.Query.IdPessoa == this.IdPessoa);
					this._UpToPessoaByIdPessoa.Query.Load();
				}

				return this._UpToPessoaByIdPessoa;
			}
			
			set
			{
				this.RemovePreSave("UpToPessoaByIdPessoa");
				

				if(value == null)
				{
					this.IdPessoa = null;
					this._UpToPessoaByIdPessoa = null;
				}
				else
				{
					this.IdPessoa = value.IdPessoa;
					this._UpToPessoaByIdPessoa = value;
					this.SetPreSave("UpToPessoaByIdPessoa", this._UpToPessoaByIdPessoa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "AgenciaCollectionByIdBanco", typeof(AgenciaCollection), new Agencia()));
			props.Add(new esPropertyDescriptor(this, "ContaCorrenteCollectionByIdBanco", typeof(ContaCorrenteCollection), new ContaCorrente()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._AgenciaCollectionByIdBanco != null)
			{
				foreach(Agencia obj in this._AgenciaCollectionByIdBanco)
				{
					if(obj.es.IsAdded)
					{
						obj.IdBanco = this.IdBanco;
					}
				}
			}
			if(this._ContaCorrenteCollectionByIdBanco != null)
			{
				foreach(ContaCorrente obj in this._ContaCorrenteCollectionByIdBanco)
				{
					if(obj.es.IsAdded)
					{
						obj.IdBanco = this.IdBanco;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esBancoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return BancoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdBanco
		{
			get
			{
				return new esQueryItem(this, BancoMetadata.ColumnNames.IdBanco, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoCompensacao
		{
			get
			{
				return new esQueryItem(this, BancoMetadata.ColumnNames.CodigoCompensacao, esSystemType.String);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, BancoMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoExterno
		{
			get
			{
				return new esQueryItem(this, BancoMetadata.ColumnNames.CodigoExterno, esSystemType.String);
			}
		} 
		
		public esQueryItem DescricaoCodigoExterno
		{
			get
			{
				return new esQueryItem(this, BancoMetadata.ColumnNames.DescricaoCodigoExterno, esSystemType.String);
			}
		} 
		
		public esQueryItem IdPessoa
		{
			get
			{
				return new esQueryItem(this, BancoMetadata.ColumnNames.IdPessoa, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("BancoCollection")]
	public partial class BancoCollection : esBancoCollection, IEnumerable<Banco>
	{
		public BancoCollection()
		{

		}
		
		public static implicit operator List<Banco>(BancoCollection coll)
		{
			List<Banco> list = new List<Banco>();
			
			foreach (Banco emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  BancoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BancoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Banco(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Banco();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public BancoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BancoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(BancoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Banco AddNew()
		{
			Banco entity = base.AddNewEntity() as Banco;
			
			return entity;
		}

		public Banco FindByPrimaryKey(System.Int32 idBanco)
		{
			return base.FindByPrimaryKey(idBanco) as Banco;
		}


		#region IEnumerable<Banco> Members

		IEnumerator<Banco> IEnumerable<Banco>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Banco;
			}
		}

		#endregion
		
		private BancoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Banco' table
	/// </summary>

	[Serializable]
	public partial class Banco : esBanco
	{
		public Banco()
		{

		}
	
		public Banco(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return BancoMetadata.Meta();
			}
		}
		
		
		
		override protected esBancoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BancoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public BancoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BancoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(BancoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private BancoQuery query;
	}



	[Serializable]
	public partial class BancoQuery : esBancoQuery
	{
		public BancoQuery()
		{

		}		
		
		public BancoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class BancoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected BancoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(BancoMetadata.ColumnNames.IdBanco, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BancoMetadata.PropertyNames.IdBanco;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BancoMetadata.ColumnNames.CodigoCompensacao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = BancoMetadata.PropertyNames.CodigoCompensacao;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BancoMetadata.ColumnNames.Nome, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = BancoMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BancoMetadata.ColumnNames.CodigoExterno, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = BancoMetadata.PropertyNames.CodigoExterno;
			c.CharacterMaxLength = 36;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BancoMetadata.ColumnNames.DescricaoCodigoExterno, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = BancoMetadata.PropertyNames.DescricaoCodigoExterno;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BancoMetadata.ColumnNames.IdPessoa, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BancoMetadata.PropertyNames.IdPessoa;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public BancoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdBanco = "IdBanco";
			 public const string CodigoCompensacao = "CodigoCompensacao";
			 public const string Nome = "Nome";
			 public const string CodigoExterno = "CodigoExterno";
			 public const string DescricaoCodigoExterno = "DescricaoCodigoExterno";
			 public const string IdPessoa = "IdPessoa";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdBanco = "IdBanco";
			 public const string CodigoCompensacao = "CodigoCompensacao";
			 public const string Nome = "Nome";
			 public const string CodigoExterno = "CodigoExterno";
			 public const string DescricaoCodigoExterno = "DescricaoCodigoExterno";
			 public const string IdPessoa = "IdPessoa";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(BancoMetadata))
			{
				if(BancoMetadata.mapDelegates == null)
				{
					BancoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (BancoMetadata.meta == null)
				{
					BancoMetadata.meta = new BancoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdBanco", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoCompensacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoExterno", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DescricaoCodigoExterno", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdPessoa", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "Banco";
				meta.Destination = "Banco";
				
				meta.spInsert = "proc_BancoInsert";				
				meta.spUpdate = "proc_BancoUpdate";		
				meta.spDelete = "proc_BancoDelete";
				meta.spLoadAll = "proc_BancoLoadAll";
				meta.spLoadByPrimaryKey = "proc_BancoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private BancoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
