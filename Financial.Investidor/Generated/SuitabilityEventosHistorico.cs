/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/09/2015 16:39:42
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityEventosHistoricoCollection : esEntityCollection
	{
		public esSuitabilityEventosHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityEventosHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityEventosHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityEventosHistoricoQuery);
		}
		#endregion
		
		virtual public SuitabilityEventosHistorico DetachEntity(SuitabilityEventosHistorico entity)
		{
			return base.DetachEntity(entity) as SuitabilityEventosHistorico;
		}
		
		virtual public SuitabilityEventosHistorico AttachEntity(SuitabilityEventosHistorico entity)
		{
			return base.AttachEntity(entity) as SuitabilityEventosHistorico;
		}
		
		virtual public void Combine(SuitabilityEventosHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityEventosHistorico this[int index]
		{
			get
			{
				return base[index] as SuitabilityEventosHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityEventosHistorico);
		}
	}



	[Serializable]
	abstract public class esSuitabilityEventosHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityEventosHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityEventosHistorico()
		{

		}

		public esSuitabilityEventosHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idSuitabilityEventos)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idSuitabilityEventos);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idSuitabilityEventos);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idSuitabilityEventos)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idSuitabilityEventos);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idSuitabilityEventos);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idSuitabilityEventos)
		{
			esSuitabilityEventosHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdSuitabilityEventos == idSuitabilityEventos);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idSuitabilityEventos)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdSuitabilityEventos",idSuitabilityEventos);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdSuitabilityEventos": this.str.IdSuitabilityEventos = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "IdMensagem": this.str.IdMensagem = (string)value; break;							
						case "Mesagem": this.str.Mesagem = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdSuitabilityEventos":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSuitabilityEventos = (System.Int32?)value;
							break;
						
						case "IdMensagem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMensagem = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityEventosHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(SuitabilityEventosHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(SuitabilityEventosHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityEventosHistorico.IdSuitabilityEventos
		/// </summary>
		virtual public System.Int32? IdSuitabilityEventos
		{
			get
			{
				return base.GetSystemInt32(SuitabilityEventosHistoricoMetadata.ColumnNames.IdSuitabilityEventos);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityEventosHistoricoMetadata.ColumnNames.IdSuitabilityEventos, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityEventosHistorico.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(SuitabilityEventosHistoricoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityEventosHistoricoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityEventosHistorico.IdMensagem
		/// </summary>
		virtual public System.Int32? IdMensagem
		{
			get
			{
				return base.GetSystemInt32(SuitabilityEventosHistoricoMetadata.ColumnNames.IdMensagem);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityEventosHistoricoMetadata.ColumnNames.IdMensagem, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityEventosHistorico.Mesagem
		/// </summary>
		virtual public System.String Mesagem
		{
			get
			{
				return base.GetSystemString(SuitabilityEventosHistoricoMetadata.ColumnNames.Mesagem);
			}
			
			set
			{
				base.SetSystemString(SuitabilityEventosHistoricoMetadata.ColumnNames.Mesagem, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityEventosHistorico.Tipo
		/// </summary>
		virtual public System.String Tipo
		{
			get
			{
				return base.GetSystemString(SuitabilityEventosHistoricoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemString(SuitabilityEventosHistoricoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityEventosHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdSuitabilityEventos
			{
				get
				{
					System.Int32? data = entity.IdSuitabilityEventos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSuitabilityEventos = null;
					else entity.IdSuitabilityEventos = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String IdMensagem
			{
				get
				{
					System.Int32? data = entity.IdMensagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMensagem = null;
					else entity.IdMensagem = Convert.ToInt32(value);
				}
			}
				
			public System.String Mesagem
			{
				get
				{
					System.String data = entity.Mesagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Mesagem = null;
					else entity.Mesagem = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.String data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToString(value);
				}
			}
			

			private esSuitabilityEventosHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityEventosHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityEventosHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityEventosHistorico : esSuitabilityEventosHistorico
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityEventosHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityEventosHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, SuitabilityEventosHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdSuitabilityEventos
		{
			get
			{
				return new esQueryItem(this, SuitabilityEventosHistoricoMetadata.ColumnNames.IdSuitabilityEventos, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, SuitabilityEventosHistoricoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdMensagem
		{
			get
			{
				return new esQueryItem(this, SuitabilityEventosHistoricoMetadata.ColumnNames.IdMensagem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Mesagem
		{
			get
			{
				return new esQueryItem(this, SuitabilityEventosHistoricoMetadata.ColumnNames.Mesagem, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, SuitabilityEventosHistoricoMetadata.ColumnNames.Tipo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityEventosHistoricoCollection")]
	public partial class SuitabilityEventosHistoricoCollection : esSuitabilityEventosHistoricoCollection, IEnumerable<SuitabilityEventosHistorico>
	{
		public SuitabilityEventosHistoricoCollection()
		{

		}
		
		public static implicit operator List<SuitabilityEventosHistorico>(SuitabilityEventosHistoricoCollection coll)
		{
			List<SuitabilityEventosHistorico> list = new List<SuitabilityEventosHistorico>();
			
			foreach (SuitabilityEventosHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityEventosHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityEventosHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityEventosHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityEventosHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityEventosHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityEventosHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityEventosHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityEventosHistorico AddNew()
		{
			SuitabilityEventosHistorico entity = base.AddNewEntity() as SuitabilityEventosHistorico;
			
			return entity;
		}

		public SuitabilityEventosHistorico FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idSuitabilityEventos)
		{
			return base.FindByPrimaryKey(dataHistorico, idSuitabilityEventos) as SuitabilityEventosHistorico;
		}


		#region IEnumerable<SuitabilityEventosHistorico> Members

		IEnumerator<SuitabilityEventosHistorico> IEnumerable<SuitabilityEventosHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityEventosHistorico;
			}
		}

		#endregion
		
		private SuitabilityEventosHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityEventosHistorico' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityEventosHistorico : esSuitabilityEventosHistorico
	{
		public SuitabilityEventosHistorico()
		{

		}
	
		public SuitabilityEventosHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityEventosHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityEventosHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityEventosHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityEventosHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityEventosHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityEventosHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityEventosHistoricoQuery query;
	}



	[Serializable]
	public partial class SuitabilityEventosHistoricoQuery : esSuitabilityEventosHistoricoQuery
	{
		public SuitabilityEventosHistoricoQuery()
		{

		}		
		
		public SuitabilityEventosHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityEventosHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityEventosHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityEventosHistoricoMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SuitabilityEventosHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityEventosHistoricoMetadata.ColumnNames.IdSuitabilityEventos, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityEventosHistoricoMetadata.PropertyNames.IdSuitabilityEventos;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityEventosHistoricoMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityEventosHistoricoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityEventosHistoricoMetadata.ColumnNames.IdMensagem, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityEventosHistoricoMetadata.PropertyNames.IdMensagem;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityEventosHistoricoMetadata.ColumnNames.Mesagem, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityEventosHistoricoMetadata.PropertyNames.Mesagem;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityEventosHistoricoMetadata.ColumnNames.Tipo, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityEventosHistoricoMetadata.PropertyNames.Tipo;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityEventosHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdSuitabilityEventos = "IdSuitabilityEventos";
			 public const string Descricao = "Descricao";
			 public const string IdMensagem = "IdMensagem";
			 public const string Mesagem = "Mesagem";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdSuitabilityEventos = "IdSuitabilityEventos";
			 public const string Descricao = "Descricao";
			 public const string IdMensagem = "IdMensagem";
			 public const string Mesagem = "Mesagem";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityEventosHistoricoMetadata))
			{
				if(SuitabilityEventosHistoricoMetadata.mapDelegates == null)
				{
					SuitabilityEventosHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityEventosHistoricoMetadata.meta == null)
				{
					SuitabilityEventosHistoricoMetadata.meta = new SuitabilityEventosHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdSuitabilityEventos", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdMensagem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Mesagem", new esTypeMap("text", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "SuitabilityEventosHistorico";
				meta.Destination = "SuitabilityEventosHistorico";
				
				meta.spInsert = "proc_SuitabilityEventosHistoricoInsert";				
				meta.spUpdate = "proc_SuitabilityEventosHistoricoUpdate";		
				meta.spDelete = "proc_SuitabilityEventosHistoricoDelete";
				meta.spLoadAll = "proc_SuitabilityEventosHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityEventosHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityEventosHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
