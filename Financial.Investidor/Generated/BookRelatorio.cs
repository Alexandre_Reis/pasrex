/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 2/11/2015 10:55:58 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Investidor
{

	[Serializable]
	abstract public class esBookRelatorioCollection : esEntityCollection
	{
		public esBookRelatorioCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "BookRelatorioCollection";
		}

		#region Query Logic
		protected void InitQuery(esBookRelatorioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esBookRelatorioQuery);
		}
		#endregion
		
		virtual public BookRelatorio DetachEntity(BookRelatorio entity)
		{
			return base.DetachEntity(entity) as BookRelatorio;
		}
		
		virtual public BookRelatorio AttachEntity(BookRelatorio entity)
		{
			return base.AttachEntity(entity) as BookRelatorio;
		}
		
		virtual public void Combine(BookRelatorioCollection collection)
		{
			base.Combine(collection);
		}
		
		new public BookRelatorio this[int index]
		{
			get
			{
				return base[index] as BookRelatorio;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(BookRelatorio);
		}
	}



	[Serializable]
	abstract public class esBookRelatorio : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esBookRelatorioQuery GetDynamicQuery()
		{
			return null;
		}

		public esBookRelatorio()
		{

		}

		public esBookRelatorio(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idBook, System.Int32 idRelatorio)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBook, idRelatorio);
			else
				return LoadByPrimaryKeyStoredProcedure(idBook, idRelatorio);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idBook, System.Int32 idRelatorio)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esBookRelatorioQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdBook == idBook, query.IdRelatorio == idRelatorio);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idBook, System.Int32 idRelatorio)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBook, idRelatorio);
			else
				return LoadByPrimaryKeyStoredProcedure(idBook, idRelatorio);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idBook, System.Int32 idRelatorio)
		{
			esBookRelatorioQuery query = this.GetDynamicQuery();
			query.Where(query.IdBook == idBook, query.IdRelatorio == idRelatorio);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idBook, System.Int32 idRelatorio)
		{
			esParameters parms = new esParameters();
			parms.Add("IdBook",idBook);			parms.Add("IdRelatorio",idRelatorio);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdBook": this.str.IdBook = (string)value; break;							
						case "IdRelatorio": this.str.IdRelatorio = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdBook":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdBook = (System.Int32?)value;
							break;
						
						case "IdRelatorio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdRelatorio = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to BookRelatorio.IdBook
		/// </summary>
		virtual public System.Int32? IdBook
		{
			get
			{
				return base.GetSystemInt32(BookRelatorioMetadata.ColumnNames.IdBook);
			}
			
			set
			{
				if(base.SetSystemInt32(BookRelatorioMetadata.ColumnNames.IdBook, value))
				{
					this._UpToBookByIdBook = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to BookRelatorio.IdRelatorio
		/// </summary>
		virtual public System.Int32? IdRelatorio
		{
			get
			{
				return base.GetSystemInt32(BookRelatorioMetadata.ColumnNames.IdRelatorio);
			}
			
			set
			{
				base.SetSystemInt32(BookRelatorioMetadata.ColumnNames.IdRelatorio, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Book _UpToBookByIdBook;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esBookRelatorio entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdBook
			{
				get
				{
					System.Int32? data = entity.IdBook;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdBook = null;
					else entity.IdBook = Convert.ToInt32(value);
				}
			}
				
			public System.String IdRelatorio
			{
				get
				{
					System.Int32? data = entity.IdRelatorio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdRelatorio = null;
					else entity.IdRelatorio = Convert.ToInt32(value);
				}
			}
			

			private esBookRelatorio entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esBookRelatorioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esBookRelatorio can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class BookRelatorio : esBookRelatorio
	{

				
		#region UpToBookByIdBook - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Book_BookRelatorio_FK1
		/// </summary>

		[XmlIgnore]
		public Book UpToBookByIdBook
		{
			get
			{
				if(this._UpToBookByIdBook == null
					&& IdBook != null					)
				{
					this._UpToBookByIdBook = new Book();
					this._UpToBookByIdBook.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToBookByIdBook", this._UpToBookByIdBook);
					this._UpToBookByIdBook.Query.Where(this._UpToBookByIdBook.Query.IdBook == this.IdBook);
					this._UpToBookByIdBook.Query.Load();
				}

				return this._UpToBookByIdBook;
			}
			
			set
			{
				this.RemovePreSave("UpToBookByIdBook");
				

				if(value == null)
				{
					this.IdBook = null;
					this._UpToBookByIdBook = null;
				}
				else
				{
					this.IdBook = value.IdBook;
					this._UpToBookByIdBook = value;
					this.SetPreSave("UpToBookByIdBook", this._UpToBookByIdBook);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToBookByIdBook != null)
			{
				this.IdBook = this._UpToBookByIdBook.IdBook;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esBookRelatorioQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return BookRelatorioMetadata.Meta();
			}
		}	
		

		public esQueryItem IdBook
		{
			get
			{
				return new esQueryItem(this, BookRelatorioMetadata.ColumnNames.IdBook, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdRelatorio
		{
			get
			{
				return new esQueryItem(this, BookRelatorioMetadata.ColumnNames.IdRelatorio, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("BookRelatorioCollection")]
	public partial class BookRelatorioCollection : esBookRelatorioCollection, IEnumerable<BookRelatorio>
	{
		public BookRelatorioCollection()
		{

		}
		
		public static implicit operator List<BookRelatorio>(BookRelatorioCollection coll)
		{
			List<BookRelatorio> list = new List<BookRelatorio>();
			
			foreach (BookRelatorio emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  BookRelatorioMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BookRelatorioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new BookRelatorio(row);
		}

		override protected esEntity CreateEntity()
		{
			return new BookRelatorio();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public BookRelatorioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BookRelatorioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(BookRelatorioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public BookRelatorio AddNew()
		{
			BookRelatorio entity = base.AddNewEntity() as BookRelatorio;
			
			return entity;
		}

		public BookRelatorio FindByPrimaryKey(System.Int32 idBook, System.Int32 idRelatorio)
		{
			return base.FindByPrimaryKey(idBook, idRelatorio) as BookRelatorio;
		}


		#region IEnumerable<BookRelatorio> Members

		IEnumerator<BookRelatorio> IEnumerable<BookRelatorio>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as BookRelatorio;
			}
		}

		#endregion
		
		private BookRelatorioQuery query;
	}


	/// <summary>
	/// Encapsulates the 'BookRelatorio' table
	/// </summary>

	[Serializable]
	public partial class BookRelatorio : esBookRelatorio
	{
		public BookRelatorio()
		{

		}
	
		public BookRelatorio(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return BookRelatorioMetadata.Meta();
			}
		}
		
		
		
		override protected esBookRelatorioQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BookRelatorioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public BookRelatorioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BookRelatorioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(BookRelatorioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private BookRelatorioQuery query;
	}



	[Serializable]
	public partial class BookRelatorioQuery : esBookRelatorioQuery
	{
		public BookRelatorioQuery()
		{

		}		
		
		public BookRelatorioQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class BookRelatorioMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected BookRelatorioMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(BookRelatorioMetadata.ColumnNames.IdBook, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BookRelatorioMetadata.PropertyNames.IdBook;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BookRelatorioMetadata.ColumnNames.IdRelatorio, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BookRelatorioMetadata.PropertyNames.IdRelatorio;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public BookRelatorioMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdBook = "IdBook";
			 public const string IdRelatorio = "IdRelatorio";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdBook = "IdBook";
			 public const string IdRelatorio = "IdRelatorio";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(BookRelatorioMetadata))
			{
				if(BookRelatorioMetadata.mapDelegates == null)
				{
					BookRelatorioMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (BookRelatorioMetadata.meta == null)
				{
					BookRelatorioMetadata.meta = new BookRelatorioMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdBook", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdRelatorio", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "BookRelatorio";
				meta.Destination = "BookRelatorio";
				
				meta.spInsert = "proc_BookRelatorioInsert";				
				meta.spUpdate = "proc_BookRelatorioUpdate";		
				meta.spDelete = "proc_BookRelatorioDelete";
				meta.spLoadAll = "proc_BookRelatorioLoadAll";
				meta.spLoadByPrimaryKey = "proc_BookRelatorioLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private BookRelatorioMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
