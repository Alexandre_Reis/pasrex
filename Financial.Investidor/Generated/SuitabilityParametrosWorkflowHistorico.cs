/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/09/2015 16:11:31
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityParametrosWorkflowHistoricoCollection : esEntityCollection
	{
		public esSuitabilityParametrosWorkflowHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityParametrosWorkflowHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityParametrosWorkflowHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityParametrosWorkflowHistoricoQuery);
		}
		#endregion
		
		virtual public SuitabilityParametrosWorkflowHistorico DetachEntity(SuitabilityParametrosWorkflowHistorico entity)
		{
			return base.DetachEntity(entity) as SuitabilityParametrosWorkflowHistorico;
		}
		
		virtual public SuitabilityParametrosWorkflowHistorico AttachEntity(SuitabilityParametrosWorkflowHistorico entity)
		{
			return base.AttachEntity(entity) as SuitabilityParametrosWorkflowHistorico;
		}
		
		virtual public void Combine(SuitabilityParametrosWorkflowHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityParametrosWorkflowHistorico this[int index]
		{
			get
			{
				return base[index] as SuitabilityParametrosWorkflowHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityParametrosWorkflowHistorico);
		}
	}



	[Serializable]
	abstract public class esSuitabilityParametrosWorkflowHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityParametrosWorkflowHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityParametrosWorkflowHistorico()
		{

		}

		public esSuitabilityParametrosWorkflowHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idParametrosWorkflow)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idParametrosWorkflow);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idParametrosWorkflow);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idParametrosWorkflow)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idParametrosWorkflow);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idParametrosWorkflow);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idParametrosWorkflow)
		{
			esSuitabilityParametrosWorkflowHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdParametrosWorkflow == idParametrosWorkflow);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idParametrosWorkflow)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdParametrosWorkflow",idParametrosWorkflow);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdParametrosWorkflow": this.str.IdParametrosWorkflow = (string)value; break;							
						case "AtivaWorkFlow": this.str.AtivaWorkFlow = (string)value; break;							
						case "VerificaEnquadramento": this.str.VerificaEnquadramento = (string)value; break;							
						case "ResponsavelNome": this.str.ResponsavelNome = (string)value; break;							
						case "ResponsavelEmail": this.str.ResponsavelEmail = (string)value; break;							
						case "ValorPeriodo": this.str.ValorPeriodo = (string)value; break;							
						case "GrandezaPeriodo": this.str.GrandezaPeriodo = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdParametrosWorkflow":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdParametrosWorkflow = (System.Int32?)value;
							break;
						
						case "ValorPeriodo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.ValorPeriodo = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflowHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflowHistorico.IdParametrosWorkflow
		/// </summary>
		virtual public System.Int32? IdParametrosWorkflow
		{
			get
			{
				return base.GetSystemInt32(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.IdParametrosWorkflow);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.IdParametrosWorkflow, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflowHistorico.AtivaWorkFlow
		/// </summary>
		virtual public System.String AtivaWorkFlow
		{
			get
			{
				return base.GetSystemString(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.AtivaWorkFlow);
			}
			
			set
			{
				base.SetSystemString(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.AtivaWorkFlow, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflowHistorico.VerificaEnquadramento
		/// </summary>
		virtual public System.String VerificaEnquadramento
		{
			get
			{
				return base.GetSystemString(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.VerificaEnquadramento);
			}
			
			set
			{
				base.SetSystemString(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.VerificaEnquadramento, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflowHistorico.ResponsavelNome
		/// </summary>
		virtual public System.String ResponsavelNome
		{
			get
			{
				return base.GetSystemString(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.ResponsavelNome);
			}
			
			set
			{
				base.SetSystemString(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.ResponsavelNome, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflowHistorico.ResponsavelEmail
		/// </summary>
		virtual public System.String ResponsavelEmail
		{
			get
			{
				return base.GetSystemString(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.ResponsavelEmail);
			}
			
			set
			{
				base.SetSystemString(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.ResponsavelEmail, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflowHistorico.ValorPeriodo
		/// </summary>
		virtual public System.Int32? ValorPeriodo
		{
			get
			{
				return base.GetSystemInt32(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.ValorPeriodo);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.ValorPeriodo, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflowHistorico.GrandezaPeriodo
		/// </summary>
		virtual public System.String GrandezaPeriodo
		{
			get
			{
				return base.GetSystemString(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.GrandezaPeriodo);
			}
			
			set
			{
				base.SetSystemString(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.GrandezaPeriodo, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityParametrosWorkflowHistorico.Tipo
		/// </summary>
		virtual public System.String Tipo
		{
			get
			{
				return base.GetSystemString(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemString(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityParametrosWorkflowHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdParametrosWorkflow
			{
				get
				{
					System.Int32? data = entity.IdParametrosWorkflow;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdParametrosWorkflow = null;
					else entity.IdParametrosWorkflow = Convert.ToInt32(value);
				}
			}
				
			public System.String AtivaWorkFlow
			{
				get
				{
					System.String data = entity.AtivaWorkFlow;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AtivaWorkFlow = null;
					else entity.AtivaWorkFlow = Convert.ToString(value);
				}
			}
				
			public System.String VerificaEnquadramento
			{
				get
				{
					System.String data = entity.VerificaEnquadramento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VerificaEnquadramento = null;
					else entity.VerificaEnquadramento = Convert.ToString(value);
				}
			}
				
			public System.String ResponsavelNome
			{
				get
				{
					System.String data = entity.ResponsavelNome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResponsavelNome = null;
					else entity.ResponsavelNome = Convert.ToString(value);
				}
			}
				
			public System.String ResponsavelEmail
			{
				get
				{
					System.String data = entity.ResponsavelEmail;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResponsavelEmail = null;
					else entity.ResponsavelEmail = Convert.ToString(value);
				}
			}
				
			public System.String ValorPeriodo
			{
				get
				{
					System.Int32? data = entity.ValorPeriodo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPeriodo = null;
					else entity.ValorPeriodo = Convert.ToInt32(value);
				}
			}
				
			public System.String GrandezaPeriodo
			{
				get
				{
					System.String data = entity.GrandezaPeriodo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.GrandezaPeriodo = null;
					else entity.GrandezaPeriodo = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.String data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToString(value);
				}
			}
			

			private esSuitabilityParametrosWorkflowHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityParametrosWorkflowHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityParametrosWorkflowHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityParametrosWorkflowHistorico : esSuitabilityParametrosWorkflowHistorico
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityParametrosWorkflowHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityParametrosWorkflowHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdParametrosWorkflow
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.IdParametrosWorkflow, esSystemType.Int32);
			}
		} 
		
		public esQueryItem AtivaWorkFlow
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.AtivaWorkFlow, esSystemType.String);
			}
		} 
		
		public esQueryItem VerificaEnquadramento
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.VerificaEnquadramento, esSystemType.String);
			}
		} 
		
		public esQueryItem ResponsavelNome
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.ResponsavelNome, esSystemType.String);
			}
		} 
		
		public esQueryItem ResponsavelEmail
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.ResponsavelEmail, esSystemType.String);
			}
		} 
		
		public esQueryItem ValorPeriodo
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.ValorPeriodo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem GrandezaPeriodo
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.GrandezaPeriodo, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.Tipo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityParametrosWorkflowHistoricoCollection")]
	public partial class SuitabilityParametrosWorkflowHistoricoCollection : esSuitabilityParametrosWorkflowHistoricoCollection, IEnumerable<SuitabilityParametrosWorkflowHistorico>
	{
		public SuitabilityParametrosWorkflowHistoricoCollection()
		{

		}
		
		public static implicit operator List<SuitabilityParametrosWorkflowHistorico>(SuitabilityParametrosWorkflowHistoricoCollection coll)
		{
			List<SuitabilityParametrosWorkflowHistorico> list = new List<SuitabilityParametrosWorkflowHistorico>();
			
			foreach (SuitabilityParametrosWorkflowHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityParametrosWorkflowHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityParametrosWorkflowHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityParametrosWorkflowHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityParametrosWorkflowHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityParametrosWorkflowHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityParametrosWorkflowHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityParametrosWorkflowHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityParametrosWorkflowHistorico AddNew()
		{
			SuitabilityParametrosWorkflowHistorico entity = base.AddNewEntity() as SuitabilityParametrosWorkflowHistorico;
			
			return entity;
		}

		public SuitabilityParametrosWorkflowHistorico FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idParametrosWorkflow)
		{
			return base.FindByPrimaryKey(dataHistorico, idParametrosWorkflow) as SuitabilityParametrosWorkflowHistorico;
		}


		#region IEnumerable<SuitabilityParametrosWorkflowHistorico> Members

		IEnumerator<SuitabilityParametrosWorkflowHistorico> IEnumerable<SuitabilityParametrosWorkflowHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityParametrosWorkflowHistorico;
			}
		}

		#endregion
		
		private SuitabilityParametrosWorkflowHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityParametrosWorkflowHistorico' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityParametrosWorkflowHistorico : esSuitabilityParametrosWorkflowHistorico
	{
		public SuitabilityParametrosWorkflowHistorico()
		{

		}
	
		public SuitabilityParametrosWorkflowHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityParametrosWorkflowHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityParametrosWorkflowHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityParametrosWorkflowHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityParametrosWorkflowHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityParametrosWorkflowHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityParametrosWorkflowHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityParametrosWorkflowHistoricoQuery query;
	}



	[Serializable]
	public partial class SuitabilityParametrosWorkflowHistoricoQuery : esSuitabilityParametrosWorkflowHistoricoQuery
	{
		public SuitabilityParametrosWorkflowHistoricoQuery()
		{

		}		
		
		public SuitabilityParametrosWorkflowHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityParametrosWorkflowHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityParametrosWorkflowHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SuitabilityParametrosWorkflowHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.IdParametrosWorkflow, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityParametrosWorkflowHistoricoMetadata.PropertyNames.IdParametrosWorkflow;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((1))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.AtivaWorkFlow, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityParametrosWorkflowHistoricoMetadata.PropertyNames.AtivaWorkFlow;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.VerificaEnquadramento, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityParametrosWorkflowHistoricoMetadata.PropertyNames.VerificaEnquadramento;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.ResponsavelNome, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityParametrosWorkflowHistoricoMetadata.PropertyNames.ResponsavelNome;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.ResponsavelEmail, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityParametrosWorkflowHistoricoMetadata.PropertyNames.ResponsavelEmail;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.ValorPeriodo, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityParametrosWorkflowHistoricoMetadata.PropertyNames.ValorPeriodo;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.GrandezaPeriodo, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityParametrosWorkflowHistoricoMetadata.PropertyNames.GrandezaPeriodo;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityParametrosWorkflowHistoricoMetadata.ColumnNames.Tipo, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityParametrosWorkflowHistoricoMetadata.PropertyNames.Tipo;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityParametrosWorkflowHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdParametrosWorkflow = "IdParametrosWorkflow";
			 public const string AtivaWorkFlow = "AtivaWorkFlow";
			 public const string VerificaEnquadramento = "VerificaEnquadramento";
			 public const string ResponsavelNome = "ResponsavelNome";
			 public const string ResponsavelEmail = "ResponsavelEmail";
			 public const string ValorPeriodo = "ValorPeriodo";
			 public const string GrandezaPeriodo = "GrandezaPeriodo";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdParametrosWorkflow = "IdParametrosWorkflow";
			 public const string AtivaWorkFlow = "AtivaWorkFlow";
			 public const string VerificaEnquadramento = "VerificaEnquadramento";
			 public const string ResponsavelNome = "ResponsavelNome";
			 public const string ResponsavelEmail = "ResponsavelEmail";
			 public const string ValorPeriodo = "ValorPeriodo";
			 public const string GrandezaPeriodo = "GrandezaPeriodo";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityParametrosWorkflowHistoricoMetadata))
			{
				if(SuitabilityParametrosWorkflowHistoricoMetadata.mapDelegates == null)
				{
					SuitabilityParametrosWorkflowHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityParametrosWorkflowHistoricoMetadata.meta == null)
				{
					SuitabilityParametrosWorkflowHistoricoMetadata.meta = new SuitabilityParametrosWorkflowHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdParametrosWorkflow", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AtivaWorkFlow", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("VerificaEnquadramento", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ResponsavelNome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ResponsavelEmail", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ValorPeriodo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("GrandezaPeriodo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "SuitabilityParametrosWorkflowHistorico";
				meta.Destination = "SuitabilityParametrosWorkflowHistorico";
				
				meta.spInsert = "proc_SuitabilityParametrosWorkflowHistoricoInsert";				
				meta.spUpdate = "proc_SuitabilityParametrosWorkflowHistoricoUpdate";		
				meta.spDelete = "proc_SuitabilityParametrosWorkflowHistoricoDelete";
				meta.spLoadAll = "proc_SuitabilityParametrosWorkflowHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityParametrosWorkflowHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityParametrosWorkflowHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
