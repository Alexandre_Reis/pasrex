/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/09/2015 12:46:10
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityValidacaoCollection : esEntityCollection
	{
		public esSuitabilityValidacaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityValidacaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityValidacaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityValidacaoQuery);
		}
		#endregion
		
		virtual public SuitabilityValidacao DetachEntity(SuitabilityValidacao entity)
		{
			return base.DetachEntity(entity) as SuitabilityValidacao;
		}
		
		virtual public SuitabilityValidacao AttachEntity(SuitabilityValidacao entity)
		{
			return base.AttachEntity(entity) as SuitabilityValidacao;
		}
		
		virtual public void Combine(SuitabilityValidacaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityValidacao this[int index]
		{
			get
			{
				return base[index] as SuitabilityValidacao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityValidacao);
		}
	}



	[Serializable]
	abstract public class esSuitabilityValidacao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityValidacaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityValidacao()
		{

		}

		public esSuitabilityValidacao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idValidacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idValidacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idValidacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idValidacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idValidacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idValidacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idValidacao)
		{
			esSuitabilityValidacaoQuery query = this.GetDynamicQuery();
			query.Where(query.IdValidacao == idValidacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idValidacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdValidacao",idValidacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdValidacao": this.str.IdValidacao = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdValidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdValidacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityValidacao.IdValidacao
		/// </summary>
		virtual public System.Int32? IdValidacao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityValidacaoMetadata.ColumnNames.IdValidacao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityValidacaoMetadata.ColumnNames.IdValidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityValidacao.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(SuitabilityValidacaoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityValidacaoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityValidacao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdValidacao
			{
				get
				{
					System.Int32? data = entity.IdValidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdValidacao = null;
					else entity.IdValidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esSuitabilityValidacao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityValidacaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityValidacao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityValidacao : esSuitabilityValidacao
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityValidacaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityValidacaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdValidacao
		{
			get
			{
				return new esQueryItem(this, SuitabilityValidacaoMetadata.ColumnNames.IdValidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, SuitabilityValidacaoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityValidacaoCollection")]
	public partial class SuitabilityValidacaoCollection : esSuitabilityValidacaoCollection, IEnumerable<SuitabilityValidacao>
	{
		public SuitabilityValidacaoCollection()
		{

		}
		
		public static implicit operator List<SuitabilityValidacao>(SuitabilityValidacaoCollection coll)
		{
			List<SuitabilityValidacao> list = new List<SuitabilityValidacao>();
			
			foreach (SuitabilityValidacao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityValidacaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityValidacaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityValidacao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityValidacao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityValidacaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityValidacaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityValidacaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityValidacao AddNew()
		{
			SuitabilityValidacao entity = base.AddNewEntity() as SuitabilityValidacao;
			
			return entity;
		}

		public SuitabilityValidacao FindByPrimaryKey(System.Int32 idValidacao)
		{
			return base.FindByPrimaryKey(idValidacao) as SuitabilityValidacao;
		}


		#region IEnumerable<SuitabilityValidacao> Members

		IEnumerator<SuitabilityValidacao> IEnumerable<SuitabilityValidacao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityValidacao;
			}
		}

		#endregion
		
		private SuitabilityValidacaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityValidacao' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityValidacao : esSuitabilityValidacao
	{
		public SuitabilityValidacao()
		{

		}
	
		public SuitabilityValidacao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityValidacaoMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityValidacaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityValidacaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityValidacaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityValidacaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityValidacaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityValidacaoQuery query;
	}



	[Serializable]
	public partial class SuitabilityValidacaoQuery : esSuitabilityValidacaoQuery
	{
		public SuitabilityValidacaoQuery()
		{

		}		
		
		public SuitabilityValidacaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityValidacaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityValidacaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityValidacaoMetadata.ColumnNames.IdValidacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityValidacaoMetadata.PropertyNames.IdValidacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityValidacaoMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityValidacaoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityValidacaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdValidacao = "IdValidacao";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdValidacao = "IdValidacao";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityValidacaoMetadata))
			{
				if(SuitabilityValidacaoMetadata.mapDelegates == null)
				{
					SuitabilityValidacaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityValidacaoMetadata.meta == null)
				{
					SuitabilityValidacaoMetadata.meta = new SuitabilityValidacaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdValidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "SuitabilityValidacao";
				meta.Destination = "SuitabilityValidacao";
				
				meta.spInsert = "proc_SuitabilityValidacaoInsert";				
				meta.spUpdate = "proc_SuitabilityValidacaoUpdate";		
				meta.spDelete = "proc_SuitabilityValidacaoDelete";
				meta.spLoadAll = "proc_SuitabilityValidacaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityValidacaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityValidacaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
