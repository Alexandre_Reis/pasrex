/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/05/2015 12:56:46
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esClienteInterfaceCollection : esEntityCollection
	{
		public esClienteInterfaceCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ClienteInterfaceCollection";
		}

		#region Query Logic
		protected void InitQuery(esClienteInterfaceQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esClienteInterfaceQuery);
		}
		#endregion
		
		virtual public ClienteInterface DetachEntity(ClienteInterface entity)
		{
			return base.DetachEntity(entity) as ClienteInterface;
		}
		
		virtual public ClienteInterface AttachEntity(ClienteInterface entity)
		{
			return base.AttachEntity(entity) as ClienteInterface;
		}
		
		virtual public void Combine(ClienteInterfaceCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ClienteInterface this[int index]
		{
			get
			{
				return base[index] as ClienteInterface;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ClienteInterface);
		}
	}



	[Serializable]
	abstract public class esClienteInterface : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esClienteInterfaceQuery GetDynamicQuery()
		{
			return null;
		}

		public esClienteInterface()
		{

		}

		public esClienteInterface(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esClienteInterfaceQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente)
		{
			esClienteInterfaceQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CodigoCVM": this.str.CodigoCVM = (string)value; break;							
						case "CodigoGazeta": this.str.CodigoGazeta = (string)value; break;							
						case "CodigoSwift": this.str.CodigoSwift = (string)value; break;							
						case "CodigoRetencaoDIRF": this.str.CodigoRetencaoDIRF = (string)value; break;							
						case "RegistroBovespa": this.str.RegistroBovespa = (string)value; break;							
						case "CategoriaContabil": this.str.CategoriaContabil = (string)value; break;							
						case "CodigoContabil": this.str.CodigoContabil = (string)value; break;							
						case "CodigoYMF": this.str.CodigoYMF = (string)value; break;							
						case "InterfaceSinacorCC": this.str.InterfaceSinacorCC = (string)value; break;							
						case "CodigoIsin": this.str.CodigoIsin = (string)value; break;							
						case "CodigoCusip": this.str.CodigoCusip = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "CategoriaContabil":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CategoriaContabil = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ClienteInterface.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ClienteInterfaceMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(ClienteInterfaceMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteInterface.CodigoCVM
		/// </summary>
		virtual public System.String CodigoCVM
		{
			get
			{
				return base.GetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoCVM);
			}
			
			set
			{
				base.SetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoCVM, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteInterface.CodigoGazeta
		/// </summary>
		virtual public System.String CodigoGazeta
		{
			get
			{
				return base.GetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoGazeta);
			}
			
			set
			{
				base.SetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoGazeta, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteInterface.CodigoSwift
		/// </summary>
		virtual public System.String CodigoSwift
		{
			get
			{
				return base.GetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoSwift);
			}
			
			set
			{
				base.SetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoSwift, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteInterface.CodigoRetencaoDIRF
		/// </summary>
		virtual public System.String CodigoRetencaoDIRF
		{
			get
			{
				return base.GetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoRetencaoDIRF);
			}
			
			set
			{
				base.SetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoRetencaoDIRF, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteInterface.RegistroBovespa
		/// </summary>
		virtual public System.String RegistroBovespa
		{
			get
			{
				return base.GetSystemString(ClienteInterfaceMetadata.ColumnNames.RegistroBovespa);
			}
			
			set
			{
				base.SetSystemString(ClienteInterfaceMetadata.ColumnNames.RegistroBovespa, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteInterface.CategoriaContabil
		/// </summary>
		virtual public System.Int32? CategoriaContabil
		{
			get
			{
				return base.GetSystemInt32(ClienteInterfaceMetadata.ColumnNames.CategoriaContabil);
			}
			
			set
			{
				base.SetSystemInt32(ClienteInterfaceMetadata.ColumnNames.CategoriaContabil, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteInterface.CodigoContabil
		/// </summary>
		virtual public System.String CodigoContabil
		{
			get
			{
				return base.GetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoContabil);
			}
			
			set
			{
				base.SetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoContabil, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteInterface.CodigoYMF
		/// </summary>
		virtual public System.String CodigoYMF
		{
			get
			{
				return base.GetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoYMF);
			}
			
			set
			{
				base.SetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoYMF, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteInterface.InterfaceSinacorCC
		/// </summary>
		virtual public System.String InterfaceSinacorCC
		{
			get
			{
				return base.GetSystemString(ClienteInterfaceMetadata.ColumnNames.InterfaceSinacorCC);
			}
			
			set
			{
				base.SetSystemString(ClienteInterfaceMetadata.ColumnNames.InterfaceSinacorCC, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteInterface.CodigoIsin
		/// </summary>
		virtual public System.String CodigoIsin
		{
			get
			{
				return base.GetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoIsin);
			}
			
			set
			{
				base.SetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoIsin, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteInterface.CodigoCusip
		/// </summary>
		virtual public System.String CodigoCusip
		{
			get
			{
				return base.GetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoCusip);
			}
			
			set
			{
				base.SetSystemString(ClienteInterfaceMetadata.ColumnNames.CodigoCusip, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esClienteInterface entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoCVM
			{
				get
				{
					System.String data = entity.CodigoCVM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCVM = null;
					else entity.CodigoCVM = Convert.ToString(value);
				}
			}
				
			public System.String CodigoGazeta
			{
				get
				{
					System.String data = entity.CodigoGazeta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoGazeta = null;
					else entity.CodigoGazeta = Convert.ToString(value);
				}
			}
				
			public System.String CodigoSwift
			{
				get
				{
					System.String data = entity.CodigoSwift;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoSwift = null;
					else entity.CodigoSwift = Convert.ToString(value);
				}
			}
				
			public System.String CodigoRetencaoDIRF
			{
				get
				{
					System.String data = entity.CodigoRetencaoDIRF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoRetencaoDIRF = null;
					else entity.CodigoRetencaoDIRF = Convert.ToString(value);
				}
			}
				
			public System.String RegistroBovespa
			{
				get
				{
					System.String data = entity.RegistroBovespa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RegistroBovespa = null;
					else entity.RegistroBovespa = Convert.ToString(value);
				}
			}
				
			public System.String CategoriaContabil
			{
				get
				{
					System.Int32? data = entity.CategoriaContabil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CategoriaContabil = null;
					else entity.CategoriaContabil = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoContabil
			{
				get
				{
					System.String data = entity.CodigoContabil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoContabil = null;
					else entity.CodigoContabil = Convert.ToString(value);
				}
			}
				
			public System.String CodigoYMF
			{
				get
				{
					System.String data = entity.CodigoYMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoYMF = null;
					else entity.CodigoYMF = Convert.ToString(value);
				}
			}
				
			public System.String InterfaceSinacorCC
			{
				get
				{
					System.String data = entity.InterfaceSinacorCC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InterfaceSinacorCC = null;
					else entity.InterfaceSinacorCC = Convert.ToString(value);
				}
			}
				
			public System.String CodigoIsin
			{
				get
				{
					System.String data = entity.CodigoIsin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoIsin = null;
					else entity.CodigoIsin = Convert.ToString(value);
				}
			}
				
			public System.String CodigoCusip
			{
				get
				{
					System.String data = entity.CodigoCusip;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCusip = null;
					else entity.CodigoCusip = Convert.ToString(value);
				}
			}
			

			private esClienteInterface entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esClienteInterfaceQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esClienteInterface can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ClienteInterface : esClienteInterface
	{

		#region UpToCliente - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Cliente_ClienteInterface_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToCliente
		{
			get
			{
				if(this._UpToCliente == null
					&& IdCliente != null					)
				{
					this._UpToCliente = new Cliente();
					this._UpToCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCliente", this._UpToCliente);
					this._UpToCliente.Query.Where(this._UpToCliente.Query.IdCliente == this.IdCliente);
					this._UpToCliente.Query.Load();
				}

				return this._UpToCliente;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToCliente");

				if(value == null)
				{
					this._UpToCliente = null;
				}
				else
				{
					this._UpToCliente = value;
					this.SetPreSave("UpToCliente", this._UpToCliente);
				}
				
				
			} 
		}

		private Cliente _UpToCliente;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esClienteInterfaceQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ClienteInterfaceMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ClienteInterfaceMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoCVM
		{
			get
			{
				return new esQueryItem(this, ClienteInterfaceMetadata.ColumnNames.CodigoCVM, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoGazeta
		{
			get
			{
				return new esQueryItem(this, ClienteInterfaceMetadata.ColumnNames.CodigoGazeta, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoSwift
		{
			get
			{
				return new esQueryItem(this, ClienteInterfaceMetadata.ColumnNames.CodigoSwift, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoRetencaoDIRF
		{
			get
			{
				return new esQueryItem(this, ClienteInterfaceMetadata.ColumnNames.CodigoRetencaoDIRF, esSystemType.String);
			}
		} 
		
		public esQueryItem RegistroBovespa
		{
			get
			{
				return new esQueryItem(this, ClienteInterfaceMetadata.ColumnNames.RegistroBovespa, esSystemType.String);
			}
		} 
		
		public esQueryItem CategoriaContabil
		{
			get
			{
				return new esQueryItem(this, ClienteInterfaceMetadata.ColumnNames.CategoriaContabil, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoContabil
		{
			get
			{
				return new esQueryItem(this, ClienteInterfaceMetadata.ColumnNames.CodigoContabil, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoYMF
		{
			get
			{
				return new esQueryItem(this, ClienteInterfaceMetadata.ColumnNames.CodigoYMF, esSystemType.String);
			}
		} 
		
		public esQueryItem InterfaceSinacorCC
		{
			get
			{
				return new esQueryItem(this, ClienteInterfaceMetadata.ColumnNames.InterfaceSinacorCC, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoIsin
		{
			get
			{
				return new esQueryItem(this, ClienteInterfaceMetadata.ColumnNames.CodigoIsin, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoCusip
		{
			get
			{
				return new esQueryItem(this, ClienteInterfaceMetadata.ColumnNames.CodigoCusip, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ClienteInterfaceCollection")]
	public partial class ClienteInterfaceCollection : esClienteInterfaceCollection, IEnumerable<ClienteInterface>
	{
		public ClienteInterfaceCollection()
		{

		}
		
		public static implicit operator List<ClienteInterface>(ClienteInterfaceCollection coll)
		{
			List<ClienteInterface> list = new List<ClienteInterface>();
			
			foreach (ClienteInterface emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ClienteInterfaceMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClienteInterfaceQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ClienteInterface(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ClienteInterface();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ClienteInterfaceQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClienteInterfaceQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ClienteInterfaceQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ClienteInterface AddNew()
		{
			ClienteInterface entity = base.AddNewEntity() as ClienteInterface;
			
			return entity;
		}

		public ClienteInterface FindByPrimaryKey(System.Int32 idCliente)
		{
			return base.FindByPrimaryKey(idCliente) as ClienteInterface;
		}


		#region IEnumerable<ClienteInterface> Members

		IEnumerator<ClienteInterface> IEnumerable<ClienteInterface>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ClienteInterface;
			}
		}

		#endregion
		
		private ClienteInterfaceQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ClienteInterface' table
	/// </summary>

	[Serializable]
	public partial class ClienteInterface : esClienteInterface
	{
		public ClienteInterface()
		{

		}
	
		public ClienteInterface(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ClienteInterfaceMetadata.Meta();
			}
		}
		
		
		
		override protected esClienteInterfaceQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClienteInterfaceQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ClienteInterfaceQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClienteInterfaceQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ClienteInterfaceQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ClienteInterfaceQuery query;
	}



	[Serializable]
	public partial class ClienteInterfaceQuery : esClienteInterfaceQuery
	{
		public ClienteInterfaceQuery()
		{

		}		
		
		public ClienteInterfaceQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ClienteInterfaceMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ClienteInterfaceMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ClienteInterfaceMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteInterfaceMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteInterfaceMetadata.ColumnNames.CodigoCVM, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteInterfaceMetadata.PropertyNames.CodigoCVM;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteInterfaceMetadata.ColumnNames.CodigoGazeta, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteInterfaceMetadata.PropertyNames.CodigoGazeta;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteInterfaceMetadata.ColumnNames.CodigoSwift, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteInterfaceMetadata.PropertyNames.CodigoSwift;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteInterfaceMetadata.ColumnNames.CodigoRetencaoDIRF, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteInterfaceMetadata.PropertyNames.CodigoRetencaoDIRF;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteInterfaceMetadata.ColumnNames.RegistroBovespa, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteInterfaceMetadata.PropertyNames.RegistroBovespa;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteInterfaceMetadata.ColumnNames.CategoriaContabil, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteInterfaceMetadata.PropertyNames.CategoriaContabil;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteInterfaceMetadata.ColumnNames.CodigoContabil, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteInterfaceMetadata.PropertyNames.CodigoContabil;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteInterfaceMetadata.ColumnNames.CodigoYMF, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteInterfaceMetadata.PropertyNames.CodigoYMF;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteInterfaceMetadata.ColumnNames.InterfaceSinacorCC, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteInterfaceMetadata.PropertyNames.InterfaceSinacorCC;
			c.CharacterMaxLength = 2000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteInterfaceMetadata.ColumnNames.CodigoIsin, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteInterfaceMetadata.PropertyNames.CodigoIsin;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteInterfaceMetadata.ColumnNames.CodigoCusip, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteInterfaceMetadata.PropertyNames.CodigoCusip;
			c.CharacterMaxLength = 9;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ClienteInterfaceMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string CodigoCVM = "CodigoCVM";
			 public const string CodigoGazeta = "CodigoGazeta";
			 public const string CodigoSwift = "CodigoSwift";
			 public const string CodigoRetencaoDIRF = "CodigoRetencaoDIRF";
			 public const string RegistroBovespa = "RegistroBovespa";
			 public const string CategoriaContabil = "CategoriaContabil";
			 public const string CodigoContabil = "CodigoContabil";
			 public const string CodigoYMF = "CodigoYMF";
			 public const string InterfaceSinacorCC = "InterfaceSinacorCC";
			 public const string CodigoIsin = "CodigoIsin";
			 public const string CodigoCusip = "CodigoCusip";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string CodigoCVM = "CodigoCVM";
			 public const string CodigoGazeta = "CodigoGazeta";
			 public const string CodigoSwift = "CodigoSwift";
			 public const string CodigoRetencaoDIRF = "CodigoRetencaoDIRF";
			 public const string RegistroBovespa = "RegistroBovespa";
			 public const string CategoriaContabil = "CategoriaContabil";
			 public const string CodigoContabil = "CodigoContabil";
			 public const string CodigoYMF = "CodigoYMF";
			 public const string InterfaceSinacorCC = "InterfaceSinacorCC";
			 public const string CodigoIsin = "CodigoIsin";
			 public const string CodigoCusip = "CodigoCusip";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ClienteInterfaceMetadata))
			{
				if(ClienteInterfaceMetadata.mapDelegates == null)
				{
					ClienteInterfaceMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ClienteInterfaceMetadata.meta == null)
				{
					ClienteInterfaceMetadata.meta = new ClienteInterfaceMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoCVM", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoGazeta", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoSwift", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoRetencaoDIRF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("RegistroBovespa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CategoriaContabil", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoContabil", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoYMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("InterfaceSinacorCC", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoIsin", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoCusip", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "ClienteInterface";
				meta.Destination = "ClienteInterface";
				
				meta.spInsert = "proc_ClienteInterfaceInsert";				
				meta.spUpdate = "proc_ClienteInterfaceUpdate";		
				meta.spDelete = "proc_ClienteInterfaceDelete";
				meta.spLoadAll = "proc_ClienteInterfaceLoadAll";
				meta.spLoadByPrimaryKey = "proc_ClienteInterfaceLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ClienteInterfaceMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
