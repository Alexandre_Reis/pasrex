/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/09/2015 12:18:30
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.InvestidorCotista;

namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityLogMensagensCollection : esEntityCollection
	{
		public esSuitabilityLogMensagensCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityLogMensagensCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityLogMensagensQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityLogMensagensQuery);
		}
		#endregion
		
		virtual public SuitabilityLogMensagens DetachEntity(SuitabilityLogMensagens entity)
		{
			return base.DetachEntity(entity) as SuitabilityLogMensagens;
		}
		
		virtual public SuitabilityLogMensagens AttachEntity(SuitabilityLogMensagens entity)
		{
			return base.AttachEntity(entity) as SuitabilityLogMensagens;
		}
		
		virtual public void Combine(SuitabilityLogMensagensCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityLogMensagens this[int index]
		{
			get
			{
				return base[index] as SuitabilityLogMensagens;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityLogMensagens);
		}
	}



	[Serializable]
	abstract public class esSuitabilityLogMensagens : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityLogMensagensQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityLogMensagens()
		{

		}

		public esSuitabilityLogMensagens(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idLogMensagem)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLogMensagem);
			else
				return LoadByPrimaryKeyStoredProcedure(idLogMensagem);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLogMensagem)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLogMensagem);
			else
				return LoadByPrimaryKeyStoredProcedure(idLogMensagem);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idLogMensagem)
		{
			esSuitabilityLogMensagensQuery query = this.GetDynamicQuery();
			query.Where(query.IdLogMensagem == idLogMensagem);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLogMensagem)
		{
			esParameters parms = new esParameters();
			parms.Add("IdLogMensagem",idLogMensagem);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLogMensagem": this.str.IdLogMensagem = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Usuario": this.str.Usuario = (string)value; break;							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "IdMensagem": this.str.IdMensagem = (string)value; break;							
						case "Mensagem": this.str.Mensagem = (string)value; break;							
						case "Resposta": this.str.Resposta = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLogMensagem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLogMensagem = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
						
						case "IdMensagem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMensagem = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityLogMensagens.IdLogMensagem
		/// </summary>
		virtual public System.Int32? IdLogMensagem
		{
			get
			{
				return base.GetSystemInt32(SuitabilityLogMensagensMetadata.ColumnNames.IdLogMensagem);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityLogMensagensMetadata.ColumnNames.IdLogMensagem, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityLogMensagens.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(SuitabilityLogMensagensMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(SuitabilityLogMensagensMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityLogMensagens.Usuario
		/// </summary>
		virtual public System.String Usuario
		{
			get
			{
				return base.GetSystemString(SuitabilityLogMensagensMetadata.ColumnNames.Usuario);
			}
			
			set
			{
				base.SetSystemString(SuitabilityLogMensagensMetadata.ColumnNames.Usuario, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityLogMensagens.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(SuitabilityLogMensagensMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				if(base.SetSystemInt32(SuitabilityLogMensagensMetadata.ColumnNames.IdCotista, value))
				{
					this._UpToCotistaByIdCotista = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityLogMensagens.IdMensagem
		/// </summary>
		virtual public System.Int32? IdMensagem
		{
			get
			{
				return base.GetSystemInt32(SuitabilityLogMensagensMetadata.ColumnNames.IdMensagem);
			}
			
			set
			{
				if(base.SetSystemInt32(SuitabilityLogMensagensMetadata.ColumnNames.IdMensagem, value))
				{
					this._UpToSuitabilityMensagensByIdMensagem = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityLogMensagens.Mensagem
		/// </summary>
		virtual public System.String Mensagem
		{
			get
			{
				return base.GetSystemString(SuitabilityLogMensagensMetadata.ColumnNames.Mensagem);
			}
			
			set
			{
				base.SetSystemString(SuitabilityLogMensagensMetadata.ColumnNames.Mensagem, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityLogMensagens.Resposta
		/// </summary>
		virtual public System.String Resposta
		{
			get
			{
				return base.GetSystemString(SuitabilityLogMensagensMetadata.ColumnNames.Resposta);
			}
			
			set
			{
				base.SetSystemString(SuitabilityLogMensagensMetadata.ColumnNames.Resposta, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cotista _UpToCotistaByIdCotista;
		[CLSCompliant(false)]
		internal protected SuitabilityMensagens _UpToSuitabilityMensagensByIdMensagem;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityLogMensagens entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLogMensagem
			{
				get
				{
					System.Int32? data = entity.IdLogMensagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLogMensagem = null;
					else entity.IdLogMensagem = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Usuario
			{
				get
				{
					System.String data = entity.Usuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Usuario = null;
					else entity.Usuario = Convert.ToString(value);
				}
			}
				
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String IdMensagem
			{
				get
				{
					System.Int32? data = entity.IdMensagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMensagem = null;
					else entity.IdMensagem = Convert.ToInt32(value);
				}
			}
				
			public System.String Mensagem
			{
				get
				{
					System.String data = entity.Mensagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Mensagem = null;
					else entity.Mensagem = Convert.ToString(value);
				}
			}
				
			public System.String Resposta
			{
				get
				{
					System.String data = entity.Resposta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Resposta = null;
					else entity.Resposta = Convert.ToString(value);
				}
			}
			

			private esSuitabilityLogMensagens entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityLogMensagensQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityLogMensagens can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityLogMensagens : esSuitabilityLogMensagens
	{

				
		#region UpToCotistaByIdCotista - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - SuitabilityLogMensagens_Cotista_FK
		/// </summary>

		[XmlIgnore]
		public Cotista UpToCotistaByIdCotista
		{
			get
			{
				if(this._UpToCotistaByIdCotista == null
					&& IdCotista != null					)
				{
					this._UpToCotistaByIdCotista = new Cotista();
					this._UpToCotistaByIdCotista.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
					this._UpToCotistaByIdCotista.Query.Where(this._UpToCotistaByIdCotista.Query.IdCotista == this.IdCotista);
					this._UpToCotistaByIdCotista.Query.Load();
				}

				return this._UpToCotistaByIdCotista;
			}
			
			set
			{
				this.RemovePreSave("UpToCotistaByIdCotista");
				

				if(value == null)
				{
					this.IdCotista = null;
					this._UpToCotistaByIdCotista = null;
				}
				else
				{
					this.IdCotista = value.IdCotista;
					this._UpToCotistaByIdCotista = value;
					this.SetPreSave("UpToCotistaByIdCotista", this._UpToCotistaByIdCotista);
				}
				
			}
		}
		#endregion
		

				
		#region UpToSuitabilityMensagensByIdMensagem - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - SuitabilityLogMensagens_SuitabilityMensagens_FK
		/// </summary>

		[XmlIgnore]
		public SuitabilityMensagens UpToSuitabilityMensagensByIdMensagem
		{
			get
			{
				if(this._UpToSuitabilityMensagensByIdMensagem == null
					&& IdMensagem != null					)
				{
					this._UpToSuitabilityMensagensByIdMensagem = new SuitabilityMensagens();
					this._UpToSuitabilityMensagensByIdMensagem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSuitabilityMensagensByIdMensagem", this._UpToSuitabilityMensagensByIdMensagem);
					this._UpToSuitabilityMensagensByIdMensagem.Query.Where(this._UpToSuitabilityMensagensByIdMensagem.Query.IdMensagem == this.IdMensagem);
					this._UpToSuitabilityMensagensByIdMensagem.Query.Load();
				}

				return this._UpToSuitabilityMensagensByIdMensagem;
			}
			
			set
			{
				this.RemovePreSave("UpToSuitabilityMensagensByIdMensagem");
				

				if(value == null)
				{
					this.IdMensagem = null;
					this._UpToSuitabilityMensagensByIdMensagem = null;
				}
				else
				{
					this.IdMensagem = value.IdMensagem;
					this._UpToSuitabilityMensagensByIdMensagem = value;
					this.SetPreSave("UpToSuitabilityMensagensByIdMensagem", this._UpToSuitabilityMensagensByIdMensagem);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToSuitabilityMensagensByIdMensagem != null)
			{
				this.IdMensagem = this._UpToSuitabilityMensagensByIdMensagem.IdMensagem;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityLogMensagensQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityLogMensagensMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLogMensagem
		{
			get
			{
				return new esQueryItem(this, SuitabilityLogMensagensMetadata.ColumnNames.IdLogMensagem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, SuitabilityLogMensagensMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Usuario
		{
			get
			{
				return new esQueryItem(this, SuitabilityLogMensagensMetadata.ColumnNames.Usuario, esSystemType.String);
			}
		} 
		
		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, SuitabilityLogMensagensMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdMensagem
		{
			get
			{
				return new esQueryItem(this, SuitabilityLogMensagensMetadata.ColumnNames.IdMensagem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Mensagem
		{
			get
			{
				return new esQueryItem(this, SuitabilityLogMensagensMetadata.ColumnNames.Mensagem, esSystemType.String);
			}
		} 
		
		public esQueryItem Resposta
		{
			get
			{
				return new esQueryItem(this, SuitabilityLogMensagensMetadata.ColumnNames.Resposta, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityLogMensagensCollection")]
	public partial class SuitabilityLogMensagensCollection : esSuitabilityLogMensagensCollection, IEnumerable<SuitabilityLogMensagens>
	{
		public SuitabilityLogMensagensCollection()
		{

		}
		
		public static implicit operator List<SuitabilityLogMensagens>(SuitabilityLogMensagensCollection coll)
		{
			List<SuitabilityLogMensagens> list = new List<SuitabilityLogMensagens>();
			
			foreach (SuitabilityLogMensagens emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityLogMensagensMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityLogMensagensQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityLogMensagens(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityLogMensagens();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityLogMensagensQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityLogMensagensQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityLogMensagensQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityLogMensagens AddNew()
		{
			SuitabilityLogMensagens entity = base.AddNewEntity() as SuitabilityLogMensagens;
			
			return entity;
		}

		public SuitabilityLogMensagens FindByPrimaryKey(System.Int32 idLogMensagem)
		{
			return base.FindByPrimaryKey(idLogMensagem) as SuitabilityLogMensagens;
		}


		#region IEnumerable<SuitabilityLogMensagens> Members

		IEnumerator<SuitabilityLogMensagens> IEnumerable<SuitabilityLogMensagens>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityLogMensagens;
			}
		}

		#endregion
		
		private SuitabilityLogMensagensQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityLogMensagens' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityLogMensagens : esSuitabilityLogMensagens
	{
		public SuitabilityLogMensagens()
		{

		}
	
		public SuitabilityLogMensagens(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityLogMensagensMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityLogMensagensQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityLogMensagensQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityLogMensagensQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityLogMensagensQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityLogMensagensQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityLogMensagensQuery query;
	}



	[Serializable]
	public partial class SuitabilityLogMensagensQuery : esSuitabilityLogMensagensQuery
	{
		public SuitabilityLogMensagensQuery()
		{

		}		
		
		public SuitabilityLogMensagensQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityLogMensagensMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityLogMensagensMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityLogMensagensMetadata.ColumnNames.IdLogMensagem, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityLogMensagensMetadata.PropertyNames.IdLogMensagem;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityLogMensagensMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SuitabilityLogMensagensMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityLogMensagensMetadata.ColumnNames.Usuario, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityLogMensagensMetadata.PropertyNames.Usuario;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityLogMensagensMetadata.ColumnNames.IdCotista, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityLogMensagensMetadata.PropertyNames.IdCotista;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityLogMensagensMetadata.ColumnNames.IdMensagem, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityLogMensagensMetadata.PropertyNames.IdMensagem;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityLogMensagensMetadata.ColumnNames.Mensagem, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityLogMensagensMetadata.PropertyNames.Mensagem;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityLogMensagensMetadata.ColumnNames.Resposta, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityLogMensagensMetadata.PropertyNames.Resposta;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityLogMensagensMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLogMensagem = "IdLogMensagem";
			 public const string Data = "Data";
			 public const string Usuario = "Usuario";
			 public const string IdCotista = "IdCotista";
			 public const string IdMensagem = "IdMensagem";
			 public const string Mensagem = "Mensagem";
			 public const string Resposta = "Resposta";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLogMensagem = "IdLogMensagem";
			 public const string Data = "Data";
			 public const string Usuario = "Usuario";
			 public const string IdCotista = "IdCotista";
			 public const string IdMensagem = "IdMensagem";
			 public const string Mensagem = "Mensagem";
			 public const string Resposta = "Resposta";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityLogMensagensMetadata))
			{
				if(SuitabilityLogMensagensMetadata.mapDelegates == null)
				{
					SuitabilityLogMensagensMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityLogMensagensMetadata.meta == null)
				{
					SuitabilityLogMensagensMetadata.meta = new SuitabilityLogMensagensMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdLogMensagem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Usuario", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdMensagem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Mensagem", new esTypeMap("text", "System.String"));
				meta.AddTypeMap("Resposta", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "SuitabilityLogMensagens";
				meta.Destination = "SuitabilityLogMensagens";
				
				meta.spInsert = "proc_SuitabilityLogMensagensInsert";				
				meta.spUpdate = "proc_SuitabilityLogMensagensUpdate";		
				meta.spDelete = "proc_SuitabilityLogMensagensDelete";
				meta.spLoadAll = "proc_SuitabilityLogMensagensLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityLogMensagensLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityLogMensagensMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
