/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 27/08/2015 12:37:46
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Investidor
{

	[Serializable]
	abstract public class esSuitabilityPerfilHistoricoCollection : esEntityCollection
	{
		public esSuitabilityPerfilHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SuitabilityPerfilHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esSuitabilityPerfilHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSuitabilityPerfilHistoricoQuery);
		}
		#endregion
		
		virtual public SuitabilityPerfilHistorico DetachEntity(SuitabilityPerfilHistorico entity)
		{
			return base.DetachEntity(entity) as SuitabilityPerfilHistorico;
		}
		
		virtual public SuitabilityPerfilHistorico AttachEntity(SuitabilityPerfilHistorico entity)
		{
			return base.AttachEntity(entity) as SuitabilityPerfilHistorico;
		}
		
		virtual public void Combine(SuitabilityPerfilHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SuitabilityPerfilHistorico this[int index]
		{
			get
			{
				return base[index] as SuitabilityPerfilHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SuitabilityPerfilHistorico);
		}
	}



	[Serializable]
	abstract public class esSuitabilityPerfilHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSuitabilityPerfilHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esSuitabilityPerfilHistorico()
		{

		}

		public esSuitabilityPerfilHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Byte idPerfilInvestidor)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idPerfilInvestidor);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idPerfilInvestidor);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Byte idPerfilInvestidor)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idPerfilInvestidor);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idPerfilInvestidor);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Byte idPerfilInvestidor)
		{
			esSuitabilityPerfilHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdPerfilInvestidor == idPerfilInvestidor);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Byte idPerfilInvestidor)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdPerfilInvestidor",idPerfilInvestidor);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdPerfilInvestidor": this.str.IdPerfilInvestidor = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Pontos": this.str.Pontos = (string)value; break;							
						case "TipoCliente": this.str.TipoCliente = (string)value; break;							
						case "PerfilInvestidor": this.str.PerfilInvestidor = (string)value; break;							
						case "Definicao": this.str.Definicao = (string)value; break;							
						case "FaixaPontuacao": this.str.FaixaPontuacao = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdPerfilInvestidor":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdPerfilInvestidor = (System.Byte?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Pontos":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Pontos = (System.Int32?)value;
							break;
						
						case "TipoCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoCliente = (System.Int32?)value;
							break;
						
						case "PerfilInvestidor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PerfilInvestidor = (System.Int32?)value;
							break;
						
						case "FaixaPontuacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FaixaPontuacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SuitabilityPerfilHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(SuitabilityPerfilHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(SuitabilityPerfilHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfilHistorico.IdPerfilInvestidor
		/// </summary>
		virtual public System.Byte? IdPerfilInvestidor
		{
			get
			{
				return base.GetSystemByte(SuitabilityPerfilHistoricoMetadata.ColumnNames.IdPerfilInvestidor);
			}
			
			set
			{
				base.SetSystemByte(SuitabilityPerfilHistoricoMetadata.ColumnNames.IdPerfilInvestidor, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfilHistorico.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(SuitabilityPerfilHistoricoMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(SuitabilityPerfilHistoricoMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfilHistorico.Pontos
		/// </summary>
		virtual public System.Int32? Pontos
		{
			get
			{
				return base.GetSystemInt32(SuitabilityPerfilHistoricoMetadata.ColumnNames.Pontos);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityPerfilHistoricoMetadata.ColumnNames.Pontos, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfilHistorico.TipoCliente
		/// </summary>
		virtual public System.Int32? TipoCliente
		{
			get
			{
				return base.GetSystemInt32(SuitabilityPerfilHistoricoMetadata.ColumnNames.TipoCliente);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityPerfilHistoricoMetadata.ColumnNames.TipoCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfilHistorico.PerfilInvestidor
		/// </summary>
		virtual public System.Int32? PerfilInvestidor
		{
			get
			{
				return base.GetSystemInt32(SuitabilityPerfilHistoricoMetadata.ColumnNames.PerfilInvestidor);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityPerfilHistoricoMetadata.ColumnNames.PerfilInvestidor, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfilHistorico.Definicao
		/// </summary>
		virtual public System.String Definicao
		{
			get
			{
				return base.GetSystemString(SuitabilityPerfilHistoricoMetadata.ColumnNames.Definicao);
			}
			
			set
			{
				base.SetSystemString(SuitabilityPerfilHistoricoMetadata.ColumnNames.Definicao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfilHistorico.FaixaPontuacao
		/// </summary>
		virtual public System.Int32? FaixaPontuacao
		{
			get
			{
				return base.GetSystemInt32(SuitabilityPerfilHistoricoMetadata.ColumnNames.FaixaPontuacao);
			}
			
			set
			{
				base.SetSystemInt32(SuitabilityPerfilHistoricoMetadata.ColumnNames.FaixaPontuacao, value);
			}
		}
		
		/// <summary>
		/// Maps to SuitabilityPerfilHistorico.Tipo
		/// </summary>
		virtual public System.String Tipo
		{
			get
			{
				return base.GetSystemString(SuitabilityPerfilHistoricoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemString(SuitabilityPerfilHistoricoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSuitabilityPerfilHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdPerfilInvestidor
			{
				get
				{
					System.Byte? data = entity.IdPerfilInvestidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilInvestidor = null;
					else entity.IdPerfilInvestidor = Convert.ToByte(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Pontos
			{
				get
				{
					System.Int32? data = entity.Pontos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pontos = null;
					else entity.Pontos = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoCliente
			{
				get
				{
					System.Int32? data = entity.TipoCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCliente = null;
					else entity.TipoCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String PerfilInvestidor
			{
				get
				{
					System.Int32? data = entity.PerfilInvestidor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerfilInvestidor = null;
					else entity.PerfilInvestidor = Convert.ToInt32(value);
				}
			}
				
			public System.String Definicao
			{
				get
				{
					System.String data = entity.Definicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Definicao = null;
					else entity.Definicao = Convert.ToString(value);
				}
			}
				
			public System.String FaixaPontuacao
			{
				get
				{
					System.Int32? data = entity.FaixaPontuacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FaixaPontuacao = null;
					else entity.FaixaPontuacao = Convert.ToInt32(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.String data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToString(value);
				}
			}
			

			private esSuitabilityPerfilHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSuitabilityPerfilHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSuitabilityPerfilHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SuitabilityPerfilHistorico : esSuitabilityPerfilHistorico
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSuitabilityPerfilHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityPerfilHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdPerfilInvestidor
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilHistoricoMetadata.ColumnNames.IdPerfilInvestidor, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilHistoricoMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Pontos
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilHistoricoMetadata.ColumnNames.Pontos, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoCliente
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilHistoricoMetadata.ColumnNames.TipoCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PerfilInvestidor
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilHistoricoMetadata.ColumnNames.PerfilInvestidor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Definicao
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilHistoricoMetadata.ColumnNames.Definicao, esSystemType.String);
			}
		} 
		
		public esQueryItem FaixaPontuacao
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilHistoricoMetadata.ColumnNames.FaixaPontuacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, SuitabilityPerfilHistoricoMetadata.ColumnNames.Tipo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SuitabilityPerfilHistoricoCollection")]
	public partial class SuitabilityPerfilHistoricoCollection : esSuitabilityPerfilHistoricoCollection, IEnumerable<SuitabilityPerfilHistorico>
	{
		public SuitabilityPerfilHistoricoCollection()
		{

		}
		
		public static implicit operator List<SuitabilityPerfilHistorico>(SuitabilityPerfilHistoricoCollection coll)
		{
			List<SuitabilityPerfilHistorico> list = new List<SuitabilityPerfilHistorico>();
			
			foreach (SuitabilityPerfilHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SuitabilityPerfilHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityPerfilHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SuitabilityPerfilHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SuitabilityPerfilHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SuitabilityPerfilHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityPerfilHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SuitabilityPerfilHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SuitabilityPerfilHistorico AddNew()
		{
			SuitabilityPerfilHistorico entity = base.AddNewEntity() as SuitabilityPerfilHistorico;
			
			return entity;
		}

		public SuitabilityPerfilHistorico FindByPrimaryKey(System.DateTime dataHistorico, System.Byte idPerfilInvestidor)
		{
			return base.FindByPrimaryKey(dataHistorico, idPerfilInvestidor) as SuitabilityPerfilHistorico;
		}


		#region IEnumerable<SuitabilityPerfilHistorico> Members

		IEnumerator<SuitabilityPerfilHistorico> IEnumerable<SuitabilityPerfilHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SuitabilityPerfilHistorico;
			}
		}

		#endregion
		
		private SuitabilityPerfilHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SuitabilityPerfilHistorico' table
	/// </summary>

	[Serializable]
	public partial class SuitabilityPerfilHistorico : esSuitabilityPerfilHistorico
	{
		public SuitabilityPerfilHistorico()
		{

		}
	
		public SuitabilityPerfilHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SuitabilityPerfilHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esSuitabilityPerfilHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SuitabilityPerfilHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SuitabilityPerfilHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SuitabilityPerfilHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SuitabilityPerfilHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SuitabilityPerfilHistoricoQuery query;
	}



	[Serializable]
	public partial class SuitabilityPerfilHistoricoQuery : esSuitabilityPerfilHistoricoQuery
	{
		public SuitabilityPerfilHistoricoQuery()
		{

		}		
		
		public SuitabilityPerfilHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SuitabilityPerfilHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SuitabilityPerfilHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SuitabilityPerfilHistoricoMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SuitabilityPerfilHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilHistoricoMetadata.ColumnNames.IdPerfilInvestidor, 1, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = SuitabilityPerfilHistoricoMetadata.PropertyNames.IdPerfilInvestidor;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilHistoricoMetadata.ColumnNames.Data, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SuitabilityPerfilHistoricoMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilHistoricoMetadata.ColumnNames.Pontos, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityPerfilHistoricoMetadata.PropertyNames.Pontos;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilHistoricoMetadata.ColumnNames.TipoCliente, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityPerfilHistoricoMetadata.PropertyNames.TipoCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilHistoricoMetadata.ColumnNames.PerfilInvestidor, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityPerfilHistoricoMetadata.PropertyNames.PerfilInvestidor;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilHistoricoMetadata.ColumnNames.Definicao, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityPerfilHistoricoMetadata.PropertyNames.Definicao;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilHistoricoMetadata.ColumnNames.FaixaPontuacao, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SuitabilityPerfilHistoricoMetadata.PropertyNames.FaixaPontuacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SuitabilityPerfilHistoricoMetadata.ColumnNames.Tipo, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = SuitabilityPerfilHistoricoMetadata.PropertyNames.Tipo;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SuitabilityPerfilHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdPerfilInvestidor = "IdPerfilInvestidor";
			 public const string Data = "Data";
			 public const string Pontos = "Pontos";
			 public const string TipoCliente = "TipoCliente";
			 public const string PerfilInvestidor = "PerfilInvestidor";
			 public const string Definicao = "Definicao";
			 public const string FaixaPontuacao = "FaixaPontuacao";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdPerfilInvestidor = "IdPerfilInvestidor";
			 public const string Data = "Data";
			 public const string Pontos = "Pontos";
			 public const string TipoCliente = "TipoCliente";
			 public const string PerfilInvestidor = "PerfilInvestidor";
			 public const string Definicao = "Definicao";
			 public const string FaixaPontuacao = "FaixaPontuacao";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SuitabilityPerfilHistoricoMetadata))
			{
				if(SuitabilityPerfilHistoricoMetadata.mapDelegates == null)
				{
					SuitabilityPerfilHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SuitabilityPerfilHistoricoMetadata.meta == null)
				{
					SuitabilityPerfilHistoricoMetadata.meta = new SuitabilityPerfilHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdPerfilInvestidor", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Pontos", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PerfilInvestidor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Definicao", new esTypeMap("text", "System.String"));
				meta.AddTypeMap("FaixaPontuacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Tipo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "SuitabilityPerfilHistorico";
				meta.Destination = "SuitabilityPerfilHistorico";
				
				meta.spInsert = "proc_SuitabilityPerfilHistoricoInsert";				
				meta.spUpdate = "proc_SuitabilityPerfilHistoricoUpdate";		
				meta.spDelete = "proc_SuitabilityPerfilHistoricoDelete";
				meta.spLoadAll = "proc_SuitabilityPerfilHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_SuitabilityPerfilHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SuitabilityPerfilHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
