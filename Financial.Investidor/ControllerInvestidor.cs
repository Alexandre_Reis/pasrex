﻿using log4net;
using Financial.Investidor.Enums;
using System.Collections.Generic;
using System;
using Financial.Common.Enums;
using System.Text;
using System.Data.SqlClient;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Bolsa.Controller;
using Financial.BMF.Controller;
using Financial.RendaFixa.Controller;
using Financial.Swap.Controller;
using Financial.Tributo.Controller;
using Financial.ContaCorrente.Controller;
using Financial.Investidor.Exceptions;
using Financial.Util;
using Financial.Util.Enums;
using Financial.Fundo.Controller;
using Financial.Gerencial.Controller;
using Financial.Bolsa;
using System.Data;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Fundo;
using Financial.Swap;
using Financial.ContaCorrente;
using Financial.Contabil.Controller;
using Financial.Gerencial.Enums;
using Financial.ContaCorrente.Enums;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Fundo.Enums;
using Financial.Captacao;

namespace Financial.Investidor.Controller
{
    /// <summary>
    /// As variáveis (Properties) de controle de processamento e integração são inicializadas com false.
    /// Se quiser rodar os componentes de negócio, precisa setá-las = true.
    /// </summary>
    public class ControllerInvestidor
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ControllerInvestidor));

        private ControllerBolsa controllerBolsa;
        private ControllerBMF controllerBMF;
        private ControllerGerencial controllerGerencial;
        private ControllerRendaFixa controllerRendaFixa;
        private ControllerSwap controllerSwap;
        private ControllerFundo controllerFundo;
        private ControllerTributo controllerTributo;
        private ControllerContaCorrente controllerContaCorrente;
        private ControllerContabil controllerContabil;

        public ControllerInvestidor()
        {
            this.controllerBolsa = new ControllerBolsa();
            this.controllerBMF = new ControllerBMF();
            this.controllerRendaFixa = new ControllerRendaFixa();
            this.controllerSwap = new ControllerSwap();
            this.controllerFundo = new ControllerFundo();
            this.controllerTributo = new ControllerTributo();
            this.controllerContaCorrente = new ControllerContaCorrente();
            this.controllerGerencial = new ControllerGerencial();
            this.controllerContabil = new ControllerContabil();
        }

        /// <summary>
        /// Reprocessa as posições, por tipo = abertura ou tipo = fechamento.
        /// Se fechamento, busca as posições de fechamento do dia, pela PosicaoHistorico.
        /// Se abertura, busca as posições de abertura do dia PosicaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoReprocessamento"></param>
        public void ReprocessaPosicao(int idCliente, DateTime data, TipoReprocessamento tipoReprocessamento, bool mantemLancamentoCCFuturo)
        {
            ReprocessaPosicao(idCliente, data, tipoReprocessamento, mantemLancamentoCCFuturo, false);
        }

        /// <summary>
        /// Reprocessa as posições, por tipo = abertura ou tipo = fechamento.
        /// Se fechamento, busca as posições de fechamento do dia, pela PosicaoHistorico.
        /// Se abertura, busca as posições de abertura do dia PosicaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoReprocessamento"></param>
        /// <param name="reset"></param>
        public void ReprocessaPosicao(int idCliente, DateTime data, TipoReprocessamento tipoReprocessamento, bool mantemLancamentoCCFuturo, bool reset)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.Status);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.DataImplantacao);
            campos.Add(cliente.Query.TipoControle);
            campos.Add(cliente.Query.IsProcessando);
            campos.Add(cliente.Query.CalculaGerencial);
            campos.Add(cliente.Query.IdLocal);
            cliente.LoadByPrimaryKey(campos, idCliente);

            short idLocal = cliente.IdLocal.Value;

            if (idLocal == LocalFeriadoFixo.Brasil)
            {
                if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    throw new ExecucaoProcessamentoPeriodoException("Data final não é dia útil");
                }
            }
            else
            {
                if (!Calendario.IsDiaUtil(data, idLocal, TipoFeriado.Outros))
                {
                    throw new ExecucaoProcessamentoPeriodoException("Data final não é dia útil");
                }
            }

            if(VerificaTravaProcessamento(data, idCliente))
            {             
                throw new ExecucaoProcessamentoPeriodoException("Cliente está com reprocessamento travado. Verifique travamento de cotas em: Processamento -> Travamento de Cotas");
            }

            byte tipoControleCliente = cliente.TipoControle.Value;

            if (!reset)
            {
                if (tipoReprocessamento != TipoReprocessamento.CalculoDiario)
                {
                    DateTime dataImplantacao = cliente.DataImplantacao.Value;
                    DateTime dataDia = cliente.DataDia.Value;
                    int status = cliente.Status.Value;

                    if (DateTime.Compare(data, dataImplantacao) < 0)
                    {
                        throw new ExecucaoProcessamentoPeriodoException("Data fim deve ser maior ou igual que a data de implantação (" + dataImplantacao.ToShortDateString() + ") do cliente " + idCliente);
                    }
                    else if (status == (int)StatusCliente.Divulgado)
                    {
                        if (DateTime.Compare(dataDia, data) < 0)
                            throw new ExecucaoProcessamentoPeriodoException("Data fim deve ser menor ou igual que a data atual (" + dataDia.ToShortDateString() + ") do cliente " + idCliente);
                    }
                    else if (DateTime.Compare(dataDia, data) <= 0)
                    {
                        throw new ExecucaoProcessamentoPeriodoException("Data fim deve ser menor que a data atual (" + dataDia.ToShortDateString() + ") do cliente " + idCliente);
                    }
                }
            }

            using (esTransactionScope scope = new esTransactionScope())
            {
                if (tipoControleCliente != (byte)TipoControleCliente.Cotista)
                {
                    this.controllerBolsa.ReprocessaPosicao(idCliente, data, tipoReprocessamento);
                    this.controllerBMF.ReprocessaPosicao(idCliente, data, tipoReprocessamento);
                    this.controllerRendaFixa.ReprocessaPosicao(idCliente, data, tipoReprocessamento);
                    this.controllerSwap.ReprocessaPosicao(idCliente, data, tipoReprocessamento);
                }

                if (tipoControleCliente == (byte)TipoControleCliente.Completo ||
                    tipoControleCliente == (byte)TipoControleCliente.CarteiraRentabil ||
                    tipoControleCliente == (byte)TipoControleCliente.Carteira)
                {
                    //O método 'ReprocessaPosicaoFundo' Já será chamado pelo 'ReprocessaPosicao'
                    //if (tipoReprocessamento != TipoReprocessamento.CalculoDiario)
                    //{
                    //    this.controllerFundo.ReprocessaPosicaoFundo(idCliente, data, tipoReprocessamento, data);
                    //}

                    this.controllerFundo.ReprocessaPosicao(idCliente, data, tipoReprocessamento, false, false);
                }
                else if (tipoControleCliente == (byte)TipoControleCliente.Cotista)
                {
                    this.controllerFundo.ReprocessaPosicaoCotista(idCliente, data, tipoReprocessamento);
                    this.controllerFundo.ReprocessaAdmPfee(idCliente, data, tipoReprocessamento);
                }
                else if (tipoControleCliente == (byte)TipoControleCliente.ApenasCotacao)
                {
                    this.controllerFundo.ReprocessaAdmPfee(idCliente, data, tipoReprocessamento);
                }

                if (tipoControleCliente != (byte)TipoControleCliente.Cotista)
                {
                    this.controllerContaCorrente.ReprocessaPosicaoSaldoCaixa(idCliente, data, tipoReprocessamento);
                    this.controllerContaCorrente.ReprocessaPosicaoLiquidacao(idCliente, data, tipoReprocessamento, mantemLancamentoCCFuturo);
                    this.controllerTributo.ReprocessaPosicao(idCliente, data);

                    if (cliente.CalculaGerencial == "S" && tipoControleCliente != (byte)TipoControleCliente.IRRendaVariavel)
                    {
                        this.controllerGerencial.ReprocessaPosicao(idCliente, data, tipoReprocessamento);
                    }
                }

                #region Muda status e data dia do cliente
                int status;
                if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                {
                    status = (int)StatusCliente.Fechado;
                }
                else if (tipoReprocessamento == TipoReprocessamento.Abertura)
                {
                    status = (int)StatusCliente.Aberto;
                }
                else
                {
                    status = (int)StatusCliente.Fechado;
                }

                if (status == (int)StatusCliente.Fechado)
                {
                    ExecutaDivulgacao(idCliente, data, false);
                }

                cliente.DataDia = data;
                cliente.Status = (byte)status;
                cliente.IsProcessando = "N";
                cliente.Save();
                #endregion

                scope.Complete();
            }
        }

        /// <summary>
        /// Verifca se processamento está bloqueado
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        private bool VerificaTravaProcessamento(DateTime data, int idCliente)
        {
            bool retorno = false;

            TravamentoCotasCollection travamentoCotasCollection = new TravamentoCotasCollection();
            TravamentoCotasQuery travamentoCotasQuery = new TravamentoCotasQuery();
            travamentoCotasQuery.Where(travamentoCotasQuery.IdCarteira == idCliente);
            travamentoCotasQuery.Where(travamentoCotasQuery.DataProcessamento.GreaterThanOrEqual(data));
            travamentoCotasQuery.Where(travamentoCotasQuery.TipoBloqueio == (int)TipoTravamentoCota.TravarReprocessamento);
            if (travamentoCotasCollection.Load(travamentoCotasQuery))
            {
                retorno = true;
            }

            return retorno;

        }

        /// <summary>
        /// Executa cálculos de abertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaAbertura(int idCliente, DateTime data, ParametrosProcessamento parametrosProcessamento)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.Status);
            campos.Add(cliente.Query.IsProcessando);
            campos.Add(cliente.Query.CalculaGerencial);
            campos.Add(cliente.Query.TipoControle);
            campos.Add(cliente.Query.IdLocal);

            if (cliente.LoadByPrimaryKey(campos, idCliente))
            {
                short idLocal = cliente.IdLocal.Value;

                DateTime dataProxima = new DateTime();
                if (idLocal == LocalFeriadoFixo.Brasil)
                {
                    dataProxima = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    dataProxima = Calendario.AdicionaDiaUtil(data, 1, idLocal, TipoFeriado.Outros);
                }

                byte tipoControleCliente = cliente.TipoControle.Value;

                cliente.Status = (byte)StatusCliente.AbertoComErro;
                cliente.DataDia = dataProxima;
                cliente.IsProcessando = "N";
                cliente.Save();

                using (esTransactionScope scope = new esTransactionScope())
                {
                    Cliente clienteTestaMae = new Cliente();
                    if (!clienteTestaMae.IsMae(idCliente))
                    {
                        if (tipoControleCliente != (byte)TipoControleCliente.Cotista &&
                            tipoControleCliente != (byte)TipoControleCliente.ApenasCotacao &&
                            tipoControleCliente != (byte)TipoControleCliente.CarteiraImportada)
                        {
                            this.controllerBolsa.ExecutaAbertura(idCliente, dataProxima);
                            this.controllerBMF.ExecutaAbertura(idCliente, dataProxima);
                            this.controllerRendaFixa.ExecutaAbertura(idCliente, dataProxima);
                            this.controllerSwap.ExecutaAbertura(idCliente, dataProxima);
                            this.controllerContaCorrente.ExecutaAbertura(idCliente, dataProxima);
                        }
                    }

                    if (tipoControleCliente == (byte)TipoControleCliente.Completo ||
                        tipoControleCliente == (byte)TipoControleCliente.CarteiraRentabil)
                    {
                        Carteira carteira = new Carteira();
                        campos = new List<esQueryItem>();
                        campos.Add(carteira.Query.TipoCalculoRetorno);
                        campos.Add(carteira.Query.TipoCota);

                        carteira.LoadByPrimaryKey(campos, idCliente);

                        if (carteira.TipoCalculoRetorno.Value == (byte)CalculoRetornoCarteira.Cota)
                        {
                            this.controllerFundo.ProcessaCotaBrutaAbertura(idCliente, dataProxima, parametrosProcessamento.CravaCota);
                            this.controllerFundo.ProcessaCotaLiquidaAbertura(idCliente, dataProxima, parametrosProcessamento.CravaCota);

                            if (carteira.TipoCota.Value == (int)TipoCotaFundo.Abertura)
                            {
                                (new OperacaoCotista()).ProcessaEventos(idCliente, dataProxima);

                                this.controllerFundo.ProcessaPLAberturaDepoisCota(idCliente, dataProxima, false);

                                ControllerContaCorrente controllerContaCorrente = new ControllerContaCorrente();
                                controllerContaCorrente.ExecutaAberturaLancamentoNaData(idCliente, dataProxima);

                                (new CalculoResultado()).CalculaResultado(idCliente, dataProxima);
                                this.controllerFundo.CalculaPrazoMedioFundo(dataProxima, idCliente);
                                this.controllerFundo.CalculaRentabilidadeFundo(dataProxima, idCliente);
                            }

                            //Cálculo de ValoresPosicao (somente para cota abertura), Joga de PosicaoCotista para PosicaoCotistaAbertura 
                            this.controllerFundo.ExecutaAberturaPosicaoCotista(idCliente, dataProxima);
                        }
                        else
                        {
                            this.controllerFundo.ProcessaAberturaPosicaoFundo(idCliente, dataProxima);
                        }
                    }
                    else if (tipoControleCliente == (byte)TipoControleCliente.Cotista)
                    {
                        this.controllerFundo.ExecutaAberturaPosicaoCotista(idCliente, dataProxima);
                    }
                    else if (tipoControleCliente == (byte)TipoControleCliente.ApenasCotacao ||
                             tipoControleCliente == (byte)TipoControleCliente.Carteira)
                    {
                        this.controllerFundo.ProcessaAberturaPosicaoFundo(idCliente, dataProxima);
                    }

                    if (tipoControleCliente != (byte)TipoControleCliente.IRRendaVariavel &&
                        tipoControleCliente != (byte)TipoControleCliente.Cotista)
                    {
                        if (cliente.CalculaGerencial == "S")
                        {
                            this.controllerGerencial.ExecutaAbertura(idCliente, dataProxima);
                        }
                    }

                    //Altera o status do cliente para Aberto não Calculado e IsProcessando para N
                    cliente.Status = (byte)StatusCliente.Aberto;
                    cliente.DataDia = dataProxima;
                    cliente.IsProcessando = "N";
                    cliente.Save();

                    scope.Complete();
                }
            }
            //
        }

        /// <summary>
        /// Backup de PosicaoAtual para PosicaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaDivulgacao(int idCliente, DateTime data, bool alteraStatus)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.Status);
            campos.Add(cliente.Query.IsProcessando);
            campos.Add(cliente.Query.CalculaGerencial);
            campos.Add(cliente.Query.TipoControle);

            if (cliente.LoadByPrimaryKey(campos, idCliente))
            {
                byte tipoControleCliente = cliente.TipoControle.Value;

                using (esTransactionScope scope = new esTransactionScope())
                {
                    if (tipoControleCliente != (byte)TipoControleCliente.Cotista)
                    {
                        this.controllerBolsa.ExecutaDivulgacao(idCliente, data);
                        this.controllerBMF.ExecutaDivulgacao(idCliente, data);
                        this.controllerRendaFixa.ExecutaDivulgacao(idCliente, data);
                        this.controllerSwap.ExecutaDivulgacao(idCliente, data);
                        this.controllerContaCorrente.ExecutaDivulgacao(idCliente, data);
                    }

                    if (tipoControleCliente == (byte)TipoControleCliente.Completo ||
                        tipoControleCliente == (byte)TipoControleCliente.CarteiraRentabil ||
                        tipoControleCliente == (byte)TipoControleCliente.Carteira ||
                        tipoControleCliente == (byte)TipoControleCliente.ApenasCotacao)
                    {
                        Carteira carteira = new Carteira();
                        campos = new List<esQueryItem>();
                        campos.Add(carteira.Query.TipoCalculoRetorno);

                        carteira.LoadByPrimaryKey(campos, idCliente);

                        if (tipoControleCliente != (byte)TipoControleCliente.Carteira ) // &&  carteira.TipoCalculoRetorno.Value == (byte)CalculoRetornoCarteira.Cota)                        
                        {
                            this.controllerFundo.ExecutaDivulgacao(idCliente, data);
                        }
                        else
                        {
                            this.controllerFundo.ExecutaDivulgacaoControleFundos(idCliente, data);
                        }
                    }
                    else if (tipoControleCliente == (byte)TipoControleCliente.Cotista)
                    {
                        this.controllerFundo.ExecutaDivulgacaoControleCotista(idCliente, data);
                    }

                    if (tipoControleCliente != (byte)TipoControleCliente.IRRendaVariavel &&
                        tipoControleCliente != (byte)TipoControleCliente.Cotista)
                    {
                        if (cliente.CalculaGerencial == "S")
                        {
                            this.controllerGerencial.ExecutaDivulgacao(idCliente, data);
                        }
                    }

                    //Altera o status do cliente para Fechado e IsProcessando para N
                    if (alteraStatus)
                    {
                        cliente.Status = (byte)StatusCliente.Divulgado;
                    }
                    cliente.IsProcessando = "N";
                    cliente.Save();

                    scope.Complete();
                }
            }
            //
        }

        /// <summary>
        /// Realiza todos os cálculos financeiros e despesas, atualiza a custódia, gera fluxos de pagamento futuros.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="parametrosProcessamento">checks indicando se executa ou não determinado tipo de processamento</param>
        public void ExecutaFechamento(int idCliente, DateTime data, ParametrosProcessamento parametrosProcessamento)
        {
            Cliente cliente = new Cliente();

            bool isMae = cliente.IsMae(idCliente);

            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.Status);
            campos.Add(cliente.Query.CalculaGerencial);
            campos.Add(cliente.Query.TipoControle);
            campos.Add(cliente.Query.IdTipo);
            if (cliente.LoadByPrimaryKey(idCliente))
            {
                byte tipoControleCliente = cliente.TipoControle.Value;
                cliente.Status = (byte)StatusCliente.FechadoComErro;
                cliente.Save();

                //Verifica se possui Evento de fundo cadastrado para a data de processamento. Se sim, exclui os movimentos gerados pelo evento.
                this.verificaEventoFundo(idCliente, data);

                if (tipoControleCliente != (byte)TipoControleCliente.Cotista &&
                    tipoControleCliente != (byte)TipoControleCliente.ApenasCotacao &&
                    tipoControleCliente != (byte)TipoControleCliente.CarteiraImportada)
                {
                    if (!isMae)
                    {
                        //Deve ser reprocessada primeiramente apenas a parte de liquidacao do contacorrente,
                        //já que todos os componentes de negócio se encarregam de gerar os seus respectivos lançamentos 
                        //diretamente em Liquidacao.
                        this.controllerContaCorrente.ReprocessaPosicaoLiquidacao(idCliente, data, TipoReprocessamento.CalculoDiario, false);

                        if (parametrosProcessamento.ProcessaBolsa)
                        {
                            this.controllerBolsa.ExecutaFechamento(idCliente, data, parametrosProcessamento);
                        }

                        if (parametrosProcessamento.ProcessaBMF)
                        {
                            this.controllerBMF.ExecutaFechamento(idCliente, data, parametrosProcessamento);
                        }

                        if (tipoControleCliente != (byte)TipoControleCliente.IRRendaVariavel)
                        {
                            if (parametrosProcessamento.ProcessaRendaFixa)
                            {
                                this.controllerRendaFixa.ExecutaFechamento(idCliente, data, parametrosProcessamento);
                            }

                            if (parametrosProcessamento.ProcessaSwap)
                            {
                                this.controllerSwap.ExecutaFechamento(idCliente, data, parametrosProcessamento);
                            }
                        }

                        if (!parametrosProcessamento.IgnoraOperacoes)
                        {
                            this.controllerTributo.ExecutaFechamento(idCliente, data);

                            //Se for do tipo carteira apenas, o cálculo é feito direto aqui
                            //Se tiver cálculo de cota envolvida (CarteiraRentabil ou Completo), 
                            //o cálculo é feito direto no ControllerFundo
                            if (tipoControleCliente == (byte)TipoControleCliente.Carteira ||
                                tipoControleCliente == (byte)TipoControleCliente.IRRendaVariavel)
                            {
                                this.controllerContaCorrente.ExecutaFechamento(idCliente, data, parametrosProcessamento);
                            }
                        }
                    }
                }

                if (isMae &&
                    tipoControleCliente != (byte)TipoControleCliente.Cotista &&
                    tipoControleCliente != (byte)TipoControleCliente.ApenasCotacao &&
                    tipoControleCliente != (byte)TipoControleCliente.CarteiraImportada)
                {
                    this.CompoeCarteiraMae(idCliente, data);

                    if (cliente.IdTipo.Value == (int)TipoClienteFixo.FDIC)
                    {
                        this.CompoeCarteiraMaeFDIC(idCliente, data);
                    }
                }

                //O Tratamento de que tipo de rotina é executada pelo tipo de controle de cliente é feito do componente Financial.Fundo
                if (tipoControleCliente != (byte)TipoControleCliente.IRRendaVariavel &&
                    tipoControleCliente != (byte)TipoControleCliente.CarteiraImportada)
                {
                    if (isMae)
                    {
                        this.controllerFundo.ExecutaFechamento(idCliente, data, parametrosProcessamento, true);
                    }
                    else
                    {
                        this.controllerFundo.ExecutaFechamento(idCliente, data, parametrosProcessamento);
                    }
                }

                if (!isMae)
                {
                    if (tipoControleCliente != (byte)TipoControleCliente.IRRendaVariavel &&
                        tipoControleCliente != (byte)TipoControleCliente.Cotista &&
                        tipoControleCliente != (byte)TipoControleCliente.ApenasCotacao &&
                        tipoControleCliente != (byte)TipoControleCliente.CarteiraImportada)
                    {
                        if (cliente.CalculaGerencial != TipoProcessamentoGerencial.NaoCalcula)
                        {
                            if (cliente.CalculaGerencial == TipoProcessamentoGerencial.Ambos)
                            {
                                this.controllerGerencial.ExecutaFechamento(idCliente, data);
                                this.controllerGerencial.ExecutaCalculoEstrategia(idCliente, data);
                            }
                            else if (cliente.CalculaGerencial == TipoProcessamentoGerencial.GerencialOperacoes)
                            {
                                this.controllerGerencial.ExecutaFechamento(idCliente, data);
                            }
                            else if (cliente.CalculaGerencial == TipoProcessamentoGerencial.GerencialEstrategia)
                            {
                                this.controllerGerencial.ExecutaCalculoEstrategia(idCliente, data);
                            }
                        }
                    }
                }

                if (!parametrosProcessamento.IgnoraOperacoes)
                {
                    if (tipoControleCliente == (byte)TipoControleCliente.Completo ||
                        tipoControleCliente == (byte)TipoControleCliente.Carteira)
                    {
                        this.controllerContabil.ExecutaProcessoEventos(idCliente, data);
                    }
                }

                #region Verifica se precisa marcar carteiras para reprocessamento, que compraram cotas do fundo
                if (ParametrosConfiguracaoSistema.Outras.RetroagirCarteirasDependentes)
                {
                    Cliente clienteCotista = new Cliente();
                    ClienteQuery clienteQuery = new ClienteQuery("CQ");
                    PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("PFHQ");
                    posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.IdCliente);
                    posicaoFundoHistoricoQuery.InnerJoin(clienteQuery).On(posicaoFundoHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                    posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCarteira.Equal(idCliente));
                    posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.DataHistorico.Equal(data));
                    posicaoFundoHistoricoQuery.Where(clienteQuery.DataDia.GreaterThan(data));
                    posicaoFundoHistoricoQuery.es.Distinct = true;

                    PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                    posicaoFundoHistoricoCollection.Load(posicaoFundoHistoricoQuery);

                    foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
                    {
                        clienteCotista.LoadByPrimaryKey(posicaoFundoHistorico.IdCliente.Value);
                        if (DateTime.Compare(clienteCotista.DataDia.Value, data) > 0)
                        {
                            BoletoRetroativo boletoRetroativo = new BoletoRetroativo();
                            if (boletoRetroativo.LoadByPrimaryKey(posicaoFundoHistorico.IdCliente.Value))
                            {
                                if (DateTime.Compare(boletoRetroativo.DataBoleto.Value, data) > 0)
                                {
                                    boletoRetroativo.DataBoleto = data;
                                }
                            }
                            else
                            {
                                boletoRetroativo.IdCliente = posicaoFundoHistorico.IdCliente.Value;
                                boletoRetroativo.DataBoleto = data;
                                boletoRetroativo.TipoMercado = (int)TipoMercado.Fundos;
                            }

                            boletoRetroativo.Save();
                        }
                    }

                }
                #endregion

                //Executa calculo de rebate para Impactar PL
                if (tipoControleCliente == (byte)TipoControleCliente.Completo ||
                    tipoControleCliente == (byte)TipoControleCliente.CarteiraRentabil ||
                    tipoControleCliente == (byte)TipoControleCliente.Cotista ||
                    tipoControleCliente == (byte)TipoControleCliente.ApenasCotacao)
                {

                    Carteira carteira = new Carteira();
                    carteira.LoadByPrimaryKey(idCliente);

                    if (carteira.RebateImpactaPL == "S")
                    {
                        CalculoRebate calculoRebate = new CalculoRebate();
                        calculoRebate.CalculaRebateImpactaPL(idCliente, data);
                    }
                }


                //Executar backup dos dados para as tabelas de histórico através da rotina de fechamento
                this.ExecutaDivulgacao(idCliente, data, false);

                //Altera o status do cliente para Calculado e IsProcessando para N
                cliente.Status = (byte)StatusCliente.Fechado;
                cliente.IsProcessando = "N";
                cliente.Save();
                //                    
            }
        }

        /// <summary>
        /// Exclui movimentos gerados pelo evento de fundos (Incorporação/Cisão/Fusão)
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void verificaEventoFundo(int idCliente, DateTime data)
        {
            EventoFundoCollection eventoFundoCollection = new EventoFundoCollection();
            eventoFundoCollection.Query.Where(eventoFundoCollection.Query.DataPosicao.Equal(data));
            eventoFundoCollection.Query.Load();

            HistoricoCota historicoCota = new HistoricoCota();

            foreach (EventoFundo eventoFundo in eventoFundoCollection)
            {
                //Apaga todos os movimentos gerados
                historicoCota.ExcluiPosicoesGeradas(eventoFundo, idCliente);
                historicoCota.ExcluiMovimentosGerados(eventoFundo, idCliente);
            }
        }

        /// <summary>
        /// Executa de forma contínua n dias de processamento (cálculo, fechamento, abertura), até a data fim.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataFim"></param>
        public void ExecutaCalculoPeriodo(int idCliente, DateTime dataFim, ParametrosProcessamento parametrosProcessamento)
        {            
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.Status);
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.IsProcessando);
            campos.Add(cliente.Query.IdLocal);

            if (cliente.LoadByPrimaryKey(campos, idCliente))
            {
                short idLocal = cliente.IdLocal.Value;

                if (idLocal == LocalFeriadoFixo.Brasil)
                {
                    if (!Calendario.IsDiaUtil(dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                    {
                        throw new ExecucaoProcessamentoPeriodoException("Data fim não é dia útil");
                    }
                }
                else
                {
                    if (!Calendario.IsDiaUtil(dataFim, idLocal, TipoFeriado.Outros))
                    {
                        throw new ExecucaoProcessamentoPeriodoException("Data fim não é dia útil");
                    }
                }

                DateTime dataDia = cliente.DataDia.Value;
                int status = cliente.Status.Value;

                if (DateTime.Compare(dataDia, dataFim) >= 0)
                {
                    throw new ExecucaoProcessamentoPeriodoException("Data fim deve ser maior que a data dia do cliente " + idCliente);
                }

                DateTime dataAux = dataDia;

                //Garante que se estiver fechado, o 1o cálculo diário será feito com o status aberto
                if (status == (int)StatusCliente.Divulgado)
                {
                    ExecutaAbertura(idCliente, dataAux, parametrosProcessamento);

                    if (idLocal == LocalFeriadoFixo.Brasil)
                    {
                        dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    else
                    {
                        dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, idLocal, TipoFeriado.Outros);
                    }
                }
                //

                StatusCliente novoStatusCliente = StatusCliente.Fechado;
                while (DateTime.Compare(dataFim, dataAux) >= 0)                                
                {

                    if (dataFim.Date == dataAux.Date && parametrosProcessamento.TipoProcessamento == TipoProcessamento.Abertura)
                    {
                        novoStatusCliente = StatusCliente.Aberto;
                    }
                    else
                    {
                        ExecutaFechamento(idCliente, dataAux, parametrosProcessamento);

                        //Feito este teste, para garantir que o processo por período terminará com o último dia 
                        //com status = calculado e não fechado.
                        if (DateTime.Compare(dataFim, dataAux) > 0)
                        {
                            ExecutaDivulgacao(idCliente, dataAux, true);
                            ExecutaAbertura(idCliente, dataAux, parametrosProcessamento);
                        }
                        //
                    }

                    if (idLocal == LocalFeriadoFixo.Brasil)
                    {
                        dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    else
                    {
                        dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, idLocal, TipoFeriado.Outros);
                    }
                }

                if (parametrosProcessamento.AberturaAutomatica)
                {
                    dataFim = Calendario.AdicionaDiaUtil(dataFim, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    AberturaAutomatica(idCliente, dataFim);
                    ExecutaDivulgacao(idCliente, dataFim, true);
                    ExecutaAbertura(idCliente, dataFim, parametrosProcessamento);
                }

                cliente.Status = (byte)novoStatusCliente;
                cliente.DataDia = dataFim;
                cliente.IsProcessando = "N";
                cliente.Save();
            }
        }

        /// <summary>
        /// Compôe os valores em bolsa, bmf, RF etc para a carteira mãe, a partir da lista de carteiras filhas.
        /// </summary>
        /// <param name="listaCarteiras"></param>
        /// <param name="idCarteiraMae"></param>
        /// <param name="data"></param>
        public bool CompoeCarteiraMae(int idCarteiraMae, DateTime data)
        {
            bool processou = false;

            Cliente clienteMae = new Cliente();
            clienteMae.Query.Select(clienteMae.Query.DataImplantacao, clienteMae.Query.IdMoeda);
            clienteMae.Query.Where(clienteMae.Query.IdCliente.Equal(idCarteiraMae));
            clienteMae.Query.Load();

            DateTime dataImplantacao = clienteMae.DataImplantacao.Value;

            CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection();
            carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae, carteiraMaeCollection.Query.IdCarteiraFilha);
            carteiraMaeCollection.Query.Where(carteiraMaeCollection.Query.IdCarteiraMae.Equal(idCarteiraMae));
            carteiraMaeCollection.Query.Load();

            bool temTaxasMae = false;
            #region Checa se a carteira mãe tem taxa adm e/ou pfee associada, se tiver não busca os lançamentos de liquidacao das filhas
            TabelaTaxaAdministracaoCollection tabelaTaxaAdministracaoCollection = new TabelaTaxaAdministracaoCollection();
            tabelaTaxaAdministracaoCollection.Query.Select(tabelaTaxaAdministracaoCollection.Query.IdTabela);
            tabelaTaxaAdministracaoCollection.Query.Where(tabelaTaxaAdministracaoCollection.Query.IdCarteira.Equal(idCarteiraMae));
            tabelaTaxaAdministracaoCollection.Query.Load();

            if (tabelaTaxaAdministracaoCollection.Count > 0)
            {
                temTaxasMae = true;
            }
            else
            {
                TabelaTaxaPerformanceCollection tabelaTaxaPerformanceCollection = new TabelaTaxaPerformanceCollection();
                tabelaTaxaPerformanceCollection.Query.Select(tabelaTaxaPerformanceCollection.Query.IdTabela);
                tabelaTaxaPerformanceCollection.Query.Where(tabelaTaxaPerformanceCollection.Query.IdCarteira.Equal(idCarteiraMae));
                tabelaTaxaPerformanceCollection.Query.Load();

                if (tabelaTaxaPerformanceCollection.Count > 0)
                {
                    temTaxasMae = true;
                }
            }
            #endregion

            if (carteiraMaeCollection.Count > 0)
            {
                processou = true;

                #region Deleta operacoes, posicoes, caixa e liquidacao da Mae
                if (data != dataImplantacao)
                {
                    OperacaoCotistaCollection operacaoCotistaCollectionDeletar = new OperacaoCotistaCollection();
                    operacaoCotistaCollectionDeletar.Query.Select(operacaoCotistaCollectionDeletar.Query.IdOperacao);
                    operacaoCotistaCollectionDeletar.Query.Where(operacaoCotistaCollectionDeletar.Query.IdCarteira.Equal(idCarteiraMae) &
                                                                 operacaoCotistaCollectionDeletar.Query.DataOperacao.Equal(data) &
                                                                 operacaoCotistaCollectionDeletar.Query.Fonte.NotEqual((byte)FonteOperacaoCotista.OperacaoMae) &
                                                                 (operacaoCotistaCollectionDeletar.Query.TipoOperacao.NotIn((byte)TipoOperacaoCotista.AplicacaoCotasEspecial,
                                                                                                                            (byte)TipoOperacaoCotista.ResgateCotasEspecial) |
                                                                  operacaoCotistaCollectionDeletar.Query.Fonte.Equal((byte)FonteOperacaoCotista.ResgateTributos)
                                                                 ));
                    operacaoCotistaCollectionDeletar.Query.Load();
                    operacaoCotistaCollectionDeletar.MarkAllAsDeleted();
                    operacaoCotistaCollectionDeletar.Save();
                }

                OperacaoBolsaCollection operacaoBolsaCollectionDeletar = new OperacaoBolsaCollection();
                operacaoBolsaCollectionDeletar.Query.Select(operacaoBolsaCollectionDeletar.Query.IdOperacao);
                operacaoBolsaCollectionDeletar.Query.Where(operacaoBolsaCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae),
                                                           operacaoBolsaCollectionDeletar.Query.Data.Equal(data));
                operacaoBolsaCollectionDeletar.Query.Load();
                operacaoBolsaCollectionDeletar.MarkAllAsDeleted();
                operacaoBolsaCollectionDeletar.Save();

                OperacaoEmprestimoBolsaCollection operacaoEmprestimoBolsaCollectionDeletar = new OperacaoEmprestimoBolsaCollection();
                operacaoEmprestimoBolsaCollectionDeletar.Query.Select(operacaoEmprestimoBolsaCollectionDeletar.Query.IdOperacao);
                operacaoEmprestimoBolsaCollectionDeletar.Query.Where(operacaoEmprestimoBolsaCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae),
                                                                     operacaoEmprestimoBolsaCollectionDeletar.Query.DataRegistro.Equal(data));
                operacaoEmprestimoBolsaCollectionDeletar.Query.Load();
                operacaoEmprestimoBolsaCollectionDeletar.MarkAllAsDeleted();
                operacaoEmprestimoBolsaCollectionDeletar.Save();

                OperacaoBMFCollection operacaoBMFCollectionDeletar = new OperacaoBMFCollection();
                operacaoBMFCollectionDeletar.Query.Select(operacaoBMFCollectionDeletar.Query.IdOperacao);
                operacaoBMFCollectionDeletar.Query.Where(operacaoBMFCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae),
                                                           operacaoBMFCollectionDeletar.Query.Data.Equal(data));
                operacaoBMFCollectionDeletar.Query.Load();
                operacaoBMFCollectionDeletar.MarkAllAsDeleted();
                operacaoBMFCollectionDeletar.Save();

                OperacaoRendaFixaCollection operacaoRendaFixaCollectionDeletar = new OperacaoRendaFixaCollection();
                operacaoRendaFixaCollectionDeletar.Query.Select(operacaoRendaFixaCollectionDeletar.Query.IdOperacao);
                operacaoRendaFixaCollectionDeletar.Query.Where(operacaoRendaFixaCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae),
                                                           operacaoRendaFixaCollectionDeletar.Query.DataOperacao.Equal(data));
                operacaoRendaFixaCollectionDeletar.Query.Load();
                operacaoRendaFixaCollectionDeletar.MarkAllAsDeleted();
                operacaoRendaFixaCollectionDeletar.Save();

                OperacaoFundoCollection operacaoFundoCollectionDeletar = new OperacaoFundoCollection();
                operacaoFundoCollectionDeletar.Query.Select(operacaoFundoCollectionDeletar.Query.IdOperacao);
                operacaoFundoCollectionDeletar.Query.Where(operacaoFundoCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae),
                                                           operacaoFundoCollectionDeletar.Query.DataOperacao.Equal(data));
                operacaoFundoCollectionDeletar.Query.Load();
                operacaoFundoCollectionDeletar.MarkAllAsDeleted();
                operacaoFundoCollectionDeletar.Save();

                PosicaoBolsaCollection posicaoBolsaCollectionDeletar = new PosicaoBolsaCollection();
                posicaoBolsaCollectionDeletar.Query.Select(posicaoBolsaCollectionDeletar.Query.IdPosicao);
                posicaoBolsaCollectionDeletar.Query.Where(posicaoBolsaCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae));
                posicaoBolsaCollectionDeletar.Query.Load();
                posicaoBolsaCollectionDeletar.MarkAllAsDeleted();
                posicaoBolsaCollectionDeletar.Save();

                PosicaoTermoBolsaCollection posicaoTermoBolsaCollectionDeletar = new PosicaoTermoBolsaCollection();
                posicaoTermoBolsaCollectionDeletar.Query.Select(posicaoTermoBolsaCollectionDeletar.Query.IdPosicao);
                posicaoTermoBolsaCollectionDeletar.Query.Where(posicaoTermoBolsaCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae));
                posicaoTermoBolsaCollectionDeletar.Query.Load();
                posicaoTermoBolsaCollectionDeletar.MarkAllAsDeleted();
                posicaoTermoBolsaCollectionDeletar.Save();

                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollectionDeletar = new PosicaoEmprestimoBolsaCollection();
                posicaoEmprestimoBolsaCollectionDeletar.Query.Select(posicaoEmprestimoBolsaCollectionDeletar.Query.IdPosicao);
                posicaoEmprestimoBolsaCollectionDeletar.Query.Where(posicaoEmprestimoBolsaCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae));
                posicaoEmprestimoBolsaCollectionDeletar.Query.Load();
                posicaoEmprestimoBolsaCollectionDeletar.MarkAllAsDeleted();
                posicaoEmprestimoBolsaCollectionDeletar.Save();

                PosicaoBMFCollection posicaoBMFCollectionDeletar = new PosicaoBMFCollection();
                posicaoBMFCollectionDeletar.Query.Select(posicaoBMFCollectionDeletar.Query.IdPosicao);
                posicaoBMFCollectionDeletar.Query.Where(posicaoBMFCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae));
                posicaoBMFCollectionDeletar.Query.Load();
                posicaoBMFCollectionDeletar.MarkAllAsDeleted();
                posicaoBMFCollectionDeletar.Save();

                PosicaoRendaFixaCollection posicaoRendaFixaCollectionDeletar = new PosicaoRendaFixaCollection();
                posicaoRendaFixaCollectionDeletar.Query.Select(posicaoRendaFixaCollectionDeletar.Query.IdPosicao);
                posicaoRendaFixaCollectionDeletar.Query.Where(posicaoRendaFixaCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae));
                posicaoRendaFixaCollectionDeletar.Query.Load();
                posicaoRendaFixaCollectionDeletar.MarkAllAsDeleted();
                posicaoRendaFixaCollectionDeletar.Save();

                PosicaoSwapCollection posicaoSwapCollectionDeletar = new PosicaoSwapCollection();
                posicaoSwapCollectionDeletar.Query.Select(posicaoSwapCollectionDeletar.Query.IdPosicao);
                posicaoSwapCollectionDeletar.Query.Where(posicaoSwapCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae));
                posicaoSwapCollectionDeletar.Query.Load();
                posicaoSwapCollectionDeletar.MarkAllAsDeleted();
                posicaoSwapCollectionDeletar.Save();

                PosicaoFundoCollection posicaoFundoCollectionDeletar = new PosicaoFundoCollection();
                posicaoFundoCollectionDeletar.Query.Select(posicaoFundoCollectionDeletar.Query.IdPosicao);
                posicaoFundoCollectionDeletar.Query.Where(posicaoFundoCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae));
                posicaoFundoCollectionDeletar.Query.Load();
                posicaoFundoCollectionDeletar.MarkAllAsDeleted();
                posicaoFundoCollectionDeletar.Save();

                LiquidacaoCollection liquidacaoCollectionDeletar = new LiquidacaoCollection();
                liquidacaoCollectionDeletar.Query.Select(liquidacaoCollectionDeletar.Query.IdLiquidacao);
                liquidacaoCollectionDeletar.Query.Where(liquidacaoCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae),
                                                        liquidacaoCollectionDeletar.Query.DataVencimento.GreaterThanOrEqual(data),
                                                        liquidacaoCollectionDeletar.Query.DataLancamento.LessThanOrEqual(data));
                liquidacaoCollectionDeletar.Query.Load();
                liquidacaoCollectionDeletar.MarkAllAsDeleted();
                liquidacaoCollectionDeletar.Save();

                SaldoCaixaCollection saldoCaixaCollectionDeletar = new SaldoCaixaCollection();
                saldoCaixaCollectionDeletar.Query.Where(saldoCaixaCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae),
                                                        saldoCaixaCollectionDeletar.Query.Data.Equal(data));
                saldoCaixaCollectionDeletar.Query.Load();
                saldoCaixaCollectionDeletar.MarkAllAsDeleted();
                saldoCaixaCollectionDeletar.Save();

                LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollectionDeletar = new LiquidacaoRendaFixaCollection();
                liquidacaoRendaFixaCollectionDeletar.Query.Select(liquidacaoRendaFixaCollectionDeletar.Query.IdLiquidacao);
                liquidacaoRendaFixaCollectionDeletar.Query.Where(liquidacaoRendaFixaCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae),
                                                           liquidacaoRendaFixaCollectionDeletar.Query.DataLiquidacao.Equal(data));
                liquidacaoRendaFixaCollectionDeletar.Query.Load();
                liquidacaoRendaFixaCollectionDeletar.MarkAllAsDeleted();
                liquidacaoRendaFixaCollectionDeletar.Save();

                ProventoBolsaClienteCollection proventoBolsaClienteCollectionDeletar = new ProventoBolsaClienteCollection();
                proventoBolsaClienteCollectionDeletar.Query.Select(proventoBolsaClienteCollectionDeletar.Query.IdProvento);
                proventoBolsaClienteCollectionDeletar.Query.Where(proventoBolsaClienteCollectionDeletar.Query.IdCliente.Equal(idCarteiraMae),
                                                                  proventoBolsaClienteCollectionDeletar.Query.DataLancamento.Equal(data));
                proventoBolsaClienteCollectionDeletar.Query.Load();
                proventoBolsaClienteCollectionDeletar.MarkAllAsDeleted();
                proventoBolsaClienteCollectionDeletar.Save();
                #endregion
            }

            foreach (CarteiraMae carteiraMae in carteiraMaeCollection)
            {
                int idCarteiraFilha = carteiraMae.IdCarteiraFilha.Value;

                Cliente clienteFilha = new Cliente();
                clienteFilha.Query.Select(clienteFilha.Query.DataDia);
                clienteFilha.Query.Where(clienteFilha.Query.IdCliente.Equal(idCarteiraFilha));
                clienteFilha.Query.Load();

                DateTime dataDiaFilha = clienteFilha.DataDia.Value;

                #region Faz Insert das filhas na Mae de Operacao

                if (data != dataImplantacao)
                {
                    OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                    operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteiraFilha),
                                                          operacaoCotistaCollection.Query.DataOperacao.Equal(data),
                                                          operacaoCotistaCollection.Query.ValorBruto.NotEqual(0),
                                                          operacaoCotistaCollection.Query.TipoOperacao.NotIn((byte)TipoOperacaoCotista.ResgateCotasEspecial,
                                                                                                             (byte)TipoOperacaoCotista.AplicacaoCotasEspecial));
                    operacaoCotistaCollection.Query.Load();

                    ContaCorrenteCollection contaCorrenteMaeColl = new ContaCorrenteCollection();
                    contaCorrenteMaeColl.Query.Where(contaCorrenteMaeColl.Query.IdPessoa.Equal(idCarteiraMae)
                                                     & contaCorrenteMaeColl.Query.IdMoeda.Equal(clienteMae.IdMoeda.Value)
                                                     & contaCorrenteMaeColl.Query.ContaDefault.Equal("S"));


                    int? idContaMae = null;
                    if (contaCorrenteMaeColl.Query.Load())
                        idContaMae = contaCorrenteMaeColl[0].IdConta.Value;

                    foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
                    {
                        int idOperacao = operacaoCotista.IdOperacao.Value;

                        byte tipoOperacao = operacaoCotista.TipoOperacao.Value;

                        if (tipoOperacao != (byte)TipoOperacaoCotista.Aplicacao)
                        {
                            tipoOperacao = (byte)TipoOperacaoCotista.ResgateBruto;
                        }

                        OperacaoCotista operacaoCotistaInsert = new OperacaoCotista();
                        operacaoCotistaInsert.LoadByPrimaryKey(idOperacao);
                        operacaoCotistaInsert.MarkAllColumnsAsDirty(DataRowState.Added);

                        operacaoCotistaInsert.IdCotista = idCarteiraMae;
                        operacaoCotistaInsert.IdCarteira = idCarteiraMae;
                        if (idContaMae.HasValue)
                            operacaoCotistaInsert.IdConta = idContaMae.Value;
                        operacaoCotistaInsert.TipoOperacao = tipoOperacao;
                        operacaoCotistaInsert.Save();
                    }
                }

                OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
                operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                    operacaoBolsaCollection.Query.Data.Equal(data));
                operacaoBolsaCollection.Query.Load();

                foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
                {
                    int idOperacao = operacaoBolsa.IdOperacao.Value;

                    OperacaoBolsa operacaoBolsaInsert = new OperacaoBolsa();
                    operacaoBolsaInsert.LoadByPrimaryKey(idOperacao);
                    operacaoBolsaInsert.MarkAllColumnsAsDirty(DataRowState.Added);

                    operacaoBolsaInsert.IdCliente = idCarteiraMae;
                    operacaoBolsaInsert.Save();
                }

                OperacaoEmprestimoBolsaCollection operacaoEmprestimoBolsaCollection = new OperacaoEmprestimoBolsaCollection();
                operacaoEmprestimoBolsaCollection.Query.Where(operacaoEmprestimoBolsaCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                              operacaoEmprestimoBolsaCollection.Query.DataRegistro.Equal(data));
                operacaoEmprestimoBolsaCollection.Query.Load();

                foreach (OperacaoEmprestimoBolsa operacaoEmprestimoBolsa in operacaoEmprestimoBolsaCollection)
                {
                    int idOperacao = operacaoEmprestimoBolsa.IdOperacao.Value;

                    OperacaoEmprestimoBolsa operacaoEmprestimoBolsaInsert = new OperacaoEmprestimoBolsa();
                    operacaoEmprestimoBolsaInsert.LoadByPrimaryKey(idOperacao);
                    operacaoEmprestimoBolsaInsert.MarkAllColumnsAsDirty(DataRowState.Added);

                    operacaoEmprestimoBolsaInsert.IdCliente = idCarteiraMae;
                    operacaoEmprestimoBolsaInsert.Save();
                }

                OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
                operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                  operacaoBMFCollection.Query.Data.Equal(data));
                operacaoBMFCollection.Query.Load();

                foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
                {
                    int idOperacao = operacaoBMF.IdOperacao.Value;

                    OperacaoBMF operacaoBMFInsert = new OperacaoBMF();
                    operacaoBMFInsert.LoadByPrimaryKey(idOperacao);
                    operacaoBMFInsert.MarkAllColumnsAsDirty(DataRowState.Added);

                    operacaoBMFInsert.IdCliente = idCarteiraMae;
                    operacaoBMFInsert.Save();
                }

                OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
                operacaoRendaFixaCollection.Query.Where(operacaoRendaFixaCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                        operacaoRendaFixaCollection.Query.DataOperacao.Equal(data));
                operacaoRendaFixaCollection.Query.Load();

                foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollection)
                {
                    int idOperacao = operacaoRendaFixa.IdOperacao.Value;

                    OperacaoRendaFixa operacaoRendaFixaInsert = new OperacaoRendaFixa();
                    operacaoRendaFixaInsert.LoadByPrimaryKey(idOperacao);
                    operacaoRendaFixaInsert.MarkAllColumnsAsDirty(DataRowState.Added);

                    operacaoRendaFixaInsert.IdCliente = idCarteiraMae;
                    operacaoRendaFixaInsert.Save();
                }

                OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
                operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                    operacaoFundoCollection.Query.DataOperacao.Equal(data));
                operacaoFundoCollection.Query.Load();

                foreach (OperacaoFundo operacaoFundo in operacaoFundoCollection)
                {
                    int idOperacao = operacaoFundo.IdOperacao.Value;

                    OperacaoFundo operacaoFundoInsert = new OperacaoFundo();
                    operacaoFundoInsert.LoadByPrimaryKey(idOperacao);
                    operacaoFundoInsert.MarkAllColumnsAsDirty(DataRowState.Added);

                    operacaoFundoInsert.IdCliente = idCarteiraMae;
                    operacaoFundoInsert.Save();
                }

                ProventoBolsaClienteCollection proventoBolsaClienteCollection = new ProventoBolsaClienteCollection();
                proventoBolsaClienteCollection.Query.Where(proventoBolsaClienteCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                           proventoBolsaClienteCollection.Query.DataLancamento.Equal(data));
                proventoBolsaClienteCollection.Query.Load();

                foreach (ProventoBolsaCliente proventoBolsaCliente in proventoBolsaClienteCollection)
                {
                    int idProvento = proventoBolsaCliente.IdProvento.Value;

                    ProventoBolsaCliente proventoBolsaClienteInsert = new ProventoBolsaCliente();
                    proventoBolsaClienteInsert.LoadByPrimaryKey(idProvento);
                    proventoBolsaClienteInsert.MarkAllColumnsAsDirty(DataRowState.Added);

                    proventoBolsaClienteInsert.IdCliente = idCarteiraMae;
                    proventoBolsaClienteInsert.Save();
                }

                LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
                liquidacaoRendaFixaCollection.Query.Where(liquidacaoRendaFixaCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                          liquidacaoRendaFixaCollection.Query.DataLiquidacao.Equal(data));
                liquidacaoRendaFixaCollection.Query.Load();

                foreach (LiquidacaoRendaFixa liquidacaoRendaFixa in liquidacaoRendaFixaCollection)
                {
                    int idLiquidacao = liquidacaoRendaFixa.IdLiquidacao.Value;

                    LiquidacaoRendaFixa liquidacaoRendaFixaInsert = new LiquidacaoRendaFixa();
                    liquidacaoRendaFixaInsert.LoadByPrimaryKey(idLiquidacao);
                    liquidacaoRendaFixaInsert.MarkAllColumnsAsDirty(DataRowState.Added);

                    liquidacaoRendaFixaInsert.IdCliente = idCarteiraMae;
                    liquidacaoRendaFixaInsert.Save();
                }
                #endregion

                decimal cambio = 1; //TODO PEGAR PELO CAMBIO DE CONVERSAO ENTRE AS MOEDAS DAS CARTEIRAS

                if (dataDiaFilha > data)
                {
                    //Trata BOLSA
                    PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                    posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                                posicaoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                                posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                    posicaoBolsaHistoricoCollection.Query.Load();

                    foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
                    {
                        PosicaoBolsa posicaoInsert = new PosicaoBolsa();
                        posicaoInsert.CdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;
                        posicaoInsert.DataVencimento = posicaoBolsaHistorico.DataVencimento;
                        posicaoInsert.IdAgente = posicaoBolsaHistorico.IdAgente;
                        posicaoInsert.Quantidade = posicaoBolsaHistorico.Quantidade;
                        posicaoInsert.QuantidadeBloqueada = posicaoBolsaHistorico.QuantidadeBloqueada;
                        posicaoInsert.QuantidadeInicial = posicaoBolsaHistorico.QuantidadeInicial;
                        posicaoInsert.TipoMercado = posicaoBolsaHistorico.TipoMercado;

                        posicaoInsert.IdCliente = idCarteiraMae;
                        posicaoInsert.PUCusto = posicaoBolsaHistorico.PUCusto.Value * cambio;
                        posicaoInsert.PUCustoLiquido = posicaoBolsaHistorico.PUCustoLiquido.Value * cambio;
                        posicaoInsert.PUMercado = posicaoBolsaHistorico.PUMercado.Value * cambio;
                        posicaoInsert.ResultadoRealizar = Math.Round(posicaoBolsaHistorico.ResultadoRealizar.Value * cambio, 2);
                        posicaoInsert.ValorCustoLiquido = Math.Round(posicaoBolsaHistorico.ValorCustoLiquido.Value * cambio, 2);
                        posicaoInsert.ValorMercado = Math.Round(posicaoBolsaHistorico.ValorMercado.Value * cambio, 2);
                        posicaoInsert.Save();
                    }

                    //Trata TERMOBOLSA
                    PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
                    posicaoTermoBolsaHistoricoCollection.Query.Where(posicaoTermoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                                     posicaoTermoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                                     posicaoTermoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                    posicaoTermoBolsaHistoricoCollection.Query.Load();

                    foreach (PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico in posicaoTermoBolsaHistoricoCollection)
                    {
                        PosicaoTermoBolsa posicaoInsert = new PosicaoTermoBolsa();
                        posicaoInsert.CdAtivoBolsa = posicaoTermoBolsaHistorico.CdAtivoBolsa;
                        posicaoInsert.DataOperacao = posicaoTermoBolsaHistorico.DataOperacao;
                        posicaoInsert.DataVencimento = posicaoTermoBolsaHistorico.DataVencimento;
                        posicaoInsert.IdAgente = posicaoTermoBolsaHistorico.IdAgente;

                        posicaoInsert.IdCliente = idCarteiraMae;
                        posicaoInsert.IdIndice = posicaoTermoBolsaHistorico.IdIndice;
                        posicaoInsert.IdOperacao = posicaoTermoBolsaHistorico.IdOperacao;
                        posicaoInsert.IdTrader = posicaoTermoBolsaHistorico.IdTrader;
                        posicaoInsert.NumeroContrato = posicaoTermoBolsaHistorico.NumeroContrato;
                        posicaoInsert.PrazoDecorrer = posicaoTermoBolsaHistorico.PrazoDecorrer;
                        posicaoInsert.PrazoTotal = posicaoTermoBolsaHistorico.PrazoTotal;
                        posicaoInsert.PUCustoLiquidoAcao = posicaoTermoBolsaHistorico.PUCustoLiquidoAcao.Value * cambio;
                        posicaoInsert.PUMercado = posicaoTermoBolsaHistorico.PUMercado.Value * cambio;
                        posicaoInsert.PUTermo = posicaoTermoBolsaHistorico.PUTermo.Value * cambio;
                        posicaoInsert.PUTermoLiquido = posicaoTermoBolsaHistorico.PUTermoLiquido.Value * cambio;
                        posicaoInsert.Quantidade = posicaoTermoBolsaHistorico.Quantidade.Value * cambio;
                        posicaoInsert.QuantidadeInicial = posicaoTermoBolsaHistorico.QuantidadeInicial.Value * cambio;
                        posicaoInsert.RendimentoApropriado = posicaoTermoBolsaHistorico.RendimentoApropriado.Value * cambio;
                        posicaoInsert.RendimentoApropriar = posicaoTermoBolsaHistorico.RendimentoApropriar.Value * cambio;
                        posicaoInsert.RendimentoDia = posicaoTermoBolsaHistorico.RendimentoDia.Value * cambio;
                        posicaoInsert.RendimentoTotal = posicaoTermoBolsaHistorico.RendimentoTotal.Value * cambio;
                        posicaoInsert.TaxaAno = posicaoTermoBolsaHistorico.TaxaAno.Value;
                        posicaoInsert.TaxaMTM = posicaoTermoBolsaHistorico.TaxaMTM.Value;
                        posicaoInsert.ValorCorrigido = posicaoTermoBolsaHistorico.ValorCorrigido.Value * cambio;
                        posicaoInsert.ValorCurva = posicaoTermoBolsaHistorico.ValorCurva.Value * cambio;
                        posicaoInsert.ValorMercado = posicaoTermoBolsaHistorico.ValorMercado.Value * cambio;
                        posicaoInsert.ValorTermo = posicaoTermoBolsaHistorico.ValorTermo.Value * cambio;
                        posicaoInsert.ValorTermoLiquido = posicaoTermoBolsaHistorico.ValorTermoLiquido.Value * cambio;

                        posicaoInsert.Save();
                    }

                    //Trata EMPRESTIMOBOLSA
                    PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
                    posicaoEmprestimoBolsaHistoricoCollection.Query.Where(posicaoEmprestimoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                                          posicaoEmprestimoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                                          posicaoEmprestimoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                    posicaoEmprestimoBolsaHistoricoCollection.Query.Load();

                    foreach (PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico in posicaoEmprestimoBolsaHistoricoCollection)
                    {
                        PosicaoEmprestimoBolsa posicaoInsert = new PosicaoEmprestimoBolsa();
                        posicaoInsert.CdAtivoBolsa = posicaoEmprestimoBolsaHistorico.CdAtivoBolsa;
                        posicaoInsert.DataRegistro = posicaoEmprestimoBolsaHistorico.DataRegistro;
                        posicaoInsert.DataVencimento = posicaoEmprestimoBolsaHistorico.DataVencimento;
                        posicaoInsert.IdAgente = posicaoEmprestimoBolsaHistorico.IdAgente;

                        posicaoInsert.IdCliente = idCarteiraMae;
                        posicaoInsert.IdOperacao = posicaoEmprestimoBolsaHistorico.IdOperacao;
                        posicaoInsert.IdTrader = posicaoEmprestimoBolsaHistorico.IdTrader;
                        posicaoInsert.NumeroContrato = posicaoEmprestimoBolsaHistorico.NumeroContrato;
                        posicaoInsert.PontaEmprestimo = posicaoEmprestimoBolsaHistorico.PontaEmprestimo;
                        posicaoInsert.PULiquidoOriginal = posicaoEmprestimoBolsaHistorico.PULiquidoOriginal * cambio;
                        posicaoInsert.PUMercado = posicaoEmprestimoBolsaHistorico.PUMercado * cambio;
                        posicaoInsert.Quantidade = posicaoEmprestimoBolsaHistorico.Quantidade.Value;
                        posicaoInsert.TaxaComissao = posicaoEmprestimoBolsaHistorico.TaxaComissao;
                        posicaoInsert.TaxaOperacao = posicaoEmprestimoBolsaHistorico.TaxaOperacao;
                        posicaoInsert.TipoEmprestimo = posicaoEmprestimoBolsaHistorico.TipoEmprestimo;
                        posicaoInsert.ValorBase = posicaoEmprestimoBolsaHistorico.ValorBase.Value * cambio;
                        posicaoInsert.ValorCorrigidoCBLC = posicaoEmprestimoBolsaHistorico.ValorCorrigidoCBLC.Value * cambio;
                        posicaoInsert.ValorCorrigidoComissao = posicaoEmprestimoBolsaHistorico.ValorCorrigidoComissao.Value * cambio;
                        posicaoInsert.ValorCorrigidoJuros = posicaoEmprestimoBolsaHistorico.ValorCorrigidoJuros.Value * cambio;
                        posicaoInsert.ValorDiarioCBLC = posicaoEmprestimoBolsaHistorico.ValorDiarioCBLC.Value * cambio;
                        posicaoInsert.ValorDiarioComissao = posicaoEmprestimoBolsaHistorico.ValorDiarioComissao.Value * cambio;
                        posicaoInsert.ValorDiarioJuros = posicaoEmprestimoBolsaHistorico.ValorDiarioJuros.Value * cambio;
                        posicaoInsert.ValorMercado = posicaoEmprestimoBolsaHistorico.ValorMercado.Value * cambio;

                        posicaoInsert.Save();
                    }

                    //Trata BMF
                    PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
                    posicaoBMFHistoricoCollection.Query.Where(posicaoBMFHistoricoCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                              posicaoBMFHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                              posicaoBMFHistoricoCollection.Query.DataHistorico.Equal(data));
                    posicaoBMFHistoricoCollection.Query.Load();

                    foreach (PosicaoBMFHistorico posicaoBMFHistorico in posicaoBMFHistoricoCollection)
                    {
                        PosicaoBMF posicaoInsert = new PosicaoBMF();
                        posicaoInsert.CdAtivoBMF = posicaoBMFHistorico.CdAtivoBMF;
                        posicaoInsert.DataVencimento = posicaoBMFHistorico.DataVencimento;
                        posicaoInsert.IdAgente = posicaoBMFHistorico.IdAgente;
                        posicaoInsert.Quantidade = posicaoBMFHistorico.Quantidade;
                        posicaoInsert.QuantidadeInicial = posicaoBMFHistorico.QuantidadeInicial;
                        posicaoInsert.Serie = posicaoBMFHistorico.Serie;
                        posicaoInsert.TipoMercado = posicaoBMFHistorico.TipoMercado;

                        posicaoInsert.IdCliente = idCarteiraMae;
                        posicaoInsert.PUCusto = posicaoBMFHistorico.PUCusto.Value * cambio;
                        posicaoInsert.PUCustoLiquido = posicaoBMFHistorico.PUCustoLiquido.Value * cambio;
                        posicaoInsert.PUMercado = posicaoBMFHistorico.PUMercado.Value * cambio;
                        posicaoInsert.ResultadoRealizar = Math.Round(posicaoBMFHistorico.ResultadoRealizar.Value * cambio, 2);
                        posicaoInsert.ValorCustoLiquido = Math.Round(posicaoBMFHistorico.ValorCustoLiquido.Value * cambio, 2);
                        posicaoInsert.ValorMercado = Math.Round(posicaoBMFHistorico.ValorMercado.Value * cambio, 2);
                        posicaoInsert.AjusteAcumulado = Math.Round(posicaoBMFHistorico.AjusteAcumulado.Value * cambio, 2);
                        posicaoInsert.AjusteDiario = Math.Round(posicaoBMFHistorico.AjusteDiario.Value * cambio, 2);
                        posicaoInsert.Save();
                    }

                    //Trata RENDAFIXA
                    PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                    posicaoRendaFixaHistoricoCollection.Query.Where(posicaoRendaFixaHistoricoCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                           posicaoRendaFixaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                           posicaoRendaFixaHistoricoCollection.Query.DataHistorico.Equal(data));
                    posicaoRendaFixaHistoricoCollection.Query.Load();

                    foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoCollection)
                    {
                        PosicaoRendaFixa posicaoRendaFixaInsert = new PosicaoRendaFixa();

                        posicaoRendaFixaInsert.DataLiquidacao = posicaoRendaFixaHistorico.DataLiquidacao;
                        posicaoRendaFixaInsert.DataOperacao = posicaoRendaFixaHistorico.DataOperacao;
                        posicaoRendaFixaInsert.DataVencimento = posicaoRendaFixaHistorico.DataVencimento;
                        posicaoRendaFixaInsert.DataVolta = posicaoRendaFixaHistorico.DataVolta;
                        posicaoRendaFixaInsert.IdTitulo = posicaoRendaFixaHistorico.IdTitulo;
                        posicaoRendaFixaInsert.Quantidade = posicaoRendaFixaHistorico.Quantidade;
                        posicaoRendaFixaInsert.QuantidadeBloqueada = posicaoRendaFixaHistorico.QuantidadeBloqueada;
                        posicaoRendaFixaInsert.QuantidadeInicial = posicaoRendaFixaHistorico.QuantidadeInicial;
                        posicaoRendaFixaInsert.TaxaOperacao = posicaoRendaFixaHistorico.TaxaOperacao;
                        posicaoRendaFixaInsert.TipoNegociacao = posicaoRendaFixaHistorico.TipoNegociacao;
                        posicaoRendaFixaInsert.TipoOperacao = posicaoRendaFixaHistorico.TipoOperacao;

                        posicaoRendaFixaInsert.IdCliente = idCarteiraMae;
                        posicaoRendaFixaInsert.PUOperacao = posicaoRendaFixaHistorico.PUOperacao.Value * cambio;
                        posicaoRendaFixaInsert.PUCurva = posicaoRendaFixaHistorico.PUCurva.Value * cambio;
                        posicaoRendaFixaInsert.PUJuros = posicaoRendaFixaHistorico.PUJuros.Value * cambio;
                        posicaoRendaFixaInsert.PUMercado = posicaoRendaFixaHistorico.PUMercado.Value * cambio;
                        posicaoRendaFixaInsert.PUOperacao = posicaoRendaFixaHistorico.PUOperacao.Value * cambio;
                        posicaoRendaFixaInsert.ValorCurva = Math.Round(posicaoRendaFixaHistorico.ValorCurva.Value * cambio, 2);
                        posicaoRendaFixaInsert.ValorIOF = Math.Round(posicaoRendaFixaHistorico.ValorIOF.Value * cambio, 2);
                        posicaoRendaFixaInsert.ValorIR = Math.Round(posicaoRendaFixaHistorico.ValorIR.Value * cambio, 2);
                        posicaoRendaFixaInsert.ValorJuros = Math.Round(posicaoRendaFixaHistorico.ValorJuros.Value * cambio, 2);
                        posicaoRendaFixaInsert.ValorMercado = Math.Round(posicaoRendaFixaHistorico.ValorMercado.Value * cambio, 2);
                        posicaoRendaFixaInsert.PUCorrecao = posicaoRendaFixaHistorico.PUCorrecao.Value * cambio;
                        posicaoRendaFixaInsert.ValorCorrecao = Math.Round(posicaoRendaFixaHistorico.ValorCorrecao.Value * cambio, 2);

                        if (posicaoRendaFixaHistorico.PUVolta.HasValue)
                        {
                            posicaoRendaFixaInsert.PUVolta = posicaoRendaFixaHistorico.PUVolta.Value * cambio;
                        }
                        if (posicaoRendaFixaHistorico.ValorVolta.HasValue)
                        {
                            posicaoRendaFixaInsert.ValorVolta = Math.Round(posicaoRendaFixaHistorico.ValorVolta.Value * cambio, 2);
                        }

                        posicaoRendaFixaInsert.Save();
                    }

                    //Trata SWAP
                    PosicaoSwapHistoricoCollection posicaoSwapHistoricoCollection = new PosicaoSwapHistoricoCollection();
                    posicaoSwapHistoricoCollection.Query.Where(posicaoSwapHistoricoCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                               posicaoSwapHistoricoCollection.Query.DataHistorico.Equal(data));
                    posicaoSwapHistoricoCollection.Query.Load();

                    foreach (PosicaoSwapHistorico posicaoSwapHistorico in posicaoSwapHistoricoCollection)
                    {
                        PosicaoSwap posicaoSwapInsert = new PosicaoSwap();

                        posicaoSwapInsert.IdCliente = idCarteiraMae;
                        posicaoSwapInsert.BaseAno = posicaoSwapHistorico.BaseAno;
                        posicaoSwapInsert.BaseAnoContraParte = posicaoSwapHistorico.BaseAnoContraParte;
                        posicaoSwapInsert.ComGarantia = posicaoSwapHistorico.ComGarantia;
                        posicaoSwapInsert.ContagemDias = posicaoSwapHistorico.ContagemDias;
                        posicaoSwapInsert.ContagemDiasContraParte = posicaoSwapHistorico.ContagemDiasContraParte;
                        posicaoSwapInsert.DataEmissao = posicaoSwapHistorico.DataEmissao;
                        posicaoSwapInsert.DataVencimento = posicaoSwapHistorico.DataVencimento;
                        posicaoSwapInsert.DiasCorridos = posicaoSwapHistorico.DiasCorridos;
                        posicaoSwapInsert.DiasUteis = posicaoSwapHistorico.DiasUteis;
                        posicaoSwapInsert.IdAgente = posicaoSwapHistorico.IdAgente;
                        posicaoSwapInsert.IdAssessor = posicaoSwapHistorico.IdAssessor;
                        posicaoSwapInsert.IdEstrategia = posicaoSwapHistorico.IdEstrategia;
                        posicaoSwapInsert.IdIndice = posicaoSwapHistorico.IdIndice;
                        posicaoSwapInsert.IdIndiceContraParte = posicaoSwapHistorico.IdIndiceContraParte;
                        posicaoSwapInsert.NumeroContrato = posicaoSwapHistorico.NumeroContrato;
                        posicaoSwapInsert.Percentual = posicaoSwapHistorico.Percentual;
                        posicaoSwapInsert.PercentualContraParte = posicaoSwapHistorico.PercentualContraParte;
                        posicaoSwapInsert.TaxaJuros = posicaoSwapHistorico.TaxaJuros;
                        posicaoSwapInsert.TaxaJurosContraParte = posicaoSwapHistorico.TaxaJurosContraParte;
                        posicaoSwapInsert.TipoApropriacao = posicaoSwapHistorico.TipoApropriacao;
                        posicaoSwapInsert.TipoApropriacaoContraParte = posicaoSwapHistorico.TipoApropriacaoContraParte;
                        posicaoSwapInsert.TipoPonta = posicaoSwapHistorico.TipoPonta;
                        posicaoSwapInsert.TipoPontaContraParte = posicaoSwapHistorico.TipoPontaContraParte;
                        posicaoSwapInsert.TipoRegistro = posicaoSwapHistorico.TipoRegistro;

                        posicaoSwapInsert.Saldo = Math.Round(posicaoSwapHistorico.Saldo.Value * cambio, 2);
                        posicaoSwapInsert.ValorBase = Math.Round(posicaoSwapHistorico.ValorBase.Value * cambio, 2);
                        posicaoSwapInsert.ValorParte = Math.Round(posicaoSwapHistorico.ValorContraParte.Value * cambio, 2);
                        posicaoSwapInsert.ValorContraParte = Math.Round(posicaoSwapHistorico.ValorContraParte.Value * cambio, 2);

                        if (posicaoSwapHistorico.ValorIndicePartida.HasValue)
                        {
                            posicaoSwapInsert.ValorIndicePartida = Math.Round(posicaoSwapHistorico.ValorIndicePartida.Value * cambio, 2);
                        }

                        if (posicaoSwapHistorico.ValorIndicePartidaContraParte.HasValue)
                        {
                            posicaoSwapInsert.ValorIndicePartidaContraParte = Math.Round(posicaoSwapHistorico.ValorIndicePartidaContraParte.Value * cambio, 2);
                        }

                        posicaoSwapInsert.Save();
                    }

                    //Trata COTAFUNDO
                    PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                    posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                                posicaoFundoHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                                posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(data));
                    posicaoFundoHistoricoCollection.Query.Load();

                    foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
                    {
                        PosicaoFundo posicaoFundoInsert = new PosicaoFundo();
                        posicaoFundoInsert.CotaAplicacao = posicaoFundoHistorico.CotaAplicacao;
                        posicaoFundoInsert.CotaDia = posicaoFundoHistorico.CotaDia;
                        posicaoFundoInsert.DataAplicacao = posicaoFundoHistorico.DataAplicacao;
                        posicaoFundoInsert.DataConversao = posicaoFundoHistorico.DataConversao;
                        posicaoFundoInsert.DataUltimaCobrancaIR = posicaoFundoHistorico.DataUltimaCobrancaIR;
                        posicaoFundoInsert.IdCarteira = posicaoFundoHistorico.IdCarteira;
                        posicaoFundoInsert.IdOperacao = posicaoFundoHistorico.IdOperacao;
                        posicaoFundoInsert.Quantidade = posicaoFundoHistorico.Quantidade;
                        posicaoFundoInsert.QuantidadeAntesCortes = posicaoFundoHistorico.QuantidadeAntesCortes;
                        posicaoFundoInsert.PosicaoIncorporada = posicaoFundoHistorico.PosicaoIncorporada;
                        posicaoFundoInsert.QuantidadeBloqueada = posicaoFundoHistorico.QuantidadeBloqueada;
                        posicaoFundoInsert.QuantidadeInicial = posicaoFundoHistorico.QuantidadeInicial;
                        posicaoFundoInsert.ValorIOFVirtual = posicaoFundoHistorico.ValorIOFVirtual;

                        posicaoFundoInsert.IdCliente = idCarteiraMae;
                        posicaoFundoInsert.ValorAplicacao = Math.Round(posicaoFundoHistorico.ValorAplicacao.Value * cambio, 2);
                        posicaoFundoInsert.ValorBruto = Math.Round(posicaoFundoHistorico.ValorBruto.Value * cambio, 2);
                        posicaoFundoInsert.ValorIOF = Math.Round(posicaoFundoHistorico.ValorIOF.Value * cambio, 2);
                        posicaoFundoInsert.ValorIR = Math.Round(posicaoFundoHistorico.ValorIR.Value * cambio, 2);
                        posicaoFundoInsert.ValorLiquido = Math.Round(posicaoFundoHistorico.ValorLiquido.Value * cambio, 2);
                        posicaoFundoInsert.ValorPerformance = Math.Round(posicaoFundoHistorico.ValorPerformance.Value * cambio, 2);
                        posicaoFundoInsert.ValorRendimento = Math.Round(posicaoFundoHistorico.ValorRendimento.Value * cambio, 2);
                        posicaoFundoInsert.Save();
                    }

                    //Trata LIQUIDACAO
                    LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
                    liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                     liquidacaoHistoricoCollection.Query.DataVencimento.GreaterThanOrEqual(data),
                                                     liquidacaoHistoricoCollection.Query.DataLancamento.LessThanOrEqual(data),
                                                     liquidacaoHistoricoCollection.Query.DataHistorico.Equal(data),
                                                     liquidacaoHistoricoCollection.Query.Origem.NotIn(OrigemLancamentoLiquidacao.Cotista.Aplicacao,
                                                                                                      OrigemLancamentoLiquidacao.Cotista.Resgate,
                                                                                                      OrigemLancamentoLiquidacao.Cotista.AplicacaoConverter));
                    if (temTaxasMae)
                    {
                        liquidacaoHistoricoCollection.Query.Origem.NotIn(OrigemLancamentoLiquidacao.Provisao.PagtoTaxaAdministracao,
                                                                         OrigemLancamentoLiquidacao.Provisao.PagtoTaxaGestao,
                                                                         OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance);
                    }

                    liquidacaoHistoricoCollection.Query.Load();

                    LiquidacaoHistoricoCollection liquidacaoHistoricoCollection2 = new LiquidacaoHistoricoCollection();
                    liquidacaoHistoricoCollection2.Query.Where(liquidacaoHistoricoCollection2.Query.IdCliente.Equal(idCarteiraFilha),
                                                     liquidacaoHistoricoCollection2.Query.DataVencimento.GreaterThanOrEqual(data),
                                                     liquidacaoHistoricoCollection2.Query.DataLancamento.LessThan(data),
                                                     liquidacaoHistoricoCollection2.Query.DataLancamento.NotEqual(liquidacaoHistoricoCollection2.Query.DataVencimento),
                                                     liquidacaoHistoricoCollection2.Query.DataHistorico.Equal(data),
                                                     liquidacaoHistoricoCollection2.Query.Origem.In(OrigemLancamentoLiquidacao.Cotista.Resgate));
                    liquidacaoHistoricoCollection2.Query.Load();

                    liquidacaoHistoricoCollection.Combine(liquidacaoHistoricoCollection2);

                    foreach (LiquidacaoHistorico liquidacaoHistorico in liquidacaoHistoricoCollection)
                    {
                        int idContaFilha = liquidacaoHistorico.IdConta.Value;

                        ContaCorrente contaCorrente = new ContaCorrente();
                        contaCorrente.LoadByPrimaryKey(idContaFilha);
                        int idMoedaFilha = contaCorrente.IdMoeda.Value;

                        ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
                        contaCorrenteCollection.Query.Select(contaCorrenteCollection.Query.IdConta);
                        contaCorrenteCollection.Query.Where(contaCorrenteCollection.Query.IdPessoa.Equal(idCarteiraMae),
                                                            contaCorrenteCollection.Query.IdMoeda.Equal(idMoedaFilha));
                        contaCorrenteCollection.Query.Load();

                        if (contaCorrenteCollection.Count > 0)
                        {
                            int idContaMae = contaCorrenteCollection[0].IdConta.Value;

                            Liquidacao liquidacaoInsert = new Liquidacao();
                            liquidacaoInsert.DataLancamento = liquidacaoHistorico.DataLancamento;
                            liquidacaoInsert.DataVencimento = liquidacaoHistorico.DataVencimento;
                            liquidacaoInsert.Descricao = liquidacaoHistorico.Descricao;
                            liquidacaoInsert.Fonte = liquidacaoHistorico.Fonte;
                            liquidacaoInsert.IdAgente = liquidacaoHistorico.IdAgente;
                            liquidacaoInsert.IdConta = idContaMae;
                            liquidacaoInsert.IdentificadorOrigem = liquidacaoHistorico.IdentificadorOrigem;
                            liquidacaoInsert.Origem = liquidacaoHistorico.Origem;
                            liquidacaoInsert.Situacao = liquidacaoHistorico.Situacao;

                            liquidacaoInsert.IdCliente = idCarteiraMae;
                            liquidacaoInsert.Valor = Math.Round(liquidacaoHistorico.Valor.Value * cambio, 2);
                            liquidacaoInsert.Save();
                        }
                    }
                }
                else
                {
                    //Trata BOLSA
                    PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                    posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                       posicaoBolsaCollection.Query.Quantidade.NotEqual(0));
                    posicaoBolsaCollection.Query.Load();

                    foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
                    {
                        int idPosicao = posicaoBolsa.IdPosicao.Value;

                        PosicaoBolsa posicaoInsert = new PosicaoBolsa();
                        posicaoInsert.LoadByPrimaryKey(idPosicao);
                        posicaoInsert.MarkAllColumnsAsDirty(DataRowState.Added);

                        posicaoInsert.IdCliente = idCarteiraMae;
                        posicaoInsert.PUCusto = posicaoBolsa.PUCusto.Value * cambio;
                        posicaoInsert.PUCustoLiquido = posicaoBolsa.PUCustoLiquido.Value * cambio;
                        posicaoInsert.ResultadoRealizar = Math.Round(posicaoBolsa.ResultadoRealizar.Value * cambio, 2);
                        posicaoInsert.ValorCustoLiquido = Math.Round(posicaoBolsa.ValorCustoLiquido.Value * cambio, 2);
                        posicaoInsert.ValorMercado = Math.Round(posicaoBolsa.ValorMercado.Value * cambio, 2);

                        posicaoInsert.PUMercado = posicaoBolsa.PUMercado.Value * cambio;
                        posicaoInsert.Save();
                    }

                    //Trata TERMOBOLSA
                    PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                    posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                            posicaoTermoBolsaCollection.Query.Quantidade.NotEqual(0));
                    posicaoTermoBolsaCollection.Query.Load();

                    foreach (PosicaoTermoBolsa posicaoTermoBolsa in posicaoTermoBolsaCollection)
                    {
                        PosicaoTermoBolsa posicaoInsert = new PosicaoTermoBolsa();
                        posicaoInsert.CdAtivoBolsa = posicaoTermoBolsa.CdAtivoBolsa;
                        posicaoInsert.DataOperacao = posicaoTermoBolsa.DataOperacao;
                        posicaoInsert.DataVencimento = posicaoTermoBolsa.DataVencimento;
                        posicaoInsert.IdAgente = posicaoTermoBolsa.IdAgente;

                        posicaoInsert.IdCliente = idCarteiraMae;
                        posicaoInsert.IdIndice = posicaoTermoBolsa.IdIndice;
                        posicaoInsert.IdOperacao = posicaoTermoBolsa.IdOperacao;
                        posicaoInsert.IdTrader = posicaoTermoBolsa.IdTrader;
                        posicaoInsert.NumeroContrato = posicaoTermoBolsa.NumeroContrato;
                        posicaoInsert.PrazoDecorrer = posicaoTermoBolsa.PrazoDecorrer;
                        posicaoInsert.PrazoTotal = posicaoTermoBolsa.PrazoTotal;
                        posicaoInsert.PUCustoLiquidoAcao = posicaoTermoBolsa.PUCustoLiquidoAcao.Value * cambio;
                        posicaoInsert.PUMercado = posicaoTermoBolsa.PUMercado.Value * cambio;
                        posicaoInsert.PUTermo = posicaoTermoBolsa.PUTermo.Value * cambio;
                        posicaoInsert.PUTermoLiquido = posicaoTermoBolsa.PUTermoLiquido.Value * cambio;
                        posicaoInsert.Quantidade = posicaoTermoBolsa.Quantidade.Value * cambio;
                        posicaoInsert.QuantidadeInicial = posicaoTermoBolsa.QuantidadeInicial.Value * cambio;
                        posicaoInsert.RendimentoApropriado = posicaoTermoBolsa.RendimentoApropriado.Value * cambio;
                        posicaoInsert.RendimentoApropriar = posicaoTermoBolsa.RendimentoApropriar.Value * cambio;
                        posicaoInsert.RendimentoDia = posicaoTermoBolsa.RendimentoDia.Value * cambio;
                        posicaoInsert.RendimentoTotal = posicaoTermoBolsa.RendimentoTotal.Value * cambio;
                        posicaoInsert.TaxaAno = posicaoTermoBolsa.TaxaAno.Value;
                        posicaoInsert.TaxaMTM = posicaoTermoBolsa.TaxaMTM.Value;
                        posicaoInsert.ValorCorrigido = posicaoTermoBolsa.ValorCorrigido.Value * cambio;
                        posicaoInsert.ValorCurva = posicaoTermoBolsa.ValorCurva.Value * cambio;
                        posicaoInsert.ValorMercado = posicaoTermoBolsa.ValorMercado.Value * cambio;
                        posicaoInsert.ValorTermo = posicaoTermoBolsa.ValorTermo.Value * cambio;
                        posicaoInsert.ValorTermoLiquido = posicaoTermoBolsa.ValorTermoLiquido.Value * cambio;

                        posicaoInsert.Save();
                    }

                    //Trata EMPRESTIMOBOLSA
                    PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                    posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                                 posicaoEmprestimoBolsaCollection.Query.Quantidade.NotEqual(0));
                    posicaoEmprestimoBolsaCollection.Query.Load();

                    foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoEmprestimoBolsaCollection)
                    {
                        PosicaoEmprestimoBolsa posicaoInsert = new PosicaoEmprestimoBolsa();
                        posicaoInsert.CdAtivoBolsa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                        posicaoInsert.DataRegistro = posicaoEmprestimoBolsa.DataRegistro;
                        posicaoInsert.DataVencimento = posicaoEmprestimoBolsa.DataVencimento;
                        posicaoInsert.IdAgente = posicaoEmprestimoBolsa.IdAgente;

                        posicaoInsert.IdCliente = idCarteiraMae;
                        posicaoInsert.IdOperacao = posicaoEmprestimoBolsa.IdOperacao;
                        posicaoInsert.IdTrader = posicaoEmprestimoBolsa.IdTrader;
                        posicaoInsert.NumeroContrato = posicaoEmprestimoBolsa.NumeroContrato;
                        posicaoInsert.PontaEmprestimo = posicaoEmprestimoBolsa.PontaEmprestimo;
                        posicaoInsert.PULiquidoOriginal = posicaoEmprestimoBolsa.PULiquidoOriginal * cambio;
                        posicaoInsert.PUMercado = posicaoEmprestimoBolsa.PUMercado * cambio;
                        posicaoInsert.Quantidade = posicaoEmprestimoBolsa.Quantidade.Value;
                        posicaoInsert.TaxaComissao = posicaoEmprestimoBolsa.TaxaComissao;
                        posicaoInsert.TaxaOperacao = posicaoEmprestimoBolsa.TaxaOperacao;
                        posicaoInsert.TipoEmprestimo = posicaoEmprestimoBolsa.TipoEmprestimo;
                        posicaoInsert.ValorBase = posicaoEmprestimoBolsa.ValorBase.Value * cambio;
                        posicaoInsert.ValorCorrigidoCBLC = posicaoEmprestimoBolsa.ValorCorrigidoCBLC.Value * cambio;
                        posicaoInsert.ValorCorrigidoComissao = posicaoEmprestimoBolsa.ValorCorrigidoComissao.Value * cambio;
                        posicaoInsert.ValorCorrigidoJuros = posicaoEmprestimoBolsa.ValorCorrigidoJuros.Value * cambio;
                        posicaoInsert.ValorDiarioCBLC = posicaoEmprestimoBolsa.ValorDiarioCBLC.Value * cambio;
                        posicaoInsert.ValorDiarioComissao = posicaoEmprestimoBolsa.ValorDiarioComissao.Value * cambio;
                        posicaoInsert.ValorDiarioJuros = posicaoEmprestimoBolsa.ValorDiarioJuros.Value * cambio;
                        posicaoInsert.ValorMercado = posicaoEmprestimoBolsa.ValorMercado.Value * cambio;

                        posicaoInsert.Save();
                    }

                    //Trata BMF
                    PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
                    posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                     posicaoBMFCollection.Query.Quantidade.NotEqual(0));
                    posicaoBMFCollection.Query.Load();

                    foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
                    {
                        int idPosicao = posicaoBMF.IdPosicao.Value;

                        PosicaoBMF posicaoInsert = new PosicaoBMF();
                        posicaoInsert.LoadByPrimaryKey(idPosicao);
                        posicaoInsert.MarkAllColumnsAsDirty(DataRowState.Added);

                        posicaoInsert.IdCliente = idCarteiraMae;
                        posicaoInsert.PUCusto = posicaoBMF.PUCusto.Value * cambio;
                        posicaoInsert.PUCustoLiquido = posicaoBMF.PUCustoLiquido.Value * cambio;
                        posicaoInsert.PUMercado = posicaoBMF.PUMercado.Value * cambio;
                        posicaoInsert.ResultadoRealizar = Math.Round(posicaoBMF.ResultadoRealizar.Value * cambio, 2);
                        posicaoInsert.ValorCustoLiquido = Math.Round(posicaoBMF.ValorCustoLiquido.Value * cambio, 2);
                        posicaoInsert.ValorMercado = Math.Round(posicaoBMF.ValorMercado.Value * cambio, 2);
                        posicaoInsert.AjusteAcumulado = Math.Round(posicaoBMF.AjusteAcumulado.Value * cambio, 2);
                        posicaoInsert.AjusteDiario = Math.Round(posicaoBMF.AjusteDiario.Value * cambio, 2);
                        posicaoInsert.Save();
                    }

                    //Trata RENDAFIXA
                    PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                    posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                     posicaoRendaFixaCollection.Query.Quantidade.NotEqual(0));
                    posicaoRendaFixaCollection.Query.Load();

                    foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                    {
                        int idPosicao = posicaoRendaFixa.IdPosicao.Value;

                        PosicaoRendaFixa posicaoRendaFixaInsert = new PosicaoRendaFixa();
                        posicaoRendaFixaInsert.LoadByPrimaryKey(idPosicao);
                        posicaoRendaFixaInsert.MarkAllColumnsAsDirty(DataRowState.Added);

                        posicaoRendaFixaInsert.IdCliente = idCarteiraMae;
                        posicaoRendaFixaInsert.PUCurva = posicaoRendaFixa.PUCurva.Value * cambio;
                        posicaoRendaFixaInsert.PUJuros = posicaoRendaFixa.PUJuros.Value * cambio;
                        posicaoRendaFixaInsert.PUMercado = posicaoRendaFixa.PUMercado.Value * cambio;
                        posicaoRendaFixaInsert.PUOperacao = posicaoRendaFixa.PUOperacao.Value * cambio;
                        posicaoRendaFixaInsert.ValorCurva = Math.Round(posicaoRendaFixa.ValorCurva.Value * cambio, 2);
                        posicaoRendaFixaInsert.ValorIOF = Math.Round(posicaoRendaFixa.ValorIOF.Value * cambio, 2);
                        posicaoRendaFixaInsert.ValorIR = Math.Round(posicaoRendaFixa.ValorIR.Value * cambio, 2);
                        posicaoRendaFixaInsert.ValorJuros = Math.Round(posicaoRendaFixa.ValorJuros.Value * cambio, 2);
                        posicaoRendaFixaInsert.ValorMercado = Math.Round(posicaoRendaFixa.ValorMercado.Value * cambio, 2);

                        if (posicaoRendaFixa.PUVolta.HasValue)
                        {
                            posicaoRendaFixaInsert.PUVolta = posicaoRendaFixa.PUVolta.Value * cambio;
                        }
                        if (posicaoRendaFixa.ValorVolta.HasValue)
                        {
                            posicaoRendaFixaInsert.ValorVolta = Math.Round(posicaoRendaFixa.ValorVolta.Value * cambio, 2);
                        }

                        posicaoRendaFixaInsert.Save();
                    }

                    //Trata SWAP
                    PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();
                    posicaoSwapCollection.Query.Where(posicaoSwapCollection.Query.IdCliente.Equal(idCarteiraFilha));
                    posicaoSwapCollection.Query.Load();

                    foreach (PosicaoSwap posicaoSwap in posicaoSwapCollection)
                    {
                        int idPosicao = posicaoSwap.IdPosicao.Value;

                        PosicaoSwap posicaoSwapInsert = new PosicaoSwap();
                        posicaoSwapInsert.LoadByPrimaryKey(idPosicao);
                        posicaoSwapInsert.MarkAllColumnsAsDirty(DataRowState.Added);

                        posicaoSwapInsert.IdCliente = idCarteiraMae;
                        posicaoSwapInsert.BaseAno = posicaoSwap.BaseAno;
                        posicaoSwapInsert.BaseAnoContraParte = posicaoSwap.BaseAnoContraParte;
                        posicaoSwapInsert.ComGarantia = posicaoSwap.ComGarantia;
                        posicaoSwapInsert.ContagemDias = posicaoSwap.ContagemDias;
                        posicaoSwapInsert.ContagemDiasContraParte = posicaoSwap.ContagemDiasContraParte;
                        posicaoSwapInsert.DataEmissao = posicaoSwap.DataEmissao;
                        posicaoSwapInsert.DataVencimento = posicaoSwap.DataVencimento;
                        posicaoSwapInsert.DiasCorridos = posicaoSwap.DiasCorridos;
                        posicaoSwapInsert.DiasUteis = posicaoSwap.DiasUteis;
                        posicaoSwapInsert.IdAgente = posicaoSwap.IdAgente;
                        posicaoSwapInsert.IdAssessor = posicaoSwap.IdAssessor;
                        posicaoSwapInsert.IdEstrategia = posicaoSwap.IdEstrategia;
                        posicaoSwapInsert.IdIndice = posicaoSwap.IdIndice;
                        posicaoSwapInsert.IdIndiceContraParte = posicaoSwap.IdIndiceContraParte;
                        posicaoSwapInsert.NumeroContrato = posicaoSwap.NumeroContrato;
                        posicaoSwapInsert.Percentual = posicaoSwap.Percentual;
                        posicaoSwapInsert.PercentualContraParte = posicaoSwap.PercentualContraParte;
                        posicaoSwapInsert.TaxaJuros = posicaoSwap.TaxaJuros;
                        posicaoSwapInsert.TaxaJurosContraParte = posicaoSwap.TaxaJurosContraParte;
                        posicaoSwapInsert.TipoApropriacao = posicaoSwap.TipoApropriacao;
                        posicaoSwapInsert.TipoApropriacaoContraParte = posicaoSwap.TipoApropriacaoContraParte;
                        posicaoSwapInsert.TipoPonta = posicaoSwap.TipoPonta;
                        posicaoSwapInsert.TipoPontaContraParte = posicaoSwap.TipoPontaContraParte;
                        posicaoSwapInsert.TipoRegistro = posicaoSwap.TipoRegistro;

                        posicaoSwapInsert.Saldo = Math.Round(posicaoSwap.Saldo.Value * cambio, 2);
                        posicaoSwapInsert.ValorBase = Math.Round(posicaoSwap.ValorBase.Value * cambio, 2);
                        posicaoSwapInsert.ValorParte = Math.Round(posicaoSwap.ValorContraParte.Value * cambio, 2);
                        posicaoSwapInsert.ValorContraParte = Math.Round(posicaoSwap.ValorContraParte.Value * cambio, 2);

                        if (posicaoSwap.ValorIndicePartida.HasValue)
                        {
                            posicaoSwapInsert.ValorIndicePartida = Math.Round(posicaoSwap.ValorIndicePartida.Value * cambio, 2);
                        }

                        if (posicaoSwap.ValorIndicePartidaContraParte.HasValue)
                        {
                            posicaoSwapInsert.ValorIndicePartidaContraParte = Math.Round(posicaoSwap.ValorIndicePartidaContraParte.Value * cambio, 2);
                        }

                        posicaoSwapInsert.Save();
                    }

                    //Trata COTAFUNDO
                    PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                    posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                     posicaoFundoCollection.Query.Quantidade.NotEqual(0));
                    posicaoFundoCollection.Query.Load();

                    foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                    {
                        int idPosicao = posicaoFundo.IdPosicao.Value;

                        PosicaoFundo posicaoFundoInsert = new PosicaoFundo();
                        posicaoFundoInsert.LoadByPrimaryKey(idPosicao);
                        posicaoFundoInsert.MarkAllColumnsAsDirty(DataRowState.Added);

                        posicaoFundoInsert.IdCliente = idCarteiraMae;
                        posicaoFundoInsert.ValorAplicacao = Math.Round(posicaoFundo.ValorAplicacao.Value * cambio, 2);
                        posicaoFundoInsert.ValorBruto = Math.Round(posicaoFundo.ValorBruto.Value * cambio, 2);
                        posicaoFundoInsert.ValorIOF = Math.Round(posicaoFundo.ValorIOF.Value * cambio, 2);
                        posicaoFundoInsert.ValorIR = Math.Round(posicaoFundo.ValorIR.Value * cambio, 2);
                        posicaoFundoInsert.ValorLiquido = Math.Round(posicaoFundo.ValorLiquido.Value * cambio, 2);
                        posicaoFundoInsert.ValorPerformance = Math.Round(posicaoFundo.ValorPerformance.Value * cambio, 2);
                        posicaoFundoInsert.ValorRendimento = Math.Round(posicaoFundo.ValorRendimento.Value * cambio, 2);
                        posicaoFundoInsert.Save();
                    }

                    //Trata LIQUIDACAO
                    LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
                    liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                     liquidacaoCollection.Query.DataVencimento.GreaterThanOrEqual(data),
                                                     liquidacaoCollection.Query.DataLancamento.LessThanOrEqual(data),
                                                     liquidacaoCollection.Query.Origem.NotIn(OrigemLancamentoLiquidacao.Cotista.Aplicacao,
                                                                                             OrigemLancamentoLiquidacao.Cotista.Resgate,
                                                                                             OrigemLancamentoLiquidacao.Cotista.AplicacaoConverter));
                    if (temTaxasMae)
                    {
                        liquidacaoCollection.Query.Origem.NotIn(OrigemLancamentoLiquidacao.Provisao.PagtoTaxaAdministracao,
                                                                OrigemLancamentoLiquidacao.Provisao.PagtoTaxaGestao,
                                                                OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance);
                    }

                    liquidacaoCollection.Query.Load();

                    LiquidacaoCollection liquidacaoCollection2 = new LiquidacaoCollection();
                    liquidacaoCollection2.Query.Where(liquidacaoCollection2.Query.IdCliente.Equal(idCarteiraFilha),
                                                     liquidacaoCollection2.Query.DataVencimento.GreaterThanOrEqual(data),
                                                     liquidacaoCollection2.Query.DataLancamento.LessThan(data),
                                                     liquidacaoCollection2.Query.DataLancamento.NotEqual(liquidacaoCollection2.Query.DataVencimento),
                                                     liquidacaoCollection2.Query.Origem.In(OrigemLancamentoLiquidacao.Cotista.Resgate));
                    liquidacaoCollection2.Query.Load();

                    liquidacaoCollection.Combine(liquidacaoCollection2);

                    foreach (Liquidacao liquidacao in liquidacaoCollection)
                    {
                        int idLiquidacao = liquidacao.IdLiquidacao.Value;
                        int idContaFilha = liquidacao.IdConta.Value;

                        ContaCorrente contaCorrente = new ContaCorrente();
                        contaCorrente.LoadByPrimaryKey(idContaFilha);
                        int idMoedaFilha = contaCorrente.IdMoeda.Value;

                        ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
                        contaCorrenteCollection.Query.Select(contaCorrenteCollection.Query.IdConta);
                        contaCorrenteCollection.Query.Where(contaCorrenteCollection.Query.IdPessoa.Equal(idCarteiraMae),
                                                            contaCorrenteCollection.Query.IdMoeda.Equal(idMoedaFilha));
                        contaCorrenteCollection.Query.Load();

                        if (contaCorrenteCollection.Count > 0)
                        {
                            int idContaMae = contaCorrenteCollection[0].IdConta.Value;

                            Liquidacao liquidacaoInsert = new Liquidacao();
                            liquidacaoInsert.LoadByPrimaryKey(idLiquidacao);
                            liquidacaoInsert.MarkAllColumnsAsDirty(DataRowState.Added);

                            liquidacaoInsert.IdCliente = idCarteiraMae;
                            liquidacaoInsert.IdConta = idContaMae;
                            liquidacaoInsert.Valor = Math.Round(liquidacao.Valor.Value * cambio, 2);
                            liquidacaoInsert.Save();
                        }
                    }
                }

                //Trata SALDOCAIXA
                SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
                saldoCaixaCollection.Query.Select(saldoCaixaCollection.Query.SaldoAbertura,
                                                  saldoCaixaCollection.Query.SaldoFechamento,
                                                  saldoCaixaCollection.Query.IdConta);
                saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                 saldoCaixaCollection.Query.Data.Equal(data));
                saldoCaixaCollection.Query.Load();

                foreach (SaldoCaixa saldoCaixa in saldoCaixaCollection)
                {
                    int idContaFilha = saldoCaixa.IdConta.Value;

                    ContaCorrente contaCorrente = new ContaCorrente();
                    contaCorrente.LoadByPrimaryKey(idContaFilha);
                    int idMoedaFilha = contaCorrente.IdMoeda.Value;

                    ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();
                    contaCorrenteCollection.Query.Select(contaCorrenteCollection.Query.IdConta);
                    contaCorrenteCollection.Query.Where(contaCorrenteCollection.Query.IdPessoa.Equal(idCarteiraMae),
                                                        contaCorrenteCollection.Query.IdMoeda.Equal(idMoedaFilha));
                    contaCorrenteCollection.Query.Load();

                    if (contaCorrenteCollection.Count > 0)
                    {
                        int idContaMae = contaCorrenteCollection[0].IdConta.Value;

                        SaldoCaixa saldoCaixaExistente = new SaldoCaixa();
                        if (saldoCaixaExistente.LoadByPrimaryKey(idCarteiraMae, data, idContaMae))
                        {
                            saldoCaixaExistente.SaldoAbertura += saldoCaixa.SaldoAbertura.Value;

                            if (saldoCaixa.SaldoFechamento.HasValue)
                            {
                                saldoCaixaExistente.SaldoFechamento += saldoCaixa.SaldoFechamento.Value;
                            }

                            saldoCaixaExistente.Save();
                        }
                        else
                        {
                            SaldoCaixa saldoCaixaNovo = new SaldoCaixa();
                            saldoCaixaNovo.Data = data;
                            saldoCaixaNovo.IdCliente = idCarteiraMae;
                            saldoCaixaNovo.IdConta = idContaMae;
                            saldoCaixaNovo.SaldoAbertura = saldoCaixa.SaldoAbertura.Value;

                            if (saldoCaixa.SaldoFechamento.HasValue)
                            {
                                saldoCaixaNovo.SaldoFechamento = saldoCaixa.SaldoFechamento.Value;
                            }
                            else
                            {
                                saldoCaixaNovo.SaldoFechamento = 0;
                            }

                            saldoCaixaNovo.Save();
                        }
                    }
                }
            }

            this.ConsolidaPosicoesMae(idCarteiraMae, data);

            return processou;
        }

        /// <summary>
        /// Compôe os valores em Renda Fixa etc para a carteira mãe, a partir da lista de carteiras filhas. Transforma em posições vendidas o que for carregado.
        /// </summary>
        /// <param name="listaCarteiras"></param>
        /// <param name="idCarteiraMae"></param>
        /// <param name="data"></param>
        public void CompoeCarteiraMaeFDIC(int codigoFDIC, DateTime data)
        {
            ClienteCollection clienteCollection = new ClienteCollection();
            ClienteQuery clienteQuery = new ClienteQuery("C");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");

            clienteQuery.Select(clienteQuery.IdCliente, clienteQuery.DataDia);
            clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
            clienteQuery.Where(carteiraQuery.CodigoCDA.Equal(codigoFDIC));
            clienteCollection.Load(clienteQuery);

            if (clienteCollection.Count > 0)
            {
                PosicaoRendaFixaCollection posicaoRendaFixaCollectionDeletar = new PosicaoRendaFixaCollection();
                posicaoRendaFixaCollectionDeletar.Query.Select(posicaoRendaFixaCollectionDeletar.Query.IdPosicao);
                posicaoRendaFixaCollectionDeletar.Query.Where(posicaoRendaFixaCollectionDeletar.Query.IdCliente.Equal(codigoFDIC),
                                                              posicaoRendaFixaCollectionDeletar.Query.Quantidade.LessThan(0));
                posicaoRendaFixaCollectionDeletar.Query.Load();
                posicaoRendaFixaCollectionDeletar.MarkAllAsDeleted();
                posicaoRendaFixaCollectionDeletar.Save();
            }

            foreach (Cliente cliente in clienteCollection)
            {
                int idCarteiraFilha = cliente.IdCliente.Value;
                DateTime dataDiaFilha = cliente.DataDia.Value;

                if (dataDiaFilha > data)
                {
                    //Trata RENDAFIXA
                    PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                    posicaoRendaFixaHistoricoCollection.Query.Where(posicaoRendaFixaHistoricoCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                           posicaoRendaFixaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                           posicaoRendaFixaHistoricoCollection.Query.DataHistorico.Equal(data));
                    posicaoRendaFixaHistoricoCollection.Query.Load();

                    foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoCollection)
                    {
                        PosicaoRendaFixa posicaoRendaFixaInsert = new PosicaoRendaFixa();

                        posicaoRendaFixaInsert.DataLiquidacao = posicaoRendaFixaHistorico.DataLiquidacao;
                        posicaoRendaFixaInsert.DataOperacao = posicaoRendaFixaHistorico.DataOperacao;
                        posicaoRendaFixaInsert.DataVencimento = posicaoRendaFixaHistorico.DataVencimento;
                        posicaoRendaFixaInsert.DataVolta = posicaoRendaFixaHistorico.DataVolta;
                        posicaoRendaFixaInsert.IdTitulo = posicaoRendaFixaHistorico.IdTitulo;
                        posicaoRendaFixaInsert.Quantidade = posicaoRendaFixaHistorico.Quantidade * -1;
                        posicaoRendaFixaInsert.QuantidadeBloqueada = posicaoRendaFixaHistorico.QuantidadeBloqueada * -1;
                        posicaoRendaFixaInsert.QuantidadeInicial = posicaoRendaFixaHistorico.QuantidadeInicial * -1;
                        posicaoRendaFixaInsert.TaxaOperacao = posicaoRendaFixaHistorico.TaxaOperacao;
                        posicaoRendaFixaInsert.TipoNegociacao = posicaoRendaFixaHistorico.TipoNegociacao;
                        posicaoRendaFixaInsert.TipoOperacao = posicaoRendaFixaHistorico.TipoOperacao;

                        posicaoRendaFixaInsert.IdCliente = codigoFDIC;
                        posicaoRendaFixaInsert.PUOperacao = posicaoRendaFixaHistorico.PUOperacao.Value;
                        posicaoRendaFixaInsert.PUCurva = posicaoRendaFixaHistorico.PUCurva.Value;
                        posicaoRendaFixaInsert.PUJuros = posicaoRendaFixaHistorico.PUJuros.Value;
                        posicaoRendaFixaInsert.PUMercado = posicaoRendaFixaHistorico.PUMercado.Value;
                        posicaoRendaFixaInsert.PUOperacao = posicaoRendaFixaHistorico.PUOperacao.Value;
                        posicaoRendaFixaInsert.ValorCurva = posicaoRendaFixaHistorico.ValorCurva.Value * -1;
                        posicaoRendaFixaInsert.ValorIOF = 0;
                        posicaoRendaFixaInsert.ValorIR = 0;
                        posicaoRendaFixaInsert.ValorJuros = posicaoRendaFixaHistorico.ValorJuros.Value * -1;
                        posicaoRendaFixaInsert.ValorMercado = posicaoRendaFixaHistorico.ValorMercado.Value * -1;
                        posicaoRendaFixaInsert.PUCorrecao = posicaoRendaFixaHistorico.PUCorrecao.Value;
                        posicaoRendaFixaInsert.ValorCorrecao = posicaoRendaFixaHistorico.ValorCorrecao.Value * -1;
                        posicaoRendaFixaInsert.CustoCustodia = 0;

                        posicaoRendaFixaInsert.Save();
                    }
                }
                else
                {
                    //Trata RENDAFIXA
                    PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                    posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCarteiraFilha),
                                                     posicaoRendaFixaCollection.Query.Quantidade.NotEqual(0));
                    posicaoRendaFixaCollection.Query.Load();

                    foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                    {
                        int idPosicao = posicaoRendaFixa.IdPosicao.Value;

                        PosicaoRendaFixa posicaoRendaFixaInsert = new PosicaoRendaFixa();
                        posicaoRendaFixaInsert.LoadByPrimaryKey(idPosicao);
                        posicaoRendaFixaInsert.MarkAllColumnsAsDirty(DataRowState.Added);

                        posicaoRendaFixaInsert.IdCliente = codigoFDIC;
                        posicaoRendaFixaInsert.ValorCurva = posicaoRendaFixa.ValorCurva.Value * -1;
                        posicaoRendaFixaInsert.ValorJuros = posicaoRendaFixa.ValorJuros.Value * -1;
                        posicaoRendaFixaInsert.ValorMercado = posicaoRendaFixa.ValorMercado.Value * -1;

                        posicaoRendaFixaInsert.Save();
                    }

                }

            }

        }

        /// <summary>
        /// Consolida posições de bolsa e BMF, método complementar ao método CompoeCarteiraMae.
        /// </summary>
        /// <param name="idCarteiraMae"></param>
        /// <param name="data"></param>
        private void ConsolidaPosicoesMae(int idCarteiraMae, DateTime data)
        {
            #region Consolida Bolsa
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa, posicaoBolsaCollection.Query.IdAgente);
            posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCarteiraMae));
            posicaoBolsaCollection.Query.GroupBy(posicaoBolsaCollection.Query.CdAtivoBolsa, posicaoBolsaCollection.Query.IdAgente);
            posicaoBolsaCollection.Query.Load();

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                int idAgente = posicaoBolsa.IdAgente.Value;

                PosicaoBolsaCollection posicaoBolsaCollectionAtivo = new PosicaoBolsaCollection();
                posicaoBolsaCollectionAtivo.Query.Where(posicaoBolsaCollectionAtivo.Query.IdCliente.Equal(idCarteiraMae),
                                                        posicaoBolsaCollectionAtivo.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                        posicaoBolsaCollectionAtivo.Query.IdAgente.Equal(idAgente));
                posicaoBolsaCollectionAtivo.Query.Load();

                if (posicaoBolsaCollectionAtivo.Count > 1)
                {
                    List<int> idsDeletar = new List<int>();
                    int i = 0;
                    decimal puCusto = 0;
                    decimal puCustoLiquido = 0;
                    decimal valorCustoLiquido = 0;
                    decimal quantidade = 0;
                    decimal resultadoRealizar = 0;
                    decimal valorMercado = 0;
                    foreach (PosicaoBolsa posicaoBolsaAjustar in posicaoBolsaCollectionAtivo)
                    {
                        if (quantidade + posicaoBolsaAjustar.Quantidade.Value != 0)
                        {
                            puCusto = ((puCusto * quantidade) + (posicaoBolsaAjustar.PUCusto.Value * posicaoBolsaAjustar.Quantidade.Value)) /
                                            (quantidade + posicaoBolsaAjustar.Quantidade.Value);
                            puCustoLiquido = ((puCustoLiquido * quantidade) + (posicaoBolsaAjustar.PUCustoLiquido.Value * posicaoBolsaAjustar.Quantidade.Value)) /
                                            (quantidade + posicaoBolsaAjustar.Quantidade.Value);
                        }

                        valorCustoLiquido += posicaoBolsaAjustar.ValorCustoLiquido.Value;
                        quantidade += posicaoBolsaAjustar.Quantidade.Value;
                        resultadoRealizar += posicaoBolsaAjustar.ResultadoRealizar.Value;
                        valorMercado += posicaoBolsaAjustar.ValorMercado.Value;

                        if (i == posicaoBolsaCollectionAtivo.Count - 1)
                        {
                            posicaoBolsaAjustar.PUCusto = puCusto;
                            posicaoBolsaAjustar.PUCustoLiquido = puCustoLiquido;
                            posicaoBolsaAjustar.ValorCustoLiquido = valorCustoLiquido;
                            posicaoBolsaAjustar.Quantidade = quantidade;
                            posicaoBolsaAjustar.ResultadoRealizar = resultadoRealizar;
                            posicaoBolsaAjustar.ValorMercado = valorMercado;
                        }
                        else
                        {
                            idsDeletar.Add(posicaoBolsaAjustar.IdPosicao.Value);
                        }

                        i += 1;
                    }

                    posicaoBolsaCollectionAtivo.Save();

                    PosicaoBolsaCollection posicaoBolsaCollectionDeletar = new PosicaoBolsaCollection();
                    posicaoBolsaCollectionDeletar.Query.Where(posicaoBolsaCollectionDeletar.Query.IdPosicao.In(idsDeletar));
                    posicaoBolsaCollectionDeletar.Query.Load();
                    posicaoBolsaCollectionDeletar.MarkAllAsDeleted();
                    posicaoBolsaCollectionDeletar.Save();
                }
            }
            #endregion

            #region Consolida BMF
            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            posicaoBMFCollection.Query.Select(posicaoBMFCollection.Query.CdAtivoBMF, posicaoBMFCollection.Query.Serie, posicaoBMFCollection.Query.IdAgente);
            posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.IdCliente.Equal(idCarteiraMae));
            posicaoBMFCollection.Query.GroupBy(posicaoBMFCollection.Query.CdAtivoBMF, posicaoBMFCollection.Query.Serie, posicaoBMFCollection.Query.IdAgente);
            posicaoBMFCollection.Query.Load();

            foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
            {
                string cdAtivoBMF = posicaoBMF.CdAtivoBMF;
                string serie = posicaoBMF.Serie;
                int idAgente = posicaoBMF.IdAgente.Value;

                PosicaoBMFCollection posicaoBMFCollectionAtivo = new PosicaoBMFCollection();
                posicaoBMFCollectionAtivo.Query.Where(posicaoBMFCollectionAtivo.Query.IdCliente.Equal(idCarteiraMae),
                                                        posicaoBMFCollectionAtivo.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                                        posicaoBMFCollectionAtivo.Query.Serie.Equal(serie),
                                                        posicaoBMFCollectionAtivo.Query.IdAgente.Equal(idAgente));
                posicaoBMFCollectionAtivo.Query.Load();

                if (posicaoBMFCollectionAtivo.Count > 1)
                {
                    List<int> idsDeletar = new List<int>();
                    int i = 0;
                    decimal puCusto = 0;
                    decimal puCustoLiquido = 0;
                    decimal valorCustoLiquido = 0;
                    int quantidade = 0;
                    decimal resultadoRealizar = 0;
                    decimal valorMercado = 0;
                    foreach (PosicaoBMF posicaoBMFAjustar in posicaoBMFCollectionAtivo)
                    {
                        if (quantidade + posicaoBMFAjustar.Quantidade.Value != 0)
                        {
                            puCusto = ((puCusto * quantidade) + (posicaoBMFAjustar.PUCusto.Value * posicaoBMFAjustar.Quantidade.Value)) /
                                            (quantidade + posicaoBMFAjustar.Quantidade.Value);
                            puCustoLiquido = ((puCustoLiquido * quantidade) + (posicaoBMFAjustar.PUCustoLiquido.Value * posicaoBMFAjustar.Quantidade.Value)) /
                                            (quantidade + posicaoBMFAjustar.Quantidade.Value);
                        }

                        valorCustoLiquido += posicaoBMFAjustar.ValorCustoLiquido.Value;
                        quantidade += posicaoBMFAjustar.Quantidade.Value;
                        resultadoRealizar += posicaoBMFAjustar.ResultadoRealizar.Value;
                        valorMercado += posicaoBMFAjustar.ValorMercado.Value;

                        if (i == posicaoBMFCollectionAtivo.Count - 1)
                        {
                            posicaoBMFAjustar.PUCusto = puCusto;
                            posicaoBMFAjustar.PUCustoLiquido = puCustoLiquido;
                            posicaoBMFAjustar.ValorCustoLiquido = valorCustoLiquido;
                            posicaoBMFAjustar.Quantidade = quantidade;
                            posicaoBMFAjustar.ResultadoRealizar = resultadoRealizar;
                            posicaoBMFAjustar.ValorMercado = valorMercado;
                        }
                        else
                        {
                            idsDeletar.Add(posicaoBMFAjustar.IdPosicao.Value);
                        }

                        i += 1;
                    }

                    posicaoBMFCollectionAtivo.Save();

                    PosicaoBMFCollection posicaoBMFCollectionDeletar = new PosicaoBMFCollection();
                    posicaoBMFCollectionDeletar.Query.Where(posicaoBMFCollectionDeletar.Query.IdPosicao.In(idsDeletar));
                    posicaoBMFCollectionDeletar.Query.Load();
                    posicaoBMFCollectionDeletar.MarkAllAsDeleted();
                    posicaoBMFCollectionDeletar.Save();
                }
            }
            #endregion
        }

        public List<Cliente> OrdenaProcessamentoClientes(List<Cliente> clientes)
        {
            List<Cliente> filhos = new List<Cliente>();
            List<Cliente> maes = new List<Cliente>();

            foreach (Cliente cliente in clientes)
            {
                CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection();
                carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraFilha);
                carteiraMaeCollection.Query.Where(carteiraMaeCollection.Query.IdCarteiraMae.Equal(cliente.IdCliente.Value));
                carteiraMaeCollection.Load(carteiraMaeCollection.Query);

                if (carteiraMaeCollection.Count > 0)
                {
                    maes.Add(cliente);
                }
                else
                {
                    filhos.Add(cliente);
                }
            }

            List<Cliente> clientesOrdenados = filhos;
            clientesOrdenados.AddRange(maes);

            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            posicaoFundoCollection.LoadAll();
            List<PosicaoFundo> posicoesFundo = new List<PosicaoFundo>();
            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
            {
                posicoesFundo.Add(posicaoFundo);
            }

            List<Cliente> clientesQueAplicam = new List<Cliente>();
            List<Cliente> clientesAplicados = new List<Cliente>();

            //Fundo A aplica no Fundo B - entao fundo B tem que ser processado antes do Fundo A
            //Na tabela de posicaofundo: Cliente = A e Carteira = B
            foreach (Cliente cliente in clientesOrdenados)
            {
                //Descobrir se algum outro cliente aplica no cliente atual (ou seja, se existe alguma posicao em que este fundo é a carteira aplicada)
                List<PosicaoFundo> posicoesAplicadas = posicoesFundo.FindAll(delegate(PosicaoFundo p) { return p.IdCarteira == cliente.IdCliente; });
                if (posicoesAplicadas.Count > 0)
                {
                    clientesAplicados.Add(cliente);
                }
                else
                {
                    clientesQueAplicam.Add(cliente);
                }
            }

            clientesOrdenados = clientesAplicados;
            clientesOrdenados.AddRange(clientesQueAplicam);

            return clientesOrdenados;
        }

        public void AberturaAutomatica(int idCliente, DateTime data)
        {
            this.controllerRendaFixa.AberturaAutomatica(idCliente, data);
        }

        #region Processa Rentabilidade

        public void ProcessaRentabilidade(DateTime dataInicio, DateTime dataFim, int cliente)
        {
            DateTime dataAux = dataInicio;

            while (DateTime.Compare(dataFim, dataAux) >= 0)
            {
                this.controllerBolsa.CalculaRentabilidadeBolsa(dataAux, cliente);
                this.controllerBMF.CalculaRentabilidadeBMF(dataAux, cliente);
                this.controllerRendaFixa.CalculaRentabilidadeRendaFixa(dataAux, cliente);
                this.controllerSwap.CalculaRentabilidadeSwap(dataAux, cliente);
                this.controllerFundo.CalculaRentabilidadeFundo(dataAux, cliente);

                dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }
        }

        #endregion

    }
}
