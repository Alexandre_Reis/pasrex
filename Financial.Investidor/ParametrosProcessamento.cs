﻿using Financial.Fundo.Enums;
using Financial.Investidor.Enums;
using System.Collections.Generic;
namespace Financial.Investidor.Controller
{
    public class ParametrosProcessamento
    {
        #region Variáveis de controle de processamento e integração
        //
        private bool processaBolsa;
        private bool processaBMF;
        private bool processaRendaFixa;
        private bool processaSwap;
        private bool processaFundo;
        private bool processaCambio;
        private bool integraBolsa;        
        private bool integraBTC;
        private bool integraBMF;
        private int integracaoRendaFixa;
        private bool integraCC;
        private bool mantemFuturo;
        private bool ignoraOperacoes;
        private bool remuneraRF;
        private bool cravaCota;
        private bool resetCusto;
        private bool processaRentabilidade;        
        private TipoProcessamento tipoProcessamento;
        
        private bool aberturaAutomatica;
        private int perfilProcessamento;
        public bool ProcessaBolsa
        {
            get { return processaBolsa; }
            set { processaBolsa = value; }
        }

        public bool ProcessaBMF
        {
            get { return processaBMF; }
            set { processaBMF = value; }
        }

        public bool ProcessaRendaFixa
        {
            get { return processaRendaFixa; }
            set { processaRendaFixa = value; }
        }

        public bool ProcessaSwap
        {
            get { return processaSwap; }
            set { processaSwap = value; }
        }

        public bool ProcessaFundo
        {
            get { return processaFundo; }
            set { processaFundo = value; }
        }

        public bool ProcessaCambio
        {
            get { return processaCambio; }
            set { processaCambio = value; }
        }

        public bool IntegraBolsa
        {
            get { return integraBolsa; }
            set { integraBolsa = value; }
        }

        public bool IntegraBTC
        {
            get { return integraBTC; }
            set { integraBTC = value; }
        }

        public bool IntegraBMF
        {
            get { return integraBMF; }
            set { integraBMF = value; }
        }

        public int IntegracaoRendaFixa
        {
            get { return integracaoRendaFixa; }
            set { integracaoRendaFixa = value; }
        }

        public bool IntegraCC
        {
            get { return integraCC; }
            set { integraCC = value; }
        }

        public bool MantemFuturo
        {
            get { return mantemFuturo; }
            set { mantemFuturo = value; }
        }

        public bool IgnoraOperacoes
        {
            get { return ignoraOperacoes; }
            set { ignoraOperacoes = value; }
        }

        public bool RemuneraRF
        {
            get { return remuneraRF; }
            set { remuneraRF = value; }
        }

        public bool CravaCota
        {
            get { return cravaCota; }
            set { cravaCota = value; }
        }

        public bool AberturaAutomatica
        {
            get { return aberturaAutomatica; }
            set { aberturaAutomatica = value; }
        }

        public int PerfilProcessamento
        {
            get { return perfilProcessamento; }
            set { perfilProcessamento = value; }
        }

        public bool ResetCusto
        {
            get { return resetCusto; }
            set { resetCusto = value; }
        }
        
        public bool ProcessaRentabilidade
        {
            get { return processaRentabilidade; }
            set { processaRentabilidade = value; }
        }

        public TipoProcessamento TipoProcessamento
        {
            get { return tipoProcessamento; }
            set { tipoProcessamento = value; }
        }


        #endregion

        List<int> listaTabelaInterfaceCliente; //Lista de int baseada em TabelaInterfaceClienteCollection

        public List<int> ListaTabelaInterfaceCliente
        {
            get { return listaTabelaInterfaceCliente; }
            set { listaTabelaInterfaceCliente = value; }
        }
    }
}
