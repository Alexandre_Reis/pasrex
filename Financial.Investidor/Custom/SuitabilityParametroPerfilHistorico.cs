/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/09/2015 11:59:51
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Common.Enums;

namespace Financial.Investidor
{
	public partial class SuitabilityParametroPerfilHistorico : esSuitabilityParametroPerfilHistorico
	{
        public static void createSuitabilityParametroPerfilHistorico(SuitabilityParametroPerfil suitabilityParametroPerfil, TipoOperacaoBanco tipoOperacaoBanco)
        {
            SuitabilityParametroPerfilHistorico suitabilityParametroPerfilHistorico = new SuitabilityParametroPerfilHistorico();
            suitabilityParametroPerfilHistorico.DataHistorico = DateTime.Now;
            suitabilityParametroPerfilHistorico.IdParametroPerfil = suitabilityParametroPerfil.IdParametroPerfil;
            suitabilityParametroPerfilHistorico.IdValidacao = suitabilityParametroPerfil.IdValidacao;
            suitabilityParametroPerfilHistorico.IdPerfilInvestidor = suitabilityParametroPerfil.PerfilInvestidor;
            suitabilityParametroPerfilHistorico.Definicao = suitabilityParametroPerfil.Definicao;
            suitabilityParametroPerfilHistorico.FaixaPontuacao = suitabilityParametroPerfil.FaixaPontuacao;
            suitabilityParametroPerfilHistorico.Tipo = tipoOperacaoBanco.ToString();

            SuitabilityValidacao suitabilityValidacao = new SuitabilityValidacao();
            suitabilityValidacao.LoadByPrimaryKey(suitabilityParametroPerfil.IdValidacao.Value);
            suitabilityParametroPerfilHistorico.Validacao = suitabilityValidacao.Descricao;

            SuitabilityPerfilInvestidor suitabilityPerfilInvestidor = new SuitabilityPerfilInvestidor();
            suitabilityPerfilInvestidor.LoadByPrimaryKey(suitabilityParametroPerfil.PerfilInvestidor.Value);
            suitabilityParametroPerfilHistorico.PerfilInvestidor = suitabilityPerfilInvestidor.Perfil;

            suitabilityParametroPerfilHistorico.Save();
        }
	}
}
