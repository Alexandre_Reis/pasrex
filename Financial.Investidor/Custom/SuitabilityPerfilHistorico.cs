/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 27/08/2015 12:37:57
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Common.Enums;

namespace Financial.Investidor
{
	public partial class SuitabilityPerfilHistorico : esSuitabilityPerfilHistorico
	{
        public static void createSuitabilityPerfilHistorico(SuitabilityPerfil suitabilityPerfil, TipoOperacaoBanco tipoOperacaoBanco)
        {
            SuitabilityPerfilHistorico suitabilityPerfilHistorico = new SuitabilityPerfilHistorico();
            suitabilityPerfilHistorico.DataHistorico = DateTime.Now;
            suitabilityPerfilHistorico.IdPerfilInvestidor = suitabilityPerfil.IdPerfilInvestidor;
            suitabilityPerfilHistorico.Data = suitabilityPerfil.Data;
            suitabilityPerfilHistorico.Pontos = suitabilityPerfil.Pontos;
            suitabilityPerfilHistorico.TipoCliente = suitabilityPerfil.TipoCliente;
            suitabilityPerfilHistorico.PerfilInvestidor = suitabilityPerfil.PerfilInvestidor;
            suitabilityPerfilHistorico.Definicao = suitabilityPerfil.Definicao;
            suitabilityPerfilHistorico.FaixaPontuacao = suitabilityPerfil.FaixaPontuacao;
            suitabilityPerfilHistorico.Tipo = tipoOperacaoBanco.ToString();
            suitabilityPerfilHistorico.Save();
        }
	}
}
