/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 09/09/2015 11:23:56
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Common.Enums;

namespace Financial.Investidor
{
	public partial class SuitabilityEventosHistorico : esSuitabilityEventosHistorico
	{
        public static void createSuitabilityEventosHistorico(SuitabilityEventos suitabilityEventos, TipoOperacaoBanco tipoOperacaoBanco)
        {
            SuitabilityEventosHistorico suitabilityEventosHistorico = new SuitabilityEventosHistorico();
            suitabilityEventosHistorico.DataHistorico = DateTime.Now;
            suitabilityEventosHistorico.IdSuitabilityEventos = suitabilityEventos.IdSuitabilityEventos;
            suitabilityEventosHistorico.Descricao = suitabilityEventos.Descricao;
            suitabilityEventosHistorico.IdMensagem = suitabilityEventos.IdMensagem;
            suitabilityEventosHistorico.Tipo = tipoOperacaoBanco.ToString();

            SuitabilityMensagens suitabilityMensagens = new SuitabilityMensagens();
            suitabilityMensagens.LoadByPrimaryKey(suitabilityEventos.IdMensagem.Value);
            suitabilityEventosHistorico.Mesagem = suitabilityMensagens.Descricao;

            suitabilityEventosHistorico.Save();
        }
	}
}
