/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 1.5.0.0
                       MyGeneration Version # 1.1.5.1
                           22/2/2007 13:16:28
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Investidor
{
	public partial class ClienteInterfaceCollection : esClienteInterfaceCollection
	{
        public ClienteInterface BuscaClienteInterfacePorCodigoYMF(string codigoYMF)
        {
            ClienteInterface clienteInterface = null;
            this.QueryReset();

            if (codigoYMF.Contains("|"))
            {
                //Buscar codigo com multiplos valores
                //atualmente multiplos valores sao apenas suportados na importacao SMA
                this.Query.Where(this.Query.CodigoYMF.Like("%" + codigoYMF + "%"));
            }
            else
            {
                this.Query.Where(this.Query.CodigoYMF == codigoYMF);
            }


            if (this.Load(this.Query))
            {
                clienteInterface = this[0];
            }

            return clienteInterface;

        }


	}
}
