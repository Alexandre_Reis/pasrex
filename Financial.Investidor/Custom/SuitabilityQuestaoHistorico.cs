/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 25/08/2015 15:02:20
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Common.Enums;

namespace Financial.Investidor
{
	public partial class SuitabilityQuestaoHistorico : esSuitabilityQuestaoHistorico
	{

        public static void createSuitabilityQuestaoHistorico(SuitabilityQuestao suitabilityQuestao, TipoOperacaoBanco tipoOperacaoBanco)
        {
            SuitabilityQuestaoHistorico suitabilityQuestaoHistorico = new SuitabilityQuestaoHistorico();
            suitabilityQuestaoHistorico.DataHistorico = DateTime.Now;
            suitabilityQuestaoHistorico.IdQuestao = suitabilityQuestao.IdQuestao;
            suitabilityQuestaoHistorico.Descricao = suitabilityQuestao.Descricao;
            suitabilityQuestaoHistorico.Fator = suitabilityQuestao.Fator;
            suitabilityQuestaoHistorico.IdValidacao = suitabilityQuestao.IdValidacao;
            suitabilityQuestaoHistorico.Tipo = tipoOperacaoBanco.ToString();

            SuitabilityValidacao suitabilityValidacao = new SuitabilityValidacao();
            suitabilityValidacao.LoadByPrimaryKey(suitabilityQuestao.IdValidacao.Value);
            suitabilityQuestaoHistorico.Validacao = suitabilityValidacao.Descricao;

            suitabilityQuestaoHistorico.Save();
        }

	}
}
