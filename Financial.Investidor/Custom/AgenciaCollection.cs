/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 1.5.0.0
                       MyGeneration Version # 1.1.5.1
                           4/10/2006 09:34:13
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Investidor
{
	public partial class AgenciaCollection : esAgenciaCollection
	{
        // Hack teste de relatorio
        public DataSet BuscaAgencia() {

            string sqlText = "";
            sqlText = "SELECT IdAgencia, IdBanco, Nome ";
            sqlText += "FROM [Agencia] ";

            return this.FillDataSet(esQueryType.Text, sqlText);
            //DataTableCollection dataTableCollection = ds.Tables;
            //this.QueryReset();
            //this.LoadAll();                     
        }

        // Hack teste de relatorio
        public void BuscaAgencia1() {                    
            this.QueryReset();            
            
            this.Query
                 .Select(this.query.IdAgencia, this.query.IdBanco, this.query.Nome);
            this.Query.Load();           
        }

        // Hack teste de relatorio
        public DataTable BuscaAgenciaDataTable() {

            string sqlText = "";
            sqlText = "SELECT IdAgencia, IdBanco, Nome ";
            sqlText += "FROM [Agencia] ";

            return this.FillDataTable(esQueryType.Text, sqlText);
            //DataTableCollection dataTableCollection = ds.Tables;
            //this.QueryReset();
            //this.LoadAll();                     
        }

        // Hack teste de relatorio
        public DataTable BuscaAgenciaDataTable1(int idAgencia) {

            string sqlText = "";
            sqlText = "SELECT IdAgencia, IdBanco, Nome ";
            sqlText += "FROM [Agencia] ";
            sqlText += "Where IdAgencia = " + idAgencia;

            return this.FillDataTable(esQueryType.Text, sqlText);
            //DataTableCollection dataTableCollection = ds.Tables;
            //this.QueryReset();
            //this.LoadAll();                     
        }

        public Agencia BuscaAgenciaPorBancoECodigo(int idBanco, string codigoAgencia)
        {
            Agencia agencia = null;

            this.QueryReset();
            this.Query.Where(this.Query.IdBanco == idBanco, this.Query.Codigo == codigoAgencia);
            if (this.Load(this.Query))
            {
                agencia = this[0];
            }

            return agencia;
        }
	}
}

