﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Common.Enums;

namespace Financial.Investidor
{
	public partial class ContaCorrenteCollection : esContaCorrenteCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(ContaCorrenteCollection));

        /// <summary>
        /// Carrega o objeto ContaCorrenteCollection com todos todos os campos de ContaCorrente.
        /// Filtra forçado para IdMoeda = ListaMoedaFixo.Real.
        /// </summary>
        /// <param name="idPessoa"></param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaContaCorrente(int idPessoa)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdPessoa.Equal(idPessoa));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto ContaCorrenteCollection com todos todos os campos de ContaCorrente.
        /// </summary>
        /// <param name="idPessoa"></param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaContaCorrente(int idPessoa, int idMoeda)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdPessoa.Equal(idPessoa),
                        this.Query.IdMoeda.Equal(idMoeda));

            bool retorno = this.Query.Load();

            return retorno;
        }

	}
}
