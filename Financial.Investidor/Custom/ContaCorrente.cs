﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor.Exceptions;
using Financial.ContaCorrente.Enums;
using Financial.ContaCorrente;
using log4net;
using Financial.Investidor.Enums;
using Financial.Common;
using Financial.Common.Enums;
using Financial.CRM;

namespace Financial.Investidor
{
	public partial class ContaCorrente : esContaCorrente {
        private static readonly ILog log = LogManager.GetLogger(typeof(ContaCorrente));


        /// <summary>
        /// Retorna bool indicando se a conta é default ou não.
        /// </summary>
        /// <returns></returns>
        public bool IsContaDefault()
        {
            return this.ContaDefault == "S";
        }

        /// <summary>
        /// Retorna a conta corrente default do cliente.
        /// ContaDefaultNaoCadastradaException para conta default não encontrada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <throws>ContaDefaultNaoCadastradaException para conta default não encontrada</throws>
        /// <returns></returns>
        public int RetornaContaDefault(int idCliente)
        {
            ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();

            Cliente cliente = new Cliente();
            int? idPessoa = null;
            if(cliente.LoadByPrimaryKey(idCliente))
            {
                idPessoa = cliente.IdPessoa;
            }

            contaCorrenteCollection.BuscaContaCorrente(idPessoa.Value);
            
            bool achouContaDefault = false;
            int i;
            for (i = 0; i < contaCorrenteCollection.Count; i++)
            {
                ContaCorrente contaCorrente = contaCorrenteCollection[i];
                if (contaCorrente.IsContaDefault())
                {
                    achouContaDefault = true;
                }
                if (achouContaDefault)
                {
                    break;
                }
            }
            if (!achouContaDefault)
            {
                Investidor.ContaCorrente contaCorrenteNovo = new Financial.Investidor.ContaCorrente();
                contaCorrenteNovo.ContaDefault = "S";
                contaCorrenteNovo.IdMoeda = (byte)ListaMoedaFixo.Real;
                contaCorrenteNovo.IdPessoa = idPessoa;
                contaCorrenteNovo.Numero = idCliente.ToString();

                TipoConta tipoConta = new TipoConta();
                tipoConta.Query.Select(tipoConta.Query.IdTipoConta.Min());
                tipoConta.Query.Load();

                if (!tipoConta.IdTipoConta.HasValue)
                {
                    tipoConta = new TipoConta();
                    tipoConta.Descricao = "Conta Depósito";
                    tipoConta.Save();
                }
                contaCorrenteNovo.IdTipoConta = tipoConta.IdTipoConta.Value;

                contaCorrenteNovo.Save();

                return contaCorrenteNovo.IdConta.Value;
            }
            else
            {
                return ((ContaCorrente)contaCorrenteCollection[i]).IdConta.Value;
            }
        }

        /// <summary>
        /// Retorna a 1a conta corrente associada com a moeda passada.
        /// ContaDefaultNaoCadastradaException para conta default não encontrada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <throws>ContaDefaultNaoCadastradaException para conta default não encontrada</throws>
        /// <returns></returns>
        public int RetornaContaDefault(int idCliente, int idMoeda)
        {
            ContaCorrenteCollection contaCorrenteCollection = new ContaCorrenteCollection();

            Cliente cliente = new Cliente();
            int? idPessoa = null;
            if (cliente.LoadByPrimaryKey(idCliente))
            {
                idPessoa = cliente.IdPessoa;
            }

            contaCorrenteCollection.BuscaContaCorrente(idPessoa.Value, idMoeda);

            bool achouContaDefault = false;
            int i;
            for (i = 0; i < contaCorrenteCollection.Count; i++)
            {
                ContaCorrente contaCorrente = contaCorrenteCollection[i];
                if (contaCorrente.IsContaDefault())
                {
                    achouContaDefault = true;
                }
                if (achouContaDefault)
                {
                    break;
                }
            }
            if (!achouContaDefault)
            {
                Moeda moeda = new Moeda();
                moeda.LoadByPrimaryKey((short)idMoeda);
                string nome = moeda.Nome;
                throw new ContaDefaultNaoCadastradaException("C/C Default não cadastrada para o cliente " + idCliente + " para a moeda " + nome);
            }
            else
            {
                return ((ContaCorrente)contaCorrenteCollection[i]).IdConta.Value;
            }
        }
	}
}
