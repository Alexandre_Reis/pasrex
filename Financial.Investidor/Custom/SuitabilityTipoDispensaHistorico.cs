/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 26/08/2015 14:55:08
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Common.Enums;

namespace Financial.Investidor
{
	public partial class SuitabilityTipoDispensaHistorico : esSuitabilityTipoDispensaHistorico
	{
        public static void createSuitabilityTipoDispensaHistorico(SuitabilityTipoDispensa suitabilityTipoDispensa, TipoOperacaoBanco tipoOperacaoBanco)
        {
            SuitabilityTipoDispensaHistorico suitabilityTipoDispensaHistorico = new SuitabilityTipoDispensaHistorico();
            suitabilityTipoDispensaHistorico.DataHistorico = DateTime.Now;
            suitabilityTipoDispensaHistorico.IdDispensa = suitabilityTipoDispensa.IdDispensa;
            suitabilityTipoDispensaHistorico.Descricao = suitabilityTipoDispensa.Descricao;
            suitabilityTipoDispensaHistorico.Tipo = tipoOperacaoBanco.ToString();
            suitabilityTipoDispensaHistorico.Save();
        }
	}
}
