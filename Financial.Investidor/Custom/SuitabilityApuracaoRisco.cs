/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 01/09/2015 09:42:12
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Investidor
{
	public partial class SuitabilityApuracaoRisco : esSuitabilityApuracaoRisco
	{
        public List<SuitabilityPerfilInvestidor> ListaPerfilRiscoFundo()
        {
            List<SuitabilityPerfilInvestidor> listaPerfis = new List<SuitabilityPerfilInvestidor>();

            string[] options = this.PerfilRiscoFundo.Split('+');
            foreach(string s in options)
            {
                int value = Convert.ToInt16(s.Substring(0, s.IndexOf('#')));
                SuitabilityPerfilInvestidor suitabilityPerfilInvestidor = new SuitabilityPerfilInvestidor();
                if(suitabilityPerfilInvestidor.LoadByPrimaryKey(value)) listaPerfis.Add(suitabilityPerfilInvestidor);
            }

            return listaPerfis;
        }
                
	}
}
