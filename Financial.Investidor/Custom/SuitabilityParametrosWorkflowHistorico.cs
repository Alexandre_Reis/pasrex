/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/09/2015 15:08:04
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Common.Enums;
using Financial.Investidor.Enums;

namespace Financial.Investidor
{
	public partial class SuitabilityParametrosWorkflowHistorico : esSuitabilityParametrosWorkflowHistorico
	{
        public static void createSuitabilityPerfilInvestidorHistorico(SuitabilityParametrosWorkflow suitabilityParametrosWorkflow, TipoOperacaoBanco tipoOperacaoBanco)
        {
            SuitabilityParametrosWorkflowHistorico suitabilityParametrosWorkflowHistorico = new SuitabilityParametrosWorkflowHistorico();
            suitabilityParametrosWorkflowHistorico.DataHistorico = DateTime.Now;
            suitabilityParametrosWorkflowHistorico.IdParametrosWorkflow = suitabilityParametrosWorkflow.IdParametrosWorkflow;
            suitabilityParametrosWorkflowHistorico.AtivaWorkFlow = suitabilityParametrosWorkflow.AtivaWorkFlow;
            
            if(suitabilityParametrosWorkflow.VerificaEnquadramento.HasValue)
                suitabilityParametrosWorkflowHistorico.VerificaEnquadramento = ((SuitabilityVerificaEnquadramento)suitabilityParametrosWorkflow.VerificaEnquadramento).ToString() ;

            suitabilityParametrosWorkflowHistorico.ResponsavelEmail = suitabilityParametrosWorkflow.ResponsavelEmail;
            suitabilityParametrosWorkflowHistorico.ResponsavelNome = suitabilityParametrosWorkflow.ResponsavelNome;
            suitabilityParametrosWorkflowHistorico.ValorPeriodo = suitabilityParametrosWorkflow.ValorPeriodo;

            if(suitabilityParametrosWorkflow.GrandezaPeriodo.HasValue)
                suitabilityParametrosWorkflowHistorico.GrandezaPeriodo = ((SuitabilityGrandezaTempo)suitabilityParametrosWorkflow.GrandezaPeriodo).ToString();

            suitabilityParametrosWorkflowHistorico.Tipo = tipoOperacaoBanco.ToString();
            suitabilityParametrosWorkflowHistorico.Save();
        }
	}
}
