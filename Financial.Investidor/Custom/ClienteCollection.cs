﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Security;
using Financial.Investidor.Enums;
using Financial.Interfaces.Export.RendaFixa;
using Financial.CRM;
using Financial.Common;
using System.Text.RegularExpressions;
using Financial.Util;
using Financial.CRM.Enums;
using Financial.RendaFixa;

namespace Financial.Investidor
{
    public partial class ClienteCollection : esClienteCollection
    {
        private Dictionary<string, string> municipiosDictionary = null;
        
        /// <summary>
        /// Adiciona uma Coluna Nova na Entidade
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn) {
            if (this.Table != null && !this.Table.Columns.Contains(columnName)) {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Carrega o objeto ClienteCollection com os campos IdCliente, Nome, Apelido.
        /// Filtra por TipoControle.NotEqual(TipoControleCliente.ApenasCotacao)
        /// e StatusAtivo.Equal(StatusAtivoCliente.Ativo).
        /// this.Load(clienteQuery);
        /// </summary>
        /// <param name="login"></param>  
        public void BuscaClientesComAcesso(string login)
        {
            ClienteQuery clienteQuery = new ClienteQuery("C");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

            clienteQuery.Select(clienteQuery.IdCliente, clienteQuery.Apelido);
            clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
            clienteQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
            clienteQuery.Where(usuarioQuery.Login == login,
                               clienteQuery.TipoControle.NotEqual(TipoControleCliente.ApenasCotacao),
                               clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo));
            clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

            this.Load(clienteQuery);
        }

        public void BuscaClientesApenasCotacao(string login)
        {
            ClienteQuery clienteQuery = new ClienteQuery("C");

            clienteQuery.Select(clienteQuery.IdCliente, clienteQuery.Apelido);
            clienteQuery.Where(clienteQuery.TipoControle.Equal(TipoControleCliente.ApenasCotacao));
            clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

            this.Load(clienteQuery);
        }

        /// <summary>
        /// Carrega o objeto ClienteCollection com os campos IdCliente, Nome, Apelido.
        //  Faz um combine de collections de clientes com acesso via PermissaoCliente 
        //  com clientes do TipoControle = SomenteCotacao.
        /// </summary>
        /// <param name="login"></param>  
        public void BuscaClientesComAcessoTodos(string login)
        {
            ClienteQuery clienteQuery = new ClienteQuery("C");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

            clienteQuery.Select(clienteQuery.IdCliente, clienteQuery.Apelido);
            clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
            clienteQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
            clienteQuery.Where(usuarioQuery.Login == login,
                        clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo),
                        clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao))
            .OrderBy(clienteQuery.Apelido.Ascending);

            this.Load(clienteQuery);

            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Where(clienteCollection.Query.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao));

            clienteCollection.Query.Load();

            this.Combine(clienteCollection);
            this.Sort = ClienteMetadata.ColumnNames.Apelido + " ASC";
        }

        /// <summary>
        /// Seta o flag de StatusRealTime (Executar/NaoExecutar) para os clientes com dataDia > data passada
        /// e StatusAtivo = 'S'.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="statusRealTime"></param>
        public void SetaFlagRealTime(DateTime data, byte statusRealTime)
        {
            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Select(clienteCollection.Query.IdCliente, clienteCollection.Query.StatusRealTime);
            clienteCollection.Query.Where(clienteCollection.Query.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                                          clienteCollection.Query.DataDia.GreaterThanOrEqual(data));
            clienteCollection.Query.Load();

            foreach (Cliente cliente in clienteCollection)
            {
                cliente.StatusRealTime = statusRealTime;
                cliente.Save();
            }
        }

        public Cliente BuscaClientePorCodigoYMF(string codigoYMF)
        {
            Cliente cliente = null;
            ClienteInterface clienteInterface = new ClienteInterfaceCollection().BuscaClienteInterfacePorCodigoYMF(codigoYMF);
            if (clienteInterface != null)
            {
                cliente = new Cliente();
                cliente.LoadByPrimaryKey(clienteInterface.IdCliente.Value);
            }
            return cliente;

        }

        public Cliente BuscaClientePorNome(string nome)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.Nome.ToUpper().Equal(nome.ToUpper()));
            this.Query.Load();

            if (this.Count == 0)
            {
                return null;
            }
            else
            {
                return this[0];
            }
        }

        public ClienteCetipViewModel ExportaClienteCetip(DateTime dataExportacao)
        {
            const string ID_SISTEMA = "SIC21";
            const int CONTA_PARTICIPANTE = 3412004;//TODO: colocar futuramente como conta 00 no agente mercado
            const int TIPO_LINHA_HEADER = 0;
            const int TIPO_LINHA_DADOS_IDENTIFICACAO = 1;
            const int TIPO_LINHA_DADOS_BASICOS = 2;
            const int TIPO_LINHA_DADOS_ENDERECO = 3;
            const int TIPO_LINHA_DADOS_PESSOA_FISICA = 4;
            const int TIPO_LINHA_DADOS_PESSOA_JURIDICA = 5;
            const int CODIGO_OPERACAO = 0;
            const int ID_PAIS_BRASIL = 1;
            const int TIPO_DOCUMENTO_CARTEIRA_IDENTIDADE = 1;
            const int CODIGO_OCUPACAO_PROFISSIONAL = 0;
            const int CODIGO_ATIVIDADE_ECONOMICA = 74;

            const string SEXO_MASCULINO = "M";
            const string TIPO_ENDERECO_COMERCIAL = "C";
            const string NATUREZA_FISCAL_ISENTO = "I";
            const string NATUREZA_FISCAL_TRIBUTAVEL = "T";
            const string SIM = "S";
            const string NAO = "N";
            const string TIPO_PESSOA_FISICA = "F";
            const string TIPO_PESSOA_JURIDICA = "J";
            const string ACAO_INCLUSAO = "I";
            const string ACAO_ALTERACAO = "A";
            const string PESSOA_POLITICAMENTE_EXPOSTA_NAO_INFORMADO = "F";

            const int QUANTIDADE_LINHAS_COMITENTE = 3;

            DateTime dataExportacaoDiaSeguinte = dataExportacao.Date.AddDays(1);

            PessoaCollection pessoas = new PessoaCollection();
            PessoaQuery pessoaQuery = new PessoaQuery("P");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            ClienteRendaFixaQuery clienteRendaFixaQuery = new ClienteRendaFixaQuery("CRF");

            pessoaQuery.InnerJoin(clienteRendaFixaQuery).On(pessoaQuery.IdPessoa == clienteRendaFixaQuery.IdCliente);
            pessoaQuery.InnerJoin(clienteQuery).On(pessoaQuery.IdPessoa == clienteQuery.IdCliente);
            
            pessoaQuery.es.DefaultConjunction = EntitySpaces.Interfaces.esConjunction.Or;
            pessoaQuery.Where(clienteRendaFixaQuery.AtivoCetip == "S" & clienteQuery.DataAtualizacao.GreaterThanOrEqual(dataExportacao) & clienteQuery.DataAtualizacao.LessThan(dataExportacaoDiaSeguinte));            
            
            
            pessoaQuery.Where(clienteRendaFixaQuery.AtivoCetip == "S" & pessoaQuery.DataUltimaAlteracao.GreaterThanOrEqual(dataExportacao) & pessoaQuery.DataUltimaAlteracao.LessThan(dataExportacaoDiaSeguinte));

            pessoaQuery.Where(clienteRendaFixaQuery.AtivoCetip == "S" & pessoaQuery.DataCadatro.GreaterThanOrEqual(dataExportacao) & pessoaQuery.DataCadatro.LessThan(dataExportacaoDiaSeguinte));
            
            pessoas.Load(pessoaQuery);

            ClienteCetipViewModel clienteCetipViewModel = new ClienteCetipViewModel();

            #region Header
            clienteCetipViewModel.header.IdSistema = ID_SISTEMA;
            clienteCetipViewModel.header.IdTipoLinha = TIPO_LINHA_HEADER;
            clienteCetipViewModel.header.CodigoOperacao = CODIGO_OPERACAO;

            int codigoBovespa = ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault;
            AgenteMercado agenteMercado = new AgenteMercadoCollection().BuscaAgenteMercadoPorCodigoBovespa(codigoBovespa);

            clienteCetipViewModel.header.EntidadeGerouArquivo = agenteMercado.Apelido;
            clienteCetipViewModel.header.Data = DateTime.Today;

            #endregion

            #region Linhas Detalhe

            foreach (Pessoa pessoa in pessoas)
            {
                Cliente cliente = new Cliente();
                if (!cliente.LoadByPrimaryKey(pessoa.IdPessoa.Value))
                {
                    continue;
                }

                if (cliente.TipoControle.Value != (byte)TipoControleCliente.Completo && cliente.TipoControle.Value != (byte)TipoControleCliente.Carteira)
                {
                    continue;
                }

                //Checar se possui endereço
                PessoaEnderecoCollection enderecos = new PessoaEnderecoCollection();
                enderecos.Query.Where(enderecos.Query.IdPessoa.Equal(pessoa.IdPessoa.Value));

                if (!enderecos.Load(enderecos.Query))
                {
                    continue;
                }

                PessoaDocumentoCollection documentos = new PessoaDocumentoCollection();
                documentos.Query.Where(documentos.Query.IdPessoa.Equal(pessoa.IdPessoa.Value));
                documentos.Query.Load();
                

                RegistroClienteCetip registro = new RegistroClienteCetip(pessoa.Tipo.Value);

                #region Dados Identificação Comitente
                registro.dadosIdentificacao.IdSistema = ID_SISTEMA;
                registro.dadosIdentificacao.IdTipoLinha = TIPO_LINHA_DADOS_IDENTIFICACAO;
                registro.dadosIdentificacao.CodigoOperacao = CODIGO_OPERACAO;
                registro.dadosIdentificacao.TitularContaCliente = CONTA_PARTICIPANTE;

                bool isPessoaFisica = true;
                if (pessoa.Tipo == (byte)TipoPessoa.Fisica)
                {
                    registro.dadosIdentificacao.TipoPessoa = TIPO_PESSOA_FISICA;
                    registro.dadosIdentificacao.CPFCNPJ = Utilitario.RemoveCaracteresEspeciais(pessoa.Cpfcnpj).PadLeft(11, '0');
                }
                else
                {
                    isPessoaFisica = false;
                    registro.dadosIdentificacao.TipoPessoa = TIPO_PESSOA_JURIDICA;
                    registro.dadosIdentificacao.CPFCNPJ = Utilitario.RemoveCaracteresEspeciais(pessoa.Cpfcnpj).PadLeft(14, '0');
                }

                registro.dadosIdentificacao.ContaDeposito = Convert.ToString(pessoa.IdPessoa.Value);
                //registro.dadosIdentificacao.ContaClienteSELIC;

                ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();

                string naturezaFiscal = NATUREZA_FISCAL_TRIBUTAVEL;
                if (clienteRendaFixa.LoadByPrimaryKey(cliente.IdCliente.Value) && clienteRendaFixa.IsentoIR == "S")
                {
                    naturezaFiscal = NATUREZA_FISCAL_ISENTO;
                }

                registro.dadosIdentificacao.NaturezaFiscal = naturezaFiscal;

                //Por enquanto vamos fazer apenas inclusao... 
                //futuramente precisamos de um flag de exportado pra Cetip. Só olhar pra data alteracao pode nao ser o bastante pois pode ter sido alterado antes da exportação
                //registro.dadosIdentificacao.Acao = pessoa.DataCadatro.Value == pessoa.DataUltimaAlteracao ? ACAO_INCLUSAO : ACAO_ALTERACAO;
                //registro.dadosIdentificacao.Acao = (cliente.DataAtualizacao == pessoa.DataCadatro | pessoa.DataUltimaAlteracao == pessoa.DataCadatro) ? ACAO_INCLUSAO : ACAO_ALTERACAO;                
                if (cliente.DataEnvioCetip == null)
                {
                    registro.dadosIdentificacao.Acao = ACAO_INCLUSAO;
                    cliente.DataEnvioCetip = DateTime.Now;
                    cliente.Save();
                }
                else
                {
                    registro.dadosIdentificacao.Acao = ACAO_ALTERACAO;
                }

                registro.dadosIdentificacao.ConfirmacaoInstituicao = SIM;
                registro.dadosIdentificacao.QuantidadeLinhas = QUANTIDADE_LINHAS_COMITENTE;
                #endregion

                #region Dados Básicos Comitente
                registro.dadosBasicos.IdSistema = ID_SISTEMA;
                registro.dadosBasicos.IdTipoLinha = TIPO_LINHA_DADOS_BASICOS;
                registro.dadosBasicos.CodigoOperacao = CODIGO_OPERACAO;
                registro.dadosBasicos.Nome = pessoa.Nome;
                registro.dadosBasicos.InvestidorEstrangeiro = NAO;
                registro.dadosBasicos.Nacionalidade = Convert.ToString(ID_PAIS_BRASIL);

                #endregion

                #region Endereço Comitente
                PessoaEndereco enderecoPrincipal = enderecos[0];
                foreach (PessoaEndereco endereco in enderecos)
                {
                    if (endereco.RecebeCorrespondencia == "S")
                    {
                        enderecoPrincipal = endereco;
                        break;
                    }
                }
                registro.endereco.IdSistema = ID_SISTEMA;
                registro.endereco.IdTipoLinha = TIPO_LINHA_DADOS_ENDERECO;
                registro.endereco.CodigoOperacao = CODIGO_OPERACAO;
                if (!string.IsNullOrEmpty(enderecoPrincipal.Endereco))
                {
                    registro.endereco.TipoEndereco = String.IsNullOrEmpty(enderecoPrincipal.TipoEndereco) ? TIPO_ENDERECO_COMERCIAL : enderecoPrincipal.TipoEndereco;
                }
                registro.endereco.Endereco = enderecoPrincipal.Endereco;
                registro.endereco.Numero = enderecoPrincipal.Numero;
                registro.endereco.Complemento = enderecoPrincipal.Complemento;
                registro.endereco.Bairro = enderecoPrincipal.Bairro;
                registro.endereco.CEP = Utilitario.RemoveCaracteresEspeciais(enderecoPrincipal.Cep);

                int codigoMunicipioCetip;
                string nomeMunicipioCetip, ufCetip;

                bool matchMunicipio = MapCodigoMunicipio(enderecoPrincipal.Cidade, enderecoPrincipal.Uf,
                    out codigoMunicipioCetip, out nomeMunicipioCetip, out ufCetip);
                if (matchMunicipio)
                {
                    registro.endereco.CodigoMunicipio = codigoMunicipioCetip;
                    registro.endereco.NomeMunicipio = nomeMunicipioCetip;
                    registro.endereco.UF = ufCetip;
                }
                else
                {
                    registro.endereco.CodigoMunicipio = 999999;
                    registro.endereco.NomeMunicipio = "";
                    registro.endereco.UF = "";
                }
                registro.endereco.Pais = Convert.ToString(ID_PAIS_BRASIL);
                registro.endereco.FILLER = "";

                PessoaTelefone telefone = pessoa.PessoaTelefone;
                if (telefone != null && !String.IsNullOrEmpty(telefone.Ddd) && !String.IsNullOrEmpty(telefone.Numero))
                {
                    registro.endereco.TipoTelefone = ""; //TODO: mapear com os tipos da Cetip
                    registro.endereco.DDDTelefone = telefone.Ddd;
                    registro.endereco.NumeroTelefone = Utilitario.RemoveCaracteresEspeciais(telefone.Numero);
                    registro.endereco.Ramal = telefone.Ramal;
                }

                string email = pessoa.Email;
                if (!string.IsNullOrEmpty(email))
                {
                    registro.endereco.TipoEmail = ""; //TODO: mapear com os tipos da Cetip
                    registro.endereco.Email = email;
                }

                #endregion

                #region Detalhes Pessoa Fisica
                if (isPessoaFisica)
                {
                    registro.detalhesPF.IdSistema = ID_SISTEMA;
                    registro.detalhesPF.IdTipoLinha = TIPO_LINHA_DADOS_PESSOA_FISICA;
                    registro.detalhesPF.CodigoOperacao = CODIGO_OPERACAO;
                    registro.detalhesPF.DataNascimento = pessoa.DataNascimento.Value;
                    registro.detalhesPF.Sexo = String.IsNullOrEmpty(pessoa.Sexo) ? SEXO_MASCULINO : pessoa.Sexo;
                    registro.detalhesPF.FiliacaoPai = pessoa.FiliacaoNomePai;
                    registro.detalhesPF.FiliacaoMae = pessoa.FiliacaoNomeMae;
                    registro.detalhesPF.NaturalidadeMunicipio = "";
                    registro.detalhesPF.NaturalidadeUF = ""; //OPCIONAL
                    registro.detalhesPF.EstadoCivil = MapEstadoCivil(pessoa.EstadoCivil);

                    if (documentos.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(documentos[0].NumeroDocumento) && !string.IsNullOrEmpty(documentos[0].OrgaoEmissor))
                        {
                            registro.detalhesPF.TipoDocumentoIdentificacao = TIPO_DOCUMENTO_CARTEIRA_IDENTIDADE;
                            registro.detalhesPF.NumeroDocumentoIdentificacao = documentos[0].NumeroDocumento;
                            registro.detalhesPF.OrgaoExpedidor = documentos[0].OrgaoEmissor;
                            registro.detalhesPF.DataExpedicao = documentos[0].DataExpedicao.HasValue ? documentos[0].DataExpedicao.Value : new DateTime(1900, 1, 1);
                        }
                    }

                    registro.detalhesPF.OcupacaoProfissionalPrincipal = Convert.ToString(CODIGO_OCUPACAO_PROFISSIONAL);
                    registro.detalhesPF.PessoaPoliticamenteExposta = string.IsNullOrEmpty(pessoa.PessoaPoliticamenteExposta) ?
                        PESSOA_POLITICAMENTE_EXPOSTA_NAO_INFORMADO :
                        pessoa.PessoaPoliticamenteExposta;
                }
                #endregion

                #region Detalhes Pessoa Juridica
                if (!isPessoaFisica)
                {
                    registro.detalhesPJ.IdSistema = ID_SISTEMA;
                    registro.detalhesPJ.IdTipoLinha = TIPO_LINHA_DADOS_PESSOA_JURIDICA;
                    registro.detalhesPJ.CodigoOperacao = CODIGO_OPERACAO;
                    registro.detalhesPJ.DataConstituicao = pessoa.DataNascimento.HasValue ? pessoa.DataNascimento.Value : new DateTime(1900, 1, 1);
                    registro.detalhesPJ.AtividadeEconomica = Convert.ToString(CODIGO_ATIVIDADE_ECONOMICA);
                    registro.detalhesPJ.GrupoEconomico = "";
                }
                #endregion

                clienteCetipViewModel.registros.Add(registro);
            }

            #endregion

            return clienteCetipViewModel;

        }

        private int MapEstadoCivil(int? estadoCivil)
        {
            const int CASADO_CETIP = 11;
            const int DIVORCIADO_CETIP = 6;
            const int OUTROS_CETIP = 10;
            const int SOLTEIRO_CETIP = 1;
            const int VIUVO_CETIP = 7;

            int estadoCivilCetip = OUTROS_CETIP;
            int estadoCivilValue = estadoCivil.HasValue ? estadoCivil.Value : (int)EstadoCivilPessoa.Outros;

            if (estadoCivilValue == (int)EstadoCivilPessoa.Casado)
            {
                estadoCivilCetip = CASADO_CETIP;
            }
            else if (estadoCivilValue == (int)EstadoCivilPessoa.Divorciado)
            {
                estadoCivilCetip = DIVORCIADO_CETIP;
            }
            else if (estadoCivilValue == (int)EstadoCivilPessoa.NaoAplicavel)
            {
                estadoCivilCetip = OUTROS_CETIP;
            }
            else if (estadoCivilValue == (int)EstadoCivilPessoa.Outros)
            {
                estadoCivilCetip = OUTROS_CETIP;
            }
            else if (estadoCivilValue == (int)EstadoCivilPessoa.Solteiro)
            {
                estadoCivilCetip = SOLTEIRO_CETIP;
            }
            else if (estadoCivilValue == (int)EstadoCivilPessoa.Viuvo)
            {
                estadoCivilCetip = VIUVO_CETIP;
            }

            return estadoCivilCetip;
        }

        private void InicializaMunicipiosDictionary(){
            string municipiosCetipFilename = AppDomain.CurrentDomain.BaseDirectory + "\\app_data\\municipios_cetip.txt";

            string[] linhasMunicipios = System.IO.File.ReadAllLines(municipiosCetipFilename);

            this.municipiosDictionary = new Dictionary<string, string>();
            foreach (string linhaMunicipio in linhasMunicipios)
            {
                string[] linhaSplitted = linhaMunicipio.Split(';');
                string codigoMunicipioSplitted = linhaSplitted[0].Trim();
                string nomeMunicipioSplitted = linhaSplitted[1].ToUpper().Trim();
                string ufMunicipioSplitted = linhaSplitted[2].ToUpper().Trim();
                this.municipiosDictionary.Add(nomeMunicipioSplitted + ";" + ufMunicipioSplitted, codigoMunicipioSplitted);
            }

        }

        private bool MapCodigoMunicipio(string nomeMunicipio, string uf, out int codigoMunicipioCetip, out string nomeMunicipioCetip, out string ufCetip)
        {
            codigoMunicipioCetip = 0;
            nomeMunicipioCetip = "";
            ufCetip = "";

            if (this.municipiosDictionary == null)
            {
                this.InicializaMunicipiosDictionary();
            }

            //Remover acentos
            byte[] tempBytes;
            tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(nomeMunicipio);
            nomeMunicipio = System.Text.Encoding.UTF8.GetString(tempBytes);
            nomeMunicipio = nomeMunicipio.Trim().ToUpper();
            uf = uf.Trim().ToUpper();

            bool match = false;
            string keyMunicipio = nomeMunicipio + ";" + uf;
            if (municipiosDictionary.ContainsKey(keyMunicipio))
            {
                codigoMunicipioCetip = Convert.ToInt32(municipiosDictionary[keyMunicipio]);
                ufCetip = uf;
                nomeMunicipioCetip = nomeMunicipio;
                match = true;
            }
            return match;
        }

        private int MapCodigoMunicipioOld(int? codigoMunicipioSinacor)
        {
            const int SAOPAULO_SINACOR = 1;
            const int SAOPAULO_CETIP = 355030;
            const int RIODEJANEIRO_SINACOR = 2;
            const int RIODEJANEIRO_CETIP = 330455;
            const int BELOHORIZONTE_SINACOR = 3;
            const int BELOHORIZONTE_CETIP = 310620;
            const int PORTOALEGRE_SINACOR = 4;
            const int PORTOALEGRE_CETIP = 431490;
            const int CURITIBA_SINACOR = 5;
            const int CURITIBA_CETIP = 410690;
            const int DISTRITOFEDERAL_SINACOR = 6;
            const int BRASILIA_CETIP = 530010;
            const int ESPIRITOSANTO_SINACOR = 7;
            const int VITORIA_CETIP = 320530;

            int codigoMunicipioCetip = SAOPAULO_CETIP;
            int codigoMunicipioSinacorValue = codigoMunicipioSinacor.HasValue ? codigoMunicipioSinacor.Value : SAOPAULO_SINACOR;

            if (codigoMunicipioSinacorValue == SAOPAULO_SINACOR)
            {
                codigoMunicipioCetip = SAOPAULO_CETIP;
            }
            else if (codigoMunicipioSinacorValue == RIODEJANEIRO_SINACOR)
            {
                codigoMunicipioCetip = RIODEJANEIRO_CETIP;
            }
            else if (codigoMunicipioSinacorValue == BELOHORIZONTE_SINACOR)
            {
                codigoMunicipioCetip = BELOHORIZONTE_CETIP;
            }
            else if (codigoMunicipioSinacorValue == PORTOALEGRE_SINACOR)
            {
                codigoMunicipioCetip = PORTOALEGRE_CETIP;
            }
            else if (codigoMunicipioSinacorValue == CURITIBA_SINACOR)
            {
                codigoMunicipioCetip = CURITIBA_CETIP;
            }
            else if (codigoMunicipioSinacorValue == DISTRITOFEDERAL_SINACOR)
            {
                codigoMunicipioCetip = BRASILIA_CETIP;
            }
            else if (codigoMunicipioSinacorValue == ESPIRITOSANTO_SINACOR)
            {
                codigoMunicipioCetip = VITORIA_CETIP;
            }

            return codigoMunicipioCetip;
        }

    }
}
