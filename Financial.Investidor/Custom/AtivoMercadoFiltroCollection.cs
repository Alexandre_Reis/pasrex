/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/12/2014 14:37:19
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.RendaFixa;
using Financial.Swap;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Swap.Controller;
using Financial.Common;


namespace Financial.Investidor
{
    public partial class AtivoMercadoFiltroCollection : esAtivoMercadoFiltroCollection
    {
        public AtivoMercadoFiltroCollection PopulaRetornaTabelaTemporaria(int idCliente, bool rendaFixa, bool swap, bool fundos, bool bmf, bool bolsa, DateTime dataInicio, DateTime dataFim)
        {
            this.DeletaAtivoMercadoFiltro();

            ClienteQuery clienteQuery = new ClienteQuery("Cliente");
            TituloRendaFixaQuery tituloQuery = new TituloRendaFixaQuery("Titulo");
            OperacaoSwapQuery operacaoSwapQuery = new OperacaoSwapQuery("OperacaoSwap");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("ativoBolsa");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("ativoBMF");
            CarteiraQuery carteiraQuery = new CarteiraQuery("Carteira");
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("PosicaoRF");
            PosicaoBolsaHistoricoQuery posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("posicaoBolsa");
            PosicaoBMFHistoricoQuery posicaoBMFQuery = new PosicaoBMFHistoricoQuery("posicaoBMF");
            PosicaoFundoHistoricoQuery posicaoFundoQuery = new PosicaoFundoHistoricoQuery("posicaoFundo");
            PosicaoSwapHistoricoQuery posicaoSwapQuery = new PosicaoSwapHistoricoQuery("posicaoSwap");

            AtivoMercadoFiltroCollection ativoMercadoFiltroCollectionColl = new AtivoMercadoFiltroCollection();
            AtivoMercadoFiltro ativoMercado;
            ClienteCollection clienteCollAux = new ClienteCollection();

            string fieldIdAtivo = "idAtivo";
            string fieldDescricao = "descricao";

            #region Renda Fixa
            if (rendaFixa)
            {
                clienteQuery.es.Distinct = true;
                clienteQuery.Select(tituloQuery.IdTitulo.As(fieldIdAtivo),
                                    tituloQuery.DescricaoCompleta.As(fieldDescricao));
                clienteQuery.InnerJoin(posicaoRendaFixaQuery).On(posicaoRendaFixaQuery.IdCliente.Equal(clienteQuery.IdCliente));
                clienteQuery.InnerJoin(tituloQuery).On(posicaoRendaFixaQuery.IdTitulo.Equal(tituloQuery.IdTitulo));
                clienteQuery.Where(clienteQuery.IdCliente.Equal(idCliente) & posicaoRendaFixaQuery.DataHistorico.GreaterThanOrEqual(dataInicio) & posicaoRendaFixaQuery.DataHistorico.LessThanOrEqual(dataFim));

                if (clienteCollAux.Load(clienteQuery))
                {
                    int idTipoAtivo = (int)TipoAtivoAuxiliar.OperacaoRendaFixa; 
                    string TipoAtivo = ((int)TipoAtivoAuxiliar.OperacaoRendaFixa).ToString();
                    foreach (Cliente cliente in clienteCollAux)
                    {
                        ativoMercado = ativoMercadoFiltroCollectionColl.AddNew();
                        ativoMercado.CompositeKey = TipoAtivo + "|" + cliente.GetColumn(fieldIdAtivo).ToString();
                        ativoMercado.Descricao = cliente.GetColumn(fieldDescricao).ToString();
                        ativoMercado.IdCliente = idCliente;
                        ativoMercado.IdAtivo = cliente.GetColumn(fieldIdAtivo).ToString();
                        ativoMercado.TipoMercado = idTipoAtivo;
                    }

                }
            }
            #endregion

            #region Ativo Bolsa
            if (bolsa)
            {
                clienteQuery = new ClienteQuery("Cliente");
                clienteCollAux = new ClienteCollection();
                clienteQuery.es.Distinct = true;
                clienteQuery.Select(ativoBolsaQuery.CdAtivoBolsa.As(fieldIdAtivo),
                                    ativoBolsaQuery.Descricao.As(fieldDescricao));
                clienteQuery.InnerJoin(posicaoBolsaQuery).On(posicaoBolsaQuery.IdCliente.Equal(clienteQuery.IdCliente));
                clienteQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa.Equal(posicaoBolsaQuery.CdAtivoBolsa));
                clienteQuery.Where(clienteQuery.IdCliente.Equal(idCliente) & posicaoBolsaQuery.DataHistorico.GreaterThanOrEqual(dataInicio) & posicaoBolsaQuery.DataHistorico.LessThanOrEqual(dataFim));

                if (clienteCollAux.Load(clienteQuery))
                {
                    int idTipoAtivo = (int)TipoAtivoAuxiliar.OperacaoBolsa; 
                    string TipoAtivo = ((int)TipoAtivoAuxiliar.OperacaoBolsa).ToString();
                    foreach (Cliente cliente in clienteCollAux)
                    {
                        ativoMercado = ativoMercadoFiltroCollectionColl.AddNew();
                        ativoMercado.CompositeKey = TipoAtivo + "|" + cliente.GetColumn(fieldIdAtivo).ToString();
                        ativoMercado.Descricao = cliente.GetColumn(fieldDescricao).ToString();
                        ativoMercado.IdCliente = idCliente;
                        ativoMercado.IdAtivo = cliente.GetColumn(fieldIdAtivo).ToString();
                        ativoMercado.TipoMercado = idTipoAtivo;
                    }

                }
            }
            #endregion

            #region Ativo BMF
            if (bmf)
            {
                clienteQuery = new ClienteQuery("Cliente");
                clienteCollAux = new ClienteCollection();
                clienteQuery.es.Distinct = true;
                clienteQuery.Select((posicaoBMFQuery.CdAtivoBMF.Cast(esCastType.String) + "-" + posicaoBMFQuery.Serie.Cast(esCastType.String)).As(fieldIdAtivo),
                                    (posicaoBMFQuery.CdAtivoBMF.Cast(esCastType.String) + "-" + posicaoBMFQuery.Serie.Cast(esCastType.String)).As(fieldDescricao));
                clienteQuery.InnerJoin(posicaoBMFQuery).On(posicaoBMFQuery.IdCliente.Equal(clienteQuery.IdCliente));
                clienteQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF.Equal(posicaoBMFQuery.CdAtivoBMF) & ativoBMFQuery.Serie.Equal(posicaoBMFQuery.Serie));
                clienteQuery.Where(clienteQuery.IdCliente.Equal(idCliente) & posicaoBMFQuery.DataHistorico.GreaterThanOrEqual(dataInicio) & posicaoBMFQuery.DataHistorico.LessThanOrEqual(dataFim));

                if (clienteCollAux.Load(clienteQuery))
                {
                    int idTipoAtivo = (int)TipoAtivoAuxiliar.OperacaoBMF; 
                    string TipoAtivo = ((int)TipoAtivoAuxiliar.OperacaoBMF).ToString();
                    foreach (Cliente cliente in clienteCollAux)
                    {
                        ativoMercado = ativoMercadoFiltroCollectionColl.AddNew();
                        ativoMercado.CompositeKey = TipoAtivo + "|" + cliente.GetColumn(fieldIdAtivo).ToString();
                        ativoMercado.Descricao = cliente.GetColumn(fieldDescricao).ToString();
                        ativoMercado.IdCliente = idCliente;
                        ativoMercado.IdAtivo = cliente.GetColumn(fieldIdAtivo).ToString();
                        ativoMercado.TipoMercado = idTipoAtivo;
                    }
                }
            }
            #endregion

            #region Ativo Fundo
            if (fundos)
            {
                clienteQuery = new ClienteQuery("Cliente");
                clienteCollAux = new ClienteCollection();
                clienteQuery.es.Distinct = true;
                clienteQuery.Select(posicaoFundoQuery.IdCarteira.As(fieldIdAtivo),
                                    carteiraQuery.Nome.As(fieldDescricao));
                clienteQuery.InnerJoin(posicaoFundoQuery).On(posicaoFundoQuery.IdCliente.Equal(clienteQuery.IdCliente));
                clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira.Equal(posicaoFundoQuery.IdCarteira));
                clienteQuery.Where(clienteQuery.IdCliente.Equal(idCliente) & posicaoFundoQuery.DataHistorico.GreaterThanOrEqual(dataInicio) & posicaoFundoQuery.DataHistorico.LessThanOrEqual(dataFim));

                if (clienteCollAux.Load(clienteQuery))
                {
                    int idTipoAtivo = (int)TipoAtivoAuxiliar.OperacaoFundos; 
                    string TipoAtivo = ((int)TipoAtivoAuxiliar.OperacaoFundos).ToString();
                    foreach (Cliente cliente in clienteCollAux)
                    {
                        ativoMercado = ativoMercadoFiltroCollectionColl.AddNew();
                        ativoMercado.CompositeKey = TipoAtivo + "|" + cliente.GetColumn(fieldIdAtivo).ToString();
                        ativoMercado.Descricao = cliente.GetColumn(fieldDescricao).ToString();
                        ativoMercado.IdCliente = idCliente;
                        ativoMercado.IdAtivo = cliente.GetColumn(fieldIdAtivo).ToString();
                        ativoMercado.TipoMercado = idTipoAtivo;
                    }
                }
            }
            #endregion

            #region Ativo Swap
            if (swap)
            {
                clienteQuery = new ClienteQuery("Cliente");
                clienteCollAux = new ClienteCollection();
                string fieldIndiceAtivo = "DescricaoIndiceAtivo";
                string fieldIndicePassivo = "DescricaoIndicePassivo";
                string tableIndiceAtivo = "IndiceAtivo";
                string tableIndicePassivo = "IndicePassivo";
                IndiceQuery indiceQueryAtivo = new IndiceQuery(tableIndiceAtivo);
                IndiceQuery indiceQueryPassivo = new IndiceQuery(tableIndicePassivo);
                PosicaoSwapHistoricoCollection posicaoSwapColl = new PosicaoSwapHistoricoCollection();

                posicaoSwapQuery.Select(posicaoSwapQuery.IdOperacao.As(fieldIdAtivo),
                                        posicaoSwapQuery.DataEmissao,
                                        posicaoSwapQuery.DataVencimento,
                                        //Ativo
                                        indiceQueryAtivo.Descricao.As(fieldIndiceAtivo),
                                        posicaoSwapQuery.PercentualContraParte,
                                        posicaoSwapQuery.TaxaJurosContraParte,
                                        //Passivo
                                        indiceQueryPassivo.Descricao.As(fieldIndicePassivo),
                                        posicaoSwapQuery.Percentual,
                                        posicaoSwapQuery.TaxaJuros);
                posicaoSwapQuery.LeftJoin(indiceQueryAtivo).On(posicaoSwapQuery.IdIndiceContraParte.Equal(indiceQueryAtivo.IdIndice));
                posicaoSwapQuery.LeftJoin(indiceQueryPassivo).On(posicaoSwapQuery.IdIndice.Equal(indiceQueryPassivo.IdIndice));
                posicaoSwapQuery.Where(posicaoSwapQuery.IdCliente.Equal(idCliente) & posicaoSwapQuery.DataHistorico.GreaterThanOrEqual(dataInicio) & posicaoSwapQuery.DataHistorico.LessThanOrEqual(dataFim));

                if (posicaoSwapColl.Load(posicaoSwapQuery))
                {
                    string TipoAtivo = ((int)TipoAtivoAuxiliar.OperacaoSwap).ToString();
                    int idTipoAtivo = (int)TipoAtivoAuxiliar.OperacaoSwap;
                    ControllerSwap controllerSwap = new ControllerSwap();
                    foreach (PosicaoSwapHistorico posicaoSwap in posicaoSwapColl)
                    {
                        ativoMercado = ativoMercadoFiltroCollectionColl.AddNew();
                        ativoMercado.CompositeKey = TipoAtivo + "|" + posicaoSwap.GetColumn(fieldIdAtivo).ToString();
                        ativoMercado.Descricao = controllerSwap.RetornaDescricaoCompleta(posicaoSwap);
                        ativoMercado.IdCliente = idCliente;
                        ativoMercado.IdAtivo = posicaoSwap.GetColumn(fieldIdAtivo).ToString();
                        ativoMercado.TipoMercado = idTipoAtivo;
                    }
                }
            }
            #endregion

            ativoMercadoFiltroCollectionColl.Save();

            return ativoMercadoFiltroCollectionColl;
        }

        private void DeletaAtivoMercadoFiltro()
        {
            AtivoMercadoFiltroCollection ativoMercadoColl = new AtivoMercadoFiltroCollection();
            ativoMercadoColl.LoadAll();
            ativoMercadoColl.MarkAllAsDeleted();
            ativoMercadoColl.Save();
        }
    }
}

