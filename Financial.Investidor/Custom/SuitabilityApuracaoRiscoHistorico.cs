/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 01/09/2015 09:42:12
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Common.Enums;
using Financial.Investidor.Enums;

namespace Financial.Investidor
{
	public partial class SuitabilityApuracaoRiscoHistorico : esSuitabilityApuracaoRiscoHistorico
	{
        public static void createSuitabilityOpcaoHistorico(SuitabilityApuracaoRisco suitabilityApuracaoRisco, TipoOperacaoBanco tipoOperacaoBanco)
        {
            SuitabilityApuracaoRiscoHistorico suitabilityApuracaoRiscoHistorico = new SuitabilityApuracaoRiscoHistorico();
            suitabilityApuracaoRiscoHistorico.DataHistorico = DateTime.Now;
            suitabilityApuracaoRiscoHistorico.IdApuracaoRisco = suitabilityApuracaoRisco.IdApuracaoRisco;
            suitabilityApuracaoRiscoHistorico.IdValidacao = suitabilityApuracaoRisco.IdValidacao;
            suitabilityApuracaoRiscoHistorico.IdPerfilCarteira = suitabilityApuracaoRisco.PerfilCarteira;
            suitabilityApuracaoRiscoHistorico.PerfilRiscoFundo = suitabilityApuracaoRisco.PerfilRiscoFundo;
            suitabilityApuracaoRiscoHistorico.Percentual = suitabilityApuracaoRisco.Percentual;
            suitabilityApuracaoRiscoHistorico.Tipo = tipoOperacaoBanco.ToString();

            SuitabilityValidacao suitabilityValidacao = new SuitabilityValidacao();
            suitabilityValidacao.LoadByPrimaryKey(suitabilityApuracaoRisco.IdValidacao.Value);
            suitabilityApuracaoRiscoHistorico.Validacao = suitabilityValidacao.Descricao;

            SuitabilityPerfilInvestidor suitabilityPerfilInvestidor = new SuitabilityPerfilInvestidor();
            suitabilityPerfilInvestidor.LoadByPrimaryKey(suitabilityApuracaoRisco.PerfilCarteira.Value);
            suitabilityApuracaoRiscoHistorico.PerfilCarteira = suitabilityPerfilInvestidor.Perfil;

            suitabilityApuracaoRiscoHistorico.Criterio = ((SuitabilityCriterio)suitabilityApuracaoRisco.Criterio).ToString();

            suitabilityApuracaoRiscoHistorico.Save();
        }
	}
}
