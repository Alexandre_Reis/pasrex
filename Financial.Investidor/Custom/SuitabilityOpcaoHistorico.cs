/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 26/08/2015 12:03:40
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Common.Enums;

namespace Financial.Investidor
{
	public partial class SuitabilityOpcaoHistorico : esSuitabilityOpcaoHistorico
	{
        public static void createSuitabilityOpcaoHistorico(SuitabilityOpcao suitabilityOpcao, TipoOperacaoBanco tipoOperacaoBanco)
        {
            SuitabilityOpcaoHistorico suitabilityOpcaoHistorico = new SuitabilityOpcaoHistorico();
            suitabilityOpcaoHistorico.DataHistorico = DateTime.Now;
            suitabilityOpcaoHistorico.IdOpcao = suitabilityOpcao.IdOpcao;
            suitabilityOpcaoHistorico.Opcao = suitabilityOpcao.Descricao;
            suitabilityOpcaoHistorico.IdQuestao = suitabilityOpcao.IdQuestao;
            suitabilityOpcaoHistorico.Pontos = suitabilityOpcao.Pontos;
            suitabilityOpcaoHistorico.Tipo = tipoOperacaoBanco.ToString();

            SuitabilityQuestao suitabilityQuestao = new SuitabilityQuestao();
            suitabilityQuestao.LoadByPrimaryKey(suitabilityOpcao.IdQuestao.Value);
            suitabilityOpcaoHistorico.Questao = suitabilityQuestao.Descricao;

            suitabilityOpcaoHistorico.Save();
        }
	}
}
