/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 01/09/2015 16:32:15
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Common.Enums;

namespace Financial.Investidor
{
	public partial class SuitabilityMensagensHistorico : esSuitabilityMensagensHistorico
	{
        public static void createSuitabilityMensagensHistorico(SuitabilityMensagens suitabilityMensagens, TipoOperacaoBanco tipoOperacaoBanco)
        {
            SuitabilityMensagensHistorico suitabilityMensagensHistorico = new SuitabilityMensagensHistorico();
            suitabilityMensagensHistorico.DataHistorico = DateTime.Now;
            suitabilityMensagensHistorico.IdMensagem = suitabilityMensagens.IdMensagem;
            suitabilityMensagensHistorico.Assunto = suitabilityMensagens.Assunto;
            suitabilityMensagensHistorico.Descricao = suitabilityMensagens.Descricao;
            suitabilityMensagensHistorico.ControlaConcordancia = suitabilityMensagens.ControlaConcordancia;
            suitabilityMensagensHistorico.Tipo = tipoOperacaoBanco.ToString();
            suitabilityMensagensHistorico.Save();
        }
	}
}
