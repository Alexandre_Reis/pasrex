﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Investidor.Enums;
using Financial.Util;
using Financial.CRM;
using Financial.Investidor.Exceptions;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Common.Enums;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Financial.InvestidorCotista;
using Financial.Enquadra;
using Financial.Interfaces.Import.YMF;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using Financial.Interfaces.Sinacor;
using Financial.Common;
using Financial.BMF.Exceptions;
using Financial.Bolsa.Exceptions;

namespace Financial.Investidor
{
    public partial class Cliente : esCliente
    {
        private DateTime? dataDiaCalculado = null;

        /// <summary>
        /// Esta property deve ser utilizada em conjunto com os campos "Status" e "DataDia" do cliente. As mesmas devem ter sido carregadas no LoadByPrimaryKey do Cliente
        /// </summary>
        public DateTime? DataDiaCalculado
        {
            get
            {
                if (this.DataDia.HasValue && this.Status.HasValue)
                {
                    this.dataDiaCalculado = this.DataDia.Value;
                    if (this.Status.Value == (byte)StatusCliente.Aberto ||
                        this.Status.Value == (byte)StatusCliente.FechadoComErro)
                    {
                        this.dataDiaCalculado = Calendario.SubtraiDiaUtil(this.DataDia.Value, 1);
                    }
                }
                return this.dataDiaCalculado;
            }
        }

        /// <summary>
        /// True se ativo. False se não ativo.
        /// </summary>
        public bool IsAtivo
        {
            get { return this.StatusAtivo == (byte)StatusAtivoCliente.Ativo; }
        }

        /// <summary>
        /// Carrega o objeto Cliente com o campo Nome.
        /// </summary>
        /// <param name="idCliente"></param>  
        public void BuscaNomeCliente(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Nome)
                 .Where(this.Query.IdCliente == idCliente);

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto Cliente com o campo TipoControle.
        /// </summary>
        /// <param name="idCliente"></param>  
        public void BuscaTipoControle(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.TipoControle)
                 .Where(this.Query.IdCliente == idCliente);

            this.Query.Load();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Cliente é Pessoa Fisica</returns>
        public bool IsTipoClientePessoaFisica()
        {
            return this.IdTipo == TipoClienteFixo.ClientePessoaFisica;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Cliente é Pessoa Juridica</returns>
        public bool IsTipoClientePessoaJuridica()
        {
            return this.IdTipo == TipoClienteFixo.ClientePessoaJuridica;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Cliente é Carteira Própria</returns>
        public bool IsTipoClienteCarteiraPropria()
        {
            return this.IdTipo == TipoClienteFixo.CarteiraPropria;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Cliente é Clube</returns>
        public bool IsTipoClienteClube()
        {
            return this.IdTipo == TipoClienteFixo.Clube;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Cliente é CTVM</returns>
        public bool IsTipoClienteCTVM()
        {
            return this.IdTipo == TipoClienteFixo.CTVM;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Cliente é DTVM</returns>
        public bool IsTipoClienteDTVM()
        {
            return this.IdTipo == TipoClienteFixo.DTVM;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Cliente é Banco</returns>
        public bool IsTipoClienteBanco()
        {
            return this.IdTipo == TipoClienteFixo.Banco;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Cliente é Fundo</returns>
        public bool IsTipoClienteFundo()
        {
            return (this.IdTipo == TipoClienteFixo.Fundo || this.IdTipo == TipoClienteFixo.FDIC);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Cliente é FDIC</returns>
        public bool IsTipoClienteFDIC()
        {
            return this.IdTipo == TipoClienteFixo.FDIC;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Cliente é InvestidorEstrangeiro</returns>
        public bool IsTipoClienteInvestidorEstrangeiro()
        {
            return this.IdTipo == TipoClienteFixo.InvestidorEstrangeiro;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Cliente é Isento IR</returns>
        public bool IsIsentoIR()
        {
            return this.IsentoIR == "S";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se Cliente é Isento IOF</returns>
        public bool IsIsentoIOF()
        {
            return this.IsentoIOF == "S";
        }

        /// <summary>
        /// Indica se Cliente está numa determinada Data
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <returns>bool indicando se Cliente Está numa Determinada Data</returns>
        public bool IsClienteNaData(int idCliente, DateTime data)
        {
            // Carrega o Cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            cliente.LoadByPrimaryKey(campos, idCliente);

            return cliente.DataDia.Value.CompareTo(data) == 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idClienteReplicar"></param>
        /// <returns>Clone do Objeto Cliente com IdClienteReplicar</returns>
        public Cliente ReplicaCliente(int idCliente, int idClienteReplicar)
        {
            //
            Cliente c = new Cliente();
            c.LoadByPrimaryKey(idClienteReplicar);
            c.MarkAllColumnsAsDirty(DataRowState.Added);
            c.IdCliente = idCliente;
            //
            return c;
        }

        //Aberracao: estou retornando um cliente quando o proprio cliente eh o novo cliente !!
        public Cliente CriaClienteViaTemplate(int idCarteira, int idCarteiraClonar, string nome, string apelido, string codigoYMF)
        {
            //                                
            /*
            */
            #region Salva o cliente de acordo com um Template
            Cliente clienteReplicado = new Cliente();
            Cliente cliente = clienteReplicado.ReplicaCliente(idCarteira, idCarteiraClonar);
            // Troca o Nome e o Apelido
            cliente.Nome = nome;
            cliente.Apelido = apelido;
            cliente.DataDia = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            cliente.DataImplantacao = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            cliente.DataInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            cliente.Status = (byte)StatusCliente.Aberto;
            cliente.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
            cliente.CalculaRealTime = "N";
            //
            cliente.StatusAtivo = (byte)StatusAtivoCliente.Ativo;
            //
            cliente.Save();
            #endregion

            #region Salva ClienteBolsa de acordo com o template
            ClienteBolsaCollection clienteBolsaCollection = new ClienteBolsaCollection();
            clienteBolsaCollection.Query.Where(clienteBolsaCollection.Query.IdCliente == idCarteiraClonar);
            clienteBolsaCollection.Query.Load();

            // Troca o idCarteira para o novo IdCarteira
            foreach (ClienteBolsa clienteBolsa in clienteBolsaCollection)
            {
                clienteBolsa.MarkAllColumnsAsDirty(DataRowState.Added);
                clienteBolsa.IdCliente = idCarteira;
                clienteBolsa.CodigoSinacor = idCarteira.ToString();
                clienteBolsa.CodigoSinacor2 = "";
            }
            if (clienteBolsaCollection.HasData)
            {
                clienteBolsaCollection.Save();
            }
            #endregion

            #region Salva ClienteBMF de acordo com o template
            ClienteBMFCollection clienteBMFCollection = new ClienteBMFCollection();
            clienteBMFCollection.Query.Where(clienteBMFCollection.Query.IdCliente == idCarteiraClonar);
            clienteBMFCollection.Query.Load();

            // Troca o idCarteira para o novo IdCarteira
            foreach (ClienteBMF clienteBMF in clienteBMFCollection)
            {
                clienteBMF.MarkAllColumnsAsDirty(DataRowState.Added);
                clienteBMF.IdCliente = idCarteira;
                clienteBMF.CodigoSinacor = idCarteira.ToString();
            }
            if (clienteBMFCollection.HasData)
            {
                clienteBMFCollection.Save();
            }
            #endregion

            #region Salva ClienteRendaFixa de acordo com o template
            ClienteRendaFixaCollection clienteRendaFixaCollection = new ClienteRendaFixaCollection();
            clienteRendaFixaCollection.Query.Where(clienteRendaFixaCollection.Query.IdCliente == idCarteiraClonar);
            clienteRendaFixaCollection.Query.Load();

            // Troca o idCarteira para o novo IdCarteira
            foreach (ClienteRendaFixa clienteRendaFixa in clienteRendaFixaCollection)
            {
                clienteRendaFixa.MarkAllColumnsAsDirty(DataRowState.Added);
                clienteRendaFixa.IdCliente = idCarteira;
            }
            if (clienteRendaFixaCollection.HasData)
            {
                clienteRendaFixaCollection.Save();
            }
            #endregion

            #region Salva ClienteInterface de acordo com o template
            ClienteInterfaceCollection clienteInterfaceCollection = new ClienteInterfaceCollection();
            clienteInterfaceCollection.Query.Where(clienteInterfaceCollection.Query.IdCliente == idCarteiraClonar);
            clienteInterfaceCollection.Query.Load();

            // Troca o idCarteira para o novo IdCarteira
            foreach (ClienteInterface clienteInterface in clienteInterfaceCollection)
            {
                clienteInterface.MarkAllColumnsAsDirty(DataRowState.Added);
                clienteInterface.IdCliente = idCarteira;

                //Se estamos importando da YMF, salvar Codigo YMF passado como parametro... Melhorar isto depois
                clienteInterface.CodigoYMF = codigoYMF;
            }
            if (clienteInterfaceCollection.HasData)
            {
                clienteInterfaceCollection.Save();
            }
            #endregion

            #region Salva Contacorrente default associada ao cliente
            ContaCorrente contaCorrente = new ContaCorrente();
            contaCorrente.IdPessoa = idCarteira;
            contaCorrente.Numero = idCarteira.ToString();

            TipoConta tipoConta = new TipoConta();
            tipoConta.Query.Select(tipoConta.Query.IdTipoConta.Min());
            tipoConta.Query.Load();

            if (!tipoConta.IdTipoConta.HasValue)
            {
                tipoConta = new TipoConta();
                tipoConta.Descricao = "Conta Depósito";
                tipoConta.Save();
            }
            contaCorrente.IdTipoConta = tipoConta.IdTipoConta.Value;
            
            contaCorrente.IdMoeda = (int)ListaMoedaFixo.Real;
            contaCorrente.ContaDefault = "S";

            contaCorrente.Save();
            #endregion

            #region Salva CodigoClienteAgente default associada ao cliente
            CodigoClienteAgenteCollection codigoClienteAgenteCollection = new CodigoClienteAgenteCollection();
            codigoClienteAgenteCollection.Query.Where(codigoClienteAgenteCollection.Query.IdCliente == idCarteiraClonar);
            codigoClienteAgenteCollection.Query.Load();

            // Troca o idCarteira para o novo IdCarteira
            foreach (CodigoClienteAgente codigoClienteAgente in codigoClienteAgenteCollection)
            {
                codigoClienteAgente.MarkAllColumnsAsDirty(DataRowState.Added);
                codigoClienteAgente.IdCliente = idCarteira;
                codigoClienteAgente.CodigoClienteBMF = idCarteira;
                codigoClienteAgente.CodigoClienteBovespa = idCarteira;
            }
            if (codigoClienteAgenteCollection.HasData)
            {
                codigoClienteAgenteCollection.Save();
            }
            #endregion

            #region Salva a carteira de acordo com um Template
            Carteira carteiraReplicada = new Carteira();
            Carteira carteira = carteiraReplicada.ReplicaCarteira(idCarteira, idCarteiraClonar);
            // Troca o Nome e o Apelido
            carteira.Nome = nome;
            carteira.Apelido = apelido;
            carteira.DataInicioCota = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            //
            carteira.StatusAtivo = (byte)StatusAtivoCarteira.Ativo;
            //
            carteira.Save();
            #endregion

            #region Salva TabelaTaxaAdministracao
            TabelaTaxaAdministracaoCollection tabelaTaxaAdministracaoCollection = new TabelaTaxaAdministracaoCollection();
            tabelaTaxaAdministracaoCollection.Query.Where(tabelaTaxaAdministracaoCollection.Query.IdCarteira == idCarteiraClonar);
            tabelaTaxaAdministracaoCollection.Query.Load();

            // Troca o idCarteira para o novo IdCarteira
            foreach (TabelaTaxaAdministracao tabelaTaxaAdministracao in tabelaTaxaAdministracaoCollection)
            {
                tabelaTaxaAdministracao.MarkAllColumnsAsDirty(DataRowState.Added);
                tabelaTaxaAdministracao.IdCarteira = idCarteira;
                tabelaTaxaAdministracao.DataReferencia = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            }
            if (tabelaTaxaAdministracaoCollection.HasData)
            {
                tabelaTaxaAdministracaoCollection.Save();
            }
            #endregion

            #region Salva ListaBenchmark
            ListaBenchmarkCollection listaBenchmarkCollection = new ListaBenchmarkCollection();
            listaBenchmarkCollection.Query.Where(listaBenchmarkCollection.Query.IdCarteira == idCarteiraClonar);
            listaBenchmarkCollection.Query.Load();
            //
            // Troca o idCarteira para o novo IdCarteira
            foreach (ListaBenchmark listaBenchmark in listaBenchmarkCollection)
            {
                listaBenchmark.MarkAllColumnsAsDirty(DataRowState.Added);
                listaBenchmark.IdCarteira = idCarteira;
            }
            if (listaBenchmarkCollection.HasData)
            {
                listaBenchmarkCollection.Save();
            }
            #endregion

            #region Salva EnquadraRegras
            EnquadraRegraCollection enquadraRegraCollection = new EnquadraRegraCollection();
            enquadraRegraCollection.Query.Where(enquadraRegraCollection.Query.IdCarteira == idCarteiraClonar);
            enquadraRegraCollection.Query.Load();
            //
            // Troca o idCarteira para o novo IdCarteira
            foreach (EnquadraRegra enquadraRegra in enquadraRegraCollection)
            {
                enquadraRegra.MarkAllColumnsAsDirty(DataRowState.Added);
                enquadraRegra.IdCarteira = idCarteira;
            }
            if (enquadraRegraCollection.HasData)
            {
                enquadraRegraCollection.Save();
            }
            #endregion
            //
            //bool permissaoInternoAuto = Convert.ToBoolean(WebConfig.AppSettings.PermissaoInternoAuto);
            bool permissaoInternoAuto = ParametrosConfiguracaoSistema.Outras.PermissaoInternoAuto;

            #region Se tem permisão automática para usuários internos, salva a permissão
            if (permissaoInternoAuto && cliente.TipoControle != (byte)TipoControleCliente.ApenasCotacao)
            {
                GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
                UsuarioQuery usuarioQuery = new UsuarioQuery("U");

                usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
                usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.In((byte)TipoPerfilGrupo.Administrador, (byte)TipoPerfilGrupo.BackOffice));

                UsuarioCollection usuarioCollection = new UsuarioCollection();
                usuarioCollection.Load(usuarioQuery);

                PermissaoClienteCollection permissaoClienteCollection = new PermissaoClienteCollection();
                foreach (Usuario usuarioPermissao in usuarioCollection)
                {
                    int idUsuario = usuarioPermissao.IdUsuario.Value;

                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    permissaoCliente.IdCliente = Convert.ToInt32(idCarteira);
                    permissaoCliente.IdUsuario = idUsuario;
                    permissaoClienteCollection.AttachEntity(permissaoCliente);
                }
                permissaoClienteCollection.Save();
            }
            #endregion

            //Checa se o cliente tem controle = Carteira com Cota ou Carteira Completa
            //e tb não é fundo de Inv nem clube de Inv.
            //Se sim, então cria cotista com o mesmo nr da carteira (testando antes se já não existe)
            if ((cliente.TipoControle == (byte)TipoControleCliente.CarteiraRentabil ||
                cliente.TipoControle == (byte)TipoControleCliente.Completo) &&
                (!cliente.IsTipoClienteFundo() && !cliente.IsTipoClienteClube()))
            {
                int idCotista = 0;
                Cotista cotistaExiste = new Cotista();
                if (!cotistaExiste.LoadByPrimaryKey(cliente.IdCliente.Value))
                {
                    Cotista cotistaNovo = new Cotista();
                    cotistaNovo.IdCotista = cliente.IdCliente.Value;
                    cotistaNovo.Nome = cliente.Nome;
                    cotistaNovo.Apelido = cliente.Apelido;
                    cotistaNovo.StatusAtivo = (byte)StatusAtivoCliente.Ativo;
                    cotistaNovo.IsentoIR = "S";
                    cotistaNovo.IsentoIOF = "S";

                    cotistaNovo.Save();

                    idCotista = cotistaNovo.IdCotista.Value;
                }

                #region Se tem permisão automática para usuários internos, salva a permissão
                if (permissaoInternoAuto)
                {
                    GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
                    UsuarioQuery usuarioQuery = new UsuarioQuery("U");

                    usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
                    usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.In((byte)TipoPerfilGrupo.Administrador, (byte)TipoPerfilGrupo.BackOffice));

                    UsuarioCollection usuarioCollection = new UsuarioCollection();
                    usuarioCollection.Load(usuarioQuery);

                    PermissaoCotistaCollection permissaoCotistaCollection = new PermissaoCotistaCollection();
                    foreach (Usuario usuarioPermissao in usuarioCollection)
                    {
                        int idUsuario = usuarioPermissao.IdUsuario.Value;

                        PermissaoCotista permissaoCotista = new PermissaoCotista();
                        permissaoCotista.IdUsuario = idUsuario;
                        permissaoCotista.IdCotista = idCotista;
                        permissaoCotistaCollection.AttachEntity(permissaoCotista);
                    }
                    permissaoCotistaCollection.Save();
                }
                #endregion
            }

            return cliente;
        }

        /// <summary>
        /// Seta o flag de StatusRealTime (Executar/NaoExecutar) para o cliente passado.
        /// Para setar o status de Executar, checa primeiro se CalculaRealTime, só seta se este for "S".
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="statusRealTime"></param>
        public void SetaFlagRealTime(int idCliente, byte statusRealTime)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCliente, this.Query.StatusRealTime, this.Query.CalculaRealTime)
                 .Where(this.Query.IdCliente == idCliente);

            this.Query.Load();

            if ((statusRealTime == (byte)StatusRealTimeCliente.Executar && this.CalculaRealTime == "S") ||
                statusRealTime == (byte)StatusRealTimeCliente.NaoExecutar)
            {
                this.StatusRealTime = statusRealTime;
                this.Save();
            }
        }

        public Pessoa RetornaOfficer()
        {
            Pessoa pessoa = new Pessoa();
            if (this.IdOfficer.HasValue)
            {
                if (pessoa.LoadByPrimaryKey(this.IdOfficer.Value))
                {
                    return pessoa;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public bool IsMae(int idCliente)
        {
            CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection();
            carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
            carteiraMaeCollection.Query.Where(carteiraMaeCollection.Query.IdCarteiraMae.Equal(idCliente));
            carteiraMaeCollection.Query.Load();

            return carteiraMaeCollection.Count > 0;
        }

        public List<int> RetornaListaIdsFilhas(int idCliente)
        {
            List<int> lista = new List<int>();

            CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection();
            carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae, carteiraMaeCollection.Query.IdCarteiraFilha);
            carteiraMaeCollection.Query.Where(carteiraMaeCollection.Query.IdCarteiraMae.Equal(idCliente));
            carteiraMaeCollection.Query.Load();

            foreach (CarteiraMae carteiraMae in carteiraMaeCollection)
            {
                lista.Add(carteiraMae.IdCarteiraFilha.Value);
            }

            return lista;
        }

        /*public void ImportaClienteYMF(Financial.Interfaces.Import.YMF.ClienteYMF clienteYMF)
        {
            const int ID_GRUPO_PROCESSAMENTO_CARTEIRAS = 3;

            Pessoa pessoa = new Pessoa();
            pessoa.ImportaClienteYMF(clienteYMF);

            //Codigo Cliente da YMF pode ser string (jogar em CodigoYMF)
            //TODO QUAIS OUTRAS TABELAS SAO CRIADAS NESTE MOMENTO ??? CLIENTEINTERFACE E OUTRAS ?
            
            this.IdCliente = pessoa.IdPessoa;
            this.Nome = pessoa.Nome;
            this.Apelido = pessoa.Apelido;
            this.IdTipo = pessoa.IsPessoaFisica() ? TipoClienteFixo.ClientePessoaFisica : TipoClienteFixo.ClientePessoaJuridica; //Está correta essa logica ?!??!!?
            this.StatusAtivo = (byte)StatusAtivoCliente.Ativo;
            this.Status = (byte)StatusCliente.Aberto;
            this.DataDia = pessoa.DataCadatro; //OU DATA ULTIMA ALTERACAO ??
            this.DataInicio = pessoa.DataCadatro; //OU DATA ULTIMA ALTERACAO ??
            this.DataImplantacao = pessoa.DataCadatro; //OU DATA ULTIMA ALTERACAO ??
            this.ApuraGanhoRV = "A";
            this.TipoControle = (byte)TipoControleCliente.Completo;
            this.IsentoIR = "S";
            this.IsentoIOF = "S";
            this.IdMoeda = (short)Financial.Common.Enums.ListaMoedaFixo.Real;
            this.IdGrupoProcessamento = ID_GRUPO_PROCESSAMENTO_CARTEIRAS; //FIXO ???
            this.IsProcessando = "N";
            this.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
            this.CalculaRealTime = "N";
            this.CalculaGerencial = "N";
            this.ZeraCaixa = "N";
            this.DescontaTributoPL = "N";
            this.GrossUP = "N";
            //this.Save();

            ClienteInterface clienteInterfaceInsert = new ClienteInterface();
            clienteInterfaceInsert.CodigoYMF = clienteYMF.CdCliente;
            //clienteInterfaceInsert.Save();

        }*/


        public void ImportaFundoYMF(FundoYMF fundoYMF,
            FundoYMFInMemory fundoYMFInMemory, out ClienteInterface clienteInterface,
            out ClienteBolsa clienteBolsa, out ClienteBMF clienteBMF,
            out ClienteRendaFixa clienteRendaFixa, out ContaCorrente contaCorrente,
            out PermissaoClienteCollection permissaoClienteCollection
            )
        {
            const int ID_GRUPO_PROCESSAMENTO_FUNDOS = 1;

            Pessoa pessoa = new PessoaCollection().BuscaPessoaPorCodigoInterface(fundoYMF.CdCliente);
            if (pessoa == null)
            {
                throw new Exception("Não foi possível encontrar o cliente cadastrado com o código YMF: " + fundoYMF.CdCliente);
            }

            clienteInterface = new ClienteInterface();
            if (!this.IdCliente.HasValue)
            {
                //Estamos inserindo um novo cliente
                this.IdCliente = pessoa.IdPessoa;

                //Criar conta corrente
                contaCorrente = new ContaCorrente();
                contaCorrente.IdPessoa = this.IdCliente;
                contaCorrente.Numero = this.IdCliente.Value.ToString();
                
                TipoConta tipoConta = new TipoConta();
                tipoConta.Query.Select(tipoConta.Query.IdTipoConta.Min());
                tipoConta.Query.Load();

                if (!tipoConta.IdTipoConta.HasValue)
                {
                    tipoConta = new TipoConta();
                    tipoConta.Descricao = "Conta Depósito";
                    tipoConta.Save();
                }
                contaCorrente.IdTipoConta = tipoConta.IdTipoConta.Value;
                
                contaCorrente.IdMoeda = (int)ListaMoedaFixo.Real;
                contaCorrente.ContaDefault = "S";

                #region Cria permissão para todos os usuários (internos) do sistema
                GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
                UsuarioQuery usuarioQuery = new UsuarioQuery("U");

                usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
                usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.In((byte)TipoPerfilGrupo.Administrador, (byte)TipoPerfilGrupo.BackOffice));

                UsuarioCollection usuarioCollection = new UsuarioCollection();
                usuarioCollection.Load(usuarioQuery);

                permissaoClienteCollection = new PermissaoClienteCollection();
                foreach (Usuario usuarioPermissao in usuarioCollection)
                {
                    int idUsuario = usuarioPermissao.IdUsuario.Value;

                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    permissaoCliente.IdCliente = this.IdCliente;
                    permissaoCliente.IdUsuario = idUsuario;
                    permissaoClienteCollection.AttachEntity(permissaoCliente);
                }
                #endregion

            }
            else
            {
                contaCorrente = null;
                permissaoClienteCollection = null;
            }


            if (!clienteInterface.LoadByPrimaryKey(this.IdCliente.Value))
            {
                //Criar cliente interface
                clienteInterface.IdCliente = this.IdCliente;
            }

            //Guardar codigoYMF
            clienteInterface.CodigoYMF = fundoYMF.CdFundo;

            //Guardar codigo retencao DIRF
            FundoIRYMF fundoIRYMF = fundoYMFInMemory.FundoIRDictionary[fundoYMF.CdFundo];
            clienteInterface.CodigoRetencaoDIRF = fundoIRYMF.CdRetencaoDirf.Trim();


            this.Nome = pessoa.Nome;
            this.Apelido = pessoa.Apelido;

            FundoParametroYMF fundoParametroYMF = fundoYMFInMemory.FundoParametroDictionary[fundoYMF.CdFundo];
            TipoFundoYMF tipoFundoYMF;
            try{
                tipoFundoYMF = fundoYMFInMemory.TipoFundoDictionary[fundoParametroYMF.CdTipoFundo];
            }catch{
                tipoFundoYMF = new TipoFundoYMF();
                tipoFundoYMF.IcSnRecebiveis = "N";
            }

            this.IdTipo = tipoFundoYMF.IcSnRecebiveis == "S" ? TipoClienteFixo.FDIC : TipoClienteFixo.Fundo;
            this.StatusAtivo = (byte)StatusAtivoCliente.Ativo;
            this.Status = (byte)StatusCliente.Aberto;
            this.DataDia = DateTime.Now;
            this.DataInicio = DateTime.Now;
            this.DataImplantacao = DateTime.Now;
            this.ApuraGanhoRV = "N";
            this.TipoControle = (byte)TipoControleCliente.Completo;
            this.IsentoIR = "S";
            this.IsentoIOF = "S";
            this.IdMoeda = (short)Financial.Common.Enums.ListaMoedaFixo.Real;
            this.IdGrupoProcessamento = ID_GRUPO_PROCESSAMENTO_FUNDOS;
            this.IsProcessando = "N";
            this.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
            this.CalculaRealTime = "N";
            this.CalculaGerencial = "N";
            this.ZeraCaixa = "N";
            this.DescontaTributoPL = (byte)DescontoPLCliente.BrutoSemImpacto;
            this.GrossUP = (byte)GrossupCliente.NaoFaz;

            clienteBolsa = new ClienteBolsa();
            if (!clienteBolsa.LoadByPrimaryKey(this.IdCliente.Value))
            {
                clienteBolsa.IdCliente = this.IdCliente;

                clienteBolsa.TipoCotacao = (byte)TipoCotacaoBolsa.Fechamento;
                clienteBolsa.IsentoIR = "S";

                // HACK ClienteBolsa.TipoCusto - Forçado para custo Medio
                clienteBolsa.TipoCusto = 1;
                clienteBolsa.CodigoSinacor = this.IdCliente.Value.ToString();
            }

            clienteBMF = new ClienteBMF();
            if (!clienteBMF.LoadByPrimaryKey(this.IdCliente.Value))
            {
                clienteBMF.IdCliente = this.IdCliente;
                clienteBMF.TipoPlataforma = (byte)TipoPlataformaOperacional.Normal;
                clienteBMF.Socio = "N";
                clienteBMF.InvestidorInstitucional = "S";
                clienteBMF.TipoCotacao = (byte)TipoCotacaoBMF.Fechamento;
                clienteBMF.CodigoSinacor = this.IdCliente.Value.ToString();
            }

            clienteRendaFixa = new ClienteRendaFixa();
            if (!clienteRendaFixa.LoadByPrimaryKey(this.IdCliente.Value))
            {
                clienteRendaFixa.IdCliente = this.IdCliente;
                clienteRendaFixa.UsaCustoMedio = "N";
                clienteRendaFixa.IsentoIOF = "S";
                clienteRendaFixa.IsentoIR = "S";

            }

        }

        public void GuardaInfoHistoricoCotaYMF(HistoricoCota historicoCotaMaisRecente)
        {
            this.DataDia = historicoCotaMaisRecente.Data;
            this.DataInicio = historicoCotaMaisRecente.Data;
            this.DataImplantacao = historicoCotaMaisRecente.Data;
        }

        /// <summary>
        /// Função usada para retornar os clientes referencia para a cobrança da mensalidade do Financial.
        /// </summary>
        /// <param name="diasDefasagem"></param>
        /// <returns></returns>
        public List<string> RetornaClientesOperacionais(int diasDefasagem)
        {
            List<string> lista = new List<string>();
            List<int> listaIds = new List<int>();

            DateTime dataHoje = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataHoje, diasDefasagem);

            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Select(clienteCollection.Query.IdCliente);
            clienteCollection.Query.Where(clienteCollection.Query.DataDia.NotEqual(clienteCollection.Query.DataImplantacao),
                                          clienteCollection.Query.DataDia.GreaterThanOrEqual(dataAnterior),
                                          clienteCollection.Query.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));
            clienteCollection.Query.OrderBy(clienteCollection.Query.Nome.Ascending);
            clienteCollection.Query.Load();

            foreach (Cliente cliente in clienteCollection)
            {
                listaIds.Add(cliente.IdCliente.Value);
            }

            HistoricoLogCollection historicoLogCollection = new HistoricoLogCollection();
            historicoLogCollection.Query.Select(historicoLogCollection.Query.Descricao);
            historicoLogCollection.Query.Where(historicoLogCollection.Query.DataInicio.GreaterThanOrEqual(dataAnterior) &
                                               (historicoLogCollection.Query.Descricao.Like("%Cálculo Diário%") |
                                                historicoLogCollection.Query.Descricao.Like("%Avança Período%")));
            historicoLogCollection.Query.Load();

            foreach (HistoricoLog historicoLog in historicoLogCollection)
            {
                string descricao = historicoLog.Descricao;
            }

            return lista;
        }

        /// <summary>
        /// Retorna por ref. datas inicio e fim de acordo com os parametros passados. Se não for passado IdCliente considera Fim como Datetime.Now e Inicio sem levar em conta Dt de Implantacao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="tipoPeriodoDatas"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        public void RetornaDatas(int? idCliente, TipoPeriodoDatas tipoPeriodoDatas, ref DateTime dataInicio, ref DateTime dataFim)
        {
            DateTime dataReferencia = new DateTime();
            DateTime dataImplantacao = new DateTime();
            if (idCliente.HasValue)
            {
                Cliente cliente = new Cliente();
                cliente.Query.Select(cliente.Query.DataDia,
                                     cliente.Query.DataImplantacao);
                cliente.Query.Where(cliente.Query.IdCliente.Equal(idCliente.Value));
                cliente.Query.Load();

                dataImplantacao = cliente.DataImplantacao.Value;

                dataReferencia = cliente.DataDia.Value;
            }
            else
            {
                dataReferencia = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            }

            if (tipoPeriodoDatas == TipoPeriodoDatas.Ano)
            {
                dataInicio = Calendario.RetornaPrimeiroDiaUtilAno(dataReferencia);
                dataFim = dataReferencia;
            }
            else if (tipoPeriodoDatas == TipoPeriodoDatas.Ano_1)
            {
                int anoAnterior = dataReferencia.Year - 1;

                dataInicio = Calendario.RetornaPrimeiroDiaUtilAno(new DateTime(anoAnterior, 1, 1), 0);
                dataFim = Calendario.RetornaUltimoDiaUtilAno(dataInicio, 0); ;
            }
            else if (tipoPeriodoDatas == TipoPeriodoDatas.Ano_2)
            {
                int anoAnterior = dataReferencia.Year - 2;

                dataInicio = Calendario.RetornaPrimeiroDiaUtilAno(new DateTime(anoAnterior, 1, 1), 0);
                dataFim = Calendario.RetornaUltimoDiaUtilAno(dataInicio, 0); ;
            }
            else if (tipoPeriodoDatas == TipoPeriodoDatas.Ano_3)
            {
                int anoAnterior = dataReferencia.Year - 3;

                dataInicio = Calendario.RetornaPrimeiroDiaUtilAno(new DateTime(anoAnterior, 1, 1), 0);
                dataFim = Calendario.RetornaUltimoDiaUtilAno(dataInicio, 0); ;
            }
            else if (tipoPeriodoDatas == TipoPeriodoDatas.Inicio)
            {
                dataInicio = dataImplantacao;
                dataFim = dataReferencia;
            }
            else if (tipoPeriodoDatas == TipoPeriodoDatas.Mes)
            {
                dataInicio = Calendario.RetornaPrimeiroDiaUtilMes(dataReferencia);
                dataFim = dataReferencia;
            }
            else if (tipoPeriodoDatas == TipoPeriodoDatas.Meses3)
            {
                dataInicio = Calendario.RetornaDiaMesAnterior(dataReferencia, 3);

                if (!Calendario.IsDiaUtil(dataInicio))
                {
                    dataInicio = Calendario.SubtraiDiaUtil(dataInicio, 1);
                }

                dataFim = dataReferencia;
            }
            else if (tipoPeriodoDatas == TipoPeriodoDatas.Meses6)
            {
                dataInicio = Calendario.RetornaDiaMesAnterior(dataReferencia, 6);

                if (!Calendario.IsDiaUtil(dataInicio))
                {
                    dataInicio = Calendario.SubtraiDiaUtil(dataInicio, 1);
                }

                dataFim = dataReferencia;
            }
            else if (tipoPeriodoDatas == TipoPeriodoDatas.Meses12)
            {
                dataInicio = Calendario.RetornaDiaMesAnterior(dataReferencia, 12);

                if (!Calendario.IsDiaUtil(dataInicio))
                {
                    dataInicio = Calendario.SubtraiDiaUtil(dataInicio, 1);
                }

                dataFim = dataReferencia;
            }
            else if (tipoPeriodoDatas == TipoPeriodoDatas.Meses24)
            {
                dataInicio = Calendario.RetornaDiaMesAnterior(dataReferencia, 24);

                if (!Calendario.IsDiaUtil(dataInicio))
                {
                    dataInicio = Calendario.SubtraiDiaUtil(dataInicio, 1);
                }

                dataFim = dataReferencia;
            }


            if (idCliente.HasValue)
            {
                if (dataInicio < dataImplantacao)
                {
                    dataInicio = dataImplantacao;
                }

                if (dataFim < dataInicio)
                {
                    dataFim = dataInicio;
                }
            }


        }
                

    }
}
