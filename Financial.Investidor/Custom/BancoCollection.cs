/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 1.5.0.0
                       MyGeneration Version # 1.1.5.1
                           4/10/2006 09:34:13
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Investidor
{
	public partial class BancoCollection : esBancoCollection
	{
        public Banco BuscaBancoPorCodigoCompensacao(string codigoCompensacao)
        {
            Banco banco = null;

            this.QueryReset();
            this.Query.Where(this.Query.CodigoCompensacao == codigoCompensacao);
            if (this.Load(this.Query))
            {
                banco = this[0];
            }

            return banco;
        }
	}
}
