﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;
using Financial.Investidor.Exceptions;
using log4net;
using Financial.Bolsa;
using Financial.Util;
using Financial.BMF;

namespace Financial.Investidor {
    public partial class CodigoClienteAgente : esCodigoClienteAgente 
    {
        /// <summary>
        /// Busca o Id do Cliente, checa inicialmente se o código default Bovespa = codigoBovespaAgente,
        ///  se for busca direto em ClienteBolsa.
        /// Caso contrário, vai buscar em CodigoClienteAgente, pegando o codigoBovespa dada o codigoBovespaAgente.
        /// </summary>
        /// <param name="codigoBovespaAgente"></param>
        /// <param name="codigoBovespaCliente"></param>
        /// <returns></returns>
        public bool BuscaIdClienteInternoBovespa(int codigoBovespaAgente, int codigoBovespaCliente)
        {
            //Busca o IdAgenteMercado default
            int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);

            bool retorno = false;
            //Se for código default, busca direto de ClienteBolsa, caso não encontre tenta buscar de CodigoClienteAgente
            if (codigoBovespaDefault == codigoBovespaAgente)
            {
                ClienteBolsaCollection clienteBolsaCollection = new ClienteBolsaCollection();
                clienteBolsaCollection.Query.Select(clienteBolsaCollection.Query.IdCliente);
                clienteBolsaCollection.Query.Where(clienteBolsaCollection.Query.CodigoSinacor.Equal(codigoBovespaCliente) |
                                                   clienteBolsaCollection.Query.CodigoSinacor2.Equal(codigoBovespaCliente));
                clienteBolsaCollection.Query.Load();

                if (clienteBolsaCollection.Count > 0)
                {
                    this.IdCliente = clienteBolsaCollection[0].IdCliente.Value;
                    return true;
                }
            }

            //Busco antes o IdAgente na AgenteMercado, baseado no codigoBovespaAgente
            AgenteMercado agenteMercado = new AgenteMercado();
            int idAgente = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);

            CodigoClienteAgenteCollection codigoClienteAgenteCollection = new CodigoClienteAgenteCollection();
            codigoClienteAgenteCollection.Query.Select(codigoClienteAgenteCollection.Query.IdCliente);
            codigoClienteAgenteCollection.Query.Where(codigoClienteAgenteCollection.Query.IdAgente.Equal(idAgente),
                                                      codigoClienteAgenteCollection.Query.CodigoClienteBovespa.Equal(codigoBovespaCliente));
            codigoClienteAgenteCollection.Query.Load();

            if (codigoClienteAgenteCollection.Count > 0)
            {
                retorno = true;
                this.IdCliente = codigoClienteAgenteCollection[0].IdCliente.Value;
            }            

            return retorno;
        }

        /// <summary>
        /// Busca o Id do Cliente, checa inicialmente se o código default Bovespa = codigoBovespaAgente,
        ///  se for busca direto em ClienteBolsa.
        /// Caso contrário, vai buscar em CodigoClienteAgente, pegando o codigoBMF dada o codigoBMFAgente.
        /// </summary>
        /// <param name="codigoBovespaAgente"></param>
        /// <param name="codigoBovespaCliente"></param>
        /// <returns></returns>
        public bool BuscaIdClienteInternoBMF(int codigoBMFAgente, int codigoBMFCliente)
        {
            //Busca o IdAgenteMercado default
            int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);

            bool retorno = false;
            //Se for código default, busca direto de ClienteBMF
            if (codigoBovespaDefault == codigoBMFAgente)
            {
                ClienteBMFCollection clienteBMFCollection = new ClienteBMFCollection();
                clienteBMFCollection.Query.Select(clienteBMFCollection.Query.IdCliente);
                clienteBMFCollection.Query.Where(clienteBMFCollection.Query.CodigoSinacor.Equal(codigoBMFCliente));
                clienteBMFCollection.Query.Load();

                if (clienteBMFCollection.Count > 0)
                {
                    this.IdCliente = clienteBMFCollection[0].IdCliente.Value;
                    return true;
                }
            }
            
            //Busco antes o IdAgente na AgenteMercado, baseado no codigoBovespaAgente
            AgenteMercado agenteMercado = new AgenteMercado();
            agenteMercado.BuscaIdAgenteMercadoBMF(codigoBMFAgente);
            int idAgente = (int)agenteMercado.IdAgente;

            CodigoClienteAgenteCollection codigoClienteAgenteCollection = new CodigoClienteAgenteCollection();
            codigoClienteAgenteCollection.Query.Select(codigoClienteAgenteCollection.Query.IdCliente);
            codigoClienteAgenteCollection.Query.Where(codigoClienteAgenteCollection.Query.IdAgente.Equal(idAgente),
                                                      codigoClienteAgenteCollection.Query.CodigoClienteBMF.Equal(codigoBMFCliente));
            codigoClienteAgenteCollection.Query.Load();

            if (codigoClienteAgenteCollection.Count > 0)
            {
                retorno = true;
                this.IdCliente = codigoClienteAgenteCollection[0].IdCliente.Value;
            }            

            return retorno;
        }

        /// <summary>
        /// Busca o código cliente Bovespa baseado no IdAgente e IdCliente passados.
        /// </summary>
        /// <param name="idAgente"></param>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public bool BuscaCodigoBovespaCliente(int idAgente, int idCliente)
        {
            CodigoClienteAgenteCollection codigoClienteAgenteCollection = new CodigoClienteAgenteCollection();
            codigoClienteAgenteCollection.Query.Select(codigoClienteAgenteCollection.Query.CodigoClienteBovespa);
            codigoClienteAgenteCollection.Query.Where(codigoClienteAgenteCollection.Query.IdAgente.Equal(idAgente),
                                                      codigoClienteAgenteCollection.Query.IdCliente.Equal(idCliente));
            codigoClienteAgenteCollection.Query.Load();

            if (codigoClienteAgenteCollection.Count > 0)
            {
                this.CodigoClienteBovespa = codigoClienteAgenteCollection[0].CodigoClienteBovespa.Value;
                return true;
            }
            
            return false;
        }
    }
}
