﻿using System;

namespace Financial.Investidor.Exceptions
{
    /// <summary>
    /// Classe base de Exceção do componente de Investidor
    /// </summary>
    public class InvestidorException : Exception
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public InvestidorException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public InvestidorException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public InvestidorException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    /// <summary>
    /// Exceção de ContaDefaultNaoCadastrada
    /// </summary>
    public class ContaDefaultNaoCadastradaException : InvestidorException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ContaDefaultNaoCadastradaException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ContaDefaultNaoCadastradaException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de ExecucaoCalculoDiarioException
    /// </summary>
    public class ExecucaoCalculoDiarioException : InvestidorException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ExecucaoCalculoDiarioException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ExecucaoCalculoDiarioException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de ExecucaoAberturaException
    /// </summary>
    public class ExecucaoAberturaException : InvestidorException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ExecucaoAberturaException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ExecucaoAberturaException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de ExecucaoFechamentoException
    /// </summary>
    public class ExecucaoFechamentoException : InvestidorException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ExecucaoFechamentoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ExecucaoFechamentoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de ExecucaoProcessamentoPeriodoException
    /// </summary>
    public class ExecucaoProcessamentoPeriodoException : InvestidorException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ExecucaoProcessamentoPeriodoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ExecucaoProcessamentoPeriodoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de ExecucaoProcessamentoPeriodoException
    /// </summary>
    public class CodigoYMFNaoCadastradoException : InvestidorException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public CodigoYMFNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public CodigoYMFNaoCadastradoException(string mensagem) : base(mensagem) { }
    }
}


