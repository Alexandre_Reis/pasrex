﻿alter table clienteinterface drop column codigoanbid
go

alter table carteira add CodigoAnbid varchar(10)
go



alter table agentemercado add CodigoAnbid varchar(10) null
go

set identity_insert Estrategia on
go
insert into Estrategia (IdEstrategia, Descricao) values (6, 'Offshore')
insert into Estrategia (IdEstrategia, Descricao) values (7, 'Capital Protegido')
insert into Estrategia (IdEstrategia, Descricao) values (8, 'Fundo Imobiliário')
insert into Estrategia (IdEstrategia, Descricao) values (9, 'FIP')
insert into Estrategia (IdEstrategia, Descricao) values (10, 'LongShort')
insert into Estrategia (IdEstrategia, Descricao) values (11, 'Previdência')
insert into Estrategia (IdEstrategia, Descricao) values (12, 'FIDC')
set identity_insert Estrategia off
go


set identity_insert ListaCategoriaFundo on
go
INSERT INTO [ListaCategoriaFundo] ([IdLista],[Descricao])VALUES(110,'Ambima')
set identity_insert ListaCategoriaFundo off
go
set identity_insert CategoriaFundo on
go
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(31, 'Investimento no Exterior', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(92, 'Off Shore Renda Fixa', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(93, 'Off  Shore Renda Mista', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(94, 'Off Shore Renda Variável', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(197, 'Ações IBOVESPA Ativo', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(199, 'Ações IBrX Ativo', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(200, 'Fundos Fechados de Ações', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(201, 'Ações IBOVESPA Indexado', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(202, 'Ações IBrX Indexado', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(204, 'Ações Livre', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(207, 'Balanceados', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(208, 'Capital Protegido', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(222, 'Previdência Renda Fixa', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(227, 'Referenciado DI', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(229, 'Renda Fixa', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(231, 'Renda Fixa Crédito Livre', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(258, 'Fundos de Investimento Imobiliário', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(272, 'Curto Prazo', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(279, 'Fundos de Índices - ETF', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(320, 'Ações Setoriais', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(321, 'Ações Small Caps', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(322, 'Ações Dividendos', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(323, 'Ações Sustentabilidade/Governança', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(324, 'Fundos de Participações', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(326, 'Exclusivo Fechado', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(327, 'Long And Short - Neutro', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(328, 'Long And Short - Direcional', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(329, 'Multimercados Macro', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(330, 'Multimercados Trading', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(331, 'Multimercados Multiestrategia', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(332, 'Multimercados Multigestor', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(333, 'Multimercados Juros e Moedas', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(334, 'Multimercados Estrategia Especifica', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(335, 'Renda Fixa Índices', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(337, 'Ações FMP - FGTS', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(338, 'Cambial', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(339, 'Previdência Balanceados Até 15', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(340, 'Previdência Balanceados 15 a 30', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(341, 'Previdência Balanceados acima de 30', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(342, 'Previdência Data Alvo', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(343, 'Previdência Multimercados', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(344, 'Previdência Ações', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(345, 'FIDC Fomento Mercantil', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(346, 'FIDC Financeiro', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(347, 'FIDC Agro Indústria e Comércio', 110)
INSERT INTO [CategoriaFundo] ([IdCategoria],[Descricao],[IdLista])VALUES(348, 'FIDC Outros', 110)
set identity_insert CategoriaFundo off
go

set identity_insert SubCategoriaFundo on
go
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(31,31, 'Investimento no Exterior')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(92,92, 'Off Shore Renda Fixa')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(93,93, 'Off  Shore Renda Mista')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(94,94, 'Off Shore Renda Variável')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(197,197, 'Ações IBOVESPA Ativo')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(199,199, 'Ações IBrX Ativo')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(200,200, 'Fundos Fechados de Ações')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(201,201, 'Ações IBOVESPA Indexado')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(202,202, 'Ações IBrX Indexado')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(204,204, 'Ações Livre')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(207,207, 'Balanceados')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(208,208, 'Capital Protegido')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(222,222, 'Previdência Renda Fixa')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(227,227, 'Referenciado DI')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(229,229, 'Renda Fixa')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(231,231, 'Renda Fixa Crédito Livre')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(258,258, 'Fundos de Investimento Imobiliário')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(272,272, 'Curto Prazo')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(279,279, 'Fundos de Índices - ETF')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(320,320, 'Ações Setoriais')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(321,321, 'Ações Small Caps')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(322,322, 'Ações Dividendos')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(323,323, 'Ações Sustentabilidade/Governança')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(324,324, 'Fundos de Participações')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(326,326, 'Exclusivo Fechado')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(327,327, 'Long And Short - Neutro')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(328,328, 'Long And Short - Direcional')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(329,329, 'Multimercados Macro')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(330,330, 'Multimercados Trading')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(331,331, 'Multimercados Multiestrategia')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(332,332, 'Multimercados Multigestor')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(333,333, 'Multimercados Juros e Moedas')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(334,334, 'Multimercados Estrategia Especifica')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(335,335, 'Renda Fixa Índices')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(337,337, 'Ações FMP - FGTS')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(338,338, 'Cambial')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(339,339, 'Previdência Balanceados Até 15')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(340,340, 'Previdência Balanceados 15 a 30')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(341,341, 'Previdência Balanceados acima de 30')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(342,342, 'Previdência Data Alvo')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(343,343, 'Previdência Multimercados')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(344,334, 'Previdência Ações')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(345,345, 'FIDC Fomento Mercantil')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(346,346, 'FIDC Financeiro')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(347,347, 'FIDC Agro Indústria e Comércio')
INSERT INTO [SubCategoriaFundo] ([IdCategoria],[IdSubCategoria],[Descricao])VALUES(348,348, 'FIDC Outros')
set identity_insert SubCategoriaFundo off
go