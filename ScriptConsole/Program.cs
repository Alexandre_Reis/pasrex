﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using EntitySpaces.Interfaces;
using ScriptConsole.Properties;
using System.IO;
using EntitySpaces.Core;
using System.Data.SqlClient;
using log4net;

namespace ScriptConsole {
    class Program {
        
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args) {
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();

            string BDs = Settings.Default.BDs;
            string[] listaBDs = BDs.Split(new Char[] { ',' });
            string server = Settings.Default.Server;
            string login = Settings.Default.Login;
            string senha = Settings.Default.Senha;

            int i = 0;
            foreach (string BD in listaBDs) {
                esConfigSettings.ConnectionInfo.Connections.Clear();

                esConnectionElement connSQL = new esConnectionElement();
                connSQL.Name = "Financial" + i;
                connSQL.ProviderMetadataKey = "esDefault";
                connSQL.SqlAccessType = esSqlAccessType.DynamicSQL;
                connSQL.Provider = "EntitySpaces.SqlClientProvider";
                connSQL.ProviderClass = "DataProvider";
                connSQL.ConnectionString = @"" + "Data Source=" + server + ";Initial Catalog=" + BD + ";User ID=" + login + ";Password=" + senha + ";" + "";
                connSQL.DatabaseVersion = "2005";
                esConfigSettings.ConnectionInfo.Connections.Add(connSQL);

                esConfigSettings.ConnectionInfo.Default = "Financial" + i;
                i++;

                ProcessaScript();
            }

        }

        private static void ProcessaScript() {
            bool indicaErro = false;
            string mensagemErro = "";

            esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();

            string arquivo = Settings.Default.ScriptFile;            
            //
            string conteudoSQL = File.ReadAllText(arquivo, Encoding.Default);

            // Cada nova Linha com o commando go é um novo comando
            string[] commands = conteudoSQL.Split(new string[] { "\ngo" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string command in commands) {

                esUtility u = new esUtility();
                try {
                    u.ExecuteNonQuery(esQueryType.Text, command, "");
                }
                catch (SqlException e) {
                    indicaErro = true;
                    string commandAux = "\n" + command.Replace("\r\n\r", "");

                    mensagemErro += commandAux + " não executado\n";
                    Console.WriteLine(e.LineNumber);
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                    Console.WriteLine(e.ToString());
                    Console.WriteLine();
                }
            }

            if (indicaErro) {
                if (log.IsDebugEnabled) {
                    log.Debug(mensagemErro);
                }               
            }
        }
    }
}