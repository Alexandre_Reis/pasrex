﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Investidor;
using EntitySpaces.Core;
using System;
using Financial.Util;
using EntitySpaces.Interfaces;
using Financial.Investidor.Controller;
using System.Collections.Generic;

namespace Test.Fundo
{
    [TestClass]
    public class AgendaComeCotasTest
    {
        esTransactionScope scope;

        /// <summary>
        ///Initialize() is called once during test execution before
        ///test methods in this test class are executed.
        ///</summary>
        [TestInitialize()]
        public void Initialize()
        {
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();

            scope = new esTransactionScope();
        }

        [TestMethod]
        public void FatorQuantidadeCotistaTest()
        {

            Cliente cliente = new Cliente();
            PosicaoCotista posicaoCotista = new PosicaoCotista();

            posicaoCotista.LoadByPrimaryKey(104930);

            cliente.LoadByPrimaryKey(posicaoCotista.IdCarteira.Value);

            decimal fatorQuantidadeCotista = Utilitario.Truncate(AgendaComeCotas.fatorQuantidadeCotista(posicaoCotista, cliente.DataImplantacao.Value), 8);

            Assert.AreEqual(fatorQuantidadeCotista, new Decimal(1.00851915));

        }

        [TestMethod]
        public void agendaComeCotasCotistaTest()
        {
            int idPosicao = 104930;
            Cliente cliente = new Cliente();
            PosicaoCotista posicaoCotista = new PosicaoCotista();

            posicaoCotista.LoadByPrimaryKey(idPosicao);

            cliente.LoadByPrimaryKey(posicaoCotista.IdCarteira.Value);

            AgendaComeCotas agendaComeCotas = AgendaComeCotas.agendaComeCotasCotista(posicaoCotista, cliente.DataImplantacao.Value);

            int idCarteira = 5058;
            decimal quantidadeAntesCortes = new Decimal(10085.19158644);

            Assert.AreEqual(agendaComeCotas.IdCarteira, idCarteira);
            Assert.AreEqual(agendaComeCotas.IdPosicao, idPosicao);
            Assert.AreEqual(Utilitario.Truncate(agendaComeCotas.QuantidadeAntesCortes.Value, 8), Utilitario.Truncate(quantidadeAntesCortes, 8));

        }

        [TestMethod]
        public void calculoDiarioTest()
        {
            int idCliente = 51380010;
            DateTime data = new DateTime(2015, 10, 14);
            ParametrosProcessamento parametros = new ParametrosProcessamento();

            parametros.CravaCota = false;
            parametros.IgnoraOperacoes = false;
            parametros.IntegraBMF = false;
            parametros.IntegraBolsa = false;
            parametros.IntegraBTC = false;
            parametros.IntegracaoRendaFixa = 0;
            parametros.IntegraCC = false;
            parametros.MantemFuturo = false;
            parametros.ProcessaBMF = true;
            parametros.ProcessaBolsa = true;
            parametros.ProcessaCambio = true;
            parametros.ProcessaFundo = true;
            parametros.ProcessaRendaFixa = true;
            parametros.ProcessaSwap = true;
            parametros.RemuneraRF = true;
            parametros.ResetCusto = false;
            parametros.ListaTabelaInterfaceCliente = new List<int>();

            ControllerInvestidor controller = new ControllerInvestidor();

            controller.ExecutaFechamento(idCliente, data, parametros);

            HistoricoCota historicoCota = new HistoricoCota();

            historicoCota.LoadByPrimaryKey(data, idCliente);

            decimal cotaAbertura = new Decimal(2494.384745410000);
            decimal cotaFechamento = new Decimal(2488.445335790000);
            decimal plAbertura = new Decimal(5673819.46);
            decimal plFechamento = new Decimal(5660309.46);
            decimal quantidadeFechamento = new Decimal(2274.636850000000);


            Assert.AreEqual(Utilitario.Truncate(historicoCota.CotaAbertura.Value, 8), Utilitario.Truncate(cotaAbertura, 8));
            Assert.AreEqual(Utilitario.Truncate(historicoCota.CotaFechamento.Value, 8), Utilitario.Truncate(cotaFechamento, 8));

            Assert.AreEqual(Utilitario.Truncate(historicoCota.PLAbertura.Value, 2), Utilitario.Truncate(plAbertura, 2));
            Assert.AreEqual(Utilitario.Truncate(historicoCota.PLFechamento.Value, 2), Utilitario.Truncate(plFechamento, 2));

            Assert.AreEqual(Utilitario.Truncate(historicoCota.QuantidadeFechamento.Value, 8), Utilitario.Truncate(quantidadeFechamento, 8));

        }


    }
}
