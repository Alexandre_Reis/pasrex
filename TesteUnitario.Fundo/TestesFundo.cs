﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using Financial.Fundo;
using TesteUnitario.Util;

namespace TesteUnitario.Fundo
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class TestesFundo
    {
        // Cria o contexto de teste para ser utilizado na recuperação dos cenários de testes em excel
        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        // Instancía as bibliotecas que serão utilizadas no teste
        TesteUnitario.Util.Util util = new TesteUnitario.Util.Util();
        Financial.Fundo.CalculoMedida calculomedida = new Financial.Fundo.CalculoMedida();

        public TestesFundo()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [DataSource("System.Data.OleDb", "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=..\\..\\..\\TesteUnitario.Fundo\\volatilidade.xls;Extended Properties='Excel 12.0 Xml;HDR=YES;IMEX=0'", "Cenários$", DataAccessMethod.Sequential), TestMethod()]
        public void TesteCalculoVolatilidade()
        {
            // Recupera os dados do cenário da linha atual da planilha para se realizar o teste
            string dataInicio = DateTime.Parse(testContextInstance.DataRow["Data Inicio"].ToString()).ToString("yyyyMMdd");
            string dataFim = DateTime.Parse(testContextInstance.DataRow["Data Fim"].ToString()).ToString("yyyyMMdd");
            decimal volatilidadeEsperada = Convert.ToDecimal(testContextInstance.DataRow["Volatilidade"]);

            // Recupera o Range de dados auxiliares que serão passados no metodo a ser testado conforme os dados do cenário atual
            List<decimal> listaValores = util.RetornaListPlanilha<decimal>("..\\..\\..\\TesteUnitario.Fundo\\volatilidade.xls", "Retornos$", "RetornoDiário", "Format(Data,'yyyyMMdd') between '" + dataInicio + "' and '" + dataFim + "'", true);

            // Chama o método que vai retornar o valor que está sendo testado para a lista de valores recuperadas conforme o cenário atual
            decimal volatilidadeTeste = calculomedida.CalculaVolatilidade(listaValores);

            // Atualiza na planilha o resultado do teste para o cenário atual
            util.AtualizaValorPlanilha(testContextInstance, "Cenários$", "Resultado", volatilidadeTeste.ToString().Replace(",", "."), "Cenário = " + testContextInstance.DataRow["Cenário"], true);

            // Realiza a verificação do teste pelo sistema
            Assert.AreEqual(Convert.ToDouble(volatilidadeEsperada), Convert.ToDouble(volatilidadeTeste), 0.00001, "Volatilidade com valor errado.");
        }
    }
}
