﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Fundo.Exceptions
{
    /// <summary>
    /// Classe base de Exceção da integracao com galgo
    /// </summary>
    /// 
    public class GalgoException : Exception
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public GalgoException()
        {
        }

        public GalgoException(string message)
            : base(message)
        {
        }

        public GalgoException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
