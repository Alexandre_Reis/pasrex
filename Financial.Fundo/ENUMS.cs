﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.Util;
using Financial.Fundo.Properties;
using System.Reflection;

namespace Financial.Fundo.Enums {

    public enum Rendimento
    {
        CotaPatrimonial = 1,
        CotaEx = 2,
        Rendimentos = 3
    }

    public enum StatusAtivoCarteira
    {
        Ativo = 1,
        Inativo = 2
    }

    public enum StatusProcessamentoCisao
    {
        Aguardando = 0,
        Processar = 1,
        Processado = 2
    }

    public enum TratamentoCotaInexistente
    {
        EnviaException = 1, //Se não encontrar cota, envia exception, podendo ser capturada em escopos superiores
        BuscaCotaPosterior = 2, //Se não entrar cota, busca na HistoricoCota a cota posterior mais próxima
        BuscaCotaAnterior = 3 //Se não entrar cota, busca na HistoricoCota a cota anterior mais próxima
    }

    public enum TipoFDIC
    {
        Aberto = 1,
        Fechado = 2
    }

    public enum CalculoCotaBruta
    {
        EmbuteAdm = 1,
        NaoEmbuteAdm = 2
    }

    public enum HardSoft
    {
        Hard = 1,
        Soft = 2
    }

    /// <summary>
    /// Todos os valores do Enum devem estar cadastrados no Resource do Componente
    /// </summary>
    public enum TipoOperacaoFundo {
        [StringValue("Aplicacao")]
        Aplicacao = 1,

        [StringValue("ResgateBruto")]
        ResgateBruto = 2,

        [StringValue("ResgateLiquido")]
        ResgateLiquido = 3,

        [StringValue("ResgateCotas")]
        ResgateCotas = 4,

        [StringValue("ResgateTotal")]
        ResgateTotal = 5,

        [StringValue("AplicacaoCotasEspecial")]
        AplicacaoCotasEspecial = 10,

        [StringValue("AplicacaoAcoesEspecial")]
        AplicacaoAcoesEspecial = 11,

        [StringValue("ResgateCotasEspecial")]
        ResgateCotasEspecial = 12,

        [StringValue("ComeCotas")]
        ComeCotas = 20,

        [StringValue("AjustePosicao")]
        AjustePosicao = 40,

        [StringValue("Amortizacao")]
        Amortizacao = 80,

        [StringValue("Juros")]
        Juros = 82,

        [StringValue("AmortizacaoJuros")]
        AmortizacaoJuros = 84,

        [StringValue("IncorporacaoResgate")]
        IncorporacaoResgate = 100,

        [StringValue("IncorporacaoAplicacao")]
        IncorporacaoAplicacao = 101,

        [StringValue("Ingresso em Ativos com Impacto na Quantidade")]
        IngressoAtivoImpactoQtde = 104,

        [StringValue("Ingresso em Ativos com Impacto na Cota")]
        IngressoAtivoImpactoCota = 105,

        [StringValue("Retirada em Ativos com Impacto na Quantidade")]
        RetiradaAtivoImpactoQtde = 106,

        [StringValue("Retirada em Ativos com Impacto na Cota")]
        RetiradaAtivoImpactoCota = 107,

        [StringValue("Aplicação Cotas")]
        AplicacaoCotas = 110,

        [StringValue("Dividendo")]
        Dividendo = 112
    }

    public enum TipoResgateFundo
    {
        Especifico = 1, //Direcionado a uma nota de aplicação específica
        FIFO = 2, //First In First Out (da mais antiga para a mais recente)
        LIFO = 3, //Last In First Out (da mais recente para a mais antiga)
        MenorImposto = 4, //Busca antes as que projetam menos tributos (IR + IOF)
        Colagem = 5 //Busca as posições coladas, depois mescla as posições do parametro original (antes de ser colado)
    }

    public enum FonteOperacaoFundo
    {
        Interno = 1,
        Manual = 2,        
        YMFComCalculo = 5,
        YMFSemCalculo = 6,
        SMA = 10,
        Ajustado = 50,
        OrdemFundo = 100
    }

    public enum TipoCarteiraFundo
    {
        [StringValue("TipoCarteiraFundo.RendaFixa")]
        RendaFixa = 1,

        [StringValue("TipoCarteiraFundo.RendaVariavel")]
        RendaVariavel = 2
    }

    public enum TipoCotaFundo {
        Abertura = 1,
        Fechamento = 2
    }

    public enum TipoCustoFundo {
        Aplicacao = 1,
        MedioAplicado = 2
    }

    public enum TipoTributacaoFundo {

        [StringValue("Curto Prazo")]
        CurtoPrazo = 1,

        [StringValue("Longo Prazo")]
        LongoPrazo = 2,

        [StringValue("Ações")]
        Acoes = 3,

        CPrazo_SemComeCotas = 4,
        
        LPrazo_SemComeCotas = 5,

        [StringValue("Isento")]
        Isento = 20,

        [StringValue("Alíquota Específica")]
        AliquotaEspecifica = 21

    }

    public enum TipoRentabilidadeFundo
    {
        PrimeiroPrimeiro = 1,
        FinalFinal = 2
    }

    public enum ContagemDiasLiquidacaoResgate {        
        DiasUteis = 1,
        DiasCorridos = 2
    }

    public enum ContagemDiasPrazoIOF
    {
        Mellon = 1,
        PrazoCorrido = 2,
        Direta = 3,
        Personalizado = 4,
        Santander = 5
    }

    public enum PrioridadeOperacaoFundo
    {
        ResgateAntes = 1,
        ResgateDepois = 2
    }

    public enum DistribuicaoDividendosCarteira
    {
        [StringValue("Não distribui")]
        NaoDistribui = 1,

        [StringValue("Distribui Cotas - Caixa")]
        DistribuiCotasCaixa = 2,

        [StringValue("Distribui Financeiro - Caixa")]
        DistribuiFinanceiroCaixa = 3,

        [StringValue("Distribui Cotas - Competência")]
        DistribuiCotasCompetencia = 4,

        [StringValue("Distribui Financeiro - Competência")]
        DistribuiFinanceiroCompetencia = 5
    }

    public static class DistribuicaoDividendosCarteiraDescricao
    {
        public static string RetornaDescricao(int distribuicaoDividendosCarteira)
        {
            return Enum.GetName(typeof(DistribuicaoDividendosCarteira), distribuicaoDividendosCarteira);
        }

        public static string RetornaStringValue(int distribuicaoDividendosCarteira)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(DistribuicaoDividendosCarteira), RetornaDescricao(distribuicaoDividendosCarteira)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    #region Provisões, Taxa Administração e Performance
    public enum TipoCalculoProvisao {
        [StringValue("Provisão")]
        Provisao = 1,

        [StringValue("Diferido")]
        Diferido = 2
    }

    public enum TipoCadastroProvisao {
        Livre = 1, //Livre para ser cadastrada qualquer provisão ou diferido
        TaxaFiscalizacaoCVM = 2 //Diferimento de Taxa de Fiscalização CVM
    }

    public enum ContagemDiasProvisao {
        [StringValue("Úteis")]
        Uteis = 1,

        [StringValue("Corridos")]
        Corridos = 2
    }

    public enum TipoCadastroAdministracao
    {
        TaxaAdministracao = 1,
        TaxaGestao = 2
    }

    public enum TipoCalculoAdministracao {
        [StringValue("Percentual PL")]
        PercentualPL = 1,

        [StringValue("Valor Fixo")]
        ValorFixo = 2,

        [StringValue("%PL ou Valor Fixo")]
        PercentualPLOuValorFixo = 3
    }

    public enum TipoApropriacaoAdministracao {
        [StringValue("Exponencial")]
        Exponencial = 1,

        [StringValue("Linear")]
        Linear = 2
    }

    public enum BaseAnoAdministracao
    {
        Base252 = 252,
        Base360 = 360,
        Base365 = 365,
        BaseAnoUteisExato = 1
    }

    public enum BaseApuracaoAdministracao {
        [StringValue("PL Dia Anterior")]
        PL_DiaAnterior = 1,

        [StringValue("Pr. Bruto Dia")]
        PBruto_Dia = 2,        

        [StringValue("Pr. Bruto Dia Anterior")]
        PBruto_DiaAnterior = 3        
    }

    public enum TipoLimitePLAdministracao {
        TotalPL = 1,
        LimiteSuperior = 2,
        LimiteInferior = 3
    }
    
    public enum TipoCalculoPerformance {
        [StringValue("PorCertificado")]
        PorCertificado = 1, //Joga na carteira o total dos certificados

        [StringValue("CotaCota")]
        CotaCota = 2, //Joga no cotista o proporcional da carteira (calculado pelo acumulado diariamente)

        [StringValue("CotaCotaDiario")]
        CotaCotaDiario = 3 //Joga no cotista o proporcional da carteira (calculado pelo diário, que vai se acumulando)
    }

    public enum BaseApuracaoPerformance
    {
        [StringValue("CotaLiquida_DiaAnterior")]
        CotaLiquida_DiaAnterior = 1,

        [StringValue("CotaBruta_Dia")]
        CotaBruta_Dia = 2,        
    }

    public enum CalculaBenchmarkNegativo
    {
        NaoCalculaPerformance = 1,
        CalculaPartePositiva = 2,
        CalculaGanhoTotal = 3,
        CalculaGanhoTotalComRecalculo = 4
    }

    public enum TipoCalculoResgatePerformance
    {
        Calculado = 1,
        ProporcionalCarteira = 2
    }
    #endregion

    #region Tipos para Taxa de Fiscalização CVM
    public enum TipoTaxaFiscalizacaoCVM
    {
        CompanhiaAberta = 1,
        SociedadeBeneficiariaIncentivoFiscal = 2,
        InstituicaoFinanceira = 3,
        InvestidorEstrangeiro = 4,
        FundoInvestimento = 5,
        FundoAplicacaoCotas = 6
    }   
    #endregion

    public static class TraducaoEnumsFundo {

        #region Funções privates para fazer Pesquisa dentro dos Enums
        /// <summary>
        /// Dado um codigo retorna um Enum do tipo TipoOperacaoFundo
        /// </summary>
        /// <exception>throws ArgumentException se não existir um enum com o valor do código</exception> 
        /// <returns></returns>
        private static TipoOperacaoFundo SearchEnumTipoOperacaoFundo(int codigo) {
            int[] tipoOperacaoFundoValues = (int[])Enum.GetValues(typeof(TipoOperacaoFundo));

            int? posicao = null;
            for (int i = 0; i < tipoOperacaoFundoValues.Length; i++) {
                if (tipoOperacaoFundoValues[i] == codigo) {
                    posicao = i;
                    break;
                }
            }

            if (posicao.HasValue) {
                string tipoOperacaoFundoString = Enum.GetNames(typeof(TipoOperacaoFundo))[posicao.Value];
                // Monta o Enum de acordo com a string
                TipoOperacaoFundo tipoOperacaoFundo = (TipoOperacaoFundo)Enum.Parse(typeof(TipoOperacaoFundo), tipoOperacaoFundoString);
                return tipoOperacaoFundo;
            }
            else {
                throw new ArgumentException("TipoOperacaoFundo não possue Constante com valor " + codigo);
            }
        }

        /// <summary>
        /// Dado um codigo retorna um Enum do tipo TipoCarteiraFundo
        /// </summary>
        /// <exception>throws ArgumentException se não existir um enum com o valor do código</exception> 
        /// <returns></returns>
        private static TipoCarteiraFundo SearchEnumTipoCarteiraFundo(int codigo) {
            int[] tipoCarteiraFundoValues = (int[])Enum.GetValues(typeof(TipoCarteiraFundo));

            int? posicao = null;
            for (int i = 0; i < tipoCarteiraFundoValues.Length; i++) {
                if (tipoCarteiraFundoValues[i] == codigo) {
                    posicao = i;
                    break;
                }
            }

            if (posicao.HasValue) {
                string tipoCarteiraFundoString = Enum.GetNames(typeof(TipoCarteiraFundo))[posicao.Value];
                // Monta o Enum de acordo com a string
                TipoCarteiraFundo tipoCarteiraFundo = (TipoCarteiraFundo)Enum.Parse(typeof(TipoCarteiraFundo), tipoCarteiraFundoString);
                return tipoCarteiraFundo;
            }
            else {
                throw new ArgumentException("TipoCarteiraFundo não possue Constante com valor " + codigo);
            }
        }

        #endregion

        public static class EnumTipoOperacaoFundo {
            /// <summary>
            /// Realiza a tradução do Enum TipoOperacaoFundo
            /// </summary>
            /// <param name="codigo"></param>
            /// <returns></returns>
            public static string TraduzEnum(int codigo) {
                TipoOperacaoFundo t = TraducaoEnumsFundo.SearchEnumTipoOperacaoFundo(codigo);
                string chave = "#" + StringEnum.GetStringValue(t);
                return Resources.ResourceManager.GetString(chave);
            }
        }

        public static class EnumTipoCarteiraFundo {
            /// <summary>
            /// Realiza a tradução do Enum TipoCarteiraFundo
            /// </summary>
            /// <param name="codigo"></param>
            /// <returns></returns>
            public static string TraduzEnum(int codigo) {
                TipoCarteiraFundo t = TraducaoEnumsFundo.SearchEnumTipoCarteiraFundo(codigo);
                string chave = "#" + StringEnum.GetStringValue(t);
                return Resources.ResourceManager.GetString(chave);
            }
        }
    }

    public enum ListaTipoAtivo
    {
        [StringValue("Bolsa")]
        Bolsa = 1,

        [StringValue("Renda Fixa")]
        RendaFixa = 2,

        [StringValue("Fundos")]
        Fundos = 3,

        [StringValue("Caixa")]
        Caixa = 4,

        [StringValue("Liquidação")]
        Liquidacao = 5,

        [StringValue("BMF")]
        BMF = 6,

        [StringValue("Swap")]
        Swap = 7
    }

    public static class ListaTipoAtivoDescricao
    {
        public static string RetornaDescricao(int idListaTipoAtivo)
        {
            return Enum.GetName(typeof(ListaTipoAtivo), idListaTipoAtivo);
        }

        public static string RetornaStringValue(int idListaTipoAtivo)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(ListaTipoAtivo), RetornaDescricao(idListaTipoAtivo)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }


    public enum StatusOrdemFundo
    {
        Digitado = 1,
        Aprovado = 2,
        Processado = 3,
        Cancelado = 4
    }

    public enum TipoEventoFundo
    {
        [StringValue("Juros")]
        Juros = 1,

        [StringValue("Amortização")]
        Amortizacao = 2,

        [StringValue("Amortização Juros")]
        AmortizacaoJuros = 3,

        [StringValue("Dividendo")]
        Dividendo = 4,

        [StringValue("Come cotas")]
        ComeCotas = 20
    }

    public static class TipoEventoFundoDescricao
    {
        public static string RetornaDescricao(int idTipoEventoFundo)
        {
            return Enum.GetName(typeof(TipoEventoFundo), idTipoEventoFundo);
        }

        public static string RetornaStringValue(int idTipoEventoFundo)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoEventoFundo), RetornaDescricao(idTipoEventoFundo)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoTabelaCargaPassivo
    {
        PosicaoData = 1
    }

    public enum TipoRetornoBenchmark //só se aplica a benchmark percentual
    {
        Deslocado = 1,
        NaoDeslocado = 2
    }

    public enum ListaCategoriaFundoFixo
    {
        CBLC = 100,
        Padrao = 10000
    }

    public enum ListaCategoriasFundosCBLC
    {
        CurtoPrazo = 1000,
        RendaFixa = 1001,
        Referenciado = 1002
    }

    public enum TipoGrupoAdm
    {
        [StringValue("Ações")]
        Acoes = 1,

        [StringValue("Ações Ativo")]
        Acoes_Ativo = 2,

        [StringValue("Opções")]
        Opcoes = 10,

        [StringValue("Opções Ativo")]
        Opcoes_Ativo = 11,

        [StringValue("Renda Fixa")]
        RendaFixa = 50,

        [StringValue("Renda Fixa Papel")]
        RendaFixa_Papel = 51,

        [StringValue("Renda Fixa Titulo")]
        RendaFixa_Titulo = 52,

        [StringValue("Fundos")]
        Fundos = 80,

        [StringValue("Fundos Ativo")]
        Fundos_Ativo = 81
    }

    public enum TipoGrupoExcecaoAdm
    {
        [StringValue("Exceto Ações")]
        Acoes = 1,

        [StringValue("Exceto Ações Ativo")]
        Acoes_Ativo = 2,

        [StringValue("Exceto Opções")]
        Opcoes = 10,

        [StringValue("Exceto Opções Ativo")]
        Opcoes_Ativo = 11,

        [StringValue("Exceto Renda Fixa")]
        RendaFixa = 50,

        [StringValue("Exceto Renda Fixa Papel")]
        RendaFixa_Papel = 51,

        [StringValue("Exceto Renda Fixa Titulo")]
        RendaFixa_Titulo = 52,

        [StringValue("Exceto Fundos")]
        Fundos = 80,

        [StringValue("Exceto Fundos Ativo")]
        Fundos_Ativo = 81,

        [StringValue("Exceto Fundos do Gestor")]
        Fundos_Gestor = 82,
    }

    public enum CalculoRetornoCarteira
    {
        Cota = 1,
        TIR = 2
    }

    public enum TipoTravamentoCota
    {
        SemTrava = 1,
        CravaCota = 2,
        Shadow = 3,
        TravarReprocessamento = 4
    }

    public enum TipoValorProvento
    {
        ValorporCota = 1,
        TaxaCotaEx = 2,
        TaxaFixa = 3
    }

    public enum EventoFundoNaoFinanceiro
    {
         Incorporacao = 1,
         Fusao = 2,
         Cisao = 3
    }

    public enum TipoAtivoAuxiliar
    {
        [StringValue("Bolsa")]
        OperacaoBolsa = 1,

        [StringValue("Renda Fixa")]
        OperacaoRendaFixa = 20,

        [StringValue("Operações Fundos - Tributado")]
        OperacaoFundos = 30,

        [StringValue("Operações Fundos - Incentivado")]
        OperacaoFundosIncentivado = 31,

        [StringValue("Conta Corrente")]
        ContaCorrente = 40,

        [StringValue("BMF")]
        OperacaoBMF = 50,

        [StringValue("Swap")]
        OperacaoSwap = 60,

        [StringValue("Op.Renda Fixa - Tributado")]
        RendaFixaTributado = 70,

        [StringValue("Rent.Media - Tributado")]
        RentAtivoTributados = 80,

        [StringValue("Op.Renda Fixa - Incentivado")]
        RendaFixaIncentivado = 90,

        [StringValue("Rent.Media - Incentivado")]
        RentAtivosIncentivados = 100,

        [StringValue("Rent.Bruta Total")]
        RentBrutaTotal = 110,

        [StringValue("Fundo")]
        Fundo = 200,

        [StringValue("Rentabilidade do Período")]
        RentabildiadePeriodo = 300
    }

    public static class TipoAtivoAuxiliarDescricao
    {
        public static string RetornaDescricao(int idTipoAtivoAuxiliar)
        {
            return Enum.GetName(typeof(TipoAtivoAuxiliar), idTipoAtivoAuxiliar);
        }

        public static string RetornaStringValue(int idTipoAtivoAuxiliar)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoAtivoAuxiliar), RetornaDescricao(idTipoAtivoAuxiliar)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    //PAS 303 - TIPO DE FUNDO(ABERTO OU FECHADO)
    public enum TipoFundo
    {
        [StringValue("Aberto")]
        Aberto = 1,

        [StringValue("Fechado")]
        Fechado = 2,
    }
    public static class TipoFundoDescricao
    {
        public static string RetornaDescricao(int idTipoFundo)
        {
            return Enum.GetName(typeof(TipoFundo), idTipoFundo);
        }

        public static string RetornaStringValue(int idTipoFundo)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoFundo), RetornaDescricao(idTipoFundo)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoParametroLiquidacao
    {
        [StringValue("Data Operação")]
        DataOperacao = 1,

        [StringValue("Data Conversão")]
        DataConversao = 2,

        [StringValue("Data Liquidação")]
        DataLiquidacao = 3
    }
    public static class TipoParametroLiqDescricao
    {
        public static string RetornaDescricao(int idTipoParametroLiquidacao)
        {
            return Enum.GetName(typeof(TipoParametroLiquidacao), idTipoParametroLiquidacao);
        }

        public static string RetornaStringValue(int idTipoParametroLiquidacao)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoParametroLiquidacao), RetornaDescricao(idTipoParametroLiquidacao)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoParametroLiqOperacao
    {
        [StringValue("Aplicação")]
        Aplicacao = 1,

        [StringValue("Resgate")]
        Resgate = 2
    }
    public static class TipoParametroLiqOperacaoDescricao
    {
        public static string RetornaDescricao(int idTipoParametroLiqOperacao)
        {
            return Enum.GetName(typeof(TipoParametroLiqOperacao), idTipoParametroLiqOperacao);
        }

        public static string RetornaStringValue(int idTipoParametroLiqOperacao)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoParametroLiqOperacao), RetornaDescricao(idTipoParametroLiqOperacao)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoExecucaoRecolhimento
    {
        [StringValue("Não Recolher")]
        NaoRecolher = 1,

        [StringValue("Recolher no momento do evento/transformação/desenquadramento")]
        Recolher = 2,

        [StringValue("Agendar o Recolhimento para uma data pré-definida")]
        Agendar = 3
    }
    public static class TipoExecucaoRecolhimentoDescricao
    {
        public static string RetornaDescricao(int idExecucaoRecolhimento)
        {
            return Enum.GetName(typeof(TipoExecucaoRecolhimento), idExecucaoRecolhimento);
        }

        public static string RetornaStringValue(int idExecucaoRecolhimento)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoExecucaoRecolhimento), RetornaDescricao(idExecucaoRecolhimento)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoAliquotaIR
    {
        [StringValue("Utilizar alíquota IR padrão do Come-Cota")]
        PadraoComeCota = 1,

        [StringValue("Utilizar alíquota IR vigente de cada cautela")]
        VigenteCautela = 2
    }
    public static class TipoAliquotaIRDescricao
    {
        public static string RetornaDescricao(int idAliquotaIR)
        {
            return Enum.GetName(typeof(TipoAliquotaIR), idAliquotaIR);
        }

        public static string RetornaStringValue(int idAliquotaIR)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoAliquotaIR), RetornaDescricao(idAliquotaIR)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    //PAS 304 - Desenquadramento Tributario
    public enum ClassificacaoTributaria
    {
        [StringValue("Curto Prazo")]
        CurtoPrazo = 1,

        [StringValue("Longo Prazo")]
        LongoPrazo = 2,

        [StringValue("Renda Variável")]
        RendaVariavels = 3
    }

    public static class ClassificacaoTributariaDescricao
    {
        public static string RetornaDescricao(int idClassificacaoTributaria)
        {
            return Enum.GetName(typeof(ClassificacaoTributaria), idClassificacaoTributaria);
        }

        public static string RetornaStringValue(int idClassificacaoTributaria)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(ClassificacaoTributaria), RetornaDescricao(idClassificacaoTributaria)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoOcorrencia
    {
        [StringValue("Apenas Registro Histórico de Desenquadramento do Fundo")]
        RegistroHistorico = 1,

        [StringValue("Desenquadramento ou Mudança Oficial na Classificação Tributária do Fundo")]
        MudancaOficial = 2,
    }

    public static class TipoOcorrenciaDescricao
    {
        public static string RetornaDescricao(int idTipoOcorrencia)
        {
            return Enum.GetName(typeof(TipoOcorrencia), idTipoOcorrencia);
        }

        public static string RetornaStringValue(int idTipoOcorrencia)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoOcorrencia), RetornaDescricao(idTipoOcorrencia)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    //PAS 305 - Consulta Fundo de Investimento Recolhimento IR
    public enum EventoTransformacaoMudancaClassificacao
    {
        [StringValue("Cisão")]
        Cisao = 1,

        [StringValue("Fusão")]
        Fusão = 2,

        [StringValue("Incorporação")]
        Incorporacao = 3,

        [StringValue("Aberto-Fechado")]
        AbertoFechado = 4,
        
        [StringValue("Fechado-Aberto")]
        FechadoAberto = 5,

        [StringValue("Mudança Classificação Tributária")]
        Mudanca = 6
    }

    public static class EventoTransformacaoMudancaClassificacaoDescricao
    {
        public static string RetornaDescricao(int idEventoTransformacaoMudancaClassificacao)
        {
            return Enum.GetName(typeof(EventoTransformacaoMudancaClassificacao), idEventoTransformacaoMudancaClassificacao);
        }

        public static string RetornaStringValue(int idEventoTransformacaoMudancaClassificacao)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(EventoTransformacaoMudancaClassificacao), RetornaDescricao(idEventoTransformacaoMudancaClassificacao)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoAberturaIndexada
    {
        [StringValue("Não Executa")]
        NaoExecuta = 0,

        [StringValue("Opcional")]
        Opcional = 1,

        [StringValue("Mandatório")]
        Mandatorio = 2
    }

    public static class TipoAberturaIndexadaDescricao
    {
        public static string RetornaDescricao(int tipoAberturaIndexada)
        {
            return Enum.GetName(typeof(TipoAberturaIndexada), tipoAberturaIndexada);
        }

        public static string RetornaStringValue(int tipoAberturaIndexada)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoAberturaIndexada), RetornaDescricao(tipoAberturaIndexada)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoMediaMovelFixo
    {
        PatrimLiquido12meses = 0,
        PatrBruto40RV = 1,
        PatrLiqFundo_90_DU = 2,
        PatrLiqBMF_90_DU = 3,
        PatrLiqBOLSA_90_DU = 4,
        PatrLiqSWAP_90_DU = 5
    }

    public static class TipoVisualizacaoResgCotista
    {
        public const string Consolidado = "C";
        public const string Analitico = "A";
    }

    public enum PeriodicidadeCotaFundo
    {

        [StringValue("Diário")]
        Diario = 1,

        [StringValue("Semanal")]
        Semanal = 2,

        [StringValue("Mensal")]
        Mensal = 3,

        [StringValue("Bimestral")]
        Bimestral = 4,

        [StringValue("Trimestral")]
        Trimestral = 5,

        [StringValue("Quadrimestral")]
        Quadrimestal = 6,

        [StringValue("Semestral")]
        Semestral = 7,

        [StringValue("Anual")]
        Anual = 8
    }
}