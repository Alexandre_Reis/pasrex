/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/09/2015 11:47:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esColagemComeCotasDetalheCollection : esEntityCollection
	{
		public esColagemComeCotasDetalheCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ColagemComeCotasDetalheCollection";
		}

		#region Query Logic
		protected void InitQuery(esColagemComeCotasDetalheQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esColagemComeCotasDetalheQuery);
		}
		#endregion
		
		virtual public ColagemComeCotasDetalhe DetachEntity(ColagemComeCotasDetalhe entity)
		{
			return base.DetachEntity(entity) as ColagemComeCotasDetalhe;
		}
		
		virtual public ColagemComeCotasDetalhe AttachEntity(ColagemComeCotasDetalhe entity)
		{
			return base.AttachEntity(entity) as ColagemComeCotasDetalhe;
		}
		
		virtual public void Combine(ColagemComeCotasDetalheCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ColagemComeCotasDetalhe this[int index]
		{
			get
			{
				return base[index] as ColagemComeCotasDetalhe;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ColagemComeCotasDetalhe);
		}
	}



	[Serializable]
	abstract public class esColagemComeCotasDetalhe : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esColagemComeCotasDetalheQuery GetDynamicQuery()
		{
			return null;
		}

		public esColagemComeCotasDetalhe()
		{

		}

		public esColagemComeCotasDetalhe(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idColagemComeCotasDetalhe)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemComeCotasDetalhe);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemComeCotasDetalhe);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idColagemComeCotasDetalhe)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemComeCotasDetalhe);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemComeCotasDetalhe);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idColagemComeCotasDetalhe)
		{
			esColagemComeCotasDetalheQuery query = this.GetDynamicQuery();
			query.Where(query.IdColagemComeCotasDetalhe == idColagemComeCotasDetalhe);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idColagemComeCotasDetalhe)
		{
			esParameters parms = new esParameters();
			parms.Add("IdColagemComeCotasDetalhe",idColagemComeCotasDetalhe);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdColagemComeCotasDetalhe": this.str.IdColagemComeCotasDetalhe = (string)value; break;							
						case "CodigoExterno": this.str.CodigoExterno = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "CotaOperacao": this.str.CotaOperacao = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "ValorCPMF": this.str.ValorCPMF = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "PrejuizoUsado": this.str.PrejuizoUsado = (string)value; break;							
						case "RendimentoComeCotas": this.str.RendimentoComeCotas = (string)value; break;							
						case "VariacaoComeCotas": this.str.VariacaoComeCotas = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdColagemComeCotasDetalhe":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemComeCotasDetalhe = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "CotaOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaOperacao = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "ValorCPMF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMF = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "PrejuizoUsado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoUsado = (System.Decimal?)value;
							break;
						
						case "RendimentoComeCotas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoComeCotas = (System.Decimal?)value;
							break;
						
						case "VariacaoComeCotas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VariacaoComeCotas = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.IdColagemComeCotasDetalhe
		/// </summary>
		virtual public System.Int32? IdColagemComeCotasDetalhe
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasDetalheMetadata.ColumnNames.IdColagemComeCotasDetalhe);
			}
			
			set
			{
				base.SetSystemInt32(ColagemComeCotasDetalheMetadata.ColumnNames.IdColagemComeCotasDetalhe, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.CodigoExterno
		/// </summary>
		virtual public System.String CodigoExterno
		{
			get
			{
				return base.GetSystemString(ColagemComeCotasDetalheMetadata.ColumnNames.CodigoExterno);
			}
			
			set
			{
				base.SetSystemString(ColagemComeCotasDetalheMetadata.ColumnNames.CodigoExterno, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(ColagemComeCotasDetalheMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemComeCotasDetalheMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasDetalheMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(ColagemComeCotasDetalheMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasDetalheMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(ColagemComeCotasDetalheMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(ColagemComeCotasDetalheMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemComeCotasDetalheMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.CotaOperacao
		/// </summary>
		virtual public System.Decimal? CotaOperacao
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.CotaOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.CotaOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.ValorCPMF
		/// </summary>
		virtual public System.Decimal? ValorCPMF
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.ValorCPMF);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.ValorCPMF, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.PrejuizoUsado
		/// </summary>
		virtual public System.Decimal? PrejuizoUsado
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.PrejuizoUsado);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.PrejuizoUsado, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.RendimentoComeCotas
		/// </summary>
		virtual public System.Decimal? RendimentoComeCotas
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.RendimentoComeCotas);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.RendimentoComeCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.VariacaoComeCotas
		/// </summary>
		virtual public System.Decimal? VariacaoComeCotas
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.VariacaoComeCotas);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.VariacaoComeCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhe.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasDetalheMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esColagemComeCotasDetalhe entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdColagemComeCotasDetalhe
			{
				get
				{
					System.Int32? data = entity.IdColagemComeCotasDetalhe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemComeCotasDetalhe = null;
					else entity.IdColagemComeCotasDetalhe = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoExterno
			{
				get
				{
					System.String data = entity.CodigoExterno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoExterno = null;
					else entity.CodigoExterno = Convert.ToString(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaOperacao
			{
				get
				{
					System.Decimal? data = entity.CotaOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaOperacao = null;
					else entity.CotaOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCPMF
			{
				get
				{
					System.Decimal? data = entity.ValorCPMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMF = null;
					else entity.ValorCPMF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrejuizoUsado
			{
				get
				{
					System.Decimal? data = entity.PrejuizoUsado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoUsado = null;
					else entity.PrejuizoUsado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoComeCotas
			{
				get
				{
					System.Decimal? data = entity.RendimentoComeCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoComeCotas = null;
					else entity.RendimentoComeCotas = Convert.ToDecimal(value);
				}
			}
				
			public System.String VariacaoComeCotas
			{
				get
				{
					System.Decimal? data = entity.VariacaoComeCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VariacaoComeCotas = null;
					else entity.VariacaoComeCotas = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
			

			private esColagemComeCotasDetalhe entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esColagemComeCotasDetalheQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esColagemComeCotasDetalhe can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ColagemComeCotasDetalhe : esColagemComeCotasDetalhe
	{

				
		#region ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ColagemComeCotass_Det_Pos_FK
		/// </summary>

		[XmlIgnore]
		public ColagemComeCotasDetalhePosicaoCollection ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe
		{
			get
			{
				if(this._ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe == null)
				{
					this._ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe = new ColagemComeCotasDetalhePosicaoCollection();
					this._ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe", this._ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe);
				
					if(this.IdColagemComeCotasDetalhe != null)
					{
						this._ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe.Query.Where(this._ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe.Query.IdColagemComeCotasDetalhe == this.IdColagemComeCotasDetalhe);
						this._ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe.Query.Load();

						// Auto-hookup Foreign Keys
						this._ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe.fks.Add(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.IdColagemComeCotasDetalhe, this.IdColagemComeCotasDetalhe);
					}
				}

				return this._ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe != null) 
				{ 
					this.RemovePostSave("ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe"); 
					this._ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe = null;
					
				} 
			} 			
		}

		private ColagemComeCotasDetalhePosicaoCollection _ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe", typeof(ColagemComeCotasDetalhePosicaoCollection), new ColagemComeCotasDetalhePosicao()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe != null)
			{
				foreach(ColagemComeCotasDetalhePosicao obj in this._ColagemComeCotasDetalhePosicaoCollectionByIdColagemComeCotasDetalhe)
				{
					if(obj.es.IsAdded)
					{
						obj.IdColagemComeCotasDetalhe = this.IdColagemComeCotasDetalhe;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esColagemComeCotasDetalheQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ColagemComeCotasDetalheMetadata.Meta();
			}
		}	
		

		public esQueryItem IdColagemComeCotasDetalhe
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.IdColagemComeCotasDetalhe, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoExterno
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.CodigoExterno, esSystemType.String);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaOperacao
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.CotaOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCPMF
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.ValorCPMF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrejuizoUsado
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.PrejuizoUsado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoComeCotas
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.RendimentoComeCotas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VariacaoComeCotas
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.VariacaoComeCotas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalheMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ColagemComeCotasDetalheCollection")]
	public partial class ColagemComeCotasDetalheCollection : esColagemComeCotasDetalheCollection, IEnumerable<ColagemComeCotasDetalhe>
	{
		public ColagemComeCotasDetalheCollection()
		{

		}
		
		public static implicit operator List<ColagemComeCotasDetalhe>(ColagemComeCotasDetalheCollection coll)
		{
			List<ColagemComeCotasDetalhe> list = new List<ColagemComeCotasDetalhe>();
			
			foreach (ColagemComeCotasDetalhe emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ColagemComeCotasDetalheMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemComeCotasDetalheQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ColagemComeCotasDetalhe(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ColagemComeCotasDetalhe();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ColagemComeCotasDetalheQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemComeCotasDetalheQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ColagemComeCotasDetalheQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ColagemComeCotasDetalhe AddNew()
		{
			ColagemComeCotasDetalhe entity = base.AddNewEntity() as ColagemComeCotasDetalhe;
			
			return entity;
		}

		public ColagemComeCotasDetalhe FindByPrimaryKey(System.Int32 idColagemComeCotasDetalhe)
		{
			return base.FindByPrimaryKey(idColagemComeCotasDetalhe) as ColagemComeCotasDetalhe;
		}


		#region IEnumerable<ColagemComeCotasDetalhe> Members

		IEnumerator<ColagemComeCotasDetalhe> IEnumerable<ColagemComeCotasDetalhe>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ColagemComeCotasDetalhe;
			}
		}

		#endregion
		
		private ColagemComeCotasDetalheQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ColagemComeCotasDetalhe' table
	/// </summary>

	[Serializable]
	public partial class ColagemComeCotasDetalhe : esColagemComeCotasDetalhe
	{
		public ColagemComeCotasDetalhe()
		{

		}
	
		public ColagemComeCotasDetalhe(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ColagemComeCotasDetalheMetadata.Meta();
			}
		}
		
		
		
		override protected esColagemComeCotasDetalheQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemComeCotasDetalheQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ColagemComeCotasDetalheQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemComeCotasDetalheQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ColagemComeCotasDetalheQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ColagemComeCotasDetalheQuery query;
	}



	[Serializable]
	public partial class ColagemComeCotasDetalheQuery : esColagemComeCotasDetalheQuery
	{
		public ColagemComeCotasDetalheQuery()
		{

		}		
		
		public ColagemComeCotasDetalheQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ColagemComeCotasDetalheMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ColagemComeCotasDetalheMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.IdColagemComeCotasDetalhe, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.IdColagemComeCotasDetalhe;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.CodigoExterno, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.CodigoExterno;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.DataReferencia, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.IdCliente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.IdCarteira, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.DataConversao, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.Quantidade, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.CotaOperacao, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.CotaOperacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.ValorBruto, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.ValorLiquido, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.ValorCPMF, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.ValorCPMF;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.ValorPerformance, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.PrejuizoUsado, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.PrejuizoUsado;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.RendimentoComeCotas, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.RendimentoComeCotas;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.VariacaoComeCotas, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.VariacaoComeCotas;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.ValorIR, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalheMetadata.ColumnNames.ValorIOF, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasDetalheMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ColagemComeCotasDetalheMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdColagemComeCotasDetalhe = "IdColagemComeCotasDetalhe";
			 public const string CodigoExterno = "CodigoExterno";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataConversao = "DataConversao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaOperacao = "CotaOperacao";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoComeCotas = "RendimentoComeCotas";
			 public const string VariacaoComeCotas = "VariacaoComeCotas";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdColagemComeCotasDetalhe = "IdColagemComeCotasDetalhe";
			 public const string CodigoExterno = "CodigoExterno";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataConversao = "DataConversao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaOperacao = "CotaOperacao";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoComeCotas = "RendimentoComeCotas";
			 public const string VariacaoComeCotas = "VariacaoComeCotas";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ColagemComeCotasDetalheMetadata))
			{
				if(ColagemComeCotasDetalheMetadata.mapDelegates == null)
				{
					ColagemComeCotasDetalheMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ColagemComeCotasDetalheMetadata.meta == null)
				{
					ColagemComeCotasDetalheMetadata.meta = new ColagemComeCotasDetalheMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdColagemComeCotasDetalhe", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoExterno", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCPMF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrejuizoUsado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoComeCotas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VariacaoComeCotas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "ColagemComeCotasDetalhe";
				meta.Destination = "ColagemComeCotasDetalhe";
				
				meta.spInsert = "proc_ColagemComeCotasDetalheInsert";				
				meta.spUpdate = "proc_ColagemComeCotasDetalheUpdate";		
				meta.spDelete = "proc_ColagemComeCotasDetalheDelete";
				meta.spLoadAll = "proc_ColagemComeCotasDetalheLoadAll";
				meta.spLoadByPrimaryKey = "proc_ColagemComeCotasDetalheLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ColagemComeCotasDetalheMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
