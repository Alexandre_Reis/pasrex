/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 28/01/2016 10:46:06
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esCalculoResultadoCollection : esEntityCollection
	{
		public esCalculoResultadoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CalculoResultadoCollection";
		}

		#region Query Logic
		protected void InitQuery(esCalculoResultadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCalculoResultadoQuery);
		}
		#endregion
		
		virtual public CalculoResultado DetachEntity(CalculoResultado entity)
		{
			return base.DetachEntity(entity) as CalculoResultado;
		}
		
		virtual public CalculoResultado AttachEntity(CalculoResultado entity)
		{
			return base.AttachEntity(entity) as CalculoResultado;
		}
		
		virtual public void Combine(CalculoResultadoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CalculoResultado this[int index]
		{
			get
			{
				return base[index] as CalculoResultado;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CalculoResultado);
		}
	}



	[Serializable]
	abstract public class esCalculoResultado : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCalculoResultadoQuery GetDynamicQuery()
		{
			return null;
		}

		public esCalculoResultado()
		{

		}

		public esCalculoResultado(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.Int32 idCliente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idCliente);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.Int32 idCliente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idCliente);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.Int32 idCliente)
		{
			esCalculoResultadoQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.IdCliente == idCliente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.Int32 idCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("IdCliente",idCliente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "Pl": this.str.Pl = (string)value; break;							
						case "PnLAno": this.str.PnLAno = (string)value; break;							
						case "CustoAno": this.str.CustoAno = (string)value; break;							
						case "ResultadoAno": this.str.ResultadoAno = (string)value; break;							
						case "PnLMes": this.str.PnLMes = (string)value; break;							
						case "CustoMes": this.str.CustoMes = (string)value; break;							
						case "ResultadoMes": this.str.ResultadoMes = (string)value; break;							
						case "PnLDia": this.str.PnLDia = (string)value; break;							
						case "CustoDia": this.str.CustoDia = (string)value; break;							
						case "ResultadoDia": this.str.ResultadoDia = (string)value; break;							
						case "PLCalculoPnL": this.str.PLCalculoPnL = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "Pl":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pl = (System.Decimal?)value;
							break;
						
						case "PnLAno":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PnLAno = (System.Decimal?)value;
							break;
						
						case "CustoAno":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CustoAno = (System.Decimal?)value;
							break;
						
						case "ResultadoAno":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoAno = (System.Decimal?)value;
							break;
						
						case "PnLMes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PnLMes = (System.Decimal?)value;
							break;
						
						case "CustoMes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CustoMes = (System.Decimal?)value;
							break;
						
						case "ResultadoMes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoMes = (System.Decimal?)value;
							break;
						
						case "PnLDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PnLDia = (System.Decimal?)value;
							break;
						
						case "CustoDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CustoDia = (System.Decimal?)value;
							break;
						
						case "ResultadoDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoDia = (System.Decimal?)value;
							break;
						
						case "PLCalculoPnL":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PLCalculoPnL = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CalculoResultado.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(CalculoResultadoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(CalculoResultadoMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CalculoResultado.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(CalculoResultadoMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoResultadoMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoResultado.PL
		/// </summary>
		virtual public System.Decimal? Pl
		{
			get
			{
				return base.GetSystemDecimal(CalculoResultadoMetadata.ColumnNames.Pl);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoResultadoMetadata.ColumnNames.Pl, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoResultado.PnL_Ano
		/// </summary>
		virtual public System.Decimal? PnLAno
		{
			get
			{
				return base.GetSystemDecimal(CalculoResultadoMetadata.ColumnNames.PnLAno);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoResultadoMetadata.ColumnNames.PnLAno, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoResultado.Custo_Ano
		/// </summary>
		virtual public System.Decimal? CustoAno
		{
			get
			{
				return base.GetSystemDecimal(CalculoResultadoMetadata.ColumnNames.CustoAno);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoResultadoMetadata.ColumnNames.CustoAno, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoResultado.Resultado_Ano
		/// </summary>
		virtual public System.Decimal? ResultadoAno
		{
			get
			{
				return base.GetSystemDecimal(CalculoResultadoMetadata.ColumnNames.ResultadoAno);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoResultadoMetadata.ColumnNames.ResultadoAno, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoResultado.PnL_Mes
		/// </summary>
		virtual public System.Decimal? PnLMes
		{
			get
			{
				return base.GetSystemDecimal(CalculoResultadoMetadata.ColumnNames.PnLMes);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoResultadoMetadata.ColumnNames.PnLMes, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoResultado.Custo_Mes
		/// </summary>
		virtual public System.Decimal? CustoMes
		{
			get
			{
				return base.GetSystemDecimal(CalculoResultadoMetadata.ColumnNames.CustoMes);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoResultadoMetadata.ColumnNames.CustoMes, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoResultado.Resultado_Mes
		/// </summary>
		virtual public System.Decimal? ResultadoMes
		{
			get
			{
				return base.GetSystemDecimal(CalculoResultadoMetadata.ColumnNames.ResultadoMes);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoResultadoMetadata.ColumnNames.ResultadoMes, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoResultado.PnL_Dia
		/// </summary>
		virtual public System.Decimal? PnLDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoResultadoMetadata.ColumnNames.PnLDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoResultadoMetadata.ColumnNames.PnLDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoResultado.Custo_Dia
		/// </summary>
		virtual public System.Decimal? CustoDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoResultadoMetadata.ColumnNames.CustoDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoResultadoMetadata.ColumnNames.CustoDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoResultado.Resultado_Dia
		/// </summary>
		virtual public System.Decimal? ResultadoDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoResultadoMetadata.ColumnNames.ResultadoDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoResultadoMetadata.ColumnNames.ResultadoDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoResultado.PL_CalculoPnL
		/// </summary>
		virtual public System.Decimal? PLCalculoPnL
		{
			get
			{
				return base.GetSystemDecimal(CalculoResultadoMetadata.ColumnNames.PLCalculoPnL);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoResultadoMetadata.ColumnNames.PLCalculoPnL, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCalculoResultado entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String Pl
			{
				get
				{
					System.Decimal? data = entity.Pl;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pl = null;
					else entity.Pl = Convert.ToDecimal(value);
				}
			}
				
			public System.String PnLAno
			{
				get
				{
					System.Decimal? data = entity.PnLAno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PnLAno = null;
					else entity.PnLAno = Convert.ToDecimal(value);
				}
			}
				
			public System.String CustoAno
			{
				get
				{
					System.Decimal? data = entity.CustoAno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CustoAno = null;
					else entity.CustoAno = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoAno
			{
				get
				{
					System.Decimal? data = entity.ResultadoAno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoAno = null;
					else entity.ResultadoAno = Convert.ToDecimal(value);
				}
			}
				
			public System.String PnLMes
			{
				get
				{
					System.Decimal? data = entity.PnLMes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PnLMes = null;
					else entity.PnLMes = Convert.ToDecimal(value);
				}
			}
				
			public System.String CustoMes
			{
				get
				{
					System.Decimal? data = entity.CustoMes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CustoMes = null;
					else entity.CustoMes = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoMes
			{
				get
				{
					System.Decimal? data = entity.ResultadoMes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoMes = null;
					else entity.ResultadoMes = Convert.ToDecimal(value);
				}
			}
				
			public System.String PnLDia
			{
				get
				{
					System.Decimal? data = entity.PnLDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PnLDia = null;
					else entity.PnLDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String CustoDia
			{
				get
				{
					System.Decimal? data = entity.CustoDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CustoDia = null;
					else entity.CustoDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoDia
			{
				get
				{
					System.Decimal? data = entity.ResultadoDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoDia = null;
					else entity.ResultadoDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String PLCalculoPnL
			{
				get
				{
					System.Decimal? data = entity.PLCalculoPnL;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PLCalculoPnL = null;
					else entity.PLCalculoPnL = Convert.ToDecimal(value);
				}
			}
			

			private esCalculoResultado entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCalculoResultadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCalculoResultado can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CalculoResultado : esCalculoResultado
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_CalculoResultado_Cliente
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCalculoResultadoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CalculoResultadoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, CalculoResultadoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, CalculoResultadoMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Pl
		{
			get
			{
				return new esQueryItem(this, CalculoResultadoMetadata.ColumnNames.Pl, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PnLAno
		{
			get
			{
				return new esQueryItem(this, CalculoResultadoMetadata.ColumnNames.PnLAno, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CustoAno
		{
			get
			{
				return new esQueryItem(this, CalculoResultadoMetadata.ColumnNames.CustoAno, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoAno
		{
			get
			{
				return new esQueryItem(this, CalculoResultadoMetadata.ColumnNames.ResultadoAno, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PnLMes
		{
			get
			{
				return new esQueryItem(this, CalculoResultadoMetadata.ColumnNames.PnLMes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CustoMes
		{
			get
			{
				return new esQueryItem(this, CalculoResultadoMetadata.ColumnNames.CustoMes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoMes
		{
			get
			{
				return new esQueryItem(this, CalculoResultadoMetadata.ColumnNames.ResultadoMes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PnLDia
		{
			get
			{
				return new esQueryItem(this, CalculoResultadoMetadata.ColumnNames.PnLDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CustoDia
		{
			get
			{
				return new esQueryItem(this, CalculoResultadoMetadata.ColumnNames.CustoDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoDia
		{
			get
			{
				return new esQueryItem(this, CalculoResultadoMetadata.ColumnNames.ResultadoDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PLCalculoPnL
		{
			get
			{
				return new esQueryItem(this, CalculoResultadoMetadata.ColumnNames.PLCalculoPnL, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CalculoResultadoCollection")]
	public partial class CalculoResultadoCollection : esCalculoResultadoCollection, IEnumerable<CalculoResultado>
	{
		public CalculoResultadoCollection()
		{

		}
		
		public static implicit operator List<CalculoResultado>(CalculoResultadoCollection coll)
		{
			List<CalculoResultado> list = new List<CalculoResultado>();
			
			foreach (CalculoResultado emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CalculoResultadoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoResultadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CalculoResultado(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CalculoResultado();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CalculoResultadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoResultadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CalculoResultadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CalculoResultado AddNew()
		{
			CalculoResultado entity = base.AddNewEntity() as CalculoResultado;
			
			return entity;
		}

		public CalculoResultado FindByPrimaryKey(System.DateTime dataReferencia, System.Int32 idCliente)
		{
			return base.FindByPrimaryKey(dataReferencia, idCliente) as CalculoResultado;
		}


		#region IEnumerable<CalculoResultado> Members

		IEnumerator<CalculoResultado> IEnumerable<CalculoResultado>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CalculoResultado;
			}
		}

		#endregion
		
		private CalculoResultadoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CalculoResultado' table
	/// </summary>

	[Serializable]
	public partial class CalculoResultado : esCalculoResultado
	{
		public CalculoResultado()
		{

		}
	
		public CalculoResultado(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CalculoResultadoMetadata.Meta();
			}
		}
		
		
		
		override protected esCalculoResultadoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoResultadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CalculoResultadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoResultadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CalculoResultadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CalculoResultadoQuery query;
	}



	[Serializable]
	public partial class CalculoResultadoQuery : esCalculoResultadoQuery
	{
		public CalculoResultadoQuery()
		{

		}		
		
		public CalculoResultadoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CalculoResultadoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CalculoResultadoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CalculoResultadoMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoResultadoMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoResultadoMetadata.ColumnNames.DataReferencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoResultadoMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoResultadoMetadata.ColumnNames.Pl, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoResultadoMetadata.PropertyNames.Pl;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoResultadoMetadata.ColumnNames.PnLAno, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoResultadoMetadata.PropertyNames.PnLAno;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoResultadoMetadata.ColumnNames.CustoAno, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoResultadoMetadata.PropertyNames.CustoAno;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoResultadoMetadata.ColumnNames.ResultadoAno, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoResultadoMetadata.PropertyNames.ResultadoAno;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoResultadoMetadata.ColumnNames.PnLMes, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoResultadoMetadata.PropertyNames.PnLMes;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoResultadoMetadata.ColumnNames.CustoMes, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoResultadoMetadata.PropertyNames.CustoMes;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoResultadoMetadata.ColumnNames.ResultadoMes, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoResultadoMetadata.PropertyNames.ResultadoMes;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoResultadoMetadata.ColumnNames.PnLDia, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoResultadoMetadata.PropertyNames.PnLDia;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoResultadoMetadata.ColumnNames.CustoDia, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoResultadoMetadata.PropertyNames.CustoDia;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoResultadoMetadata.ColumnNames.ResultadoDia, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoResultadoMetadata.PropertyNames.ResultadoDia;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoResultadoMetadata.ColumnNames.PLCalculoPnL, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoResultadoMetadata.PropertyNames.PLCalculoPnL;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CalculoResultadoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string DataReferencia = "DataReferencia";
			 public const string Pl = "PL";
			 public const string PnLAno = "PnL_Ano";
			 public const string CustoAno = "Custo_Ano";
			 public const string ResultadoAno = "Resultado_Ano";
			 public const string PnLMes = "PnL_Mes";
			 public const string CustoMes = "Custo_Mes";
			 public const string ResultadoMes = "Resultado_Mes";
			 public const string PnLDia = "PnL_Dia";
			 public const string CustoDia = "Custo_Dia";
			 public const string ResultadoDia = "Resultado_Dia";
			 public const string PLCalculoPnL = "PL_CalculoPnL";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string DataReferencia = "DataReferencia";
			 public const string Pl = "Pl";
			 public const string PnLAno = "PnLAno";
			 public const string CustoAno = "CustoAno";
			 public const string ResultadoAno = "ResultadoAno";
			 public const string PnLMes = "PnLMes";
			 public const string CustoMes = "CustoMes";
			 public const string ResultadoMes = "ResultadoMes";
			 public const string PnLDia = "PnLDia";
			 public const string CustoDia = "CustoDia";
			 public const string ResultadoDia = "ResultadoDia";
			 public const string PLCalculoPnL = "PLCalculoPnL";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CalculoResultadoMetadata))
			{
				if(CalculoResultadoMetadata.mapDelegates == null)
				{
					CalculoResultadoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CalculoResultadoMetadata.meta == null)
				{
					CalculoResultadoMetadata.meta = new CalculoResultadoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PL", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PnL_Ano", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Custo_Ano", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Resultado_Ano", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PnL_Mes", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Custo_Mes", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Resultado_Mes", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PnL_Dia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Custo_Dia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Resultado_Dia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PL_CalculoPnL", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CalculoResultado";
				meta.Destination = "CalculoResultado";
				
				meta.spInsert = "proc_CalculoResultadoInsert";				
				meta.spUpdate = "proc_CalculoResultadoUpdate";		
				meta.spDelete = "proc_CalculoResultadoDelete";
				meta.spLoadAll = "proc_CalculoResultadoLoadAll";
				meta.spLoadByPrimaryKey = "proc_CalculoResultadoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CalculoResultadoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
