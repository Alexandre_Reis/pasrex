/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/12/2014 15:53:03
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esEventoFundoMovimentoCautelasCollection : esEntityCollection
	{
		public esEventoFundoMovimentoCautelasCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EventoFundoMovimentoCautelasCollection";
		}

		#region Query Logic
		protected void InitQuery(esEventoFundoMovimentoCautelasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEventoFundoMovimentoCautelasQuery);
		}
		#endregion
		
		virtual public EventoFundoMovimentoCautelas DetachEntity(EventoFundoMovimentoCautelas entity)
		{
			return base.DetachEntity(entity) as EventoFundoMovimentoCautelas;
		}
		
		virtual public EventoFundoMovimentoCautelas AttachEntity(EventoFundoMovimentoCautelas entity)
		{
			return base.AttachEntity(entity) as EventoFundoMovimentoCautelas;
		}
		
		virtual public void Combine(EventoFundoMovimentoCautelasCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EventoFundoMovimentoCautelas this[int index]
		{
			get
			{
				return base[index] as EventoFundoMovimentoCautelas;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EventoFundoMovimentoCautelas);
		}
	}



	[Serializable]
	abstract public class esEventoFundoMovimentoCautelas : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEventoFundoMovimentoCautelasQuery GetDynamicQuery()
		{
			return null;
		}

		public esEventoFundoMovimentoCautelas()
		{

		}

		public esEventoFundoMovimentoCautelas(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEventoFundo, System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEventoFundo, idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idEventoFundo, idOperacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEventoFundo, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEventoFundo, idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idEventoFundo, idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEventoFundo, System.Int32 idOperacao)
		{
			esEventoFundoMovimentoCautelasQuery query = this.GetDynamicQuery();
			query.Where(query.IdEventoFundo == idEventoFundo, query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEventoFundo, System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEventoFundo",idEventoFundo);			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEventoFundo": this.str.IdEventoFundo = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEventoFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoFundo = (System.Int32?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EventoFundoMovimentoCautelas.IdEventoFundo
		/// </summary>
		virtual public System.Int32? IdEventoFundo
		{
			get
			{
				return base.GetSystemInt32(EventoFundoMovimentoCautelasMetadata.ColumnNames.IdEventoFundo);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoFundoMovimentoCautelasMetadata.ColumnNames.IdEventoFundo, value))
				{
					this._UpToEventoFundoByIdEventoFundo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoMovimentoCautelas.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(EventoFundoMovimentoCautelasMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoMovimentoCautelasMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoMovimentoCautelas.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(EventoFundoMovimentoCautelasMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoMovimentoCautelasMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected EventoFundo _UpToEventoFundoByIdEventoFundo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEventoFundoMovimentoCautelas entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEventoFundo
			{
				get
				{
					System.Int32? data = entity.IdEventoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoFundo = null;
					else entity.IdEventoFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
			

			private esEventoFundoMovimentoCautelas entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEventoFundoMovimentoCautelasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEventoFundoMovimentoCautelas can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EventoFundoMovimentoCautelas : esEventoFundoMovimentoCautelas
	{

				
		#region UpToEventoFundoByIdEventoFundo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EventoFundoMovimentoCautelas_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundo UpToEventoFundoByIdEventoFundo
		{
			get
			{
				if(this._UpToEventoFundoByIdEventoFundo == null
					&& IdEventoFundo != null					)
				{
					this._UpToEventoFundoByIdEventoFundo = new EventoFundo();
					this._UpToEventoFundoByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Where(this._UpToEventoFundoByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Load();
				}

				return this._UpToEventoFundoByIdEventoFundo;
			}
			
			set
			{
				this.RemovePreSave("UpToEventoFundoByIdEventoFundo");
				

				if(value == null)
				{
					this.IdEventoFundo = null;
					this._UpToEventoFundoByIdEventoFundo = null;
				}
				else
				{
					this.IdEventoFundo = value.IdEventoFundo;
					this._UpToEventoFundoByIdEventoFundo = value;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEventoFundoByIdEventoFundo != null)
			{
				this.IdEventoFundo = this._UpToEventoFundoByIdEventoFundo.IdEventoFundo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEventoFundoMovimentoCautelasQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoMovimentoCautelasMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEventoFundo
		{
			get
			{
				return new esQueryItem(this, EventoFundoMovimentoCautelasMetadata.ColumnNames.IdEventoFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, EventoFundoMovimentoCautelasMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, EventoFundoMovimentoCautelasMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EventoFundoMovimentoCautelasCollection")]
	public partial class EventoFundoMovimentoCautelasCollection : esEventoFundoMovimentoCautelasCollection, IEnumerable<EventoFundoMovimentoCautelas>
	{
		public EventoFundoMovimentoCautelasCollection()
		{

		}
		
		public static implicit operator List<EventoFundoMovimentoCautelas>(EventoFundoMovimentoCautelasCollection coll)
		{
			List<EventoFundoMovimentoCautelas> list = new List<EventoFundoMovimentoCautelas>();
			
			foreach (EventoFundoMovimentoCautelas emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EventoFundoMovimentoCautelasMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoMovimentoCautelasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EventoFundoMovimentoCautelas(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EventoFundoMovimentoCautelas();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EventoFundoMovimentoCautelasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoMovimentoCautelasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EventoFundoMovimentoCautelasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EventoFundoMovimentoCautelas AddNew()
		{
			EventoFundoMovimentoCautelas entity = base.AddNewEntity() as EventoFundoMovimentoCautelas;
			
			return entity;
		}

		public EventoFundoMovimentoCautelas FindByPrimaryKey(System.Int32 idEventoFundo, System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(idEventoFundo, idOperacao) as EventoFundoMovimentoCautelas;
		}


		#region IEnumerable<EventoFundoMovimentoCautelas> Members

		IEnumerator<EventoFundoMovimentoCautelas> IEnumerable<EventoFundoMovimentoCautelas>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EventoFundoMovimentoCautelas;
			}
		}

		#endregion
		
		private EventoFundoMovimentoCautelasQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EventoFundoMovimentoCautelas' table
	/// </summary>

	[Serializable]
	public partial class EventoFundoMovimentoCautelas : esEventoFundoMovimentoCautelas
	{
		public EventoFundoMovimentoCautelas()
		{

		}
	
		public EventoFundoMovimentoCautelas(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoMovimentoCautelasMetadata.Meta();
			}
		}
		
		
		
		override protected esEventoFundoMovimentoCautelasQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoMovimentoCautelasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EventoFundoMovimentoCautelasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoMovimentoCautelasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EventoFundoMovimentoCautelasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EventoFundoMovimentoCautelasQuery query;
	}



	[Serializable]
	public partial class EventoFundoMovimentoCautelasQuery : esEventoFundoMovimentoCautelasQuery
	{
		public EventoFundoMovimentoCautelasQuery()
		{

		}		
		
		public EventoFundoMovimentoCautelasQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EventoFundoMovimentoCautelasMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EventoFundoMovimentoCautelasMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EventoFundoMovimentoCautelasMetadata.ColumnNames.IdEventoFundo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoMovimentoCautelasMetadata.PropertyNames.IdEventoFundo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoMovimentoCautelasMetadata.ColumnNames.IdOperacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoMovimentoCautelasMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoMovimentoCautelasMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoMovimentoCautelasMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EventoFundoMovimentoCautelasMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EventoFundoMovimentoCautelasMetadata))
			{
				if(EventoFundoMovimentoCautelasMetadata.mapDelegates == null)
				{
					EventoFundoMovimentoCautelasMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EventoFundoMovimentoCautelasMetadata.meta == null)
				{
					EventoFundoMovimentoCautelasMetadata.meta = new EventoFundoMovimentoCautelasMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEventoFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "EventoFundoMovimentoCautelas";
				meta.Destination = "EventoFundoMovimentoCautelas";
				
				meta.spInsert = "proc_EventoFundoMovimentoCautelasInsert";				
				meta.spUpdate = "proc_EventoFundoMovimentoCautelasUpdate";		
				meta.spDelete = "proc_EventoFundoMovimentoCautelasDelete";
				meta.spLoadAll = "proc_EventoFundoMovimentoCautelasLoadAll";
				meta.spLoadByPrimaryKey = "proc_EventoFundoMovimentoCautelasLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EventoFundoMovimentoCautelasMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
