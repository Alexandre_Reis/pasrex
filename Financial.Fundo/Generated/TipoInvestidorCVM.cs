/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:23 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTipoInvestidorCVMCollection : esEntityCollection
	{
		public esTipoInvestidorCVMCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TipoInvestidorCVMCollection";
		}

		#region Query Logic
		protected void InitQuery(esTipoInvestidorCVMQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTipoInvestidorCVMQuery);
		}
		#endregion
		
		virtual public TipoInvestidorCVM DetachEntity(TipoInvestidorCVM entity)
		{
			return base.DetachEntity(entity) as TipoInvestidorCVM;
		}
		
		virtual public TipoInvestidorCVM AttachEntity(TipoInvestidorCVM entity)
		{
			return base.AttachEntity(entity) as TipoInvestidorCVM;
		}
		
		virtual public void Combine(TipoInvestidorCVMCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TipoInvestidorCVM this[int index]
		{
			get
			{
				return base[index] as TipoInvestidorCVM;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TipoInvestidorCVM);
		}
	}



	[Serializable]
	abstract public class esTipoInvestidorCVM : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTipoInvestidorCVMQuery GetDynamicQuery()
		{
			return null;
		}

		public esTipoInvestidorCVM()
		{

		}

		public esTipoInvestidorCVM(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 tipoCVM)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(tipoCVM);
			else
				return LoadByPrimaryKeyStoredProcedure(tipoCVM);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 tipoCVM)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTipoInvestidorCVMQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.TipoCVM == tipoCVM);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 tipoCVM)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(tipoCVM);
			else
				return LoadByPrimaryKeyStoredProcedure(tipoCVM);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 tipoCVM)
		{
			esTipoInvestidorCVMQuery query = this.GetDynamicQuery();
			query.Where(query.TipoCVM == tipoCVM);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 tipoCVM)
		{
			esParameters parms = new esParameters();
			parms.Add("TipoCVM",tipoCVM);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "TipoCVM": this.str.TipoCVM = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "TipoCVM":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoCVM = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TipoInvestidorCVM.TipoCVM
		/// </summary>
		virtual public System.Int32? TipoCVM
		{
			get
			{
				return base.GetSystemInt32(TipoInvestidorCVMMetadata.ColumnNames.TipoCVM);
			}
			
			set
			{
				base.SetSystemInt32(TipoInvestidorCVMMetadata.ColumnNames.TipoCVM, value);
			}
		}
		
		/// <summary>
		/// Maps to TipoInvestidorCVM.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(TipoInvestidorCVMMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(TipoInvestidorCVMMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTipoInvestidorCVM entity)
			{
				this.entity = entity;
			}
			
	
			public System.String TipoCVM
			{
				get
				{
					System.Int32? data = entity.TipoCVM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCVM = null;
					else entity.TipoCVM = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esTipoInvestidorCVM entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTipoInvestidorCVMQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTipoInvestidorCVM can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TipoInvestidorCVM : esTipoInvestidorCVM
	{

				
		#region TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TipoInvestidorCVM_TabelaTaxaFiscalizacaoCVM_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaFiscalizacaoCVMCollection TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM
		{
			get
			{
				if(this._TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM == null)
				{
					this._TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM = new TabelaTaxaFiscalizacaoCVMCollection();
					this._TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM", this._TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM);
				
					if(this.TipoCVM != null)
					{
						this._TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM.Query.Where(this._TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM.Query.TipoCVM == this.TipoCVM);
						this._TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM.fks.Add(TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.TipoCVM, this.TipoCVM);
					}
				}

				return this._TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM != null) 
				{ 
					this.RemovePostSave("TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM"); 
					this._TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM = null;
					
				} 
			} 			
		}

		private TabelaTaxaFiscalizacaoCVMCollection _TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "TabelaTaxaFiscalizacaoCVMCollectionByTipoCVM", typeof(TabelaTaxaFiscalizacaoCVMCollection), new TabelaTaxaFiscalizacaoCVM()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTipoInvestidorCVMQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TipoInvestidorCVMMetadata.Meta();
			}
		}	
		

		public esQueryItem TipoCVM
		{
			get
			{
				return new esQueryItem(this, TipoInvestidorCVMMetadata.ColumnNames.TipoCVM, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, TipoInvestidorCVMMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TipoInvestidorCVMCollection")]
	public partial class TipoInvestidorCVMCollection : esTipoInvestidorCVMCollection, IEnumerable<TipoInvestidorCVM>
	{
		public TipoInvestidorCVMCollection()
		{

		}
		
		public static implicit operator List<TipoInvestidorCVM>(TipoInvestidorCVMCollection coll)
		{
			List<TipoInvestidorCVM> list = new List<TipoInvestidorCVM>();
			
			foreach (TipoInvestidorCVM emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TipoInvestidorCVMMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TipoInvestidorCVMQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TipoInvestidorCVM(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TipoInvestidorCVM();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TipoInvestidorCVMQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TipoInvestidorCVMQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TipoInvestidorCVMQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TipoInvestidorCVM AddNew()
		{
			TipoInvestidorCVM entity = base.AddNewEntity() as TipoInvestidorCVM;
			
			return entity;
		}

		public TipoInvestidorCVM FindByPrimaryKey(System.Int32 tipoCVM)
		{
			return base.FindByPrimaryKey(tipoCVM) as TipoInvestidorCVM;
		}


		#region IEnumerable<TipoInvestidorCVM> Members

		IEnumerator<TipoInvestidorCVM> IEnumerable<TipoInvestidorCVM>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TipoInvestidorCVM;
			}
		}

		#endregion
		
		private TipoInvestidorCVMQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TipoInvestidorCVM' table
	/// </summary>

	[Serializable]
	public partial class TipoInvestidorCVM : esTipoInvestidorCVM
	{
		public TipoInvestidorCVM()
		{

		}
	
		public TipoInvestidorCVM(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TipoInvestidorCVMMetadata.Meta();
			}
		}
		
		
		
		override protected esTipoInvestidorCVMQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TipoInvestidorCVMQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TipoInvestidorCVMQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TipoInvestidorCVMQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TipoInvestidorCVMQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TipoInvestidorCVMQuery query;
	}



	[Serializable]
	public partial class TipoInvestidorCVMQuery : esTipoInvestidorCVMQuery
	{
		public TipoInvestidorCVMQuery()
		{

		}		
		
		public TipoInvestidorCVMQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TipoInvestidorCVMMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TipoInvestidorCVMMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TipoInvestidorCVMMetadata.ColumnNames.TipoCVM, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TipoInvestidorCVMMetadata.PropertyNames.TipoCVM;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TipoInvestidorCVMMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TipoInvestidorCVMMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 400;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TipoInvestidorCVMMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string TipoCVM = "TipoCVM";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string TipoCVM = "TipoCVM";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TipoInvestidorCVMMetadata))
			{
				if(TipoInvestidorCVMMetadata.mapDelegates == null)
				{
					TipoInvestidorCVMMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TipoInvestidorCVMMetadata.meta == null)
				{
					TipoInvestidorCVMMetadata.meta = new TipoInvestidorCVMMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("TipoCVM", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "TipoInvestidorCVM";
				meta.Destination = "TipoInvestidorCVM";
				
				meta.spInsert = "proc_TipoInvestidorCVMInsert";				
				meta.spUpdate = "proc_TipoInvestidorCVMUpdate";		
				meta.spDelete = "proc_TipoInvestidorCVMDelete";
				meta.spLoadAll = "proc_TipoInvestidorCVMLoadAll";
				meta.spLoadByPrimaryKey = "proc_TipoInvestidorCVMLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TipoInvestidorCVMMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
