/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:20 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		





		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esSubCategoriaFundoCollection : esEntityCollection
	{
		public esSubCategoriaFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SubCategoriaFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esSubCategoriaFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSubCategoriaFundoQuery);
		}
		#endregion
		
		virtual public SubCategoriaFundo DetachEntity(SubCategoriaFundo entity)
		{
			return base.DetachEntity(entity) as SubCategoriaFundo;
		}
		
		virtual public SubCategoriaFundo AttachEntity(SubCategoriaFundo entity)
		{
			return base.AttachEntity(entity) as SubCategoriaFundo;
		}
		
		virtual public void Combine(SubCategoriaFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SubCategoriaFundo this[int index]
		{
			get
			{
				return base[index] as SubCategoriaFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SubCategoriaFundo);
		}
	}



	[Serializable]
	abstract public class esSubCategoriaFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSubCategoriaFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esSubCategoriaFundo()
		{

		}

		public esSubCategoriaFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCategoria, System.Int32 idSubCategoria)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCategoria, idSubCategoria);
			else
				return LoadByPrimaryKeyStoredProcedure(idCategoria, idSubCategoria);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCategoria, System.Int32 idSubCategoria)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esSubCategoriaFundoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCategoria == idCategoria, query.IdSubCategoria == idSubCategoria);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCategoria, System.Int32 idSubCategoria)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCategoria, idSubCategoria);
			else
				return LoadByPrimaryKeyStoredProcedure(idCategoria, idSubCategoria);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCategoria, System.Int32 idSubCategoria)
		{
			esSubCategoriaFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdCategoria == idCategoria, query.IdSubCategoria == idSubCategoria);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCategoria, System.Int32 idSubCategoria)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCategoria",idCategoria);			parms.Add("IdSubCategoria",idSubCategoria);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCategoria": this.str.IdCategoria = (string)value; break;							
						case "IdSubCategoria": this.str.IdSubCategoria = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCategoria":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCategoria = (System.Int32?)value;
							break;
						
						case "IdSubCategoria":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSubCategoria = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SubCategoriaFundo.IdCategoria
		/// </summary>
		virtual public System.Int32? IdCategoria
		{
			get
			{
				return base.GetSystemInt32(SubCategoriaFundoMetadata.ColumnNames.IdCategoria);
			}
			
			set
			{
				if(base.SetSystemInt32(SubCategoriaFundoMetadata.ColumnNames.IdCategoria, value))
				{
					this._UpToCategoriaFundoByIdCategoria = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SubCategoriaFundo.IdSubCategoria
		/// </summary>
		virtual public System.Int32? IdSubCategoria
		{
			get
			{
				return base.GetSystemInt32(SubCategoriaFundoMetadata.ColumnNames.IdSubCategoria);
			}
			
			set
			{
				base.SetSystemInt32(SubCategoriaFundoMetadata.ColumnNames.IdSubCategoria, value);
			}
		}
		
		/// <summary>
		/// Maps to SubCategoriaFundo.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(SubCategoriaFundoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(SubCategoriaFundoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected CategoriaFundo _UpToCategoriaFundoByIdCategoria;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSubCategoriaFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCategoria
			{
				get
				{
					System.Int32? data = entity.IdCategoria;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCategoria = null;
					else entity.IdCategoria = Convert.ToInt32(value);
				}
			}
				
			public System.String IdSubCategoria
			{
				get
				{
					System.Int32? data = entity.IdSubCategoria;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSubCategoria = null;
					else entity.IdSubCategoria = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esSubCategoriaFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSubCategoriaFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSubCategoriaFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SubCategoriaFundo : esSubCategoriaFundo
	{

				
		#region UpToCategoriaFundoByIdCategoria - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - CategoriaFundo_SubCategoriaFundo_FK1
		/// </summary>

		[XmlIgnore]
		public CategoriaFundo UpToCategoriaFundoByIdCategoria
		{
			get
			{
				if(this._UpToCategoriaFundoByIdCategoria == null
					&& IdCategoria != null					)
				{
					this._UpToCategoriaFundoByIdCategoria = new CategoriaFundo();
					this._UpToCategoriaFundoByIdCategoria.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCategoriaFundoByIdCategoria", this._UpToCategoriaFundoByIdCategoria);
					this._UpToCategoriaFundoByIdCategoria.Query.Where(this._UpToCategoriaFundoByIdCategoria.Query.IdCategoria == this.IdCategoria);
					this._UpToCategoriaFundoByIdCategoria.Query.Load();
				}

				return this._UpToCategoriaFundoByIdCategoria;
			}
			
			set
			{
				this.RemovePreSave("UpToCategoriaFundoByIdCategoria");
				

				if(value == null)
				{
					this.IdCategoria = null;
					this._UpToCategoriaFundoByIdCategoria = null;
				}
				else
				{
					this.IdCategoria = value.IdCategoria;
					this._UpToCategoriaFundoByIdCategoria = value;
					this.SetPreSave("UpToCategoriaFundoByIdCategoria", this._UpToCategoriaFundoByIdCategoria);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToCategoriaFundoByIdCategoria != null)
			{
				this.IdCategoria = this._UpToCategoriaFundoByIdCategoria.IdCategoria;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSubCategoriaFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SubCategoriaFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCategoria
		{
			get
			{
				return new esQueryItem(this, SubCategoriaFundoMetadata.ColumnNames.IdCategoria, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdSubCategoria
		{
			get
			{
				return new esQueryItem(this, SubCategoriaFundoMetadata.ColumnNames.IdSubCategoria, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, SubCategoriaFundoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SubCategoriaFundoCollection")]
	public partial class SubCategoriaFundoCollection : esSubCategoriaFundoCollection, IEnumerable<SubCategoriaFundo>
	{
		public SubCategoriaFundoCollection()
		{

		}
		
		public static implicit operator List<SubCategoriaFundo>(SubCategoriaFundoCollection coll)
		{
			List<SubCategoriaFundo> list = new List<SubCategoriaFundo>();
			
			foreach (SubCategoriaFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SubCategoriaFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SubCategoriaFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SubCategoriaFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SubCategoriaFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SubCategoriaFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SubCategoriaFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SubCategoriaFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SubCategoriaFundo AddNew()
		{
			SubCategoriaFundo entity = base.AddNewEntity() as SubCategoriaFundo;
			
			return entity;
		}

		public SubCategoriaFundo FindByPrimaryKey(System.Int32 idCategoria, System.Int32 idSubCategoria)
		{
			return base.FindByPrimaryKey(idCategoria, idSubCategoria) as SubCategoriaFundo;
		}


		#region IEnumerable<SubCategoriaFundo> Members

		IEnumerator<SubCategoriaFundo> IEnumerable<SubCategoriaFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SubCategoriaFundo;
			}
		}

		#endregion
		
		private SubCategoriaFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SubCategoriaFundo' table
	/// </summary>

	[Serializable]
	public partial class SubCategoriaFundo : esSubCategoriaFundo
	{
		public SubCategoriaFundo()
		{

		}
	
		public SubCategoriaFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SubCategoriaFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esSubCategoriaFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SubCategoriaFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SubCategoriaFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SubCategoriaFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SubCategoriaFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SubCategoriaFundoQuery query;
	}



	[Serializable]
	public partial class SubCategoriaFundoQuery : esSubCategoriaFundoQuery
	{
		public SubCategoriaFundoQuery()
		{

		}		
		
		public SubCategoriaFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SubCategoriaFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SubCategoriaFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SubCategoriaFundoMetadata.ColumnNames.IdCategoria, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SubCategoriaFundoMetadata.PropertyNames.IdCategoria;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubCategoriaFundoMetadata.ColumnNames.IdSubCategoria, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SubCategoriaFundoMetadata.PropertyNames.IdSubCategoria;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubCategoriaFundoMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = SubCategoriaFundoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SubCategoriaFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCategoria = "IdCategoria";
			 public const string IdSubCategoria = "IdSubCategoria";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCategoria = "IdCategoria";
			 public const string IdSubCategoria = "IdSubCategoria";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SubCategoriaFundoMetadata))
			{
				if(SubCategoriaFundoMetadata.mapDelegates == null)
				{
					SubCategoriaFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SubCategoriaFundoMetadata.meta == null)
				{
					SubCategoriaFundoMetadata.meta = new SubCategoriaFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCategoria", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdSubCategoria", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "SubCategoriaFundo";
				meta.Destination = "SubCategoriaFundo";
				
				meta.spInsert = "proc_SubCategoriaFundoInsert";				
				meta.spUpdate = "proc_SubCategoriaFundoUpdate";		
				meta.spDelete = "proc_SubCategoriaFundoDelete";
				meta.spLoadAll = "proc_SubCategoriaFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_SubCategoriaFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SubCategoriaFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
