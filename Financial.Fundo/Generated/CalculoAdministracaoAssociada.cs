/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/09/2014 13:19:43
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esCalculoAdministracaoAssociadaCollection : esEntityCollection
	{
		public esCalculoAdministracaoAssociadaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CalculoAdministracaoAssociadaCollection";
		}

		#region Query Logic
		protected void InitQuery(esCalculoAdministracaoAssociadaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCalculoAdministracaoAssociadaQuery);
		}
		#endregion
		
		virtual public CalculoAdministracaoAssociada DetachEntity(CalculoAdministracaoAssociada entity)
		{
			return base.DetachEntity(entity) as CalculoAdministracaoAssociada;
		}
		
		virtual public CalculoAdministracaoAssociada AttachEntity(CalculoAdministracaoAssociada entity)
		{
			return base.AttachEntity(entity) as CalculoAdministracaoAssociada;
		}
		
		virtual public void Combine(CalculoAdministracaoAssociadaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CalculoAdministracaoAssociada this[int index]
		{
			get
			{
				return base[index] as CalculoAdministracaoAssociada;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CalculoAdministracaoAssociada);
		}
	}



	[Serializable]
	abstract public class esCalculoAdministracaoAssociada : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCalculoAdministracaoAssociadaQuery GetDynamicQuery()
		{
			return null;
		}

		public esCalculoAdministracaoAssociada()
		{

		}

		public esCalculoAdministracaoAssociada(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabela)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabela)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabela)
		{
			esCalculoAdministracaoAssociadaQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabela == idTabela);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabela)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabela",idTabela);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "ValorDia": this.str.ValorDia = (string)value; break;							
						case "ValorAcumulado": this.str.ValorAcumulado = (string)value; break;							
						case "DataFimApropriacao": this.str.DataFimApropriacao = (string)value; break;							
						case "DataPagamento": this.str.DataPagamento = (string)value; break;							
						case "ValorCPMFDia": this.str.ValorCPMFDia = (string)value; break;							
						case "ValorCPMFAcumulado": this.str.ValorCPMFAcumulado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "ValorDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorDia = (System.Decimal?)value;
							break;
						
						case "ValorAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAcumulado = (System.Decimal?)value;
							break;
						
						case "DataFimApropriacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFimApropriacao = (System.DateTime?)value;
							break;
						
						case "DataPagamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPagamento = (System.DateTime?)value;
							break;
						
						case "ValorCPMFDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMFDia = (System.Decimal?)value;
							break;
						
						case "ValorCPMFAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMFAcumulado = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CalculoAdministracaoAssociada.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(CalculoAdministracaoAssociadaMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				base.SetSystemInt32(CalculoAdministracaoAssociadaMetadata.ColumnNames.IdTabela, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoAssociada.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(CalculoAdministracaoAssociadaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(CalculoAdministracaoAssociadaMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoAssociada.ValorDia
		/// </summary>
		virtual public System.Decimal? ValorDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoAssociada.ValorAcumulado
		/// </summary>
		virtual public System.Decimal? ValorAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorAcumulado, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoAssociada.DataFimApropriacao
		/// </summary>
		virtual public System.DateTime? DataFimApropriacao
		{
			get
			{
				return base.GetSystemDateTime(CalculoAdministracaoAssociadaMetadata.ColumnNames.DataFimApropriacao);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoAdministracaoAssociadaMetadata.ColumnNames.DataFimApropriacao, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoAssociada.DataPagamento
		/// </summary>
		virtual public System.DateTime? DataPagamento
		{
			get
			{
				return base.GetSystemDateTime(CalculoAdministracaoAssociadaMetadata.ColumnNames.DataPagamento);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoAdministracaoAssociadaMetadata.ColumnNames.DataPagamento, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoAssociada.ValorCPMFDia
		/// </summary>
		virtual public System.Decimal? ValorCPMFDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorCPMFDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorCPMFDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoAssociada.ValorCPMFAcumulado
		/// </summary>
		virtual public System.Decimal? ValorCPMFAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorCPMFAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorCPMFAcumulado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCalculoAdministracaoAssociada entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorDia
			{
				get
				{
					System.Decimal? data = entity.ValorDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDia = null;
					else entity.ValorDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorAcumulado
			{
				get
				{
					System.Decimal? data = entity.ValorAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAcumulado = null;
					else entity.ValorAcumulado = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataFimApropriacao
			{
				get
				{
					System.DateTime? data = entity.DataFimApropriacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFimApropriacao = null;
					else entity.DataFimApropriacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataPagamento
			{
				get
				{
					System.DateTime? data = entity.DataPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPagamento = null;
					else entity.DataPagamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorCPMFDia
			{
				get
				{
					System.Decimal? data = entity.ValorCPMFDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMFDia = null;
					else entity.ValorCPMFDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCPMFAcumulado
			{
				get
				{
					System.Decimal? data = entity.ValorCPMFAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMFAcumulado = null;
					else entity.ValorCPMFAcumulado = Convert.ToDecimal(value);
				}
			}
			

			private esCalculoAdministracaoAssociada entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCalculoAdministracaoAssociadaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCalculoAdministracaoAssociada can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CalculoAdministracaoAssociada : esCalculoAdministracaoAssociada
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_CalculoAdministracaoAssociada_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		#region UpToTabelaTaxaAdministracao - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - TabelaTaxaAdministracao_CalculoAdministracaoAssociada_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaAdministracao UpToTabelaTaxaAdministracao
		{
			get
			{
				if(this._UpToTabelaTaxaAdministracao == null
					&& IdTabela != null					)
				{
					this._UpToTabelaTaxaAdministracao = new TabelaTaxaAdministracao();
					this._UpToTabelaTaxaAdministracao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTabelaTaxaAdministracao", this._UpToTabelaTaxaAdministracao);
					this._UpToTabelaTaxaAdministracao.Query.Where(this._UpToTabelaTaxaAdministracao.Query.IdTabela == this.IdTabela);
					this._UpToTabelaTaxaAdministracao.Query.Load();
				}

				return this._UpToTabelaTaxaAdministracao;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToTabelaTaxaAdministracao");

				if(value == null)
				{
					this._UpToTabelaTaxaAdministracao = null;
				}
				else
				{
					this._UpToTabelaTaxaAdministracao = value;
					this.SetPreSave("UpToTabelaTaxaAdministracao", this._UpToTabelaTaxaAdministracao);
				}
				
				
			} 
		}

		private TabelaTaxaAdministracao _UpToTabelaTaxaAdministracao;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTabelaTaxaAdministracao != null)
			{
				this.IdTabela = this._UpToTabelaTaxaAdministracao.IdTabela;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCalculoAdministracaoAssociadaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CalculoAdministracaoAssociadaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoAssociadaMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoAssociadaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorDia
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorAcumulado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataFimApropriacao
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoAssociadaMetadata.ColumnNames.DataFimApropriacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataPagamento
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoAssociadaMetadata.ColumnNames.DataPagamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorCPMFDia
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorCPMFDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCPMFAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorCPMFAcumulado, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CalculoAdministracaoAssociadaCollection")]
	public partial class CalculoAdministracaoAssociadaCollection : esCalculoAdministracaoAssociadaCollection, IEnumerable<CalculoAdministracaoAssociada>
	{
		public CalculoAdministracaoAssociadaCollection()
		{

		}
		
		public static implicit operator List<CalculoAdministracaoAssociada>(CalculoAdministracaoAssociadaCollection coll)
		{
			List<CalculoAdministracaoAssociada> list = new List<CalculoAdministracaoAssociada>();
			
			foreach (CalculoAdministracaoAssociada emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CalculoAdministracaoAssociadaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoAdministracaoAssociadaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CalculoAdministracaoAssociada(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CalculoAdministracaoAssociada();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CalculoAdministracaoAssociadaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoAdministracaoAssociadaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CalculoAdministracaoAssociadaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CalculoAdministracaoAssociada AddNew()
		{
			CalculoAdministracaoAssociada entity = base.AddNewEntity() as CalculoAdministracaoAssociada;
			
			return entity;
		}

		public CalculoAdministracaoAssociada FindByPrimaryKey(System.Int32 idTabela)
		{
			return base.FindByPrimaryKey(idTabela) as CalculoAdministracaoAssociada;
		}


		#region IEnumerable<CalculoAdministracaoAssociada> Members

		IEnumerator<CalculoAdministracaoAssociada> IEnumerable<CalculoAdministracaoAssociada>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CalculoAdministracaoAssociada;
			}
		}

		#endregion
		
		private CalculoAdministracaoAssociadaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CalculoAdministracaoAssociada' table
	/// </summary>

	[Serializable]
	public partial class CalculoAdministracaoAssociada : esCalculoAdministracaoAssociada
	{
		public CalculoAdministracaoAssociada()
		{

		}
	
		public CalculoAdministracaoAssociada(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CalculoAdministracaoAssociadaMetadata.Meta();
			}
		}
		
		
		
		override protected esCalculoAdministracaoAssociadaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoAdministracaoAssociadaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CalculoAdministracaoAssociadaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoAdministracaoAssociadaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CalculoAdministracaoAssociadaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CalculoAdministracaoAssociadaQuery query;
	}



	[Serializable]
	public partial class CalculoAdministracaoAssociadaQuery : esCalculoAdministracaoAssociadaQuery
	{
		public CalculoAdministracaoAssociadaQuery()
		{

		}		
		
		public CalculoAdministracaoAssociadaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CalculoAdministracaoAssociadaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CalculoAdministracaoAssociadaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CalculoAdministracaoAssociadaMetadata.ColumnNames.IdTabela, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoAdministracaoAssociadaMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoAssociadaMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoAdministracaoAssociadaMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorDia, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoAdministracaoAssociadaMetadata.PropertyNames.ValorDia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorAcumulado, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoAdministracaoAssociadaMetadata.PropertyNames.ValorAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoAssociadaMetadata.ColumnNames.DataFimApropriacao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoAdministracaoAssociadaMetadata.PropertyNames.DataFimApropriacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoAssociadaMetadata.ColumnNames.DataPagamento, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoAdministracaoAssociadaMetadata.PropertyNames.DataPagamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorCPMFDia, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoAdministracaoAssociadaMetadata.PropertyNames.ValorCPMFDia;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoAssociadaMetadata.ColumnNames.ValorCPMFAcumulado, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoAdministracaoAssociadaMetadata.PropertyNames.ValorCPMFAcumulado;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CalculoAdministracaoAssociadaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorDia = "ValorDia";
			 public const string ValorAcumulado = "ValorAcumulado";
			 public const string DataFimApropriacao = "DataFimApropriacao";
			 public const string DataPagamento = "DataPagamento";
			 public const string ValorCPMFDia = "ValorCPMFDia";
			 public const string ValorCPMFAcumulado = "ValorCPMFAcumulado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorDia = "ValorDia";
			 public const string ValorAcumulado = "ValorAcumulado";
			 public const string DataFimApropriacao = "DataFimApropriacao";
			 public const string DataPagamento = "DataPagamento";
			 public const string ValorCPMFDia = "ValorCPMFDia";
			 public const string ValorCPMFAcumulado = "ValorCPMFAcumulado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CalculoAdministracaoAssociadaMetadata))
			{
				if(CalculoAdministracaoAssociadaMetadata.mapDelegates == null)
				{
					CalculoAdministracaoAssociadaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CalculoAdministracaoAssociadaMetadata.meta == null)
				{
					CalculoAdministracaoAssociadaMetadata.meta = new CalculoAdministracaoAssociadaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorAcumulado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataFimApropriacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataPagamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorCPMFDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCPMFAcumulado", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CalculoAdministracaoAssociada";
				meta.Destination = "CalculoAdministracaoAssociada";
				
				meta.spInsert = "proc_CalculoAdministracaoAssociadaInsert";				
				meta.spUpdate = "proc_CalculoAdministracaoAssociadaUpdate";		
				meta.spDelete = "proc_CalculoAdministracaoAssociadaDelete";
				meta.spLoadAll = "proc_CalculoAdministracaoAssociadaLoadAll";
				meta.spLoadByPrimaryKey = "proc_CalculoAdministracaoAssociadaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CalculoAdministracaoAssociadaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
