/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/12/2014 17:58:30
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esEventoFundoPosicaoCautelasCollection : esEntityCollection
	{
		public esEventoFundoPosicaoCautelasCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EventoFundoPosicaoCautelasCollection";
		}

		#region Query Logic
		protected void InitQuery(esEventoFundoPosicaoCautelasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEventoFundoPosicaoCautelasQuery);
		}
		#endregion
		
		virtual public EventoFundoPosicaoCautelas DetachEntity(EventoFundoPosicaoCautelas entity)
		{
			return base.DetachEntity(entity) as EventoFundoPosicaoCautelas;
		}
		
		virtual public EventoFundoPosicaoCautelas AttachEntity(EventoFundoPosicaoCautelas entity)
		{
			return base.AttachEntity(entity) as EventoFundoPosicaoCautelas;
		}
		
		virtual public void Combine(EventoFundoPosicaoCautelasCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EventoFundoPosicaoCautelas this[int index]
		{
			get
			{
				return base[index] as EventoFundoPosicaoCautelas;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EventoFundoPosicaoCautelas);
		}
	}



	[Serializable]
	abstract public class esEventoFundoPosicaoCautelas : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEventoFundoPosicaoCautelasQuery GetDynamicQuery()
		{
			return null;
		}

		public esEventoFundoPosicaoCautelas()
		{

		}

		public esEventoFundoPosicaoCautelas(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEventoFundo, System.Int32 idPosicao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEventoFundo, idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idEventoFundo, idPosicao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEventoFundo, System.Int32 idPosicao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEventoFundo, idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idEventoFundo, idPosicao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEventoFundo, System.Int32 idPosicao)
		{
			esEventoFundoPosicaoCautelasQuery query = this.GetDynamicQuery();
			query.Where(query.IdEventoFundo == idEventoFundo, query.IdPosicao == idPosicao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEventoFundo, System.Int32 idPosicao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEventoFundo",idEventoFundo);			parms.Add("IdPosicao",idPosicao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEventoFundo": this.str.IdEventoFundo = (string)value; break;							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEventoFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoFundo = (System.Int32?)value;
							break;
						
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EventoFundoPosicaoCautelas.IdEventoFundo
		/// </summary>
		virtual public System.Int32? IdEventoFundo
		{
			get
			{
				return base.GetSystemInt32(EventoFundoPosicaoCautelasMetadata.ColumnNames.IdEventoFundo);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoFundoPosicaoCautelasMetadata.ColumnNames.IdEventoFundo, value))
				{
					this._UpToEventoFundoByIdEventoFundo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoPosicaoCautelas.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(EventoFundoPosicaoCautelasMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoPosicaoCautelasMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoPosicaoCautelas.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(EventoFundoPosicaoCautelasMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoPosicaoCautelasMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected EventoFundo _UpToEventoFundoByIdEventoFundo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEventoFundoPosicaoCautelas entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEventoFundo
			{
				get
				{
					System.Int32? data = entity.IdEventoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoFundo = null;
					else entity.IdEventoFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
			

			private esEventoFundoPosicaoCautelas entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEventoFundoPosicaoCautelasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEventoFundoPosicaoCautelas can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EventoFundoPosicaoCautelas : esEventoFundoPosicaoCautelas
	{

				
		#region UpToEventoFundoByIdEventoFundo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EventoFundoPosicaoCautelas_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundo UpToEventoFundoByIdEventoFundo
		{
			get
			{
				if(this._UpToEventoFundoByIdEventoFundo == null
					&& IdEventoFundo != null					)
				{
					this._UpToEventoFundoByIdEventoFundo = new EventoFundo();
					this._UpToEventoFundoByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Where(this._UpToEventoFundoByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Load();
				}

				return this._UpToEventoFundoByIdEventoFundo;
			}
			
			set
			{
				this.RemovePreSave("UpToEventoFundoByIdEventoFundo");
				

				if(value == null)
				{
					this.IdEventoFundo = null;
					this._UpToEventoFundoByIdEventoFundo = null;
				}
				else
				{
					this.IdEventoFundo = value.IdEventoFundo;
					this._UpToEventoFundoByIdEventoFundo = value;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEventoFundoByIdEventoFundo != null)
			{
				this.IdEventoFundo = this._UpToEventoFundoByIdEventoFundo.IdEventoFundo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEventoFundoPosicaoCautelasQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoPosicaoCautelasMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEventoFundo
		{
			get
			{
				return new esQueryItem(this, EventoFundoPosicaoCautelasMetadata.ColumnNames.IdEventoFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, EventoFundoPosicaoCautelasMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, EventoFundoPosicaoCautelasMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EventoFundoPosicaoCautelasCollection")]
	public partial class EventoFundoPosicaoCautelasCollection : esEventoFundoPosicaoCautelasCollection, IEnumerable<EventoFundoPosicaoCautelas>
	{
		public EventoFundoPosicaoCautelasCollection()
		{

		}
		
		public static implicit operator List<EventoFundoPosicaoCautelas>(EventoFundoPosicaoCautelasCollection coll)
		{
			List<EventoFundoPosicaoCautelas> list = new List<EventoFundoPosicaoCautelas>();
			
			foreach (EventoFundoPosicaoCautelas emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EventoFundoPosicaoCautelasMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoPosicaoCautelasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EventoFundoPosicaoCautelas(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EventoFundoPosicaoCautelas();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EventoFundoPosicaoCautelasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoPosicaoCautelasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EventoFundoPosicaoCautelasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EventoFundoPosicaoCautelas AddNew()
		{
			EventoFundoPosicaoCautelas entity = base.AddNewEntity() as EventoFundoPosicaoCautelas;
			
			return entity;
		}

		public EventoFundoPosicaoCautelas FindByPrimaryKey(System.Int32 idEventoFundo, System.Int32 idPosicao)
		{
			return base.FindByPrimaryKey(idEventoFundo, idPosicao) as EventoFundoPosicaoCautelas;
		}


		#region IEnumerable<EventoFundoPosicaoCautelas> Members

		IEnumerator<EventoFundoPosicaoCautelas> IEnumerable<EventoFundoPosicaoCautelas>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EventoFundoPosicaoCautelas;
			}
		}

		#endregion
		
		private EventoFundoPosicaoCautelasQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EventoFundoPosicaoCautelas' table
	/// </summary>

	[Serializable]
	public partial class EventoFundoPosicaoCautelas : esEventoFundoPosicaoCautelas
	{
		public EventoFundoPosicaoCautelas()
		{

		}
	
		public EventoFundoPosicaoCautelas(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoPosicaoCautelasMetadata.Meta();
			}
		}
		
		
		
		override protected esEventoFundoPosicaoCautelasQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoPosicaoCautelasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EventoFundoPosicaoCautelasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoPosicaoCautelasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EventoFundoPosicaoCautelasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EventoFundoPosicaoCautelasQuery query;
	}



	[Serializable]
	public partial class EventoFundoPosicaoCautelasQuery : esEventoFundoPosicaoCautelasQuery
	{
		public EventoFundoPosicaoCautelasQuery()
		{

		}		
		
		public EventoFundoPosicaoCautelasQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EventoFundoPosicaoCautelasMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EventoFundoPosicaoCautelasMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EventoFundoPosicaoCautelasMetadata.ColumnNames.IdEventoFundo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoPosicaoCautelasMetadata.PropertyNames.IdEventoFundo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoPosicaoCautelasMetadata.ColumnNames.IdPosicao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoPosicaoCautelasMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoPosicaoCautelasMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoPosicaoCautelasMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EventoFundoPosicaoCautelasMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string IdPosicao = "IdPosicao";
			 public const string IdCliente = "IdCliente";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string IdPosicao = "IdPosicao";
			 public const string IdCliente = "IdCliente";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EventoFundoPosicaoCautelasMetadata))
			{
				if(EventoFundoPosicaoCautelasMetadata.mapDelegates == null)
				{
					EventoFundoPosicaoCautelasMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EventoFundoPosicaoCautelasMetadata.meta == null)
				{
					EventoFundoPosicaoCautelasMetadata.meta = new EventoFundoPosicaoCautelasMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEventoFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "EventoFundoPosicaoCautelas";
				meta.Destination = "EventoFundoPosicaoCautelas";
				
				meta.spInsert = "proc_EventoFundoPosicaoCautelasInsert";				
				meta.spUpdate = "proc_EventoFundoPosicaoCautelasUpdate";		
				meta.spDelete = "proc_EventoFundoPosicaoCautelasDelete";
				meta.spLoadAll = "proc_EventoFundoPosicaoCautelasLoadAll";
				meta.spLoadByPrimaryKey = "proc_EventoFundoPosicaoCautelasLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EventoFundoPosicaoCautelasMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
