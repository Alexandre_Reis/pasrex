/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 11/12/2015 1:57:32 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	












		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esLaminaCollection : esEntityCollection
	{
		public esLaminaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "LaminaCollection";
		}

		#region Query Logic
		protected void InitQuery(esLaminaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esLaminaQuery);
		}
		#endregion
		
		virtual public Lamina DetachEntity(Lamina entity)
		{
			return base.DetachEntity(entity) as Lamina;
		}
		
		virtual public Lamina AttachEntity(Lamina entity)
		{
			return base.AttachEntity(entity) as Lamina;
		}
		
		virtual public void Combine(LaminaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Lamina this[int index]
		{
			get
			{
				return base[index] as Lamina;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Lamina);
		}
	}



	[Serializable]
	abstract public class esLamina : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esLaminaQuery GetDynamicQuery()
		{
			return null;
		}

		public esLamina()
		{

		}

		public esLamina(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCarteira, System.DateTime inicioVigencia)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira, inicioVigencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira, inicioVigencia);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCarteira, System.DateTime inicioVigencia)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esLaminaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCarteira == idCarteira, query.InicioVigencia == inicioVigencia);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCarteira, System.DateTime inicioVigencia)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira, inicioVigencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira, inicioVigencia);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCarteira, System.DateTime inicioVigencia)
		{
			esLaminaQuery query = this.GetDynamicQuery();
			query.Where(query.IdCarteira == idCarteira, query.InicioVigencia == inicioVigencia);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCarteira, System.DateTime inicioVigencia)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCarteira",idCarteira);			parms.Add("InicioVigencia",inicioVigencia);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "InicioVigencia": this.str.InicioVigencia = (string)value; break;							
						case "EnderecoEletronico": this.str.EnderecoEletronico = (string)value; break;							
						case "DescricaoPublicoAlvo": this.str.DescricaoPublicoAlvo = (string)value; break;							
						case "ObjetivoFundo": this.str.ObjetivoFundo = (string)value; break;							
						case "DescricaoPoliticaInvestimento": this.str.DescricaoPoliticaInvestimento = (string)value; break;							
						case "LimiteAplicacaoExterior": this.str.LimiteAplicacaoExterior = (string)value; break;							
						case "LimiteCreditoPrivado": this.str.LimiteCreditoPrivado = (string)value; break;							
						case "DerivativosProtecaoCarteira": this.str.DerivativosProtecaoCarteira = (string)value; break;							
						case "LimiteAlavancagem": this.str.LimiteAlavancagem = (string)value; break;							
						case "PrazoCarencia": this.str.PrazoCarencia = (string)value; break;							
						case "TaxaEntrada": this.str.TaxaEntrada = (string)value; break;							
						case "TaxaSaida": this.str.TaxaSaida = (string)value; break;							
						case "Risco": this.str.Risco = (string)value; break;							
						case "FundoEstruturado": this.str.FundoEstruturado = (string)value; break;							
						case "CenariosApuracaoRentabilidade": this.str.CenariosApuracaoRentabilidade = (string)value; break;							
						case "DesempenhoFundo": this.str.DesempenhoFundo = (string)value; break;							
						case "PoliticaDistribuicao": this.str.PoliticaDistribuicao = (string)value; break;							
						case "Telefone": this.str.Telefone = (string)value; break;							
						case "UrlAtendimento": this.str.UrlAtendimento = (string)value; break;							
						case "Reclamacoes": this.str.Reclamacoes = (string)value; break;							
						case "FundosInvestimento": this.str.FundosInvestimento = (string)value; break;							
						case "FundosInvestimentoCotas": this.str.FundosInvestimentoCotas = (string)value; break;							
						case "EstrategiaPerdas": this.str.EstrategiaPerdas = (string)value; break;							
						case "RegulamentoPerdasPatrimoniais": this.str.RegulamentoPerdasPatrimoniais = (string)value; break;							
						case "RegulamentoPatrimonioNegativo": this.str.RegulamentoPatrimonioNegativo = (string)value; break;							
						case "ExisteCarencia": this.str.ExisteCarencia = (string)value; break;							
						case "ExisteTaxaEntrada": this.str.ExisteTaxaEntrada = (string)value; break;							
						case "ExisteTaxaSaida": this.str.ExisteTaxaSaida = (string)value; break;							
						case "LimiteConcentracaoEmissor": this.str.LimiteConcentracaoEmissor = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "InicioVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.InicioVigencia = (System.DateTime?)value;
							break;
						
						case "LimiteAplicacaoExterior":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.LimiteAplicacaoExterior = (System.Decimal?)value;
							break;
						
						case "LimiteCreditoPrivado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.LimiteCreditoPrivado = (System.Decimal?)value;
							break;
						
						case "LimiteAlavancagem":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.LimiteAlavancagem = (System.Decimal?)value;
							break;
						
						case "Risco":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Risco = (System.Int32?)value;
							break;
						
						case "LimiteConcentracaoEmissor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.LimiteConcentracaoEmissor = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Lamina.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(LaminaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(LaminaMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Lamina.InicioVigencia
		/// </summary>
		virtual public System.DateTime? InicioVigencia
		{
			get
			{
				return base.GetSystemDateTime(LaminaMetadata.ColumnNames.InicioVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(LaminaMetadata.ColumnNames.InicioVigencia, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.EnderecoEletronico
		/// </summary>
		virtual public System.String EnderecoEletronico
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.EnderecoEletronico);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.EnderecoEletronico, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.DescricaoPublicoAlvo
		/// </summary>
		virtual public System.String DescricaoPublicoAlvo
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.DescricaoPublicoAlvo);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.DescricaoPublicoAlvo, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.ObjetivoFundo
		/// </summary>
		virtual public System.String ObjetivoFundo
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.ObjetivoFundo);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.ObjetivoFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.DescricaoPoliticaInvestimento
		/// </summary>
		virtual public System.String DescricaoPoliticaInvestimento
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.DescricaoPoliticaInvestimento);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.DescricaoPoliticaInvestimento, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.LimiteAplicacaoExterior
		/// </summary>
		virtual public System.Decimal? LimiteAplicacaoExterior
		{
			get
			{
				return base.GetSystemDecimal(LaminaMetadata.ColumnNames.LimiteAplicacaoExterior);
			}
			
			set
			{
				base.SetSystemDecimal(LaminaMetadata.ColumnNames.LimiteAplicacaoExterior, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.LimiteCreditoPrivado
		/// </summary>
		virtual public System.Decimal? LimiteCreditoPrivado
		{
			get
			{
				return base.GetSystemDecimal(LaminaMetadata.ColumnNames.LimiteCreditoPrivado);
			}
			
			set
			{
				base.SetSystemDecimal(LaminaMetadata.ColumnNames.LimiteCreditoPrivado, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.DerivativosProtecaoCarteira
		/// </summary>
		virtual public System.String DerivativosProtecaoCarteira
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.DerivativosProtecaoCarteira);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.DerivativosProtecaoCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.LimiteAlavancagem
		/// </summary>
		virtual public System.Decimal? LimiteAlavancagem
		{
			get
			{
				return base.GetSystemDecimal(LaminaMetadata.ColumnNames.LimiteAlavancagem);
			}
			
			set
			{
				base.SetSystemDecimal(LaminaMetadata.ColumnNames.LimiteAlavancagem, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.PrazoCarencia
		/// </summary>
		virtual public System.String PrazoCarencia
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.PrazoCarencia);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.PrazoCarencia, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.TaxaEntrada
		/// </summary>
		virtual public System.String TaxaEntrada
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.TaxaEntrada);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.TaxaEntrada, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.TaxaSaida
		/// </summary>
		virtual public System.String TaxaSaida
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.TaxaSaida);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.TaxaSaida, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.Risco
		/// </summary>
		virtual public System.Int32? Risco
		{
			get
			{
				return base.GetSystemInt32(LaminaMetadata.ColumnNames.Risco);
			}
			
			set
			{
				base.SetSystemInt32(LaminaMetadata.ColumnNames.Risco, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.FundoEstruturado
		/// </summary>
		virtual public System.String FundoEstruturado
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.FundoEstruturado);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.FundoEstruturado, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.CenariosApuracaoRentabilidade
		/// </summary>
		virtual public System.String CenariosApuracaoRentabilidade
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.CenariosApuracaoRentabilidade);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.CenariosApuracaoRentabilidade, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.DesempenhoFundo
		/// </summary>
		virtual public System.String DesempenhoFundo
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.DesempenhoFundo);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.DesempenhoFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.PoliticaDistribuicao
		/// </summary>
		virtual public System.String PoliticaDistribuicao
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.PoliticaDistribuicao);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.PoliticaDistribuicao, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.Telefone
		/// </summary>
		virtual public System.String Telefone
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.Telefone);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.Telefone, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.UrlAtendimento
		/// </summary>
		virtual public System.String UrlAtendimento
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.UrlAtendimento);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.UrlAtendimento, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.Reclamacoes
		/// </summary>
		virtual public System.String Reclamacoes
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.Reclamacoes);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.Reclamacoes, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.FundosInvestimento
		/// </summary>
		virtual public System.String FundosInvestimento
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.FundosInvestimento);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.FundosInvestimento, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.FundosInvestimentoCotas
		/// </summary>
		virtual public System.String FundosInvestimentoCotas
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.FundosInvestimentoCotas);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.FundosInvestimentoCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.EstrategiaPerdas
		/// </summary>
		virtual public System.String EstrategiaPerdas
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.EstrategiaPerdas);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.EstrategiaPerdas, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.RegulamentoPerdasPatrimoniais
		/// </summary>
		virtual public System.String RegulamentoPerdasPatrimoniais
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.RegulamentoPerdasPatrimoniais);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.RegulamentoPerdasPatrimoniais, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.RegulamentoPatrimonioNegativo
		/// </summary>
		virtual public System.String RegulamentoPatrimonioNegativo
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.RegulamentoPatrimonioNegativo);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.RegulamentoPatrimonioNegativo, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.ExisteCarencia
		/// </summary>
		virtual public System.String ExisteCarencia
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.ExisteCarencia);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.ExisteCarencia, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.ExisteTaxaEntrada
		/// </summary>
		virtual public System.String ExisteTaxaEntrada
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.ExisteTaxaEntrada);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.ExisteTaxaEntrada, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.ExisteTaxaSaida
		/// </summary>
		virtual public System.String ExisteTaxaSaida
		{
			get
			{
				return base.GetSystemString(LaminaMetadata.ColumnNames.ExisteTaxaSaida);
			}
			
			set
			{
				base.SetSystemString(LaminaMetadata.ColumnNames.ExisteTaxaSaida, value);
			}
		}
		
		/// <summary>
		/// Maps to Lamina.LimiteConcentracaoEmissor
		/// </summary>
		virtual public System.Decimal? LimiteConcentracaoEmissor
		{
			get
			{
				return base.GetSystemDecimal(LaminaMetadata.ColumnNames.LimiteConcentracaoEmissor);
			}
			
			set
			{
				base.SetSystemDecimal(LaminaMetadata.ColumnNames.LimiteConcentracaoEmissor, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esLamina entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String InicioVigencia
			{
				get
				{
					System.DateTime? data = entity.InicioVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InicioVigencia = null;
					else entity.InicioVigencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String EnderecoEletronico
			{
				get
				{
					System.String data = entity.EnderecoEletronico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EnderecoEletronico = null;
					else entity.EnderecoEletronico = Convert.ToString(value);
				}
			}
				
			public System.String DescricaoPublicoAlvo
			{
				get
				{
					System.String data = entity.DescricaoPublicoAlvo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescricaoPublicoAlvo = null;
					else entity.DescricaoPublicoAlvo = Convert.ToString(value);
				}
			}
				
			public System.String ObjetivoFundo
			{
				get
				{
					System.String data = entity.ObjetivoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ObjetivoFundo = null;
					else entity.ObjetivoFundo = Convert.ToString(value);
				}
			}
				
			public System.String DescricaoPoliticaInvestimento
			{
				get
				{
					System.String data = entity.DescricaoPoliticaInvestimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescricaoPoliticaInvestimento = null;
					else entity.DescricaoPoliticaInvestimento = Convert.ToString(value);
				}
			}
				
			public System.String LimiteAplicacaoExterior
			{
				get
				{
					System.Decimal? data = entity.LimiteAplicacaoExterior;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LimiteAplicacaoExterior = null;
					else entity.LimiteAplicacaoExterior = Convert.ToDecimal(value);
				}
			}
				
			public System.String LimiteCreditoPrivado
			{
				get
				{
					System.Decimal? data = entity.LimiteCreditoPrivado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LimiteCreditoPrivado = null;
					else entity.LimiteCreditoPrivado = Convert.ToDecimal(value);
				}
			}
				
			public System.String DerivativosProtecaoCarteira
			{
				get
				{
					System.String data = entity.DerivativosProtecaoCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DerivativosProtecaoCarteira = null;
					else entity.DerivativosProtecaoCarteira = Convert.ToString(value);
				}
			}
				
			public System.String LimiteAlavancagem
			{
				get
				{
					System.Decimal? data = entity.LimiteAlavancagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LimiteAlavancagem = null;
					else entity.LimiteAlavancagem = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrazoCarencia
			{
				get
				{
					System.String data = entity.PrazoCarencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoCarencia = null;
					else entity.PrazoCarencia = Convert.ToString(value);
				}
			}
				
			public System.String TaxaEntrada
			{
				get
				{
					System.String data = entity.TaxaEntrada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaEntrada = null;
					else entity.TaxaEntrada = Convert.ToString(value);
				}
			}
				
			public System.String TaxaSaida
			{
				get
				{
					System.String data = entity.TaxaSaida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaSaida = null;
					else entity.TaxaSaida = Convert.ToString(value);
				}
			}
				
			public System.String Risco
			{
				get
				{
					System.Int32? data = entity.Risco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Risco = null;
					else entity.Risco = Convert.ToInt32(value);
				}
			}
				
			public System.String FundoEstruturado
			{
				get
				{
					System.String data = entity.FundoEstruturado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FundoEstruturado = null;
					else entity.FundoEstruturado = Convert.ToString(value);
				}
			}
				
			public System.String CenariosApuracaoRentabilidade
			{
				get
				{
					System.String data = entity.CenariosApuracaoRentabilidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CenariosApuracaoRentabilidade = null;
					else entity.CenariosApuracaoRentabilidade = Convert.ToString(value);
				}
			}
				
			public System.String DesempenhoFundo
			{
				get
				{
					System.String data = entity.DesempenhoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DesempenhoFundo = null;
					else entity.DesempenhoFundo = Convert.ToString(value);
				}
			}
				
			public System.String PoliticaDistribuicao
			{
				get
				{
					System.String data = entity.PoliticaDistribuicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PoliticaDistribuicao = null;
					else entity.PoliticaDistribuicao = Convert.ToString(value);
				}
			}
				
			public System.String Telefone
			{
				get
				{
					System.String data = entity.Telefone;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Telefone = null;
					else entity.Telefone = Convert.ToString(value);
				}
			}
				
			public System.String UrlAtendimento
			{
				get
				{
					System.String data = entity.UrlAtendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UrlAtendimento = null;
					else entity.UrlAtendimento = Convert.ToString(value);
				}
			}
				
			public System.String Reclamacoes
			{
				get
				{
					System.String data = entity.Reclamacoes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Reclamacoes = null;
					else entity.Reclamacoes = Convert.ToString(value);
				}
			}
				
			public System.String FundosInvestimento
			{
				get
				{
					System.String data = entity.FundosInvestimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FundosInvestimento = null;
					else entity.FundosInvestimento = Convert.ToString(value);
				}
			}
				
			public System.String FundosInvestimentoCotas
			{
				get
				{
					System.String data = entity.FundosInvestimentoCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FundosInvestimentoCotas = null;
					else entity.FundosInvestimentoCotas = Convert.ToString(value);
				}
			}
				
			public System.String EstrategiaPerdas
			{
				get
				{
					System.String data = entity.EstrategiaPerdas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EstrategiaPerdas = null;
					else entity.EstrategiaPerdas = Convert.ToString(value);
				}
			}
				
			public System.String RegulamentoPerdasPatrimoniais
			{
				get
				{
					System.String data = entity.RegulamentoPerdasPatrimoniais;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RegulamentoPerdasPatrimoniais = null;
					else entity.RegulamentoPerdasPatrimoniais = Convert.ToString(value);
				}
			}
				
			public System.String RegulamentoPatrimonioNegativo
			{
				get
				{
					System.String data = entity.RegulamentoPatrimonioNegativo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RegulamentoPatrimonioNegativo = null;
					else entity.RegulamentoPatrimonioNegativo = Convert.ToString(value);
				}
			}
				
			public System.String ExisteCarencia
			{
				get
				{
					System.String data = entity.ExisteCarencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExisteCarencia = null;
					else entity.ExisteCarencia = Convert.ToString(value);
				}
			}
				
			public System.String ExisteTaxaEntrada
			{
				get
				{
					System.String data = entity.ExisteTaxaEntrada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExisteTaxaEntrada = null;
					else entity.ExisteTaxaEntrada = Convert.ToString(value);
				}
			}
				
			public System.String ExisteTaxaSaida
			{
				get
				{
					System.String data = entity.ExisteTaxaSaida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExisteTaxaSaida = null;
					else entity.ExisteTaxaSaida = Convert.ToString(value);
				}
			}
				
			public System.String LimiteConcentracaoEmissor
			{
				get
				{
					System.Decimal? data = entity.LimiteConcentracaoEmissor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LimiteConcentracaoEmissor = null;
					else entity.LimiteConcentracaoEmissor = Convert.ToDecimal(value);
				}
			}
			

			private esLamina entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esLaminaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esLamina can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Lamina : esLamina
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_Lamina_Carteira
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esLaminaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return LaminaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem InicioVigencia
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.InicioVigencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem EnderecoEletronico
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.EnderecoEletronico, esSystemType.String);
			}
		} 
		
		public esQueryItem DescricaoPublicoAlvo
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.DescricaoPublicoAlvo, esSystemType.String);
			}
		} 
		
		public esQueryItem ObjetivoFundo
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.ObjetivoFundo, esSystemType.String);
			}
		} 
		
		public esQueryItem DescricaoPoliticaInvestimento
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.DescricaoPoliticaInvestimento, esSystemType.String);
			}
		} 
		
		public esQueryItem LimiteAplicacaoExterior
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.LimiteAplicacaoExterior, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem LimiteCreditoPrivado
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.LimiteCreditoPrivado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DerivativosProtecaoCarteira
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.DerivativosProtecaoCarteira, esSystemType.String);
			}
		} 
		
		public esQueryItem LimiteAlavancagem
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.LimiteAlavancagem, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrazoCarencia
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.PrazoCarencia, esSystemType.String);
			}
		} 
		
		public esQueryItem TaxaEntrada
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.TaxaEntrada, esSystemType.String);
			}
		} 
		
		public esQueryItem TaxaSaida
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.TaxaSaida, esSystemType.String);
			}
		} 
		
		public esQueryItem Risco
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.Risco, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FundoEstruturado
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.FundoEstruturado, esSystemType.String);
			}
		} 
		
		public esQueryItem CenariosApuracaoRentabilidade
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.CenariosApuracaoRentabilidade, esSystemType.String);
			}
		} 
		
		public esQueryItem DesempenhoFundo
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.DesempenhoFundo, esSystemType.String);
			}
		} 
		
		public esQueryItem PoliticaDistribuicao
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.PoliticaDistribuicao, esSystemType.String);
			}
		} 
		
		public esQueryItem Telefone
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.Telefone, esSystemType.String);
			}
		} 
		
		public esQueryItem UrlAtendimento
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.UrlAtendimento, esSystemType.String);
			}
		} 
		
		public esQueryItem Reclamacoes
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.Reclamacoes, esSystemType.String);
			}
		} 
		
		public esQueryItem FundosInvestimento
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.FundosInvestimento, esSystemType.String);
			}
		} 
		
		public esQueryItem FundosInvestimentoCotas
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.FundosInvestimentoCotas, esSystemType.String);
			}
		} 
		
		public esQueryItem EstrategiaPerdas
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.EstrategiaPerdas, esSystemType.String);
			}
		} 
		
		public esQueryItem RegulamentoPerdasPatrimoniais
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.RegulamentoPerdasPatrimoniais, esSystemType.String);
			}
		} 
		
		public esQueryItem RegulamentoPatrimonioNegativo
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.RegulamentoPatrimonioNegativo, esSystemType.String);
			}
		} 
		
		public esQueryItem ExisteCarencia
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.ExisteCarencia, esSystemType.String);
			}
		} 
		
		public esQueryItem ExisteTaxaEntrada
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.ExisteTaxaEntrada, esSystemType.String);
			}
		} 
		
		public esQueryItem ExisteTaxaSaida
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.ExisteTaxaSaida, esSystemType.String);
			}
		} 
		
		public esQueryItem LimiteConcentracaoEmissor
		{
			get
			{
				return new esQueryItem(this, LaminaMetadata.ColumnNames.LimiteConcentracaoEmissor, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("LaminaCollection")]
	public partial class LaminaCollection : esLaminaCollection, IEnumerable<Lamina>
	{
		public LaminaCollection()
		{

		}
		
		public static implicit operator List<Lamina>(LaminaCollection coll)
		{
			List<Lamina> list = new List<Lamina>();
			
			foreach (Lamina emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  LaminaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LaminaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Lamina(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Lamina();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public LaminaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LaminaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(LaminaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Lamina AddNew()
		{
			Lamina entity = base.AddNewEntity() as Lamina;
			
			return entity;
		}

		public Lamina FindByPrimaryKey(System.Int32 idCarteira, System.DateTime inicioVigencia)
		{
			return base.FindByPrimaryKey(idCarteira, inicioVigencia) as Lamina;
		}


		#region IEnumerable<Lamina> Members

		IEnumerator<Lamina> IEnumerable<Lamina>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Lamina;
			}
		}

		#endregion
		
		private LaminaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Lamina' table
	/// </summary>

	[Serializable]
	public partial class Lamina : esLamina
	{
		public Lamina()
		{

		}
	
		public Lamina(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return LaminaMetadata.Meta();
			}
		}
		
		
		
		override protected esLaminaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LaminaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public LaminaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LaminaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(LaminaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private LaminaQuery query;
	}



	[Serializable]
	public partial class LaminaQuery : esLaminaQuery
	{
		public LaminaQuery()
		{

		}		
		
		public LaminaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class LaminaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected LaminaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(LaminaMetadata.ColumnNames.IdCarteira, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LaminaMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.InicioVigencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LaminaMetadata.PropertyNames.InicioVigencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.EnderecoEletronico, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.EnderecoEletronico;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.DescricaoPublicoAlvo, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.DescricaoPublicoAlvo;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.ObjetivoFundo, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.ObjetivoFundo;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.DescricaoPoliticaInvestimento, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.DescricaoPoliticaInvestimento;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.LimiteAplicacaoExterior, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LaminaMetadata.PropertyNames.LimiteAplicacaoExterior;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.LimiteCreditoPrivado, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LaminaMetadata.PropertyNames.LimiteCreditoPrivado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.DerivativosProtecaoCarteira, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.DerivativosProtecaoCarteira;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.LimiteAlavancagem, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LaminaMetadata.PropertyNames.LimiteAlavancagem;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.PrazoCarencia, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.PrazoCarencia;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.TaxaEntrada, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.TaxaEntrada;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.TaxaSaida, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.TaxaSaida;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.Risco, 13, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LaminaMetadata.PropertyNames.Risco;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.FundoEstruturado, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.FundoEstruturado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.CenariosApuracaoRentabilidade, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.CenariosApuracaoRentabilidade;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.DesempenhoFundo, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.DesempenhoFundo;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.PoliticaDistribuicao, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.PoliticaDistribuicao;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.Telefone, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.Telefone;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.UrlAtendimento, 19, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.UrlAtendimento;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.Reclamacoes, 20, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.Reclamacoes;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.FundosInvestimento, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.FundosInvestimento;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.FundosInvestimentoCotas, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.FundosInvestimentoCotas;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.EstrategiaPerdas, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.EstrategiaPerdas;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.RegulamentoPerdasPatrimoniais, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.RegulamentoPerdasPatrimoniais;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.RegulamentoPatrimonioNegativo, 25, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.RegulamentoPatrimonioNegativo;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.ExisteCarencia, 26, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.ExisteCarencia;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.ExisteTaxaEntrada, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.ExisteTaxaEntrada;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.ExisteTaxaSaida, 28, typeof(System.String), esSystemType.String);
			c.PropertyName = LaminaMetadata.PropertyNames.ExisteTaxaSaida;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LaminaMetadata.ColumnNames.LimiteConcentracaoEmissor, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LaminaMetadata.PropertyNames.LimiteConcentracaoEmissor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public LaminaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string InicioVigencia = "InicioVigencia";
			 public const string EnderecoEletronico = "EnderecoEletronico";
			 public const string DescricaoPublicoAlvo = "DescricaoPublicoAlvo";
			 public const string ObjetivoFundo = "ObjetivoFundo";
			 public const string DescricaoPoliticaInvestimento = "DescricaoPoliticaInvestimento";
			 public const string LimiteAplicacaoExterior = "LimiteAplicacaoExterior";
			 public const string LimiteCreditoPrivado = "LimiteCreditoPrivado";
			 public const string DerivativosProtecaoCarteira = "DerivativosProtecaoCarteira";
			 public const string LimiteAlavancagem = "LimiteAlavancagem";
			 public const string PrazoCarencia = "PrazoCarencia";
			 public const string TaxaEntrada = "TaxaEntrada";
			 public const string TaxaSaida = "TaxaSaida";
			 public const string Risco = "Risco";
			 public const string FundoEstruturado = "FundoEstruturado";
			 public const string CenariosApuracaoRentabilidade = "CenariosApuracaoRentabilidade";
			 public const string DesempenhoFundo = "DesempenhoFundo";
			 public const string PoliticaDistribuicao = "PoliticaDistribuicao";
			 public const string Telefone = "Telefone";
			 public const string UrlAtendimento = "UrlAtendimento";
			 public const string Reclamacoes = "Reclamacoes";
			 public const string FundosInvestimento = "FundosInvestimento";
			 public const string FundosInvestimentoCotas = "FundosInvestimentoCotas";
			 public const string EstrategiaPerdas = "EstrategiaPerdas";
			 public const string RegulamentoPerdasPatrimoniais = "RegulamentoPerdasPatrimoniais";
			 public const string RegulamentoPatrimonioNegativo = "RegulamentoPatrimonioNegativo";
			 public const string ExisteCarencia = "ExisteCarencia";
			 public const string ExisteTaxaEntrada = "ExisteTaxaEntrada";
			 public const string ExisteTaxaSaida = "ExisteTaxaSaida";
			 public const string LimiteConcentracaoEmissor = "LimiteConcentracaoEmissor";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string InicioVigencia = "InicioVigencia";
			 public const string EnderecoEletronico = "EnderecoEletronico";
			 public const string DescricaoPublicoAlvo = "DescricaoPublicoAlvo";
			 public const string ObjetivoFundo = "ObjetivoFundo";
			 public const string DescricaoPoliticaInvestimento = "DescricaoPoliticaInvestimento";
			 public const string LimiteAplicacaoExterior = "LimiteAplicacaoExterior";
			 public const string LimiteCreditoPrivado = "LimiteCreditoPrivado";
			 public const string DerivativosProtecaoCarteira = "DerivativosProtecaoCarteira";
			 public const string LimiteAlavancagem = "LimiteAlavancagem";
			 public const string PrazoCarencia = "PrazoCarencia";
			 public const string TaxaEntrada = "TaxaEntrada";
			 public const string TaxaSaida = "TaxaSaida";
			 public const string Risco = "Risco";
			 public const string FundoEstruturado = "FundoEstruturado";
			 public const string CenariosApuracaoRentabilidade = "CenariosApuracaoRentabilidade";
			 public const string DesempenhoFundo = "DesempenhoFundo";
			 public const string PoliticaDistribuicao = "PoliticaDistribuicao";
			 public const string Telefone = "Telefone";
			 public const string UrlAtendimento = "UrlAtendimento";
			 public const string Reclamacoes = "Reclamacoes";
			 public const string FundosInvestimento = "FundosInvestimento";
			 public const string FundosInvestimentoCotas = "FundosInvestimentoCotas";
			 public const string EstrategiaPerdas = "EstrategiaPerdas";
			 public const string RegulamentoPerdasPatrimoniais = "RegulamentoPerdasPatrimoniais";
			 public const string RegulamentoPatrimonioNegativo = "RegulamentoPatrimonioNegativo";
			 public const string ExisteCarencia = "ExisteCarencia";
			 public const string ExisteTaxaEntrada = "ExisteTaxaEntrada";
			 public const string ExisteTaxaSaida = "ExisteTaxaSaida";
			 public const string LimiteConcentracaoEmissor = "LimiteConcentracaoEmissor";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(LaminaMetadata))
			{
				if(LaminaMetadata.mapDelegates == null)
				{
					LaminaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (LaminaMetadata.meta == null)
				{
					LaminaMetadata.meta = new LaminaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("InicioVigencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("EnderecoEletronico", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DescricaoPublicoAlvo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ObjetivoFundo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DescricaoPoliticaInvestimento", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("LimiteAplicacaoExterior", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("LimiteCreditoPrivado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DerivativosProtecaoCarteira", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("LimiteAlavancagem", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrazoCarencia", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TaxaEntrada", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TaxaSaida", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Risco", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FundoEstruturado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("CenariosApuracaoRentabilidade", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DesempenhoFundo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PoliticaDistribuicao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Telefone", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("UrlAtendimento", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Reclamacoes", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FundosInvestimento", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FundosInvestimentoCotas", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("EstrategiaPerdas", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("RegulamentoPerdasPatrimoniais", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("RegulamentoPatrimonioNegativo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ExisteCarencia", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ExisteTaxaEntrada", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ExisteTaxaSaida", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("LimiteConcentracaoEmissor", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "Lamina";
				meta.Destination = "Lamina";
				
				meta.spInsert = "proc_LaminaInsert";				
				meta.spUpdate = "proc_LaminaUpdate";		
				meta.spDelete = "proc_LaminaDelete";
				meta.spLoadAll = "proc_LaminaLoadAll";
				meta.spLoadByPrimaryKey = "proc_LaminaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private LaminaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
