/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/01/2016 14:34:34
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		





		
using Financial.Contabil;



		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTabelaProvisaoCollection : esEntityCollection
	{
		public esTabelaProvisaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaProvisaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaProvisaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaProvisaoQuery);
		}
		#endregion
		
		virtual public TabelaProvisao DetachEntity(TabelaProvisao entity)
		{
			return base.DetachEntity(entity) as TabelaProvisao;
		}
		
		virtual public TabelaProvisao AttachEntity(TabelaProvisao entity)
		{
			return base.AttachEntity(entity) as TabelaProvisao;
		}
		
		virtual public void Combine(TabelaProvisaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaProvisao this[int index]
		{
			get
			{
				return base[index] as TabelaProvisao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaProvisao);
		}
	}



	[Serializable]
	abstract public class esTabelaProvisao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaProvisaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaProvisao()
		{

		}

		public esTabelaProvisao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabela)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTabela)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaProvisaoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTabela == idTabela);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabela)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabela)
		{
			esTabelaProvisaoQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabela == idTabela);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabela)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabela",idTabela);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "TipoCalculo": this.str.TipoCalculo = (string)value; break;							
						case "ValorTotal": this.str.ValorTotal = (string)value; break;							
						case "ContagemDias": this.str.ContagemDias = (string)value; break;							
						case "DiaRenovacao": this.str.DiaRenovacao = (string)value; break;							
						case "NumeroMesesRenovacao": this.str.NumeroMesesRenovacao = (string)value; break;							
						case "NumeroDiasPagamento": this.str.NumeroDiasPagamento = (string)value; break;							
						case "IdCadastro": this.str.IdCadastro = (string)value; break;							
						case "DataFim": this.str.DataFim = (string)value; break;							
						case "IdEventoProvisao": this.str.IdEventoProvisao = (string)value; break;							
						case "IdEventoPagamento": this.str.IdEventoPagamento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "TipoCalculo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCalculo = (System.Byte?)value;
							break;
						
						case "ValorTotal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorTotal = (System.Decimal?)value;
							break;
						
						case "ContagemDias":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContagemDias = (System.Byte?)value;
							break;
						
						case "DiaRenovacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.DiaRenovacao = (System.Byte?)value;
							break;
						
						case "NumeroMesesRenovacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.NumeroMesesRenovacao = (System.Byte?)value;
							break;
						
						case "NumeroDiasPagamento":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.NumeroDiasPagamento = (System.Int16?)value;
							break;
						
						case "IdCadastro":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCadastro = (System.Int32?)value;
							break;
						
						case "DataFim":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFim = (System.DateTime?)value;
							break;
						
						case "IdEventoProvisao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoProvisao = (System.Int32?)value;
							break;
						
						case "IdEventoPagamento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoPagamento = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaProvisao.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(TabelaProvisaoMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				base.SetSystemInt32(TabelaProvisaoMetadata.ColumnNames.IdTabela, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaProvisao.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaProvisaoMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaProvisaoMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaProvisao.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(TabelaProvisaoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(TabelaProvisaoMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaProvisao.TipoCalculo
		/// </summary>
		virtual public System.Byte? TipoCalculo
		{
			get
			{
				return base.GetSystemByte(TabelaProvisaoMetadata.ColumnNames.TipoCalculo);
			}
			
			set
			{
				base.SetSystemByte(TabelaProvisaoMetadata.ColumnNames.TipoCalculo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaProvisao.ValorTotal
		/// </summary>
		virtual public System.Decimal? ValorTotal
		{
			get
			{
				return base.GetSystemDecimal(TabelaProvisaoMetadata.ColumnNames.ValorTotal);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaProvisaoMetadata.ColumnNames.ValorTotal, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaProvisao.ContagemDias
		/// </summary>
		virtual public System.Byte? ContagemDias
		{
			get
			{
				return base.GetSystemByte(TabelaProvisaoMetadata.ColumnNames.ContagemDias);
			}
			
			set
			{
				base.SetSystemByte(TabelaProvisaoMetadata.ColumnNames.ContagemDias, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaProvisao.DiaRenovacao
		/// </summary>
		virtual public System.Byte? DiaRenovacao
		{
			get
			{
				return base.GetSystemByte(TabelaProvisaoMetadata.ColumnNames.DiaRenovacao);
			}
			
			set
			{
				base.SetSystemByte(TabelaProvisaoMetadata.ColumnNames.DiaRenovacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaProvisao.NumeroMesesRenovacao
		/// </summary>
		virtual public System.Byte? NumeroMesesRenovacao
		{
			get
			{
				return base.GetSystemByte(TabelaProvisaoMetadata.ColumnNames.NumeroMesesRenovacao);
			}
			
			set
			{
				base.SetSystemByte(TabelaProvisaoMetadata.ColumnNames.NumeroMesesRenovacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaProvisao.NumeroDiasPagamento
		/// </summary>
		virtual public System.Int16? NumeroDiasPagamento
		{
			get
			{
				return base.GetSystemInt16(TabelaProvisaoMetadata.ColumnNames.NumeroDiasPagamento);
			}
			
			set
			{
				base.SetSystemInt16(TabelaProvisaoMetadata.ColumnNames.NumeroDiasPagamento, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaProvisao.IdCadastro
		/// </summary>
		virtual public System.Int32? IdCadastro
		{
			get
			{
				return base.GetSystemInt32(TabelaProvisaoMetadata.ColumnNames.IdCadastro);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaProvisaoMetadata.ColumnNames.IdCadastro, value))
				{
					this._UpToCadastroProvisaoByIdCadastro = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaProvisao.DataFim
		/// </summary>
		virtual public System.DateTime? DataFim
		{
			get
			{
				return base.GetSystemDateTime(TabelaProvisaoMetadata.ColumnNames.DataFim);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaProvisaoMetadata.ColumnNames.DataFim, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaProvisao.IdEventoProvisao
		/// </summary>
		virtual public System.Int32? IdEventoProvisao
		{
			get
			{
				return base.GetSystemInt32(TabelaProvisaoMetadata.ColumnNames.IdEventoProvisao);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaProvisaoMetadata.ColumnNames.IdEventoProvisao, value))
				{
					this._UpToContabRoteiroByIdEventoProvisao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaProvisao.IdEventoPagamento
		/// </summary>
		virtual public System.Int32? IdEventoPagamento
		{
			get
			{
				return base.GetSystemInt32(TabelaProvisaoMetadata.ColumnNames.IdEventoPagamento);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaProvisaoMetadata.ColumnNames.IdEventoPagamento, value))
				{
					this._UpToContabRoteiroByIdEventoPagamento = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected CadastroProvisao _UpToCadastroProvisaoByIdCadastro;
		[CLSCompliant(false)]
		internal protected ContabRoteiro _UpToContabRoteiroByIdEventoProvisao;
		[CLSCompliant(false)]
		internal protected ContabRoteiro _UpToContabRoteiroByIdEventoPagamento;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaProvisao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoCalculo
			{
				get
				{
					System.Byte? data = entity.TipoCalculo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCalculo = null;
					else entity.TipoCalculo = Convert.ToByte(value);
				}
			}
				
			public System.String ValorTotal
			{
				get
				{
					System.Decimal? data = entity.ValorTotal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorTotal = null;
					else entity.ValorTotal = Convert.ToDecimal(value);
				}
			}
				
			public System.String ContagemDias
			{
				get
				{
					System.Byte? data = entity.ContagemDias;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContagemDias = null;
					else entity.ContagemDias = Convert.ToByte(value);
				}
			}
				
			public System.String DiaRenovacao
			{
				get
				{
					System.Byte? data = entity.DiaRenovacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiaRenovacao = null;
					else entity.DiaRenovacao = Convert.ToByte(value);
				}
			}
				
			public System.String NumeroMesesRenovacao
			{
				get
				{
					System.Byte? data = entity.NumeroMesesRenovacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroMesesRenovacao = null;
					else entity.NumeroMesesRenovacao = Convert.ToByte(value);
				}
			}
				
			public System.String NumeroDiasPagamento
			{
				get
				{
					System.Int16? data = entity.NumeroDiasPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroDiasPagamento = null;
					else entity.NumeroDiasPagamento = Convert.ToInt16(value);
				}
			}
				
			public System.String IdCadastro
			{
				get
				{
					System.Int32? data = entity.IdCadastro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCadastro = null;
					else entity.IdCadastro = Convert.ToInt32(value);
				}
			}
				
			public System.String DataFim
			{
				get
				{
					System.DateTime? data = entity.DataFim;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFim = null;
					else entity.DataFim = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdEventoProvisao
			{
				get
				{
					System.Int32? data = entity.IdEventoProvisao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoProvisao = null;
					else entity.IdEventoProvisao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEventoPagamento
			{
				get
				{
					System.Int32? data = entity.IdEventoPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoPagamento = null;
					else entity.IdEventoPagamento = Convert.ToInt32(value);
				}
			}
			

			private esTabelaProvisao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaProvisaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaProvisao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaProvisao : esTabelaProvisao
	{

				
		#region CalculoProvisao - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - TabelaProvisao_CalculoProvisao_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoProvisao CalculoProvisao
		{
			get
			{
				if(this._CalculoProvisao == null)
				{
					this._CalculoProvisao = new CalculoProvisao();
					this._CalculoProvisao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("CalculoProvisao", this._CalculoProvisao);
				
					if(this.IdTabela != null)
					{
						this._CalculoProvisao.Query.Where(this._CalculoProvisao.Query.IdTabela == this.IdTabela);
						this._CalculoProvisao.Query.Load();
					}
				}

				return this._CalculoProvisao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoProvisao != null) 
				{ 
					this.RemovePostOneSave("CalculoProvisao"); 
					this._CalculoProvisao = null;
					
				} 
			}          			
		}

		private CalculoProvisao _CalculoProvisao;
		#endregion

				
		#region CalculoProvisaoHistoricoCollectionByIdTabela - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TabelaProvisao_CalculoProvisaoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoProvisaoHistoricoCollection CalculoProvisaoHistoricoCollectionByIdTabela
		{
			get
			{
				if(this._CalculoProvisaoHistoricoCollectionByIdTabela == null)
				{
					this._CalculoProvisaoHistoricoCollectionByIdTabela = new CalculoProvisaoHistoricoCollection();
					this._CalculoProvisaoHistoricoCollectionByIdTabela.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoProvisaoHistoricoCollectionByIdTabela", this._CalculoProvisaoHistoricoCollectionByIdTabela);
				
					if(this.IdTabela != null)
					{
						this._CalculoProvisaoHistoricoCollectionByIdTabela.Query.Where(this._CalculoProvisaoHistoricoCollectionByIdTabela.Query.IdTabela == this.IdTabela);
						this._CalculoProvisaoHistoricoCollectionByIdTabela.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoProvisaoHistoricoCollectionByIdTabela.fks.Add(CalculoProvisaoHistoricoMetadata.ColumnNames.IdTabela, this.IdTabela);
					}
				}

				return this._CalculoProvisaoHistoricoCollectionByIdTabela;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoProvisaoHistoricoCollectionByIdTabela != null) 
				{ 
					this.RemovePostSave("CalculoProvisaoHistoricoCollectionByIdTabela"); 
					this._CalculoProvisaoHistoricoCollectionByIdTabela = null;
					
				} 
			} 			
		}

		private CalculoProvisaoHistoricoCollection _CalculoProvisaoHistoricoCollectionByIdTabela;
		#endregion

				
		#region UpToCadastroProvisaoByIdCadastro - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - CadastroProvisao_TabelaProvisao_FK1
		/// </summary>

		[XmlIgnore]
		public CadastroProvisao UpToCadastroProvisaoByIdCadastro
		{
			get
			{
				if(this._UpToCadastroProvisaoByIdCadastro == null
					&& IdCadastro != null					)
				{
					this._UpToCadastroProvisaoByIdCadastro = new CadastroProvisao();
					this._UpToCadastroProvisaoByIdCadastro.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCadastroProvisaoByIdCadastro", this._UpToCadastroProvisaoByIdCadastro);
					this._UpToCadastroProvisaoByIdCadastro.Query.Where(this._UpToCadastroProvisaoByIdCadastro.Query.IdCadastro == this.IdCadastro);
					this._UpToCadastroProvisaoByIdCadastro.Query.Load();
				}

				return this._UpToCadastroProvisaoByIdCadastro;
			}
			
			set
			{
				this.RemovePreSave("UpToCadastroProvisaoByIdCadastro");
				

				if(value == null)
				{
					this.IdCadastro = null;
					this._UpToCadastroProvisaoByIdCadastro = null;
				}
				else
				{
					this.IdCadastro = value.IdCadastro;
					this._UpToCadastroProvisaoByIdCadastro = value;
					this.SetPreSave("UpToCadastroProvisaoByIdCadastro", this._UpToCadastroProvisaoByIdCadastro);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContabRoteiroByIdEventoProvisao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabRoteiro_TabelaProvisao_FK1
		/// </summary>

		[XmlIgnore]
		public ContabRoteiro UpToContabRoteiroByIdEventoProvisao
		{
			get
			{
				if(this._UpToContabRoteiroByIdEventoProvisao == null
					&& IdEventoProvisao != null					)
				{
					this._UpToContabRoteiroByIdEventoProvisao = new ContabRoteiro();
					this._UpToContabRoteiroByIdEventoProvisao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabRoteiroByIdEventoProvisao", this._UpToContabRoteiroByIdEventoProvisao);
					this._UpToContabRoteiroByIdEventoProvisao.Query.Where(this._UpToContabRoteiroByIdEventoProvisao.Query.IdEvento == this.IdEventoProvisao);
					this._UpToContabRoteiroByIdEventoProvisao.Query.Load();
				}

				return this._UpToContabRoteiroByIdEventoProvisao;
			}
			
			set
			{
				this.RemovePreSave("UpToContabRoteiroByIdEventoProvisao");
				

				if(value == null)
				{
					this.IdEventoProvisao = null;
					this._UpToContabRoteiroByIdEventoProvisao = null;
				}
				else
				{
					this.IdEventoProvisao = value.IdEvento;
					this._UpToContabRoteiroByIdEventoProvisao = value;
					this.SetPreSave("UpToContabRoteiroByIdEventoProvisao", this._UpToContabRoteiroByIdEventoProvisao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContabRoteiroByIdEventoPagamento - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabRoteiro_TabelaProvisao_FK2
		/// </summary>

		[XmlIgnore]
		public ContabRoteiro UpToContabRoteiroByIdEventoPagamento
		{
			get
			{
				if(this._UpToContabRoteiroByIdEventoPagamento == null
					&& IdEventoPagamento != null					)
				{
					this._UpToContabRoteiroByIdEventoPagamento = new ContabRoteiro();
					this._UpToContabRoteiroByIdEventoPagamento.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabRoteiroByIdEventoPagamento", this._UpToContabRoteiroByIdEventoPagamento);
					this._UpToContabRoteiroByIdEventoPagamento.Query.Where(this._UpToContabRoteiroByIdEventoPagamento.Query.IdEvento == this.IdEventoPagamento);
					this._UpToContabRoteiroByIdEventoPagamento.Query.Load();
				}

				return this._UpToContabRoteiroByIdEventoPagamento;
			}
			
			set
			{
				this.RemovePreSave("UpToContabRoteiroByIdEventoPagamento");
				

				if(value == null)
				{
					this.IdEventoPagamento = null;
					this._UpToContabRoteiroByIdEventoPagamento = null;
				}
				else
				{
					this.IdEventoPagamento = value.IdEvento;
					this._UpToContabRoteiroByIdEventoPagamento = value;
					this.SetPreSave("UpToContabRoteiroByIdEventoPagamento", this._UpToContabRoteiroByIdEventoPagamento);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "CalculoProvisaoHistoricoCollectionByIdTabela", typeof(CalculoProvisaoHistoricoCollection), new CalculoProvisaoHistorico()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToCadastroProvisaoByIdCadastro != null)
			{
				this.IdCadastro = this._UpToCadastroProvisaoByIdCadastro.IdCadastro;
			}
			if(!this.es.IsDeleted && this._UpToContabRoteiroByIdEventoProvisao != null)
			{
				this.IdEventoProvisao = this._UpToContabRoteiroByIdEventoProvisao.IdEvento;
			}
			if(!this.es.IsDeleted && this._UpToContabRoteiroByIdEventoPagamento != null)
			{
				this.IdEventoPagamento = this._UpToContabRoteiroByIdEventoPagamento.IdEvento;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._CalculoProvisaoHistoricoCollectionByIdTabela != null)
			{
				foreach(CalculoProvisaoHistorico obj in this._CalculoProvisaoHistoricoCollectionByIdTabela)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTabela = this.IdTabela;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
			if(this._CalculoProvisao != null)
			{
				if(this._CalculoProvisao.es.IsAdded)
				{
					this._CalculoProvisao.IdTabela = this.IdTabela;
				}
			}
		}
		
	}



	[Serializable]
	abstract public class esTabelaProvisaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaProvisaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, TabelaProvisaoMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaProvisaoMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TabelaProvisaoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoCalculo
		{
			get
			{
				return new esQueryItem(this, TabelaProvisaoMetadata.ColumnNames.TipoCalculo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ValorTotal
		{
			get
			{
				return new esQueryItem(this, TabelaProvisaoMetadata.ColumnNames.ValorTotal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ContagemDias
		{
			get
			{
				return new esQueryItem(this, TabelaProvisaoMetadata.ColumnNames.ContagemDias, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DiaRenovacao
		{
			get
			{
				return new esQueryItem(this, TabelaProvisaoMetadata.ColumnNames.DiaRenovacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem NumeroMesesRenovacao
		{
			get
			{
				return new esQueryItem(this, TabelaProvisaoMetadata.ColumnNames.NumeroMesesRenovacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem NumeroDiasPagamento
		{
			get
			{
				return new esQueryItem(this, TabelaProvisaoMetadata.ColumnNames.NumeroDiasPagamento, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdCadastro
		{
			get
			{
				return new esQueryItem(this, TabelaProvisaoMetadata.ColumnNames.IdCadastro, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataFim
		{
			get
			{
				return new esQueryItem(this, TabelaProvisaoMetadata.ColumnNames.DataFim, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdEventoProvisao
		{
			get
			{
				return new esQueryItem(this, TabelaProvisaoMetadata.ColumnNames.IdEventoProvisao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEventoPagamento
		{
			get
			{
				return new esQueryItem(this, TabelaProvisaoMetadata.ColumnNames.IdEventoPagamento, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaProvisaoCollection")]
	public partial class TabelaProvisaoCollection : esTabelaProvisaoCollection, IEnumerable<TabelaProvisao>
	{
		public TabelaProvisaoCollection()
		{

		}
		
		public static implicit operator List<TabelaProvisao>(TabelaProvisaoCollection coll)
		{
			List<TabelaProvisao> list = new List<TabelaProvisao>();
			
			foreach (TabelaProvisao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaProvisaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaProvisaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaProvisao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaProvisao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaProvisaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaProvisaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaProvisaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaProvisao AddNew()
		{
			TabelaProvisao entity = base.AddNewEntity() as TabelaProvisao;
			
			return entity;
		}

		public TabelaProvisao FindByPrimaryKey(System.Int32 idTabela)
		{
			return base.FindByPrimaryKey(idTabela) as TabelaProvisao;
		}


		#region IEnumerable<TabelaProvisao> Members

		IEnumerator<TabelaProvisao> IEnumerable<TabelaProvisao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaProvisao;
			}
		}

		#endregion
		
		private TabelaProvisaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaProvisao' table
	/// </summary>

	[Serializable]
	public partial class TabelaProvisao : esTabelaProvisao
	{
		public TabelaProvisao()
		{

		}
	
		public TabelaProvisao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaProvisaoMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaProvisaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaProvisaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaProvisaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaProvisaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaProvisaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaProvisaoQuery query;
	}



	[Serializable]
	public partial class TabelaProvisaoQuery : esTabelaProvisaoQuery
	{
		public TabelaProvisaoQuery()
		{

		}		
		
		public TabelaProvisaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaProvisaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaProvisaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaProvisaoMetadata.ColumnNames.IdTabela, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaProvisaoMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProvisaoMetadata.ColumnNames.DataReferencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaProvisaoMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProvisaoMetadata.ColumnNames.IdCarteira, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaProvisaoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProvisaoMetadata.ColumnNames.TipoCalculo, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaProvisaoMetadata.PropertyNames.TipoCalculo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProvisaoMetadata.ColumnNames.ValorTotal, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaProvisaoMetadata.PropertyNames.ValorTotal;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProvisaoMetadata.ColumnNames.ContagemDias, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaProvisaoMetadata.PropertyNames.ContagemDias;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProvisaoMetadata.ColumnNames.DiaRenovacao, 6, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaProvisaoMetadata.PropertyNames.DiaRenovacao;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProvisaoMetadata.ColumnNames.NumeroMesesRenovacao, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaProvisaoMetadata.PropertyNames.NumeroMesesRenovacao;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProvisaoMetadata.ColumnNames.NumeroDiasPagamento, 8, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TabelaProvisaoMetadata.PropertyNames.NumeroDiasPagamento;	
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProvisaoMetadata.ColumnNames.IdCadastro, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaProvisaoMetadata.PropertyNames.IdCadastro;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProvisaoMetadata.ColumnNames.DataFim, 10, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaProvisaoMetadata.PropertyNames.DataFim;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProvisaoMetadata.ColumnNames.IdEventoProvisao, 11, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaProvisaoMetadata.PropertyNames.IdEventoProvisao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaProvisaoMetadata.ColumnNames.IdEventoPagamento, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaProvisaoMetadata.PropertyNames.IdEventoPagamento;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaProvisaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCarteira = "IdCarteira";
			 public const string TipoCalculo = "TipoCalculo";
			 public const string ValorTotal = "ValorTotal";
			 public const string ContagemDias = "ContagemDias";
			 public const string DiaRenovacao = "DiaRenovacao";
			 public const string NumeroMesesRenovacao = "NumeroMesesRenovacao";
			 public const string NumeroDiasPagamento = "NumeroDiasPagamento";
			 public const string IdCadastro = "IdCadastro";
			 public const string DataFim = "DataFim";
			 public const string IdEventoProvisao = "IdEventoProvisao";
			 public const string IdEventoPagamento = "IdEventoPagamento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCarteira = "IdCarteira";
			 public const string TipoCalculo = "TipoCalculo";
			 public const string ValorTotal = "ValorTotal";
			 public const string ContagemDias = "ContagemDias";
			 public const string DiaRenovacao = "DiaRenovacao";
			 public const string NumeroMesesRenovacao = "NumeroMesesRenovacao";
			 public const string NumeroDiasPagamento = "NumeroDiasPagamento";
			 public const string IdCadastro = "IdCadastro";
			 public const string DataFim = "DataFim";
			 public const string IdEventoProvisao = "IdEventoProvisao";
			 public const string IdEventoPagamento = "IdEventoPagamento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaProvisaoMetadata))
			{
				if(TabelaProvisaoMetadata.mapDelegates == null)
				{
					TabelaProvisaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaProvisaoMetadata.meta == null)
				{
					TabelaProvisaoMetadata.meta = new TabelaProvisaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoCalculo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ValorTotal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ContagemDias", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DiaRenovacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("NumeroMesesRenovacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("NumeroDiasPagamento", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdCadastro", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataFim", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdEventoProvisao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEventoPagamento", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "TabelaProvisao";
				meta.Destination = "TabelaProvisao";
				
				meta.spInsert = "proc_TabelaProvisaoInsert";				
				meta.spUpdate = "proc_TabelaProvisaoUpdate";		
				meta.spDelete = "proc_TabelaProvisaoDelete";
				meta.spLoadAll = "proc_TabelaProvisaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaProvisaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaProvisaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
