/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 17/12/2014 10:22:34
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esEventoFundoSwapCollection : esEntityCollection
	{
		public esEventoFundoSwapCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EventoFundoSwapCollection";
		}

		#region Query Logic
		protected void InitQuery(esEventoFundoSwapQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEventoFundoSwapQuery);
		}
		#endregion
		
		virtual public EventoFundoSwap DetachEntity(EventoFundoSwap entity)
		{
			return base.DetachEntity(entity) as EventoFundoSwap;
		}
		
		virtual public EventoFundoSwap AttachEntity(EventoFundoSwap entity)
		{
			return base.AttachEntity(entity) as EventoFundoSwap;
		}
		
		virtual public void Combine(EventoFundoSwapCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EventoFundoSwap this[int index]
		{
			get
			{
				return base[index] as EventoFundoSwap;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EventoFundoSwap);
		}
	}



	[Serializable]
	abstract public class esEventoFundoSwap : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEventoFundoSwapQuery GetDynamicQuery()
		{
			return null;
		}

		public esEventoFundoSwap()
		{

		}

		public esEventoFundoSwap(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataEmissao, System.DateTime dataVencimento, System.Int32 idEventoFundo, System.String numeroContrato, System.Int32 tipoMercado, System.Byte tipoPonta, System.Byte tipoPontaContraParte)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataEmissao, dataVencimento, idEventoFundo, numeroContrato, tipoMercado, tipoPonta, tipoPontaContraParte);
			else
				return LoadByPrimaryKeyStoredProcedure(dataEmissao, dataVencimento, idEventoFundo, numeroContrato, tipoMercado, tipoPonta, tipoPontaContraParte);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataEmissao, System.DateTime dataVencimento, System.Int32 idEventoFundo, System.String numeroContrato, System.Int32 tipoMercado, System.Byte tipoPonta, System.Byte tipoPontaContraParte)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataEmissao, dataVencimento, idEventoFundo, numeroContrato, tipoMercado, tipoPonta, tipoPontaContraParte);
			else
				return LoadByPrimaryKeyStoredProcedure(dataEmissao, dataVencimento, idEventoFundo, numeroContrato, tipoMercado, tipoPonta, tipoPontaContraParte);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataEmissao, System.DateTime dataVencimento, System.Int32 idEventoFundo, System.String numeroContrato, System.Int32 tipoMercado, System.Byte tipoPonta, System.Byte tipoPontaContraParte)
		{
			esEventoFundoSwapQuery query = this.GetDynamicQuery();
			query.Where(query.DataEmissao == dataEmissao, query.DataVencimento == dataVencimento, query.IdEventoFundo == idEventoFundo, query.NumeroContrato == numeroContrato, query.TipoMercado == tipoMercado, query.TipoPonta == tipoPonta, query.TipoPontaContraParte == tipoPontaContraParte);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataEmissao, System.DateTime dataVencimento, System.Int32 idEventoFundo, System.String numeroContrato, System.Int32 tipoMercado, System.Byte tipoPonta, System.Byte tipoPontaContraParte)
		{
			esParameters parms = new esParameters();
			parms.Add("DataEmissao",dataEmissao);			parms.Add("DataVencimento",dataVencimento);			parms.Add("IdEventoFundo",idEventoFundo);			parms.Add("NumeroContrato",numeroContrato);			parms.Add("TipoMercado",tipoMercado);			parms.Add("TipoPonta",tipoPonta);			parms.Add("TipoPontaContraParte",tipoPontaContraParte);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEventoFundo": this.str.IdEventoFundo = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "NumeroContrato": this.str.NumeroContrato = (string)value; break;							
						case "DataEmissao": this.str.DataEmissao = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "TipoPonta": this.str.TipoPonta = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;							
						case "TipoPontaContraParte": this.str.TipoPontaContraParte = (string)value; break;							
						case "IdIndiceContraParte": this.str.IdIndiceContraParte = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeOriginal": this.str.QuantidadeOriginal = (string)value; break;							
						case "Processar": this.str.Processar = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEventoFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoFundo = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoMercado = (System.Int32?)value;
							break;
						
						case "DataEmissao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEmissao = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "TipoPonta":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoPonta = (System.Byte?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
						
						case "TipoPontaContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoPontaContraParte = (System.Byte?)value;
							break;
						
						case "IdIndiceContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndiceContraParte = (System.Int16?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "QuantidadeOriginal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeOriginal = (System.Decimal?)value;
							break;
						
						case "Processar":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Processar = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EventoFundoSwap.IdEventoFundo
		/// </summary>
		virtual public System.Int32? IdEventoFundo
		{
			get
			{
				return base.GetSystemInt32(EventoFundoSwapMetadata.ColumnNames.IdEventoFundo);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoFundoSwapMetadata.ColumnNames.IdEventoFundo, value))
				{
					this._UpToEventoFundoByIdEventoFundo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoSwap.TipoMercado
		/// </summary>
		virtual public System.Int32? TipoMercado
		{
			get
			{
				return base.GetSystemInt32(EventoFundoSwapMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoSwapMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoSwap.NumeroContrato
		/// </summary>
		virtual public System.String NumeroContrato
		{
			get
			{
				return base.GetSystemString(EventoFundoSwapMetadata.ColumnNames.NumeroContrato);
			}
			
			set
			{
				base.SetSystemString(EventoFundoSwapMetadata.ColumnNames.NumeroContrato, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoSwap.DataEmissao
		/// </summary>
		virtual public System.DateTime? DataEmissao
		{
			get
			{
				return base.GetSystemDateTime(EventoFundoSwapMetadata.ColumnNames.DataEmissao);
			}
			
			set
			{
				base.SetSystemDateTime(EventoFundoSwapMetadata.ColumnNames.DataEmissao, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoSwap.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(EventoFundoSwapMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(EventoFundoSwapMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoSwap.TipoPonta
		/// </summary>
		virtual public System.Byte? TipoPonta
		{
			get
			{
				return base.GetSystemByte(EventoFundoSwapMetadata.ColumnNames.TipoPonta);
			}
			
			set
			{
				base.SetSystemByte(EventoFundoSwapMetadata.ColumnNames.TipoPonta, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoSwap.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(EventoFundoSwapMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				base.SetSystemInt16(EventoFundoSwapMetadata.ColumnNames.IdIndice, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoSwap.TipoPontaContraParte
		/// </summary>
		virtual public System.Byte? TipoPontaContraParte
		{
			get
			{
				return base.GetSystemByte(EventoFundoSwapMetadata.ColumnNames.TipoPontaContraParte);
			}
			
			set
			{
				base.SetSystemByte(EventoFundoSwapMetadata.ColumnNames.TipoPontaContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoSwap.IdIndiceContraParte
		/// </summary>
		virtual public System.Int16? IdIndiceContraParte
		{
			get
			{
				return base.GetSystemInt16(EventoFundoSwapMetadata.ColumnNames.IdIndiceContraParte);
			}
			
			set
			{
				base.SetSystemInt16(EventoFundoSwapMetadata.ColumnNames.IdIndiceContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoSwap.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(EventoFundoSwapMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(EventoFundoSwapMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoSwap.QuantidadeOriginal
		/// </summary>
		virtual public System.Decimal? QuantidadeOriginal
		{
			get
			{
				return base.GetSystemDecimal(EventoFundoSwapMetadata.ColumnNames.QuantidadeOriginal);
			}
			
			set
			{
				base.SetSystemDecimal(EventoFundoSwapMetadata.ColumnNames.QuantidadeOriginal, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoSwap.Processar
		/// </summary>
		virtual public System.Byte? Processar
		{
			get
			{
				return base.GetSystemByte(EventoFundoSwapMetadata.ColumnNames.Processar);
			}
			
			set
			{
				base.SetSystemByte(EventoFundoSwapMetadata.ColumnNames.Processar, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected EventoFundo _UpToEventoFundoByIdEventoFundo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEventoFundoSwap entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEventoFundo
			{
				get
				{
					System.Int32? data = entity.IdEventoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoFundo = null;
					else entity.IdEventoFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Int32? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToInt32(value);
				}
			}
				
			public System.String NumeroContrato
			{
				get
				{
					System.String data = entity.NumeroContrato;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroContrato = null;
					else entity.NumeroContrato = Convert.ToString(value);
				}
			}
				
			public System.String DataEmissao
			{
				get
				{
					System.DateTime? data = entity.DataEmissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEmissao = null;
					else entity.DataEmissao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoPonta
			{
				get
				{
					System.Byte? data = entity.TipoPonta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPonta = null;
					else entity.TipoPonta = Convert.ToByte(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String TipoPontaContraParte
			{
				get
				{
					System.Byte? data = entity.TipoPontaContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPontaContraParte = null;
					else entity.TipoPontaContraParte = Convert.ToByte(value);
				}
			}
				
			public System.String IdIndiceContraParte
			{
				get
				{
					System.Int16? data = entity.IdIndiceContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndiceContraParte = null;
					else entity.IdIndiceContraParte = Convert.ToInt16(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeOriginal
			{
				get
				{
					System.Decimal? data = entity.QuantidadeOriginal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeOriginal = null;
					else entity.QuantidadeOriginal = Convert.ToDecimal(value);
				}
			}
				
			public System.String Processar
			{
				get
				{
					System.Byte? data = entity.Processar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Processar = null;
					else entity.Processar = Convert.ToByte(value);
				}
			}
			

			private esEventoFundoSwap entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEventoFundoSwapQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEventoFundoSwap can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EventoFundoSwap : esEventoFundoSwap
	{

				
		#region UpToEventoFundoByIdEventoFundo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EventoFundoSwap_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundo UpToEventoFundoByIdEventoFundo
		{
			get
			{
				if(this._UpToEventoFundoByIdEventoFundo == null
					&& IdEventoFundo != null					)
				{
					this._UpToEventoFundoByIdEventoFundo = new EventoFundo();
					this._UpToEventoFundoByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Where(this._UpToEventoFundoByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Load();
				}

				return this._UpToEventoFundoByIdEventoFundo;
			}
			
			set
			{
				this.RemovePreSave("UpToEventoFundoByIdEventoFundo");
				

				if(value == null)
				{
					this.IdEventoFundo = null;
					this._UpToEventoFundoByIdEventoFundo = null;
				}
				else
				{
					this.IdEventoFundo = value.IdEventoFundo;
					this._UpToEventoFundoByIdEventoFundo = value;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEventoFundoByIdEventoFundo != null)
			{
				this.IdEventoFundo = this._UpToEventoFundoByIdEventoFundo.IdEventoFundo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEventoFundoSwapQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoSwapMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEventoFundo
		{
			get
			{
				return new esQueryItem(this, EventoFundoSwapMetadata.ColumnNames.IdEventoFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, EventoFundoSwapMetadata.ColumnNames.TipoMercado, esSystemType.Int32);
			}
		} 
		
		public esQueryItem NumeroContrato
		{
			get
			{
				return new esQueryItem(this, EventoFundoSwapMetadata.ColumnNames.NumeroContrato, esSystemType.String);
			}
		} 
		
		public esQueryItem DataEmissao
		{
			get
			{
				return new esQueryItem(this, EventoFundoSwapMetadata.ColumnNames.DataEmissao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, EventoFundoSwapMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoPonta
		{
			get
			{
				return new esQueryItem(this, EventoFundoSwapMetadata.ColumnNames.TipoPonta, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, EventoFundoSwapMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TipoPontaContraParte
		{
			get
			{
				return new esQueryItem(this, EventoFundoSwapMetadata.ColumnNames.TipoPontaContraParte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdIndiceContraParte
		{
			get
			{
				return new esQueryItem(this, EventoFundoSwapMetadata.ColumnNames.IdIndiceContraParte, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, EventoFundoSwapMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeOriginal
		{
			get
			{
				return new esQueryItem(this, EventoFundoSwapMetadata.ColumnNames.QuantidadeOriginal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Processar
		{
			get
			{
				return new esQueryItem(this, EventoFundoSwapMetadata.ColumnNames.Processar, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EventoFundoSwapCollection")]
	public partial class EventoFundoSwapCollection : esEventoFundoSwapCollection, IEnumerable<EventoFundoSwap>
	{
		public EventoFundoSwapCollection()
		{

		}
		
		public static implicit operator List<EventoFundoSwap>(EventoFundoSwapCollection coll)
		{
			List<EventoFundoSwap> list = new List<EventoFundoSwap>();
			
			foreach (EventoFundoSwap emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EventoFundoSwapMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoSwapQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EventoFundoSwap(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EventoFundoSwap();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EventoFundoSwapQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoSwapQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EventoFundoSwapQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EventoFundoSwap AddNew()
		{
			EventoFundoSwap entity = base.AddNewEntity() as EventoFundoSwap;
			
			return entity;
		}

		public EventoFundoSwap FindByPrimaryKey(System.DateTime dataEmissao, System.DateTime dataVencimento, System.Int32 idEventoFundo, System.String numeroContrato, System.Int32 tipoMercado, System.Byte tipoPonta, System.Byte tipoPontaContraParte)
		{
			return base.FindByPrimaryKey(dataEmissao, dataVencimento, idEventoFundo, numeroContrato, tipoMercado, tipoPonta, tipoPontaContraParte) as EventoFundoSwap;
		}


		#region IEnumerable<EventoFundoSwap> Members

		IEnumerator<EventoFundoSwap> IEnumerable<EventoFundoSwap>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EventoFundoSwap;
			}
		}

		#endregion
		
		private EventoFundoSwapQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EventoFundoSwap' table
	/// </summary>

	[Serializable]
	public partial class EventoFundoSwap : esEventoFundoSwap
	{
		public EventoFundoSwap()
		{

		}
	
		public EventoFundoSwap(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoSwapMetadata.Meta();
			}
		}
		
		
		
		override protected esEventoFundoSwapQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoSwapQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EventoFundoSwapQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoSwapQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EventoFundoSwapQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EventoFundoSwapQuery query;
	}



	[Serializable]
	public partial class EventoFundoSwapQuery : esEventoFundoSwapQuery
	{
		public EventoFundoSwapQuery()
		{

		}		
		
		public EventoFundoSwapQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EventoFundoSwapMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EventoFundoSwapMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EventoFundoSwapMetadata.ColumnNames.IdEventoFundo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoSwapMetadata.PropertyNames.IdEventoFundo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoSwapMetadata.ColumnNames.TipoMercado, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoSwapMetadata.PropertyNames.TipoMercado;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoSwapMetadata.ColumnNames.NumeroContrato, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = EventoFundoSwapMetadata.PropertyNames.NumeroContrato;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoSwapMetadata.ColumnNames.DataEmissao, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EventoFundoSwapMetadata.PropertyNames.DataEmissao;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoSwapMetadata.ColumnNames.DataVencimento, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EventoFundoSwapMetadata.PropertyNames.DataVencimento;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoSwapMetadata.ColumnNames.TipoPonta, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EventoFundoSwapMetadata.PropertyNames.TipoPonta;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoSwapMetadata.ColumnNames.IdIndice, 6, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = EventoFundoSwapMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoSwapMetadata.ColumnNames.TipoPontaContraParte, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EventoFundoSwapMetadata.PropertyNames.TipoPontaContraParte;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoSwapMetadata.ColumnNames.IdIndiceContraParte, 8, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = EventoFundoSwapMetadata.PropertyNames.IdIndiceContraParte;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoSwapMetadata.ColumnNames.Quantidade, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EventoFundoSwapMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoSwapMetadata.ColumnNames.QuantidadeOriginal, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EventoFundoSwapMetadata.PropertyNames.QuantidadeOriginal;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoSwapMetadata.ColumnNames.Processar, 11, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EventoFundoSwapMetadata.PropertyNames.Processar;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EventoFundoSwapMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string TipoMercado = "TipoMercado";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string DataEmissao = "DataEmissao";
			 public const string DataVencimento = "DataVencimento";
			 public const string TipoPonta = "TipoPonta";
			 public const string IdIndice = "IdIndice";
			 public const string TipoPontaContraParte = "TipoPontaContraParte";
			 public const string IdIndiceContraParte = "IdIndiceContraParte";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeOriginal = "QuantidadeOriginal";
			 public const string Processar = "Processar";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string TipoMercado = "TipoMercado";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string DataEmissao = "DataEmissao";
			 public const string DataVencimento = "DataVencimento";
			 public const string TipoPonta = "TipoPonta";
			 public const string IdIndice = "IdIndice";
			 public const string TipoPontaContraParte = "TipoPontaContraParte";
			 public const string IdIndiceContraParte = "IdIndiceContraParte";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeOriginal = "QuantidadeOriginal";
			 public const string Processar = "Processar";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EventoFundoSwapMetadata))
			{
				if(EventoFundoSwapMetadata.mapDelegates == null)
				{
					EventoFundoSwapMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EventoFundoSwapMetadata.meta == null)
				{
					EventoFundoSwapMetadata.meta = new EventoFundoSwapMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEventoFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("NumeroContrato", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataEmissao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoPonta", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TipoPontaContraParte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdIndiceContraParte", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeOriginal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Processar", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "EventoFundoSwap";
				meta.Destination = "EventoFundoSwap";
				
				meta.spInsert = "proc_EventoFundoSwapInsert";				
				meta.spUpdate = "proc_EventoFundoSwapUpdate";		
				meta.spDelete = "proc_EventoFundoSwapDelete";
				meta.spLoadAll = "proc_EventoFundoSwapLoadAll";
				meta.spLoadByPrimaryKey = "proc_EventoFundoSwapLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EventoFundoSwapMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
