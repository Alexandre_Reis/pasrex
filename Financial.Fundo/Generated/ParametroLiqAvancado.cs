/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 22/12/2014 17:49:21
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esParametroLiqAvancadoCollection : esEntityCollection
	{
		public esParametroLiqAvancadoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ParametroLiqAvancadoCollection";
		}

		#region Query Logic
		protected void InitQuery(esParametroLiqAvancadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esParametroLiqAvancadoQuery);
		}
		#endregion
		
		virtual public ParametroLiqAvancado DetachEntity(ParametroLiqAvancado entity)
		{
			return base.DetachEntity(entity) as ParametroLiqAvancado;
		}
		
		virtual public ParametroLiqAvancado AttachEntity(ParametroLiqAvancado entity)
		{
			return base.AttachEntity(entity) as ParametroLiqAvancado;
		}
		
		virtual public void Combine(ParametroLiqAvancadoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ParametroLiqAvancado this[int index]
		{
			get
			{
				return base[index] as ParametroLiqAvancado;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ParametroLiqAvancado);
		}
	}



	[Serializable]
	abstract public class esParametroLiqAvancado : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esParametroLiqAvancadoQuery GetDynamicQuery()
		{
			return null;
		}

		public esParametroLiqAvancado()
		{

		}

		public esParametroLiqAvancado(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataVigencia, System.Int32 idCarteira, System.Int32 tipoData, System.Int32 tipoOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataVigencia, idCarteira, tipoData, tipoOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(dataVigencia, idCarteira, tipoData, tipoOperacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataVigencia, System.Int32 idCarteira, System.Int32 tipoData, System.Int32 tipoOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataVigencia, idCarteira, tipoData, tipoOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(dataVigencia, idCarteira, tipoData, tipoOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataVigencia, System.Int32 idCarteira, System.Int32 tipoData, System.Int32 tipoOperacao)
		{
			esParametroLiqAvancadoQuery query = this.GetDynamicQuery();
			query.Where(query.DataVigencia == dataVigencia, query.IdCarteira == idCarteira, query.TipoData == tipoData, query.TipoOperacao == tipoOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataVigencia, System.Int32 idCarteira, System.Int32 tipoData, System.Int32 tipoOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("DataVigencia",dataVigencia);			parms.Add("IdCarteira",idCarteira);			parms.Add("TipoData",tipoData);			parms.Add("TipoOperacao",tipoOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataVigencia": this.str.DataVigencia = (string)value; break;							
						case "TipoData": this.str.TipoData = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "Parametro": this.str.Parametro = (string)value; break;							
						case "ParametroValido": this.str.ParametroValido = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVigencia = (System.DateTime?)value;
							break;
						
						case "TipoData":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoData = (System.Int32?)value;
							break;
						
						case "TipoOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoOperacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ParametroLiqAvancado.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(ParametroLiqAvancadoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(ParametroLiqAvancadoMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to ParametroLiqAvancado.DataVigencia
		/// </summary>
		virtual public System.DateTime? DataVigencia
		{
			get
			{
				return base.GetSystemDateTime(ParametroLiqAvancadoMetadata.ColumnNames.DataVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(ParametroLiqAvancadoMetadata.ColumnNames.DataVigencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ParametroLiqAvancado.TipoData
		/// </summary>
		virtual public System.Int32? TipoData
		{
			get
			{
				return base.GetSystemInt32(ParametroLiqAvancadoMetadata.ColumnNames.TipoData);
			}
			
			set
			{
				base.SetSystemInt32(ParametroLiqAvancadoMetadata.ColumnNames.TipoData, value);
			}
		}
		
		/// <summary>
		/// Maps to ParametroLiqAvancado.TipoOperacao
		/// </summary>
		virtual public System.Int32? TipoOperacao
		{
			get
			{
				return base.GetSystemInt32(ParametroLiqAvancadoMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemInt32(ParametroLiqAvancadoMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ParametroLiqAvancado.Parametro
		/// </summary>
		virtual public System.String Parametro
		{
			get
			{
				return base.GetSystemString(ParametroLiqAvancadoMetadata.ColumnNames.Parametro);
			}
			
			set
			{
				base.SetSystemString(ParametroLiqAvancadoMetadata.ColumnNames.Parametro, value);
			}
		}
		
		/// <summary>
		/// Maps to ParametroLiqAvancado.ParametroValido
		/// </summary>
		virtual public System.String ParametroValido
		{
			get
			{
				return base.GetSystemString(ParametroLiqAvancadoMetadata.ColumnNames.ParametroValido);
			}
			
			set
			{
				base.SetSystemString(ParametroLiqAvancadoMetadata.ColumnNames.ParametroValido, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esParametroLiqAvancado entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataVigencia
			{
				get
				{
					System.DateTime? data = entity.DataVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVigencia = null;
					else entity.DataVigencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoData
			{
				get
				{
					System.Int32? data = entity.TipoData;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoData = null;
					else entity.TipoData = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.Int32? data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String Parametro
			{
				get
				{
					System.String data = entity.Parametro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Parametro = null;
					else entity.Parametro = Convert.ToString(value);
				}
			}
				
			public System.String ParametroValido
			{
				get
				{
					System.String data = entity.ParametroValido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParametroValido = null;
					else entity.ParametroValido = Convert.ToString(value);
				}
			}
			

			private esParametroLiqAvancado entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esParametroLiqAvancadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esParametroLiqAvancado can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ParametroLiqAvancado : esParametroLiqAvancado
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esParametroLiqAvancadoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ParametroLiqAvancadoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, ParametroLiqAvancadoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataVigencia
		{
			get
			{
				return new esQueryItem(this, ParametroLiqAvancadoMetadata.ColumnNames.DataVigencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoData
		{
			get
			{
				return new esQueryItem(this, ParametroLiqAvancadoMetadata.ColumnNames.TipoData, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, ParametroLiqAvancadoMetadata.ColumnNames.TipoOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Parametro
		{
			get
			{
				return new esQueryItem(this, ParametroLiqAvancadoMetadata.ColumnNames.Parametro, esSystemType.String);
			}
		} 
		
		public esQueryItem ParametroValido
		{
			get
			{
				return new esQueryItem(this, ParametroLiqAvancadoMetadata.ColumnNames.ParametroValido, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ParametroLiqAvancadoCollection")]
	public partial class ParametroLiqAvancadoCollection : esParametroLiqAvancadoCollection, IEnumerable<ParametroLiqAvancado>
	{
		public ParametroLiqAvancadoCollection()
		{

		}
		
		public static implicit operator List<ParametroLiqAvancado>(ParametroLiqAvancadoCollection coll)
		{
			List<ParametroLiqAvancado> list = new List<ParametroLiqAvancado>();
			
			foreach (ParametroLiqAvancado emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ParametroLiqAvancadoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ParametroLiqAvancadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ParametroLiqAvancado(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ParametroLiqAvancado();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ParametroLiqAvancadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ParametroLiqAvancadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ParametroLiqAvancadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ParametroLiqAvancado AddNew()
		{
			ParametroLiqAvancado entity = base.AddNewEntity() as ParametroLiqAvancado;
			
			return entity;
		}

		public ParametroLiqAvancado FindByPrimaryKey(System.DateTime dataVigencia, System.Int32 idCarteira, System.Int32 tipoData, System.Int32 tipoOperacao)
		{
			return base.FindByPrimaryKey(dataVigencia, idCarteira, tipoData, tipoOperacao) as ParametroLiqAvancado;
		}


		#region IEnumerable<ParametroLiqAvancado> Members

		IEnumerator<ParametroLiqAvancado> IEnumerable<ParametroLiqAvancado>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ParametroLiqAvancado;
			}
		}

		#endregion
		
		private ParametroLiqAvancadoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ParametroLiqAvancado' table
	/// </summary>

	[Serializable]
	public partial class ParametroLiqAvancado : esParametroLiqAvancado
	{
		public ParametroLiqAvancado()
		{

		}
	
		public ParametroLiqAvancado(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ParametroLiqAvancadoMetadata.Meta();
			}
		}
		
		
		
		override protected esParametroLiqAvancadoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ParametroLiqAvancadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ParametroLiqAvancadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ParametroLiqAvancadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ParametroLiqAvancadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ParametroLiqAvancadoQuery query;
	}



	[Serializable]
	public partial class ParametroLiqAvancadoQuery : esParametroLiqAvancadoQuery
	{
		public ParametroLiqAvancadoQuery()
		{

		}		
		
		public ParametroLiqAvancadoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ParametroLiqAvancadoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ParametroLiqAvancadoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ParametroLiqAvancadoMetadata.ColumnNames.IdCarteira, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ParametroLiqAvancadoMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ParametroLiqAvancadoMetadata.ColumnNames.DataVigencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ParametroLiqAvancadoMetadata.PropertyNames.DataVigencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ParametroLiqAvancadoMetadata.ColumnNames.TipoData, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ParametroLiqAvancadoMetadata.PropertyNames.TipoData;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ParametroLiqAvancadoMetadata.ColumnNames.TipoOperacao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ParametroLiqAvancadoMetadata.PropertyNames.TipoOperacao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ParametroLiqAvancadoMetadata.ColumnNames.Parametro, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = ParametroLiqAvancadoMetadata.PropertyNames.Parametro;
			c.CharacterMaxLength = 150;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ParametroLiqAvancadoMetadata.ColumnNames.ParametroValido, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = ParametroLiqAvancadoMetadata.PropertyNames.ParametroValido;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ParametroLiqAvancadoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string DataVigencia = "DataVigencia";
			 public const string TipoData = "TipoData";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Parametro = "Parametro";
			 public const string ParametroValido = "ParametroValido";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string DataVigencia = "DataVigencia";
			 public const string TipoData = "TipoData";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Parametro = "Parametro";
			 public const string ParametroValido = "ParametroValido";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ParametroLiqAvancadoMetadata))
			{
				if(ParametroLiqAvancadoMetadata.mapDelegates == null)
				{
					ParametroLiqAvancadoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ParametroLiqAvancadoMetadata.meta == null)
				{
					ParametroLiqAvancadoMetadata.meta = new ParametroLiqAvancadoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataVigencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoData", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Parametro", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ParametroValido", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "ParametroLiqAvancado";
				meta.Destination = "ParametroLiqAvancado";
				
				meta.spInsert = "proc_ParametroLiqAvancadoInsert";				
				meta.spUpdate = "proc_ParametroLiqAvancadoUpdate";		
				meta.spDelete = "proc_ParametroLiqAvancadoDelete";
				meta.spLoadAll = "proc_ParametroLiqAvancadoLoadAll";
				meta.spLoadByPrimaryKey = "proc_ParametroLiqAvancadoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ParametroLiqAvancadoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
