/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/12/2014 11:48:51
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esEventoFundoCautelaCollection : esEntityCollection
	{
		public esEventoFundoCautelaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EventoFundoCautelaCollection";
		}

		#region Query Logic
		protected void InitQuery(esEventoFundoCautelaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEventoFundoCautelaQuery);
		}
		#endregion
		
		virtual public EventoFundoCautela DetachEntity(EventoFundoCautela entity)
		{
			return base.DetachEntity(entity) as EventoFundoCautela;
		}
		
		virtual public EventoFundoCautela AttachEntity(EventoFundoCautela entity)
		{
			return base.AttachEntity(entity) as EventoFundoCautela;
		}
		
		virtual public void Combine(EventoFundoCautelaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EventoFundoCautela this[int index]
		{
			get
			{
				return base[index] as EventoFundoCautela;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EventoFundoCautela);
		}
	}



	[Serializable]
	abstract public class esEventoFundoCautela : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEventoFundoCautelaQuery GetDynamicQuery()
		{
			return null;
		}

		public esEventoFundoCautela()
		{

		}

		public esEventoFundoCautela(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataAplicacao, System.Int32 idCotista, System.Int32 idEventoFundo, System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataAplicacao, idCotista, idEventoFundo, idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(dataAplicacao, idCotista, idEventoFundo, idOperacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataAplicacao, System.Int32 idCotista, System.Int32 idEventoFundo, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataAplicacao, idCotista, idEventoFundo, idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(dataAplicacao, idCotista, idEventoFundo, idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataAplicacao, System.Int32 idCotista, System.Int32 idEventoFundo, System.Int32 idOperacao)
		{
			esEventoFundoCautelaQuery query = this.GetDynamicQuery();
			query.Where(query.DataAplicacao == dataAplicacao, query.IdCotista == idCotista, query.IdEventoFundo == idEventoFundo, query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataAplicacao, System.Int32 idCotista, System.Int32 idEventoFundo, System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("DataAplicacao",dataAplicacao);			parms.Add("IdCotista",idCotista);			parms.Add("IdEventoFundo",idEventoFundo);			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEventoFundo": this.str.IdEventoFundo = (string)value; break;							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "DataAplicacao": this.str.DataAplicacao = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Processar": this.str.Processar = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEventoFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoFundo = (System.Int32?)value;
							break;
						
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
						
						case "DataAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAplicacao = (System.DateTime?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "Processar":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Processar = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EventoFundoCautela.IdEventoFundo
		/// </summary>
		virtual public System.Int32? IdEventoFundo
		{
			get
			{
				return base.GetSystemInt32(EventoFundoCautelaMetadata.ColumnNames.IdEventoFundo);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoFundoCautelaMetadata.ColumnNames.IdEventoFundo, value))
				{
					this._UpToEventoFundoByIdEventoFundo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoCautela.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(EventoFundoCautelaMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoCautelaMetadata.ColumnNames.IdCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoCautela.DataAplicacao
		/// </summary>
		virtual public System.DateTime? DataAplicacao
		{
			get
			{
				return base.GetSystemDateTime(EventoFundoCautelaMetadata.ColumnNames.DataAplicacao);
			}
			
			set
			{
				base.SetSystemDateTime(EventoFundoCautelaMetadata.ColumnNames.DataAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoCautela.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(EventoFundoCautelaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoCautelaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoCautela.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(EventoFundoCautelaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(EventoFundoCautelaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoCautela.Processar
		/// </summary>
		virtual public System.Byte? Processar
		{
			get
			{
				return base.GetSystemByte(EventoFundoCautelaMetadata.ColumnNames.Processar);
			}
			
			set
			{
				base.SetSystemByte(EventoFundoCautelaMetadata.ColumnNames.Processar, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected EventoFundo _UpToEventoFundoByIdEventoFundo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEventoFundoCautela entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEventoFundo
			{
				get
				{
					System.Int32? data = entity.IdEventoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoFundo = null;
					else entity.IdEventoFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String DataAplicacao
			{
				get
				{
					System.DateTime? data = entity.DataAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAplicacao = null;
					else entity.DataAplicacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Processar
			{
				get
				{
					System.Byte? data = entity.Processar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Processar = null;
					else entity.Processar = Convert.ToByte(value);
				}
			}
			

			private esEventoFundoCautela entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEventoFundoCautelaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEventoFundoCautela can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EventoFundoCautela : esEventoFundoCautela
	{

				
		#region UpToEventoFundoByIdEventoFundo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EventoFundoCautela_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundo UpToEventoFundoByIdEventoFundo
		{
			get
			{
				if(this._UpToEventoFundoByIdEventoFundo == null
					&& IdEventoFundo != null					)
				{
					this._UpToEventoFundoByIdEventoFundo = new EventoFundo();
					this._UpToEventoFundoByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Where(this._UpToEventoFundoByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Load();
				}

				return this._UpToEventoFundoByIdEventoFundo;
			}
			
			set
			{
				this.RemovePreSave("UpToEventoFundoByIdEventoFundo");
				

				if(value == null)
				{
					this.IdEventoFundo = null;
					this._UpToEventoFundoByIdEventoFundo = null;
				}
				else
				{
					this.IdEventoFundo = value.IdEventoFundo;
					this._UpToEventoFundoByIdEventoFundo = value;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEventoFundoByIdEventoFundo != null)
			{
				this.IdEventoFundo = this._UpToEventoFundoByIdEventoFundo.IdEventoFundo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEventoFundoCautelaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoCautelaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEventoFundo
		{
			get
			{
				return new esQueryItem(this, EventoFundoCautelaMetadata.ColumnNames.IdEventoFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, EventoFundoCautelaMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataAplicacao
		{
			get
			{
				return new esQueryItem(this, EventoFundoCautelaMetadata.ColumnNames.DataAplicacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, EventoFundoCautelaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, EventoFundoCautelaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Processar
		{
			get
			{
				return new esQueryItem(this, EventoFundoCautelaMetadata.ColumnNames.Processar, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EventoFundoCautelaCollection")]
	public partial class EventoFundoCautelaCollection : esEventoFundoCautelaCollection, IEnumerable<EventoFundoCautela>
	{
		public EventoFundoCautelaCollection()
		{

		}
		
		public static implicit operator List<EventoFundoCautela>(EventoFundoCautelaCollection coll)
		{
			List<EventoFundoCautela> list = new List<EventoFundoCautela>();
			
			foreach (EventoFundoCautela emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EventoFundoCautelaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoCautelaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EventoFundoCautela(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EventoFundoCautela();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EventoFundoCautelaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoCautelaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EventoFundoCautelaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EventoFundoCautela AddNew()
		{
			EventoFundoCautela entity = base.AddNewEntity() as EventoFundoCautela;
			
			return entity;
		}

		public EventoFundoCautela FindByPrimaryKey(System.DateTime dataAplicacao, System.Int32 idCotista, System.Int32 idEventoFundo, System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(dataAplicacao, idCotista, idEventoFundo, idOperacao) as EventoFundoCautela;
		}


		#region IEnumerable<EventoFundoCautela> Members

		IEnumerator<EventoFundoCautela> IEnumerable<EventoFundoCautela>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EventoFundoCautela;
			}
		}

		#endregion
		
		private EventoFundoCautelaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EventoFundoCautela' table
	/// </summary>

	[Serializable]
	public partial class EventoFundoCautela : esEventoFundoCautela
	{
		public EventoFundoCautela()
		{

		}
	
		public EventoFundoCautela(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoCautelaMetadata.Meta();
			}
		}
		
		
		
		override protected esEventoFundoCautelaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoCautelaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EventoFundoCautelaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoCautelaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EventoFundoCautelaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EventoFundoCautelaQuery query;
	}



	[Serializable]
	public partial class EventoFundoCautelaQuery : esEventoFundoCautelaQuery
	{
		public EventoFundoCautelaQuery()
		{

		}		
		
		public EventoFundoCautelaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EventoFundoCautelaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EventoFundoCautelaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EventoFundoCautelaMetadata.ColumnNames.IdEventoFundo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoCautelaMetadata.PropertyNames.IdEventoFundo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoCautelaMetadata.ColumnNames.IdCotista, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoCautelaMetadata.PropertyNames.IdCotista;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoCautelaMetadata.ColumnNames.DataAplicacao, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EventoFundoCautelaMetadata.PropertyNames.DataAplicacao;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoCautelaMetadata.ColumnNames.IdOperacao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoCautelaMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoCautelaMetadata.ColumnNames.Quantidade, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EventoFundoCautelaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoCautelaMetadata.ColumnNames.Processar, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EventoFundoCautelaMetadata.PropertyNames.Processar;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EventoFundoCautelaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string IdCotista = "IdCotista";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string IdOperacao = "IdOperacao";
			 public const string Quantidade = "Quantidade";
			 public const string Processar = "Processar";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string IdCotista = "IdCotista";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string IdOperacao = "IdOperacao";
			 public const string Quantidade = "Quantidade";
			 public const string Processar = "Processar";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EventoFundoCautelaMetadata))
			{
				if(EventoFundoCautelaMetadata.mapDelegates == null)
				{
					EventoFundoCautelaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EventoFundoCautelaMetadata.meta == null)
				{
					EventoFundoCautelaMetadata.meta = new EventoFundoCautelaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEventoFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataAplicacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Processar", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "EventoFundoCautela";
				meta.Destination = "EventoFundoCautela";
				
				meta.spInsert = "proc_EventoFundoCautelaInsert";				
				meta.spUpdate = "proc_EventoFundoCautelaUpdate";		
				meta.spDelete = "proc_EventoFundoCautelaDelete";
				meta.spLoadAll = "proc_EventoFundoCautelaLoadAll";
				meta.spLoadByPrimaryKey = "proc_EventoFundoCautelaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EventoFundoCautelaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
