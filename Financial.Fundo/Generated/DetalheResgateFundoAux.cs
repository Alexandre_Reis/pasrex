/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 20/01/2015 12:13:38
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esDetalheResgateFundoAuxCollection : esEntityCollection
	{
		public esDetalheResgateFundoAuxCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "DetalheResgateFundoAuxCollection";
		}

		#region Query Logic
		protected void InitQuery(esDetalheResgateFundoAuxQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esDetalheResgateFundoAuxQuery);
		}
		#endregion
		
		virtual public DetalheResgateFundoAux DetachEntity(DetalheResgateFundoAux entity)
		{
			return base.DetachEntity(entity) as DetalheResgateFundoAux;
		}
		
		virtual public DetalheResgateFundoAux AttachEntity(DetalheResgateFundoAux entity)
		{
			return base.AttachEntity(entity) as DetalheResgateFundoAux;
		}
		
		virtual public void Combine(DetalheResgateFundoAuxCollection collection)
		{
			base.Combine(collection);
		}
		
		new public DetalheResgateFundoAux this[int index]
		{
			get
			{
				return base[index] as DetalheResgateFundoAux;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(DetalheResgateFundoAux);
		}
	}



	[Serializable]
	abstract public class esDetalheResgateFundoAux : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esDetalheResgateFundoAuxQuery GetDynamicQuery()
		{
			return null;
		}

		public esDetalheResgateFundoAux()
		{

		}

		public esDetalheResgateFundoAux(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao, System.Int32 idPosicaoResgatada)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao, idPosicaoResgatada);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao, idPosicaoResgatada);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao, System.Int32 idPosicaoResgatada)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao, idPosicaoResgatada);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao, idPosicaoResgatada);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao, System.Int32 idPosicaoResgatada)
		{
			esDetalheResgateFundoAuxQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao, query.IdPosicaoResgatada == idPosicaoResgatada);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao, System.Int32 idPosicaoResgatada)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);			parms.Add("IdPosicaoResgatada",idPosicaoResgatada);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdPosicaoResgatada": this.str.IdPosicaoResgatada = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "ValorCPMF": this.str.ValorCPMF = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "PrejuizoUsado": this.str.PrejuizoUsado = (string)value; break;							
						case "RendimentoResgate": this.str.RendimentoResgate = (string)value; break;							
						case "VariacaoResgate": this.str.VariacaoResgate = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdPosicaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoResgatada = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "ValorCPMF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMF = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "PrejuizoUsado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoUsado = (System.Decimal?)value;
							break;
						
						case "RendimentoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoResgate = (System.Decimal?)value;
							break;
						
						case "VariacaoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VariacaoResgate = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "TipoOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoOperacao = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(DetalheResgateFundoAuxMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(DetalheResgateFundoAuxMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.IdPosicaoResgatada
		/// </summary>
		virtual public System.Int32? IdPosicaoResgatada
		{
			get
			{
				return base.GetSystemInt32(DetalheResgateFundoAuxMetadata.ColumnNames.IdPosicaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(DetalheResgateFundoAuxMetadata.ColumnNames.IdPosicaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(DetalheResgateFundoAuxMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(DetalheResgateFundoAuxMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(DetalheResgateFundoAuxMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(DetalheResgateFundoAuxMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.ValorCPMF
		/// </summary>
		virtual public System.Decimal? ValorCPMF
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.ValorCPMF);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.ValorCPMF, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.PrejuizoUsado
		/// </summary>
		virtual public System.Decimal? PrejuizoUsado
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.PrejuizoUsado);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.PrejuizoUsado, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.RendimentoResgate
		/// </summary>
		virtual public System.Decimal? RendimentoResgate
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.RendimentoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.RendimentoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.VariacaoResgate
		/// </summary>
		virtual public System.Decimal? VariacaoResgate
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.VariacaoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.VariacaoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoAuxMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(DetalheResgateFundoAuxMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(DetalheResgateFundoAuxMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundoAux.TipoOperacao
		/// </summary>
		virtual public System.Byte? TipoOperacao
		{
			get
			{
				return base.GetSystemByte(DetalheResgateFundoAuxMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemByte(DetalheResgateFundoAuxMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esDetalheResgateFundoAux entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdPosicaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoResgatada = null;
					else entity.IdPosicaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCPMF
			{
				get
				{
					System.Decimal? data = entity.ValorCPMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMF = null;
					else entity.ValorCPMF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrejuizoUsado
			{
				get
				{
					System.Decimal? data = entity.PrejuizoUsado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoUsado = null;
					else entity.PrejuizoUsado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoResgate
			{
				get
				{
					System.Decimal? data = entity.RendimentoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoResgate = null;
					else entity.RendimentoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String VariacaoResgate
			{
				get
				{
					System.Decimal? data = entity.VariacaoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VariacaoResgate = null;
					else entity.VariacaoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.Byte? data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToByte(value);
				}
			}
			

			private esDetalheResgateFundoAux entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esDetalheResgateFundoAuxQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esDetalheResgateFundoAux can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class DetalheResgateFundoAux : esDetalheResgateFundoAux
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esDetalheResgateFundoAuxQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return DetalheResgateFundoAuxMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicaoResgatada
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.IdPosicaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCPMF
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.ValorCPMF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrejuizoUsado
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.PrejuizoUsado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoResgate
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.RendimentoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VariacaoResgate
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.VariacaoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoAuxMetadata.ColumnNames.TipoOperacao, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("DetalheResgateFundoAuxCollection")]
	public partial class DetalheResgateFundoAuxCollection : esDetalheResgateFundoAuxCollection, IEnumerable<DetalheResgateFundoAux>
	{
		public DetalheResgateFundoAuxCollection()
		{

		}
		
		public static implicit operator List<DetalheResgateFundoAux>(DetalheResgateFundoAuxCollection coll)
		{
			List<DetalheResgateFundoAux> list = new List<DetalheResgateFundoAux>();
			
			foreach (DetalheResgateFundoAux emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  DetalheResgateFundoAuxMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DetalheResgateFundoAuxQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new DetalheResgateFundoAux(row);
		}

		override protected esEntity CreateEntity()
		{
			return new DetalheResgateFundoAux();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public DetalheResgateFundoAuxQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DetalheResgateFundoAuxQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(DetalheResgateFundoAuxQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public DetalheResgateFundoAux AddNew()
		{
			DetalheResgateFundoAux entity = base.AddNewEntity() as DetalheResgateFundoAux;
			
			return entity;
		}

		public DetalheResgateFundoAux FindByPrimaryKey(System.Int32 idOperacao, System.Int32 idPosicaoResgatada)
		{
			return base.FindByPrimaryKey(idOperacao, idPosicaoResgatada) as DetalheResgateFundoAux;
		}


		#region IEnumerable<DetalheResgateFundoAux> Members

		IEnumerator<DetalheResgateFundoAux> IEnumerable<DetalheResgateFundoAux>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as DetalheResgateFundoAux;
			}
		}

		#endregion
		
		private DetalheResgateFundoAuxQuery query;
	}


	/// <summary>
	/// Encapsulates the 'DetalheResgateFundoAux' table
	/// </summary>

	[Serializable]
	public partial class DetalheResgateFundoAux : esDetalheResgateFundoAux
	{
		public DetalheResgateFundoAux()
		{

		}
	
		public DetalheResgateFundoAux(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return DetalheResgateFundoAuxMetadata.Meta();
			}
		}
		
		
		
		override protected esDetalheResgateFundoAuxQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DetalheResgateFundoAuxQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public DetalheResgateFundoAuxQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DetalheResgateFundoAuxQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(DetalheResgateFundoAuxQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private DetalheResgateFundoAuxQuery query;
	}



	[Serializable]
	public partial class DetalheResgateFundoAuxQuery : esDetalheResgateFundoAuxQuery
	{
		public DetalheResgateFundoAuxQuery()
		{

		}		
		
		public DetalheResgateFundoAuxQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class DetalheResgateFundoAuxMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected DetalheResgateFundoAuxMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.IdPosicaoResgatada, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.IdPosicaoResgatada;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.IdCarteira, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.Quantidade, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.ValorBruto, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.ValorLiquido, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.ValorCPMF, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.ValorCPMF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.ValorPerformance, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.PrejuizoUsado, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.PrejuizoUsado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.RendimentoResgate, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.RendimentoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.VariacaoResgate, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.VariacaoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.ValorIR, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.ValorIOF, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.DataOperacao, 14, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoAuxMetadata.ColumnNames.TipoOperacao, 15, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = DetalheResgateFundoAuxMetadata.PropertyNames.TipoOperacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public DetalheResgateFundoAuxMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string Quantidade = "Quantidade";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string DataOperacao = "DataOperacao";
			 public const string TipoOperacao = "TipoOperacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string Quantidade = "Quantidade";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string DataOperacao = "DataOperacao";
			 public const string TipoOperacao = "TipoOperacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(DetalheResgateFundoAuxMetadata))
			{
				if(DetalheResgateFundoAuxMetadata.mapDelegates == null)
				{
					DetalheResgateFundoAuxMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (DetalheResgateFundoAuxMetadata.meta == null)
				{
					DetalheResgateFundoAuxMetadata.meta = new DetalheResgateFundoAuxMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCPMF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrejuizoUsado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VariacaoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "DetalheResgateFundoAux";
				meta.Destination = "DetalheResgateFundoAux";
				
				meta.spInsert = "proc_DetalheResgateFundoAuxInsert";				
				meta.spUpdate = "proc_DetalheResgateFundoAuxUpdate";		
				meta.spDelete = "proc_DetalheResgateFundoAuxDelete";
				meta.spLoadAll = "proc_DetalheResgateFundoAuxLoadAll";
				meta.spLoadByPrimaryKey = "proc_DetalheResgateFundoAuxLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private DetalheResgateFundoAuxMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
