/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 25/05/2015 13:56:49
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTributacaoFundoFieCollection : esEntityCollection
	{
		public esTributacaoFundoFieCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TributacaoFundoFieCollection";
		}

		#region Query Logic
		protected void InitQuery(esTributacaoFundoFieQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTributacaoFundoFieQuery);
		}
		#endregion
		
		virtual public TributacaoFundoFie DetachEntity(TributacaoFundoFie entity)
		{
			return base.DetachEntity(entity) as TributacaoFundoFie;
		}
		
		virtual public TributacaoFundoFie AttachEntity(TributacaoFundoFie entity)
		{
			return base.AttachEntity(entity) as TributacaoFundoFie;
		}
		
		virtual public void Combine(TributacaoFundoFieCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TributacaoFundoFie this[int index]
		{
			get
			{
				return base[index] as TributacaoFundoFie;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TributacaoFundoFie);
		}
	}



	[Serializable]
	abstract public class esTributacaoFundoFie : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTributacaoFundoFieQuery GetDynamicQuery()
		{
			return null;
		}

		public esTributacaoFundoFie()
		{

		}

		public esTributacaoFundoFie(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idOperacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int32 idOperacao)
		{
			esTributacaoFundoFieQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "FieTabelaIr": this.str.FieTabelaIr = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "FieTabelaIr":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FieTabelaIr = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TributacaoFundoFie.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(TributacaoFundoFieMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(TributacaoFundoFieMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to TributacaoFundoFie.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(TributacaoFundoFieMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				if(base.SetSystemInt32(TributacaoFundoFieMetadata.ColumnNames.IdOperacao, value))
				{
					this._UpToOperacaoFundoByIdOperacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TributacaoFundoFie.FieTabelaIr
		/// </summary>
		virtual public System.Int32? FieTabelaIr
		{
			get
			{
				return base.GetSystemInt32(TributacaoFundoFieMetadata.ColumnNames.FieTabelaIr);
			}
			
			set
			{
				base.SetSystemInt32(TributacaoFundoFieMetadata.ColumnNames.FieTabelaIr, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected OperacaoFundo _UpToOperacaoFundoByIdOperacao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTributacaoFundoFie entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String FieTabelaIr
			{
				get
				{
					System.Int32? data = entity.FieTabelaIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FieTabelaIr = null;
					else entity.FieTabelaIr = Convert.ToInt32(value);
				}
			}
			

			private esTributacaoFundoFie entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTributacaoFundoFieQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTributacaoFundoFie can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TributacaoFundoFie : esTributacaoFundoFie
	{

				
		#region UpToOperacaoFundoByIdOperacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TributacaoCautelaFie_OperacaoFundo_FK
		/// </summary>

		[XmlIgnore]
		public OperacaoFundo UpToOperacaoFundoByIdOperacao
		{
			get
			{
				if(this._UpToOperacaoFundoByIdOperacao == null
					&& IdOperacao != null					)
				{
					this._UpToOperacaoFundoByIdOperacao = new OperacaoFundo();
					this._UpToOperacaoFundoByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOperacaoFundoByIdOperacao", this._UpToOperacaoFundoByIdOperacao);
					this._UpToOperacaoFundoByIdOperacao.Query.Where(this._UpToOperacaoFundoByIdOperacao.Query.IdOperacao == this.IdOperacao);
					this._UpToOperacaoFundoByIdOperacao.Query.Load();
				}

				return this._UpToOperacaoFundoByIdOperacao;
			}
			
			set
			{
				this.RemovePreSave("UpToOperacaoFundoByIdOperacao");
				

				if(value == null)
				{
					this.IdOperacao = null;
					this._UpToOperacaoFundoByIdOperacao = null;
				}
				else
				{
					this.IdOperacao = value.IdOperacao;
					this._UpToOperacaoFundoByIdOperacao = value;
					this.SetPreSave("UpToOperacaoFundoByIdOperacao", this._UpToOperacaoFundoByIdOperacao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToOperacaoFundoByIdOperacao != null)
			{
				this.IdOperacao = this._UpToOperacaoFundoByIdOperacao.IdOperacao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTributacaoFundoFieQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TributacaoFundoFieMetadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, TributacaoFundoFieMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, TributacaoFundoFieMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FieTabelaIr
		{
			get
			{
				return new esQueryItem(this, TributacaoFundoFieMetadata.ColumnNames.FieTabelaIr, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TributacaoFundoFieCollection")]
	public partial class TributacaoFundoFieCollection : esTributacaoFundoFieCollection, IEnumerable<TributacaoFundoFie>
	{
		public TributacaoFundoFieCollection()
		{

		}
		
		public static implicit operator List<TributacaoFundoFie>(TributacaoFundoFieCollection coll)
		{
			List<TributacaoFundoFie> list = new List<TributacaoFundoFie>();
			
			foreach (TributacaoFundoFie emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TributacaoFundoFieMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TributacaoFundoFieQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TributacaoFundoFie(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TributacaoFundoFie();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TributacaoFundoFieQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TributacaoFundoFieQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TributacaoFundoFieQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TributacaoFundoFie AddNew()
		{
			TributacaoFundoFie entity = base.AddNewEntity() as TributacaoFundoFie;
			
			return entity;
		}

		public TributacaoFundoFie FindByPrimaryKey(System.DateTime data, System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(data, idOperacao) as TributacaoFundoFie;
		}


		#region IEnumerable<TributacaoFundoFie> Members

		IEnumerator<TributacaoFundoFie> IEnumerable<TributacaoFundoFie>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TributacaoFundoFie;
			}
		}

		#endregion
		
		private TributacaoFundoFieQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TributacaoFundoFie' table
	/// </summary>

	[Serializable]
	public partial class TributacaoFundoFie : esTributacaoFundoFie
	{
		public TributacaoFundoFie()
		{

		}
	
		public TributacaoFundoFie(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TributacaoFundoFieMetadata.Meta();
			}
		}
		
		
		
		override protected esTributacaoFundoFieQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TributacaoFundoFieQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TributacaoFundoFieQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TributacaoFundoFieQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TributacaoFundoFieQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TributacaoFundoFieQuery query;
	}



	[Serializable]
	public partial class TributacaoFundoFieQuery : esTributacaoFundoFieQuery
	{
		public TributacaoFundoFieQuery()
		{

		}		
		
		public TributacaoFundoFieQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TributacaoFundoFieMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TributacaoFundoFieMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TributacaoFundoFieMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TributacaoFundoFieMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TributacaoFundoFieMetadata.ColumnNames.IdOperacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TributacaoFundoFieMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TributacaoFundoFieMetadata.ColumnNames.FieTabelaIr, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TributacaoFundoFieMetadata.PropertyNames.FieTabelaIr;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TributacaoFundoFieMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string IdOperacao = "IdOperacao";
			 public const string FieTabelaIr = "FieTabelaIr";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string IdOperacao = "IdOperacao";
			 public const string FieTabelaIr = "FieTabelaIr";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TributacaoFundoFieMetadata))
			{
				if(TributacaoFundoFieMetadata.mapDelegates == null)
				{
					TributacaoFundoFieMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TributacaoFundoFieMetadata.meta == null)
				{
					TributacaoFundoFieMetadata.meta = new TributacaoFundoFieMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FieTabelaIr", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "TributacaoFundoFie";
				meta.Destination = "TributacaoFundoFie";
				
				meta.spInsert = "proc_TributacaoFundoFieInsert";				
				meta.spUpdate = "proc_TributacaoFundoFieUpdate";		
				meta.spDelete = "proc_TributacaoFundoFieDelete";
				meta.spLoadAll = "proc_TributacaoFundoFieLoadAll";
				meta.spLoadByPrimaryKey = "proc_TributacaoFundoFieLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TributacaoFundoFieMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
