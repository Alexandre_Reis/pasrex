/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/09/2015 11:47:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esColagemResgateDetalheCollection : esEntityCollection
	{
		public esColagemResgateDetalheCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ColagemResgateDetalheCollection";
		}

		#region Query Logic
		protected void InitQuery(esColagemResgateDetalheQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esColagemResgateDetalheQuery);
		}
		#endregion
		
		virtual public ColagemResgateDetalhe DetachEntity(ColagemResgateDetalhe entity)
		{
			return base.DetachEntity(entity) as ColagemResgateDetalhe;
		}
		
		virtual public ColagemResgateDetalhe AttachEntity(ColagemResgateDetalhe entity)
		{
			return base.AttachEntity(entity) as ColagemResgateDetalhe;
		}
		
		virtual public void Combine(ColagemResgateDetalheCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ColagemResgateDetalhe this[int index]
		{
			get
			{
				return base[index] as ColagemResgateDetalhe;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ColagemResgateDetalhe);
		}
	}



	[Serializable]
	abstract public class esColagemResgateDetalhe : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esColagemResgateDetalheQuery GetDynamicQuery()
		{
			return null;
		}

		public esColagemResgateDetalhe()
		{

		}

		public esColagemResgateDetalhe(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idColagemResgateDetalhe)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemResgateDetalhe);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemResgateDetalhe);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idColagemResgateDetalhe)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemResgateDetalhe);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemResgateDetalhe);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idColagemResgateDetalhe)
		{
			esColagemResgateDetalheQuery query = this.GetDynamicQuery();
			query.Where(query.IdColagemResgateDetalhe == idColagemResgateDetalhe);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idColagemResgateDetalhe)
		{
			esParameters parms = new esParameters();
			parms.Add("IdColagemResgateDetalhe",idColagemResgateDetalhe);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdColagemResgateDetalhe": this.str.IdColagemResgateDetalhe = (string)value; break;							
						case "CodigoExterno": this.str.CodigoExterno = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "CotaOperacao": this.str.CotaOperacao = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "ValorCPMF": this.str.ValorCPMF = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "PrejuizoUsado": this.str.PrejuizoUsado = (string)value; break;							
						case "RendimentoResgate": this.str.RendimentoResgate = (string)value; break;							
						case "VariacaoResgate": this.str.VariacaoResgate = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdColagemResgateDetalhe":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemResgateDetalhe = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "CotaOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaOperacao = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "ValorCPMF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMF = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "PrejuizoUsado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoUsado = (System.Decimal?)value;
							break;
						
						case "RendimentoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoResgate = (System.Decimal?)value;
							break;
						
						case "VariacaoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VariacaoResgate = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.IdColagemResgateDetalhe
		/// </summary>
		virtual public System.Int32? IdColagemResgateDetalhe
		{
			get
			{
				return base.GetSystemInt32(ColagemResgateDetalheMetadata.ColumnNames.IdColagemResgateDetalhe);
			}
			
			set
			{
				base.SetSystemInt32(ColagemResgateDetalheMetadata.ColumnNames.IdColagemResgateDetalhe, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.CodigoExterno
		/// </summary>
		virtual public System.String CodigoExterno
		{
			get
			{
				return base.GetSystemString(ColagemResgateDetalheMetadata.ColumnNames.CodigoExterno);
			}
			
			set
			{
				base.SetSystemString(ColagemResgateDetalheMetadata.ColumnNames.CodigoExterno, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(ColagemResgateDetalheMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemResgateDetalheMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ColagemResgateDetalheMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(ColagemResgateDetalheMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(ColagemResgateDetalheMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(ColagemResgateDetalheMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(ColagemResgateDetalheMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemResgateDetalheMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.CotaOperacao
		/// </summary>
		virtual public System.Decimal? CotaOperacao
		{
			get
			{
				return base.GetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.CotaOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.CotaOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.ValorCPMF
		/// </summary>
		virtual public System.Decimal? ValorCPMF
		{
			get
			{
				return base.GetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.ValorCPMF);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.ValorCPMF, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.PrejuizoUsado
		/// </summary>
		virtual public System.Decimal? PrejuizoUsado
		{
			get
			{
				return base.GetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.PrejuizoUsado);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.PrejuizoUsado, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.RendimentoResgate
		/// </summary>
		virtual public System.Decimal? RendimentoResgate
		{
			get
			{
				return base.GetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.RendimentoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.RendimentoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.VariacaoResgate
		/// </summary>
		virtual public System.Decimal? VariacaoResgate
		{
			get
			{
				return base.GetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.VariacaoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.VariacaoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhe.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemResgateDetalheMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esColagemResgateDetalhe entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdColagemResgateDetalhe
			{
				get
				{
					System.Int32? data = entity.IdColagemResgateDetalhe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemResgateDetalhe = null;
					else entity.IdColagemResgateDetalhe = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoExterno
			{
				get
				{
					System.String data = entity.CodigoExterno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoExterno = null;
					else entity.CodigoExterno = Convert.ToString(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaOperacao
			{
				get
				{
					System.Decimal? data = entity.CotaOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaOperacao = null;
					else entity.CotaOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCPMF
			{
				get
				{
					System.Decimal? data = entity.ValorCPMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMF = null;
					else entity.ValorCPMF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrejuizoUsado
			{
				get
				{
					System.Decimal? data = entity.PrejuizoUsado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoUsado = null;
					else entity.PrejuizoUsado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoResgate
			{
				get
				{
					System.Decimal? data = entity.RendimentoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoResgate = null;
					else entity.RendimentoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String VariacaoResgate
			{
				get
				{
					System.Decimal? data = entity.VariacaoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VariacaoResgate = null;
					else entity.VariacaoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
			

			private esColagemResgateDetalhe entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esColagemResgateDetalheQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esColagemResgateDetalhe can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ColagemResgateDetalhe : esColagemResgateDetalhe
	{

				
		#region ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ColagemResgates_Det_Pos_FK
		/// </summary>

		[XmlIgnore]
		public ColagemResgateDetalhePosicaoCollection ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe
		{
			get
			{
				if(this._ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe == null)
				{
					this._ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe = new ColagemResgateDetalhePosicaoCollection();
					this._ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe", this._ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe);
				
					if(this.IdColagemResgateDetalhe != null)
					{
						this._ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe.Query.Where(this._ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe.Query.IdColagemResgateDetalhe == this.IdColagemResgateDetalhe);
						this._ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe.Query.Load();

						// Auto-hookup Foreign Keys
						this._ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe.fks.Add(ColagemResgateDetalhePosicaoMetadata.ColumnNames.IdColagemResgateDetalhe, this.IdColagemResgateDetalhe);
					}
				}

				return this._ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe != null) 
				{ 
					this.RemovePostSave("ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe"); 
					this._ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe = null;
					
				} 
			} 			
		}

		private ColagemResgateDetalhePosicaoCollection _ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe", typeof(ColagemResgateDetalhePosicaoCollection), new ColagemResgateDetalhePosicao()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe != null)
			{
				foreach(ColagemResgateDetalhePosicao obj in this._ColagemResgateDetalhePosicaoCollectionByIdColagemResgateDetalhe)
				{
					if(obj.es.IsAdded)
					{
						obj.IdColagemResgateDetalhe = this.IdColagemResgateDetalhe;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esColagemResgateDetalheQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ColagemResgateDetalheMetadata.Meta();
			}
		}	
		

		public esQueryItem IdColagemResgateDetalhe
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.IdColagemResgateDetalhe, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoExterno
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.CodigoExterno, esSystemType.String);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaOperacao
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.CotaOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCPMF
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.ValorCPMF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrejuizoUsado
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.PrejuizoUsado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoResgate
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.RendimentoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VariacaoResgate
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.VariacaoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalheMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ColagemResgateDetalheCollection")]
	public partial class ColagemResgateDetalheCollection : esColagemResgateDetalheCollection, IEnumerable<ColagemResgateDetalhe>
	{
		public ColagemResgateDetalheCollection()
		{

		}
		
		public static implicit operator List<ColagemResgateDetalhe>(ColagemResgateDetalheCollection coll)
		{
			List<ColagemResgateDetalhe> list = new List<ColagemResgateDetalhe>();
			
			foreach (ColagemResgateDetalhe emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ColagemResgateDetalheMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemResgateDetalheQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ColagemResgateDetalhe(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ColagemResgateDetalhe();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ColagemResgateDetalheQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemResgateDetalheQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ColagemResgateDetalheQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ColagemResgateDetalhe AddNew()
		{
			ColagemResgateDetalhe entity = base.AddNewEntity() as ColagemResgateDetalhe;
			
			return entity;
		}

		public ColagemResgateDetalhe FindByPrimaryKey(System.Int32 idColagemResgateDetalhe)
		{
			return base.FindByPrimaryKey(idColagemResgateDetalhe) as ColagemResgateDetalhe;
		}


		#region IEnumerable<ColagemResgateDetalhe> Members

		IEnumerator<ColagemResgateDetalhe> IEnumerable<ColagemResgateDetalhe>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ColagemResgateDetalhe;
			}
		}

		#endregion
		
		private ColagemResgateDetalheQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ColagemResgateDetalhe' table
	/// </summary>

	[Serializable]
	public partial class ColagemResgateDetalhe : esColagemResgateDetalhe
	{
		public ColagemResgateDetalhe()
		{

		}
	
		public ColagemResgateDetalhe(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ColagemResgateDetalheMetadata.Meta();
			}
		}
		
		
		
		override protected esColagemResgateDetalheQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemResgateDetalheQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ColagemResgateDetalheQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemResgateDetalheQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ColagemResgateDetalheQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ColagemResgateDetalheQuery query;
	}



	[Serializable]
	public partial class ColagemResgateDetalheQuery : esColagemResgateDetalheQuery
	{
		public ColagemResgateDetalheQuery()
		{

		}		
		
		public ColagemResgateDetalheQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ColagemResgateDetalheMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ColagemResgateDetalheMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.IdColagemResgateDetalhe, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.IdColagemResgateDetalhe;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.CodigoExterno, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.CodigoExterno;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.DataReferencia, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.IdCliente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.IdCarteira, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.DataConversao, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.Quantidade, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.CotaOperacao, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.CotaOperacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.ValorBruto, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.ValorLiquido, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.ValorCPMF, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.ValorCPMF;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.ValorPerformance, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.PrejuizoUsado, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.PrejuizoUsado;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.RendimentoResgate, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.RendimentoResgate;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.VariacaoResgate, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.VariacaoResgate;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.ValorIR, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalheMetadata.ColumnNames.ValorIOF, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemResgateDetalheMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ColagemResgateDetalheMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdColagemResgateDetalhe = "IdColagemResgateDetalhe";
			 public const string CodigoExterno = "CodigoExterno";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataConversao = "DataConversao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaOperacao = "CotaOperacao";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdColagemResgateDetalhe = "IdColagemResgateDetalhe";
			 public const string CodigoExterno = "CodigoExterno";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataConversao = "DataConversao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaOperacao = "CotaOperacao";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ColagemResgateDetalheMetadata))
			{
				if(ColagemResgateDetalheMetadata.mapDelegates == null)
				{
					ColagemResgateDetalheMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ColagemResgateDetalheMetadata.meta == null)
				{
					ColagemResgateDetalheMetadata.meta = new ColagemResgateDetalheMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdColagemResgateDetalhe", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoExterno", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCPMF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrejuizoUsado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VariacaoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "ColagemResgateDetalhe";
				meta.Destination = "ColagemResgateDetalhe";
				
				meta.spInsert = "proc_ColagemResgateDetalheInsert";				
				meta.spUpdate = "proc_ColagemResgateDetalheUpdate";		
				meta.spDelete = "proc_ColagemResgateDetalheDelete";
				meta.spLoadAll = "proc_ColagemResgateDetalheLoadAll";
				meta.spLoadByPrimaryKey = "proc_ColagemResgateDetalheLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ColagemResgateDetalheMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
