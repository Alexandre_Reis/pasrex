/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 25/03/2015 16:41:01
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.InvestidorCotista;


namespace Financial.Fundo
{

	[Serializable]
	abstract public class esSeriesOffShoreCollection : esEntityCollection
	{
		public esSeriesOffShoreCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SeriesOffShoreCollection";
		}

		#region Query Logic
		protected void InitQuery(esSeriesOffShoreQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSeriesOffShoreQuery);
		}
		#endregion
		
		virtual public SeriesOffShore DetachEntity(SeriesOffShore entity)
		{
			return base.DetachEntity(entity) as SeriesOffShore;
		}
		
		virtual public SeriesOffShore AttachEntity(SeriesOffShore entity)
		{
			return base.AttachEntity(entity) as SeriesOffShore;
		}
		
		virtual public void Combine(SeriesOffShoreCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SeriesOffShore this[int index]
		{
			get
			{
				return base[index] as SeriesOffShore;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SeriesOffShore);
		}
	}



	[Serializable]
	abstract public class esSeriesOffShore : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSeriesOffShoreQuery GetDynamicQuery()
		{
			return null;
		}

		public esSeriesOffShore()
		{

		}

		public esSeriesOffShore(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idSeriesOffShore)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idSeriesOffShore);
			else
				return LoadByPrimaryKeyStoredProcedure(idSeriesOffShore);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idSeriesOffShore)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idSeriesOffShore);
			else
				return LoadByPrimaryKeyStoredProcedure(idSeriesOffShore);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idSeriesOffShore)
		{
			esSeriesOffShoreQuery query = this.GetDynamicQuery();
			query.Where(query.IdSeriesOffShore == idSeriesOffShore);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idSeriesOffShore)
		{
			esParameters parms = new esParameters();
			parms.Add("IdSeriesOffShore",idSeriesOffShore);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdSeriesOffShore": this.str.IdSeriesOffShore = (string)value; break;							
						case "IdClassesOffShore": this.str.IdClassesOffShore = (string)value; break;							
						case "Mnemonico": this.str.Mnemonico = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "VlCotaInicial": this.str.VlCotaInicial = (string)value; break;							
						case "Isin": this.str.Isin = (string)value; break;							
						case "DataRollUp": this.str.DataRollUp = (string)value; break;							
						case "Inativa": this.str.Inativa = (string)value; break;							
						case "DataInicioAplicacao": this.str.DataInicioAplicacao = (string)value; break;							
						case "DataFimAplicacao": this.str.DataFimAplicacao = (string)value; break;							
						case "SerieBase": this.str.SerieBase = (string)value; break;							
						case "VlCotaBase": this.str.VlCotaBase = (string)value; break;							
						case "PrazoRepeticaoCotas": this.str.PrazoRepeticaoCotas = (string)value; break;							
						case "PrazoValidadePrevia": this.str.PrazoValidadePrevia = (string)value; break;							
						case "IdIndicePrevia": this.str.IdIndicePrevia = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdSeriesOffShore":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSeriesOffShore = (System.Int32?)value;
							break;
						
						case "IdClassesOffShore":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClassesOffShore = (System.Int32?)value;
							break;
						
						case "VlCotaInicial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlCotaInicial = (System.Decimal?)value;
							break;
						
						case "DataRollUp":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRollUp = (System.DateTime?)value;
							break;
						
						case "DataInicioAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicioAplicacao = (System.DateTime?)value;
							break;
						
						case "DataFimAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFimAplicacao = (System.DateTime?)value;
							break;
						
						case "VlCotaBase":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlCotaBase = (System.Decimal?)value;
							break;
						
						case "PrazoRepeticaoCotas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PrazoRepeticaoCotas = (System.Int32?)value;
							break;
						
						case "PrazoValidadePrevia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PrazoValidadePrevia = (System.Int32?)value;
							break;
						
						case "IdIndicePrevia":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndicePrevia = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SeriesOffShore.IdSeriesOffShore
		/// </summary>
		virtual public System.Int32? IdSeriesOffShore
		{
			get
			{
				return base.GetSystemInt32(SeriesOffShoreMetadata.ColumnNames.IdSeriesOffShore);
			}
			
			set
			{
				base.SetSystemInt32(SeriesOffShoreMetadata.ColumnNames.IdSeriesOffShore, value);
			}
		}
		
		/// <summary>
		/// Maps to SeriesOffShore.IdClassesOffShore
		/// </summary>
		virtual public System.Int32? IdClassesOffShore
		{
			get
			{
				return base.GetSystemInt32(SeriesOffShoreMetadata.ColumnNames.IdClassesOffShore);
			}
			
			set
			{
				if(base.SetSystemInt32(SeriesOffShoreMetadata.ColumnNames.IdClassesOffShore, value))
				{
					this._UpToClassesOffShoreByIdClassesOffShore = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SeriesOffShore.Mnemonico
		/// </summary>
		virtual public System.String Mnemonico
		{
			get
			{
				return base.GetSystemString(SeriesOffShoreMetadata.ColumnNames.Mnemonico);
			}
			
			set
			{
				base.SetSystemString(SeriesOffShoreMetadata.ColumnNames.Mnemonico, value);
			}
		}
		
		/// <summary>
		/// Maps to SeriesOffShore.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(SeriesOffShoreMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(SeriesOffShoreMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to SeriesOffShore.VlCotaInicial
		/// </summary>
		virtual public System.Decimal? VlCotaInicial
		{
			get
			{
				return base.GetSystemDecimal(SeriesOffShoreMetadata.ColumnNames.VlCotaInicial);
			}
			
			set
			{
				base.SetSystemDecimal(SeriesOffShoreMetadata.ColumnNames.VlCotaInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to SeriesOffShore.ISIN
		/// </summary>
		virtual public System.String Isin
		{
			get
			{
				return base.GetSystemString(SeriesOffShoreMetadata.ColumnNames.Isin);
			}
			
			set
			{
				base.SetSystemString(SeriesOffShoreMetadata.ColumnNames.Isin, value);
			}
		}
		
		/// <summary>
		/// Maps to SeriesOffShore.DataRollUp
		/// </summary>
		virtual public System.DateTime? DataRollUp
		{
			get
			{
				return base.GetSystemDateTime(SeriesOffShoreMetadata.ColumnNames.DataRollUp);
			}
			
			set
			{
				base.SetSystemDateTime(SeriesOffShoreMetadata.ColumnNames.DataRollUp, value);
			}
		}
		
		/// <summary>
		/// Maps to SeriesOffShore.Inativa
		/// </summary>
		virtual public System.String Inativa
		{
			get
			{
				return base.GetSystemString(SeriesOffShoreMetadata.ColumnNames.Inativa);
			}
			
			set
			{
				base.SetSystemString(SeriesOffShoreMetadata.ColumnNames.Inativa, value);
			}
		}
		
		/// <summary>
		/// Maps to SeriesOffShore.DataInicioAplicacao
		/// </summary>
		virtual public System.DateTime? DataInicioAplicacao
		{
			get
			{
				return base.GetSystemDateTime(SeriesOffShoreMetadata.ColumnNames.DataInicioAplicacao);
			}
			
			set
			{
				base.SetSystemDateTime(SeriesOffShoreMetadata.ColumnNames.DataInicioAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to SeriesOffShore.DataFimAplicacao
		/// </summary>
		virtual public System.DateTime? DataFimAplicacao
		{
			get
			{
				return base.GetSystemDateTime(SeriesOffShoreMetadata.ColumnNames.DataFimAplicacao);
			}
			
			set
			{
				base.SetSystemDateTime(SeriesOffShoreMetadata.ColumnNames.DataFimAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to SeriesOffShore.SerieBase
		/// </summary>
		virtual public System.String SerieBase
		{
			get
			{
				return base.GetSystemString(SeriesOffShoreMetadata.ColumnNames.SerieBase);
			}
			
			set
			{
				base.SetSystemString(SeriesOffShoreMetadata.ColumnNames.SerieBase, value);
			}
		}
		
		/// <summary>
		/// Maps to SeriesOffShore.VlCotaBase
		/// </summary>
		virtual public System.Decimal? VlCotaBase
		{
			get
			{
				return base.GetSystemDecimal(SeriesOffShoreMetadata.ColumnNames.VlCotaBase);
			}
			
			set
			{
				base.SetSystemDecimal(SeriesOffShoreMetadata.ColumnNames.VlCotaBase, value);
			}
		}
		
		/// <summary>
		/// Maps to SeriesOffShore.PrazoRepeticaoCotas
		/// </summary>
		virtual public System.Int32? PrazoRepeticaoCotas
		{
			get
			{
				return base.GetSystemInt32(SeriesOffShoreMetadata.ColumnNames.PrazoRepeticaoCotas);
			}
			
			set
			{
				base.SetSystemInt32(SeriesOffShoreMetadata.ColumnNames.PrazoRepeticaoCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to SeriesOffShore.PrazoValidadePrevia
		/// </summary>
		virtual public System.Int32? PrazoValidadePrevia
		{
			get
			{
				return base.GetSystemInt32(SeriesOffShoreMetadata.ColumnNames.PrazoValidadePrevia);
			}
			
			set
			{
				base.SetSystemInt32(SeriesOffShoreMetadata.ColumnNames.PrazoValidadePrevia, value);
			}
		}
		
		/// <summary>
		/// Maps to SeriesOffShore.IdIndicePrevia
		/// </summary>
		virtual public System.Int16? IdIndicePrevia
		{
			get
			{
				return base.GetSystemInt16(SeriesOffShoreMetadata.ColumnNames.IdIndicePrevia);
			}
			
			set
			{
				base.SetSystemInt16(SeriesOffShoreMetadata.ColumnNames.IdIndicePrevia, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected ClassesOffShore _UpToClassesOffShoreByIdClassesOffShore;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSeriesOffShore entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdSeriesOffShore
			{
				get
				{
					System.Int32? data = entity.IdSeriesOffShore;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSeriesOffShore = null;
					else entity.IdSeriesOffShore = Convert.ToInt32(value);
				}
			}
				
			public System.String IdClassesOffShore
			{
				get
				{
					System.Int32? data = entity.IdClassesOffShore;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClassesOffShore = null;
					else entity.IdClassesOffShore = Convert.ToInt32(value);
				}
			}
				
			public System.String Mnemonico
			{
				get
				{
					System.String data = entity.Mnemonico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Mnemonico = null;
					else entity.Mnemonico = Convert.ToString(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String VlCotaInicial
			{
				get
				{
					System.Decimal? data = entity.VlCotaInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlCotaInicial = null;
					else entity.VlCotaInicial = Convert.ToDecimal(value);
				}
			}
				
			public System.String Isin
			{
				get
				{
					System.String data = entity.Isin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Isin = null;
					else entity.Isin = Convert.ToString(value);
				}
			}
				
			public System.String DataRollUp
			{
				get
				{
					System.DateTime? data = entity.DataRollUp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRollUp = null;
					else entity.DataRollUp = Convert.ToDateTime(value);
				}
			}
				
			public System.String Inativa
			{
				get
				{
					System.String data = entity.Inativa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Inativa = null;
					else entity.Inativa = Convert.ToString(value);
				}
			}
				
			public System.String DataInicioAplicacao
			{
				get
				{
					System.DateTime? data = entity.DataInicioAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicioAplicacao = null;
					else entity.DataInicioAplicacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataFimAplicacao
			{
				get
				{
					System.DateTime? data = entity.DataFimAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFimAplicacao = null;
					else entity.DataFimAplicacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String SerieBase
			{
				get
				{
					System.String data = entity.SerieBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SerieBase = null;
					else entity.SerieBase = Convert.ToString(value);
				}
			}
				
			public System.String VlCotaBase
			{
				get
				{
					System.Decimal? data = entity.VlCotaBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlCotaBase = null;
					else entity.VlCotaBase = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrazoRepeticaoCotas
			{
				get
				{
					System.Int32? data = entity.PrazoRepeticaoCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoRepeticaoCotas = null;
					else entity.PrazoRepeticaoCotas = Convert.ToInt32(value);
				}
			}
				
			public System.String PrazoValidadePrevia
			{
				get
				{
					System.Int32? data = entity.PrazoValidadePrevia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoValidadePrevia = null;
					else entity.PrazoValidadePrevia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdIndicePrevia
			{
				get
				{
					System.Int16? data = entity.IdIndicePrevia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndicePrevia = null;
					else entity.IdIndicePrevia = Convert.ToInt16(value);
				}
			}
			

			private esSeriesOffShore entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSeriesOffShoreQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSeriesOffShore can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SeriesOffShore : esSeriesOffShore
	{

				
		#region EventoRollUpCollectionByIdSerieOrigem - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EventoRollUp_SerieDest_FK
		/// </summary>

		[XmlIgnore]
		public EventoRollUpCollection EventoRollUpCollectionByIdSerieOrigem
		{
			get
			{
				if(this._EventoRollUpCollectionByIdSerieOrigem == null)
				{
					this._EventoRollUpCollectionByIdSerieOrigem = new EventoRollUpCollection();
					this._EventoRollUpCollectionByIdSerieOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoRollUpCollectionByIdSerieOrigem", this._EventoRollUpCollectionByIdSerieOrigem);
				
					if(this.IdSeriesOffShore != null)
					{
						this._EventoRollUpCollectionByIdSerieOrigem.Query.Where(this._EventoRollUpCollectionByIdSerieOrigem.Query.IdSerieOrigem == this.IdSeriesOffShore);
						this._EventoRollUpCollectionByIdSerieOrigem.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoRollUpCollectionByIdSerieOrigem.fks.Add(EventoRollUpMetadata.ColumnNames.IdSerieOrigem, this.IdSeriesOffShore);
					}
				}

				return this._EventoRollUpCollectionByIdSerieOrigem;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoRollUpCollectionByIdSerieOrigem != null) 
				{ 
					this.RemovePostSave("EventoRollUpCollectionByIdSerieOrigem"); 
					this._EventoRollUpCollectionByIdSerieOrigem = null;
					
				} 
			} 			
		}

		private EventoRollUpCollection _EventoRollUpCollectionByIdSerieOrigem;
		#endregion

				
		#region EventoRollUpCollectionByIdSerieDestino - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EventoRollUp_SerieOrig_FK
		/// </summary>

		[XmlIgnore]
		public EventoRollUpCollection EventoRollUpCollectionByIdSerieDestino
		{
			get
			{
				if(this._EventoRollUpCollectionByIdSerieDestino == null)
				{
					this._EventoRollUpCollectionByIdSerieDestino = new EventoRollUpCollection();
					this._EventoRollUpCollectionByIdSerieDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoRollUpCollectionByIdSerieDestino", this._EventoRollUpCollectionByIdSerieDestino);
				
					if(this.IdSeriesOffShore != null)
					{
						this._EventoRollUpCollectionByIdSerieDestino.Query.Where(this._EventoRollUpCollectionByIdSerieDestino.Query.IdSerieDestino == this.IdSeriesOffShore);
						this._EventoRollUpCollectionByIdSerieDestino.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoRollUpCollectionByIdSerieDestino.fks.Add(EventoRollUpMetadata.ColumnNames.IdSerieDestino, this.IdSeriesOffShore);
					}
				}

				return this._EventoRollUpCollectionByIdSerieDestino;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoRollUpCollectionByIdSerieDestino != null) 
				{ 
					this.RemovePostSave("EventoRollUpCollectionByIdSerieDestino"); 
					this._EventoRollUpCollectionByIdSerieDestino = null;
					
				} 
			} 			
		}

		private EventoRollUpCollection _EventoRollUpCollectionByIdSerieDestino;
		#endregion

				
		#region TransferenciaSerieCollectionByIdSerieOrigem - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TransSerie_SerieDest_FK
		/// </summary>

		[XmlIgnore]
		public TransferenciaSerieCollection TransferenciaSerieCollectionByIdSerieOrigem
		{
			get
			{
				if(this._TransferenciaSerieCollectionByIdSerieOrigem == null)
				{
					this._TransferenciaSerieCollectionByIdSerieOrigem = new TransferenciaSerieCollection();
					this._TransferenciaSerieCollectionByIdSerieOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaSerieCollectionByIdSerieOrigem", this._TransferenciaSerieCollectionByIdSerieOrigem);
				
					if(this.IdSeriesOffShore != null)
					{
						this._TransferenciaSerieCollectionByIdSerieOrigem.Query.Where(this._TransferenciaSerieCollectionByIdSerieOrigem.Query.IdSerieOrigem == this.IdSeriesOffShore);
						this._TransferenciaSerieCollectionByIdSerieOrigem.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaSerieCollectionByIdSerieOrigem.fks.Add(TransferenciaSerieMetadata.ColumnNames.IdSerieOrigem, this.IdSeriesOffShore);
					}
				}

				return this._TransferenciaSerieCollectionByIdSerieOrigem;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaSerieCollectionByIdSerieOrigem != null) 
				{ 
					this.RemovePostSave("TransferenciaSerieCollectionByIdSerieOrigem"); 
					this._TransferenciaSerieCollectionByIdSerieOrigem = null;
					
				} 
			} 			
		}

		private TransferenciaSerieCollection _TransferenciaSerieCollectionByIdSerieOrigem;
		#endregion

				
		#region TransferenciaSerieCollectionByIdSerieDestino - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TransSerie_SerieOrig_FK
		/// </summary>

		[XmlIgnore]
		public TransferenciaSerieCollection TransferenciaSerieCollectionByIdSerieDestino
		{
			get
			{
				if(this._TransferenciaSerieCollectionByIdSerieDestino == null)
				{
					this._TransferenciaSerieCollectionByIdSerieDestino = new TransferenciaSerieCollection();
					this._TransferenciaSerieCollectionByIdSerieDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaSerieCollectionByIdSerieDestino", this._TransferenciaSerieCollectionByIdSerieDestino);
				
					if(this.IdSeriesOffShore != null)
					{
						this._TransferenciaSerieCollectionByIdSerieDestino.Query.Where(this._TransferenciaSerieCollectionByIdSerieDestino.Query.IdSerieDestino == this.IdSeriesOffShore);
						this._TransferenciaSerieCollectionByIdSerieDestino.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaSerieCollectionByIdSerieDestino.fks.Add(TransferenciaSerieMetadata.ColumnNames.IdSerieDestino, this.IdSeriesOffShore);
					}
				}

				return this._TransferenciaSerieCollectionByIdSerieDestino;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaSerieCollectionByIdSerieDestino != null) 
				{ 
					this.RemovePostSave("TransferenciaSerieCollectionByIdSerieDestino"); 
					this._TransferenciaSerieCollectionByIdSerieDestino = null;
					
				} 
			} 			
		}

		private TransferenciaSerieCollection _TransferenciaSerieCollectionByIdSerieDestino;
		#endregion

				
		#region UpToClassesOffShoreByIdClassesOffShore - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Serie_Classes_FK1
		/// </summary>

		[XmlIgnore]
		public ClassesOffShore UpToClassesOffShoreByIdClassesOffShore
		{
			get
			{
				if(this._UpToClassesOffShoreByIdClassesOffShore == null
					&& IdClassesOffShore != null					)
				{
					this._UpToClassesOffShoreByIdClassesOffShore = new ClassesOffShore();
					this._UpToClassesOffShoreByIdClassesOffShore.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClassesOffShoreByIdClassesOffShore", this._UpToClassesOffShoreByIdClassesOffShore);
					this._UpToClassesOffShoreByIdClassesOffShore.Query.Where(this._UpToClassesOffShoreByIdClassesOffShore.Query.IdClassesOffShore == this.IdClassesOffShore);
					this._UpToClassesOffShoreByIdClassesOffShore.Query.Load();
				}

				return this._UpToClassesOffShoreByIdClassesOffShore;
			}
			
			set
			{
				this.RemovePreSave("UpToClassesOffShoreByIdClassesOffShore");
				

				if(value == null)
				{
					this.IdClassesOffShore = null;
					this._UpToClassesOffShoreByIdClassesOffShore = null;
				}
				else
				{
					this.IdClassesOffShore = value.IdClassesOffShore;
					this._UpToClassesOffShoreByIdClassesOffShore = value;
					this.SetPreSave("UpToClassesOffShoreByIdClassesOffShore", this._UpToClassesOffShoreByIdClassesOffShore);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "EventoRollUpCollectionByIdSerieOrigem", typeof(EventoRollUpCollection), new EventoRollUp()));
			props.Add(new esPropertyDescriptor(this, "EventoRollUpCollectionByIdSerieDestino", typeof(EventoRollUpCollection), new EventoRollUp()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaSerieCollectionByIdSerieOrigem", typeof(TransferenciaSerieCollection), new TransferenciaSerie()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaSerieCollectionByIdSerieDestino", typeof(TransferenciaSerieCollection), new TransferenciaSerie()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._EventoRollUpCollectionByIdSerieOrigem != null)
			{
				foreach(EventoRollUp obj in this._EventoRollUpCollectionByIdSerieOrigem)
				{
					if(obj.es.IsAdded)
					{
						obj.IdSerieOrigem = this.IdSeriesOffShore;
					}
				}
			}
			if(this._EventoRollUpCollectionByIdSerieDestino != null)
			{
				foreach(EventoRollUp obj in this._EventoRollUpCollectionByIdSerieDestino)
				{
					if(obj.es.IsAdded)
					{
						obj.IdSerieDestino = this.IdSeriesOffShore;
					}
				}
			}
			if(this._TransferenciaSerieCollectionByIdSerieOrigem != null)
			{
				foreach(TransferenciaSerie obj in this._TransferenciaSerieCollectionByIdSerieOrigem)
				{
					if(obj.es.IsAdded)
					{
						obj.IdSerieOrigem = this.IdSeriesOffShore;
					}
				}
			}
			if(this._TransferenciaSerieCollectionByIdSerieDestino != null)
			{
				foreach(TransferenciaSerie obj in this._TransferenciaSerieCollectionByIdSerieDestino)
				{
					if(obj.es.IsAdded)
					{
						obj.IdSerieDestino = this.IdSeriesOffShore;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSeriesOffShoreQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SeriesOffShoreMetadata.Meta();
			}
		}	
		

		public esQueryItem IdSeriesOffShore
		{
			get
			{
				return new esQueryItem(this, SeriesOffShoreMetadata.ColumnNames.IdSeriesOffShore, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdClassesOffShore
		{
			get
			{
				return new esQueryItem(this, SeriesOffShoreMetadata.ColumnNames.IdClassesOffShore, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Mnemonico
		{
			get
			{
				return new esQueryItem(this, SeriesOffShoreMetadata.ColumnNames.Mnemonico, esSystemType.String);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, SeriesOffShoreMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem VlCotaInicial
		{
			get
			{
				return new esQueryItem(this, SeriesOffShoreMetadata.ColumnNames.VlCotaInicial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Isin
		{
			get
			{
				return new esQueryItem(this, SeriesOffShoreMetadata.ColumnNames.Isin, esSystemType.String);
			}
		} 
		
		public esQueryItem DataRollUp
		{
			get
			{
				return new esQueryItem(this, SeriesOffShoreMetadata.ColumnNames.DataRollUp, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Inativa
		{
			get
			{
				return new esQueryItem(this, SeriesOffShoreMetadata.ColumnNames.Inativa, esSystemType.String);
			}
		} 
		
		public esQueryItem DataInicioAplicacao
		{
			get
			{
				return new esQueryItem(this, SeriesOffShoreMetadata.ColumnNames.DataInicioAplicacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataFimAplicacao
		{
			get
			{
				return new esQueryItem(this, SeriesOffShoreMetadata.ColumnNames.DataFimAplicacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem SerieBase
		{
			get
			{
				return new esQueryItem(this, SeriesOffShoreMetadata.ColumnNames.SerieBase, esSystemType.String);
			}
		} 
		
		public esQueryItem VlCotaBase
		{
			get
			{
				return new esQueryItem(this, SeriesOffShoreMetadata.ColumnNames.VlCotaBase, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrazoRepeticaoCotas
		{
			get
			{
				return new esQueryItem(this, SeriesOffShoreMetadata.ColumnNames.PrazoRepeticaoCotas, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PrazoValidadePrevia
		{
			get
			{
				return new esQueryItem(this, SeriesOffShoreMetadata.ColumnNames.PrazoValidadePrevia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdIndicePrevia
		{
			get
			{
				return new esQueryItem(this, SeriesOffShoreMetadata.ColumnNames.IdIndicePrevia, esSystemType.Int16);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SeriesOffShoreCollection")]
	public partial class SeriesOffShoreCollection : esSeriesOffShoreCollection, IEnumerable<SeriesOffShore>
	{
		public SeriesOffShoreCollection()
		{

		}
		
		public static implicit operator List<SeriesOffShore>(SeriesOffShoreCollection coll)
		{
			List<SeriesOffShore> list = new List<SeriesOffShore>();
			
			foreach (SeriesOffShore emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SeriesOffShoreMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SeriesOffShoreQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SeriesOffShore(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SeriesOffShore();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SeriesOffShoreQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SeriesOffShoreQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SeriesOffShoreQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SeriesOffShore AddNew()
		{
			SeriesOffShore entity = base.AddNewEntity() as SeriesOffShore;
			
			return entity;
		}

		public SeriesOffShore FindByPrimaryKey(System.Int32 idSeriesOffShore)
		{
			return base.FindByPrimaryKey(idSeriesOffShore) as SeriesOffShore;
		}


		#region IEnumerable<SeriesOffShore> Members

		IEnumerator<SeriesOffShore> IEnumerable<SeriesOffShore>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SeriesOffShore;
			}
		}

		#endregion
		
		private SeriesOffShoreQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SeriesOffShore' table
	/// </summary>

	[Serializable]
	public partial class SeriesOffShore : esSeriesOffShore
	{
		public SeriesOffShore()
		{

		}
	
		public SeriesOffShore(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SeriesOffShoreMetadata.Meta();
			}
		}
		
		
		
		override protected esSeriesOffShoreQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SeriesOffShoreQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SeriesOffShoreQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SeriesOffShoreQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SeriesOffShoreQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SeriesOffShoreQuery query;
	}



	[Serializable]
	public partial class SeriesOffShoreQuery : esSeriesOffShoreQuery
	{
		public SeriesOffShoreQuery()
		{

		}		
		
		public SeriesOffShoreQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SeriesOffShoreMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SeriesOffShoreMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SeriesOffShoreMetadata.ColumnNames.IdSeriesOffShore, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SeriesOffShoreMetadata.PropertyNames.IdSeriesOffShore;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SeriesOffShoreMetadata.ColumnNames.IdClassesOffShore, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SeriesOffShoreMetadata.PropertyNames.IdClassesOffShore;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SeriesOffShoreMetadata.ColumnNames.Mnemonico, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = SeriesOffShoreMetadata.PropertyNames.Mnemonico;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SeriesOffShoreMetadata.ColumnNames.Descricao, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = SeriesOffShoreMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SeriesOffShoreMetadata.ColumnNames.VlCotaInicial, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = SeriesOffShoreMetadata.PropertyNames.VlCotaInicial;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SeriesOffShoreMetadata.ColumnNames.Isin, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = SeriesOffShoreMetadata.PropertyNames.Isin;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SeriesOffShoreMetadata.ColumnNames.DataRollUp, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SeriesOffShoreMetadata.PropertyNames.DataRollUp;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SeriesOffShoreMetadata.ColumnNames.Inativa, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = SeriesOffShoreMetadata.PropertyNames.Inativa;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SeriesOffShoreMetadata.ColumnNames.DataInicioAplicacao, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SeriesOffShoreMetadata.PropertyNames.DataInicioAplicacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SeriesOffShoreMetadata.ColumnNames.DataFimAplicacao, 9, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SeriesOffShoreMetadata.PropertyNames.DataFimAplicacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SeriesOffShoreMetadata.ColumnNames.SerieBase, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = SeriesOffShoreMetadata.PropertyNames.SerieBase;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SeriesOffShoreMetadata.ColumnNames.VlCotaBase, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = SeriesOffShoreMetadata.PropertyNames.VlCotaBase;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SeriesOffShoreMetadata.ColumnNames.PrazoRepeticaoCotas, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SeriesOffShoreMetadata.PropertyNames.PrazoRepeticaoCotas;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SeriesOffShoreMetadata.ColumnNames.PrazoValidadePrevia, 13, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SeriesOffShoreMetadata.PropertyNames.PrazoValidadePrevia;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SeriesOffShoreMetadata.ColumnNames.IdIndicePrevia, 14, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = SeriesOffShoreMetadata.PropertyNames.IdIndicePrevia;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SeriesOffShoreMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdSeriesOffShore = "IdSeriesOffShore";
			 public const string IdClassesOffShore = "IdClassesOffShore";
			 public const string Mnemonico = "Mnemonico";
			 public const string Descricao = "Descricao";
			 public const string VlCotaInicial = "VlCotaInicial";
			 public const string Isin = "ISIN";
			 public const string DataRollUp = "DataRollUp";
			 public const string Inativa = "Inativa";
			 public const string DataInicioAplicacao = "DataInicioAplicacao";
			 public const string DataFimAplicacao = "DataFimAplicacao";
			 public const string SerieBase = "SerieBase";
			 public const string VlCotaBase = "VlCotaBase";
			 public const string PrazoRepeticaoCotas = "PrazoRepeticaoCotas";
			 public const string PrazoValidadePrevia = "PrazoValidadePrevia";
			 public const string IdIndicePrevia = "IdIndicePrevia";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdSeriesOffShore = "IdSeriesOffShore";
			 public const string IdClassesOffShore = "IdClassesOffShore";
			 public const string Mnemonico = "Mnemonico";
			 public const string Descricao = "Descricao";
			 public const string VlCotaInicial = "VlCotaInicial";
			 public const string Isin = "Isin";
			 public const string DataRollUp = "DataRollUp";
			 public const string Inativa = "Inativa";
			 public const string DataInicioAplicacao = "DataInicioAplicacao";
			 public const string DataFimAplicacao = "DataFimAplicacao";
			 public const string SerieBase = "SerieBase";
			 public const string VlCotaBase = "VlCotaBase";
			 public const string PrazoRepeticaoCotas = "PrazoRepeticaoCotas";
			 public const string PrazoValidadePrevia = "PrazoValidadePrevia";
			 public const string IdIndicePrevia = "IdIndicePrevia";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SeriesOffShoreMetadata))
			{
				if(SeriesOffShoreMetadata.mapDelegates == null)
				{
					SeriesOffShoreMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SeriesOffShoreMetadata.meta == null)
				{
					SeriesOffShoreMetadata.meta = new SeriesOffShoreMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdSeriesOffShore", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdClassesOffShore", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Mnemonico", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VlCotaInicial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ISIN", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataRollUp", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Inativa", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DataInicioAplicacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataFimAplicacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("SerieBase", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("VlCotaBase", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrazoRepeticaoCotas", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PrazoValidadePrevia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdIndicePrevia", new esTypeMap("smallint", "System.Int16"));			
				
				
				
				meta.Source = "SeriesOffShore";
				meta.Destination = "SeriesOffShore";
				
				meta.spInsert = "proc_SeriesOffShoreInsert";				
				meta.spUpdate = "proc_SeriesOffShoreUpdate";		
				meta.spDelete = "proc_SeriesOffShoreDelete";
				meta.spLoadAll = "proc_SeriesOffShoreLoadAll";
				meta.spLoadByPrimaryKey = "proc_SeriesOffShoreLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SeriesOffShoreMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
