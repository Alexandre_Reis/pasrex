/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 22/04/2016 12:19:26
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esImplantacaoPosicaoCotistaCollection : esEntityCollection
	{
		public esImplantacaoPosicaoCotistaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ImplantacaoPosicaoCotistaCollection";
		}

		#region Query Logic
		protected void InitQuery(esImplantacaoPosicaoCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esImplantacaoPosicaoCotistaQuery);
		}
		#endregion
		
		virtual public ImplantacaoPosicaoCotista DetachEntity(ImplantacaoPosicaoCotista entity)
		{
			return base.DetachEntity(entity) as ImplantacaoPosicaoCotista;
		}
		
		virtual public ImplantacaoPosicaoCotista AttachEntity(ImplantacaoPosicaoCotista entity)
		{
			return base.AttachEntity(entity) as ImplantacaoPosicaoCotista;
		}
		
		virtual public void Combine(ImplantacaoPosicaoCotistaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ImplantacaoPosicaoCotista this[int index]
		{
			get
			{
				return base[index] as ImplantacaoPosicaoCotista;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ImplantacaoPosicaoCotista);
		}
	}



	[Serializable]
	abstract public class esImplantacaoPosicaoCotista : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esImplantacaoPosicaoCotistaQuery GetDynamicQuery()
		{
			return null;
		}

		public esImplantacaoPosicaoCotista()
		{

		}

		public esImplantacaoPosicaoCotista(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idImplantacaoPosicaoCotista)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idImplantacaoPosicaoCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idImplantacaoPosicaoCotista);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idImplantacaoPosicaoCotista)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idImplantacaoPosicaoCotista);
			else
				return LoadByPrimaryKeyStoredProcedure(idImplantacaoPosicaoCotista);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idImplantacaoPosicaoCotista)
		{
			esImplantacaoPosicaoCotistaQuery query = this.GetDynamicQuery();
			query.Where(query.IdImplantacaoPosicaoCotista == idImplantacaoPosicaoCotista);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idImplantacaoPosicaoCotista)
		{
			esParameters parms = new esParameters();
			parms.Add("IdImplantacaoPosicaoCotista",idImplantacaoPosicaoCotista);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdImplantacaoPosicaoCotista": this.str.IdImplantacaoPosicaoCotista = (string)value; break;							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "ValorAplicacao": this.str.ValorAplicacao = (string)value; break;							
						case "DataAplicacao": this.str.DataAplicacao = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "CotaAplicacao": this.str.CotaAplicacao = (string)value; break;							
						case "DataUltimaCobrancaIR": this.str.DataUltimaCobrancaIR = (string)value; break;							
						case "DataUltimoCortePfee": this.str.DataUltimoCortePfee = (string)value; break;							
						case "DataLiquidacao": this.str.DataLiquidacao = (string)value; break;							
						case "Status": this.str.Status = (string)value; break;							
						case "DataImportacao": this.str.DataImportacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdImplantacaoPosicaoCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdImplantacaoPosicaoCotista = (System.Int32?)value;
							break;
						
						case "IdCotista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCotista = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "ValorAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAplicacao = (System.Decimal?)value;
							break;
						
						case "DataAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAplicacao = (System.DateTime?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "CotaAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaAplicacao = (System.Decimal?)value;
							break;
						
						case "DataUltimaCobrancaIR":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimaCobrancaIR = (System.DateTime?)value;
							break;
						
						case "DataUltimoCortePfee":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimoCortePfee = (System.DateTime?)value;
							break;
						
						case "DataLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiquidacao = (System.DateTime?)value;
							break;
						
						case "DataImportacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataImportacao = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ImplantacaoPosicaoCotista.IdImplantacaoPosicaoCotista
		/// </summary>
		virtual public System.Int32? IdImplantacaoPosicaoCotista
		{
			get
			{
				return base.GetSystemInt32(ImplantacaoPosicaoCotistaMetadata.ColumnNames.IdImplantacaoPosicaoCotista);
			}
			
			set
			{
				base.SetSystemInt32(ImplantacaoPosicaoCotistaMetadata.ColumnNames.IdImplantacaoPosicaoCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to ImplantacaoPosicaoCotista.IdCotista
		/// </summary>
		virtual public System.Int32? IdCotista
		{
			get
			{
				return base.GetSystemInt32(ImplantacaoPosicaoCotistaMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				base.SetSystemInt32(ImplantacaoPosicaoCotistaMetadata.ColumnNames.IdCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to ImplantacaoPosicaoCotista.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(ImplantacaoPosicaoCotistaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(ImplantacaoPosicaoCotistaMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to ImplantacaoPosicaoCotista.ValorAplicacao
		/// </summary>
		virtual public System.Decimal? ValorAplicacao
		{
			get
			{
				return base.GetSystemDecimal(ImplantacaoPosicaoCotistaMetadata.ColumnNames.ValorAplicacao);
			}
			
			set
			{
				base.SetSystemDecimal(ImplantacaoPosicaoCotistaMetadata.ColumnNames.ValorAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ImplantacaoPosicaoCotista.DataAplicacao
		/// </summary>
		virtual public System.DateTime? DataAplicacao
		{
			get
			{
				return base.GetSystemDateTime(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataAplicacao);
			}
			
			set
			{
				base.SetSystemDateTime(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ImplantacaoPosicaoCotista.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to ImplantacaoPosicaoCotista.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(ImplantacaoPosicaoCotistaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(ImplantacaoPosicaoCotistaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to ImplantacaoPosicaoCotista.CotaAplicacao
		/// </summary>
		virtual public System.Decimal? CotaAplicacao
		{
			get
			{
				return base.GetSystemDecimal(ImplantacaoPosicaoCotistaMetadata.ColumnNames.CotaAplicacao);
			}
			
			set
			{
				base.SetSystemDecimal(ImplantacaoPosicaoCotistaMetadata.ColumnNames.CotaAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ImplantacaoPosicaoCotista.DataUltimaCobrancaIR
		/// </summary>
		virtual public System.DateTime? DataUltimaCobrancaIR
		{
			get
			{
				return base.GetSystemDateTime(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataUltimaCobrancaIR);
			}
			
			set
			{
				base.SetSystemDateTime(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataUltimaCobrancaIR, value);
			}
		}
		
		/// <summary>
		/// Maps to ImplantacaoPosicaoCotista.DataUltimoCortePfee
		/// </summary>
		virtual public System.DateTime? DataUltimoCortePfee
		{
			get
			{
				return base.GetSystemDateTime(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataUltimoCortePfee);
			}
			
			set
			{
				base.SetSystemDateTime(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataUltimoCortePfee, value);
			}
		}
		
		/// <summary>
		/// Maps to ImplantacaoPosicaoCotista.DataLiquidacao
		/// </summary>
		virtual public System.DateTime? DataLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ImplantacaoPosicaoCotista.Status
		/// </summary>
		virtual public System.String Status
		{
			get
			{
				return base.GetSystemString(ImplantacaoPosicaoCotistaMetadata.ColumnNames.Status);
			}
			
			set
			{
				base.SetSystemString(ImplantacaoPosicaoCotistaMetadata.ColumnNames.Status, value);
			}
		}
		
		/// <summary>
		/// Maps to ImplantacaoPosicaoCotista.DataImportacao
		/// </summary>
		virtual public System.DateTime? DataImportacao
		{
			get
			{
				return base.GetSystemDateTime(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataImportacao);
			}
			
			set
			{
				base.SetSystemDateTime(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataImportacao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esImplantacaoPosicaoCotista entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdImplantacaoPosicaoCotista
			{
				get
				{
					System.Int32? data = entity.IdImplantacaoPosicaoCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdImplantacaoPosicaoCotista = null;
					else entity.IdImplantacaoPosicaoCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCotista
			{
				get
				{
					System.Int32? data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorAplicacao
			{
				get
				{
					System.Decimal? data = entity.ValorAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAplicacao = null;
					else entity.ValorAplicacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataAplicacao
			{
				get
				{
					System.DateTime? data = entity.DataAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAplicacao = null;
					else entity.DataAplicacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaAplicacao
			{
				get
				{
					System.Decimal? data = entity.CotaAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaAplicacao = null;
					else entity.CotaAplicacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataUltimaCobrancaIR
			{
				get
				{
					System.DateTime? data = entity.DataUltimaCobrancaIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimaCobrancaIR = null;
					else entity.DataUltimaCobrancaIR = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataUltimoCortePfee
			{
				get
				{
					System.DateTime? data = entity.DataUltimoCortePfee;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimoCortePfee = null;
					else entity.DataUltimoCortePfee = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DataLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiquidacao = null;
					else entity.DataLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String Status
			{
				get
				{
					System.String data = entity.Status;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Status = null;
					else entity.Status = Convert.ToString(value);
				}
			}
				
			public System.String DataImportacao
			{
				get
				{
					System.DateTime? data = entity.DataImportacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataImportacao = null;
					else entity.DataImportacao = Convert.ToDateTime(value);
				}
			}
			

			private esImplantacaoPosicaoCotista entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esImplantacaoPosicaoCotistaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esImplantacaoPosicaoCotista can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ImplantacaoPosicaoCotista : esImplantacaoPosicaoCotista
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esImplantacaoPosicaoCotistaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ImplantacaoPosicaoCotistaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdImplantacaoPosicaoCotista
		{
			get
			{
				return new esQueryItem(this, ImplantacaoPosicaoCotistaMetadata.ColumnNames.IdImplantacaoPosicaoCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, ImplantacaoPosicaoCotistaMetadata.ColumnNames.IdCotista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, ImplantacaoPosicaoCotistaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorAplicacao
		{
			get
			{
				return new esQueryItem(this, ImplantacaoPosicaoCotistaMetadata.ColumnNames.ValorAplicacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataAplicacao
		{
			get
			{
				return new esQueryItem(this, ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataAplicacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, ImplantacaoPosicaoCotistaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaAplicacao
		{
			get
			{
				return new esQueryItem(this, ImplantacaoPosicaoCotistaMetadata.ColumnNames.CotaAplicacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataUltimaCobrancaIR
		{
			get
			{
				return new esQueryItem(this, ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataUltimaCobrancaIR, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataUltimoCortePfee
		{
			get
			{
				return new esQueryItem(this, ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataUltimoCortePfee, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataLiquidacao
		{
			get
			{
				return new esQueryItem(this, ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Status
		{
			get
			{
				return new esQueryItem(this, ImplantacaoPosicaoCotistaMetadata.ColumnNames.Status, esSystemType.String);
			}
		} 
		
		public esQueryItem DataImportacao
		{
			get
			{
				return new esQueryItem(this, ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataImportacao, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ImplantacaoPosicaoCotistaCollection")]
	public partial class ImplantacaoPosicaoCotistaCollection : esImplantacaoPosicaoCotistaCollection, IEnumerable<ImplantacaoPosicaoCotista>
	{
		public ImplantacaoPosicaoCotistaCollection()
		{

		}
		
		public static implicit operator List<ImplantacaoPosicaoCotista>(ImplantacaoPosicaoCotistaCollection coll)
		{
			List<ImplantacaoPosicaoCotista> list = new List<ImplantacaoPosicaoCotista>();
			
			foreach (ImplantacaoPosicaoCotista emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ImplantacaoPosicaoCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ImplantacaoPosicaoCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ImplantacaoPosicaoCotista(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ImplantacaoPosicaoCotista();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ImplantacaoPosicaoCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ImplantacaoPosicaoCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ImplantacaoPosicaoCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ImplantacaoPosicaoCotista AddNew()
		{
			ImplantacaoPosicaoCotista entity = base.AddNewEntity() as ImplantacaoPosicaoCotista;
			
			return entity;
		}

		public ImplantacaoPosicaoCotista FindByPrimaryKey(System.Int32 idImplantacaoPosicaoCotista)
		{
			return base.FindByPrimaryKey(idImplantacaoPosicaoCotista) as ImplantacaoPosicaoCotista;
		}


		#region IEnumerable<ImplantacaoPosicaoCotista> Members

		IEnumerator<ImplantacaoPosicaoCotista> IEnumerable<ImplantacaoPosicaoCotista>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ImplantacaoPosicaoCotista;
			}
		}

		#endregion
		
		private ImplantacaoPosicaoCotistaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ImplantacaoPosicaoCotista' table
	/// </summary>

	[Serializable]
	public partial class ImplantacaoPosicaoCotista : esImplantacaoPosicaoCotista
	{
		public ImplantacaoPosicaoCotista()
		{

		}
	
		public ImplantacaoPosicaoCotista(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ImplantacaoPosicaoCotistaMetadata.Meta();
			}
		}
		
		
		
		override protected esImplantacaoPosicaoCotistaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ImplantacaoPosicaoCotistaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ImplantacaoPosicaoCotistaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ImplantacaoPosicaoCotistaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ImplantacaoPosicaoCotistaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ImplantacaoPosicaoCotistaQuery query;
	}



	[Serializable]
	public partial class ImplantacaoPosicaoCotistaQuery : esImplantacaoPosicaoCotistaQuery
	{
		public ImplantacaoPosicaoCotistaQuery()
		{

		}		
		
		public ImplantacaoPosicaoCotistaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ImplantacaoPosicaoCotistaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ImplantacaoPosicaoCotistaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ImplantacaoPosicaoCotistaMetadata.ColumnNames.IdImplantacaoPosicaoCotista, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ImplantacaoPosicaoCotistaMetadata.PropertyNames.IdImplantacaoPosicaoCotista;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ImplantacaoPosicaoCotistaMetadata.ColumnNames.IdCotista, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ImplantacaoPosicaoCotistaMetadata.PropertyNames.IdCotista;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ImplantacaoPosicaoCotistaMetadata.ColumnNames.IdCarteira, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ImplantacaoPosicaoCotistaMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ImplantacaoPosicaoCotistaMetadata.ColumnNames.ValorAplicacao, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ImplantacaoPosicaoCotistaMetadata.PropertyNames.ValorAplicacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataAplicacao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ImplantacaoPosicaoCotistaMetadata.PropertyNames.DataAplicacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataConversao, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ImplantacaoPosicaoCotistaMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ImplantacaoPosicaoCotistaMetadata.ColumnNames.Quantidade, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ImplantacaoPosicaoCotistaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ImplantacaoPosicaoCotistaMetadata.ColumnNames.CotaAplicacao, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ImplantacaoPosicaoCotistaMetadata.PropertyNames.CotaAplicacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataUltimaCobrancaIR, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ImplantacaoPosicaoCotistaMetadata.PropertyNames.DataUltimaCobrancaIR;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataUltimoCortePfee, 9, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ImplantacaoPosicaoCotistaMetadata.PropertyNames.DataUltimoCortePfee;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataLiquidacao, 10, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ImplantacaoPosicaoCotistaMetadata.PropertyNames.DataLiquidacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ImplantacaoPosicaoCotistaMetadata.ColumnNames.Status, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = ImplantacaoPosicaoCotistaMetadata.PropertyNames.Status;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ImplantacaoPosicaoCotistaMetadata.ColumnNames.DataImportacao, 12, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ImplantacaoPosicaoCotistaMetadata.PropertyNames.DataImportacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ImplantacaoPosicaoCotistaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdImplantacaoPosicaoCotista = "IdImplantacaoPosicaoCotista";
			 public const string IdCotista = "IdCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorAplicacao = "ValorAplicacao";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string DataConversao = "DataConversao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaAplicacao = "CotaAplicacao";
			 public const string DataUltimaCobrancaIR = "DataUltimaCobrancaIR";
			 public const string DataUltimoCortePfee = "DataUltimoCortePfee";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string Status = "Status";
			 public const string DataImportacao = "DataImportacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdImplantacaoPosicaoCotista = "IdImplantacaoPosicaoCotista";
			 public const string IdCotista = "IdCotista";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorAplicacao = "ValorAplicacao";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string DataConversao = "DataConversao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaAplicacao = "CotaAplicacao";
			 public const string DataUltimaCobrancaIR = "DataUltimaCobrancaIR";
			 public const string DataUltimoCortePfee = "DataUltimoCortePfee";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string Status = "Status";
			 public const string DataImportacao = "DataImportacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ImplantacaoPosicaoCotistaMetadata))
			{
				if(ImplantacaoPosicaoCotistaMetadata.mapDelegates == null)
				{
					ImplantacaoPosicaoCotistaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ImplantacaoPosicaoCotistaMetadata.meta == null)
				{
					ImplantacaoPosicaoCotistaMetadata.meta = new ImplantacaoPosicaoCotistaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdImplantacaoPosicaoCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCotista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorAplicacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataAplicacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaAplicacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataUltimaCobrancaIR", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataUltimoCortePfee", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataLiquidacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Status", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DataImportacao", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "ImplantacaoPosicaoCotista";
				meta.Destination = "ImplantacaoPosicaoCotista";
				
				meta.spInsert = "proc_ImplantacaoPosicaoCotistaInsert";				
				meta.spUpdate = "proc_ImplantacaoPosicaoCotistaUpdate";		
				meta.spDelete = "proc_ImplantacaoPosicaoCotistaDelete";
				meta.spLoadAll = "proc_ImplantacaoPosicaoCotistaLoadAll";
				meta.spLoadByPrimaryKey = "proc_ImplantacaoPosicaoCotistaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ImplantacaoPosicaoCotistaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
