/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/14/2015 5:20:58 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	












		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTabelaInformeDesempenhoCollection : esEntityCollection
	{
		public esTabelaInformeDesempenhoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaInformeDesempenhoCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaInformeDesempenhoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaInformeDesempenhoQuery);
		}
		#endregion
		
		virtual public TabelaInformeDesempenho DetachEntity(TabelaInformeDesempenho entity)
		{
			return base.DetachEntity(entity) as TabelaInformeDesempenho;
		}
		
		virtual public TabelaInformeDesempenho AttachEntity(TabelaInformeDesempenho entity)
		{
			return base.AttachEntity(entity) as TabelaInformeDesempenho;
		}
		
		virtual public void Combine(TabelaInformeDesempenhoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaInformeDesempenho this[int index]
		{
			get
			{
				return base[index] as TabelaInformeDesempenho;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaInformeDesempenho);
		}
	}



	[Serializable]
	abstract public class esTabelaInformeDesempenho : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaInformeDesempenhoQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaInformeDesempenho()
		{

		}

		public esTabelaInformeDesempenho(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCarteira, System.DateTime data)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira, data);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira, data);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCarteira, System.DateTime data)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaInformeDesempenhoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCarteira == idCarteira, query.Data == data);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCarteira, System.DateTime data)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira, data);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira, data);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCarteira, System.DateTime data)
		{
			esTabelaInformeDesempenhoQuery query = this.GetDynamicQuery();
			query.Where(query.IdCarteira == idCarteira, query.Data == data);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCarteira, System.DateTime data)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCarteira",idCarteira);			parms.Add("Data",data);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "ValorAdmAdministrador": this.str.ValorAdmAdministrador = (string)value; break;							
						case "ValorPfeeAdministrador": this.str.ValorPfeeAdministrador = (string)value; break;							
						case "ValorOutrasAdministrador": this.str.ValorOutrasAdministrador = (string)value; break;							
						case "ValorAdmGestor": this.str.ValorAdmGestor = (string)value; break;							
						case "ValorPfeeGestor": this.str.ValorPfeeGestor = (string)value; break;							
						case "ValorOutrasGestor": this.str.ValorOutrasGestor = (string)value; break;							
						case "ValorAdministracaoPaga": this.str.ValorAdministracaoPaga = (string)value; break;							
						case "ValorPfeePaga": this.str.ValorPfeePaga = (string)value; break;							
						case "ValorCustodiaPaga": this.str.ValorCustodiaPaga = (string)value; break;							
						case "ValorOutrasDespesasPagas": this.str.ValorOutrasDespesasPagas = (string)value; break;							
						case "ValorAdministracaoPagaGrupoEconomicoADM": this.str.ValorAdministracaoPagaGrupoEconomicoADM = (string)value; break;							
						case "ValorDespesasOperacionaisGrupoEconomicoADM": this.str.ValorDespesasOperacionaisGrupoEconomicoADM = (string)value; break;							
						case "ValorAdministracaoPagaGrupoEconomicoGestor": this.str.ValorAdministracaoPagaGrupoEconomicoGestor = (string)value; break;							
						case "ValorDespesasOperacionaisGrupoEconomicoGestor": this.str.ValorDespesasOperacionaisGrupoEconomicoGestor = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "ValorAdmAdministrador":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAdmAdministrador = (System.Decimal?)value;
							break;
						
						case "ValorPfeeAdministrador":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPfeeAdministrador = (System.Decimal?)value;
							break;
						
						case "ValorOutrasAdministrador":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorOutrasAdministrador = (System.Decimal?)value;
							break;
						
						case "ValorAdmGestor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAdmGestor = (System.Decimal?)value;
							break;
						
						case "ValorPfeeGestor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPfeeGestor = (System.Decimal?)value;
							break;
						
						case "ValorOutrasGestor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorOutrasGestor = (System.Decimal?)value;
							break;
						
						case "ValorAdministracaoPaga":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAdministracaoPaga = (System.Decimal?)value;
							break;
						
						case "ValorPfeePaga":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPfeePaga = (System.Decimal?)value;
							break;
						
						case "ValorCustodiaPaga":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCustodiaPaga = (System.Decimal?)value;
							break;
						
						case "ValorOutrasDespesasPagas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorOutrasDespesasPagas = (System.Decimal?)value;
							break;
						
						case "ValorAdministracaoPagaGrupoEconomicoADM":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAdministracaoPagaGrupoEconomicoADM = (System.Decimal?)value;
							break;
						
						case "ValorDespesasOperacionaisGrupoEconomicoADM":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorDespesasOperacionaisGrupoEconomicoADM = (System.Decimal?)value;
							break;
						
						case "ValorAdministracaoPagaGrupoEconomicoGestor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAdministracaoPagaGrupoEconomicoGestor = (System.Decimal?)value;
							break;
						
						case "ValorDespesasOperacionaisGrupoEconomicoGestor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorDespesasOperacionaisGrupoEconomicoGestor = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(TabelaInformeDesempenhoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(TabelaInformeDesempenhoMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(TabelaInformeDesempenhoMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaInformeDesempenhoMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.ValorAdmAdministrador
		/// </summary>
		virtual public System.Decimal? ValorAdmAdministrador
		{
			get
			{
				return base.GetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdmAdministrador);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdmAdministrador, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.ValorPfeeAdministrador
		/// </summary>
		virtual public System.Decimal? ValorPfeeAdministrador
		{
			get
			{
				return base.GetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorPfeeAdministrador);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorPfeeAdministrador, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.ValorOutrasAdministrador
		/// </summary>
		virtual public System.Decimal? ValorOutrasAdministrador
		{
			get
			{
				return base.GetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorOutrasAdministrador);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorOutrasAdministrador, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.ValorAdmGestor
		/// </summary>
		virtual public System.Decimal? ValorAdmGestor
		{
			get
			{
				return base.GetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdmGestor);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdmGestor, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.ValorPfeeGestor
		/// </summary>
		virtual public System.Decimal? ValorPfeeGestor
		{
			get
			{
				return base.GetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorPfeeGestor);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorPfeeGestor, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.ValorOutrasGestor
		/// </summary>
		virtual public System.Decimal? ValorOutrasGestor
		{
			get
			{
				return base.GetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorOutrasGestor);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorOutrasGestor, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.ValorAdministracaoPaga
		/// </summary>
		virtual public System.Decimal? ValorAdministracaoPaga
		{
			get
			{
				return base.GetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdministracaoPaga);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdministracaoPaga, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.ValorPfeePaga
		/// </summary>
		virtual public System.Decimal? ValorPfeePaga
		{
			get
			{
				return base.GetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorPfeePaga);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorPfeePaga, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.ValorCustodiaPaga
		/// </summary>
		virtual public System.Decimal? ValorCustodiaPaga
		{
			get
			{
				return base.GetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorCustodiaPaga);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorCustodiaPaga, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.ValorOutrasDespesasPagas
		/// </summary>
		virtual public System.Decimal? ValorOutrasDespesasPagas
		{
			get
			{
				return base.GetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorOutrasDespesasPagas);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorOutrasDespesasPagas, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.ValorAdministracaoPagaGrupoEconomicoADM
		/// </summary>
		virtual public System.Decimal? ValorAdministracaoPagaGrupoEconomicoADM
		{
			get
			{
				return base.GetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdministracaoPagaGrupoEconomicoADM);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdministracaoPagaGrupoEconomicoADM, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.ValorDespesasOperacionaisGrupoEconomicoADM
		/// </summary>
		virtual public System.Decimal? ValorDespesasOperacionaisGrupoEconomicoADM
		{
			get
			{
				return base.GetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorDespesasOperacionaisGrupoEconomicoADM);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorDespesasOperacionaisGrupoEconomicoADM, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.ValorAdministracaoPagaGrupoEconomicoGestor
		/// </summary>
		virtual public System.Decimal? ValorAdministracaoPagaGrupoEconomicoGestor
		{
			get
			{
				return base.GetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdministracaoPagaGrupoEconomicoGestor);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdministracaoPagaGrupoEconomicoGestor, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaInformeDesempenho.ValorDespesasOperacionaisGrupoEconomicoGestor
		/// </summary>
		virtual public System.Decimal? ValorDespesasOperacionaisGrupoEconomicoGestor
		{
			get
			{
				return base.GetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorDespesasOperacionaisGrupoEconomicoGestor);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaInformeDesempenhoMetadata.ColumnNames.ValorDespesasOperacionaisGrupoEconomicoGestor, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaInformeDesempenho entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorAdmAdministrador
			{
				get
				{
					System.Decimal? data = entity.ValorAdmAdministrador;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAdmAdministrador = null;
					else entity.ValorAdmAdministrador = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPfeeAdministrador
			{
				get
				{
					System.Decimal? data = entity.ValorPfeeAdministrador;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPfeeAdministrador = null;
					else entity.ValorPfeeAdministrador = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorOutrasAdministrador
			{
				get
				{
					System.Decimal? data = entity.ValorOutrasAdministrador;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorOutrasAdministrador = null;
					else entity.ValorOutrasAdministrador = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorAdmGestor
			{
				get
				{
					System.Decimal? data = entity.ValorAdmGestor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAdmGestor = null;
					else entity.ValorAdmGestor = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPfeeGestor
			{
				get
				{
					System.Decimal? data = entity.ValorPfeeGestor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPfeeGestor = null;
					else entity.ValorPfeeGestor = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorOutrasGestor
			{
				get
				{
					System.Decimal? data = entity.ValorOutrasGestor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorOutrasGestor = null;
					else entity.ValorOutrasGestor = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorAdministracaoPaga
			{
				get
				{
					System.Decimal? data = entity.ValorAdministracaoPaga;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAdministracaoPaga = null;
					else entity.ValorAdministracaoPaga = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPfeePaga
			{
				get
				{
					System.Decimal? data = entity.ValorPfeePaga;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPfeePaga = null;
					else entity.ValorPfeePaga = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCustodiaPaga
			{
				get
				{
					System.Decimal? data = entity.ValorCustodiaPaga;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCustodiaPaga = null;
					else entity.ValorCustodiaPaga = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorOutrasDespesasPagas
			{
				get
				{
					System.Decimal? data = entity.ValorOutrasDespesasPagas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorOutrasDespesasPagas = null;
					else entity.ValorOutrasDespesasPagas = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorAdministracaoPagaGrupoEconomicoADM
			{
				get
				{
					System.Decimal? data = entity.ValorAdministracaoPagaGrupoEconomicoADM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAdministracaoPagaGrupoEconomicoADM = null;
					else entity.ValorAdministracaoPagaGrupoEconomicoADM = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorDespesasOperacionaisGrupoEconomicoADM
			{
				get
				{
					System.Decimal? data = entity.ValorDespesasOperacionaisGrupoEconomicoADM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDespesasOperacionaisGrupoEconomicoADM = null;
					else entity.ValorDespesasOperacionaisGrupoEconomicoADM = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorAdministracaoPagaGrupoEconomicoGestor
			{
				get
				{
					System.Decimal? data = entity.ValorAdministracaoPagaGrupoEconomicoGestor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAdministracaoPagaGrupoEconomicoGestor = null;
					else entity.ValorAdministracaoPagaGrupoEconomicoGestor = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorDespesasOperacionaisGrupoEconomicoGestor
			{
				get
				{
					System.Decimal? data = entity.ValorDespesasOperacionaisGrupoEconomicoGestor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDespesasOperacionaisGrupoEconomicoGestor = null;
					else entity.ValorDespesasOperacionaisGrupoEconomicoGestor = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaInformeDesempenho entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaInformeDesempenhoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaInformeDesempenho can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaInformeDesempenho : esTabelaInformeDesempenho
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaInformeDesempenhoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaInformeDesempenhoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorAdmAdministrador
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdmAdministrador, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPfeeAdministrador
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.ValorPfeeAdministrador, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorOutrasAdministrador
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.ValorOutrasAdministrador, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorAdmGestor
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdmGestor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPfeeGestor
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.ValorPfeeGestor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorOutrasGestor
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.ValorOutrasGestor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorAdministracaoPaga
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdministracaoPaga, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPfeePaga
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.ValorPfeePaga, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCustodiaPaga
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.ValorCustodiaPaga, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorOutrasDespesasPagas
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.ValorOutrasDespesasPagas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorAdministracaoPagaGrupoEconomicoADM
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdministracaoPagaGrupoEconomicoADM, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorDespesasOperacionaisGrupoEconomicoADM
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.ValorDespesasOperacionaisGrupoEconomicoADM, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorAdministracaoPagaGrupoEconomicoGestor
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdministracaoPagaGrupoEconomicoGestor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorDespesasOperacionaisGrupoEconomicoGestor
		{
			get
			{
				return new esQueryItem(this, TabelaInformeDesempenhoMetadata.ColumnNames.ValorDespesasOperacionaisGrupoEconomicoGestor, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaInformeDesempenhoCollection")]
	public partial class TabelaInformeDesempenhoCollection : esTabelaInformeDesempenhoCollection, IEnumerable<TabelaInformeDesempenho>
	{
		public TabelaInformeDesempenhoCollection()
		{

		}
		
		public static implicit operator List<TabelaInformeDesempenho>(TabelaInformeDesempenhoCollection coll)
		{
			List<TabelaInformeDesempenho> list = new List<TabelaInformeDesempenho>();
			
			foreach (TabelaInformeDesempenho emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaInformeDesempenhoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaInformeDesempenhoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaInformeDesempenho(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaInformeDesempenho();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaInformeDesempenhoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaInformeDesempenhoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaInformeDesempenhoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaInformeDesempenho AddNew()
		{
			TabelaInformeDesempenho entity = base.AddNewEntity() as TabelaInformeDesempenho;
			
			return entity;
		}

		public TabelaInformeDesempenho FindByPrimaryKey(System.Int32 idCarteira, System.DateTime data)
		{
			return base.FindByPrimaryKey(idCarteira, data) as TabelaInformeDesempenho;
		}


		#region IEnumerable<TabelaInformeDesempenho> Members

		IEnumerator<TabelaInformeDesempenho> IEnumerable<TabelaInformeDesempenho>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaInformeDesempenho;
			}
		}

		#endregion
		
		private TabelaInformeDesempenhoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaInformeDesempenho' table
	/// </summary>

	[Serializable]
	public partial class TabelaInformeDesempenho : esTabelaInformeDesempenho
	{
		public TabelaInformeDesempenho()
		{

		}
	
		public TabelaInformeDesempenho(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaInformeDesempenhoMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaInformeDesempenhoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaInformeDesempenhoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaInformeDesempenhoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaInformeDesempenhoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaInformeDesempenhoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaInformeDesempenhoQuery query;
	}



	[Serializable]
	public partial class TabelaInformeDesempenhoQuery : esTabelaInformeDesempenhoQuery
	{
		public TabelaInformeDesempenhoQuery()
		{

		}		
		
		public TabelaInformeDesempenhoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaInformeDesempenhoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaInformeDesempenhoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.IdCarteira, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdmAdministrador, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.ValorAdmAdministrador;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.ValorPfeeAdministrador, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.ValorPfeeAdministrador;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.ValorOutrasAdministrador, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.ValorOutrasAdministrador;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdmGestor, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.ValorAdmGestor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.ValorPfeeGestor, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.ValorPfeeGestor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.ValorOutrasGestor, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.ValorOutrasGestor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdministracaoPaga, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.ValorAdministracaoPaga;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0.0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.ValorPfeePaga, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.ValorPfeePaga;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0.0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.ValorCustodiaPaga, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.ValorCustodiaPaga;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0.0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.ValorOutrasDespesasPagas, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.ValorOutrasDespesasPagas;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0.0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdministracaoPagaGrupoEconomicoADM, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.ValorAdministracaoPagaGrupoEconomicoADM;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0.0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.ValorDespesasOperacionaisGrupoEconomicoADM, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.ValorDespesasOperacionaisGrupoEconomicoADM;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0.0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.ValorAdministracaoPagaGrupoEconomicoGestor, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.ValorAdministracaoPagaGrupoEconomicoGestor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0.0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaInformeDesempenhoMetadata.ColumnNames.ValorDespesasOperacionaisGrupoEconomicoGestor, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaInformeDesempenhoMetadata.PropertyNames.ValorDespesasOperacionaisGrupoEconomicoGestor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0.0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaInformeDesempenhoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string Data = "Data";
			 public const string ValorAdmAdministrador = "ValorAdmAdministrador";
			 public const string ValorPfeeAdministrador = "ValorPfeeAdministrador";
			 public const string ValorOutrasAdministrador = "ValorOutrasAdministrador";
			 public const string ValorAdmGestor = "ValorAdmGestor";
			 public const string ValorPfeeGestor = "ValorPfeeGestor";
			 public const string ValorOutrasGestor = "ValorOutrasGestor";
			 public const string ValorAdministracaoPaga = "ValorAdministracaoPaga";
			 public const string ValorPfeePaga = "ValorPfeePaga";
			 public const string ValorCustodiaPaga = "ValorCustodiaPaga";
			 public const string ValorOutrasDespesasPagas = "ValorOutrasDespesasPagas";
			 public const string ValorAdministracaoPagaGrupoEconomicoADM = "ValorAdministracaoPagaGrupoEconomicoADM";
			 public const string ValorDespesasOperacionaisGrupoEconomicoADM = "ValorDespesasOperacionaisGrupoEconomicoADM";
			 public const string ValorAdministracaoPagaGrupoEconomicoGestor = "ValorAdministracaoPagaGrupoEconomicoGestor";
			 public const string ValorDespesasOperacionaisGrupoEconomicoGestor = "ValorDespesasOperacionaisGrupoEconomicoGestor";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string Data = "Data";
			 public const string ValorAdmAdministrador = "ValorAdmAdministrador";
			 public const string ValorPfeeAdministrador = "ValorPfeeAdministrador";
			 public const string ValorOutrasAdministrador = "ValorOutrasAdministrador";
			 public const string ValorAdmGestor = "ValorAdmGestor";
			 public const string ValorPfeeGestor = "ValorPfeeGestor";
			 public const string ValorOutrasGestor = "ValorOutrasGestor";
			 public const string ValorAdministracaoPaga = "ValorAdministracaoPaga";
			 public const string ValorPfeePaga = "ValorPfeePaga";
			 public const string ValorCustodiaPaga = "ValorCustodiaPaga";
			 public const string ValorOutrasDespesasPagas = "ValorOutrasDespesasPagas";
			 public const string ValorAdministracaoPagaGrupoEconomicoADM = "ValorAdministracaoPagaGrupoEconomicoADM";
			 public const string ValorDespesasOperacionaisGrupoEconomicoADM = "ValorDespesasOperacionaisGrupoEconomicoADM";
			 public const string ValorAdministracaoPagaGrupoEconomicoGestor = "ValorAdministracaoPagaGrupoEconomicoGestor";
			 public const string ValorDespesasOperacionaisGrupoEconomicoGestor = "ValorDespesasOperacionaisGrupoEconomicoGestor";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaInformeDesempenhoMetadata))
			{
				if(TabelaInformeDesempenhoMetadata.mapDelegates == null)
				{
					TabelaInformeDesempenhoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaInformeDesempenhoMetadata.meta == null)
				{
					TabelaInformeDesempenhoMetadata.meta = new TabelaInformeDesempenhoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorAdmAdministrador", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPfeeAdministrador", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorOutrasAdministrador", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorAdmGestor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPfeeGestor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorOutrasGestor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorAdministracaoPaga", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPfeePaga", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCustodiaPaga", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorOutrasDespesasPagas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorAdministracaoPagaGrupoEconomicoADM", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorDespesasOperacionaisGrupoEconomicoADM", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorAdministracaoPagaGrupoEconomicoGestor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorDespesasOperacionaisGrupoEconomicoGestor", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaInformeDesempenho";
				meta.Destination = "TabelaInformeDesempenho";
				
				meta.spInsert = "proc_TabelaInformeDesempenhoInsert";				
				meta.spUpdate = "proc_TabelaInformeDesempenhoUpdate";		
				meta.spDelete = "proc_TabelaInformeDesempenhoDelete";
				meta.spLoadAll = "proc_TabelaInformeDesempenhoLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaInformeDesempenhoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaInformeDesempenhoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
