/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/09/2015 17:46:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esColagemResgateMapaFundoCollection : esEntityCollection
	{
		public esColagemResgateMapaFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ColagemResgateMapaFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esColagemResgateMapaFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esColagemResgateMapaFundoQuery);
		}
		#endregion
		
		virtual public ColagemResgateMapaFundo DetachEntity(ColagemResgateMapaFundo entity)
		{
			return base.DetachEntity(entity) as ColagemResgateMapaFundo;
		}
		
		virtual public ColagemResgateMapaFundo AttachEntity(ColagemResgateMapaFundo entity)
		{
			return base.AttachEntity(entity) as ColagemResgateMapaFundo;
		}
		
		virtual public void Combine(ColagemResgateMapaFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ColagemResgateMapaFundo this[int index]
		{
			get
			{
				return base[index] as ColagemResgateMapaFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ColagemResgateMapaFundo);
		}
	}



	[Serializable]
	abstract public class esColagemResgateMapaFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esColagemResgateMapaFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esColagemResgateMapaFundo()
		{

		}

		public esColagemResgateMapaFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idColagemResgateMapaFundo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemResgateMapaFundo);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemResgateMapaFundo);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idColagemResgateMapaFundo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemResgateMapaFundo);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemResgateMapaFundo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idColagemResgateMapaFundo)
		{
			esColagemResgateMapaFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdColagemResgateMapaFundo == idColagemResgateMapaFundo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idColagemResgateMapaFundo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdColagemResgateMapaFundo",idColagemResgateMapaFundo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdColagemResgateMapaFundo": this.str.IdColagemResgateMapaFundo = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "DetalheImportado": this.str.DetalheImportado = (string)value; break;							
						case "IdOperacaoResgateVinculada": this.str.IdOperacaoResgateVinculada = (string)value; break;							
						case "IdColagemResgateDetalhe": this.str.IdColagemResgateDetalhe = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdColagemResgateMapaFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemResgateMapaFundo = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "IdOperacaoResgateVinculada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoResgateVinculada = (System.Int32?)value;
							break;
						
						case "IdColagemResgateDetalhe":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemResgateDetalhe = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ColagemResgateMapaFundo.IdColagemResgateMapaFundo
		/// </summary>
		virtual public System.Int32? IdColagemResgateMapaFundo
		{
			get
			{
				return base.GetSystemInt32(ColagemResgateMapaFundoMetadata.ColumnNames.IdColagemResgateMapaFundo);
			}
			
			set
			{
				base.SetSystemInt32(ColagemResgateMapaFundoMetadata.ColumnNames.IdColagemResgateMapaFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateMapaFundo.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(ColagemResgateMapaFundoMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemResgateMapaFundoMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateMapaFundo.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ColagemResgateMapaFundoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(ColagemResgateMapaFundoMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateMapaFundo.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(ColagemResgateMapaFundoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(ColagemResgateMapaFundoMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateMapaFundo.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(ColagemResgateMapaFundoMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemResgateMapaFundoMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateMapaFundo.DetalheImportado
		/// </summary>
		virtual public System.String DetalheImportado
		{
			get
			{
				return base.GetSystemString(ColagemResgateMapaFundoMetadata.ColumnNames.DetalheImportado);
			}
			
			set
			{
				base.SetSystemString(ColagemResgateMapaFundoMetadata.ColumnNames.DetalheImportado, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateMapaFundo.IdOperacaoResgateVinculada
		/// </summary>
		virtual public System.Int32? IdOperacaoResgateVinculada
		{
			get
			{
				return base.GetSystemInt32(ColagemResgateMapaFundoMetadata.ColumnNames.IdOperacaoResgateVinculada);
			}
			
			set
			{
				base.SetSystemInt32(ColagemResgateMapaFundoMetadata.ColumnNames.IdOperacaoResgateVinculada, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateMapaFundo.IdColagemResgateDetalhe
		/// </summary>
		virtual public System.Int32? IdColagemResgateDetalhe
		{
			get
			{
				return base.GetSystemInt32(ColagemResgateMapaFundoMetadata.ColumnNames.IdColagemResgateDetalhe);
			}
			
			set
			{
				base.SetSystemInt32(ColagemResgateMapaFundoMetadata.ColumnNames.IdColagemResgateDetalhe, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esColagemResgateMapaFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdColagemResgateMapaFundo
			{
				get
				{
					System.Int32? data = entity.IdColagemResgateMapaFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemResgateMapaFundo = null;
					else entity.IdColagemResgateMapaFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DetalheImportado
			{
				get
				{
					System.String data = entity.DetalheImportado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DetalheImportado = null;
					else entity.DetalheImportado = Convert.ToString(value);
				}
			}
				
			public System.String IdOperacaoResgateVinculada
			{
				get
				{
					System.Int32? data = entity.IdOperacaoResgateVinculada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoResgateVinculada = null;
					else entity.IdOperacaoResgateVinculada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdColagemResgateDetalhe
			{
				get
				{
					System.Int32? data = entity.IdColagemResgateDetalhe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemResgateDetalhe = null;
					else entity.IdColagemResgateDetalhe = Convert.ToInt32(value);
				}
			}
			

			private esColagemResgateMapaFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esColagemResgateMapaFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esColagemResgateMapaFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ColagemResgateMapaFundo : esColagemResgateMapaFundo
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esColagemResgateMapaFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ColagemResgateMapaFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdColagemResgateMapaFundo
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaFundoMetadata.ColumnNames.IdColagemResgateMapaFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaFundoMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaFundoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaFundoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaFundoMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DetalheImportado
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaFundoMetadata.ColumnNames.DetalheImportado, esSystemType.String);
			}
		} 
		
		public esQueryItem IdOperacaoResgateVinculada
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaFundoMetadata.ColumnNames.IdOperacaoResgateVinculada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdColagemResgateDetalhe
		{
			get
			{
				return new esQueryItem(this, ColagemResgateMapaFundoMetadata.ColumnNames.IdColagemResgateDetalhe, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ColagemResgateMapaFundoCollection")]
	public partial class ColagemResgateMapaFundoCollection : esColagemResgateMapaFundoCollection, IEnumerable<ColagemResgateMapaFundo>
	{
		public ColagemResgateMapaFundoCollection()
		{

		}
		
		public static implicit operator List<ColagemResgateMapaFundo>(ColagemResgateMapaFundoCollection coll)
		{
			List<ColagemResgateMapaFundo> list = new List<ColagemResgateMapaFundo>();
			
			foreach (ColagemResgateMapaFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ColagemResgateMapaFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemResgateMapaFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ColagemResgateMapaFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ColagemResgateMapaFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ColagemResgateMapaFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemResgateMapaFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ColagemResgateMapaFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ColagemResgateMapaFundo AddNew()
		{
			ColagemResgateMapaFundo entity = base.AddNewEntity() as ColagemResgateMapaFundo;
			
			return entity;
		}

		public ColagemResgateMapaFundo FindByPrimaryKey(System.Int32 idColagemResgateMapaFundo)
		{
			return base.FindByPrimaryKey(idColagemResgateMapaFundo) as ColagemResgateMapaFundo;
		}


		#region IEnumerable<ColagemResgateMapaFundo> Members

		IEnumerator<ColagemResgateMapaFundo> IEnumerable<ColagemResgateMapaFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ColagemResgateMapaFundo;
			}
		}

		#endregion
		
		private ColagemResgateMapaFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ColagemResgateMapaFundo' table
	/// </summary>

	[Serializable]
	public partial class ColagemResgateMapaFundo : esColagemResgateMapaFundo
	{
		public ColagemResgateMapaFundo()
		{

		}
	
		public ColagemResgateMapaFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ColagemResgateMapaFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esColagemResgateMapaFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemResgateMapaFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ColagemResgateMapaFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemResgateMapaFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ColagemResgateMapaFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ColagemResgateMapaFundoQuery query;
	}



	[Serializable]
	public partial class ColagemResgateMapaFundoQuery : esColagemResgateMapaFundoQuery
	{
		public ColagemResgateMapaFundoQuery()
		{

		}		
		
		public ColagemResgateMapaFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ColagemResgateMapaFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ColagemResgateMapaFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ColagemResgateMapaFundoMetadata.ColumnNames.IdColagemResgateMapaFundo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemResgateMapaFundoMetadata.PropertyNames.IdColagemResgateMapaFundo;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateMapaFundoMetadata.ColumnNames.DataReferencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemResgateMapaFundoMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateMapaFundoMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemResgateMapaFundoMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateMapaFundoMetadata.ColumnNames.IdCarteira, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemResgateMapaFundoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateMapaFundoMetadata.ColumnNames.DataConversao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemResgateMapaFundoMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateMapaFundoMetadata.ColumnNames.DetalheImportado, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = ColagemResgateMapaFundoMetadata.PropertyNames.DetalheImportado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateMapaFundoMetadata.ColumnNames.IdOperacaoResgateVinculada, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemResgateMapaFundoMetadata.PropertyNames.IdOperacaoResgateVinculada;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateMapaFundoMetadata.ColumnNames.IdColagemResgateDetalhe, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemResgateMapaFundoMetadata.PropertyNames.IdColagemResgateDetalhe;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ColagemResgateMapaFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdColagemResgateMapaFundo = "IdColagemResgateMapaFundo";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataConversao = "DataConversao";
			 public const string DetalheImportado = "DetalheImportado";
			 public const string IdOperacaoResgateVinculada = "IdOperacaoResgateVinculada";
			 public const string IdColagemResgateDetalhe = "IdColagemResgateDetalhe";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdColagemResgateMapaFundo = "IdColagemResgateMapaFundo";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataConversao = "DataConversao";
			 public const string DetalheImportado = "DetalheImportado";
			 public const string IdOperacaoResgateVinculada = "IdOperacaoResgateVinculada";
			 public const string IdColagemResgateDetalhe = "IdColagemResgateDetalhe";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ColagemResgateMapaFundoMetadata))
			{
				if(ColagemResgateMapaFundoMetadata.mapDelegates == null)
				{
					ColagemResgateMapaFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ColagemResgateMapaFundoMetadata.meta == null)
				{
					ColagemResgateMapaFundoMetadata.meta = new ColagemResgateMapaFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdColagemResgateMapaFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DetalheImportado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdOperacaoResgateVinculada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdColagemResgateDetalhe", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "ColagemResgateMapaFundo";
				meta.Destination = "ColagemResgateMapaFundo";
				
				meta.spInsert = "proc_ColagemResgateMapaFundoInsert";				
				meta.spUpdate = "proc_ColagemResgateMapaFundoUpdate";		
				meta.spDelete = "proc_ColagemResgateMapaFundoDelete";
				meta.spLoadAll = "proc_ColagemResgateMapaFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_ColagemResgateMapaFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ColagemResgateMapaFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
