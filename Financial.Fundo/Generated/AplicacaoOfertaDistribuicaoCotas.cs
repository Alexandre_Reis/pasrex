/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 24/07/2015 11:28:00
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esAplicacaoOfertaDistribuicaoCotasCollection : esEntityCollection
	{
		public esAplicacaoOfertaDistribuicaoCotasCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AplicacaoOfertaDistribuicaoCotasCollection";
		}

		#region Query Logic
		protected void InitQuery(esAplicacaoOfertaDistribuicaoCotasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAplicacaoOfertaDistribuicaoCotasQuery);
		}
		#endregion
		
		virtual public AplicacaoOfertaDistribuicaoCotas DetachEntity(AplicacaoOfertaDistribuicaoCotas entity)
		{
			return base.DetachEntity(entity) as AplicacaoOfertaDistribuicaoCotas;
		}
		
		virtual public AplicacaoOfertaDistribuicaoCotas AttachEntity(AplicacaoOfertaDistribuicaoCotas entity)
		{
			return base.AttachEntity(entity) as AplicacaoOfertaDistribuicaoCotas;
		}
		
		virtual public void Combine(AplicacaoOfertaDistribuicaoCotasCollection collection)
		{
			base.Combine(collection);
		}
		
		new public AplicacaoOfertaDistribuicaoCotas this[int index]
		{
			get
			{
				return base[index] as AplicacaoOfertaDistribuicaoCotas;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(AplicacaoOfertaDistribuicaoCotas);
		}
	}



	[Serializable]
	abstract public class esAplicacaoOfertaDistribuicaoCotas : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAplicacaoOfertaDistribuicaoCotasQuery GetDynamicQuery()
		{
			return null;
		}

		public esAplicacaoOfertaDistribuicaoCotas()
		{

		}

		public esAplicacaoOfertaDistribuicaoCotas(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOfertaDistribuicaoCotas, System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOfertaDistribuicaoCotas, idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOfertaDistribuicaoCotas, idOperacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOfertaDistribuicaoCotas, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOfertaDistribuicaoCotas, idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOfertaDistribuicaoCotas, idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOfertaDistribuicaoCotas, System.Int32 idOperacao)
		{
			esAplicacaoOfertaDistribuicaoCotasQuery query = this.GetDynamicQuery();
			query.Where(query.IdOfertaDistribuicaoCotas == idOfertaDistribuicaoCotas, query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOfertaDistribuicaoCotas, System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOfertaDistribuicaoCotas",idOfertaDistribuicaoCotas);			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOfertaDistribuicaoCotas": this.str.IdOfertaDistribuicaoCotas = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOfertaDistribuicaoCotas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOfertaDistribuicaoCotas = (System.Int32?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to AplicacaoOfertaDistribuicaoCotas.IdOfertaDistribuicaoCotas
		/// </summary>
		virtual public System.Int32? IdOfertaDistribuicaoCotas
		{
			get
			{
				return base.GetSystemInt32(AplicacaoOfertaDistribuicaoCotasMetadata.ColumnNames.IdOfertaDistribuicaoCotas);
			}
			
			set
			{
				if(base.SetSystemInt32(AplicacaoOfertaDistribuicaoCotasMetadata.ColumnNames.IdOfertaDistribuicaoCotas, value))
				{
					this._UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AplicacaoOfertaDistribuicaoCotas.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(AplicacaoOfertaDistribuicaoCotasMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(AplicacaoOfertaDistribuicaoCotasMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected OfertaDistribuicaoCotas _UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAplicacaoOfertaDistribuicaoCotas entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOfertaDistribuicaoCotas
			{
				get
				{
					System.Int32? data = entity.IdOfertaDistribuicaoCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOfertaDistribuicaoCotas = null;
					else entity.IdOfertaDistribuicaoCotas = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
			

			private esAplicacaoOfertaDistribuicaoCotas entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAplicacaoOfertaDistribuicaoCotasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAplicacaoOfertaDistribuicaoCotas can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class AplicacaoOfertaDistribuicaoCotas : esAplicacaoOfertaDistribuicaoCotas
	{

				
		#region UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AplicacaoOfertaDistribuicaoCotas_OfertaDistribuicaoCotas_FK1
		/// </summary>

		[XmlIgnore]
		public OfertaDistribuicaoCotas UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas
		{
			get
			{
				if(this._UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas == null
					&& IdOfertaDistribuicaoCotas != null					)
				{
					this._UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas = new OfertaDistribuicaoCotas();
					this._UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas", this._UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas);
					this._UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas.Query.Where(this._UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas.Query.IdOfertaDistribuicaoCotas == this.IdOfertaDistribuicaoCotas);
					this._UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas.Query.Load();
				}

				return this._UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas;
			}
			
			set
			{
				this.RemovePreSave("UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas");
				

				if(value == null)
				{
					this.IdOfertaDistribuicaoCotas = null;
					this._UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas = null;
				}
				else
				{
					this.IdOfertaDistribuicaoCotas = value.IdOfertaDistribuicaoCotas;
					this._UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas = value;
					this.SetPreSave("UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas", this._UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas != null)
			{
				this.IdOfertaDistribuicaoCotas = this._UpToOfertaDistribuicaoCotasByIdOfertaDistribuicaoCotas.IdOfertaDistribuicaoCotas;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAplicacaoOfertaDistribuicaoCotasQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AplicacaoOfertaDistribuicaoCotasMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOfertaDistribuicaoCotas
		{
			get
			{
				return new esQueryItem(this, AplicacaoOfertaDistribuicaoCotasMetadata.ColumnNames.IdOfertaDistribuicaoCotas, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, AplicacaoOfertaDistribuicaoCotasMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("AplicacaoOfertaDistribuicaoCotasCollection")]
	public partial class AplicacaoOfertaDistribuicaoCotasCollection : esAplicacaoOfertaDistribuicaoCotasCollection, IEnumerable<AplicacaoOfertaDistribuicaoCotas>
	{
		public AplicacaoOfertaDistribuicaoCotasCollection()
		{

		}
		
		public static implicit operator List<AplicacaoOfertaDistribuicaoCotas>(AplicacaoOfertaDistribuicaoCotasCollection coll)
		{
			List<AplicacaoOfertaDistribuicaoCotas> list = new List<AplicacaoOfertaDistribuicaoCotas>();
			
			foreach (AplicacaoOfertaDistribuicaoCotas emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AplicacaoOfertaDistribuicaoCotasMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AplicacaoOfertaDistribuicaoCotasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new AplicacaoOfertaDistribuicaoCotas(row);
		}

		override protected esEntity CreateEntity()
		{
			return new AplicacaoOfertaDistribuicaoCotas();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AplicacaoOfertaDistribuicaoCotasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AplicacaoOfertaDistribuicaoCotasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AplicacaoOfertaDistribuicaoCotasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public AplicacaoOfertaDistribuicaoCotas AddNew()
		{
			AplicacaoOfertaDistribuicaoCotas entity = base.AddNewEntity() as AplicacaoOfertaDistribuicaoCotas;
			
			return entity;
		}

		public AplicacaoOfertaDistribuicaoCotas FindByPrimaryKey(System.Int32 idOfertaDistribuicaoCotas, System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(idOfertaDistribuicaoCotas, idOperacao) as AplicacaoOfertaDistribuicaoCotas;
		}


		#region IEnumerable<AplicacaoOfertaDistribuicaoCotas> Members

		IEnumerator<AplicacaoOfertaDistribuicaoCotas> IEnumerable<AplicacaoOfertaDistribuicaoCotas>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as AplicacaoOfertaDistribuicaoCotas;
			}
		}

		#endregion
		
		private AplicacaoOfertaDistribuicaoCotasQuery query;
	}


	/// <summary>
	/// Encapsulates the 'AplicacaoOfertaDistribuicaoCotas' table
	/// </summary>

	[Serializable]
	public partial class AplicacaoOfertaDistribuicaoCotas : esAplicacaoOfertaDistribuicaoCotas
	{
		public AplicacaoOfertaDistribuicaoCotas()
		{

		}
	
		public AplicacaoOfertaDistribuicaoCotas(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AplicacaoOfertaDistribuicaoCotasMetadata.Meta();
			}
		}
		
		
		
		override protected esAplicacaoOfertaDistribuicaoCotasQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AplicacaoOfertaDistribuicaoCotasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AplicacaoOfertaDistribuicaoCotasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AplicacaoOfertaDistribuicaoCotasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AplicacaoOfertaDistribuicaoCotasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AplicacaoOfertaDistribuicaoCotasQuery query;
	}



	[Serializable]
	public partial class AplicacaoOfertaDistribuicaoCotasQuery : esAplicacaoOfertaDistribuicaoCotasQuery
	{
		public AplicacaoOfertaDistribuicaoCotasQuery()
		{

		}		
		
		public AplicacaoOfertaDistribuicaoCotasQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AplicacaoOfertaDistribuicaoCotasMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AplicacaoOfertaDistribuicaoCotasMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AplicacaoOfertaDistribuicaoCotasMetadata.ColumnNames.IdOfertaDistribuicaoCotas, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AplicacaoOfertaDistribuicaoCotasMetadata.PropertyNames.IdOfertaDistribuicaoCotas;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AplicacaoOfertaDistribuicaoCotasMetadata.ColumnNames.IdOperacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AplicacaoOfertaDistribuicaoCotasMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public AplicacaoOfertaDistribuicaoCotasMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOfertaDistribuicaoCotas = "IdOfertaDistribuicaoCotas";
			 public const string IdOperacao = "IdOperacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOfertaDistribuicaoCotas = "IdOfertaDistribuicaoCotas";
			 public const string IdOperacao = "IdOperacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AplicacaoOfertaDistribuicaoCotasMetadata))
			{
				if(AplicacaoOfertaDistribuicaoCotasMetadata.mapDelegates == null)
				{
					AplicacaoOfertaDistribuicaoCotasMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AplicacaoOfertaDistribuicaoCotasMetadata.meta == null)
				{
					AplicacaoOfertaDistribuicaoCotasMetadata.meta = new AplicacaoOfertaDistribuicaoCotasMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOfertaDistribuicaoCotas", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "AplicacaoOfertaDistribuicaoCotas";
				meta.Destination = "AplicacaoOfertaDistribuicaoCotas";
				
				meta.spInsert = "proc_AplicacaoOfertaDistribuicaoCotasInsert";				
				meta.spUpdate = "proc_AplicacaoOfertaDistribuicaoCotasUpdate";		
				meta.spDelete = "proc_AplicacaoOfertaDistribuicaoCotasDelete";
				meta.spLoadAll = "proc_AplicacaoOfertaDistribuicaoCotasLoadAll";
				meta.spLoadByPrimaryKey = "proc_AplicacaoOfertaDistribuicaoCotasLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AplicacaoOfertaDistribuicaoCotasMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
