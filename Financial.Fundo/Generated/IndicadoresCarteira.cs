/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:19 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esIndicadoresCarteiraCollection : esEntityCollection
	{
		public esIndicadoresCarteiraCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "IndicadoresCarteiraCollection";
		}

		#region Query Logic
		protected void InitQuery(esIndicadoresCarteiraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esIndicadoresCarteiraQuery);
		}
		#endregion
		
		virtual public IndicadoresCarteira DetachEntity(IndicadoresCarteira entity)
		{
			return base.DetachEntity(entity) as IndicadoresCarteira;
		}
		
		virtual public IndicadoresCarteira AttachEntity(IndicadoresCarteira entity)
		{
			return base.AttachEntity(entity) as IndicadoresCarteira;
		}
		
		virtual public void Combine(IndicadoresCarteiraCollection collection)
		{
			base.Combine(collection);
		}
		
		new public IndicadoresCarteira this[int index]
		{
			get
			{
				return base[index] as IndicadoresCarteira;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(IndicadoresCarteira);
		}
	}



	[Serializable]
	abstract public class esIndicadoresCarteira : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esIndicadoresCarteiraQuery GetDynamicQuery()
		{
			return null;
		}

		public esIndicadoresCarteira()
		{

		}

		public esIndicadoresCarteira(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int32 idCarteira)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idCarteira);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime data, System.Int32 idCarteira)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esIndicadoresCarteiraQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Data == data, query.IdCarteira == idCarteira);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int32 idCarteira)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idCarteira);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int32 idCarteira)
		{
			esIndicadoresCarteiraQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.IdCarteira == idCarteira);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int32 idCarteira)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("IdCarteira",idCarteira);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "RetornoDia": this.str.RetornoDia = (string)value; break;							
						case "RetornoMes": this.str.RetornoMes = (string)value; break;							
						case "RetornoAno": this.str.RetornoAno = (string)value; break;							
						case "Volatilidade": this.str.Volatilidade = (string)value; break;							
						case "RetornoInicio": this.str.RetornoInicio = (string)value; break;							
						case "Sharpe3Meses": this.str.Sharpe3Meses = (string)value; break;							
						case "Sharpe6Meses": this.str.Sharpe6Meses = (string)value; break;							
						case "Sharpe12Meses": this.str.Sharpe12Meses = (string)value; break;							
						case "RetornoDiaCotaRendimento": this.str.RetornoDiaCotaRendimento = (string)value; break;							
						case "CotaPatrimonial": this.str.CotaPatrimonial = (string)value; break;							
						case "CotaRendimento": this.str.CotaRendimento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "RetornoDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RetornoDia = (System.Decimal?)value;
							break;
						
						case "RetornoMes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RetornoMes = (System.Decimal?)value;
							break;
						
						case "RetornoAno":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RetornoAno = (System.Decimal?)value;
							break;
						
						case "Volatilidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Volatilidade = (System.Decimal?)value;
							break;
						
						case "RetornoInicio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RetornoInicio = (System.Decimal?)value;
							break;
						
						case "Sharpe3Meses":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Sharpe3Meses = (System.Decimal?)value;
							break;
						
						case "Sharpe6Meses":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Sharpe6Meses = (System.Decimal?)value;
							break;
						
						case "Sharpe12Meses":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Sharpe12Meses = (System.Decimal?)value;
							break;
						
						case "RetornoDiaCotaRendimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RetornoDiaCotaRendimento = (System.Decimal?)value;
							break;
						
						case "CotaPatrimonial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaPatrimonial = (System.Decimal?)value;
							break;
						
						case "CotaRendimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaRendimento = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to IndicadoresCarteira.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(IndicadoresCarteiraMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(IndicadoresCarteiraMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to IndicadoresCarteira.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(IndicadoresCarteiraMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(IndicadoresCarteiraMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to IndicadoresCarteira.RetornoDia
		/// </summary>
		virtual public System.Decimal? RetornoDia
		{
			get
			{
				return base.GetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.RetornoDia);
			}
			
			set
			{
				base.SetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.RetornoDia, value);
			}
		}
		
		/// <summary>
		/// Maps to IndicadoresCarteira.RetornoMes
		/// </summary>
		virtual public System.Decimal? RetornoMes
		{
			get
			{
				return base.GetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.RetornoMes);
			}
			
			set
			{
				base.SetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.RetornoMes, value);
			}
		}
		
		/// <summary>
		/// Maps to IndicadoresCarteira.RetornoAno
		/// </summary>
		virtual public System.Decimal? RetornoAno
		{
			get
			{
				return base.GetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.RetornoAno);
			}
			
			set
			{
				base.SetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.RetornoAno, value);
			}
		}
		
		/// <summary>
		/// Maps to IndicadoresCarteira.Volatilidade
		/// </summary>
		virtual public System.Decimal? Volatilidade
		{
			get
			{
				return base.GetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.Volatilidade);
			}
			
			set
			{
				base.SetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.Volatilidade, value);
			}
		}
		
		/// <summary>
		/// Maps to IndicadoresCarteira.RetornoInicio
		/// </summary>
		virtual public System.Decimal? RetornoInicio
		{
			get
			{
				return base.GetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.RetornoInicio);
			}
			
			set
			{
				base.SetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.RetornoInicio, value);
			}
		}
		
		/// <summary>
		/// Maps to IndicadoresCarteira.Sharpe3Meses
		/// </summary>
		virtual public System.Decimal? Sharpe3Meses
		{
			get
			{
				return base.GetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.Sharpe3Meses);
			}
			
			set
			{
				base.SetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.Sharpe3Meses, value);
			}
		}
		
		/// <summary>
		/// Maps to IndicadoresCarteira.Sharpe6Meses
		/// </summary>
		virtual public System.Decimal? Sharpe6Meses
		{
			get
			{
				return base.GetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.Sharpe6Meses);
			}
			
			set
			{
				base.SetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.Sharpe6Meses, value);
			}
		}
		
		/// <summary>
		/// Maps to IndicadoresCarteira.Sharpe12Meses
		/// </summary>
		virtual public System.Decimal? Sharpe12Meses
		{
			get
			{
				return base.GetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.Sharpe12Meses);
			}
			
			set
			{
				base.SetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.Sharpe12Meses, value);
			}
		}
		
		/// <summary>
		/// Maps to IndicadoresCarteira.RetornoDiaCotaRendimento
		/// </summary>
		virtual public System.Decimal? RetornoDiaCotaRendimento
		{
			get
			{
				return base.GetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.RetornoDiaCotaRendimento);
			}
			
			set
			{
				base.SetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.RetornoDiaCotaRendimento, value);
			}
		}
		
		/// <summary>
		/// Maps to IndicadoresCarteira.CotaPatrimonial
		/// </summary>
		virtual public System.Decimal? CotaPatrimonial
		{
			get
			{
				return base.GetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.CotaPatrimonial);
			}
			
			set
			{
				base.SetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.CotaPatrimonial, value);
			}
		}
		
		/// <summary>
		/// Maps to IndicadoresCarteira.CotaRendimento
		/// </summary>
		virtual public System.Decimal? CotaRendimento
		{
			get
			{
				return base.GetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.CotaRendimento);
			}
			
			set
			{
				base.SetSystemDecimal(IndicadoresCarteiraMetadata.ColumnNames.CotaRendimento, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esIndicadoresCarteira entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String RetornoDia
			{
				get
				{
					System.Decimal? data = entity.RetornoDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RetornoDia = null;
					else entity.RetornoDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String RetornoMes
			{
				get
				{
					System.Decimal? data = entity.RetornoMes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RetornoMes = null;
					else entity.RetornoMes = Convert.ToDecimal(value);
				}
			}
				
			public System.String RetornoAno
			{
				get
				{
					System.Decimal? data = entity.RetornoAno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RetornoAno = null;
					else entity.RetornoAno = Convert.ToDecimal(value);
				}
			}
				
			public System.String Volatilidade
			{
				get
				{
					System.Decimal? data = entity.Volatilidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Volatilidade = null;
					else entity.Volatilidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String RetornoInicio
			{
				get
				{
					System.Decimal? data = entity.RetornoInicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RetornoInicio = null;
					else entity.RetornoInicio = Convert.ToDecimal(value);
				}
			}
				
			public System.String Sharpe3Meses
			{
				get
				{
					System.Decimal? data = entity.Sharpe3Meses;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Sharpe3Meses = null;
					else entity.Sharpe3Meses = Convert.ToDecimal(value);
				}
			}
				
			public System.String Sharpe6Meses
			{
				get
				{
					System.Decimal? data = entity.Sharpe6Meses;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Sharpe6Meses = null;
					else entity.Sharpe6Meses = Convert.ToDecimal(value);
				}
			}
				
			public System.String Sharpe12Meses
			{
				get
				{
					System.Decimal? data = entity.Sharpe12Meses;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Sharpe12Meses = null;
					else entity.Sharpe12Meses = Convert.ToDecimal(value);
				}
			}
				
			public System.String RetornoDiaCotaRendimento
			{
				get
				{
					System.Decimal? data = entity.RetornoDiaCotaRendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RetornoDiaCotaRendimento = null;
					else entity.RetornoDiaCotaRendimento = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaPatrimonial
			{
				get
				{
					System.Decimal? data = entity.CotaPatrimonial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaPatrimonial = null;
					else entity.CotaPatrimonial = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaRendimento
			{
				get
				{
					System.Decimal? data = entity.CotaRendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaRendimento = null;
					else entity.CotaRendimento = Convert.ToDecimal(value);
				}
			}
			

			private esIndicadoresCarteira entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esIndicadoresCarteiraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esIndicadoresCarteira can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class IndicadoresCarteira : esIndicadoresCarteira
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esIndicadoresCarteiraQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return IndicadoresCarteiraMetadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, IndicadoresCarteiraMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, IndicadoresCarteiraMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem RetornoDia
		{
			get
			{
				return new esQueryItem(this, IndicadoresCarteiraMetadata.ColumnNames.RetornoDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RetornoMes
		{
			get
			{
				return new esQueryItem(this, IndicadoresCarteiraMetadata.ColumnNames.RetornoMes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RetornoAno
		{
			get
			{
				return new esQueryItem(this, IndicadoresCarteiraMetadata.ColumnNames.RetornoAno, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Volatilidade
		{
			get
			{
				return new esQueryItem(this, IndicadoresCarteiraMetadata.ColumnNames.Volatilidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RetornoInicio
		{
			get
			{
				return new esQueryItem(this, IndicadoresCarteiraMetadata.ColumnNames.RetornoInicio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Sharpe3Meses
		{
			get
			{
				return new esQueryItem(this, IndicadoresCarteiraMetadata.ColumnNames.Sharpe3Meses, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Sharpe6Meses
		{
			get
			{
				return new esQueryItem(this, IndicadoresCarteiraMetadata.ColumnNames.Sharpe6Meses, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Sharpe12Meses
		{
			get
			{
				return new esQueryItem(this, IndicadoresCarteiraMetadata.ColumnNames.Sharpe12Meses, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RetornoDiaCotaRendimento
		{
			get
			{
				return new esQueryItem(this, IndicadoresCarteiraMetadata.ColumnNames.RetornoDiaCotaRendimento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaPatrimonial
		{
			get
			{
				return new esQueryItem(this, IndicadoresCarteiraMetadata.ColumnNames.CotaPatrimonial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaRendimento
		{
			get
			{
				return new esQueryItem(this, IndicadoresCarteiraMetadata.ColumnNames.CotaRendimento, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("IndicadoresCarteiraCollection")]
	public partial class IndicadoresCarteiraCollection : esIndicadoresCarteiraCollection, IEnumerable<IndicadoresCarteira>
	{
		public IndicadoresCarteiraCollection()
		{

		}
		
		public static implicit operator List<IndicadoresCarteira>(IndicadoresCarteiraCollection coll)
		{
			List<IndicadoresCarteira> list = new List<IndicadoresCarteira>();
			
			foreach (IndicadoresCarteira emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  IndicadoresCarteiraMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IndicadoresCarteiraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new IndicadoresCarteira(row);
		}

		override protected esEntity CreateEntity()
		{
			return new IndicadoresCarteira();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public IndicadoresCarteiraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IndicadoresCarteiraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(IndicadoresCarteiraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public IndicadoresCarteira AddNew()
		{
			IndicadoresCarteira entity = base.AddNewEntity() as IndicadoresCarteira;
			
			return entity;
		}

		public IndicadoresCarteira FindByPrimaryKey(System.DateTime data, System.Int32 idCarteira)
		{
			return base.FindByPrimaryKey(data, idCarteira) as IndicadoresCarteira;
		}


		#region IEnumerable<IndicadoresCarteira> Members

		IEnumerator<IndicadoresCarteira> IEnumerable<IndicadoresCarteira>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as IndicadoresCarteira;
			}
		}

		#endregion
		
		private IndicadoresCarteiraQuery query;
	}


	/// <summary>
	/// Encapsulates the 'IndicadoresCarteira' table
	/// </summary>

	[Serializable]
	public partial class IndicadoresCarteira : esIndicadoresCarteira
	{
		public IndicadoresCarteira()
		{

		}
	
		public IndicadoresCarteira(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return IndicadoresCarteiraMetadata.Meta();
			}
		}
		
		
		
		override protected esIndicadoresCarteiraQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IndicadoresCarteiraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public IndicadoresCarteiraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IndicadoresCarteiraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(IndicadoresCarteiraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private IndicadoresCarteiraQuery query;
	}



	[Serializable]
	public partial class IndicadoresCarteiraQuery : esIndicadoresCarteiraQuery
	{
		public IndicadoresCarteiraQuery()
		{

		}		
		
		public IndicadoresCarteiraQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class IndicadoresCarteiraMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected IndicadoresCarteiraMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(IndicadoresCarteiraMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = IndicadoresCarteiraMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndicadoresCarteiraMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IndicadoresCarteiraMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndicadoresCarteiraMetadata.ColumnNames.RetornoDia, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IndicadoresCarteiraMetadata.PropertyNames.RetornoDia;	
			c.NumericPrecision = 20;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndicadoresCarteiraMetadata.ColumnNames.RetornoMes, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IndicadoresCarteiraMetadata.PropertyNames.RetornoMes;	
			c.NumericPrecision = 20;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndicadoresCarteiraMetadata.ColumnNames.RetornoAno, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IndicadoresCarteiraMetadata.PropertyNames.RetornoAno;	
			c.NumericPrecision = 20;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndicadoresCarteiraMetadata.ColumnNames.Volatilidade, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IndicadoresCarteiraMetadata.PropertyNames.Volatilidade;	
			c.NumericPrecision = 20;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndicadoresCarteiraMetadata.ColumnNames.RetornoInicio, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IndicadoresCarteiraMetadata.PropertyNames.RetornoInicio;	
			c.NumericPrecision = 20;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndicadoresCarteiraMetadata.ColumnNames.Sharpe3Meses, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IndicadoresCarteiraMetadata.PropertyNames.Sharpe3Meses;	
			c.NumericPrecision = 20;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndicadoresCarteiraMetadata.ColumnNames.Sharpe6Meses, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IndicadoresCarteiraMetadata.PropertyNames.Sharpe6Meses;	
			c.NumericPrecision = 20;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndicadoresCarteiraMetadata.ColumnNames.Sharpe12Meses, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IndicadoresCarteiraMetadata.PropertyNames.Sharpe12Meses;	
			c.NumericPrecision = 20;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndicadoresCarteiraMetadata.ColumnNames.RetornoDiaCotaRendimento, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IndicadoresCarteiraMetadata.PropertyNames.RetornoDiaCotaRendimento;	
			c.NumericPrecision = 20;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndicadoresCarteiraMetadata.ColumnNames.CotaPatrimonial, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IndicadoresCarteiraMetadata.PropertyNames.CotaPatrimonial;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IndicadoresCarteiraMetadata.ColumnNames.CotaRendimento, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IndicadoresCarteiraMetadata.PropertyNames.CotaRendimento;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public IndicadoresCarteiraMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string IdCarteira = "IdCarteira";
			 public const string RetornoDia = "RetornoDia";
			 public const string RetornoMes = "RetornoMes";
			 public const string RetornoAno = "RetornoAno";
			 public const string Volatilidade = "Volatilidade";
			 public const string RetornoInicio = "RetornoInicio";
			 public const string Sharpe3Meses = "Sharpe3Meses";
			 public const string Sharpe6Meses = "Sharpe6Meses";
			 public const string Sharpe12Meses = "Sharpe12Meses";
			 public const string RetornoDiaCotaRendimento = "RetornoDiaCotaRendimento";
			 public const string CotaPatrimonial = "CotaPatrimonial";
			 public const string CotaRendimento = "CotaRendimento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string IdCarteira = "IdCarteira";
			 public const string RetornoDia = "RetornoDia";
			 public const string RetornoMes = "RetornoMes";
			 public const string RetornoAno = "RetornoAno";
			 public const string Volatilidade = "Volatilidade";
			 public const string RetornoInicio = "RetornoInicio";
			 public const string Sharpe3Meses = "Sharpe3Meses";
			 public const string Sharpe6Meses = "Sharpe6Meses";
			 public const string Sharpe12Meses = "Sharpe12Meses";
			 public const string RetornoDiaCotaRendimento = "RetornoDiaCotaRendimento";
			 public const string CotaPatrimonial = "CotaPatrimonial";
			 public const string CotaRendimento = "CotaRendimento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(IndicadoresCarteiraMetadata))
			{
				if(IndicadoresCarteiraMetadata.mapDelegates == null)
				{
					IndicadoresCarteiraMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (IndicadoresCarteiraMetadata.meta == null)
				{
					IndicadoresCarteiraMetadata.meta = new IndicadoresCarteiraMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RetornoDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RetornoMes", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RetornoAno", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Volatilidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RetornoInicio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Sharpe3Meses", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Sharpe6Meses", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Sharpe12Meses", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RetornoDiaCotaRendimento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaPatrimonial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaRendimento", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "IndicadoresCarteira";
				meta.Destination = "IndicadoresCarteira";
				
				meta.spInsert = "proc_IndicadoresCarteiraInsert";				
				meta.spUpdate = "proc_IndicadoresCarteiraUpdate";		
				meta.spDelete = "proc_IndicadoresCarteiraDelete";
				meta.spLoadAll = "proc_IndicadoresCarteiraLoadAll";
				meta.spLoadByPrimaryKey = "proc_IndicadoresCarteiraLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private IndicadoresCarteiraMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
