/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/03/2016 10:17:09
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esPerfilClassificacaoFundoEntidadeCollection : esEntityCollection
	{
		public esPerfilClassificacaoFundoEntidadeCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PerfilClassificacaoFundoEntidadeCollection";
		}

		#region Query Logic
		protected void InitQuery(esPerfilClassificacaoFundoEntidadeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPerfilClassificacaoFundoEntidadeQuery);
		}
		#endregion
		
		virtual public PerfilClassificacaoFundoEntidade DetachEntity(PerfilClassificacaoFundoEntidade entity)
		{
			return base.DetachEntity(entity) as PerfilClassificacaoFundoEntidade;
		}
		
		virtual public PerfilClassificacaoFundoEntidade AttachEntity(PerfilClassificacaoFundoEntidade entity)
		{
			return base.AttachEntity(entity) as PerfilClassificacaoFundoEntidade;
		}
		
		virtual public void Combine(PerfilClassificacaoFundoEntidadeCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PerfilClassificacaoFundoEntidade this[int index]
		{
			get
			{
				return base[index] as PerfilClassificacaoFundoEntidade;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PerfilClassificacaoFundoEntidade);
		}
	}



	[Serializable]
	abstract public class esPerfilClassificacaoFundoEntidade : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPerfilClassificacaoFundoEntidadeQuery GetDynamicQuery()
		{
			return null;
		}

		public esPerfilClassificacaoFundoEntidade()
		{

		}

		public esPerfilClassificacaoFundoEntidade(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPerfilClassificacaoFundoEntidade)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilClassificacaoFundoEntidade);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilClassificacaoFundoEntidade);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPerfilClassificacaoFundoEntidade)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilClassificacaoFundoEntidade);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilClassificacaoFundoEntidade);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPerfilClassificacaoFundoEntidade)
		{
			esPerfilClassificacaoFundoEntidadeQuery query = this.GetDynamicQuery();
			query.Where(query.IdPerfilClassificacaoFundoEntidade == idPerfilClassificacaoFundoEntidade);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPerfilClassificacaoFundoEntidade)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPerfilClassificacaoFundoEntidade",idPerfilClassificacaoFundoEntidade);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPerfilClassificacaoFundoEntidade": this.str.IdPerfilClassificacaoFundoEntidade = (string)value; break;							
						case "IdPerfilClassificacao": this.str.IdPerfilClassificacao = (string)value; break;							
						case "IdEntidade": this.str.IdEntidade = (string)value; break;							
						case "IdEntidadeClassificacao": this.str.IdEntidadeClassificacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPerfilClassificacaoFundoEntidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilClassificacaoFundoEntidade = (System.Int32?)value;
							break;
						
						case "IdPerfilClassificacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilClassificacao = (System.Int32?)value;
							break;
						
						case "IdEntidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEntidade = (System.Int32?)value;
							break;
						
						case "IdEntidadeClassificacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEntidadeClassificacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PerfilClassificacaoFundoEntidade.IdPerfilClassificacaoFundoEntidade
		/// </summary>
		virtual public System.Int32? IdPerfilClassificacaoFundoEntidade
		{
			get
			{
				return base.GetSystemInt32(PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdPerfilClassificacaoFundoEntidade);
			}
			
			set
			{
				base.SetSystemInt32(PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdPerfilClassificacaoFundoEntidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilClassificacaoFundoEntidade.IdPerfilClassificacao
		/// </summary>
		virtual public System.Int32? IdPerfilClassificacao
		{
			get
			{
				return base.GetSystemInt32(PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdPerfilClassificacao);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdPerfilClassificacao, value))
				{
					this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PerfilClassificacaoFundoEntidade.IdEntidade
		/// </summary>
		virtual public System.Int32? IdEntidade
		{
			get
			{
				return base.GetSystemInt32(PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdEntidade);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdEntidade, value))
				{
					this._UpToEntidadePerfilFundoByIdEntidade = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PerfilClassificacaoFundoEntidade.IdEntidadeClassificacao
		/// </summary>
		virtual public System.Int32? IdEntidadeClassificacao
		{
			get
			{
				return base.GetSystemInt32(PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdEntidadeClassificacao);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdEntidadeClassificacao, value))
				{
					this._UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected EntidadeClassificacaoPerfilFundo _UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao;
		[CLSCompliant(false)]
		internal protected EntidadePerfilFundo _UpToEntidadePerfilFundoByIdEntidade;
		[CLSCompliant(false)]
		internal protected PerfilClassificacaoFundo _UpToPerfilClassificacaoFundoByIdPerfilClassificacao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPerfilClassificacaoFundoEntidade entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPerfilClassificacaoFundoEntidade
			{
				get
				{
					System.Int32? data = entity.IdPerfilClassificacaoFundoEntidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilClassificacaoFundoEntidade = null;
					else entity.IdPerfilClassificacaoFundoEntidade = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPerfilClassificacao
			{
				get
				{
					System.Int32? data = entity.IdPerfilClassificacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilClassificacao = null;
					else entity.IdPerfilClassificacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEntidade
			{
				get
				{
					System.Int32? data = entity.IdEntidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEntidade = null;
					else entity.IdEntidade = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEntidadeClassificacao
			{
				get
				{
					System.Int32? data = entity.IdEntidadeClassificacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEntidadeClassificacao = null;
					else entity.IdEntidadeClassificacao = Convert.ToInt32(value);
				}
			}
			

			private esPerfilClassificacaoFundoEntidade entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPerfilClassificacaoFundoEntidadeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPerfilClassificacaoFundoEntidade can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PerfilClassificacaoFundoEntidade : esPerfilClassificacaoFundoEntidade
	{

				
		#region UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PerfilClassificacaoFundoEntidade_EntidadeClassificacaoPerfilFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EntidadeClassificacaoPerfilFundo UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao
		{
			get
			{
				if(this._UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao == null
					&& IdEntidadeClassificacao != null					)
				{
					this._UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao = new EntidadeClassificacaoPerfilFundo();
					this._UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao", this._UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao);
					this._UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao.Query.Where(this._UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao.Query.IdEntidadeClassificacao == this.IdEntidadeClassificacao);
					this._UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao.Query.Load();
				}

				return this._UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao;
			}
			
			set
			{
				this.RemovePreSave("UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao");
				

				if(value == null)
				{
					this.IdEntidadeClassificacao = null;
					this._UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao = null;
				}
				else
				{
					this.IdEntidadeClassificacao = value.IdEntidadeClassificacao;
					this._UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao = value;
					this.SetPreSave("UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao", this._UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToEntidadePerfilFundoByIdEntidade - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PerfilClassificacaoFundoEntidade_EntidadePerfilFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EntidadePerfilFundo UpToEntidadePerfilFundoByIdEntidade
		{
			get
			{
				if(this._UpToEntidadePerfilFundoByIdEntidade == null
					&& IdEntidade != null					)
				{
					this._UpToEntidadePerfilFundoByIdEntidade = new EntidadePerfilFundo();
					this._UpToEntidadePerfilFundoByIdEntidade.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEntidadePerfilFundoByIdEntidade", this._UpToEntidadePerfilFundoByIdEntidade);
					this._UpToEntidadePerfilFundoByIdEntidade.Query.Where(this._UpToEntidadePerfilFundoByIdEntidade.Query.IdEntidade == this.IdEntidade);
					this._UpToEntidadePerfilFundoByIdEntidade.Query.Load();
				}

				return this._UpToEntidadePerfilFundoByIdEntidade;
			}
			
			set
			{
				this.RemovePreSave("UpToEntidadePerfilFundoByIdEntidade");
				

				if(value == null)
				{
					this.IdEntidade = null;
					this._UpToEntidadePerfilFundoByIdEntidade = null;
				}
				else
				{
					this.IdEntidade = value.IdEntidade;
					this._UpToEntidadePerfilFundoByIdEntidade = value;
					this.SetPreSave("UpToEntidadePerfilFundoByIdEntidade", this._UpToEntidadePerfilFundoByIdEntidade);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPerfilClassificacaoFundoByIdPerfilClassificacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PerfilClassificacaoFundoEntidade_PerfilClassificacaoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilClassificacaoFundo UpToPerfilClassificacaoFundoByIdPerfilClassificacao
		{
			get
			{
				if(this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao == null
					&& IdPerfilClassificacao != null					)
				{
					this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao = new PerfilClassificacaoFundo();
					this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPerfilClassificacaoFundoByIdPerfilClassificacao", this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao);
					this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao.Query.Where(this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao.Query.IdPerfil == this.IdPerfilClassificacao);
					this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao.Query.Load();
				}

				return this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao;
			}
			
			set
			{
				this.RemovePreSave("UpToPerfilClassificacaoFundoByIdPerfilClassificacao");
				

				if(value == null)
				{
					this.IdPerfilClassificacao = null;
					this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao = null;
				}
				else
				{
					this.IdPerfilClassificacao = value.IdPerfil;
					this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao = value;
					this.SetPreSave("UpToPerfilClassificacaoFundoByIdPerfilClassificacao", this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao != null)
			{
				this.IdEntidadeClassificacao = this._UpToEntidadeClassificacaoPerfilFundoByIdEntidadeClassificacao.IdEntidadeClassificacao;
			}
			if(!this.es.IsDeleted && this._UpToEntidadePerfilFundoByIdEntidade != null)
			{
				this.IdEntidade = this._UpToEntidadePerfilFundoByIdEntidade.IdEntidade;
			}
			if(!this.es.IsDeleted && this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao != null)
			{
				this.IdPerfilClassificacao = this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao.IdPerfil;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPerfilClassificacaoFundoEntidadeQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PerfilClassificacaoFundoEntidadeMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPerfilClassificacaoFundoEntidade
		{
			get
			{
				return new esQueryItem(this, PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdPerfilClassificacaoFundoEntidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPerfilClassificacao
		{
			get
			{
				return new esQueryItem(this, PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdPerfilClassificacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEntidade
		{
			get
			{
				return new esQueryItem(this, PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdEntidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEntidadeClassificacao
		{
			get
			{
				return new esQueryItem(this, PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdEntidadeClassificacao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PerfilClassificacaoFundoEntidadeCollection")]
	public partial class PerfilClassificacaoFundoEntidadeCollection : esPerfilClassificacaoFundoEntidadeCollection, IEnumerable<PerfilClassificacaoFundoEntidade>
	{
		public PerfilClassificacaoFundoEntidadeCollection()
		{

		}
		
		public static implicit operator List<PerfilClassificacaoFundoEntidade>(PerfilClassificacaoFundoEntidadeCollection coll)
		{
			List<PerfilClassificacaoFundoEntidade> list = new List<PerfilClassificacaoFundoEntidade>();
			
			foreach (PerfilClassificacaoFundoEntidade emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PerfilClassificacaoFundoEntidadeMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilClassificacaoFundoEntidadeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PerfilClassificacaoFundoEntidade(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PerfilClassificacaoFundoEntidade();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PerfilClassificacaoFundoEntidadeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilClassificacaoFundoEntidadeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PerfilClassificacaoFundoEntidadeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PerfilClassificacaoFundoEntidade AddNew()
		{
			PerfilClassificacaoFundoEntidade entity = base.AddNewEntity() as PerfilClassificacaoFundoEntidade;
			
			return entity;
		}

		public PerfilClassificacaoFundoEntidade FindByPrimaryKey(System.Int32 idPerfilClassificacaoFundoEntidade)
		{
			return base.FindByPrimaryKey(idPerfilClassificacaoFundoEntidade) as PerfilClassificacaoFundoEntidade;
		}


		#region IEnumerable<PerfilClassificacaoFundoEntidade> Members

		IEnumerator<PerfilClassificacaoFundoEntidade> IEnumerable<PerfilClassificacaoFundoEntidade>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PerfilClassificacaoFundoEntidade;
			}
		}

		#endregion
		
		private PerfilClassificacaoFundoEntidadeQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PerfilClassificacaoFundoEntidade' table
	/// </summary>

	[Serializable]
	public partial class PerfilClassificacaoFundoEntidade : esPerfilClassificacaoFundoEntidade
	{
		public PerfilClassificacaoFundoEntidade()
		{

		}
	
		public PerfilClassificacaoFundoEntidade(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PerfilClassificacaoFundoEntidadeMetadata.Meta();
			}
		}
		
		
		
		override protected esPerfilClassificacaoFundoEntidadeQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilClassificacaoFundoEntidadeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PerfilClassificacaoFundoEntidadeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilClassificacaoFundoEntidadeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PerfilClassificacaoFundoEntidadeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PerfilClassificacaoFundoEntidadeQuery query;
	}



	[Serializable]
	public partial class PerfilClassificacaoFundoEntidadeQuery : esPerfilClassificacaoFundoEntidadeQuery
	{
		public PerfilClassificacaoFundoEntidadeQuery()
		{

		}		
		
		public PerfilClassificacaoFundoEntidadeQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PerfilClassificacaoFundoEntidadeMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PerfilClassificacaoFundoEntidadeMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdPerfilClassificacaoFundoEntidade, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilClassificacaoFundoEntidadeMetadata.PropertyNames.IdPerfilClassificacaoFundoEntidade;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdPerfilClassificacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilClassificacaoFundoEntidadeMetadata.PropertyNames.IdPerfilClassificacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdEntidade, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilClassificacaoFundoEntidadeMetadata.PropertyNames.IdEntidade;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilClassificacaoFundoEntidadeMetadata.ColumnNames.IdEntidadeClassificacao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilClassificacaoFundoEntidadeMetadata.PropertyNames.IdEntidadeClassificacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PerfilClassificacaoFundoEntidadeMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPerfilClassificacaoFundoEntidade = "IdPerfilClassificacaoFundoEntidade";
			 public const string IdPerfilClassificacao = "IdPerfilClassificacao";
			 public const string IdEntidade = "IdEntidade";
			 public const string IdEntidadeClassificacao = "IdEntidadeClassificacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPerfilClassificacaoFundoEntidade = "IdPerfilClassificacaoFundoEntidade";
			 public const string IdPerfilClassificacao = "IdPerfilClassificacao";
			 public const string IdEntidade = "IdEntidade";
			 public const string IdEntidadeClassificacao = "IdEntidadeClassificacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PerfilClassificacaoFundoEntidadeMetadata))
			{
				if(PerfilClassificacaoFundoEntidadeMetadata.mapDelegates == null)
				{
					PerfilClassificacaoFundoEntidadeMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PerfilClassificacaoFundoEntidadeMetadata.meta == null)
				{
					PerfilClassificacaoFundoEntidadeMetadata.meta = new PerfilClassificacaoFundoEntidadeMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPerfilClassificacaoFundoEntidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPerfilClassificacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEntidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEntidadeClassificacao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "PerfilClassificacaoFundoEntidade";
				meta.Destination = "PerfilClassificacaoFundoEntidade";
				
				meta.spInsert = "proc_PerfilClassificacaoFundoEntidadeInsert";				
				meta.spUpdate = "proc_PerfilClassificacaoFundoEntidadeUpdate";		
				meta.spDelete = "proc_PerfilClassificacaoFundoEntidadeDelete";
				meta.spLoadAll = "proc_PerfilClassificacaoFundoEntidadeLoadAll";
				meta.spLoadByPrimaryKey = "proc_PerfilClassificacaoFundoEntidadeLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PerfilClassificacaoFundoEntidadeMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
