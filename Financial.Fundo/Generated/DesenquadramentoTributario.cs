/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 05/01/2015 17:25:20
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esDesenquadramentoTributarioCollection : esEntityCollection
	{
		public esDesenquadramentoTributarioCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "DesenquadramentoTributarioCollection";
		}

		#region Query Logic
		protected void InitQuery(esDesenquadramentoTributarioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esDesenquadramentoTributarioQuery);
		}
		#endregion
		
		virtual public DesenquadramentoTributario DetachEntity(DesenquadramentoTributario entity)
		{
			return base.DetachEntity(entity) as DesenquadramentoTributario;
		}
		
		virtual public DesenquadramentoTributario AttachEntity(DesenquadramentoTributario entity)
		{
			return base.AttachEntity(entity) as DesenquadramentoTributario;
		}
		
		virtual public void Combine(DesenquadramentoTributarioCollection collection)
		{
			base.Combine(collection);
		}
		
		new public DesenquadramentoTributario this[int index]
		{
			get
			{
				return base[index] as DesenquadramentoTributario;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(DesenquadramentoTributario);
		}
	}



	[Serializable]
	abstract public class esDesenquadramentoTributario : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esDesenquadramentoTributarioQuery GetDynamicQuery()
		{
			return null;
		}

		public esDesenquadramentoTributario()
		{

		}

		public esDesenquadramentoTributario(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idDesenquadramentoTributario)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idDesenquadramentoTributario);
			else
				return LoadByPrimaryKeyStoredProcedure(idDesenquadramentoTributario);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idDesenquadramentoTributario)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idDesenquadramentoTributario);
			else
				return LoadByPrimaryKeyStoredProcedure(idDesenquadramentoTributario);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idDesenquadramentoTributario)
		{
			esDesenquadramentoTributarioQuery query = this.GetDynamicQuery();
			query.Where(query.IdDesenquadramentoTributario == idDesenquadramentoTributario);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idDesenquadramentoTributario)
		{
			esParameters parms = new esParameters();
			parms.Add("IdDesenquadramentoTributario",idDesenquadramentoTributario);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdDesenquadramentoTributario": this.str.IdDesenquadramentoTributario = (string)value; break;							
						case "TipoOcorrencia": this.str.TipoOcorrencia = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "ClassificacaoTributariaAtual": this.str.ClassificacaoTributariaAtual = (string)value; break;							
						case "MotivoMudancaOcorrencia": this.str.MotivoMudancaOcorrencia = (string)value; break;							
						case "ExecucaoRecolhimento": this.str.ExecucaoRecolhimento = (string)value; break;							
						case "AliquotaIR": this.str.AliquotaIR = (string)value; break;							
						case "DataRecolhimento": this.str.DataRecolhimento = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdDesenquadramentoTributario":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdDesenquadramentoTributario = (System.Int32?)value;
							break;
						
						case "TipoOcorrencia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoOcorrencia = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "ClassificacaoTributariaAtual":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.ClassificacaoTributariaAtual = (System.Int32?)value;
							break;
						
						case "ExecucaoRecolhimento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.ExecucaoRecolhimento = (System.Int32?)value;
							break;
						
						case "AliquotaIR":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.AliquotaIR = (System.Int32?)value;
							break;
						
						case "DataRecolhimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRecolhimento = (System.DateTime?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to DesenquadramentoTributario.IdDesenquadramentoTributario
		/// </summary>
		virtual public System.Int32? IdDesenquadramentoTributario
		{
			get
			{
				return base.GetSystemInt32(DesenquadramentoTributarioMetadata.ColumnNames.IdDesenquadramentoTributario);
			}
			
			set
			{
				base.SetSystemInt32(DesenquadramentoTributarioMetadata.ColumnNames.IdDesenquadramentoTributario, value);
			}
		}
		
		/// <summary>
		/// Maps to DesenquadramentoTributario.TipoOcorrencia
		/// </summary>
		virtual public System.Int32? TipoOcorrencia
		{
			get
			{
				return base.GetSystemInt32(DesenquadramentoTributarioMetadata.ColumnNames.TipoOcorrencia);
			}
			
			set
			{
				base.SetSystemInt32(DesenquadramentoTributarioMetadata.ColumnNames.TipoOcorrencia, value);
			}
		}
		
		/// <summary>
		/// Maps to DesenquadramentoTributario.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(DesenquadramentoTributarioMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(DesenquadramentoTributarioMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to DesenquadramentoTributario.ClassificacaoTributariaAtual
		/// </summary>
		virtual public System.Int32? ClassificacaoTributariaAtual
		{
			get
			{
				return base.GetSystemInt32(DesenquadramentoTributarioMetadata.ColumnNames.ClassificacaoTributariaAtual);
			}
			
			set
			{
				base.SetSystemInt32(DesenquadramentoTributarioMetadata.ColumnNames.ClassificacaoTributariaAtual, value);
			}
		}
		
		/// <summary>
		/// Maps to DesenquadramentoTributario.MotivoMudancaOcorrencia
		/// </summary>
		virtual public System.String MotivoMudancaOcorrencia
		{
			get
			{
				return base.GetSystemString(DesenquadramentoTributarioMetadata.ColumnNames.MotivoMudancaOcorrencia);
			}
			
			set
			{
				base.SetSystemString(DesenquadramentoTributarioMetadata.ColumnNames.MotivoMudancaOcorrencia, value);
			}
		}
		
		/// <summary>
		/// Maps to DesenquadramentoTributario.ExecucaoRecolhimento
		/// </summary>
		virtual public System.Int32? ExecucaoRecolhimento
		{
			get
			{
				return base.GetSystemInt32(DesenquadramentoTributarioMetadata.ColumnNames.ExecucaoRecolhimento);
			}
			
			set
			{
				base.SetSystemInt32(DesenquadramentoTributarioMetadata.ColumnNames.ExecucaoRecolhimento, value);
			}
		}
		
		/// <summary>
		/// Maps to DesenquadramentoTributario.AliquotaIR
		/// </summary>
		virtual public System.Int32? AliquotaIR
		{
			get
			{
				return base.GetSystemInt32(DesenquadramentoTributarioMetadata.ColumnNames.AliquotaIR);
			}
			
			set
			{
				base.SetSystemInt32(DesenquadramentoTributarioMetadata.ColumnNames.AliquotaIR, value);
			}
		}
		
		/// <summary>
		/// Maps to DesenquadramentoTributario.DataRecolhimento
		/// </summary>
		virtual public System.DateTime? DataRecolhimento
		{
			get
			{
				return base.GetSystemDateTime(DesenquadramentoTributarioMetadata.ColumnNames.DataRecolhimento);
			}
			
			set
			{
				base.SetSystemDateTime(DesenquadramentoTributarioMetadata.ColumnNames.DataRecolhimento, value);
			}
		}
		
		/// <summary>
		/// Maps to DesenquadramentoTributario.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(DesenquadramentoTributarioMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(DesenquadramentoTributarioMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esDesenquadramentoTributario entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdDesenquadramentoTributario
			{
				get
				{
					System.Int32? data = entity.IdDesenquadramentoTributario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdDesenquadramentoTributario = null;
					else entity.IdDesenquadramentoTributario = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoOcorrencia
			{
				get
				{
					System.Int32? data = entity.TipoOcorrencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOcorrencia = null;
					else entity.TipoOcorrencia = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String ClassificacaoTributariaAtual
			{
				get
				{
					System.Int32? data = entity.ClassificacaoTributariaAtual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ClassificacaoTributariaAtual = null;
					else entity.ClassificacaoTributariaAtual = Convert.ToInt32(value);
				}
			}
				
			public System.String MotivoMudancaOcorrencia
			{
				get
				{
					System.String data = entity.MotivoMudancaOcorrencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MotivoMudancaOcorrencia = null;
					else entity.MotivoMudancaOcorrencia = Convert.ToString(value);
				}
			}
				
			public System.String ExecucaoRecolhimento
			{
				get
				{
					System.Int32? data = entity.ExecucaoRecolhimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExecucaoRecolhimento = null;
					else entity.ExecucaoRecolhimento = Convert.ToInt32(value);
				}
			}
				
			public System.String AliquotaIR
			{
				get
				{
					System.Int32? data = entity.AliquotaIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AliquotaIR = null;
					else entity.AliquotaIR = Convert.ToInt32(value);
				}
			}
				
			public System.String DataRecolhimento
			{
				get
				{
					System.DateTime? data = entity.DataRecolhimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRecolhimento = null;
					else entity.DataRecolhimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
			

			private esDesenquadramentoTributario entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esDesenquadramentoTributarioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esDesenquadramentoTributario can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class DesenquadramentoTributario : esDesenquadramentoTributario
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__Desenquad__Fundo__18E19391
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esDesenquadramentoTributarioQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return DesenquadramentoTributarioMetadata.Meta();
			}
		}	
		

		public esQueryItem IdDesenquadramentoTributario
		{
			get
			{
				return new esQueryItem(this, DesenquadramentoTributarioMetadata.ColumnNames.IdDesenquadramentoTributario, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoOcorrencia
		{
			get
			{
				return new esQueryItem(this, DesenquadramentoTributarioMetadata.ColumnNames.TipoOcorrencia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, DesenquadramentoTributarioMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ClassificacaoTributariaAtual
		{
			get
			{
				return new esQueryItem(this, DesenquadramentoTributarioMetadata.ColumnNames.ClassificacaoTributariaAtual, esSystemType.Int32);
			}
		} 
		
		public esQueryItem MotivoMudancaOcorrencia
		{
			get
			{
				return new esQueryItem(this, DesenquadramentoTributarioMetadata.ColumnNames.MotivoMudancaOcorrencia, esSystemType.String);
			}
		} 
		
		public esQueryItem ExecucaoRecolhimento
		{
			get
			{
				return new esQueryItem(this, DesenquadramentoTributarioMetadata.ColumnNames.ExecucaoRecolhimento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem AliquotaIR
		{
			get
			{
				return new esQueryItem(this, DesenquadramentoTributarioMetadata.ColumnNames.AliquotaIR, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataRecolhimento
		{
			get
			{
				return new esQueryItem(this, DesenquadramentoTributarioMetadata.ColumnNames.DataRecolhimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, DesenquadramentoTributarioMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("DesenquadramentoTributarioCollection")]
	public partial class DesenquadramentoTributarioCollection : esDesenquadramentoTributarioCollection, IEnumerable<DesenquadramentoTributario>
	{
		public DesenquadramentoTributarioCollection()
		{

		}
		
		public static implicit operator List<DesenquadramentoTributario>(DesenquadramentoTributarioCollection coll)
		{
			List<DesenquadramentoTributario> list = new List<DesenquadramentoTributario>();
			
			foreach (DesenquadramentoTributario emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  DesenquadramentoTributarioMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DesenquadramentoTributarioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new DesenquadramentoTributario(row);
		}

		override protected esEntity CreateEntity()
		{
			return new DesenquadramentoTributario();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public DesenquadramentoTributarioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DesenquadramentoTributarioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(DesenquadramentoTributarioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public DesenquadramentoTributario AddNew()
		{
			DesenquadramentoTributario entity = base.AddNewEntity() as DesenquadramentoTributario;
			
			return entity;
		}

		public DesenquadramentoTributario FindByPrimaryKey(System.Int32 idDesenquadramentoTributario)
		{
			return base.FindByPrimaryKey(idDesenquadramentoTributario) as DesenquadramentoTributario;
		}


		#region IEnumerable<DesenquadramentoTributario> Members

		IEnumerator<DesenquadramentoTributario> IEnumerable<DesenquadramentoTributario>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as DesenquadramentoTributario;
			}
		}

		#endregion
		
		private DesenquadramentoTributarioQuery query;
	}


	/// <summary>
	/// Encapsulates the 'DesenquadramentoTributario' table
	/// </summary>

	[Serializable]
	public partial class DesenquadramentoTributario : esDesenquadramentoTributario
	{
		public DesenquadramentoTributario()
		{

		}
	
		public DesenquadramentoTributario(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return DesenquadramentoTributarioMetadata.Meta();
			}
		}
		
		
		
		override protected esDesenquadramentoTributarioQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DesenquadramentoTributarioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public DesenquadramentoTributarioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DesenquadramentoTributarioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(DesenquadramentoTributarioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private DesenquadramentoTributarioQuery query;
	}



	[Serializable]
	public partial class DesenquadramentoTributarioQuery : esDesenquadramentoTributarioQuery
	{
		public DesenquadramentoTributarioQuery()
		{

		}		
		
		public DesenquadramentoTributarioQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class DesenquadramentoTributarioMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected DesenquadramentoTributarioMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(DesenquadramentoTributarioMetadata.ColumnNames.IdDesenquadramentoTributario, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DesenquadramentoTributarioMetadata.PropertyNames.IdDesenquadramentoTributario;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DesenquadramentoTributarioMetadata.ColumnNames.TipoOcorrencia, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DesenquadramentoTributarioMetadata.PropertyNames.TipoOcorrencia;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DesenquadramentoTributarioMetadata.ColumnNames.Data, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = DesenquadramentoTributarioMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DesenquadramentoTributarioMetadata.ColumnNames.ClassificacaoTributariaAtual, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DesenquadramentoTributarioMetadata.PropertyNames.ClassificacaoTributariaAtual;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DesenquadramentoTributarioMetadata.ColumnNames.MotivoMudancaOcorrencia, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = DesenquadramentoTributarioMetadata.PropertyNames.MotivoMudancaOcorrencia;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DesenquadramentoTributarioMetadata.ColumnNames.ExecucaoRecolhimento, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DesenquadramentoTributarioMetadata.PropertyNames.ExecucaoRecolhimento;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DesenquadramentoTributarioMetadata.ColumnNames.AliquotaIR, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DesenquadramentoTributarioMetadata.PropertyNames.AliquotaIR;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DesenquadramentoTributarioMetadata.ColumnNames.DataRecolhimento, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = DesenquadramentoTributarioMetadata.PropertyNames.DataRecolhimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DesenquadramentoTributarioMetadata.ColumnNames.IdCarteira, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DesenquadramentoTributarioMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public DesenquadramentoTributarioMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdDesenquadramentoTributario = "IdDesenquadramentoTributario";
			 public const string TipoOcorrencia = "TipoOcorrencia";
			 public const string Data = "Data";
			 public const string ClassificacaoTributariaAtual = "ClassificacaoTributariaAtual";
			 public const string MotivoMudancaOcorrencia = "MotivoMudancaOcorrencia";
			 public const string ExecucaoRecolhimento = "ExecucaoRecolhimento";
			 public const string AliquotaIR = "AliquotaIR";
			 public const string DataRecolhimento = "DataRecolhimento";
			 public const string IdCarteira = "IdCarteira";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdDesenquadramentoTributario = "IdDesenquadramentoTributario";
			 public const string TipoOcorrencia = "TipoOcorrencia";
			 public const string Data = "Data";
			 public const string ClassificacaoTributariaAtual = "ClassificacaoTributariaAtual";
			 public const string MotivoMudancaOcorrencia = "MotivoMudancaOcorrencia";
			 public const string ExecucaoRecolhimento = "ExecucaoRecolhimento";
			 public const string AliquotaIR = "AliquotaIR";
			 public const string DataRecolhimento = "DataRecolhimento";
			 public const string IdCarteira = "IdCarteira";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(DesenquadramentoTributarioMetadata))
			{
				if(DesenquadramentoTributarioMetadata.mapDelegates == null)
				{
					DesenquadramentoTributarioMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (DesenquadramentoTributarioMetadata.meta == null)
				{
					DesenquadramentoTributarioMetadata.meta = new DesenquadramentoTributarioMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdDesenquadramentoTributario", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoOcorrencia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ClassificacaoTributariaAtual", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("MotivoMudancaOcorrencia", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ExecucaoRecolhimento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AliquotaIR", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataRecolhimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "DesenquadramentoTributario";
				meta.Destination = "DesenquadramentoTributario";
				
				meta.spInsert = "proc_DesenquadramentoTributarioInsert";				
				meta.spUpdate = "proc_DesenquadramentoTributarioUpdate";		
				meta.spDelete = "proc_DesenquadramentoTributarioDelete";
				meta.spLoadAll = "proc_DesenquadramentoTributarioLoadAll";
				meta.spLoadByPrimaryKey = "proc_DesenquadramentoTributarioLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private DesenquadramentoTributarioMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
