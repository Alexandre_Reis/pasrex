/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 20/01/2015 12:13:37
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esColagemOperResgPosFundoCollection : esEntityCollection
	{
		public esColagemOperResgPosFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ColagemOperResgPosFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esColagemOperResgPosFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esColagemOperResgPosFundoQuery);
		}
		#endregion
		
		virtual public ColagemOperResgPosFundo DetachEntity(ColagemOperResgPosFundo entity)
		{
			return base.DetachEntity(entity) as ColagemOperResgPosFundo;
		}
		
		virtual public ColagemOperResgPosFundo AttachEntity(ColagemOperResgPosFundo entity)
		{
			return base.AttachEntity(entity) as ColagemOperResgPosFundo;
		}
		
		virtual public void Combine(ColagemOperResgPosFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ColagemOperResgPosFundo this[int index]
		{
			get
			{
				return base[index] as ColagemOperResgPosFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ColagemOperResgPosFundo);
		}
	}



	[Serializable]
	abstract public class esColagemOperResgPosFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esColagemOperResgPosFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esColagemOperResgPosFundo()
		{

		}

		public esColagemOperResgPosFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idColagemOperResgPosFundo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemOperResgPosFundo);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemOperResgPosFundo);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idColagemOperResgPosFundo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemOperResgPosFundo);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemOperResgPosFundo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idColagemOperResgPosFundo)
		{
			esColagemOperResgPosFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdColagemOperResgPosFundo == idColagemOperResgPosFundo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idColagemOperResgPosFundo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdColagemOperResgPosFundo",idColagemOperResgPosFundo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdColagemOperResgPosFundo": this.str.IdColagemOperResgPosFundo = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdColagemResgateDetalhe": this.str.IdColagemResgateDetalhe = (string)value; break;							
						case "IdOperacaoResgateVinculada": this.str.IdOperacaoResgateVinculada = (string)value; break;							
						case "IdPosicaoVinculada": this.str.IdPosicaoVinculada = (string)value; break;							
						case "QuantidadeVinculada": this.str.QuantidadeVinculada = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdColagemOperResgPosFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemOperResgPosFundo = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdColagemResgateDetalhe":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemResgateDetalhe = (System.Int32?)value;
							break;
						
						case "IdOperacaoResgateVinculada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoResgateVinculada = (System.Int32?)value;
							break;
						
						case "IdPosicaoVinculada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoVinculada = (System.Int32?)value;
							break;
						
						case "QuantidadeVinculada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeVinculada = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ColagemOperResgPosFundo.IdColagemOperResgPosFundo
		/// </summary>
		virtual public System.Int32? IdColagemOperResgPosFundo
		{
			get
			{
				return base.GetSystemInt32(ColagemOperResgPosFundoMetadata.ColumnNames.IdColagemOperResgPosFundo);
			}
			
			set
			{
				base.SetSystemInt32(ColagemOperResgPosFundoMetadata.ColumnNames.IdColagemOperResgPosFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemOperResgPosFundo.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(ColagemOperResgPosFundoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(ColagemOperResgPosFundoMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemOperResgPosFundo.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ColagemOperResgPosFundoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(ColagemOperResgPosFundoMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemOperResgPosFundo.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(ColagemOperResgPosFundoMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemOperResgPosFundoMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemOperResgPosFundo.IdColagemResgateDetalhe
		/// </summary>
		virtual public System.Int32? IdColagemResgateDetalhe
		{
			get
			{
				return base.GetSystemInt32(ColagemOperResgPosFundoMetadata.ColumnNames.IdColagemResgateDetalhe);
			}
			
			set
			{
				base.SetSystemInt32(ColagemOperResgPosFundoMetadata.ColumnNames.IdColagemResgateDetalhe, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemOperResgPosFundo.IdOperacaoResgateVinculada
		/// </summary>
		virtual public System.Int32? IdOperacaoResgateVinculada
		{
			get
			{
				return base.GetSystemInt32(ColagemOperResgPosFundoMetadata.ColumnNames.IdOperacaoResgateVinculada);
			}
			
			set
			{
				base.SetSystemInt32(ColagemOperResgPosFundoMetadata.ColumnNames.IdOperacaoResgateVinculada, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemOperResgPosFundo.IdPosicaoVinculada
		/// </summary>
		virtual public System.Int32? IdPosicaoVinculada
		{
			get
			{
				return base.GetSystemInt32(ColagemOperResgPosFundoMetadata.ColumnNames.IdPosicaoVinculada);
			}
			
			set
			{
				base.SetSystemInt32(ColagemOperResgPosFundoMetadata.ColumnNames.IdPosicaoVinculada, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemOperResgPosFundo.QuantidadeVinculada
		/// </summary>
		virtual public System.Decimal? QuantidadeVinculada
		{
			get
			{
				return base.GetSystemDecimal(ColagemOperResgPosFundoMetadata.ColumnNames.QuantidadeVinculada);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemOperResgPosFundoMetadata.ColumnNames.QuantidadeVinculada, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esColagemOperResgPosFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdColagemOperResgPosFundo
			{
				get
				{
					System.Int32? data = entity.IdColagemOperResgPosFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemOperResgPosFundo = null;
					else entity.IdColagemOperResgPosFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdColagemResgateDetalhe
			{
				get
				{
					System.Int32? data = entity.IdColagemResgateDetalhe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemResgateDetalhe = null;
					else entity.IdColagemResgateDetalhe = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacaoResgateVinculada
			{
				get
				{
					System.Int32? data = entity.IdOperacaoResgateVinculada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoResgateVinculada = null;
					else entity.IdOperacaoResgateVinculada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicaoVinculada
			{
				get
				{
					System.Int32? data = entity.IdPosicaoVinculada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoVinculada = null;
					else entity.IdPosicaoVinculada = Convert.ToInt32(value);
				}
			}
				
			public System.String QuantidadeVinculada
			{
				get
				{
					System.Decimal? data = entity.QuantidadeVinculada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeVinculada = null;
					else entity.QuantidadeVinculada = Convert.ToDecimal(value);
				}
			}
			

			private esColagemOperResgPosFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esColagemOperResgPosFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esColagemOperResgPosFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ColagemOperResgPosFundo : esColagemOperResgPosFundo
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esColagemOperResgPosFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ColagemOperResgPosFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdColagemOperResgPosFundo
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosFundoMetadata.ColumnNames.IdColagemOperResgPosFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosFundoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosFundoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosFundoMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdColagemResgateDetalhe
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosFundoMetadata.ColumnNames.IdColagemResgateDetalhe, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacaoResgateVinculada
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosFundoMetadata.ColumnNames.IdOperacaoResgateVinculada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicaoVinculada
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosFundoMetadata.ColumnNames.IdPosicaoVinculada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QuantidadeVinculada
		{
			get
			{
				return new esQueryItem(this, ColagemOperResgPosFundoMetadata.ColumnNames.QuantidadeVinculada, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ColagemOperResgPosFundoCollection")]
	public partial class ColagemOperResgPosFundoCollection : esColagemOperResgPosFundoCollection, IEnumerable<ColagemOperResgPosFundo>
	{
		public ColagemOperResgPosFundoCollection()
		{

		}
		
		public static implicit operator List<ColagemOperResgPosFundo>(ColagemOperResgPosFundoCollection coll)
		{
			List<ColagemOperResgPosFundo> list = new List<ColagemOperResgPosFundo>();
			
			foreach (ColagemOperResgPosFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ColagemOperResgPosFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemOperResgPosFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ColagemOperResgPosFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ColagemOperResgPosFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ColagemOperResgPosFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemOperResgPosFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ColagemOperResgPosFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ColagemOperResgPosFundo AddNew()
		{
			ColagemOperResgPosFundo entity = base.AddNewEntity() as ColagemOperResgPosFundo;
			
			return entity;
		}

		public ColagemOperResgPosFundo FindByPrimaryKey(System.Int32 idColagemOperResgPosFundo)
		{
			return base.FindByPrimaryKey(idColagemOperResgPosFundo) as ColagemOperResgPosFundo;
		}


		#region IEnumerable<ColagemOperResgPosFundo> Members

		IEnumerator<ColagemOperResgPosFundo> IEnumerable<ColagemOperResgPosFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ColagemOperResgPosFundo;
			}
		}

		#endregion
		
		private ColagemOperResgPosFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ColagemOperResgPosFundo' table
	/// </summary>

	[Serializable]
	public partial class ColagemOperResgPosFundo : esColagemOperResgPosFundo
	{
		public ColagemOperResgPosFundo()
		{

		}
	
		public ColagemOperResgPosFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ColagemOperResgPosFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esColagemOperResgPosFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemOperResgPosFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ColagemOperResgPosFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemOperResgPosFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ColagemOperResgPosFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ColagemOperResgPosFundoQuery query;
	}



	[Serializable]
	public partial class ColagemOperResgPosFundoQuery : esColagemOperResgPosFundoQuery
	{
		public ColagemOperResgPosFundoQuery()
		{

		}		
		
		public ColagemOperResgPosFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ColagemOperResgPosFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ColagemOperResgPosFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ColagemOperResgPosFundoMetadata.ColumnNames.IdColagemOperResgPosFundo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemOperResgPosFundoMetadata.PropertyNames.IdColagemOperResgPosFundo;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemOperResgPosFundoMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemOperResgPosFundoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemOperResgPosFundoMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemOperResgPosFundoMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemOperResgPosFundoMetadata.ColumnNames.DataReferencia, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemOperResgPosFundoMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemOperResgPosFundoMetadata.ColumnNames.IdColagemResgateDetalhe, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemOperResgPosFundoMetadata.PropertyNames.IdColagemResgateDetalhe;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemOperResgPosFundoMetadata.ColumnNames.IdOperacaoResgateVinculada, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemOperResgPosFundoMetadata.PropertyNames.IdOperacaoResgateVinculada;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemOperResgPosFundoMetadata.ColumnNames.IdPosicaoVinculada, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemOperResgPosFundoMetadata.PropertyNames.IdPosicaoVinculada;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemOperResgPosFundoMetadata.ColumnNames.QuantidadeVinculada, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemOperResgPosFundoMetadata.PropertyNames.QuantidadeVinculada;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ColagemOperResgPosFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdColagemOperResgPosFundo = "IdColagemOperResgPosFundo";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCliente = "IdCliente";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdColagemResgateDetalhe = "IdColagemResgateDetalhe";
			 public const string IdOperacaoResgateVinculada = "IdOperacaoResgateVinculada";
			 public const string IdPosicaoVinculada = "IdPosicaoVinculada";
			 public const string QuantidadeVinculada = "QuantidadeVinculada";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdColagemOperResgPosFundo = "IdColagemOperResgPosFundo";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCliente = "IdCliente";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdColagemResgateDetalhe = "IdColagemResgateDetalhe";
			 public const string IdOperacaoResgateVinculada = "IdOperacaoResgateVinculada";
			 public const string IdPosicaoVinculada = "IdPosicaoVinculada";
			 public const string QuantidadeVinculada = "QuantidadeVinculada";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ColagemOperResgPosFundoMetadata))
			{
				if(ColagemOperResgPosFundoMetadata.mapDelegates == null)
				{
					ColagemOperResgPosFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ColagemOperResgPosFundoMetadata.meta == null)
				{
					ColagemOperResgPosFundoMetadata.meta = new ColagemOperResgPosFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdColagemOperResgPosFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdColagemResgateDetalhe", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacaoResgateVinculada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicaoVinculada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QuantidadeVinculada", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "ColagemOperResgPosFundo";
				meta.Destination = "ColagemOperResgPosFundo";
				
				meta.spInsert = "proc_ColagemOperResgPosFundoInsert";				
				meta.spUpdate = "proc_ColagemOperResgPosFundoUpdate";		
				meta.spDelete = "proc_ColagemOperResgPosFundoDelete";
				meta.spLoadAll = "proc_ColagemOperResgPosFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_ColagemOperResgPosFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ColagemOperResgPosFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
