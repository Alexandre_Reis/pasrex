/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 30/09/2015 11:47:11
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esColagemComeCotasCollection : esEntityCollection
	{
		public esColagemComeCotasCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ColagemComeCotasCollection";
		}

		#region Query Logic
		protected void InitQuery(esColagemComeCotasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esColagemComeCotasQuery);
		}
		#endregion
		
		virtual public ColagemComeCotas DetachEntity(ColagemComeCotas entity)
		{
			return base.DetachEntity(entity) as ColagemComeCotas;
		}
		
		virtual public ColagemComeCotas AttachEntity(ColagemComeCotas entity)
		{
			return base.AttachEntity(entity) as ColagemComeCotas;
		}
		
		virtual public void Combine(ColagemComeCotasCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ColagemComeCotas this[int index]
		{
			get
			{
				return base[index] as ColagemComeCotas;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ColagemComeCotas);
		}
	}



	[Serializable]
	abstract public class esColagemComeCotas : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esColagemComeCotasQuery GetDynamicQuery()
		{
			return null;
		}

		public esColagemComeCotas()
		{

		}

		public esColagemComeCotas(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idColagemComeCotas)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemComeCotas);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemComeCotas);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idColagemComeCotas)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemComeCotas);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemComeCotas);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idColagemComeCotas)
		{
			esColagemComeCotasQuery query = this.GetDynamicQuery();
			query.Where(query.IdColagemComeCotas == idColagemComeCotas);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idColagemComeCotas)
		{
			esParameters parms = new esParameters();
			parms.Add("IdColagemComeCotas",idColagemComeCotas);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdColagemComeCotas": this.str.IdColagemComeCotas = (string)value; break;							
						case "TipoRegistro": this.str.TipoRegistro = (string)value; break;							
						case "CodigoExterno": this.str.CodigoExterno = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "CotaOperacao": this.str.CotaOperacao = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "ValorCPMF": this.str.ValorCPMF = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "PrejuizoUsado": this.str.PrejuizoUsado = (string)value; break;							
						case "RendimentoComeCotas": this.str.RendimentoComeCotas = (string)value; break;							
						case "VariacaoComeCotas": this.str.VariacaoComeCotas = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "Processado": this.str.Processado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdColagemComeCotas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemComeCotas = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "CotaOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaOperacao = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "ValorCPMF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMF = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "PrejuizoUsado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoUsado = (System.Decimal?)value;
							break;
						
						case "RendimentoComeCotas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoComeCotas = (System.Decimal?)value;
							break;
						
						case "VariacaoComeCotas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VariacaoComeCotas = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ColagemComeCotas.IdColagemComeCotas
		/// </summary>
		virtual public System.Int32? IdColagemComeCotas
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasMetadata.ColumnNames.IdColagemComeCotas);
			}
			
			set
			{
				base.SetSystemInt32(ColagemComeCotasMetadata.ColumnNames.IdColagemComeCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.TipoRegistro
		/// </summary>
		virtual public System.String TipoRegistro
		{
			get
			{
				return base.GetSystemString(ColagemComeCotasMetadata.ColumnNames.TipoRegistro);
			}
			
			set
			{
				base.SetSystemString(ColagemComeCotasMetadata.ColumnNames.TipoRegistro, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.CodigoExterno
		/// </summary>
		virtual public System.String CodigoExterno
		{
			get
			{
				return base.GetSystemString(ColagemComeCotasMetadata.ColumnNames.CodigoExterno);
			}
			
			set
			{
				base.SetSystemString(ColagemComeCotasMetadata.ColumnNames.CodigoExterno, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(ColagemComeCotasMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemComeCotasMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(ColagemComeCotasMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(ColagemComeCotasMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(ColagemComeCotasMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemComeCotasMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.CotaOperacao
		/// </summary>
		virtual public System.Decimal? CotaOperacao
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.CotaOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.CotaOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.ValorCPMF
		/// </summary>
		virtual public System.Decimal? ValorCPMF
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.ValorCPMF);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.ValorCPMF, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.PrejuizoUsado
		/// </summary>
		virtual public System.Decimal? PrejuizoUsado
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.PrejuizoUsado);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.PrejuizoUsado, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.RendimentoComeCotas
		/// </summary>
		virtual public System.Decimal? RendimentoComeCotas
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.RendimentoComeCotas);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.RendimentoComeCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.VariacaoComeCotas
		/// </summary>
		virtual public System.Decimal? VariacaoComeCotas
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.VariacaoComeCotas);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.VariacaoComeCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotas.Processado
		/// </summary>
		virtual public System.String Processado
		{
			get
			{
				return base.GetSystemString(ColagemComeCotasMetadata.ColumnNames.Processado);
			}
			
			set
			{
				base.SetSystemString(ColagemComeCotasMetadata.ColumnNames.Processado, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esColagemComeCotas entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdColagemComeCotas
			{
				get
				{
					System.Int32? data = entity.IdColagemComeCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemComeCotas = null;
					else entity.IdColagemComeCotas = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoRegistro
			{
				get
				{
					System.String data = entity.TipoRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoRegistro = null;
					else entity.TipoRegistro = Convert.ToString(value);
				}
			}
				
			public System.String CodigoExterno
			{
				get
				{
					System.String data = entity.CodigoExterno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoExterno = null;
					else entity.CodigoExterno = Convert.ToString(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaOperacao
			{
				get
				{
					System.Decimal? data = entity.CotaOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaOperacao = null;
					else entity.CotaOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCPMF
			{
				get
				{
					System.Decimal? data = entity.ValorCPMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMF = null;
					else entity.ValorCPMF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrejuizoUsado
			{
				get
				{
					System.Decimal? data = entity.PrejuizoUsado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoUsado = null;
					else entity.PrejuizoUsado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoComeCotas
			{
				get
				{
					System.Decimal? data = entity.RendimentoComeCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoComeCotas = null;
					else entity.RendimentoComeCotas = Convert.ToDecimal(value);
				}
			}
				
			public System.String VariacaoComeCotas
			{
				get
				{
					System.Decimal? data = entity.VariacaoComeCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VariacaoComeCotas = null;
					else entity.VariacaoComeCotas = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String Processado
			{
				get
				{
					System.String data = entity.Processado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Processado = null;
					else entity.Processado = Convert.ToString(value);
				}
			}
			

			private esColagemComeCotas entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esColagemComeCotasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esColagemComeCotas can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ColagemComeCotas : esColagemComeCotas
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esColagemComeCotasQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ColagemComeCotasMetadata.Meta();
			}
		}	
		

		public esQueryItem IdColagemComeCotas
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.IdColagemComeCotas, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoRegistro
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.TipoRegistro, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoExterno
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.CodigoExterno, esSystemType.String);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaOperacao
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.CotaOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCPMF
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.ValorCPMF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrejuizoUsado
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.PrejuizoUsado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoComeCotas
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.RendimentoComeCotas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VariacaoComeCotas
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.VariacaoComeCotas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Processado
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMetadata.ColumnNames.Processado, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ColagemComeCotasCollection")]
	public partial class ColagemComeCotasCollection : esColagemComeCotasCollection, IEnumerable<ColagemComeCotas>
	{
		public ColagemComeCotasCollection()
		{

		}
		
		public static implicit operator List<ColagemComeCotas>(ColagemComeCotasCollection coll)
		{
			List<ColagemComeCotas> list = new List<ColagemComeCotas>();
			
			foreach (ColagemComeCotas emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ColagemComeCotasMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemComeCotasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ColagemComeCotas(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ColagemComeCotas();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ColagemComeCotasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemComeCotasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ColagemComeCotasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ColagemComeCotas AddNew()
		{
			ColagemComeCotas entity = base.AddNewEntity() as ColagemComeCotas;
			
			return entity;
		}

		public ColagemComeCotas FindByPrimaryKey(System.Int32 idColagemComeCotas)
		{
			return base.FindByPrimaryKey(idColagemComeCotas) as ColagemComeCotas;
		}


		#region IEnumerable<ColagemComeCotas> Members

		IEnumerator<ColagemComeCotas> IEnumerable<ColagemComeCotas>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ColagemComeCotas;
			}
		}

		#endregion
		
		private ColagemComeCotasQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ColagemComeCotas' table
	/// </summary>

	[Serializable]
	public partial class ColagemComeCotas : esColagemComeCotas
	{
		public ColagemComeCotas()
		{

		}
	
		public ColagemComeCotas(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ColagemComeCotasMetadata.Meta();
			}
		}
		
		
		
		override protected esColagemComeCotasQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemComeCotasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ColagemComeCotasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemComeCotasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ColagemComeCotasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ColagemComeCotasQuery query;
	}



	[Serializable]
	public partial class ColagemComeCotasQuery : esColagemComeCotasQuery
	{
		public ColagemComeCotasQuery()
		{

		}		
		
		public ColagemComeCotasQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ColagemComeCotasMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ColagemComeCotasMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.IdColagemComeCotas, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.IdColagemComeCotas;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.TipoRegistro, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.TipoRegistro;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.CodigoExterno, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.CodigoExterno;
			c.CharacterMaxLength = 30;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.DataReferencia, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.IdCliente, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.IdCarteira, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.DataConversao, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.Quantidade, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.CotaOperacao, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.CotaOperacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.ValorBruto, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.ValorLiquido, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.ValorCPMF, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.ValorCPMF;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.ValorPerformance, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.PrejuizoUsado, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.PrejuizoUsado;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.RendimentoComeCotas, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.RendimentoComeCotas;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.VariacaoComeCotas, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.VariacaoComeCotas;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.ValorIR, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.ValorIOF, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMetadata.ColumnNames.Processado, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = ColagemComeCotasMetadata.PropertyNames.Processado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ColagemComeCotasMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdColagemComeCotas = "IdColagemComeCotas";
			 public const string TipoRegistro = "TipoRegistro";
			 public const string CodigoExterno = "CodigoExterno";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataConversao = "DataConversao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaOperacao = "CotaOperacao";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoComeCotas = "RendimentoComeCotas";
			 public const string VariacaoComeCotas = "VariacaoComeCotas";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string Processado = "Processado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdColagemComeCotas = "IdColagemComeCotas";
			 public const string TipoRegistro = "TipoRegistro";
			 public const string CodigoExterno = "CodigoExterno";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataConversao = "DataConversao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaOperacao = "CotaOperacao";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoComeCotas = "RendimentoComeCotas";
			 public const string VariacaoComeCotas = "VariacaoComeCotas";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string Processado = "Processado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ColagemComeCotasMetadata))
			{
				if(ColagemComeCotasMetadata.mapDelegates == null)
				{
					ColagemComeCotasMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ColagemComeCotasMetadata.meta == null)
				{
					ColagemComeCotasMetadata.meta = new ColagemComeCotasMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdColagemComeCotas", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoRegistro", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("CodigoExterno", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCPMF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrejuizoUsado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoComeCotas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VariacaoComeCotas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Processado", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "ColagemComeCotas";
				meta.Destination = "ColagemComeCotas";
				
				meta.spInsert = "proc_ColagemComeCotasInsert";				
				meta.spUpdate = "proc_ColagemComeCotasUpdate";		
				meta.spDelete = "proc_ColagemComeCotasDelete";
				meta.spLoadAll = "proc_ColagemComeCotasLoadAll";
				meta.spLoadByPrimaryKey = "proc_ColagemComeCotasLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ColagemComeCotasMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
