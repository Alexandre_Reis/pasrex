/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:23 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTabelaTaxaFiscalizacaoCVMCollection : esEntityCollection
	{
		public esTabelaTaxaFiscalizacaoCVMCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaTaxaFiscalizacaoCVMCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaTaxaFiscalizacaoCVMQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaTaxaFiscalizacaoCVMQuery);
		}
		#endregion
		
		virtual public TabelaTaxaFiscalizacaoCVM DetachEntity(TabelaTaxaFiscalizacaoCVM entity)
		{
			return base.DetachEntity(entity) as TabelaTaxaFiscalizacaoCVM;
		}
		
		virtual public TabelaTaxaFiscalizacaoCVM AttachEntity(TabelaTaxaFiscalizacaoCVM entity)
		{
			return base.AttachEntity(entity) as TabelaTaxaFiscalizacaoCVM;
		}
		
		virtual public void Combine(TabelaTaxaFiscalizacaoCVMCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaTaxaFiscalizacaoCVM this[int index]
		{
			get
			{
				return base[index] as TabelaTaxaFiscalizacaoCVM;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaTaxaFiscalizacaoCVM);
		}
	}



	[Serializable]
	abstract public class esTabelaTaxaFiscalizacaoCVM : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaTaxaFiscalizacaoCVMQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaTaxaFiscalizacaoCVM()
		{

		}

		public esTabelaTaxaFiscalizacaoCVM(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 tipoCVM, System.Decimal faixaPL)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(tipoCVM, faixaPL);
			else
				return LoadByPrimaryKeyStoredProcedure(tipoCVM, faixaPL);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 tipoCVM, System.Decimal faixaPL)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaTaxaFiscalizacaoCVMQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.TipoCVM == tipoCVM, query.FaixaPL == faixaPL);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 tipoCVM, System.Decimal faixaPL)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(tipoCVM, faixaPL);
			else
				return LoadByPrimaryKeyStoredProcedure(tipoCVM, faixaPL);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 tipoCVM, System.Decimal faixaPL)
		{
			esTabelaTaxaFiscalizacaoCVMQuery query = this.GetDynamicQuery();
			query.Where(query.TipoCVM == tipoCVM, query.FaixaPL == faixaPL);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 tipoCVM, System.Decimal faixaPL)
		{
			esParameters parms = new esParameters();
			parms.Add("TipoCVM",tipoCVM);			parms.Add("FaixaPL",faixaPL);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "TipoCVM": this.str.TipoCVM = (string)value; break;							
						case "FaixaPL": this.str.FaixaPL = (string)value; break;							
						case "ValorTaxa": this.str.ValorTaxa = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "TipoCVM":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoCVM = (System.Int32?)value;
							break;
						
						case "FaixaPL":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.FaixaPL = (System.Decimal?)value;
							break;
						
						case "ValorTaxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorTaxa = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaTaxaFiscalizacaoCVM.TipoCVM
		/// </summary>
		virtual public System.Int32? TipoCVM
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.TipoCVM);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.TipoCVM, value))
				{
					this._UpToTipoInvestidorCVMByTipoCVM = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaFiscalizacaoCVM.FaixaPL
		/// </summary>
		virtual public System.Decimal? FaixaPL
		{
			get
			{
				return base.GetSystemDecimal(TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.FaixaPL);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.FaixaPL, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaFiscalizacaoCVM.ValorTaxa
		/// </summary>
		virtual public System.Decimal? ValorTaxa
		{
			get
			{
				return base.GetSystemDecimal(TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.ValorTaxa);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.ValorTaxa, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected TipoInvestidorCVM _UpToTipoInvestidorCVMByTipoCVM;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaTaxaFiscalizacaoCVM entity)
			{
				this.entity = entity;
			}
			
	
			public System.String TipoCVM
			{
				get
				{
					System.Int32? data = entity.TipoCVM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCVM = null;
					else entity.TipoCVM = Convert.ToInt32(value);
				}
			}
				
			public System.String FaixaPL
			{
				get
				{
					System.Decimal? data = entity.FaixaPL;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FaixaPL = null;
					else entity.FaixaPL = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorTaxa
			{
				get
				{
					System.Decimal? data = entity.ValorTaxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorTaxa = null;
					else entity.ValorTaxa = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaTaxaFiscalizacaoCVM entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaTaxaFiscalizacaoCVMQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaTaxaFiscalizacaoCVM can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaTaxaFiscalizacaoCVM : esTabelaTaxaFiscalizacaoCVM
	{

				
		#region UpToTipoInvestidorCVMByTipoCVM - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TipoInvestidorCVM_TabelaTaxaFiscalizacaoCVM_FK1
		/// </summary>

		[XmlIgnore]
		public TipoInvestidorCVM UpToTipoInvestidorCVMByTipoCVM
		{
			get
			{
				if(this._UpToTipoInvestidorCVMByTipoCVM == null
					&& TipoCVM != null					)
				{
					this._UpToTipoInvestidorCVMByTipoCVM = new TipoInvestidorCVM();
					this._UpToTipoInvestidorCVMByTipoCVM.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTipoInvestidorCVMByTipoCVM", this._UpToTipoInvestidorCVMByTipoCVM);
					this._UpToTipoInvestidorCVMByTipoCVM.Query.Where(this._UpToTipoInvestidorCVMByTipoCVM.Query.TipoCVM == this.TipoCVM);
					this._UpToTipoInvestidorCVMByTipoCVM.Query.Load();
				}

				return this._UpToTipoInvestidorCVMByTipoCVM;
			}
			
			set
			{
				this.RemovePreSave("UpToTipoInvestidorCVMByTipoCVM");
				

				if(value == null)
				{
					this.TipoCVM = null;
					this._UpToTipoInvestidorCVMByTipoCVM = null;
				}
				else
				{
					this.TipoCVM = value.TipoCVM;
					this._UpToTipoInvestidorCVMByTipoCVM = value;
					this.SetPreSave("UpToTipoInvestidorCVMByTipoCVM", this._UpToTipoInvestidorCVMByTipoCVM);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaTaxaFiscalizacaoCVMQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaTaxaFiscalizacaoCVMMetadata.Meta();
			}
		}	
		

		public esQueryItem TipoCVM
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.TipoCVM, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FaixaPL
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.FaixaPL, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorTaxa
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.ValorTaxa, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaTaxaFiscalizacaoCVMCollection")]
	public partial class TabelaTaxaFiscalizacaoCVMCollection : esTabelaTaxaFiscalizacaoCVMCollection, IEnumerable<TabelaTaxaFiscalizacaoCVM>
	{
		public TabelaTaxaFiscalizacaoCVMCollection()
		{

		}
		
		public static implicit operator List<TabelaTaxaFiscalizacaoCVM>(TabelaTaxaFiscalizacaoCVMCollection coll)
		{
			List<TabelaTaxaFiscalizacaoCVM> list = new List<TabelaTaxaFiscalizacaoCVM>();
			
			foreach (TabelaTaxaFiscalizacaoCVM emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaTaxaFiscalizacaoCVMMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaTaxaFiscalizacaoCVMQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaTaxaFiscalizacaoCVM(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaTaxaFiscalizacaoCVM();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaTaxaFiscalizacaoCVMQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaTaxaFiscalizacaoCVMQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaTaxaFiscalizacaoCVMQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaTaxaFiscalizacaoCVM AddNew()
		{
			TabelaTaxaFiscalizacaoCVM entity = base.AddNewEntity() as TabelaTaxaFiscalizacaoCVM;
			
			return entity;
		}

		public TabelaTaxaFiscalizacaoCVM FindByPrimaryKey(System.Int32 tipoCVM, System.Decimal faixaPL)
		{
			return base.FindByPrimaryKey(tipoCVM, faixaPL) as TabelaTaxaFiscalizacaoCVM;
		}


		#region IEnumerable<TabelaTaxaFiscalizacaoCVM> Members

		IEnumerator<TabelaTaxaFiscalizacaoCVM> IEnumerable<TabelaTaxaFiscalizacaoCVM>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaTaxaFiscalizacaoCVM;
			}
		}

		#endregion
		
		private TabelaTaxaFiscalizacaoCVMQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaTaxaFiscalizacaoCVM' table
	/// </summary>

	[Serializable]
	public partial class TabelaTaxaFiscalizacaoCVM : esTabelaTaxaFiscalizacaoCVM
	{
		public TabelaTaxaFiscalizacaoCVM()
		{

		}
	
		public TabelaTaxaFiscalizacaoCVM(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaTaxaFiscalizacaoCVMMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaTaxaFiscalizacaoCVMQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaTaxaFiscalizacaoCVMQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaTaxaFiscalizacaoCVMQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaTaxaFiscalizacaoCVMQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaTaxaFiscalizacaoCVMQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaTaxaFiscalizacaoCVMQuery query;
	}



	[Serializable]
	public partial class TabelaTaxaFiscalizacaoCVMQuery : esTabelaTaxaFiscalizacaoCVMQuery
	{
		public TabelaTaxaFiscalizacaoCVMQuery()
		{

		}		
		
		public TabelaTaxaFiscalizacaoCVMQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaTaxaFiscalizacaoCVMMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaTaxaFiscalizacaoCVMMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.TipoCVM, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaFiscalizacaoCVMMetadata.PropertyNames.TipoCVM;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.FaixaPL, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTaxaFiscalizacaoCVMMetadata.PropertyNames.FaixaPL;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaFiscalizacaoCVMMetadata.ColumnNames.ValorTaxa, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTaxaFiscalizacaoCVMMetadata.PropertyNames.ValorTaxa;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaTaxaFiscalizacaoCVMMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string TipoCVM = "TipoCVM";
			 public const string FaixaPL = "FaixaPL";
			 public const string ValorTaxa = "ValorTaxa";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string TipoCVM = "TipoCVM";
			 public const string FaixaPL = "FaixaPL";
			 public const string ValorTaxa = "ValorTaxa";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaTaxaFiscalizacaoCVMMetadata))
			{
				if(TabelaTaxaFiscalizacaoCVMMetadata.mapDelegates == null)
				{
					TabelaTaxaFiscalizacaoCVMMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaTaxaFiscalizacaoCVMMetadata.meta == null)
				{
					TabelaTaxaFiscalizacaoCVMMetadata.meta = new TabelaTaxaFiscalizacaoCVMMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("TipoCVM", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FaixaPL", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorTaxa", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaTaxaFiscalizacaoCVM";
				meta.Destination = "TabelaTaxaFiscalizacaoCVM";
				
				meta.spInsert = "proc_TabelaTaxaFiscalizacaoCVMInsert";				
				meta.spUpdate = "proc_TabelaTaxaFiscalizacaoCVMUpdate";		
				meta.spDelete = "proc_TabelaTaxaFiscalizacaoCVMDelete";
				meta.spLoadAll = "proc_TabelaTaxaFiscalizacaoCVMLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaTaxaFiscalizacaoCVMLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaTaxaFiscalizacaoCVMMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
