/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/12/2014 10:04:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

    [Serializable]
    abstract public class esParametroLiqAvancadoAnaliticoCollection : esEntityCollection
    {
        public esParametroLiqAvancadoAnaliticoCollection()
        {

        }

        protected override string GetCollectionName()
        {
            return "ParametroLiqAvancadoAnaliticoCollection";
        }

        #region Query Logic
        protected void InitQuery(esParametroLiqAvancadoAnaliticoQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntityCollection)this).Connection;
        }

        protected bool OnQueryLoaded(DataTable table)
        {
            this.PopulateCollection(table);
            return (this.RowCount > 0) ? true : false;
        }

        protected override void HookupQuery(esDynamicQuery query)
        {
            this.InitQuery(query as esParametroLiqAvancadoAnaliticoQuery);
        }
        #endregion

        virtual public ParametroLiqAvancadoAnalitico DetachEntity(ParametroLiqAvancadoAnalitico entity)
        {
            return base.DetachEntity(entity) as ParametroLiqAvancadoAnalitico;
        }

        virtual public ParametroLiqAvancadoAnalitico AttachEntity(ParametroLiqAvancadoAnalitico entity)
        {
            return base.AttachEntity(entity) as ParametroLiqAvancadoAnalitico;
        }

        virtual public void Combine(ParametroLiqAvancadoAnaliticoCollection collection)
        {
            base.Combine(collection);
        }

        new public ParametroLiqAvancadoAnalitico this[int index]
        {
            get
            {
                return base[index] as ParametroLiqAvancadoAnalitico;
            }
        }

        public override Type GetEntityType()
        {
            return typeof(ParametroLiqAvancadoAnalitico);
        }
    }



    [Serializable]
    abstract public class esParametroLiqAvancadoAnalitico : esEntity
    {
        /// <summary>
        /// Used internally by the entity's DynamicQuery mechanism.
        /// </summary>
        virtual protected esParametroLiqAvancadoAnaliticoQuery GetDynamicQuery()
        {
            return null;
        }

        public esParametroLiqAvancadoAnalitico()
        {

        }

        public esParametroLiqAvancadoAnalitico(DataRow row)
            : base(row)
        {

        }

        #region LoadByPrimaryKey
        public virtual bool LoadByPrimaryKey(System.DateTime dataVigencia, System.Int32 idCarteira, System.String idParametroLiqAvancado, System.Int32 tipoData, System.Int32 tipoOperacao)
        {
            if (this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(dataVigencia, idCarteira, idParametroLiqAvancado, tipoData, tipoOperacao);
            else
                return LoadByPrimaryKeyStoredProcedure(dataVigencia, idCarteira, idParametroLiqAvancado, tipoData, tipoOperacao);
        }

        public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataVigencia, System.Int32 idCarteira, System.String idParametroLiqAvancado, System.Int32 tipoData, System.Int32 tipoOperacao)
        {
            if (sqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(dataVigencia, idCarteira, idParametroLiqAvancado, tipoData, tipoOperacao);
            else
                return LoadByPrimaryKeyStoredProcedure(dataVigencia, idCarteira, idParametroLiqAvancado, tipoData, tipoOperacao);
        }

        private bool LoadByPrimaryKeyDynamic(System.DateTime dataVigencia, System.Int32 idCarteira, System.String idParametroLiqAvancado, System.Int32 tipoData, System.Int32 tipoOperacao)
        {
            esParametroLiqAvancadoAnaliticoQuery query = this.GetDynamicQuery();
            query.Where(query.DataVigencia == dataVigencia, query.IdCarteira == idCarteira, query.IdParametroLiqAvancado == idParametroLiqAvancado, query.TipoData == tipoData, query.TipoOperacao == tipoOperacao);
            return query.Load();
        }

        private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataVigencia, System.Int32 idCarteira, System.String idParametroLiqAvancado, System.Int32 tipoData, System.Int32 tipoOperacao)
        {
            esParameters parms = new esParameters();
            parms.Add("DataVigencia", dataVigencia); parms.Add("IdCarteira", idCarteira); parms.Add("IdParametroLiqAvancado", idParametroLiqAvancado); parms.Add("TipoData", tipoData); parms.Add("TipoOperacao", tipoOperacao);
            return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
        }
        #endregion



        #region Properties


        public override void SetProperties(IDictionary values)
        {
            foreach (string propertyName in values.Keys)
            {
                this.SetProperty(propertyName, values[propertyName]);
            }
        }

        public override void SetProperty(string name, object value)
        {
            if (this.Row == null) this.AddNew();

            esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
            if (col != null)
            {
                if (value == null || value.GetType().ToString() == "System.String")
                {
                    // Use the strongly typed property
                    switch (name)
                    {
                        case "IdParametroLiqAvancado": this.str.IdParametroLiqAvancado = (string)value; break;
                        case "IdCarteira": this.str.IdCarteira = (string)value; break;
                        case "DataVigencia": this.str.DataVigencia = (string)value; break;
                        case "TipoData": this.str.TipoData = (string)value; break;
                        case "TipoOperacao": this.str.TipoOperacao = (string)value; break;
                        case "ParametroUm": this.str.ParametroUm = (string)value; break;
                        case "ParametroDois": this.str.ParametroDois = (string)value; break;
                        case "ParametroComposto": this.str.ParametroComposto = (string)value; break;
                        case "OrdemProcessamento": this.str.OrdemProcessamento = (string)value; break;
                    }
                }
                else
                {
                    switch (name)
                    {
                        case "IdCarteira":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdCarteira = (System.Int32?)value;
                            break;

                        case "DataVigencia":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataVigencia = (System.DateTime?)value;
                            break;

                        case "TipoData":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.TipoData = (System.Int32?)value;
                            break;

                        case "TipoOperacao":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.TipoOperacao = (System.Int32?)value;
                            break;

                        case "OrdemProcessamento":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.OrdemProcessamento = (System.Int32?)value;
                            break;


                        default:
                            break;
                    }
                }
            }
            else if (this.Row.Table.Columns.Contains(name))
            {
                this.Row[name] = value;
            }
            else
            {
                throw new Exception("SetProperty Error: '" + name + "' not found");
            }
        }


        /// <summary>
        /// Maps to ParametroLiqAvancadoAnalitico.IdParametroLiqAvancado
        /// </summary>
        virtual public System.String IdParametroLiqAvancado
        {
            get
            {
                return base.GetSystemString(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.IdParametroLiqAvancado);
            }

            set
            {
                base.SetSystemString(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.IdParametroLiqAvancado, value);
            }
        }

        /// <summary>
        /// Maps to ParametroLiqAvancadoAnalitico.IdCarteira
        /// </summary>
        virtual public System.Int32? IdCarteira
        {
            get
            {
                return base.GetSystemInt32(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.IdCarteira);
            }

            set
            {
                base.SetSystemInt32(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.IdCarteira, value);
            }
        }

        /// <summary>
        /// Maps to ParametroLiqAvancadoAnalitico.DataVigencia
        /// </summary>
        virtual public System.DateTime? DataVigencia
        {
            get
            {
                return base.GetSystemDateTime(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.DataVigencia);
            }

            set
            {
                base.SetSystemDateTime(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.DataVigencia, value);
            }
        }

        /// <summary>
        /// Maps to ParametroLiqAvancadoAnalitico.TipoData
        /// </summary>
        virtual public System.Int32? TipoData
        {
            get
            {
                return base.GetSystemInt32(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.TipoData);
            }

            set
            {
                base.SetSystemInt32(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.TipoData, value);
            }
        }

        /// <summary>
        /// Maps to ParametroLiqAvancadoAnalitico.TipoOperacao
        /// </summary>
        virtual public System.Int32? TipoOperacao
        {
            get
            {
                return base.GetSystemInt32(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.TipoOperacao);
            }

            set
            {
                base.SetSystemInt32(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.TipoOperacao, value);
            }
        }

        /// <summary>
        /// Maps to ParametroLiqAvancadoAnalitico.ParametroUm
        /// </summary>
        virtual public System.String ParametroUm
        {
            get
            {
                return base.GetSystemString(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.ParametroUm);
            }

            set
            {
                base.SetSystemString(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.ParametroUm, value);
            }
        }

        /// <summary>
        /// Maps to ParametroLiqAvancadoAnalitico.ParametroDois
        /// </summary>
        virtual public System.String ParametroDois
        {
            get
            {
                return base.GetSystemString(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.ParametroDois);
            }

            set
            {
                base.SetSystemString(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.ParametroDois, value);
            }
        }

        /// <summary>
        /// Maps to ParametroLiqAvancadoAnalitico.ParametroComposto
        /// </summary>
        virtual public System.String ParametroComposto
        {
            get
            {
                return base.GetSystemString(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.ParametroComposto);
            }

            set
            {
                base.SetSystemString(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.ParametroComposto, value);
            }
        }

        /// <summary>
        /// Maps to ParametroLiqAvancadoAnalitico.OrdemProcessamento
        /// </summary>
        virtual public System.Int32? OrdemProcessamento
        {
            get
            {
                return base.GetSystemInt32(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.OrdemProcessamento);
            }

            set
            {
                base.SetSystemInt32(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.OrdemProcessamento, value);
            }
        }

        #endregion

        #region String Properties


        [BrowsableAttribute(false)]
        public esStrings str
        {
            get
            {
                if (esstrings == null)
                {
                    esstrings = new esStrings(this);
                }
                return esstrings;
            }
        }


        [Serializable]
        sealed public class esStrings
        {
            public esStrings(esParametroLiqAvancadoAnalitico entity)
            {
                this.entity = entity;
            }


            public System.String IdParametroLiqAvancado
            {
                get
                {
                    System.String data = entity.IdParametroLiqAvancado;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdParametroLiqAvancado = null;
                    else entity.IdParametroLiqAvancado = Convert.ToString(value);
                }
            }

            public System.String IdCarteira
            {
                get
                {
                    System.Int32? data = entity.IdCarteira;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdCarteira = null;
                    else entity.IdCarteira = Convert.ToInt32(value);
                }
            }

            public System.String DataVigencia
            {
                get
                {
                    System.DateTime? data = entity.DataVigencia;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.DataVigencia = null;
                    else entity.DataVigencia = Convert.ToDateTime(value);
                }
            }

            public System.String TipoData
            {
                get
                {
                    System.Int32? data = entity.TipoData;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.TipoData = null;
                    else entity.TipoData = Convert.ToInt32(value);
                }
            }

            public System.String TipoOperacao
            {
                get
                {
                    System.Int32? data = entity.TipoOperacao;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.TipoOperacao = null;
                    else entity.TipoOperacao = Convert.ToInt32(value);
                }
            }

            public System.String ParametroUm
            {
                get
                {
                    System.String data = entity.ParametroUm;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.ParametroUm = null;
                    else entity.ParametroUm = Convert.ToString(value);
                }
            }

            public System.String ParametroDois
            {
                get
                {
                    System.String data = entity.ParametroDois;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.ParametroDois = null;
                    else entity.ParametroDois = Convert.ToString(value);
                }
            }

            public System.String ParametroComposto
            {
                get
                {
                    System.String data = entity.ParametroComposto;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.ParametroComposto = null;
                    else entity.ParametroComposto = Convert.ToString(value);
                }
            }

            public System.String OrdemProcessamento
            {
                get
                {
                    System.Int32? data = entity.OrdemProcessamento;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.OrdemProcessamento = null;
                    else entity.OrdemProcessamento = Convert.ToInt32(value);
                }
            }


            private esParametroLiqAvancadoAnalitico entity;
        }
        #endregion

        #region Query Logic
        protected void InitQuery(esParametroLiqAvancadoAnaliticoQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntity)this).Connection;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        protected bool OnQueryLoaded(DataTable table)
        {
            bool dataFound = this.PopulateEntity(table);

            if (this.RowCount > 1)
            {
                throw new Exception("esParametroLiqAvancadoAnalitico can only hold one record of data");
            }

            return dataFound;
        }
        #endregion

        [NonSerialized]
        private esStrings esstrings;
    }



    public partial class ParametroLiqAvancadoAnalitico : esParametroLiqAvancadoAnalitico
    {


        /// <summary>
        /// Used internally by the entity's hierarchical properties.
        /// </summary>
        protected override List<esPropertyDescriptor> GetHierarchicalProperties()
        {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();


            return props;
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PreSave.
        /// </summary>
        protected override void ApplyPreSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostSave.
        /// </summary>
        protected override void ApplyPostSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostOneToOneSave.
        /// </summary>
        protected override void ApplyPostOneSaveKeys()
        {
        }

    }



    [Serializable]
    abstract public class esParametroLiqAvancadoAnaliticoQuery : esDynamicQuery
    {
        override protected IMetadata Meta
        {
            get
            {
                return ParametroLiqAvancadoAnaliticoMetadata.Meta();
            }
        }


        public esQueryItem IdParametroLiqAvancado
        {
            get
            {
                return new esQueryItem(this, ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.IdParametroLiqAvancado, esSystemType.String);
            }
        }

        public esQueryItem IdCarteira
        {
            get
            {
                return new esQueryItem(this, ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
            }
        }

        public esQueryItem DataVigencia
        {
            get
            {
                return new esQueryItem(this, ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.DataVigencia, esSystemType.DateTime);
            }
        }

        public esQueryItem TipoData
        {
            get
            {
                return new esQueryItem(this, ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.TipoData, esSystemType.Int32);
            }
        }

        public esQueryItem TipoOperacao
        {
            get
            {
                return new esQueryItem(this, ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.TipoOperacao, esSystemType.Int32);
            }
        }

        public esQueryItem ParametroUm
        {
            get
            {
                return new esQueryItem(this, ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.ParametroUm, esSystemType.String);
            }
        }

        public esQueryItem ParametroDois
        {
            get
            {
                return new esQueryItem(this, ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.ParametroDois, esSystemType.String);
            }
        }

        public esQueryItem ParametroComposto
        {
            get
            {
                return new esQueryItem(this, ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.ParametroComposto, esSystemType.String);
            }
        }

        public esQueryItem OrdemProcessamento
        {
            get
            {
                return new esQueryItem(this, ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.OrdemProcessamento, esSystemType.Int32);
            }
        }

    }



    [Serializable]
    [XmlType("ParametroLiqAvancadoAnaliticoCollection")]
    public partial class ParametroLiqAvancadoAnaliticoCollection : esParametroLiqAvancadoAnaliticoCollection, IEnumerable<ParametroLiqAvancadoAnalitico>
    {
        public ParametroLiqAvancadoAnaliticoCollection()
        {

        }

        public static implicit operator List<ParametroLiqAvancadoAnalitico>(ParametroLiqAvancadoAnaliticoCollection coll)
        {
            List<ParametroLiqAvancadoAnalitico> list = new List<ParametroLiqAvancadoAnalitico>();

            foreach (ParametroLiqAvancadoAnalitico emp in coll)
            {
                list.Add(emp);
            }

            return list;
        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return ParametroLiqAvancadoAnaliticoMetadata.Meta();
            }
        }



        override protected esDynamicQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new ParametroLiqAvancadoAnaliticoQuery();
                this.InitQuery(query);
            }
            return this.query;
        }

        override protected esEntity CreateEntityForCollection(DataRow row)
        {
            return new ParametroLiqAvancadoAnalitico(row);
        }

        override protected esEntity CreateEntity()
        {
            return new ParametroLiqAvancadoAnalitico();
        }


        #endregion


        [BrowsableAttribute(false)]
        public ParametroLiqAvancadoAnaliticoQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new ParametroLiqAvancadoAnaliticoQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }

        public bool Load(ParametroLiqAvancadoAnaliticoQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        public ParametroLiqAvancadoAnalitico AddNew()
        {
            ParametroLiqAvancadoAnalitico entity = base.AddNewEntity() as ParametroLiqAvancadoAnalitico;

            return entity;
        }

        public ParametroLiqAvancadoAnalitico FindByPrimaryKey(System.DateTime dataVigencia, System.Int32 idCarteira, System.String idParametroLiqAvancado, System.Int32 tipoData, System.Int32 tipoOperacao)
        {
            return base.FindByPrimaryKey(dataVigencia, idCarteira, idParametroLiqAvancado, tipoData, tipoOperacao) as ParametroLiqAvancadoAnalitico;
        }


        #region IEnumerable<ParametroLiqAvancadoAnalitico> Members

        IEnumerator<ParametroLiqAvancadoAnalitico> IEnumerable<ParametroLiqAvancadoAnalitico>.GetEnumerator()
        {
            System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
            System.Collections.IEnumerator iterator = enumer.GetEnumerator();

            while (iterator.MoveNext())
            {
                yield return iterator.Current as ParametroLiqAvancadoAnalitico;
            }
        }

        #endregion

        private ParametroLiqAvancadoAnaliticoQuery query;
    }


    /// <summary>
    /// Encapsulates the 'ParametroLiqAvancadoAnalitico' table
    /// </summary>

    [Serializable]
    public partial class ParametroLiqAvancadoAnalitico : esParametroLiqAvancadoAnalitico
    {
        public ParametroLiqAvancadoAnalitico()
        {

        }

        public ParametroLiqAvancadoAnalitico(DataRow row)
            : base(row)
        {

        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return ParametroLiqAvancadoAnaliticoMetadata.Meta();
            }
        }



        override protected esParametroLiqAvancadoAnaliticoQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new ParametroLiqAvancadoAnaliticoQuery();
                this.InitQuery(query);
            }
            return this.query;
        }
        #endregion




        [BrowsableAttribute(false)]
        public ParametroLiqAvancadoAnaliticoQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new ParametroLiqAvancadoAnaliticoQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }


        public bool Load(ParametroLiqAvancadoAnaliticoQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        private ParametroLiqAvancadoAnaliticoQuery query;
    }



    [Serializable]
    public partial class ParametroLiqAvancadoAnaliticoQuery : esParametroLiqAvancadoAnaliticoQuery
    {
        public ParametroLiqAvancadoAnaliticoQuery()
        {

        }

        public ParametroLiqAvancadoAnaliticoQuery(string joinAlias)
        {
            this.es.JoinAlias = joinAlias;
        }


    }



    [Serializable]
    public partial class ParametroLiqAvancadoAnaliticoMetadata : esMetadata, IMetadata
    {
        #region Protected Constructor
        protected ParametroLiqAvancadoAnaliticoMetadata()
        {
            _columns = new esColumnMetadataCollection();
            esColumnMetadata c;

            c = new esColumnMetadata(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.IdParametroLiqAvancado, 0, typeof(System.String), esSystemType.String);
            c.PropertyName = ParametroLiqAvancadoAnaliticoMetadata.PropertyNames.IdParametroLiqAvancado;
            c.IsInPrimaryKey = true;
            c.CharacterMaxLength = 10;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ParametroLiqAvancadoAnaliticoMetadata.PropertyNames.IdCarteira;
            c.IsInPrimaryKey = true;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.DataVigencia, 2, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = ParametroLiqAvancadoAnaliticoMetadata.PropertyNames.DataVigencia;
            c.IsInPrimaryKey = true;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.TipoData, 3, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ParametroLiqAvancadoAnaliticoMetadata.PropertyNames.TipoData;
            c.IsInPrimaryKey = true;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.TipoOperacao, 4, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ParametroLiqAvancadoAnaliticoMetadata.PropertyNames.TipoOperacao;
            c.IsInPrimaryKey = true;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.ParametroUm, 5, typeof(System.String), esSystemType.String);
            c.PropertyName = ParametroLiqAvancadoAnaliticoMetadata.PropertyNames.ParametroUm;
            c.CharacterMaxLength = 50;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.ParametroDois, 6, typeof(System.String), esSystemType.String);
            c.PropertyName = ParametroLiqAvancadoAnaliticoMetadata.PropertyNames.ParametroDois;
            c.CharacterMaxLength = 50;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.ParametroComposto, 7, typeof(System.String), esSystemType.String);
            c.PropertyName = ParametroLiqAvancadoAnaliticoMetadata.PropertyNames.ParametroComposto;
            c.CharacterMaxLength = 50;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ParametroLiqAvancadoAnaliticoMetadata.ColumnNames.OrdemProcessamento, 8, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ParametroLiqAvancadoAnaliticoMetadata.PropertyNames.OrdemProcessamento;
            c.NumericPrecision = 10;
            _columns.Add(c);


        }
        #endregion

        static public ParametroLiqAvancadoAnaliticoMetadata Meta()
        {
            return meta;
        }

        public Guid DataID
        {
            get { return base._dataID; }
        }

        public bool MultiProviderMode
        {
            get { return false; }
        }

        public esColumnMetadataCollection Columns
        {
            get { return base._columns; }
        }

        #region ColumnNames
        public class ColumnNames
        {
            public const string IdParametroLiqAvancado = "IdParametroLiqAvancado";
            public const string IdCarteira = "IdCarteira";
            public const string DataVigencia = "DataVigencia";
            public const string TipoData = "TipoData";
            public const string TipoOperacao = "TipoOperacao";
            public const string ParametroUm = "ParametroUm";
            public const string ParametroDois = "ParametroDois";
            public const string ParametroComposto = "ParametroComposto";
            public const string OrdemProcessamento = "OrdemProcessamento";
        }
        #endregion

        #region PropertyNames
        public class PropertyNames
        {
            public const string IdParametroLiqAvancado = "IdParametroLiqAvancado";
            public const string IdCarteira = "IdCarteira";
            public const string DataVigencia = "DataVigencia";
            public const string TipoData = "TipoData";
            public const string TipoOperacao = "TipoOperacao";
            public const string ParametroUm = "ParametroUm";
            public const string ParametroDois = "ParametroDois";
            public const string ParametroComposto = "ParametroComposto";
            public const string OrdemProcessamento = "OrdemProcessamento";
        }
        #endregion

        public esProviderSpecificMetadata GetProviderMetadata(string mapName)
        {
            MapToMeta mapMethod = mapDelegates[mapName];

            if (mapMethod != null)
                return mapMethod(mapName);
            else
                return null;
        }

        #region MAP esDefault

        static private int RegisterDelegateesDefault()
        {
            // This is only executed once per the life of the application
            lock (typeof(ParametroLiqAvancadoAnaliticoMetadata))
            {
                if (ParametroLiqAvancadoAnaliticoMetadata.mapDelegates == null)
                {
                    ParametroLiqAvancadoAnaliticoMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
                }

                if (ParametroLiqAvancadoAnaliticoMetadata.meta == null)
                {
                    ParametroLiqAvancadoAnaliticoMetadata.meta = new ParametroLiqAvancadoAnaliticoMetadata();
                }

                MapToMeta mapMethod = new MapToMeta(meta.esDefault);
                mapDelegates.Add("esDefault", mapMethod);
                mapMethod("esDefault");
            }
            return 0;
        }

        private esProviderSpecificMetadata esDefault(string mapName)
        {
            if (!_providerMetadataMaps.ContainsKey(mapName))
            {
                esProviderSpecificMetadata meta = new esProviderSpecificMetadata();


                meta.AddTypeMap("IdParametroLiqAvancado", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("DataVigencia", new esTypeMap("datetime", "System.DateTime"));
                meta.AddTypeMap("TipoData", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("TipoOperacao", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("ParametroUm", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("ParametroDois", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("ParametroComposto", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("OrdemProcessamento", new esTypeMap("int", "System.Int32"));



                meta.Source = "ParametroLiqAvancadoAnalitico";
                meta.Destination = "ParametroLiqAvancadoAnalitico";

                meta.spInsert = "proc_ParametroLiqAvancadoAnaliticoInsert";
                meta.spUpdate = "proc_ParametroLiqAvancadoAnaliticoUpdate";
                meta.spDelete = "proc_ParametroLiqAvancadoAnaliticoDelete";
                meta.spLoadAll = "proc_ParametroLiqAvancadoAnaliticoLoadAll";
                meta.spLoadByPrimaryKey = "proc_ParametroLiqAvancadoAnaliticoLoadByPrimaryKey";

                this._providerMetadataMaps["esDefault"] = meta;
            }

            return this._providerMetadataMaps["esDefault"];
        }

        #endregion

        static private ParametroLiqAvancadoAnaliticoMetadata meta;
        static protected Dictionary<string, MapToMeta> mapDelegates;
        static private int _esDefault = RegisterDelegateesDefault();
    }
}
