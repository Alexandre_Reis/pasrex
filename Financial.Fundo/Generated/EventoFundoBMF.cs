/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 13/07/2015 12:47:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esEventoFundoBMFCollection : esEntityCollection
	{
		public esEventoFundoBMFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EventoFundoBMFCollection";
		}

		#region Query Logic
		protected void InitQuery(esEventoFundoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEventoFundoBMFQuery);
		}
		#endregion
		
		virtual public EventoFundoBMF DetachEntity(EventoFundoBMF entity)
		{
			return base.DetachEntity(entity) as EventoFundoBMF;
		}
		
		virtual public EventoFundoBMF AttachEntity(EventoFundoBMF entity)
		{
			return base.AttachEntity(entity) as EventoFundoBMF;
		}
		
		virtual public void Combine(EventoFundoBMFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EventoFundoBMF this[int index]
		{
			get
			{
				return base[index] as EventoFundoBMF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EventoFundoBMF);
		}
	}



	[Serializable]
	abstract public class esEventoFundoBMF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEventoFundoBMFQuery GetDynamicQuery()
		{
			return null;
		}

		public esEventoFundoBMF()
		{

		}

		public esEventoFundoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String cdAtivoBMF, System.Int32 idAgente, System.Int32 idEventoFundo, System.String serie, System.Int32 tipoMercado)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivoBMF, idAgente, idEventoFundo, serie, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivoBMF, idAgente, idEventoFundo, serie, tipoMercado);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String cdAtivoBMF, System.Int32 idAgente, System.Int32 idEventoFundo, System.String serie, System.Int32 tipoMercado)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivoBMF, idAgente, idEventoFundo, serie, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivoBMF, idAgente, idEventoFundo, serie, tipoMercado);
		}

		private bool LoadByPrimaryKeyDynamic(System.String cdAtivoBMF, System.Int32 idAgente, System.Int32 idEventoFundo, System.String serie, System.Int32 tipoMercado)
		{
			esEventoFundoBMFQuery query = this.GetDynamicQuery();
			query.Where(query.CdAtivoBMF == cdAtivoBMF, query.IdAgente == idAgente, query.IdEventoFundo == idEventoFundo, query.Serie == serie, query.TipoMercado == tipoMercado);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String cdAtivoBMF, System.Int32 idAgente, System.Int32 idEventoFundo, System.String serie, System.Int32 tipoMercado)
		{
			esParameters parms = new esParameters();
			parms.Add("CdAtivoBMF",cdAtivoBMF);			parms.Add("IdAgente",idAgente);			parms.Add("IdEventoFundo",idEventoFundo);			parms.Add("Serie",serie);			parms.Add("TipoMercado",tipoMercado);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEventoFundo": this.str.IdEventoFundo = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "Serie": this.str.Serie = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "Processar": this.str.Processar = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEventoFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoFundo = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoMercado = (System.Int32?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "Processar":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Processar = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EventoFundoBMF.IdEventoFundo
		/// </summary>
		virtual public System.Int32? IdEventoFundo
		{
			get
			{
				return base.GetSystemInt32(EventoFundoBMFMetadata.ColumnNames.IdEventoFundo);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoFundoBMFMetadata.ColumnNames.IdEventoFundo, value))
				{
					this._UpToEventoFundoByIdEventoFundo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoBMF.TipoMercado
		/// </summary>
		virtual public System.Int32? TipoMercado
		{
			get
			{
				return base.GetSystemInt32(EventoFundoBMFMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoBMFMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoBMF.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(EventoFundoBMFMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				base.SetSystemString(EventoFundoBMFMetadata.ColumnNames.CdAtivoBMF, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoBMF.Serie
		/// </summary>
		virtual public System.String Serie
		{
			get
			{
				return base.GetSystemString(EventoFundoBMFMetadata.ColumnNames.Serie);
			}
			
			set
			{
				base.SetSystemString(EventoFundoBMFMetadata.ColumnNames.Serie, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoBMF.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(EventoFundoBMFMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(EventoFundoBMFMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoBMF.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(EventoFundoBMFMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(EventoFundoBMFMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoBMF.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(EventoFundoBMFMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoBMFMetadata.ColumnNames.IdAgente, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoBMF.Processar
		/// </summary>
		virtual public System.Byte? Processar
		{
			get
			{
				return base.GetSystemByte(EventoFundoBMFMetadata.ColumnNames.Processar);
			}
			
			set
			{
				base.SetSystemByte(EventoFundoBMFMetadata.ColumnNames.Processar, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected EventoFundo _UpToEventoFundoByIdEventoFundo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEventoFundoBMF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEventoFundo
			{
				get
				{
					System.Int32? data = entity.IdEventoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoFundo = null;
					else entity.IdEventoFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Int32? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String Serie
			{
				get
				{
					System.String data = entity.Serie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Serie = null;
					else entity.Serie = Convert.ToString(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String Processar
			{
				get
				{
					System.Byte? data = entity.Processar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Processar = null;
					else entity.Processar = Convert.ToByte(value);
				}
			}
			

			private esEventoFundoBMF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEventoFundoBMFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEventoFundoBMF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EventoFundoBMF : esEventoFundoBMF
	{

				
		#region UpToEventoFundoByIdEventoFundo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EventoFundoBMF_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundo UpToEventoFundoByIdEventoFundo
		{
			get
			{
				if(this._UpToEventoFundoByIdEventoFundo == null
					&& IdEventoFundo != null					)
				{
					this._UpToEventoFundoByIdEventoFundo = new EventoFundo();
					this._UpToEventoFundoByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Where(this._UpToEventoFundoByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Load();
				}

				return this._UpToEventoFundoByIdEventoFundo;
			}
			
			set
			{
				this.RemovePreSave("UpToEventoFundoByIdEventoFundo");
				

				if(value == null)
				{
					this.IdEventoFundo = null;
					this._UpToEventoFundoByIdEventoFundo = null;
				}
				else
				{
					this.IdEventoFundo = value.IdEventoFundo;
					this._UpToEventoFundoByIdEventoFundo = value;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEventoFundoByIdEventoFundo != null)
			{
				this.IdEventoFundo = this._UpToEventoFundoByIdEventoFundo.IdEventoFundo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEventoFundoBMFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoBMFMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEventoFundo
		{
			get
			{
				return new esQueryItem(this, EventoFundoBMFMetadata.ColumnNames.IdEventoFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, EventoFundoBMFMetadata.ColumnNames.TipoMercado, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, EventoFundoBMFMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem Serie
		{
			get
			{
				return new esQueryItem(this, EventoFundoBMFMetadata.ColumnNames.Serie, esSystemType.String);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, EventoFundoBMFMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, EventoFundoBMFMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, EventoFundoBMFMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Processar
		{
			get
			{
				return new esQueryItem(this, EventoFundoBMFMetadata.ColumnNames.Processar, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EventoFundoBMFCollection")]
	public partial class EventoFundoBMFCollection : esEventoFundoBMFCollection, IEnumerable<EventoFundoBMF>
	{
		public EventoFundoBMFCollection()
		{

		}
		
		public static implicit operator List<EventoFundoBMF>(EventoFundoBMFCollection coll)
		{
			List<EventoFundoBMF> list = new List<EventoFundoBMF>();
			
			foreach (EventoFundoBMF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EventoFundoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EventoFundoBMF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EventoFundoBMF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EventoFundoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EventoFundoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EventoFundoBMF AddNew()
		{
			EventoFundoBMF entity = base.AddNewEntity() as EventoFundoBMF;
			
			return entity;
		}

		public EventoFundoBMF FindByPrimaryKey(System.String cdAtivoBMF, System.Int32 idAgente, System.Int32 idEventoFundo, System.String serie, System.Int32 tipoMercado)
		{
			return base.FindByPrimaryKey(cdAtivoBMF, idAgente, idEventoFundo, serie, tipoMercado) as EventoFundoBMF;
		}


		#region IEnumerable<EventoFundoBMF> Members

		IEnumerator<EventoFundoBMF> IEnumerable<EventoFundoBMF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EventoFundoBMF;
			}
		}

		#endregion
		
		private EventoFundoBMFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EventoFundoBMF' table
	/// </summary>

	[Serializable]
	public partial class EventoFundoBMF : esEventoFundoBMF
	{
		public EventoFundoBMF()
		{

		}
	
		public EventoFundoBMF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoBMFMetadata.Meta();
			}
		}
		
		
		
		override protected esEventoFundoBMFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoBMFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EventoFundoBMFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoBMFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EventoFundoBMFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EventoFundoBMFQuery query;
	}



	[Serializable]
	public partial class EventoFundoBMFQuery : esEventoFundoBMFQuery
	{
		public EventoFundoBMFQuery()
		{

		}		
		
		public EventoFundoBMFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EventoFundoBMFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EventoFundoBMFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EventoFundoBMFMetadata.ColumnNames.IdEventoFundo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoBMFMetadata.PropertyNames.IdEventoFundo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoBMFMetadata.ColumnNames.TipoMercado, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoBMFMetadata.PropertyNames.TipoMercado;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoBMFMetadata.ColumnNames.CdAtivoBMF, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = EventoFundoBMFMetadata.PropertyNames.CdAtivoBMF;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoBMFMetadata.ColumnNames.Serie, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = EventoFundoBMFMetadata.PropertyNames.Serie;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoBMFMetadata.ColumnNames.Quantidade, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EventoFundoBMFMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoBMFMetadata.ColumnNames.ValorLiquido, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EventoFundoBMFMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoBMFMetadata.ColumnNames.IdAgente, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoBMFMetadata.PropertyNames.IdAgente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoBMFMetadata.ColumnNames.Processar, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EventoFundoBMFMetadata.PropertyNames.Processar;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EventoFundoBMFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string TipoMercado = "TipoMercado";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string Quantidade = "Quantidade";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string IdAgente = "IdAgente";
			 public const string Processar = "Processar";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string TipoMercado = "TipoMercado";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string Serie = "Serie";
			 public const string Quantidade = "Quantidade";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string IdAgente = "IdAgente";
			 public const string Processar = "Processar";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EventoFundoBMFMetadata))
			{
				if(EventoFundoBMFMetadata.mapDelegates == null)
				{
					EventoFundoBMFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EventoFundoBMFMetadata.meta == null)
				{
					EventoFundoBMFMetadata.meta = new EventoFundoBMFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEventoFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Serie", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Processar", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "EventoFundoBMF";
				meta.Destination = "EventoFundoBMF";
				
				meta.spInsert = "proc_EventoFundoBMFInsert";				
				meta.spUpdate = "proc_EventoFundoBMFUpdate";		
				meta.spDelete = "proc_EventoFundoBMFDelete";
				meta.spLoadAll = "proc_EventoFundoBMFLoadAll";
				meta.spLoadByPrimaryKey = "proc_EventoFundoBMFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EventoFundoBMFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
