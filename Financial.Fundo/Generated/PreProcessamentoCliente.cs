/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 01/02/2016 18:33:22
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esPreProcessamentoClienteCollection : esEntityCollection
	{
		public esPreProcessamentoClienteCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PreProcessamentoClienteCollection";
		}

		#region Query Logic
		protected void InitQuery(esPreProcessamentoClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPreProcessamentoClienteQuery);
		}
		#endregion
		
		virtual public PreProcessamentoCliente DetachEntity(PreProcessamentoCliente entity)
		{
			return base.DetachEntity(entity) as PreProcessamentoCliente;
		}
		
		virtual public PreProcessamentoCliente AttachEntity(PreProcessamentoCliente entity)
		{
			return base.AttachEntity(entity) as PreProcessamentoCliente;
		}
		
		virtual public void Combine(PreProcessamentoClienteCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PreProcessamentoCliente this[int index]
		{
			get
			{
				return base[index] as PreProcessamentoCliente;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PreProcessamentoCliente);
		}
	}



	[Serializable]
	abstract public class esPreProcessamentoCliente : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPreProcessamentoClienteQuery GetDynamicQuery()
		{
			return null;
		}

		public esPreProcessamentoCliente()
		{

		}

		public esPreProcessamentoCliente(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPreProcessamentoCliente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPreProcessamentoCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idPreProcessamentoCliente);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPreProcessamentoCliente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPreProcessamentoCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idPreProcessamentoCliente);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPreProcessamentoCliente)
		{
			esPreProcessamentoClienteQuery query = this.GetDynamicQuery();
			query.Where(query.IdPreProcessamentoCliente == idPreProcessamentoCliente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPreProcessamentoCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPreProcessamentoCliente",idPreProcessamentoCliente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPreProcessamentoCliente": this.str.IdPreProcessamentoCliente = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "Chave": this.str.Chave = (string)value; break;							
						case "DataDia": this.str.DataDia = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPreProcessamentoCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPreProcessamentoCliente = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "Chave":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Chave = (System.Int32?)value;
							break;
						
						case "DataDia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataDia = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PreProcessamentoCliente.IdPreProcessamentoCliente
		/// </summary>
		virtual public System.Int32? IdPreProcessamentoCliente
		{
			get
			{
				return base.GetSystemInt32(PreProcessamentoClienteMetadata.ColumnNames.IdPreProcessamentoCliente);
			}
			
			set
			{
				base.SetSystemInt32(PreProcessamentoClienteMetadata.ColumnNames.IdPreProcessamentoCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to PreProcessamentoCliente.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PreProcessamentoClienteMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(PreProcessamentoClienteMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to PreProcessamentoCliente.Chave
		/// </summary>
		virtual public System.Int32? Chave
		{
			get
			{
				return base.GetSystemInt32(PreProcessamentoClienteMetadata.ColumnNames.Chave);
			}
			
			set
			{
				base.SetSystemInt32(PreProcessamentoClienteMetadata.ColumnNames.Chave, value);
			}
		}
		
		/// <summary>
		/// Maps to PreProcessamentoCliente.DataDia
		/// </summary>
		virtual public System.DateTime? DataDia
		{
			get
			{
				return base.GetSystemDateTime(PreProcessamentoClienteMetadata.ColumnNames.DataDia);
			}
			
			set
			{
				base.SetSystemDateTime(PreProcessamentoClienteMetadata.ColumnNames.DataDia, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPreProcessamentoCliente entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPreProcessamentoCliente
			{
				get
				{
					System.Int32? data = entity.IdPreProcessamentoCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPreProcessamentoCliente = null;
					else entity.IdPreProcessamentoCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String Chave
			{
				get
				{
					System.Int32? data = entity.Chave;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Chave = null;
					else entity.Chave = Convert.ToInt32(value);
				}
			}
				
			public System.String DataDia
			{
				get
				{
					System.DateTime? data = entity.DataDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataDia = null;
					else entity.DataDia = Convert.ToDateTime(value);
				}
			}
			

			private esPreProcessamentoCliente entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPreProcessamentoClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPreProcessamentoCliente can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PreProcessamentoCliente : esPreProcessamentoCliente
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPreProcessamentoClienteQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PreProcessamentoClienteMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPreProcessamentoCliente
		{
			get
			{
				return new esQueryItem(this, PreProcessamentoClienteMetadata.ColumnNames.IdPreProcessamentoCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PreProcessamentoClienteMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Chave
		{
			get
			{
				return new esQueryItem(this, PreProcessamentoClienteMetadata.ColumnNames.Chave, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataDia
		{
			get
			{
				return new esQueryItem(this, PreProcessamentoClienteMetadata.ColumnNames.DataDia, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PreProcessamentoClienteCollection")]
	public partial class PreProcessamentoClienteCollection : esPreProcessamentoClienteCollection, IEnumerable<PreProcessamentoCliente>
	{
		public PreProcessamentoClienteCollection()
		{

		}
		
		public static implicit operator List<PreProcessamentoCliente>(PreProcessamentoClienteCollection coll)
		{
			List<PreProcessamentoCliente> list = new List<PreProcessamentoCliente>();
			
			foreach (PreProcessamentoCliente emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PreProcessamentoClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PreProcessamentoClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PreProcessamentoCliente(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PreProcessamentoCliente();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PreProcessamentoClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PreProcessamentoClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PreProcessamentoClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PreProcessamentoCliente AddNew()
		{
			PreProcessamentoCliente entity = base.AddNewEntity() as PreProcessamentoCliente;
			
			return entity;
		}

		public PreProcessamentoCliente FindByPrimaryKey(System.Int32 idPreProcessamentoCliente)
		{
			return base.FindByPrimaryKey(idPreProcessamentoCliente) as PreProcessamentoCliente;
		}


		#region IEnumerable<PreProcessamentoCliente> Members

		IEnumerator<PreProcessamentoCliente> IEnumerable<PreProcessamentoCliente>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PreProcessamentoCliente;
			}
		}

		#endregion
		
		private PreProcessamentoClienteQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PreProcessamentoCliente' table
	/// </summary>

	[Serializable]
	public partial class PreProcessamentoCliente : esPreProcessamentoCliente
	{
		public PreProcessamentoCliente()
		{

		}
	
		public PreProcessamentoCliente(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PreProcessamentoClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esPreProcessamentoClienteQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PreProcessamentoClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PreProcessamentoClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PreProcessamentoClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PreProcessamentoClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PreProcessamentoClienteQuery query;
	}



	[Serializable]
	public partial class PreProcessamentoClienteQuery : esPreProcessamentoClienteQuery
	{
		public PreProcessamentoClienteQuery()
		{

		}		
		
		public PreProcessamentoClienteQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PreProcessamentoClienteMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PreProcessamentoClienteMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PreProcessamentoClienteMetadata.ColumnNames.IdPreProcessamentoCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PreProcessamentoClienteMetadata.PropertyNames.IdPreProcessamentoCliente;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PreProcessamentoClienteMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PreProcessamentoClienteMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PreProcessamentoClienteMetadata.ColumnNames.Chave, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PreProcessamentoClienteMetadata.PropertyNames.Chave;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PreProcessamentoClienteMetadata.ColumnNames.DataDia, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PreProcessamentoClienteMetadata.PropertyNames.DataDia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PreProcessamentoClienteMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPreProcessamentoCliente = "IdPreProcessamentoCliente";
			 public const string IdCliente = "IdCliente";
			 public const string Chave = "Chave";
			 public const string DataDia = "DataDia";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPreProcessamentoCliente = "IdPreProcessamentoCliente";
			 public const string IdCliente = "IdCliente";
			 public const string Chave = "Chave";
			 public const string DataDia = "DataDia";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PreProcessamentoClienteMetadata))
			{
				if(PreProcessamentoClienteMetadata.mapDelegates == null)
				{
					PreProcessamentoClienteMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PreProcessamentoClienteMetadata.meta == null)
				{
					PreProcessamentoClienteMetadata.meta = new PreProcessamentoClienteMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPreProcessamentoCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Chave", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataDia", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "PreProcessamentoCliente";
				meta.Destination = "PreProcessamentoCliente";
				
				meta.spInsert = "proc_PreProcessamentoClienteInsert";				
				meta.spUpdate = "proc_PreProcessamentoClienteUpdate";		
				meta.spDelete = "proc_PreProcessamentoClienteDelete";
				meta.spLoadAll = "proc_PreProcessamentoClienteLoadAll";
				meta.spLoadByPrimaryKey = "proc_PreProcessamentoClienteLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PreProcessamentoClienteMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
