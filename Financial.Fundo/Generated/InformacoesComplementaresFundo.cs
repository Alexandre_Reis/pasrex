/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 06/10/2015 14:44:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	












		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esInformacoesComplementaresFundoCollection : esEntityCollection
	{
		public esInformacoesComplementaresFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "InformacoesComplementaresFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esInformacoesComplementaresFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esInformacoesComplementaresFundoQuery);
		}
		#endregion
		
		virtual public InformacoesComplementaresFundo DetachEntity(InformacoesComplementaresFundo entity)
		{
			return base.DetachEntity(entity) as InformacoesComplementaresFundo;
		}
		
		virtual public InformacoesComplementaresFundo AttachEntity(InformacoesComplementaresFundo entity)
		{
			return base.AttachEntity(entity) as InformacoesComplementaresFundo;
		}
		
		virtual public void Combine(InformacoesComplementaresFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public InformacoesComplementaresFundo this[int index]
		{
			get
			{
				return base[index] as InformacoesComplementaresFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(InformacoesComplementaresFundo);
		}
	}



	[Serializable]
	abstract public class esInformacoesComplementaresFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esInformacoesComplementaresFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esInformacoesComplementaresFundo()
		{

		}

		public esInformacoesComplementaresFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCarteira, System.DateTime dataInicioVigencia)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira, dataInicioVigencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira, dataInicioVigencia);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCarteira, System.DateTime dataInicioVigencia)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esInformacoesComplementaresFundoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCarteira == idCarteira, query.DataInicioVigencia == dataInicioVigencia);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCarteira, System.DateTime dataInicioVigencia)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira, dataInicioVigencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira, dataInicioVigencia);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCarteira, System.DateTime dataInicioVigencia)
		{
			esInformacoesComplementaresFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdCarteira == idCarteira, query.DataInicioVigencia == dataInicioVigencia);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCarteira, System.DateTime dataInicioVigencia)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCarteira",idCarteira);			parms.Add("DataInicioVigencia",dataInicioVigencia);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataInicioVigencia": this.str.DataInicioVigencia = (string)value; break;							
						case "Periodicidade": this.str.Periodicidade = (string)value; break;							
						case "LocalFormaDivulgacao": this.str.LocalFormaDivulgacao = (string)value; break;							
						case "LocalFormaSolicitacaoCotista": this.str.LocalFormaSolicitacaoCotista = (string)value; break;							
						case "FatoresRisco": this.str.FatoresRisco = (string)value; break;							
						case "PoliticaExercicioVoto": this.str.PoliticaExercicioVoto = (string)value; break;							
						case "TributacaoAplicavel": this.str.TributacaoAplicavel = (string)value; break;							
						case "PoliticaAdministracaoRisco": this.str.PoliticaAdministracaoRisco = (string)value; break;							
						case "AgenciaClassificacaoRisco": this.str.AgenciaClassificacaoRisco = (string)value; break;							
						case "RecursosServicosGestor": this.str.RecursosServicosGestor = (string)value; break;							
						case "PrestadoresServicos": this.str.PrestadoresServicos = (string)value; break;							
						case "PoliticaDistribuicaoCotas": this.str.PoliticaDistribuicaoCotas = (string)value; break;							
						case "Observacoes": this.str.Observacoes = (string)value; break;							
						case "DsLocalDivulg": this.str.DsLocalDivulg = (string)value; break;							
						case "DsResp": this.str.DsResp = (string)value; break;							
						case "CodVotoGestAssemb": this.str.CodVotoGestAssemb = (string)value; break;							
						case "ApresDetalheAdm": this.str.ApresDetalheAdm = (string)value; break;							
						case "DsServicoPrestado": this.str.DsServicoPrestado = (string)value; break;							
						case "CodDistrOfertaPub": this.str.CodDistrOfertaPub = (string)value; break;							
						case "InformAutoregulAnbima": this.str.InformAutoregulAnbima = (string)value; break;							
						case "AgencClassifRatin": this.str.AgencClassifRatin = (string)value; break;							
						case "CodMeioDivulg": this.str.CodMeioDivulg = (string)value; break;							
						case "CodMeio": this.str.CodMeio = (string)value; break;							
						case "DsLocal": this.str.DsLocal = (string)value; break;							
						case "NrCnpj": this.str.NrCnpj = (string)value; break;							
						case "NmPrest": this.str.NmPrest = (string)value; break;							
						case "DisclAdvert": this.str.DisclAdvert = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataInicioVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicioVigencia = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(InformacoesComplementaresFundoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(InformacoesComplementaresFundoMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.DataInicioVigencia
		/// </summary>
		virtual public System.DateTime? DataInicioVigencia
		{
			get
			{
				return base.GetSystemDateTime(InformacoesComplementaresFundoMetadata.ColumnNames.DataInicioVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(InformacoesComplementaresFundoMetadata.ColumnNames.DataInicioVigencia, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.Periodicidade
		/// </summary>
		virtual public System.String Periodicidade
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.Periodicidade);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.Periodicidade, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.LocalFormaDivulgacao
		/// </summary>
		virtual public System.String LocalFormaDivulgacao
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.LocalFormaDivulgacao);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.LocalFormaDivulgacao, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.LocalFormaSolicitacaoCotista
		/// </summary>
		virtual public System.String LocalFormaSolicitacaoCotista
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.LocalFormaSolicitacaoCotista);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.LocalFormaSolicitacaoCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.FatoresRisco
		/// </summary>
		virtual public System.String FatoresRisco
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.FatoresRisco);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.FatoresRisco, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.PoliticaExercicioVoto
		/// </summary>
		virtual public System.String PoliticaExercicioVoto
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.PoliticaExercicioVoto);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.PoliticaExercicioVoto, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.TributacaoAplicavel
		/// </summary>
		virtual public System.String TributacaoAplicavel
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.TributacaoAplicavel);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.TributacaoAplicavel, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.PoliticaAdministracaoRisco
		/// </summary>
		virtual public System.String PoliticaAdministracaoRisco
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.PoliticaAdministracaoRisco);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.PoliticaAdministracaoRisco, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.AgenciaClassificacaoRisco
		/// </summary>
		virtual public System.String AgenciaClassificacaoRisco
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.AgenciaClassificacaoRisco);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.AgenciaClassificacaoRisco, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.RecursosServicosGestor
		/// </summary>
		virtual public System.String RecursosServicosGestor
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.RecursosServicosGestor);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.RecursosServicosGestor, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.PrestadoresServicos
		/// </summary>
		virtual public System.String PrestadoresServicos
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.PrestadoresServicos);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.PrestadoresServicos, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.PoliticaDistribuicaoCotas
		/// </summary>
		virtual public System.String PoliticaDistribuicaoCotas
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.PoliticaDistribuicaoCotas);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.PoliticaDistribuicaoCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.Observacoes
		/// </summary>
		virtual public System.String Observacoes
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.Observacoes);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.Observacoes, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.DsLocalDivulg
		/// </summary>
		virtual public System.String DsLocalDivulg
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.DsLocalDivulg);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.DsLocalDivulg, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.DsResp
		/// </summary>
		virtual public System.String DsResp
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.DsResp);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.DsResp, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.CodVotoGestAssemb
		/// </summary>
		virtual public System.String CodVotoGestAssemb
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.CodVotoGestAssemb);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.CodVotoGestAssemb, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.ApresDetalheAdm
		/// </summary>
		virtual public System.String ApresDetalheAdm
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.ApresDetalheAdm);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.ApresDetalheAdm, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.DsServicoPrestado
		/// </summary>
		virtual public System.String DsServicoPrestado
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.DsServicoPrestado);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.DsServicoPrestado, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.CodDistrOfertaPub
		/// </summary>
		virtual public System.String CodDistrOfertaPub
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.CodDistrOfertaPub);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.CodDistrOfertaPub, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.InformAutoregulAnbima
		/// </summary>
		virtual public System.String InformAutoregulAnbima
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.InformAutoregulAnbima);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.InformAutoregulAnbima, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.AgencClassifRatin
		/// </summary>
		virtual public System.String AgencClassifRatin
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.AgencClassifRatin);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.AgencClassifRatin, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.CodMeioDivulg
		/// </summary>
		virtual public System.String CodMeioDivulg
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.CodMeioDivulg);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.CodMeioDivulg, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.CodMeio
		/// </summary>
		virtual public System.String CodMeio
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.CodMeio);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.CodMeio, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.DsLocal
		/// </summary>
		virtual public System.String DsLocal
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.DsLocal);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.DsLocal, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.NrCnpj
		/// </summary>
		virtual public System.String NrCnpj
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.NrCnpj);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.NrCnpj, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.NmPrest
		/// </summary>
		virtual public System.String NmPrest
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.NmPrest);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.NmPrest, value);
			}
		}
		
		/// <summary>
		/// Maps to InformacoesComplementaresFundo.DisclAdvert
		/// </summary>
		virtual public System.String DisclAdvert
		{
			get
			{
				return base.GetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.DisclAdvert);
			}
			
			set
			{
				base.SetSystemString(InformacoesComplementaresFundoMetadata.ColumnNames.DisclAdvert, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esInformacoesComplementaresFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataInicioVigencia
			{
				get
				{
					System.DateTime? data = entity.DataInicioVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicioVigencia = null;
					else entity.DataInicioVigencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String Periodicidade
			{
				get
				{
					System.String data = entity.Periodicidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Periodicidade = null;
					else entity.Periodicidade = Convert.ToString(value);
				}
			}
				
			public System.String LocalFormaDivulgacao
			{
				get
				{
					System.String data = entity.LocalFormaDivulgacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LocalFormaDivulgacao = null;
					else entity.LocalFormaDivulgacao = Convert.ToString(value);
				}
			}
				
			public System.String LocalFormaSolicitacaoCotista
			{
				get
				{
					System.String data = entity.LocalFormaSolicitacaoCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LocalFormaSolicitacaoCotista = null;
					else entity.LocalFormaSolicitacaoCotista = Convert.ToString(value);
				}
			}
				
			public System.String FatoresRisco
			{
				get
				{
					System.String data = entity.FatoresRisco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FatoresRisco = null;
					else entity.FatoresRisco = Convert.ToString(value);
				}
			}
				
			public System.String PoliticaExercicioVoto
			{
				get
				{
					System.String data = entity.PoliticaExercicioVoto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PoliticaExercicioVoto = null;
					else entity.PoliticaExercicioVoto = Convert.ToString(value);
				}
			}
				
			public System.String TributacaoAplicavel
			{
				get
				{
					System.String data = entity.TributacaoAplicavel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TributacaoAplicavel = null;
					else entity.TributacaoAplicavel = Convert.ToString(value);
				}
			}
				
			public System.String PoliticaAdministracaoRisco
			{
				get
				{
					System.String data = entity.PoliticaAdministracaoRisco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PoliticaAdministracaoRisco = null;
					else entity.PoliticaAdministracaoRisco = Convert.ToString(value);
				}
			}
				
			public System.String AgenciaClassificacaoRisco
			{
				get
				{
					System.String data = entity.AgenciaClassificacaoRisco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AgenciaClassificacaoRisco = null;
					else entity.AgenciaClassificacaoRisco = Convert.ToString(value);
				}
			}
				
			public System.String RecursosServicosGestor
			{
				get
				{
					System.String data = entity.RecursosServicosGestor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RecursosServicosGestor = null;
					else entity.RecursosServicosGestor = Convert.ToString(value);
				}
			}
				
			public System.String PrestadoresServicos
			{
				get
				{
					System.String data = entity.PrestadoresServicos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrestadoresServicos = null;
					else entity.PrestadoresServicos = Convert.ToString(value);
				}
			}
				
			public System.String PoliticaDistribuicaoCotas
			{
				get
				{
					System.String data = entity.PoliticaDistribuicaoCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PoliticaDistribuicaoCotas = null;
					else entity.PoliticaDistribuicaoCotas = Convert.ToString(value);
				}
			}
				
			public System.String Observacoes
			{
				get
				{
					System.String data = entity.Observacoes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Observacoes = null;
					else entity.Observacoes = Convert.ToString(value);
				}
			}
				
			public System.String DsLocalDivulg
			{
				get
				{
					System.String data = entity.DsLocalDivulg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DsLocalDivulg = null;
					else entity.DsLocalDivulg = Convert.ToString(value);
				}
			}
				
			public System.String DsResp
			{
				get
				{
					System.String data = entity.DsResp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DsResp = null;
					else entity.DsResp = Convert.ToString(value);
				}
			}
				
			public System.String CodVotoGestAssemb
			{
				get
				{
					System.String data = entity.CodVotoGestAssemb;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodVotoGestAssemb = null;
					else entity.CodVotoGestAssemb = Convert.ToString(value);
				}
			}
				
			public System.String ApresDetalheAdm
			{
				get
				{
					System.String data = entity.ApresDetalheAdm;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ApresDetalheAdm = null;
					else entity.ApresDetalheAdm = Convert.ToString(value);
				}
			}
				
			public System.String DsServicoPrestado
			{
				get
				{
					System.String data = entity.DsServicoPrestado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DsServicoPrestado = null;
					else entity.DsServicoPrestado = Convert.ToString(value);
				}
			}
				
			public System.String CodDistrOfertaPub
			{
				get
				{
					System.String data = entity.CodDistrOfertaPub;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodDistrOfertaPub = null;
					else entity.CodDistrOfertaPub = Convert.ToString(value);
				}
			}
				
			public System.String InformAutoregulAnbima
			{
				get
				{
					System.String data = entity.InformAutoregulAnbima;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InformAutoregulAnbima = null;
					else entity.InformAutoregulAnbima = Convert.ToString(value);
				}
			}
				
			public System.String AgencClassifRatin
			{
				get
				{
					System.String data = entity.AgencClassifRatin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AgencClassifRatin = null;
					else entity.AgencClassifRatin = Convert.ToString(value);
				}
			}
				
			public System.String CodMeioDivulg
			{
				get
				{
					System.String data = entity.CodMeioDivulg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodMeioDivulg = null;
					else entity.CodMeioDivulg = Convert.ToString(value);
				}
			}
				
			public System.String CodMeio
			{
				get
				{
					System.String data = entity.CodMeio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodMeio = null;
					else entity.CodMeio = Convert.ToString(value);
				}
			}
				
			public System.String DsLocal
			{
				get
				{
					System.String data = entity.DsLocal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DsLocal = null;
					else entity.DsLocal = Convert.ToString(value);
				}
			}
				
			public System.String NrCnpj
			{
				get
				{
					System.String data = entity.NrCnpj;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NrCnpj = null;
					else entity.NrCnpj = Convert.ToString(value);
				}
			}
				
			public System.String NmPrest
			{
				get
				{
					System.String data = entity.NmPrest;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NmPrest = null;
					else entity.NmPrest = Convert.ToString(value);
				}
			}
				
			public System.String DisclAdvert
			{
				get
				{
					System.String data = entity.DisclAdvert;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DisclAdvert = null;
					else entity.DisclAdvert = Convert.ToString(value);
				}
			}
			

			private esInformacoesComplementaresFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esInformacoesComplementaresFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esInformacoesComplementaresFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class InformacoesComplementaresFundo : esInformacoesComplementaresFundo
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_InformacoesComplementaresFundo_Carteira
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esInformacoesComplementaresFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return InformacoesComplementaresFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataInicioVigencia
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.DataInicioVigencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Periodicidade
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.Periodicidade, esSystemType.String);
			}
		} 
		
		public esQueryItem LocalFormaDivulgacao
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.LocalFormaDivulgacao, esSystemType.String);
			}
		} 
		
		public esQueryItem LocalFormaSolicitacaoCotista
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.LocalFormaSolicitacaoCotista, esSystemType.String);
			}
		} 
		
		public esQueryItem FatoresRisco
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.FatoresRisco, esSystemType.String);
			}
		} 
		
		public esQueryItem PoliticaExercicioVoto
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.PoliticaExercicioVoto, esSystemType.String);
			}
		} 
		
		public esQueryItem TributacaoAplicavel
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.TributacaoAplicavel, esSystemType.String);
			}
		} 
		
		public esQueryItem PoliticaAdministracaoRisco
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.PoliticaAdministracaoRisco, esSystemType.String);
			}
		} 
		
		public esQueryItem AgenciaClassificacaoRisco
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.AgenciaClassificacaoRisco, esSystemType.String);
			}
		} 
		
		public esQueryItem RecursosServicosGestor
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.RecursosServicosGestor, esSystemType.String);
			}
		} 
		
		public esQueryItem PrestadoresServicos
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.PrestadoresServicos, esSystemType.String);
			}
		} 
		
		public esQueryItem PoliticaDistribuicaoCotas
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.PoliticaDistribuicaoCotas, esSystemType.String);
			}
		} 
		
		public esQueryItem Observacoes
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.Observacoes, esSystemType.String);
			}
		} 
		
		public esQueryItem DsLocalDivulg
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.DsLocalDivulg, esSystemType.String);
			}
		} 
		
		public esQueryItem DsResp
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.DsResp, esSystemType.String);
			}
		} 
		
		public esQueryItem CodVotoGestAssemb
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.CodVotoGestAssemb, esSystemType.String);
			}
		} 
		
		public esQueryItem ApresDetalheAdm
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.ApresDetalheAdm, esSystemType.String);
			}
		} 
		
		public esQueryItem DsServicoPrestado
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.DsServicoPrestado, esSystemType.String);
			}
		} 
		
		public esQueryItem CodDistrOfertaPub
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.CodDistrOfertaPub, esSystemType.String);
			}
		} 
		
		public esQueryItem InformAutoregulAnbima
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.InformAutoregulAnbima, esSystemType.String);
			}
		} 
		
		public esQueryItem AgencClassifRatin
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.AgencClassifRatin, esSystemType.String);
			}
		} 
		
		public esQueryItem CodMeioDivulg
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.CodMeioDivulg, esSystemType.String);
			}
		} 
		
		public esQueryItem CodMeio
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.CodMeio, esSystemType.String);
			}
		} 
		
		public esQueryItem DsLocal
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.DsLocal, esSystemType.String);
			}
		} 
		
		public esQueryItem NrCnpj
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.NrCnpj, esSystemType.String);
			}
		} 
		
		public esQueryItem NmPrest
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.NmPrest, esSystemType.String);
			}
		} 
		
		public esQueryItem DisclAdvert
		{
			get
			{
				return new esQueryItem(this, InformacoesComplementaresFundoMetadata.ColumnNames.DisclAdvert, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("InformacoesComplementaresFundoCollection")]
	public partial class InformacoesComplementaresFundoCollection : esInformacoesComplementaresFundoCollection, IEnumerable<InformacoesComplementaresFundo>
	{
		public InformacoesComplementaresFundoCollection()
		{

		}
		
		public static implicit operator List<InformacoesComplementaresFundo>(InformacoesComplementaresFundoCollection coll)
		{
			List<InformacoesComplementaresFundo> list = new List<InformacoesComplementaresFundo>();
			
			foreach (InformacoesComplementaresFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  InformacoesComplementaresFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new InformacoesComplementaresFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new InformacoesComplementaresFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new InformacoesComplementaresFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public InformacoesComplementaresFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new InformacoesComplementaresFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(InformacoesComplementaresFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public InformacoesComplementaresFundo AddNew()
		{
			InformacoesComplementaresFundo entity = base.AddNewEntity() as InformacoesComplementaresFundo;
			
			return entity;
		}

		public InformacoesComplementaresFundo FindByPrimaryKey(System.Int32 idCarteira, System.DateTime dataInicioVigencia)
		{
			return base.FindByPrimaryKey(idCarteira, dataInicioVigencia) as InformacoesComplementaresFundo;
		}


		#region IEnumerable<InformacoesComplementaresFundo> Members

		IEnumerator<InformacoesComplementaresFundo> IEnumerable<InformacoesComplementaresFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as InformacoesComplementaresFundo;
			}
		}

		#endregion
		
		private InformacoesComplementaresFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'InformacoesComplementaresFundo' table
	/// </summary>

	[Serializable]
	public partial class InformacoesComplementaresFundo : esInformacoesComplementaresFundo
	{
		public InformacoesComplementaresFundo()
		{

		}
	
		public InformacoesComplementaresFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return InformacoesComplementaresFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esInformacoesComplementaresFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new InformacoesComplementaresFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public InformacoesComplementaresFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new InformacoesComplementaresFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(InformacoesComplementaresFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private InformacoesComplementaresFundoQuery query;
	}



	[Serializable]
	public partial class InformacoesComplementaresFundoQuery : esInformacoesComplementaresFundoQuery
	{
		public InformacoesComplementaresFundoQuery()
		{

		}		
		
		public InformacoesComplementaresFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class InformacoesComplementaresFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected InformacoesComplementaresFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.IdCarteira, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.DataInicioVigencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.DataInicioVigencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.Periodicidade, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.Periodicidade;
			c.CharacterMaxLength = 250;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.LocalFormaDivulgacao, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.LocalFormaDivulgacao;
			c.CharacterMaxLength = 300;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.LocalFormaSolicitacaoCotista, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.LocalFormaSolicitacaoCotista;
			c.CharacterMaxLength = 250;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.FatoresRisco, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.FatoresRisco;
			c.CharacterMaxLength = 2000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.PoliticaExercicioVoto, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.PoliticaExercicioVoto;
			c.CharacterMaxLength = 1000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.TributacaoAplicavel, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.TributacaoAplicavel;
			c.CharacterMaxLength = 2500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.PoliticaAdministracaoRisco, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.PoliticaAdministracaoRisco;
			c.CharacterMaxLength = 2500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.AgenciaClassificacaoRisco, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.AgenciaClassificacaoRisco;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.RecursosServicosGestor, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.RecursosServicosGestor;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.PrestadoresServicos, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.PrestadoresServicos;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.PoliticaDistribuicaoCotas, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.PoliticaDistribuicaoCotas;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.Observacoes, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.Observacoes;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.DsLocalDivulg, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.DsLocalDivulg;
			c.CharacterMaxLength = 300;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.DsResp, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.DsResp;
			c.CharacterMaxLength = 250;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.CodVotoGestAssemb, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.CodVotoGestAssemb;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.ApresDetalheAdm, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.ApresDetalheAdm;
			c.CharacterMaxLength = 2500;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.DsServicoPrestado, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.DsServicoPrestado;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.CodDistrOfertaPub, 19, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.CodDistrOfertaPub;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.InformAutoregulAnbima, 20, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.InformAutoregulAnbima;
			c.CharacterMaxLength = 2500;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.AgencClassifRatin, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.AgencClassifRatin;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.CodMeioDivulg, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.CodMeioDivulg;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.CodMeio, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.CodMeio;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.DsLocal, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.DsLocal;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.NrCnpj, 25, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.NrCnpj;
			c.CharacterMaxLength = 14;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.NmPrest, 26, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.NmPrest;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(InformacoesComplementaresFundoMetadata.ColumnNames.DisclAdvert, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = InformacoesComplementaresFundoMetadata.PropertyNames.DisclAdvert;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public InformacoesComplementaresFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string DataInicioVigencia = "DataInicioVigencia";
			 public const string Periodicidade = "Periodicidade";
			 public const string LocalFormaDivulgacao = "LocalFormaDivulgacao";
			 public const string LocalFormaSolicitacaoCotista = "LocalFormaSolicitacaoCotista";
			 public const string FatoresRisco = "FatoresRisco";
			 public const string PoliticaExercicioVoto = "PoliticaExercicioVoto";
			 public const string TributacaoAplicavel = "TributacaoAplicavel";
			 public const string PoliticaAdministracaoRisco = "PoliticaAdministracaoRisco";
			 public const string AgenciaClassificacaoRisco = "AgenciaClassificacaoRisco";
			 public const string RecursosServicosGestor = "RecursosServicosGestor";
			 public const string PrestadoresServicos = "PrestadoresServicos";
			 public const string PoliticaDistribuicaoCotas = "PoliticaDistribuicaoCotas";
			 public const string Observacoes = "Observacoes";
			 public const string DsLocalDivulg = "DsLocalDivulg";
			 public const string DsResp = "DsResp";
			 public const string CodVotoGestAssemb = "CodVotoGestAssemb";
			 public const string ApresDetalheAdm = "ApresDetalheAdm";
			 public const string DsServicoPrestado = "DsServicoPrestado";
			 public const string CodDistrOfertaPub = "CodDistrOfertaPub";
			 public const string InformAutoregulAnbima = "InformAutoregulAnbima";
			 public const string AgencClassifRatin = "AgencClassifRatin";
			 public const string CodMeioDivulg = "CodMeioDivulg";
			 public const string CodMeio = "CodMeio";
			 public const string DsLocal = "DsLocal";
			 public const string NrCnpj = "NrCnpj";
			 public const string NmPrest = "NmPrest";
			 public const string DisclAdvert = "DisclAdvert";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string DataInicioVigencia = "DataInicioVigencia";
			 public const string Periodicidade = "Periodicidade";
			 public const string LocalFormaDivulgacao = "LocalFormaDivulgacao";
			 public const string LocalFormaSolicitacaoCotista = "LocalFormaSolicitacaoCotista";
			 public const string FatoresRisco = "FatoresRisco";
			 public const string PoliticaExercicioVoto = "PoliticaExercicioVoto";
			 public const string TributacaoAplicavel = "TributacaoAplicavel";
			 public const string PoliticaAdministracaoRisco = "PoliticaAdministracaoRisco";
			 public const string AgenciaClassificacaoRisco = "AgenciaClassificacaoRisco";
			 public const string RecursosServicosGestor = "RecursosServicosGestor";
			 public const string PrestadoresServicos = "PrestadoresServicos";
			 public const string PoliticaDistribuicaoCotas = "PoliticaDistribuicaoCotas";
			 public const string Observacoes = "Observacoes";
			 public const string DsLocalDivulg = "DsLocalDivulg";
			 public const string DsResp = "DsResp";
			 public const string CodVotoGestAssemb = "CodVotoGestAssemb";
			 public const string ApresDetalheAdm = "ApresDetalheAdm";
			 public const string DsServicoPrestado = "DsServicoPrestado";
			 public const string CodDistrOfertaPub = "CodDistrOfertaPub";
			 public const string InformAutoregulAnbima = "InformAutoregulAnbima";
			 public const string AgencClassifRatin = "AgencClassifRatin";
			 public const string CodMeioDivulg = "CodMeioDivulg";
			 public const string CodMeio = "CodMeio";
			 public const string DsLocal = "DsLocal";
			 public const string NrCnpj = "NrCnpj";
			 public const string NmPrest = "NmPrest";
			 public const string DisclAdvert = "DisclAdvert";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(InformacoesComplementaresFundoMetadata))
			{
				if(InformacoesComplementaresFundoMetadata.mapDelegates == null)
				{
					InformacoesComplementaresFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (InformacoesComplementaresFundoMetadata.meta == null)
				{
					InformacoesComplementaresFundoMetadata.meta = new InformacoesComplementaresFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataInicioVigencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Periodicidade", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("LocalFormaDivulgacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("LocalFormaSolicitacaoCotista", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FatoresRisco", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PoliticaExercicioVoto", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TributacaoAplicavel", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PoliticaAdministracaoRisco", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("AgenciaClassificacaoRisco", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("RecursosServicosGestor", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PrestadoresServicos", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PoliticaDistribuicaoCotas", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Observacoes", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DsLocalDivulg", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DsResp", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodVotoGestAssemb", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ApresDetalheAdm", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DsServicoPrestado", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodDistrOfertaPub", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("InformAutoregulAnbima", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("AgencClassifRatin", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodMeioDivulg", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodMeio", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DsLocal", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("NrCnpj", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("NmPrest", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DisclAdvert", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "InformacoesComplementaresFundo";
				meta.Destination = "InformacoesComplementaresFundo";
				
				meta.spInsert = "proc_InformacoesComplementaresFundoInsert";				
				meta.spUpdate = "proc_InformacoesComplementaresFundoUpdate";		
				meta.spDelete = "proc_InformacoesComplementaresFundoDelete";
				meta.spLoadAll = "proc_InformacoesComplementaresFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_InformacoesComplementaresFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private InformacoesComplementaresFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
