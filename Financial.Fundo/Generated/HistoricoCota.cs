/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/08/2014 16:40:35
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

    [Serializable]
    abstract public class esHistoricoCotaCollection : esEntityCollection
    {
        public esHistoricoCotaCollection()
        {

        }

        protected override string GetCollectionName()
        {
            return "HistoricoCotaCollection";
        }

        #region Query Logic
        protected void InitQuery(esHistoricoCotaQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntityCollection)this).Connection;
        }

        protected bool OnQueryLoaded(DataTable table)
        {
            this.PopulateCollection(table);
            return (this.RowCount > 0) ? true : false;
        }

        protected override void HookupQuery(esDynamicQuery query)
        {
            this.InitQuery(query as esHistoricoCotaQuery);
        }
        #endregion

        virtual public HistoricoCota DetachEntity(HistoricoCota entity)
        {
            return base.DetachEntity(entity) as HistoricoCota;
        }

        virtual public HistoricoCota AttachEntity(HistoricoCota entity)
        {
            return base.AttachEntity(entity) as HistoricoCota;
        }

        virtual public void Combine(HistoricoCotaCollection collection)
        {
            base.Combine(collection);
        }

        new public HistoricoCota this[int index]
        {
            get
            {
                return base[index] as HistoricoCota;
            }
        }

        public override Type GetEntityType()
        {
            return typeof(HistoricoCota);
        }
    }



    [Serializable]
    abstract public class esHistoricoCota : esEntity
    {
        /// <summary>
        /// Used internally by the entity's DynamicQuery mechanism.
        /// </summary>
        virtual protected esHistoricoCotaQuery GetDynamicQuery()
        {
            return null;
        }

        public esHistoricoCota()
        {

        }

        public esHistoricoCota(DataRow row)
            : base(row)
        {

        }

        #region LoadByPrimaryKey
        public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int32 idCarteira)
        {
            if (this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(data, idCarteira);
            else
                return LoadByPrimaryKeyStoredProcedure(data, idCarteira);
        }

        /// <summary>
        /// Loads an entity by primary key
        /// </summary>
        /// <remarks>
        /// EntitySpaces requires primary keys be defined on all tables.
        /// If a table does not have a primary key set,
        /// this method will not compile.
        /// Does not support sqlAcessType. It only works with DynamicQuery
        /// </remarks>
        /// <param name="fieldsToReturn">Fields desired</param>
        public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime data, System.Int32 idCarteira)
        {
            esQueryItem[] fields = fieldsToReturn.ToArray();
            esHistoricoCotaQuery query = this.GetDynamicQuery();
            query
                .Select(fields)
                .Where(query.Data == data, query.IdCarteira == idCarteira);

            return query.Load();
        }

        public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int32 idCarteira)
        {
            if (sqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(data, idCarteira);
            else
                return LoadByPrimaryKeyStoredProcedure(data, idCarteira);
        }

        private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int32 idCarteira)
        {
            esHistoricoCotaQuery query = this.GetDynamicQuery();
            query.Where(query.Data == data, query.IdCarteira == idCarteira);
            return query.Load();
        }

        private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int32 idCarteira)
        {
            esParameters parms = new esParameters();
            parms.Add("Data", data); parms.Add("IdCarteira", idCarteira);
            return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
        }
        #endregion



        #region Properties


        public override void SetProperties(IDictionary values)
        {
            foreach (string propertyName in values.Keys)
            {
                this.SetProperty(propertyName, values[propertyName]);
            }
        }

        public override void SetProperty(string name, object value)
        {
            if (this.Row == null) this.AddNew();

            esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
            if (col != null)
            {
                if (value == null || value.GetType().ToString() == "System.String")
                {
                    // Use the strongly typed property
                    switch (name)
                    {
                        case "Data": this.str.Data = (string)value; break;
                        case "IdCarteira": this.str.IdCarteira = (string)value; break;
                        case "CotaAbertura": this.str.CotaAbertura = (string)value; break;
                        case "CotaFechamento": this.str.CotaFechamento = (string)value; break;
                        case "CotaBruta": this.str.CotaBruta = (string)value; break;
                        case "PLAbertura": this.str.PLAbertura = (string)value; break;
                        case "PLFechamento": this.str.PLFechamento = (string)value; break;
                        case "PatrimonioBruto": this.str.PatrimonioBruto = (string)value; break;
                        case "QuantidadeFechamento": this.str.QuantidadeFechamento = (string)value; break;
                        case "AjustePL": this.str.AjustePL = (string)value; break;
						case "CotaImportada": this.str.CotaImportada = (string)value; break;
						case "CotaEx": this.str.CotaEx = (string)value; break;
                        case "CotaRendimento": this.str.CotaRendimento = (string)value; break;
                        case "ProventoAcumulado": this.str.ProventoAcumulado = (string)value; break;
                    }
                }
                else
                {
                    switch (name)
                    {
                        case "Data":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.Data = (System.DateTime?)value;
                            break;

                        case "IdCarteira":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdCarteira = (System.Int32?)value;
                            break;

                        case "CotaAbertura":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.CotaAbertura = (System.Decimal?)value;
                            break;

                        case "CotaFechamento":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.CotaFechamento = (System.Decimal?)value;
                            break;

                        case "CotaBruta":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.CotaBruta = (System.Decimal?)value;
                            break;

                        case "PLAbertura":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.PLAbertura = (System.Decimal?)value;
                            break;

                        case "PLFechamento":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.PLFechamento = (System.Decimal?)value;
                            break;

                        case "PatrimonioBruto":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.PatrimonioBruto = (System.Decimal?)value;
                            break;

                        case "QuantidadeFechamento":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.QuantidadeFechamento = (System.Decimal?)value;
                            break;

                        case "AjustePL":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.AjustePL = (System.Decimal?)value;
                            break;

						case "CotaEx":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaEx = (System.Decimal?)value;
							break;

                        case "CotaRendimento":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.CotaRendimento = (System.Decimal?)value;
                            break;

                        case "ProventoAcumulado":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.ProventoAcumulado = (System.Decimal?)value;
                            break;
					

                        default:
                            break;
                    }
                }
            }
            else if (this.Row.Table.Columns.Contains(name))
            {
                this.Row[name] = value;
            }
            else
            {
                throw new Exception("SetProperty Error: '" + name + "' not found");
            }
        }


        /// <summary>
        /// Maps to HistoricoCota.Data
        /// </summary>
        virtual public System.DateTime? Data
        {
            get
            {
                return base.GetSystemDateTime(HistoricoCotaMetadata.ColumnNames.Data);
            }

            set
            {
                base.SetSystemDateTime(HistoricoCotaMetadata.ColumnNames.Data, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCota.IdCarteira
        /// </summary>
        virtual public System.Int32? IdCarteira
        {
            get
            {
                return base.GetSystemInt32(HistoricoCotaMetadata.ColumnNames.IdCarteira);
            }

            set
            {
                if (base.SetSystemInt32(HistoricoCotaMetadata.ColumnNames.IdCarteira, value))
                {
                    this._UpToCarteiraByIdCarteira = null;
                }
            }
        }

        /// <summary>
        /// Maps to HistoricoCota.CotaAbertura
        /// </summary>
        virtual public System.Decimal? CotaAbertura
        {
            get
            {
                return base.GetSystemDecimal(HistoricoCotaMetadata.ColumnNames.CotaAbertura);
            }

            set
            {
                base.SetSystemDecimal(HistoricoCotaMetadata.ColumnNames.CotaAbertura, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCota.CotaFechamento
        /// </summary>
        virtual public System.Decimal? CotaFechamento
        {
            get
            {
                return base.GetSystemDecimal(HistoricoCotaMetadata.ColumnNames.CotaFechamento);
            }

            set
            {
                base.SetSystemDecimal(HistoricoCotaMetadata.ColumnNames.CotaFechamento, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCota.CotaBruta
        /// </summary>
        virtual public System.Decimal? CotaBruta
        {
            get
            {
                return base.GetSystemDecimal(HistoricoCotaMetadata.ColumnNames.CotaBruta);
            }

            set
            {
                base.SetSystemDecimal(HistoricoCotaMetadata.ColumnNames.CotaBruta, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCota.PLAbertura
        /// </summary>
        virtual public System.Decimal? PLAbertura
        {
            get
            {
                return base.GetSystemDecimal(HistoricoCotaMetadata.ColumnNames.PLAbertura);
            }

            set
            {
                base.SetSystemDecimal(HistoricoCotaMetadata.ColumnNames.PLAbertura, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCota.PLFechamento
        /// </summary>
        virtual public System.Decimal? PLFechamento
        {
            get
            {
                return base.GetSystemDecimal(HistoricoCotaMetadata.ColumnNames.PLFechamento);
            }

            set
            {
                base.SetSystemDecimal(HistoricoCotaMetadata.ColumnNames.PLFechamento, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCota.PatrimonioBruto
        /// </summary>
        virtual public System.Decimal? PatrimonioBruto
        {
            get
            {
                return base.GetSystemDecimal(HistoricoCotaMetadata.ColumnNames.PatrimonioBruto);
            }

            set
            {
                base.SetSystemDecimal(HistoricoCotaMetadata.ColumnNames.PatrimonioBruto, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCota.QuantidadeFechamento
        /// </summary>
        virtual public System.Decimal? QuantidadeFechamento
        {
            get
            {
                return base.GetSystemDecimal(HistoricoCotaMetadata.ColumnNames.QuantidadeFechamento);
            }

            set
            {
                base.SetSystemDecimal(HistoricoCotaMetadata.ColumnNames.QuantidadeFechamento, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCota.AjustePL
        /// </summary>
        virtual public System.Decimal? AjustePL
        {
            get
            {
                return base.GetSystemDecimal(HistoricoCotaMetadata.ColumnNames.AjustePL);
            }

            set
            {
                base.SetSystemDecimal(HistoricoCotaMetadata.ColumnNames.AjustePL, value);
            }
        }

		/// <summary>
		/// Maps to HistoricoCota.CotaImportada
		/// </summary>
		virtual public System.String CotaImportada
		{
			get
			{
				return base.GetSystemString(HistoricoCotaMetadata.ColumnNames.CotaImportada);
			}
			
			set
			{
				base.SetSystemString(HistoricoCotaMetadata.ColumnNames.CotaImportada, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoCota.CotaEx
		/// </summary>
		virtual public System.Decimal? CotaEx
		{
			get
			{
				return base.GetSystemDecimal(HistoricoCotaMetadata.ColumnNames.CotaEx);
			}
			
			set
			{
				base.SetSystemDecimal(HistoricoCotaMetadata.ColumnNames.CotaEx, value);
			}
		}

        /// <summary>
        /// Maps to HistoricoCota.CotaRendimento
        /// </summary>
        virtual public System.Decimal? CotaRendimento
        {
            get
            {
                return base.GetSystemDecimal(HistoricoCotaMetadata.ColumnNames.CotaRendimento);
            }

            set
            {
                base.SetSystemDecimal(HistoricoCotaMetadata.ColumnNames.CotaRendimento, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCota.CotaRendimento
        /// </summary>
        virtual public System.Decimal? ProventoAcumulado
        {
            get
            {
                return base.GetSystemDecimal(HistoricoCotaMetadata.ColumnNames.ProventoAcumulado);
            }

            set
            {
                base.SetSystemDecimal(HistoricoCotaMetadata.ColumnNames.ProventoAcumulado, value);
            }
        }
		
        [CLSCompliant(false)]
        internal protected Carteira _UpToCarteiraByIdCarteira;
        #endregion

        #region String Properties


        [BrowsableAttribute(false)]
        public esStrings str
        {
            get
            {
                if (esstrings == null)
                {
                    esstrings = new esStrings(this);
                }
                return esstrings;
            }
        }


        [Serializable]
        sealed public class esStrings
        {
            public esStrings(esHistoricoCota entity)
            {
                this.entity = entity;
            }


            public System.String Data
            {
                get
                {
                    System.DateTime? data = entity.Data;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Data = null;
                    else entity.Data = Convert.ToDateTime(value);
                }
            }

            public System.String IdCarteira
            {
                get
                {
                    System.Int32? data = entity.IdCarteira;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdCarteira = null;
                    else entity.IdCarteira = Convert.ToInt32(value);
                }
            }

            public System.String CotaAbertura
            {
                get
                {
                    System.Decimal? data = entity.CotaAbertura;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.CotaAbertura = null;
                    else entity.CotaAbertura = Convert.ToDecimal(value);
                }
            }

            public System.String CotaFechamento
            {
                get
                {
                    System.Decimal? data = entity.CotaFechamento;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.CotaFechamento = null;
                    else entity.CotaFechamento = Convert.ToDecimal(value);
                }
            }

            public System.String CotaBruta
            {
                get
                {
                    System.Decimal? data = entity.CotaBruta;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.CotaBruta = null;
                    else entity.CotaBruta = Convert.ToDecimal(value);
                }
            }

            public System.String PLAbertura
            {
                get
                {
                    System.Decimal? data = entity.PLAbertura;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.PLAbertura = null;
                    else entity.PLAbertura = Convert.ToDecimal(value);
                }
            }

            public System.String PLFechamento
            {
                get
                {
                    System.Decimal? data = entity.PLFechamento;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.PLFechamento = null;
                    else entity.PLFechamento = Convert.ToDecimal(value);
                }
            }

            public System.String PatrimonioBruto
            {
                get
                {
                    System.Decimal? data = entity.PatrimonioBruto;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.PatrimonioBruto = null;
                    else entity.PatrimonioBruto = Convert.ToDecimal(value);
                }
            }

            public System.String QuantidadeFechamento
            {
                get
                {
                    System.Decimal? data = entity.QuantidadeFechamento;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.QuantidadeFechamento = null;
                    else entity.QuantidadeFechamento = Convert.ToDecimal(value);
                }
            }

            public System.String AjustePL
            {
                get
                {
                    System.Decimal? data = entity.AjustePL;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.AjustePL = null;
                    else entity.AjustePL = Convert.ToDecimal(value);
                }
            }

			public System.String CotaImportada
			{
				get
				{
					System.String data = entity.CotaImportada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaImportada = null;
					else entity.CotaImportada = Convert.ToString(value);
				}
			}
			
			public System.String CotaEx
			{
				get
				{
					System.Decimal? data = entity.CotaEx;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaEx = null;
					else entity.CotaEx = Convert.ToDecimal(value);
				}
			}

            public System.String CotaRendimento
            {
                get
                {
                    System.Decimal? data = entity.CotaRendimento;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.CotaRendimento = null;
                    else entity.CotaRendimento = Convert.ToDecimal(value);
                }
            }

            public System.String ProventoAcumulado
            {
                get
                {
                    System.Decimal? data = entity.ProventoAcumulado;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.ProventoAcumulado = null;
                    else entity.ProventoAcumulado = Convert.ToDecimal(value);
                }
            }

            private esHistoricoCota entity;
        }
        #endregion

        #region Query Logic
        protected void InitQuery(esHistoricoCotaQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntity)this).Connection;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        protected bool OnQueryLoaded(DataTable table)
        {
            bool dataFound = this.PopulateEntity(table);

            if (this.RowCount > 1)
            {
                throw new Exception("esHistoricoCota can only hold one record of data");
            }

            return dataFound;
        }
        #endregion

        [NonSerialized]
        private esStrings esstrings;
    }



    public partial class HistoricoCota : esHistoricoCota
    {


        #region UpToCarteiraByIdCarteira - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - Carteira_HistoricoCota_FK1
        /// </summary>

        [XmlIgnore]
        public Carteira UpToCarteiraByIdCarteira
        {
            get
            {
                if (this._UpToCarteiraByIdCarteira == null
                    && IdCarteira != null)
                {
                    this._UpToCarteiraByIdCarteira = new Carteira();
                    this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
                    this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
                    this._UpToCarteiraByIdCarteira.Query.Load();
                }

                return this._UpToCarteiraByIdCarteira;
            }

            set
            {
                this.RemovePreSave("UpToCarteiraByIdCarteira");


                if (value == null)
                {
                    this.IdCarteira = null;
                    this._UpToCarteiraByIdCarteira = null;
                }
                else
                {
                    this.IdCarteira = value.IdCarteira;
                    this._UpToCarteiraByIdCarteira = value;
                    this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
                }

            }
        }
        #endregion



        /// <summary>
        /// Used internally by the entity's hierarchical properties.
        /// </summary>
        protected override List<esPropertyDescriptor> GetHierarchicalProperties()
        {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();


            return props;
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PreSave.
        /// </summary>
        protected override void ApplyPreSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostSave.
        /// </summary>
        protected override void ApplyPostSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostOneToOneSave.
        /// </summary>
        protected override void ApplyPostOneSaveKeys()
        {
        }

    }



    [Serializable]
    abstract public class esHistoricoCotaQuery : esDynamicQuery
    {
        override protected IMetadata Meta
        {
            get
            {
                return HistoricoCotaMetadata.Meta();
            }
        }


        public esQueryItem Data
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaMetadata.ColumnNames.Data, esSystemType.DateTime);
            }
        }

        public esQueryItem IdCarteira
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
            }
        }

        public esQueryItem CotaAbertura
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaMetadata.ColumnNames.CotaAbertura, esSystemType.Decimal);
            }
        }

        public esQueryItem CotaFechamento
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaMetadata.ColumnNames.CotaFechamento, esSystemType.Decimal);
            }
        }

        public esQueryItem CotaBruta
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaMetadata.ColumnNames.CotaBruta, esSystemType.Decimal);
            }
        }

        public esQueryItem PLAbertura
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaMetadata.ColumnNames.PLAbertura, esSystemType.Decimal);
            }
        }

        public esQueryItem PLFechamento
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaMetadata.ColumnNames.PLFechamento, esSystemType.Decimal);
            }
        }

        public esQueryItem PatrimonioBruto
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaMetadata.ColumnNames.PatrimonioBruto, esSystemType.Decimal);
            }
        }

        public esQueryItem QuantidadeFechamento
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaMetadata.ColumnNames.QuantidadeFechamento, esSystemType.Decimal);
            }
        }

        public esQueryItem AjustePL
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaMetadata.ColumnNames.AjustePL, esSystemType.Decimal);
            }
        }

		public esQueryItem CotaImportada
		{
			get
			{
				return new esQueryItem(this, HistoricoCotaMetadata.ColumnNames.CotaImportada, esSystemType.String);
			}
		} 
		
		public esQueryItem CotaEx
		{
			get
			{
				return new esQueryItem(this, HistoricoCotaMetadata.ColumnNames.CotaEx, esSystemType.Decimal);
			}
		}

        public esQueryItem CotaRendimento
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaMetadata.ColumnNames.CotaRendimento, esSystemType.Decimal);
            }
        }

        public esQueryItem ProventoAcumulado
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaMetadata.ColumnNames.ProventoAcumulado, esSystemType.Decimal);
            }
        } 
    }



    [Serializable]
    [XmlType("HistoricoCotaCollection")]
    public partial class HistoricoCotaCollection : esHistoricoCotaCollection, IEnumerable<HistoricoCota>
    {
        public HistoricoCotaCollection()
        {

        }

        public static implicit operator List<HistoricoCota>(HistoricoCotaCollection coll)
        {
            List<HistoricoCota> list = new List<HistoricoCota>();

            foreach (HistoricoCota emp in coll)
            {
                list.Add(emp);
            }

            return list;
        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return HistoricoCotaMetadata.Meta();
            }
        }



        override protected esDynamicQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new HistoricoCotaQuery();
                this.InitQuery(query);
            }
            return this.query;
        }

        override protected esEntity CreateEntityForCollection(DataRow row)
        {
            return new HistoricoCota(row);
        }

        override protected esEntity CreateEntity()
        {
            return new HistoricoCota();
        }


        #endregion


        [BrowsableAttribute(false)]
        public HistoricoCotaQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new HistoricoCotaQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }

        public bool Load(HistoricoCotaQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        public HistoricoCota AddNew()
        {
            HistoricoCota entity = base.AddNewEntity() as HistoricoCota;

            return entity;
        }

        public HistoricoCota FindByPrimaryKey(System.DateTime data, System.Int32 idCarteira)
        {
            return base.FindByPrimaryKey(data, idCarteira) as HistoricoCota;
        }


        #region IEnumerable<HistoricoCota> Members

        IEnumerator<HistoricoCota> IEnumerable<HistoricoCota>.GetEnumerator()
        {
            System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
            System.Collections.IEnumerator iterator = enumer.GetEnumerator();

            while (iterator.MoveNext())
            {
                yield return iterator.Current as HistoricoCota;
            }
        }

        #endregion

        private HistoricoCotaQuery query;
    }


    /// <summary>
    /// Encapsulates the 'HistoricoCota' table
    /// </summary>

    [Serializable]
    public partial class HistoricoCota : esHistoricoCota
    {
        public HistoricoCota()
        {

        }

        public HistoricoCota(DataRow row)
            : base(row)
        {

        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return HistoricoCotaMetadata.Meta();
            }
        }



        override protected esHistoricoCotaQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new HistoricoCotaQuery();
                this.InitQuery(query);
            }
            return this.query;
        }
        #endregion




        [BrowsableAttribute(false)]
        public HistoricoCotaQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new HistoricoCotaQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }


        public bool Load(HistoricoCotaQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        private HistoricoCotaQuery query;
    }



    [Serializable]
    public partial class HistoricoCotaQuery : esHistoricoCotaQuery
    {
        public HistoricoCotaQuery()
        {

        }

        public HistoricoCotaQuery(string joinAlias)
        {
            this.es.JoinAlias = joinAlias;
        }


    }



    [Serializable]
    public partial class HistoricoCotaMetadata : esMetadata, IMetadata
    {
        #region Protected Constructor
        protected HistoricoCotaMetadata()
        {
            _columns = new esColumnMetadataCollection();
            esColumnMetadata c;

            c = new esColumnMetadata(HistoricoCotaMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = HistoricoCotaMetadata.PropertyNames.Data;
            c.IsInPrimaryKey = true;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = HistoricoCotaMetadata.PropertyNames.IdCarteira;
            c.IsInPrimaryKey = true;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaMetadata.ColumnNames.CotaAbertura, 2, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = HistoricoCotaMetadata.PropertyNames.CotaAbertura;
            c.NumericPrecision = 28;
            c.NumericScale = 12;
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaMetadata.ColumnNames.CotaFechamento, 3, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = HistoricoCotaMetadata.PropertyNames.CotaFechamento;
            c.NumericPrecision = 28;
            c.NumericScale = 12;
            c.HasDefault = true;
            c.Default = @"((0))";
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaMetadata.ColumnNames.CotaBruta, 4, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = HistoricoCotaMetadata.PropertyNames.CotaBruta;
            c.NumericPrecision = 28;
            c.NumericScale = 12;
            c.HasDefault = true;
            c.Default = @"((0))";
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaMetadata.ColumnNames.PLAbertura, 5, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = HistoricoCotaMetadata.PropertyNames.PLAbertura;
            c.NumericPrecision = 16;
            c.NumericScale = 2;
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaMetadata.ColumnNames.PLFechamento, 6, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = HistoricoCotaMetadata.PropertyNames.PLFechamento;
            c.NumericPrecision = 16;
            c.NumericScale = 2;
            c.HasDefault = true;
            c.Default = @"((0))";
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaMetadata.ColumnNames.PatrimonioBruto, 7, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = HistoricoCotaMetadata.PropertyNames.PatrimonioBruto;
            c.NumericPrecision = 16;
            c.NumericScale = 2;
            c.HasDefault = true;
            c.Default = @"((0))";
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaMetadata.ColumnNames.QuantidadeFechamento, 8, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = HistoricoCotaMetadata.PropertyNames.QuantidadeFechamento;
            c.NumericPrecision = 28;
            c.NumericScale = 12;
            c.HasDefault = true;
            c.Default = @"((0))";
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaMetadata.ColumnNames.AjustePL, 9, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = HistoricoCotaMetadata.PropertyNames.AjustePL;
            c.NumericPrecision = 16;
            c.NumericScale = 2;
            c.HasDefault = true;
            c.Default = @"((0))";
            _columns.Add(c);


			c = new esColumnMetadata(HistoricoCotaMetadata.ColumnNames.CotaImportada, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = HistoricoCotaMetadata.PropertyNames.CotaImportada;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoCotaMetadata.ColumnNames.CotaEx, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = HistoricoCotaMetadata.PropertyNames.CotaEx;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c);

            c = new esColumnMetadata(HistoricoCotaMetadata.ColumnNames.CotaRendimento, 12, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = HistoricoCotaMetadata.PropertyNames.CotaRendimento;
            c.NumericPrecision = 28;
            c.NumericScale = 10;
            c.HasDefault = true;
            c.Default = @"((0))";
            _columns.Add(c);

            c = new esColumnMetadata(HistoricoCotaMetadata.ColumnNames.ProventoAcumulado, 12, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = HistoricoCotaMetadata.PropertyNames.ProventoAcumulado;
            c.NumericPrecision = 28;
            c.NumericScale = 10;
            c.HasDefault = true;
            c.Default = @"((0))";
            _columns.Add(c); 
			
				
        }
        #endregion

        static public HistoricoCotaMetadata Meta()
        {
            return meta;
        }

        public Guid DataID
        {
            get { return base._dataID; }
        }

        public bool MultiProviderMode
        {
            get { return false; }
        }

        public esColumnMetadataCollection Columns
        {
            get { return base._columns; }
        }

        #region ColumnNames
        public class ColumnNames
        {
            public const string Data = "Data";
            public const string IdCarteira = "IdCarteira";
            public const string CotaAbertura = "CotaAbertura";
            public const string CotaFechamento = "CotaFechamento";
            public const string CotaBruta = "CotaBruta";
            public const string PLAbertura = "PLAbertura";
            public const string PLFechamento = "PLFechamento";
            public const string PatrimonioBruto = "PatrimonioBruto";
            public const string QuantidadeFechamento = "QuantidadeFechamento";
            public const string AjustePL = "AjustePL";
			public const string CotaImportada = "CotaImportada";
			public const string CotaEx = "CotaEx";
            public const string CotaRendimento = "CotaRendimento";
            public const string ProventoAcumulado = "ProventoAcumulado";
        }
        #endregion

        #region PropertyNames
        public class PropertyNames
        {
            public const string Data = "Data";
            public const string IdCarteira = "IdCarteira";
            public const string CotaAbertura = "CotaAbertura";
            public const string CotaFechamento = "CotaFechamento";
            public const string CotaBruta = "CotaBruta";
            public const string PLAbertura = "PLAbertura";
            public const string PLFechamento = "PLFechamento";
            public const string PatrimonioBruto = "PatrimonioBruto";
            public const string QuantidadeFechamento = "QuantidadeFechamento";
            public const string AjustePL = "AjustePL";
			public const string CotaImportada = "CotaImportada";
			public const string CotaEx = "CotaEx";
            public const string CotaRendimento = "CotaRendimento";
            public const string ProventoAcumulado = "ProventoAcumulado";
        }
        #endregion

        public esProviderSpecificMetadata GetProviderMetadata(string mapName)
        {
            MapToMeta mapMethod = mapDelegates[mapName];

            if (mapMethod != null)
                return mapMethod(mapName);
            else
                return null;
        }

        #region MAP esDefault

        static private int RegisterDelegateesDefault()
        {
            // This is only executed once per the life of the application
            lock (typeof(HistoricoCotaMetadata))
            {
                if (HistoricoCotaMetadata.mapDelegates == null)
                {
                    HistoricoCotaMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
                }

                if (HistoricoCotaMetadata.meta == null)
                {
                    HistoricoCotaMetadata.meta = new HistoricoCotaMetadata();
                }

                MapToMeta mapMethod = new MapToMeta(meta.esDefault);
                mapDelegates.Add("esDefault", mapMethod);
                mapMethod("esDefault");
            }
            return 0;
        }

        private esProviderSpecificMetadata esDefault(string mapName)
        {
            if (!_providerMetadataMaps.ContainsKey(mapName))
            {
                esProviderSpecificMetadata meta = new esProviderSpecificMetadata();


                meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
                meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("CotaAbertura", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("CotaFechamento", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("CotaBruta", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("PLAbertura", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("PLFechamento", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("PatrimonioBruto", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("QuantidadeFechamento", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("AjustePL", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaImportada", new esTypeMap("char", "System.String"));			
				meta.AddTypeMap("CotaEx", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("CotaRendimento", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("ProventoAcumulado", new esTypeMap("decimal", "System.Decimal"));


                meta.Source = "HistoricoCota";
                meta.Destination = "HistoricoCota";

                meta.spInsert = "proc_HistoricoCotaInsert";
                meta.spUpdate = "proc_HistoricoCotaUpdate";
                meta.spDelete = "proc_HistoricoCotaDelete";
                meta.spLoadAll = "proc_HistoricoCotaLoadAll";
                meta.spLoadByPrimaryKey = "proc_HistoricoCotaLoadByPrimaryKey";

                this._providerMetadataMaps["esDefault"] = meta;
            }

            return this._providerMetadataMaps["esDefault"];
        }

        #endregion

        static private HistoricoCotaMetadata meta;
        static protected Dictionary<string, MapToMeta> mapDelegates;
        static private int _esDefault = RegisterDelegateesDefault();
    }
}
