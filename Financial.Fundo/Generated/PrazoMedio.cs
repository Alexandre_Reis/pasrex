/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 20/05/2015 11:09:10
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esPrazoMedioCollection : esEntityCollection
	{
		public esPrazoMedioCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PrazoMedioCollection";
		}

		#region Query Logic
		protected void InitQuery(esPrazoMedioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPrazoMedioQuery);
		}
		#endregion
		
		virtual public PrazoMedio DetachEntity(PrazoMedio entity)
		{
			return base.DetachEntity(entity) as PrazoMedio;
		}
		
		virtual public PrazoMedio AttachEntity(PrazoMedio entity)
		{
			return base.AttachEntity(entity) as PrazoMedio;
		}
		
		virtual public void Combine(PrazoMedioCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PrazoMedio this[int index]
		{
			get
			{
				return base[index] as PrazoMedio;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PrazoMedio);
		}
	}



	[Serializable]
	abstract public class esPrazoMedio : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPrazoMedioQuery GetDynamicQuery()
		{
			return null;
		}

		public esPrazoMedio()
		{

		}

		public esPrazoMedio(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPrazoMedio)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPrazoMedio);
			else
				return LoadByPrimaryKeyStoredProcedure(idPrazoMedio);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPrazoMedio)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPrazoMedio);
			else
				return LoadByPrimaryKeyStoredProcedure(idPrazoMedio);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPrazoMedio)
		{
			esPrazoMedioQuery query = this.GetDynamicQuery();
			query.Where(query.IdPrazoMedio == idPrazoMedio);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPrazoMedio)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPrazoMedio",idPrazoMedio);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPrazoMedio": this.str.IdPrazoMedio = (string)value; break;							
						case "DataAtual": this.str.DataAtual = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "MercadoAtivo": this.str.MercadoAtivo = (string)value; break;							
						case "PrazoMedio": this.str.PrazoMedio = (string)value; break;							
						case "ValorPosicao": this.str.ValorPosicao = (string)value; break;							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPrazoMedio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPrazoMedio = (System.Int32?)value;
							break;
						
						case "DataAtual":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAtual = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "MercadoAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.MercadoAtivo = (System.Int32?)value;
							break;
						
						case "PrazoMedio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrazoMedio = (System.Decimal?)value;
							break;
						
						case "ValorPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPosicao = (System.Decimal?)value;
							break;
						
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PrazoMedio.IdPrazoMedio
		/// </summary>
		virtual public System.Int32? IdPrazoMedio
		{
			get
			{
				return base.GetSystemInt32(PrazoMedioMetadata.ColumnNames.IdPrazoMedio);
			}
			
			set
			{
				base.SetSystemInt32(PrazoMedioMetadata.ColumnNames.IdPrazoMedio, value);
			}
		}
		
		/// <summary>
		/// Maps to PrazoMedio.DataAtual
		/// </summary>
		virtual public System.DateTime? DataAtual
		{
			get
			{
				return base.GetSystemDateTime(PrazoMedioMetadata.ColumnNames.DataAtual);
			}
			
			set
			{
				base.SetSystemDateTime(PrazoMedioMetadata.ColumnNames.DataAtual, value);
			}
		}
		
		/// <summary>
		/// Maps to PrazoMedio.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PrazoMedioMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(PrazoMedioMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to PrazoMedio.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(PrazoMedioMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(PrazoMedioMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to PrazoMedio.MercadoAtivo
		/// </summary>
		virtual public System.Int32? MercadoAtivo
		{
			get
			{
				return base.GetSystemInt32(PrazoMedioMetadata.ColumnNames.MercadoAtivo);
			}
			
			set
			{
				base.SetSystemInt32(PrazoMedioMetadata.ColumnNames.MercadoAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to PrazoMedio.PrazoMedio
		/// </summary>
		virtual public System.Decimal? PrazoMedio
		{
			get
			{
				return base.GetSystemDecimal(PrazoMedioMetadata.ColumnNames.PrazoMedio);
			}
			
			set
			{
				base.SetSystemDecimal(PrazoMedioMetadata.ColumnNames.PrazoMedio, value);
			}
		}
		
		/// <summary>
		/// Maps to PrazoMedio.ValorPosicao
		/// </summary>
		virtual public System.Decimal? ValorPosicao
		{
			get
			{
				return base.GetSystemDecimal(PrazoMedioMetadata.ColumnNames.ValorPosicao);
			}
			
			set
			{
				base.SetSystemDecimal(PrazoMedioMetadata.ColumnNames.ValorPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PrazoMedio.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PrazoMedioMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PrazoMedioMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPrazoMedio entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPrazoMedio
			{
				get
				{
					System.Int32? data = entity.IdPrazoMedio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPrazoMedio = null;
					else entity.IdPrazoMedio = Convert.ToInt32(value);
				}
			}
				
			public System.String DataAtual
			{
				get
				{
					System.DateTime? data = entity.DataAtual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAtual = null;
					else entity.DataAtual = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String MercadoAtivo
			{
				get
				{
					System.Int32? data = entity.MercadoAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MercadoAtivo = null;
					else entity.MercadoAtivo = Convert.ToInt32(value);
				}
			}
				
			public System.String PrazoMedio
			{
				get
				{
					System.Decimal? data = entity.PrazoMedio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoMedio = null;
					else entity.PrazoMedio = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPosicao
			{
				get
				{
					System.Decimal? data = entity.ValorPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPosicao = null;
					else entity.ValorPosicao = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
			

			private esPrazoMedio entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPrazoMedioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPrazoMedio can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PrazoMedio : esPrazoMedio
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPrazoMedioQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PrazoMedioMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPrazoMedio
		{
			get
			{
				return new esQueryItem(this, PrazoMedioMetadata.ColumnNames.IdPrazoMedio, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataAtual
		{
			get
			{
				return new esQueryItem(this, PrazoMedioMetadata.ColumnNames.DataAtual, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PrazoMedioMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, PrazoMedioMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem MercadoAtivo
		{
			get
			{
				return new esQueryItem(this, PrazoMedioMetadata.ColumnNames.MercadoAtivo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PrazoMedio
		{
			get
			{
				return new esQueryItem(this, PrazoMedioMetadata.ColumnNames.PrazoMedio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPosicao
		{
			get
			{
				return new esQueryItem(this, PrazoMedioMetadata.ColumnNames.ValorPosicao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PrazoMedioMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PrazoMedioCollection")]
	public partial class PrazoMedioCollection : esPrazoMedioCollection, IEnumerable<PrazoMedio>
	{
		public PrazoMedioCollection()
		{

		}
		
		public static implicit operator List<PrazoMedio>(PrazoMedioCollection coll)
		{
			List<PrazoMedio> list = new List<PrazoMedio>();
			
			foreach (PrazoMedio emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PrazoMedioMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PrazoMedioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PrazoMedio(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PrazoMedio();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PrazoMedioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PrazoMedioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PrazoMedioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PrazoMedio AddNew()
		{
			PrazoMedio entity = base.AddNewEntity() as PrazoMedio;
			
			return entity;
		}

		public PrazoMedio FindByPrimaryKey(System.Int32 idPrazoMedio)
		{
			return base.FindByPrimaryKey(idPrazoMedio) as PrazoMedio;
		}


		#region IEnumerable<PrazoMedio> Members

		IEnumerator<PrazoMedio> IEnumerable<PrazoMedio>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PrazoMedio;
			}
		}

		#endregion
		
		private PrazoMedioQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PrazoMedio' table
	/// </summary>

	[Serializable]
	public partial class PrazoMedio : esPrazoMedio
	{
		public PrazoMedio()
		{

		}
	
		public PrazoMedio(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PrazoMedioMetadata.Meta();
			}
		}
		
		
		
		override protected esPrazoMedioQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PrazoMedioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PrazoMedioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PrazoMedioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PrazoMedioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PrazoMedioQuery query;
	}



	[Serializable]
	public partial class PrazoMedioQuery : esPrazoMedioQuery
	{
		public PrazoMedioQuery()
		{

		}		
		
		public PrazoMedioQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PrazoMedioMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PrazoMedioMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PrazoMedioMetadata.ColumnNames.IdPrazoMedio, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PrazoMedioMetadata.PropertyNames.IdPrazoMedio;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrazoMedioMetadata.ColumnNames.DataAtual, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PrazoMedioMetadata.PropertyNames.DataAtual;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrazoMedioMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PrazoMedioMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrazoMedioMetadata.ColumnNames.Descricao, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = PrazoMedioMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrazoMedioMetadata.ColumnNames.MercadoAtivo, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PrazoMedioMetadata.PropertyNames.MercadoAtivo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrazoMedioMetadata.ColumnNames.PrazoMedio, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PrazoMedioMetadata.PropertyNames.PrazoMedio;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrazoMedioMetadata.ColumnNames.ValorPosicao, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PrazoMedioMetadata.PropertyNames.ValorPosicao;	
			c.NumericPrecision = 25;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PrazoMedioMetadata.ColumnNames.IdPosicao, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PrazoMedioMetadata.PropertyNames.IdPosicao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PrazoMedioMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPrazoMedio = "IdPrazoMedio";
			 public const string DataAtual = "DataAtual";
			 public const string IdCliente = "IdCliente";
			 public const string Descricao = "Descricao";
			 public const string MercadoAtivo = "MercadoAtivo";
			 public const string PrazoMedio = "PrazoMedio";
			 public const string ValorPosicao = "ValorPosicao";
			 public const string IdPosicao = "IdPosicao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPrazoMedio = "IdPrazoMedio";
			 public const string DataAtual = "DataAtual";
			 public const string IdCliente = "IdCliente";
			 public const string Descricao = "Descricao";
			 public const string MercadoAtivo = "MercadoAtivo";
			 public const string PrazoMedio = "PrazoMedio";
			 public const string ValorPosicao = "ValorPosicao";
			 public const string IdPosicao = "IdPosicao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PrazoMedioMetadata))
			{
				if(PrazoMedioMetadata.mapDelegates == null)
				{
					PrazoMedioMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PrazoMedioMetadata.meta == null)
				{
					PrazoMedioMetadata.meta = new PrazoMedioMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPrazoMedio", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataAtual", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("MercadoAtivo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PrazoMedio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPosicao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "PrazoMedio";
				meta.Destination = "PrazoMedio";
				
				meta.spInsert = "proc_PrazoMedioInsert";				
				meta.spUpdate = "proc_PrazoMedioUpdate";		
				meta.spDelete = "proc_PrazoMedioDelete";
				meta.spLoadAll = "proc_PrazoMedioLoadAll";
				meta.spLoadByPrimaryKey = "proc_PrazoMedioLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PrazoMedioMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
