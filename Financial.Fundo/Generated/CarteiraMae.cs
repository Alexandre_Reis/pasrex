/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:17 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esCarteiraMaeCollection : esEntityCollection
	{
		public esCarteiraMaeCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CarteiraMaeCollection";
		}

		#region Query Logic
		protected void InitQuery(esCarteiraMaeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCarteiraMaeQuery);
		}
		#endregion
		
		virtual public CarteiraMae DetachEntity(CarteiraMae entity)
		{
			return base.DetachEntity(entity) as CarteiraMae;
		}
		
		virtual public CarteiraMae AttachEntity(CarteiraMae entity)
		{
			return base.AttachEntity(entity) as CarteiraMae;
		}
		
		virtual public void Combine(CarteiraMaeCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CarteiraMae this[int index]
		{
			get
			{
				return base[index] as CarteiraMae;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CarteiraMae);
		}
	}



	[Serializable]
	abstract public class esCarteiraMae : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCarteiraMaeQuery GetDynamicQuery()
		{
			return null;
		}

		public esCarteiraMae()
		{

		}

		public esCarteiraMae(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCarteiraFilha, System.Int32 idCarteiraMae)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteiraFilha, idCarteiraMae);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteiraFilha, idCarteiraMae);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCarteiraFilha, System.Int32 idCarteiraMae)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCarteiraMaeQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCarteiraFilha == idCarteiraFilha, query.IdCarteiraMae == idCarteiraMae);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCarteiraFilha, System.Int32 idCarteiraMae)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteiraFilha, idCarteiraMae);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteiraFilha, idCarteiraMae);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCarteiraFilha, System.Int32 idCarteiraMae)
		{
			esCarteiraMaeQuery query = this.GetDynamicQuery();
			query.Where(query.IdCarteiraFilha == idCarteiraFilha, query.IdCarteiraMae == idCarteiraMae);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCarteiraFilha, System.Int32 idCarteiraMae)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCarteiraFilha",idCarteiraFilha);			parms.Add("IdCarteiraMae",idCarteiraMae);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteiraFilha": this.str.IdCarteiraFilha = (string)value; break;							
						case "IdCarteiraMae": this.str.IdCarteiraMae = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteiraFilha":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraFilha = (System.Int32?)value;
							break;
						
						case "IdCarteiraMae":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraMae = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CarteiraMae.IdCarteiraFilha
		/// </summary>
		virtual public System.Int32? IdCarteiraFilha
		{
			get
			{
				return base.GetSystemInt32(CarteiraMaeMetadata.ColumnNames.IdCarteiraFilha);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMaeMetadata.ColumnNames.IdCarteiraFilha, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraMae.IdCarteiraMae
		/// </summary>
		virtual public System.Int32? IdCarteiraMae
		{
			get
			{
				return base.GetSystemInt32(CarteiraMaeMetadata.ColumnNames.IdCarteiraMae);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMaeMetadata.ColumnNames.IdCarteiraMae, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCarteiraMae entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteiraFilha
			{
				get
				{
					System.Int32? data = entity.IdCarteiraFilha;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraFilha = null;
					else entity.IdCarteiraFilha = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteiraMae
			{
				get
				{
					System.Int32? data = entity.IdCarteiraMae;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraMae = null;
					else entity.IdCarteiraMae = Convert.ToInt32(value);
				}
			}
			

			private esCarteiraMae entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCarteiraMaeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCarteiraMae can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CarteiraMae : esCarteiraMae
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCarteiraMaeQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CarteiraMaeMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteiraFilha
		{
			get
			{
				return new esQueryItem(this, CarteiraMaeMetadata.ColumnNames.IdCarteiraFilha, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteiraMae
		{
			get
			{
				return new esQueryItem(this, CarteiraMaeMetadata.ColumnNames.IdCarteiraMae, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CarteiraMaeCollection")]
	public partial class CarteiraMaeCollection : esCarteiraMaeCollection, IEnumerable<CarteiraMae>
	{
		public CarteiraMaeCollection()
		{

		}
		
		public static implicit operator List<CarteiraMae>(CarteiraMaeCollection coll)
		{
			List<CarteiraMae> list = new List<CarteiraMae>();
			
			foreach (CarteiraMae emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CarteiraMaeMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CarteiraMaeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CarteiraMae(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CarteiraMae();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CarteiraMaeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CarteiraMaeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CarteiraMaeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CarteiraMae AddNew()
		{
			CarteiraMae entity = base.AddNewEntity() as CarteiraMae;
			
			return entity;
		}

		public CarteiraMae FindByPrimaryKey(System.Int32 idCarteiraFilha, System.Int32 idCarteiraMae)
		{
			return base.FindByPrimaryKey(idCarteiraFilha, idCarteiraMae) as CarteiraMae;
		}


		#region IEnumerable<CarteiraMae> Members

		IEnumerator<CarteiraMae> IEnumerable<CarteiraMae>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CarteiraMae;
			}
		}

		#endregion
		
		private CarteiraMaeQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CarteiraMae' table
	/// </summary>

	[Serializable]
	public partial class CarteiraMae : esCarteiraMae
	{
		public CarteiraMae()
		{

		}
	
		public CarteiraMae(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CarteiraMaeMetadata.Meta();
			}
		}
		
		
		
		override protected esCarteiraMaeQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CarteiraMaeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CarteiraMaeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CarteiraMaeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CarteiraMaeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CarteiraMaeQuery query;
	}



	[Serializable]
	public partial class CarteiraMaeQuery : esCarteiraMaeQuery
	{
		public CarteiraMaeQuery()
		{

		}		
		
		public CarteiraMaeQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CarteiraMaeMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CarteiraMaeMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CarteiraMaeMetadata.ColumnNames.IdCarteiraFilha, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMaeMetadata.PropertyNames.IdCarteiraFilha;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMaeMetadata.ColumnNames.IdCarteiraMae, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMaeMetadata.PropertyNames.IdCarteiraMae;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CarteiraMaeMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteiraFilha = "IdCarteiraFilha";
			 public const string IdCarteiraMae = "IdCarteiraMae";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteiraFilha = "IdCarteiraFilha";
			 public const string IdCarteiraMae = "IdCarteiraMae";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CarteiraMaeMetadata))
			{
				if(CarteiraMaeMetadata.mapDelegates == null)
				{
					CarteiraMaeMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CarteiraMaeMetadata.meta == null)
				{
					CarteiraMaeMetadata.meta = new CarteiraMaeMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteiraFilha", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteiraMae", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "CarteiraMae";
				meta.Destination = "CarteiraMae";
				
				meta.spInsert = "proc_CarteiraMaeInsert";				
				meta.spUpdate = "proc_CarteiraMaeUpdate";		
				meta.spDelete = "proc_CarteiraMaeDelete";
				meta.spLoadAll = "proc_CarteiraMaeLoadAll";
				meta.spLoadByPrimaryKey = "proc_CarteiraMaeLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CarteiraMaeMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
