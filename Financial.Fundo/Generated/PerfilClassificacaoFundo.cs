/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 14/03/2016 15:13:39
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esPerfilClassificacaoFundoCollection : esEntityCollection
	{
		public esPerfilClassificacaoFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PerfilClassificacaoFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esPerfilClassificacaoFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPerfilClassificacaoFundoQuery);
		}
		#endregion
		
		virtual public PerfilClassificacaoFundo DetachEntity(PerfilClassificacaoFundo entity)
		{
			return base.DetachEntity(entity) as PerfilClassificacaoFundo;
		}
		
		virtual public PerfilClassificacaoFundo AttachEntity(PerfilClassificacaoFundo entity)
		{
			return base.AttachEntity(entity) as PerfilClassificacaoFundo;
		}
		
		virtual public void Combine(PerfilClassificacaoFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PerfilClassificacaoFundo this[int index]
		{
			get
			{
				return base[index] as PerfilClassificacaoFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PerfilClassificacaoFundo);
		}
	}



	[Serializable]
	abstract public class esPerfilClassificacaoFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPerfilClassificacaoFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esPerfilClassificacaoFundo()
		{

		}

		public esPerfilClassificacaoFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPerfil)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfil);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfil);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPerfil)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfil);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfil);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPerfil)
		{
			esPerfilClassificacaoFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdPerfil == idPerfil);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPerfil)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPerfil",idPerfil);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPerfil": this.str.IdPerfil = (string)value; break;							
						case "Codigo": this.str.Codigo = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "InicioVigencia": this.str.InicioVigencia = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPerfil":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfil = (System.Int32?)value;
							break;
						
						case "InicioVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.InicioVigencia = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PerfilClassificacaoFundo.IdPerfil
		/// </summary>
		virtual public System.Int32? IdPerfil
		{
			get
			{
				return base.GetSystemInt32(PerfilClassificacaoFundoMetadata.ColumnNames.IdPerfil);
			}
			
			set
			{
				base.SetSystemInt32(PerfilClassificacaoFundoMetadata.ColumnNames.IdPerfil, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilClassificacaoFundo.Codigo
		/// </summary>
		virtual public System.String Codigo
		{
			get
			{
				return base.GetSystemString(PerfilClassificacaoFundoMetadata.ColumnNames.Codigo);
			}
			
			set
			{
				base.SetSystemString(PerfilClassificacaoFundoMetadata.ColumnNames.Codigo, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilClassificacaoFundo.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(PerfilClassificacaoFundoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(PerfilClassificacaoFundoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilClassificacaoFundo.InicioVigencia
		/// </summary>
		virtual public System.DateTime? InicioVigencia
		{
			get
			{
				return base.GetSystemDateTime(PerfilClassificacaoFundoMetadata.ColumnNames.InicioVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(PerfilClassificacaoFundoMetadata.ColumnNames.InicioVigencia, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPerfilClassificacaoFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPerfil
			{
				get
				{
					System.Int32? data = entity.IdPerfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfil = null;
					else entity.IdPerfil = Convert.ToInt32(value);
				}
			}
				
			public System.String Codigo
			{
				get
				{
					System.String data = entity.Codigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Codigo = null;
					else entity.Codigo = Convert.ToString(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String InicioVigencia
			{
				get
				{
					System.DateTime? data = entity.InicioVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InicioVigencia = null;
					else entity.InicioVigencia = Convert.ToDateTime(value);
				}
			}
			

			private esPerfilClassificacaoFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPerfilClassificacaoFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPerfilClassificacaoFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PerfilClassificacaoFundo : esPerfilClassificacaoFundo
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPerfilClassificacaoFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PerfilClassificacaoFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPerfil
		{
			get
			{
				return new esQueryItem(this, PerfilClassificacaoFundoMetadata.ColumnNames.IdPerfil, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Codigo
		{
			get
			{
				return new esQueryItem(this, PerfilClassificacaoFundoMetadata.ColumnNames.Codigo, esSystemType.String);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, PerfilClassificacaoFundoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem InicioVigencia
		{
			get
			{
				return new esQueryItem(this, PerfilClassificacaoFundoMetadata.ColumnNames.InicioVigencia, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PerfilClassificacaoFundoCollection")]
	public partial class PerfilClassificacaoFundoCollection : esPerfilClassificacaoFundoCollection, IEnumerable<PerfilClassificacaoFundo>
	{
		public PerfilClassificacaoFundoCollection()
		{

		}
		
		public static implicit operator List<PerfilClassificacaoFundo>(PerfilClassificacaoFundoCollection coll)
		{
			List<PerfilClassificacaoFundo> list = new List<PerfilClassificacaoFundo>();
			
			foreach (PerfilClassificacaoFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PerfilClassificacaoFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilClassificacaoFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PerfilClassificacaoFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PerfilClassificacaoFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PerfilClassificacaoFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilClassificacaoFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PerfilClassificacaoFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PerfilClassificacaoFundo AddNew()
		{
			PerfilClassificacaoFundo entity = base.AddNewEntity() as PerfilClassificacaoFundo;
			
			return entity;
		}

		public PerfilClassificacaoFundo FindByPrimaryKey(System.Int32 idPerfil)
		{
			return base.FindByPrimaryKey(idPerfil) as PerfilClassificacaoFundo;
		}


		#region IEnumerable<PerfilClassificacaoFundo> Members

		IEnumerator<PerfilClassificacaoFundo> IEnumerable<PerfilClassificacaoFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PerfilClassificacaoFundo;
			}
		}

		#endregion
		
		private PerfilClassificacaoFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PerfilClassificacaoFundo' table
	/// </summary>

	[Serializable]
	public partial class PerfilClassificacaoFundo : esPerfilClassificacaoFundo
	{
		public PerfilClassificacaoFundo()
		{

		}
	
		public PerfilClassificacaoFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PerfilClassificacaoFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esPerfilClassificacaoFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilClassificacaoFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PerfilClassificacaoFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilClassificacaoFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PerfilClassificacaoFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PerfilClassificacaoFundoQuery query;
	}



	[Serializable]
	public partial class PerfilClassificacaoFundoQuery : esPerfilClassificacaoFundoQuery
	{
		public PerfilClassificacaoFundoQuery()
		{

		}		
		
		public PerfilClassificacaoFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PerfilClassificacaoFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PerfilClassificacaoFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PerfilClassificacaoFundoMetadata.ColumnNames.IdPerfil, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilClassificacaoFundoMetadata.PropertyNames.IdPerfil;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilClassificacaoFundoMetadata.ColumnNames.Codigo, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = PerfilClassificacaoFundoMetadata.PropertyNames.Codigo;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilClassificacaoFundoMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = PerfilClassificacaoFundoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilClassificacaoFundoMetadata.ColumnNames.InicioVigencia, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PerfilClassificacaoFundoMetadata.PropertyNames.InicioVigencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PerfilClassificacaoFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPerfil = "IdPerfil";
			 public const string Codigo = "Codigo";
			 public const string Descricao = "Descricao";
			 public const string InicioVigencia = "InicioVigencia";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPerfil = "IdPerfil";
			 public const string Codigo = "Codigo";
			 public const string Descricao = "Descricao";
			 public const string InicioVigencia = "InicioVigencia";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PerfilClassificacaoFundoMetadata))
			{
				if(PerfilClassificacaoFundoMetadata.mapDelegates == null)
				{
					PerfilClassificacaoFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PerfilClassificacaoFundoMetadata.meta == null)
				{
					PerfilClassificacaoFundoMetadata.meta = new PerfilClassificacaoFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPerfil", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Codigo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("InicioVigencia", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "PerfilClassificacaoFundo";
				meta.Destination = "PerfilClassificacaoFundo";
				
				meta.spInsert = "proc_PerfilClassificacaoFundoInsert";				
				meta.spUpdate = "proc_PerfilClassificacaoFundoUpdate";		
				meta.spDelete = "proc_PerfilClassificacaoFundoDelete";
				meta.spLoadAll = "proc_PerfilClassificacaoFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_PerfilClassificacaoFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PerfilClassificacaoFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
