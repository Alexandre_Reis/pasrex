/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 10/03/2015 17:09:42
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esRentabilidadeSerieCollection : esEntityCollection
	{
		public esRentabilidadeSerieCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "RentabilidadeSerieCollection";
		}

		#region Query Logic
		protected void InitQuery(esRentabilidadeSerieQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esRentabilidadeSerieQuery);
		}
		#endregion
		
		virtual public RentabilidadeSerie DetachEntity(RentabilidadeSerie entity)
		{
			return base.DetachEntity(entity) as RentabilidadeSerie;
		}
		
		virtual public RentabilidadeSerie AttachEntity(RentabilidadeSerie entity)
		{
			return base.AttachEntity(entity) as RentabilidadeSerie;
		}
		
		virtual public void Combine(RentabilidadeSerieCollection collection)
		{
			base.Combine(collection);
		}
		
		new public RentabilidadeSerie this[int index]
		{
			get
			{
				return base[index] as RentabilidadeSerie;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(RentabilidadeSerie);
		}
	}



	[Serializable]
	abstract public class esRentabilidadeSerie : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esRentabilidadeSerieQuery GetDynamicQuery()
		{
			return null;
		}

		public esRentabilidadeSerie()
		{

		}

		public esRentabilidadeSerie(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idRentabilidadeSerie)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idRentabilidadeSerie);
			else
				return LoadByPrimaryKeyStoredProcedure(idRentabilidadeSerie);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idRentabilidadeSerie)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idRentabilidadeSerie);
			else
				return LoadByPrimaryKeyStoredProcedure(idRentabilidadeSerie);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idRentabilidadeSerie)
		{
			esRentabilidadeSerieQuery query = this.GetDynamicQuery();
			query.Where(query.IdRentabilidadeSerie == idRentabilidadeSerie);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idRentabilidadeSerie)
		{
			esParameters parms = new esParameters();
			parms.Add("IdRentabilidadeSerie",idRentabilidadeSerie);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdRentabilidadeSerie": this.str.IdRentabilidadeSerie = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "IdFundo": this.str.IdFundo = (string)value; break;							
						case "IdSerie": this.str.IdSerie = (string)value; break;							
						case "QuantidadeSerie": this.str.QuantidadeSerie = (string)value; break;							
						case "Rentabilidade": this.str.Rentabilidade = (string)value; break;							
						case "RentabilidadeAcumuluda": this.str.RentabilidadeAcumuluda = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdRentabilidadeSerie":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdRentabilidadeSerie = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdFundo = (System.Int32?)value;
							break;
						
						case "IdSerie":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSerie = (System.Int32?)value;
							break;
						
						case "QuantidadeSerie":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeSerie = (System.Decimal?)value;
							break;
						
						case "Rentabilidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Rentabilidade = (System.Decimal?)value;
							break;
						
						case "RentabilidadeAcumuluda":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RentabilidadeAcumuluda = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to RentabilidadeSerie.IdRentabilidadeSerie
		/// </summary>
		virtual public System.Int32? IdRentabilidadeSerie
		{
			get
			{
				return base.GetSystemInt32(RentabilidadeSerieMetadata.ColumnNames.IdRentabilidadeSerie);
			}
			
			set
			{
				base.SetSystemInt32(RentabilidadeSerieMetadata.ColumnNames.IdRentabilidadeSerie, value);
			}
		}
		
		/// <summary>
		/// Maps to RentabilidadeSerie.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(RentabilidadeSerieMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(RentabilidadeSerieMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to RentabilidadeSerie.IdFundo
		/// </summary>
		virtual public System.Int32? IdFundo
		{
			get
			{
				return base.GetSystemInt32(RentabilidadeSerieMetadata.ColumnNames.IdFundo);
			}
			
			set
			{
				base.SetSystemInt32(RentabilidadeSerieMetadata.ColumnNames.IdFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to RentabilidadeSerie.IdSerie
		/// </summary>
		virtual public System.Int32? IdSerie
		{
			get
			{
				return base.GetSystemInt32(RentabilidadeSerieMetadata.ColumnNames.IdSerie);
			}
			
			set
			{
				base.SetSystemInt32(RentabilidadeSerieMetadata.ColumnNames.IdSerie, value);
			}
		}
		
		/// <summary>
		/// Maps to RentabilidadeSerie.QuantidadeSerie
		/// </summary>
		virtual public System.Decimal? QuantidadeSerie
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeSerieMetadata.ColumnNames.QuantidadeSerie);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeSerieMetadata.ColumnNames.QuantidadeSerie, value);
			}
		}
		
		/// <summary>
		/// Maps to RentabilidadeSerie.Rentabilidade
		/// </summary>
		virtual public System.Decimal? Rentabilidade
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeSerieMetadata.ColumnNames.Rentabilidade);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeSerieMetadata.ColumnNames.Rentabilidade, value);
			}
		}
		
		/// <summary>
		/// Maps to RentabilidadeSerie.RentabilidadeAcumuluda
		/// </summary>
		virtual public System.Decimal? RentabilidadeAcumuluda
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeSerieMetadata.ColumnNames.RentabilidadeAcumuluda);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeSerieMetadata.ColumnNames.RentabilidadeAcumuluda, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esRentabilidadeSerie entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdRentabilidadeSerie
			{
				get
				{
					System.Int32? data = entity.IdRentabilidadeSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdRentabilidadeSerie = null;
					else entity.IdRentabilidadeSerie = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdFundo
			{
				get
				{
					System.Int32? data = entity.IdFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdFundo = null;
					else entity.IdFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String IdSerie
			{
				get
				{
					System.Int32? data = entity.IdSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSerie = null;
					else entity.IdSerie = Convert.ToInt32(value);
				}
			}
				
			public System.String QuantidadeSerie
			{
				get
				{
					System.Decimal? data = entity.QuantidadeSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeSerie = null;
					else entity.QuantidadeSerie = Convert.ToDecimal(value);
				}
			}
				
			public System.String Rentabilidade
			{
				get
				{
					System.Decimal? data = entity.Rentabilidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Rentabilidade = null;
					else entity.Rentabilidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String RentabilidadeAcumuluda
			{
				get
				{
					System.Decimal? data = entity.RentabilidadeAcumuluda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RentabilidadeAcumuluda = null;
					else entity.RentabilidadeAcumuluda = Convert.ToDecimal(value);
				}
			}
			

			private esRentabilidadeSerie entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esRentabilidadeSerieQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esRentabilidadeSerie can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class RentabilidadeSerie : esRentabilidadeSerie
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esRentabilidadeSerieQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return RentabilidadeSerieMetadata.Meta();
			}
		}	
		

		public esQueryItem IdRentabilidadeSerie
		{
			get
			{
				return new esQueryItem(this, RentabilidadeSerieMetadata.ColumnNames.IdRentabilidadeSerie, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, RentabilidadeSerieMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdFundo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeSerieMetadata.ColumnNames.IdFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdSerie
		{
			get
			{
				return new esQueryItem(this, RentabilidadeSerieMetadata.ColumnNames.IdSerie, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QuantidadeSerie
		{
			get
			{
				return new esQueryItem(this, RentabilidadeSerieMetadata.ColumnNames.QuantidadeSerie, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Rentabilidade
		{
			get
			{
				return new esQueryItem(this, RentabilidadeSerieMetadata.ColumnNames.Rentabilidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RentabilidadeAcumuluda
		{
			get
			{
				return new esQueryItem(this, RentabilidadeSerieMetadata.ColumnNames.RentabilidadeAcumuluda, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("RentabilidadeSerieCollection")]
	public partial class RentabilidadeSerieCollection : esRentabilidadeSerieCollection, IEnumerable<RentabilidadeSerie>
	{
		public RentabilidadeSerieCollection()
		{

		}
		
		public static implicit operator List<RentabilidadeSerie>(RentabilidadeSerieCollection coll)
		{
			List<RentabilidadeSerie> list = new List<RentabilidadeSerie>();
			
			foreach (RentabilidadeSerie emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  RentabilidadeSerieMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new RentabilidadeSerieQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new RentabilidadeSerie(row);
		}

		override protected esEntity CreateEntity()
		{
			return new RentabilidadeSerie();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public RentabilidadeSerieQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new RentabilidadeSerieQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(RentabilidadeSerieQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public RentabilidadeSerie AddNew()
		{
			RentabilidadeSerie entity = base.AddNewEntity() as RentabilidadeSerie;
			
			return entity;
		}

		public RentabilidadeSerie FindByPrimaryKey(System.Int32 idRentabilidadeSerie)
		{
			return base.FindByPrimaryKey(idRentabilidadeSerie) as RentabilidadeSerie;
		}


		#region IEnumerable<RentabilidadeSerie> Members

		IEnumerator<RentabilidadeSerie> IEnumerable<RentabilidadeSerie>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as RentabilidadeSerie;
			}
		}

		#endregion
		
		private RentabilidadeSerieQuery query;
	}


	/// <summary>
	/// Encapsulates the 'RentabilidadeSerie' table
	/// </summary>

	[Serializable]
	public partial class RentabilidadeSerie : esRentabilidadeSerie
	{
		public RentabilidadeSerie()
		{

		}
	
		public RentabilidadeSerie(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return RentabilidadeSerieMetadata.Meta();
			}
		}
		
		
		
		override protected esRentabilidadeSerieQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new RentabilidadeSerieQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public RentabilidadeSerieQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new RentabilidadeSerieQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(RentabilidadeSerieQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private RentabilidadeSerieQuery query;
	}



	[Serializable]
	public partial class RentabilidadeSerieQuery : esRentabilidadeSerieQuery
	{
		public RentabilidadeSerieQuery()
		{

		}		
		
		public RentabilidadeSerieQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class RentabilidadeSerieMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected RentabilidadeSerieMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(RentabilidadeSerieMetadata.ColumnNames.IdRentabilidadeSerie, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = RentabilidadeSerieMetadata.PropertyNames.IdRentabilidadeSerie;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeSerieMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = RentabilidadeSerieMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeSerieMetadata.ColumnNames.IdFundo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = RentabilidadeSerieMetadata.PropertyNames.IdFundo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeSerieMetadata.ColumnNames.IdSerie, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = RentabilidadeSerieMetadata.PropertyNames.IdSerie;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeSerieMetadata.ColumnNames.QuantidadeSerie, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeSerieMetadata.PropertyNames.QuantidadeSerie;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeSerieMetadata.ColumnNames.Rentabilidade, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeSerieMetadata.PropertyNames.Rentabilidade;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeSerieMetadata.ColumnNames.RentabilidadeAcumuluda, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeSerieMetadata.PropertyNames.RentabilidadeAcumuluda;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public RentabilidadeSerieMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdRentabilidadeSerie = "IdRentabilidadeSerie";
			 public const string Data = "Data";
			 public const string IdFundo = "IdFundo";
			 public const string IdSerie = "IdSerie";
			 public const string QuantidadeSerie = "QuantidadeSerie";
			 public const string Rentabilidade = "Rentabilidade";
			 public const string RentabilidadeAcumuluda = "RentabilidadeAcumuluda";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdRentabilidadeSerie = "IdRentabilidadeSerie";
			 public const string Data = "Data";
			 public const string IdFundo = "IdFundo";
			 public const string IdSerie = "IdSerie";
			 public const string QuantidadeSerie = "QuantidadeSerie";
			 public const string Rentabilidade = "Rentabilidade";
			 public const string RentabilidadeAcumuluda = "RentabilidadeAcumuluda";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(RentabilidadeSerieMetadata))
			{
				if(RentabilidadeSerieMetadata.mapDelegates == null)
				{
					RentabilidadeSerieMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (RentabilidadeSerieMetadata.meta == null)
				{
					RentabilidadeSerieMetadata.meta = new RentabilidadeSerieMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdRentabilidadeSerie", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdSerie", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QuantidadeSerie", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Rentabilidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RentabilidadeAcumuluda", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "RentabilidadeSerie";
				meta.Destination = "RentabilidadeSerie";
				
				meta.spInsert = "proc_RentabilidadeSerieInsert";				
				meta.spUpdate = "proc_RentabilidadeSerieUpdate";		
				meta.spDelete = "proc_RentabilidadeSerieDelete";
				meta.spLoadAll = "proc_RentabilidadeSerieLoadAll";
				meta.spLoadByPrimaryKey = "proc_RentabilidadeSerieLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private RentabilidadeSerieMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
