/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:21 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTabelaEscalonamentoAdmCollection : esEntityCollection
	{
		public esTabelaEscalonamentoAdmCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaEscalonamentoAdmCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaEscalonamentoAdmQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaEscalonamentoAdmQuery);
		}
		#endregion
		
		virtual public TabelaEscalonamentoAdm DetachEntity(TabelaEscalonamentoAdm entity)
		{
			return base.DetachEntity(entity) as TabelaEscalonamentoAdm;
		}
		
		virtual public TabelaEscalonamentoAdm AttachEntity(TabelaEscalonamentoAdm entity)
		{
			return base.AttachEntity(entity) as TabelaEscalonamentoAdm;
		}
		
		virtual public void Combine(TabelaEscalonamentoAdmCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaEscalonamentoAdm this[int index]
		{
			get
			{
				return base[index] as TabelaEscalonamentoAdm;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaEscalonamentoAdm);
		}
	}



	[Serializable]
	abstract public class esTabelaEscalonamentoAdm : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaEscalonamentoAdmQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaEscalonamentoAdm()
		{

		}

		public esTabelaEscalonamentoAdm(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabela, System.Decimal faixaPL)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela, faixaPL);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela, faixaPL);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTabela, System.Decimal faixaPL)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaEscalonamentoAdmQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTabela == idTabela, query.FaixaPL == faixaPL);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabela, System.Decimal faixaPL)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela, faixaPL);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela, faixaPL);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabela, System.Decimal faixaPL)
		{
			esTabelaEscalonamentoAdmQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabela == idTabela, query.FaixaPL == faixaPL);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabela, System.Decimal faixaPL)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabela",idTabela);			parms.Add("FaixaPL",faixaPL);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "FaixaPL": this.str.FaixaPL = (string)value; break;							
						case "Percentual": this.str.Percentual = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "FaixaPL":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.FaixaPL = (System.Decimal?)value;
							break;
						
						case "Percentual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Percentual = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaEscalonamentoAdm.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(TabelaEscalonamentoAdmMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaEscalonamentoAdmMetadata.ColumnNames.IdTabela, value))
				{
					this._UpToTabelaTaxaAdministracaoByIdTabela = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaEscalonamentoAdm.FaixaPL
		/// </summary>
		virtual public System.Decimal? FaixaPL
		{
			get
			{
				return base.GetSystemDecimal(TabelaEscalonamentoAdmMetadata.ColumnNames.FaixaPL);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaEscalonamentoAdmMetadata.ColumnNames.FaixaPL, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaEscalonamentoAdm.Percentual
		/// </summary>
		virtual public System.Decimal? Percentual
		{
			get
			{
				return base.GetSystemDecimal(TabelaEscalonamentoAdmMetadata.ColumnNames.Percentual);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaEscalonamentoAdmMetadata.ColumnNames.Percentual, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected TabelaTaxaAdministracao _UpToTabelaTaxaAdministracaoByIdTabela;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaEscalonamentoAdm entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String FaixaPL
			{
				get
				{
					System.Decimal? data = entity.FaixaPL;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FaixaPL = null;
					else entity.FaixaPL = Convert.ToDecimal(value);
				}
			}
				
			public System.String Percentual
			{
				get
				{
					System.Decimal? data = entity.Percentual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Percentual = null;
					else entity.Percentual = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaEscalonamentoAdm entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaEscalonamentoAdmQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaEscalonamentoAdm can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaEscalonamentoAdm : esTabelaEscalonamentoAdm
	{

				
		#region UpToTabelaTaxaAdministracaoByIdTabela - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_TabelaTaxaAdministracao_TabelaEscalonamentoAdm_1
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaAdministracao UpToTabelaTaxaAdministracaoByIdTabela
		{
			get
			{
				if(this._UpToTabelaTaxaAdministracaoByIdTabela == null
					&& IdTabela != null					)
				{
					this._UpToTabelaTaxaAdministracaoByIdTabela = new TabelaTaxaAdministracao();
					this._UpToTabelaTaxaAdministracaoByIdTabela.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTabelaTaxaAdministracaoByIdTabela", this._UpToTabelaTaxaAdministracaoByIdTabela);
					this._UpToTabelaTaxaAdministracaoByIdTabela.Query.Where(this._UpToTabelaTaxaAdministracaoByIdTabela.Query.IdTabela == this.IdTabela);
					this._UpToTabelaTaxaAdministracaoByIdTabela.Query.Load();
				}

				return this._UpToTabelaTaxaAdministracaoByIdTabela;
			}
			
			set
			{
				this.RemovePreSave("UpToTabelaTaxaAdministracaoByIdTabela");
				

				if(value == null)
				{
					this.IdTabela = null;
					this._UpToTabelaTaxaAdministracaoByIdTabela = null;
				}
				else
				{
					this.IdTabela = value.IdTabela;
					this._UpToTabelaTaxaAdministracaoByIdTabela = value;
					this.SetPreSave("UpToTabelaTaxaAdministracaoByIdTabela", this._UpToTabelaTaxaAdministracaoByIdTabela);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTabelaTaxaAdministracaoByIdTabela != null)
			{
				this.IdTabela = this._UpToTabelaTaxaAdministracaoByIdTabela.IdTabela;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaEscalonamentoAdmQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaEscalonamentoAdmMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, TabelaEscalonamentoAdmMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FaixaPL
		{
			get
			{
				return new esQueryItem(this, TabelaEscalonamentoAdmMetadata.ColumnNames.FaixaPL, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Percentual
		{
			get
			{
				return new esQueryItem(this, TabelaEscalonamentoAdmMetadata.ColumnNames.Percentual, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaEscalonamentoAdmCollection")]
	public partial class TabelaEscalonamentoAdmCollection : esTabelaEscalonamentoAdmCollection, IEnumerable<TabelaEscalonamentoAdm>
	{
		public TabelaEscalonamentoAdmCollection()
		{

		}
		
		public static implicit operator List<TabelaEscalonamentoAdm>(TabelaEscalonamentoAdmCollection coll)
		{
			List<TabelaEscalonamentoAdm> list = new List<TabelaEscalonamentoAdm>();
			
			foreach (TabelaEscalonamentoAdm emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaEscalonamentoAdmMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaEscalonamentoAdmQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaEscalonamentoAdm(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaEscalonamentoAdm();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaEscalonamentoAdmQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaEscalonamentoAdmQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaEscalonamentoAdmQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaEscalonamentoAdm AddNew()
		{
			TabelaEscalonamentoAdm entity = base.AddNewEntity() as TabelaEscalonamentoAdm;
			
			return entity;
		}

		public TabelaEscalonamentoAdm FindByPrimaryKey(System.Int32 idTabela, System.Decimal faixaPL)
		{
			return base.FindByPrimaryKey(idTabela, faixaPL) as TabelaEscalonamentoAdm;
		}


		#region IEnumerable<TabelaEscalonamentoAdm> Members

		IEnumerator<TabelaEscalonamentoAdm> IEnumerable<TabelaEscalonamentoAdm>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaEscalonamentoAdm;
			}
		}

		#endregion
		
		private TabelaEscalonamentoAdmQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaEscalonamentoAdm' table
	/// </summary>

	[Serializable]
	public partial class TabelaEscalonamentoAdm : esTabelaEscalonamentoAdm
	{
		public TabelaEscalonamentoAdm()
		{

		}
	
		public TabelaEscalonamentoAdm(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaEscalonamentoAdmMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaEscalonamentoAdmQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaEscalonamentoAdmQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaEscalonamentoAdmQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaEscalonamentoAdmQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaEscalonamentoAdmQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaEscalonamentoAdmQuery query;
	}



	[Serializable]
	public partial class TabelaEscalonamentoAdmQuery : esTabelaEscalonamentoAdmQuery
	{
		public TabelaEscalonamentoAdmQuery()
		{

		}		
		
		public TabelaEscalonamentoAdmQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaEscalonamentoAdmMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaEscalonamentoAdmMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaEscalonamentoAdmMetadata.ColumnNames.IdTabela, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaEscalonamentoAdmMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaEscalonamentoAdmMetadata.ColumnNames.FaixaPL, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaEscalonamentoAdmMetadata.PropertyNames.FaixaPL;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaEscalonamentoAdmMetadata.ColumnNames.Percentual, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaEscalonamentoAdmMetadata.PropertyNames.Percentual;	
			c.NumericPrecision = 16;
			c.NumericScale = 6;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaEscalonamentoAdmMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string FaixaPL = "FaixaPL";
			 public const string Percentual = "Percentual";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string FaixaPL = "FaixaPL";
			 public const string Percentual = "Percentual";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaEscalonamentoAdmMetadata))
			{
				if(TabelaEscalonamentoAdmMetadata.mapDelegates == null)
				{
					TabelaEscalonamentoAdmMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaEscalonamentoAdmMetadata.meta == null)
				{
					TabelaEscalonamentoAdmMetadata.meta = new TabelaEscalonamentoAdmMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FaixaPL", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Percentual", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaEscalonamentoAdm";
				meta.Destination = "TabelaEscalonamentoAdm";
				
				meta.spInsert = "proc_TabelaEscalonamentoAdmInsert";				
				meta.spUpdate = "proc_TabelaEscalonamentoAdmUpdate";		
				meta.spDelete = "proc_TabelaEscalonamentoAdmDelete";
				meta.spLoadAll = "proc_TabelaEscalonamentoAdmLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaEscalonamentoAdmLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaEscalonamentoAdmMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
