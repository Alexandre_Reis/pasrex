/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/03/2015 16:07:13
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esRentabilidadeFundoOffShoreCollection : esEntityCollection
	{
		public esRentabilidadeFundoOffShoreCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "RentabilidadeFundoOffShoreCollection";
		}

		#region Query Logic
		protected void InitQuery(esRentabilidadeFundoOffShoreQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esRentabilidadeFundoOffShoreQuery);
		}
		#endregion
		
		virtual public RentabilidadeFundoOffShore DetachEntity(RentabilidadeFundoOffShore entity)
		{
			return base.DetachEntity(entity) as RentabilidadeFundoOffShore;
		}
		
		virtual public RentabilidadeFundoOffShore AttachEntity(RentabilidadeFundoOffShore entity)
		{
			return base.AttachEntity(entity) as RentabilidadeFundoOffShore;
		}
		
		virtual public void Combine(RentabilidadeFundoOffShoreCollection collection)
		{
			base.Combine(collection);
		}
		
		new public RentabilidadeFundoOffShore this[int index]
		{
			get
			{
				return base[index] as RentabilidadeFundoOffShore;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(RentabilidadeFundoOffShore);
		}
	}



	[Serializable]
	abstract public class esRentabilidadeFundoOffShore : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esRentabilidadeFundoOffShoreQuery GetDynamicQuery()
		{
			return null;
		}

		public esRentabilidadeFundoOffShore()
		{

		}

		public esRentabilidadeFundoOffShore(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idRentabilidadeFundoOffShore)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idRentabilidadeFundoOffShore);
			else
				return LoadByPrimaryKeyStoredProcedure(idRentabilidadeFundoOffShore);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idRentabilidadeFundoOffShore)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idRentabilidadeFundoOffShore);
			else
				return LoadByPrimaryKeyStoredProcedure(idRentabilidadeFundoOffShore);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idRentabilidadeFundoOffShore)
		{
			esRentabilidadeFundoOffShoreQuery query = this.GetDynamicQuery();
			query.Where(query.IdRentabilidadeFundoOffShore == idRentabilidadeFundoOffShore);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idRentabilidadeFundoOffShore)
		{
			esParameters parms = new esParameters();
			parms.Add("IdRentabilidadeFundoOffShore",idRentabilidadeFundoOffShore);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdRentabilidadeFundoOffShore": this.str.IdRentabilidadeFundoOffShore = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "IdFundo": this.str.IdFundo = (string)value; break;							
						case "Patrimonio": this.str.Patrimonio = (string)value; break;							
						case "ValorCota": this.str.ValorCota = (string)value; break;							
						case "PatrimonioAnterior": this.str.PatrimonioAnterior = (string)value; break;							
						case "ValorCotaAnterior": this.str.ValorCotaAnterior = (string)value; break;							
						case "Rentabilidade": this.str.Rentabilidade = (string)value; break;							
						case "RentabilidadePercentual": this.str.RentabilidadePercentual = (string)value; break;							
						case "RentabilidadeAcumuluda": this.str.RentabilidadeAcumuluda = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdRentabilidadeFundoOffShore":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdRentabilidadeFundoOffShore = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdFundo = (System.Int32?)value;
							break;
						
						case "Patrimonio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Patrimonio = (System.Decimal?)value;
							break;
						
						case "ValorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCota = (System.Decimal?)value;
							break;
						
						case "PatrimonioAnterior":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PatrimonioAnterior = (System.Decimal?)value;
							break;
						
						case "ValorCotaAnterior":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCotaAnterior = (System.Decimal?)value;
							break;
						
						case "Rentabilidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Rentabilidade = (System.Decimal?)value;
							break;
						
						case "RentabilidadePercentual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RentabilidadePercentual = (System.Decimal?)value;
							break;
						
						case "RentabilidadeAcumuluda":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RentabilidadeAcumuluda = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to RentabilidadeFundoOffShore.IdRentabilidadeFundoOffShore
		/// </summary>
		virtual public System.Int32? IdRentabilidadeFundoOffShore
		{
			get
			{
				return base.GetSystemInt32(RentabilidadeFundoOffShoreMetadata.ColumnNames.IdRentabilidadeFundoOffShore);
			}
			
			set
			{
				base.SetSystemInt32(RentabilidadeFundoOffShoreMetadata.ColumnNames.IdRentabilidadeFundoOffShore, value);
			}
		}
		
		/// <summary>
		/// Maps to RentabilidadeFundoOffShore.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(RentabilidadeFundoOffShoreMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(RentabilidadeFundoOffShoreMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to RentabilidadeFundoOffShore.IdFundo
		/// </summary>
		virtual public System.Int32? IdFundo
		{
			get
			{
				return base.GetSystemInt32(RentabilidadeFundoOffShoreMetadata.ColumnNames.IdFundo);
			}
			
			set
			{
				base.SetSystemInt32(RentabilidadeFundoOffShoreMetadata.ColumnNames.IdFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to RentabilidadeFundoOffShore.Patrimonio
		/// </summary>
		virtual public System.Decimal? Patrimonio
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeFundoOffShoreMetadata.ColumnNames.Patrimonio);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeFundoOffShoreMetadata.ColumnNames.Patrimonio, value);
			}
		}
		
		/// <summary>
		/// Maps to RentabilidadeFundoOffShore.ValorCota
		/// </summary>
		virtual public System.Decimal? ValorCota
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeFundoOffShoreMetadata.ColumnNames.ValorCota);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeFundoOffShoreMetadata.ColumnNames.ValorCota, value);
			}
		}
		
		/// <summary>
		/// Maps to RentabilidadeFundoOffShore.PatrimonioAnterior
		/// </summary>
		virtual public System.Decimal? PatrimonioAnterior
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeFundoOffShoreMetadata.ColumnNames.PatrimonioAnterior);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeFundoOffShoreMetadata.ColumnNames.PatrimonioAnterior, value);
			}
		}
		
		/// <summary>
		/// Maps to RentabilidadeFundoOffShore.ValorCotaAnterior
		/// </summary>
		virtual public System.Decimal? ValorCotaAnterior
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeFundoOffShoreMetadata.ColumnNames.ValorCotaAnterior);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeFundoOffShoreMetadata.ColumnNames.ValorCotaAnterior, value);
			}
		}
		
		/// <summary>
		/// Maps to RentabilidadeFundoOffShore.Rentabilidade
		/// </summary>
		virtual public System.Decimal? Rentabilidade
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeFundoOffShoreMetadata.ColumnNames.Rentabilidade);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeFundoOffShoreMetadata.ColumnNames.Rentabilidade, value);
			}
		}
		
		/// <summary>
		/// Maps to RentabilidadeFundoOffShore.RentabilidadePercentual
		/// </summary>
		virtual public System.Decimal? RentabilidadePercentual
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeFundoOffShoreMetadata.ColumnNames.RentabilidadePercentual);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeFundoOffShoreMetadata.ColumnNames.RentabilidadePercentual, value);
			}
		}
		
		/// <summary>
		/// Maps to RentabilidadeFundoOffShore.RentabilidadeAcumuluda
		/// </summary>
		virtual public System.Decimal? RentabilidadeAcumuluda
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeFundoOffShoreMetadata.ColumnNames.RentabilidadeAcumuluda);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeFundoOffShoreMetadata.ColumnNames.RentabilidadeAcumuluda, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esRentabilidadeFundoOffShore entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdRentabilidadeFundoOffShore
			{
				get
				{
					System.Int32? data = entity.IdRentabilidadeFundoOffShore;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdRentabilidadeFundoOffShore = null;
					else entity.IdRentabilidadeFundoOffShore = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdFundo
			{
				get
				{
					System.Int32? data = entity.IdFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdFundo = null;
					else entity.IdFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String Patrimonio
			{
				get
				{
					System.Decimal? data = entity.Patrimonio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Patrimonio = null;
					else entity.Patrimonio = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCota
			{
				get
				{
					System.Decimal? data = entity.ValorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCota = null;
					else entity.ValorCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String PatrimonioAnterior
			{
				get
				{
					System.Decimal? data = entity.PatrimonioAnterior;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PatrimonioAnterior = null;
					else entity.PatrimonioAnterior = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCotaAnterior
			{
				get
				{
					System.Decimal? data = entity.ValorCotaAnterior;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCotaAnterior = null;
					else entity.ValorCotaAnterior = Convert.ToDecimal(value);
				}
			}
				
			public System.String Rentabilidade
			{
				get
				{
					System.Decimal? data = entity.Rentabilidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Rentabilidade = null;
					else entity.Rentabilidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String RentabilidadePercentual
			{
				get
				{
					System.Decimal? data = entity.RentabilidadePercentual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RentabilidadePercentual = null;
					else entity.RentabilidadePercentual = Convert.ToDecimal(value);
				}
			}
				
			public System.String RentabilidadeAcumuluda
			{
				get
				{
					System.Decimal? data = entity.RentabilidadeAcumuluda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RentabilidadeAcumuluda = null;
					else entity.RentabilidadeAcumuluda = Convert.ToDecimal(value);
				}
			}
			

			private esRentabilidadeFundoOffShore entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esRentabilidadeFundoOffShoreQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esRentabilidadeFundoOffShore can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class RentabilidadeFundoOffShore : esRentabilidadeFundoOffShore
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esRentabilidadeFundoOffShoreQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return RentabilidadeFundoOffShoreMetadata.Meta();
			}
		}	
		

		public esQueryItem IdRentabilidadeFundoOffShore
		{
			get
			{
				return new esQueryItem(this, RentabilidadeFundoOffShoreMetadata.ColumnNames.IdRentabilidadeFundoOffShore, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, RentabilidadeFundoOffShoreMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdFundo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeFundoOffShoreMetadata.ColumnNames.IdFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Patrimonio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeFundoOffShoreMetadata.ColumnNames.Patrimonio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCota
		{
			get
			{
				return new esQueryItem(this, RentabilidadeFundoOffShoreMetadata.ColumnNames.ValorCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PatrimonioAnterior
		{
			get
			{
				return new esQueryItem(this, RentabilidadeFundoOffShoreMetadata.ColumnNames.PatrimonioAnterior, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCotaAnterior
		{
			get
			{
				return new esQueryItem(this, RentabilidadeFundoOffShoreMetadata.ColumnNames.ValorCotaAnterior, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Rentabilidade
		{
			get
			{
				return new esQueryItem(this, RentabilidadeFundoOffShoreMetadata.ColumnNames.Rentabilidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RentabilidadePercentual
		{
			get
			{
				return new esQueryItem(this, RentabilidadeFundoOffShoreMetadata.ColumnNames.RentabilidadePercentual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RentabilidadeAcumuluda
		{
			get
			{
				return new esQueryItem(this, RentabilidadeFundoOffShoreMetadata.ColumnNames.RentabilidadeAcumuluda, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("RentabilidadeFundoOffShoreCollection")]
	public partial class RentabilidadeFundoOffShoreCollection : esRentabilidadeFundoOffShoreCollection, IEnumerable<RentabilidadeFundoOffShore>
	{
		public RentabilidadeFundoOffShoreCollection()
		{

		}
		
		public static implicit operator List<RentabilidadeFundoOffShore>(RentabilidadeFundoOffShoreCollection coll)
		{
			List<RentabilidadeFundoOffShore> list = new List<RentabilidadeFundoOffShore>();
			
			foreach (RentabilidadeFundoOffShore emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  RentabilidadeFundoOffShoreMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new RentabilidadeFundoOffShoreQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new RentabilidadeFundoOffShore(row);
		}

		override protected esEntity CreateEntity()
		{
			return new RentabilidadeFundoOffShore();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public RentabilidadeFundoOffShoreQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new RentabilidadeFundoOffShoreQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(RentabilidadeFundoOffShoreQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public RentabilidadeFundoOffShore AddNew()
		{
			RentabilidadeFundoOffShore entity = base.AddNewEntity() as RentabilidadeFundoOffShore;
			
			return entity;
		}

		public RentabilidadeFundoOffShore FindByPrimaryKey(System.Int32 idRentabilidadeFundoOffShore)
		{
			return base.FindByPrimaryKey(idRentabilidadeFundoOffShore) as RentabilidadeFundoOffShore;
		}


		#region IEnumerable<RentabilidadeFundoOffShore> Members

		IEnumerator<RentabilidadeFundoOffShore> IEnumerable<RentabilidadeFundoOffShore>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as RentabilidadeFundoOffShore;
			}
		}

		#endregion
		
		private RentabilidadeFundoOffShoreQuery query;
	}


	/// <summary>
	/// Encapsulates the 'RentabilidadeFundoOffShore' table
	/// </summary>

	[Serializable]
	public partial class RentabilidadeFundoOffShore : esRentabilidadeFundoOffShore
	{
		public RentabilidadeFundoOffShore()
		{

		}
	
		public RentabilidadeFundoOffShore(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return RentabilidadeFundoOffShoreMetadata.Meta();
			}
		}
		
		
		
		override protected esRentabilidadeFundoOffShoreQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new RentabilidadeFundoOffShoreQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public RentabilidadeFundoOffShoreQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new RentabilidadeFundoOffShoreQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(RentabilidadeFundoOffShoreQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private RentabilidadeFundoOffShoreQuery query;
	}



	[Serializable]
	public partial class RentabilidadeFundoOffShoreQuery : esRentabilidadeFundoOffShoreQuery
	{
		public RentabilidadeFundoOffShoreQuery()
		{

		}		
		
		public RentabilidadeFundoOffShoreQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class RentabilidadeFundoOffShoreMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected RentabilidadeFundoOffShoreMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(RentabilidadeFundoOffShoreMetadata.ColumnNames.IdRentabilidadeFundoOffShore, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = RentabilidadeFundoOffShoreMetadata.PropertyNames.IdRentabilidadeFundoOffShore;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeFundoOffShoreMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = RentabilidadeFundoOffShoreMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeFundoOffShoreMetadata.ColumnNames.IdFundo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = RentabilidadeFundoOffShoreMetadata.PropertyNames.IdFundo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeFundoOffShoreMetadata.ColumnNames.Patrimonio, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeFundoOffShoreMetadata.PropertyNames.Patrimonio;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeFundoOffShoreMetadata.ColumnNames.ValorCota, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeFundoOffShoreMetadata.PropertyNames.ValorCota;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeFundoOffShoreMetadata.ColumnNames.PatrimonioAnterior, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeFundoOffShoreMetadata.PropertyNames.PatrimonioAnterior;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeFundoOffShoreMetadata.ColumnNames.ValorCotaAnterior, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeFundoOffShoreMetadata.PropertyNames.ValorCotaAnterior;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeFundoOffShoreMetadata.ColumnNames.Rentabilidade, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeFundoOffShoreMetadata.PropertyNames.Rentabilidade;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeFundoOffShoreMetadata.ColumnNames.RentabilidadePercentual, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeFundoOffShoreMetadata.PropertyNames.RentabilidadePercentual;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeFundoOffShoreMetadata.ColumnNames.RentabilidadeAcumuluda, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeFundoOffShoreMetadata.PropertyNames.RentabilidadeAcumuluda;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public RentabilidadeFundoOffShoreMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdRentabilidadeFundoOffShore = "IdRentabilidadeFundoOffShore";
			 public const string Data = "Data";
			 public const string IdFundo = "IdFundo";
			 public const string Patrimonio = "Patrimonio";
			 public const string ValorCota = "ValorCota";
			 public const string PatrimonioAnterior = "PatrimonioAnterior";
			 public const string ValorCotaAnterior = "ValorCotaAnterior";
			 public const string Rentabilidade = "Rentabilidade";
			 public const string RentabilidadePercentual = "RentabilidadePercentual";
			 public const string RentabilidadeAcumuluda = "RentabilidadeAcumuluda";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdRentabilidadeFundoOffShore = "IdRentabilidadeFundoOffShore";
			 public const string Data = "Data";
			 public const string IdFundo = "IdFundo";
			 public const string Patrimonio = "Patrimonio";
			 public const string ValorCota = "ValorCota";
			 public const string PatrimonioAnterior = "PatrimonioAnterior";
			 public const string ValorCotaAnterior = "ValorCotaAnterior";
			 public const string Rentabilidade = "Rentabilidade";
			 public const string RentabilidadePercentual = "RentabilidadePercentual";
			 public const string RentabilidadeAcumuluda = "RentabilidadeAcumuluda";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(RentabilidadeFundoOffShoreMetadata))
			{
				if(RentabilidadeFundoOffShoreMetadata.mapDelegates == null)
				{
					RentabilidadeFundoOffShoreMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (RentabilidadeFundoOffShoreMetadata.meta == null)
				{
					RentabilidadeFundoOffShoreMetadata.meta = new RentabilidadeFundoOffShoreMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdRentabilidadeFundoOffShore", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Patrimonio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PatrimonioAnterior", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCotaAnterior", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Rentabilidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RentabilidadePercentual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RentabilidadeAcumuluda", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "RentabilidadeFundoOffShore";
				meta.Destination = "RentabilidadeFundoOffShore";
				
				meta.spInsert = "proc_RentabilidadeFundoOffShoreInsert";				
				meta.spUpdate = "proc_RentabilidadeFundoOffShoreUpdate";		
				meta.spDelete = "proc_RentabilidadeFundoOffShoreDelete";
				meta.spLoadAll = "proc_RentabilidadeFundoOffShoreLoadAll";
				meta.spLoadByPrimaryKey = "proc_RentabilidadeFundoOffShoreLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private RentabilidadeFundoOffShoreMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
