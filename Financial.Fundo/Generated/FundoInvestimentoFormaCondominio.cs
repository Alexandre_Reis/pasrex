/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 05/01/2015 17:25:21
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esFundoInvestimentoFormaCondominioCollection : esEntityCollection
	{
		public esFundoInvestimentoFormaCondominioCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "FundoInvestimentoFormaCondominioCollection";
		}

		#region Query Logic
		protected void InitQuery(esFundoInvestimentoFormaCondominioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esFundoInvestimentoFormaCondominioQuery);
		}
		#endregion
		
		virtual public FundoInvestimentoFormaCondominio DetachEntity(FundoInvestimentoFormaCondominio entity)
		{
			return base.DetachEntity(entity) as FundoInvestimentoFormaCondominio;
		}
		
		virtual public FundoInvestimentoFormaCondominio AttachEntity(FundoInvestimentoFormaCondominio entity)
		{
			return base.AttachEntity(entity) as FundoInvestimentoFormaCondominio;
		}
		
		virtual public void Combine(FundoInvestimentoFormaCondominioCollection collection)
		{
			base.Combine(collection);
		}
		
		new public FundoInvestimentoFormaCondominio this[int index]
		{
			get
			{
				return base[index] as FundoInvestimentoFormaCondominio;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(FundoInvestimentoFormaCondominio);
		}
	}



	[Serializable]
	abstract public class esFundoInvestimentoFormaCondominio : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esFundoInvestimentoFormaCondominioQuery GetDynamicQuery()
		{
			return null;
		}

		public esFundoInvestimentoFormaCondominio()
		{

		}

		public esFundoInvestimentoFormaCondominio(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idFormaCondominio)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idFormaCondominio);
			else
				return LoadByPrimaryKeyStoredProcedure(idFormaCondominio);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idFormaCondominio)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idFormaCondominio);
			else
				return LoadByPrimaryKeyStoredProcedure(idFormaCondominio);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idFormaCondominio)
		{
			esFundoInvestimentoFormaCondominioQuery query = this.GetDynamicQuery();
			query.Where(query.IdFormaCondominio == idFormaCondominio);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idFormaCondominio)
		{
			esParameters parms = new esParameters();
			parms.Add("IdFormaCondominio",idFormaCondominio);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdFormaCondominio": this.str.IdFormaCondominio = (string)value; break;							
						case "DataInicioVigencia": this.str.DataInicioVigencia = (string)value; break;							
						case "FormaCondominio": this.str.FormaCondominio = (string)value; break;							
						case "ExecucaoRecolhimento": this.str.ExecucaoRecolhimento = (string)value; break;							
						case "AliquotaIR": this.str.AliquotaIR = (string)value; break;							
						case "DataRecolhimento": this.str.DataRecolhimento = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdFormaCondominio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdFormaCondominio = (System.Int32?)value;
							break;
						
						case "DataInicioVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicioVigencia = (System.DateTime?)value;
							break;
						
						case "FormaCondominio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FormaCondominio = (System.Int32?)value;
							break;
						
						case "ExecucaoRecolhimento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.ExecucaoRecolhimento = (System.Int32?)value;
							break;
						
						case "AliquotaIR":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.AliquotaIR = (System.Int32?)value;
							break;
						
						case "DataRecolhimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRecolhimento = (System.DateTime?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to FundoInvestimentoFormaCondominio.IdFormaCondominio
		/// </summary>
		virtual public System.Int32? IdFormaCondominio
		{
			get
			{
				return base.GetSystemInt32(FundoInvestimentoFormaCondominioMetadata.ColumnNames.IdFormaCondominio);
			}
			
			set
			{
				base.SetSystemInt32(FundoInvestimentoFormaCondominioMetadata.ColumnNames.IdFormaCondominio, value);
			}
		}
		
		/// <summary>
		/// Maps to FundoInvestimentoFormaCondominio.DataInicioVigencia
		/// </summary>
		virtual public System.DateTime? DataInicioVigencia
		{
			get
			{
				return base.GetSystemDateTime(FundoInvestimentoFormaCondominioMetadata.ColumnNames.DataInicioVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(FundoInvestimentoFormaCondominioMetadata.ColumnNames.DataInicioVigencia, value);
			}
		}
		
		/// <summary>
		/// Maps to FundoInvestimentoFormaCondominio.FormaCondominio
		/// </summary>
		virtual public System.Int32? FormaCondominio
		{
			get
			{
				return base.GetSystemInt32(FundoInvestimentoFormaCondominioMetadata.ColumnNames.FormaCondominio);
			}
			
			set
			{
				base.SetSystemInt32(FundoInvestimentoFormaCondominioMetadata.ColumnNames.FormaCondominio, value);
			}
		}
		
		/// <summary>
		/// Maps to FundoInvestimentoFormaCondominio.ExecucaoRecolhimento
		/// </summary>
		virtual public System.Int32? ExecucaoRecolhimento
		{
			get
			{
				return base.GetSystemInt32(FundoInvestimentoFormaCondominioMetadata.ColumnNames.ExecucaoRecolhimento);
			}
			
			set
			{
				base.SetSystemInt32(FundoInvestimentoFormaCondominioMetadata.ColumnNames.ExecucaoRecolhimento, value);
			}
		}
		
		/// <summary>
		/// Maps to FundoInvestimentoFormaCondominio.AliquotaIR
		/// </summary>
		virtual public System.Int32? AliquotaIR
		{
			get
			{
				return base.GetSystemInt32(FundoInvestimentoFormaCondominioMetadata.ColumnNames.AliquotaIR);
			}
			
			set
			{
				base.SetSystemInt32(FundoInvestimentoFormaCondominioMetadata.ColumnNames.AliquotaIR, value);
			}
		}
		
		/// <summary>
		/// Maps to FundoInvestimentoFormaCondominio.DataRecolhimento
		/// </summary>
		virtual public System.DateTime? DataRecolhimento
		{
			get
			{
				return base.GetSystemDateTime(FundoInvestimentoFormaCondominioMetadata.ColumnNames.DataRecolhimento);
			}
			
			set
			{
				base.SetSystemDateTime(FundoInvestimentoFormaCondominioMetadata.ColumnNames.DataRecolhimento, value);
			}
		}
		
		/// <summary>
		/// Maps to FundoInvestimentoFormaCondominio.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(FundoInvestimentoFormaCondominioMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(FundoInvestimentoFormaCondominioMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esFundoInvestimentoFormaCondominio entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdFormaCondominio
			{
				get
				{
					System.Int32? data = entity.IdFormaCondominio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdFormaCondominio = null;
					else entity.IdFormaCondominio = Convert.ToInt32(value);
				}
			}
				
			public System.String DataInicioVigencia
			{
				get
				{
					System.DateTime? data = entity.DataInicioVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicioVigencia = null;
					else entity.DataInicioVigencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String FormaCondominio
			{
				get
				{
					System.Int32? data = entity.FormaCondominio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FormaCondominio = null;
					else entity.FormaCondominio = Convert.ToInt32(value);
				}
			}
				
			public System.String ExecucaoRecolhimento
			{
				get
				{
					System.Int32? data = entity.ExecucaoRecolhimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExecucaoRecolhimento = null;
					else entity.ExecucaoRecolhimento = Convert.ToInt32(value);
				}
			}
				
			public System.String AliquotaIR
			{
				get
				{
					System.Int32? data = entity.AliquotaIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AliquotaIR = null;
					else entity.AliquotaIR = Convert.ToInt32(value);
				}
			}
				
			public System.String DataRecolhimento
			{
				get
				{
					System.DateTime? data = entity.DataRecolhimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRecolhimento = null;
					else entity.DataRecolhimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
			

			private esFundoInvestimentoFormaCondominio entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esFundoInvestimentoFormaCondominioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esFundoInvestimentoFormaCondominio can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class FundoInvestimentoFormaCondominio : esFundoInvestimentoFormaCondominio
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Foreign Key Name - FK__FundoInve__Fundo__160526E6
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esFundoInvestimentoFormaCondominioQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return FundoInvestimentoFormaCondominioMetadata.Meta();
			}
		}	
		

		public esQueryItem IdFormaCondominio
		{
			get
			{
				return new esQueryItem(this, FundoInvestimentoFormaCondominioMetadata.ColumnNames.IdFormaCondominio, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataInicioVigencia
		{
			get
			{
				return new esQueryItem(this, FundoInvestimentoFormaCondominioMetadata.ColumnNames.DataInicioVigencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FormaCondominio
		{
			get
			{
				return new esQueryItem(this, FundoInvestimentoFormaCondominioMetadata.ColumnNames.FormaCondominio, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ExecucaoRecolhimento
		{
			get
			{
				return new esQueryItem(this, FundoInvestimentoFormaCondominioMetadata.ColumnNames.ExecucaoRecolhimento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem AliquotaIR
		{
			get
			{
				return new esQueryItem(this, FundoInvestimentoFormaCondominioMetadata.ColumnNames.AliquotaIR, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataRecolhimento
		{
			get
			{
				return new esQueryItem(this, FundoInvestimentoFormaCondominioMetadata.ColumnNames.DataRecolhimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, FundoInvestimentoFormaCondominioMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("FundoInvestimentoFormaCondominioCollection")]
	public partial class FundoInvestimentoFormaCondominioCollection : esFundoInvestimentoFormaCondominioCollection, IEnumerable<FundoInvestimentoFormaCondominio>
	{
		public FundoInvestimentoFormaCondominioCollection()
		{

		}
		
		public static implicit operator List<FundoInvestimentoFormaCondominio>(FundoInvestimentoFormaCondominioCollection coll)
		{
			List<FundoInvestimentoFormaCondominio> list = new List<FundoInvestimentoFormaCondominio>();
			
			foreach (FundoInvestimentoFormaCondominio emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  FundoInvestimentoFormaCondominioMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new FundoInvestimentoFormaCondominioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new FundoInvestimentoFormaCondominio(row);
		}

		override protected esEntity CreateEntity()
		{
			return new FundoInvestimentoFormaCondominio();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public FundoInvestimentoFormaCondominioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new FundoInvestimentoFormaCondominioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(FundoInvestimentoFormaCondominioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public FundoInvestimentoFormaCondominio AddNew()
		{
			FundoInvestimentoFormaCondominio entity = base.AddNewEntity() as FundoInvestimentoFormaCondominio;
			
			return entity;
		}

		public FundoInvestimentoFormaCondominio FindByPrimaryKey(System.Int32 idFormaCondominio)
		{
			return base.FindByPrimaryKey(idFormaCondominio) as FundoInvestimentoFormaCondominio;
		}


		#region IEnumerable<FundoInvestimentoFormaCondominio> Members

		IEnumerator<FundoInvestimentoFormaCondominio> IEnumerable<FundoInvestimentoFormaCondominio>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as FundoInvestimentoFormaCondominio;
			}
		}

		#endregion
		
		private FundoInvestimentoFormaCondominioQuery query;
	}


	/// <summary>
	/// Encapsulates the 'FundoInvestimentoFormaCondominio' table
	/// </summary>

	[Serializable]
	public partial class FundoInvestimentoFormaCondominio : esFundoInvestimentoFormaCondominio
	{
		public FundoInvestimentoFormaCondominio()
		{

		}
	
		public FundoInvestimentoFormaCondominio(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return FundoInvestimentoFormaCondominioMetadata.Meta();
			}
		}
		
		
		
		override protected esFundoInvestimentoFormaCondominioQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new FundoInvestimentoFormaCondominioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public FundoInvestimentoFormaCondominioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new FundoInvestimentoFormaCondominioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(FundoInvestimentoFormaCondominioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private FundoInvestimentoFormaCondominioQuery query;
	}



	[Serializable]
	public partial class FundoInvestimentoFormaCondominioQuery : esFundoInvestimentoFormaCondominioQuery
	{
		public FundoInvestimentoFormaCondominioQuery()
		{

		}		
		
		public FundoInvestimentoFormaCondominioQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class FundoInvestimentoFormaCondominioMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected FundoInvestimentoFormaCondominioMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(FundoInvestimentoFormaCondominioMetadata.ColumnNames.IdFormaCondominio, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = FundoInvestimentoFormaCondominioMetadata.PropertyNames.IdFormaCondominio;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FundoInvestimentoFormaCondominioMetadata.ColumnNames.DataInicioVigencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = FundoInvestimentoFormaCondominioMetadata.PropertyNames.DataInicioVigencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FundoInvestimentoFormaCondominioMetadata.ColumnNames.FormaCondominio, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = FundoInvestimentoFormaCondominioMetadata.PropertyNames.FormaCondominio;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FundoInvestimentoFormaCondominioMetadata.ColumnNames.ExecucaoRecolhimento, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = FundoInvestimentoFormaCondominioMetadata.PropertyNames.ExecucaoRecolhimento;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FundoInvestimentoFormaCondominioMetadata.ColumnNames.AliquotaIR, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = FundoInvestimentoFormaCondominioMetadata.PropertyNames.AliquotaIR;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FundoInvestimentoFormaCondominioMetadata.ColumnNames.DataRecolhimento, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = FundoInvestimentoFormaCondominioMetadata.PropertyNames.DataRecolhimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FundoInvestimentoFormaCondominioMetadata.ColumnNames.IdCarteira, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = FundoInvestimentoFormaCondominioMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public FundoInvestimentoFormaCondominioMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdFormaCondominio = "IdFormaCondominio";
			 public const string DataInicioVigencia = "DataInicioVigencia";
			 public const string FormaCondominio = "FormaCondominio";
			 public const string ExecucaoRecolhimento = "ExecucaoRecolhimento";
			 public const string AliquotaIR = "AliquotaIR";
			 public const string DataRecolhimento = "DataRecolhimento";
			 public const string IdCarteira = "IdCarteira";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdFormaCondominio = "IdFormaCondominio";
			 public const string DataInicioVigencia = "DataInicioVigencia";
			 public const string FormaCondominio = "FormaCondominio";
			 public const string ExecucaoRecolhimento = "ExecucaoRecolhimento";
			 public const string AliquotaIR = "AliquotaIR";
			 public const string DataRecolhimento = "DataRecolhimento";
			 public const string IdCarteira = "IdCarteira";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(FundoInvestimentoFormaCondominioMetadata))
			{
				if(FundoInvestimentoFormaCondominioMetadata.mapDelegates == null)
				{
					FundoInvestimentoFormaCondominioMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (FundoInvestimentoFormaCondominioMetadata.meta == null)
				{
					FundoInvestimentoFormaCondominioMetadata.meta = new FundoInvestimentoFormaCondominioMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdFormaCondominio", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataInicioVigencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FormaCondominio", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ExecucaoRecolhimento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AliquotaIR", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataRecolhimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "FundoInvestimentoFormaCondominio";
				meta.Destination = "FundoInvestimentoFormaCondominio";
				
				meta.spInsert = "proc_FundoInvestimentoFormaCondominioInsert";				
				meta.spUpdate = "proc_FundoInvestimentoFormaCondominioUpdate";		
				meta.spDelete = "proc_FundoInvestimentoFormaCondominioDelete";
				meta.spLoadAll = "proc_FundoInvestimentoFormaCondominioLoadAll";
				meta.spLoadByPrimaryKey = "proc_FundoInvestimentoFormaCondominioLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private FundoInvestimentoFormaCondominioMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
