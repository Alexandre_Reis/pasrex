/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 28/03/2016 17:28:10
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esPosicaoFundoCollection : esEntityCollection
	{
		public esPosicaoFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoFundoQuery);
		}
		#endregion
		
		virtual public PosicaoFundo DetachEntity(PosicaoFundo entity)
		{
			return base.DetachEntity(entity) as PosicaoFundo;
		}
		
		virtual public PosicaoFundo AttachEntity(PosicaoFundo entity)
		{
			return base.AttachEntity(entity) as PosicaoFundo;
		}
		
		virtual public void Combine(PosicaoFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoFundo this[int index]
		{
			get
			{
				return base[index] as PosicaoFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoFundo);
		}
	}



	[Serializable]
	abstract public class esPosicaoFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoFundo()
		{

		}

		public esPosicaoFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao)
		{
			esPosicaoFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "ValorAplicacao": this.str.ValorAplicacao = (string)value; break;							
						case "DataAplicacao": this.str.DataAplicacao = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "CotaAplicacao": this.str.CotaAplicacao = (string)value; break;							
						case "CotaDia": this.str.CotaDia = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "QuantidadeInicial": this.str.QuantidadeInicial = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeBloqueada": this.str.QuantidadeBloqueada = (string)value; break;							
						case "DataUltimaCobrancaIR": this.str.DataUltimaCobrancaIR = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "ValorIOFVirtual": this.str.ValorIOFVirtual = (string)value; break;							
						case "QuantidadeAntesCortes": this.str.QuantidadeAntesCortes = (string)value; break;							
						case "ValorRendimento": this.str.ValorRendimento = (string)value; break;							
						case "DataUltimoCortePfee": this.str.DataUltimoCortePfee = (string)value; break;							
						case "PosicaoIncorporada": this.str.PosicaoIncorporada = (string)value; break;							
						case "FieTabelaIr": this.str.FieTabelaIr = (string)value; break;							
						case "QtdePendenteLiquidacao": this.str.QtdePendenteLiquidacao = (string)value; break;							
						case "ValorPendenteLiquidacao": this.str.ValorPendenteLiquidacao = (string)value; break;							
						case "AmortizacaoAcumuladaPorCota": this.str.AmortizacaoAcumuladaPorCota = (string)value; break;							
						case "JurosAcumuladoPorCota": this.str.JurosAcumuladoPorCota = (string)value; break;							
						case "AmortizacaoAcumuladaPorValor": this.str.AmortizacaoAcumuladaPorValor = (string)value; break;							
						case "JurosAcumuladoPorValor": this.str.JurosAcumuladoPorValor = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "ValorAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAplicacao = (System.Decimal?)value;
							break;
						
						case "DataAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAplicacao = (System.DateTime?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "CotaAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaAplicacao = (System.Decimal?)value;
							break;
						
						case "CotaDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaDia = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "QuantidadeInicial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeInicial = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "QuantidadeBloqueada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeBloqueada = (System.Decimal?)value;
							break;
						
						case "DataUltimaCobrancaIR":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimaCobrancaIR = (System.DateTime?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "ValorIOFVirtual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOFVirtual = (System.Decimal?)value;
							break;
						
						case "QuantidadeAntesCortes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeAntesCortes = (System.Decimal?)value;
							break;
						
						case "ValorRendimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorRendimento = (System.Decimal?)value;
							break;
						
						case "DataUltimoCortePfee":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimoCortePfee = (System.DateTime?)value;
							break;
						
						case "FieTabelaIr":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FieTabelaIr = (System.Int32?)value;
							break;
						
						case "QtdePendenteLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdePendenteLiquidacao = (System.Decimal?)value;
							break;
						
						case "ValorPendenteLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPendenteLiquidacao = (System.Decimal?)value;
							break;
						
						case "AmortizacaoAcumuladaPorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AmortizacaoAcumuladaPorCota = (System.Decimal?)value;
							break;
						
						case "JurosAcumuladoPorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.JurosAcumuladoPorCota = (System.Decimal?)value;
							break;
						
						case "AmortizacaoAcumuladaPorValor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AmortizacaoAcumuladaPorValor = (System.Decimal?)value;
							break;
						
						case "JurosAcumuladoPorValor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.JurosAcumuladoPorValor = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoFundo.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoFundoMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoFundoMetadata.ColumnNames.IdOperacao, value))
				{
					this._UpToOperacaoFundoByIdOperacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoFundoMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoFundoMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.ValorAplicacao
		/// </summary>
		virtual public System.Decimal? ValorAplicacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorAplicacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.DataAplicacao
		/// </summary>
		virtual public System.DateTime? DataAplicacao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoFundoMetadata.ColumnNames.DataAplicacao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoFundoMetadata.ColumnNames.DataAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoFundoMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoFundoMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.CotaAplicacao
		/// </summary>
		virtual public System.Decimal? CotaAplicacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.CotaAplicacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.CotaAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.CotaDia
		/// </summary>
		virtual public System.Decimal? CotaDia
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.CotaDia);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.CotaDia, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.QuantidadeInicial
		/// </summary>
		virtual public System.Decimal? QuantidadeInicial
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.QuantidadeInicial);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.QuantidadeInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.QuantidadeBloqueada
		/// </summary>
		virtual public System.Decimal? QuantidadeBloqueada
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.QuantidadeBloqueada);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.QuantidadeBloqueada, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.DataUltimaCobrancaIR
		/// </summary>
		virtual public System.DateTime? DataUltimaCobrancaIR
		{
			get
			{
				return base.GetSystemDateTime(PosicaoFundoMetadata.ColumnNames.DataUltimaCobrancaIR);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoFundoMetadata.ColumnNames.DataUltimaCobrancaIR, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.ValorIOFVirtual
		/// </summary>
		virtual public System.Decimal? ValorIOFVirtual
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorIOFVirtual);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorIOFVirtual, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.QuantidadeAntesCortes
		/// </summary>
		virtual public System.Decimal? QuantidadeAntesCortes
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.QuantidadeAntesCortes);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.QuantidadeAntesCortes, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.ValorRendimento
		/// </summary>
		virtual public System.Decimal? ValorRendimento
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorRendimento);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorRendimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.DataUltimoCortePfee
		/// </summary>
		virtual public System.DateTime? DataUltimoCortePfee
		{
			get
			{
				return base.GetSystemDateTime(PosicaoFundoMetadata.ColumnNames.DataUltimoCortePfee);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoFundoMetadata.ColumnNames.DataUltimoCortePfee, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.PosicaoIncorporada
		/// </summary>
		virtual public System.String PosicaoIncorporada
		{
			get
			{
				return base.GetSystemString(PosicaoFundoMetadata.ColumnNames.PosicaoIncorporada);
			}
			
			set
			{
				base.SetSystemString(PosicaoFundoMetadata.ColumnNames.PosicaoIncorporada, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.FieTabelaIr
		/// </summary>
		virtual public System.Int32? FieTabelaIr
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoMetadata.ColumnNames.FieTabelaIr);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoFundoMetadata.ColumnNames.FieTabelaIr, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.QtdePendenteLiquidacao
		/// </summary>
		virtual public System.Decimal? QtdePendenteLiquidacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.QtdePendenteLiquidacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.QtdePendenteLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.ValorPendenteLiquidacao
		/// </summary>
		virtual public System.Decimal? ValorPendenteLiquidacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorPendenteLiquidacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.ValorPendenteLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.AmortizacaoAcumuladaPorCota
		/// </summary>
		virtual public System.Decimal? AmortizacaoAcumuladaPorCota
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.AmortizacaoAcumuladaPorCota);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.AmortizacaoAcumuladaPorCota, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.JurosAcumuladoPorCota
		/// </summary>
		virtual public System.Decimal? JurosAcumuladoPorCota
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.JurosAcumuladoPorCota);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.JurosAcumuladoPorCota, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.AmortizacaoAcumuladaPorValor
		/// </summary>
		virtual public System.Decimal? AmortizacaoAcumuladaPorValor
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.AmortizacaoAcumuladaPorValor);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.AmortizacaoAcumuladaPorValor, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundo.JurosAcumuladoPorValor
		/// </summary>
		virtual public System.Decimal? JurosAcumuladoPorValor
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoMetadata.ColumnNames.JurosAcumuladoPorValor);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoMetadata.ColumnNames.JurosAcumuladoPorValor, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected OperacaoFundo _UpToOperacaoFundoByIdOperacao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorAplicacao
			{
				get
				{
					System.Decimal? data = entity.ValorAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAplicacao = null;
					else entity.ValorAplicacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataAplicacao
			{
				get
				{
					System.DateTime? data = entity.DataAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAplicacao = null;
					else entity.DataAplicacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String CotaAplicacao
			{
				get
				{
					System.Decimal? data = entity.CotaAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaAplicacao = null;
					else entity.CotaAplicacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaDia
			{
				get
				{
					System.Decimal? data = entity.CotaDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaDia = null;
					else entity.CotaDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeInicial
			{
				get
				{
					System.Decimal? data = entity.QuantidadeInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeInicial = null;
					else entity.QuantidadeInicial = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeBloqueada
			{
				get
				{
					System.Decimal? data = entity.QuantidadeBloqueada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeBloqueada = null;
					else entity.QuantidadeBloqueada = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataUltimaCobrancaIR
			{
				get
				{
					System.DateTime? data = entity.DataUltimaCobrancaIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimaCobrancaIR = null;
					else entity.DataUltimaCobrancaIR = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOFVirtual
			{
				get
				{
					System.Decimal? data = entity.ValorIOFVirtual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOFVirtual = null;
					else entity.ValorIOFVirtual = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeAntesCortes
			{
				get
				{
					System.Decimal? data = entity.QuantidadeAntesCortes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeAntesCortes = null;
					else entity.QuantidadeAntesCortes = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorRendimento
			{
				get
				{
					System.Decimal? data = entity.ValorRendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorRendimento = null;
					else entity.ValorRendimento = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataUltimoCortePfee
			{
				get
				{
					System.DateTime? data = entity.DataUltimoCortePfee;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimoCortePfee = null;
					else entity.DataUltimoCortePfee = Convert.ToDateTime(value);
				}
			}
				
			public System.String PosicaoIncorporada
			{
				get
				{
					System.String data = entity.PosicaoIncorporada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PosicaoIncorporada = null;
					else entity.PosicaoIncorporada = Convert.ToString(value);
				}
			}
				
			public System.String FieTabelaIr
			{
				get
				{
					System.Int32? data = entity.FieTabelaIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FieTabelaIr = null;
					else entity.FieTabelaIr = Convert.ToInt32(value);
				}
			}
				
			public System.String QtdePendenteLiquidacao
			{
				get
				{
					System.Decimal? data = entity.QtdePendenteLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdePendenteLiquidacao = null;
					else entity.QtdePendenteLiquidacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPendenteLiquidacao
			{
				get
				{
					System.Decimal? data = entity.ValorPendenteLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPendenteLiquidacao = null;
					else entity.ValorPendenteLiquidacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String AmortizacaoAcumuladaPorCota
			{
				get
				{
					System.Decimal? data = entity.AmortizacaoAcumuladaPorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AmortizacaoAcumuladaPorCota = null;
					else entity.AmortizacaoAcumuladaPorCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String JurosAcumuladoPorCota
			{
				get
				{
					System.Decimal? data = entity.JurosAcumuladoPorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.JurosAcumuladoPorCota = null;
					else entity.JurosAcumuladoPorCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String AmortizacaoAcumuladaPorValor
			{
				get
				{
					System.Decimal? data = entity.AmortizacaoAcumuladaPorValor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AmortizacaoAcumuladaPorValor = null;
					else entity.AmortizacaoAcumuladaPorValor = Convert.ToDecimal(value);
				}
			}
				
			public System.String JurosAcumuladoPorValor
			{
				get
				{
					System.Decimal? data = entity.JurosAcumuladoPorValor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.JurosAcumuladoPorValor = null;
					else entity.JurosAcumuladoPorValor = Convert.ToDecimal(value);
				}
			}
			

			private esPosicaoFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoFundo : esPosicaoFundo
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_PosicaoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PosicaoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToOperacaoFundoByIdOperacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Operacao_PosicaoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoFundo UpToOperacaoFundoByIdOperacao
		{
			get
			{
				if(this._UpToOperacaoFundoByIdOperacao == null
					&& IdOperacao != null					)
				{
					this._UpToOperacaoFundoByIdOperacao = new OperacaoFundo();
					this._UpToOperacaoFundoByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOperacaoFundoByIdOperacao", this._UpToOperacaoFundoByIdOperacao);
					this._UpToOperacaoFundoByIdOperacao.Query.Where(this._UpToOperacaoFundoByIdOperacao.Query.IdOperacao == this.IdOperacao);
					this._UpToOperacaoFundoByIdOperacao.Query.Load();
				}

				return this._UpToOperacaoFundoByIdOperacao;
			}
			
			set
			{
				this.RemovePreSave("UpToOperacaoFundoByIdOperacao");
				

				if(value == null)
				{
					this.IdOperacao = null;
					this._UpToOperacaoFundoByIdOperacao = null;
				}
				else
				{
					this.IdOperacao = value.IdOperacao;
					this._UpToOperacaoFundoByIdOperacao = value;
					this.SetPreSave("UpToOperacaoFundoByIdOperacao", this._UpToOperacaoFundoByIdOperacao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToOperacaoFundoByIdOperacao != null)
			{
				this.IdOperacao = this._UpToOperacaoFundoByIdOperacao.IdOperacao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorAplicacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.ValorAplicacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataAplicacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.DataAplicacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CotaAplicacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.CotaAplicacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaDia
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.CotaDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeInicial
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.QuantidadeInicial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeBloqueada
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.QuantidadeBloqueada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataUltimaCobrancaIR
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.DataUltimaCobrancaIR, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOFVirtual
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.ValorIOFVirtual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeAntesCortes
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.QuantidadeAntesCortes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorRendimento
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.ValorRendimento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataUltimoCortePfee
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.DataUltimoCortePfee, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PosicaoIncorporada
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.PosicaoIncorporada, esSystemType.String);
			}
		} 
		
		public esQueryItem FieTabelaIr
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.FieTabelaIr, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QtdePendenteLiquidacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.QtdePendenteLiquidacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPendenteLiquidacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.ValorPendenteLiquidacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AmortizacaoAcumuladaPorCota
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.AmortizacaoAcumuladaPorCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem JurosAcumuladoPorCota
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.JurosAcumuladoPorCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AmortizacaoAcumuladaPorValor
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.AmortizacaoAcumuladaPorValor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem JurosAcumuladoPorValor
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoMetadata.ColumnNames.JurosAcumuladoPorValor, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PosicaoFundoCollection")]
	public partial class PosicaoFundoCollection : esPosicaoFundoCollection, IEnumerable<PosicaoFundo>
	{
		public PosicaoFundoCollection()
		{

		}
		
		public static implicit operator List<PosicaoFundo>(PosicaoFundoCollection coll)
		{
			List<PosicaoFundo> list = new List<PosicaoFundo>();
			
			foreach (PosicaoFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoFundo AddNew()
		{
			PosicaoFundo entity = base.AddNewEntity() as PosicaoFundo;
			
			return entity;
		}

		public PosicaoFundo FindByPrimaryKey(System.Int32 idPosicao)
		{
			return base.FindByPrimaryKey(idPosicao) as PosicaoFundo;
		}


		#region IEnumerable<PosicaoFundo> Members

		IEnumerator<PosicaoFundo> IEnumerable<PosicaoFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoFundo;
			}
		}

		#endregion
		
		private PosicaoFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoFundo' table
	/// </summary>

	[Serializable]
	public partial class PosicaoFundo : esPosicaoFundo
	{
		public PosicaoFundo()
		{

		}
	
		public PosicaoFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoFundoQuery query;
	}



	[Serializable]
	public partial class PosicaoFundoQuery : esPosicaoFundoQuery
	{
		public PosicaoFundoQuery()
		{

		}		
		
		public PosicaoFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.IdOperacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.IdCarteira, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.ValorAplicacao, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.ValorAplicacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.DataAplicacao, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.DataAplicacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.DataConversao, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.CotaAplicacao, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.CotaAplicacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.CotaDia, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.CotaDia;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.ValorBruto, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.ValorLiquido, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.QuantidadeInicial, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.QuantidadeInicial;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.Quantidade, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.QuantidadeBloqueada, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.QuantidadeBloqueada;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.DataUltimaCobrancaIR, 14, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.DataUltimaCobrancaIR;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.ValorIR, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.ValorIOF, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.ValorPerformance, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.ValorIOFVirtual, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.ValorIOFVirtual;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.QuantidadeAntesCortes, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.QuantidadeAntesCortes;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.ValorRendimento, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.ValorRendimento;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.DataUltimoCortePfee, 21, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.DataUltimoCortePfee;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.PosicaoIncorporada, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.PosicaoIncorporada;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.FieTabelaIr, 23, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.FieTabelaIr;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.QtdePendenteLiquidacao, 24, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.QtdePendenteLiquidacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.ValorPendenteLiquidacao, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.ValorPendenteLiquidacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.AmortizacaoAcumuladaPorCota, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.AmortizacaoAcumuladaPorCota;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.JurosAcumuladoPorCota, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.JurosAcumuladoPorCota;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.AmortizacaoAcumuladaPorValor, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.AmortizacaoAcumuladaPorValor;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoMetadata.ColumnNames.JurosAcumuladoPorValor, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoMetadata.PropertyNames.JurosAcumuladoPorValor;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PosicaoFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorAplicacao = "ValorAplicacao";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string DataConversao = "DataConversao";
			 public const string CotaAplicacao = "CotaAplicacao";
			 public const string CotaDia = "CotaDia";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string DataUltimaCobrancaIR = "DataUltimaCobrancaIR";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string ValorIOFVirtual = "ValorIOFVirtual";
			 public const string QuantidadeAntesCortes = "QuantidadeAntesCortes";
			 public const string ValorRendimento = "ValorRendimento";
			 public const string DataUltimoCortePfee = "DataUltimoCortePfee";
			 public const string PosicaoIncorporada = "PosicaoIncorporada";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string QtdePendenteLiquidacao = "QtdePendenteLiquidacao";
			 public const string ValorPendenteLiquidacao = "ValorPendenteLiquidacao";
			 public const string AmortizacaoAcumuladaPorCota = "AmortizacaoAcumuladaPorCota";
			 public const string JurosAcumuladoPorCota = "JurosAcumuladoPorCota";
			 public const string AmortizacaoAcumuladaPorValor = "AmortizacaoAcumuladaPorValor";
			 public const string JurosAcumuladoPorValor = "JurosAcumuladoPorValor";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorAplicacao = "ValorAplicacao";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string DataConversao = "DataConversao";
			 public const string CotaAplicacao = "CotaAplicacao";
			 public const string CotaDia = "CotaDia";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string DataUltimaCobrancaIR = "DataUltimaCobrancaIR";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string ValorIOFVirtual = "ValorIOFVirtual";
			 public const string QuantidadeAntesCortes = "QuantidadeAntesCortes";
			 public const string ValorRendimento = "ValorRendimento";
			 public const string DataUltimoCortePfee = "DataUltimoCortePfee";
			 public const string PosicaoIncorporada = "PosicaoIncorporada";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string QtdePendenteLiquidacao = "QtdePendenteLiquidacao";
			 public const string ValorPendenteLiquidacao = "ValorPendenteLiquidacao";
			 public const string AmortizacaoAcumuladaPorCota = "AmortizacaoAcumuladaPorCota";
			 public const string JurosAcumuladoPorCota = "JurosAcumuladoPorCota";
			 public const string AmortizacaoAcumuladaPorValor = "AmortizacaoAcumuladaPorValor";
			 public const string JurosAcumuladoPorValor = "JurosAcumuladoPorValor";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoFundoMetadata))
			{
				if(PosicaoFundoMetadata.mapDelegates == null)
				{
					PosicaoFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoFundoMetadata.meta == null)
				{
					PosicaoFundoMetadata.meta = new PosicaoFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorAplicacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataAplicacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CotaAplicacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeInicial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeBloqueada", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataUltimaCobrancaIR", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOFVirtual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeAntesCortes", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorRendimento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataUltimoCortePfee", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PosicaoIncorporada", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FieTabelaIr", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QtdePendenteLiquidacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPendenteLiquidacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AmortizacaoAcumuladaPorCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("JurosAcumuladoPorCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AmortizacaoAcumuladaPorValor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("JurosAcumuladoPorValor", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "PosicaoFundo";
				meta.Destination = "PosicaoFundo";
				
				meta.spInsert = "proc_PosicaoFundoInsert";				
				meta.spUpdate = "proc_PosicaoFundoUpdate";		
				meta.spDelete = "proc_PosicaoFundoDelete";
				meta.spLoadAll = "proc_PosicaoFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
