/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 22/12/2014 17:49:41
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTipoParametroLiqAvancadoCollection : esEntityCollection
	{
		public esTipoParametroLiqAvancadoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TipoParametroLiqAvancadoCollection";
		}

		#region Query Logic
		protected void InitQuery(esTipoParametroLiqAvancadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTipoParametroLiqAvancadoQuery);
		}
		#endregion
		
		virtual public TipoParametroLiqAvancado DetachEntity(TipoParametroLiqAvancado entity)
		{
			return base.DetachEntity(entity) as TipoParametroLiqAvancado;
		}
		
		virtual public TipoParametroLiqAvancado AttachEntity(TipoParametroLiqAvancado entity)
		{
			return base.AttachEntity(entity) as TipoParametroLiqAvancado;
		}
		
		virtual public void Combine(TipoParametroLiqAvancadoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TipoParametroLiqAvancado this[int index]
		{
			get
			{
				return base[index] as TipoParametroLiqAvancado;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TipoParametroLiqAvancado);
		}
	}



	[Serializable]
	abstract public class esTipoParametroLiqAvancado : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTipoParametroLiqAvancadoQuery GetDynamicQuery()
		{
			return null;
		}

		public esTipoParametroLiqAvancado()
		{

		}

		public esTipoParametroLiqAvancado(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String idParametroLiqAvancado)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idParametroLiqAvancado);
			else
				return LoadByPrimaryKeyStoredProcedure(idParametroLiqAvancado);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String idParametroLiqAvancado)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idParametroLiqAvancado);
			else
				return LoadByPrimaryKeyStoredProcedure(idParametroLiqAvancado);
		}

		private bool LoadByPrimaryKeyDynamic(System.String idParametroLiqAvancado)
		{
			esTipoParametroLiqAvancadoQuery query = this.GetDynamicQuery();
			query.Where(query.IdParametroLiqAvancado == idParametroLiqAvancado);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String idParametroLiqAvancado)
		{
			esParameters parms = new esParameters();
			parms.Add("IdParametroLiqAvancado",idParametroLiqAvancado);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdParametroLiqAvancado": this.str.IdParametroLiqAvancado = (string)value; break;							
						case "SignificadoParametro": this.str.SignificadoParametro = (string)value; break;							
						case "VariacaoComplemento": this.str.VariacaoComplemento = (string)value; break;							
						case "SignificadoComplemento": this.str.SignificadoComplemento = (string)value; break;							
						case "Exemplo": this.str.Exemplo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TipoParametroLiqAvancado.IdParametroLiqAvancado
		/// </summary>
		virtual public System.String IdParametroLiqAvancado
		{
			get
			{
				return base.GetSystemString(TipoParametroLiqAvancadoMetadata.ColumnNames.IdParametroLiqAvancado);
			}
			
			set
			{
				base.SetSystemString(TipoParametroLiqAvancadoMetadata.ColumnNames.IdParametroLiqAvancado, value);
			}
		}
		
		/// <summary>
		/// Maps to TipoParametroLiqAvancado.SignificadoParametro
		/// </summary>
		virtual public System.String SignificadoParametro
		{
			get
			{
				return base.GetSystemString(TipoParametroLiqAvancadoMetadata.ColumnNames.SignificadoParametro);
			}
			
			set
			{
				base.SetSystemString(TipoParametroLiqAvancadoMetadata.ColumnNames.SignificadoParametro, value);
			}
		}
		
		/// <summary>
		/// Maps to TipoParametroLiqAvancado.VariacaoComplemento
		/// </summary>
		virtual public System.String VariacaoComplemento
		{
			get
			{
				return base.GetSystemString(TipoParametroLiqAvancadoMetadata.ColumnNames.VariacaoComplemento);
			}
			
			set
			{
				base.SetSystemString(TipoParametroLiqAvancadoMetadata.ColumnNames.VariacaoComplemento, value);
			}
		}
		
		/// <summary>
		/// Maps to TipoParametroLiqAvancado.SignificadoComplemento
		/// </summary>
		virtual public System.String SignificadoComplemento
		{
			get
			{
				return base.GetSystemString(TipoParametroLiqAvancadoMetadata.ColumnNames.SignificadoComplemento);
			}
			
			set
			{
				base.SetSystemString(TipoParametroLiqAvancadoMetadata.ColumnNames.SignificadoComplemento, value);
			}
		}
		
		/// <summary>
		/// Maps to TipoParametroLiqAvancado.Exemplo
		/// </summary>
		virtual public System.String Exemplo
		{
			get
			{
				return base.GetSystemString(TipoParametroLiqAvancadoMetadata.ColumnNames.Exemplo);
			}
			
			set
			{
				base.SetSystemString(TipoParametroLiqAvancadoMetadata.ColumnNames.Exemplo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTipoParametroLiqAvancado entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdParametroLiqAvancado
			{
				get
				{
					System.String data = entity.IdParametroLiqAvancado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdParametroLiqAvancado = null;
					else entity.IdParametroLiqAvancado = Convert.ToString(value);
				}
			}
				
			public System.String SignificadoParametro
			{
				get
				{
					System.String data = entity.SignificadoParametro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SignificadoParametro = null;
					else entity.SignificadoParametro = Convert.ToString(value);
				}
			}
				
			public System.String VariacaoComplemento
			{
				get
				{
					System.String data = entity.VariacaoComplemento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VariacaoComplemento = null;
					else entity.VariacaoComplemento = Convert.ToString(value);
				}
			}
				
			public System.String SignificadoComplemento
			{
				get
				{
					System.String data = entity.SignificadoComplemento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SignificadoComplemento = null;
					else entity.SignificadoComplemento = Convert.ToString(value);
				}
			}
				
			public System.String Exemplo
			{
				get
				{
					System.String data = entity.Exemplo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Exemplo = null;
					else entity.Exemplo = Convert.ToString(value);
				}
			}
			

			private esTipoParametroLiqAvancado entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTipoParametroLiqAvancadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTipoParametroLiqAvancado can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TipoParametroLiqAvancado : esTipoParametroLiqAvancado
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTipoParametroLiqAvancadoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TipoParametroLiqAvancadoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdParametroLiqAvancado
		{
			get
			{
				return new esQueryItem(this, TipoParametroLiqAvancadoMetadata.ColumnNames.IdParametroLiqAvancado, esSystemType.String);
			}
		} 
		
		public esQueryItem SignificadoParametro
		{
			get
			{
				return new esQueryItem(this, TipoParametroLiqAvancadoMetadata.ColumnNames.SignificadoParametro, esSystemType.String);
			}
		} 
		
		public esQueryItem VariacaoComplemento
		{
			get
			{
				return new esQueryItem(this, TipoParametroLiqAvancadoMetadata.ColumnNames.VariacaoComplemento, esSystemType.String);
			}
		} 
		
		public esQueryItem SignificadoComplemento
		{
			get
			{
				return new esQueryItem(this, TipoParametroLiqAvancadoMetadata.ColumnNames.SignificadoComplemento, esSystemType.String);
			}
		} 
		
		public esQueryItem Exemplo
		{
			get
			{
				return new esQueryItem(this, TipoParametroLiqAvancadoMetadata.ColumnNames.Exemplo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TipoParametroLiqAvancadoCollection")]
	public partial class TipoParametroLiqAvancadoCollection : esTipoParametroLiqAvancadoCollection, IEnumerable<TipoParametroLiqAvancado>
	{
		public TipoParametroLiqAvancadoCollection()
		{

		}
		
		public static implicit operator List<TipoParametroLiqAvancado>(TipoParametroLiqAvancadoCollection coll)
		{
			List<TipoParametroLiqAvancado> list = new List<TipoParametroLiqAvancado>();
			
			foreach (TipoParametroLiqAvancado emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TipoParametroLiqAvancadoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TipoParametroLiqAvancadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TipoParametroLiqAvancado(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TipoParametroLiqAvancado();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TipoParametroLiqAvancadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TipoParametroLiqAvancadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TipoParametroLiqAvancadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TipoParametroLiqAvancado AddNew()
		{
			TipoParametroLiqAvancado entity = base.AddNewEntity() as TipoParametroLiqAvancado;
			
			return entity;
		}

		public TipoParametroLiqAvancado FindByPrimaryKey(System.String idParametroLiqAvancado)
		{
			return base.FindByPrimaryKey(idParametroLiqAvancado) as TipoParametroLiqAvancado;
		}


		#region IEnumerable<TipoParametroLiqAvancado> Members

		IEnumerator<TipoParametroLiqAvancado> IEnumerable<TipoParametroLiqAvancado>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TipoParametroLiqAvancado;
			}
		}

		#endregion
		
		private TipoParametroLiqAvancadoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TipoParametroLiqAvancado' table
	/// </summary>

	[Serializable]
	public partial class TipoParametroLiqAvancado : esTipoParametroLiqAvancado
	{
		public TipoParametroLiqAvancado()
		{

		}
	
		public TipoParametroLiqAvancado(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TipoParametroLiqAvancadoMetadata.Meta();
			}
		}
		
		
		
		override protected esTipoParametroLiqAvancadoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TipoParametroLiqAvancadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TipoParametroLiqAvancadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TipoParametroLiqAvancadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TipoParametroLiqAvancadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TipoParametroLiqAvancadoQuery query;
	}



	[Serializable]
	public partial class TipoParametroLiqAvancadoQuery : esTipoParametroLiqAvancadoQuery
	{
		public TipoParametroLiqAvancadoQuery()
		{

		}		
		
		public TipoParametroLiqAvancadoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TipoParametroLiqAvancadoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TipoParametroLiqAvancadoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TipoParametroLiqAvancadoMetadata.ColumnNames.IdParametroLiqAvancado, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = TipoParametroLiqAvancadoMetadata.PropertyNames.IdParametroLiqAvancado;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TipoParametroLiqAvancadoMetadata.ColumnNames.SignificadoParametro, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TipoParametroLiqAvancadoMetadata.PropertyNames.SignificadoParametro;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TipoParametroLiqAvancadoMetadata.ColumnNames.VariacaoComplemento, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TipoParametroLiqAvancadoMetadata.PropertyNames.VariacaoComplemento;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TipoParametroLiqAvancadoMetadata.ColumnNames.SignificadoComplemento, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TipoParametroLiqAvancadoMetadata.PropertyNames.SignificadoComplemento;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TipoParametroLiqAvancadoMetadata.ColumnNames.Exemplo, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = TipoParametroLiqAvancadoMetadata.PropertyNames.Exemplo;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TipoParametroLiqAvancadoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdParametroLiqAvancado = "IdParametroLiqAvancado";
			 public const string SignificadoParametro = "SignificadoParametro";
			 public const string VariacaoComplemento = "VariacaoComplemento";
			 public const string SignificadoComplemento = "SignificadoComplemento";
			 public const string Exemplo = "Exemplo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdParametroLiqAvancado = "IdParametroLiqAvancado";
			 public const string SignificadoParametro = "SignificadoParametro";
			 public const string VariacaoComplemento = "VariacaoComplemento";
			 public const string SignificadoComplemento = "SignificadoComplemento";
			 public const string Exemplo = "Exemplo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TipoParametroLiqAvancadoMetadata))
			{
				if(TipoParametroLiqAvancadoMetadata.mapDelegates == null)
				{
					TipoParametroLiqAvancadoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TipoParametroLiqAvancadoMetadata.meta == null)
				{
					TipoParametroLiqAvancadoMetadata.meta = new TipoParametroLiqAvancadoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdParametroLiqAvancado", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("SignificadoParametro", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("VariacaoComplemento", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("SignificadoComplemento", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Exemplo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "TipoParametroLiqAvancado";
				meta.Destination = "TipoParametroLiqAvancado";
				
				meta.spInsert = "proc_TipoParametroLiqAvancadoInsert";				
				meta.spUpdate = "proc_TipoParametroLiqAvancadoUpdate";		
				meta.spDelete = "proc_TipoParametroLiqAvancadoDelete";
				meta.spLoadAll = "proc_TipoParametroLiqAvancadoLoadAll";
				meta.spLoadByPrimaryKey = "proc_TipoParametroLiqAvancadoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TipoParametroLiqAvancadoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
