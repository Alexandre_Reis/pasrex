/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:08 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		







		

		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esCadastroProvisaoCollection : esEntityCollection
	{
		public esCadastroProvisaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CadastroProvisaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esCadastroProvisaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCadastroProvisaoQuery);
		}
		#endregion
		
		virtual public CadastroProvisao DetachEntity(CadastroProvisao entity)
		{
			return base.DetachEntity(entity) as CadastroProvisao;
		}
		
		virtual public CadastroProvisao AttachEntity(CadastroProvisao entity)
		{
			return base.AttachEntity(entity) as CadastroProvisao;
		}
		
		virtual public void Combine(CadastroProvisaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CadastroProvisao this[int index]
		{
			get
			{
				return base[index] as CadastroProvisao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CadastroProvisao);
		}
	}



	[Serializable]
	abstract public class esCadastroProvisao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCadastroProvisaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esCadastroProvisao()
		{

		}

		public esCadastroProvisao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCadastro)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCadastro);
			else
				return LoadByPrimaryKeyStoredProcedure(idCadastro);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCadastro)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCadastroProvisaoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCadastro == idCadastro);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCadastro)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCadastro);
			else
				return LoadByPrimaryKeyStoredProcedure(idCadastro);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCadastro)
		{
			esCadastroProvisaoQuery query = this.GetDynamicQuery();
			query.Where(query.IdCadastro == idCadastro);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCadastro)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCadastro",idCadastro);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCadastro": this.str.IdCadastro = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCadastro":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCadastro = (System.Int32?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Tipo = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CadastroProvisao.IdCadastro
		/// </summary>
		virtual public System.Int32? IdCadastro
		{
			get
			{
				return base.GetSystemInt32(CadastroProvisaoMetadata.ColumnNames.IdCadastro);
			}
			
			set
			{
				base.SetSystemInt32(CadastroProvisaoMetadata.ColumnNames.IdCadastro, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroProvisao.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(CadastroProvisaoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(CadastroProvisaoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroProvisao.Tipo
		/// </summary>
		virtual public System.Byte? Tipo
		{
			get
			{
				return base.GetSystemByte(CadastroProvisaoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemByte(CadastroProvisaoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCadastroProvisao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCadastro
			{
				get
				{
					System.Int32? data = entity.IdCadastro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCadastro = null;
					else entity.IdCadastro = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Byte? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToByte(value);
				}
			}
			

			private esCadastroProvisao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCadastroProvisaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCadastroProvisao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CadastroProvisao : esCadastroProvisao
	{

				
		#region TabelaProvisaoCollectionByIdCadastro - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - CadastroProvisao_TabelaProvisao_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaProvisaoCollection TabelaProvisaoCollectionByIdCadastro
		{
			get
			{
				if(this._TabelaProvisaoCollectionByIdCadastro == null)
				{
					this._TabelaProvisaoCollectionByIdCadastro = new TabelaProvisaoCollection();
					this._TabelaProvisaoCollectionByIdCadastro.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaProvisaoCollectionByIdCadastro", this._TabelaProvisaoCollectionByIdCadastro);
				
					if(this.IdCadastro != null)
					{
						this._TabelaProvisaoCollectionByIdCadastro.Query.Where(this._TabelaProvisaoCollectionByIdCadastro.Query.IdCadastro == this.IdCadastro);
						this._TabelaProvisaoCollectionByIdCadastro.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaProvisaoCollectionByIdCadastro.fks.Add(TabelaProvisaoMetadata.ColumnNames.IdCadastro, this.IdCadastro);
					}
				}

				return this._TabelaProvisaoCollectionByIdCadastro;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaProvisaoCollectionByIdCadastro != null) 
				{ 
					this.RemovePostSave("TabelaProvisaoCollectionByIdCadastro"); 
					this._TabelaProvisaoCollectionByIdCadastro = null;
					
				} 
			} 			
		}

		private TabelaProvisaoCollection _TabelaProvisaoCollectionByIdCadastro;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "TabelaProvisaoCollectionByIdCadastro", typeof(TabelaProvisaoCollection), new TabelaProvisao()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._TabelaProvisaoCollectionByIdCadastro != null)
			{
				foreach(TabelaProvisao obj in this._TabelaProvisaoCollectionByIdCadastro)
				{
					if(obj.es.IsAdded)
					{
						obj.IdCadastro = this.IdCadastro;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCadastroProvisaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CadastroProvisaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCadastro
		{
			get
			{
				return new esQueryItem(this, CadastroProvisaoMetadata.ColumnNames.IdCadastro, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, CadastroProvisaoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, CadastroProvisaoMetadata.ColumnNames.Tipo, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CadastroProvisaoCollection")]
	public partial class CadastroProvisaoCollection : esCadastroProvisaoCollection, IEnumerable<CadastroProvisao>
	{
		public CadastroProvisaoCollection()
		{

		}
		
		public static implicit operator List<CadastroProvisao>(CadastroProvisaoCollection coll)
		{
			List<CadastroProvisao> list = new List<CadastroProvisao>();
			
			foreach (CadastroProvisao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CadastroProvisaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CadastroProvisaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CadastroProvisao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CadastroProvisao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CadastroProvisaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CadastroProvisaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CadastroProvisaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CadastroProvisao AddNew()
		{
			CadastroProvisao entity = base.AddNewEntity() as CadastroProvisao;
			
			return entity;
		}

		public CadastroProvisao FindByPrimaryKey(System.Int32 idCadastro)
		{
			return base.FindByPrimaryKey(idCadastro) as CadastroProvisao;
		}


		#region IEnumerable<CadastroProvisao> Members

		IEnumerator<CadastroProvisao> IEnumerable<CadastroProvisao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CadastroProvisao;
			}
		}

		#endregion
		
		private CadastroProvisaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CadastroProvisao' table
	/// </summary>

	[Serializable]
	public partial class CadastroProvisao : esCadastroProvisao
	{
		public CadastroProvisao()
		{

		}
	
		public CadastroProvisao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CadastroProvisaoMetadata.Meta();
			}
		}
		
		
		
		override protected esCadastroProvisaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CadastroProvisaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CadastroProvisaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CadastroProvisaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CadastroProvisaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CadastroProvisaoQuery query;
	}



	[Serializable]
	public partial class CadastroProvisaoQuery : esCadastroProvisaoQuery
	{
		public CadastroProvisaoQuery()
		{

		}		
		
		public CadastroProvisaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CadastroProvisaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CadastroProvisaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CadastroProvisaoMetadata.ColumnNames.IdCadastro, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CadastroProvisaoMetadata.PropertyNames.IdCadastro;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroProvisaoMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CadastroProvisaoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroProvisaoMetadata.ColumnNames.Tipo, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CadastroProvisaoMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CadastroProvisaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCadastro = "IdCadastro";
			 public const string Descricao = "Descricao";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCadastro = "IdCadastro";
			 public const string Descricao = "Descricao";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CadastroProvisaoMetadata))
			{
				if(CadastroProvisaoMetadata.mapDelegates == null)
				{
					CadastroProvisaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CadastroProvisaoMetadata.meta == null)
				{
					CadastroProvisaoMetadata.meta = new CadastroProvisaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCadastro", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "CadastroProvisao";
				meta.Destination = "CadastroProvisao";
				
				meta.spInsert = "proc_CadastroProvisaoInsert";				
				meta.spUpdate = "proc_CadastroProvisaoUpdate";		
				meta.spDelete = "proc_CadastroProvisaoDelete";
				meta.spLoadAll = "proc_CadastroProvisaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_CadastroProvisaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CadastroProvisaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
