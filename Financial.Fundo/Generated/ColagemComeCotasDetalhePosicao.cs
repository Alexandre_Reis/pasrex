/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 22/01/2015 16:27:42
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esColagemComeCotasDetalhePosicaoCollection : esEntityCollection
	{
		public esColagemComeCotasDetalhePosicaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ColagemComeCotasDetalhePosicaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esColagemComeCotasDetalhePosicaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esColagemComeCotasDetalhePosicaoQuery);
		}
		#endregion
		
		virtual public ColagemComeCotasDetalhePosicao DetachEntity(ColagemComeCotasDetalhePosicao entity)
		{
			return base.DetachEntity(entity) as ColagemComeCotasDetalhePosicao;
		}
		
		virtual public ColagemComeCotasDetalhePosicao AttachEntity(ColagemComeCotasDetalhePosicao entity)
		{
			return base.AttachEntity(entity) as ColagemComeCotasDetalhePosicao;
		}
		
		virtual public void Combine(ColagemComeCotasDetalhePosicaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ColagemComeCotasDetalhePosicao this[int index]
		{
			get
			{
				return base[index] as ColagemComeCotasDetalhePosicao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ColagemComeCotasDetalhePosicao);
		}
	}



	[Serializable]
	abstract public class esColagemComeCotasDetalhePosicao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esColagemComeCotasDetalhePosicaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esColagemComeCotasDetalhePosicao()
		{

		}

		public esColagemComeCotasDetalhePosicao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idColagemComeCotasDetalhePosicao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemComeCotasDetalhePosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemComeCotasDetalhePosicao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idColagemComeCotasDetalhePosicao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemComeCotasDetalhePosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemComeCotasDetalhePosicao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idColagemComeCotasDetalhePosicao)
		{
			esColagemComeCotasDetalhePosicaoQuery query = this.GetDynamicQuery();
			query.Where(query.IdColagemComeCotasDetalhePosicao == idColagemComeCotasDetalhePosicao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idColagemComeCotasDetalhePosicao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdColagemComeCotasDetalhePosicao",idColagemComeCotasDetalhePosicao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdColagemComeCotasDetalhePosicao": this.str.IdColagemComeCotasDetalhePosicao = (string)value; break;							
						case "IdColagemComeCotasDetalhe": this.str.IdColagemComeCotasDetalhe = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "QuantidadeInicial": this.str.QuantidadeInicial = (string)value; break;							
						case "QuantidadeRestante": this.str.QuantidadeRestante = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdColagemComeCotasDetalhePosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemComeCotasDetalhePosicao = (System.Int32?)value;
							break;
						
						case "IdColagemComeCotasDetalhe":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemComeCotasDetalhe = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "QuantidadeInicial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeInicial = (System.Decimal?)value;
							break;
						
						case "QuantidadeRestante":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeRestante = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhePosicao.IdColagemComeCotasDetalhePosicao
		/// </summary>
		virtual public System.Int32? IdColagemComeCotasDetalhePosicao
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.IdColagemComeCotasDetalhePosicao);
			}
			
			set
			{
				base.SetSystemInt32(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.IdColagemComeCotasDetalhePosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhePosicao.IdColagemComeCotasDetalhe
		/// </summary>
		virtual public System.Int32? IdColagemComeCotasDetalhe
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.IdColagemComeCotasDetalhe);
			}
			
			set
			{
				if(base.SetSystemInt32(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.IdColagemComeCotasDetalhe, value))
				{
					this._UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhePosicao.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhePosicao.QuantidadeInicial
		/// </summary>
		virtual public System.Decimal? QuantidadeInicial
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.QuantidadeInicial);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.QuantidadeInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasDetalhePosicao.QuantidadeRestante
		/// </summary>
		virtual public System.Decimal? QuantidadeRestante
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.QuantidadeRestante);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.QuantidadeRestante, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected ColagemComeCotasDetalhe _UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esColagemComeCotasDetalhePosicao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdColagemComeCotasDetalhePosicao
			{
				get
				{
					System.Int32? data = entity.IdColagemComeCotasDetalhePosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemComeCotasDetalhePosicao = null;
					else entity.IdColagemComeCotasDetalhePosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdColagemComeCotasDetalhe
			{
				get
				{
					System.Int32? data = entity.IdColagemComeCotasDetalhe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemComeCotasDetalhe = null;
					else entity.IdColagemComeCotasDetalhe = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String QuantidadeInicial
			{
				get
				{
					System.Decimal? data = entity.QuantidadeInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeInicial = null;
					else entity.QuantidadeInicial = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeRestante
			{
				get
				{
					System.Decimal? data = entity.QuantidadeRestante;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeRestante = null;
					else entity.QuantidadeRestante = Convert.ToDecimal(value);
				}
			}
			

			private esColagemComeCotasDetalhePosicao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esColagemComeCotasDetalhePosicaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esColagemComeCotasDetalhePosicao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ColagemComeCotasDetalhePosicao : esColagemComeCotasDetalhePosicao
	{

				
		#region UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ColagemComeCotass_Det_Pos_FK
		/// </summary>

		[XmlIgnore]
		public ColagemComeCotasDetalhe UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe
		{
			get
			{
				if(this._UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe == null
					&& IdColagemComeCotasDetalhe != null					)
				{
					this._UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe = new ColagemComeCotasDetalhe();
					this._UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe", this._UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe);
					this._UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe.Query.Where(this._UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe.Query.IdColagemComeCotasDetalhe == this.IdColagemComeCotasDetalhe);
					this._UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe.Query.Load();
				}

				return this._UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe;
			}
			
			set
			{
				this.RemovePreSave("UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe");
				

				if(value == null)
				{
					this.IdColagemComeCotasDetalhe = null;
					this._UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe = null;
				}
				else
				{
					this.IdColagemComeCotasDetalhe = value.IdColagemComeCotasDetalhe;
					this._UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe = value;
					this.SetPreSave("UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe", this._UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe != null)
			{
				this.IdColagemComeCotasDetalhe = this._UpToColagemComeCotasDetalheByIdColagemComeCotasDetalhe.IdColagemComeCotasDetalhe;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esColagemComeCotasDetalhePosicaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ColagemComeCotasDetalhePosicaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdColagemComeCotasDetalhePosicao
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.IdColagemComeCotasDetalhePosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdColagemComeCotasDetalhe
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.IdColagemComeCotasDetalhe, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem QuantidadeInicial
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.QuantidadeInicial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeRestante
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.QuantidadeRestante, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ColagemComeCotasDetalhePosicaoCollection")]
	public partial class ColagemComeCotasDetalhePosicaoCollection : esColagemComeCotasDetalhePosicaoCollection, IEnumerable<ColagemComeCotasDetalhePosicao>
	{
		public ColagemComeCotasDetalhePosicaoCollection()
		{

		}
		
		public static implicit operator List<ColagemComeCotasDetalhePosicao>(ColagemComeCotasDetalhePosicaoCollection coll)
		{
			List<ColagemComeCotasDetalhePosicao> list = new List<ColagemComeCotasDetalhePosicao>();
			
			foreach (ColagemComeCotasDetalhePosicao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ColagemComeCotasDetalhePosicaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemComeCotasDetalhePosicaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ColagemComeCotasDetalhePosicao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ColagemComeCotasDetalhePosicao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ColagemComeCotasDetalhePosicaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemComeCotasDetalhePosicaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ColagemComeCotasDetalhePosicaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ColagemComeCotasDetalhePosicao AddNew()
		{
			ColagemComeCotasDetalhePosicao entity = base.AddNewEntity() as ColagemComeCotasDetalhePosicao;
			
			return entity;
		}

		public ColagemComeCotasDetalhePosicao FindByPrimaryKey(System.Int32 idColagemComeCotasDetalhePosicao)
		{
			return base.FindByPrimaryKey(idColagemComeCotasDetalhePosicao) as ColagemComeCotasDetalhePosicao;
		}


		#region IEnumerable<ColagemComeCotasDetalhePosicao> Members

		IEnumerator<ColagemComeCotasDetalhePosicao> IEnumerable<ColagemComeCotasDetalhePosicao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ColagemComeCotasDetalhePosicao;
			}
		}

		#endregion
		
		private ColagemComeCotasDetalhePosicaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ColagemComeCotasDetalhePosicao' table
	/// </summary>

	[Serializable]
	public partial class ColagemComeCotasDetalhePosicao : esColagemComeCotasDetalhePosicao
	{
		public ColagemComeCotasDetalhePosicao()
		{

		}
	
		public ColagemComeCotasDetalhePosicao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ColagemComeCotasDetalhePosicaoMetadata.Meta();
			}
		}
		
		
		
		override protected esColagemComeCotasDetalhePosicaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemComeCotasDetalhePosicaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ColagemComeCotasDetalhePosicaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemComeCotasDetalhePosicaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ColagemComeCotasDetalhePosicaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ColagemComeCotasDetalhePosicaoQuery query;
	}



	[Serializable]
	public partial class ColagemComeCotasDetalhePosicaoQuery : esColagemComeCotasDetalhePosicaoQuery
	{
		public ColagemComeCotasDetalhePosicaoQuery()
		{

		}		
		
		public ColagemComeCotasDetalhePosicaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ColagemComeCotasDetalhePosicaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ColagemComeCotasDetalhePosicaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.IdColagemComeCotasDetalhePosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasDetalhePosicaoMetadata.PropertyNames.IdColagemComeCotasDetalhePosicao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.IdColagemComeCotasDetalhe, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasDetalhePosicaoMetadata.PropertyNames.IdColagemComeCotasDetalhe;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.DataReferencia, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemComeCotasDetalhePosicaoMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.QuantidadeInicial, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasDetalhePosicaoMetadata.PropertyNames.QuantidadeInicial;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasDetalhePosicaoMetadata.ColumnNames.QuantidadeRestante, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasDetalhePosicaoMetadata.PropertyNames.QuantidadeRestante;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ColagemComeCotasDetalhePosicaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdColagemComeCotasDetalhePosicao = "IdColagemComeCotasDetalhePosicao";
			 public const string IdColagemComeCotasDetalhe = "IdColagemComeCotasDetalhe";
			 public const string DataReferencia = "DataReferencia";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string QuantidadeRestante = "QuantidadeRestante";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdColagemComeCotasDetalhePosicao = "IdColagemComeCotasDetalhePosicao";
			 public const string IdColagemComeCotasDetalhe = "IdColagemComeCotasDetalhe";
			 public const string DataReferencia = "DataReferencia";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string QuantidadeRestante = "QuantidadeRestante";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ColagemComeCotasDetalhePosicaoMetadata))
			{
				if(ColagemComeCotasDetalhePosicaoMetadata.mapDelegates == null)
				{
					ColagemComeCotasDetalhePosicaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ColagemComeCotasDetalhePosicaoMetadata.meta == null)
				{
					ColagemComeCotasDetalhePosicaoMetadata.meta = new ColagemComeCotasDetalhePosicaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdColagemComeCotasDetalhePosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdColagemComeCotasDetalhe", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("QuantidadeInicial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeRestante", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "ColagemComeCotasDetalhePosicao";
				meta.Destination = "ColagemComeCotasDetalhePosicao";
				
				meta.spInsert = "proc_ColagemComeCotasDetalhePosicaoInsert";				
				meta.spUpdate = "proc_ColagemComeCotasDetalhePosicaoUpdate";		
				meta.spDelete = "proc_ColagemComeCotasDetalhePosicaoDelete";
				meta.spLoadAll = "proc_ColagemComeCotasDetalhePosicaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_ColagemComeCotasDetalhePosicaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ColagemComeCotasDetalhePosicaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
