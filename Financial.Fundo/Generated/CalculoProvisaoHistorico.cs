/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:10 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		




		




		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esCalculoProvisaoHistoricoCollection : esEntityCollection
	{
		public esCalculoProvisaoHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CalculoProvisaoHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esCalculoProvisaoHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCalculoProvisaoHistoricoQuery);
		}
		#endregion
		
		virtual public CalculoProvisaoHistorico DetachEntity(CalculoProvisaoHistorico entity)
		{
			return base.DetachEntity(entity) as CalculoProvisaoHistorico;
		}
		
		virtual public CalculoProvisaoHistorico AttachEntity(CalculoProvisaoHistorico entity)
		{
			return base.AttachEntity(entity) as CalculoProvisaoHistorico;
		}
		
		virtual public void Combine(CalculoProvisaoHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CalculoProvisaoHistorico this[int index]
		{
			get
			{
				return base[index] as CalculoProvisaoHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CalculoProvisaoHistorico);
		}
	}



	[Serializable]
	abstract public class esCalculoProvisaoHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCalculoProvisaoHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esCalculoProvisaoHistorico()
		{

		}

		public esCalculoProvisaoHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idTabela)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idTabela);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataHistorico, System.Int32 idTabela)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCalculoProvisaoHistoricoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataHistorico == dataHistorico, query.IdTabela == idTabela);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idTabela)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idTabela);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idTabela)
		{
			esCalculoProvisaoHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdTabela == idTabela);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idTabela)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdTabela",idTabela);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "ValorDia": this.str.ValorDia = (string)value; break;							
						case "ValorAcumulado": this.str.ValorAcumulado = (string)value; break;							
						case "DataFimApropriacao": this.str.DataFimApropriacao = (string)value; break;							
						case "DataPagamento": this.str.DataPagamento = (string)value; break;							
						case "ValorCPMFDia": this.str.ValorCPMFDia = (string)value; break;							
						case "ValorCPMFAcumulado": this.str.ValorCPMFAcumulado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "ValorDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorDia = (System.Decimal?)value;
							break;
						
						case "ValorAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAcumulado = (System.Decimal?)value;
							break;
						
						case "DataFimApropriacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFimApropriacao = (System.DateTime?)value;
							break;
						
						case "DataPagamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPagamento = (System.DateTime?)value;
							break;
						
						case "ValorCPMFDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMFDia = (System.Decimal?)value;
							break;
						
						case "ValorCPMFAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMFAcumulado = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CalculoProvisaoHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(CalculoProvisaoHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoProvisaoHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoProvisaoHistorico.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(CalculoProvisaoHistoricoMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				if(base.SetSystemInt32(CalculoProvisaoHistoricoMetadata.ColumnNames.IdTabela, value))
				{
					this._UpToTabelaProvisaoByIdTabela = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CalculoProvisaoHistorico.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(CalculoProvisaoHistoricoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(CalculoProvisaoHistoricoMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CalculoProvisaoHistorico.ValorDia
		/// </summary>
		virtual public System.Decimal? ValorDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoProvisaoHistoricoMetadata.ColumnNames.ValorDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoProvisaoHistoricoMetadata.ColumnNames.ValorDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoProvisaoHistorico.ValorAcumulado
		/// </summary>
		virtual public System.Decimal? ValorAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoProvisaoHistoricoMetadata.ColumnNames.ValorAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoProvisaoHistoricoMetadata.ColumnNames.ValorAcumulado, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoProvisaoHistorico.DataFimApropriacao
		/// </summary>
		virtual public System.DateTime? DataFimApropriacao
		{
			get
			{
				return base.GetSystemDateTime(CalculoProvisaoHistoricoMetadata.ColumnNames.DataFimApropriacao);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoProvisaoHistoricoMetadata.ColumnNames.DataFimApropriacao, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoProvisaoHistorico.DataPagamento
		/// </summary>
		virtual public System.DateTime? DataPagamento
		{
			get
			{
				return base.GetSystemDateTime(CalculoProvisaoHistoricoMetadata.ColumnNames.DataPagamento);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoProvisaoHistoricoMetadata.ColumnNames.DataPagamento, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoProvisaoHistorico.ValorCPMFDia
		/// </summary>
		virtual public System.Decimal? ValorCPMFDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoProvisaoHistoricoMetadata.ColumnNames.ValorCPMFDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoProvisaoHistoricoMetadata.ColumnNames.ValorCPMFDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoProvisaoHistorico.ValorCPMFAcumulado
		/// </summary>
		virtual public System.Decimal? ValorCPMFAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoProvisaoHistoricoMetadata.ColumnNames.ValorCPMFAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoProvisaoHistoricoMetadata.ColumnNames.ValorCPMFAcumulado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected TabelaProvisao _UpToTabelaProvisaoByIdTabela;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCalculoProvisaoHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorDia
			{
				get
				{
					System.Decimal? data = entity.ValorDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDia = null;
					else entity.ValorDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorAcumulado
			{
				get
				{
					System.Decimal? data = entity.ValorAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAcumulado = null;
					else entity.ValorAcumulado = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataFimApropriacao
			{
				get
				{
					System.DateTime? data = entity.DataFimApropriacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFimApropriacao = null;
					else entity.DataFimApropriacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataPagamento
			{
				get
				{
					System.DateTime? data = entity.DataPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPagamento = null;
					else entity.DataPagamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorCPMFDia
			{
				get
				{
					System.Decimal? data = entity.ValorCPMFDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMFDia = null;
					else entity.ValorCPMFDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCPMFAcumulado
			{
				get
				{
					System.Decimal? data = entity.ValorCPMFAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMFAcumulado = null;
					else entity.ValorCPMFAcumulado = Convert.ToDecimal(value);
				}
			}
			

			private esCalculoProvisaoHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCalculoProvisaoHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCalculoProvisaoHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CalculoProvisaoHistorico : esCalculoProvisaoHistorico
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_CalculoProvisaoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTabelaProvisaoByIdTabela - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TabelaProvisao_CalculoProvisaoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaProvisao UpToTabelaProvisaoByIdTabela
		{
			get
			{
				if(this._UpToTabelaProvisaoByIdTabela == null
					&& IdTabela != null					)
				{
					this._UpToTabelaProvisaoByIdTabela = new TabelaProvisao();
					this._UpToTabelaProvisaoByIdTabela.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTabelaProvisaoByIdTabela", this._UpToTabelaProvisaoByIdTabela);
					this._UpToTabelaProvisaoByIdTabela.Query.Where(this._UpToTabelaProvisaoByIdTabela.Query.IdTabela == this.IdTabela);
					this._UpToTabelaProvisaoByIdTabela.Query.Load();
				}

				return this._UpToTabelaProvisaoByIdTabela;
			}
			
			set
			{
				this.RemovePreSave("UpToTabelaProvisaoByIdTabela");
				

				if(value == null)
				{
					this.IdTabela = null;
					this._UpToTabelaProvisaoByIdTabela = null;
				}
				else
				{
					this.IdTabela = value.IdTabela;
					this._UpToTabelaProvisaoByIdTabela = value;
					this.SetPreSave("UpToTabelaProvisaoByIdTabela", this._UpToTabelaProvisaoByIdTabela);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTabelaProvisaoByIdTabela != null)
			{
				this.IdTabela = this._UpToTabelaProvisaoByIdTabela.IdTabela;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCalculoProvisaoHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CalculoProvisaoHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoHistoricoMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoHistoricoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorDia
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoHistoricoMetadata.ColumnNames.ValorDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoHistoricoMetadata.ColumnNames.ValorAcumulado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataFimApropriacao
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoHistoricoMetadata.ColumnNames.DataFimApropriacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataPagamento
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoHistoricoMetadata.ColumnNames.DataPagamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorCPMFDia
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoHistoricoMetadata.ColumnNames.ValorCPMFDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCPMFAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoHistoricoMetadata.ColumnNames.ValorCPMFAcumulado, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CalculoProvisaoHistoricoCollection")]
	public partial class CalculoProvisaoHistoricoCollection : esCalculoProvisaoHistoricoCollection, IEnumerable<CalculoProvisaoHistorico>
	{
		public CalculoProvisaoHistoricoCollection()
		{

		}
		
		public static implicit operator List<CalculoProvisaoHistorico>(CalculoProvisaoHistoricoCollection coll)
		{
			List<CalculoProvisaoHistorico> list = new List<CalculoProvisaoHistorico>();
			
			foreach (CalculoProvisaoHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CalculoProvisaoHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoProvisaoHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CalculoProvisaoHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CalculoProvisaoHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CalculoProvisaoHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoProvisaoHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CalculoProvisaoHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CalculoProvisaoHistorico AddNew()
		{
			CalculoProvisaoHistorico entity = base.AddNewEntity() as CalculoProvisaoHistorico;
			
			return entity;
		}

		public CalculoProvisaoHistorico FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idTabela)
		{
			return base.FindByPrimaryKey(dataHistorico, idTabela) as CalculoProvisaoHistorico;
		}


		#region IEnumerable<CalculoProvisaoHistorico> Members

		IEnumerator<CalculoProvisaoHistorico> IEnumerable<CalculoProvisaoHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CalculoProvisaoHistorico;
			}
		}

		#endregion
		
		private CalculoProvisaoHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CalculoProvisaoHistorico' table
	/// </summary>

	[Serializable]
	public partial class CalculoProvisaoHistorico : esCalculoProvisaoHistorico
	{
		public CalculoProvisaoHistorico()
		{

		}
	
		public CalculoProvisaoHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CalculoProvisaoHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esCalculoProvisaoHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoProvisaoHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CalculoProvisaoHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoProvisaoHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CalculoProvisaoHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CalculoProvisaoHistoricoQuery query;
	}



	[Serializable]
	public partial class CalculoProvisaoHistoricoQuery : esCalculoProvisaoHistoricoQuery
	{
		public CalculoProvisaoHistoricoQuery()
		{

		}		
		
		public CalculoProvisaoHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CalculoProvisaoHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CalculoProvisaoHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CalculoProvisaoHistoricoMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoProvisaoHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoProvisaoHistoricoMetadata.ColumnNames.IdTabela, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoProvisaoHistoricoMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoProvisaoHistoricoMetadata.ColumnNames.IdCarteira, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoProvisaoHistoricoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoProvisaoHistoricoMetadata.ColumnNames.ValorDia, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoProvisaoHistoricoMetadata.PropertyNames.ValorDia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoProvisaoHistoricoMetadata.ColumnNames.ValorAcumulado, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoProvisaoHistoricoMetadata.PropertyNames.ValorAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoProvisaoHistoricoMetadata.ColumnNames.DataFimApropriacao, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoProvisaoHistoricoMetadata.PropertyNames.DataFimApropriacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoProvisaoHistoricoMetadata.ColumnNames.DataPagamento, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoProvisaoHistoricoMetadata.PropertyNames.DataPagamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoProvisaoHistoricoMetadata.ColumnNames.ValorCPMFDia, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoProvisaoHistoricoMetadata.PropertyNames.ValorCPMFDia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoProvisaoHistoricoMetadata.ColumnNames.ValorCPMFAcumulado, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoProvisaoHistoricoMetadata.PropertyNames.ValorCPMFAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CalculoProvisaoHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdTabela = "IdTabela";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorDia = "ValorDia";
			 public const string ValorAcumulado = "ValorAcumulado";
			 public const string DataFimApropriacao = "DataFimApropriacao";
			 public const string DataPagamento = "DataPagamento";
			 public const string ValorCPMFDia = "ValorCPMFDia";
			 public const string ValorCPMFAcumulado = "ValorCPMFAcumulado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdTabela = "IdTabela";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorDia = "ValorDia";
			 public const string ValorAcumulado = "ValorAcumulado";
			 public const string DataFimApropriacao = "DataFimApropriacao";
			 public const string DataPagamento = "DataPagamento";
			 public const string ValorCPMFDia = "ValorCPMFDia";
			 public const string ValorCPMFAcumulado = "ValorCPMFAcumulado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CalculoProvisaoHistoricoMetadata))
			{
				if(CalculoProvisaoHistoricoMetadata.mapDelegates == null)
				{
					CalculoProvisaoHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CalculoProvisaoHistoricoMetadata.meta == null)
				{
					CalculoProvisaoHistoricoMetadata.meta = new CalculoProvisaoHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorAcumulado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataFimApropriacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataPagamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorCPMFDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCPMFAcumulado", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CalculoProvisaoHistorico";
				meta.Destination = "CalculoProvisaoHistorico";
				
				meta.spInsert = "proc_CalculoProvisaoHistoricoInsert";				
				meta.spUpdate = "proc_CalculoProvisaoHistoricoUpdate";		
				meta.spDelete = "proc_CalculoProvisaoHistoricoDelete";
				meta.spLoadAll = "proc_CalculoProvisaoHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_CalculoProvisaoHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CalculoProvisaoHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
