/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/10/2015 16:27:07
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.InvestidorCotista;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esOfertaDistribuicaoCotasCollection : esEntityCollection
	{
		public esOfertaDistribuicaoCotasCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OfertaDistribuicaoCotasCollection";
		}

		#region Query Logic
		protected void InitQuery(esOfertaDistribuicaoCotasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOfertaDistribuicaoCotasQuery);
		}
		#endregion
		
		virtual public OfertaDistribuicaoCotas DetachEntity(OfertaDistribuicaoCotas entity)
		{
			return base.DetachEntity(entity) as OfertaDistribuicaoCotas;
		}
		
		virtual public OfertaDistribuicaoCotas AttachEntity(OfertaDistribuicaoCotas entity)
		{
			return base.AttachEntity(entity) as OfertaDistribuicaoCotas;
		}
		
		virtual public void Combine(OfertaDistribuicaoCotasCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OfertaDistribuicaoCotas this[int index]
		{
			get
			{
				return base[index] as OfertaDistribuicaoCotas;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OfertaDistribuicaoCotas);
		}
	}



	[Serializable]
	abstract public class esOfertaDistribuicaoCotas : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOfertaDistribuicaoCotasQuery GetDynamicQuery()
		{
			return null;
		}

		public esOfertaDistribuicaoCotas()
		{

		}

		public esOfertaDistribuicaoCotas(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOfertaDistribuicaoCotas)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOfertaDistribuicaoCotas);
			else
				return LoadByPrimaryKeyStoredProcedure(idOfertaDistribuicaoCotas);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOfertaDistribuicaoCotas)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOfertaDistribuicaoCotas);
			else
				return LoadByPrimaryKeyStoredProcedure(idOfertaDistribuicaoCotas);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOfertaDistribuicaoCotas)
		{
			esOfertaDistribuicaoCotasQuery query = this.GetDynamicQuery();
			query.Where(query.IdOfertaDistribuicaoCotas == idOfertaDistribuicaoCotas);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOfertaDistribuicaoCotas)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOfertaDistribuicaoCotas",idOfertaDistribuicaoCotas);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOfertaDistribuicaoCotas": this.str.IdOfertaDistribuicaoCotas = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "PrimeiraEmissao": this.str.PrimeiraEmissao = (string)value; break;							
						case "ApenasInvestidorProfissional": this.str.ApenasInvestidorProfissional = (string)value; break;							
						case "ApenasInvestidorQualificado": this.str.ApenasInvestidorQualificado = (string)value; break;							
						case "DataInicialOferta": this.str.DataInicialOferta = (string)value; break;							
						case "DataFinalOferta": this.str.DataFinalOferta = (string)value; break;							
						case "QuantidadeTotalCotas": this.str.QuantidadeTotalCotas = (string)value; break;							
						case "CotaEspecifica": this.str.CotaEspecifica = (string)value; break;							
						case "ValorCota": this.str.ValorCota = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOfertaDistribuicaoCotas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOfertaDistribuicaoCotas = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataInicialOferta":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicialOferta = (System.DateTime?)value;
							break;
						
						case "DataFinalOferta":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFinalOferta = (System.DateTime?)value;
							break;
						
						case "QuantidadeTotalCotas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.QuantidadeTotalCotas = (System.Int32?)value;
							break;
						
						case "ValorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCota = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OfertaDistribuicaoCotas.IdOfertaDistribuicaoCotas
		/// </summary>
		virtual public System.Int32? IdOfertaDistribuicaoCotas
		{
			get
			{
				return base.GetSystemInt32(OfertaDistribuicaoCotasMetadata.ColumnNames.IdOfertaDistribuicaoCotas);
			}
			
			set
			{
				base.SetSystemInt32(OfertaDistribuicaoCotasMetadata.ColumnNames.IdOfertaDistribuicaoCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to OfertaDistribuicaoCotas.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(OfertaDistribuicaoCotasMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(OfertaDistribuicaoCotasMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OfertaDistribuicaoCotas.PrimeiraEmissao
		/// </summary>
		virtual public System.String PrimeiraEmissao
		{
			get
			{
				return base.GetSystemString(OfertaDistribuicaoCotasMetadata.ColumnNames.PrimeiraEmissao);
			}
			
			set
			{
				base.SetSystemString(OfertaDistribuicaoCotasMetadata.ColumnNames.PrimeiraEmissao, value);
			}
		}
		
		/// <summary>
		/// Maps to OfertaDistribuicaoCotas.ApenasInvestidorProfissional
		/// </summary>
		virtual public System.String ApenasInvestidorProfissional
		{
			get
			{
				return base.GetSystemString(OfertaDistribuicaoCotasMetadata.ColumnNames.ApenasInvestidorProfissional);
			}
			
			set
			{
				base.SetSystemString(OfertaDistribuicaoCotasMetadata.ColumnNames.ApenasInvestidorProfissional, value);
			}
		}
		
		/// <summary>
		/// Maps to OfertaDistribuicaoCotas.ApenasInvestidorQualificado
		/// </summary>
		virtual public System.String ApenasInvestidorQualificado
		{
			get
			{
				return base.GetSystemString(OfertaDistribuicaoCotasMetadata.ColumnNames.ApenasInvestidorQualificado);
			}
			
			set
			{
				base.SetSystemString(OfertaDistribuicaoCotasMetadata.ColumnNames.ApenasInvestidorQualificado, value);
			}
		}
		
		/// <summary>
		/// Maps to OfertaDistribuicaoCotas.DataInicialOferta
		/// </summary>
		virtual public System.DateTime? DataInicialOferta
		{
			get
			{
				return base.GetSystemDateTime(OfertaDistribuicaoCotasMetadata.ColumnNames.DataInicialOferta);
			}
			
			set
			{
				base.SetSystemDateTime(OfertaDistribuicaoCotasMetadata.ColumnNames.DataInicialOferta, value);
			}
		}
		
		/// <summary>
		/// Maps to OfertaDistribuicaoCotas.DataFinalOferta
		/// </summary>
		virtual public System.DateTime? DataFinalOferta
		{
			get
			{
				return base.GetSystemDateTime(OfertaDistribuicaoCotasMetadata.ColumnNames.DataFinalOferta);
			}
			
			set
			{
				base.SetSystemDateTime(OfertaDistribuicaoCotasMetadata.ColumnNames.DataFinalOferta, value);
			}
		}
		
		/// <summary>
		/// Maps to OfertaDistribuicaoCotas.QuantidadeTotalCotas
		/// </summary>
		virtual public System.Int32? QuantidadeTotalCotas
		{
			get
			{
				return base.GetSystemInt32(OfertaDistribuicaoCotasMetadata.ColumnNames.QuantidadeTotalCotas);
			}
			
			set
			{
				base.SetSystemInt32(OfertaDistribuicaoCotasMetadata.ColumnNames.QuantidadeTotalCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to OfertaDistribuicaoCotas.CotaEspecifica
		/// </summary>
		virtual public System.String CotaEspecifica
		{
			get
			{
				return base.GetSystemString(OfertaDistribuicaoCotasMetadata.ColumnNames.CotaEspecifica);
			}
			
			set
			{
				base.SetSystemString(OfertaDistribuicaoCotasMetadata.ColumnNames.CotaEspecifica, value);
			}
		}
		
		/// <summary>
		/// Maps to OfertaDistribuicaoCotas.ValorCota
		/// </summary>
		virtual public System.Decimal? ValorCota
		{
			get
			{
				return base.GetSystemDecimal(OfertaDistribuicaoCotasMetadata.ColumnNames.ValorCota);
			}
			
			set
			{
				base.SetSystemDecimal(OfertaDistribuicaoCotasMetadata.ColumnNames.ValorCota, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOfertaDistribuicaoCotas entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOfertaDistribuicaoCotas
			{
				get
				{
					System.Int32? data = entity.IdOfertaDistribuicaoCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOfertaDistribuicaoCotas = null;
					else entity.IdOfertaDistribuicaoCotas = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String PrimeiraEmissao
			{
				get
				{
					System.String data = entity.PrimeiraEmissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrimeiraEmissao = null;
					else entity.PrimeiraEmissao = Convert.ToString(value);
				}
			}
				
			public System.String ApenasInvestidorProfissional
			{
				get
				{
					System.String data = entity.ApenasInvestidorProfissional;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ApenasInvestidorProfissional = null;
					else entity.ApenasInvestidorProfissional = Convert.ToString(value);
				}
			}
				
			public System.String ApenasInvestidorQualificado
			{
				get
				{
					System.String data = entity.ApenasInvestidorQualificado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ApenasInvestidorQualificado = null;
					else entity.ApenasInvestidorQualificado = Convert.ToString(value);
				}
			}
				
			public System.String DataInicialOferta
			{
				get
				{
					System.DateTime? data = entity.DataInicialOferta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicialOferta = null;
					else entity.DataInicialOferta = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataFinalOferta
			{
				get
				{
					System.DateTime? data = entity.DataFinalOferta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFinalOferta = null;
					else entity.DataFinalOferta = Convert.ToDateTime(value);
				}
			}
				
			public System.String QuantidadeTotalCotas
			{
				get
				{
					System.Int32? data = entity.QuantidadeTotalCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeTotalCotas = null;
					else entity.QuantidadeTotalCotas = Convert.ToInt32(value);
				}
			}
				
			public System.String CotaEspecifica
			{
				get
				{
					System.String data = entity.CotaEspecifica;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaEspecifica = null;
					else entity.CotaEspecifica = Convert.ToString(value);
				}
			}
				
			public System.String ValorCota
			{
				get
				{
					System.Decimal? data = entity.ValorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCota = null;
					else entity.ValorCota = Convert.ToDecimal(value);
				}
			}
			

			private esOfertaDistribuicaoCotas entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOfertaDistribuicaoCotasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOfertaDistribuicaoCotas can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OfertaDistribuicaoCotas : esOfertaDistribuicaoCotas
	{

		#region UpToOperacaoCotistaCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - AplicacaoOfertaDistribuicaoCotas_OfertaDistribuicaoCotas_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoCotistaCollection UpToOperacaoCotistaCollection
		{
			get
			{
				if(this._UpToOperacaoCotistaCollection == null)
				{
					this._UpToOperacaoCotistaCollection = new OperacaoCotistaCollection();
					this._UpToOperacaoCotistaCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToOperacaoCotistaCollection", this._UpToOperacaoCotistaCollection);
					this._UpToOperacaoCotistaCollection.ManyToManyOfertaDistribuicaoCotasCollection(this.IdOfertaDistribuicaoCotas);
				}

				return this._UpToOperacaoCotistaCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToOperacaoCotistaCollection != null) 
				{ 
					this.RemovePostSave("UpToOperacaoCotistaCollection"); 
					this._UpToOperacaoCotistaCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - AplicacaoOfertaDistribuicaoCotas_OfertaDistribuicaoCotas_FK1
		/// </summary>
		public void AssociateOperacaoCotistaCollection(OperacaoCotista entity)
		{
			if (this._AplicacaoOfertaDistribuicaoCotasCollection == null)
			{
				this._AplicacaoOfertaDistribuicaoCotasCollection = new AplicacaoOfertaDistribuicaoCotasCollection();
				this._AplicacaoOfertaDistribuicaoCotasCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("AplicacaoOfertaDistribuicaoCotasCollection", this._AplicacaoOfertaDistribuicaoCotasCollection);
			}

			AplicacaoOfertaDistribuicaoCotas obj = this._AplicacaoOfertaDistribuicaoCotasCollection.AddNew();
			obj.IdOfertaDistribuicaoCotas = this.IdOfertaDistribuicaoCotas;
			obj.IdOperacao = entity.IdOperacao;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - AplicacaoOfertaDistribuicaoCotas_OfertaDistribuicaoCotas_FK1
		/// </summary>
		public void DissociateOperacaoCotistaCollection(OperacaoCotista entity)
		{
			if (this._AplicacaoOfertaDistribuicaoCotasCollection == null)
			{
				this._AplicacaoOfertaDistribuicaoCotasCollection = new AplicacaoOfertaDistribuicaoCotasCollection();
				this._AplicacaoOfertaDistribuicaoCotasCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("AplicacaoOfertaDistribuicaoCotasCollection", this._AplicacaoOfertaDistribuicaoCotasCollection);
			}

			AplicacaoOfertaDistribuicaoCotas obj = this._AplicacaoOfertaDistribuicaoCotasCollection.AddNew();
			obj.IdOfertaDistribuicaoCotas = this.IdOfertaDistribuicaoCotas;
			obj.IdOperacao = entity.IdOperacao;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private OperacaoCotistaCollection _UpToOperacaoCotistaCollection;
		private AplicacaoOfertaDistribuicaoCotasCollection _AplicacaoOfertaDistribuicaoCotasCollection;
		#endregion

				
		#region AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AplicacaoOfertaDistribuicaoCotas_OfertaDistribuicaoCotas_FK1
		/// </summary>

		[XmlIgnore]
		public AplicacaoOfertaDistribuicaoCotasCollection AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas
		{
			get
			{
				if(this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas == null)
				{
					this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas = new AplicacaoOfertaDistribuicaoCotasCollection();
					this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas", this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas);
				
					if(this.IdOfertaDistribuicaoCotas != null)
					{
						this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas.Query.Where(this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas.Query.IdOfertaDistribuicaoCotas == this.IdOfertaDistribuicaoCotas);
						this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas.Query.Load();

						// Auto-hookup Foreign Keys
						this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas.fks.Add(AplicacaoOfertaDistribuicaoCotasMetadata.ColumnNames.IdOfertaDistribuicaoCotas, this.IdOfertaDistribuicaoCotas);
					}
				}

				return this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas != null) 
				{ 
					this.RemovePostSave("AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas"); 
					this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas = null;
					
				} 
			} 			
		}

		private AplicacaoOfertaDistribuicaoCotasCollection _AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas;
		#endregion

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_OfertaDistribuicaoCotas_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas", typeof(AplicacaoOfertaDistribuicaoCotasCollection), new AplicacaoOfertaDistribuicaoCotas()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._AplicacaoOfertaDistribuicaoCotasCollection != null)
			{
				foreach(AplicacaoOfertaDistribuicaoCotas obj in this._AplicacaoOfertaDistribuicaoCotasCollection)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOfertaDistribuicaoCotas = this.IdOfertaDistribuicaoCotas;
					}
				}
			}
			if(this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas != null)
			{
				foreach(AplicacaoOfertaDistribuicaoCotas obj in this._AplicacaoOfertaDistribuicaoCotasCollectionByIdOfertaDistribuicaoCotas)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOfertaDistribuicaoCotas = this.IdOfertaDistribuicaoCotas;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}
	
	public partial class OfertaDistribuicaoCotasCollection : esOfertaDistribuicaoCotasCollection
	{
		#region ManyToManyOperacaoCotistaCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyOperacaoCotistaCollection(System.Int32? IdOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao", IdOperacao);
	
			return base.Load( esQueryType.ManyToMany, 
				"OfertaDistribuicaoCotas,AplicacaoOfertaDistribuicaoCotas|IdOfertaDistribuicaoCotas,IdOfertaDistribuicaoCotas|IdOperacao",	parms);
		}
		#endregion
	}




	[Serializable]
	abstract public class esOfertaDistribuicaoCotasQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OfertaDistribuicaoCotasMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOfertaDistribuicaoCotas
		{
			get
			{
				return new esQueryItem(this, OfertaDistribuicaoCotasMetadata.ColumnNames.IdOfertaDistribuicaoCotas, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, OfertaDistribuicaoCotasMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PrimeiraEmissao
		{
			get
			{
				return new esQueryItem(this, OfertaDistribuicaoCotasMetadata.ColumnNames.PrimeiraEmissao, esSystemType.String);
			}
		} 
		
		public esQueryItem ApenasInvestidorProfissional
		{
			get
			{
				return new esQueryItem(this, OfertaDistribuicaoCotasMetadata.ColumnNames.ApenasInvestidorProfissional, esSystemType.String);
			}
		} 
		
		public esQueryItem ApenasInvestidorQualificado
		{
			get
			{
				return new esQueryItem(this, OfertaDistribuicaoCotasMetadata.ColumnNames.ApenasInvestidorQualificado, esSystemType.String);
			}
		} 
		
		public esQueryItem DataInicialOferta
		{
			get
			{
				return new esQueryItem(this, OfertaDistribuicaoCotasMetadata.ColumnNames.DataInicialOferta, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataFinalOferta
		{
			get
			{
				return new esQueryItem(this, OfertaDistribuicaoCotasMetadata.ColumnNames.DataFinalOferta, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem QuantidadeTotalCotas
		{
			get
			{
				return new esQueryItem(this, OfertaDistribuicaoCotasMetadata.ColumnNames.QuantidadeTotalCotas, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CotaEspecifica
		{
			get
			{
				return new esQueryItem(this, OfertaDistribuicaoCotasMetadata.ColumnNames.CotaEspecifica, esSystemType.String);
			}
		} 
		
		public esQueryItem ValorCota
		{
			get
			{
				return new esQueryItem(this, OfertaDistribuicaoCotasMetadata.ColumnNames.ValorCota, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OfertaDistribuicaoCotasCollection")]
	public partial class OfertaDistribuicaoCotasCollection : esOfertaDistribuicaoCotasCollection, IEnumerable<OfertaDistribuicaoCotas>
	{
		public OfertaDistribuicaoCotasCollection()
		{

		}
		
		public static implicit operator List<OfertaDistribuicaoCotas>(OfertaDistribuicaoCotasCollection coll)
		{
			List<OfertaDistribuicaoCotas> list = new List<OfertaDistribuicaoCotas>();
			
			foreach (OfertaDistribuicaoCotas emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OfertaDistribuicaoCotasMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OfertaDistribuicaoCotasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OfertaDistribuicaoCotas(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OfertaDistribuicaoCotas();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OfertaDistribuicaoCotasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OfertaDistribuicaoCotasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OfertaDistribuicaoCotasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OfertaDistribuicaoCotas AddNew()
		{
			OfertaDistribuicaoCotas entity = base.AddNewEntity() as OfertaDistribuicaoCotas;
			
			return entity;
		}

		public OfertaDistribuicaoCotas FindByPrimaryKey(System.Int32 idOfertaDistribuicaoCotas)
		{
			return base.FindByPrimaryKey(idOfertaDistribuicaoCotas) as OfertaDistribuicaoCotas;
		}


		#region IEnumerable<OfertaDistribuicaoCotas> Members

		IEnumerator<OfertaDistribuicaoCotas> IEnumerable<OfertaDistribuicaoCotas>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OfertaDistribuicaoCotas;
			}
		}

		#endregion
		
		private OfertaDistribuicaoCotasQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OfertaDistribuicaoCotas' table
	/// </summary>

	[Serializable]
	public partial class OfertaDistribuicaoCotas : esOfertaDistribuicaoCotas
	{
		public OfertaDistribuicaoCotas()
		{

		}
	
		public OfertaDistribuicaoCotas(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OfertaDistribuicaoCotasMetadata.Meta();
			}
		}
		
		
		
		override protected esOfertaDistribuicaoCotasQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OfertaDistribuicaoCotasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OfertaDistribuicaoCotasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OfertaDistribuicaoCotasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OfertaDistribuicaoCotasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OfertaDistribuicaoCotasQuery query;
	}



	[Serializable]
	public partial class OfertaDistribuicaoCotasQuery : esOfertaDistribuicaoCotasQuery
	{
		public OfertaDistribuicaoCotasQuery()
		{

		}		
		
		public OfertaDistribuicaoCotasQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OfertaDistribuicaoCotasMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OfertaDistribuicaoCotasMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OfertaDistribuicaoCotasMetadata.ColumnNames.IdOfertaDistribuicaoCotas, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OfertaDistribuicaoCotasMetadata.PropertyNames.IdOfertaDistribuicaoCotas;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OfertaDistribuicaoCotasMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OfertaDistribuicaoCotasMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OfertaDistribuicaoCotasMetadata.ColumnNames.PrimeiraEmissao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = OfertaDistribuicaoCotasMetadata.PropertyNames.PrimeiraEmissao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OfertaDistribuicaoCotasMetadata.ColumnNames.ApenasInvestidorProfissional, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = OfertaDistribuicaoCotasMetadata.PropertyNames.ApenasInvestidorProfissional;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OfertaDistribuicaoCotasMetadata.ColumnNames.ApenasInvestidorQualificado, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = OfertaDistribuicaoCotasMetadata.PropertyNames.ApenasInvestidorQualificado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OfertaDistribuicaoCotasMetadata.ColumnNames.DataInicialOferta, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OfertaDistribuicaoCotasMetadata.PropertyNames.DataInicialOferta;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OfertaDistribuicaoCotasMetadata.ColumnNames.DataFinalOferta, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OfertaDistribuicaoCotasMetadata.PropertyNames.DataFinalOferta;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OfertaDistribuicaoCotasMetadata.ColumnNames.QuantidadeTotalCotas, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OfertaDistribuicaoCotasMetadata.PropertyNames.QuantidadeTotalCotas;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OfertaDistribuicaoCotasMetadata.ColumnNames.CotaEspecifica, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = OfertaDistribuicaoCotasMetadata.PropertyNames.CotaEspecifica;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OfertaDistribuicaoCotasMetadata.ColumnNames.ValorCota, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OfertaDistribuicaoCotasMetadata.PropertyNames.ValorCota;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OfertaDistribuicaoCotasMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOfertaDistribuicaoCotas = "IdOfertaDistribuicaoCotas";
			 public const string IdCarteira = "IdCarteira";
			 public const string PrimeiraEmissao = "PrimeiraEmissao";
			 public const string ApenasInvestidorProfissional = "ApenasInvestidorProfissional";
			 public const string ApenasInvestidorQualificado = "ApenasInvestidorQualificado";
			 public const string DataInicialOferta = "DataInicialOferta";
			 public const string DataFinalOferta = "DataFinalOferta";
			 public const string QuantidadeTotalCotas = "QuantidadeTotalCotas";
			 public const string CotaEspecifica = "CotaEspecifica";
			 public const string ValorCota = "ValorCota";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOfertaDistribuicaoCotas = "IdOfertaDistribuicaoCotas";
			 public const string IdCarteira = "IdCarteira";
			 public const string PrimeiraEmissao = "PrimeiraEmissao";
			 public const string ApenasInvestidorProfissional = "ApenasInvestidorProfissional";
			 public const string ApenasInvestidorQualificado = "ApenasInvestidorQualificado";
			 public const string DataInicialOferta = "DataInicialOferta";
			 public const string DataFinalOferta = "DataFinalOferta";
			 public const string QuantidadeTotalCotas = "QuantidadeTotalCotas";
			 public const string CotaEspecifica = "CotaEspecifica";
			 public const string ValorCota = "ValorCota";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OfertaDistribuicaoCotasMetadata))
			{
				if(OfertaDistribuicaoCotasMetadata.mapDelegates == null)
				{
					OfertaDistribuicaoCotasMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OfertaDistribuicaoCotasMetadata.meta == null)
				{
					OfertaDistribuicaoCotasMetadata.meta = new OfertaDistribuicaoCotasMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOfertaDistribuicaoCotas", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PrimeiraEmissao", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ApenasInvestidorProfissional", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ApenasInvestidorQualificado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DataInicialOferta", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataFinalOferta", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("QuantidadeTotalCotas", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CotaEspecifica", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ValorCota", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "OfertaDistribuicaoCotas";
				meta.Destination = "OfertaDistribuicaoCotas";
				
				meta.spInsert = "proc_OfertaDistribuicaoCotasInsert";				
				meta.spUpdate = "proc_OfertaDistribuicaoCotasUpdate";		
				meta.spDelete = "proc_OfertaDistribuicaoCotasDelete";
				meta.spLoadAll = "proc_OfertaDistribuicaoCotasLoadAll";
				meta.spLoadByPrimaryKey = "proc_OfertaDistribuicaoCotasLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OfertaDistribuicaoCotasMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
