/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 13/05/2015 11:38:18
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		





using Financial.Contabil;
		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTabelaTaxaAdministracaoCollection : esEntityCollection
	{
		public esTabelaTaxaAdministracaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaTaxaAdministracaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaTaxaAdministracaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaTaxaAdministracaoQuery);
		}
		#endregion
		
		virtual public TabelaTaxaAdministracao DetachEntity(TabelaTaxaAdministracao entity)
		{
			return base.DetachEntity(entity) as TabelaTaxaAdministracao;
		}
		
		virtual public TabelaTaxaAdministracao AttachEntity(TabelaTaxaAdministracao entity)
		{
			return base.AttachEntity(entity) as TabelaTaxaAdministracao;
		}
		
		virtual public void Combine(TabelaTaxaAdministracaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaTaxaAdministracao this[int index]
		{
			get
			{
				return base[index] as TabelaTaxaAdministracao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaTaxaAdministracao);
		}
	}



	[Serializable]
	abstract public class esTabelaTaxaAdministracao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaTaxaAdministracaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaTaxaAdministracao()
		{

		}

		public esTabelaTaxaAdministracao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabela)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTabela)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaTaxaAdministracaoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTabela == idTabela);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabela)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabela)
		{
			esTabelaTaxaAdministracaoQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabela == idTabela);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabela)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabela",idTabela);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "TipoCalculo": this.str.TipoCalculo = (string)value; break;							
						case "TipoApropriacao": this.str.TipoApropriacao = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;							
						case "BaseApuracao": this.str.BaseApuracao = (string)value; break;							
						case "BaseAno": this.str.BaseAno = (string)value; break;							
						case "TipoLimitePL": this.str.TipoLimitePL = (string)value; break;							
						case "ValorLimite": this.str.ValorLimite = (string)value; break;							
						case "ValorFixoTotal": this.str.ValorFixoTotal = (string)value; break;							
						case "ContagemDias": this.str.ContagemDias = (string)value; break;							
						case "ValorMinimo": this.str.ValorMinimo = (string)value; break;							
						case "ValorMaximo": this.str.ValorMaximo = (string)value; break;							
						case "NumeroMesesRenovacao": this.str.NumeroMesesRenovacao = (string)value; break;							
						case "NumeroDiasPagamento": this.str.NumeroDiasPagamento = (string)value; break;							
						case "ImpactaPL": this.str.ImpactaPL = (string)value; break;							
						case "IdCadastro": this.str.IdCadastro = (string)value; break;							
						case "DataFim": this.str.DataFim = (string)value; break;							
						case "IdEventoProvisao": this.str.IdEventoProvisao = (string)value; break;							
						case "IdEventoPagamento": this.str.IdEventoPagamento = (string)value; break;							
						case "TipoGrupo": this.str.TipoGrupo = (string)value; break;							
						case "CodigoAtivo": this.str.CodigoAtivo = (string)value; break;							
						case "EscalonaProporcional": this.str.EscalonaProporcional = (string)value; break;							
						case "Consolidadora": this.str.Consolidadora = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "TipoCalculo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCalculo = (System.Byte?)value;
							break;
						
						case "TipoApropriacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoApropriacao = (System.Byte?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
						
						case "BaseApuracao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.BaseApuracao = (System.Byte?)value;
							break;
						
						case "BaseAno":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.BaseAno = (System.Int16?)value;
							break;
						
						case "TipoLimitePL":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoLimitePL = (System.Byte?)value;
							break;
						
						case "ValorLimite":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLimite = (System.Decimal?)value;
							break;
						
						case "ValorFixoTotal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorFixoTotal = (System.Decimal?)value;
							break;
						
						case "ContagemDias":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContagemDias = (System.Byte?)value;
							break;
						
						case "ValorMinimo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMinimo = (System.Decimal?)value;
							break;
						
						case "ValorMaximo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMaximo = (System.Decimal?)value;
							break;
						
						case "NumeroMesesRenovacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.NumeroMesesRenovacao = (System.Byte?)value;
							break;
						
						case "NumeroDiasPagamento":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.NumeroDiasPagamento = (System.Byte?)value;
							break;
						
						case "IdCadastro":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCadastro = (System.Int32?)value;
							break;
						
						case "DataFim":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFim = (System.DateTime?)value;
							break;
						
						case "IdEventoProvisao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoProvisao = (System.Int32?)value;
							break;
						
						case "IdEventoPagamento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoPagamento = (System.Int32?)value;
							break;
						
						case "TipoGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoGrupo = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaAdministracaoMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				base.SetSystemInt32(TabelaTaxaAdministracaoMetadata.ColumnNames.IdTabela, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaTaxaAdministracaoMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaTaxaAdministracaoMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaAdministracaoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(TabelaTaxaAdministracaoMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.TipoCalculo
		/// </summary>
		virtual public System.Byte? TipoCalculo
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaAdministracaoMetadata.ColumnNames.TipoCalculo);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaAdministracaoMetadata.ColumnNames.TipoCalculo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.TipoApropriacao
		/// </summary>
		virtual public System.Byte? TipoApropriacao
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaAdministracaoMetadata.ColumnNames.TipoApropriacao);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaAdministracaoMetadata.ColumnNames.TipoApropriacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(TabelaTaxaAdministracaoMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTaxaAdministracaoMetadata.ColumnNames.Taxa, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.BaseApuracao
		/// </summary>
		virtual public System.Byte? BaseApuracao
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaAdministracaoMetadata.ColumnNames.BaseApuracao);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaAdministracaoMetadata.ColumnNames.BaseApuracao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.BaseAno
		/// </summary>
		virtual public System.Int16? BaseAno
		{
			get
			{
				return base.GetSystemInt16(TabelaTaxaAdministracaoMetadata.ColumnNames.BaseAno);
			}
			
			set
			{
				base.SetSystemInt16(TabelaTaxaAdministracaoMetadata.ColumnNames.BaseAno, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.TipoLimitePL
		/// </summary>
		virtual public System.Byte? TipoLimitePL
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaAdministracaoMetadata.ColumnNames.TipoLimitePL);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaAdministracaoMetadata.ColumnNames.TipoLimitePL, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.ValorLimite
		/// </summary>
		virtual public System.Decimal? ValorLimite
		{
			get
			{
				return base.GetSystemDecimal(TabelaTaxaAdministracaoMetadata.ColumnNames.ValorLimite);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTaxaAdministracaoMetadata.ColumnNames.ValorLimite, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.ValorFixoTotal
		/// </summary>
		virtual public System.Decimal? ValorFixoTotal
		{
			get
			{
				return base.GetSystemDecimal(TabelaTaxaAdministracaoMetadata.ColumnNames.ValorFixoTotal);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTaxaAdministracaoMetadata.ColumnNames.ValorFixoTotal, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.ContagemDias
		/// </summary>
		virtual public System.Byte? ContagemDias
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaAdministracaoMetadata.ColumnNames.ContagemDias);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaAdministracaoMetadata.ColumnNames.ContagemDias, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.ValorMinimo
		/// </summary>
		virtual public System.Decimal? ValorMinimo
		{
			get
			{
				return base.GetSystemDecimal(TabelaTaxaAdministracaoMetadata.ColumnNames.ValorMinimo);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTaxaAdministracaoMetadata.ColumnNames.ValorMinimo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.ValorMaximo
		/// </summary>
		virtual public System.Decimal? ValorMaximo
		{
			get
			{
				return base.GetSystemDecimal(TabelaTaxaAdministracaoMetadata.ColumnNames.ValorMaximo);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTaxaAdministracaoMetadata.ColumnNames.ValorMaximo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.NumeroMesesRenovacao
		/// </summary>
		virtual public System.Byte? NumeroMesesRenovacao
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaAdministracaoMetadata.ColumnNames.NumeroMesesRenovacao);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaAdministracaoMetadata.ColumnNames.NumeroMesesRenovacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.NumeroDiasPagamento
		/// </summary>
		virtual public System.Byte? NumeroDiasPagamento
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaAdministracaoMetadata.ColumnNames.NumeroDiasPagamento);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaAdministracaoMetadata.ColumnNames.NumeroDiasPagamento, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.ImpactaPL
		/// </summary>
		virtual public System.String ImpactaPL
		{
			get
			{
				return base.GetSystemString(TabelaTaxaAdministracaoMetadata.ColumnNames.ImpactaPL);
			}
			
			set
			{
				base.SetSystemString(TabelaTaxaAdministracaoMetadata.ColumnNames.ImpactaPL, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.IdCadastro
		/// </summary>
		virtual public System.Int32? IdCadastro
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaAdministracaoMetadata.ColumnNames.IdCadastro);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaTaxaAdministracaoMetadata.ColumnNames.IdCadastro, value))
				{
					this._UpToCadastroTaxaAdministracaoByIdCadastro = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.DataFim
		/// </summary>
		virtual public System.DateTime? DataFim
		{
			get
			{
				return base.GetSystemDateTime(TabelaTaxaAdministracaoMetadata.ColumnNames.DataFim);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaTaxaAdministracaoMetadata.ColumnNames.DataFim, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.IdEventoProvisao
		/// </summary>
		virtual public System.Int32? IdEventoProvisao
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaAdministracaoMetadata.ColumnNames.IdEventoProvisao);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaTaxaAdministracaoMetadata.ColumnNames.IdEventoProvisao, value))
				{
					this._UpToContabRoteiroByIdEventoProvisao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.IdEventoPagamento
		/// </summary>
		virtual public System.Int32? IdEventoPagamento
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaAdministracaoMetadata.ColumnNames.IdEventoPagamento);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaTaxaAdministracaoMetadata.ColumnNames.IdEventoPagamento, value))
				{
					this._UpToContabRoteiroByIdEventoPagamento = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.TipoGrupo
		/// </summary>
		virtual public System.Int32? TipoGrupo
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaAdministracaoMetadata.ColumnNames.TipoGrupo);
			}
			
			set
			{
				base.SetSystemInt32(TabelaTaxaAdministracaoMetadata.ColumnNames.TipoGrupo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.CodigoAtivo
		/// </summary>
		virtual public System.String CodigoAtivo
		{
			get
			{
				return base.GetSystemString(TabelaTaxaAdministracaoMetadata.ColumnNames.CodigoAtivo);
			}
			
			set
			{
				base.SetSystemString(TabelaTaxaAdministracaoMetadata.ColumnNames.CodigoAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.EscalonaProporcional
		/// </summary>
		virtual public System.String EscalonaProporcional
		{
			get
			{
				return base.GetSystemString(TabelaTaxaAdministracaoMetadata.ColumnNames.EscalonaProporcional);
			}
			
			set
			{
				base.SetSystemString(TabelaTaxaAdministracaoMetadata.ColumnNames.EscalonaProporcional, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaAdministracao.Consolidadora
		/// </summary>
		virtual public System.String Consolidadora
		{
			get
			{
				return base.GetSystemString(TabelaTaxaAdministracaoMetadata.ColumnNames.Consolidadora);
			}
			
			set
			{
				base.SetSystemString(TabelaTaxaAdministracaoMetadata.ColumnNames.Consolidadora, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected CadastroTaxaAdministracao _UpToCadastroTaxaAdministracaoByIdCadastro;
		[CLSCompliant(false)]
		internal protected ContabRoteiro _UpToContabRoteiroByIdEventoProvisao;
		[CLSCompliant(false)]
		internal protected ContabRoteiro _UpToContabRoteiroByIdEventoPagamento;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaTaxaAdministracao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoCalculo
			{
				get
				{
					System.Byte? data = entity.TipoCalculo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCalculo = null;
					else entity.TipoCalculo = Convert.ToByte(value);
				}
			}
				
			public System.String TipoApropriacao
			{
				get
				{
					System.Byte? data = entity.TipoApropriacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoApropriacao = null;
					else entity.TipoApropriacao = Convert.ToByte(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
				
			public System.String BaseApuracao
			{
				get
				{
					System.Byte? data = entity.BaseApuracao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseApuracao = null;
					else entity.BaseApuracao = Convert.ToByte(value);
				}
			}
				
			public System.String BaseAno
			{
				get
				{
					System.Int16? data = entity.BaseAno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseAno = null;
					else entity.BaseAno = Convert.ToInt16(value);
				}
			}
				
			public System.String TipoLimitePL
			{
				get
				{
					System.Byte? data = entity.TipoLimitePL;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoLimitePL = null;
					else entity.TipoLimitePL = Convert.ToByte(value);
				}
			}
				
			public System.String ValorLimite
			{
				get
				{
					System.Decimal? data = entity.ValorLimite;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLimite = null;
					else entity.ValorLimite = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorFixoTotal
			{
				get
				{
					System.Decimal? data = entity.ValorFixoTotal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorFixoTotal = null;
					else entity.ValorFixoTotal = Convert.ToDecimal(value);
				}
			}
				
			public System.String ContagemDias
			{
				get
				{
					System.Byte? data = entity.ContagemDias;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContagemDias = null;
					else entity.ContagemDias = Convert.ToByte(value);
				}
			}
				
			public System.String ValorMinimo
			{
				get
				{
					System.Decimal? data = entity.ValorMinimo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMinimo = null;
					else entity.ValorMinimo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorMaximo
			{
				get
				{
					System.Decimal? data = entity.ValorMaximo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMaximo = null;
					else entity.ValorMaximo = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumeroMesesRenovacao
			{
				get
				{
					System.Byte? data = entity.NumeroMesesRenovacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroMesesRenovacao = null;
					else entity.NumeroMesesRenovacao = Convert.ToByte(value);
				}
			}
				
			public System.String NumeroDiasPagamento
			{
				get
				{
					System.Byte? data = entity.NumeroDiasPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroDiasPagamento = null;
					else entity.NumeroDiasPagamento = Convert.ToByte(value);
				}
			}
				
			public System.String ImpactaPL
			{
				get
				{
					System.String data = entity.ImpactaPL;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ImpactaPL = null;
					else entity.ImpactaPL = Convert.ToString(value);
				}
			}
				
			public System.String IdCadastro
			{
				get
				{
					System.Int32? data = entity.IdCadastro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCadastro = null;
					else entity.IdCadastro = Convert.ToInt32(value);
				}
			}
				
			public System.String DataFim
			{
				get
				{
					System.DateTime? data = entity.DataFim;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFim = null;
					else entity.DataFim = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdEventoProvisao
			{
				get
				{
					System.Int32? data = entity.IdEventoProvisao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoProvisao = null;
					else entity.IdEventoProvisao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEventoPagamento
			{
				get
				{
					System.Int32? data = entity.IdEventoPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoPagamento = null;
					else entity.IdEventoPagamento = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoGrupo
			{
				get
				{
					System.Int32? data = entity.TipoGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoGrupo = null;
					else entity.TipoGrupo = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoAtivo
			{
				get
				{
					System.String data = entity.CodigoAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoAtivo = null;
					else entity.CodigoAtivo = Convert.ToString(value);
				}
			}
				
			public System.String EscalonaProporcional
			{
				get
				{
					System.String data = entity.EscalonaProporcional;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EscalonaProporcional = null;
					else entity.EscalonaProporcional = Convert.ToString(value);
				}
			}
				
			public System.String Consolidadora
			{
				get
				{
					System.String data = entity.Consolidadora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Consolidadora = null;
					else entity.Consolidadora = Convert.ToString(value);
				}
			}
			

			private esTabelaTaxaAdministracao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaTaxaAdministracaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaTaxaAdministracao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaTaxaAdministracao : esTabelaTaxaAdministracao
	{

				
		#region CalculoAdministracao - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - TabelaTaxaAdministracao_CalculoAdministracao_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoAdministracao CalculoAdministracao
		{
			get
			{
				if(this._CalculoAdministracao == null)
				{
					this._CalculoAdministracao = new CalculoAdministracao();
					this._CalculoAdministracao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("CalculoAdministracao", this._CalculoAdministracao);
				
					if(this.IdTabela != null)
					{
						this._CalculoAdministracao.Query.Where(this._CalculoAdministracao.Query.IdTabela == this.IdTabela);
						this._CalculoAdministracao.Query.Load();
					}
				}

				return this._CalculoAdministracao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoAdministracao != null) 
				{ 
					this.RemovePostOneSave("CalculoAdministracao"); 
					this._CalculoAdministracao = null;
					
				} 
			}          			
		}

		private CalculoAdministracao _CalculoAdministracao;
		#endregion

				
		#region CalculoAdministracaoAssociada - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - TabelaTaxaAdministracao_CalculoAdministracaoAssociada_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoAdministracaoAssociada CalculoAdministracaoAssociada
		{
			get
			{
				if(this._CalculoAdministracaoAssociada == null)
				{
					this._CalculoAdministracaoAssociada = new CalculoAdministracaoAssociada();
					this._CalculoAdministracaoAssociada.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("CalculoAdministracaoAssociada", this._CalculoAdministracaoAssociada);
				
					if(this.IdTabela != null)
					{
						this._CalculoAdministracaoAssociada.Query.Where(this._CalculoAdministracaoAssociada.Query.IdTabela == this.IdTabela);
						this._CalculoAdministracaoAssociada.Query.Load();
					}
				}

				return this._CalculoAdministracaoAssociada;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoAdministracaoAssociada != null) 
				{ 
					this.RemovePostOneSave("CalculoAdministracaoAssociada"); 
					this._CalculoAdministracaoAssociada = null;
					
				} 
			}          			
		}

		private CalculoAdministracaoAssociada _CalculoAdministracaoAssociada;
		#endregion

				
		#region CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TabelaTaxaAdministracao_CalculoAdministracaoAssociadaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoAdministracaoAssociadaHistoricoCollection CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela
		{
			get
			{
				if(this._CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela == null)
				{
					this._CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela = new CalculoAdministracaoAssociadaHistoricoCollection();
					this._CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela", this._CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela);
				
					if(this.IdTabela != null)
					{
						this._CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela.Query.Where(this._CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela.Query.IdTabela == this.IdTabela);
						this._CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela.fks.Add(CalculoAdministracaoAssociadaHistoricoMetadata.ColumnNames.IdTabela, this.IdTabela);
					}
				}

				return this._CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela != null) 
				{ 
					this.RemovePostSave("CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela"); 
					this._CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela = null;
					
				} 
			} 			
		}

		private CalculoAdministracaoAssociadaHistoricoCollection _CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela;
		#endregion

				
		#region CalculoAdministracaoHistoricoCollectionByIdTabela - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TabelaTaxaAdministracao_CalculoAdministracaoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoAdministracaoHistoricoCollection CalculoAdministracaoHistoricoCollectionByIdTabela
		{
			get
			{
				if(this._CalculoAdministracaoHistoricoCollectionByIdTabela == null)
				{
					this._CalculoAdministracaoHistoricoCollectionByIdTabela = new CalculoAdministracaoHistoricoCollection();
					this._CalculoAdministracaoHistoricoCollectionByIdTabela.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoAdministracaoHistoricoCollectionByIdTabela", this._CalculoAdministracaoHistoricoCollectionByIdTabela);
				
					if(this.IdTabela != null)
					{
						this._CalculoAdministracaoHistoricoCollectionByIdTabela.Query.Where(this._CalculoAdministracaoHistoricoCollectionByIdTabela.Query.IdTabela == this.IdTabela);
						this._CalculoAdministracaoHistoricoCollectionByIdTabela.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoAdministracaoHistoricoCollectionByIdTabela.fks.Add(CalculoAdministracaoHistoricoMetadata.ColumnNames.IdTabela, this.IdTabela);
					}
				}

				return this._CalculoAdministracaoHistoricoCollectionByIdTabela;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoAdministracaoHistoricoCollectionByIdTabela != null) 
				{ 
					this.RemovePostSave("CalculoAdministracaoHistoricoCollectionByIdTabela"); 
					this._CalculoAdministracaoHistoricoCollectionByIdTabela = null;
					
				} 
			} 			
		}

		private CalculoAdministracaoHistoricoCollection _CalculoAdministracaoHistoricoCollectionByIdTabela;
		#endregion

				
		#region TabelaEscalonamentoAdmCollectionByIdTabela - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_TabelaTaxaAdministracao_TabelaEscalonamentoAdm_1
		/// </summary>

		[XmlIgnore]
		public TabelaEscalonamentoAdmCollection TabelaEscalonamentoAdmCollectionByIdTabela
		{
			get
			{
				if(this._TabelaEscalonamentoAdmCollectionByIdTabela == null)
				{
					this._TabelaEscalonamentoAdmCollectionByIdTabela = new TabelaEscalonamentoAdmCollection();
					this._TabelaEscalonamentoAdmCollectionByIdTabela.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaEscalonamentoAdmCollectionByIdTabela", this._TabelaEscalonamentoAdmCollectionByIdTabela);
				
					if(this.IdTabela != null)
					{
						this._TabelaEscalonamentoAdmCollectionByIdTabela.Query.Where(this._TabelaEscalonamentoAdmCollectionByIdTabela.Query.IdTabela == this.IdTabela);
						this._TabelaEscalonamentoAdmCollectionByIdTabela.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaEscalonamentoAdmCollectionByIdTabela.fks.Add(TabelaEscalonamentoAdmMetadata.ColumnNames.IdTabela, this.IdTabela);
					}
				}

				return this._TabelaEscalonamentoAdmCollectionByIdTabela;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaEscalonamentoAdmCollectionByIdTabela != null) 
				{ 
					this.RemovePostSave("TabelaEscalonamentoAdmCollectionByIdTabela"); 
					this._TabelaEscalonamentoAdmCollectionByIdTabela = null;
					
				} 
			} 			
		}

		private TabelaEscalonamentoAdmCollection _TabelaEscalonamentoAdmCollectionByIdTabela;
		#endregion

				
		#region TabelaExcecaoAdmCollectionByIdTabela - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TabelaExcecaoAdm_TabelaTaxaAdministracao_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaExcecaoAdmCollection TabelaExcecaoAdmCollectionByIdTabela
		{
			get
			{
				if(this._TabelaExcecaoAdmCollectionByIdTabela == null)
				{
					this._TabelaExcecaoAdmCollectionByIdTabela = new TabelaExcecaoAdmCollection();
					this._TabelaExcecaoAdmCollectionByIdTabela.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaExcecaoAdmCollectionByIdTabela", this._TabelaExcecaoAdmCollectionByIdTabela);
				
					if(this.IdTabela != null)
					{
						this._TabelaExcecaoAdmCollectionByIdTabela.Query.Where(this._TabelaExcecaoAdmCollectionByIdTabela.Query.IdTabela == this.IdTabela);
						this._TabelaExcecaoAdmCollectionByIdTabela.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaExcecaoAdmCollectionByIdTabela.fks.Add(TabelaExcecaoAdmMetadata.ColumnNames.IdTabela, this.IdTabela);
					}
				}

				return this._TabelaExcecaoAdmCollectionByIdTabela;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaExcecaoAdmCollectionByIdTabela != null) 
				{ 
					this.RemovePostSave("TabelaExcecaoAdmCollectionByIdTabela"); 
					this._TabelaExcecaoAdmCollectionByIdTabela = null;
					
				} 
			} 			
		}

		private TabelaExcecaoAdmCollection _TabelaExcecaoAdmCollectionByIdTabela;
		#endregion

				
		#region TabelaExpurgoAdministracaoCollectionByIdTabela - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_TabelaExpurgoAdministracao_TabelaTaxaAdministracao
		/// </summary>

		[XmlIgnore]
		public TabelaExpurgoAdministracaoCollection TabelaExpurgoAdministracaoCollectionByIdTabela
		{
			get
			{
				if(this._TabelaExpurgoAdministracaoCollectionByIdTabela == null)
				{
					this._TabelaExpurgoAdministracaoCollectionByIdTabela = new TabelaExpurgoAdministracaoCollection();
					this._TabelaExpurgoAdministracaoCollectionByIdTabela.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaExpurgoAdministracaoCollectionByIdTabela", this._TabelaExpurgoAdministracaoCollectionByIdTabela);
				
					if(this.IdTabela != null)
					{
						this._TabelaExpurgoAdministracaoCollectionByIdTabela.Query.Where(this._TabelaExpurgoAdministracaoCollectionByIdTabela.Query.IdTabela == this.IdTabela);
						this._TabelaExpurgoAdministracaoCollectionByIdTabela.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaExpurgoAdministracaoCollectionByIdTabela.fks.Add(TabelaExpurgoAdministracaoMetadata.ColumnNames.IdTabela, this.IdTabela);
					}
				}

				return this._TabelaExpurgoAdministracaoCollectionByIdTabela;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaExpurgoAdministracaoCollectionByIdTabela != null) 
				{ 
					this.RemovePostSave("TabelaExpurgoAdministracaoCollectionByIdTabela"); 
					this._TabelaExpurgoAdministracaoCollectionByIdTabela = null;
					
				} 
			} 			
		}

		private TabelaExpurgoAdministracaoCollection _TabelaExpurgoAdministracaoCollectionByIdTabela;
		#endregion

				
		#region TaxaGlobalCollectionByIdTabelaGlobal - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TaxaGlobal_TabelaTaxaAdministracao_FK1
		/// </summary>

		[XmlIgnore]
		public TaxaGlobalCollection TaxaGlobalCollectionByIdTabelaGlobal
		{
			get
			{
				if(this._TaxaGlobalCollectionByIdTabelaGlobal == null)
				{
					this._TaxaGlobalCollectionByIdTabelaGlobal = new TaxaGlobalCollection();
					this._TaxaGlobalCollectionByIdTabelaGlobal.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TaxaGlobalCollectionByIdTabelaGlobal", this._TaxaGlobalCollectionByIdTabelaGlobal);
				
					if(this.IdTabela != null)
					{
						this._TaxaGlobalCollectionByIdTabelaGlobal.Query.Where(this._TaxaGlobalCollectionByIdTabelaGlobal.Query.IdTabelaGlobal == this.IdTabela);
						this._TaxaGlobalCollectionByIdTabelaGlobal.Query.Load();

						// Auto-hookup Foreign Keys
						this._TaxaGlobalCollectionByIdTabelaGlobal.fks.Add(TaxaGlobalMetadata.ColumnNames.IdTabelaGlobal, this.IdTabela);
					}
				}

				return this._TaxaGlobalCollectionByIdTabelaGlobal;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TaxaGlobalCollectionByIdTabelaGlobal != null) 
				{ 
					this.RemovePostSave("TaxaGlobalCollectionByIdTabelaGlobal"); 
					this._TaxaGlobalCollectionByIdTabelaGlobal = null;
					
				} 
			} 			
		}

		private TaxaGlobalCollection _TaxaGlobalCollectionByIdTabelaGlobal;
		#endregion

				
		#region TaxaGlobalCollectionByIdTabelaAssociada - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TaxaGlobal_TabelaTaxaAdministracao_FK2
		/// </summary>

		[XmlIgnore]
		public TaxaGlobalCollection TaxaGlobalCollectionByIdTabelaAssociada
		{
			get
			{
				if(this._TaxaGlobalCollectionByIdTabelaAssociada == null)
				{
					this._TaxaGlobalCollectionByIdTabelaAssociada = new TaxaGlobalCollection();
					this._TaxaGlobalCollectionByIdTabelaAssociada.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TaxaGlobalCollectionByIdTabelaAssociada", this._TaxaGlobalCollectionByIdTabelaAssociada);
				
					if(this.IdTabela != null)
					{
						this._TaxaGlobalCollectionByIdTabelaAssociada.Query.Where(this._TaxaGlobalCollectionByIdTabelaAssociada.Query.IdTabelaAssociada == this.IdTabela);
						this._TaxaGlobalCollectionByIdTabelaAssociada.Query.Load();

						// Auto-hookup Foreign Keys
						this._TaxaGlobalCollectionByIdTabelaAssociada.fks.Add(TaxaGlobalMetadata.ColumnNames.IdTabelaAssociada, this.IdTabela);
					}
				}

				return this._TaxaGlobalCollectionByIdTabelaAssociada;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TaxaGlobalCollectionByIdTabelaAssociada != null) 
				{ 
					this.RemovePostSave("TaxaGlobalCollectionByIdTabelaAssociada"); 
					this._TaxaGlobalCollectionByIdTabelaAssociada = null;
					
				} 
			} 			
		}

		private TaxaGlobalCollection _TaxaGlobalCollectionByIdTabelaAssociada;
		#endregion

				
		#region UpToCadastroTaxaAdministracaoByIdCadastro - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - CadastroTaxaAdministracao_TabelaTaxaAdministracao_FK1
		/// </summary>

		[XmlIgnore]
		public CadastroTaxaAdministracao UpToCadastroTaxaAdministracaoByIdCadastro
		{
			get
			{
				if(this._UpToCadastroTaxaAdministracaoByIdCadastro == null
					&& IdCadastro != null					)
				{
					this._UpToCadastroTaxaAdministracaoByIdCadastro = new CadastroTaxaAdministracao();
					this._UpToCadastroTaxaAdministracaoByIdCadastro.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCadastroTaxaAdministracaoByIdCadastro", this._UpToCadastroTaxaAdministracaoByIdCadastro);
					this._UpToCadastroTaxaAdministracaoByIdCadastro.Query.Where(this._UpToCadastroTaxaAdministracaoByIdCadastro.Query.IdCadastro == this.IdCadastro);
					this._UpToCadastroTaxaAdministracaoByIdCadastro.Query.Load();
				}

				return this._UpToCadastroTaxaAdministracaoByIdCadastro;
			}
			
			set
			{
				this.RemovePreSave("UpToCadastroTaxaAdministracaoByIdCadastro");
				

				if(value == null)
				{
					this.IdCadastro = null;
					this._UpToCadastroTaxaAdministracaoByIdCadastro = null;
				}
				else
				{
					this.IdCadastro = value.IdCadastro;
					this._UpToCadastroTaxaAdministracaoByIdCadastro = value;
					this.SetPreSave("UpToCadastroTaxaAdministracaoByIdCadastro", this._UpToCadastroTaxaAdministracaoByIdCadastro);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContabRoteiroByIdEventoProvisao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabRoteiro_TabelaTaxaAdministracao_FK1
		/// </summary>

		[XmlIgnore]
		public ContabRoteiro UpToContabRoteiroByIdEventoProvisao
		{
			get
			{
				if(this._UpToContabRoteiroByIdEventoProvisao == null
					&& IdEventoProvisao != null					)
				{
					this._UpToContabRoteiroByIdEventoProvisao = new ContabRoteiro();
					this._UpToContabRoteiroByIdEventoProvisao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabRoteiroByIdEventoProvisao", this._UpToContabRoteiroByIdEventoProvisao);
					this._UpToContabRoteiroByIdEventoProvisao.Query.Where(this._UpToContabRoteiroByIdEventoProvisao.Query.IdEvento == this.IdEventoProvisao);
					this._UpToContabRoteiroByIdEventoProvisao.Query.Load();
				}

				return this._UpToContabRoteiroByIdEventoProvisao;
			}
			
			set
			{
				this.RemovePreSave("UpToContabRoteiroByIdEventoProvisao");
				

				if(value == null)
				{
					this.IdEventoProvisao = null;
					this._UpToContabRoteiroByIdEventoProvisao = null;
				}
				else
				{
					this.IdEventoProvisao = value.IdEvento;
					this._UpToContabRoteiroByIdEventoProvisao = value;
					this.SetPreSave("UpToContabRoteiroByIdEventoProvisao", this._UpToContabRoteiroByIdEventoProvisao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContabRoteiroByIdEventoPagamento - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabRoteiro_TabelaTaxaAdministracao_FK2
		/// </summary>

		[XmlIgnore]
		public ContabRoteiro UpToContabRoteiroByIdEventoPagamento
		{
			get
			{
				if(this._UpToContabRoteiroByIdEventoPagamento == null
					&& IdEventoPagamento != null					)
				{
					this._UpToContabRoteiroByIdEventoPagamento = new ContabRoteiro();
					this._UpToContabRoteiroByIdEventoPagamento.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabRoteiroByIdEventoPagamento", this._UpToContabRoteiroByIdEventoPagamento);
					this._UpToContabRoteiroByIdEventoPagamento.Query.Where(this._UpToContabRoteiroByIdEventoPagamento.Query.IdEvento == this.IdEventoPagamento);
					this._UpToContabRoteiroByIdEventoPagamento.Query.Load();
				}

				return this._UpToContabRoteiroByIdEventoPagamento;
			}
			
			set
			{
				this.RemovePreSave("UpToContabRoteiroByIdEventoPagamento");
				

				if(value == null)
				{
					this.IdEventoPagamento = null;
					this._UpToContabRoteiroByIdEventoPagamento = null;
				}
				else
				{
					this.IdEventoPagamento = value.IdEvento;
					this._UpToContabRoteiroByIdEventoPagamento = value;
					this.SetPreSave("UpToContabRoteiroByIdEventoPagamento", this._UpToContabRoteiroByIdEventoPagamento);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela", typeof(CalculoAdministracaoAssociadaHistoricoCollection), new CalculoAdministracaoAssociadaHistorico()));
			props.Add(new esPropertyDescriptor(this, "CalculoAdministracaoHistoricoCollectionByIdTabela", typeof(CalculoAdministracaoHistoricoCollection), new CalculoAdministracaoHistorico()));
			props.Add(new esPropertyDescriptor(this, "TabelaEscalonamentoAdmCollectionByIdTabela", typeof(TabelaEscalonamentoAdmCollection), new TabelaEscalonamentoAdm()));
			props.Add(new esPropertyDescriptor(this, "TabelaExcecaoAdmCollectionByIdTabela", typeof(TabelaExcecaoAdmCollection), new TabelaExcecaoAdm()));
			props.Add(new esPropertyDescriptor(this, "TabelaExpurgoAdministracaoCollectionByIdTabela", typeof(TabelaExpurgoAdministracaoCollection), new TabelaExpurgoAdministracao()));
			props.Add(new esPropertyDescriptor(this, "TaxaGlobalCollectionByIdTabelaGlobal", typeof(TaxaGlobalCollection), new TaxaGlobal()));
			props.Add(new esPropertyDescriptor(this, "TaxaGlobalCollectionByIdTabelaAssociada", typeof(TaxaGlobalCollection), new TaxaGlobal()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToCadastroTaxaAdministracaoByIdCadastro != null)
			{
				this.IdCadastro = this._UpToCadastroTaxaAdministracaoByIdCadastro.IdCadastro;
			}
			if(!this.es.IsDeleted && this._UpToContabRoteiroByIdEventoProvisao != null)
			{
				this.IdEventoProvisao = this._UpToContabRoteiroByIdEventoProvisao.IdEvento;
			}
			if(!this.es.IsDeleted && this._UpToContabRoteiroByIdEventoPagamento != null)
			{
				this.IdEventoPagamento = this._UpToContabRoteiroByIdEventoPagamento.IdEvento;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela != null)
			{
				foreach(CalculoAdministracaoAssociadaHistorico obj in this._CalculoAdministracaoAssociadaHistoricoCollectionByIdTabela)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTabela = this.IdTabela;
					}
				}
			}
			if(this._CalculoAdministracaoHistoricoCollectionByIdTabela != null)
			{
				foreach(CalculoAdministracaoHistorico obj in this._CalculoAdministracaoHistoricoCollectionByIdTabela)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTabela = this.IdTabela;
					}
				}
			}
			if(this._TabelaEscalonamentoAdmCollectionByIdTabela != null)
			{
				foreach(TabelaEscalonamentoAdm obj in this._TabelaEscalonamentoAdmCollectionByIdTabela)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTabela = this.IdTabela;
					}
				}
			}
			if(this._TabelaExcecaoAdmCollectionByIdTabela != null)
			{
				foreach(TabelaExcecaoAdm obj in this._TabelaExcecaoAdmCollectionByIdTabela)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTabela = this.IdTabela;
					}
				}
			}
			if(this._TabelaExpurgoAdministracaoCollectionByIdTabela != null)
			{
				foreach(TabelaExpurgoAdministracao obj in this._TabelaExpurgoAdministracaoCollectionByIdTabela)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTabela = this.IdTabela;
					}
				}
			}
			if(this._TaxaGlobalCollectionByIdTabelaGlobal != null)
			{
				foreach(TaxaGlobal obj in this._TaxaGlobalCollectionByIdTabelaGlobal)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTabelaGlobal = this.IdTabela;
					}
				}
			}
			if(this._TaxaGlobalCollectionByIdTabelaAssociada != null)
			{
				foreach(TaxaGlobal obj in this._TaxaGlobalCollectionByIdTabelaAssociada)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTabelaAssociada = this.IdTabela;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
			if(this._CalculoAdministracao != null)
			{
				if(this._CalculoAdministracao.es.IsAdded)
				{
					this._CalculoAdministracao.IdTabela = this.IdTabela;
				}
			}
			if(this._CalculoAdministracaoAssociada != null)
			{
				if(this._CalculoAdministracaoAssociada.es.IsAdded)
				{
					this._CalculoAdministracaoAssociada.IdTabela = this.IdTabela;
				}
			}
		}
		
	}



	[Serializable]
	abstract public class esTabelaTaxaAdministracaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaTaxaAdministracaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoCalculo
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.TipoCalculo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoApropriacao
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.TipoApropriacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem BaseApuracao
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.BaseApuracao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem BaseAno
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.BaseAno, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TipoLimitePL
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.TipoLimitePL, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ValorLimite
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.ValorLimite, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorFixoTotal
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.ValorFixoTotal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ContagemDias
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.ContagemDias, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ValorMinimo
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.ValorMinimo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorMaximo
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.ValorMaximo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumeroMesesRenovacao
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.NumeroMesesRenovacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem NumeroDiasPagamento
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.NumeroDiasPagamento, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ImpactaPL
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.ImpactaPL, esSystemType.String);
			}
		} 
		
		public esQueryItem IdCadastro
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.IdCadastro, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataFim
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.DataFim, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdEventoProvisao
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.IdEventoProvisao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEventoPagamento
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.IdEventoPagamento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoGrupo
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.TipoGrupo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoAtivo
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.CodigoAtivo, esSystemType.String);
			}
		} 
		
		public esQueryItem EscalonaProporcional
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.EscalonaProporcional, esSystemType.String);
			}
		} 
		
		public esQueryItem Consolidadora
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaAdministracaoMetadata.ColumnNames.Consolidadora, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaTaxaAdministracaoCollection")]
	public partial class TabelaTaxaAdministracaoCollection : esTabelaTaxaAdministracaoCollection, IEnumerable<TabelaTaxaAdministracao>
	{
		public TabelaTaxaAdministracaoCollection()
		{

		}
		
		public static implicit operator List<TabelaTaxaAdministracao>(TabelaTaxaAdministracaoCollection coll)
		{
			List<TabelaTaxaAdministracao> list = new List<TabelaTaxaAdministracao>();
			
			foreach (TabelaTaxaAdministracao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaTaxaAdministracaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaTaxaAdministracaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaTaxaAdministracao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaTaxaAdministracao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaTaxaAdministracaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaTaxaAdministracaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaTaxaAdministracaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaTaxaAdministracao AddNew()
		{
			TabelaTaxaAdministracao entity = base.AddNewEntity() as TabelaTaxaAdministracao;
			
			return entity;
		}

		public TabelaTaxaAdministracao FindByPrimaryKey(System.Int32 idTabela)
		{
			return base.FindByPrimaryKey(idTabela) as TabelaTaxaAdministracao;
		}


		#region IEnumerable<TabelaTaxaAdministracao> Members

		IEnumerator<TabelaTaxaAdministracao> IEnumerable<TabelaTaxaAdministracao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaTaxaAdministracao;
			}
		}

		#endregion
		
		private TabelaTaxaAdministracaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaTaxaAdministracao' table
	/// </summary>

	[Serializable]
	public partial class TabelaTaxaAdministracao : esTabelaTaxaAdministracao
	{
		public TabelaTaxaAdministracao()
		{

		}
	
		public TabelaTaxaAdministracao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaTaxaAdministracaoMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaTaxaAdministracaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaTaxaAdministracaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaTaxaAdministracaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaTaxaAdministracaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaTaxaAdministracaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaTaxaAdministracaoQuery query;
	}



	[Serializable]
	public partial class TabelaTaxaAdministracaoQuery : esTabelaTaxaAdministracaoQuery
	{
		public TabelaTaxaAdministracaoQuery()
		{

		}		
		
		public TabelaTaxaAdministracaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaTaxaAdministracaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaTaxaAdministracaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.IdTabela, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.DataReferencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.IdCarteira, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.TipoCalculo, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.TipoCalculo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.TipoApropriacao, 4, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.TipoApropriacao;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.Taxa, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 16;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.BaseApuracao, 6, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.BaseApuracao;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.BaseAno, 7, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.BaseAno;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.TipoLimitePL, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.TipoLimitePL;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.ValorLimite, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.ValorLimite;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.ValorFixoTotal, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.ValorFixoTotal;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.ContagemDias, 11, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.ContagemDias;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.ValorMinimo, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.ValorMinimo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.ValorMaximo, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.ValorMaximo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.NumeroMesesRenovacao, 14, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.NumeroMesesRenovacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.NumeroDiasPagamento, 15, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.NumeroDiasPagamento;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.ImpactaPL, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.ImpactaPL;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.IdCadastro, 17, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.IdCadastro;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.DataFim, 18, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.DataFim;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.IdEventoProvisao, 19, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.IdEventoProvisao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.IdEventoPagamento, 20, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.IdEventoPagamento;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.TipoGrupo, 21, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.TipoGrupo;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.CodigoAtivo, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.CodigoAtivo;
			c.CharacterMaxLength = 8;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.EscalonaProporcional, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.EscalonaProporcional;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaAdministracaoMetadata.ColumnNames.Consolidadora, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaTaxaAdministracaoMetadata.PropertyNames.Consolidadora;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaTaxaAdministracaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCarteira = "IdCarteira";
			 public const string TipoCalculo = "TipoCalculo";
			 public const string TipoApropriacao = "TipoApropriacao";
			 public const string Taxa = "Taxa";
			 public const string BaseApuracao = "BaseApuracao";
			 public const string BaseAno = "BaseAno";
			 public const string TipoLimitePL = "TipoLimitePL";
			 public const string ValorLimite = "ValorLimite";
			 public const string ValorFixoTotal = "ValorFixoTotal";
			 public const string ContagemDias = "ContagemDias";
			 public const string ValorMinimo = "ValorMinimo";
			 public const string ValorMaximo = "ValorMaximo";
			 public const string NumeroMesesRenovacao = "NumeroMesesRenovacao";
			 public const string NumeroDiasPagamento = "NumeroDiasPagamento";
			 public const string ImpactaPL = "ImpactaPL";
			 public const string IdCadastro = "IdCadastro";
			 public const string DataFim = "DataFim";
			 public const string IdEventoProvisao = "IdEventoProvisao";
			 public const string IdEventoPagamento = "IdEventoPagamento";
			 public const string TipoGrupo = "TipoGrupo";
			 public const string CodigoAtivo = "CodigoAtivo";
			 public const string EscalonaProporcional = "EscalonaProporcional";
			 public const string Consolidadora = "Consolidadora";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCarteira = "IdCarteira";
			 public const string TipoCalculo = "TipoCalculo";
			 public const string TipoApropriacao = "TipoApropriacao";
			 public const string Taxa = "Taxa";
			 public const string BaseApuracao = "BaseApuracao";
			 public const string BaseAno = "BaseAno";
			 public const string TipoLimitePL = "TipoLimitePL";
			 public const string ValorLimite = "ValorLimite";
			 public const string ValorFixoTotal = "ValorFixoTotal";
			 public const string ContagemDias = "ContagemDias";
			 public const string ValorMinimo = "ValorMinimo";
			 public const string ValorMaximo = "ValorMaximo";
			 public const string NumeroMesesRenovacao = "NumeroMesesRenovacao";
			 public const string NumeroDiasPagamento = "NumeroDiasPagamento";
			 public const string ImpactaPL = "ImpactaPL";
			 public const string IdCadastro = "IdCadastro";
			 public const string DataFim = "DataFim";
			 public const string IdEventoProvisao = "IdEventoProvisao";
			 public const string IdEventoPagamento = "IdEventoPagamento";
			 public const string TipoGrupo = "TipoGrupo";
			 public const string CodigoAtivo = "CodigoAtivo";
			 public const string EscalonaProporcional = "EscalonaProporcional";
			 public const string Consolidadora = "Consolidadora";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaTaxaAdministracaoMetadata))
			{
				if(TabelaTaxaAdministracaoMetadata.mapDelegates == null)
				{
					TabelaTaxaAdministracaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaTaxaAdministracaoMetadata.meta == null)
				{
					TabelaTaxaAdministracaoMetadata.meta = new TabelaTaxaAdministracaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoCalculo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoApropriacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("BaseApuracao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("BaseAno", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TipoLimitePL", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ValorLimite", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorFixoTotal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ContagemDias", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ValorMinimo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorMaximo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("NumeroMesesRenovacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("NumeroDiasPagamento", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ImpactaPL", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdCadastro", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataFim", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdEventoProvisao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEventoPagamento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoGrupo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoAtivo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("EscalonaProporcional", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Consolidadora", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "TabelaTaxaAdministracao";
				meta.Destination = "TabelaTaxaAdministracao";
				
				meta.spInsert = "proc_TabelaTaxaAdministracaoInsert";				
				meta.spUpdate = "proc_TabelaTaxaAdministracaoUpdate";		
				meta.spDelete = "proc_TabelaTaxaAdministracaoDelete";
				meta.spLoadAll = "proc_TabelaTaxaAdministracaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaTaxaAdministracaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaTaxaAdministracaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
