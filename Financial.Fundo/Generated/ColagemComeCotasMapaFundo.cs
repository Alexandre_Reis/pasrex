/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 22/01/2015 16:27:42
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esColagemComeCotasMapaFundoCollection : esEntityCollection
	{
		public esColagemComeCotasMapaFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ColagemComeCotasMapaFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esColagemComeCotasMapaFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esColagemComeCotasMapaFundoQuery);
		}
		#endregion
		
		virtual public ColagemComeCotasMapaFundo DetachEntity(ColagemComeCotasMapaFundo entity)
		{
			return base.DetachEntity(entity) as ColagemComeCotasMapaFundo;
		}
		
		virtual public ColagemComeCotasMapaFundo AttachEntity(ColagemComeCotasMapaFundo entity)
		{
			return base.AttachEntity(entity) as ColagemComeCotasMapaFundo;
		}
		
		virtual public void Combine(ColagemComeCotasMapaFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ColagemComeCotasMapaFundo this[int index]
		{
			get
			{
				return base[index] as ColagemComeCotasMapaFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ColagemComeCotasMapaFundo);
		}
	}



	[Serializable]
	abstract public class esColagemComeCotasMapaFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esColagemComeCotasMapaFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esColagemComeCotasMapaFundo()
		{

		}

		public esColagemComeCotasMapaFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idColagemComeCotasMapaFundo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemComeCotasMapaFundo);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemComeCotasMapaFundo);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idColagemComeCotasMapaFundo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemComeCotasMapaFundo);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemComeCotasMapaFundo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idColagemComeCotasMapaFundo)
		{
			esColagemComeCotasMapaFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdColagemComeCotasMapaFundo == idColagemComeCotasMapaFundo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idColagemComeCotasMapaFundo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdColagemComeCotasMapaFundo",idColagemComeCotasMapaFundo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdColagemComeCotasMapaFundo": this.str.IdColagemComeCotasMapaFundo = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "DetalheImportado": this.str.DetalheImportado = (string)value; break;							
						case "IdOperacaoComeCotasVinculada": this.str.IdOperacaoComeCotasVinculada = (string)value; break;							
						case "Participacao": this.str.Participacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdColagemComeCotasMapaFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemComeCotasMapaFundo = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "IdOperacaoComeCotasVinculada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoComeCotasVinculada = (System.Int32?)value;
							break;
						
						case "Participacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Participacao = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ColagemComeCotasMapaFundo.IdColagemComeCotasMapaFundo
		/// </summary>
		virtual public System.Int32? IdColagemComeCotasMapaFundo
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasMapaFundoMetadata.ColumnNames.IdColagemComeCotasMapaFundo);
			}
			
			set
			{
				base.SetSystemInt32(ColagemComeCotasMapaFundoMetadata.ColumnNames.IdColagemComeCotasMapaFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasMapaFundo.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(ColagemComeCotasMapaFundoMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemComeCotasMapaFundoMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasMapaFundo.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasMapaFundoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(ColagemComeCotasMapaFundoMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasMapaFundo.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasMapaFundoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(ColagemComeCotasMapaFundoMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasMapaFundo.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(ColagemComeCotasMapaFundoMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemComeCotasMapaFundoMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasMapaFundo.DetalheImportado
		/// </summary>
		virtual public System.String DetalheImportado
		{
			get
			{
				return base.GetSystemString(ColagemComeCotasMapaFundoMetadata.ColumnNames.DetalheImportado);
			}
			
			set
			{
				base.SetSystemString(ColagemComeCotasMapaFundoMetadata.ColumnNames.DetalheImportado, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasMapaFundo.IdOperacaoComeCotasVinculada
		/// </summary>
		virtual public System.Int32? IdOperacaoComeCotasVinculada
		{
			get
			{
				return base.GetSystemInt32(ColagemComeCotasMapaFundoMetadata.ColumnNames.IdOperacaoComeCotasVinculada);
			}
			
			set
			{
				base.SetSystemInt32(ColagemComeCotasMapaFundoMetadata.ColumnNames.IdOperacaoComeCotasVinculada, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemComeCotasMapaFundo.Participacao
		/// </summary>
		virtual public System.Decimal? Participacao
		{
			get
			{
				return base.GetSystemDecimal(ColagemComeCotasMapaFundoMetadata.ColumnNames.Participacao);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemComeCotasMapaFundoMetadata.ColumnNames.Participacao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esColagemComeCotasMapaFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdColagemComeCotasMapaFundo
			{
				get
				{
					System.Int32? data = entity.IdColagemComeCotasMapaFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemComeCotasMapaFundo = null;
					else entity.IdColagemComeCotasMapaFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DetalheImportado
			{
				get
				{
					System.String data = entity.DetalheImportado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DetalheImportado = null;
					else entity.DetalheImportado = Convert.ToString(value);
				}
			}
				
			public System.String IdOperacaoComeCotasVinculada
			{
				get
				{
					System.Int32? data = entity.IdOperacaoComeCotasVinculada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoComeCotasVinculada = null;
					else entity.IdOperacaoComeCotasVinculada = Convert.ToInt32(value);
				}
			}
				
			public System.String Participacao
			{
				get
				{
					System.Decimal? data = entity.Participacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Participacao = null;
					else entity.Participacao = Convert.ToDecimal(value);
				}
			}
			

			private esColagemComeCotasMapaFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esColagemComeCotasMapaFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esColagemComeCotasMapaFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ColagemComeCotasMapaFundo : esColagemComeCotasMapaFundo
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esColagemComeCotasMapaFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ColagemComeCotasMapaFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdColagemComeCotasMapaFundo
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMapaFundoMetadata.ColumnNames.IdColagemComeCotasMapaFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMapaFundoMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMapaFundoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMapaFundoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMapaFundoMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DetalheImportado
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMapaFundoMetadata.ColumnNames.DetalheImportado, esSystemType.String);
			}
		} 
		
		public esQueryItem IdOperacaoComeCotasVinculada
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMapaFundoMetadata.ColumnNames.IdOperacaoComeCotasVinculada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Participacao
		{
			get
			{
				return new esQueryItem(this, ColagemComeCotasMapaFundoMetadata.ColumnNames.Participacao, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ColagemComeCotasMapaFundoCollection")]
	public partial class ColagemComeCotasMapaFundoCollection : esColagemComeCotasMapaFundoCollection, IEnumerable<ColagemComeCotasMapaFundo>
	{
		public ColagemComeCotasMapaFundoCollection()
		{

		}
		
		public static implicit operator List<ColagemComeCotasMapaFundo>(ColagemComeCotasMapaFundoCollection coll)
		{
			List<ColagemComeCotasMapaFundo> list = new List<ColagemComeCotasMapaFundo>();
			
			foreach (ColagemComeCotasMapaFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ColagemComeCotasMapaFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemComeCotasMapaFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ColagemComeCotasMapaFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ColagemComeCotasMapaFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ColagemComeCotasMapaFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemComeCotasMapaFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ColagemComeCotasMapaFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ColagemComeCotasMapaFundo AddNew()
		{
			ColagemComeCotasMapaFundo entity = base.AddNewEntity() as ColagemComeCotasMapaFundo;
			
			return entity;
		}

		public ColagemComeCotasMapaFundo FindByPrimaryKey(System.Int32 idColagemComeCotasMapaFundo)
		{
			return base.FindByPrimaryKey(idColagemComeCotasMapaFundo) as ColagemComeCotasMapaFundo;
		}


		#region IEnumerable<ColagemComeCotasMapaFundo> Members

		IEnumerator<ColagemComeCotasMapaFundo> IEnumerable<ColagemComeCotasMapaFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ColagemComeCotasMapaFundo;
			}
		}

		#endregion
		
		private ColagemComeCotasMapaFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ColagemComeCotasMapaFundo' table
	/// </summary>

	[Serializable]
	public partial class ColagemComeCotasMapaFundo : esColagemComeCotasMapaFundo
	{
		public ColagemComeCotasMapaFundo()
		{

		}
	
		public ColagemComeCotasMapaFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ColagemComeCotasMapaFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esColagemComeCotasMapaFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemComeCotasMapaFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ColagemComeCotasMapaFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemComeCotasMapaFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ColagemComeCotasMapaFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ColagemComeCotasMapaFundoQuery query;
	}



	[Serializable]
	public partial class ColagemComeCotasMapaFundoQuery : esColagemComeCotasMapaFundoQuery
	{
		public ColagemComeCotasMapaFundoQuery()
		{

		}		
		
		public ColagemComeCotasMapaFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ColagemComeCotasMapaFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ColagemComeCotasMapaFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ColagemComeCotasMapaFundoMetadata.ColumnNames.IdColagemComeCotasMapaFundo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasMapaFundoMetadata.PropertyNames.IdColagemComeCotasMapaFundo;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMapaFundoMetadata.ColumnNames.DataReferencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemComeCotasMapaFundoMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMapaFundoMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasMapaFundoMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMapaFundoMetadata.ColumnNames.IdCarteira, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasMapaFundoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMapaFundoMetadata.ColumnNames.DataConversao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemComeCotasMapaFundoMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMapaFundoMetadata.ColumnNames.DetalheImportado, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = ColagemComeCotasMapaFundoMetadata.PropertyNames.DetalheImportado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMapaFundoMetadata.ColumnNames.IdOperacaoComeCotasVinculada, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemComeCotasMapaFundoMetadata.PropertyNames.IdOperacaoComeCotasVinculada;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemComeCotasMapaFundoMetadata.ColumnNames.Participacao, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemComeCotasMapaFundoMetadata.PropertyNames.Participacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ColagemComeCotasMapaFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdColagemComeCotasMapaFundo = "IdColagemComeCotasMapaFundo";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataConversao = "DataConversao";
			 public const string DetalheImportado = "DetalheImportado";
			 public const string IdOperacaoComeCotasVinculada = "IdOperacaoComeCotasVinculada";
			 public const string Participacao = "Participacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdColagemComeCotasMapaFundo = "IdColagemComeCotasMapaFundo";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataConversao = "DataConversao";
			 public const string DetalheImportado = "DetalheImportado";
			 public const string IdOperacaoComeCotasVinculada = "IdOperacaoComeCotasVinculada";
			 public const string Participacao = "Participacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ColagemComeCotasMapaFundoMetadata))
			{
				if(ColagemComeCotasMapaFundoMetadata.mapDelegates == null)
				{
					ColagemComeCotasMapaFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ColagemComeCotasMapaFundoMetadata.meta == null)
				{
					ColagemComeCotasMapaFundoMetadata.meta = new ColagemComeCotasMapaFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdColagemComeCotasMapaFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DetalheImportado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdOperacaoComeCotasVinculada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Participacao", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "ColagemComeCotasMapaFundo";
				meta.Destination = "ColagemComeCotasMapaFundo";
				
				meta.spInsert = "proc_ColagemComeCotasMapaFundoInsert";				
				meta.spUpdate = "proc_ColagemComeCotasMapaFundoUpdate";		
				meta.spDelete = "proc_ColagemComeCotasMapaFundoDelete";
				meta.spLoadAll = "proc_ColagemComeCotasMapaFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_ColagemComeCotasMapaFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ColagemComeCotasMapaFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
