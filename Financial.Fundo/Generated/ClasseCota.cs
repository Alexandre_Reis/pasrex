/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 31/07/2015 17:35:54
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esClasseCotaCollection : esEntityCollection
	{
		public esClasseCotaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ClasseCotaCollection";
		}

		#region Query Logic
		protected void InitQuery(esClasseCotaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esClasseCotaQuery);
		}
		#endregion
		
		virtual public ClasseCota DetachEntity(ClasseCota entity)
		{
			return base.DetachEntity(entity) as ClasseCota;
		}
		
		virtual public ClasseCota AttachEntity(ClasseCota entity)
		{
			return base.AttachEntity(entity) as ClasseCota;
		}
		
		virtual public void Combine(ClasseCotaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ClasseCota this[int index]
		{
			get
			{
				return base[index] as ClasseCota;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ClasseCota);
		}
	}



	[Serializable]
	abstract public class esClasseCota : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esClasseCotaQuery GetDynamicQuery()
		{
			return null;
		}

		public esClasseCota()
		{

		}

		public esClasseCota(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idClasseCota)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idClasseCota);
			else
				return LoadByPrimaryKeyStoredProcedure(idClasseCota);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idClasseCota)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idClasseCota);
			else
				return LoadByPrimaryKeyStoredProcedure(idClasseCota);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idClasseCota)
		{
			esClasseCotaQuery query = this.GetDynamicQuery();
			query.Where(query.IdClasseCota == idClasseCota);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idClasseCota)
		{
			esParameters parms = new esParameters();
			parms.Add("IdClasseCota",idClasseCota);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdClasseCota": this.str.IdClasseCota = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdClasseCota":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClasseCota = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ClasseCota.IdClasseCota
		/// </summary>
		virtual public System.Int32? IdClasseCota
		{
			get
			{
				return base.GetSystemInt32(ClasseCotaMetadata.ColumnNames.IdClasseCota);
			}
			
			set
			{
				base.SetSystemInt32(ClasseCotaMetadata.ColumnNames.IdClasseCota, value);
			}
		}
		
		/// <summary>
		/// Maps to ClasseCota.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(ClasseCotaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(ClasseCotaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esClasseCota entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdClasseCota
			{
				get
				{
					System.Int32? data = entity.IdClasseCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClasseCota = null;
					else entity.IdClasseCota = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esClasseCota entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esClasseCotaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esClasseCota can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ClasseCota : esClasseCota
	{

				
		#region CarteiraComplementoCollectionByIdClasseCota - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - CarteiraComplemento_ClasseCota_FK1
		/// </summary>

		[XmlIgnore]
		public CarteiraComplementoCollection CarteiraComplementoCollectionByIdClasseCota
		{
			get
			{
				if(this._CarteiraComplementoCollectionByIdClasseCota == null)
				{
					this._CarteiraComplementoCollectionByIdClasseCota = new CarteiraComplementoCollection();
					this._CarteiraComplementoCollectionByIdClasseCota.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CarteiraComplementoCollectionByIdClasseCota", this._CarteiraComplementoCollectionByIdClasseCota);
				
					if(this.IdClasseCota != null)
					{
						this._CarteiraComplementoCollectionByIdClasseCota.Query.Where(this._CarteiraComplementoCollectionByIdClasseCota.Query.IdClasseCota == this.IdClasseCota);
						this._CarteiraComplementoCollectionByIdClasseCota.Query.Load();

						// Auto-hookup Foreign Keys
						this._CarteiraComplementoCollectionByIdClasseCota.fks.Add(CarteiraComplementoMetadata.ColumnNames.IdClasseCota, this.IdClasseCota);
					}
				}

				return this._CarteiraComplementoCollectionByIdClasseCota;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CarteiraComplementoCollectionByIdClasseCota != null) 
				{ 
					this.RemovePostSave("CarteiraComplementoCollectionByIdClasseCota"); 
					this._CarteiraComplementoCollectionByIdClasseCota = null;
					
				} 
			} 			
		}

		private CarteiraComplementoCollection _CarteiraComplementoCollectionByIdClasseCota;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "CarteiraComplementoCollectionByIdClasseCota", typeof(CarteiraComplementoCollection), new CarteiraComplemento()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._CarteiraComplementoCollectionByIdClasseCota != null)
			{
				foreach(CarteiraComplemento obj in this._CarteiraComplementoCollectionByIdClasseCota)
				{
					if(obj.es.IsAdded)
					{
						obj.IdClasseCota = this.IdClasseCota;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esClasseCotaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ClasseCotaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdClasseCota
		{
			get
			{
				return new esQueryItem(this, ClasseCotaMetadata.ColumnNames.IdClasseCota, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, ClasseCotaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ClasseCotaCollection")]
	public partial class ClasseCotaCollection : esClasseCotaCollection, IEnumerable<ClasseCota>
	{
		public ClasseCotaCollection()
		{

		}
		
		public static implicit operator List<ClasseCota>(ClasseCotaCollection coll)
		{
			List<ClasseCota> list = new List<ClasseCota>();
			
			foreach (ClasseCota emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ClasseCotaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClasseCotaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ClasseCota(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ClasseCota();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ClasseCotaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClasseCotaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ClasseCotaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ClasseCota AddNew()
		{
			ClasseCota entity = base.AddNewEntity() as ClasseCota;
			
			return entity;
		}

		public ClasseCota FindByPrimaryKey(System.Int32 idClasseCota)
		{
			return base.FindByPrimaryKey(idClasseCota) as ClasseCota;
		}


		#region IEnumerable<ClasseCota> Members

		IEnumerator<ClasseCota> IEnumerable<ClasseCota>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ClasseCota;
			}
		}

		#endregion
		
		private ClasseCotaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ClasseCota' table
	/// </summary>

	[Serializable]
	public partial class ClasseCota : esClasseCota
	{
		public ClasseCota()
		{

		}
	
		public ClasseCota(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ClasseCotaMetadata.Meta();
			}
		}
		
		
		
		override protected esClasseCotaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClasseCotaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ClasseCotaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClasseCotaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ClasseCotaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ClasseCotaQuery query;
	}



	[Serializable]
	public partial class ClasseCotaQuery : esClasseCotaQuery
	{
		public ClasseCotaQuery()
		{

		}		
		
		public ClasseCotaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ClasseCotaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ClasseCotaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ClasseCotaMetadata.ColumnNames.IdClasseCota, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClasseCotaMetadata.PropertyNames.IdClasseCota;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClasseCotaMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ClasseCotaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ClasseCotaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdClasseCota = "IdClasseCota";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdClasseCota = "IdClasseCota";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ClasseCotaMetadata))
			{
				if(ClasseCotaMetadata.mapDelegates == null)
				{
					ClasseCotaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ClasseCotaMetadata.meta == null)
				{
					ClasseCotaMetadata.meta = new ClasseCotaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdClasseCota", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "ClasseCota";
				meta.Destination = "ClasseCota";
				
				meta.spInsert = "proc_ClasseCotaInsert";				
				meta.spUpdate = "proc_ClasseCotaUpdate";		
				meta.spDelete = "proc_ClasseCotaDelete";
				meta.spLoadAll = "proc_ClasseCotaLoadAll";
				meta.spLoadByPrimaryKey = "proc_ClasseCotaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ClasseCotaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
