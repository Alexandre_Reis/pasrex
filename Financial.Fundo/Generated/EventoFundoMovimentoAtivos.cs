/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/12/2014 10:03:07
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esEventoFundoMovimentoAtivosCollection : esEntityCollection
	{
		public esEventoFundoMovimentoAtivosCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EventoFundoMovimentoAtivosCollection";
		}

		#region Query Logic
		protected void InitQuery(esEventoFundoMovimentoAtivosQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEventoFundoMovimentoAtivosQuery);
		}
		#endregion
		
		virtual public EventoFundoMovimentoAtivos DetachEntity(EventoFundoMovimentoAtivos entity)
		{
			return base.DetachEntity(entity) as EventoFundoMovimentoAtivos;
		}
		
		virtual public EventoFundoMovimentoAtivos AttachEntity(EventoFundoMovimentoAtivos entity)
		{
			return base.AttachEntity(entity) as EventoFundoMovimentoAtivos;
		}
		
		virtual public void Combine(EventoFundoMovimentoAtivosCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EventoFundoMovimentoAtivos this[int index]
		{
			get
			{
				return base[index] as EventoFundoMovimentoAtivos;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EventoFundoMovimentoAtivos);
		}
	}



	[Serializable]
	abstract public class esEventoFundoMovimentoAtivos : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEventoFundoMovimentoAtivosQuery GetDynamicQuery()
		{
			return null;
		}

		public esEventoFundoMovimentoAtivos()
		{

		}

		public esEventoFundoMovimentoAtivos(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEventoFundo, System.Int32 idOperacao, System.Int32 tipoMercado)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEventoFundo, idOperacao, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(idEventoFundo, idOperacao, tipoMercado);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEventoFundo, System.Int32 idOperacao, System.Int32 tipoMercado)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEventoFundo, idOperacao, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(idEventoFundo, idOperacao, tipoMercado);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEventoFundo, System.Int32 idOperacao, System.Int32 tipoMercado)
		{
			esEventoFundoMovimentoAtivosQuery query = this.GetDynamicQuery();
			query.Where(query.IdEventoFundo == idEventoFundo, query.IdOperacao == idOperacao, query.TipoMercado == tipoMercado);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEventoFundo, System.Int32 idOperacao, System.Int32 tipoMercado)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEventoFundo",idEventoFundo);			parms.Add("IdOperacao",idOperacao);			parms.Add("TipoMercado",tipoMercado);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEventoFundo": this.str.IdEventoFundo = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEventoFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoFundo = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoMercado = (System.Int32?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EventoFundoMovimentoAtivos.IdEventoFundo
		/// </summary>
		virtual public System.Int32? IdEventoFundo
		{
			get
			{
				return base.GetSystemInt32(EventoFundoMovimentoAtivosMetadata.ColumnNames.IdEventoFundo);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoFundoMovimentoAtivosMetadata.ColumnNames.IdEventoFundo, value))
				{
					this._UpToEventoFundoByIdEventoFundo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoMovimentoAtivos.TipoMercado
		/// </summary>
		virtual public System.Int32? TipoMercado
		{
			get
			{
				return base.GetSystemInt32(EventoFundoMovimentoAtivosMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoMovimentoAtivosMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoMovimentoAtivos.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(EventoFundoMovimentoAtivosMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoMovimentoAtivosMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoMovimentoAtivos.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(EventoFundoMovimentoAtivosMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoMovimentoAtivosMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected EventoFundo _UpToEventoFundoByIdEventoFundo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEventoFundoMovimentoAtivos entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEventoFundo
			{
				get
				{
					System.Int32? data = entity.IdEventoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoFundo = null;
					else entity.IdEventoFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Int32? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
			

			private esEventoFundoMovimentoAtivos entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEventoFundoMovimentoAtivosQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEventoFundoMovimentoAtivos can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EventoFundoMovimentoAtivos : esEventoFundoMovimentoAtivos
	{

				
		#region UpToEventoFundoByIdEventoFundo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EventoFundoMovimentoAtivos_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundo UpToEventoFundoByIdEventoFundo
		{
			get
			{
				if(this._UpToEventoFundoByIdEventoFundo == null
					&& IdEventoFundo != null					)
				{
					this._UpToEventoFundoByIdEventoFundo = new EventoFundo();
					this._UpToEventoFundoByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Where(this._UpToEventoFundoByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Load();
				}

				return this._UpToEventoFundoByIdEventoFundo;
			}
			
			set
			{
				this.RemovePreSave("UpToEventoFundoByIdEventoFundo");
				

				if(value == null)
				{
					this.IdEventoFundo = null;
					this._UpToEventoFundoByIdEventoFundo = null;
				}
				else
				{
					this.IdEventoFundo = value.IdEventoFundo;
					this._UpToEventoFundoByIdEventoFundo = value;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEventoFundoByIdEventoFundo != null)
			{
				this.IdEventoFundo = this._UpToEventoFundoByIdEventoFundo.IdEventoFundo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEventoFundoMovimentoAtivosQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoMovimentoAtivosMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEventoFundo
		{
			get
			{
				return new esQueryItem(this, EventoFundoMovimentoAtivosMetadata.ColumnNames.IdEventoFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, EventoFundoMovimentoAtivosMetadata.ColumnNames.TipoMercado, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, EventoFundoMovimentoAtivosMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, EventoFundoMovimentoAtivosMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EventoFundoMovimentoAtivosCollection")]
	public partial class EventoFundoMovimentoAtivosCollection : esEventoFundoMovimentoAtivosCollection, IEnumerable<EventoFundoMovimentoAtivos>
	{
		public EventoFundoMovimentoAtivosCollection()
		{

		}
		
		public static implicit operator List<EventoFundoMovimentoAtivos>(EventoFundoMovimentoAtivosCollection coll)
		{
			List<EventoFundoMovimentoAtivos> list = new List<EventoFundoMovimentoAtivos>();
			
			foreach (EventoFundoMovimentoAtivos emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EventoFundoMovimentoAtivosMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoMovimentoAtivosQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EventoFundoMovimentoAtivos(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EventoFundoMovimentoAtivos();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EventoFundoMovimentoAtivosQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoMovimentoAtivosQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EventoFundoMovimentoAtivosQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EventoFundoMovimentoAtivos AddNew()
		{
			EventoFundoMovimentoAtivos entity = base.AddNewEntity() as EventoFundoMovimentoAtivos;
			
			return entity;
		}

		public EventoFundoMovimentoAtivos FindByPrimaryKey(System.Int32 idEventoFundo, System.Int32 idOperacao, System.Int32 tipoMercado)
		{
			return base.FindByPrimaryKey(idEventoFundo, idOperacao, tipoMercado) as EventoFundoMovimentoAtivos;
		}


		#region IEnumerable<EventoFundoMovimentoAtivos> Members

		IEnumerator<EventoFundoMovimentoAtivos> IEnumerable<EventoFundoMovimentoAtivos>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EventoFundoMovimentoAtivos;
			}
		}

		#endregion
		
		private EventoFundoMovimentoAtivosQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EventoFundoMovimentoAtivos' table
	/// </summary>

	[Serializable]
	public partial class EventoFundoMovimentoAtivos : esEventoFundoMovimentoAtivos
	{
		public EventoFundoMovimentoAtivos()
		{

		}
	
		public EventoFundoMovimentoAtivos(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoMovimentoAtivosMetadata.Meta();
			}
		}
		
		
		
		override protected esEventoFundoMovimentoAtivosQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoMovimentoAtivosQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EventoFundoMovimentoAtivosQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoMovimentoAtivosQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EventoFundoMovimentoAtivosQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EventoFundoMovimentoAtivosQuery query;
	}



	[Serializable]
	public partial class EventoFundoMovimentoAtivosQuery : esEventoFundoMovimentoAtivosQuery
	{
		public EventoFundoMovimentoAtivosQuery()
		{

		}		
		
		public EventoFundoMovimentoAtivosQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EventoFundoMovimentoAtivosMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EventoFundoMovimentoAtivosMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EventoFundoMovimentoAtivosMetadata.ColumnNames.IdEventoFundo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoMovimentoAtivosMetadata.PropertyNames.IdEventoFundo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoMovimentoAtivosMetadata.ColumnNames.TipoMercado, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoMovimentoAtivosMetadata.PropertyNames.TipoMercado;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoMovimentoAtivosMetadata.ColumnNames.IdOperacao, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoMovimentoAtivosMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoMovimentoAtivosMetadata.ColumnNames.IdCliente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoMovimentoAtivosMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EventoFundoMovimentoAtivosMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string TipoMercado = "TipoMercado";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string TipoMercado = "TipoMercado";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EventoFundoMovimentoAtivosMetadata))
			{
				if(EventoFundoMovimentoAtivosMetadata.mapDelegates == null)
				{
					EventoFundoMovimentoAtivosMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EventoFundoMovimentoAtivosMetadata.meta == null)
				{
					EventoFundoMovimentoAtivosMetadata.meta = new EventoFundoMovimentoAtivosMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEventoFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "EventoFundoMovimentoAtivos";
				meta.Destination = "EventoFundoMovimentoAtivos";
				
				meta.spInsert = "proc_EventoFundoMovimentoAtivosInsert";				
				meta.spUpdate = "proc_EventoFundoMovimentoAtivosUpdate";		
				meta.spDelete = "proc_EventoFundoMovimentoAtivosDelete";
				meta.spLoadAll = "proc_EventoFundoMovimentoAtivosLoadAll";
				meta.spLoadByPrimaryKey = "proc_EventoFundoMovimentoAtivosLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EventoFundoMovimentoAtivosMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
