/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:09 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		






		


		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esCadastroTaxaAdministracaoCollection : esEntityCollection
	{
		public esCadastroTaxaAdministracaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CadastroTaxaAdministracaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esCadastroTaxaAdministracaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCadastroTaxaAdministracaoQuery);
		}
		#endregion
		
		virtual public CadastroTaxaAdministracao DetachEntity(CadastroTaxaAdministracao entity)
		{
			return base.DetachEntity(entity) as CadastroTaxaAdministracao;
		}
		
		virtual public CadastroTaxaAdministracao AttachEntity(CadastroTaxaAdministracao entity)
		{
			return base.AttachEntity(entity) as CadastroTaxaAdministracao;
		}
		
		virtual public void Combine(CadastroTaxaAdministracaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CadastroTaxaAdministracao this[int index]
		{
			get
			{
				return base[index] as CadastroTaxaAdministracao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CadastroTaxaAdministracao);
		}
	}



	[Serializable]
	abstract public class esCadastroTaxaAdministracao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCadastroTaxaAdministracaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esCadastroTaxaAdministracao()
		{

		}

		public esCadastroTaxaAdministracao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCadastro)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCadastro);
			else
				return LoadByPrimaryKeyStoredProcedure(idCadastro);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCadastro)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCadastroTaxaAdministracaoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCadastro == idCadastro);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCadastro)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCadastro);
			else
				return LoadByPrimaryKeyStoredProcedure(idCadastro);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCadastro)
		{
			esCadastroTaxaAdministracaoQuery query = this.GetDynamicQuery();
			query.Where(query.IdCadastro == idCadastro);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCadastro)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCadastro",idCadastro);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCadastro": this.str.IdCadastro = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCadastro":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCadastro = (System.Int32?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Tipo = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CadastroTaxaAdministracao.IdCadastro
		/// </summary>
		virtual public System.Int32? IdCadastro
		{
			get
			{
				return base.GetSystemInt32(CadastroTaxaAdministracaoMetadata.ColumnNames.IdCadastro);
			}
			
			set
			{
				base.SetSystemInt32(CadastroTaxaAdministracaoMetadata.ColumnNames.IdCadastro, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroTaxaAdministracao.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(CadastroTaxaAdministracaoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(CadastroTaxaAdministracaoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to CadastroTaxaAdministracao.Tipo
		/// </summary>
		virtual public System.Byte? Tipo
		{
			get
			{
				return base.GetSystemByte(CadastroTaxaAdministracaoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemByte(CadastroTaxaAdministracaoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCadastroTaxaAdministracao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCadastro
			{
				get
				{
					System.Int32? data = entity.IdCadastro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCadastro = null;
					else entity.IdCadastro = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Byte? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToByte(value);
				}
			}
			

			private esCadastroTaxaAdministracao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCadastroTaxaAdministracaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCadastroTaxaAdministracao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CadastroTaxaAdministracao : esCadastroTaxaAdministracao
	{

				
		#region TabelaTaxaAdministracaoCollectionByIdCadastro - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - CadastroTaxaAdministracao_TabelaTaxaAdministracao_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaAdministracaoCollection TabelaTaxaAdministracaoCollectionByIdCadastro
		{
			get
			{
				if(this._TabelaTaxaAdministracaoCollectionByIdCadastro == null)
				{
					this._TabelaTaxaAdministracaoCollectionByIdCadastro = new TabelaTaxaAdministracaoCollection();
					this._TabelaTaxaAdministracaoCollectionByIdCadastro.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaTaxaAdministracaoCollectionByIdCadastro", this._TabelaTaxaAdministracaoCollectionByIdCadastro);
				
					if(this.IdCadastro != null)
					{
						this._TabelaTaxaAdministracaoCollectionByIdCadastro.Query.Where(this._TabelaTaxaAdministracaoCollectionByIdCadastro.Query.IdCadastro == this.IdCadastro);
						this._TabelaTaxaAdministracaoCollectionByIdCadastro.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaTaxaAdministracaoCollectionByIdCadastro.fks.Add(TabelaTaxaAdministracaoMetadata.ColumnNames.IdCadastro, this.IdCadastro);
					}
				}

				return this._TabelaTaxaAdministracaoCollectionByIdCadastro;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaTaxaAdministracaoCollectionByIdCadastro != null) 
				{ 
					this.RemovePostSave("TabelaTaxaAdministracaoCollectionByIdCadastro"); 
					this._TabelaTaxaAdministracaoCollectionByIdCadastro = null;
					
				} 
			} 			
		}

		private TabelaTaxaAdministracaoCollection _TabelaTaxaAdministracaoCollectionByIdCadastro;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "TabelaTaxaAdministracaoCollectionByIdCadastro", typeof(TabelaTaxaAdministracaoCollection), new TabelaTaxaAdministracao()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._TabelaTaxaAdministracaoCollectionByIdCadastro != null)
			{
				foreach(TabelaTaxaAdministracao obj in this._TabelaTaxaAdministracaoCollectionByIdCadastro)
				{
					if(obj.es.IsAdded)
					{
						obj.IdCadastro = this.IdCadastro;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCadastroTaxaAdministracaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CadastroTaxaAdministracaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCadastro
		{
			get
			{
				return new esQueryItem(this, CadastroTaxaAdministracaoMetadata.ColumnNames.IdCadastro, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, CadastroTaxaAdministracaoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, CadastroTaxaAdministracaoMetadata.ColumnNames.Tipo, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CadastroTaxaAdministracaoCollection")]
	public partial class CadastroTaxaAdministracaoCollection : esCadastroTaxaAdministracaoCollection, IEnumerable<CadastroTaxaAdministracao>
	{
		public CadastroTaxaAdministracaoCollection()
		{

		}
		
		public static implicit operator List<CadastroTaxaAdministracao>(CadastroTaxaAdministracaoCollection coll)
		{
			List<CadastroTaxaAdministracao> list = new List<CadastroTaxaAdministracao>();
			
			foreach (CadastroTaxaAdministracao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CadastroTaxaAdministracaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CadastroTaxaAdministracaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CadastroTaxaAdministracao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CadastroTaxaAdministracao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CadastroTaxaAdministracaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CadastroTaxaAdministracaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CadastroTaxaAdministracaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CadastroTaxaAdministracao AddNew()
		{
			CadastroTaxaAdministracao entity = base.AddNewEntity() as CadastroTaxaAdministracao;
			
			return entity;
		}

		public CadastroTaxaAdministracao FindByPrimaryKey(System.Int32 idCadastro)
		{
			return base.FindByPrimaryKey(idCadastro) as CadastroTaxaAdministracao;
		}


		#region IEnumerable<CadastroTaxaAdministracao> Members

		IEnumerator<CadastroTaxaAdministracao> IEnumerable<CadastroTaxaAdministracao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CadastroTaxaAdministracao;
			}
		}

		#endregion
		
		private CadastroTaxaAdministracaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CadastroTaxaAdministracao' table
	/// </summary>

	[Serializable]
	public partial class CadastroTaxaAdministracao : esCadastroTaxaAdministracao
	{
		public CadastroTaxaAdministracao()
		{

		}
	
		public CadastroTaxaAdministracao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CadastroTaxaAdministracaoMetadata.Meta();
			}
		}
		
		
		
		override protected esCadastroTaxaAdministracaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CadastroTaxaAdministracaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CadastroTaxaAdministracaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CadastroTaxaAdministracaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CadastroTaxaAdministracaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CadastroTaxaAdministracaoQuery query;
	}



	[Serializable]
	public partial class CadastroTaxaAdministracaoQuery : esCadastroTaxaAdministracaoQuery
	{
		public CadastroTaxaAdministracaoQuery()
		{

		}		
		
		public CadastroTaxaAdministracaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CadastroTaxaAdministracaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CadastroTaxaAdministracaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CadastroTaxaAdministracaoMetadata.ColumnNames.IdCadastro, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CadastroTaxaAdministracaoMetadata.PropertyNames.IdCadastro;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroTaxaAdministracaoMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CadastroTaxaAdministracaoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CadastroTaxaAdministracaoMetadata.ColumnNames.Tipo, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CadastroTaxaAdministracaoMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CadastroTaxaAdministracaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCadastro = "IdCadastro";
			 public const string Descricao = "Descricao";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCadastro = "IdCadastro";
			 public const string Descricao = "Descricao";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CadastroTaxaAdministracaoMetadata))
			{
				if(CadastroTaxaAdministracaoMetadata.mapDelegates == null)
				{
					CadastroTaxaAdministracaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CadastroTaxaAdministracaoMetadata.meta == null)
				{
					CadastroTaxaAdministracaoMetadata.meta = new CadastroTaxaAdministracaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCadastro", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "CadastroTaxaAdministracao";
				meta.Destination = "CadastroTaxaAdministracao";
				
				meta.spInsert = "proc_CadastroTaxaAdministracaoInsert";				
				meta.spUpdate = "proc_CadastroTaxaAdministracaoUpdate";		
				meta.spDelete = "proc_CadastroTaxaAdministracaoDelete";
				meta.spLoadAll = "proc_CadastroTaxaAdministracaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_CadastroTaxaAdministracaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CadastroTaxaAdministracaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
