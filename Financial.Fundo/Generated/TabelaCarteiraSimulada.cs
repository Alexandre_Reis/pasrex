/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:20 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTabelaCarteiraSimuladaCollection : esEntityCollection
	{
		public esTabelaCarteiraSimuladaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaCarteiraSimuladaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaCarteiraSimuladaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaCarteiraSimuladaQuery);
		}
		#endregion
		
		virtual public TabelaCarteiraSimulada DetachEntity(TabelaCarteiraSimulada entity)
		{
			return base.DetachEntity(entity) as TabelaCarteiraSimulada;
		}
		
		virtual public TabelaCarteiraSimulada AttachEntity(TabelaCarteiraSimulada entity)
		{
			return base.AttachEntity(entity) as TabelaCarteiraSimulada;
		}
		
		virtual public void Combine(TabelaCarteiraSimuladaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaCarteiraSimulada this[int index]
		{
			get
			{
				return base[index] as TabelaCarteiraSimulada;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaCarteiraSimulada);
		}
	}



	[Serializable]
	abstract public class esTabelaCarteiraSimulada : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaCarteiraSimuladaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaCarteiraSimulada()
		{

		}

		public esTabelaCarteiraSimulada(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCarteira, System.Int32 tipoAtivo, System.String codigoAtivo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira, tipoAtivo, codigoAtivo);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira, tipoAtivo, codigoAtivo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCarteira, System.Int32 tipoAtivo, System.String codigoAtivo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaCarteiraSimuladaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCarteira == idCarteira, query.TipoAtivo == tipoAtivo, query.CodigoAtivo == codigoAtivo);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCarteira, System.Int32 tipoAtivo, System.String codigoAtivo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira, tipoAtivo, codigoAtivo);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira, tipoAtivo, codigoAtivo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCarteira, System.Int32 tipoAtivo, System.String codigoAtivo)
		{
			esTabelaCarteiraSimuladaQuery query = this.GetDynamicQuery();
			query.Where(query.IdCarteira == idCarteira, query.TipoAtivo == tipoAtivo, query.CodigoAtivo == codigoAtivo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCarteira, System.Int32 tipoAtivo, System.String codigoAtivo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCarteira",idCarteira);			parms.Add("TipoAtivo",tipoAtivo);			parms.Add("CodigoAtivo",codigoAtivo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "TipoAtivo": this.str.TipoAtivo = (string)value; break;							
						case "CodigoAtivo": this.str.CodigoAtivo = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "Percentual": this.str.Percentual = (string)value; break;							
						case "DataFim": this.str.DataFim = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "TipoAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoAtivo = (System.Int32?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "Percentual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Percentual = (System.Decimal?)value;
							break;
						
						case "DataFim":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFim = (System.DateTime?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaCarteiraSimulada.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(TabelaCarteiraSimuladaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(TabelaCarteiraSimuladaMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCarteiraSimulada.TipoAtivo
		/// </summary>
		virtual public System.Int32? TipoAtivo
		{
			get
			{
				return base.GetSystemInt32(TabelaCarteiraSimuladaMetadata.ColumnNames.TipoAtivo);
			}
			
			set
			{
				base.SetSystemInt32(TabelaCarteiraSimuladaMetadata.ColumnNames.TipoAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCarteiraSimulada.CodigoAtivo
		/// </summary>
		virtual public System.String CodigoAtivo
		{
			get
			{
				return base.GetSystemString(TabelaCarteiraSimuladaMetadata.ColumnNames.CodigoAtivo);
			}
			
			set
			{
				base.SetSystemString(TabelaCarteiraSimuladaMetadata.ColumnNames.CodigoAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCarteiraSimulada.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(TabelaCarteiraSimuladaMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCarteiraSimuladaMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCarteiraSimulada.Percentual
		/// </summary>
		virtual public System.Decimal? Percentual
		{
			get
			{
				return base.GetSystemDecimal(TabelaCarteiraSimuladaMetadata.ColumnNames.Percentual);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCarteiraSimuladaMetadata.ColumnNames.Percentual, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCarteiraSimulada.DataFim
		/// </summary>
		virtual public System.DateTime? DataFim
		{
			get
			{
				return base.GetSystemDateTime(TabelaCarteiraSimuladaMetadata.ColumnNames.DataFim);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaCarteiraSimuladaMetadata.ColumnNames.DataFim, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCarteiraSimulada.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(TabelaCarteiraSimuladaMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				base.SetSystemInt16(TabelaCarteiraSimuladaMetadata.ColumnNames.IdIndice, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCarteiraSimulada.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(TabelaCarteiraSimuladaMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCarteiraSimuladaMetadata.ColumnNames.Taxa, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaCarteiraSimulada entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoAtivo
			{
				get
				{
					System.Int32? data = entity.TipoAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoAtivo = null;
					else entity.TipoAtivo = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoAtivo
			{
				get
				{
					System.String data = entity.CodigoAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoAtivo = null;
					else entity.CodigoAtivo = Convert.ToString(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String Percentual
			{
				get
				{
					System.Decimal? data = entity.Percentual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Percentual = null;
					else entity.Percentual = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataFim
			{
				get
				{
					System.DateTime? data = entity.DataFim;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFim = null;
					else entity.DataFim = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaCarteiraSimulada entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaCarteiraSimuladaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaCarteiraSimulada can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaCarteiraSimulada : esTabelaCarteiraSimulada
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaCarteiraSimuladaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCarteiraSimuladaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TabelaCarteiraSimuladaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoAtivo
		{
			get
			{
				return new esQueryItem(this, TabelaCarteiraSimuladaMetadata.ColumnNames.TipoAtivo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoAtivo
		{
			get
			{
				return new esQueryItem(this, TabelaCarteiraSimuladaMetadata.ColumnNames.CodigoAtivo, esSystemType.String);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, TabelaCarteiraSimuladaMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Percentual
		{
			get
			{
				return new esQueryItem(this, TabelaCarteiraSimuladaMetadata.ColumnNames.Percentual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataFim
		{
			get
			{
				return new esQueryItem(this, TabelaCarteiraSimuladaMetadata.ColumnNames.DataFim, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, TabelaCarteiraSimuladaMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, TabelaCarteiraSimuladaMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaCarteiraSimuladaCollection")]
	public partial class TabelaCarteiraSimuladaCollection : esTabelaCarteiraSimuladaCollection, IEnumerable<TabelaCarteiraSimulada>
	{
		public TabelaCarteiraSimuladaCollection()
		{

		}
		
		public static implicit operator List<TabelaCarteiraSimulada>(TabelaCarteiraSimuladaCollection coll)
		{
			List<TabelaCarteiraSimulada> list = new List<TabelaCarteiraSimulada>();
			
			foreach (TabelaCarteiraSimulada emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaCarteiraSimuladaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCarteiraSimuladaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaCarteiraSimulada(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaCarteiraSimulada();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaCarteiraSimuladaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCarteiraSimuladaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaCarteiraSimuladaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaCarteiraSimulada AddNew()
		{
			TabelaCarteiraSimulada entity = base.AddNewEntity() as TabelaCarteiraSimulada;
			
			return entity;
		}

		public TabelaCarteiraSimulada FindByPrimaryKey(System.Int32 idCarteira, System.Int32 tipoAtivo, System.String codigoAtivo)
		{
			return base.FindByPrimaryKey(idCarteira, tipoAtivo, codigoAtivo) as TabelaCarteiraSimulada;
		}


		#region IEnumerable<TabelaCarteiraSimulada> Members

		IEnumerator<TabelaCarteiraSimulada> IEnumerable<TabelaCarteiraSimulada>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaCarteiraSimulada;
			}
		}

		#endregion
		
		private TabelaCarteiraSimuladaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaCarteiraSimulada' table
	/// </summary>

	[Serializable]
	public partial class TabelaCarteiraSimulada : esTabelaCarteiraSimulada
	{
		public TabelaCarteiraSimulada()
		{

		}
	
		public TabelaCarteiraSimulada(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCarteiraSimuladaMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaCarteiraSimuladaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCarteiraSimuladaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaCarteiraSimuladaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCarteiraSimuladaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaCarteiraSimuladaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaCarteiraSimuladaQuery query;
	}



	[Serializable]
	public partial class TabelaCarteiraSimuladaQuery : esTabelaCarteiraSimuladaQuery
	{
		public TabelaCarteiraSimuladaQuery()
		{

		}		
		
		public TabelaCarteiraSimuladaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaCarteiraSimuladaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaCarteiraSimuladaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaCarteiraSimuladaMetadata.ColumnNames.IdCarteira, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCarteiraSimuladaMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCarteiraSimuladaMetadata.ColumnNames.TipoAtivo, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCarteiraSimuladaMetadata.PropertyNames.TipoAtivo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCarteiraSimuladaMetadata.ColumnNames.CodigoAtivo, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaCarteiraSimuladaMetadata.PropertyNames.CodigoAtivo;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCarteiraSimuladaMetadata.ColumnNames.Valor, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCarteiraSimuladaMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCarteiraSimuladaMetadata.ColumnNames.Percentual, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCarteiraSimuladaMetadata.PropertyNames.Percentual;	
			c.NumericPrecision = 12;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCarteiraSimuladaMetadata.ColumnNames.DataFim, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaCarteiraSimuladaMetadata.PropertyNames.DataFim;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCarteiraSimuladaMetadata.ColumnNames.IdIndice, 6, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TabelaCarteiraSimuladaMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCarteiraSimuladaMetadata.ColumnNames.Taxa, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCarteiraSimuladaMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 12;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaCarteiraSimuladaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string TipoAtivo = "TipoAtivo";
			 public const string CodigoAtivo = "CodigoAtivo";
			 public const string Valor = "Valor";
			 public const string Percentual = "Percentual";
			 public const string DataFim = "DataFim";
			 public const string IdIndice = "IdIndice";
			 public const string Taxa = "Taxa";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string TipoAtivo = "TipoAtivo";
			 public const string CodigoAtivo = "CodigoAtivo";
			 public const string Valor = "Valor";
			 public const string Percentual = "Percentual";
			 public const string DataFim = "DataFim";
			 public const string IdIndice = "IdIndice";
			 public const string Taxa = "Taxa";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaCarteiraSimuladaMetadata))
			{
				if(TabelaCarteiraSimuladaMetadata.mapDelegates == null)
				{
					TabelaCarteiraSimuladaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaCarteiraSimuladaMetadata.meta == null)
				{
					TabelaCarteiraSimuladaMetadata.meta = new TabelaCarteiraSimuladaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoAtivo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoAtivo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Percentual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataFim", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaCarteiraSimulada";
				meta.Destination = "TabelaCarteiraSimulada";
				
				meta.spInsert = "proc_TabelaCarteiraSimuladaInsert";				
				meta.spUpdate = "proc_TabelaCarteiraSimuladaUpdate";		
				meta.spDelete = "proc_TabelaCarteiraSimuladaDelete";
				meta.spLoadAll = "proc_TabelaCarteiraSimuladaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaCarteiraSimuladaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaCarteiraSimuladaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
