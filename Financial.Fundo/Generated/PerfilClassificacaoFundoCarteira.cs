/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/03/2016 10:24:38
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esPerfilClassificacaoFundoCarteiraCollection : esEntityCollection
	{
		public esPerfilClassificacaoFundoCarteiraCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PerfilClassificacaoFundoCarteiraCollection";
		}

		#region Query Logic
		protected void InitQuery(esPerfilClassificacaoFundoCarteiraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPerfilClassificacaoFundoCarteiraQuery);
		}
		#endregion
		
		virtual public PerfilClassificacaoFundoCarteira DetachEntity(PerfilClassificacaoFundoCarteira entity)
		{
			return base.DetachEntity(entity) as PerfilClassificacaoFundoCarteira;
		}
		
		virtual public PerfilClassificacaoFundoCarteira AttachEntity(PerfilClassificacaoFundoCarteira entity)
		{
			return base.AttachEntity(entity) as PerfilClassificacaoFundoCarteira;
		}
		
		virtual public void Combine(PerfilClassificacaoFundoCarteiraCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PerfilClassificacaoFundoCarteira this[int index]
		{
			get
			{
				return base[index] as PerfilClassificacaoFundoCarteira;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PerfilClassificacaoFundoCarteira);
		}
	}



	[Serializable]
	abstract public class esPerfilClassificacaoFundoCarteira : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPerfilClassificacaoFundoCarteiraQuery GetDynamicQuery()
		{
			return null;
		}

		public esPerfilClassificacaoFundoCarteira()
		{

		}

		public esPerfilClassificacaoFundoCarteira(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPerfilClassificacaoFundoCarteira)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilClassificacaoFundoCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilClassificacaoFundoCarteira);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPerfilClassificacaoFundoCarteira)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilClassificacaoFundoCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilClassificacaoFundoCarteira);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPerfilClassificacaoFundoCarteira)
		{
			esPerfilClassificacaoFundoCarteiraQuery query = this.GetDynamicQuery();
			query.Where(query.IdPerfilClassificacaoFundoCarteira == idPerfilClassificacaoFundoCarteira);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPerfilClassificacaoFundoCarteira)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPerfilClassificacaoFundoCarteira",idPerfilClassificacaoFundoCarteira);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPerfilClassificacaoFundoCarteira": this.str.IdPerfilClassificacaoFundoCarteira = (string)value; break;							
						case "IdPerfilClassificacao": this.str.IdPerfilClassificacao = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "InicioVigenciaPerfil": this.str.InicioVigenciaPerfil = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPerfilClassificacaoFundoCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilClassificacaoFundoCarteira = (System.Int32?)value;
							break;
						
						case "IdPerfilClassificacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilClassificacao = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "InicioVigenciaPerfil":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.InicioVigenciaPerfil = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PerfilClassificacaoFundoCarteira.IdPerfilClassificacaoFundoCarteira
		/// </summary>
		virtual public System.Int32? IdPerfilClassificacaoFundoCarteira
		{
			get
			{
				return base.GetSystemInt32(PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.IdPerfilClassificacaoFundoCarteira);
			}
			
			set
			{
				base.SetSystemInt32(PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.IdPerfilClassificacaoFundoCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilClassificacaoFundoCarteira.IdPerfilClassificacao
		/// </summary>
		virtual public System.Int32? IdPerfilClassificacao
		{
			get
			{
				return base.GetSystemInt32(PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.IdPerfilClassificacao);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.IdPerfilClassificacao, value))
				{
					this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PerfilClassificacaoFundoCarteira.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PerfilClassificacaoFundoCarteira.InicioVigenciaPerfil
		/// </summary>
		virtual public System.DateTime? InicioVigenciaPerfil
		{
			get
			{
				return base.GetSystemDateTime(PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.InicioVigenciaPerfil);
			}
			
			set
			{
				base.SetSystemDateTime(PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.InicioVigenciaPerfil, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected PerfilClassificacaoFundo _UpToPerfilClassificacaoFundoByIdPerfilClassificacao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPerfilClassificacaoFundoCarteira entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPerfilClassificacaoFundoCarteira
			{
				get
				{
					System.Int32? data = entity.IdPerfilClassificacaoFundoCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilClassificacaoFundoCarteira = null;
					else entity.IdPerfilClassificacaoFundoCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPerfilClassificacao
			{
				get
				{
					System.Int32? data = entity.IdPerfilClassificacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilClassificacao = null;
					else entity.IdPerfilClassificacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String InicioVigenciaPerfil
			{
				get
				{
					System.DateTime? data = entity.InicioVigenciaPerfil;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InicioVigenciaPerfil = null;
					else entity.InicioVigenciaPerfil = Convert.ToDateTime(value);
				}
			}
			

			private esPerfilClassificacaoFundoCarteira entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPerfilClassificacaoFundoCarteiraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPerfilClassificacaoFundoCarteira can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PerfilClassificacaoFundoCarteira : esPerfilClassificacaoFundoCarteira
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PerfilClassificacaoFundoCarteira_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPerfilClassificacaoFundoByIdPerfilClassificacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PerfilClassificacaoFundoCarteira_PerfilClassificacaoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilClassificacaoFundo UpToPerfilClassificacaoFundoByIdPerfilClassificacao
		{
			get
			{
				if(this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao == null
					&& IdPerfilClassificacao != null					)
				{
					this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao = new PerfilClassificacaoFundo();
					this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPerfilClassificacaoFundoByIdPerfilClassificacao", this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao);
					this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao.Query.Where(this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao.Query.IdPerfil == this.IdPerfilClassificacao);
					this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao.Query.Load();
				}

				return this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao;
			}
			
			set
			{
				this.RemovePreSave("UpToPerfilClassificacaoFundoByIdPerfilClassificacao");
				

				if(value == null)
				{
					this.IdPerfilClassificacao = null;
					this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao = null;
				}
				else
				{
					this.IdPerfilClassificacao = value.IdPerfil;
					this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao = value;
					this.SetPreSave("UpToPerfilClassificacaoFundoByIdPerfilClassificacao", this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao != null)
			{
				this.IdPerfilClassificacao = this._UpToPerfilClassificacaoFundoByIdPerfilClassificacao.IdPerfil;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPerfilClassificacaoFundoCarteiraQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PerfilClassificacaoFundoCarteiraMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPerfilClassificacaoFundoCarteira
		{
			get
			{
				return new esQueryItem(this, PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.IdPerfilClassificacaoFundoCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPerfilClassificacao
		{
			get
			{
				return new esQueryItem(this, PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.IdPerfilClassificacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem InicioVigenciaPerfil
		{
			get
			{
				return new esQueryItem(this, PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.InicioVigenciaPerfil, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PerfilClassificacaoFundoCarteiraCollection")]
	public partial class PerfilClassificacaoFundoCarteiraCollection : esPerfilClassificacaoFundoCarteiraCollection, IEnumerable<PerfilClassificacaoFundoCarteira>
	{
		public PerfilClassificacaoFundoCarteiraCollection()
		{

		}
		
		public static implicit operator List<PerfilClassificacaoFundoCarteira>(PerfilClassificacaoFundoCarteiraCollection coll)
		{
			List<PerfilClassificacaoFundoCarteira> list = new List<PerfilClassificacaoFundoCarteira>();
			
			foreach (PerfilClassificacaoFundoCarteira emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PerfilClassificacaoFundoCarteiraMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilClassificacaoFundoCarteiraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PerfilClassificacaoFundoCarteira(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PerfilClassificacaoFundoCarteira();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PerfilClassificacaoFundoCarteiraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilClassificacaoFundoCarteiraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PerfilClassificacaoFundoCarteiraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PerfilClassificacaoFundoCarteira AddNew()
		{
			PerfilClassificacaoFundoCarteira entity = base.AddNewEntity() as PerfilClassificacaoFundoCarteira;
			
			return entity;
		}

		public PerfilClassificacaoFundoCarteira FindByPrimaryKey(System.Int32 idPerfilClassificacaoFundoCarteira)
		{
			return base.FindByPrimaryKey(idPerfilClassificacaoFundoCarteira) as PerfilClassificacaoFundoCarteira;
		}


		#region IEnumerable<PerfilClassificacaoFundoCarteira> Members

		IEnumerator<PerfilClassificacaoFundoCarteira> IEnumerable<PerfilClassificacaoFundoCarteira>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PerfilClassificacaoFundoCarteira;
			}
		}

		#endregion
		
		private PerfilClassificacaoFundoCarteiraQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PerfilClassificacaoFundoCarteira' table
	/// </summary>

	[Serializable]
	public partial class PerfilClassificacaoFundoCarteira : esPerfilClassificacaoFundoCarteira
	{
		public PerfilClassificacaoFundoCarteira()
		{

		}
	
		public PerfilClassificacaoFundoCarteira(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PerfilClassificacaoFundoCarteiraMetadata.Meta();
			}
		}
		
		
		
		override protected esPerfilClassificacaoFundoCarteiraQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilClassificacaoFundoCarteiraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PerfilClassificacaoFundoCarteiraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilClassificacaoFundoCarteiraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PerfilClassificacaoFundoCarteiraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PerfilClassificacaoFundoCarteiraQuery query;
	}



	[Serializable]
	public partial class PerfilClassificacaoFundoCarteiraQuery : esPerfilClassificacaoFundoCarteiraQuery
	{
		public PerfilClassificacaoFundoCarteiraQuery()
		{

		}		
		
		public PerfilClassificacaoFundoCarteiraQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PerfilClassificacaoFundoCarteiraMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PerfilClassificacaoFundoCarteiraMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.IdPerfilClassificacaoFundoCarteira, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilClassificacaoFundoCarteiraMetadata.PropertyNames.IdPerfilClassificacaoFundoCarteira;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.IdPerfilClassificacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilClassificacaoFundoCarteiraMetadata.PropertyNames.IdPerfilClassificacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.IdCarteira, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilClassificacaoFundoCarteiraMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.InicioVigenciaPerfil, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PerfilClassificacaoFundoCarteiraMetadata.PropertyNames.InicioVigenciaPerfil;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PerfilClassificacaoFundoCarteiraMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPerfilClassificacaoFundoCarteira = "IdPerfilClassificacaoFundoCarteira";
			 public const string IdPerfilClassificacao = "IdPerfilClassificacao";
			 public const string IdCarteira = "IdCarteira";
			 public const string InicioVigenciaPerfil = "InicioVigenciaPerfil";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPerfilClassificacaoFundoCarteira = "IdPerfilClassificacaoFundoCarteira";
			 public const string IdPerfilClassificacao = "IdPerfilClassificacao";
			 public const string IdCarteira = "IdCarteira";
			 public const string InicioVigenciaPerfil = "InicioVigenciaPerfil";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PerfilClassificacaoFundoCarteiraMetadata))
			{
				if(PerfilClassificacaoFundoCarteiraMetadata.mapDelegates == null)
				{
					PerfilClassificacaoFundoCarteiraMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PerfilClassificacaoFundoCarteiraMetadata.meta == null)
				{
					PerfilClassificacaoFundoCarteiraMetadata.meta = new PerfilClassificacaoFundoCarteiraMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPerfilClassificacaoFundoCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPerfilClassificacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("InicioVigenciaPerfil", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "PerfilClassificacaoFundoCarteira";
				meta.Destination = "PerfilClassificacaoFundoCarteira";
				
				meta.spInsert = "proc_PerfilClassificacaoFundoCarteiraInsert";				
				meta.spUpdate = "proc_PerfilClassificacaoFundoCarteiraUpdate";		
				meta.spDelete = "proc_PerfilClassificacaoFundoCarteiraDelete";
				meta.spLoadAll = "proc_PerfilClassificacaoFundoCarteiraLoadAll";
				meta.spLoadByPrimaryKey = "proc_PerfilClassificacaoFundoCarteiraLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PerfilClassificacaoFundoCarteiraMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
