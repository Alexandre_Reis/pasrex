/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/12/2014 12:01:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esEventoFundoLiquidacaoCollection : esEntityCollection
	{
		public esEventoFundoLiquidacaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EventoFundoLiquidacaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esEventoFundoLiquidacaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEventoFundoLiquidacaoQuery);
		}
		#endregion
		
		virtual public EventoFundoLiquidacao DetachEntity(EventoFundoLiquidacao entity)
		{
			return base.DetachEntity(entity) as EventoFundoLiquidacao;
		}
		
		virtual public EventoFundoLiquidacao AttachEntity(EventoFundoLiquidacao entity)
		{
			return base.AttachEntity(entity) as EventoFundoLiquidacao;
		}
		
		virtual public void Combine(EventoFundoLiquidacaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EventoFundoLiquidacao this[int index]
		{
			get
			{
				return base[index] as EventoFundoLiquidacao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EventoFundoLiquidacao);
		}
	}



	[Serializable]
	abstract public class esEventoFundoLiquidacao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEventoFundoLiquidacaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esEventoFundoLiquidacao()
		{

		}

		public esEventoFundoLiquidacao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataLancamento, System.DateTime dataVencimento, System.String descricao, System.Int32 idEventoFundo, System.Int32 tipoMercado)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataLancamento, dataVencimento, descricao, idEventoFundo, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(dataLancamento, dataVencimento, descricao, idEventoFundo, tipoMercado);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataLancamento, System.DateTime dataVencimento, System.String descricao, System.Int32 idEventoFundo, System.Int32 tipoMercado)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataLancamento, dataVencimento, descricao, idEventoFundo, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(dataLancamento, dataVencimento, descricao, idEventoFundo, tipoMercado);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataLancamento, System.DateTime dataVencimento, System.String descricao, System.Int32 idEventoFundo, System.Int32 tipoMercado)
		{
			esEventoFundoLiquidacaoQuery query = this.GetDynamicQuery();
			query.Where(query.DataLancamento == dataLancamento, query.DataVencimento == dataVencimento, query.Descricao == descricao, query.IdEventoFundo == idEventoFundo, query.TipoMercado == tipoMercado);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataLancamento, System.DateTime dataVencimento, System.String descricao, System.Int32 idEventoFundo, System.Int32 tipoMercado)
		{
			esParameters parms = new esParameters();
			parms.Add("DataLancamento",dataLancamento);			parms.Add("DataVencimento",dataVencimento);			parms.Add("Descricao",descricao);			parms.Add("IdEventoFundo",idEventoFundo);			parms.Add("TipoMercado",tipoMercado);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEventoFundo": this.str.IdEventoFundo = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "DataLancamento": this.str.DataLancamento = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeConstante": this.str.QuantidadeConstante = (string)value; break;							
						case "Contador": this.str.Contador = (string)value; break;							
						case "Processar": this.str.Processar = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEventoFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoFundo = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoMercado = (System.Int32?)value;
							break;
						
						case "DataLancamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLancamento = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "QuantidadeConstante":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.QuantidadeConstante = (System.Int32?)value;
							break;
						
						case "Contador":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Contador = (System.Int32?)value;
							break;
						
						case "Processar":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Processar = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EventoFundoLiquidacao.IdEventoFundo
		/// </summary>
		virtual public System.Int32? IdEventoFundo
		{
			get
			{
				return base.GetSystemInt32(EventoFundoLiquidacaoMetadata.ColumnNames.IdEventoFundo);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoFundoLiquidacaoMetadata.ColumnNames.IdEventoFundo, value))
				{
					this._UpToEventoFundoByIdEventoFundo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoLiquidacao.TipoMercado
		/// </summary>
		virtual public System.Int32? TipoMercado
		{
			get
			{
				return base.GetSystemInt32(EventoFundoLiquidacaoMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoLiquidacaoMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoLiquidacao.DataLancamento
		/// </summary>
		virtual public System.DateTime? DataLancamento
		{
			get
			{
				return base.GetSystemDateTime(EventoFundoLiquidacaoMetadata.ColumnNames.DataLancamento);
			}
			
			set
			{
				base.SetSystemDateTime(EventoFundoLiquidacaoMetadata.ColumnNames.DataLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoLiquidacao.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(EventoFundoLiquidacaoMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(EventoFundoLiquidacaoMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoLiquidacao.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(EventoFundoLiquidacaoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(EventoFundoLiquidacaoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoLiquidacao.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(EventoFundoLiquidacaoMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(EventoFundoLiquidacaoMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoLiquidacao.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(EventoFundoLiquidacaoMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(EventoFundoLiquidacaoMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoLiquidacao.QuantidadeConstante
		/// </summary>
		virtual public System.Int32? QuantidadeConstante
		{
			get
			{
				return base.GetSystemInt32(EventoFundoLiquidacaoMetadata.ColumnNames.QuantidadeConstante);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoLiquidacaoMetadata.ColumnNames.QuantidadeConstante, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoLiquidacao.Contador
		/// </summary>
		virtual public System.Int32? Contador
		{
			get
			{
				return base.GetSystemInt32(EventoFundoLiquidacaoMetadata.ColumnNames.Contador);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoLiquidacaoMetadata.ColumnNames.Contador, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoLiquidacao.Processar
		/// </summary>
		virtual public System.Byte? Processar
		{
			get
			{
				return base.GetSystemByte(EventoFundoLiquidacaoMetadata.ColumnNames.Processar);
			}
			
			set
			{
				base.SetSystemByte(EventoFundoLiquidacaoMetadata.ColumnNames.Processar, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected EventoFundo _UpToEventoFundoByIdEventoFundo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEventoFundoLiquidacao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEventoFundo
			{
				get
				{
					System.Int32? data = entity.IdEventoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoFundo = null;
					else entity.IdEventoFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Int32? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToInt32(value);
				}
			}
				
			public System.String DataLancamento
			{
				get
				{
					System.DateTime? data = entity.DataLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLancamento = null;
					else entity.DataLancamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeConstante
			{
				get
				{
					System.Int32? data = entity.QuantidadeConstante;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeConstante = null;
					else entity.QuantidadeConstante = Convert.ToInt32(value);
				}
			}
				
			public System.String Contador
			{
				get
				{
					System.Int32? data = entity.Contador;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Contador = null;
					else entity.Contador = Convert.ToInt32(value);
				}
			}
				
			public System.String Processar
			{
				get
				{
					System.Byte? data = entity.Processar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Processar = null;
					else entity.Processar = Convert.ToByte(value);
				}
			}
			

			private esEventoFundoLiquidacao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEventoFundoLiquidacaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEventoFundoLiquidacao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EventoFundoLiquidacao : esEventoFundoLiquidacao
	{

				
		#region UpToEventoFundoByIdEventoFundo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EventoFundoLiquidacao_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundo UpToEventoFundoByIdEventoFundo
		{
			get
			{
				if(this._UpToEventoFundoByIdEventoFundo == null
					&& IdEventoFundo != null					)
				{
					this._UpToEventoFundoByIdEventoFundo = new EventoFundo();
					this._UpToEventoFundoByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Where(this._UpToEventoFundoByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Load();
				}

				return this._UpToEventoFundoByIdEventoFundo;
			}
			
			set
			{
				this.RemovePreSave("UpToEventoFundoByIdEventoFundo");
				

				if(value == null)
				{
					this.IdEventoFundo = null;
					this._UpToEventoFundoByIdEventoFundo = null;
				}
				else
				{
					this.IdEventoFundo = value.IdEventoFundo;
					this._UpToEventoFundoByIdEventoFundo = value;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEventoFundoByIdEventoFundo != null)
			{
				this.IdEventoFundo = this._UpToEventoFundoByIdEventoFundo.IdEventoFundo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEventoFundoLiquidacaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoLiquidacaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEventoFundo
		{
			get
			{
				return new esQueryItem(this, EventoFundoLiquidacaoMetadata.ColumnNames.IdEventoFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, EventoFundoLiquidacaoMetadata.ColumnNames.TipoMercado, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataLancamento
		{
			get
			{
				return new esQueryItem(this, EventoFundoLiquidacaoMetadata.ColumnNames.DataLancamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, EventoFundoLiquidacaoMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, EventoFundoLiquidacaoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, EventoFundoLiquidacaoMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, EventoFundoLiquidacaoMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeConstante
		{
			get
			{
				return new esQueryItem(this, EventoFundoLiquidacaoMetadata.ColumnNames.QuantidadeConstante, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Contador
		{
			get
			{
				return new esQueryItem(this, EventoFundoLiquidacaoMetadata.ColumnNames.Contador, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Processar
		{
			get
			{
				return new esQueryItem(this, EventoFundoLiquidacaoMetadata.ColumnNames.Processar, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EventoFundoLiquidacaoCollection")]
	public partial class EventoFundoLiquidacaoCollection : esEventoFundoLiquidacaoCollection, IEnumerable<EventoFundoLiquidacao>
	{
		public EventoFundoLiquidacaoCollection()
		{

		}
		
		public static implicit operator List<EventoFundoLiquidacao>(EventoFundoLiquidacaoCollection coll)
		{
			List<EventoFundoLiquidacao> list = new List<EventoFundoLiquidacao>();
			
			foreach (EventoFundoLiquidacao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EventoFundoLiquidacaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoLiquidacaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EventoFundoLiquidacao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EventoFundoLiquidacao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EventoFundoLiquidacaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoLiquidacaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EventoFundoLiquidacaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EventoFundoLiquidacao AddNew()
		{
			EventoFundoLiquidacao entity = base.AddNewEntity() as EventoFundoLiquidacao;
			
			return entity;
		}

		public EventoFundoLiquidacao FindByPrimaryKey(System.DateTime dataLancamento, System.DateTime dataVencimento, System.String descricao, System.Int32 idEventoFundo, System.Int32 tipoMercado)
		{
			return base.FindByPrimaryKey(dataLancamento, dataVencimento, descricao, idEventoFundo, tipoMercado) as EventoFundoLiquidacao;
		}


		#region IEnumerable<EventoFundoLiquidacao> Members

		IEnumerator<EventoFundoLiquidacao> IEnumerable<EventoFundoLiquidacao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EventoFundoLiquidacao;
			}
		}

		#endregion
		
		private EventoFundoLiquidacaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EventoFundoLiquidacao' table
	/// </summary>

	[Serializable]
	public partial class EventoFundoLiquidacao : esEventoFundoLiquidacao
	{
		public EventoFundoLiquidacao()
		{

		}
	
		public EventoFundoLiquidacao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoLiquidacaoMetadata.Meta();
			}
		}
		
		
		
		override protected esEventoFundoLiquidacaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoLiquidacaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EventoFundoLiquidacaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoLiquidacaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EventoFundoLiquidacaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EventoFundoLiquidacaoQuery query;
	}



	[Serializable]
	public partial class EventoFundoLiquidacaoQuery : esEventoFundoLiquidacaoQuery
	{
		public EventoFundoLiquidacaoQuery()
		{

		}		
		
		public EventoFundoLiquidacaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EventoFundoLiquidacaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EventoFundoLiquidacaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EventoFundoLiquidacaoMetadata.ColumnNames.IdEventoFundo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoLiquidacaoMetadata.PropertyNames.IdEventoFundo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoLiquidacaoMetadata.ColumnNames.TipoMercado, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoLiquidacaoMetadata.PropertyNames.TipoMercado;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoLiquidacaoMetadata.ColumnNames.DataLancamento, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EventoFundoLiquidacaoMetadata.PropertyNames.DataLancamento;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoLiquidacaoMetadata.ColumnNames.DataVencimento, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EventoFundoLiquidacaoMetadata.PropertyNames.DataVencimento;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoLiquidacaoMetadata.ColumnNames.Descricao, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = EventoFundoLiquidacaoMetadata.PropertyNames.Descricao;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 600;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoLiquidacaoMetadata.ColumnNames.Valor, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EventoFundoLiquidacaoMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoLiquidacaoMetadata.ColumnNames.Quantidade, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EventoFundoLiquidacaoMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoLiquidacaoMetadata.ColumnNames.QuantidadeConstante, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoLiquidacaoMetadata.PropertyNames.QuantidadeConstante;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoLiquidacaoMetadata.ColumnNames.Contador, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoLiquidacaoMetadata.PropertyNames.Contador;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoLiquidacaoMetadata.ColumnNames.Processar, 9, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EventoFundoLiquidacaoMetadata.PropertyNames.Processar;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EventoFundoLiquidacaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataVencimento = "DataVencimento";
			 public const string Descricao = "Descricao";
			 public const string Valor = "Valor";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeConstante = "QuantidadeConstante";
			 public const string Contador = "Contador";
			 public const string Processar = "Processar";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataVencimento = "DataVencimento";
			 public const string Descricao = "Descricao";
			 public const string Valor = "Valor";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeConstante = "QuantidadeConstante";
			 public const string Contador = "Contador";
			 public const string Processar = "Processar";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EventoFundoLiquidacaoMetadata))
			{
				if(EventoFundoLiquidacaoMetadata.mapDelegates == null)
				{
					EventoFundoLiquidacaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EventoFundoLiquidacaoMetadata.meta == null)
				{
					EventoFundoLiquidacaoMetadata.meta = new EventoFundoLiquidacaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEventoFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataLancamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeConstante", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Contador", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Processar", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "EventoFundoLiquidacao";
				meta.Destination = "EventoFundoLiquidacao";
				
				meta.spInsert = "proc_EventoFundoLiquidacaoInsert";				
				meta.spUpdate = "proc_EventoFundoLiquidacaoUpdate";		
				meta.spDelete = "proc_EventoFundoLiquidacaoDelete";
				meta.spLoadAll = "proc_EventoFundoLiquidacaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_EventoFundoLiquidacaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EventoFundoLiquidacaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
