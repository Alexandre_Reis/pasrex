/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/05/2015 12:24:25
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esMediaMovelCollection : esEntityCollection
	{
		public esMediaMovelCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "MediaMovelCollection";
		}

		#region Query Logic
		protected void InitQuery(esMediaMovelQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esMediaMovelQuery);
		}
		#endregion
		
		virtual public MediaMovel DetachEntity(MediaMovel entity)
		{
			return base.DetachEntity(entity) as MediaMovel;
		}
		
		virtual public MediaMovel AttachEntity(MediaMovel entity)
		{
			return base.AttachEntity(entity) as MediaMovel;
		}
		
		virtual public void Combine(MediaMovelCollection collection)
		{
			base.Combine(collection);
		}
		
		new public MediaMovel this[int index]
		{
			get
			{
				return base[index] as MediaMovel;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(MediaMovel);
		}
	}



	[Serializable]
	abstract public class esMediaMovel : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esMediaMovelQuery GetDynamicQuery()
		{
			return null;
		}

		public esMediaMovel()
		{

		}

		public esMediaMovel(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idMediaMovel)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idMediaMovel);
			else
				return LoadByPrimaryKeyStoredProcedure(idMediaMovel);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idMediaMovel)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idMediaMovel);
			else
				return LoadByPrimaryKeyStoredProcedure(idMediaMovel);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idMediaMovel)
		{
			esMediaMovelQuery query = this.GetDynamicQuery();
			query.Where(query.IdMediaMovel == idMediaMovel);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idMediaMovel)
		{
			esParameters parms = new esParameters();
			parms.Add("IdMediaMovel",idMediaMovel);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdMediaMovel": this.str.IdMediaMovel = (string)value; break;							
						case "DataAtual": this.str.DataAtual = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "TipoMediaMovel": this.str.TipoMediaMovel = (string)value; break;							
						case "MediaMovel": this.str.MediaMovel = (string)value; break;							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "ChaveComposta": this.str.ChaveComposta = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdMediaMovel":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMediaMovel = (System.Int32?)value;
							break;
						
						case "DataAtual":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAtual = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "TipoMediaMovel":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoMediaMovel = (System.Int32?)value;
							break;
						
						case "MediaMovel":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.MediaMovel = (System.Decimal?)value;
							break;
						
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to MediaMovel.IdMediaMovel
		/// </summary>
		virtual public System.Int32? IdMediaMovel
		{
			get
			{
				return base.GetSystemInt32(MediaMovelMetadata.ColumnNames.IdMediaMovel);
			}
			
			set
			{
				base.SetSystemInt32(MediaMovelMetadata.ColumnNames.IdMediaMovel, value);
			}
		}
		
		/// <summary>
		/// Maps to MediaMovel.DataAtual
		/// </summary>
		virtual public System.DateTime? DataAtual
		{
			get
			{
				return base.GetSystemDateTime(MediaMovelMetadata.ColumnNames.DataAtual);
			}
			
			set
			{
				base.SetSystemDateTime(MediaMovelMetadata.ColumnNames.DataAtual, value);
			}
		}
		
		/// <summary>
		/// Maps to MediaMovel.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(MediaMovelMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(MediaMovelMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to MediaMovel.TipoMediaMovel
		/// </summary>
		virtual public System.Int32? TipoMediaMovel
		{
			get
			{
				return base.GetSystemInt32(MediaMovelMetadata.ColumnNames.TipoMediaMovel);
			}
			
			set
			{
				base.SetSystemInt32(MediaMovelMetadata.ColumnNames.TipoMediaMovel, value);
			}
		}
		
		/// <summary>
		/// Maps to MediaMovel.MediaMovel
		/// </summary>
		virtual public System.Decimal? MediaMovel
		{
			get
			{
				return base.GetSystemDecimal(MediaMovelMetadata.ColumnNames.MediaMovel);
			}
			
			set
			{
				base.SetSystemDecimal(MediaMovelMetadata.ColumnNames.MediaMovel, value);
			}
		}
		
		/// <summary>
		/// Maps to MediaMovel.idPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(MediaMovelMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(MediaMovelMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to MediaMovel.ChaveComposta
		/// </summary>
		virtual public System.String ChaveComposta
		{
			get
			{
				return base.GetSystemString(MediaMovelMetadata.ColumnNames.ChaveComposta);
			}
			
			set
			{
				base.SetSystemString(MediaMovelMetadata.ColumnNames.ChaveComposta, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esMediaMovel entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdMediaMovel
			{
				get
				{
					System.Int32? data = entity.IdMediaMovel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMediaMovel = null;
					else entity.IdMediaMovel = Convert.ToInt32(value);
				}
			}
				
			public System.String DataAtual
			{
				get
				{
					System.DateTime? data = entity.DataAtual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAtual = null;
					else entity.DataAtual = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoMediaMovel
			{
				get
				{
					System.Int32? data = entity.TipoMediaMovel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMediaMovel = null;
					else entity.TipoMediaMovel = Convert.ToInt32(value);
				}
			}
				
			public System.String MediaMovel
			{
				get
				{
					System.Decimal? data = entity.MediaMovel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MediaMovel = null;
					else entity.MediaMovel = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String ChaveComposta
			{
				get
				{
					System.String data = entity.ChaveComposta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ChaveComposta = null;
					else entity.ChaveComposta = Convert.ToString(value);
				}
			}
			

			private esMediaMovel entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esMediaMovelQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esMediaMovel can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class MediaMovel : esMediaMovel
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esMediaMovelQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return MediaMovelMetadata.Meta();
			}
		}	
		

		public esQueryItem IdMediaMovel
		{
			get
			{
				return new esQueryItem(this, MediaMovelMetadata.ColumnNames.IdMediaMovel, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataAtual
		{
			get
			{
				return new esQueryItem(this, MediaMovelMetadata.ColumnNames.DataAtual, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, MediaMovelMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoMediaMovel
		{
			get
			{
				return new esQueryItem(this, MediaMovelMetadata.ColumnNames.TipoMediaMovel, esSystemType.Int32);
			}
		} 
		
		public esQueryItem MediaMovel
		{
			get
			{
				return new esQueryItem(this, MediaMovelMetadata.ColumnNames.MediaMovel, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, MediaMovelMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ChaveComposta
		{
			get
			{
				return new esQueryItem(this, MediaMovelMetadata.ColumnNames.ChaveComposta, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("MediaMovelCollection")]
	public partial class MediaMovelCollection : esMediaMovelCollection, IEnumerable<MediaMovel>
	{
		public MediaMovelCollection()
		{

		}
		
		public static implicit operator List<MediaMovel>(MediaMovelCollection coll)
		{
			List<MediaMovel> list = new List<MediaMovel>();
			
			foreach (MediaMovel emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  MediaMovelMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new MediaMovelQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new MediaMovel(row);
		}

		override protected esEntity CreateEntity()
		{
			return new MediaMovel();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public MediaMovelQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new MediaMovelQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(MediaMovelQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public MediaMovel AddNew()
		{
			MediaMovel entity = base.AddNewEntity() as MediaMovel;
			
			return entity;
		}

		public MediaMovel FindByPrimaryKey(System.Int32 idMediaMovel)
		{
			return base.FindByPrimaryKey(idMediaMovel) as MediaMovel;
		}


		#region IEnumerable<MediaMovel> Members

		IEnumerator<MediaMovel> IEnumerable<MediaMovel>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as MediaMovel;
			}
		}

		#endregion
		
		private MediaMovelQuery query;
	}


	/// <summary>
	/// Encapsulates the 'MediaMovel' table
	/// </summary>

	[Serializable]
	public partial class MediaMovel : esMediaMovel
	{
		public MediaMovel()
		{

		}
	
		public MediaMovel(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return MediaMovelMetadata.Meta();
			}
		}
		
		
		
		override protected esMediaMovelQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new MediaMovelQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public MediaMovelQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new MediaMovelQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(MediaMovelQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private MediaMovelQuery query;
	}



	[Serializable]
	public partial class MediaMovelQuery : esMediaMovelQuery
	{
		public MediaMovelQuery()
		{

		}		
		
		public MediaMovelQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class MediaMovelMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected MediaMovelMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(MediaMovelMetadata.ColumnNames.IdMediaMovel, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = MediaMovelMetadata.PropertyNames.IdMediaMovel;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MediaMovelMetadata.ColumnNames.DataAtual, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = MediaMovelMetadata.PropertyNames.DataAtual;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MediaMovelMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = MediaMovelMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MediaMovelMetadata.ColumnNames.TipoMediaMovel, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = MediaMovelMetadata.PropertyNames.TipoMediaMovel;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MediaMovelMetadata.ColumnNames.MediaMovel, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = MediaMovelMetadata.PropertyNames.MediaMovel;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MediaMovelMetadata.ColumnNames.IdPosicao, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = MediaMovelMetadata.PropertyNames.IdPosicao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MediaMovelMetadata.ColumnNames.ChaveComposta, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = MediaMovelMetadata.PropertyNames.ChaveComposta;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public MediaMovelMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdMediaMovel = "IdMediaMovel";
			 public const string DataAtual = "DataAtual";
			 public const string IdCliente = "IdCliente";
			 public const string TipoMediaMovel = "TipoMediaMovel";
			 public const string MediaMovel = "MediaMovel";
			 public const string IdPosicao = "idPosicao";
			 public const string ChaveComposta = "ChaveComposta";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdMediaMovel = "IdMediaMovel";
			 public const string DataAtual = "DataAtual";
			 public const string IdCliente = "IdCliente";
			 public const string TipoMediaMovel = "TipoMediaMovel";
			 public const string MediaMovel = "MediaMovel";
			 public const string IdPosicao = "IdPosicao";
			 public const string ChaveComposta = "ChaveComposta";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(MediaMovelMetadata))
			{
				if(MediaMovelMetadata.mapDelegates == null)
				{
					MediaMovelMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (MediaMovelMetadata.meta == null)
				{
					MediaMovelMetadata.meta = new MediaMovelMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdMediaMovel", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataAtual", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoMediaMovel", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("MediaMovel", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("idPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ChaveComposta", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "MediaMovel";
				meta.Destination = "MediaMovel";
				
				meta.spInsert = "proc_MediaMovelInsert";				
				meta.spUpdate = "proc_MediaMovelUpdate";		
				meta.spDelete = "proc_MediaMovelDelete";
				meta.spLoadAll = "proc_MediaMovelLoadAll";
				meta.spLoadByPrimaryKey = "proc_MediaMovelLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private MediaMovelMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
