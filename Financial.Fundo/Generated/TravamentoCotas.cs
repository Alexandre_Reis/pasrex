/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 07/11/2014 14:30:49
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTravamentoCotasCollection : esEntityCollection
	{
		public esTravamentoCotasCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TravamentoCotasCollection";
		}

		#region Query Logic
		protected void InitQuery(esTravamentoCotasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTravamentoCotasQuery);
		}
		#endregion
		
		virtual public TravamentoCotas DetachEntity(TravamentoCotas entity)
		{
			return base.DetachEntity(entity) as TravamentoCotas;
		}
		
		virtual public TravamentoCotas AttachEntity(TravamentoCotas entity)
		{
			return base.AttachEntity(entity) as TravamentoCotas;
		}
		
		virtual public void Combine(TravamentoCotasCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TravamentoCotas this[int index]
		{
			get
			{
				return base[index] as TravamentoCotas;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TravamentoCotas);
		}
	}



	[Serializable]
	abstract public class esTravamentoCotas : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTravamentoCotasQuery GetDynamicQuery()
		{
			return null;
		}

		public esTravamentoCotas()
		{

		}

		public esTravamentoCotas(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTravamentoCota)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTravamentoCota);
			else
				return LoadByPrimaryKeyStoredProcedure(idTravamentoCota);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTravamentoCota)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTravamentoCota);
			else
				return LoadByPrimaryKeyStoredProcedure(idTravamentoCota);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTravamentoCota)
		{
			esTravamentoCotasQuery query = this.GetDynamicQuery();
			query.Where(query.IdTravamentoCota == idTravamentoCota);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTravamentoCota)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTravamentoCota",idTravamentoCota);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTravamentoCota": this.str.IdTravamentoCota = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataProcessamento": this.str.DataProcessamento = (string)value; break;							
						case "TipoBloqueio": this.str.TipoBloqueio = (string)value; break;							
						case "ValorCota": this.str.ValorCota = (string)value; break;							
						case "ValorCotaCalculada": this.str.ValorCotaCalculada = (string)value; break;							
						case "AjusteCompensacao": this.str.AjusteCompensacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTravamentoCota":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTravamentoCota = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataProcessamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataProcessamento = (System.DateTime?)value;
							break;
						
						case "TipoBloqueio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoBloqueio = (System.Int32?)value;
							break;
						
						case "ValorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCota = (System.Decimal?)value;
							break;
						
						case "ValorCotaCalculada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCotaCalculada = (System.Decimal?)value;
							break;
						
						case "AjusteCompensacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AjusteCompensacao = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TravamentoCotas.IdTravamentoCota
		/// </summary>
		virtual public System.Int32? IdTravamentoCota
		{
			get
			{
				return base.GetSystemInt32(TravamentoCotasMetadata.ColumnNames.IdTravamentoCota);
			}
			
			set
			{
				base.SetSystemInt32(TravamentoCotasMetadata.ColumnNames.IdTravamentoCota, value);
			}
		}
		
		/// <summary>
		/// Maps to TravamentoCotas.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(TravamentoCotasMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(TravamentoCotasMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TravamentoCotas.DataProcessamento
		/// </summary>
		virtual public System.DateTime? DataProcessamento
		{
			get
			{
				return base.GetSystemDateTime(TravamentoCotasMetadata.ColumnNames.DataProcessamento);
			}
			
			set
			{
				base.SetSystemDateTime(TravamentoCotasMetadata.ColumnNames.DataProcessamento, value);
			}
		}
		
		/// <summary>
		/// Maps to TravamentoCotas.TipoBloqueio
		/// </summary>
		virtual public System.Int32? TipoBloqueio
		{
			get
			{
				return base.GetSystemInt32(TravamentoCotasMetadata.ColumnNames.TipoBloqueio);
			}
			
			set
			{
				base.SetSystemInt32(TravamentoCotasMetadata.ColumnNames.TipoBloqueio, value);
			}
		}
		
		/// <summary>
		/// Maps to TravamentoCotas.ValorCota
		/// </summary>
		virtual public System.Decimal? ValorCota
		{
			get
			{
				return base.GetSystemDecimal(TravamentoCotasMetadata.ColumnNames.ValorCota);
			}
			
			set
			{
				base.SetSystemDecimal(TravamentoCotasMetadata.ColumnNames.ValorCota, value);
			}
		}
		
		/// <summary>
		/// Maps to TravamentoCotas.ValorCotaCalculada
		/// </summary>
		virtual public System.Decimal? ValorCotaCalculada
		{
			get
			{
				return base.GetSystemDecimal(TravamentoCotasMetadata.ColumnNames.ValorCotaCalculada);
			}
			
			set
			{
				base.SetSystemDecimal(TravamentoCotasMetadata.ColumnNames.ValorCotaCalculada, value);
			}
		}
		
		/// <summary>
		/// Maps to TravamentoCotas.AjusteCompensacao
		/// </summary>
		virtual public System.Decimal? AjusteCompensacao
		{
			get
			{
				return base.GetSystemDecimal(TravamentoCotasMetadata.ColumnNames.AjusteCompensacao);
			}
			
			set
			{
				base.SetSystemDecimal(TravamentoCotasMetadata.ColumnNames.AjusteCompensacao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTravamentoCotas entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTravamentoCota
			{
				get
				{
					System.Int32? data = entity.IdTravamentoCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTravamentoCota = null;
					else entity.IdTravamentoCota = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataProcessamento
			{
				get
				{
					System.DateTime? data = entity.DataProcessamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataProcessamento = null;
					else entity.DataProcessamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoBloqueio
			{
				get
				{
					System.Int32? data = entity.TipoBloqueio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoBloqueio = null;
					else entity.TipoBloqueio = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorCota
			{
				get
				{
					System.Decimal? data = entity.ValorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCota = null;
					else entity.ValorCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCotaCalculada
			{
				get
				{
					System.Decimal? data = entity.ValorCotaCalculada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCotaCalculada = null;
					else entity.ValorCotaCalculada = Convert.ToDecimal(value);
				}
			}
				
			public System.String AjusteCompensacao
			{
				get
				{
					System.Decimal? data = entity.AjusteCompensacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AjusteCompensacao = null;
					else entity.AjusteCompensacao = Convert.ToDecimal(value);
				}
			}
			

			private esTravamentoCotas entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTravamentoCotasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTravamentoCotas can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TravamentoCotas : esTravamentoCotas
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_TravamentoCotas_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTravamentoCotasQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TravamentoCotasMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTravamentoCota
		{
			get
			{
				return new esQueryItem(this, TravamentoCotasMetadata.ColumnNames.IdTravamentoCota, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TravamentoCotasMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataProcessamento
		{
			get
			{
				return new esQueryItem(this, TravamentoCotasMetadata.ColumnNames.DataProcessamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoBloqueio
		{
			get
			{
				return new esQueryItem(this, TravamentoCotasMetadata.ColumnNames.TipoBloqueio, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorCota
		{
			get
			{
				return new esQueryItem(this, TravamentoCotasMetadata.ColumnNames.ValorCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCotaCalculada
		{
			get
			{
				return new esQueryItem(this, TravamentoCotasMetadata.ColumnNames.ValorCotaCalculada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AjusteCompensacao
		{
			get
			{
				return new esQueryItem(this, TravamentoCotasMetadata.ColumnNames.AjusteCompensacao, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TravamentoCotasCollection")]
	public partial class TravamentoCotasCollection : esTravamentoCotasCollection, IEnumerable<TravamentoCotas>
	{
		public TravamentoCotasCollection()
		{

		}
		
		public static implicit operator List<TravamentoCotas>(TravamentoCotasCollection coll)
		{
			List<TravamentoCotas> list = new List<TravamentoCotas>();
			
			foreach (TravamentoCotas emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TravamentoCotasMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TravamentoCotasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TravamentoCotas(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TravamentoCotas();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TravamentoCotasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TravamentoCotasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TravamentoCotasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TravamentoCotas AddNew()
		{
			TravamentoCotas entity = base.AddNewEntity() as TravamentoCotas;
			
			return entity;
		}

		public TravamentoCotas FindByPrimaryKey(System.Int32 idTravamentoCota)
		{
			return base.FindByPrimaryKey(idTravamentoCota) as TravamentoCotas;
		}


		#region IEnumerable<TravamentoCotas> Members

		IEnumerator<TravamentoCotas> IEnumerable<TravamentoCotas>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TravamentoCotas;
			}
		}

		#endregion
		
		private TravamentoCotasQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TravamentoCotas' table
	/// </summary>

	[Serializable]
	public partial class TravamentoCotas : esTravamentoCotas
	{
		public TravamentoCotas()
		{

		}
	
		public TravamentoCotas(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TravamentoCotasMetadata.Meta();
			}
		}
		
		
		
		override protected esTravamentoCotasQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TravamentoCotasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TravamentoCotasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TravamentoCotasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TravamentoCotasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TravamentoCotasQuery query;
	}



	[Serializable]
	public partial class TravamentoCotasQuery : esTravamentoCotasQuery
	{
		public TravamentoCotasQuery()
		{

		}		
		
		public TravamentoCotasQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TravamentoCotasMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TravamentoCotasMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TravamentoCotasMetadata.ColumnNames.IdTravamentoCota, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TravamentoCotasMetadata.PropertyNames.IdTravamentoCota;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TravamentoCotasMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TravamentoCotasMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TravamentoCotasMetadata.ColumnNames.DataProcessamento, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TravamentoCotasMetadata.PropertyNames.DataProcessamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TravamentoCotasMetadata.ColumnNames.TipoBloqueio, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TravamentoCotasMetadata.PropertyNames.TipoBloqueio;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TravamentoCotasMetadata.ColumnNames.ValorCota, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TravamentoCotasMetadata.PropertyNames.ValorCota;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TravamentoCotasMetadata.ColumnNames.ValorCotaCalculada, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TravamentoCotasMetadata.PropertyNames.ValorCotaCalculada;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TravamentoCotasMetadata.ColumnNames.AjusteCompensacao, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TravamentoCotasMetadata.PropertyNames.AjusteCompensacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TravamentoCotasMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTravamentoCota = "IdTravamentoCota";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataProcessamento = "DataProcessamento";
			 public const string TipoBloqueio = "TipoBloqueio";
			 public const string ValorCota = "ValorCota";
			 public const string ValorCotaCalculada = "ValorCotaCalculada";
			 public const string AjusteCompensacao = "AjusteCompensacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTravamentoCota = "IdTravamentoCota";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataProcessamento = "DataProcessamento";
			 public const string TipoBloqueio = "TipoBloqueio";
			 public const string ValorCota = "ValorCota";
			 public const string ValorCotaCalculada = "ValorCotaCalculada";
			 public const string AjusteCompensacao = "AjusteCompensacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TravamentoCotasMetadata))
			{
				if(TravamentoCotasMetadata.mapDelegates == null)
				{
					TravamentoCotasMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TravamentoCotasMetadata.meta == null)
				{
					TravamentoCotasMetadata.meta = new TravamentoCotasMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTravamentoCota", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataProcessamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoBloqueio", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCotaCalculada", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AjusteCompensacao", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TravamentoCotas";
				meta.Destination = "TravamentoCotas";
				
				meta.spInsert = "proc_TravamentoCotasInsert";				
				meta.spUpdate = "proc_TravamentoCotasUpdate";		
				meta.spDelete = "proc_TravamentoCotasDelete";
				meta.spLoadAll = "proc_TravamentoCotasLoadAll";
				meta.spLoadByPrimaryKey = "proc_TravamentoCotasLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TravamentoCotasMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
