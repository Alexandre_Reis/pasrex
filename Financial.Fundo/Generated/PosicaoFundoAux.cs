/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 28/03/2016 17:28:10
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esPosicaoFundoAuxCollection : esEntityCollection
	{
		public esPosicaoFundoAuxCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoFundoAuxCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoFundoAuxQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoFundoAuxQuery);
		}
		#endregion
		
		virtual public PosicaoFundoAux DetachEntity(PosicaoFundoAux entity)
		{
			return base.DetachEntity(entity) as PosicaoFundoAux;
		}
		
		virtual public PosicaoFundoAux AttachEntity(PosicaoFundoAux entity)
		{
			return base.AttachEntity(entity) as PosicaoFundoAux;
		}
		
		virtual public void Combine(PosicaoFundoAuxCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoFundoAux this[int index]
		{
			get
			{
				return base[index] as PosicaoFundoAux;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoFundoAux);
		}
	}



	[Serializable]
	abstract public class esPosicaoFundoAux : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoFundoAuxQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoFundoAux()
		{

		}

		public esPosicaoFundoAux(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicaoFundoAux)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicaoFundoAux);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicaoFundoAux);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicaoFundoAux)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicaoFundoAux);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicaoFundoAux);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicaoFundoAux)
		{
			esPosicaoFundoAuxQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicaoFundoAux == idPosicaoFundoAux);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicaoFundoAux)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicaoFundoAux",idPosicaoFundoAux);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicaoFundoAux": this.str.IdPosicaoFundoAux = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "ValorAplicacao": this.str.ValorAplicacao = (string)value; break;							
						case "DataAplicacao": this.str.DataAplicacao = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "CotaAplicacao": this.str.CotaAplicacao = (string)value; break;							
						case "CotaDia": this.str.CotaDia = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "QuantidadeInicial": this.str.QuantidadeInicial = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeBloqueada": this.str.QuantidadeBloqueada = (string)value; break;							
						case "DataUltimaCobrancaIR": this.str.DataUltimaCobrancaIR = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "ValorIOFVirtual": this.str.ValorIOFVirtual = (string)value; break;							
						case "QuantidadeAntesCortes": this.str.QuantidadeAntesCortes = (string)value; break;							
						case "ValorRendimento": this.str.ValorRendimento = (string)value; break;							
						case "DataUltimoCortePfee": this.str.DataUltimoCortePfee = (string)value; break;							
						case "PosicaoIncorporada": this.str.PosicaoIncorporada = (string)value; break;							
						case "FieTabelaIr": this.str.FieTabelaIr = (string)value; break;							
						case "AmortizacaoAcumuladaPorCota": this.str.AmortizacaoAcumuladaPorCota = (string)value; break;							
						case "JurosAcumuladoPorCota": this.str.JurosAcumuladoPorCota = (string)value; break;							
						case "AmortizacaoAcumuladaPorValor": this.str.AmortizacaoAcumuladaPorValor = (string)value; break;							
						case "JurosAcumuladoPorValor": this.str.JurosAcumuladoPorValor = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicaoFundoAux":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoFundoAux = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "ValorAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAplicacao = (System.Decimal?)value;
							break;
						
						case "DataAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAplicacao = (System.DateTime?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "CotaAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaAplicacao = (System.Decimal?)value;
							break;
						
						case "CotaDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaDia = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "QuantidadeInicial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeInicial = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "QuantidadeBloqueada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeBloqueada = (System.Decimal?)value;
							break;
						
						case "DataUltimaCobrancaIR":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimaCobrancaIR = (System.DateTime?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "ValorIOFVirtual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOFVirtual = (System.Decimal?)value;
							break;
						
						case "QuantidadeAntesCortes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeAntesCortes = (System.Decimal?)value;
							break;
						
						case "ValorRendimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorRendimento = (System.Decimal?)value;
							break;
						
						case "DataUltimoCortePfee":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimoCortePfee = (System.DateTime?)value;
							break;
						
						case "FieTabelaIr":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FieTabelaIr = (System.Int32?)value;
							break;

						case "AmortizacaoAcumuladaPorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AmortizacaoAcumuladaPorCota = (System.Decimal?)value;
							break;
						
						case "JurosAcumuladoPorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.JurosAcumuladoPorCota = (System.Decimal?)value;
							break;
						
						case "AmortizacaoAcumuladaPorValor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AmortizacaoAcumuladaPorValor = (System.Decimal?)value;
							break;
						
						case "JurosAcumuladoPorValor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.JurosAcumuladoPorValor = (System.Decimal?)value;
							break;
					
						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoFundoAux.IdPosicaoFundoAux
		/// </summary>
		virtual public System.Int32? IdPosicaoFundoAux
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoAuxMetadata.ColumnNames.IdPosicaoFundoAux);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoFundoAuxMetadata.ColumnNames.IdPosicaoFundoAux, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(PosicaoFundoAuxMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoFundoAuxMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoAuxMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoFundoAuxMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoAuxMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoFundoAuxMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoAuxMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoFundoAuxMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoAuxMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoFundoAuxMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.ValorAplicacao
		/// </summary>
		virtual public System.Decimal? ValorAplicacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorAplicacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.DataAplicacao
		/// </summary>
		virtual public System.DateTime? DataAplicacao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoFundoAuxMetadata.ColumnNames.DataAplicacao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoFundoAuxMetadata.ColumnNames.DataAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoFundoAuxMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoFundoAuxMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.CotaAplicacao
		/// </summary>
		virtual public System.Decimal? CotaAplicacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.CotaAplicacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.CotaAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.CotaDia
		/// </summary>
		virtual public System.Decimal? CotaDia
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.CotaDia);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.CotaDia, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.QuantidadeInicial
		/// </summary>
		virtual public System.Decimal? QuantidadeInicial
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.QuantidadeInicial);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.QuantidadeInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.QuantidadeBloqueada
		/// </summary>
		virtual public System.Decimal? QuantidadeBloqueada
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.QuantidadeBloqueada);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.QuantidadeBloqueada, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.DataUltimaCobrancaIR
		/// </summary>
		virtual public System.DateTime? DataUltimaCobrancaIR
		{
			get
			{
				return base.GetSystemDateTime(PosicaoFundoAuxMetadata.ColumnNames.DataUltimaCobrancaIR);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoFundoAuxMetadata.ColumnNames.DataUltimaCobrancaIR, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.ValorIOFVirtual
		/// </summary>
		virtual public System.Decimal? ValorIOFVirtual
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorIOFVirtual);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorIOFVirtual, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.QuantidadeAntesCortes
		/// </summary>
		virtual public System.Decimal? QuantidadeAntesCortes
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.QuantidadeAntesCortes);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.QuantidadeAntesCortes, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.ValorRendimento
		/// </summary>
		virtual public System.Decimal? ValorRendimento
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorRendimento);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.ValorRendimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.DataUltimoCortePfee
		/// </summary>
		virtual public System.DateTime? DataUltimoCortePfee
		{
			get
			{
				return base.GetSystemDateTime(PosicaoFundoAuxMetadata.ColumnNames.DataUltimoCortePfee);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoFundoAuxMetadata.ColumnNames.DataUltimoCortePfee, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.PosicaoIncorporada
		/// </summary>
		virtual public System.String PosicaoIncorporada
		{
			get
			{
				return base.GetSystemString(PosicaoFundoAuxMetadata.ColumnNames.PosicaoIncorporada);
			}
			
			set
			{
				base.SetSystemString(PosicaoFundoAuxMetadata.ColumnNames.PosicaoIncorporada, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.FieTabelaIr
		/// </summary>
		virtual public System.Int32? FieTabelaIr
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoAuxMetadata.ColumnNames.FieTabelaIr);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoFundoAuxMetadata.ColumnNames.FieTabelaIr, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.AmortizacaoAcumuladaPorCota
		/// </summary>
		virtual public System.Decimal? AmortizacaoAcumuladaPorCota
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.AmortizacaoAcumuladaPorCota);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.AmortizacaoAcumuladaPorCota, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.JurosAcumuladoPorCota
		/// </summary>
		virtual public System.Decimal? JurosAcumuladoPorCota
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.JurosAcumuladoPorCota);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.JurosAcumuladoPorCota, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.AmortizacaoAcumuladaPorValor
		/// </summary>
		virtual public System.Decimal? AmortizacaoAcumuladaPorValor
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.AmortizacaoAcumuladaPorValor);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.AmortizacaoAcumuladaPorValor, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoAux.JurosAcumuladoPorValor
		/// </summary>
		virtual public System.Decimal? JurosAcumuladoPorValor
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.JurosAcumuladoPorValor);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoAuxMetadata.ColumnNames.JurosAcumuladoPorValor, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoFundoAux entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicaoFundoAux
			{
				get
				{
					System.Int32? data = entity.IdPosicaoFundoAux;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoFundoAux = null;
					else entity.IdPosicaoFundoAux = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorAplicacao
			{
				get
				{
					System.Decimal? data = entity.ValorAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAplicacao = null;
					else entity.ValorAplicacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataAplicacao
			{
				get
				{
					System.DateTime? data = entity.DataAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAplicacao = null;
					else entity.DataAplicacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String CotaAplicacao
			{
				get
				{
					System.Decimal? data = entity.CotaAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaAplicacao = null;
					else entity.CotaAplicacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaDia
			{
				get
				{
					System.Decimal? data = entity.CotaDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaDia = null;
					else entity.CotaDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeInicial
			{
				get
				{
					System.Decimal? data = entity.QuantidadeInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeInicial = null;
					else entity.QuantidadeInicial = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeBloqueada
			{
				get
				{
					System.Decimal? data = entity.QuantidadeBloqueada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeBloqueada = null;
					else entity.QuantidadeBloqueada = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataUltimaCobrancaIR
			{
				get
				{
					System.DateTime? data = entity.DataUltimaCobrancaIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimaCobrancaIR = null;
					else entity.DataUltimaCobrancaIR = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOFVirtual
			{
				get
				{
					System.Decimal? data = entity.ValorIOFVirtual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOFVirtual = null;
					else entity.ValorIOFVirtual = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeAntesCortes
			{
				get
				{
					System.Decimal? data = entity.QuantidadeAntesCortes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeAntesCortes = null;
					else entity.QuantidadeAntesCortes = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorRendimento
			{
				get
				{
					System.Decimal? data = entity.ValorRendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorRendimento = null;
					else entity.ValorRendimento = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataUltimoCortePfee
			{
				get
				{
					System.DateTime? data = entity.DataUltimoCortePfee;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimoCortePfee = null;
					else entity.DataUltimoCortePfee = Convert.ToDateTime(value);
				}
			}
				
			public System.String PosicaoIncorporada
			{
				get
				{
					System.String data = entity.PosicaoIncorporada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PosicaoIncorporada = null;
					else entity.PosicaoIncorporada = Convert.ToString(value);
				}
			}
				
			public System.String FieTabelaIr
			{
				get
				{
					System.Int32? data = entity.FieTabelaIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FieTabelaIr = null;
					else entity.FieTabelaIr = Convert.ToInt32(value);
				}
			}
				
			public System.String AmortizacaoAcumuladaPorCota
			{
				get
				{
					System.Decimal? data = entity.AmortizacaoAcumuladaPorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AmortizacaoAcumuladaPorCota = null;
					else entity.AmortizacaoAcumuladaPorCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String JurosAcumuladoPorCota
			{
				get
				{
					System.Decimal? data = entity.JurosAcumuladoPorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.JurosAcumuladoPorCota = null;
					else entity.JurosAcumuladoPorCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String AmortizacaoAcumuladaPorValor
			{
				get
				{
					System.Decimal? data = entity.AmortizacaoAcumuladaPorValor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AmortizacaoAcumuladaPorValor = null;
					else entity.AmortizacaoAcumuladaPorValor = Convert.ToDecimal(value);
				}
			}
				
			public System.String JurosAcumuladoPorValor
			{
				get
				{
					System.Decimal? data = entity.JurosAcumuladoPorValor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.JurosAcumuladoPorValor = null;
					else entity.JurosAcumuladoPorValor = Convert.ToDecimal(value);
				}
			}
			

			private esPosicaoFundoAux entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoFundoAuxQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoFundoAux can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoFundoAux : esPosicaoFundoAux
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoFundoAuxQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoFundoAuxMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicaoFundoAux
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.IdPosicaoFundoAux, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorAplicacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.ValorAplicacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataAplicacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.DataAplicacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CotaAplicacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.CotaAplicacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaDia
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.CotaDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeInicial
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.QuantidadeInicial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeBloqueada
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.QuantidadeBloqueada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataUltimaCobrancaIR
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.DataUltimaCobrancaIR, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOFVirtual
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.ValorIOFVirtual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeAntesCortes
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.QuantidadeAntesCortes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorRendimento
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.ValorRendimento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataUltimoCortePfee
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.DataUltimoCortePfee, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PosicaoIncorporada
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.PosicaoIncorporada, esSystemType.String);
			}
		} 
		
		public esQueryItem FieTabelaIr
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.FieTabelaIr, esSystemType.Int32);
			}
		} 
		
		public esQueryItem AmortizacaoAcumuladaPorCota
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.AmortizacaoAcumuladaPorCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem JurosAcumuladoPorCota
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.JurosAcumuladoPorCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AmortizacaoAcumuladaPorValor
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.AmortizacaoAcumuladaPorValor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem JurosAcumuladoPorValor
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoAuxMetadata.ColumnNames.JurosAcumuladoPorValor, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PosicaoFundoAuxCollection")]
	public partial class PosicaoFundoAuxCollection : esPosicaoFundoAuxCollection, IEnumerable<PosicaoFundoAux>
	{
		public PosicaoFundoAuxCollection()
		{

		}
		
		public static implicit operator List<PosicaoFundoAux>(PosicaoFundoAuxCollection coll)
		{
			List<PosicaoFundoAux> list = new List<PosicaoFundoAux>();
			
			foreach (PosicaoFundoAux emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoFundoAuxMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoFundoAuxQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoFundoAux(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoFundoAux();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoFundoAuxQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoFundoAuxQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoFundoAuxQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoFundoAux AddNew()
		{
			PosicaoFundoAux entity = base.AddNewEntity() as PosicaoFundoAux;
			
			return entity;
		}

		public PosicaoFundoAux FindByPrimaryKey(System.Int32 idPosicaoFundoAux)
		{
			return base.FindByPrimaryKey(idPosicaoFundoAux) as PosicaoFundoAux;
		}


		#region IEnumerable<PosicaoFundoAux> Members

		IEnumerator<PosicaoFundoAux> IEnumerable<PosicaoFundoAux>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoFundoAux;
			}
		}

		#endregion
		
		private PosicaoFundoAuxQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoFundoAux' table
	/// </summary>

	[Serializable]
	public partial class PosicaoFundoAux : esPosicaoFundoAux
	{
		public PosicaoFundoAux()
		{

		}
	
		public PosicaoFundoAux(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoFundoAuxMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoFundoAuxQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoFundoAuxQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoFundoAuxQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoFundoAuxQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoFundoAuxQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoFundoAuxQuery query;
	}



	[Serializable]
	public partial class PosicaoFundoAuxQuery : esPosicaoFundoAuxQuery
	{
		public PosicaoFundoAuxQuery()
		{

		}		
		
		public PosicaoFundoAuxQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoFundoAuxMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoFundoAuxMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.IdPosicaoFundoAux, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.IdPosicaoFundoAux;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.DataReferencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.IdPosicao, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.IdPosicao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.IdOperacao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.IdCliente, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.IdCarteira, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.ValorAplicacao, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.ValorAplicacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.DataAplicacao, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.DataAplicacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.DataConversao, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.CotaAplicacao, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.CotaAplicacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.CotaDia, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.CotaDia;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.ValorBruto, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.ValorLiquido, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.QuantidadeInicial, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.QuantidadeInicial;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.Quantidade, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.QuantidadeBloqueada, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.QuantidadeBloqueada;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.DataUltimaCobrancaIR, 16, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.DataUltimaCobrancaIR;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.ValorIR, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.ValorIOF, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.ValorPerformance, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.ValorIOFVirtual, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.ValorIOFVirtual;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.QuantidadeAntesCortes, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.QuantidadeAntesCortes;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.ValorRendimento, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.ValorRendimento;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.DataUltimoCortePfee, 23, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.DataUltimoCortePfee;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.PosicaoIncorporada, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.PosicaoIncorporada;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			

			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.FieTabelaIr, 25, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.FieTabelaIr;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.AmortizacaoAcumuladaPorCota, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.AmortizacaoAcumuladaPorCota;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.JurosAcumuladoPorCota, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.JurosAcumuladoPorCota;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.AmortizacaoAcumuladaPorValor, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.AmortizacaoAcumuladaPorValor;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoAuxMetadata.ColumnNames.JurosAcumuladoPorValor, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoAuxMetadata.PropertyNames.JurosAcumuladoPorValor;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PosicaoFundoAuxMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicaoFundoAux = "IdPosicaoFundoAux";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdPosicao = "IdPosicao";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorAplicacao = "ValorAplicacao";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string DataConversao = "DataConversao";
			 public const string CotaAplicacao = "CotaAplicacao";
			 public const string CotaDia = "CotaDia";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string DataUltimaCobrancaIR = "DataUltimaCobrancaIR";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string ValorIOFVirtual = "ValorIOFVirtual";
			 public const string QuantidadeAntesCortes = "QuantidadeAntesCortes";
			 public const string ValorRendimento = "ValorRendimento";
			 public const string DataUltimoCortePfee = "DataUltimoCortePfee";
			 public const string PosicaoIncorporada = "PosicaoIncorporada";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string AmortizacaoAcumuladaPorCota = "AmortizacaoAcumuladaPorCota";
			 public const string JurosAcumuladoPorCota = "JurosAcumuladoPorCota";
			 public const string AmortizacaoAcumuladaPorValor = "AmortizacaoAcumuladaPorValor";
			 public const string JurosAcumuladoPorValor = "JurosAcumuladoPorValor";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicaoFundoAux = "IdPosicaoFundoAux";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdPosicao = "IdPosicao";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorAplicacao = "ValorAplicacao";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string DataConversao = "DataConversao";
			 public const string CotaAplicacao = "CotaAplicacao";
			 public const string CotaDia = "CotaDia";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string DataUltimaCobrancaIR = "DataUltimaCobrancaIR";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string ValorIOFVirtual = "ValorIOFVirtual";
			 public const string QuantidadeAntesCortes = "QuantidadeAntesCortes";
			 public const string ValorRendimento = "ValorRendimento";
			 public const string DataUltimoCortePfee = "DataUltimoCortePfee";
			 public const string PosicaoIncorporada = "PosicaoIncorporada";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string AmortizacaoAcumuladaPorCota = "AmortizacaoAcumuladaPorCota";
			 public const string JurosAcumuladoPorCota = "JurosAcumuladoPorCota";
			 public const string AmortizacaoAcumuladaPorValor = "AmortizacaoAcumuladaPorValor";
			 public const string JurosAcumuladoPorValor = "JurosAcumuladoPorValor";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoFundoAuxMetadata))
			{
				if(PosicaoFundoAuxMetadata.mapDelegates == null)
				{
					PosicaoFundoAuxMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoFundoAuxMetadata.meta == null)
				{
					PosicaoFundoAuxMetadata.meta = new PosicaoFundoAuxMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicaoFundoAux", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorAplicacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataAplicacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CotaAplicacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeInicial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeBloqueada", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataUltimaCobrancaIR", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOFVirtual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeAntesCortes", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorRendimento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataUltimoCortePfee", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PosicaoIncorporada", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FieTabelaIr", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AmortizacaoAcumuladaPorCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("JurosAcumuladoPorCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AmortizacaoAcumuladaPorValor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("JurosAcumuladoPorValor", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "PosicaoFundoAux";
				meta.Destination = "PosicaoFundoAux";
				
				meta.spInsert = "proc_PosicaoFundoAuxInsert";				
				meta.spUpdate = "proc_PosicaoFundoAuxUpdate";		
				meta.spDelete = "proc_PosicaoFundoAuxDelete";
				meta.spLoadAll = "proc_PosicaoFundoAuxLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoFundoAuxLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoFundoAuxMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
