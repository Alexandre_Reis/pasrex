/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:22 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTabelaReclamacoesCotistasCollection : esEntityCollection
	{
		public esTabelaReclamacoesCotistasCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaReclamacoesCotistasCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaReclamacoesCotistasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaReclamacoesCotistasQuery);
		}
		#endregion
		
		virtual public TabelaReclamacoesCotistas DetachEntity(TabelaReclamacoesCotistas entity)
		{
			return base.DetachEntity(entity) as TabelaReclamacoesCotistas;
		}
		
		virtual public TabelaReclamacoesCotistas AttachEntity(TabelaReclamacoesCotistas entity)
		{
			return base.AttachEntity(entity) as TabelaReclamacoesCotistas;
		}
		
		virtual public void Combine(TabelaReclamacoesCotistasCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaReclamacoesCotistas this[int index]
		{
			get
			{
				return base[index] as TabelaReclamacoesCotistas;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaReclamacoesCotistas);
		}
	}



	[Serializable]
	abstract public class esTabelaReclamacoesCotistas : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaReclamacoesCotistasQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaReclamacoesCotistas()
		{

		}

		public esTabelaReclamacoesCotistas(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data);
			else
				return LoadByPrimaryKeyStoredProcedure(data);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime data)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaReclamacoesCotistasQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Data == data);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data);
			else
				return LoadByPrimaryKeyStoredProcedure(data);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data)
		{
			esTabelaReclamacoesCotistasQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "Procedentes": this.str.Procedentes = (string)value; break;							
						case "Improcedentes": this.str.Improcedentes = (string)value; break;							
						case "Resolvidas": this.str.Resolvidas = (string)value; break;							
						case "NaoResolvidas": this.str.NaoResolvidas = (string)value; break;							
						case "Administracao": this.str.Administracao = (string)value; break;							
						case "Gestao": this.str.Gestao = (string)value; break;							
						case "Taxas": this.str.Taxas = (string)value; break;							
						case "PoliticaInvestimento": this.str.PoliticaInvestimento = (string)value; break;							
						case "Aportes": this.str.Aportes = (string)value; break;							
						case "Resgates": this.str.Resgates = (string)value; break;							
						case "FornecimentoInformacoes": this.str.FornecimentoInformacoes = (string)value; break;							
						case "AssembleiasGerais": this.str.AssembleiasGerais = (string)value; break;							
						case "Cotas": this.str.Cotas = (string)value; break;							
						case "Outros": this.str.Outros = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Procedentes":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Procedentes = (System.Int32?)value;
							break;
						
						case "Improcedentes":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Improcedentes = (System.Int32?)value;
							break;
						
						case "Resolvidas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Resolvidas = (System.Int32?)value;
							break;
						
						case "NaoResolvidas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NaoResolvidas = (System.Int32?)value;
							break;
						
						case "Administracao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Administracao = (System.Int32?)value;
							break;
						
						case "Gestao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Gestao = (System.Int32?)value;
							break;
						
						case "Taxas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Taxas = (System.Int32?)value;
							break;
						
						case "PoliticaInvestimento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PoliticaInvestimento = (System.Int32?)value;
							break;
						
						case "Aportes":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Aportes = (System.Int32?)value;
							break;
						
						case "Resgates":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Resgates = (System.Int32?)value;
							break;
						
						case "FornecimentoInformacoes":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FornecimentoInformacoes = (System.Int32?)value;
							break;
						
						case "AssembleiasGerais":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.AssembleiasGerais = (System.Int32?)value;
							break;
						
						case "Cotas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Cotas = (System.Int32?)value;
							break;
						
						case "Outros":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Outros = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaReclamacoesCotistas.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(TabelaReclamacoesCotistasMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaReclamacoesCotistasMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaReclamacoesCotistas.Procedentes
		/// </summary>
		virtual public System.Int32? Procedentes
		{
			get
			{
				return base.GetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Procedentes);
			}
			
			set
			{
				base.SetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Procedentes, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaReclamacoesCotistas.Improcedentes
		/// </summary>
		virtual public System.Int32? Improcedentes
		{
			get
			{
				return base.GetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Improcedentes);
			}
			
			set
			{
				base.SetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Improcedentes, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaReclamacoesCotistas.Resolvidas
		/// </summary>
		virtual public System.Int32? Resolvidas
		{
			get
			{
				return base.GetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Resolvidas);
			}
			
			set
			{
				base.SetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Resolvidas, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaReclamacoesCotistas.NaoResolvidas
		/// </summary>
		virtual public System.Int32? NaoResolvidas
		{
			get
			{
				return base.GetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.NaoResolvidas);
			}
			
			set
			{
				base.SetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.NaoResolvidas, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaReclamacoesCotistas.Administracao
		/// </summary>
		virtual public System.Int32? Administracao
		{
			get
			{
				return base.GetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Administracao);
			}
			
			set
			{
				base.SetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Administracao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaReclamacoesCotistas.Gestao
		/// </summary>
		virtual public System.Int32? Gestao
		{
			get
			{
				return base.GetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Gestao);
			}
			
			set
			{
				base.SetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Gestao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaReclamacoesCotistas.Taxas
		/// </summary>
		virtual public System.Int32? Taxas
		{
			get
			{
				return base.GetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Taxas);
			}
			
			set
			{
				base.SetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Taxas, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaReclamacoesCotistas.PoliticaInvestimento
		/// </summary>
		virtual public System.Int32? PoliticaInvestimento
		{
			get
			{
				return base.GetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.PoliticaInvestimento);
			}
			
			set
			{
				base.SetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.PoliticaInvestimento, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaReclamacoesCotistas.Aportes
		/// </summary>
		virtual public System.Int32? Aportes
		{
			get
			{
				return base.GetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Aportes);
			}
			
			set
			{
				base.SetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Aportes, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaReclamacoesCotistas.Resgates
		/// </summary>
		virtual public System.Int32? Resgates
		{
			get
			{
				return base.GetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Resgates);
			}
			
			set
			{
				base.SetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Resgates, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaReclamacoesCotistas.FornecimentoInformacoes
		/// </summary>
		virtual public System.Int32? FornecimentoInformacoes
		{
			get
			{
				return base.GetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.FornecimentoInformacoes);
			}
			
			set
			{
				base.SetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.FornecimentoInformacoes, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaReclamacoesCotistas.AssembleiasGerais
		/// </summary>
		virtual public System.Int32? AssembleiasGerais
		{
			get
			{
				return base.GetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.AssembleiasGerais);
			}
			
			set
			{
				base.SetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.AssembleiasGerais, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaReclamacoesCotistas.Cotas
		/// </summary>
		virtual public System.Int32? Cotas
		{
			get
			{
				return base.GetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Cotas);
			}
			
			set
			{
				base.SetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Cotas, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaReclamacoesCotistas.Outros
		/// </summary>
		virtual public System.Int32? Outros
		{
			get
			{
				return base.GetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Outros);
			}
			
			set
			{
				base.SetSystemInt32(TabelaReclamacoesCotistasMetadata.ColumnNames.Outros, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaReclamacoesCotistas entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Procedentes
			{
				get
				{
					System.Int32? data = entity.Procedentes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Procedentes = null;
					else entity.Procedentes = Convert.ToInt32(value);
				}
			}
				
			public System.String Improcedentes
			{
				get
				{
					System.Int32? data = entity.Improcedentes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Improcedentes = null;
					else entity.Improcedentes = Convert.ToInt32(value);
				}
			}
				
			public System.String Resolvidas
			{
				get
				{
					System.Int32? data = entity.Resolvidas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Resolvidas = null;
					else entity.Resolvidas = Convert.ToInt32(value);
				}
			}
				
			public System.String NaoResolvidas
			{
				get
				{
					System.Int32? data = entity.NaoResolvidas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NaoResolvidas = null;
					else entity.NaoResolvidas = Convert.ToInt32(value);
				}
			}
				
			public System.String Administracao
			{
				get
				{
					System.Int32? data = entity.Administracao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Administracao = null;
					else entity.Administracao = Convert.ToInt32(value);
				}
			}
				
			public System.String Gestao
			{
				get
				{
					System.Int32? data = entity.Gestao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Gestao = null;
					else entity.Gestao = Convert.ToInt32(value);
				}
			}
				
			public System.String Taxas
			{
				get
				{
					System.Int32? data = entity.Taxas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxas = null;
					else entity.Taxas = Convert.ToInt32(value);
				}
			}
				
			public System.String PoliticaInvestimento
			{
				get
				{
					System.Int32? data = entity.PoliticaInvestimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PoliticaInvestimento = null;
					else entity.PoliticaInvestimento = Convert.ToInt32(value);
				}
			}
				
			public System.String Aportes
			{
				get
				{
					System.Int32? data = entity.Aportes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Aportes = null;
					else entity.Aportes = Convert.ToInt32(value);
				}
			}
				
			public System.String Resgates
			{
				get
				{
					System.Int32? data = entity.Resgates;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Resgates = null;
					else entity.Resgates = Convert.ToInt32(value);
				}
			}
				
			public System.String FornecimentoInformacoes
			{
				get
				{
					System.Int32? data = entity.FornecimentoInformacoes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FornecimentoInformacoes = null;
					else entity.FornecimentoInformacoes = Convert.ToInt32(value);
				}
			}
				
			public System.String AssembleiasGerais
			{
				get
				{
					System.Int32? data = entity.AssembleiasGerais;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AssembleiasGerais = null;
					else entity.AssembleiasGerais = Convert.ToInt32(value);
				}
			}
				
			public System.String Cotas
			{
				get
				{
					System.Int32? data = entity.Cotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cotas = null;
					else entity.Cotas = Convert.ToInt32(value);
				}
			}
				
			public System.String Outros
			{
				get
				{
					System.Int32? data = entity.Outros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Outros = null;
					else entity.Outros = Convert.ToInt32(value);
				}
			}
			

			private esTabelaReclamacoesCotistas entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaReclamacoesCotistasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaReclamacoesCotistas can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaReclamacoesCotistas : esTabelaReclamacoesCotistas
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaReclamacoesCotistasQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaReclamacoesCotistasMetadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, TabelaReclamacoesCotistasMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Procedentes
		{
			get
			{
				return new esQueryItem(this, TabelaReclamacoesCotistasMetadata.ColumnNames.Procedentes, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Improcedentes
		{
			get
			{
				return new esQueryItem(this, TabelaReclamacoesCotistasMetadata.ColumnNames.Improcedentes, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Resolvidas
		{
			get
			{
				return new esQueryItem(this, TabelaReclamacoesCotistasMetadata.ColumnNames.Resolvidas, esSystemType.Int32);
			}
		} 
		
		public esQueryItem NaoResolvidas
		{
			get
			{
				return new esQueryItem(this, TabelaReclamacoesCotistasMetadata.ColumnNames.NaoResolvidas, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Administracao
		{
			get
			{
				return new esQueryItem(this, TabelaReclamacoesCotistasMetadata.ColumnNames.Administracao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Gestao
		{
			get
			{
				return new esQueryItem(this, TabelaReclamacoesCotistasMetadata.ColumnNames.Gestao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Taxas
		{
			get
			{
				return new esQueryItem(this, TabelaReclamacoesCotistasMetadata.ColumnNames.Taxas, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PoliticaInvestimento
		{
			get
			{
				return new esQueryItem(this, TabelaReclamacoesCotistasMetadata.ColumnNames.PoliticaInvestimento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Aportes
		{
			get
			{
				return new esQueryItem(this, TabelaReclamacoesCotistasMetadata.ColumnNames.Aportes, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Resgates
		{
			get
			{
				return new esQueryItem(this, TabelaReclamacoesCotistasMetadata.ColumnNames.Resgates, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FornecimentoInformacoes
		{
			get
			{
				return new esQueryItem(this, TabelaReclamacoesCotistasMetadata.ColumnNames.FornecimentoInformacoes, esSystemType.Int32);
			}
		} 
		
		public esQueryItem AssembleiasGerais
		{
			get
			{
				return new esQueryItem(this, TabelaReclamacoesCotistasMetadata.ColumnNames.AssembleiasGerais, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Cotas
		{
			get
			{
				return new esQueryItem(this, TabelaReclamacoesCotistasMetadata.ColumnNames.Cotas, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Outros
		{
			get
			{
				return new esQueryItem(this, TabelaReclamacoesCotistasMetadata.ColumnNames.Outros, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaReclamacoesCotistasCollection")]
	public partial class TabelaReclamacoesCotistasCollection : esTabelaReclamacoesCotistasCollection, IEnumerable<TabelaReclamacoesCotistas>
	{
		public TabelaReclamacoesCotistasCollection()
		{

		}
		
		public static implicit operator List<TabelaReclamacoesCotistas>(TabelaReclamacoesCotistasCollection coll)
		{
			List<TabelaReclamacoesCotistas> list = new List<TabelaReclamacoesCotistas>();
			
			foreach (TabelaReclamacoesCotistas emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaReclamacoesCotistasMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaReclamacoesCotistasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaReclamacoesCotistas(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaReclamacoesCotistas();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaReclamacoesCotistasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaReclamacoesCotistasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaReclamacoesCotistasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaReclamacoesCotistas AddNew()
		{
			TabelaReclamacoesCotistas entity = base.AddNewEntity() as TabelaReclamacoesCotistas;
			
			return entity;
		}

		public TabelaReclamacoesCotistas FindByPrimaryKey(System.DateTime data)
		{
			return base.FindByPrimaryKey(data) as TabelaReclamacoesCotistas;
		}


		#region IEnumerable<TabelaReclamacoesCotistas> Members

		IEnumerator<TabelaReclamacoesCotistas> IEnumerable<TabelaReclamacoesCotistas>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaReclamacoesCotistas;
			}
		}

		#endregion
		
		private TabelaReclamacoesCotistasQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaReclamacoesCotistas' table
	/// </summary>

	[Serializable]
	public partial class TabelaReclamacoesCotistas : esTabelaReclamacoesCotistas
	{
		public TabelaReclamacoesCotistas()
		{

		}
	
		public TabelaReclamacoesCotistas(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaReclamacoesCotistasMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaReclamacoesCotistasQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaReclamacoesCotistasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaReclamacoesCotistasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaReclamacoesCotistasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaReclamacoesCotistasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaReclamacoesCotistasQuery query;
	}



	[Serializable]
	public partial class TabelaReclamacoesCotistasQuery : esTabelaReclamacoesCotistasQuery
	{
		public TabelaReclamacoesCotistasQuery()
		{

		}		
		
		public TabelaReclamacoesCotistasQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaReclamacoesCotistasMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaReclamacoesCotistasMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaReclamacoesCotistasMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaReclamacoesCotistasMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaReclamacoesCotistasMetadata.ColumnNames.Procedentes, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaReclamacoesCotistasMetadata.PropertyNames.Procedentes;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaReclamacoesCotistasMetadata.ColumnNames.Improcedentes, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaReclamacoesCotistasMetadata.PropertyNames.Improcedentes;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaReclamacoesCotistasMetadata.ColumnNames.Resolvidas, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaReclamacoesCotistasMetadata.PropertyNames.Resolvidas;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaReclamacoesCotistasMetadata.ColumnNames.NaoResolvidas, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaReclamacoesCotistasMetadata.PropertyNames.NaoResolvidas;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaReclamacoesCotistasMetadata.ColumnNames.Administracao, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaReclamacoesCotistasMetadata.PropertyNames.Administracao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaReclamacoesCotistasMetadata.ColumnNames.Gestao, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaReclamacoesCotistasMetadata.PropertyNames.Gestao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaReclamacoesCotistasMetadata.ColumnNames.Taxas, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaReclamacoesCotistasMetadata.PropertyNames.Taxas;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaReclamacoesCotistasMetadata.ColumnNames.PoliticaInvestimento, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaReclamacoesCotistasMetadata.PropertyNames.PoliticaInvestimento;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaReclamacoesCotistasMetadata.ColumnNames.Aportes, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaReclamacoesCotistasMetadata.PropertyNames.Aportes;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaReclamacoesCotistasMetadata.ColumnNames.Resgates, 10, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaReclamacoesCotistasMetadata.PropertyNames.Resgates;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaReclamacoesCotistasMetadata.ColumnNames.FornecimentoInformacoes, 11, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaReclamacoesCotistasMetadata.PropertyNames.FornecimentoInformacoes;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaReclamacoesCotistasMetadata.ColumnNames.AssembleiasGerais, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaReclamacoesCotistasMetadata.PropertyNames.AssembleiasGerais;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaReclamacoesCotistasMetadata.ColumnNames.Cotas, 13, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaReclamacoesCotistasMetadata.PropertyNames.Cotas;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaReclamacoesCotistasMetadata.ColumnNames.Outros, 14, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaReclamacoesCotistasMetadata.PropertyNames.Outros;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaReclamacoesCotistasMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string Procedentes = "Procedentes";
			 public const string Improcedentes = "Improcedentes";
			 public const string Resolvidas = "Resolvidas";
			 public const string NaoResolvidas = "NaoResolvidas";
			 public const string Administracao = "Administracao";
			 public const string Gestao = "Gestao";
			 public const string Taxas = "Taxas";
			 public const string PoliticaInvestimento = "PoliticaInvestimento";
			 public const string Aportes = "Aportes";
			 public const string Resgates = "Resgates";
			 public const string FornecimentoInformacoes = "FornecimentoInformacoes";
			 public const string AssembleiasGerais = "AssembleiasGerais";
			 public const string Cotas = "Cotas";
			 public const string Outros = "Outros";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string Procedentes = "Procedentes";
			 public const string Improcedentes = "Improcedentes";
			 public const string Resolvidas = "Resolvidas";
			 public const string NaoResolvidas = "NaoResolvidas";
			 public const string Administracao = "Administracao";
			 public const string Gestao = "Gestao";
			 public const string Taxas = "Taxas";
			 public const string PoliticaInvestimento = "PoliticaInvestimento";
			 public const string Aportes = "Aportes";
			 public const string Resgates = "Resgates";
			 public const string FornecimentoInformacoes = "FornecimentoInformacoes";
			 public const string AssembleiasGerais = "AssembleiasGerais";
			 public const string Cotas = "Cotas";
			 public const string Outros = "Outros";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaReclamacoesCotistasMetadata))
			{
				if(TabelaReclamacoesCotistasMetadata.mapDelegates == null)
				{
					TabelaReclamacoesCotistasMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaReclamacoesCotistasMetadata.meta == null)
				{
					TabelaReclamacoesCotistasMetadata.meta = new TabelaReclamacoesCotistasMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Procedentes", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Improcedentes", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Resolvidas", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("NaoResolvidas", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Administracao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Gestao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Taxas", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PoliticaInvestimento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Aportes", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Resgates", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FornecimentoInformacoes", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AssembleiasGerais", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Cotas", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Outros", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "TabelaReclamacoesCotistas";
				meta.Destination = "TabelaReclamacoesCotistas";
				
				meta.spInsert = "proc_TabelaReclamacoesCotistasInsert";				
				meta.spUpdate = "proc_TabelaReclamacoesCotistasUpdate";		
				meta.spDelete = "proc_TabelaReclamacoesCotistasDelete";
				meta.spLoadAll = "proc_TabelaReclamacoesCotistasLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaReclamacoesCotistasLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaReclamacoesCotistasMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
