/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 19/04/2016 17:07:28
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Common;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esOperacaoFundoCollection : esEntityCollection
	{
		public esOperacaoFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OperacaoFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esOperacaoFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOperacaoFundoQuery);
		}
		#endregion
		
		virtual public OperacaoFundo DetachEntity(OperacaoFundo entity)
		{
			return base.DetachEntity(entity) as OperacaoFundo;
		}
		
		virtual public OperacaoFundo AttachEntity(OperacaoFundo entity)
		{
			return base.AttachEntity(entity) as OperacaoFundo;
		}
		
		virtual public void Combine(OperacaoFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OperacaoFundo this[int index]
		{
			get
			{
				return base[index] as OperacaoFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OperacaoFundo);
		}
	}



	[Serializable]
	abstract public class esOperacaoFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOperacaoFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esOperacaoFundo()
		{

		}

		public esOperacaoFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao)
		{
			esOperacaoFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "DataLiquidacao": this.str.DataLiquidacao = (string)value; break;							
						case "DataAgendamento": this.str.DataAgendamento = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "TipoResgate": this.str.TipoResgate = (string)value; break;							
						case "IdPosicaoResgatada": this.str.IdPosicaoResgatada = (string)value; break;							
						case "IdFormaLiquidacao": this.str.IdFormaLiquidacao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "CotaOperacao": this.str.CotaOperacao = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "ValorCPMF": this.str.ValorCPMF = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "PrejuizoUsado": this.str.PrejuizoUsado = (string)value; break;							
						case "RendimentoResgate": this.str.RendimentoResgate = (string)value; break;							
						case "VariacaoResgate": this.str.VariacaoResgate = (string)value; break;							
						case "IdConta": this.str.IdConta = (string)value; break;							
						case "Observacao": this.str.Observacao = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "IdAgenda": this.str.IdAgenda = (string)value; break;							
						case "CotaInformada": this.str.CotaInformada = (string)value; break;							
						case "IdOperacaoResgatada": this.str.IdOperacaoResgatada = (string)value; break;							
						case "RegistroEditado": this.str.RegistroEditado = (string)value; break;							
						case "ValoresColados": this.str.ValoresColados = (string)value; break;							
						case "IdLocalNegociacao": this.str.IdLocalNegociacao = (string)value; break;							
						case "DataRegistro": this.str.DataRegistro = (string)value; break;							
						case "DepositoRetirada": this.str.DepositoRetirada = (string)value; break;							
						case "IdBoletaExterna": this.str.IdBoletaExterna = (string)value; break;							
						case "FieModalidade": this.str.FieModalidade = (string)value; break;							
						case "FieTabelaIr": this.str.FieTabelaIr = (string)value; break;							
						case "DataAplicCautelaResgatada": this.str.DataAplicCautelaResgatada = (string)value; break;							
						case "UserTimeStamp": this.str.UserTimeStamp = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "IdCategoriaMovimentacao": this.str.IdCategoriaMovimentacao = (string)value; break;							
						case "ExclusaoLogica": this.str.ExclusaoLogica = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "DataLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiquidacao = (System.DateTime?)value;
							break;
						
						case "DataAgendamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAgendamento = (System.DateTime?)value;
							break;
						
						case "TipoOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoOperacao = (System.Byte?)value;
							break;
						
						case "TipoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoResgate = (System.Byte?)value;
							break;
						
						case "IdPosicaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoResgatada = (System.Int32?)value;
							break;
						
						case "IdFormaLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdFormaLiquidacao = (System.Byte?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "CotaOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaOperacao = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "ValorCPMF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMF = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "PrejuizoUsado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoUsado = (System.Decimal?)value;
							break;
						
						case "RendimentoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoResgate = (System.Decimal?)value;
							break;
						
						case "VariacaoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VariacaoResgate = (System.Decimal?)value;
							break;
						
						case "IdConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConta = (System.Int32?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "IdAgenda":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenda = (System.Int32?)value;
							break;
						
						case "CotaInformada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaInformada = (System.Decimal?)value;
							break;
						
						case "IdOperacaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoResgatada = (System.Int32?)value;
							break;
						
						case "IdLocalNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalNegociacao = (System.Int32?)value;
							break;
						
						case "DataRegistro":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRegistro = (System.DateTime?)value;
							break;
						
						case "DepositoRetirada":
						
							if (value == null || value.GetType().ToString() == "System.Boolean")
								this.DepositoRetirada = (System.Boolean?)value;
							break;
						
						case "IdBoletaExterna":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdBoletaExterna = (System.Int32?)value;
							break;
						
						case "FieModalidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FieModalidade = (System.Int32?)value;
							break;
						
						case "FieTabelaIr":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FieTabelaIr = (System.Int32?)value;
							break;
						
						case "DataAplicCautelaResgatada":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAplicCautelaResgatada = (System.DateTime?)value;
							break;
						
						case "UserTimeStamp":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.UserTimeStamp = (System.DateTime?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "IdCategoriaMovimentacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCategoriaMovimentacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OperacaoFundo.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoFundoMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoFundoMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoFundoMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoFundoMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.DataLiquidacao
		/// </summary>
		virtual public System.DateTime? DataLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoFundoMetadata.ColumnNames.DataLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoFundoMetadata.ColumnNames.DataLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.DataAgendamento
		/// </summary>
		virtual public System.DateTime? DataAgendamento
		{
			get
			{
				return base.GetSystemDateTime(OperacaoFundoMetadata.ColumnNames.DataAgendamento);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoFundoMetadata.ColumnNames.DataAgendamento, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.TipoOperacao
		/// </summary>
		virtual public System.Byte? TipoOperacao
		{
			get
			{
				return base.GetSystemByte(OperacaoFundoMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemByte(OperacaoFundoMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.TipoResgate
		/// </summary>
		virtual public System.Byte? TipoResgate
		{
			get
			{
				return base.GetSystemByte(OperacaoFundoMetadata.ColumnNames.TipoResgate);
			}
			
			set
			{
				base.SetSystemByte(OperacaoFundoMetadata.ColumnNames.TipoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.IdPosicaoResgatada
		/// </summary>
		virtual public System.Int32? IdPosicaoResgatada
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdPosicaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdPosicaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.IdFormaLiquidacao
		/// </summary>
		virtual public System.Byte? IdFormaLiquidacao
		{
			get
			{
				return base.GetSystemByte(OperacaoFundoMetadata.ColumnNames.IdFormaLiquidacao);
			}
			
			set
			{
				if(base.SetSystemByte(OperacaoFundoMetadata.ColumnNames.IdFormaLiquidacao, value))
				{
					this._UpToFormaLiquidacaoByIdFormaLiquidacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.CotaOperacao
		/// </summary>
		virtual public System.Decimal? CotaOperacao
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoMetadata.ColumnNames.CotaOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoMetadata.ColumnNames.CotaOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.ValorCPMF
		/// </summary>
		virtual public System.Decimal? ValorCPMF
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoMetadata.ColumnNames.ValorCPMF);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoMetadata.ColumnNames.ValorCPMF, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.PrejuizoUsado
		/// </summary>
		virtual public System.Decimal? PrejuizoUsado
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoMetadata.ColumnNames.PrejuizoUsado);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoMetadata.ColumnNames.PrejuizoUsado, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.RendimentoResgate
		/// </summary>
		virtual public System.Decimal? RendimentoResgate
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoMetadata.ColumnNames.RendimentoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoMetadata.ColumnNames.RendimentoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.VariacaoResgate
		/// </summary>
		virtual public System.Decimal? VariacaoResgate
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoMetadata.ColumnNames.VariacaoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoMetadata.ColumnNames.VariacaoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.IdConta
		/// </summary>
		virtual public System.Int32? IdConta
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdConta);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdConta, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.Observacao
		/// </summary>
		virtual public System.String Observacao
		{
			get
			{
				return base.GetSystemString(OperacaoFundoMetadata.ColumnNames.Observacao);
			}
			
			set
			{
				base.SetSystemString(OperacaoFundoMetadata.ColumnNames.Observacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(OperacaoFundoMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(OperacaoFundoMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.IdAgenda
		/// </summary>
		virtual public System.Int32? IdAgenda
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdAgenda);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdAgenda, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.CotaInformada
		/// </summary>
		virtual public System.Decimal? CotaInformada
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoMetadata.ColumnNames.CotaInformada);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoMetadata.ColumnNames.CotaInformada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.IdOperacaoResgatada
		/// </summary>
		virtual public System.Int32? IdOperacaoResgatada
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdOperacaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdOperacaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.RegistroEditado
		/// </summary>
		virtual public System.String RegistroEditado
		{
			get
			{
				return base.GetSystemString(OperacaoFundoMetadata.ColumnNames.RegistroEditado);
			}
			
			set
			{
				base.SetSystemString(OperacaoFundoMetadata.ColumnNames.RegistroEditado, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.ValoresColados
		/// </summary>
		virtual public System.String ValoresColados
		{
			get
			{
				return base.GetSystemString(OperacaoFundoMetadata.ColumnNames.ValoresColados);
			}
			
			set
			{
				base.SetSystemString(OperacaoFundoMetadata.ColumnNames.ValoresColados, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.IdLocalNegociacao
		/// </summary>
		virtual public System.Int32? IdLocalNegociacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdLocalNegociacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdLocalNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.DataRegistro
		/// </summary>
		virtual public System.DateTime? DataRegistro
		{
			get
			{
				return base.GetSystemDateTime(OperacaoFundoMetadata.ColumnNames.DataRegistro);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoFundoMetadata.ColumnNames.DataRegistro, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.DepositoRetirada
		/// </summary>
		virtual public System.Boolean? DepositoRetirada
		{
			get
			{
				return base.GetSystemBoolean(OperacaoFundoMetadata.ColumnNames.DepositoRetirada);
			}
			
			set
			{
				base.SetSystemBoolean(OperacaoFundoMetadata.ColumnNames.DepositoRetirada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.IdBoletaExterna
		/// </summary>
		virtual public System.Int32? IdBoletaExterna
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdBoletaExterna);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdBoletaExterna, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.FieModalidade
		/// </summary>
		virtual public System.Int32? FieModalidade
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoMetadata.ColumnNames.FieModalidade);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoMetadata.ColumnNames.FieModalidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.FieTabelaIr
		/// </summary>
		virtual public System.Int32? FieTabelaIr
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoMetadata.ColumnNames.FieTabelaIr);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoMetadata.ColumnNames.FieTabelaIr, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.DataAplicCautelaResgatada
		/// </summary>
		virtual public System.DateTime? DataAplicCautelaResgatada
		{
			get
			{
				return base.GetSystemDateTime(OperacaoFundoMetadata.ColumnNames.DataAplicCautelaResgatada);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoFundoMetadata.ColumnNames.DataAplicCautelaResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.UserTimeStamp
		/// </summary>
		virtual public System.DateTime? UserTimeStamp
		{
			get
			{
				return base.GetSystemDateTime(OperacaoFundoMetadata.ColumnNames.UserTimeStamp);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoFundoMetadata.ColumnNames.UserTimeStamp, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.IdCategoriaMovimentacao
		/// </summary>
		virtual public System.Int32? IdCategoriaMovimentacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdCategoriaMovimentacao);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoFundoMetadata.ColumnNames.IdCategoriaMovimentacao, value))
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundo.ExclusaoLogica
		/// </summary>
		virtual public System.String ExclusaoLogica
		{
			get
			{
				return base.GetSystemString(OperacaoFundoMetadata.ColumnNames.ExclusaoLogica);
			}
			
			set
			{
				base.SetSystemString(OperacaoFundoMetadata.ColumnNames.ExclusaoLogica, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected CategoriaMovimentacao _UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected FormaLiquidacao _UpToFormaLiquidacaoByIdFormaLiquidacao;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOperacaoFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DataLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiquidacao = null;
					else entity.DataLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataAgendamento
			{
				get
				{
					System.DateTime? data = entity.DataAgendamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAgendamento = null;
					else entity.DataAgendamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.Byte? data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToByte(value);
				}
			}
				
			public System.String TipoResgate
			{
				get
				{
					System.Byte? data = entity.TipoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoResgate = null;
					else entity.TipoResgate = Convert.ToByte(value);
				}
			}
				
			public System.String IdPosicaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdPosicaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoResgatada = null;
					else entity.IdPosicaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdFormaLiquidacao
			{
				get
				{
					System.Byte? data = entity.IdFormaLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdFormaLiquidacao = null;
					else entity.IdFormaLiquidacao = Convert.ToByte(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaOperacao
			{
				get
				{
					System.Decimal? data = entity.CotaOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaOperacao = null;
					else entity.CotaOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCPMF
			{
				get
				{
					System.Decimal? data = entity.ValorCPMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMF = null;
					else entity.ValorCPMF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrejuizoUsado
			{
				get
				{
					System.Decimal? data = entity.PrejuizoUsado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoUsado = null;
					else entity.PrejuizoUsado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoResgate
			{
				get
				{
					System.Decimal? data = entity.RendimentoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoResgate = null;
					else entity.RendimentoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String VariacaoResgate
			{
				get
				{
					System.Decimal? data = entity.VariacaoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VariacaoResgate = null;
					else entity.VariacaoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdConta
			{
				get
				{
					System.Int32? data = entity.IdConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConta = null;
					else entity.IdConta = Convert.ToInt32(value);
				}
			}
				
			public System.String Observacao
			{
				get
				{
					System.String data = entity.Observacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Observacao = null;
					else entity.Observacao = Convert.ToString(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String IdAgenda
			{
				get
				{
					System.Int32? data = entity.IdAgenda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenda = null;
					else entity.IdAgenda = Convert.ToInt32(value);
				}
			}
				
			public System.String CotaInformada
			{
				get
				{
					System.Decimal? data = entity.CotaInformada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaInformada = null;
					else entity.CotaInformada = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdOperacaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdOperacaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoResgatada = null;
					else entity.IdOperacaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String RegistroEditado
			{
				get
				{
					System.String data = entity.RegistroEditado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RegistroEditado = null;
					else entity.RegistroEditado = Convert.ToString(value);
				}
			}
				
			public System.String ValoresColados
			{
				get
				{
					System.String data = entity.ValoresColados;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValoresColados = null;
					else entity.ValoresColados = Convert.ToString(value);
				}
			}
				
			public System.String IdLocalNegociacao
			{
				get
				{
					System.Int32? data = entity.IdLocalNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalNegociacao = null;
					else entity.IdLocalNegociacao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataRegistro
			{
				get
				{
					System.DateTime? data = entity.DataRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRegistro = null;
					else entity.DataRegistro = Convert.ToDateTime(value);
				}
			}
				
			public System.String DepositoRetirada
			{
				get
				{
					System.Boolean? data = entity.DepositoRetirada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DepositoRetirada = null;
					else entity.DepositoRetirada = Convert.ToBoolean(value);
				}
			}
				
			public System.String IdBoletaExterna
			{
				get
				{
					System.Int32? data = entity.IdBoletaExterna;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdBoletaExterna = null;
					else entity.IdBoletaExterna = Convert.ToInt32(value);
				}
			}
				
			public System.String FieModalidade
			{
				get
				{
					System.Int32? data = entity.FieModalidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FieModalidade = null;
					else entity.FieModalidade = Convert.ToInt32(value);
				}
			}
				
			public System.String FieTabelaIr
			{
				get
				{
					System.Int32? data = entity.FieTabelaIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FieTabelaIr = null;
					else entity.FieTabelaIr = Convert.ToInt32(value);
				}
			}
				
			public System.String DataAplicCautelaResgatada
			{
				get
				{
					System.DateTime? data = entity.DataAplicCautelaResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAplicCautelaResgatada = null;
					else entity.DataAplicCautelaResgatada = Convert.ToDateTime(value);
				}
			}
				
			public System.String UserTimeStamp
			{
				get
				{
					System.DateTime? data = entity.UserTimeStamp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UserTimeStamp = null;
					else entity.UserTimeStamp = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCategoriaMovimentacao
			{
				get
				{
					System.Int32? data = entity.IdCategoriaMovimentacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCategoriaMovimentacao = null;
					else entity.IdCategoriaMovimentacao = Convert.ToInt32(value);
				}
			}
				
			public System.String ExclusaoLogica
			{
				get
				{
					System.String data = entity.ExclusaoLogica;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExclusaoLogica = null;
					else entity.ExclusaoLogica = Convert.ToString(value);
				}
			}
			

			private esOperacaoFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOperacaoFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOperacaoFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OperacaoFundo : esOperacaoFundo
	{

				
		#region PosicaoFundoCollectionByIdOperacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Operacao_PosicaoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoFundoCollection PosicaoFundoCollectionByIdOperacao
		{
			get
			{
				if(this._PosicaoFundoCollectionByIdOperacao == null)
				{
					this._PosicaoFundoCollectionByIdOperacao = new PosicaoFundoCollection();
					this._PosicaoFundoCollectionByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoFundoCollectionByIdOperacao", this._PosicaoFundoCollectionByIdOperacao);
				
					if(this.IdOperacao != null)
					{
						this._PosicaoFundoCollectionByIdOperacao.Query.Where(this._PosicaoFundoCollectionByIdOperacao.Query.IdOperacao == this.IdOperacao);
						this._PosicaoFundoCollectionByIdOperacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoFundoCollectionByIdOperacao.fks.Add(PosicaoFundoMetadata.ColumnNames.IdOperacao, this.IdOperacao);
					}
				}

				return this._PosicaoFundoCollectionByIdOperacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoFundoCollectionByIdOperacao != null) 
				{ 
					this.RemovePostSave("PosicaoFundoCollectionByIdOperacao"); 
					this._PosicaoFundoCollectionByIdOperacao = null;
					
				} 
			} 			
		}

		private PosicaoFundoCollection _PosicaoFundoCollectionByIdOperacao;
		#endregion

				
		#region TributacaoFundoFieCollectionByIdOperacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TributacaoFundoFie_OperacaoFundo_FK
		/// </summary>

		[XmlIgnore]
		public TributacaoFundoFieCollection TributacaoFundoFieCollectionByIdOperacao
		{
			get
			{
				if(this._TributacaoFundoFieCollectionByIdOperacao == null)
				{
					this._TributacaoFundoFieCollectionByIdOperacao = new TributacaoFundoFieCollection();
					this._TributacaoFundoFieCollectionByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TributacaoFundoFieCollectionByIdOperacao", this._TributacaoFundoFieCollectionByIdOperacao);
				
					if(this.IdOperacao != null)
					{
						this._TributacaoFundoFieCollectionByIdOperacao.Query.Where(this._TributacaoFundoFieCollectionByIdOperacao.Query.IdOperacao == this.IdOperacao);
						this._TributacaoFundoFieCollectionByIdOperacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._TributacaoFundoFieCollectionByIdOperacao.fks.Add(TributacaoFundoFieMetadata.ColumnNames.IdOperacao, this.IdOperacao);
					}
				}

				return this._TributacaoFundoFieCollectionByIdOperacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TributacaoFundoFieCollectionByIdOperacao != null) 
				{ 
					this.RemovePostSave("TributacaoFundoFieCollectionByIdOperacao"); 
					this._TributacaoFundoFieCollectionByIdOperacao = null;
					
				} 
			} 			
		}

		private TributacaoFundoFieCollection _TributacaoFundoFieCollectionByIdOperacao;
		#endregion

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_OperacaoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCategoriaMovimentacaoByIdCategoriaMovimentacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OperacaoFundo_CatMovimentacao_FK1
		/// </summary>

		[XmlIgnore]
		public CategoriaMovimentacao UpToCategoriaMovimentacaoByIdCategoriaMovimentacao
		{
			get
			{
				if(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao == null
					&& IdCategoriaMovimentacao != null					)
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = new CategoriaMovimentacao();
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Where(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.IdCategoriaMovimentacao == this.IdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Load();
				}

				return this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
			}
			
			set
			{
				this.RemovePreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao");
				

				if(value == null)
				{
					this.IdCategoriaMovimentacao = null;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
				else
				{
					this.IdCategoriaMovimentacao = value.IdCategoriaMovimentacao;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = value;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_OperacaoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToFormaLiquidacaoByIdFormaLiquidacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FormaLiquidacao_OperacaoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public FormaLiquidacao UpToFormaLiquidacaoByIdFormaLiquidacao
		{
			get
			{
				if(this._UpToFormaLiquidacaoByIdFormaLiquidacao == null
					&& IdFormaLiquidacao != null					)
				{
					this._UpToFormaLiquidacaoByIdFormaLiquidacao = new FormaLiquidacao();
					this._UpToFormaLiquidacaoByIdFormaLiquidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToFormaLiquidacaoByIdFormaLiquidacao", this._UpToFormaLiquidacaoByIdFormaLiquidacao);
					this._UpToFormaLiquidacaoByIdFormaLiquidacao.Query.Where(this._UpToFormaLiquidacaoByIdFormaLiquidacao.Query.IdFormaLiquidacao == this.IdFormaLiquidacao);
					this._UpToFormaLiquidacaoByIdFormaLiquidacao.Query.Load();
				}

				return this._UpToFormaLiquidacaoByIdFormaLiquidacao;
			}
			
			set
			{
				this.RemovePreSave("UpToFormaLiquidacaoByIdFormaLiquidacao");
				

				if(value == null)
				{
					this.IdFormaLiquidacao = null;
					this._UpToFormaLiquidacaoByIdFormaLiquidacao = null;
				}
				else
				{
					this.IdFormaLiquidacao = value.IdFormaLiquidacao;
					this._UpToFormaLiquidacaoByIdFormaLiquidacao = value;
					this.SetPreSave("UpToFormaLiquidacaoByIdFormaLiquidacao", this._UpToFormaLiquidacaoByIdFormaLiquidacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OpFundo_Trader_FK
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "PosicaoFundoCollectionByIdOperacao", typeof(PosicaoFundoCollection), new PosicaoFundo()));
			props.Add(new esPropertyDescriptor(this, "TributacaoFundoFieCollectionByIdOperacao", typeof(TributacaoFundoFieCollection), new TributacaoFundoFie()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao != null)
			{
				this.IdCategoriaMovimentacao = this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.IdCategoriaMovimentacao;
			}
			if(!this.es.IsDeleted && this._UpToFormaLiquidacaoByIdFormaLiquidacao != null)
			{
				this.IdFormaLiquidacao = this._UpToFormaLiquidacaoByIdFormaLiquidacao.IdFormaLiquidacao;
			}
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._PosicaoFundoCollectionByIdOperacao != null)
			{
				foreach(PosicaoFundo obj in this._PosicaoFundoCollectionByIdOperacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOperacao = this.IdOperacao;
					}
				}
			}
			if(this._TributacaoFundoFieCollectionByIdOperacao != null)
			{
				foreach(TributacaoFundoFie obj in this._TributacaoFundoFieCollectionByIdOperacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOperacao = this.IdOperacao;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOperacaoFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataLiquidacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.DataLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataAgendamento
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.DataAgendamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.TipoOperacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoResgate
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.TipoResgate, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdPosicaoResgatada
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.IdPosicaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdFormaLiquidacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.IdFormaLiquidacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.CotaOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCPMF
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.ValorCPMF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrejuizoUsado
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.PrejuizoUsado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoResgate
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.RendimentoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VariacaoResgate
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.VariacaoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdConta
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.IdConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Observacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.Observacao, esSystemType.String);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdAgenda
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.IdAgenda, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CotaInformada
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.CotaInformada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdOperacaoResgatada
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.IdOperacaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem RegistroEditado
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.RegistroEditado, esSystemType.String);
			}
		} 
		
		public esQueryItem ValoresColados
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.ValoresColados, esSystemType.String);
			}
		} 
		
		public esQueryItem IdLocalNegociacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.IdLocalNegociacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataRegistro
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.DataRegistro, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DepositoRetirada
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.DepositoRetirada, esSystemType.Boolean);
			}
		} 
		
		public esQueryItem IdBoletaExterna
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.IdBoletaExterna, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FieModalidade
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.FieModalidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FieTabelaIr
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.FieTabelaIr, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataAplicCautelaResgatada
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.DataAplicCautelaResgatada, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem UserTimeStamp
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.UserTimeStamp, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCategoriaMovimentacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.IdCategoriaMovimentacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ExclusaoLogica
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoMetadata.ColumnNames.ExclusaoLogica, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OperacaoFundoCollection")]
	public partial class OperacaoFundoCollection : esOperacaoFundoCollection, IEnumerable<OperacaoFundo>
	{
		public OperacaoFundoCollection()
		{

		}
		
		public static implicit operator List<OperacaoFundo>(OperacaoFundoCollection coll)
		{
			List<OperacaoFundo> list = new List<OperacaoFundo>();
			
			foreach (OperacaoFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OperacaoFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OperacaoFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OperacaoFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OperacaoFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OperacaoFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OperacaoFundo AddNew()
		{
			OperacaoFundo entity = base.AddNewEntity() as OperacaoFundo;
			
			return entity;
		}

		public OperacaoFundo FindByPrimaryKey(System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(idOperacao) as OperacaoFundo;
		}


		#region IEnumerable<OperacaoFundo> Members

		IEnumerator<OperacaoFundo> IEnumerable<OperacaoFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OperacaoFundo;
			}
		}

		#endregion
		
		private OperacaoFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OperacaoFundo' table
	/// </summary>

	[Serializable]
	public partial class OperacaoFundo : esOperacaoFundo
	{
		public OperacaoFundo()
		{

		}
	
		public OperacaoFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esOperacaoFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OperacaoFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OperacaoFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OperacaoFundoQuery query;
	}



	[Serializable]
	public partial class OperacaoFundoQuery : esOperacaoFundoQuery
	{
		public OperacaoFundoQuery()
		{

		}		
		
		public OperacaoFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OperacaoFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OperacaoFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.IdCarteira, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.DataOperacao, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.DataConversao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.DataLiquidacao, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.DataLiquidacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.DataAgendamento, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.DataAgendamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.TipoOperacao, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.TipoOperacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.TipoResgate, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.TipoResgate;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.IdPosicaoResgatada, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.IdPosicaoResgatada;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.IdFormaLiquidacao, 10, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.IdFormaLiquidacao;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.Quantidade, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.CotaOperacao, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.CotaOperacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.ValorBruto, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.ValorLiquido, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.ValorIR, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.ValorIOF, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.ValorCPMF, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.ValorCPMF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.ValorPerformance, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.PrejuizoUsado, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.PrejuizoUsado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.RendimentoResgate, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.RendimentoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.VariacaoResgate, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.VariacaoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.IdConta, 22, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.IdConta;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.Observacao, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.Observacao;
			c.CharacterMaxLength = 400;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.Fonte, 24, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.IdAgenda, 25, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.IdAgenda;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.CotaInformada, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.CotaInformada;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.IdOperacaoResgatada, 27, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.IdOperacaoResgatada;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.RegistroEditado, 28, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.RegistroEditado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.ValoresColados, 29, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.ValoresColados;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.IdLocalNegociacao, 30, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.IdLocalNegociacao;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('1')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.DataRegistro, 31, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.DataRegistro;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.DepositoRetirada, 32, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.DepositoRetirada;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.IdBoletaExterna, 33, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.IdBoletaExterna;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.FieModalidade, 34, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.FieModalidade;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.FieTabelaIr, 35, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.FieTabelaIr;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.DataAplicCautelaResgatada, 36, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.DataAplicCautelaResgatada;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.UserTimeStamp, 37, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.UserTimeStamp;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"(getdate())";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.IdTrader, 38, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.IdCategoriaMovimentacao, 39, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.IdCategoriaMovimentacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoMetadata.ColumnNames.ExclusaoLogica, 40, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoFundoMetadata.PropertyNames.ExclusaoLogica;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OperacaoFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataConversao = "DataConversao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string DataAgendamento = "DataAgendamento";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string TipoResgate = "TipoResgate";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string IdFormaLiquidacao = "IdFormaLiquidacao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaOperacao = "CotaOperacao";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string IdConta = "IdConta";
			 public const string Observacao = "Observacao";
			 public const string Fonte = "Fonte";
			 public const string IdAgenda = "IdAgenda";
			 public const string CotaInformada = "CotaInformada";
			 public const string IdOperacaoResgatada = "IdOperacaoResgatada";
			 public const string RegistroEditado = "RegistroEditado";
			 public const string ValoresColados = "ValoresColados";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string DataRegistro = "DataRegistro";
			 public const string DepositoRetirada = "DepositoRetirada";
			 public const string IdBoletaExterna = "IdBoletaExterna";
			 public const string FieModalidade = "FieModalidade";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string DataAplicCautelaResgatada = "DataAplicCautelaResgatada";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string IdTrader = "IdTrader";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
			 public const string ExclusaoLogica = "ExclusaoLogica";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataConversao = "DataConversao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string DataAgendamento = "DataAgendamento";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string TipoResgate = "TipoResgate";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string IdFormaLiquidacao = "IdFormaLiquidacao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaOperacao = "CotaOperacao";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string IdConta = "IdConta";
			 public const string Observacao = "Observacao";
			 public const string Fonte = "Fonte";
			 public const string IdAgenda = "IdAgenda";
			 public const string CotaInformada = "CotaInformada";
			 public const string IdOperacaoResgatada = "IdOperacaoResgatada";
			 public const string RegistroEditado = "RegistroEditado";
			 public const string ValoresColados = "ValoresColados";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string DataRegistro = "DataRegistro";
			 public const string DepositoRetirada = "DepositoRetirada";
			 public const string IdBoletaExterna = "IdBoletaExterna";
			 public const string FieModalidade = "FieModalidade";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string DataAplicCautelaResgatada = "DataAplicCautelaResgatada";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string IdTrader = "IdTrader";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
			 public const string ExclusaoLogica = "ExclusaoLogica";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OperacaoFundoMetadata))
			{
				if(OperacaoFundoMetadata.mapDelegates == null)
				{
					OperacaoFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OperacaoFundoMetadata.meta == null)
				{
					OperacaoFundoMetadata.meta = new OperacaoFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataLiquidacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataAgendamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoResgate", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdPosicaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdFormaLiquidacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCPMF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrejuizoUsado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VariacaoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Observacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdAgenda", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CotaInformada", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdOperacaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RegistroEditado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ValoresColados", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdLocalNegociacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataRegistro", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DepositoRetirada", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("IdBoletaExterna", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FieModalidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FieTabelaIr", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataAplicCautelaResgatada", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("UserTimeStamp", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCategoriaMovimentacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ExclusaoLogica", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "OperacaoFundo";
				meta.Destination = "OperacaoFundo";
				
				meta.spInsert = "proc_OperacaoFundoInsert";				
				meta.spUpdate = "proc_OperacaoFundoUpdate";		
				meta.spDelete = "proc_OperacaoFundoDelete";
				meta.spLoadAll = "proc_OperacaoFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_OperacaoFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OperacaoFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
