/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/05/2015 12:56:46
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esDetalheResgateFundoCollection : esEntityCollection
	{
		public esDetalheResgateFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "DetalheResgateFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esDetalheResgateFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esDetalheResgateFundoQuery);
		}
		#endregion
		
		virtual public DetalheResgateFundo DetachEntity(DetalheResgateFundo entity)
		{
			return base.DetachEntity(entity) as DetalheResgateFundo;
		}
		
		virtual public DetalheResgateFundo AttachEntity(DetalheResgateFundo entity)
		{
			return base.AttachEntity(entity) as DetalheResgateFundo;
		}
		
		virtual public void Combine(DetalheResgateFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public DetalheResgateFundo this[int index]
		{
			get
			{
				return base[index] as DetalheResgateFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(DetalheResgateFundo);
		}
	}



	[Serializable]
	abstract public class esDetalheResgateFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esDetalheResgateFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esDetalheResgateFundo()
		{

		}

		public esDetalheResgateFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao, System.Int32 idPosicaoResgatada)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao, idPosicaoResgatada);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao, idPosicaoResgatada);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao, System.Int32 idPosicaoResgatada)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao, idPosicaoResgatada);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao, idPosicaoResgatada);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao, System.Int32 idPosicaoResgatada)
		{
			esDetalheResgateFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao, query.IdPosicaoResgatada == idPosicaoResgatada);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao, System.Int32 idPosicaoResgatada)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);			parms.Add("IdPosicaoResgatada",idPosicaoResgatada);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdPosicaoResgatada": this.str.IdPosicaoResgatada = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "ValorCPMF": this.str.ValorCPMF = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "PrejuizoUsado": this.str.PrejuizoUsado = (string)value; break;							
						case "RendimentoResgate": this.str.RendimentoResgate = (string)value; break;							
						case "VariacaoResgate": this.str.VariacaoResgate = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdPosicaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoResgatada = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "ValorCPMF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMF = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "PrejuizoUsado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoUsado = (System.Decimal?)value;
							break;
						
						case "RendimentoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoResgate = (System.Decimal?)value;
							break;
						
						case "VariacaoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VariacaoResgate = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to DetalheResgateFundo.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(DetalheResgateFundoMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(DetalheResgateFundoMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundo.IdPosicaoResgatada
		/// </summary>
		virtual public System.Int32? IdPosicaoResgatada
		{
			get
			{
				return base.GetSystemInt32(DetalheResgateFundoMetadata.ColumnNames.IdPosicaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(DetalheResgateFundoMetadata.ColumnNames.IdPosicaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundo.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(DetalheResgateFundoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(DetalheResgateFundoMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundo.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(DetalheResgateFundoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(DetalheResgateFundoMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundo.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundo.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundo.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundo.ValorCPMF
		/// </summary>
		virtual public System.Decimal? ValorCPMF
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.ValorCPMF);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.ValorCPMF, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundo.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundo.PrejuizoUsado
		/// </summary>
		virtual public System.Decimal? PrejuizoUsado
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.PrejuizoUsado);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.PrejuizoUsado, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundo.RendimentoResgate
		/// </summary>
		virtual public System.Decimal? RendimentoResgate
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.RendimentoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.RendimentoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundo.VariacaoResgate
		/// </summary>
		virtual public System.Decimal? VariacaoResgate
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.VariacaoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.VariacaoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundo.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalheResgateFundo.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(DetalheResgateFundoMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esDetalheResgateFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdPosicaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoResgatada = null;
					else entity.IdPosicaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCPMF
			{
				get
				{
					System.Decimal? data = entity.ValorCPMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMF = null;
					else entity.ValorCPMF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrejuizoUsado
			{
				get
				{
					System.Decimal? data = entity.PrejuizoUsado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoUsado = null;
					else entity.PrejuizoUsado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoResgate
			{
				get
				{
					System.Decimal? data = entity.RendimentoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoResgate = null;
					else entity.RendimentoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String VariacaoResgate
			{
				get
				{
					System.Decimal? data = entity.VariacaoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VariacaoResgate = null;
					else entity.VariacaoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
			

			private esDetalheResgateFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esDetalheResgateFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esDetalheResgateFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class DetalheResgateFundo : esDetalheResgateFundo
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_DetalheResgateFundo_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_DetalheResgateFundo_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esDetalheResgateFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return DetalheResgateFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicaoResgatada
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoMetadata.ColumnNames.IdPosicaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCPMF
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoMetadata.ColumnNames.ValorCPMF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrejuizoUsado
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoMetadata.ColumnNames.PrejuizoUsado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoResgate
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoMetadata.ColumnNames.RendimentoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VariacaoResgate
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoMetadata.ColumnNames.VariacaoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, DetalheResgateFundoMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("DetalheResgateFundoCollection")]
	public partial class DetalheResgateFundoCollection : esDetalheResgateFundoCollection, IEnumerable<DetalheResgateFundo>
	{
		public DetalheResgateFundoCollection()
		{

		}
		
		public static implicit operator List<DetalheResgateFundo>(DetalheResgateFundoCollection coll)
		{
			List<DetalheResgateFundo> list = new List<DetalheResgateFundo>();
			
			foreach (DetalheResgateFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  DetalheResgateFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DetalheResgateFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new DetalheResgateFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new DetalheResgateFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public DetalheResgateFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DetalheResgateFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(DetalheResgateFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public DetalheResgateFundo AddNew()
		{
			DetalheResgateFundo entity = base.AddNewEntity() as DetalheResgateFundo;
			
			return entity;
		}

		public DetalheResgateFundo FindByPrimaryKey(System.Int32 idOperacao, System.Int32 idPosicaoResgatada)
		{
			return base.FindByPrimaryKey(idOperacao, idPosicaoResgatada) as DetalheResgateFundo;
		}


		#region IEnumerable<DetalheResgateFundo> Members

		IEnumerator<DetalheResgateFundo> IEnumerable<DetalheResgateFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as DetalheResgateFundo;
			}
		}

		#endregion
		
		private DetalheResgateFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'DetalheResgateFundo' table
	/// </summary>

	[Serializable]
	public partial class DetalheResgateFundo : esDetalheResgateFundo
	{
		public DetalheResgateFundo()
		{

		}
	
		public DetalheResgateFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return DetalheResgateFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDetalheResgateFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DetalheResgateFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public DetalheResgateFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DetalheResgateFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(DetalheResgateFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private DetalheResgateFundoQuery query;
	}



	[Serializable]
	public partial class DetalheResgateFundoQuery : esDetalheResgateFundoQuery
	{
		public DetalheResgateFundoQuery()
		{

		}		
		
		public DetalheResgateFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class DetalheResgateFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected DetalheResgateFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(DetalheResgateFundoMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DetalheResgateFundoMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoMetadata.ColumnNames.IdPosicaoResgatada, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DetalheResgateFundoMetadata.PropertyNames.IdPosicaoResgatada;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DetalheResgateFundoMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoMetadata.ColumnNames.IdCarteira, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DetalheResgateFundoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoMetadata.ColumnNames.Quantidade, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoMetadata.ColumnNames.ValorBruto, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoMetadata.ColumnNames.ValorLiquido, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoMetadata.ColumnNames.ValorCPMF, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoMetadata.PropertyNames.ValorCPMF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoMetadata.ColumnNames.ValorPerformance, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoMetadata.ColumnNames.PrejuizoUsado, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoMetadata.PropertyNames.PrejuizoUsado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoMetadata.ColumnNames.RendimentoResgate, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoMetadata.PropertyNames.RendimentoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoMetadata.ColumnNames.VariacaoResgate, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoMetadata.PropertyNames.VariacaoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoMetadata.ColumnNames.ValorIR, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalheResgateFundoMetadata.ColumnNames.ValorIOF, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalheResgateFundoMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public DetalheResgateFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string Quantidade = "Quantidade";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string Quantidade = "Quantidade";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(DetalheResgateFundoMetadata))
			{
				if(DetalheResgateFundoMetadata.mapDelegates == null)
				{
					DetalheResgateFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (DetalheResgateFundoMetadata.meta == null)
				{
					DetalheResgateFundoMetadata.meta = new DetalheResgateFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCPMF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrejuizoUsado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VariacaoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "DetalheResgateFundo";
				meta.Destination = "DetalheResgateFundo";
				
				meta.spInsert = "proc_DetalheResgateFundoInsert";				
				meta.spUpdate = "proc_DetalheResgateFundoUpdate";		
				meta.spDelete = "proc_DetalheResgateFundoDelete";
				meta.spLoadAll = "proc_DetalheResgateFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_DetalheResgateFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private DetalheResgateFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
