/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 31/07/2015 16:30:51
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esCarteiraComplementoCollection : esEntityCollection
	{
		public esCarteiraComplementoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CarteiraComplementoCollection";
		}

		#region Query Logic
		protected void InitQuery(esCarteiraComplementoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCarteiraComplementoQuery);
		}
		#endregion
		
		virtual public CarteiraComplemento DetachEntity(CarteiraComplemento entity)
		{
			return base.DetachEntity(entity) as CarteiraComplemento;
		}
		
		virtual public CarteiraComplemento AttachEntity(CarteiraComplemento entity)
		{
			return base.AttachEntity(entity) as CarteiraComplemento;
		}
		
		virtual public void Combine(CarteiraComplementoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CarteiraComplemento this[int index]
		{
			get
			{
				return base[index] as CarteiraComplemento;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CarteiraComplemento);
		}
	}



	[Serializable]
	abstract public class esCarteiraComplemento : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCarteiraComplementoQuery GetDynamicQuery()
		{
			return null;
		}

		public esCarteiraComplemento()
		{

		}

		public esCarteiraComplemento(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCarteiraComplemento)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteiraComplemento);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteiraComplemento);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCarteiraComplemento)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteiraComplemento);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteiraComplemento);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCarteiraComplemento)
		{
			esCarteiraComplementoQuery query = this.GetDynamicQuery();
			query.Where(query.IdCarteiraComplemento == idCarteiraComplemento);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCarteiraComplemento)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCarteiraComplemento",idCarteiraComplemento);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteiraComplemento": this.str.IdCarteiraComplemento = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "NomeEmissaoSerie": this.str.NomeEmissaoSerie = (string)value; break;							
						case "IdCarteiraFundoPai": this.str.IdCarteiraFundoPai = (string)value; break;							
						case "CodigoTituloBovespa": this.str.CodigoTituloBovespa = (string)value; break;							
						case "MultiplasEmissoes": this.str.MultiplasEmissoes = (string)value; break;							
						case "MultiplasSeries": this.str.MultiplasSeries = (string)value; break;							
						case "NumeroEmissao": this.str.NumeroEmissao = (string)value; break;							
						case "NumeroSerie": this.str.NumeroSerie = (string)value; break;							
						case "DataBaseInicioEmissaoSerie": this.str.DataBaseInicioEmissaoSerie = (string)value; break;							
						case "EmissaoEncerravel": this.str.EmissaoEncerravel = (string)value; break;							
						case "DataFimEmissao": this.str.DataFimEmissao = (string)value; break;							
						case "IdClasseCota": this.str.IdClasseCota = (string)value; break;							
						case "QuantidadeCotasAportadas": this.str.QuantidadeCotasAportadas = (string)value; break;							
						case "PermiteRendimento": this.str.PermiteRendimento = (string)value; break;							
						case "PermiteAmortizacao": this.str.PermiteAmortizacao = (string)value; break;							
						case "PeriodicidadeCota": this.str.PeriodicidadeCota = (string)value; break;							
						case "PeriodicidadeRendimento": this.str.PeriodicidadeRendimento = (string)value; break;							
						case "InicioPeriodicidadeRendimento": this.str.InicioPeriodicidadeRendimento = (string)value; break;							
						case "PeriodicidadeAmortizacao": this.str.PeriodicidadeAmortizacao = (string)value; break;							
						case "InicioPeriodicidadeAmortizacao": this.str.InicioPeriodicidadeAmortizacao = (string)value; break;							
						case "RetemIRFonteRendimento": this.str.RetemIRFonteRendimento = (string)value; break;							
						case "ExecutaResgateFimEmissao": this.str.ExecutaResgateFimEmissao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteiraComplemento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraComplemento = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdCarteiraFundoPai":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraFundoPai = (System.Int32?)value;
							break;
						
						case "NumeroEmissao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroEmissao = (System.Int32?)value;
							break;
						
						case "NumeroSerie":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroSerie = (System.Int32?)value;
							break;
						
						case "DataBaseInicioEmissaoSerie":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataBaseInicioEmissaoSerie = (System.DateTime?)value;
							break;
						
						case "DataFimEmissao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFimEmissao = (System.DateTime?)value;
							break;
						
						case "IdClasseCota":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClasseCota = (System.Int32?)value;
							break;
						
						case "QuantidadeCotasAportadas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeCotasAportadas = (System.Decimal?)value;
							break;
						
						case "PeriodicidadeCota":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PeriodicidadeCota = (System.Int32?)value;
							break;
						
						case "PeriodicidadeRendimento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PeriodicidadeRendimento = (System.Int32?)value;
							break;
						
						case "InicioPeriodicidadeRendimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.InicioPeriodicidadeRendimento = (System.DateTime?)value;
							break;
						
						case "PeriodicidadeAmortizacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PeriodicidadeAmortizacao = (System.Int32?)value;
							break;
						
						case "InicioPeriodicidadeAmortizacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.InicioPeriodicidadeAmortizacao = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CarteiraComplemento.IdCarteiraComplemento
		/// </summary>
		virtual public System.Int32? IdCarteiraComplemento
		{
			get
			{
				return base.GetSystemInt32(CarteiraComplementoMetadata.ColumnNames.IdCarteiraComplemento);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraComplementoMetadata.ColumnNames.IdCarteiraComplemento, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(CarteiraComplementoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(CarteiraComplementoMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.NomeEmissaoSerie
		/// </summary>
		virtual public System.String NomeEmissaoSerie
		{
			get
			{
				return base.GetSystemString(CarteiraComplementoMetadata.ColumnNames.NomeEmissaoSerie);
			}
			
			set
			{
				base.SetSystemString(CarteiraComplementoMetadata.ColumnNames.NomeEmissaoSerie, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.IdCarteiraFundoPai
		/// </summary>
		virtual public System.Int32? IdCarteiraFundoPai
		{
			get
			{
				return base.GetSystemInt32(CarteiraComplementoMetadata.ColumnNames.IdCarteiraFundoPai);
			}
			
			set
			{
				if(base.SetSystemInt32(CarteiraComplementoMetadata.ColumnNames.IdCarteiraFundoPai, value))
				{
					this._UpToCarteiraByIdCarteiraFundoPai = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.CodigoTituloBovespa
		/// </summary>
		virtual public System.String CodigoTituloBovespa
		{
			get
			{
				return base.GetSystemString(CarteiraComplementoMetadata.ColumnNames.CodigoTituloBovespa);
			}
			
			set
			{
				base.SetSystemString(CarteiraComplementoMetadata.ColumnNames.CodigoTituloBovespa, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.MultiplasEmissoes
		/// </summary>
		virtual public System.String MultiplasEmissoes
		{
			get
			{
				return base.GetSystemString(CarteiraComplementoMetadata.ColumnNames.MultiplasEmissoes);
			}
			
			set
			{
				base.SetSystemString(CarteiraComplementoMetadata.ColumnNames.MultiplasEmissoes, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.MultiplasSeries
		/// </summary>
		virtual public System.String MultiplasSeries
		{
			get
			{
				return base.GetSystemString(CarteiraComplementoMetadata.ColumnNames.MultiplasSeries);
			}
			
			set
			{
				base.SetSystemString(CarteiraComplementoMetadata.ColumnNames.MultiplasSeries, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.NumeroEmissao
		/// </summary>
		virtual public System.Int32? NumeroEmissao
		{
			get
			{
				return base.GetSystemInt32(CarteiraComplementoMetadata.ColumnNames.NumeroEmissao);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraComplementoMetadata.ColumnNames.NumeroEmissao, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.NumeroSerie
		/// </summary>
		virtual public System.Int32? NumeroSerie
		{
			get
			{
				return base.GetSystemInt32(CarteiraComplementoMetadata.ColumnNames.NumeroSerie);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraComplementoMetadata.ColumnNames.NumeroSerie, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.DataBaseInicioEmissaoSerie
		/// </summary>
		virtual public System.DateTime? DataBaseInicioEmissaoSerie
		{
			get
			{
				return base.GetSystemDateTime(CarteiraComplementoMetadata.ColumnNames.DataBaseInicioEmissaoSerie);
			}
			
			set
			{
				base.SetSystemDateTime(CarteiraComplementoMetadata.ColumnNames.DataBaseInicioEmissaoSerie, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.EmissaoEncerravel
		/// </summary>
		virtual public System.String EmissaoEncerravel
		{
			get
			{
				return base.GetSystemString(CarteiraComplementoMetadata.ColumnNames.EmissaoEncerravel);
			}
			
			set
			{
				base.SetSystemString(CarteiraComplementoMetadata.ColumnNames.EmissaoEncerravel, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.DataFimEmissao
		/// </summary>
		virtual public System.DateTime? DataFimEmissao
		{
			get
			{
				return base.GetSystemDateTime(CarteiraComplementoMetadata.ColumnNames.DataFimEmissao);
			}
			
			set
			{
				base.SetSystemDateTime(CarteiraComplementoMetadata.ColumnNames.DataFimEmissao, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.IdClasseCota
		/// </summary>
		virtual public System.Int32? IdClasseCota
		{
			get
			{
				return base.GetSystemInt32(CarteiraComplementoMetadata.ColumnNames.IdClasseCota);
			}
			
			set
			{
				if(base.SetSystemInt32(CarteiraComplementoMetadata.ColumnNames.IdClasseCota, value))
				{
					this._UpToClasseCotaByIdClasseCota = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.QuantidadeCotasAportadas
		/// </summary>
		virtual public System.Decimal? QuantidadeCotasAportadas
		{
			get
			{
				return base.GetSystemDecimal(CarteiraComplementoMetadata.ColumnNames.QuantidadeCotasAportadas);
			}
			
			set
			{
				base.SetSystemDecimal(CarteiraComplementoMetadata.ColumnNames.QuantidadeCotasAportadas, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.PermiteRendimento
		/// </summary>
		virtual public System.String PermiteRendimento
		{
			get
			{
				return base.GetSystemString(CarteiraComplementoMetadata.ColumnNames.PermiteRendimento);
			}
			
			set
			{
				base.SetSystemString(CarteiraComplementoMetadata.ColumnNames.PermiteRendimento, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.PermiteAmortizacao
		/// </summary>
		virtual public System.String PermiteAmortizacao
		{
			get
			{
				return base.GetSystemString(CarteiraComplementoMetadata.ColumnNames.PermiteAmortizacao);
			}
			
			set
			{
				base.SetSystemString(CarteiraComplementoMetadata.ColumnNames.PermiteAmortizacao, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.PeriodicidadeCota
		/// </summary>
		virtual public System.Int32? PeriodicidadeCota
		{
			get
			{
				return base.GetSystemInt32(CarteiraComplementoMetadata.ColumnNames.PeriodicidadeCota);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraComplementoMetadata.ColumnNames.PeriodicidadeCota, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.PeriodicidadeRendimento
		/// </summary>
		virtual public System.Int32? PeriodicidadeRendimento
		{
			get
			{
				return base.GetSystemInt32(CarteiraComplementoMetadata.ColumnNames.PeriodicidadeRendimento);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraComplementoMetadata.ColumnNames.PeriodicidadeRendimento, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.InicioPeriodicidadeRendimento
		/// </summary>
		virtual public System.DateTime? InicioPeriodicidadeRendimento
		{
			get
			{
				return base.GetSystemDateTime(CarteiraComplementoMetadata.ColumnNames.InicioPeriodicidadeRendimento);
			}
			
			set
			{
				base.SetSystemDateTime(CarteiraComplementoMetadata.ColumnNames.InicioPeriodicidadeRendimento, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.PeriodicidadeAmortizacao
		/// </summary>
		virtual public System.Int32? PeriodicidadeAmortizacao
		{
			get
			{
				return base.GetSystemInt32(CarteiraComplementoMetadata.ColumnNames.PeriodicidadeAmortizacao);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraComplementoMetadata.ColumnNames.PeriodicidadeAmortizacao, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.InicioPeriodicidadeAmortizacao
		/// </summary>
		virtual public System.DateTime? InicioPeriodicidadeAmortizacao
		{
			get
			{
				return base.GetSystemDateTime(CarteiraComplementoMetadata.ColumnNames.InicioPeriodicidadeAmortizacao);
			}
			
			set
			{
				base.SetSystemDateTime(CarteiraComplementoMetadata.ColumnNames.InicioPeriodicidadeAmortizacao, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.RetemIRFonteRendimento
		/// </summary>
		virtual public System.String RetemIRFonteRendimento
		{
			get
			{
				return base.GetSystemString(CarteiraComplementoMetadata.ColumnNames.RetemIRFonteRendimento);
			}
			
			set
			{
				base.SetSystemString(CarteiraComplementoMetadata.ColumnNames.RetemIRFonteRendimento, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraComplemento.ExecutaResgateFimEmissao
		/// </summary>
		virtual public System.String ExecutaResgateFimEmissao
		{
			get
			{
				return base.GetSystemString(CarteiraComplementoMetadata.ColumnNames.ExecutaResgateFimEmissao);
			}
			
			set
			{
				base.SetSystemString(CarteiraComplementoMetadata.ColumnNames.ExecutaResgateFimEmissao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteiraFundoPai;
		[CLSCompliant(false)]
		internal protected ClasseCota _UpToClasseCotaByIdClasseCota;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCarteiraComplemento entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteiraComplemento
			{
				get
				{
					System.Int32? data = entity.IdCarteiraComplemento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraComplemento = null;
					else entity.IdCarteiraComplemento = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String NomeEmissaoSerie
			{
				get
				{
					System.String data = entity.NomeEmissaoSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NomeEmissaoSerie = null;
					else entity.NomeEmissaoSerie = Convert.ToString(value);
				}
			}
				
			public System.String IdCarteiraFundoPai
			{
				get
				{
					System.Int32? data = entity.IdCarteiraFundoPai;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraFundoPai = null;
					else entity.IdCarteiraFundoPai = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoTituloBovespa
			{
				get
				{
					System.String data = entity.CodigoTituloBovespa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoTituloBovespa = null;
					else entity.CodigoTituloBovespa = Convert.ToString(value);
				}
			}
				
			public System.String MultiplasEmissoes
			{
				get
				{
					System.String data = entity.MultiplasEmissoes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MultiplasEmissoes = null;
					else entity.MultiplasEmissoes = Convert.ToString(value);
				}
			}
				
			public System.String MultiplasSeries
			{
				get
				{
					System.String data = entity.MultiplasSeries;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MultiplasSeries = null;
					else entity.MultiplasSeries = Convert.ToString(value);
				}
			}
				
			public System.String NumeroEmissao
			{
				get
				{
					System.Int32? data = entity.NumeroEmissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroEmissao = null;
					else entity.NumeroEmissao = Convert.ToInt32(value);
				}
			}
				
			public System.String NumeroSerie
			{
				get
				{
					System.Int32? data = entity.NumeroSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroSerie = null;
					else entity.NumeroSerie = Convert.ToInt32(value);
				}
			}
				
			public System.String DataBaseInicioEmissaoSerie
			{
				get
				{
					System.DateTime? data = entity.DataBaseInicioEmissaoSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataBaseInicioEmissaoSerie = null;
					else entity.DataBaseInicioEmissaoSerie = Convert.ToDateTime(value);
				}
			}
				
			public System.String EmissaoEncerravel
			{
				get
				{
					System.String data = entity.EmissaoEncerravel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EmissaoEncerravel = null;
					else entity.EmissaoEncerravel = Convert.ToString(value);
				}
			}
				
			public System.String DataFimEmissao
			{
				get
				{
					System.DateTime? data = entity.DataFimEmissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFimEmissao = null;
					else entity.DataFimEmissao = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdClasseCota
			{
				get
				{
					System.Int32? data = entity.IdClasseCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClasseCota = null;
					else entity.IdClasseCota = Convert.ToInt32(value);
				}
			}
				
			public System.String QuantidadeCotasAportadas
			{
				get
				{
					System.Decimal? data = entity.QuantidadeCotasAportadas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeCotasAportadas = null;
					else entity.QuantidadeCotasAportadas = Convert.ToDecimal(value);
				}
			}
				
			public System.String PermiteRendimento
			{
				get
				{
					System.String data = entity.PermiteRendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PermiteRendimento = null;
					else entity.PermiteRendimento = Convert.ToString(value);
				}
			}
				
			public System.String PermiteAmortizacao
			{
				get
				{
					System.String data = entity.PermiteAmortizacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PermiteAmortizacao = null;
					else entity.PermiteAmortizacao = Convert.ToString(value);
				}
			}
				
			public System.String PeriodicidadeCota
			{
				get
				{
					System.Int32? data = entity.PeriodicidadeCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PeriodicidadeCota = null;
					else entity.PeriodicidadeCota = Convert.ToInt32(value);
				}
			}
				
			public System.String PeriodicidadeRendimento
			{
				get
				{
					System.Int32? data = entity.PeriodicidadeRendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PeriodicidadeRendimento = null;
					else entity.PeriodicidadeRendimento = Convert.ToInt32(value);
				}
			}
				
			public System.String InicioPeriodicidadeRendimento
			{
				get
				{
					System.DateTime? data = entity.InicioPeriodicidadeRendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InicioPeriodicidadeRendimento = null;
					else entity.InicioPeriodicidadeRendimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String PeriodicidadeAmortizacao
			{
				get
				{
					System.Int32? data = entity.PeriodicidadeAmortizacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PeriodicidadeAmortizacao = null;
					else entity.PeriodicidadeAmortizacao = Convert.ToInt32(value);
				}
			}
				
			public System.String InicioPeriodicidadeAmortizacao
			{
				get
				{
					System.DateTime? data = entity.InicioPeriodicidadeAmortizacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InicioPeriodicidadeAmortizacao = null;
					else entity.InicioPeriodicidadeAmortizacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String RetemIRFonteRendimento
			{
				get
				{
					System.String data = entity.RetemIRFonteRendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RetemIRFonteRendimento = null;
					else entity.RetemIRFonteRendimento = Convert.ToString(value);
				}
			}
				
			public System.String ExecutaResgateFimEmissao
			{
				get
				{
					System.String data = entity.ExecutaResgateFimEmissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExecutaResgateFimEmissao = null;
					else entity.ExecutaResgateFimEmissao = Convert.ToString(value);
				}
			}
			

			private esCarteiraComplemento entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCarteiraComplementoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCarteiraComplemento can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CarteiraComplemento : esCarteiraComplemento
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - CarteiraComplemento_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCarteiraByIdCarteiraFundoPai - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - CarteiraComplemento_Carteira_FK2
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteiraFundoPai
		{
			get
			{
				if(this._UpToCarteiraByIdCarteiraFundoPai == null
					&& IdCarteiraFundoPai != null					)
				{
					this._UpToCarteiraByIdCarteiraFundoPai = new Carteira();
					this._UpToCarteiraByIdCarteiraFundoPai.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteiraFundoPai", this._UpToCarteiraByIdCarteiraFundoPai);
					this._UpToCarteiraByIdCarteiraFundoPai.Query.Where(this._UpToCarteiraByIdCarteiraFundoPai.Query.IdCarteira == this.IdCarteiraFundoPai);
					this._UpToCarteiraByIdCarteiraFundoPai.Query.Load();
				}

				return this._UpToCarteiraByIdCarteiraFundoPai;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteiraFundoPai");
				

				if(value == null)
				{
					this.IdCarteiraFundoPai = null;
					this._UpToCarteiraByIdCarteiraFundoPai = null;
				}
				else
				{
					this.IdCarteiraFundoPai = value.IdCarteira;
					this._UpToCarteiraByIdCarteiraFundoPai = value;
					this.SetPreSave("UpToCarteiraByIdCarteiraFundoPai", this._UpToCarteiraByIdCarteiraFundoPai);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClasseCotaByIdClasseCota - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - CarteiraComplemento_ClasseCota_FK1
		/// </summary>

		[XmlIgnore]
		public ClasseCota UpToClasseCotaByIdClasseCota
		{
			get
			{
				if(this._UpToClasseCotaByIdClasseCota == null
					&& IdClasseCota != null					)
				{
					this._UpToClasseCotaByIdClasseCota = new ClasseCota();
					this._UpToClasseCotaByIdClasseCota.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClasseCotaByIdClasseCota", this._UpToClasseCotaByIdClasseCota);
					this._UpToClasseCotaByIdClasseCota.Query.Where(this._UpToClasseCotaByIdClasseCota.Query.IdClasseCota == this.IdClasseCota);
					this._UpToClasseCotaByIdClasseCota.Query.Load();
				}

				return this._UpToClasseCotaByIdClasseCota;
			}
			
			set
			{
				this.RemovePreSave("UpToClasseCotaByIdClasseCota");
				

				if(value == null)
				{
					this.IdClasseCota = null;
					this._UpToClasseCotaByIdClasseCota = null;
				}
				else
				{
					this.IdClasseCota = value.IdClasseCota;
					this._UpToClasseCotaByIdClasseCota = value;
					this.SetPreSave("UpToClasseCotaByIdClasseCota", this._UpToClasseCotaByIdClasseCota);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCarteiraComplementoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CarteiraComplementoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteiraComplemento
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.IdCarteiraComplemento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem NomeEmissaoSerie
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.NomeEmissaoSerie, esSystemType.String);
			}
		} 
		
		public esQueryItem IdCarteiraFundoPai
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.IdCarteiraFundoPai, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoTituloBovespa
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.CodigoTituloBovespa, esSystemType.String);
			}
		} 
		
		public esQueryItem MultiplasEmissoes
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.MultiplasEmissoes, esSystemType.String);
			}
		} 
		
		public esQueryItem MultiplasSeries
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.MultiplasSeries, esSystemType.String);
			}
		} 
		
		public esQueryItem NumeroEmissao
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.NumeroEmissao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem NumeroSerie
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.NumeroSerie, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataBaseInicioEmissaoSerie
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.DataBaseInicioEmissaoSerie, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem EmissaoEncerravel
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.EmissaoEncerravel, esSystemType.String);
			}
		} 
		
		public esQueryItem DataFimEmissao
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.DataFimEmissao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdClasseCota
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.IdClasseCota, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QuantidadeCotasAportadas
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.QuantidadeCotasAportadas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PermiteRendimento
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.PermiteRendimento, esSystemType.String);
			}
		} 
		
		public esQueryItem PermiteAmortizacao
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.PermiteAmortizacao, esSystemType.String);
			}
		} 
		
		public esQueryItem PeriodicidadeCota
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.PeriodicidadeCota, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PeriodicidadeRendimento
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.PeriodicidadeRendimento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem InicioPeriodicidadeRendimento
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.InicioPeriodicidadeRendimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PeriodicidadeAmortizacao
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.PeriodicidadeAmortizacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem InicioPeriodicidadeAmortizacao
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.InicioPeriodicidadeAmortizacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem RetemIRFonteRendimento
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.RetemIRFonteRendimento, esSystemType.String);
			}
		} 
		
		public esQueryItem ExecutaResgateFimEmissao
		{
			get
			{
				return new esQueryItem(this, CarteiraComplementoMetadata.ColumnNames.ExecutaResgateFimEmissao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CarteiraComplementoCollection")]
	public partial class CarteiraComplementoCollection : esCarteiraComplementoCollection, IEnumerable<CarteiraComplemento>
	{
		public CarteiraComplementoCollection()
		{

		}
		
		public static implicit operator List<CarteiraComplemento>(CarteiraComplementoCollection coll)
		{
			List<CarteiraComplemento> list = new List<CarteiraComplemento>();
			
			foreach (CarteiraComplemento emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CarteiraComplementoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CarteiraComplementoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CarteiraComplemento(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CarteiraComplemento();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CarteiraComplementoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CarteiraComplementoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CarteiraComplementoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CarteiraComplemento AddNew()
		{
			CarteiraComplemento entity = base.AddNewEntity() as CarteiraComplemento;
			
			return entity;
		}

		public CarteiraComplemento FindByPrimaryKey(System.Int32 idCarteiraComplemento)
		{
			return base.FindByPrimaryKey(idCarteiraComplemento) as CarteiraComplemento;
		}


		#region IEnumerable<CarteiraComplemento> Members

		IEnumerator<CarteiraComplemento> IEnumerable<CarteiraComplemento>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CarteiraComplemento;
			}
		}

		#endregion
		
		private CarteiraComplementoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CarteiraComplemento' table
	/// </summary>

	[Serializable]
	public partial class CarteiraComplemento : esCarteiraComplemento
	{
		public CarteiraComplemento()
		{

		}
	
		public CarteiraComplemento(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CarteiraComplementoMetadata.Meta();
			}
		}
		
		
		
		override protected esCarteiraComplementoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CarteiraComplementoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CarteiraComplementoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CarteiraComplementoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CarteiraComplementoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CarteiraComplementoQuery query;
	}



	[Serializable]
	public partial class CarteiraComplementoQuery : esCarteiraComplementoQuery
	{
		public CarteiraComplementoQuery()
		{

		}		
		
		public CarteiraComplementoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CarteiraComplementoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CarteiraComplementoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.IdCarteiraComplemento, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.IdCarteiraComplemento;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.NomeEmissaoSerie, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.NomeEmissaoSerie;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.IdCarteiraFundoPai, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.IdCarteiraFundoPai;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.CodigoTituloBovespa, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.CodigoTituloBovespa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.MultiplasEmissoes, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.MultiplasEmissoes;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.MultiplasSeries, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.MultiplasSeries;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.NumeroEmissao, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.NumeroEmissao;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.NumeroSerie, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.NumeroSerie;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.DataBaseInicioEmissaoSerie, 9, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.DataBaseInicioEmissaoSerie;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.EmissaoEncerravel, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.EmissaoEncerravel;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('S')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.DataFimEmissao, 11, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.DataFimEmissao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.IdClasseCota, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.IdClasseCota;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.QuantidadeCotasAportadas, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.QuantidadeCotasAportadas;	
			c.NumericPrecision = 18;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.PermiteRendimento, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.PermiteRendimento;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('S')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.PermiteAmortizacao, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.PermiteAmortizacao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('S')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.PeriodicidadeCota, 16, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.PeriodicidadeCota;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.PeriodicidadeRendimento, 17, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.PeriodicidadeRendimento;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.InicioPeriodicidadeRendimento, 18, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.InicioPeriodicidadeRendimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.PeriodicidadeAmortizacao, 19, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.PeriodicidadeAmortizacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.InicioPeriodicidadeAmortizacao, 20, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.InicioPeriodicidadeAmortizacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.RetemIRFonteRendimento, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.RetemIRFonteRendimento;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraComplementoMetadata.ColumnNames.ExecutaResgateFimEmissao, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraComplementoMetadata.PropertyNames.ExecutaResgateFimEmissao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CarteiraComplementoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteiraComplemento = "IdCarteiraComplemento";
			 public const string IdCarteira = "IdCarteira";
			 public const string NomeEmissaoSerie = "NomeEmissaoSerie";
			 public const string IdCarteiraFundoPai = "IdCarteiraFundoPai";
			 public const string CodigoTituloBovespa = "CodigoTituloBovespa";
			 public const string MultiplasEmissoes = "MultiplasEmissoes";
			 public const string MultiplasSeries = "MultiplasSeries";
			 public const string NumeroEmissao = "NumeroEmissao";
			 public const string NumeroSerie = "NumeroSerie";
			 public const string DataBaseInicioEmissaoSerie = "DataBaseInicioEmissaoSerie";
			 public const string EmissaoEncerravel = "EmissaoEncerravel";
			 public const string DataFimEmissao = "DataFimEmissao";
			 public const string IdClasseCota = "IdClasseCota";
			 public const string QuantidadeCotasAportadas = "QuantidadeCotasAportadas";
			 public const string PermiteRendimento = "PermiteRendimento";
			 public const string PermiteAmortizacao = "PermiteAmortizacao";
			 public const string PeriodicidadeCota = "PeriodicidadeCota";
			 public const string PeriodicidadeRendimento = "PeriodicidadeRendimento";
			 public const string InicioPeriodicidadeRendimento = "InicioPeriodicidadeRendimento";
			 public const string PeriodicidadeAmortizacao = "PeriodicidadeAmortizacao";
			 public const string InicioPeriodicidadeAmortizacao = "InicioPeriodicidadeAmortizacao";
			 public const string RetemIRFonteRendimento = "RetemIRFonteRendimento";
			 public const string ExecutaResgateFimEmissao = "ExecutaResgateFimEmissao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteiraComplemento = "IdCarteiraComplemento";
			 public const string IdCarteira = "IdCarteira";
			 public const string NomeEmissaoSerie = "NomeEmissaoSerie";
			 public const string IdCarteiraFundoPai = "IdCarteiraFundoPai";
			 public const string CodigoTituloBovespa = "CodigoTituloBovespa";
			 public const string MultiplasEmissoes = "MultiplasEmissoes";
			 public const string MultiplasSeries = "MultiplasSeries";
			 public const string NumeroEmissao = "NumeroEmissao";
			 public const string NumeroSerie = "NumeroSerie";
			 public const string DataBaseInicioEmissaoSerie = "DataBaseInicioEmissaoSerie";
			 public const string EmissaoEncerravel = "EmissaoEncerravel";
			 public const string DataFimEmissao = "DataFimEmissao";
			 public const string IdClasseCota = "IdClasseCota";
			 public const string QuantidadeCotasAportadas = "QuantidadeCotasAportadas";
			 public const string PermiteRendimento = "PermiteRendimento";
			 public const string PermiteAmortizacao = "PermiteAmortizacao";
			 public const string PeriodicidadeCota = "PeriodicidadeCota";
			 public const string PeriodicidadeRendimento = "PeriodicidadeRendimento";
			 public const string InicioPeriodicidadeRendimento = "InicioPeriodicidadeRendimento";
			 public const string PeriodicidadeAmortizacao = "PeriodicidadeAmortizacao";
			 public const string InicioPeriodicidadeAmortizacao = "InicioPeriodicidadeAmortizacao";
			 public const string RetemIRFonteRendimento = "RetemIRFonteRendimento";
			 public const string ExecutaResgateFimEmissao = "ExecutaResgateFimEmissao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CarteiraComplementoMetadata))
			{
				if(CarteiraComplementoMetadata.mapDelegates == null)
				{
					CarteiraComplementoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CarteiraComplementoMetadata.meta == null)
				{
					CarteiraComplementoMetadata.meta = new CarteiraComplementoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteiraComplemento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("NomeEmissaoSerie", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdCarteiraFundoPai", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoTituloBovespa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("MultiplasEmissoes", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("MultiplasSeries", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("NumeroEmissao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("NumeroSerie", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataBaseInicioEmissaoSerie", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("EmissaoEncerravel", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DataFimEmissao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdClasseCota", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QuantidadeCotasAportadas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PermiteRendimento", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("PermiteAmortizacao", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("PeriodicidadeCota", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PeriodicidadeRendimento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("InicioPeriodicidadeRendimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PeriodicidadeAmortizacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("InicioPeriodicidadeAmortizacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("RetemIRFonteRendimento", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ExecutaResgateFimEmissao", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "CarteiraComplemento";
				meta.Destination = "CarteiraComplemento";
				
				meta.spInsert = "proc_CarteiraComplementoInsert";				
				meta.spUpdate = "proc_CarteiraComplementoUpdate";		
				meta.spDelete = "proc_CarteiraComplementoDelete";
				meta.spLoadAll = "proc_CarteiraComplementoLoadAll";
				meta.spLoadByPrimaryKey = "proc_CarteiraComplementoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CarteiraComplementoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
