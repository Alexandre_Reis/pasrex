/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/03/2015 11:22:08
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Common;


namespace Financial.Fundo
{

	[Serializable]
	abstract public class esClassesOffShoreCollection : esEntityCollection
	{
		public esClassesOffShoreCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ClassesOffShoreCollection";
		}

		#region Query Logic
		protected void InitQuery(esClassesOffShoreQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esClassesOffShoreQuery);
		}
		#endregion
		
		virtual public ClassesOffShore DetachEntity(ClassesOffShore entity)
		{
			return base.DetachEntity(entity) as ClassesOffShore;
		}
		
		virtual public ClassesOffShore AttachEntity(ClassesOffShore entity)
		{
			return base.AttachEntity(entity) as ClassesOffShore;
		}
		
		virtual public void Combine(ClassesOffShoreCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ClassesOffShore this[int index]
		{
			get
			{
				return base[index] as ClassesOffShore;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ClassesOffShore);
		}
	}



	[Serializable]
	abstract public class esClassesOffShore : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esClassesOffShoreQuery GetDynamicQuery()
		{
			return null;
		}

		public esClassesOffShore()
		{

		}

		public esClassesOffShore(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idClassesOffShore)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idClassesOffShore);
			else
				return LoadByPrimaryKeyStoredProcedure(idClassesOffShore);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idClassesOffShore)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idClassesOffShore);
			else
				return LoadByPrimaryKeyStoredProcedure(idClassesOffShore);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idClassesOffShore)
		{
			esClassesOffShoreQuery query = this.GetDynamicQuery();
			query.Where(query.IdClassesOffShore == idClassesOffShore);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idClassesOffShore)
		{
			esParameters parms = new esParameters();
			parms.Add("IdClassesOffShore",idClassesOffShore);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdClassesOffShore": this.str.IdClassesOffShore = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "DataInicio": this.str.DataInicio = (string)value; break;							
						case "SerieUnica": this.str.SerieUnica = (string)value; break;							
						case "FrequenciaSerie": this.str.FrequenciaSerie = (string)value; break;							
						case "PoliticaInvestimentos": this.str.PoliticaInvestimentos = (string)value; break;							
						case "Moeda": this.str.Moeda = (string)value; break;							
						case "Domicilio": this.str.Domicilio = (string)value; break;							
						case "TaxaAdm": this.str.TaxaAdm = (string)value; break;							
						case "TaxaCustodia": this.str.TaxaCustodia = (string)value; break;							
						case "TaxaPerformance": this.str.TaxaPerformance = (string)value; break;							
						case "IndexadorPerf": this.str.IndexadorPerf = (string)value; break;							
						case "FreqCalcPerformance": this.str.FreqCalcPerformance = (string)value; break;							
						case "LockUp": this.str.LockUp = (string)value; break;							
						case "HardSoft": this.str.HardSoft = (string)value; break;							
						case "PenalidadeResgate": this.str.PenalidadeResgate = (string)value; break;							
						case "VlMinimoAplicacao": this.str.VlMinimoAplicacao = (string)value; break;							
						case "VlMinimoResgate": this.str.VlMinimoResgate = (string)value; break;							
						case "VlMinimoPermanencia": this.str.VlMinimoPermanencia = (string)value; break;							
						case "QtdDiasConvResgate": this.str.QtdDiasConvResgate = (string)value; break;							
						case "QtdDiasLiqFin": this.str.QtdDiasLiqFin = (string)value; break;							
						case "HoldBack": this.str.HoldBack = (string)value; break;							
						case "AnivHoldBack": this.str.AnivHoldBack = (string)value; break;							
						case "SidePocket": this.str.SidePocket = (string)value; break;							
						case "PrazoValidadePrevia": this.str.PrazoValidadePrevia = (string)value; break;							
						case "IdIndicePrevia": this.str.IdIndicePrevia = (string)value; break;							
						case "PrazoRepeticaoCotas": this.str.PrazoRepeticaoCotas = (string)value; break;
						case "QtdDiasConvAplicacao": this.str.QtdDiasConvAplicacao = (string)value; break;							
						case "QtdDiasLiqFinAplic": this.str.QtdDiasLiqFinAplic = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdClassesOffShore":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClassesOffShore = (System.Int32?)value;
							break;
						
						case "DataInicio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicio = (System.DateTime?)value;
							break;
						
						case "FrequenciaSerie":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FrequenciaSerie = (System.Int32?)value;
							break;
						
						case "Moeda":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.Moeda = (System.Int16?)value;
							break;
						
						case "Domicilio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Domicilio = (System.Int32?)value;
							break;
						
						case "TaxaAdm":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaAdm = (System.Decimal?)value;
							break;
						
						case "TaxaCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaCustodia = (System.Decimal?)value;
							break;
						
						case "TaxaPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaPerformance = (System.Decimal?)value;
							break;
						
						case "IndexadorPerf":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IndexadorPerf = (System.Int16?)value;
							break;
						
						case "FreqCalcPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FreqCalcPerformance = (System.Int32?)value;
							break;
						
						case "LockUp":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.LockUp = (System.Int32?)value;
							break;
						
						case "HardSoft":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.HardSoft = (System.Int32?)value;
							break;
						
						case "PenalidadeResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PenalidadeResgate = (System.Decimal?)value;
							break;
						
						case "VlMinimoAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlMinimoAplicacao = (System.Decimal?)value;
							break;
						
						case "VlMinimoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlMinimoResgate = (System.Decimal?)value;
							break;
						
						case "VlMinimoPermanencia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlMinimoPermanencia = (System.Decimal?)value;
							break;
						
						case "QtdDiasConvResgate":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.QtdDiasConvResgate = (System.Int32?)value;
							break;
						
						case "QtdDiasLiqFin":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.QtdDiasLiqFin = (System.Int32?)value;
							break;
						
						case "HoldBack":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.HoldBack = (System.Decimal?)value;
							break;
						
						case "AnivHoldBack":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.AnivHoldBack = (System.DateTime?)value;
							break;
						
						case "PrazoValidadePrevia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PrazoValidadePrevia = (System.Int32?)value;
							break;
						
						case "IdIndicePrevia":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndicePrevia = (System.Int16?)value;
							break;
						
						case "PrazoRepeticaoCotas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PrazoRepeticaoCotas = (System.Int32?)value;
							break;
					
						case "QtdDiasConvAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.QtdDiasConvAplicacao = (System.Int32?)value;
							break;
						
						case "QtdDiasLiqFinAplic":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.QtdDiasLiqFinAplic = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ClassesOffShore.IdClassesOffShore
		/// </summary>
		virtual public System.Int32? IdClassesOffShore
		{
			get
			{
				return base.GetSystemInt32(ClassesOffShoreMetadata.ColumnNames.IdClassesOffShore);
			}
			
			set
			{
				base.SetSystemInt32(ClassesOffShoreMetadata.ColumnNames.IdClassesOffShore, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(ClassesOffShoreMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(ClassesOffShoreMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.DataInicio
		/// </summary>
		virtual public System.DateTime? DataInicio
		{
			get
			{
				return base.GetSystemDateTime(ClassesOffShoreMetadata.ColumnNames.DataInicio);
			}
			
			set
			{
				base.SetSystemDateTime(ClassesOffShoreMetadata.ColumnNames.DataInicio, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.SerieUnica
		/// </summary>
		virtual public System.String SerieUnica
		{
			get
			{
				return base.GetSystemString(ClassesOffShoreMetadata.ColumnNames.SerieUnica);
			}
			
			set
			{
				base.SetSystemString(ClassesOffShoreMetadata.ColumnNames.SerieUnica, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.FrequenciaSerie
		/// </summary>
		virtual public System.Int32? FrequenciaSerie
		{
			get
			{
				return base.GetSystemInt32(ClassesOffShoreMetadata.ColumnNames.FrequenciaSerie);
			}
			
			set
			{
				base.SetSystemInt32(ClassesOffShoreMetadata.ColumnNames.FrequenciaSerie, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.PoliticaInvestimentos
		/// </summary>
		virtual public System.String PoliticaInvestimentos
		{
			get
			{
				return base.GetSystemString(ClassesOffShoreMetadata.ColumnNames.PoliticaInvestimentos);
			}
			
			set
			{
				base.SetSystemString(ClassesOffShoreMetadata.ColumnNames.PoliticaInvestimentos, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.Moeda
		/// </summary>
		virtual public System.Int16? Moeda
		{
			get
			{
				return base.GetSystemInt16(ClassesOffShoreMetadata.ColumnNames.Moeda);
			}
			
			set
			{
				if(base.SetSystemInt16(ClassesOffShoreMetadata.ColumnNames.Moeda, value))
				{
					this._UpToMoedaByMoeda = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.Domicilio
		/// </summary>
		virtual public System.Int32? Domicilio
		{
			get
			{
				return base.GetSystemInt32(ClassesOffShoreMetadata.ColumnNames.Domicilio);
			}
			
			set
			{
				if(base.SetSystemInt32(ClassesOffShoreMetadata.ColumnNames.Domicilio, value))
				{
					this._UpToLocalNegociacaoByDomicilio = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.TaxaAdm
		/// </summary>
		virtual public System.Decimal? TaxaAdm
		{
			get
			{
				return base.GetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.TaxaAdm);
			}
			
			set
			{
				base.SetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.TaxaAdm, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.TaxaCustodia
		/// </summary>
		virtual public System.Decimal? TaxaCustodia
		{
			get
			{
				return base.GetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.TaxaCustodia);
			}
			
			set
			{
				base.SetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.TaxaCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.TaxaPerformance
		/// </summary>
		virtual public System.Decimal? TaxaPerformance
		{
			get
			{
				return base.GetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.TaxaPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.TaxaPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.IndexadorPerf
		/// </summary>
		virtual public System.Int16? IndexadorPerf
		{
			get
			{
				return base.GetSystemInt16(ClassesOffShoreMetadata.ColumnNames.IndexadorPerf);
			}
			
			set
			{
				if(base.SetSystemInt16(ClassesOffShoreMetadata.ColumnNames.IndexadorPerf, value))
				{
					this._UpToIndiceByIndexadorPerf = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.FreqCalcPerformance
		/// </summary>
		virtual public System.Int32? FreqCalcPerformance
		{
			get
			{
				return base.GetSystemInt32(ClassesOffShoreMetadata.ColumnNames.FreqCalcPerformance);
			}
			
			set
			{
				base.SetSystemInt32(ClassesOffShoreMetadata.ColumnNames.FreqCalcPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.LockUp
		/// </summary>
		virtual public System.Int32? LockUp
		{
			get
			{
				return base.GetSystemInt32(ClassesOffShoreMetadata.ColumnNames.LockUp);
			}
			
			set
			{
				base.SetSystemInt32(ClassesOffShoreMetadata.ColumnNames.LockUp, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.HardSoft
		/// </summary>
		virtual public System.Int32? HardSoft
		{
			get
			{
				return base.GetSystemInt32(ClassesOffShoreMetadata.ColumnNames.HardSoft);
			}
			
			set
			{
				base.SetSystemInt32(ClassesOffShoreMetadata.ColumnNames.HardSoft, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.PenalidadeResgate
		/// </summary>
		virtual public System.Decimal? PenalidadeResgate
		{
			get
			{
				return base.GetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.PenalidadeResgate);
			}
			
			set
			{
				base.SetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.PenalidadeResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.VlMinimoAplicacao
		/// </summary>
		virtual public System.Decimal? VlMinimoAplicacao
		{
			get
			{
				return base.GetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.VlMinimoAplicacao);
			}
			
			set
			{
				base.SetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.VlMinimoAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.VlMinimoResgate
		/// </summary>
		virtual public System.Decimal? VlMinimoResgate
		{
			get
			{
				return base.GetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.VlMinimoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.VlMinimoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.VlMinimoPermanencia
		/// </summary>
		virtual public System.Decimal? VlMinimoPermanencia
		{
			get
			{
				return base.GetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.VlMinimoPermanencia);
			}
			
			set
			{
				base.SetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.VlMinimoPermanencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.QtdDiasConvResgate
		/// </summary>
		virtual public System.Int32? QtdDiasConvResgate
		{
			get
			{
				return base.GetSystemInt32(ClassesOffShoreMetadata.ColumnNames.QtdDiasConvResgate);
			}
			
			set
			{
				base.SetSystemInt32(ClassesOffShoreMetadata.ColumnNames.QtdDiasConvResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.QtdDiasLiqFin
		/// </summary>
		virtual public System.Int32? QtdDiasLiqFin
		{
			get
			{
				return base.GetSystemInt32(ClassesOffShoreMetadata.ColumnNames.QtdDiasLiqFin);
			}
			
			set
			{
				base.SetSystemInt32(ClassesOffShoreMetadata.ColumnNames.QtdDiasLiqFin, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.HoldBack
		/// </summary>
		virtual public System.Decimal? HoldBack
		{
			get
			{
				return base.GetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.HoldBack);
			}
			
			set
			{
				base.SetSystemDecimal(ClassesOffShoreMetadata.ColumnNames.HoldBack, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.AnivHoldBack
		/// </summary>
		virtual public System.DateTime? AnivHoldBack
		{
			get
			{
				return base.GetSystemDateTime(ClassesOffShoreMetadata.ColumnNames.AnivHoldBack);
			}
			
			set
			{
				base.SetSystemDateTime(ClassesOffShoreMetadata.ColumnNames.AnivHoldBack, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.SidePocket
		/// </summary>
		virtual public System.String SidePocket
		{
			get
			{
				return base.GetSystemString(ClassesOffShoreMetadata.ColumnNames.SidePocket);
			}
			
			set
			{
				base.SetSystemString(ClassesOffShoreMetadata.ColumnNames.SidePocket, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.PrazoValidadePrevia
		/// </summary>
		virtual public System.Int32? PrazoValidadePrevia
		{
			get
			{
				return base.GetSystemInt32(ClassesOffShoreMetadata.ColumnNames.PrazoValidadePrevia);

			}
			
			set
			{
				base.SetSystemInt32(ClassesOffShoreMetadata.ColumnNames.PrazoValidadePrevia, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.IdIndicePrevia
		/// </summary>
		virtual public System.Int16? IdIndicePrevia
		{
			get
			{
				return base.GetSystemInt16(ClassesOffShoreMetadata.ColumnNames.IdIndicePrevia);
			}
			
			set
			{
				base.SetSystemInt16(ClassesOffShoreMetadata.ColumnNames.IdIndicePrevia, value);
			}
		}
		
		/// <summary>
		/// Maps to ClassesOffShore.PrazoRepeticaoCotas
		/// </summary>
		virtual public System.Int32? PrazoRepeticaoCotas
		{
			get
			{
				return base.GetSystemInt32(ClassesOffShoreMetadata.ColumnNames.PrazoRepeticaoCotas);
			}
			
			set
			{
				base.SetSystemInt32(ClassesOffShoreMetadata.ColumnNames.PrazoRepeticaoCotas, value);
            }
		}

		/// Maps to ClassesOffShore.QtdDiasConvAplicacao
		/// </summary>
		virtual public System.Int32? QtdDiasConvAplicacao
		{
			get
			{
				return base.GetSystemInt32(ClassesOffShoreMetadata.ColumnNames.QtdDiasConvAplicacao);
            }

            set
            {
				base.SetSystemInt32(ClassesOffShoreMetadata.ColumnNames.QtdDiasConvAplicacao, value);
            }
        }

		/// Maps to ClassesOffShore.QtdDiasLiqFinAplic
		/// </summary>
		virtual public System.Int32? QtdDiasLiqFinAplic
		{
			get
			{
				return base.GetSystemInt32(ClassesOffShoreMetadata.ColumnNames.QtdDiasLiqFinAplic);
            }
            
            set
            {
				base.SetSystemInt32(ClassesOffShoreMetadata.ColumnNames.QtdDiasLiqFinAplic, value);
			}
        }
		
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIndexadorPerf;
		[CLSCompliant(false)]
		internal protected LocalNegociacao _UpToLocalNegociacaoByDomicilio;
		[CLSCompliant(false)]
		internal protected Moeda _UpToMoedaByMoeda;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esClassesOffShore entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdClassesOffShore
			{
				get
				{
					System.Int32? data = entity.IdClassesOffShore;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClassesOffShore = null;
					else entity.IdClassesOffShore = Convert.ToInt32(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String DataInicio
			{
				get
				{
					System.DateTime? data = entity.DataInicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicio = null;
					else entity.DataInicio = Convert.ToDateTime(value);
				}
			}
				
			public System.String SerieUnica
			{
				get
				{
					System.String data = entity.SerieUnica;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SerieUnica = null;
					else entity.SerieUnica = Convert.ToString(value);
				}
			}
				
			public System.String FrequenciaSerie
			{
				get
				{
					System.Int32? data = entity.FrequenciaSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FrequenciaSerie = null;
					else entity.FrequenciaSerie = Convert.ToInt32(value);
				}
			}
				
			public System.String PoliticaInvestimentos
			{
				get
				{
					System.String data = entity.PoliticaInvestimentos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PoliticaInvestimentos = null;
					else entity.PoliticaInvestimentos = Convert.ToString(value);
				}
			}
				
			public System.String Moeda
			{
				get
				{
					System.Int16? data = entity.Moeda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Moeda = null;
					else entity.Moeda = Convert.ToInt16(value);
				}
			}
				
			public System.String Domicilio
			{
				get
				{
					System.Int32? data = entity.Domicilio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Domicilio = null;
					else entity.Domicilio = Convert.ToInt32(value);
				}
			}
				
			public System.String TaxaAdm
			{
				get
				{
					System.Decimal? data = entity.TaxaAdm;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaAdm = null;
					else entity.TaxaAdm = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaCustodia
			{
				get
				{
					System.Decimal? data = entity.TaxaCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaCustodia = null;
					else entity.TaxaCustodia = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaPerformance
			{
				get
				{
					System.Decimal? data = entity.TaxaPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaPerformance = null;
					else entity.TaxaPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String IndexadorPerf
			{
				get
				{
					System.Int16? data = entity.IndexadorPerf;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IndexadorPerf = null;
					else entity.IndexadorPerf = Convert.ToInt16(value);
				}
			}
				
			public System.String FreqCalcPerformance
			{
				get
				{
					System.Int32? data = entity.FreqCalcPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FreqCalcPerformance = null;
					else entity.FreqCalcPerformance = Convert.ToInt32(value);
				}
			}
				
			public System.String LockUp
			{
				get
				{
					System.Int32? data = entity.LockUp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LockUp = null;
					else entity.LockUp = Convert.ToInt32(value);
				}
			}
				
			public System.String HardSoft
			{
				get
				{
					System.Int32? data = entity.HardSoft;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.HardSoft = null;
					else entity.HardSoft = Convert.ToInt32(value);
				}
			}
				
			public System.String PenalidadeResgate
			{
				get
				{
					System.Decimal? data = entity.PenalidadeResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PenalidadeResgate = null;
					else entity.PenalidadeResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlMinimoAplicacao
			{
				get
				{
					System.Decimal? data = entity.VlMinimoAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlMinimoAplicacao = null;
					else entity.VlMinimoAplicacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlMinimoResgate
			{
				get
				{
					System.Decimal? data = entity.VlMinimoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlMinimoResgate = null;
					else entity.VlMinimoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlMinimoPermanencia
			{
				get
				{
					System.Decimal? data = entity.VlMinimoPermanencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlMinimoPermanencia = null;
					else entity.VlMinimoPermanencia = Convert.ToDecimal(value);
				}
			}
				
			public System.String QtdDiasConvResgate
			{
				get
				{
					System.Int32? data = entity.QtdDiasConvResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdDiasConvResgate = null;
					else entity.QtdDiasConvResgate = Convert.ToInt32(value);
				}
			}
				
			public System.String QtdDiasLiqFin
			{
				get
				{
					System.Int32? data = entity.QtdDiasLiqFin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdDiasLiqFin = null;
					else entity.QtdDiasLiqFin = Convert.ToInt32(value);
				}
			}
				
			public System.String HoldBack
			{
				get
				{
					System.Decimal? data = entity.HoldBack;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.HoldBack = null;
					else entity.HoldBack = Convert.ToDecimal(value);
				}
			}
				
			public System.String AnivHoldBack
			{
				get
				{
					System.DateTime? data = entity.AnivHoldBack;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AnivHoldBack = null;
					else entity.AnivHoldBack = Convert.ToDateTime(value);
				}
			}
				
			public System.String SidePocket
			{
				get
				{
					System.String data = entity.SidePocket;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SidePocket = null;
					else entity.SidePocket = Convert.ToString(value);
				}
			}
				
			public System.String PrazoValidadePrevia
			{
				get
				{
					System.Int32? data = entity.PrazoValidadePrevia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoValidadePrevia = null;
					else entity.PrazoValidadePrevia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdIndicePrevia
			{
				get
				{
					System.Int16? data = entity.IdIndicePrevia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndicePrevia = null;
					else entity.IdIndicePrevia = Convert.ToInt16(value);
				}
			}
				
			public System.String PrazoRepeticaoCotas
			{
				get
				{
					System.Int32? data = entity.PrazoRepeticaoCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoRepeticaoCotas = null;
					else entity.PrazoRepeticaoCotas = Convert.ToInt32(value);
				}
			}
			
			public System.String QtdDiasConvAplicacao
			{
				get
				{
					System.Int32? data = entity.QtdDiasConvAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdDiasConvAplicacao = null;
					else entity.QtdDiasConvAplicacao = Convert.ToInt32(value);
				}
			}
				
			public System.String QtdDiasLiqFinAplic
			{
				get
				{
					System.Int32? data = entity.QtdDiasLiqFinAplic;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdDiasLiqFinAplic = null;
					else entity.QtdDiasLiqFinAplic = Convert.ToInt32(value);
				}
			}
			

			private esClassesOffShore entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esClassesOffShoreQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esClassesOffShore can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ClassesOffShore : esClassesOffShore
	{

				
		#region SeriesOffShoreCollectionByIdClassesOffShore - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Serie_Classes_FK1
		/// </summary>

		[XmlIgnore]
		public SeriesOffShoreCollection SeriesOffShoreCollectionByIdClassesOffShore
		{
			get
			{
				if(this._SeriesOffShoreCollectionByIdClassesOffShore == null)
				{
					this._SeriesOffShoreCollectionByIdClassesOffShore = new SeriesOffShoreCollection();
					this._SeriesOffShoreCollectionByIdClassesOffShore.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("SeriesOffShoreCollectionByIdClassesOffShore", this._SeriesOffShoreCollectionByIdClassesOffShore);
				
					if(this.IdClassesOffShore != null)
					{
						this._SeriesOffShoreCollectionByIdClassesOffShore.Query.Where(this._SeriesOffShoreCollectionByIdClassesOffShore.Query.IdClassesOffShore == this.IdClassesOffShore);
						this._SeriesOffShoreCollectionByIdClassesOffShore.Query.Load();

						// Auto-hookup Foreign Keys
						this._SeriesOffShoreCollectionByIdClassesOffShore.fks.Add(SeriesOffShoreMetadata.ColumnNames.IdClassesOffShore, this.IdClassesOffShore);
					}
				}

				return this._SeriesOffShoreCollectionByIdClassesOffShore;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._SeriesOffShoreCollectionByIdClassesOffShore != null) 
				{ 
					this.RemovePostSave("SeriesOffShoreCollectionByIdClassesOffShore"); 
					this._SeriesOffShoreCollectionByIdClassesOffShore = null;
					
				} 
			} 			
		}

		private SeriesOffShoreCollection _SeriesOffShoreCollectionByIdClassesOffShore;
		#endregion

		#region UpToCarteira - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Classes_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteira
		{
			get
			{
				if(this._UpToCarteira == null
					&& IdClassesOffShore != null					)
				{
					this._UpToCarteira = new Carteira();
					this._UpToCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteira", this._UpToCarteira);
					this._UpToCarteira.Query.Where(this._UpToCarteira.Query.IdCarteira == this.IdClassesOffShore);
					this._UpToCarteira.Query.Load();
				}

				return this._UpToCarteira;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToCarteira");

				if(value == null)
				{
					this._UpToCarteira = null;
				}
				else
				{
					this._UpToCarteira = value;
					this.SetPreSave("UpToCarteira", this._UpToCarteira);
				}
				
				
			} 
		}

		private Carteira _UpToCarteira;
		#endregion

				
		#region UpToIndiceByIndexadorPerf - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__ClassesOf__Index__334B710A
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIndexadorPerf
		{
			get
			{
				if(this._UpToIndiceByIndexadorPerf == null
					&& IndexadorPerf != null					)
				{
					this._UpToIndiceByIndexadorPerf = new Indice();
					this._UpToIndiceByIndexadorPerf.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIndexadorPerf", this._UpToIndiceByIndexadorPerf);
					this._UpToIndiceByIndexadorPerf.Query.Where(this._UpToIndiceByIndexadorPerf.Query.IdIndice == this.IndexadorPerf);
					this._UpToIndiceByIndexadorPerf.Query.Load();
				}

				return this._UpToIndiceByIndexadorPerf;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIndexadorPerf");
				

				if(value == null)
				{
					this.IndexadorPerf = null;
					this._UpToIndiceByIndexadorPerf = null;
				}
				else
				{
					this.IndexadorPerf = value.IdIndice;
					this._UpToIndiceByIndexadorPerf = value;
					this.SetPreSave("UpToIndiceByIndexadorPerf", this._UpToIndiceByIndexadorPerf);
				}
				
			}
		}
		#endregion
		

				
		#region UpToLocalNegociacaoByDomicilio - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__ClassesOf__Domic__32574CD1
		/// </summary>

		[XmlIgnore]
		public LocalNegociacao UpToLocalNegociacaoByDomicilio
		{
			get
			{
				if(this._UpToLocalNegociacaoByDomicilio == null
					&& Domicilio != null					)
				{
					this._UpToLocalNegociacaoByDomicilio = new LocalNegociacao();
					this._UpToLocalNegociacaoByDomicilio.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToLocalNegociacaoByDomicilio", this._UpToLocalNegociacaoByDomicilio);
					this._UpToLocalNegociacaoByDomicilio.Query.Where(this._UpToLocalNegociacaoByDomicilio.Query.IdLocalNegociacao == this.Domicilio);
					this._UpToLocalNegociacaoByDomicilio.Query.Load();
				}

				return this._UpToLocalNegociacaoByDomicilio;
			}
			
			set
			{
				this.RemovePreSave("UpToLocalNegociacaoByDomicilio");
				

				if(value == null)
				{
					this.Domicilio = null;
					this._UpToLocalNegociacaoByDomicilio = null;
				}
				else
				{
					this.Domicilio = value.IdLocalNegociacao;
					this._UpToLocalNegociacaoByDomicilio = value;
					this.SetPreSave("UpToLocalNegociacaoByDomicilio", this._UpToLocalNegociacaoByDomicilio);
				}
				
			}
		}
		#endregion
		

				
		#region UpToMoedaByMoeda - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__ClassesOf__Moeda__31632898
		/// </summary>

		[XmlIgnore]
		public Moeda UpToMoedaByMoeda
		{
			get
			{
				if(this._UpToMoedaByMoeda == null
					&& Moeda != null					)
				{
					this._UpToMoedaByMoeda = new Moeda();
					this._UpToMoedaByMoeda.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToMoedaByMoeda", this._UpToMoedaByMoeda);
					this._UpToMoedaByMoeda.Query.Where(this._UpToMoedaByMoeda.Query.IdMoeda == this.Moeda);
					this._UpToMoedaByMoeda.Query.Load();
				}

				return this._UpToMoedaByMoeda;
			}
			
			set
			{
				this.RemovePreSave("UpToMoedaByMoeda");
				

				if(value == null)
				{
					this.Moeda = null;
					this._UpToMoedaByMoeda = null;
				}
				else
				{
					this.Moeda = value.IdMoeda;
					this._UpToMoedaByMoeda = value;
					this.SetPreSave("UpToMoedaByMoeda", this._UpToMoedaByMoeda);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "SeriesOffShoreCollectionByIdClassesOffShore", typeof(SeriesOffShoreCollection), new SeriesOffShore()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToLocalNegociacaoByDomicilio != null)
			{
				this.Domicilio = this._UpToLocalNegociacaoByDomicilio.IdLocalNegociacao;
			}
			if(!this.es.IsDeleted && this._UpToMoedaByMoeda != null)
			{
				this.Moeda = this._UpToMoedaByMoeda.IdMoeda;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esClassesOffShoreQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ClassesOffShoreMetadata.Meta();
			}
		}	
		

		public esQueryItem IdClassesOffShore
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.IdClassesOffShore, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem DataInicio
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.DataInicio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem SerieUnica
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.SerieUnica, esSystemType.String);
			}
		} 
		
		public esQueryItem FrequenciaSerie
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.FrequenciaSerie, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PoliticaInvestimentos
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.PoliticaInvestimentos, esSystemType.String);
			}
		} 
		
		public esQueryItem Moeda
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.Moeda, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Domicilio
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.Domicilio, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TaxaAdm
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.TaxaAdm, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaCustodia
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.TaxaCustodia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaPerformance
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.TaxaPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IndexadorPerf
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.IndexadorPerf, esSystemType.Int16);
			}
		} 
		
		public esQueryItem FreqCalcPerformance
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.FreqCalcPerformance, esSystemType.Int32);
			}
		} 
		
		public esQueryItem LockUp
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.LockUp, esSystemType.Int32);
			}
		} 
		
		public esQueryItem HardSoft
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.HardSoft, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PenalidadeResgate
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.PenalidadeResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlMinimoAplicacao
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.VlMinimoAplicacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlMinimoResgate
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.VlMinimoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlMinimoPermanencia
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.VlMinimoPermanencia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QtdDiasConvResgate
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.QtdDiasConvResgate, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QtdDiasLiqFin
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.QtdDiasLiqFin, esSystemType.Int32);
			}
		} 
		
		public esQueryItem HoldBack
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.HoldBack, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AnivHoldBack
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.AnivHoldBack, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem SidePocket
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.SidePocket, esSystemType.String);
			}
		} 
		
		public esQueryItem PrazoValidadePrevia
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.PrazoValidadePrevia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdIndicePrevia
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.IdIndicePrevia, esSystemType.Int16);
			}
		}

        public esQueryItem PrazoRepeticaoCotas
        {
            get
            {
                return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.PrazoRepeticaoCotas, esSystemType.Int32);
            }
        }

		public esQueryItem QtdDiasConvAplicacao
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.QtdDiasConvAplicacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QtdDiasLiqFinAplic
		{
			get
			{
				return new esQueryItem(this, ClassesOffShoreMetadata.ColumnNames.QtdDiasLiqFinAplic, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ClassesOffShoreCollection")]
	public partial class ClassesOffShoreCollection : esClassesOffShoreCollection, IEnumerable<ClassesOffShore>
	{
		public ClassesOffShoreCollection()
		{

		}
		
		public static implicit operator List<ClassesOffShore>(ClassesOffShoreCollection coll)
		{
			List<ClassesOffShore> list = new List<ClassesOffShore>();
			
			foreach (ClassesOffShore emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ClassesOffShoreMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClassesOffShoreQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ClassesOffShore(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ClassesOffShore();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ClassesOffShoreQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClassesOffShoreQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ClassesOffShoreQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ClassesOffShore AddNew()
		{
			ClassesOffShore entity = base.AddNewEntity() as ClassesOffShore;
			
			return entity;
		}

		public ClassesOffShore FindByPrimaryKey(System.Int32 idClassesOffShore)
		{
			return base.FindByPrimaryKey(idClassesOffShore) as ClassesOffShore;
		}


		#region IEnumerable<ClassesOffShore> Members

		IEnumerator<ClassesOffShore> IEnumerable<ClassesOffShore>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ClassesOffShore;
			}
		}

		#endregion
		
		private ClassesOffShoreQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ClassesOffShore' table
	/// </summary>

	[Serializable]
	public partial class ClassesOffShore : esClassesOffShore
	{
		public ClassesOffShore()
		{

		}
	
		public ClassesOffShore(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ClassesOffShoreMetadata.Meta();
			}
		}
		
		
		
		override protected esClassesOffShoreQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClassesOffShoreQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ClassesOffShoreQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClassesOffShoreQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ClassesOffShoreQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ClassesOffShoreQuery query;
	}



	[Serializable]
	public partial class ClassesOffShoreQuery : esClassesOffShoreQuery
	{
		public ClassesOffShoreQuery()
		{

		}		
		
		public ClassesOffShoreQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ClassesOffShoreMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ClassesOffShoreMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.IdClassesOffShore, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.IdClassesOffShore;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.DataInicio, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.DataInicio;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.SerieUnica, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.SerieUnica;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.FrequenciaSerie, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.FrequenciaSerie;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.PoliticaInvestimentos, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.PoliticaInvestimentos;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.Moeda, 6, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.Moeda;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.Domicilio, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.Domicilio;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.TaxaAdm, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.TaxaAdm;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.TaxaCustodia, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.TaxaCustodia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.TaxaPerformance, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.TaxaPerformance;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.IndexadorPerf, 11, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.IndexadorPerf;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.FreqCalcPerformance, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.FreqCalcPerformance;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.LockUp, 13, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.LockUp;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.HardSoft, 14, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.HardSoft;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.PenalidadeResgate, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.PenalidadeResgate;	
			c.NumericPrecision = 5;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.VlMinimoAplicacao, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.VlMinimoAplicacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.VlMinimoResgate, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.VlMinimoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.VlMinimoPermanencia, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.VlMinimoPermanencia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.QtdDiasConvResgate, 19, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.QtdDiasConvResgate;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.QtdDiasLiqFin, 20, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.QtdDiasLiqFin;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.HoldBack, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.HoldBack;	
			c.NumericPrecision = 5;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.AnivHoldBack, 22, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.AnivHoldBack;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.SidePocket, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.SidePocket;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
            c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.PrazoValidadePrevia, 24, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ClassesOffShoreMetadata.PropertyNames.PrazoValidadePrevia; 
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c); 
           
            
            c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.IdIndicePrevia, 25, typeof(System.Int16), esSystemType.Int16);
            c.PropertyName = ClassesOffShoreMetadata.PropertyNames.IdIndicePrevia; 
            c.NumericPrecision = 5;
            c.IsNullable = true;
            _columns.Add(c); 
           
            
            c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.PrazoRepeticaoCotas, 26, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ClassesOffShoreMetadata.PropertyNames.PrazoRepeticaoCotas; 
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c);	

			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.QtdDiasConvAplicacao, 24, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.QtdDiasConvAplicacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 

			c = new esColumnMetadata(ClassesOffShoreMetadata.ColumnNames.QtdDiasLiqFinAplic, 25, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClassesOffShoreMetadata.PropertyNames.QtdDiasLiqFinAplic;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ClassesOffShoreMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdClassesOffShore = "IdClassesOffShore";
			 public const string Nome = "Nome";
			 public const string DataInicio = "DataInicio";
			 public const string SerieUnica = "SerieUnica";
			 public const string FrequenciaSerie = "FrequenciaSerie";
			 public const string PoliticaInvestimentos = "PoliticaInvestimentos";
			 public const string Moeda = "Moeda";
			 public const string Domicilio = "Domicilio";
			 public const string TaxaAdm = "TaxaAdm";
			 public const string TaxaCustodia = "TaxaCustodia";
			 public const string TaxaPerformance = "TaxaPerformance";
			 public const string IndexadorPerf = "IndexadorPerf";
			 public const string FreqCalcPerformance = "FreqCalcPerformance";
			 public const string LockUp = "LockUp";
			 public const string HardSoft = "HardSoft";
			 public const string PenalidadeResgate = "PenalidadeResgate";
			 public const string VlMinimoAplicacao = "VlMinimoAplicacao";
			 public const string VlMinimoResgate = "VlMinimoResgate";
			 public const string VlMinimoPermanencia = "VlMinimoPermanencia";
			 public const string QtdDiasConvResgate = "QtdDiasConvResgate";
			 public const string QtdDiasLiqFin = "QtdDiasLiqFin";
			 public const string HoldBack = "HoldBack";
			 public const string AnivHoldBack = "AnivHoldBack";
			 public const string SidePocket = "SidePocket";
			 public const string PrazoValidadePrevia = "PrazoValidadePrevia";
			 public const string IdIndicePrevia = "IdIndicePrevia";
			 public const string PrazoRepeticaoCotas = "PrazoRepeticaoCotas";
			 public const string QtdDiasConvAplicacao = "QtdDiasConvAplicacao";
			 public const string QtdDiasLiqFinAplic = "QtdDiasLiqFinAplic";

		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdClassesOffShore = "IdClassesOffShore";
			 public const string Nome = "Nome";
			 public const string DataInicio = "DataInicio";
			 public const string SerieUnica = "SerieUnica";
			 public const string FrequenciaSerie = "FrequenciaSerie";
			 public const string PoliticaInvestimentos = "PoliticaInvestimentos";
			 public const string Moeda = "Moeda";
			 public const string Domicilio = "Domicilio";
			 public const string TaxaAdm = "TaxaAdm";
			 public const string TaxaCustodia = "TaxaCustodia";
			 public const string TaxaPerformance = "TaxaPerformance";
			 public const string IndexadorPerf = "IndexadorPerf";
			 public const string FreqCalcPerformance = "FreqCalcPerformance";
			 public const string LockUp = "LockUp";
			 public const string HardSoft = "HardSoft";
			 public const string PenalidadeResgate = "PenalidadeResgate";
			 public const string VlMinimoAplicacao = "VlMinimoAplicacao";
			 public const string VlMinimoResgate = "VlMinimoResgate";
			 public const string VlMinimoPermanencia = "VlMinimoPermanencia";
			 public const string QtdDiasConvResgate = "QtdDiasConvResgate";
			 public const string QtdDiasLiqFin = "QtdDiasLiqFin";
			 public const string HoldBack = "HoldBack";
			 public const string AnivHoldBack = "AnivHoldBack";
			 public const string SidePocket = "SidePocket";
			 public const string PrazoValidadePrevia = "PrazoValidadePrevia";
			 public const string IdIndicePrevia = "IdIndicePrevia";
			 public const string PrazoRepeticaoCotas = "PrazoRepeticaoCotas";
			 public const string QtdDiasConvAplicacao = "QtdDiasConvAplicacao";
			 public const string QtdDiasLiqFinAplic = "QtdDiasLiqFinAplic";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ClassesOffShoreMetadata))
			{
				if(ClassesOffShoreMetadata.mapDelegates == null)
				{
					ClassesOffShoreMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ClassesOffShoreMetadata.meta == null)
				{
					ClassesOffShoreMetadata.meta = new ClassesOffShoreMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdClassesOffShore", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataInicio", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("SerieUnica", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FrequenciaSerie", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PoliticaInvestimentos", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Moeda", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Domicilio", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TaxaAdm", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaCustodia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IndexadorPerf", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("FreqCalcPerformance", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("LockUp", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("HardSoft", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PenalidadeResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VlMinimoAplicacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VlMinimoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VlMinimoPermanencia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QtdDiasConvResgate", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QtdDiasLiqFin", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("HoldBack", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AnivHoldBack", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("SidePocket", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("PrazoValidadePrevia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdIndicePrevia", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("PrazoRepeticaoCotas", new esTypeMap("int", "System.Int32"));			
				meta.AddTypeMap("QtdDiasConvAplicacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QtdDiasLiqFinAplic", new esTypeMap("int", "System.Int32"));			
				
				meta.Source = "ClassesOffShore";
				meta.Destination = "ClassesOffShore";
				
				meta.spInsert = "proc_ClassesOffShoreInsert";				
				meta.spUpdate = "proc_ClassesOffShoreUpdate";		
				meta.spDelete = "proc_ClassesOffShoreDelete";
				meta.spLoadAll = "proc_ClassesOffShoreLoadAll";
				meta.spLoadByPrimaryKey = "proc_ClassesOffShoreLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ClassesOffShoreMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
