/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/02/2016 14:54:46
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		









		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esCalculoAdministracaoHistoricoCollection : esEntityCollection
	{
		public esCalculoAdministracaoHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CalculoAdministracaoHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esCalculoAdministracaoHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCalculoAdministracaoHistoricoQuery);
		}
		#endregion
		
		virtual public CalculoAdministracaoHistorico DetachEntity(CalculoAdministracaoHistorico entity)
		{
			return base.DetachEntity(entity) as CalculoAdministracaoHistorico;
		}
		
		virtual public CalculoAdministracaoHistorico AttachEntity(CalculoAdministracaoHistorico entity)
		{
			return base.AttachEntity(entity) as CalculoAdministracaoHistorico;
		}
		
		virtual public void Combine(CalculoAdministracaoHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CalculoAdministracaoHistorico this[int index]
		{
			get
			{
				return base[index] as CalculoAdministracaoHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CalculoAdministracaoHistorico);
		}
	}



	[Serializable]
	abstract public class esCalculoAdministracaoHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCalculoAdministracaoHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esCalculoAdministracaoHistorico()
		{

		}

		public esCalculoAdministracaoHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idTabela)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idTabela);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataHistorico, System.Int32 idTabela)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCalculoAdministracaoHistoricoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataHistorico == dataHistorico, query.IdTabela == idTabela);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idTabela)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idTabela);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idTabela)
		{
			esCalculoAdministracaoHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdTabela == idTabela);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idTabela)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdTabela",idTabela);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "ValorDia": this.str.ValorDia = (string)value; break;							
						case "ValorAcumulado": this.str.ValorAcumulado = (string)value; break;							
						case "DataFimApropriacao": this.str.DataFimApropriacao = (string)value; break;							
						case "DataPagamento": this.str.DataPagamento = (string)value; break;							
						case "ValorCPMFDia": this.str.ValorCPMFDia = (string)value; break;							
						case "ValorCPMFAcumulado": this.str.ValorCPMFAcumulado = (string)value; break;							
						case "ValorBase": this.str.ValorBase = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "ValorDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorDia = (System.Decimal?)value;
							break;
						
						case "ValorAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAcumulado = (System.Decimal?)value;
							break;
						
						case "DataFimApropriacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFimApropriacao = (System.DateTime?)value;
							break;
						
						case "DataPagamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPagamento = (System.DateTime?)value;
							break;
						
						case "ValorCPMFDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMFDia = (System.Decimal?)value;
							break;
						
						case "ValorCPMFAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMFAcumulado = (System.Decimal?)value;
							break;
						
						case "ValorBase":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBase = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CalculoAdministracaoHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(CalculoAdministracaoHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoAdministracaoHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoHistorico.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(CalculoAdministracaoHistoricoMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				if(base.SetSystemInt32(CalculoAdministracaoHistoricoMetadata.ColumnNames.IdTabela, value))
				{
					this._UpToTabelaTaxaAdministracaoByIdTabela = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoHistorico.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(CalculoAdministracaoHistoricoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(CalculoAdministracaoHistoricoMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoHistorico.ValorDia
		/// </summary>
		virtual public System.Decimal? ValorDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoHistorico.ValorAcumulado
		/// </summary>
		virtual public System.Decimal? ValorAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorAcumulado, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoHistorico.DataFimApropriacao
		/// </summary>
		virtual public System.DateTime? DataFimApropriacao
		{
			get
			{
				return base.GetSystemDateTime(CalculoAdministracaoHistoricoMetadata.ColumnNames.DataFimApropriacao);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoAdministracaoHistoricoMetadata.ColumnNames.DataFimApropriacao, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoHistorico.DataPagamento
		/// </summary>
		virtual public System.DateTime? DataPagamento
		{
			get
			{
				return base.GetSystemDateTime(CalculoAdministracaoHistoricoMetadata.ColumnNames.DataPagamento);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoAdministracaoHistoricoMetadata.ColumnNames.DataPagamento, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoHistorico.ValorCPMFDia
		/// </summary>
		virtual public System.Decimal? ValorCPMFDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorCPMFDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorCPMFDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoHistorico.ValorCPMFAcumulado
		/// </summary>
		virtual public System.Decimal? ValorCPMFAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorCPMFAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorCPMFAcumulado, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoAdministracaoHistorico.ValorBase
		/// </summary>
		virtual public System.Decimal? ValorBase
		{
			get
			{
				return base.GetSystemDecimal(CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorBase);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorBase, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected TabelaTaxaAdministracao _UpToTabelaTaxaAdministracaoByIdTabela;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCalculoAdministracaoHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorDia
			{
				get
				{
					System.Decimal? data = entity.ValorDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDia = null;
					else entity.ValorDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorAcumulado
			{
				get
				{
					System.Decimal? data = entity.ValorAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAcumulado = null;
					else entity.ValorAcumulado = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataFimApropriacao
			{
				get
				{
					System.DateTime? data = entity.DataFimApropriacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFimApropriacao = null;
					else entity.DataFimApropriacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataPagamento
			{
				get
				{
					System.DateTime? data = entity.DataPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPagamento = null;
					else entity.DataPagamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorCPMFDia
			{
				get
				{
					System.Decimal? data = entity.ValorCPMFDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMFDia = null;
					else entity.ValorCPMFDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCPMFAcumulado
			{
				get
				{
					System.Decimal? data = entity.ValorCPMFAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMFAcumulado = null;
					else entity.ValorCPMFAcumulado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBase
			{
				get
				{
					System.Decimal? data = entity.ValorBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBase = null;
					else entity.ValorBase = Convert.ToDecimal(value);
				}
			}
			

			private esCalculoAdministracaoHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCalculoAdministracaoHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCalculoAdministracaoHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CalculoAdministracaoHistorico : esCalculoAdministracaoHistorico
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_CalculoAdministracaoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTabelaTaxaAdministracaoByIdTabela - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TabelaTaxaAdministracao_CalculoAdministracaoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaAdministracao UpToTabelaTaxaAdministracaoByIdTabela
		{
			get
			{
				if(this._UpToTabelaTaxaAdministracaoByIdTabela == null
					&& IdTabela != null					)
				{
					this._UpToTabelaTaxaAdministracaoByIdTabela = new TabelaTaxaAdministracao();
					this._UpToTabelaTaxaAdministracaoByIdTabela.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTabelaTaxaAdministracaoByIdTabela", this._UpToTabelaTaxaAdministracaoByIdTabela);
					this._UpToTabelaTaxaAdministracaoByIdTabela.Query.Where(this._UpToTabelaTaxaAdministracaoByIdTabela.Query.IdTabela == this.IdTabela);
					this._UpToTabelaTaxaAdministracaoByIdTabela.Query.Load();
				}

				return this._UpToTabelaTaxaAdministracaoByIdTabela;
			}
			
			set
			{
				this.RemovePreSave("UpToTabelaTaxaAdministracaoByIdTabela");
				

				if(value == null)
				{
					this.IdTabela = null;
					this._UpToTabelaTaxaAdministracaoByIdTabela = null;
				}
				else
				{
					this.IdTabela = value.IdTabela;
					this._UpToTabelaTaxaAdministracaoByIdTabela = value;
					this.SetPreSave("UpToTabelaTaxaAdministracaoByIdTabela", this._UpToTabelaTaxaAdministracaoByIdTabela);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTabelaTaxaAdministracaoByIdTabela != null)
			{
				this.IdTabela = this._UpToTabelaTaxaAdministracaoByIdTabela.IdTabela;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCalculoAdministracaoHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CalculoAdministracaoHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoHistoricoMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoHistoricoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorDia
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorAcumulado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataFimApropriacao
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoHistoricoMetadata.ColumnNames.DataFimApropriacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataPagamento
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoHistoricoMetadata.ColumnNames.DataPagamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorCPMFDia
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorCPMFDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCPMFAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorCPMFAcumulado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBase
		{
			get
			{
				return new esQueryItem(this, CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorBase, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CalculoAdministracaoHistoricoCollection")]
	public partial class CalculoAdministracaoHistoricoCollection : esCalculoAdministracaoHistoricoCollection, IEnumerable<CalculoAdministracaoHistorico>
	{
		public CalculoAdministracaoHistoricoCollection()
		{

		}
		
		public static implicit operator List<CalculoAdministracaoHistorico>(CalculoAdministracaoHistoricoCollection coll)
		{
			List<CalculoAdministracaoHistorico> list = new List<CalculoAdministracaoHistorico>();
			
			foreach (CalculoAdministracaoHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CalculoAdministracaoHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoAdministracaoHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CalculoAdministracaoHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CalculoAdministracaoHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CalculoAdministracaoHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoAdministracaoHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CalculoAdministracaoHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CalculoAdministracaoHistorico AddNew()
		{
			CalculoAdministracaoHistorico entity = base.AddNewEntity() as CalculoAdministracaoHistorico;
			
			return entity;
		}

		public CalculoAdministracaoHistorico FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idTabela)
		{
			return base.FindByPrimaryKey(dataHistorico, idTabela) as CalculoAdministracaoHistorico;
		}


		#region IEnumerable<CalculoAdministracaoHistorico> Members

		IEnumerator<CalculoAdministracaoHistorico> IEnumerable<CalculoAdministracaoHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CalculoAdministracaoHistorico;
			}
		}

		#endregion
		
		private CalculoAdministracaoHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CalculoAdministracaoHistorico' table
	/// </summary>

	[Serializable]
	public partial class CalculoAdministracaoHistorico : esCalculoAdministracaoHistorico
	{
		public CalculoAdministracaoHistorico()
		{

		}
	
		public CalculoAdministracaoHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CalculoAdministracaoHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esCalculoAdministracaoHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoAdministracaoHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CalculoAdministracaoHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoAdministracaoHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CalculoAdministracaoHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CalculoAdministracaoHistoricoQuery query;
	}



	[Serializable]
	public partial class CalculoAdministracaoHistoricoQuery : esCalculoAdministracaoHistoricoQuery
	{
		public CalculoAdministracaoHistoricoQuery()
		{

		}		
		
		public CalculoAdministracaoHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CalculoAdministracaoHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CalculoAdministracaoHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CalculoAdministracaoHistoricoMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoAdministracaoHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoHistoricoMetadata.ColumnNames.IdTabela, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoAdministracaoHistoricoMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoHistoricoMetadata.ColumnNames.IdCarteira, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoAdministracaoHistoricoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorDia, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoAdministracaoHistoricoMetadata.PropertyNames.ValorDia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorAcumulado, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoAdministracaoHistoricoMetadata.PropertyNames.ValorAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoHistoricoMetadata.ColumnNames.DataFimApropriacao, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoAdministracaoHistoricoMetadata.PropertyNames.DataFimApropriacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoHistoricoMetadata.ColumnNames.DataPagamento, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoAdministracaoHistoricoMetadata.PropertyNames.DataPagamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorCPMFDia, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoAdministracaoHistoricoMetadata.PropertyNames.ValorCPMFDia;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorCPMFAcumulado, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoAdministracaoHistoricoMetadata.PropertyNames.ValorCPMFAcumulado;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoAdministracaoHistoricoMetadata.ColumnNames.ValorBase, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoAdministracaoHistoricoMetadata.PropertyNames.ValorBase;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CalculoAdministracaoHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdTabela = "IdTabela";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorDia = "ValorDia";
			 public const string ValorAcumulado = "ValorAcumulado";
			 public const string DataFimApropriacao = "DataFimApropriacao";
			 public const string DataPagamento = "DataPagamento";
			 public const string ValorCPMFDia = "ValorCPMFDia";
			 public const string ValorCPMFAcumulado = "ValorCPMFAcumulado";
			 public const string ValorBase = "ValorBase";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdTabela = "IdTabela";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorDia = "ValorDia";
			 public const string ValorAcumulado = "ValorAcumulado";
			 public const string DataFimApropriacao = "DataFimApropriacao";
			 public const string DataPagamento = "DataPagamento";
			 public const string ValorCPMFDia = "ValorCPMFDia";
			 public const string ValorCPMFAcumulado = "ValorCPMFAcumulado";
			 public const string ValorBase = "ValorBase";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CalculoAdministracaoHistoricoMetadata))
			{
				if(CalculoAdministracaoHistoricoMetadata.mapDelegates == null)
				{
					CalculoAdministracaoHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CalculoAdministracaoHistoricoMetadata.meta == null)
				{
					CalculoAdministracaoHistoricoMetadata.meta = new CalculoAdministracaoHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorAcumulado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataFimApropriacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataPagamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorCPMFDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCPMFAcumulado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBase", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CalculoAdministracaoHistorico";
				meta.Destination = "CalculoAdministracaoHistorico";
				
				meta.spInsert = "proc_CalculoAdministracaoHistoricoInsert";				
				meta.spUpdate = "proc_CalculoAdministracaoHistoricoUpdate";		
				meta.spDelete = "proc_CalculoAdministracaoHistoricoDelete";
				meta.spLoadAll = "proc_CalculoAdministracaoHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_CalculoAdministracaoHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CalculoAdministracaoHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
