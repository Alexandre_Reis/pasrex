/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 05/03/2015 16:51:50
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esEventoRollUpCollection : esEntityCollection
	{
		public esEventoRollUpCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EventoRollUpCollection";
		}

		#region Query Logic
		protected void InitQuery(esEventoRollUpQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEventoRollUpQuery);
		}
		#endregion
		
		virtual public EventoRollUp DetachEntity(EventoRollUp entity)
		{
			return base.DetachEntity(entity) as EventoRollUp;
		}
		
		virtual public EventoRollUp AttachEntity(EventoRollUp entity)
		{
			return base.AttachEntity(entity) as EventoRollUp;
		}
		
		virtual public void Combine(EventoRollUpCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EventoRollUp this[int index]
		{
			get
			{
				return base[index] as EventoRollUp;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EventoRollUp);
		}
	}



	[Serializable]
	abstract public class esEventoRollUp : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEventoRollUpQuery GetDynamicQuery()
		{
			return null;
		}

		public esEventoRollUp()
		{

		}

		public esEventoRollUp(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEventoRollUp)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEventoRollUp);
			else
				return LoadByPrimaryKeyStoredProcedure(idEventoRollUp);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEventoRollUp)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEventoRollUp);
			else
				return LoadByPrimaryKeyStoredProcedure(idEventoRollUp);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEventoRollUp)
		{
			esEventoRollUpQuery query = this.GetDynamicQuery();
			query.Where(query.IdEventoRollUp == idEventoRollUp);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEventoRollUp)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEventoRollUp",idEventoRollUp);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEventoRollUp": this.str.IdEventoRollUp = (string)value; break;							
						case "DataExecucao": this.str.DataExecucao = (string)value; break;							
						case "DataNav": this.str.DataNav = (string)value; break;							
						case "IdFundoOffShore": this.str.IdFundoOffShore = (string)value; break;							
						case "IdSerieOrigem": this.str.IdSerieOrigem = (string)value; break;							
						case "IdSerieDestino": this.str.IdSerieDestino = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEventoRollUp":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoRollUp = (System.Int32?)value;
							break;
						
						case "DataExecucao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataExecucao = (System.DateTime?)value;
							break;
						
						case "DataNav":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataNav = (System.DateTime?)value;
							break;
						
						case "IdFundoOffShore":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdFundoOffShore = (System.Int32?)value;
							break;
						
						case "IdSerieOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSerieOrigem = (System.Int32?)value;
							break;
						
						case "IdSerieDestino":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSerieDestino = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EventoRollUp.IdEventoRollUp
		/// </summary>
		virtual public System.Int32? IdEventoRollUp
		{
			get
			{
				return base.GetSystemInt32(EventoRollUpMetadata.ColumnNames.IdEventoRollUp);
			}
			
			set
			{
				base.SetSystemInt32(EventoRollUpMetadata.ColumnNames.IdEventoRollUp, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoRollUp.DataExecucao
		/// </summary>
		virtual public System.DateTime? DataExecucao
		{
			get
			{
				return base.GetSystemDateTime(EventoRollUpMetadata.ColumnNames.DataExecucao);
			}
			
			set
			{
				base.SetSystemDateTime(EventoRollUpMetadata.ColumnNames.DataExecucao, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoRollUp.DataNav
		/// </summary>
		virtual public System.DateTime? DataNav
		{
			get
			{
				return base.GetSystemDateTime(EventoRollUpMetadata.ColumnNames.DataNav);
			}
			
			set
			{
				base.SetSystemDateTime(EventoRollUpMetadata.ColumnNames.DataNav, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoRollUp.IdFundoOffShore
		/// </summary>
		virtual public System.Int32? IdFundoOffShore
		{
			get
			{
				return base.GetSystemInt32(EventoRollUpMetadata.ColumnNames.IdFundoOffShore);
			}
			
			set
			{
				base.SetSystemInt32(EventoRollUpMetadata.ColumnNames.IdFundoOffShore, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoRollUp.IdSerieOrigem
		/// </summary>
		virtual public System.Int32? IdSerieOrigem
		{
			get
			{
				return base.GetSystemInt32(EventoRollUpMetadata.ColumnNames.IdSerieOrigem);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoRollUpMetadata.ColumnNames.IdSerieOrigem, value))
				{
					this._UpToSeriesOffShoreByIdSerieOrigem = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoRollUp.IdSerieDestino
		/// </summary>
		virtual public System.Int32? IdSerieDestino
		{
			get
			{
				return base.GetSystemInt32(EventoRollUpMetadata.ColumnNames.IdSerieDestino);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoRollUpMetadata.ColumnNames.IdSerieDestino, value))
				{
					this._UpToSeriesOffShoreByIdSerieDestino = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected SeriesOffShore _UpToSeriesOffShoreByIdSerieOrigem;
		[CLSCompliant(false)]
		internal protected SeriesOffShore _UpToSeriesOffShoreByIdSerieDestino;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEventoRollUp entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEventoRollUp
			{
				get
				{
					System.Int32? data = entity.IdEventoRollUp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoRollUp = null;
					else entity.IdEventoRollUp = Convert.ToInt32(value);
				}
			}
				
			public System.String DataExecucao
			{
				get
				{
					System.DateTime? data = entity.DataExecucao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataExecucao = null;
					else entity.DataExecucao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataNav
			{
				get
				{
					System.DateTime? data = entity.DataNav;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataNav = null;
					else entity.DataNav = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdFundoOffShore
			{
				get
				{
					System.Int32? data = entity.IdFundoOffShore;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdFundoOffShore = null;
					else entity.IdFundoOffShore = Convert.ToInt32(value);
				}
			}
				
			public System.String IdSerieOrigem
			{
				get
				{
					System.Int32? data = entity.IdSerieOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSerieOrigem = null;
					else entity.IdSerieOrigem = Convert.ToInt32(value);
				}
			}
				
			public System.String IdSerieDestino
			{
				get
				{
					System.Int32? data = entity.IdSerieDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSerieDestino = null;
					else entity.IdSerieDestino = Convert.ToInt32(value);
				}
			}
			

			private esEventoRollUp entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEventoRollUpQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEventoRollUp can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EventoRollUp : esEventoRollUp
	{

				
		#region UpToSeriesOffShoreByIdSerieOrigem - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EventoRollUp_SerieDest_FK
		/// </summary>

		[XmlIgnore]
		public SeriesOffShore UpToSeriesOffShoreByIdSerieOrigem
		{
			get
			{
				if(this._UpToSeriesOffShoreByIdSerieOrigem == null
					&& IdSerieOrigem != null					)
				{
					this._UpToSeriesOffShoreByIdSerieOrigem = new SeriesOffShore();
					this._UpToSeriesOffShoreByIdSerieOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSeriesOffShoreByIdSerieOrigem", this._UpToSeriesOffShoreByIdSerieOrigem);
					this._UpToSeriesOffShoreByIdSerieOrigem.Query.Where(this._UpToSeriesOffShoreByIdSerieOrigem.Query.IdSeriesOffShore == this.IdSerieOrigem);
					this._UpToSeriesOffShoreByIdSerieOrigem.Query.Load();
				}

				return this._UpToSeriesOffShoreByIdSerieOrigem;
			}
			
			set
			{
				this.RemovePreSave("UpToSeriesOffShoreByIdSerieOrigem");
				

				if(value == null)
				{
					this.IdSerieOrigem = null;
					this._UpToSeriesOffShoreByIdSerieOrigem = null;
				}
				else
				{
					this.IdSerieOrigem = value.IdSeriesOffShore;
					this._UpToSeriesOffShoreByIdSerieOrigem = value;
					this.SetPreSave("UpToSeriesOffShoreByIdSerieOrigem", this._UpToSeriesOffShoreByIdSerieOrigem);
				}
				
			}
		}
		#endregion
		

				
		#region UpToSeriesOffShoreByIdSerieDestino - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EventoRollUp_SerieOrig_FK
		/// </summary>

		[XmlIgnore]
		public SeriesOffShore UpToSeriesOffShoreByIdSerieDestino
		{
			get
			{
				if(this._UpToSeriesOffShoreByIdSerieDestino == null
					&& IdSerieDestino != null					)
				{
					this._UpToSeriesOffShoreByIdSerieDestino = new SeriesOffShore();
					this._UpToSeriesOffShoreByIdSerieDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSeriesOffShoreByIdSerieDestino", this._UpToSeriesOffShoreByIdSerieDestino);
					this._UpToSeriesOffShoreByIdSerieDestino.Query.Where(this._UpToSeriesOffShoreByIdSerieDestino.Query.IdSeriesOffShore == this.IdSerieDestino);
					this._UpToSeriesOffShoreByIdSerieDestino.Query.Load();
				}

				return this._UpToSeriesOffShoreByIdSerieDestino;
			}
			
			set
			{
				this.RemovePreSave("UpToSeriesOffShoreByIdSerieDestino");
				

				if(value == null)
				{
					this.IdSerieDestino = null;
					this._UpToSeriesOffShoreByIdSerieDestino = null;
				}
				else
				{
					this.IdSerieDestino = value.IdSeriesOffShore;
					this._UpToSeriesOffShoreByIdSerieDestino = value;
					this.SetPreSave("UpToSeriesOffShoreByIdSerieDestino", this._UpToSeriesOffShoreByIdSerieDestino);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToSeriesOffShoreByIdSerieOrigem != null)
			{
				this.IdSerieOrigem = this._UpToSeriesOffShoreByIdSerieOrigem.IdSeriesOffShore;
			}
			if(!this.es.IsDeleted && this._UpToSeriesOffShoreByIdSerieDestino != null)
			{
				this.IdSerieDestino = this._UpToSeriesOffShoreByIdSerieDestino.IdSeriesOffShore;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEventoRollUpQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EventoRollUpMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEventoRollUp
		{
			get
			{
				return new esQueryItem(this, EventoRollUpMetadata.ColumnNames.IdEventoRollUp, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataExecucao
		{
			get
			{
				return new esQueryItem(this, EventoRollUpMetadata.ColumnNames.DataExecucao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataNav
		{
			get
			{
				return new esQueryItem(this, EventoRollUpMetadata.ColumnNames.DataNav, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdFundoOffShore
		{
			get
			{
				return new esQueryItem(this, EventoRollUpMetadata.ColumnNames.IdFundoOffShore, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdSerieOrigem
		{
			get
			{
				return new esQueryItem(this, EventoRollUpMetadata.ColumnNames.IdSerieOrigem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdSerieDestino
		{
			get
			{
				return new esQueryItem(this, EventoRollUpMetadata.ColumnNames.IdSerieDestino, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EventoRollUpCollection")]
	public partial class EventoRollUpCollection : esEventoRollUpCollection, IEnumerable<EventoRollUp>
	{
		public EventoRollUpCollection()
		{

		}
		
		public static implicit operator List<EventoRollUp>(EventoRollUpCollection coll)
		{
			List<EventoRollUp> list = new List<EventoRollUp>();
			
			foreach (EventoRollUp emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EventoRollUpMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoRollUpQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EventoRollUp(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EventoRollUp();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EventoRollUpQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoRollUpQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EventoRollUpQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EventoRollUp AddNew()
		{
			EventoRollUp entity = base.AddNewEntity() as EventoRollUp;
			
			return entity;
		}

		public EventoRollUp FindByPrimaryKey(System.Int32 idEventoRollUp)
		{
			return base.FindByPrimaryKey(idEventoRollUp) as EventoRollUp;
		}


		#region IEnumerable<EventoRollUp> Members

		IEnumerator<EventoRollUp> IEnumerable<EventoRollUp>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EventoRollUp;
			}
		}

		#endregion
		
		private EventoRollUpQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EventoRollUp' table
	/// </summary>

	[Serializable]
	public partial class EventoRollUp : esEventoRollUp
	{
		public EventoRollUp()
		{

		}
	
		public EventoRollUp(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EventoRollUpMetadata.Meta();
			}
		}
		
		
		
		override protected esEventoRollUpQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoRollUpQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EventoRollUpQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoRollUpQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EventoRollUpQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EventoRollUpQuery query;
	}



	[Serializable]
	public partial class EventoRollUpQuery : esEventoRollUpQuery
	{
		public EventoRollUpQuery()
		{

		}		
		
		public EventoRollUpQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EventoRollUpMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EventoRollUpMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EventoRollUpMetadata.ColumnNames.IdEventoRollUp, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoRollUpMetadata.PropertyNames.IdEventoRollUp;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoRollUpMetadata.ColumnNames.DataExecucao, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EventoRollUpMetadata.PropertyNames.DataExecucao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoRollUpMetadata.ColumnNames.DataNav, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EventoRollUpMetadata.PropertyNames.DataNav;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoRollUpMetadata.ColumnNames.IdFundoOffShore, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoRollUpMetadata.PropertyNames.IdFundoOffShore;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoRollUpMetadata.ColumnNames.IdSerieOrigem, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoRollUpMetadata.PropertyNames.IdSerieOrigem;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoRollUpMetadata.ColumnNames.IdSerieDestino, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoRollUpMetadata.PropertyNames.IdSerieDestino;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EventoRollUpMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEventoRollUp = "IdEventoRollUp";
			 public const string DataExecucao = "DataExecucao";
			 public const string DataNav = "DataNav";
			 public const string IdFundoOffShore = "IdFundoOffShore";
			 public const string IdSerieOrigem = "IdSerieOrigem";
			 public const string IdSerieDestino = "IdSerieDestino";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEventoRollUp = "IdEventoRollUp";
			 public const string DataExecucao = "DataExecucao";
			 public const string DataNav = "DataNav";
			 public const string IdFundoOffShore = "IdFundoOffShore";
			 public const string IdSerieOrigem = "IdSerieOrigem";
			 public const string IdSerieDestino = "IdSerieDestino";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EventoRollUpMetadata))
			{
				if(EventoRollUpMetadata.mapDelegates == null)
				{
					EventoRollUpMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EventoRollUpMetadata.meta == null)
				{
					EventoRollUpMetadata.meta = new EventoRollUpMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEventoRollUp", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataExecucao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataNav", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdFundoOffShore", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdSerieOrigem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdSerieDestino", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "EventoRollUp";
				meta.Destination = "EventoRollUp";
				
				meta.spInsert = "proc_EventoRollUpInsert";				
				meta.spUpdate = "proc_EventoRollUpUpdate";		
				meta.spDelete = "proc_EventoRollUpDelete";
				meta.spLoadAll = "proc_EventoRollUpLoadAll";
				meta.spLoadByPrimaryKey = "proc_EventoRollUpLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EventoRollUpMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
