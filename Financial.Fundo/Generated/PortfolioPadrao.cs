/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 14/12/2015 18:15:40
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esPortfolioPadraoCollection : esEntityCollection
	{
		public esPortfolioPadraoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PortfolioPadraoCollection";
		}

		#region Query Logic
		protected void InitQuery(esPortfolioPadraoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPortfolioPadraoQuery);
		}
		#endregion
		
		virtual public PortfolioPadrao DetachEntity(PortfolioPadrao entity)
		{
			return base.DetachEntity(entity) as PortfolioPadrao;
		}
		
		virtual public PortfolioPadrao AttachEntity(PortfolioPadrao entity)
		{
			return base.AttachEntity(entity) as PortfolioPadrao;
		}
		
		virtual public void Combine(PortfolioPadraoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PortfolioPadrao this[int index]
		{
			get
			{
				return base[index] as PortfolioPadrao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PortfolioPadrao);
		}
	}



	[Serializable]
	abstract public class esPortfolioPadrao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPortfolioPadraoQuery GetDynamicQuery()
		{
			return null;
		}

		public esPortfolioPadrao()
		{

		}

		public esPortfolioPadrao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPortfolioPadrao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPortfolioPadrao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPortfolioPadrao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPortfolioPadrao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPortfolioPadrao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPortfolioPadrao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPortfolioPadrao)
		{
			esPortfolioPadraoQuery query = this.GetDynamicQuery();
			query.Where(query.IdPortfolioPadrao == idPortfolioPadrao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPortfolioPadrao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPortfolioPadrao",idPortfolioPadrao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPortfolioPadrao": this.str.IdPortfolioPadrao = (string)value; break;							
						case "DataVigencia": this.str.DataVigencia = (string)value; break;							
						case "IdCarteiraTaxaGestao": this.str.IdCarteiraTaxaGestao = (string)value; break;							
						case "IdCarteiraTaxaPerformance": this.str.IdCarteiraTaxaPerformance = (string)value; break;							
						case "IdCarteiraDespesasReceitas": this.str.IdCarteiraDespesasReceitas = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPortfolioPadrao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPortfolioPadrao = (System.Int32?)value;
							break;
						
						case "DataVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVigencia = (System.DateTime?)value;
							break;
						
						case "IdCarteiraTaxaGestao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraTaxaGestao = (System.Int32?)value;
							break;
						
						case "IdCarteiraTaxaPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraTaxaPerformance = (System.Int32?)value;
							break;
						
						case "IdCarteiraDespesasReceitas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraDespesasReceitas = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PortfolioPadrao.IdPortfolioPadrao
		/// </summary>
		virtual public System.Int32? IdPortfolioPadrao
		{
			get
			{
				return base.GetSystemInt32(PortfolioPadraoMetadata.ColumnNames.IdPortfolioPadrao);
			}
			
			set
			{
				base.SetSystemInt32(PortfolioPadraoMetadata.ColumnNames.IdPortfolioPadrao, value);
			}
		}
		
		/// <summary>
		/// Maps to PortfolioPadrao.DataVigencia
		/// </summary>
		virtual public System.DateTime? DataVigencia
		{
			get
			{
				return base.GetSystemDateTime(PortfolioPadraoMetadata.ColumnNames.DataVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(PortfolioPadraoMetadata.ColumnNames.DataVigencia, value);
			}
		}
		
		/// <summary>
		/// Maps to PortfolioPadrao.IdCarteiraTaxaGestao
		/// </summary>
		virtual public System.Int32? IdCarteiraTaxaGestao
		{
			get
			{
				return base.GetSystemInt32(PortfolioPadraoMetadata.ColumnNames.IdCarteiraTaxaGestao);
			}
			
			set
			{
				if(base.SetSystemInt32(PortfolioPadraoMetadata.ColumnNames.IdCarteiraTaxaGestao, value))
				{
					this._UpToCarteiraByIdCarteiraTaxaGestao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PortfolioPadrao.IdCarteiraTaxaPerformance
		/// </summary>
		virtual public System.Int32? IdCarteiraTaxaPerformance
		{
			get
			{
				return base.GetSystemInt32(PortfolioPadraoMetadata.ColumnNames.IdCarteiraTaxaPerformance);
			}
			
			set
			{
				if(base.SetSystemInt32(PortfolioPadraoMetadata.ColumnNames.IdCarteiraTaxaPerformance, value))
				{
					this._UpToCarteiraByIdCarteiraTaxaPerformance = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PortfolioPadrao.IdCarteiraDespesasReceitas
		/// </summary>
		virtual public System.Int32? IdCarteiraDespesasReceitas
		{
			get
			{
				return base.GetSystemInt32(PortfolioPadraoMetadata.ColumnNames.IdCarteiraDespesasReceitas);
			}
			
			set
			{
				if(base.SetSystemInt32(PortfolioPadraoMetadata.ColumnNames.IdCarteiraDespesasReceitas, value))
				{
					this._UpToCarteiraByIdCarteiraDespesasReceitas = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteiraTaxaGestao;
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteiraTaxaPerformance;
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteiraDespesasReceitas;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPortfolioPadrao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPortfolioPadrao
			{
				get
				{
					System.Int32? data = entity.IdPortfolioPadrao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPortfolioPadrao = null;
					else entity.IdPortfolioPadrao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataVigencia
			{
				get
				{
					System.DateTime? data = entity.DataVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVigencia = null;
					else entity.DataVigencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteiraTaxaGestao
			{
				get
				{
					System.Int32? data = entity.IdCarteiraTaxaGestao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraTaxaGestao = null;
					else entity.IdCarteiraTaxaGestao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteiraTaxaPerformance
			{
				get
				{
					System.Int32? data = entity.IdCarteiraTaxaPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraTaxaPerformance = null;
					else entity.IdCarteiraTaxaPerformance = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteiraDespesasReceitas
			{
				get
				{
					System.Int32? data = entity.IdCarteiraDespesasReceitas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraDespesasReceitas = null;
					else entity.IdCarteiraDespesasReceitas = Convert.ToInt32(value);
				}
			}
			

			private esPortfolioPadrao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPortfolioPadraoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPortfolioPadrao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PortfolioPadrao : esPortfolioPadrao
	{

				
		#region UpToCarteiraByIdCarteiraTaxaGestao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_CarteiraTaxaGestao_Carteira
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteiraTaxaGestao
		{
			get
			{
				if(this._UpToCarteiraByIdCarteiraTaxaGestao == null
					&& IdCarteiraTaxaGestao != null					)
				{
					this._UpToCarteiraByIdCarteiraTaxaGestao = new Carteira();
					this._UpToCarteiraByIdCarteiraTaxaGestao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteiraTaxaGestao", this._UpToCarteiraByIdCarteiraTaxaGestao);
					this._UpToCarteiraByIdCarteiraTaxaGestao.Query.Where(this._UpToCarteiraByIdCarteiraTaxaGestao.Query.IdCarteira == this.IdCarteiraTaxaGestao);
					this._UpToCarteiraByIdCarteiraTaxaGestao.Query.Load();
				}

				return this._UpToCarteiraByIdCarteiraTaxaGestao;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteiraTaxaGestao");
				

				if(value == null)
				{
					this.IdCarteiraTaxaGestao = null;
					this._UpToCarteiraByIdCarteiraTaxaGestao = null;
				}
				else
				{
					this.IdCarteiraTaxaGestao = value.IdCarteira;
					this._UpToCarteiraByIdCarteiraTaxaGestao = value;
					this.SetPreSave("UpToCarteiraByIdCarteiraTaxaGestao", this._UpToCarteiraByIdCarteiraTaxaGestao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCarteiraByIdCarteiraTaxaPerformance - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_CarteiraTaxaPerformance_Carteira
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteiraTaxaPerformance
		{
			get
			{
				if(this._UpToCarteiraByIdCarteiraTaxaPerformance == null
					&& IdCarteiraTaxaPerformance != null					)
				{
					this._UpToCarteiraByIdCarteiraTaxaPerformance = new Carteira();
					this._UpToCarteiraByIdCarteiraTaxaPerformance.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteiraTaxaPerformance", this._UpToCarteiraByIdCarteiraTaxaPerformance);
					this._UpToCarteiraByIdCarteiraTaxaPerformance.Query.Where(this._UpToCarteiraByIdCarteiraTaxaPerformance.Query.IdCarteira == this.IdCarteiraTaxaPerformance);
					this._UpToCarteiraByIdCarteiraTaxaPerformance.Query.Load();
				}

				return this._UpToCarteiraByIdCarteiraTaxaPerformance;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteiraTaxaPerformance");
				

				if(value == null)
				{
					this.IdCarteiraTaxaPerformance = null;
					this._UpToCarteiraByIdCarteiraTaxaPerformance = null;
				}
				else
				{
					this.IdCarteiraTaxaPerformance = value.IdCarteira;
					this._UpToCarteiraByIdCarteiraTaxaPerformance = value;
					this.SetPreSave("UpToCarteiraByIdCarteiraTaxaPerformance", this._UpToCarteiraByIdCarteiraTaxaPerformance);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCarteiraByIdCarteiraDespesasReceitas - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_CarteiraDespesasReceitas_Carteira
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteiraDespesasReceitas
		{
			get
			{
				if(this._UpToCarteiraByIdCarteiraDespesasReceitas == null
					&& IdCarteiraDespesasReceitas != null					)
				{
					this._UpToCarteiraByIdCarteiraDespesasReceitas = new Carteira();
					this._UpToCarteiraByIdCarteiraDespesasReceitas.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteiraDespesasReceitas", this._UpToCarteiraByIdCarteiraDespesasReceitas);
					this._UpToCarteiraByIdCarteiraDespesasReceitas.Query.Where(this._UpToCarteiraByIdCarteiraDespesasReceitas.Query.IdCarteira == this.IdCarteiraDespesasReceitas);
					this._UpToCarteiraByIdCarteiraDespesasReceitas.Query.Load();
				}

				return this._UpToCarteiraByIdCarteiraDespesasReceitas;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteiraDespesasReceitas");
				

				if(value == null)
				{
					this.IdCarteiraDespesasReceitas = null;
					this._UpToCarteiraByIdCarteiraDespesasReceitas = null;
				}
				else
				{
					this.IdCarteiraDespesasReceitas = value.IdCarteira;
					this._UpToCarteiraByIdCarteiraDespesasReceitas = value;
					this.SetPreSave("UpToCarteiraByIdCarteiraDespesasReceitas", this._UpToCarteiraByIdCarteiraDespesasReceitas);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPortfolioPadraoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PortfolioPadraoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPortfolioPadrao
		{
			get
			{
				return new esQueryItem(this, PortfolioPadraoMetadata.ColumnNames.IdPortfolioPadrao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataVigencia
		{
			get
			{
				return new esQueryItem(this, PortfolioPadraoMetadata.ColumnNames.DataVigencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteiraTaxaGestao
		{
			get
			{
				return new esQueryItem(this, PortfolioPadraoMetadata.ColumnNames.IdCarteiraTaxaGestao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteiraTaxaPerformance
		{
			get
			{
				return new esQueryItem(this, PortfolioPadraoMetadata.ColumnNames.IdCarteiraTaxaPerformance, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteiraDespesasReceitas
		{
			get
			{
				return new esQueryItem(this, PortfolioPadraoMetadata.ColumnNames.IdCarteiraDespesasReceitas, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PortfolioPadraoCollection")]
	public partial class PortfolioPadraoCollection : esPortfolioPadraoCollection, IEnumerable<PortfolioPadrao>
	{
		public PortfolioPadraoCollection()
		{

		}
		
		public static implicit operator List<PortfolioPadrao>(PortfolioPadraoCollection coll)
		{
			List<PortfolioPadrao> list = new List<PortfolioPadrao>();
			
			foreach (PortfolioPadrao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PortfolioPadraoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PortfolioPadraoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PortfolioPadrao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PortfolioPadrao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PortfolioPadraoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PortfolioPadraoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PortfolioPadraoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PortfolioPadrao AddNew()
		{
			PortfolioPadrao entity = base.AddNewEntity() as PortfolioPadrao;
			
			return entity;
		}

		public PortfolioPadrao FindByPrimaryKey(System.Int32 idPortfolioPadrao)
		{
			return base.FindByPrimaryKey(idPortfolioPadrao) as PortfolioPadrao;
		}


		#region IEnumerable<PortfolioPadrao> Members

		IEnumerator<PortfolioPadrao> IEnumerable<PortfolioPadrao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PortfolioPadrao;
			}
		}

		#endregion
		
		private PortfolioPadraoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PortfolioPadrao' table
	/// </summary>

	[Serializable]
	public partial class PortfolioPadrao : esPortfolioPadrao
	{
		public PortfolioPadrao()
		{

		}
	
		public PortfolioPadrao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PortfolioPadraoMetadata.Meta();
			}
		}
		
		
		
		override protected esPortfolioPadraoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PortfolioPadraoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PortfolioPadraoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PortfolioPadraoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PortfolioPadraoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PortfolioPadraoQuery query;
	}



	[Serializable]
	public partial class PortfolioPadraoQuery : esPortfolioPadraoQuery
	{
		public PortfolioPadraoQuery()
		{

		}		
		
		public PortfolioPadraoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PortfolioPadraoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PortfolioPadraoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PortfolioPadraoMetadata.ColumnNames.IdPortfolioPadrao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PortfolioPadraoMetadata.PropertyNames.IdPortfolioPadrao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PortfolioPadraoMetadata.ColumnNames.DataVigencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PortfolioPadraoMetadata.PropertyNames.DataVigencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PortfolioPadraoMetadata.ColumnNames.IdCarteiraTaxaGestao, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PortfolioPadraoMetadata.PropertyNames.IdCarteiraTaxaGestao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PortfolioPadraoMetadata.ColumnNames.IdCarteiraTaxaPerformance, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PortfolioPadraoMetadata.PropertyNames.IdCarteiraTaxaPerformance;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PortfolioPadraoMetadata.ColumnNames.IdCarteiraDespesasReceitas, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PortfolioPadraoMetadata.PropertyNames.IdCarteiraDespesasReceitas;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PortfolioPadraoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPortfolioPadrao = "IdPortfolioPadrao";
			 public const string DataVigencia = "DataVigencia";
			 public const string IdCarteiraTaxaGestao = "IdCarteiraTaxaGestao";
			 public const string IdCarteiraTaxaPerformance = "IdCarteiraTaxaPerformance";
			 public const string IdCarteiraDespesasReceitas = "IdCarteiraDespesasReceitas";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPortfolioPadrao = "IdPortfolioPadrao";
			 public const string DataVigencia = "DataVigencia";
			 public const string IdCarteiraTaxaGestao = "IdCarteiraTaxaGestao";
			 public const string IdCarteiraTaxaPerformance = "IdCarteiraTaxaPerformance";
			 public const string IdCarteiraDespesasReceitas = "IdCarteiraDespesasReceitas";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PortfolioPadraoMetadata))
			{
				if(PortfolioPadraoMetadata.mapDelegates == null)
				{
					PortfolioPadraoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PortfolioPadraoMetadata.meta == null)
				{
					PortfolioPadraoMetadata.meta = new PortfolioPadraoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPortfolioPadrao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataVigencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteiraTaxaGestao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteiraTaxaPerformance", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteiraDespesasReceitas", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "PortfolioPadrao";
				meta.Destination = "PortfolioPadrao";
				
				meta.spInsert = "proc_PortfolioPadraoInsert";				
				meta.spUpdate = "proc_PortfolioPadraoUpdate";		
				meta.spDelete = "proc_PortfolioPadraoDelete";
				meta.spLoadAll = "proc_PortfolioPadraoLoadAll";
				meta.spLoadByPrimaryKey = "proc_PortfolioPadraoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PortfolioPadraoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
