/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 14/03/2016 11:41:34
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esEntidadeClassificacaoPerfilFundoCollection : esEntityCollection
	{
		public esEntidadeClassificacaoPerfilFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EntidadeClassificacaoPerfilFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esEntidadeClassificacaoPerfilFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEntidadeClassificacaoPerfilFundoQuery);
		}
		#endregion
		
		virtual public EntidadeClassificacaoPerfilFundo DetachEntity(EntidadeClassificacaoPerfilFundo entity)
		{
			return base.DetachEntity(entity) as EntidadeClassificacaoPerfilFundo;
		}
		
		virtual public EntidadeClassificacaoPerfilFundo AttachEntity(EntidadeClassificacaoPerfilFundo entity)
		{
			return base.AttachEntity(entity) as EntidadeClassificacaoPerfilFundo;
		}
		
		virtual public void Combine(EntidadeClassificacaoPerfilFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EntidadeClassificacaoPerfilFundo this[int index]
		{
			get
			{
				return base[index] as EntidadeClassificacaoPerfilFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EntidadeClassificacaoPerfilFundo);
		}
	}



	[Serializable]
	abstract public class esEntidadeClassificacaoPerfilFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEntidadeClassificacaoPerfilFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esEntidadeClassificacaoPerfilFundo()
		{

		}

		public esEntidadeClassificacaoPerfilFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEntidadeClassificacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEntidadeClassificacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idEntidadeClassificacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEntidadeClassificacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEntidadeClassificacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idEntidadeClassificacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEntidadeClassificacao)
		{
			esEntidadeClassificacaoPerfilFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdEntidadeClassificacao == idEntidadeClassificacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEntidadeClassificacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEntidadeClassificacao",idEntidadeClassificacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEntidadeClassificacao": this.str.IdEntidadeClassificacao = (string)value; break;							
						case "IdEntidadePerfilFundo": this.str.IdEntidadePerfilFundo = (string)value; break;							
						case "TipoFundoCarteira": this.str.TipoFundoCarteira = (string)value; break;							
						case "DescricaoTipoFundoCarteira": this.str.DescricaoTipoFundoCarteira = (string)value; break;							
						case "InicioVigencia": this.str.InicioVigencia = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEntidadeClassificacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEntidadeClassificacao = (System.Int32?)value;
							break;
						
						case "IdEntidadePerfilFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEntidadePerfilFundo = (System.Int32?)value;
							break;
						
						case "InicioVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.InicioVigencia = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EntidadeClassificacaoPerfilFundo.IdEntidadeClassificacao
		/// </summary>
		virtual public System.Int32? IdEntidadeClassificacao
		{
			get
			{
				return base.GetSystemInt32(EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.IdEntidadeClassificacao);
			}
			
			set
			{
				base.SetSystemInt32(EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.IdEntidadeClassificacao, value);
			}
		}
		
		/// <summary>
		/// Maps to EntidadeClassificacaoPerfilFundo.IdEntidadePerfilFundo
		/// </summary>
		virtual public System.Int32? IdEntidadePerfilFundo
		{
			get
			{
				return base.GetSystemInt32(EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.IdEntidadePerfilFundo);
			}
			
			set
			{
				if(base.SetSystemInt32(EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.IdEntidadePerfilFundo, value))
				{
					this._UpToEntidadePerfilFundoByIdEntidadePerfilFundo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EntidadeClassificacaoPerfilFundo.TipoFundoCarteira
		/// </summary>
		virtual public System.String TipoFundoCarteira
		{
			get
			{
				return base.GetSystemString(EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.TipoFundoCarteira);
			}
			
			set
			{
				base.SetSystemString(EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.TipoFundoCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to EntidadeClassificacaoPerfilFundo.DescricaoTipoFundoCarteira
		/// </summary>
		virtual public System.String DescricaoTipoFundoCarteira
		{
			get
			{
				return base.GetSystemString(EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.DescricaoTipoFundoCarteira);
			}
			
			set
			{
				base.SetSystemString(EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.DescricaoTipoFundoCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to EntidadeClassificacaoPerfilFundo.InicioVigencia
		/// </summary>
		virtual public System.DateTime? InicioVigencia
		{
			get
			{
				return base.GetSystemDateTime(EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.InicioVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.InicioVigencia, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected EntidadePerfilFundo _UpToEntidadePerfilFundoByIdEntidadePerfilFundo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEntidadeClassificacaoPerfilFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEntidadeClassificacao
			{
				get
				{
					System.Int32? data = entity.IdEntidadeClassificacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEntidadeClassificacao = null;
					else entity.IdEntidadeClassificacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEntidadePerfilFundo
			{
				get
				{
					System.Int32? data = entity.IdEntidadePerfilFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEntidadePerfilFundo = null;
					else entity.IdEntidadePerfilFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoFundoCarteira
			{
				get
				{
					System.String data = entity.TipoFundoCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoFundoCarteira = null;
					else entity.TipoFundoCarteira = Convert.ToString(value);
				}
			}
				
			public System.String DescricaoTipoFundoCarteira
			{
				get
				{
					System.String data = entity.DescricaoTipoFundoCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescricaoTipoFundoCarteira = null;
					else entity.DescricaoTipoFundoCarteira = Convert.ToString(value);
				}
			}
				
			public System.String InicioVigencia
			{
				get
				{
					System.DateTime? data = entity.InicioVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InicioVigencia = null;
					else entity.InicioVigencia = Convert.ToDateTime(value);
				}
			}
			

			private esEntidadeClassificacaoPerfilFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEntidadeClassificacaoPerfilFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEntidadeClassificacaoPerfilFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EntidadeClassificacaoPerfilFundo : esEntidadeClassificacaoPerfilFundo
	{

				
		#region UpToEntidadePerfilFundoByIdEntidadePerfilFundo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EntidadeClassificacaoPerfilFundo_EntidadePerfilFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EntidadePerfilFundo UpToEntidadePerfilFundoByIdEntidadePerfilFundo
		{
			get
			{
				if(this._UpToEntidadePerfilFundoByIdEntidadePerfilFundo == null
					&& IdEntidadePerfilFundo != null					)
				{
					this._UpToEntidadePerfilFundoByIdEntidadePerfilFundo = new EntidadePerfilFundo();
					this._UpToEntidadePerfilFundoByIdEntidadePerfilFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEntidadePerfilFundoByIdEntidadePerfilFundo", this._UpToEntidadePerfilFundoByIdEntidadePerfilFundo);
					this._UpToEntidadePerfilFundoByIdEntidadePerfilFundo.Query.Where(this._UpToEntidadePerfilFundoByIdEntidadePerfilFundo.Query.IdEntidade == this.IdEntidadePerfilFundo);
					this._UpToEntidadePerfilFundoByIdEntidadePerfilFundo.Query.Load();
				}

				return this._UpToEntidadePerfilFundoByIdEntidadePerfilFundo;
			}
			
			set
			{
				this.RemovePreSave("UpToEntidadePerfilFundoByIdEntidadePerfilFundo");
				

				if(value == null)
				{
					this.IdEntidadePerfilFundo = null;
					this._UpToEntidadePerfilFundoByIdEntidadePerfilFundo = null;
				}
				else
				{
					this.IdEntidadePerfilFundo = value.IdEntidade;
					this._UpToEntidadePerfilFundoByIdEntidadePerfilFundo = value;
					this.SetPreSave("UpToEntidadePerfilFundoByIdEntidadePerfilFundo", this._UpToEntidadePerfilFundoByIdEntidadePerfilFundo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEntidadePerfilFundoByIdEntidadePerfilFundo != null)
			{
				this.IdEntidadePerfilFundo = this._UpToEntidadePerfilFundoByIdEntidadePerfilFundo.IdEntidade;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEntidadeClassificacaoPerfilFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EntidadeClassificacaoPerfilFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEntidadeClassificacao
		{
			get
			{
				return new esQueryItem(this, EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.IdEntidadeClassificacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEntidadePerfilFundo
		{
			get
			{
				return new esQueryItem(this, EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.IdEntidadePerfilFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoFundoCarteira
		{
			get
			{
				return new esQueryItem(this, EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.TipoFundoCarteira, esSystemType.String);
			}
		} 
		
		public esQueryItem DescricaoTipoFundoCarteira
		{
			get
			{
				return new esQueryItem(this, EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.DescricaoTipoFundoCarteira, esSystemType.String);
			}
		} 
		
		public esQueryItem InicioVigencia
		{
			get
			{
				return new esQueryItem(this, EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.InicioVigencia, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EntidadeClassificacaoPerfilFundoCollection")]
	public partial class EntidadeClassificacaoPerfilFundoCollection : esEntidadeClassificacaoPerfilFundoCollection, IEnumerable<EntidadeClassificacaoPerfilFundo>
	{
		public EntidadeClassificacaoPerfilFundoCollection()
		{

		}
		
		public static implicit operator List<EntidadeClassificacaoPerfilFundo>(EntidadeClassificacaoPerfilFundoCollection coll)
		{
			List<EntidadeClassificacaoPerfilFundo> list = new List<EntidadeClassificacaoPerfilFundo>();
			
			foreach (EntidadeClassificacaoPerfilFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EntidadeClassificacaoPerfilFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EntidadeClassificacaoPerfilFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EntidadeClassificacaoPerfilFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EntidadeClassificacaoPerfilFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EntidadeClassificacaoPerfilFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EntidadeClassificacaoPerfilFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EntidadeClassificacaoPerfilFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EntidadeClassificacaoPerfilFundo AddNew()
		{
			EntidadeClassificacaoPerfilFundo entity = base.AddNewEntity() as EntidadeClassificacaoPerfilFundo;
			
			return entity;
		}

		public EntidadeClassificacaoPerfilFundo FindByPrimaryKey(System.Int32 idEntidadeClassificacao)
		{
			return base.FindByPrimaryKey(idEntidadeClassificacao) as EntidadeClassificacaoPerfilFundo;
		}


		#region IEnumerable<EntidadeClassificacaoPerfilFundo> Members

		IEnumerator<EntidadeClassificacaoPerfilFundo> IEnumerable<EntidadeClassificacaoPerfilFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EntidadeClassificacaoPerfilFundo;
			}
		}

		#endregion
		
		private EntidadeClassificacaoPerfilFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EntidadeClassificacaoPerfilFundo' table
	/// </summary>

	[Serializable]
	public partial class EntidadeClassificacaoPerfilFundo : esEntidadeClassificacaoPerfilFundo
	{
		public EntidadeClassificacaoPerfilFundo()
		{

		}
	
		public EntidadeClassificacaoPerfilFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EntidadeClassificacaoPerfilFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esEntidadeClassificacaoPerfilFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EntidadeClassificacaoPerfilFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EntidadeClassificacaoPerfilFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EntidadeClassificacaoPerfilFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EntidadeClassificacaoPerfilFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EntidadeClassificacaoPerfilFundoQuery query;
	}



	[Serializable]
	public partial class EntidadeClassificacaoPerfilFundoQuery : esEntidadeClassificacaoPerfilFundoQuery
	{
		public EntidadeClassificacaoPerfilFundoQuery()
		{

		}		
		
		public EntidadeClassificacaoPerfilFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EntidadeClassificacaoPerfilFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EntidadeClassificacaoPerfilFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.IdEntidadeClassificacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EntidadeClassificacaoPerfilFundoMetadata.PropertyNames.IdEntidadeClassificacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.IdEntidadePerfilFundo, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EntidadeClassificacaoPerfilFundoMetadata.PropertyNames.IdEntidadePerfilFundo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.TipoFundoCarteira, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = EntidadeClassificacaoPerfilFundoMetadata.PropertyNames.TipoFundoCarteira;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.DescricaoTipoFundoCarteira, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = EntidadeClassificacaoPerfilFundoMetadata.PropertyNames.DescricaoTipoFundoCarteira;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EntidadeClassificacaoPerfilFundoMetadata.ColumnNames.InicioVigencia, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EntidadeClassificacaoPerfilFundoMetadata.PropertyNames.InicioVigencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EntidadeClassificacaoPerfilFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEntidadeClassificacao = "IdEntidadeClassificacao";
			 public const string IdEntidadePerfilFundo = "IdEntidadePerfilFundo";
			 public const string TipoFundoCarteira = "TipoFundoCarteira";
			 public const string DescricaoTipoFundoCarteira = "DescricaoTipoFundoCarteira";
			 public const string InicioVigencia = "InicioVigencia";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEntidadeClassificacao = "IdEntidadeClassificacao";
			 public const string IdEntidadePerfilFundo = "IdEntidadePerfilFundo";
			 public const string TipoFundoCarteira = "TipoFundoCarteira";
			 public const string DescricaoTipoFundoCarteira = "DescricaoTipoFundoCarteira";
			 public const string InicioVigencia = "InicioVigencia";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EntidadeClassificacaoPerfilFundoMetadata))
			{
				if(EntidadeClassificacaoPerfilFundoMetadata.mapDelegates == null)
				{
					EntidadeClassificacaoPerfilFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EntidadeClassificacaoPerfilFundoMetadata.meta == null)
				{
					EntidadeClassificacaoPerfilFundoMetadata.meta = new EntidadeClassificacaoPerfilFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEntidadeClassificacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEntidadePerfilFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoFundoCarteira", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DescricaoTipoFundoCarteira", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("InicioVigencia", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "EntidadeClassificacaoPerfilFundo";
				meta.Destination = "EntidadeClassificacaoPerfilFundo";
				
				meta.spInsert = "proc_EntidadeClassificacaoPerfilFundoInsert";				
				meta.spUpdate = "proc_EntidadeClassificacaoPerfilFundoUpdate";		
				meta.spDelete = "proc_EntidadeClassificacaoPerfilFundoDelete";
				meta.spLoadAll = "proc_EntidadeClassificacaoPerfilFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_EntidadeClassificacaoPerfilFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EntidadeClassificacaoPerfilFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
