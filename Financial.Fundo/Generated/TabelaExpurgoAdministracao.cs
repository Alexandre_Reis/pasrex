/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:21 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTabelaExpurgoAdministracaoCollection : esEntityCollection
	{
		public esTabelaExpurgoAdministracaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaExpurgoAdministracaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaExpurgoAdministracaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaExpurgoAdministracaoQuery);
		}
		#endregion
		
		virtual public TabelaExpurgoAdministracao DetachEntity(TabelaExpurgoAdministracao entity)
		{
			return base.DetachEntity(entity) as TabelaExpurgoAdministracao;
		}
		
		virtual public TabelaExpurgoAdministracao AttachEntity(TabelaExpurgoAdministracao entity)
		{
			return base.AttachEntity(entity) as TabelaExpurgoAdministracao;
		}
		
		virtual public void Combine(TabelaExpurgoAdministracaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaExpurgoAdministracao this[int index]
		{
			get
			{
				return base[index] as TabelaExpurgoAdministracao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaExpurgoAdministracao);
		}
	}



	[Serializable]
	abstract public class esTabelaExpurgoAdministracao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaExpurgoAdministracaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaExpurgoAdministracao()
		{

		}

		public esTabelaExpurgoAdministracao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.Int32 idTabela, System.Int32 tipo, System.String codigo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idTabela, tipo, codigo);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idTabela, tipo, codigo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.Int32 idTabela, System.Int32 tipo, System.String codigo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaExpurgoAdministracaoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.IdTabela == idTabela, query.Tipo == tipo, query.Codigo == codigo);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.Int32 idTabela, System.Int32 tipo, System.String codigo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idTabela, tipo, codigo);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idTabela, tipo, codigo);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.Int32 idTabela, System.Int32 tipo, System.String codigo)
		{
			esTabelaExpurgoAdministracaoQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.IdTabela == idTabela, query.Tipo == tipo, query.Codigo == codigo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.Int32 idTabela, System.Int32 tipo, System.String codigo)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("IdTabela",idTabela);			parms.Add("Tipo",tipo);			parms.Add("Codigo",codigo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;							
						case "Codigo": this.str.Codigo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Tipo = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaExpurgoAdministracao.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaExpurgoAdministracaoMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaExpurgoAdministracaoMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaExpurgoAdministracao.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(TabelaExpurgoAdministracaoMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaExpurgoAdministracaoMetadata.ColumnNames.IdTabela, value))
				{
					this._UpToTabelaTaxaAdministracaoByIdTabela = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaExpurgoAdministracao.Tipo
		/// </summary>
		virtual public System.Int32? Tipo
		{
			get
			{
				return base.GetSystemInt32(TabelaExpurgoAdministracaoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemInt32(TabelaExpurgoAdministracaoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaExpurgoAdministracao.Codigo
		/// </summary>
		virtual public System.String Codigo
		{
			get
			{
				return base.GetSystemString(TabelaExpurgoAdministracaoMetadata.ColumnNames.Codigo);
			}
			
			set
			{
				base.SetSystemString(TabelaExpurgoAdministracaoMetadata.ColumnNames.Codigo, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected TabelaTaxaAdministracao _UpToTabelaTaxaAdministracaoByIdTabela;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaExpurgoAdministracao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Int32? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToInt32(value);
				}
			}
				
			public System.String Codigo
			{
				get
				{
					System.String data = entity.Codigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Codigo = null;
					else entity.Codigo = Convert.ToString(value);
				}
			}
			

			private esTabelaExpurgoAdministracao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaExpurgoAdministracaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaExpurgoAdministracao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaExpurgoAdministracao : esTabelaExpurgoAdministracao
	{

				
		#region UpToTabelaTaxaAdministracaoByIdTabela - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_TabelaExpurgoAdministracao_TabelaTaxaAdministracao
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaAdministracao UpToTabelaTaxaAdministracaoByIdTabela
		{
			get
			{
				if(this._UpToTabelaTaxaAdministracaoByIdTabela == null
					&& IdTabela != null					)
				{
					this._UpToTabelaTaxaAdministracaoByIdTabela = new TabelaTaxaAdministracao();
					this._UpToTabelaTaxaAdministracaoByIdTabela.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTabelaTaxaAdministracaoByIdTabela", this._UpToTabelaTaxaAdministracaoByIdTabela);
					this._UpToTabelaTaxaAdministracaoByIdTabela.Query.Where(this._UpToTabelaTaxaAdministracaoByIdTabela.Query.IdTabela == this.IdTabela);
					this._UpToTabelaTaxaAdministracaoByIdTabela.Query.Load();
				}

				return this._UpToTabelaTaxaAdministracaoByIdTabela;
			}
			
			set
			{
				this.RemovePreSave("UpToTabelaTaxaAdministracaoByIdTabela");
				

				if(value == null)
				{
					this.IdTabela = null;
					this._UpToTabelaTaxaAdministracaoByIdTabela = null;
				}
				else
				{
					this.IdTabela = value.IdTabela;
					this._UpToTabelaTaxaAdministracaoByIdTabela = value;
					this.SetPreSave("UpToTabelaTaxaAdministracaoByIdTabela", this._UpToTabelaTaxaAdministracaoByIdTabela);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTabelaTaxaAdministracaoByIdTabela != null)
			{
				this.IdTabela = this._UpToTabelaTaxaAdministracaoByIdTabela.IdTabela;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaExpurgoAdministracaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaExpurgoAdministracaoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaExpurgoAdministracaoMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, TabelaExpurgoAdministracaoMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, TabelaExpurgoAdministracaoMetadata.ColumnNames.Tipo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Codigo
		{
			get
			{
				return new esQueryItem(this, TabelaExpurgoAdministracaoMetadata.ColumnNames.Codigo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaExpurgoAdministracaoCollection")]
	public partial class TabelaExpurgoAdministracaoCollection : esTabelaExpurgoAdministracaoCollection, IEnumerable<TabelaExpurgoAdministracao>
	{
		public TabelaExpurgoAdministracaoCollection()
		{

		}
		
		public static implicit operator List<TabelaExpurgoAdministracao>(TabelaExpurgoAdministracaoCollection coll)
		{
			List<TabelaExpurgoAdministracao> list = new List<TabelaExpurgoAdministracao>();
			
			foreach (TabelaExpurgoAdministracao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaExpurgoAdministracaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaExpurgoAdministracaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaExpurgoAdministracao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaExpurgoAdministracao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaExpurgoAdministracaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaExpurgoAdministracaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaExpurgoAdministracaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaExpurgoAdministracao AddNew()
		{
			TabelaExpurgoAdministracao entity = base.AddNewEntity() as TabelaExpurgoAdministracao;
			
			return entity;
		}

		public TabelaExpurgoAdministracao FindByPrimaryKey(System.DateTime dataReferencia, System.Int32 idTabela, System.Int32 tipo, System.String codigo)
		{
			return base.FindByPrimaryKey(dataReferencia, idTabela, tipo, codigo) as TabelaExpurgoAdministracao;
		}


		#region IEnumerable<TabelaExpurgoAdministracao> Members

		IEnumerator<TabelaExpurgoAdministracao> IEnumerable<TabelaExpurgoAdministracao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaExpurgoAdministracao;
			}
		}

		#endregion
		
		private TabelaExpurgoAdministracaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaExpurgoAdministracao' table
	/// </summary>

	[Serializable]
	public partial class TabelaExpurgoAdministracao : esTabelaExpurgoAdministracao
	{
		public TabelaExpurgoAdministracao()
		{

		}
	
		public TabelaExpurgoAdministracao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaExpurgoAdministracaoMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaExpurgoAdministracaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaExpurgoAdministracaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaExpurgoAdministracaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaExpurgoAdministracaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaExpurgoAdministracaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaExpurgoAdministracaoQuery query;
	}



	[Serializable]
	public partial class TabelaExpurgoAdministracaoQuery : esTabelaExpurgoAdministracaoQuery
	{
		public TabelaExpurgoAdministracaoQuery()
		{

		}		
		
		public TabelaExpurgoAdministracaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaExpurgoAdministracaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaExpurgoAdministracaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaExpurgoAdministracaoMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaExpurgoAdministracaoMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaExpurgoAdministracaoMetadata.ColumnNames.IdTabela, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaExpurgoAdministracaoMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaExpurgoAdministracaoMetadata.ColumnNames.Tipo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaExpurgoAdministracaoMetadata.PropertyNames.Tipo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaExpurgoAdministracaoMetadata.ColumnNames.Codigo, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaExpurgoAdministracaoMetadata.PropertyNames.Codigo;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 14;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaExpurgoAdministracaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdTabela = "IdTabela";
			 public const string Tipo = "Tipo";
			 public const string Codigo = "Codigo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdTabela = "IdTabela";
			 public const string Tipo = "Tipo";
			 public const string Codigo = "Codigo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaExpurgoAdministracaoMetadata))
			{
				if(TabelaExpurgoAdministracaoMetadata.mapDelegates == null)
				{
					TabelaExpurgoAdministracaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaExpurgoAdministracaoMetadata.meta == null)
				{
					TabelaExpurgoAdministracaoMetadata.meta = new TabelaExpurgoAdministracaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Tipo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Codigo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "TabelaExpurgoAdministracao";
				meta.Destination = "TabelaExpurgoAdministracao";
				
				meta.spInsert = "proc_TabelaExpurgoAdministracaoInsert";				
				meta.spUpdate = "proc_TabelaExpurgoAdministracaoUpdate";		
				meta.spDelete = "proc_TabelaExpurgoAdministracaoDelete";
				meta.spLoadAll = "proc_TabelaExpurgoAdministracaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaExpurgoAdministracaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaExpurgoAdministracaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
