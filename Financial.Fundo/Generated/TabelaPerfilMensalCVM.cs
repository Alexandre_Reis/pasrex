/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 10/22/2015 3:20:57 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	












		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTabelaPerfilMensalCVMCollection : esEntityCollection
	{
		public esTabelaPerfilMensalCVMCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaPerfilMensalCVMCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaPerfilMensalCVMQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaPerfilMensalCVMQuery);
		}
		#endregion
		
		virtual public TabelaPerfilMensalCVM DetachEntity(TabelaPerfilMensalCVM entity)
		{
			return base.DetachEntity(entity) as TabelaPerfilMensalCVM;
		}
		
		virtual public TabelaPerfilMensalCVM AttachEntity(TabelaPerfilMensalCVM entity)
		{
			return base.AttachEntity(entity) as TabelaPerfilMensalCVM;
		}
		
		virtual public void Combine(TabelaPerfilMensalCVMCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaPerfilMensalCVM this[int index]
		{
			get
			{
				return base[index] as TabelaPerfilMensalCVM;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaPerfilMensalCVM);
		}
	}



	[Serializable]
	abstract public class esTabelaPerfilMensalCVM : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaPerfilMensalCVMQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaPerfilMensalCVM()
		{

		}

		public esTabelaPerfilMensalCVM(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int32 idCarteira)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idCarteira);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime data, System.Int32 idCarteira)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaPerfilMensalCVMQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Data == data, query.IdCarteira == idCarteira);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int32 idCarteira)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idCarteira);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int32 idCarteira)
		{
			esTabelaPerfilMensalCVMQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.IdCarteira == idCarteira);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int32 idCarteira)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("IdCarteira",idCarteira);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "Secao3": this.str.Secao3 = (string)value; break;							
						case "Secao4": this.str.Secao4 = (string)value; break;							
						case "Secao5": this.str.Secao5 = (string)value; break;							
						case "Secao6": this.str.Secao6 = (string)value; break;							
						case "Secao7": this.str.Secao7 = (string)value; break;							
						case "Secao8": this.str.Secao8 = (string)value; break;							
						case "Secao11PercentCota": this.str.Secao11PercentCota = (string)value; break;							
						case "Secao11Fator1": this.str.Secao11Fator1 = (string)value; break;							
						case "Secao11Cenario1": this.str.Secao11Cenario1 = (string)value; break;							
						case "Secao11Fator2": this.str.Secao11Fator2 = (string)value; break;							
						case "Secao11Cenario2": this.str.Secao11Cenario2 = (string)value; break;							
						case "Secao11Fator3": this.str.Secao11Fator3 = (string)value; break;							
						case "Secao11Cenario3": this.str.Secao11Cenario3 = (string)value; break;							
						case "Secao11Fator4": this.str.Secao11Fator4 = (string)value; break;							
						case "Secao11Cenario4": this.str.Secao11Cenario4 = (string)value; break;							
						case "Secao11Fator5": this.str.Secao11Fator5 = (string)value; break;							
						case "Secao11Cenario5": this.str.Secao11Cenario5 = (string)value; break;							
						case "Secao12": this.str.Secao12 = (string)value; break;							
						case "Secao13": this.str.Secao13 = (string)value; break;							
						case "Secao14": this.str.Secao14 = (string)value; break;							
						case "Secao15": this.str.Secao15 = (string)value; break;							
						case "Secao16Fator": this.str.Secao16Fator = (string)value; break;							
						case "Secao16Variacao": this.str.Secao16Variacao = (string)value; break;							
						case "Secao18CnpjComitente1": this.str.Secao18CnpjComitente1 = (string)value; break;							
						case "Secao18CnpjComitente2": this.str.Secao18CnpjComitente2 = (string)value; break;							
						case "Secao18CnpjComitente3": this.str.Secao18CnpjComitente3 = (string)value; break;							
						case "Secao18ParteRelacionada1": this.str.Secao18ParteRelacionada1 = (string)value; break;							
						case "Secao18ParteRelacionada2": this.str.Secao18ParteRelacionada2 = (string)value; break;							
						case "Secao18ParteRelacionada3": this.str.Secao18ParteRelacionada3 = (string)value; break;							
						case "Secao18ValorTotal1": this.str.Secao18ValorTotal1 = (string)value; break;							
						case "Secao18ValorTotal2": this.str.Secao18ValorTotal2 = (string)value; break;							
						case "Secao18ValorTotal3": this.str.Secao18ValorTotal3 = (string)value; break;							
						case "Secao18VedadaCobrancaTaxa": this.str.Secao18VedadaCobrancaTaxa = (string)value; break;							
						case "Secao18DataUltimaCobranca": this.str.Secao18DataUltimaCobranca = (string)value; break;							
						case "Secao18ValorUltimaCota": this.str.Secao18ValorUltimaCota = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "Secao5":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Secao5 = (System.Decimal?)value;
							break;
						
						case "Secao6":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Secao6 = (System.Byte?)value;
							break;
						
						case "Secao7":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Secao7 = (System.Decimal?)value;
							break;
						
						case "Secao11PercentCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Secao11PercentCota = (System.Decimal?)value;
							break;
						
						case "Secao12":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Secao12 = (System.Decimal?)value;
							break;
						
						case "Secao13":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Secao13 = (System.Decimal?)value;
							break;
						
						case "Secao14":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Secao14 = (System.Decimal?)value;
							break;
						
						case "Secao15":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Secao15 = (System.Decimal?)value;
							break;
						
						case "Secao16Variacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Secao16Variacao = (System.Decimal?)value;
							break;
						
						case "Secao18ValorTotal1":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Secao18ValorTotal1 = (System.Decimal?)value;
							break;
						
						case "Secao18ValorTotal2":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Secao18ValorTotal2 = (System.Decimal?)value;
							break;
						
						case "Secao18ValorTotal3":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Secao18ValorTotal3 = (System.Decimal?)value;
							break;
						
						case "Secao18DataUltimaCobranca":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Secao18DataUltimaCobranca = (System.DateTime?)value;
							break;
						
						case "Secao18ValorUltimaCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Secao18ValorUltimaCota = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(TabelaPerfilMensalCVMMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaPerfilMensalCVMMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(TabelaPerfilMensalCVMMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaPerfilMensalCVMMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao3
		/// </summary>
		virtual public System.String Secao3
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao3);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao3, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao4
		/// </summary>
		virtual public System.String Secao4
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao4);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao4, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao5
		/// </summary>
		virtual public System.Decimal? Secao5
		{
			get
			{
				return base.GetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao5);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao5, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao6
		/// </summary>
		virtual public System.Byte? Secao6
		{
			get
			{
				return base.GetSystemByte(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao6);
			}
			
			set
			{
				base.SetSystemByte(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao6, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao7
		/// </summary>
		virtual public System.Decimal? Secao7
		{
			get
			{
				return base.GetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao7);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao7, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao8
		/// </summary>
		virtual public System.String Secao8
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao8);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao8, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao11_PercentCota
		/// </summary>
		virtual public System.Decimal? Secao11PercentCota
		{
			get
			{
				return base.GetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11PercentCota);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11PercentCota, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao11_Fator1
		/// </summary>
		virtual public System.String Secao11Fator1
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator1);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator1, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao11_Cenario1
		/// </summary>
		virtual public System.String Secao11Cenario1
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario1);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario1, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao11_Fator2
		/// </summary>
		virtual public System.String Secao11Fator2
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator2);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator2, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao11_Cenario2
		/// </summary>
		virtual public System.String Secao11Cenario2
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario2);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario2, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao11_Fator3
		/// </summary>
		virtual public System.String Secao11Fator3
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator3);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator3, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao11_Cenario3
		/// </summary>
		virtual public System.String Secao11Cenario3
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario3);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario3, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao11_Fator4
		/// </summary>
		virtual public System.String Secao11Fator4
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator4);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator4, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao11_Cenario4
		/// </summary>
		virtual public System.String Secao11Cenario4
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario4);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario4, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao11_Fator5
		/// </summary>
		virtual public System.String Secao11Fator5
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator5);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator5, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao11_Cenario5
		/// </summary>
		virtual public System.String Secao11Cenario5
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario5);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario5, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao12
		/// </summary>
		virtual public System.Decimal? Secao12
		{
			get
			{
				return base.GetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao12);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao12, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao13
		/// </summary>
		virtual public System.Decimal? Secao13
		{
			get
			{
				return base.GetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao13);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao13, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao14
		/// </summary>
		virtual public System.Decimal? Secao14
		{
			get
			{
				return base.GetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao14);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao14, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao15
		/// </summary>
		virtual public System.Decimal? Secao15
		{
			get
			{
				return base.GetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao15);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao15, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao16_Fator
		/// </summary>
		virtual public System.String Secao16Fator
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao16Fator);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao16Fator, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao16_Variacao
		/// </summary>
		virtual public System.Decimal? Secao16Variacao
		{
			get
			{
				return base.GetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao16Variacao);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao16Variacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao18_CnpjComitente1
		/// </summary>
		virtual public System.String Secao18CnpjComitente1
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18CnpjComitente1);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18CnpjComitente1, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao18_CnpjComitente2
		/// </summary>
		virtual public System.String Secao18CnpjComitente2
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18CnpjComitente2);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18CnpjComitente2, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao18_CnpjComitente3
		/// </summary>
		virtual public System.String Secao18CnpjComitente3
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18CnpjComitente3);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18CnpjComitente3, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao18_ParteRelacionada1
		/// </summary>
		virtual public System.String Secao18ParteRelacionada1
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ParteRelacionada1);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ParteRelacionada1, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao18_ParteRelacionada2
		/// </summary>
		virtual public System.String Secao18ParteRelacionada2
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ParteRelacionada2);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ParteRelacionada2, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao18_ParteRelacionada3
		/// </summary>
		virtual public System.String Secao18ParteRelacionada3
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ParteRelacionada3);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ParteRelacionada3, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao18_ValorTotal1
		/// </summary>
		virtual public System.Decimal? Secao18ValorTotal1
		{
			get
			{
				return base.GetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorTotal1);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorTotal1, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao18_ValorTotal2
		/// </summary>
		virtual public System.Decimal? Secao18ValorTotal2
		{
			get
			{
				return base.GetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorTotal2);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorTotal2, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao18_ValorTotal3
		/// </summary>
		virtual public System.Decimal? Secao18ValorTotal3
		{
			get
			{
				return base.GetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorTotal3);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorTotal3, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao18_VedadaCobrancaTaxa
		/// </summary>
		virtual public System.String Secao18VedadaCobrancaTaxa
		{
			get
			{
				return base.GetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18VedadaCobrancaTaxa);
			}
			
			set
			{
				base.SetSystemString(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18VedadaCobrancaTaxa, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao18_DataUltimaCobranca
		/// </summary>
		virtual public System.DateTime? Secao18DataUltimaCobranca
		{
			get
			{
				return base.GetSystemDateTime(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18DataUltimaCobranca);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18DataUltimaCobranca, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaPerfilMensalCVM.Secao18_ValorUltimaCota
		/// </summary>
		virtual public System.Decimal? Secao18ValorUltimaCota
		{
			get
			{
				return base.GetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorUltimaCota);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorUltimaCota, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaPerfilMensalCVM entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String Secao3
			{
				get
				{
					System.String data = entity.Secao3;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao3 = null;
					else entity.Secao3 = Convert.ToString(value);
				}
			}
				
			public System.String Secao4
			{
				get
				{
					System.String data = entity.Secao4;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao4 = null;
					else entity.Secao4 = Convert.ToString(value);
				}
			}
				
			public System.String Secao5
			{
				get
				{
					System.Decimal? data = entity.Secao5;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao5 = null;
					else entity.Secao5 = Convert.ToDecimal(value);
				}
			}
				
			public System.String Secao6
			{
				get
				{
					System.Byte? data = entity.Secao6;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao6 = null;
					else entity.Secao6 = Convert.ToByte(value);
				}
			}
				
			public System.String Secao7
			{
				get
				{
					System.Decimal? data = entity.Secao7;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao7 = null;
					else entity.Secao7 = Convert.ToDecimal(value);
				}
			}
				
			public System.String Secao8
			{
				get
				{
					System.String data = entity.Secao8;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao8 = null;
					else entity.Secao8 = Convert.ToString(value);
				}
			}
				
			public System.String Secao11PercentCota
			{
				get
				{
					System.Decimal? data = entity.Secao11PercentCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao11PercentCota = null;
					else entity.Secao11PercentCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String Secao11Fator1
			{
				get
				{
					System.String data = entity.Secao11Fator1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao11Fator1 = null;
					else entity.Secao11Fator1 = Convert.ToString(value);
				}
			}
				
			public System.String Secao11Cenario1
			{
				get
				{
					System.String data = entity.Secao11Cenario1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao11Cenario1 = null;
					else entity.Secao11Cenario1 = Convert.ToString(value);
				}
			}
				
			public System.String Secao11Fator2
			{
				get
				{
					System.String data = entity.Secao11Fator2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao11Fator2 = null;
					else entity.Secao11Fator2 = Convert.ToString(value);
				}
			}
				
			public System.String Secao11Cenario2
			{
				get
				{
					System.String data = entity.Secao11Cenario2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao11Cenario2 = null;
					else entity.Secao11Cenario2 = Convert.ToString(value);
				}
			}
				
			public System.String Secao11Fator3
			{
				get
				{
					System.String data = entity.Secao11Fator3;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao11Fator3 = null;
					else entity.Secao11Fator3 = Convert.ToString(value);
				}
			}
				
			public System.String Secao11Cenario3
			{
				get
				{
					System.String data = entity.Secao11Cenario3;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao11Cenario3 = null;
					else entity.Secao11Cenario3 = Convert.ToString(value);
				}
			}
				
			public System.String Secao11Fator4
			{
				get
				{
					System.String data = entity.Secao11Fator4;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao11Fator4 = null;
					else entity.Secao11Fator4 = Convert.ToString(value);
				}
			}
				
			public System.String Secao11Cenario4
			{
				get
				{
					System.String data = entity.Secao11Cenario4;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao11Cenario4 = null;
					else entity.Secao11Cenario4 = Convert.ToString(value);
				}
			}
				
			public System.String Secao11Fator5
			{
				get
				{
					System.String data = entity.Secao11Fator5;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao11Fator5 = null;
					else entity.Secao11Fator5 = Convert.ToString(value);
				}
			}
				
			public System.String Secao11Cenario5
			{
				get
				{
					System.String data = entity.Secao11Cenario5;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao11Cenario5 = null;
					else entity.Secao11Cenario5 = Convert.ToString(value);
				}
			}
				
			public System.String Secao12
			{
				get
				{
					System.Decimal? data = entity.Secao12;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao12 = null;
					else entity.Secao12 = Convert.ToDecimal(value);
				}
			}
				
			public System.String Secao13
			{
				get
				{
					System.Decimal? data = entity.Secao13;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao13 = null;
					else entity.Secao13 = Convert.ToDecimal(value);
				}
			}
				
			public System.String Secao14
			{
				get
				{
					System.Decimal? data = entity.Secao14;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao14 = null;
					else entity.Secao14 = Convert.ToDecimal(value);
				}
			}
				
			public System.String Secao15
			{
				get
				{
					System.Decimal? data = entity.Secao15;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao15 = null;
					else entity.Secao15 = Convert.ToDecimal(value);
				}
			}
				
			public System.String Secao16Fator
			{
				get
				{
					System.String data = entity.Secao16Fator;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao16Fator = null;
					else entity.Secao16Fator = Convert.ToString(value);
				}
			}
				
			public System.String Secao16Variacao
			{
				get
				{
					System.Decimal? data = entity.Secao16Variacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao16Variacao = null;
					else entity.Secao16Variacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String Secao18CnpjComitente1
			{
				get
				{
					System.String data = entity.Secao18CnpjComitente1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao18CnpjComitente1 = null;
					else entity.Secao18CnpjComitente1 = Convert.ToString(value);
				}
			}
				
			public System.String Secao18CnpjComitente2
			{
				get
				{
					System.String data = entity.Secao18CnpjComitente2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao18CnpjComitente2 = null;
					else entity.Secao18CnpjComitente2 = Convert.ToString(value);
				}
			}
				
			public System.String Secao18CnpjComitente3
			{
				get
				{
					System.String data = entity.Secao18CnpjComitente3;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao18CnpjComitente3 = null;
					else entity.Secao18CnpjComitente3 = Convert.ToString(value);
				}
			}
				
			public System.String Secao18ParteRelacionada1
			{
				get
				{
					System.String data = entity.Secao18ParteRelacionada1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao18ParteRelacionada1 = null;
					else entity.Secao18ParteRelacionada1 = Convert.ToString(value);
				}
			}
				
			public System.String Secao18ParteRelacionada2
			{
				get
				{
					System.String data = entity.Secao18ParteRelacionada2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao18ParteRelacionada2 = null;
					else entity.Secao18ParteRelacionada2 = Convert.ToString(value);
				}
			}
				
			public System.String Secao18ParteRelacionada3
			{
				get
				{
					System.String data = entity.Secao18ParteRelacionada3;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao18ParteRelacionada3 = null;
					else entity.Secao18ParteRelacionada3 = Convert.ToString(value);
				}
			}
				
			public System.String Secao18ValorTotal1
			{
				get
				{
					System.Decimal? data = entity.Secao18ValorTotal1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao18ValorTotal1 = null;
					else entity.Secao18ValorTotal1 = Convert.ToDecimal(value);
				}
			}
				
			public System.String Secao18ValorTotal2
			{
				get
				{
					System.Decimal? data = entity.Secao18ValorTotal2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao18ValorTotal2 = null;
					else entity.Secao18ValorTotal2 = Convert.ToDecimal(value);
				}
			}
				
			public System.String Secao18ValorTotal3
			{
				get
				{
					System.Decimal? data = entity.Secao18ValorTotal3;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao18ValorTotal3 = null;
					else entity.Secao18ValorTotal3 = Convert.ToDecimal(value);
				}
			}
				
			public System.String Secao18VedadaCobrancaTaxa
			{
				get
				{
					System.String data = entity.Secao18VedadaCobrancaTaxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao18VedadaCobrancaTaxa = null;
					else entity.Secao18VedadaCobrancaTaxa = Convert.ToString(value);
				}
			}
				
			public System.String Secao18DataUltimaCobranca
			{
				get
				{
					System.DateTime? data = entity.Secao18DataUltimaCobranca;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao18DataUltimaCobranca = null;
					else entity.Secao18DataUltimaCobranca = Convert.ToDateTime(value);
				}
			}
				
			public System.String Secao18ValorUltimaCota
			{
				get
				{
					System.Decimal? data = entity.Secao18ValorUltimaCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Secao18ValorUltimaCota = null;
					else entity.Secao18ValorUltimaCota = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaPerfilMensalCVM entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaPerfilMensalCVMQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaPerfilMensalCVM can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaPerfilMensalCVM : esTabelaPerfilMensalCVM
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_TabelaPerfilMensalCVM_Carteira
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaPerfilMensalCVMQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaPerfilMensalCVMMetadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Secao3
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao3, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao4
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao4, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao5
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao5, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Secao6
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao6, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Secao7
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao7, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Secao8
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao8, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao11PercentCota
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11PercentCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Secao11Fator1
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator1, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao11Cenario1
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario1, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao11Fator2
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator2, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao11Cenario2
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario2, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao11Fator3
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator3, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao11Cenario3
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario3, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao11Fator4
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator4, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao11Cenario4
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario4, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao11Fator5
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator5, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao11Cenario5
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario5, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao12
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao12, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Secao13
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao13, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Secao14
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao14, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Secao15
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao15, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Secao16Fator
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao16Fator, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao16Variacao
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao16Variacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Secao18CnpjComitente1
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18CnpjComitente1, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao18CnpjComitente2
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18CnpjComitente2, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao18CnpjComitente3
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18CnpjComitente3, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao18ParteRelacionada1
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ParteRelacionada1, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao18ParteRelacionada2
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ParteRelacionada2, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao18ParteRelacionada3
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ParteRelacionada3, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao18ValorTotal1
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorTotal1, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Secao18ValorTotal2
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorTotal2, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Secao18ValorTotal3
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorTotal3, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Secao18VedadaCobrancaTaxa
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18VedadaCobrancaTaxa, esSystemType.String);
			}
		} 
		
		public esQueryItem Secao18DataUltimaCobranca
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18DataUltimaCobranca, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Secao18ValorUltimaCota
		{
			get
			{
				return new esQueryItem(this, TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorUltimaCota, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaPerfilMensalCVMCollection")]
	public partial class TabelaPerfilMensalCVMCollection : esTabelaPerfilMensalCVMCollection, IEnumerable<TabelaPerfilMensalCVM>
	{
		public TabelaPerfilMensalCVMCollection()
		{

		}
		
		public static implicit operator List<TabelaPerfilMensalCVM>(TabelaPerfilMensalCVMCollection coll)
		{
			List<TabelaPerfilMensalCVM> list = new List<TabelaPerfilMensalCVM>();
			
			foreach (TabelaPerfilMensalCVM emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaPerfilMensalCVMMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaPerfilMensalCVMQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaPerfilMensalCVM(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaPerfilMensalCVM();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaPerfilMensalCVMQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaPerfilMensalCVMQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaPerfilMensalCVMQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaPerfilMensalCVM AddNew()
		{
			TabelaPerfilMensalCVM entity = base.AddNewEntity() as TabelaPerfilMensalCVM;
			
			return entity;
		}

		public TabelaPerfilMensalCVM FindByPrimaryKey(System.DateTime data, System.Int32 idCarteira)
		{
			return base.FindByPrimaryKey(data, idCarteira) as TabelaPerfilMensalCVM;
		}


		#region IEnumerable<TabelaPerfilMensalCVM> Members

		IEnumerator<TabelaPerfilMensalCVM> IEnumerable<TabelaPerfilMensalCVM>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaPerfilMensalCVM;
			}
		}

		#endregion
		
		private TabelaPerfilMensalCVMQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaPerfilMensalCVM' table
	/// </summary>

	[Serializable]
	public partial class TabelaPerfilMensalCVM : esTabelaPerfilMensalCVM
	{
		public TabelaPerfilMensalCVM()
		{

		}
	
		public TabelaPerfilMensalCVM(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaPerfilMensalCVMMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaPerfilMensalCVMQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaPerfilMensalCVMQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaPerfilMensalCVMQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaPerfilMensalCVMQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaPerfilMensalCVMQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaPerfilMensalCVMQuery query;
	}



	[Serializable]
	public partial class TabelaPerfilMensalCVMQuery : esTabelaPerfilMensalCVMQuery
	{
		public TabelaPerfilMensalCVMQuery()
		{

		}		
		
		public TabelaPerfilMensalCVMQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaPerfilMensalCVMMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaPerfilMensalCVMMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao3, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao3;
			c.CharacterMaxLength = 4000;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao4, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao4;
			c.CharacterMaxLength = 4000;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao5, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao5;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao6, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao6;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao7, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao7;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao8, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao8;
			c.CharacterMaxLength = 4000;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11PercentCota, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao11PercentCota;	
			c.NumericPrecision = 24;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator1, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao11Fator1;
			c.CharacterMaxLength = 150;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario1, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao11Cenario1;
			c.CharacterMaxLength = 150;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator2, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao11Fator2;
			c.CharacterMaxLength = 150;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario2, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao11Cenario2;
			c.CharacterMaxLength = 150;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator3, 13, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao11Fator3;
			c.CharacterMaxLength = 150;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario3, 14, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao11Cenario3;
			c.CharacterMaxLength = 150;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator4, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao11Fator4;
			c.CharacterMaxLength = 150;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario4, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao11Cenario4;
			c.CharacterMaxLength = 150;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Fator5, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao11Fator5;
			c.CharacterMaxLength = 150;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao11Cenario5, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao11Cenario5;
			c.CharacterMaxLength = 150;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao12, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao12;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao13, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao13;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao14, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao14;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao15, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao15;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao16Fator, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao16Fator;
			c.CharacterMaxLength = 150;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao16Variacao, 24, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao16Variacao;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18CnpjComitente1, 25, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao18CnpjComitente1;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18CnpjComitente2, 26, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao18CnpjComitente2;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18CnpjComitente3, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao18CnpjComitente3;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ParteRelacionada1, 28, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao18ParteRelacionada1;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ParteRelacionada2, 29, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao18ParteRelacionada2;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ParteRelacionada3, 30, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao18ParteRelacionada3;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorTotal1, 31, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao18ValorTotal1;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorTotal2, 32, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao18ValorTotal2;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorTotal3, 33, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao18ValorTotal3;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18VedadaCobrancaTaxa, 34, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao18VedadaCobrancaTaxa;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18DataUltimaCobranca, 35, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao18DataUltimaCobranca;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaPerfilMensalCVMMetadata.ColumnNames.Secao18ValorUltimaCota, 36, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaPerfilMensalCVMMetadata.PropertyNames.Secao18ValorUltimaCota;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaPerfilMensalCVMMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string IdCarteira = "IdCarteira";
			 public const string Secao3 = "Secao3";
			 public const string Secao4 = "Secao4";
			 public const string Secao5 = "Secao5";
			 public const string Secao6 = "Secao6";
			 public const string Secao7 = "Secao7";
			 public const string Secao8 = "Secao8";
			 public const string Secao11PercentCota = "Secao11_PercentCota";
			 public const string Secao11Fator1 = "Secao11_Fator1";
			 public const string Secao11Cenario1 = "Secao11_Cenario1";
			 public const string Secao11Fator2 = "Secao11_Fator2";
			 public const string Secao11Cenario2 = "Secao11_Cenario2";
			 public const string Secao11Fator3 = "Secao11_Fator3";
			 public const string Secao11Cenario3 = "Secao11_Cenario3";
			 public const string Secao11Fator4 = "Secao11_Fator4";
			 public const string Secao11Cenario4 = "Secao11_Cenario4";
			 public const string Secao11Fator5 = "Secao11_Fator5";
			 public const string Secao11Cenario5 = "Secao11_Cenario5";
			 public const string Secao12 = "Secao12";
			 public const string Secao13 = "Secao13";
			 public const string Secao14 = "Secao14";
			 public const string Secao15 = "Secao15";
			 public const string Secao16Fator = "Secao16_Fator";
			 public const string Secao16Variacao = "Secao16_Variacao";
			 public const string Secao18CnpjComitente1 = "Secao18_CnpjComitente1";
			 public const string Secao18CnpjComitente2 = "Secao18_CnpjComitente2";
			 public const string Secao18CnpjComitente3 = "Secao18_CnpjComitente3";
			 public const string Secao18ParteRelacionada1 = "Secao18_ParteRelacionada1";
			 public const string Secao18ParteRelacionada2 = "Secao18_ParteRelacionada2";
			 public const string Secao18ParteRelacionada3 = "Secao18_ParteRelacionada3";
			 public const string Secao18ValorTotal1 = "Secao18_ValorTotal1";
			 public const string Secao18ValorTotal2 = "Secao18_ValorTotal2";
			 public const string Secao18ValorTotal3 = "Secao18_ValorTotal3";
			 public const string Secao18VedadaCobrancaTaxa = "Secao18_VedadaCobrancaTaxa";
			 public const string Secao18DataUltimaCobranca = "Secao18_DataUltimaCobranca";
			 public const string Secao18ValorUltimaCota = "Secao18_ValorUltimaCota";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string IdCarteira = "IdCarteira";
			 public const string Secao3 = "Secao3";
			 public const string Secao4 = "Secao4";
			 public const string Secao5 = "Secao5";
			 public const string Secao6 = "Secao6";
			 public const string Secao7 = "Secao7";
			 public const string Secao8 = "Secao8";
			 public const string Secao11PercentCota = "Secao11PercentCota";
			 public const string Secao11Fator1 = "Secao11Fator1";
			 public const string Secao11Cenario1 = "Secao11Cenario1";
			 public const string Secao11Fator2 = "Secao11Fator2";
			 public const string Secao11Cenario2 = "Secao11Cenario2";
			 public const string Secao11Fator3 = "Secao11Fator3";
			 public const string Secao11Cenario3 = "Secao11Cenario3";
			 public const string Secao11Fator4 = "Secao11Fator4";
			 public const string Secao11Cenario4 = "Secao11Cenario4";
			 public const string Secao11Fator5 = "Secao11Fator5";
			 public const string Secao11Cenario5 = "Secao11Cenario5";
			 public const string Secao12 = "Secao12";
			 public const string Secao13 = "Secao13";
			 public const string Secao14 = "Secao14";
			 public const string Secao15 = "Secao15";
			 public const string Secao16Fator = "Secao16Fator";
			 public const string Secao16Variacao = "Secao16Variacao";
			 public const string Secao18CnpjComitente1 = "Secao18CnpjComitente1";
			 public const string Secao18CnpjComitente2 = "Secao18CnpjComitente2";
			 public const string Secao18CnpjComitente3 = "Secao18CnpjComitente3";
			 public const string Secao18ParteRelacionada1 = "Secao18ParteRelacionada1";
			 public const string Secao18ParteRelacionada2 = "Secao18ParteRelacionada2";
			 public const string Secao18ParteRelacionada3 = "Secao18ParteRelacionada3";
			 public const string Secao18ValorTotal1 = "Secao18ValorTotal1";
			 public const string Secao18ValorTotal2 = "Secao18ValorTotal2";
			 public const string Secao18ValorTotal3 = "Secao18ValorTotal3";
			 public const string Secao18VedadaCobrancaTaxa = "Secao18VedadaCobrancaTaxa";
			 public const string Secao18DataUltimaCobranca = "Secao18DataUltimaCobranca";
			 public const string Secao18ValorUltimaCota = "Secao18ValorUltimaCota";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaPerfilMensalCVMMetadata))
			{
				if(TabelaPerfilMensalCVMMetadata.mapDelegates == null)
				{
					TabelaPerfilMensalCVMMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaPerfilMensalCVMMetadata.meta == null)
				{
					TabelaPerfilMensalCVMMetadata.meta = new TabelaPerfilMensalCVMMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Secao3", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao4", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao5", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Secao6", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Secao7", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Secao8", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao11_PercentCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Secao11_Fator1", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao11_Cenario1", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao11_Fator2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao11_Cenario2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao11_Fator3", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao11_Cenario3", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao11_Fator4", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao11_Cenario4", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao11_Fator5", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao11_Cenario5", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao12", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Secao13", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Secao14", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Secao15", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Secao16_Fator", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao16_Variacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Secao18_CnpjComitente1", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao18_CnpjComitente2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao18_CnpjComitente3", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Secao18_ParteRelacionada1", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Secao18_ParteRelacionada2", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Secao18_ParteRelacionada3", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Secao18_ValorTotal1", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Secao18_ValorTotal2", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Secao18_ValorTotal3", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Secao18_VedadaCobrancaTaxa", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Secao18_DataUltimaCobranca", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Secao18_ValorUltimaCota", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaPerfilMensalCVM";
				meta.Destination = "TabelaPerfilMensalCVM";
				
				meta.spInsert = "proc_TabelaPerfilMensalCVMInsert";				
				meta.spUpdate = "proc_TabelaPerfilMensalCVMUpdate";		
				meta.spDelete = "proc_TabelaPerfilMensalCVMDelete";
				meta.spLoadAll = "proc_TabelaPerfilMensalCVMLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaPerfilMensalCVMLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaPerfilMensalCVMMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
