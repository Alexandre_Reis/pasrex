/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:18 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esHistoricoIncorporacaoPosicaoCollection : esEntityCollection
	{
		public esHistoricoIncorporacaoPosicaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "HistoricoIncorporacaoPosicaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esHistoricoIncorporacaoPosicaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esHistoricoIncorporacaoPosicaoQuery);
		}
		#endregion
		
		virtual public HistoricoIncorporacaoPosicao DetachEntity(HistoricoIncorporacaoPosicao entity)
		{
			return base.DetachEntity(entity) as HistoricoIncorporacaoPosicao;
		}
		
		virtual public HistoricoIncorporacaoPosicao AttachEntity(HistoricoIncorporacaoPosicao entity)
		{
			return base.AttachEntity(entity) as HistoricoIncorporacaoPosicao;
		}
		
		virtual public void Combine(HistoricoIncorporacaoPosicaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public HistoricoIncorporacaoPosicao this[int index]
		{
			get
			{
				return base[index] as HistoricoIncorporacaoPosicao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(HistoricoIncorporacaoPosicao);
		}
	}



	[Serializable]
	abstract public class esHistoricoIncorporacaoPosicao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esHistoricoIncorporacaoPosicaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esHistoricoIncorporacaoPosicao()
		{

		}

		public esHistoricoIncorporacaoPosicao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao, System.DateTime dataIncorporacao, System.Int32 idCarteiraOrigem, System.Int32 idCarteiraDestino, System.DateTime dataConversao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataIncorporacao, idCarteiraOrigem, idCarteiraDestino, dataConversao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataIncorporacao, idCarteiraOrigem, idCarteiraDestino, dataConversao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPosicao, System.DateTime dataIncorporacao, System.Int32 idCarteiraOrigem, System.Int32 idCarteiraDestino, System.DateTime dataConversao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esHistoricoIncorporacaoPosicaoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPosicao == idPosicao, query.DataIncorporacao == dataIncorporacao, query.IdCarteiraOrigem == idCarteiraOrigem, query.IdCarteiraDestino == idCarteiraDestino, query.DataConversao == dataConversao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao, System.DateTime dataIncorporacao, System.Int32 idCarteiraOrigem, System.Int32 idCarteiraDestino, System.DateTime dataConversao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataIncorporacao, idCarteiraOrigem, idCarteiraDestino, dataConversao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataIncorporacao, idCarteiraOrigem, idCarteiraDestino, dataConversao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao, System.DateTime dataIncorporacao, System.Int32 idCarteiraOrigem, System.Int32 idCarteiraDestino, System.DateTime dataConversao)
		{
			esHistoricoIncorporacaoPosicaoQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao, query.DataIncorporacao == dataIncorporacao, query.IdCarteiraOrigem == idCarteiraOrigem, query.IdCarteiraDestino == idCarteiraDestino, query.DataConversao == dataConversao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao, System.DateTime dataIncorporacao, System.Int32 idCarteiraOrigem, System.Int32 idCarteiraDestino, System.DateTime dataConversao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);			parms.Add("DataIncorporacao",dataIncorporacao);			parms.Add("IdCarteiraOrigem",idCarteiraOrigem);			parms.Add("IdCarteiraDestino",idCarteiraDestino);			parms.Add("DataConversao",dataConversao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "DataIncorporacao": this.str.DataIncorporacao = (string)value; break;							
						case "IdCarteiraOrigem": this.str.IdCarteiraOrigem = (string)value; break;							
						case "IdCarteiraDestino": this.str.IdCarteiraDestino = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "DataIncorporacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataIncorporacao = (System.DateTime?)value;
							break;
						
						case "IdCarteiraOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraOrigem = (System.Int32?)value;
							break;
						
						case "IdCarteiraDestino":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraDestino = (System.Int32?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to HistoricoIncorporacaoPosicao.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(HistoricoIncorporacaoPosicaoMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(HistoricoIncorporacaoPosicaoMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoIncorporacaoPosicao.DataIncorporacao
		/// </summary>
		virtual public System.DateTime? DataIncorporacao
		{
			get
			{
				return base.GetSystemDateTime(HistoricoIncorporacaoPosicaoMetadata.ColumnNames.DataIncorporacao);
			}
			
			set
			{
				base.SetSystemDateTime(HistoricoIncorporacaoPosicaoMetadata.ColumnNames.DataIncorporacao, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoIncorporacaoPosicao.IdCarteiraOrigem
		/// </summary>
		virtual public System.Int32? IdCarteiraOrigem
		{
			get
			{
				return base.GetSystemInt32(HistoricoIncorporacaoPosicaoMetadata.ColumnNames.IdCarteiraOrigem);
			}
			
			set
			{
				base.SetSystemInt32(HistoricoIncorporacaoPosicaoMetadata.ColumnNames.IdCarteiraOrigem, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoIncorporacaoPosicao.IdCarteiraDestino
		/// </summary>
		virtual public System.Int32? IdCarteiraDestino
		{
			get
			{
				return base.GetSystemInt32(HistoricoIncorporacaoPosicaoMetadata.ColumnNames.IdCarteiraDestino);
			}
			
			set
			{
				base.SetSystemInt32(HistoricoIncorporacaoPosicaoMetadata.ColumnNames.IdCarteiraDestino, value);
			}
		}
		
		/// <summary>
		/// Maps to HistoricoIncorporacaoPosicao.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(HistoricoIncorporacaoPosicaoMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(HistoricoIncorporacaoPosicaoMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esHistoricoIncorporacaoPosicao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataIncorporacao
			{
				get
				{
					System.DateTime? data = entity.DataIncorporacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataIncorporacao = null;
					else entity.DataIncorporacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteiraOrigem
			{
				get
				{
					System.Int32? data = entity.IdCarteiraOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraOrigem = null;
					else entity.IdCarteiraOrigem = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteiraDestino
			{
				get
				{
					System.Int32? data = entity.IdCarteiraDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraDestino = null;
					else entity.IdCarteiraDestino = Convert.ToInt32(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
			

			private esHistoricoIncorporacaoPosicao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esHistoricoIncorporacaoPosicaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esHistoricoIncorporacaoPosicao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class HistoricoIncorporacaoPosicao : esHistoricoIncorporacaoPosicao
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esHistoricoIncorporacaoPosicaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoIncorporacaoPosicaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, HistoricoIncorporacaoPosicaoMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataIncorporacao
		{
			get
			{
				return new esQueryItem(this, HistoricoIncorporacaoPosicaoMetadata.ColumnNames.DataIncorporacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteiraOrigem
		{
			get
			{
				return new esQueryItem(this, HistoricoIncorporacaoPosicaoMetadata.ColumnNames.IdCarteiraOrigem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteiraDestino
		{
			get
			{
				return new esQueryItem(this, HistoricoIncorporacaoPosicaoMetadata.ColumnNames.IdCarteiraDestino, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, HistoricoIncorporacaoPosicaoMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("HistoricoIncorporacaoPosicaoCollection")]
	public partial class HistoricoIncorporacaoPosicaoCollection : esHistoricoIncorporacaoPosicaoCollection, IEnumerable<HistoricoIncorporacaoPosicao>
	{
		public HistoricoIncorporacaoPosicaoCollection()
		{

		}
		
		public static implicit operator List<HistoricoIncorporacaoPosicao>(HistoricoIncorporacaoPosicaoCollection coll)
		{
			List<HistoricoIncorporacaoPosicao> list = new List<HistoricoIncorporacaoPosicao>();
			
			foreach (HistoricoIncorporacaoPosicao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  HistoricoIncorporacaoPosicaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HistoricoIncorporacaoPosicaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new HistoricoIncorporacaoPosicao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new HistoricoIncorporacaoPosicao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public HistoricoIncorporacaoPosicaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoIncorporacaoPosicaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(HistoricoIncorporacaoPosicaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public HistoricoIncorporacaoPosicao AddNew()
		{
			HistoricoIncorporacaoPosicao entity = base.AddNewEntity() as HistoricoIncorporacaoPosicao;
			
			return entity;
		}

		public HistoricoIncorporacaoPosicao FindByPrimaryKey(System.Int32 idPosicao, System.DateTime dataIncorporacao, System.Int32 idCarteiraOrigem, System.Int32 idCarteiraDestino, System.DateTime dataConversao)
		{
			return base.FindByPrimaryKey(idPosicao, dataIncorporacao, idCarteiraOrigem, idCarteiraDestino, dataConversao) as HistoricoIncorporacaoPosicao;
		}


		#region IEnumerable<HistoricoIncorporacaoPosicao> Members

		IEnumerator<HistoricoIncorporacaoPosicao> IEnumerable<HistoricoIncorporacaoPosicao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as HistoricoIncorporacaoPosicao;
			}
		}

		#endregion
		
		private HistoricoIncorporacaoPosicaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'HistoricoIncorporacaoPosicao' table
	/// </summary>

	[Serializable]
	public partial class HistoricoIncorporacaoPosicao : esHistoricoIncorporacaoPosicao
	{
		public HistoricoIncorporacaoPosicao()
		{

		}
	
		public HistoricoIncorporacaoPosicao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return HistoricoIncorporacaoPosicaoMetadata.Meta();
			}
		}
		
		
		
		override protected esHistoricoIncorporacaoPosicaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new HistoricoIncorporacaoPosicaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public HistoricoIncorporacaoPosicaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new HistoricoIncorporacaoPosicaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(HistoricoIncorporacaoPosicaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private HistoricoIncorporacaoPosicaoQuery query;
	}



	[Serializable]
	public partial class HistoricoIncorporacaoPosicaoQuery : esHistoricoIncorporacaoPosicaoQuery
	{
		public HistoricoIncorporacaoPosicaoQuery()
		{

		}		
		
		public HistoricoIncorporacaoPosicaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class HistoricoIncorporacaoPosicaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected HistoricoIncorporacaoPosicaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(HistoricoIncorporacaoPosicaoMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoIncorporacaoPosicaoMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoIncorporacaoPosicaoMetadata.ColumnNames.DataIncorporacao, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoIncorporacaoPosicaoMetadata.PropertyNames.DataIncorporacao;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoIncorporacaoPosicaoMetadata.ColumnNames.IdCarteiraOrigem, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoIncorporacaoPosicaoMetadata.PropertyNames.IdCarteiraOrigem;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoIncorporacaoPosicaoMetadata.ColumnNames.IdCarteiraDestino, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = HistoricoIncorporacaoPosicaoMetadata.PropertyNames.IdCarteiraDestino;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(HistoricoIncorporacaoPosicaoMetadata.ColumnNames.DataConversao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = HistoricoIncorporacaoPosicaoMetadata.PropertyNames.DataConversao;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public HistoricoIncorporacaoPosicaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataIncorporacao = "DataIncorporacao";
			 public const string IdCarteiraOrigem = "IdCarteiraOrigem";
			 public const string IdCarteiraDestino = "IdCarteiraDestino";
			 public const string DataConversao = "DataConversao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataIncorporacao = "DataIncorporacao";
			 public const string IdCarteiraOrigem = "IdCarteiraOrigem";
			 public const string IdCarteiraDestino = "IdCarteiraDestino";
			 public const string DataConversao = "DataConversao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(HistoricoIncorporacaoPosicaoMetadata))
			{
				if(HistoricoIncorporacaoPosicaoMetadata.mapDelegates == null)
				{
					HistoricoIncorporacaoPosicaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (HistoricoIncorporacaoPosicaoMetadata.meta == null)
				{
					HistoricoIncorporacaoPosicaoMetadata.meta = new HistoricoIncorporacaoPosicaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataIncorporacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteiraOrigem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteiraDestino", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "HistoricoIncorporacaoPosicao";
				meta.Destination = "HistoricoIncorporacaoPosicao";
				
				meta.spInsert = "proc_HistoricoIncorporacaoPosicaoInsert";				
				meta.spUpdate = "proc_HistoricoIncorporacaoPosicaoUpdate";		
				meta.spDelete = "proc_HistoricoIncorporacaoPosicaoDelete";
				meta.spLoadAll = "proc_HistoricoIncorporacaoPosicaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_HistoricoIncorporacaoPosicaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private HistoricoIncorporacaoPosicaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
