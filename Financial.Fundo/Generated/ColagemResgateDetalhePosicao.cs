/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 19/01/2015 11:34:46
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esColagemResgateDetalhePosicaoCollection : esEntityCollection
	{
		public esColagemResgateDetalhePosicaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ColagemResgateDetalhePosicaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esColagemResgateDetalhePosicaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esColagemResgateDetalhePosicaoQuery);
		}
		#endregion
		
		virtual public ColagemResgateDetalhePosicao DetachEntity(ColagemResgateDetalhePosicao entity)
		{
			return base.DetachEntity(entity) as ColagemResgateDetalhePosicao;
		}
		
		virtual public ColagemResgateDetalhePosicao AttachEntity(ColagemResgateDetalhePosicao entity)
		{
			return base.AttachEntity(entity) as ColagemResgateDetalhePosicao;
		}
		
		virtual public void Combine(ColagemResgateDetalhePosicaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ColagemResgateDetalhePosicao this[int index]
		{
			get
			{
				return base[index] as ColagemResgateDetalhePosicao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ColagemResgateDetalhePosicao);
		}
	}



	[Serializable]
	abstract public class esColagemResgateDetalhePosicao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esColagemResgateDetalhePosicaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esColagemResgateDetalhePosicao()
		{

		}

		public esColagemResgateDetalhePosicao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idColagemResgateDetalhePosicao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemResgateDetalhePosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemResgateDetalhePosicao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idColagemResgateDetalhePosicao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idColagemResgateDetalhePosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idColagemResgateDetalhePosicao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idColagemResgateDetalhePosicao)
		{
			esColagemResgateDetalhePosicaoQuery query = this.GetDynamicQuery();
			query.Where(query.IdColagemResgateDetalhePosicao == idColagemResgateDetalhePosicao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idColagemResgateDetalhePosicao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdColagemResgateDetalhePosicao",idColagemResgateDetalhePosicao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdColagemResgateDetalhePosicao": this.str.IdColagemResgateDetalhePosicao = (string)value; break;							
						case "IdColagemResgateDetalhe": this.str.IdColagemResgateDetalhe = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "QuantidadeInicial": this.str.QuantidadeInicial = (string)value; break;							
						case "QuantidadeRestante": this.str.QuantidadeRestante = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdColagemResgateDetalhePosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemResgateDetalhePosicao = (System.Int32?)value;
							break;
						
						case "IdColagemResgateDetalhe":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdColagemResgateDetalhe = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "QuantidadeInicial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeInicial = (System.Decimal?)value;
							break;
						
						case "QuantidadeRestante":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeRestante = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ColagemResgateDetalhePosicao.IdColagemResgateDetalhePosicao
		/// </summary>
		virtual public System.Int32? IdColagemResgateDetalhePosicao
		{
			get
			{
				return base.GetSystemInt32(ColagemResgateDetalhePosicaoMetadata.ColumnNames.IdColagemResgateDetalhePosicao);
			}
			
			set
			{
				base.SetSystemInt32(ColagemResgateDetalhePosicaoMetadata.ColumnNames.IdColagemResgateDetalhePosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhePosicao.IdColagemResgateDetalhe
		/// </summary>
		virtual public System.Int32? IdColagemResgateDetalhe
		{
			get
			{
				return base.GetSystemInt32(ColagemResgateDetalhePosicaoMetadata.ColumnNames.IdColagemResgateDetalhe);
			}
			
			set
			{
				if(base.SetSystemInt32(ColagemResgateDetalhePosicaoMetadata.ColumnNames.IdColagemResgateDetalhe, value))
				{
					this._UpToColagemResgateDetalheByIdColagemResgateDetalhe = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhePosicao.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(ColagemResgateDetalhePosicaoMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(ColagemResgateDetalhePosicaoMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhePosicao.QuantidadeInicial
		/// </summary>
		virtual public System.Decimal? QuantidadeInicial
		{
			get
			{
				return base.GetSystemDecimal(ColagemResgateDetalhePosicaoMetadata.ColumnNames.QuantidadeInicial);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemResgateDetalhePosicaoMetadata.ColumnNames.QuantidadeInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to ColagemResgateDetalhePosicao.QuantidadeRestante
		/// </summary>
		virtual public System.Decimal? QuantidadeRestante
		{
			get
			{
				return base.GetSystemDecimal(ColagemResgateDetalhePosicaoMetadata.ColumnNames.QuantidadeRestante);
			}
			
			set
			{
				base.SetSystemDecimal(ColagemResgateDetalhePosicaoMetadata.ColumnNames.QuantidadeRestante, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected ColagemResgateDetalhe _UpToColagemResgateDetalheByIdColagemResgateDetalhe;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esColagemResgateDetalhePosicao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdColagemResgateDetalhePosicao
			{
				get
				{
					System.Int32? data = entity.IdColagemResgateDetalhePosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemResgateDetalhePosicao = null;
					else entity.IdColagemResgateDetalhePosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdColagemResgateDetalhe
			{
				get
				{
					System.Int32? data = entity.IdColagemResgateDetalhe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdColagemResgateDetalhe = null;
					else entity.IdColagemResgateDetalhe = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String QuantidadeInicial
			{
				get
				{
					System.Decimal? data = entity.QuantidadeInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeInicial = null;
					else entity.QuantidadeInicial = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeRestante
			{
				get
				{
					System.Decimal? data = entity.QuantidadeRestante;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeRestante = null;
					else entity.QuantidadeRestante = Convert.ToDecimal(value);
				}
			}
			

			private esColagemResgateDetalhePosicao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esColagemResgateDetalhePosicaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esColagemResgateDetalhePosicao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ColagemResgateDetalhePosicao : esColagemResgateDetalhePosicao
	{

				
		#region UpToColagemResgateDetalheByIdColagemResgateDetalhe - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ColagemResgates_Det_Pos_FK
		/// </summary>

		[XmlIgnore]
		public ColagemResgateDetalhe UpToColagemResgateDetalheByIdColagemResgateDetalhe
		{
			get
			{
				if(this._UpToColagemResgateDetalheByIdColagemResgateDetalhe == null
					&& IdColagemResgateDetalhe != null					)
				{
					this._UpToColagemResgateDetalheByIdColagemResgateDetalhe = new ColagemResgateDetalhe();
					this._UpToColagemResgateDetalheByIdColagemResgateDetalhe.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToColagemResgateDetalheByIdColagemResgateDetalhe", this._UpToColagemResgateDetalheByIdColagemResgateDetalhe);
					this._UpToColagemResgateDetalheByIdColagemResgateDetalhe.Query.Where(this._UpToColagemResgateDetalheByIdColagemResgateDetalhe.Query.IdColagemResgateDetalhe == this.IdColagemResgateDetalhe);
					this._UpToColagemResgateDetalheByIdColagemResgateDetalhe.Query.Load();
				}

				return this._UpToColagemResgateDetalheByIdColagemResgateDetalhe;
			}
			
			set
			{
				this.RemovePreSave("UpToColagemResgateDetalheByIdColagemResgateDetalhe");
				

				if(value == null)
				{
					this.IdColagemResgateDetalhe = null;
					this._UpToColagemResgateDetalheByIdColagemResgateDetalhe = null;
				}
				else
				{
					this.IdColagemResgateDetalhe = value.IdColagemResgateDetalhe;
					this._UpToColagemResgateDetalheByIdColagemResgateDetalhe = value;
					this.SetPreSave("UpToColagemResgateDetalheByIdColagemResgateDetalhe", this._UpToColagemResgateDetalheByIdColagemResgateDetalhe);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToColagemResgateDetalheByIdColagemResgateDetalhe != null)
			{
				this.IdColagemResgateDetalhe = this._UpToColagemResgateDetalheByIdColagemResgateDetalhe.IdColagemResgateDetalhe;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esColagemResgateDetalhePosicaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ColagemResgateDetalhePosicaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdColagemResgateDetalhePosicao
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalhePosicaoMetadata.ColumnNames.IdColagemResgateDetalhePosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdColagemResgateDetalhe
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalhePosicaoMetadata.ColumnNames.IdColagemResgateDetalhe, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalhePosicaoMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem QuantidadeInicial
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalhePosicaoMetadata.ColumnNames.QuantidadeInicial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeRestante
		{
			get
			{
				return new esQueryItem(this, ColagemResgateDetalhePosicaoMetadata.ColumnNames.QuantidadeRestante, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ColagemResgateDetalhePosicaoCollection")]
	public partial class ColagemResgateDetalhePosicaoCollection : esColagemResgateDetalhePosicaoCollection, IEnumerable<ColagemResgateDetalhePosicao>
	{
		public ColagemResgateDetalhePosicaoCollection()
		{

		}
		
		public static implicit operator List<ColagemResgateDetalhePosicao>(ColagemResgateDetalhePosicaoCollection coll)
		{
			List<ColagemResgateDetalhePosicao> list = new List<ColagemResgateDetalhePosicao>();
			
			foreach (ColagemResgateDetalhePosicao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ColagemResgateDetalhePosicaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemResgateDetalhePosicaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ColagemResgateDetalhePosicao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ColagemResgateDetalhePosicao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ColagemResgateDetalhePosicaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemResgateDetalhePosicaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ColagemResgateDetalhePosicaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ColagemResgateDetalhePosicao AddNew()
		{
			ColagemResgateDetalhePosicao entity = base.AddNewEntity() as ColagemResgateDetalhePosicao;
			
			return entity;
		}

		public ColagemResgateDetalhePosicao FindByPrimaryKey(System.Int32 idColagemResgateDetalhePosicao)
		{
			return base.FindByPrimaryKey(idColagemResgateDetalhePosicao) as ColagemResgateDetalhePosicao;
		}


		#region IEnumerable<ColagemResgateDetalhePosicao> Members

		IEnumerator<ColagemResgateDetalhePosicao> IEnumerable<ColagemResgateDetalhePosicao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ColagemResgateDetalhePosicao;
			}
		}

		#endregion
		
		private ColagemResgateDetalhePosicaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ColagemResgateDetalhePosicao' table
	/// </summary>

	[Serializable]
	public partial class ColagemResgateDetalhePosicao : esColagemResgateDetalhePosicao
	{
		public ColagemResgateDetalhePosicao()
		{

		}
	
		public ColagemResgateDetalhePosicao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ColagemResgateDetalhePosicaoMetadata.Meta();
			}
		}
		
		
		
		override protected esColagemResgateDetalhePosicaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ColagemResgateDetalhePosicaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ColagemResgateDetalhePosicaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ColagemResgateDetalhePosicaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ColagemResgateDetalhePosicaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ColagemResgateDetalhePosicaoQuery query;
	}



	[Serializable]
	public partial class ColagemResgateDetalhePosicaoQuery : esColagemResgateDetalhePosicaoQuery
	{
		public ColagemResgateDetalhePosicaoQuery()
		{

		}		
		
		public ColagemResgateDetalhePosicaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ColagemResgateDetalhePosicaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ColagemResgateDetalhePosicaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ColagemResgateDetalhePosicaoMetadata.ColumnNames.IdColagemResgateDetalhePosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemResgateDetalhePosicaoMetadata.PropertyNames.IdColagemResgateDetalhePosicao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalhePosicaoMetadata.ColumnNames.IdColagemResgateDetalhe, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ColagemResgateDetalhePosicaoMetadata.PropertyNames.IdColagemResgateDetalhe;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalhePosicaoMetadata.ColumnNames.DataReferencia, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ColagemResgateDetalhePosicaoMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalhePosicaoMetadata.ColumnNames.QuantidadeInicial, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemResgateDetalhePosicaoMetadata.PropertyNames.QuantidadeInicial;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ColagemResgateDetalhePosicaoMetadata.ColumnNames.QuantidadeRestante, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ColagemResgateDetalhePosicaoMetadata.PropertyNames.QuantidadeRestante;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ColagemResgateDetalhePosicaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdColagemResgateDetalhePosicao = "IdColagemResgateDetalhePosicao";
			 public const string IdColagemResgateDetalhe = "IdColagemResgateDetalhe";
			 public const string DataReferencia = "DataReferencia";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string QuantidadeRestante = "QuantidadeRestante";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdColagemResgateDetalhePosicao = "IdColagemResgateDetalhePosicao";
			 public const string IdColagemResgateDetalhe = "IdColagemResgateDetalhe";
			 public const string DataReferencia = "DataReferencia";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string QuantidadeRestante = "QuantidadeRestante";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ColagemResgateDetalhePosicaoMetadata))
			{
				if(ColagemResgateDetalhePosicaoMetadata.mapDelegates == null)
				{
					ColagemResgateDetalhePosicaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ColagemResgateDetalhePosicaoMetadata.meta == null)
				{
					ColagemResgateDetalhePosicaoMetadata.meta = new ColagemResgateDetalhePosicaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdColagemResgateDetalhePosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdColagemResgateDetalhe", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("QuantidadeInicial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeRestante", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "ColagemResgateDetalhePosicao";
				meta.Destination = "ColagemResgateDetalhePosicao";
				
				meta.spInsert = "proc_ColagemResgateDetalhePosicaoInsert";				
				meta.spUpdate = "proc_ColagemResgateDetalhePosicaoUpdate";		
				meta.spDelete = "proc_ColagemResgateDetalhePosicaoDelete";
				meta.spLoadAll = "proc_ColagemResgateDetalhePosicaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_ColagemResgateDetalhePosicaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ColagemResgateDetalhePosicaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
