/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 28/03/2016 17:28:11
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		









using Financial.Investidor;
		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esPosicaoFundoHistoricoCollection : esEntityCollection
	{
		public esPosicaoFundoHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoFundoHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoFundoHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoFundoHistoricoQuery);
		}
		#endregion
		
		virtual public PosicaoFundoHistorico DetachEntity(PosicaoFundoHistorico entity)
		{
			return base.DetachEntity(entity) as PosicaoFundoHistorico;
		}
		
		virtual public PosicaoFundoHistorico AttachEntity(PosicaoFundoHistorico entity)
		{
			return base.AttachEntity(entity) as PosicaoFundoHistorico;
		}
		
		virtual public void Combine(PosicaoFundoHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoFundoHistorico this[int index]
		{
			get
			{
				return base[index] as PosicaoFundoHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoFundoHistorico);
		}
	}



	[Serializable]
	abstract public class esPosicaoFundoHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoFundoHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoFundoHistorico()
		{

		}

		public esPosicaoFundoHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPosicaoFundoHistoricoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esPosicaoFundoHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);			parms.Add("DataHistorico",dataHistorico);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "ValorAplicacao": this.str.ValorAplicacao = (string)value; break;							
						case "DataAplicacao": this.str.DataAplicacao = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "CotaAplicacao": this.str.CotaAplicacao = (string)value; break;							
						case "CotaDia": this.str.CotaDia = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "QuantidadeInicial": this.str.QuantidadeInicial = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeBloqueada": this.str.QuantidadeBloqueada = (string)value; break;							
						case "DataUltimaCobrancaIR": this.str.DataUltimaCobrancaIR = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "ValorIOFVirtual": this.str.ValorIOFVirtual = (string)value; break;							
						case "QuantidadeAntesCortes": this.str.QuantidadeAntesCortes = (string)value; break;							
						case "ValorRendimento": this.str.ValorRendimento = (string)value; break;							
						case "DataUltimoCortePfee": this.str.DataUltimoCortePfee = (string)value; break;							
						case "PosicaoIncorporada": this.str.PosicaoIncorporada = (string)value; break;							
						case "FieTabelaIr": this.str.FieTabelaIr = (string)value; break;							
						case "QtdePendenteLiquidacao": this.str.QtdePendenteLiquidacao = (string)value; break;							
						case "ValorPendenteLiquidacao": this.str.ValorPendenteLiquidacao = (string)value; break;							
						case "AmortizacaoAcumuladaPorCota": this.str.AmortizacaoAcumuladaPorCota = (string)value; break;							
						case "JurosAcumuladoPorCota": this.str.JurosAcumuladoPorCota = (string)value; break;							
						case "AmortizacaoAcumuladaPorValor": this.str.AmortizacaoAcumuladaPorValor = (string)value; break;							
						case "JurosAcumuladoPorValor": this.str.JurosAcumuladoPorValor = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "ValorAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAplicacao = (System.Decimal?)value;
							break;
						
						case "DataAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAplicacao = (System.DateTime?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "CotaAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaAplicacao = (System.Decimal?)value;
							break;
						
						case "CotaDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaDia = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "QuantidadeInicial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeInicial = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "QuantidadeBloqueada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeBloqueada = (System.Decimal?)value;
							break;
						
						case "DataUltimaCobrancaIR":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimaCobrancaIR = (System.DateTime?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "ValorIOFVirtual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOFVirtual = (System.Decimal?)value;
							break;
						
						case "QuantidadeAntesCortes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeAntesCortes = (System.Decimal?)value;
							break;
						
						case "ValorRendimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorRendimento = (System.Decimal?)value;
							break;
						
						case "DataUltimoCortePfee":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimoCortePfee = (System.DateTime?)value;
							break;
						
						case "FieTabelaIr":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FieTabelaIr = (System.Int32?)value;
							break;
						
						case "QtdePendenteLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdePendenteLiquidacao = (System.Decimal?)value;
							break;
						
						case "ValorPendenteLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPendenteLiquidacao = (System.Decimal?)value;
							break;
						
						case "AmortizacaoAcumuladaPorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AmortizacaoAcumuladaPorCota = (System.Decimal?)value;
							break;
						
						case "JurosAcumuladoPorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.JurosAcumuladoPorCota = (System.Decimal?)value;
							break;
						
						case "AmortizacaoAcumuladaPorValor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AmortizacaoAcumuladaPorValor = (System.Decimal?)value;
							break;
						
						case "JurosAcumuladoPorValor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.JurosAcumuladoPorValor = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoHistoricoMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoFundoHistoricoMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(PosicaoFundoHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoFundoHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoHistoricoMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoFundoHistoricoMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoHistoricoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoFundoHistoricoMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoHistoricoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoFundoHistoricoMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.ValorAplicacao
		/// </summary>
		virtual public System.Decimal? ValorAplicacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorAplicacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.DataAplicacao
		/// </summary>
		virtual public System.DateTime? DataAplicacao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoFundoHistoricoMetadata.ColumnNames.DataAplicacao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoFundoHistoricoMetadata.ColumnNames.DataAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoFundoHistoricoMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoFundoHistoricoMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.CotaAplicacao
		/// </summary>
		virtual public System.Decimal? CotaAplicacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.CotaAplicacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.CotaAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.CotaDia
		/// </summary>
		virtual public System.Decimal? CotaDia
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.CotaDia);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.CotaDia, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.QuantidadeInicial
		/// </summary>
		virtual public System.Decimal? QuantidadeInicial
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.QuantidadeInicial);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.QuantidadeInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.QuantidadeBloqueada
		/// </summary>
		virtual public System.Decimal? QuantidadeBloqueada
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.QuantidadeBloqueada);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.QuantidadeBloqueada, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.DataUltimaCobrancaIR
		/// </summary>
		virtual public System.DateTime? DataUltimaCobrancaIR
		{
			get
			{
				return base.GetSystemDateTime(PosicaoFundoHistoricoMetadata.ColumnNames.DataUltimaCobrancaIR);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoFundoHistoricoMetadata.ColumnNames.DataUltimaCobrancaIR, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.ValorIOFVirtual
		/// </summary>
		virtual public System.Decimal? ValorIOFVirtual
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorIOFVirtual);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorIOFVirtual, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.QuantidadeAntesCortes
		/// </summary>
		virtual public System.Decimal? QuantidadeAntesCortes
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.QuantidadeAntesCortes);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.QuantidadeAntesCortes, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.ValorRendimento
		/// </summary>
		virtual public System.Decimal? ValorRendimento
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorRendimento);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorRendimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.DataUltimoCortePfee
		/// </summary>
		virtual public System.DateTime? DataUltimoCortePfee
		{
			get
			{
				return base.GetSystemDateTime(PosicaoFundoHistoricoMetadata.ColumnNames.DataUltimoCortePfee);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoFundoHistoricoMetadata.ColumnNames.DataUltimoCortePfee, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.PosicaoIncorporada
		/// </summary>
		virtual public System.String PosicaoIncorporada
		{
			get
			{
				return base.GetSystemString(PosicaoFundoHistoricoMetadata.ColumnNames.PosicaoIncorporada);
			}
			
			set
			{
				base.SetSystemString(PosicaoFundoHistoricoMetadata.ColumnNames.PosicaoIncorporada, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.FieTabelaIr
		/// </summary>
		virtual public System.Int32? FieTabelaIr
		{
			get
			{
				return base.GetSystemInt32(PosicaoFundoHistoricoMetadata.ColumnNames.FieTabelaIr);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoFundoHistoricoMetadata.ColumnNames.FieTabelaIr, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.QtdePendenteLiquidacao
		/// </summary>
		virtual public System.Decimal? QtdePendenteLiquidacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.QtdePendenteLiquidacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.QtdePendenteLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.ValorPendenteLiquidacao
		/// </summary>
		virtual public System.Decimal? ValorPendenteLiquidacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorPendenteLiquidacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.ValorPendenteLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.AmortizacaoAcumuladaPorCota
		/// </summary>
		virtual public System.Decimal? AmortizacaoAcumuladaPorCota
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.AmortizacaoAcumuladaPorCota);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.AmortizacaoAcumuladaPorCota, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.JurosAcumuladoPorCota
		/// </summary>
		virtual public System.Decimal? JurosAcumuladoPorCota
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.JurosAcumuladoPorCota);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.JurosAcumuladoPorCota, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.AmortizacaoAcumuladaPorValor
		/// </summary>
		virtual public System.Decimal? AmortizacaoAcumuladaPorValor
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.AmortizacaoAcumuladaPorValor);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.AmortizacaoAcumuladaPorValor, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoFundoHistorico.JurosAcumuladoPorValor
		/// </summary>
		virtual public System.Decimal? JurosAcumuladoPorValor
		{
			get
			{
				return base.GetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.JurosAcumuladoPorValor);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoFundoHistoricoMetadata.ColumnNames.JurosAcumuladoPorValor, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoFundoHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorAplicacao
			{
				get
				{
					System.Decimal? data = entity.ValorAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAplicacao = null;
					else entity.ValorAplicacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataAplicacao
			{
				get
				{
					System.DateTime? data = entity.DataAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAplicacao = null;
					else entity.DataAplicacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String CotaAplicacao
			{
				get
				{
					System.Decimal? data = entity.CotaAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaAplicacao = null;
					else entity.CotaAplicacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaDia
			{
				get
				{
					System.Decimal? data = entity.CotaDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaDia = null;
					else entity.CotaDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeInicial
			{
				get
				{
					System.Decimal? data = entity.QuantidadeInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeInicial = null;
					else entity.QuantidadeInicial = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeBloqueada
			{
				get
				{
					System.Decimal? data = entity.QuantidadeBloqueada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeBloqueada = null;
					else entity.QuantidadeBloqueada = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataUltimaCobrancaIR
			{
				get
				{
					System.DateTime? data = entity.DataUltimaCobrancaIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimaCobrancaIR = null;
					else entity.DataUltimaCobrancaIR = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOFVirtual
			{
				get
				{
					System.Decimal? data = entity.ValorIOFVirtual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOFVirtual = null;
					else entity.ValorIOFVirtual = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeAntesCortes
			{
				get
				{
					System.Decimal? data = entity.QuantidadeAntesCortes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeAntesCortes = null;
					else entity.QuantidadeAntesCortes = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorRendimento
			{
				get
				{
					System.Decimal? data = entity.ValorRendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorRendimento = null;
					else entity.ValorRendimento = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataUltimoCortePfee
			{
				get
				{
					System.DateTime? data = entity.DataUltimoCortePfee;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimoCortePfee = null;
					else entity.DataUltimoCortePfee = Convert.ToDateTime(value);
				}
			}
				
			public System.String PosicaoIncorporada
			{
				get
				{
					System.String data = entity.PosicaoIncorporada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PosicaoIncorporada = null;
					else entity.PosicaoIncorporada = Convert.ToString(value);
				}
			}
				
			public System.String FieTabelaIr
			{
				get
				{
					System.Int32? data = entity.FieTabelaIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FieTabelaIr = null;
					else entity.FieTabelaIr = Convert.ToInt32(value);
				}
			}
				
			public System.String QtdePendenteLiquidacao
			{
				get
				{
					System.Decimal? data = entity.QtdePendenteLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdePendenteLiquidacao = null;
					else entity.QtdePendenteLiquidacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPendenteLiquidacao
			{
				get
				{
					System.Decimal? data = entity.ValorPendenteLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPendenteLiquidacao = null;
					else entity.ValorPendenteLiquidacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String AmortizacaoAcumuladaPorCota
			{
				get
				{
					System.Decimal? data = entity.AmortizacaoAcumuladaPorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AmortizacaoAcumuladaPorCota = null;
					else entity.AmortizacaoAcumuladaPorCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String JurosAcumuladoPorCota
			{
				get
				{
					System.Decimal? data = entity.JurosAcumuladoPorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.JurosAcumuladoPorCota = null;
					else entity.JurosAcumuladoPorCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String AmortizacaoAcumuladaPorValor
			{
				get
				{
					System.Decimal? data = entity.AmortizacaoAcumuladaPorValor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AmortizacaoAcumuladaPorValor = null;
					else entity.AmortizacaoAcumuladaPorValor = Convert.ToDecimal(value);
				}
			}
				
			public System.String JurosAcumuladoPorValor
			{
				get
				{
					System.Decimal? data = entity.JurosAcumuladoPorValor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.JurosAcumuladoPorValor = null;
					else entity.JurosAcumuladoPorValor = Convert.ToDecimal(value);
				}
			}
			

			private esPosicaoFundoHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoFundoHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoFundoHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoFundoHistorico : esPosicaoFundoHistorico
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_PosicaoFundoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PosicaoFundoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoFundoHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoFundoHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorAplicacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.ValorAplicacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataAplicacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.DataAplicacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CotaAplicacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.CotaAplicacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaDia
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.CotaDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeInicial
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.QuantidadeInicial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeBloqueada
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.QuantidadeBloqueada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataUltimaCobrancaIR
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.DataUltimaCobrancaIR, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOFVirtual
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.ValorIOFVirtual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeAntesCortes
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.QuantidadeAntesCortes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorRendimento
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.ValorRendimento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataUltimoCortePfee
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.DataUltimoCortePfee, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PosicaoIncorporada
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.PosicaoIncorporada, esSystemType.String);
			}
		} 
		
		public esQueryItem FieTabelaIr
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.FieTabelaIr, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QtdePendenteLiquidacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.QtdePendenteLiquidacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPendenteLiquidacao
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.ValorPendenteLiquidacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AmortizacaoAcumuladaPorCota
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.AmortizacaoAcumuladaPorCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem JurosAcumuladoPorCota
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.JurosAcumuladoPorCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AmortizacaoAcumuladaPorValor
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.AmortizacaoAcumuladaPorValor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem JurosAcumuladoPorValor
		{
			get
			{
				return new esQueryItem(this, PosicaoFundoHistoricoMetadata.ColumnNames.JurosAcumuladoPorValor, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PosicaoFundoHistoricoCollection")]
	public partial class PosicaoFundoHistoricoCollection : esPosicaoFundoHistoricoCollection, IEnumerable<PosicaoFundoHistorico>
	{
		public PosicaoFundoHistoricoCollection()
		{

		}
		
		public static implicit operator List<PosicaoFundoHistorico>(PosicaoFundoHistoricoCollection coll)
		{
			List<PosicaoFundoHistorico> list = new List<PosicaoFundoHistorico>();
			
			foreach (PosicaoFundoHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoFundoHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoFundoHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoFundoHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoFundoHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoFundoHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoFundoHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoFundoHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoFundoHistorico AddNew()
		{
			PosicaoFundoHistorico entity = base.AddNewEntity() as PosicaoFundoHistorico;
			
			return entity;
		}

		public PosicaoFundoHistorico FindByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			return base.FindByPrimaryKey(idPosicao, dataHistorico) as PosicaoFundoHistorico;
		}


		#region IEnumerable<PosicaoFundoHistorico> Members

		IEnumerator<PosicaoFundoHistorico> IEnumerable<PosicaoFundoHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoFundoHistorico;
			}
		}

		#endregion
		
		private PosicaoFundoHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoFundoHistorico' table
	/// </summary>

	[Serializable]
	public partial class PosicaoFundoHistorico : esPosicaoFundoHistorico
	{
		public PosicaoFundoHistorico()
		{

		}
	
		public PosicaoFundoHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoFundoHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoFundoHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoFundoHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoFundoHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoFundoHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoFundoHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoFundoHistoricoQuery query;
	}



	[Serializable]
	public partial class PosicaoFundoHistoricoQuery : esPosicaoFundoHistoricoQuery
	{
		public PosicaoFundoHistoricoQuery()
		{

		}		
		
		public PosicaoFundoHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoFundoHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoFundoHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.DataHistorico, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.IdOperacao, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.IdCliente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.IdCarteira, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.ValorAplicacao, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.ValorAplicacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.DataAplicacao, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.DataAplicacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.DataConversao, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.CotaAplicacao, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.CotaAplicacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.CotaDia, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.CotaDia;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.ValorBruto, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.ValorLiquido, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.QuantidadeInicial, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.QuantidadeInicial;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.Quantidade, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.QuantidadeBloqueada, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.QuantidadeBloqueada;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.DataUltimaCobrancaIR, 15, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.DataUltimaCobrancaIR;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.ValorIR, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.ValorIOF, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.ValorPerformance, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.ValorIOFVirtual, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.ValorIOFVirtual;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.QuantidadeAntesCortes, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.QuantidadeAntesCortes;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.ValorRendimento, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.ValorRendimento;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.DataUltimoCortePfee, 22, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.DataUltimoCortePfee;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.PosicaoIncorporada, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.PosicaoIncorporada;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.FieTabelaIr, 24, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.FieTabelaIr;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.QtdePendenteLiquidacao, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.QtdePendenteLiquidacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.ValorPendenteLiquidacao, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.ValorPendenteLiquidacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.AmortizacaoAcumuladaPorCota, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.AmortizacaoAcumuladaPorCota;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.JurosAcumuladoPorCota, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.JurosAcumuladoPorCota;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.AmortizacaoAcumuladaPorValor, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.AmortizacaoAcumuladaPorValor;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoFundoHistoricoMetadata.ColumnNames.JurosAcumuladoPorValor, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoFundoHistoricoMetadata.PropertyNames.JurosAcumuladoPorValor;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PosicaoFundoHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorAplicacao = "ValorAplicacao";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string DataConversao = "DataConversao";
			 public const string CotaAplicacao = "CotaAplicacao";
			 public const string CotaDia = "CotaDia";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string DataUltimaCobrancaIR = "DataUltimaCobrancaIR";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string ValorIOFVirtual = "ValorIOFVirtual";
			 public const string QuantidadeAntesCortes = "QuantidadeAntesCortes";
			 public const string ValorRendimento = "ValorRendimento";
			 public const string DataUltimoCortePfee = "DataUltimoCortePfee";
			 public const string PosicaoIncorporada = "PosicaoIncorporada";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string QtdePendenteLiquidacao = "QtdePendenteLiquidacao";
			 public const string ValorPendenteLiquidacao = "ValorPendenteLiquidacao";
			 public const string AmortizacaoAcumuladaPorCota = "AmortizacaoAcumuladaPorCota";
			 public const string JurosAcumuladoPorCota = "JurosAcumuladoPorCota";
			 public const string AmortizacaoAcumuladaPorValor = "AmortizacaoAcumuladaPorValor";
			 public const string JurosAcumuladoPorValor = "JurosAcumuladoPorValor";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorAplicacao = "ValorAplicacao";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string DataConversao = "DataConversao";
			 public const string CotaAplicacao = "CotaAplicacao";
			 public const string CotaDia = "CotaDia";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string DataUltimaCobrancaIR = "DataUltimaCobrancaIR";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string ValorIOFVirtual = "ValorIOFVirtual";
			 public const string QuantidadeAntesCortes = "QuantidadeAntesCortes";
			 public const string ValorRendimento = "ValorRendimento";
			 public const string DataUltimoCortePfee = "DataUltimoCortePfee";
			 public const string PosicaoIncorporada = "PosicaoIncorporada";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string QtdePendenteLiquidacao = "QtdePendenteLiquidacao";
			 public const string ValorPendenteLiquidacao = "ValorPendenteLiquidacao";
			 public const string AmortizacaoAcumuladaPorCota = "AmortizacaoAcumuladaPorCota";
			 public const string JurosAcumuladoPorCota = "JurosAcumuladoPorCota";
			 public const string AmortizacaoAcumuladaPorValor = "AmortizacaoAcumuladaPorValor";
			 public const string JurosAcumuladoPorValor = "JurosAcumuladoPorValor";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoFundoHistoricoMetadata))
			{
				if(PosicaoFundoHistoricoMetadata.mapDelegates == null)
				{
					PosicaoFundoHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoFundoHistoricoMetadata.meta == null)
				{
					PosicaoFundoHistoricoMetadata.meta = new PosicaoFundoHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorAplicacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataAplicacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CotaAplicacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeInicial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeBloqueada", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataUltimaCobrancaIR", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOFVirtual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeAntesCortes", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorRendimento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataUltimoCortePfee", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PosicaoIncorporada", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("FieTabelaIr", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QtdePendenteLiquidacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPendenteLiquidacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AmortizacaoAcumuladaPorCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("JurosAcumuladoPorCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AmortizacaoAcumuladaPorValor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("JurosAcumuladoPorValor", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "PosicaoFundoHistorico";
				meta.Destination = "PosicaoFundoHistorico";
				
				meta.spInsert = "proc_PosicaoFundoHistoricoInsert";				
				meta.spUpdate = "proc_PosicaoFundoHistoricoUpdate";		
				meta.spDelete = "proc_PosicaoFundoHistoricoDelete";
				meta.spLoadAll = "proc_PosicaoFundoHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoFundoHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoFundoHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
