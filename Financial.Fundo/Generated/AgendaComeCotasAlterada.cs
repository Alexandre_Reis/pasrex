/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 03/07/2015 20:13:07
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esAgendaComeCotasAlteradaCollection : esEntityCollection
	{
		public esAgendaComeCotasAlteradaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AgendaComeCotasAlteradaCollection";
		}

		#region Query Logic
		protected void InitQuery(esAgendaComeCotasAlteradaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAgendaComeCotasAlteradaQuery);
		}
		#endregion
		
		virtual public AgendaComeCotasAlterada DetachEntity(AgendaComeCotasAlterada entity)
		{
			return base.DetachEntity(entity) as AgendaComeCotasAlterada;
		}
		
		virtual public AgendaComeCotasAlterada AttachEntity(AgendaComeCotasAlterada entity)
		{
			return base.AttachEntity(entity) as AgendaComeCotasAlterada;
		}
		
		virtual public void Combine(AgendaComeCotasAlteradaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public AgendaComeCotasAlterada this[int index]
		{
			get
			{
				return base[index] as AgendaComeCotasAlterada;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(AgendaComeCotasAlterada);
		}
	}



	[Serializable]
	abstract public class esAgendaComeCotasAlterada : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAgendaComeCotasAlteradaQuery GetDynamicQuery()
		{
			return null;
		}

		public esAgendaComeCotasAlterada()
		{

		}

		public esAgendaComeCotasAlterada(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idAgendaComeCotas)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgendaComeCotas);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgendaComeCotas);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idAgendaComeCotas)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgendaComeCotas);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgendaComeCotas);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idAgendaComeCotas)
		{
			esAgendaComeCotasAlteradaQuery query = this.GetDynamicQuery();
			query.Where(query.IdAgendaComeCotas == idAgendaComeCotas);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idAgendaComeCotas)
		{
			esParameters parms = new esParameters();
			parms.Add("IdAgendaComeCotas",idAgendaComeCotas);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdAgendaComeCotas": this.str.IdAgendaComeCotas = (string)value; break;							
						case "ValorIrAgendadoAntigo": this.str.ValorIrAgendadoAntigo = (string)value; break;							
						case "ValorIOFVirtualAntigo": this.str.ValorIOFVirtualAntigo = (string)value; break;							
						case "Residual15Antigo": this.str.Residual15Antigo = (string)value; break;							
						case "Residual175Antigo": this.str.Residual175Antigo = (string)value; break;							
						case "Residual20Antigo": this.str.Residual20Antigo = (string)value; break;							
						case "Residual225Antigo": this.str.Residual225Antigo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdAgendaComeCotas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgendaComeCotas = (System.Int32?)value;
							break;
						
						case "ValorIrAgendadoAntigo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIrAgendadoAntigo = (System.Decimal?)value;
							break;
						
						case "ValorIOFVirtualAntigo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOFVirtualAntigo = (System.Decimal?)value;
							break;
						
						case "Residual15Antigo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Residual15Antigo = (System.Decimal?)value;
							break;
						
						case "Residual175Antigo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Residual175Antigo = (System.Decimal?)value;
							break;
						
						case "Residual20Antigo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Residual20Antigo = (System.Decimal?)value;
							break;
						
						case "Residual225Antigo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Residual225Antigo = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to AgendaComeCotasAlterada.IdAgendaComeCotas
		/// </summary>
		virtual public System.Int32? IdAgendaComeCotas
		{
			get
			{
				return base.GetSystemInt32(AgendaComeCotasAlteradaMetadata.ColumnNames.IdAgendaComeCotas);
			}
			
			set
			{
				base.SetSystemInt32(AgendaComeCotasAlteradaMetadata.ColumnNames.IdAgendaComeCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotasAlterada.ValorIrAgendadoAntigo
		/// </summary>
		virtual public System.Decimal? ValorIrAgendadoAntigo
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasAlteradaMetadata.ColumnNames.ValorIrAgendadoAntigo);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasAlteradaMetadata.ColumnNames.ValorIrAgendadoAntigo, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotasAlterada.ValorIOFVirtualAntigo
		/// </summary>
		virtual public System.Decimal? ValorIOFVirtualAntigo
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasAlteradaMetadata.ColumnNames.ValorIOFVirtualAntigo);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasAlteradaMetadata.ColumnNames.ValorIOFVirtualAntigo, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotasAlterada.Residual15Antigo
		/// </summary>
		virtual public System.Decimal? Residual15Antigo
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasAlteradaMetadata.ColumnNames.Residual15Antigo);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasAlteradaMetadata.ColumnNames.Residual15Antigo, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotasAlterada.Residual175Antigo
		/// </summary>
		virtual public System.Decimal? Residual175Antigo
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasAlteradaMetadata.ColumnNames.Residual175Antigo);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasAlteradaMetadata.ColumnNames.Residual175Antigo, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotasAlterada.Residual20Antigo
		/// </summary>
		virtual public System.Decimal? Residual20Antigo
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasAlteradaMetadata.ColumnNames.Residual20Antigo);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasAlteradaMetadata.ColumnNames.Residual20Antigo, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotasAlterada.Residual225Antigo
		/// </summary>
		virtual public System.Decimal? Residual225Antigo
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasAlteradaMetadata.ColumnNames.Residual225Antigo);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasAlteradaMetadata.ColumnNames.Residual225Antigo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAgendaComeCotasAlterada entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdAgendaComeCotas
			{
				get
				{
					System.Int32? data = entity.IdAgendaComeCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgendaComeCotas = null;
					else entity.IdAgendaComeCotas = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorIrAgendadoAntigo
			{
				get
				{
					System.Decimal? data = entity.ValorIrAgendadoAntigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIrAgendadoAntigo = null;
					else entity.ValorIrAgendadoAntigo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOFVirtualAntigo
			{
				get
				{
					System.Decimal? data = entity.ValorIOFVirtualAntigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOFVirtualAntigo = null;
					else entity.ValorIOFVirtualAntigo = Convert.ToDecimal(value);
				}
			}
				
			public System.String Residual15Antigo
			{
				get
				{
					System.Decimal? data = entity.Residual15Antigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Residual15Antigo = null;
					else entity.Residual15Antigo = Convert.ToDecimal(value);
				}
			}
				
			public System.String Residual175Antigo
			{
				get
				{
					System.Decimal? data = entity.Residual175Antigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Residual175Antigo = null;
					else entity.Residual175Antigo = Convert.ToDecimal(value);
				}
			}
				
			public System.String Residual20Antigo
			{
				get
				{
					System.Decimal? data = entity.Residual20Antigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Residual20Antigo = null;
					else entity.Residual20Antigo = Convert.ToDecimal(value);
				}
			}
				
			public System.String Residual225Antigo
			{
				get
				{
					System.Decimal? data = entity.Residual225Antigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Residual225Antigo = null;
					else entity.Residual225Antigo = Convert.ToDecimal(value);
				}
			}
			

			private esAgendaComeCotasAlterada entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAgendaComeCotasAlteradaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAgendaComeCotasAlterada can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class AgendaComeCotasAlterada : esAgendaComeCotasAlterada
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAgendaComeCotasAlteradaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AgendaComeCotasAlteradaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdAgendaComeCotas
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasAlteradaMetadata.ColumnNames.IdAgendaComeCotas, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorIrAgendadoAntigo
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasAlteradaMetadata.ColumnNames.ValorIrAgendadoAntigo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOFVirtualAntigo
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasAlteradaMetadata.ColumnNames.ValorIOFVirtualAntigo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Residual15Antigo
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasAlteradaMetadata.ColumnNames.Residual15Antigo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Residual175Antigo
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasAlteradaMetadata.ColumnNames.Residual175Antigo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Residual20Antigo
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasAlteradaMetadata.ColumnNames.Residual20Antigo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Residual225Antigo
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasAlteradaMetadata.ColumnNames.Residual225Antigo, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("AgendaComeCotasAlteradaCollection")]
	public partial class AgendaComeCotasAlteradaCollection : esAgendaComeCotasAlteradaCollection, IEnumerable<AgendaComeCotasAlterada>
	{
		public AgendaComeCotasAlteradaCollection()
		{

		}
		
		public static implicit operator List<AgendaComeCotasAlterada>(AgendaComeCotasAlteradaCollection coll)
		{
			List<AgendaComeCotasAlterada> list = new List<AgendaComeCotasAlterada>();
			
			foreach (AgendaComeCotasAlterada emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AgendaComeCotasAlteradaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgendaComeCotasAlteradaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new AgendaComeCotasAlterada(row);
		}

		override protected esEntity CreateEntity()
		{
			return new AgendaComeCotasAlterada();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AgendaComeCotasAlteradaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgendaComeCotasAlteradaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AgendaComeCotasAlteradaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public AgendaComeCotasAlterada AddNew()
		{
			AgendaComeCotasAlterada entity = base.AddNewEntity() as AgendaComeCotasAlterada;
			
			return entity;
		}

		public AgendaComeCotasAlterada FindByPrimaryKey(System.Int32 idAgendaComeCotas)
		{
			return base.FindByPrimaryKey(idAgendaComeCotas) as AgendaComeCotasAlterada;
		}


		#region IEnumerable<AgendaComeCotasAlterada> Members

		IEnumerator<AgendaComeCotasAlterada> IEnumerable<AgendaComeCotasAlterada>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as AgendaComeCotasAlterada;
			}
		}

		#endregion
		
		private AgendaComeCotasAlteradaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'AgendaComeCotasAlterada' table
	/// </summary>

	[Serializable]
	public partial class AgendaComeCotasAlterada : esAgendaComeCotasAlterada
	{
		public AgendaComeCotasAlterada()
		{

		}
	
		public AgendaComeCotasAlterada(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AgendaComeCotasAlteradaMetadata.Meta();
			}
		}
		
		
		
		override protected esAgendaComeCotasAlteradaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgendaComeCotasAlteradaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AgendaComeCotasAlteradaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgendaComeCotasAlteradaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AgendaComeCotasAlteradaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AgendaComeCotasAlteradaQuery query;
	}



	[Serializable]
	public partial class AgendaComeCotasAlteradaQuery : esAgendaComeCotasAlteradaQuery
	{
		public AgendaComeCotasAlteradaQuery()
		{

		}		
		
		public AgendaComeCotasAlteradaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AgendaComeCotasAlteradaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AgendaComeCotasAlteradaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AgendaComeCotasAlteradaMetadata.ColumnNames.IdAgendaComeCotas, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaComeCotasAlteradaMetadata.PropertyNames.IdAgendaComeCotas;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasAlteradaMetadata.ColumnNames.ValorIrAgendadoAntigo, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasAlteradaMetadata.PropertyNames.ValorIrAgendadoAntigo;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasAlteradaMetadata.ColumnNames.ValorIOFVirtualAntigo, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasAlteradaMetadata.PropertyNames.ValorIOFVirtualAntigo;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasAlteradaMetadata.ColumnNames.Residual15Antigo, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasAlteradaMetadata.PropertyNames.Residual15Antigo;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasAlteradaMetadata.ColumnNames.Residual175Antigo, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasAlteradaMetadata.PropertyNames.Residual175Antigo;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasAlteradaMetadata.ColumnNames.Residual20Antigo, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasAlteradaMetadata.PropertyNames.Residual20Antigo;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasAlteradaMetadata.ColumnNames.Residual225Antigo, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasAlteradaMetadata.PropertyNames.Residual225Antigo;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public AgendaComeCotasAlteradaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdAgendaComeCotas = "IdAgendaComeCotas";
			 public const string ValorIrAgendadoAntigo = "ValorIrAgendadoAntigo";
			 public const string ValorIOFVirtualAntigo = "ValorIOFVirtualAntigo";
			 public const string Residual15Antigo = "Residual15Antigo";
			 public const string Residual175Antigo = "Residual175Antigo";
			 public const string Residual20Antigo = "Residual20Antigo";
			 public const string Residual225Antigo = "Residual225Antigo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdAgendaComeCotas = "IdAgendaComeCotas";
			 public const string ValorIrAgendadoAntigo = "ValorIrAgendadoAntigo";
			 public const string ValorIOFVirtualAntigo = "ValorIOFVirtualAntigo";
			 public const string Residual15Antigo = "Residual15Antigo";
			 public const string Residual175Antigo = "Residual175Antigo";
			 public const string Residual20Antigo = "Residual20Antigo";
			 public const string Residual225Antigo = "Residual225Antigo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AgendaComeCotasAlteradaMetadata))
			{
				if(AgendaComeCotasAlteradaMetadata.mapDelegates == null)
				{
					AgendaComeCotasAlteradaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AgendaComeCotasAlteradaMetadata.meta == null)
				{
					AgendaComeCotasAlteradaMetadata.meta = new AgendaComeCotasAlteradaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdAgendaComeCotas", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorIrAgendadoAntigo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOFVirtualAntigo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Residual15Antigo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Residual175Antigo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Residual20Antigo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Residual225Antigo", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "AgendaComeCotasAlterada";
				meta.Destination = "AgendaComeCotasAlterada";
				
				meta.spInsert = "proc_AgendaComeCotasAlteradaInsert";				
				meta.spUpdate = "proc_AgendaComeCotasAlteradaUpdate";		
				meta.spDelete = "proc_AgendaComeCotasAlteradaDelete";
				meta.spLoadAll = "proc_AgendaComeCotasAlteradaLoadAll";
				meta.spLoadByPrimaryKey = "proc_AgendaComeCotasAlteradaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AgendaComeCotasAlteradaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
