/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 10/11/2015 17:30:19
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esAliquotaEspecificaCollection : esEntityCollection
	{
		public esAliquotaEspecificaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AliquotaEspecificaCollection";
		}

		#region Query Logic
		protected void InitQuery(esAliquotaEspecificaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAliquotaEspecificaQuery);
		}
		#endregion
		
		virtual public AliquotaEspecifica DetachEntity(AliquotaEspecifica entity)
		{
			return base.DetachEntity(entity) as AliquotaEspecifica;
		}
		
		virtual public AliquotaEspecifica AttachEntity(AliquotaEspecifica entity)
		{
			return base.AttachEntity(entity) as AliquotaEspecifica;
		}
		
		virtual public void Combine(AliquotaEspecificaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public AliquotaEspecifica this[int index]
		{
			get
			{
				return base[index] as AliquotaEspecifica;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(AliquotaEspecifica);
		}
	}



	[Serializable]
	abstract public class esAliquotaEspecifica : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAliquotaEspecificaQuery GetDynamicQuery()
		{
			return null;
		}

		public esAliquotaEspecifica()
		{

		}

		public esAliquotaEspecifica(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataValidade, System.Int32 idCarteira)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataValidade, idCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(dataValidade, idCarteira);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataValidade, System.Int32 idCarteira)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataValidade, idCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(dataValidade, idCarteira);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataValidade, System.Int32 idCarteira)
		{
			esAliquotaEspecificaQuery query = this.GetDynamicQuery();
			query.Where(query.DataValidade == dataValidade, query.IdCarteira == idCarteira);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataValidade, System.Int32 idCarteira)
		{
			esParameters parms = new esParameters();
			parms.Add("DataValidade",dataValidade);			parms.Add("IdCarteira",idCarteira);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataValidade": this.str.DataValidade = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataValidade":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataValidade = (System.DateTime?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to AliquotaEspecifica.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(AliquotaEspecificaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(AliquotaEspecificaMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AliquotaEspecifica.DataValidade
		/// </summary>
		virtual public System.DateTime? DataValidade
		{
			get
			{
				return base.GetSystemDateTime(AliquotaEspecificaMetadata.ColumnNames.DataValidade);
			}
			
			set
			{
				base.SetSystemDateTime(AliquotaEspecificaMetadata.ColumnNames.DataValidade, value);
			}
		}
		
		/// <summary>
		/// Maps to AliquotaEspecifica.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(AliquotaEspecificaMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(AliquotaEspecificaMetadata.ColumnNames.Taxa, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAliquotaEspecifica entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataValidade
			{
				get
				{
					System.DateTime? data = entity.DataValidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataValidade = null;
					else entity.DataValidade = Convert.ToDateTime(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
			

			private esAliquotaEspecifica entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAliquotaEspecificaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAliquotaEspecifica can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class AliquotaEspecifica : esAliquotaEspecifica
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AliquotaEspecifica_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAliquotaEspecificaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AliquotaEspecificaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, AliquotaEspecificaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataValidade
		{
			get
			{
				return new esQueryItem(this, AliquotaEspecificaMetadata.ColumnNames.DataValidade, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, AliquotaEspecificaMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("AliquotaEspecificaCollection")]
	public partial class AliquotaEspecificaCollection : esAliquotaEspecificaCollection, IEnumerable<AliquotaEspecifica>
	{
		public AliquotaEspecificaCollection()
		{

		}
		
		public static implicit operator List<AliquotaEspecifica>(AliquotaEspecificaCollection coll)
		{
			List<AliquotaEspecifica> list = new List<AliquotaEspecifica>();
			
			foreach (AliquotaEspecifica emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AliquotaEspecificaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AliquotaEspecificaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new AliquotaEspecifica(row);
		}

		override protected esEntity CreateEntity()
		{
			return new AliquotaEspecifica();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AliquotaEspecificaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AliquotaEspecificaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AliquotaEspecificaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public AliquotaEspecifica AddNew()
		{
			AliquotaEspecifica entity = base.AddNewEntity() as AliquotaEspecifica;
			
			return entity;
		}

		public AliquotaEspecifica FindByPrimaryKey(System.DateTime dataValidade, System.Int32 idCarteira)
		{
			return base.FindByPrimaryKey(dataValidade, idCarteira) as AliquotaEspecifica;
		}


		#region IEnumerable<AliquotaEspecifica> Members

		IEnumerator<AliquotaEspecifica> IEnumerable<AliquotaEspecifica>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as AliquotaEspecifica;
			}
		}

		#endregion
		
		private AliquotaEspecificaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'AliquotaEspecifica' table
	/// </summary>

	[Serializable]
	public partial class AliquotaEspecifica : esAliquotaEspecifica
	{
		public AliquotaEspecifica()
		{

		}
	
		public AliquotaEspecifica(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AliquotaEspecificaMetadata.Meta();
			}
		}
		
		
		
		override protected esAliquotaEspecificaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AliquotaEspecificaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AliquotaEspecificaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AliquotaEspecificaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AliquotaEspecificaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AliquotaEspecificaQuery query;
	}



	[Serializable]
	public partial class AliquotaEspecificaQuery : esAliquotaEspecificaQuery
	{
		public AliquotaEspecificaQuery()
		{

		}		
		
		public AliquotaEspecificaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AliquotaEspecificaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AliquotaEspecificaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AliquotaEspecificaMetadata.ColumnNames.IdCarteira, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AliquotaEspecificaMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AliquotaEspecificaMetadata.ColumnNames.DataValidade, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AliquotaEspecificaMetadata.PropertyNames.DataValidade;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AliquotaEspecificaMetadata.ColumnNames.Taxa, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AliquotaEspecificaMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 5;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public AliquotaEspecificaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string DataValidade = "DataValidade";
			 public const string Taxa = "Taxa";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string DataValidade = "DataValidade";
			 public const string Taxa = "Taxa";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AliquotaEspecificaMetadata))
			{
				if(AliquotaEspecificaMetadata.mapDelegates == null)
				{
					AliquotaEspecificaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AliquotaEspecificaMetadata.meta == null)
				{
					AliquotaEspecificaMetadata.meta = new AliquotaEspecificaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataValidade", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "AliquotaEspecifica";
				meta.Destination = "AliquotaEspecifica";
				
				meta.spInsert = "proc_AliquotaEspecificaInsert";				
				meta.spUpdate = "proc_AliquotaEspecificaUpdate";		
				meta.spDelete = "proc_AliquotaEspecificaDelete";
				meta.spLoadAll = "proc_AliquotaEspecificaLoadAll";
				meta.spLoadByPrimaryKey = "proc_AliquotaEspecificaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AliquotaEspecificaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
