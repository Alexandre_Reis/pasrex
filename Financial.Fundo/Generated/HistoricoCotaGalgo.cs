/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 17/12/2015 09:22:51
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

    [Serializable]
    abstract public class esHistoricoCotaGalgoCollection : esEntityCollection
    {
        public esHistoricoCotaGalgoCollection()
        {

        }

        protected override string GetCollectionName()
        {
            return "HistoricoCotaGalgoCollection";
        }

        #region Query Logic
        protected void InitQuery(esHistoricoCotaGalgoQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntityCollection)this).Connection;
        }

        protected bool OnQueryLoaded(DataTable table)
        {
            this.PopulateCollection(table);
            return (this.RowCount > 0) ? true : false;
        }

        protected override void HookupQuery(esDynamicQuery query)
        {
            this.InitQuery(query as esHistoricoCotaGalgoQuery);
        }
        #endregion

        virtual public HistoricoCotaGalgo DetachEntity(HistoricoCotaGalgo entity)
        {
            return base.DetachEntity(entity) as HistoricoCotaGalgo;
        }

        virtual public HistoricoCotaGalgo AttachEntity(HistoricoCotaGalgo entity)
        {
            return base.AttachEntity(entity) as HistoricoCotaGalgo;
        }

        virtual public void Combine(HistoricoCotaGalgoCollection collection)
        {
            base.Combine(collection);
        }

        new public HistoricoCotaGalgo this[int index]
        {
            get
            {
                return base[index] as HistoricoCotaGalgo;
            }
        }

        public override Type GetEntityType()
        {
            return typeof(HistoricoCotaGalgo);
        }
    }



    [Serializable]
    abstract public class esHistoricoCotaGalgo : esEntity
    {
        /// <summary>
        /// Used internally by the entity's DynamicQuery mechanism.
        /// </summary>
        virtual protected esHistoricoCotaGalgoQuery GetDynamicQuery()
        {
            return null;
        }

        public esHistoricoCotaGalgo()
        {

        }

        public esHistoricoCotaGalgo(DataRow row)
            : base(row)
        {

        }

        #region LoadByPrimaryKey
        public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int32 idCarteira)
        {
            if (this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(data, idCarteira);
            else
                return LoadByPrimaryKeyStoredProcedure(data, idCarteira);
        }

        public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int32 idCarteira)
        {
            if (sqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(data, idCarteira);
            else
                return LoadByPrimaryKeyStoredProcedure(data, idCarteira);
        }

        private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int32 idCarteira)
        {
            esHistoricoCotaGalgoQuery query = this.GetDynamicQuery();
            query.Where(query.Data == data, query.IdCarteira == idCarteira);
            return query.Load();
        }

        private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int32 idCarteira)
        {
            esParameters parms = new esParameters();
            parms.Add("Data", data); parms.Add("IdCarteira", idCarteira);
            return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
        }
        #endregion



        #region Properties


        public override void SetProperties(IDictionary values)
        {
            foreach (string propertyName in values.Keys)
            {
                this.SetProperty(propertyName, values[propertyName]);
            }
        }

        public override void SetProperty(string name, object value)
        {
            if (this.Row == null) this.AddNew();

            esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
            if (col != null)
            {
                if (value == null || value.GetType().ToString() == "System.String")
                {
                    // Use the strongly typed property
                    switch (name)
                    {
                        case "Data": this.str.Data = (string)value; break;
                        case "IdCarteira": this.str.IdCarteira = (string)value; break;
                        case "Cota": this.str.Cota = (string)value; break;
                        case "Pl": this.str.Pl = (string)value; break;
                        case "DataImportacao": this.str.DataImportacao = (string)value; break;
                        case "UsuarioImportacao": this.str.UsuarioImportacao = (string)value; break;
                        case "Aprovar": this.str.Aprovar = (string)value; break;
                        case "DataAprovacao": this.str.DataAprovacao = (string)value; break;
                        case "UsuarioAprovacao": this.str.UsuarioAprovacao = (string)value; break;
                    }
                }
                else
                {
                    switch (name)
                    {
                        case "Data":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.Data = (System.DateTime?)value;
                            break;

                        case "IdCarteira":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdCarteira = (System.Int32?)value;
                            break;

                        case "Cota":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.Cota = (System.Decimal?)value;
                            break;

                        case "Pl":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.Pl = (System.Decimal?)value;
                            break;

                        case "DataImportacao":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataImportacao = (System.DateTime?)value;
                            break;

                        case "Aprovar":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.Aprovar = (System.Int32?)value;
                            break;

                        case "DataAprovacao":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataAprovacao = (System.DateTime?)value;
                            break;


                        default:
                            break;
                    }
                }
            }
            else if (this.Row.Table.Columns.Contains(name))
            {
                this.Row[name] = value;
            }
            else
            {
                throw new Exception("SetProperty Error: '" + name + "' not found");
            }
        }


        /// <summary>
        /// Maps to HistoricoCotaGalgo.Data
        /// </summary>
        virtual public System.DateTime? Data
        {
            get
            {
                return base.GetSystemDateTime(HistoricoCotaGalgoMetadata.ColumnNames.Data);
            }

            set
            {
                base.SetSystemDateTime(HistoricoCotaGalgoMetadata.ColumnNames.Data, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCotaGalgo.IdCarteira
        /// </summary>
        virtual public System.Int32? IdCarteira
        {
            get
            {
                return base.GetSystemInt32(HistoricoCotaGalgoMetadata.ColumnNames.IdCarteira);
            }

            set
            {
                if (base.SetSystemInt32(HistoricoCotaGalgoMetadata.ColumnNames.IdCarteira, value))
                {
                    this._UpToCarteiraByIdCarteira = null;
                }
            }
        }

        /// <summary>
        /// Maps to HistoricoCotaGalgo.Cota
        /// </summary>
        virtual public System.Decimal? Cota
        {
            get
            {
                return base.GetSystemDecimal(HistoricoCotaGalgoMetadata.ColumnNames.Cota);
            }

            set
            {
                base.SetSystemDecimal(HistoricoCotaGalgoMetadata.ColumnNames.Cota, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCotaGalgo.PL
        /// </summary>
        virtual public System.Decimal? Pl
        {
            get
            {
                return base.GetSystemDecimal(HistoricoCotaGalgoMetadata.ColumnNames.Pl);
            }

            set
            {
                base.SetSystemDecimal(HistoricoCotaGalgoMetadata.ColumnNames.Pl, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCotaGalgo.DataImportacao
        /// </summary>
        virtual public System.DateTime? DataImportacao
        {
            get
            {
                return base.GetSystemDateTime(HistoricoCotaGalgoMetadata.ColumnNames.DataImportacao);
            }

            set
            {
                base.SetSystemDateTime(HistoricoCotaGalgoMetadata.ColumnNames.DataImportacao, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCotaGalgo.UsuarioImportacao
        /// </summary>
        virtual public System.String UsuarioImportacao
        {
            get
            {
                return base.GetSystemString(HistoricoCotaGalgoMetadata.ColumnNames.UsuarioImportacao);
            }

            set
            {
                base.SetSystemString(HistoricoCotaGalgoMetadata.ColumnNames.UsuarioImportacao, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCotaGalgo.Aprovar
        /// </summary>
        virtual public System.Int32? Aprovar
        {
            get
            {
                return base.GetSystemInt32(HistoricoCotaGalgoMetadata.ColumnNames.Aprovar);
            }

            set
            {
                base.SetSystemInt32(HistoricoCotaGalgoMetadata.ColumnNames.Aprovar, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCotaGalgo.DataAprovacao
        /// </summary>
        virtual public System.DateTime? DataAprovacao
        {
            get
            {
                return base.GetSystemDateTime(HistoricoCotaGalgoMetadata.ColumnNames.DataAprovacao);
            }

            set
            {
                base.SetSystemDateTime(HistoricoCotaGalgoMetadata.ColumnNames.DataAprovacao, value);
            }
        }

        /// <summary>
        /// Maps to HistoricoCotaGalgo.UsuarioAprovacao
        /// </summary>
        virtual public System.String UsuarioAprovacao
        {
            get
            {
                return base.GetSystemString(HistoricoCotaGalgoMetadata.ColumnNames.UsuarioAprovacao);
            }

            set
            {
                base.SetSystemString(HistoricoCotaGalgoMetadata.ColumnNames.UsuarioAprovacao, value);
            }
        }

        [CLSCompliant(false)]
        internal protected Carteira _UpToCarteiraByIdCarteira;
        #endregion

        #region String Properties


        [BrowsableAttribute(false)]
        public esStrings str
        {
            get
            {
                if (esstrings == null)
                {
                    esstrings = new esStrings(this);
                }
                return esstrings;
            }
        }


        [Serializable]
        sealed public class esStrings
        {
            public esStrings(esHistoricoCotaGalgo entity)
            {
                this.entity = entity;
            }


            public System.String Data
            {
                get
                {
                    System.DateTime? data = entity.Data;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Data = null;
                    else entity.Data = Convert.ToDateTime(value);
                }
            }

            public System.String IdCarteira
            {
                get
                {
                    System.Int32? data = entity.IdCarteira;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdCarteira = null;
                    else entity.IdCarteira = Convert.ToInt32(value);
                }
            }

            public System.String Cota
            {
                get
                {
                    System.Decimal? data = entity.Cota;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Cota = null;
                    else entity.Cota = Convert.ToDecimal(value);
                }
            }

            public System.String Pl
            {
                get
                {
                    System.Decimal? data = entity.Pl;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Pl = null;
                    else entity.Pl = Convert.ToDecimal(value);
                }
            }

            public System.String DataImportacao
            {
                get
                {
                    System.DateTime? data = entity.DataImportacao;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.DataImportacao = null;
                    else entity.DataImportacao = Convert.ToDateTime(value);
                }
            }

            public System.String UsuarioImportacao
            {
                get
                {
                    System.String data = entity.UsuarioImportacao;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.UsuarioImportacao = null;
                    else entity.UsuarioImportacao = Convert.ToString(value);
                }
            }

            public System.String Aprovar
            {
                get
                {
                    System.Int32? data = entity.Aprovar;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Aprovar = null;
                    else entity.Aprovar = Convert.ToInt32(value);
                }
            }

            public System.String DataAprovacao
            {
                get
                {
                    System.DateTime? data = entity.DataAprovacao;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.DataAprovacao = null;
                    else entity.DataAprovacao = Convert.ToDateTime(value);
                }
            }

            public System.String UsuarioAprovacao
            {
                get
                {
                    System.String data = entity.UsuarioAprovacao;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.UsuarioAprovacao = null;
                    else entity.UsuarioAprovacao = Convert.ToString(value);
                }
            }


            private esHistoricoCotaGalgo entity;
        }
        #endregion

        #region Query Logic
        protected void InitQuery(esHistoricoCotaGalgoQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntity)this).Connection;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        protected bool OnQueryLoaded(DataTable table)
        {
            bool dataFound = this.PopulateEntity(table);

            if (this.RowCount > 1)
            {
                throw new Exception("esHistoricoCotaGalgo can only hold one record of data");
            }

            return dataFound;
        }
        #endregion

        [NonSerialized]
        private esStrings esstrings;
    }



    public partial class HistoricoCotaGalgo : esHistoricoCotaGalgo
    {


        #region UpToCarteiraByIdCarteira - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - Carteira_HistoricoCotaGalgo_FK1
        /// </summary>

        [XmlIgnore]
        public Carteira UpToCarteiraByIdCarteira
        {
            get
            {
                if (this._UpToCarteiraByIdCarteira == null
                    && IdCarteira != null)
                {
                    this._UpToCarteiraByIdCarteira = new Carteira();
                    this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
                    this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
                    this._UpToCarteiraByIdCarteira.Query.Load();
                }

                return this._UpToCarteiraByIdCarteira;
            }

            set
            {
                this.RemovePreSave("UpToCarteiraByIdCarteira");


                if (value == null)
                {
                    this.IdCarteira = null;
                    this._UpToCarteiraByIdCarteira = null;
                }
                else
                {
                    this.IdCarteira = value.IdCarteira;
                    this._UpToCarteiraByIdCarteira = value;
                    this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
                }

            }
        }
        #endregion



        /// <summary>
        /// Used internally by the entity's hierarchical properties.
        /// </summary>
        protected override List<esPropertyDescriptor> GetHierarchicalProperties()
        {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();


            return props;
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PreSave.
        /// </summary>
        protected override void ApplyPreSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostSave.
        /// </summary>
        protected override void ApplyPostSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostOneToOneSave.
        /// </summary>
        protected override void ApplyPostOneSaveKeys()
        {
        }

    }



    [Serializable]
    abstract public class esHistoricoCotaGalgoQuery : esDynamicQuery
    {
        override protected IMetadata Meta
        {
            get
            {
                return HistoricoCotaGalgoMetadata.Meta();
            }
        }


        public esQueryItem Data
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaGalgoMetadata.ColumnNames.Data, esSystemType.DateTime);
            }
        }

        public esQueryItem IdCarteira
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaGalgoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
            }
        }

        public esQueryItem Cota
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaGalgoMetadata.ColumnNames.Cota, esSystemType.Decimal);
            }
        }

        public esQueryItem Pl
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaGalgoMetadata.ColumnNames.Pl, esSystemType.Decimal);
            }
        }

        public esQueryItem DataImportacao
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaGalgoMetadata.ColumnNames.DataImportacao, esSystemType.DateTime);
            }
        }

        public esQueryItem UsuarioImportacao
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaGalgoMetadata.ColumnNames.UsuarioImportacao, esSystemType.String);
            }
        }

        public esQueryItem Aprovar
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaGalgoMetadata.ColumnNames.Aprovar, esSystemType.Int32);
            }
        }

        public esQueryItem DataAprovacao
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaGalgoMetadata.ColumnNames.DataAprovacao, esSystemType.DateTime);
            }
        }

        public esQueryItem UsuarioAprovacao
        {
            get
            {
                return new esQueryItem(this, HistoricoCotaGalgoMetadata.ColumnNames.UsuarioAprovacao, esSystemType.String);
            }
        }

    }



    [Serializable]
    [XmlType("HistoricoCotaGalgoCollection")]
    public partial class HistoricoCotaGalgoCollection : esHistoricoCotaGalgoCollection, IEnumerable<HistoricoCotaGalgo>
    {
        public HistoricoCotaGalgoCollection()
        {

        }

        public static implicit operator List<HistoricoCotaGalgo>(HistoricoCotaGalgoCollection coll)
        {
            List<HistoricoCotaGalgo> list = new List<HistoricoCotaGalgo>();

            foreach (HistoricoCotaGalgo emp in coll)
            {
                list.Add(emp);
            }

            return list;
        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return HistoricoCotaGalgoMetadata.Meta();
            }
        }



        override protected esDynamicQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new HistoricoCotaGalgoQuery();
                this.InitQuery(query);
            }
            return this.query;
        }

        override protected esEntity CreateEntityForCollection(DataRow row)
        {
            return new HistoricoCotaGalgo(row);
        }

        override protected esEntity CreateEntity()
        {
            return new HistoricoCotaGalgo();
        }


        #endregion


        [BrowsableAttribute(false)]
        public HistoricoCotaGalgoQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new HistoricoCotaGalgoQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }

        public bool Load(HistoricoCotaGalgoQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        public HistoricoCotaGalgo AddNew()
        {
            HistoricoCotaGalgo entity = base.AddNewEntity() as HistoricoCotaGalgo;

            return entity;
        }

        public HistoricoCotaGalgo FindByPrimaryKey(System.DateTime data, System.Int32 idCarteira)
        {
            return base.FindByPrimaryKey(data, idCarteira) as HistoricoCotaGalgo;
        }


        #region IEnumerable<HistoricoCotaGalgo> Members

        IEnumerator<HistoricoCotaGalgo> IEnumerable<HistoricoCotaGalgo>.GetEnumerator()
        {
            System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
            System.Collections.IEnumerator iterator = enumer.GetEnumerator();

            while (iterator.MoveNext())
            {
                yield return iterator.Current as HistoricoCotaGalgo;
            }
        }

        #endregion

        private HistoricoCotaGalgoQuery query;
    }


    /// <summary>
    /// Encapsulates the 'HistoricoCotaGalgo' table
    /// </summary>

    [Serializable]
    public partial class HistoricoCotaGalgo : esHistoricoCotaGalgo
    {
        public HistoricoCotaGalgo()
        {

        }

        public HistoricoCotaGalgo(DataRow row)
            : base(row)
        {

        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return HistoricoCotaGalgoMetadata.Meta();
            }
        }



        override protected esHistoricoCotaGalgoQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new HistoricoCotaGalgoQuery();
                this.InitQuery(query);
            }
            return this.query;
        }
        #endregion




        [BrowsableAttribute(false)]
        public HistoricoCotaGalgoQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new HistoricoCotaGalgoQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }


        public bool Load(HistoricoCotaGalgoQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        private HistoricoCotaGalgoQuery query;
    }



    [Serializable]
    public partial class HistoricoCotaGalgoQuery : esHistoricoCotaGalgoQuery
    {
        public HistoricoCotaGalgoQuery()
        {

        }

        public HistoricoCotaGalgoQuery(string joinAlias)
        {
            this.es.JoinAlias = joinAlias;
        }


    }



    [Serializable]
    public partial class HistoricoCotaGalgoMetadata : esMetadata, IMetadata
    {
        #region Protected Constructor
        protected HistoricoCotaGalgoMetadata()
        {
            _columns = new esColumnMetadataCollection();
            esColumnMetadata c;

            c = new esColumnMetadata(HistoricoCotaGalgoMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = HistoricoCotaGalgoMetadata.PropertyNames.Data;
            c.IsInPrimaryKey = true;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaGalgoMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = HistoricoCotaGalgoMetadata.PropertyNames.IdCarteira;
            c.IsInPrimaryKey = true;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaGalgoMetadata.ColumnNames.Cota, 2, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = HistoricoCotaGalgoMetadata.PropertyNames.Cota;
            c.NumericPrecision = 28;
            c.NumericScale = 12;
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaGalgoMetadata.ColumnNames.Pl, 3, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = HistoricoCotaGalgoMetadata.PropertyNames.Pl;
            c.NumericPrecision = 16;
            c.NumericScale = 2;
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaGalgoMetadata.ColumnNames.DataImportacao, 4, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = HistoricoCotaGalgoMetadata.PropertyNames.DataImportacao;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaGalgoMetadata.ColumnNames.UsuarioImportacao, 5, typeof(System.String), esSystemType.String);
            c.PropertyName = HistoricoCotaGalgoMetadata.PropertyNames.UsuarioImportacao;
            c.CharacterMaxLength = 50;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaGalgoMetadata.ColumnNames.Aprovar, 6, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = HistoricoCotaGalgoMetadata.PropertyNames.Aprovar;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaGalgoMetadata.ColumnNames.DataAprovacao, 7, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = HistoricoCotaGalgoMetadata.PropertyNames.DataAprovacao;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(HistoricoCotaGalgoMetadata.ColumnNames.UsuarioAprovacao, 8, typeof(System.String), esSystemType.String);
            c.PropertyName = HistoricoCotaGalgoMetadata.PropertyNames.UsuarioAprovacao;
            c.CharacterMaxLength = 50;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


        }
        #endregion

        static public HistoricoCotaGalgoMetadata Meta()
        {
            return meta;
        }

        public Guid DataID
        {
            get { return base._dataID; }
        }

        public bool MultiProviderMode
        {
            get { return false; }
        }

        public esColumnMetadataCollection Columns
        {
            get { return base._columns; }
        }

        #region ColumnNames
        public class ColumnNames
        {
            public const string Data = "Data";
            public const string IdCarteira = "IdCarteira";
            public const string Cota = "Cota";
            public const string Pl = "PL";
            public const string DataImportacao = "DataImportacao";
            public const string UsuarioImportacao = "UsuarioImportacao";
            public const string Aprovar = "Aprovar";
            public const string DataAprovacao = "DataAprovacao";
            public const string UsuarioAprovacao = "UsuarioAprovacao";
        }
        #endregion

        #region PropertyNames
        public class PropertyNames
        {
            public const string Data = "Data";
            public const string IdCarteira = "IdCarteira";
            public const string Cota = "Cota";
            public const string Pl = "Pl";
            public const string DataImportacao = "DataImportacao";
            public const string UsuarioImportacao = "UsuarioImportacao";
            public const string Aprovar = "Aprovar";
            public const string DataAprovacao = "DataAprovacao";
            public const string UsuarioAprovacao = "UsuarioAprovacao";
        }
        #endregion

        public esProviderSpecificMetadata GetProviderMetadata(string mapName)
        {
            MapToMeta mapMethod = mapDelegates[mapName];

            if (mapMethod != null)
                return mapMethod(mapName);
            else
                return null;
        }

        #region MAP esDefault

        static private int RegisterDelegateesDefault()
        {
            // This is only executed once per the life of the application
            lock (typeof(HistoricoCotaGalgoMetadata))
            {
                if (HistoricoCotaGalgoMetadata.mapDelegates == null)
                {
                    HistoricoCotaGalgoMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
                }

                if (HistoricoCotaGalgoMetadata.meta == null)
                {
                    HistoricoCotaGalgoMetadata.meta = new HistoricoCotaGalgoMetadata();
                }

                MapToMeta mapMethod = new MapToMeta(meta.esDefault);
                mapDelegates.Add("esDefault", mapMethod);
                mapMethod("esDefault");
            }
            return 0;
        }

        private esProviderSpecificMetadata esDefault(string mapName)
        {
            if (!_providerMetadataMaps.ContainsKey(mapName))
            {
                esProviderSpecificMetadata meta = new esProviderSpecificMetadata();


                meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
                meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("Cota", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("PL", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("DataImportacao", new esTypeMap("datetime", "System.DateTime"));
                meta.AddTypeMap("UsuarioImportacao", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("Aprovar", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("DataAprovacao", new esTypeMap("datetime", "System.DateTime"));
                meta.AddTypeMap("UsuarioAprovacao", new esTypeMap("varchar", "System.String"));



                meta.Source = "HistoricoCotaGalgo";
                meta.Destination = "HistoricoCotaGalgo";

                meta.spInsert = "proc_HistoricoCotaGalgoInsert";
                meta.spUpdate = "proc_HistoricoCotaGalgoUpdate";
                meta.spDelete = "proc_HistoricoCotaGalgoDelete";
                meta.spLoadAll = "proc_HistoricoCotaGalgoLoadAll";
                meta.spLoadByPrimaryKey = "proc_HistoricoCotaGalgoLoadByPrimaryKey";

                this._providerMetadataMaps["esDefault"] = meta;
            }

            return this._providerMetadataMaps["esDefault"];
        }

        #endregion

        static private HistoricoCotaGalgoMetadata meta;
        static protected Dictionary<string, MapToMeta> mapDelegates;
        static private int _esDefault = RegisterDelegateesDefault();
    }
}
