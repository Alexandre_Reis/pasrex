/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 13/12/2014 17:12:49
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esEventoFundoCaixaCollection : esEntityCollection
	{
		public esEventoFundoCaixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EventoFundoCaixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esEventoFundoCaixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEventoFundoCaixaQuery);
		}
		#endregion
		
		virtual public EventoFundoCaixa DetachEntity(EventoFundoCaixa entity)
		{
			return base.DetachEntity(entity) as EventoFundoCaixa;
		}
		
		virtual public EventoFundoCaixa AttachEntity(EventoFundoCaixa entity)
		{
			return base.AttachEntity(entity) as EventoFundoCaixa;
		}
		
		virtual public void Combine(EventoFundoCaixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EventoFundoCaixa this[int index]
		{
			get
			{
				return base[index] as EventoFundoCaixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EventoFundoCaixa);
		}
	}



	[Serializable]
	abstract public class esEventoFundoCaixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEventoFundoCaixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esEventoFundoCaixa()
		{

		}

		public esEventoFundoCaixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int32 idConta, System.Int32 idEventoFundo, System.Decimal saldoAbertura, System.Int32 tipoMercado)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idConta, idEventoFundo, saldoAbertura, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idConta, idEventoFundo, saldoAbertura, tipoMercado);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int32 idConta, System.Int32 idEventoFundo, System.Decimal saldoAbertura, System.Int32 tipoMercado)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idConta, idEventoFundo, saldoAbertura, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idConta, idEventoFundo, saldoAbertura, tipoMercado);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int32 idConta, System.Int32 idEventoFundo, System.Decimal saldoAbertura, System.Int32 tipoMercado)
		{
			esEventoFundoCaixaQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.IdConta == idConta, query.IdEventoFundo == idEventoFundo, query.SaldoAbertura == saldoAbertura, query.TipoMercado == tipoMercado);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int32 idConta, System.Int32 idEventoFundo, System.Decimal saldoAbertura, System.Int32 tipoMercado)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("IdConta",idConta);			parms.Add("IdEventoFundo",idEventoFundo);			parms.Add("SaldoAbertura",saldoAbertura);			parms.Add("TipoMercado",tipoMercado);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEventoFundo": this.str.IdEventoFundo = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "IdConta": this.str.IdConta = (string)value; break;							
						case "SaldoAbertura": this.str.SaldoAbertura = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "QuantidadeConstante": this.str.QuantidadeConstante = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Processar": this.str.Processar = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEventoFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoFundo = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoMercado = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConta = (System.Int32?)value;
							break;
						
						case "SaldoAbertura":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.SaldoAbertura = (System.Decimal?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "QuantidadeConstante":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.QuantidadeConstante = (System.Int32?)value;
							break;
						
						case "Processar":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Processar = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EventoFundoCaixa.IdEventoFundo
		/// </summary>
		virtual public System.Int32? IdEventoFundo
		{
			get
			{
				return base.GetSystemInt32(EventoFundoCaixaMetadata.ColumnNames.IdEventoFundo);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoFundoCaixaMetadata.ColumnNames.IdEventoFundo, value))
				{
					this._UpToEventoFundoByIdEventoFundo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoCaixa.TipoMercado
		/// </summary>
		virtual public System.Int32? TipoMercado
		{
			get
			{
				return base.GetSystemInt32(EventoFundoCaixaMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoCaixaMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoCaixa.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(EventoFundoCaixaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(EventoFundoCaixaMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoCaixa.IdConta
		/// </summary>
		virtual public System.Int32? IdConta
		{
			get
			{
				return base.GetSystemInt32(EventoFundoCaixaMetadata.ColumnNames.IdConta);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoCaixaMetadata.ColumnNames.IdConta, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoCaixa.SaldoAbertura
		/// </summary>
		virtual public System.Decimal? SaldoAbertura
		{
			get
			{
				return base.GetSystemDecimal(EventoFundoCaixaMetadata.ColumnNames.SaldoAbertura);
			}
			
			set
			{
				base.SetSystemDecimal(EventoFundoCaixaMetadata.ColumnNames.SaldoAbertura, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoCaixa.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(EventoFundoCaixaMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(EventoFundoCaixaMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoCaixa.QuantidadeConstante
		/// </summary>
		virtual public System.Int32? QuantidadeConstante
		{
			get
			{
				return base.GetSystemInt32(EventoFundoCaixaMetadata.ColumnNames.QuantidadeConstante);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoCaixaMetadata.ColumnNames.QuantidadeConstante, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoCaixa.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(EventoFundoCaixaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(EventoFundoCaixaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoCaixa.Processar
		/// </summary>
		virtual public System.Byte? Processar
		{
			get
			{
				return base.GetSystemByte(EventoFundoCaixaMetadata.ColumnNames.Processar);
			}
			
			set
			{
				base.SetSystemByte(EventoFundoCaixaMetadata.ColumnNames.Processar, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected EventoFundo _UpToEventoFundoByIdEventoFundo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEventoFundoCaixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEventoFundo
			{
				get
				{
					System.Int32? data = entity.IdEventoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoFundo = null;
					else entity.IdEventoFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Int32? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdConta
			{
				get
				{
					System.Int32? data = entity.IdConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConta = null;
					else entity.IdConta = Convert.ToInt32(value);
				}
			}
				
			public System.String SaldoAbertura
			{
				get
				{
					System.Decimal? data = entity.SaldoAbertura;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SaldoAbertura = null;
					else entity.SaldoAbertura = Convert.ToDecimal(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeConstante
			{
				get
				{
					System.Int32? data = entity.QuantidadeConstante;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeConstante = null;
					else entity.QuantidadeConstante = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Processar
			{
				get
				{
					System.Byte? data = entity.Processar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Processar = null;
					else entity.Processar = Convert.ToByte(value);
				}
			}
			

			private esEventoFundoCaixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEventoFundoCaixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEventoFundoCaixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EventoFundoCaixa : esEventoFundoCaixa
	{

				
		#region UpToEventoFundoByIdEventoFundo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EventoFundoCaixa_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundo UpToEventoFundoByIdEventoFundo
		{
			get
			{
				if(this._UpToEventoFundoByIdEventoFundo == null
					&& IdEventoFundo != null					)
				{
					this._UpToEventoFundoByIdEventoFundo = new EventoFundo();
					this._UpToEventoFundoByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Where(this._UpToEventoFundoByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Load();
				}

				return this._UpToEventoFundoByIdEventoFundo;
			}
			
			set
			{
				this.RemovePreSave("UpToEventoFundoByIdEventoFundo");
				

				if(value == null)
				{
					this.IdEventoFundo = null;
					this._UpToEventoFundoByIdEventoFundo = null;
				}
				else
				{
					this.IdEventoFundo = value.IdEventoFundo;
					this._UpToEventoFundoByIdEventoFundo = value;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEventoFundoByIdEventoFundo != null)
			{
				this.IdEventoFundo = this._UpToEventoFundoByIdEventoFundo.IdEventoFundo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEventoFundoCaixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoCaixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEventoFundo
		{
			get
			{
				return new esQueryItem(this, EventoFundoCaixaMetadata.ColumnNames.IdEventoFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, EventoFundoCaixaMetadata.ColumnNames.TipoMercado, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, EventoFundoCaixaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdConta
		{
			get
			{
				return new esQueryItem(this, EventoFundoCaixaMetadata.ColumnNames.IdConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem SaldoAbertura
		{
			get
			{
				return new esQueryItem(this, EventoFundoCaixaMetadata.ColumnNames.SaldoAbertura, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, EventoFundoCaixaMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeConstante
		{
			get
			{
				return new esQueryItem(this, EventoFundoCaixaMetadata.ColumnNames.QuantidadeConstante, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, EventoFundoCaixaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Processar
		{
			get
			{
				return new esQueryItem(this, EventoFundoCaixaMetadata.ColumnNames.Processar, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EventoFundoCaixaCollection")]
	public partial class EventoFundoCaixaCollection : esEventoFundoCaixaCollection, IEnumerable<EventoFundoCaixa>
	{
		public EventoFundoCaixaCollection()
		{

		}
		
		public static implicit operator List<EventoFundoCaixa>(EventoFundoCaixaCollection coll)
		{
			List<EventoFundoCaixa> list = new List<EventoFundoCaixa>();
			
			foreach (EventoFundoCaixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EventoFundoCaixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoCaixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EventoFundoCaixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EventoFundoCaixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EventoFundoCaixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoCaixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EventoFundoCaixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EventoFundoCaixa AddNew()
		{
			EventoFundoCaixa entity = base.AddNewEntity() as EventoFundoCaixa;
			
			return entity;
		}

		public EventoFundoCaixa FindByPrimaryKey(System.DateTime data, System.Int32 idConta, System.Int32 idEventoFundo, System.Decimal saldoAbertura, System.Int32 tipoMercado)
		{
			return base.FindByPrimaryKey(data, idConta, idEventoFundo, saldoAbertura, tipoMercado) as EventoFundoCaixa;
		}


		#region IEnumerable<EventoFundoCaixa> Members

		IEnumerator<EventoFundoCaixa> IEnumerable<EventoFundoCaixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EventoFundoCaixa;
			}
		}

		#endregion
		
		private EventoFundoCaixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EventoFundoCaixa' table
	/// </summary>

	[Serializable]
	public partial class EventoFundoCaixa : esEventoFundoCaixa
	{
		public EventoFundoCaixa()
		{

		}
	
		public EventoFundoCaixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoCaixaMetadata.Meta();
			}
		}
		
		
		
		override protected esEventoFundoCaixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoCaixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EventoFundoCaixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoCaixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EventoFundoCaixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EventoFundoCaixaQuery query;
	}



	[Serializable]
	public partial class EventoFundoCaixaQuery : esEventoFundoCaixaQuery
	{
		public EventoFundoCaixaQuery()
		{

		}		
		
		public EventoFundoCaixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EventoFundoCaixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EventoFundoCaixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EventoFundoCaixaMetadata.ColumnNames.IdEventoFundo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoCaixaMetadata.PropertyNames.IdEventoFundo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoCaixaMetadata.ColumnNames.TipoMercado, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoCaixaMetadata.PropertyNames.TipoMercado;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoCaixaMetadata.ColumnNames.Data, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EventoFundoCaixaMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoCaixaMetadata.ColumnNames.IdConta, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoCaixaMetadata.PropertyNames.IdConta;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoCaixaMetadata.ColumnNames.SaldoAbertura, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EventoFundoCaixaMetadata.PropertyNames.SaldoAbertura;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoCaixaMetadata.ColumnNames.Valor, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EventoFundoCaixaMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoCaixaMetadata.ColumnNames.QuantidadeConstante, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoCaixaMetadata.PropertyNames.QuantidadeConstante;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoCaixaMetadata.ColumnNames.Descricao, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = EventoFundoCaixaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoCaixaMetadata.ColumnNames.Processar, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EventoFundoCaixaMetadata.PropertyNames.Processar;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EventoFundoCaixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string TipoMercado = "TipoMercado";
			 public const string Data = "Data";
			 public const string IdConta = "IdConta";
			 public const string SaldoAbertura = "SaldoAbertura";
			 public const string Valor = "Valor";
			 public const string QuantidadeConstante = "QuantidadeConstante";
			 public const string Descricao = "Descricao";
			 public const string Processar = "Processar";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string TipoMercado = "TipoMercado";
			 public const string Data = "Data";
			 public const string IdConta = "IdConta";
			 public const string SaldoAbertura = "SaldoAbertura";
			 public const string Valor = "Valor";
			 public const string QuantidadeConstante = "QuantidadeConstante";
			 public const string Descricao = "Descricao";
			 public const string Processar = "Processar";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EventoFundoCaixaMetadata))
			{
				if(EventoFundoCaixaMetadata.mapDelegates == null)
				{
					EventoFundoCaixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EventoFundoCaixaMetadata.meta == null)
				{
					EventoFundoCaixaMetadata.meta = new EventoFundoCaixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEventoFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("SaldoAbertura", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeConstante", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Processar", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "EventoFundoCaixa";
				meta.Destination = "EventoFundoCaixa";
				
				meta.spInsert = "proc_EventoFundoCaixaInsert";				
				meta.spUpdate = "proc_EventoFundoCaixaUpdate";		
				meta.spDelete = "proc_EventoFundoCaixaDelete";
				meta.spLoadAll = "proc_EventoFundoCaixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_EventoFundoCaixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EventoFundoCaixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
