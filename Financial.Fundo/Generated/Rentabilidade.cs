/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 20/10/2015 15:21:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esRentabilidadeCollection : esEntityCollection
	{
		public esRentabilidadeCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "RentabilidadeCollection";
		}

		#region Query Logic
		protected void InitQuery(esRentabilidadeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esRentabilidadeQuery);
		}
		#endregion
		
		virtual public Rentabilidade DetachEntity(Rentabilidade entity)
		{
			return base.DetachEntity(entity) as Rentabilidade;
		}
		
		virtual public Rentabilidade AttachEntity(Rentabilidade entity)
		{
			return base.AttachEntity(entity) as Rentabilidade;
		}
		
		virtual public void Combine(RentabilidadeCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Rentabilidade this[int index]
		{
			get
			{
				return base[index] as Rentabilidade;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Rentabilidade);
		}
	}



	[Serializable]
	abstract public class esRentabilidade : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esRentabilidadeQuery GetDynamicQuery()
		{
			return null;
		}

		public esRentabilidade()
		{

		}

		public esRentabilidade(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idRentabilidade)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idRentabilidade);
			else
				return LoadByPrimaryKeyStoredProcedure(idRentabilidade);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idRentabilidade)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idRentabilidade);
			else
				return LoadByPrimaryKeyStoredProcedure(idRentabilidade);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idRentabilidade)
		{
			esRentabilidadeQuery query = this.GetDynamicQuery();
			query.Where(query.IdRentabilidade == idRentabilidade);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idRentabilidade)
		{
			esParameters parms = new esParameters();
			parms.Add("IdRentabilidade",idRentabilidade);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdRentabilidade": this.str.IdRentabilidade = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "TipoAtivo": this.str.TipoAtivo = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CodigoAtivo": this.str.CodigoAtivo = (string)value; break;							
						case "CodigoMoedaAtivo": this.str.CodigoMoedaAtivo = (string)value; break;							
						case "PatrimonioInicialMoedaAtivo": this.str.PatrimonioInicialMoedaAtivo = (string)value; break;							
						case "QuantidadeInicialAtivo": this.str.QuantidadeInicialAtivo = (string)value; break;							
						case "ValorFinanceiroEntradaMoedaAtivo": this.str.ValorFinanceiroEntradaMoedaAtivo = (string)value; break;							
						case "QuantidadeTotalEntradaAtivo": this.str.QuantidadeTotalEntradaAtivo = (string)value; break;							
						case "ValorFinanceiroSaidaMoedaAtivo": this.str.ValorFinanceiroSaidaMoedaAtivo = (string)value; break;							
						case "QuantidadeTotalSaidaAtivo": this.str.QuantidadeTotalSaidaAtivo = (string)value; break;							
						case "ValorFinanceiroRendasMoedaAtivo": this.str.ValorFinanceiroRendasMoedaAtivo = (string)value; break;							
						case "PatrimonioFinalMoedaAtivo": this.str.PatrimonioFinalMoedaAtivo = (string)value; break;							
						case "QuantidadeFinalAtivo": this.str.QuantidadeFinalAtivo = (string)value; break;							
						case "RentabilidadeMoedaAtivo": this.str.RentabilidadeMoedaAtivo = (string)value; break;							
						case "CodigoMoedaPortfolio": this.str.CodigoMoedaPortfolio = (string)value; break;							
						case "PatrimonioInicialMoedaPortfolio": this.str.PatrimonioInicialMoedaPortfolio = (string)value; break;							
						case "ValorFinanceiroEntradaMoedaPortfolio": this.str.ValorFinanceiroEntradaMoedaPortfolio = (string)value; break;							
						case "ValorFinanceiroSaidaMoedaPortfolio": this.str.ValorFinanceiroSaidaMoedaPortfolio = (string)value; break;							
						case "ValorFinanceiroRendasMoedaPortfolio": this.str.ValorFinanceiroRendasMoedaPortfolio = (string)value; break;							
						case "PatrimonioFinalMoedaPortfolio": this.str.PatrimonioFinalMoedaPortfolio = (string)value; break;							
						case "RentabilidadeMoedaPortfolio": this.str.RentabilidadeMoedaPortfolio = (string)value; break;							
						case "IdTituloRendaFixa": this.str.IdTituloRendaFixa = (string)value; break;							
						case "IdOperacaoSwap": this.str.IdOperacaoSwap = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "CdAtivoBMF": this.str.CdAtivoBMF = (string)value; break;							
						case "SerieBMF": this.str.SerieBMF = (string)value; break;							
						case "CotaGerencial": this.str.CotaGerencial = (string)value; break;							
						case "RentabilidadeBrutaDiariaMoedaAtivo": this.str.RentabilidadeBrutaDiariaMoedaAtivo = (string)value; break;							
						case "RentabilidadeBrutaAcumMoedaAtivo": this.str.RentabilidadeBrutaAcumMoedaAtivo = (string)value; break;							
						case "RentabilidadeBrutaDiariaMoedaPortfolio": this.str.RentabilidadeBrutaDiariaMoedaPortfolio = (string)value; break;							
						case "RentabilidadeBrutaAcumMoedaPortfolio": this.str.RentabilidadeBrutaAcumMoedaPortfolio = (string)value; break;							
						case "PatrimonioInicialBrutoMoedaPortfolio": this.str.PatrimonioInicialBrutoMoedaPortfolio = (string)value; break;							
						case "PatrimonioInicialBrutoMoedaAtivo": this.str.PatrimonioInicialBrutoMoedaAtivo = (string)value; break;							
						case "PatrimonioFinalBrutoMoedaPortfolio": this.str.PatrimonioFinalBrutoMoedaPortfolio = (string)value; break;							
						case "PatrimonioFinalBrutoMoedaAtivo": this.str.PatrimonioFinalBrutoMoedaAtivo = (string)value; break;							
						case "RentabilidadeAtivosIcentivados": this.str.RentabilidadeAtivosIcentivados = (string)value; break;							
						case "RentabilidadeAtivosTributados": this.str.RentabilidadeAtivosTributados = (string)value; break;							
						case "RentabilidadeGrossUpDiariaMoedaAtivo": this.str.RentabilidadeGrossUpDiariaMoedaAtivo = (string)value; break;							
						case "RentabilidadeGrossUpAcumMoedaAtivo": this.str.RentabilidadeGrossUpAcumMoedaAtivo = (string)value; break;							
						case "RentabilidadeGrossUpDiariaMoedaPortfolio": this.str.RentabilidadeGrossUpDiariaMoedaPortfolio = (string)value; break;							
						case "RentabilidadeGrossUpAcumMoedaPortfolio": this.str.RentabilidadeGrossUpAcumMoedaPortfolio = (string)value; break;							
						case "PatrimonioInicialGrossUpMoedaAtivo": this.str.PatrimonioInicialGrossUpMoedaAtivo = (string)value; break;							
						case "PatrimonioInicialGrossUpMoedaPortfolio": this.str.PatrimonioInicialGrossUpMoedaPortfolio = (string)value; break;							
						case "PatrimonioFinalGrossUpMoedaAtivo": this.str.PatrimonioFinalGrossUpMoedaAtivo = (string)value; break;							
						case "PatrimonioFinalGrossUpMoedaPortfolio": this.str.PatrimonioFinalGrossUpMoedaPortfolio = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdRentabilidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdRentabilidade = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "TipoAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoAtivo = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "PatrimonioInicialMoedaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PatrimonioInicialMoedaAtivo = (System.Decimal?)value;
							break;
						
						case "QuantidadeInicialAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeInicialAtivo = (System.Decimal?)value;
							break;
						
						case "ValorFinanceiroEntradaMoedaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorFinanceiroEntradaMoedaAtivo = (System.Decimal?)value;
							break;
						
						case "QuantidadeTotalEntradaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeTotalEntradaAtivo = (System.Decimal?)value;
							break;
						
						case "ValorFinanceiroSaidaMoedaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorFinanceiroSaidaMoedaAtivo = (System.Decimal?)value;
							break;
						
						case "QuantidadeTotalSaidaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeTotalSaidaAtivo = (System.Decimal?)value;
							break;
						
						case "ValorFinanceiroRendasMoedaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorFinanceiroRendasMoedaAtivo = (System.Decimal?)value;
							break;
						
						case "PatrimonioFinalMoedaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PatrimonioFinalMoedaAtivo = (System.Decimal?)value;
							break;
						
						case "QuantidadeFinalAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeFinalAtivo = (System.Decimal?)value;
							break;
						
						case "RentabilidadeMoedaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RentabilidadeMoedaAtivo = (System.Decimal?)value;
							break;
						
						case "PatrimonioInicialMoedaPortfolio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PatrimonioInicialMoedaPortfolio = (System.Decimal?)value;
							break;
						
						case "ValorFinanceiroEntradaMoedaPortfolio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorFinanceiroEntradaMoedaPortfolio = (System.Decimal?)value;
							break;
						
						case "ValorFinanceiroSaidaMoedaPortfolio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorFinanceiroSaidaMoedaPortfolio = (System.Decimal?)value;
							break;
						
						case "ValorFinanceiroRendasMoedaPortfolio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorFinanceiroRendasMoedaPortfolio = (System.Decimal?)value;
							break;
						
						case "PatrimonioFinalMoedaPortfolio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PatrimonioFinalMoedaPortfolio = (System.Decimal?)value;
							break;
						
						case "RentabilidadeMoedaPortfolio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RentabilidadeMoedaPortfolio = (System.Decimal?)value;
							break;
						
						case "IdTituloRendaFixa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTituloRendaFixa = (System.Int32?)value;
							break;
						
						case "IdOperacaoSwap":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoSwap = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "CotaGerencial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaGerencial = (System.Decimal?)value;
							break;
						
						case "RentabilidadeBrutaDiariaMoedaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RentabilidadeBrutaDiariaMoedaAtivo = (System.Decimal?)value;
							break;
						
						case "RentabilidadeBrutaAcumMoedaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RentabilidadeBrutaAcumMoedaAtivo = (System.Decimal?)value;
							break;
						
						case "RentabilidadeBrutaDiariaMoedaPortfolio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RentabilidadeBrutaDiariaMoedaPortfolio = (System.Decimal?)value;
							break;
						
						case "RentabilidadeBrutaAcumMoedaPortfolio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RentabilidadeBrutaAcumMoedaPortfolio = (System.Decimal?)value;
							break;
						
						case "PatrimonioInicialBrutoMoedaPortfolio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PatrimonioInicialBrutoMoedaPortfolio = (System.Decimal?)value;
							break;
						
						case "PatrimonioInicialBrutoMoedaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PatrimonioInicialBrutoMoedaAtivo = (System.Decimal?)value;
							break;
						
						case "PatrimonioFinalBrutoMoedaPortfolio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PatrimonioFinalBrutoMoedaPortfolio = (System.Decimal?)value;
							break;
						
						case "PatrimonioFinalBrutoMoedaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PatrimonioFinalBrutoMoedaAtivo = (System.Decimal?)value;
							break;
						
						case "RentabilidadeAtivosIcentivados":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RentabilidadeAtivosIcentivados = (System.Decimal?)value;
							break;
						
						case "RentabilidadeAtivosTributados":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RentabilidadeAtivosTributados = (System.Decimal?)value;
							break;
						
						case "RentabilidadeGrossUpDiariaMoedaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RentabilidadeGrossUpDiariaMoedaAtivo = (System.Decimal?)value;
							break;
						
						case "RentabilidadeGrossUpAcumMoedaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RentabilidadeGrossUpAcumMoedaAtivo = (System.Decimal?)value;
							break;
						
						case "RentabilidadeGrossUpDiariaMoedaPortfolio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RentabilidadeGrossUpDiariaMoedaPortfolio = (System.Decimal?)value;
							break;
						
						case "RentabilidadeGrossUpAcumMoedaPortfolio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RentabilidadeGrossUpAcumMoedaPortfolio = (System.Decimal?)value;
							break;
						
						case "PatrimonioInicialGrossUpMoedaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PatrimonioInicialGrossUpMoedaAtivo = (System.Decimal?)value;
							break;
						
						case "PatrimonioInicialGrossUpMoedaPortfolio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PatrimonioInicialGrossUpMoedaPortfolio = (System.Decimal?)value;
							break;
						
						case "PatrimonioFinalGrossUpMoedaAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PatrimonioFinalGrossUpMoedaAtivo = (System.Decimal?)value;
							break;
						
						case "PatrimonioFinalGrossUpMoedaPortfolio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PatrimonioFinalGrossUpMoedaPortfolio = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Rentabilidade.IdRentabilidade
		/// </summary>
		virtual public System.Int32? IdRentabilidade
		{
			get
			{
				return base.GetSystemInt32(RentabilidadeMetadata.ColumnNames.IdRentabilidade);
			}
			
			set
			{
				base.SetSystemInt32(RentabilidadeMetadata.ColumnNames.IdRentabilidade, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(RentabilidadeMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(RentabilidadeMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.TipoAtivo
		/// </summary>
		virtual public System.Int32? TipoAtivo
		{
			get
			{
				return base.GetSystemInt32(RentabilidadeMetadata.ColumnNames.TipoAtivo);
			}
			
			set
			{
				base.SetSystemInt32(RentabilidadeMetadata.ColumnNames.TipoAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(RentabilidadeMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(RentabilidadeMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.CodigoAtivo
		/// </summary>
		virtual public System.String CodigoAtivo
		{
			get
			{
				return base.GetSystemString(RentabilidadeMetadata.ColumnNames.CodigoAtivo);
			}
			
			set
			{
				base.SetSystemString(RentabilidadeMetadata.ColumnNames.CodigoAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.CodigoMoedaAtivo
		/// </summary>
		virtual public System.String CodigoMoedaAtivo
		{
			get
			{
				return base.GetSystemString(RentabilidadeMetadata.ColumnNames.CodigoMoedaAtivo);
			}
			
			set
			{
				base.SetSystemString(RentabilidadeMetadata.ColumnNames.CodigoMoedaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.PatrimonioInicialMoedaAtivo
		/// </summary>
		virtual public System.Decimal? PatrimonioInicialMoedaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioInicialMoedaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioInicialMoedaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.QuantidadeInicialAtivo
		/// </summary>
		virtual public System.Decimal? QuantidadeInicialAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.QuantidadeInicialAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.QuantidadeInicialAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.ValorFinanceiroEntradaMoedaAtivo
		/// </summary>
		virtual public System.Decimal? ValorFinanceiroEntradaMoedaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.ValorFinanceiroEntradaMoedaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.ValorFinanceiroEntradaMoedaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.QuantidadeTotalEntradaAtivo
		/// </summary>
		virtual public System.Decimal? QuantidadeTotalEntradaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.QuantidadeTotalEntradaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.QuantidadeTotalEntradaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.ValorFinanceiroSaidaMoedaAtivo
		/// </summary>
		virtual public System.Decimal? ValorFinanceiroSaidaMoedaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.ValorFinanceiroSaidaMoedaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.ValorFinanceiroSaidaMoedaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.QuantidadeTotalSaidaAtivo
		/// </summary>
		virtual public System.Decimal? QuantidadeTotalSaidaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.QuantidadeTotalSaidaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.QuantidadeTotalSaidaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.ValorFinanceiroRendasMoedaAtivo
		/// </summary>
		virtual public System.Decimal? ValorFinanceiroRendasMoedaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.ValorFinanceiroRendasMoedaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.ValorFinanceiroRendasMoedaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.PatrimonioFinalMoedaAtivo
		/// </summary>
		virtual public System.Decimal? PatrimonioFinalMoedaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioFinalMoedaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioFinalMoedaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.QuantidadeFinalAtivo
		/// </summary>
		virtual public System.Decimal? QuantidadeFinalAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.QuantidadeFinalAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.QuantidadeFinalAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.RentabilidadeMoedaAtivo
		/// </summary>
		virtual public System.Decimal? RentabilidadeMoedaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeMoedaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeMoedaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.CodigoMoedaPortfolio
		/// </summary>
		virtual public System.String CodigoMoedaPortfolio
		{
			get
			{
				return base.GetSystemString(RentabilidadeMetadata.ColumnNames.CodigoMoedaPortfolio);
			}
			
			set
			{
				base.SetSystemString(RentabilidadeMetadata.ColumnNames.CodigoMoedaPortfolio, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.PatrimonioInicialMoedaPortfolio
		/// </summary>
		virtual public System.Decimal? PatrimonioInicialMoedaPortfolio
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioInicialMoedaPortfolio);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioInicialMoedaPortfolio, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.ValorFinanceiroEntradaMoedaPortfolio
		/// </summary>
		virtual public System.Decimal? ValorFinanceiroEntradaMoedaPortfolio
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.ValorFinanceiroEntradaMoedaPortfolio);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.ValorFinanceiroEntradaMoedaPortfolio, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.ValorFinanceiroSaidaMoedaPortfolio
		/// </summary>
		virtual public System.Decimal? ValorFinanceiroSaidaMoedaPortfolio
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.ValorFinanceiroSaidaMoedaPortfolio);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.ValorFinanceiroSaidaMoedaPortfolio, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.ValorFinanceiroRendasMoedaPortfolio
		/// </summary>
		virtual public System.Decimal? ValorFinanceiroRendasMoedaPortfolio
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.ValorFinanceiroRendasMoedaPortfolio);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.ValorFinanceiroRendasMoedaPortfolio, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.PatrimonioFinalMoedaPortfolio
		/// </summary>
		virtual public System.Decimal? PatrimonioFinalMoedaPortfolio
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioFinalMoedaPortfolio);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioFinalMoedaPortfolio, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.RentabilidadeMoedaPortfolio
		/// </summary>
		virtual public System.Decimal? RentabilidadeMoedaPortfolio
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeMoedaPortfolio);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeMoedaPortfolio, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.IdTituloRendaFixa
		/// </summary>
		virtual public System.Int32? IdTituloRendaFixa
		{
			get
			{
				return base.GetSystemInt32(RentabilidadeMetadata.ColumnNames.IdTituloRendaFixa);
			}
			
			set
			{
				base.SetSystemInt32(RentabilidadeMetadata.ColumnNames.IdTituloRendaFixa, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.IdOperacaoSwap
		/// </summary>
		virtual public System.Int32? IdOperacaoSwap
		{
			get
			{
				return base.GetSystemInt32(RentabilidadeMetadata.ColumnNames.IdOperacaoSwap);
			}
			
			set
			{
				base.SetSystemInt32(RentabilidadeMetadata.ColumnNames.IdOperacaoSwap, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(RentabilidadeMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(RentabilidadeMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(RentabilidadeMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				base.SetSystemString(RentabilidadeMetadata.ColumnNames.CdAtivoBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.CdAtivoBMF
		/// </summary>
		virtual public System.String CdAtivoBMF
		{
			get
			{
				return base.GetSystemString(RentabilidadeMetadata.ColumnNames.CdAtivoBMF);
			}
			
			set
			{
				base.SetSystemString(RentabilidadeMetadata.ColumnNames.CdAtivoBMF, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.SerieBMF
		/// </summary>
		virtual public System.String SerieBMF
		{
			get
			{
				return base.GetSystemString(RentabilidadeMetadata.ColumnNames.SerieBMF);
			}
			
			set
			{
				base.SetSystemString(RentabilidadeMetadata.ColumnNames.SerieBMF, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.CotaGerencial
		/// </summary>
		virtual public System.Decimal? CotaGerencial
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.CotaGerencial);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.CotaGerencial, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.RentabilidadeBrutaDiariaMoedaAtivo
		/// </summary>
		virtual public System.Decimal? RentabilidadeBrutaDiariaMoedaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaDiariaMoedaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaDiariaMoedaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.RentabilidadeBrutaAcumMoedaAtivo
		/// </summary>
		virtual public System.Decimal? RentabilidadeBrutaAcumMoedaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaAcumMoedaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaAcumMoedaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.RentabilidadeBrutaDiariaMoedaPortfolio
		/// </summary>
		virtual public System.Decimal? RentabilidadeBrutaDiariaMoedaPortfolio
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaDiariaMoedaPortfolio);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaDiariaMoedaPortfolio, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.RentabilidadeBrutaAcumMoedaPortfolio
		/// </summary>
		virtual public System.Decimal? RentabilidadeBrutaAcumMoedaPortfolio
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaAcumMoedaPortfolio);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaAcumMoedaPortfolio, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.PatrimonioInicialBrutoMoedaPortfolio
		/// </summary>
		virtual public System.Decimal? PatrimonioInicialBrutoMoedaPortfolio
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioInicialBrutoMoedaPortfolio);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioInicialBrutoMoedaPortfolio, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.PatrimonioInicialBrutoMoedaAtivo
		/// </summary>
		virtual public System.Decimal? PatrimonioInicialBrutoMoedaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioInicialBrutoMoedaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioInicialBrutoMoedaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.PatrimonioFinalBrutoMoedaPortfolio
		/// </summary>
		virtual public System.Decimal? PatrimonioFinalBrutoMoedaPortfolio
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioFinalBrutoMoedaPortfolio);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioFinalBrutoMoedaPortfolio, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.PatrimonioFinalBrutoMoedaAtivo
		/// </summary>
		virtual public System.Decimal? PatrimonioFinalBrutoMoedaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioFinalBrutoMoedaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioFinalBrutoMoedaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.RentabilidadeAtivosIcentivados
		/// </summary>
		virtual public System.Decimal? RentabilidadeAtivosIcentivados
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeAtivosIcentivados);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeAtivosIcentivados, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.RentabilidadeAtivosTributados
		/// </summary>
		virtual public System.Decimal? RentabilidadeAtivosTributados
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeAtivosTributados);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeAtivosTributados, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.RentabilidadeGrossUpDiariaMoedaAtivo
		/// </summary>
		virtual public System.Decimal? RentabilidadeGrossUpDiariaMoedaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpDiariaMoedaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpDiariaMoedaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.RentabilidadeGrossUpAcumMoedaAtivo
		/// </summary>
		virtual public System.Decimal? RentabilidadeGrossUpAcumMoedaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpAcumMoedaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpAcumMoedaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.RentabilidadeGrossUpDiariaMoedaPortfolio
		/// </summary>
		virtual public System.Decimal? RentabilidadeGrossUpDiariaMoedaPortfolio
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpDiariaMoedaPortfolio);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpDiariaMoedaPortfolio, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.RentabilidadeGrossUpAcumMoedaPortfolio
		/// </summary>
		virtual public System.Decimal? RentabilidadeGrossUpAcumMoedaPortfolio
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpAcumMoedaPortfolio);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpAcumMoedaPortfolio, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.PatrimonioInicialGrossUpMoedaAtivo
		/// </summary>
		virtual public System.Decimal? PatrimonioInicialGrossUpMoedaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioInicialGrossUpMoedaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioInicialGrossUpMoedaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.PatrimonioInicialGrossUpMoedaPortfolio
		/// </summary>
		virtual public System.Decimal? PatrimonioInicialGrossUpMoedaPortfolio
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioInicialGrossUpMoedaPortfolio);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioInicialGrossUpMoedaPortfolio, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.PatrimonioFinalGrossUpMoedaAtivo
		/// </summary>
		virtual public System.Decimal? PatrimonioFinalGrossUpMoedaAtivo
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioFinalGrossUpMoedaAtivo);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioFinalGrossUpMoedaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Rentabilidade.PatrimonioFinalGrossUpMoedaPortfolio
		/// </summary>
		virtual public System.Decimal? PatrimonioFinalGrossUpMoedaPortfolio
		{
			get
			{
				return base.GetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioFinalGrossUpMoedaPortfolio);
			}
			
			set
			{
				base.SetSystemDecimal(RentabilidadeMetadata.ColumnNames.PatrimonioFinalGrossUpMoedaPortfolio, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esRentabilidade entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdRentabilidade
			{
				get
				{
					System.Int32? data = entity.IdRentabilidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdRentabilidade = null;
					else entity.IdRentabilidade = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoAtivo
			{
				get
				{
					System.Int32? data = entity.TipoAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoAtivo = null;
					else entity.TipoAtivo = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoAtivo
			{
				get
				{
					System.String data = entity.CodigoAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoAtivo = null;
					else entity.CodigoAtivo = Convert.ToString(value);
				}
			}
				
			public System.String CodigoMoedaAtivo
			{
				get
				{
					System.String data = entity.CodigoMoedaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoMoedaAtivo = null;
					else entity.CodigoMoedaAtivo = Convert.ToString(value);
				}
			}
				
			public System.String PatrimonioInicialMoedaAtivo
			{
				get
				{
					System.Decimal? data = entity.PatrimonioInicialMoedaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PatrimonioInicialMoedaAtivo = null;
					else entity.PatrimonioInicialMoedaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeInicialAtivo
			{
				get
				{
					System.Decimal? data = entity.QuantidadeInicialAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeInicialAtivo = null;
					else entity.QuantidadeInicialAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorFinanceiroEntradaMoedaAtivo
			{
				get
				{
					System.Decimal? data = entity.ValorFinanceiroEntradaMoedaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorFinanceiroEntradaMoedaAtivo = null;
					else entity.ValorFinanceiroEntradaMoedaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeTotalEntradaAtivo
			{
				get
				{
					System.Decimal? data = entity.QuantidadeTotalEntradaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeTotalEntradaAtivo = null;
					else entity.QuantidadeTotalEntradaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorFinanceiroSaidaMoedaAtivo
			{
				get
				{
					System.Decimal? data = entity.ValorFinanceiroSaidaMoedaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorFinanceiroSaidaMoedaAtivo = null;
					else entity.ValorFinanceiroSaidaMoedaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeTotalSaidaAtivo
			{
				get
				{
					System.Decimal? data = entity.QuantidadeTotalSaidaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeTotalSaidaAtivo = null;
					else entity.QuantidadeTotalSaidaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorFinanceiroRendasMoedaAtivo
			{
				get
				{
					System.Decimal? data = entity.ValorFinanceiroRendasMoedaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorFinanceiroRendasMoedaAtivo = null;
					else entity.ValorFinanceiroRendasMoedaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String PatrimonioFinalMoedaAtivo
			{
				get
				{
					System.Decimal? data = entity.PatrimonioFinalMoedaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PatrimonioFinalMoedaAtivo = null;
					else entity.PatrimonioFinalMoedaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeFinalAtivo
			{
				get
				{
					System.Decimal? data = entity.QuantidadeFinalAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeFinalAtivo = null;
					else entity.QuantidadeFinalAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String RentabilidadeMoedaAtivo
			{
				get
				{
					System.Decimal? data = entity.RentabilidadeMoedaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RentabilidadeMoedaAtivo = null;
					else entity.RentabilidadeMoedaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodigoMoedaPortfolio
			{
				get
				{
					System.String data = entity.CodigoMoedaPortfolio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoMoedaPortfolio = null;
					else entity.CodigoMoedaPortfolio = Convert.ToString(value);
				}
			}
				
			public System.String PatrimonioInicialMoedaPortfolio
			{
				get
				{
					System.Decimal? data = entity.PatrimonioInicialMoedaPortfolio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PatrimonioInicialMoedaPortfolio = null;
					else entity.PatrimonioInicialMoedaPortfolio = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorFinanceiroEntradaMoedaPortfolio
			{
				get
				{
					System.Decimal? data = entity.ValorFinanceiroEntradaMoedaPortfolio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorFinanceiroEntradaMoedaPortfolio = null;
					else entity.ValorFinanceiroEntradaMoedaPortfolio = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorFinanceiroSaidaMoedaPortfolio
			{
				get
				{
					System.Decimal? data = entity.ValorFinanceiroSaidaMoedaPortfolio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorFinanceiroSaidaMoedaPortfolio = null;
					else entity.ValorFinanceiroSaidaMoedaPortfolio = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorFinanceiroRendasMoedaPortfolio
			{
				get
				{
					System.Decimal? data = entity.ValorFinanceiroRendasMoedaPortfolio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorFinanceiroRendasMoedaPortfolio = null;
					else entity.ValorFinanceiroRendasMoedaPortfolio = Convert.ToDecimal(value);
				}
			}
				
			public System.String PatrimonioFinalMoedaPortfolio
			{
				get
				{
					System.Decimal? data = entity.PatrimonioFinalMoedaPortfolio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PatrimonioFinalMoedaPortfolio = null;
					else entity.PatrimonioFinalMoedaPortfolio = Convert.ToDecimal(value);
				}
			}
				
			public System.String RentabilidadeMoedaPortfolio
			{
				get
				{
					System.Decimal? data = entity.RentabilidadeMoedaPortfolio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RentabilidadeMoedaPortfolio = null;
					else entity.RentabilidadeMoedaPortfolio = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdTituloRendaFixa
			{
				get
				{
					System.Int32? data = entity.IdTituloRendaFixa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTituloRendaFixa = null;
					else entity.IdTituloRendaFixa = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacaoSwap
			{
				get
				{
					System.Int32? data = entity.IdOperacaoSwap;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoSwap = null;
					else entity.IdOperacaoSwap = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String CdAtivoBMF
			{
				get
				{
					System.String data = entity.CdAtivoBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBMF = null;
					else entity.CdAtivoBMF = Convert.ToString(value);
				}
			}
				
			public System.String SerieBMF
			{
				get
				{
					System.String data = entity.SerieBMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SerieBMF = null;
					else entity.SerieBMF = Convert.ToString(value);
				}
			}
				
			public System.String CotaGerencial
			{
				get
				{
					System.Decimal? data = entity.CotaGerencial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaGerencial = null;
					else entity.CotaGerencial = Convert.ToDecimal(value);
				}
			}
				
			public System.String RentabilidadeBrutaDiariaMoedaAtivo
			{
				get
				{
					System.Decimal? data = entity.RentabilidadeBrutaDiariaMoedaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RentabilidadeBrutaDiariaMoedaAtivo = null;
					else entity.RentabilidadeBrutaDiariaMoedaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String RentabilidadeBrutaAcumMoedaAtivo
			{
				get
				{
					System.Decimal? data = entity.RentabilidadeBrutaAcumMoedaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RentabilidadeBrutaAcumMoedaAtivo = null;
					else entity.RentabilidadeBrutaAcumMoedaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String RentabilidadeBrutaDiariaMoedaPortfolio
			{
				get
				{
					System.Decimal? data = entity.RentabilidadeBrutaDiariaMoedaPortfolio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RentabilidadeBrutaDiariaMoedaPortfolio = null;
					else entity.RentabilidadeBrutaDiariaMoedaPortfolio = Convert.ToDecimal(value);
				}
			}
				
			public System.String RentabilidadeBrutaAcumMoedaPortfolio
			{
				get
				{
					System.Decimal? data = entity.RentabilidadeBrutaAcumMoedaPortfolio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RentabilidadeBrutaAcumMoedaPortfolio = null;
					else entity.RentabilidadeBrutaAcumMoedaPortfolio = Convert.ToDecimal(value);
				}
			}
				
			public System.String PatrimonioInicialBrutoMoedaPortfolio
			{
				get
				{
					System.Decimal? data = entity.PatrimonioInicialBrutoMoedaPortfolio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PatrimonioInicialBrutoMoedaPortfolio = null;
					else entity.PatrimonioInicialBrutoMoedaPortfolio = Convert.ToDecimal(value);
				}
			}
				
			public System.String PatrimonioInicialBrutoMoedaAtivo
			{
				get
				{
					System.Decimal? data = entity.PatrimonioInicialBrutoMoedaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PatrimonioInicialBrutoMoedaAtivo = null;
					else entity.PatrimonioInicialBrutoMoedaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String PatrimonioFinalBrutoMoedaPortfolio
			{
				get
				{
					System.Decimal? data = entity.PatrimonioFinalBrutoMoedaPortfolio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PatrimonioFinalBrutoMoedaPortfolio = null;
					else entity.PatrimonioFinalBrutoMoedaPortfolio = Convert.ToDecimal(value);
				}
			}
				
			public System.String PatrimonioFinalBrutoMoedaAtivo
			{
				get
				{
					System.Decimal? data = entity.PatrimonioFinalBrutoMoedaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PatrimonioFinalBrutoMoedaAtivo = null;
					else entity.PatrimonioFinalBrutoMoedaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String RentabilidadeAtivosIcentivados
			{
				get
				{
					System.Decimal? data = entity.RentabilidadeAtivosIcentivados;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RentabilidadeAtivosIcentivados = null;
					else entity.RentabilidadeAtivosIcentivados = Convert.ToDecimal(value);
				}
			}
				
			public System.String RentabilidadeAtivosTributados
			{
				get
				{
					System.Decimal? data = entity.RentabilidadeAtivosTributados;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RentabilidadeAtivosTributados = null;
					else entity.RentabilidadeAtivosTributados = Convert.ToDecimal(value);
				}
			}
				
			public System.String RentabilidadeGrossUpDiariaMoedaAtivo
			{
				get
				{
					System.Decimal? data = entity.RentabilidadeGrossUpDiariaMoedaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RentabilidadeGrossUpDiariaMoedaAtivo = null;
					else entity.RentabilidadeGrossUpDiariaMoedaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String RentabilidadeGrossUpAcumMoedaAtivo
			{
				get
				{
					System.Decimal? data = entity.RentabilidadeGrossUpAcumMoedaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RentabilidadeGrossUpAcumMoedaAtivo = null;
					else entity.RentabilidadeGrossUpAcumMoedaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String RentabilidadeGrossUpDiariaMoedaPortfolio
			{
				get
				{
					System.Decimal? data = entity.RentabilidadeGrossUpDiariaMoedaPortfolio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RentabilidadeGrossUpDiariaMoedaPortfolio = null;
					else entity.RentabilidadeGrossUpDiariaMoedaPortfolio = Convert.ToDecimal(value);
				}
			}
				
			public System.String RentabilidadeGrossUpAcumMoedaPortfolio
			{
				get
				{
					System.Decimal? data = entity.RentabilidadeGrossUpAcumMoedaPortfolio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RentabilidadeGrossUpAcumMoedaPortfolio = null;
					else entity.RentabilidadeGrossUpAcumMoedaPortfolio = Convert.ToDecimal(value);
				}
			}
				
			public System.String PatrimonioInicialGrossUpMoedaAtivo
			{
				get
				{
					System.Decimal? data = entity.PatrimonioInicialGrossUpMoedaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PatrimonioInicialGrossUpMoedaAtivo = null;
					else entity.PatrimonioInicialGrossUpMoedaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String PatrimonioInicialGrossUpMoedaPortfolio
			{
				get
				{
					System.Decimal? data = entity.PatrimonioInicialGrossUpMoedaPortfolio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PatrimonioInicialGrossUpMoedaPortfolio = null;
					else entity.PatrimonioInicialGrossUpMoedaPortfolio = Convert.ToDecimal(value);
				}
			}
				
			public System.String PatrimonioFinalGrossUpMoedaAtivo
			{
				get
				{
					System.Decimal? data = entity.PatrimonioFinalGrossUpMoedaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PatrimonioFinalGrossUpMoedaAtivo = null;
					else entity.PatrimonioFinalGrossUpMoedaAtivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String PatrimonioFinalGrossUpMoedaPortfolio
			{
				get
				{
					System.Decimal? data = entity.PatrimonioFinalGrossUpMoedaPortfolio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PatrimonioFinalGrossUpMoedaPortfolio = null;
					else entity.PatrimonioFinalGrossUpMoedaPortfolio = Convert.ToDecimal(value);
				}
			}
			

			private esRentabilidade entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esRentabilidadeQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esRentabilidade can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Rentabilidade : esRentabilidade
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esRentabilidadeQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return RentabilidadeMetadata.Meta();
			}
		}	
		

		public esQueryItem IdRentabilidade
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.IdRentabilidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.TipoAtivo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.CodigoAtivo, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoMoedaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.CodigoMoedaAtivo, esSystemType.String);
			}
		} 
		
		public esQueryItem PatrimonioInicialMoedaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.PatrimonioInicialMoedaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeInicialAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.QuantidadeInicialAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorFinanceiroEntradaMoedaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.ValorFinanceiroEntradaMoedaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeTotalEntradaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.QuantidadeTotalEntradaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorFinanceiroSaidaMoedaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.ValorFinanceiroSaidaMoedaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeTotalSaidaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.QuantidadeTotalSaidaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorFinanceiroRendasMoedaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.ValorFinanceiroRendasMoedaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PatrimonioFinalMoedaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.PatrimonioFinalMoedaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeFinalAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.QuantidadeFinalAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RentabilidadeMoedaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.RentabilidadeMoedaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodigoMoedaPortfolio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.CodigoMoedaPortfolio, esSystemType.String);
			}
		} 
		
		public esQueryItem PatrimonioInicialMoedaPortfolio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.PatrimonioInicialMoedaPortfolio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorFinanceiroEntradaMoedaPortfolio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.ValorFinanceiroEntradaMoedaPortfolio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorFinanceiroSaidaMoedaPortfolio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.ValorFinanceiroSaidaMoedaPortfolio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorFinanceiroRendasMoedaPortfolio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.ValorFinanceiroRendasMoedaPortfolio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PatrimonioFinalMoedaPortfolio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.PatrimonioFinalMoedaPortfolio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RentabilidadeMoedaPortfolio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.RentabilidadeMoedaPortfolio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdTituloRendaFixa
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.IdTituloRendaFixa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacaoSwap
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.IdOperacaoSwap, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem CdAtivoBMF
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.CdAtivoBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem SerieBMF
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.SerieBMF, esSystemType.String);
			}
		} 
		
		public esQueryItem CotaGerencial
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.CotaGerencial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RentabilidadeBrutaDiariaMoedaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaDiariaMoedaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RentabilidadeBrutaAcumMoedaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaAcumMoedaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RentabilidadeBrutaDiariaMoedaPortfolio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaDiariaMoedaPortfolio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RentabilidadeBrutaAcumMoedaPortfolio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaAcumMoedaPortfolio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PatrimonioInicialBrutoMoedaPortfolio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.PatrimonioInicialBrutoMoedaPortfolio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PatrimonioInicialBrutoMoedaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.PatrimonioInicialBrutoMoedaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PatrimonioFinalBrutoMoedaPortfolio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.PatrimonioFinalBrutoMoedaPortfolio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PatrimonioFinalBrutoMoedaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.PatrimonioFinalBrutoMoedaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RentabilidadeAtivosIcentivados
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.RentabilidadeAtivosIcentivados, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RentabilidadeAtivosTributados
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.RentabilidadeAtivosTributados, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RentabilidadeGrossUpDiariaMoedaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpDiariaMoedaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RentabilidadeGrossUpAcumMoedaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpAcumMoedaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RentabilidadeGrossUpDiariaMoedaPortfolio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpDiariaMoedaPortfolio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RentabilidadeGrossUpAcumMoedaPortfolio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpAcumMoedaPortfolio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PatrimonioInicialGrossUpMoedaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.PatrimonioInicialGrossUpMoedaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PatrimonioInicialGrossUpMoedaPortfolio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.PatrimonioInicialGrossUpMoedaPortfolio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PatrimonioFinalGrossUpMoedaAtivo
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.PatrimonioFinalGrossUpMoedaAtivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PatrimonioFinalGrossUpMoedaPortfolio
		{
			get
			{
				return new esQueryItem(this, RentabilidadeMetadata.ColumnNames.PatrimonioFinalGrossUpMoedaPortfolio, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("RentabilidadeCollection")]
	public partial class RentabilidadeCollection : esRentabilidadeCollection, IEnumerable<Rentabilidade>
	{
		public RentabilidadeCollection()
		{

		}
		
		public static implicit operator List<Rentabilidade>(RentabilidadeCollection coll)
		{
			List<Rentabilidade> list = new List<Rentabilidade>();
			
			foreach (Rentabilidade emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  RentabilidadeMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new RentabilidadeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Rentabilidade(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Rentabilidade();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public RentabilidadeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new RentabilidadeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(RentabilidadeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Rentabilidade AddNew()
		{
			Rentabilidade entity = base.AddNewEntity() as Rentabilidade;
			
			return entity;
		}

		public Rentabilidade FindByPrimaryKey(System.Int32 idRentabilidade)
		{
			return base.FindByPrimaryKey(idRentabilidade) as Rentabilidade;
		}


		#region IEnumerable<Rentabilidade> Members

		IEnumerator<Rentabilidade> IEnumerable<Rentabilidade>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Rentabilidade;
			}
		}

		#endregion
		
		private RentabilidadeQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Rentabilidade' table
	/// </summary>

	[Serializable]
	public partial class Rentabilidade : esRentabilidade
	{
		public Rentabilidade()
		{

		}
	
		public Rentabilidade(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return RentabilidadeMetadata.Meta();
			}
		}
		
		
		
		override protected esRentabilidadeQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new RentabilidadeQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public RentabilidadeQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new RentabilidadeQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(RentabilidadeQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private RentabilidadeQuery query;
	}



	[Serializable]
	public partial class RentabilidadeQuery : esRentabilidadeQuery
	{
		public RentabilidadeQuery()
		{

		}		
		
		public RentabilidadeQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class RentabilidadeMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected RentabilidadeMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.IdRentabilidade, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.IdRentabilidade;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.TipoAtivo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.TipoAtivo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.IdCliente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.CodigoAtivo, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.CodigoAtivo;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.CodigoMoedaAtivo, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.CodigoMoedaAtivo;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.PatrimonioInicialMoedaAtivo, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.PatrimonioInicialMoedaAtivo;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.QuantidadeInicialAtivo, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.QuantidadeInicialAtivo;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.ValorFinanceiroEntradaMoedaAtivo, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.ValorFinanceiroEntradaMoedaAtivo;	
			c.NumericPrecision = 25;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.QuantidadeTotalEntradaAtivo, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.QuantidadeTotalEntradaAtivo;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.ValorFinanceiroSaidaMoedaAtivo, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.ValorFinanceiroSaidaMoedaAtivo;	
			c.NumericPrecision = 25;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.QuantidadeTotalSaidaAtivo, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.QuantidadeTotalSaidaAtivo;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.ValorFinanceiroRendasMoedaAtivo, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.ValorFinanceiroRendasMoedaAtivo;	
			c.NumericPrecision = 25;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.PatrimonioFinalMoedaAtivo, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.PatrimonioFinalMoedaAtivo;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.QuantidadeFinalAtivo, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.QuantidadeFinalAtivo;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.RentabilidadeMoedaAtivo, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.RentabilidadeMoedaAtivo;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.CodigoMoedaPortfolio, 16, typeof(System.String), esSystemType.String);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.CodigoMoedaPortfolio;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.PatrimonioInicialMoedaPortfolio, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.PatrimonioInicialMoedaPortfolio;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.ValorFinanceiroEntradaMoedaPortfolio, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.ValorFinanceiroEntradaMoedaPortfolio;	
			c.NumericPrecision = 25;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.ValorFinanceiroSaidaMoedaPortfolio, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.ValorFinanceiroSaidaMoedaPortfolio;	
			c.NumericPrecision = 25;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.ValorFinanceiroRendasMoedaPortfolio, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.ValorFinanceiroRendasMoedaPortfolio;	
			c.NumericPrecision = 25;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.PatrimonioFinalMoedaPortfolio, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.PatrimonioFinalMoedaPortfolio;	
			c.NumericPrecision = 25;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.RentabilidadeMoedaPortfolio, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.RentabilidadeMoedaPortfolio;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.IdTituloRendaFixa, 23, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.IdTituloRendaFixa;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.IdOperacaoSwap, 24, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.IdOperacaoSwap;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.IdCarteira, 25, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.CdAtivoBolsa, 26, typeof(System.String), esSystemType.String);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.CdAtivoBMF, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.CdAtivoBMF;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.SerieBMF, 28, typeof(System.String), esSystemType.String);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.SerieBMF;
			c.CharacterMaxLength = 5;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.CotaGerencial, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.CotaGerencial;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaDiariaMoedaAtivo, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.RentabilidadeBrutaDiariaMoedaAtivo;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaAcumMoedaAtivo, 31, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.RentabilidadeBrutaAcumMoedaAtivo;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaDiariaMoedaPortfolio, 32, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.RentabilidadeBrutaDiariaMoedaPortfolio;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.RentabilidadeBrutaAcumMoedaPortfolio, 33, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.RentabilidadeBrutaAcumMoedaPortfolio;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.PatrimonioInicialBrutoMoedaPortfolio, 34, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.PatrimonioInicialBrutoMoedaPortfolio;	
			c.NumericPrecision = 28;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.PatrimonioInicialBrutoMoedaAtivo, 35, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.PatrimonioInicialBrutoMoedaAtivo;	
			c.NumericPrecision = 28;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.PatrimonioFinalBrutoMoedaPortfolio, 36, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.PatrimonioFinalBrutoMoedaPortfolio;	
			c.NumericPrecision = 28;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.PatrimonioFinalBrutoMoedaAtivo, 37, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.PatrimonioFinalBrutoMoedaAtivo;	
			c.NumericPrecision = 28;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.RentabilidadeAtivosIcentivados, 38, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.RentabilidadeAtivosIcentivados;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.RentabilidadeAtivosTributados, 39, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.RentabilidadeAtivosTributados;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpDiariaMoedaAtivo, 40, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.RentabilidadeGrossUpDiariaMoedaAtivo;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpAcumMoedaAtivo, 41, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.RentabilidadeGrossUpAcumMoedaAtivo;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpDiariaMoedaPortfolio, 42, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.RentabilidadeGrossUpDiariaMoedaPortfolio;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.RentabilidadeGrossUpAcumMoedaPortfolio, 43, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.RentabilidadeGrossUpAcumMoedaPortfolio;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.PatrimonioInicialGrossUpMoedaAtivo, 44, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.PatrimonioInicialGrossUpMoedaAtivo;	
			c.NumericPrecision = 28;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.PatrimonioInicialGrossUpMoedaPortfolio, 45, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.PatrimonioInicialGrossUpMoedaPortfolio;	
			c.NumericPrecision = 28;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.PatrimonioFinalGrossUpMoedaAtivo, 46, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.PatrimonioFinalGrossUpMoedaAtivo;	
			c.NumericPrecision = 28;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RentabilidadeMetadata.ColumnNames.PatrimonioFinalGrossUpMoedaPortfolio, 47, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = RentabilidadeMetadata.PropertyNames.PatrimonioFinalGrossUpMoedaPortfolio;	
			c.NumericPrecision = 28;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public RentabilidadeMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdRentabilidade = "IdRentabilidade";
			 public const string Data = "Data";
			 public const string TipoAtivo = "TipoAtivo";
			 public const string IdCliente = "IdCliente";
			 public const string CodigoAtivo = "CodigoAtivo";
			 public const string CodigoMoedaAtivo = "CodigoMoedaAtivo";
			 public const string PatrimonioInicialMoedaAtivo = "PatrimonioInicialMoedaAtivo";
			 public const string QuantidadeInicialAtivo = "QuantidadeInicialAtivo";
			 public const string ValorFinanceiroEntradaMoedaAtivo = "ValorFinanceiroEntradaMoedaAtivo";
			 public const string QuantidadeTotalEntradaAtivo = "QuantidadeTotalEntradaAtivo";
			 public const string ValorFinanceiroSaidaMoedaAtivo = "ValorFinanceiroSaidaMoedaAtivo";
			 public const string QuantidadeTotalSaidaAtivo = "QuantidadeTotalSaidaAtivo";
			 public const string ValorFinanceiroRendasMoedaAtivo = "ValorFinanceiroRendasMoedaAtivo";
			 public const string PatrimonioFinalMoedaAtivo = "PatrimonioFinalMoedaAtivo";
			 public const string QuantidadeFinalAtivo = "QuantidadeFinalAtivo";
			 public const string RentabilidadeMoedaAtivo = "RentabilidadeMoedaAtivo";
			 public const string CodigoMoedaPortfolio = "CodigoMoedaPortfolio";
			 public const string PatrimonioInicialMoedaPortfolio = "PatrimonioInicialMoedaPortfolio";
			 public const string ValorFinanceiroEntradaMoedaPortfolio = "ValorFinanceiroEntradaMoedaPortfolio";
			 public const string ValorFinanceiroSaidaMoedaPortfolio = "ValorFinanceiroSaidaMoedaPortfolio";
			 public const string ValorFinanceiroRendasMoedaPortfolio = "ValorFinanceiroRendasMoedaPortfolio";
			 public const string PatrimonioFinalMoedaPortfolio = "PatrimonioFinalMoedaPortfolio";
			 public const string RentabilidadeMoedaPortfolio = "RentabilidadeMoedaPortfolio";
			 public const string IdTituloRendaFixa = "IdTituloRendaFixa";
			 public const string IdOperacaoSwap = "IdOperacaoSwap";
			 public const string IdCarteira = "IdCarteira";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string SerieBMF = "SerieBMF";
			 public const string CotaGerencial = "CotaGerencial";
			 public const string RentabilidadeBrutaDiariaMoedaAtivo = "RentabilidadeBrutaDiariaMoedaAtivo";
			 public const string RentabilidadeBrutaAcumMoedaAtivo = "RentabilidadeBrutaAcumMoedaAtivo";
			 public const string RentabilidadeBrutaDiariaMoedaPortfolio = "RentabilidadeBrutaDiariaMoedaPortfolio";
			 public const string RentabilidadeBrutaAcumMoedaPortfolio = "RentabilidadeBrutaAcumMoedaPortfolio";
			 public const string PatrimonioInicialBrutoMoedaPortfolio = "PatrimonioInicialBrutoMoedaPortfolio";
			 public const string PatrimonioInicialBrutoMoedaAtivo = "PatrimonioInicialBrutoMoedaAtivo";
			 public const string PatrimonioFinalBrutoMoedaPortfolio = "PatrimonioFinalBrutoMoedaPortfolio";
			 public const string PatrimonioFinalBrutoMoedaAtivo = "PatrimonioFinalBrutoMoedaAtivo";
			 public const string RentabilidadeAtivosIcentivados = "RentabilidadeAtivosIcentivados";
			 public const string RentabilidadeAtivosTributados = "RentabilidadeAtivosTributados";
			 public const string RentabilidadeGrossUpDiariaMoedaAtivo = "RentabilidadeGrossUpDiariaMoedaAtivo";
			 public const string RentabilidadeGrossUpAcumMoedaAtivo = "RentabilidadeGrossUpAcumMoedaAtivo";
			 public const string RentabilidadeGrossUpDiariaMoedaPortfolio = "RentabilidadeGrossUpDiariaMoedaPortfolio";
			 public const string RentabilidadeGrossUpAcumMoedaPortfolio = "RentabilidadeGrossUpAcumMoedaPortfolio";
			 public const string PatrimonioInicialGrossUpMoedaAtivo = "PatrimonioInicialGrossUpMoedaAtivo";
			 public const string PatrimonioInicialGrossUpMoedaPortfolio = "PatrimonioInicialGrossUpMoedaPortfolio";
			 public const string PatrimonioFinalGrossUpMoedaAtivo = "PatrimonioFinalGrossUpMoedaAtivo";
			 public const string PatrimonioFinalGrossUpMoedaPortfolio = "PatrimonioFinalGrossUpMoedaPortfolio";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdRentabilidade = "IdRentabilidade";
			 public const string Data = "Data";
			 public const string TipoAtivo = "TipoAtivo";
			 public const string IdCliente = "IdCliente";
			 public const string CodigoAtivo = "CodigoAtivo";
			 public const string CodigoMoedaAtivo = "CodigoMoedaAtivo";
			 public const string PatrimonioInicialMoedaAtivo = "PatrimonioInicialMoedaAtivo";
			 public const string QuantidadeInicialAtivo = "QuantidadeInicialAtivo";
			 public const string ValorFinanceiroEntradaMoedaAtivo = "ValorFinanceiroEntradaMoedaAtivo";
			 public const string QuantidadeTotalEntradaAtivo = "QuantidadeTotalEntradaAtivo";
			 public const string ValorFinanceiroSaidaMoedaAtivo = "ValorFinanceiroSaidaMoedaAtivo";
			 public const string QuantidadeTotalSaidaAtivo = "QuantidadeTotalSaidaAtivo";
			 public const string ValorFinanceiroRendasMoedaAtivo = "ValorFinanceiroRendasMoedaAtivo";
			 public const string PatrimonioFinalMoedaAtivo = "PatrimonioFinalMoedaAtivo";
			 public const string QuantidadeFinalAtivo = "QuantidadeFinalAtivo";
			 public const string RentabilidadeMoedaAtivo = "RentabilidadeMoedaAtivo";
			 public const string CodigoMoedaPortfolio = "CodigoMoedaPortfolio";
			 public const string PatrimonioInicialMoedaPortfolio = "PatrimonioInicialMoedaPortfolio";
			 public const string ValorFinanceiroEntradaMoedaPortfolio = "ValorFinanceiroEntradaMoedaPortfolio";
			 public const string ValorFinanceiroSaidaMoedaPortfolio = "ValorFinanceiroSaidaMoedaPortfolio";
			 public const string ValorFinanceiroRendasMoedaPortfolio = "ValorFinanceiroRendasMoedaPortfolio";
			 public const string PatrimonioFinalMoedaPortfolio = "PatrimonioFinalMoedaPortfolio";
			 public const string RentabilidadeMoedaPortfolio = "RentabilidadeMoedaPortfolio";
			 public const string IdTituloRendaFixa = "IdTituloRendaFixa";
			 public const string IdOperacaoSwap = "IdOperacaoSwap";
			 public const string IdCarteira = "IdCarteira";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string CdAtivoBMF = "CdAtivoBMF";
			 public const string SerieBMF = "SerieBMF";
			 public const string CotaGerencial = "CotaGerencial";
			 public const string RentabilidadeBrutaDiariaMoedaAtivo = "RentabilidadeBrutaDiariaMoedaAtivo";
			 public const string RentabilidadeBrutaAcumMoedaAtivo = "RentabilidadeBrutaAcumMoedaAtivo";
			 public const string RentabilidadeBrutaDiariaMoedaPortfolio = "RentabilidadeBrutaDiariaMoedaPortfolio";
			 public const string RentabilidadeBrutaAcumMoedaPortfolio = "RentabilidadeBrutaAcumMoedaPortfolio";
			 public const string PatrimonioInicialBrutoMoedaPortfolio = "PatrimonioInicialBrutoMoedaPortfolio";
			 public const string PatrimonioInicialBrutoMoedaAtivo = "PatrimonioInicialBrutoMoedaAtivo";
			 public const string PatrimonioFinalBrutoMoedaPortfolio = "PatrimonioFinalBrutoMoedaPortfolio";
			 public const string PatrimonioFinalBrutoMoedaAtivo = "PatrimonioFinalBrutoMoedaAtivo";
			 public const string RentabilidadeAtivosIcentivados = "RentabilidadeAtivosIcentivados";
			 public const string RentabilidadeAtivosTributados = "RentabilidadeAtivosTributados";
			 public const string RentabilidadeGrossUpDiariaMoedaAtivo = "RentabilidadeGrossUpDiariaMoedaAtivo";
			 public const string RentabilidadeGrossUpAcumMoedaAtivo = "RentabilidadeGrossUpAcumMoedaAtivo";
			 public const string RentabilidadeGrossUpDiariaMoedaPortfolio = "RentabilidadeGrossUpDiariaMoedaPortfolio";
			 public const string RentabilidadeGrossUpAcumMoedaPortfolio = "RentabilidadeGrossUpAcumMoedaPortfolio";
			 public const string PatrimonioInicialGrossUpMoedaAtivo = "PatrimonioInicialGrossUpMoedaAtivo";
			 public const string PatrimonioInicialGrossUpMoedaPortfolio = "PatrimonioInicialGrossUpMoedaPortfolio";
			 public const string PatrimonioFinalGrossUpMoedaAtivo = "PatrimonioFinalGrossUpMoedaAtivo";
			 public const string PatrimonioFinalGrossUpMoedaPortfolio = "PatrimonioFinalGrossUpMoedaPortfolio";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(RentabilidadeMetadata))
			{
				if(RentabilidadeMetadata.mapDelegates == null)
				{
					RentabilidadeMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (RentabilidadeMetadata.meta == null)
				{
					RentabilidadeMetadata.meta = new RentabilidadeMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdRentabilidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoAtivo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoAtivo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoMoedaAtivo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PatrimonioInicialMoedaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeInicialAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorFinanceiroEntradaMoedaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeTotalEntradaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorFinanceiroSaidaMoedaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeTotalSaidaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorFinanceiroRendasMoedaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PatrimonioFinalMoedaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeFinalAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RentabilidadeMoedaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CodigoMoedaPortfolio", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PatrimonioInicialMoedaPortfolio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorFinanceiroEntradaMoedaPortfolio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorFinanceiroSaidaMoedaPortfolio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorFinanceiroRendasMoedaPortfolio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PatrimonioFinalMoedaPortfolio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RentabilidadeMoedaPortfolio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdTituloRendaFixa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacaoSwap", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CdAtivoBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("SerieBMF", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CotaGerencial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RentabilidadeBrutaDiariaMoedaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RentabilidadeBrutaAcumMoedaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RentabilidadeBrutaDiariaMoedaPortfolio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RentabilidadeBrutaAcumMoedaPortfolio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PatrimonioInicialBrutoMoedaPortfolio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PatrimonioInicialBrutoMoedaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PatrimonioFinalBrutoMoedaPortfolio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PatrimonioFinalBrutoMoedaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RentabilidadeAtivosIcentivados", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RentabilidadeAtivosTributados", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RentabilidadeGrossUpDiariaMoedaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RentabilidadeGrossUpAcumMoedaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RentabilidadeGrossUpDiariaMoedaPortfolio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RentabilidadeGrossUpAcumMoedaPortfolio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PatrimonioInicialGrossUpMoedaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PatrimonioInicialGrossUpMoedaPortfolio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PatrimonioFinalGrossUpMoedaAtivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PatrimonioFinalGrossUpMoedaPortfolio", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "Rentabilidade";
				meta.Destination = "Rentabilidade";
				
				meta.spInsert = "proc_RentabilidadeInsert";				
				meta.spUpdate = "proc_RentabilidadeUpdate";		
				meta.spDelete = "proc_RentabilidadeDelete";
				meta.spLoadAll = "proc_RentabilidadeLoadAll";
				meta.spLoadByPrimaryKey = "proc_RentabilidadeLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private RentabilidadeMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
