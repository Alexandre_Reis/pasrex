/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:17 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		





		








		
				

using Financial.Enquadra;
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esCategoriaFundoCollection : esEntityCollection
	{
		public esCategoriaFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CategoriaFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esCategoriaFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCategoriaFundoQuery);
		}
		#endregion
		
		virtual public CategoriaFundo DetachEntity(CategoriaFundo entity)
		{
			return base.DetachEntity(entity) as CategoriaFundo;
		}
		
		virtual public CategoriaFundo AttachEntity(CategoriaFundo entity)
		{
			return base.AttachEntity(entity) as CategoriaFundo;
		}
		
		virtual public void Combine(CategoriaFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CategoriaFundo this[int index]
		{
			get
			{
				return base[index] as CategoriaFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CategoriaFundo);
		}
	}



	[Serializable]
	abstract public class esCategoriaFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCategoriaFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esCategoriaFundo()
		{

		}

		public esCategoriaFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCategoria)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCategoria);
			else
				return LoadByPrimaryKeyStoredProcedure(idCategoria);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCategoria)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCategoriaFundoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCategoria == idCategoria);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCategoria)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCategoria);
			else
				return LoadByPrimaryKeyStoredProcedure(idCategoria);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCategoria)
		{
			esCategoriaFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdCategoria == idCategoria);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCategoria)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCategoria",idCategoria);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCategoria": this.str.IdCategoria = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "IdLista": this.str.IdLista = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCategoria":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCategoria = (System.Int32?)value;
							break;
						
						case "IdLista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLista = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CategoriaFundo.IdCategoria
		/// </summary>
		virtual public System.Int32? IdCategoria
		{
			get
			{
				return base.GetSystemInt32(CategoriaFundoMetadata.ColumnNames.IdCategoria);
			}
			
			set
			{
				base.SetSystemInt32(CategoriaFundoMetadata.ColumnNames.IdCategoria, value);
			}
		}
		
		/// <summary>
		/// Maps to CategoriaFundo.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(CategoriaFundoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(CategoriaFundoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to CategoriaFundo.IdLista
		/// </summary>
		virtual public System.Int32? IdLista
		{
			get
			{
				return base.GetSystemInt32(CategoriaFundoMetadata.ColumnNames.IdLista);
			}
			
			set
			{
				if(base.SetSystemInt32(CategoriaFundoMetadata.ColumnNames.IdLista, value))
				{
					this._UpToListaCategoriaFundoByIdLista = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected ListaCategoriaFundo _UpToListaCategoriaFundoByIdLista;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCategoriaFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCategoria
			{
				get
				{
					System.Int32? data = entity.IdCategoria;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCategoria = null;
					else entity.IdCategoria = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String IdLista
			{
				get
				{
					System.Int32? data = entity.IdLista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLista = null;
					else entity.IdLista = Convert.ToInt32(value);
				}
			}
			

			private esCategoriaFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCategoriaFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCategoriaFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CategoriaFundo : esCategoriaFundo
	{

				
		#region CarteiraCollectionByIdCategoria - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - CategoriaFundo_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public CarteiraCollection CarteiraCollectionByIdCategoria
		{
			get
			{
				if(this._CarteiraCollectionByIdCategoria == null)
				{
					this._CarteiraCollectionByIdCategoria = new CarteiraCollection();
					this._CarteiraCollectionByIdCategoria.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CarteiraCollectionByIdCategoria", this._CarteiraCollectionByIdCategoria);
				
					if(this.IdCategoria != null)
					{
						this._CarteiraCollectionByIdCategoria.Query.Where(this._CarteiraCollectionByIdCategoria.Query.IdCategoria == this.IdCategoria);
						this._CarteiraCollectionByIdCategoria.Query.Load();

						// Auto-hookup Foreign Keys
						this._CarteiraCollectionByIdCategoria.fks.Add(CarteiraMetadata.ColumnNames.IdCategoria, this.IdCategoria);
					}
				}

				return this._CarteiraCollectionByIdCategoria;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CarteiraCollectionByIdCategoria != null) 
				{ 
					this.RemovePostSave("CarteiraCollectionByIdCategoria"); 
					this._CarteiraCollectionByIdCategoria = null;
					
				} 
			} 			
		}

		private CarteiraCollection _CarteiraCollectionByIdCategoria;
		#endregion

				
		#region SubCategoriaFundoCollectionByIdCategoria - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - CategoriaFundo_SubCategoriaFundo_FK1
		/// </summary>

		[XmlIgnore]
		public SubCategoriaFundoCollection SubCategoriaFundoCollectionByIdCategoria
		{
			get
			{
				if(this._SubCategoriaFundoCollectionByIdCategoria == null)
				{
					this._SubCategoriaFundoCollectionByIdCategoria = new SubCategoriaFundoCollection();
					this._SubCategoriaFundoCollectionByIdCategoria.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("SubCategoriaFundoCollectionByIdCategoria", this._SubCategoriaFundoCollectionByIdCategoria);
				
					if(this.IdCategoria != null)
					{
						this._SubCategoriaFundoCollectionByIdCategoria.Query.Where(this._SubCategoriaFundoCollectionByIdCategoria.Query.IdCategoria == this.IdCategoria);
						this._SubCategoriaFundoCollectionByIdCategoria.Query.Load();

						// Auto-hookup Foreign Keys
						this._SubCategoriaFundoCollectionByIdCategoria.fks.Add(SubCategoriaFundoMetadata.ColumnNames.IdCategoria, this.IdCategoria);
					}
				}

				return this._SubCategoriaFundoCollectionByIdCategoria;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._SubCategoriaFundoCollectionByIdCategoria != null) 
				{ 
					this.RemovePostSave("SubCategoriaFundoCollectionByIdCategoria"); 
					this._SubCategoriaFundoCollectionByIdCategoria = null;
					
				} 
			} 			
		}

		private SubCategoriaFundoCollection _SubCategoriaFundoCollectionByIdCategoria;
		#endregion

				
		#region UpToListaCategoriaFundoByIdLista - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ListaCategoriaFundo_CategoriaFundo_FK1
		/// </summary>

		[XmlIgnore]
		public ListaCategoriaFundo UpToListaCategoriaFundoByIdLista
		{
			get
			{
				if(this._UpToListaCategoriaFundoByIdLista == null
					&& IdLista != null					)
				{
					this._UpToListaCategoriaFundoByIdLista = new ListaCategoriaFundo();
					this._UpToListaCategoriaFundoByIdLista.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToListaCategoriaFundoByIdLista", this._UpToListaCategoriaFundoByIdLista);
					this._UpToListaCategoriaFundoByIdLista.Query.Where(this._UpToListaCategoriaFundoByIdLista.Query.IdLista == this.IdLista);
					this._UpToListaCategoriaFundoByIdLista.Query.Load();
				}

				return this._UpToListaCategoriaFundoByIdLista;
			}
			
			set
			{
				this.RemovePreSave("UpToListaCategoriaFundoByIdLista");
				

				if(value == null)
				{
					this.IdLista = null;
					this._UpToListaCategoriaFundoByIdLista = null;
				}
				else
				{
					this.IdLista = value.IdLista;
					this._UpToListaCategoriaFundoByIdLista = value;
					this.SetPreSave("UpToListaCategoriaFundoByIdLista", this._UpToListaCategoriaFundoByIdLista);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "CarteiraCollectionByIdCategoria", typeof(CarteiraCollection), new Carteira()));
			props.Add(new esPropertyDescriptor(this, "SubCategoriaFundoCollectionByIdCategoria", typeof(SubCategoriaFundoCollection), new SubCategoriaFundo()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToListaCategoriaFundoByIdLista != null)
			{
				this.IdLista = this._UpToListaCategoriaFundoByIdLista.IdLista;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._CarteiraCollectionByIdCategoria != null)
			{
				foreach(Carteira obj in this._CarteiraCollectionByIdCategoria)
				{
					if(obj.es.IsAdded)
					{
						obj.IdCategoria = this.IdCategoria;
					}
				}
			}
			if(this._SubCategoriaFundoCollectionByIdCategoria != null)
			{
				foreach(SubCategoriaFundo obj in this._SubCategoriaFundoCollectionByIdCategoria)
				{
					if(obj.es.IsAdded)
					{
						obj.IdCategoria = this.IdCategoria;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCategoriaFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CategoriaFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCategoria
		{
			get
			{
				return new esQueryItem(this, CategoriaFundoMetadata.ColumnNames.IdCategoria, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, CategoriaFundoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdLista
		{
			get
			{
				return new esQueryItem(this, CategoriaFundoMetadata.ColumnNames.IdLista, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CategoriaFundoCollection")]
	public partial class CategoriaFundoCollection : esCategoriaFundoCollection, IEnumerable<CategoriaFundo>
	{
		public CategoriaFundoCollection()
		{

		}
		
		public static implicit operator List<CategoriaFundo>(CategoriaFundoCollection coll)
		{
			List<CategoriaFundo> list = new List<CategoriaFundo>();
			
			foreach (CategoriaFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CategoriaFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CategoriaFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CategoriaFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CategoriaFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CategoriaFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CategoriaFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CategoriaFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CategoriaFundo AddNew()
		{
			CategoriaFundo entity = base.AddNewEntity() as CategoriaFundo;
			
			return entity;
		}

		public CategoriaFundo FindByPrimaryKey(System.Int32 idCategoria)
		{
			return base.FindByPrimaryKey(idCategoria) as CategoriaFundo;
		}


		#region IEnumerable<CategoriaFundo> Members

		IEnumerator<CategoriaFundo> IEnumerable<CategoriaFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CategoriaFundo;
			}
		}

		#endregion
		
		private CategoriaFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CategoriaFundo' table
	/// </summary>

	[Serializable]
	public partial class CategoriaFundo : esCategoriaFundo
	{
		public CategoriaFundo()
		{

		}
	
		public CategoriaFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CategoriaFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esCategoriaFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CategoriaFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CategoriaFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CategoriaFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CategoriaFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CategoriaFundoQuery query;
	}



	[Serializable]
	public partial class CategoriaFundoQuery : esCategoriaFundoQuery
	{
		public CategoriaFundoQuery()
		{

		}		
		
		public CategoriaFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CategoriaFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CategoriaFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CategoriaFundoMetadata.ColumnNames.IdCategoria, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CategoriaFundoMetadata.PropertyNames.IdCategoria;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CategoriaFundoMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CategoriaFundoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CategoriaFundoMetadata.ColumnNames.IdLista, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CategoriaFundoMetadata.PropertyNames.IdLista;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CategoriaFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCategoria = "IdCategoria";
			 public const string Descricao = "Descricao";
			 public const string IdLista = "IdLista";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCategoria = "IdCategoria";
			 public const string Descricao = "Descricao";
			 public const string IdLista = "IdLista";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CategoriaFundoMetadata))
			{
				if(CategoriaFundoMetadata.mapDelegates == null)
				{
					CategoriaFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CategoriaFundoMetadata.meta == null)
				{
					CategoriaFundoMetadata.meta = new CategoriaFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCategoria", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdLista", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "CategoriaFundo";
				meta.Destination = "CategoriaFundo";
				
				meta.spInsert = "proc_CategoriaFundoInsert";				
				meta.spUpdate = "proc_CategoriaFundoUpdate";		
				meta.spDelete = "proc_CategoriaFundoDelete";
				meta.spLoadAll = "proc_CategoriaFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_CategoriaFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CategoriaFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
