/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/03/2016 16:10:55
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esAgendaFundoCollection : esEntityCollection
	{
		public esAgendaFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AgendaFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esAgendaFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAgendaFundoQuery);
		}
		#endregion
		
		virtual public AgendaFundo DetachEntity(AgendaFundo entity)
		{
			return base.DetachEntity(entity) as AgendaFundo;
		}
		
		virtual public AgendaFundo AttachEntity(AgendaFundo entity)
		{
			return base.AttachEntity(entity) as AgendaFundo;
		}
		
		virtual public void Combine(AgendaFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public AgendaFundo this[int index]
		{
			get
			{
				return base[index] as AgendaFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(AgendaFundo);
		}
	}



	[Serializable]
	abstract public class esAgendaFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAgendaFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esAgendaFundo()
		{

		}

		public esAgendaFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idAgenda)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgenda);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgenda);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idAgenda)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esAgendaFundoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdAgenda == idAgenda);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idAgenda)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgenda);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgenda);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idAgenda)
		{
			esAgendaFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdAgenda == idAgenda);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idAgenda)
		{
			esParameters parms = new esParameters();
			parms.Add("IdAgenda",idAgenda);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdAgenda": this.str.IdAgenda = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "TipoEvento": this.str.TipoEvento = (string)value; break;							
						case "DataEvento": this.str.DataEvento = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;							
						case "TipoValorInput": this.str.TipoValorInput = (string)value; break;							
						case "ImpactarCota": this.str.ImpactarCota = (string)value; break;							
						case "CotaFechamentoCheia": this.str.CotaFechamentoCheia = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdAgenda":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenda = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "TipoEvento":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoEvento = (System.Byte?)value;
							break;
						
						case "DataEvento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEvento = (System.DateTime?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
						
						case "TipoValorInput":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoValorInput = (System.Int32?)value;
							break;
						
						case "CotaFechamentoCheia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaFechamentoCheia = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to AgendaFundo.IdAgenda
		/// </summary>
		virtual public System.Int32? IdAgenda
		{
			get
			{
				return base.GetSystemInt32(AgendaFundoMetadata.ColumnNames.IdAgenda);
			}
			
			set
			{
				base.SetSystemInt32(AgendaFundoMetadata.ColumnNames.IdAgenda, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaFundo.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(AgendaFundoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(AgendaFundoMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AgendaFundo.TipoEvento
		/// </summary>
		virtual public System.Byte? TipoEvento
		{
			get
			{
				return base.GetSystemByte(AgendaFundoMetadata.ColumnNames.TipoEvento);
			}
			
			set
			{
				base.SetSystemByte(AgendaFundoMetadata.ColumnNames.TipoEvento, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaFundo.DataEvento
		/// </summary>
		virtual public System.DateTime? DataEvento
		{
			get
			{
				return base.GetSystemDateTime(AgendaFundoMetadata.ColumnNames.DataEvento);
			}
			
			set
			{
				base.SetSystemDateTime(AgendaFundoMetadata.ColumnNames.DataEvento, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaFundo.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(AgendaFundoMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaFundoMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaFundo.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(AgendaFundoMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaFundoMetadata.ColumnNames.Taxa, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaFundo.TipoValorInput
		/// </summary>
		virtual public System.Int32? TipoValorInput
		{
			get
			{
				return base.GetSystemInt32(AgendaFundoMetadata.ColumnNames.TipoValorInput);
			}
			
			set
			{
				base.SetSystemInt32(AgendaFundoMetadata.ColumnNames.TipoValorInput, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaFundo.ImpactarCota
		/// </summary>
		virtual public System.String ImpactarCota
		{
			get
			{
				return base.GetSystemString(AgendaFundoMetadata.ColumnNames.ImpactarCota);
			}
			
			set
			{
				base.SetSystemString(AgendaFundoMetadata.ColumnNames.ImpactarCota, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaFundo.CotaFechamentoCheia
		/// </summary>
		virtual public System.Decimal? CotaFechamentoCheia
		{
			get
			{
				return base.GetSystemDecimal(AgendaFundoMetadata.ColumnNames.CotaFechamentoCheia);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaFundoMetadata.ColumnNames.CotaFechamentoCheia, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAgendaFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdAgenda
			{
				get
				{
					System.Int32? data = entity.IdAgenda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenda = null;
					else entity.IdAgenda = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoEvento
			{
				get
				{
					System.Byte? data = entity.TipoEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoEvento = null;
					else entity.TipoEvento = Convert.ToByte(value);
				}
			}
				
			public System.String DataEvento
			{
				get
				{
					System.DateTime? data = entity.DataEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEvento = null;
					else entity.DataEvento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoValorInput
			{
				get
				{
					System.Int32? data = entity.TipoValorInput;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoValorInput = null;
					else entity.TipoValorInput = Convert.ToInt32(value);
				}
			}
				
			public System.String ImpactarCota
			{
				get
				{
					System.String data = entity.ImpactarCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ImpactarCota = null;
					else entity.ImpactarCota = Convert.ToString(value);
				}
			}

			public System.String CotaFechamentoCheia
			{
				get
				{
					System.Decimal? data = entity.CotaFechamentoCheia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaFechamentoCheia = null;
					else entity.CotaFechamentoCheia = Convert.ToDecimal(value);
				}
			}
			
			private esAgendaFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAgendaFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAgendaFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class AgendaFundo : esAgendaFundo
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_AgendaFundo_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAgendaFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AgendaFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdAgenda
		{
			get
			{
				return new esQueryItem(this, AgendaFundoMetadata.ColumnNames.IdAgenda, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, AgendaFundoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoEvento
		{
			get
			{
				return new esQueryItem(this, AgendaFundoMetadata.ColumnNames.TipoEvento, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataEvento
		{
			get
			{
				return new esQueryItem(this, AgendaFundoMetadata.ColumnNames.DataEvento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, AgendaFundoMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, AgendaFundoMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoValorInput
		{
			get
			{
				return new esQueryItem(this, AgendaFundoMetadata.ColumnNames.TipoValorInput, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ImpactarCota
		{
			get
			{
				return new esQueryItem(this, AgendaFundoMetadata.ColumnNames.ImpactarCota, esSystemType.String);
			}
		} 
		
		public esQueryItem CotaFechamentoCheia
		{
			get
			{
				return new esQueryItem(this, AgendaFundoMetadata.ColumnNames.CotaFechamentoCheia, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("AgendaFundoCollection")]
	public partial class AgendaFundoCollection : esAgendaFundoCollection, IEnumerable<AgendaFundo>
	{
		public AgendaFundoCollection()
		{

		}
		
		public static implicit operator List<AgendaFundo>(AgendaFundoCollection coll)
		{
			List<AgendaFundo> list = new List<AgendaFundo>();
			
			foreach (AgendaFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AgendaFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgendaFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new AgendaFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new AgendaFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AgendaFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgendaFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AgendaFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public AgendaFundo AddNew()
		{
			AgendaFundo entity = base.AddNewEntity() as AgendaFundo;
			
			return entity;
		}

		public AgendaFundo FindByPrimaryKey(System.Int32 idAgenda)
		{
			return base.FindByPrimaryKey(idAgenda) as AgendaFundo;
		}


		#region IEnumerable<AgendaFundo> Members

		IEnumerator<AgendaFundo> IEnumerable<AgendaFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as AgendaFundo;
			}
		}

		#endregion
		
		private AgendaFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'AgendaFundo' table
	/// </summary>

	[Serializable]
	public partial class AgendaFundo : esAgendaFundo
	{
		public AgendaFundo()
		{

		}
	
		public AgendaFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AgendaFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esAgendaFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgendaFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AgendaFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgendaFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AgendaFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AgendaFundoQuery query;
	}



	[Serializable]
	public partial class AgendaFundoQuery : esAgendaFundoQuery
	{
		public AgendaFundoQuery()
		{

		}		
		
		public AgendaFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AgendaFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AgendaFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AgendaFundoMetadata.ColumnNames.IdAgenda, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaFundoMetadata.PropertyNames.IdAgenda;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaFundoMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaFundoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaFundoMetadata.ColumnNames.TipoEvento, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = AgendaFundoMetadata.PropertyNames.TipoEvento;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaFundoMetadata.ColumnNames.DataEvento, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AgendaFundoMetadata.PropertyNames.DataEvento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaFundoMetadata.ColumnNames.Valor, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaFundoMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 30;
			c.NumericScale = 20;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaFundoMetadata.ColumnNames.Taxa, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaFundoMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 30;
			c.NumericScale = 20;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaFundoMetadata.ColumnNames.TipoValorInput, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaFundoMetadata.PropertyNames.TipoValorInput;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((1))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaFundoMetadata.ColumnNames.ImpactarCota, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = AgendaFundoMetadata.PropertyNames.ImpactarCota;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('S')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaFundoMetadata.ColumnNames.CotaFechamentoCheia, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaFundoMetadata.PropertyNames.CotaFechamentoCheia;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
		}
		#endregion
	
		static public AgendaFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdAgenda = "IdAgenda";
			 public const string IdCarteira = "IdCarteira";
			 public const string TipoEvento = "TipoEvento";
			 public const string DataEvento = "DataEvento";
			 public const string Valor = "Valor";
			 public const string Taxa = "Taxa";
			 public const string TipoValorInput = "TipoValorInput";
			 public const string ImpactarCota = "ImpactarCota";
			 public const string CotaFechamentoCheia = "CotaFechamentoCheia";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdAgenda = "IdAgenda";
			 public const string IdCarteira = "IdCarteira";
			 public const string TipoEvento = "TipoEvento";
			 public const string DataEvento = "DataEvento";
			 public const string Valor = "Valor";
			 public const string Taxa = "Taxa";
			 public const string TipoValorInput = "TipoValorInput";
			 public const string ImpactarCota = "ImpactarCota";
			 public const string CotaFechamentoCheia = "CotaFechamentoCheia";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AgendaFundoMetadata))
			{
				if(AgendaFundoMetadata.mapDelegates == null)
				{
					AgendaFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AgendaFundoMetadata.meta == null)
				{
					AgendaFundoMetadata.meta = new AgendaFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdAgenda", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoEvento", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataEvento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoValorInput", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ImpactarCota", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("CotaFechamentoCheia", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "AgendaFundo";
				meta.Destination = "AgendaFundo";
				
				meta.spInsert = "proc_AgendaFundoInsert";				
				meta.spUpdate = "proc_AgendaFundoUpdate";		
				meta.spDelete = "proc_AgendaFundoDelete";
				meta.spLoadAll = "proc_AgendaFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_AgendaFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AgendaFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
