/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 14/03/2016 10:55:58
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esEntidadePerfilFundoCollection : esEntityCollection
	{
		public esEntidadePerfilFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EntidadePerfilFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esEntidadePerfilFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEntidadePerfilFundoQuery);
		}
		#endregion
		
		virtual public EntidadePerfilFundo DetachEntity(EntidadePerfilFundo entity)
		{
			return base.DetachEntity(entity) as EntidadePerfilFundo;
		}
		
		virtual public EntidadePerfilFundo AttachEntity(EntidadePerfilFundo entity)
		{
			return base.AttachEntity(entity) as EntidadePerfilFundo;
		}
		
		virtual public void Combine(EntidadePerfilFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EntidadePerfilFundo this[int index]
		{
			get
			{
				return base[index] as EntidadePerfilFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EntidadePerfilFundo);
		}
	}



	[Serializable]
	abstract public class esEntidadePerfilFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEntidadePerfilFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esEntidadePerfilFundo()
		{

		}

		public esEntidadePerfilFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEntidade)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEntidade);
			else
				return LoadByPrimaryKeyStoredProcedure(idEntidade);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEntidade)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEntidade);
			else
				return LoadByPrimaryKeyStoredProcedure(idEntidade);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEntidade)
		{
			esEntidadePerfilFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdEntidade == idEntidade);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEntidade)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEntidade",idEntidade);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEntidade": this.str.IdEntidade = (string)value; break;							
						case "Codigo": this.str.Codigo = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEntidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEntidade = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EntidadePerfilFundo.IdEntidade
		/// </summary>
		virtual public System.Int32? IdEntidade
		{
			get
			{
				return base.GetSystemInt32(EntidadePerfilFundoMetadata.ColumnNames.IdEntidade);
			}
			
			set
			{
				base.SetSystemInt32(EntidadePerfilFundoMetadata.ColumnNames.IdEntidade, value);
			}
		}
		
		/// <summary>
		/// Maps to EntidadePerfilFundo.Codigo
		/// </summary>
		virtual public System.String Codigo
		{
			get
			{
				return base.GetSystemString(EntidadePerfilFundoMetadata.ColumnNames.Codigo);
			}
			
			set
			{
				base.SetSystemString(EntidadePerfilFundoMetadata.ColumnNames.Codigo, value);
			}
		}
		
		/// <summary>
		/// Maps to EntidadePerfilFundo.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(EntidadePerfilFundoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(EntidadePerfilFundoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEntidadePerfilFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEntidade
			{
				get
				{
					System.Int32? data = entity.IdEntidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEntidade = null;
					else entity.IdEntidade = Convert.ToInt32(value);
				}
			}
				
			public System.String Codigo
			{
				get
				{
					System.String data = entity.Codigo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Codigo = null;
					else entity.Codigo = Convert.ToString(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esEntidadePerfilFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEntidadePerfilFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEntidadePerfilFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EntidadePerfilFundo : esEntidadePerfilFundo
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEntidadePerfilFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EntidadePerfilFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEntidade
		{
			get
			{
				return new esQueryItem(this, EntidadePerfilFundoMetadata.ColumnNames.IdEntidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Codigo
		{
			get
			{
				return new esQueryItem(this, EntidadePerfilFundoMetadata.ColumnNames.Codigo, esSystemType.String);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, EntidadePerfilFundoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EntidadePerfilFundoCollection")]
	public partial class EntidadePerfilFundoCollection : esEntidadePerfilFundoCollection, IEnumerable<EntidadePerfilFundo>
	{
		public EntidadePerfilFundoCollection()
		{

		}
		
		public static implicit operator List<EntidadePerfilFundo>(EntidadePerfilFundoCollection coll)
		{
			List<EntidadePerfilFundo> list = new List<EntidadePerfilFundo>();
			
			foreach (EntidadePerfilFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EntidadePerfilFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EntidadePerfilFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EntidadePerfilFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EntidadePerfilFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EntidadePerfilFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EntidadePerfilFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EntidadePerfilFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EntidadePerfilFundo AddNew()
		{
			EntidadePerfilFundo entity = base.AddNewEntity() as EntidadePerfilFundo;
			
			return entity;
		}

		public EntidadePerfilFundo FindByPrimaryKey(System.Int32 idEntidade)
		{
			return base.FindByPrimaryKey(idEntidade) as EntidadePerfilFundo;
		}


		#region IEnumerable<EntidadePerfilFundo> Members

		IEnumerator<EntidadePerfilFundo> IEnumerable<EntidadePerfilFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EntidadePerfilFundo;
			}
		}

		#endregion
		
		private EntidadePerfilFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EntidadePerfilFundo' table
	/// </summary>

	[Serializable]
	public partial class EntidadePerfilFundo : esEntidadePerfilFundo
	{
		public EntidadePerfilFundo()
		{

		}
	
		public EntidadePerfilFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EntidadePerfilFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esEntidadePerfilFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EntidadePerfilFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EntidadePerfilFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EntidadePerfilFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EntidadePerfilFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EntidadePerfilFundoQuery query;
	}



	[Serializable]
	public partial class EntidadePerfilFundoQuery : esEntidadePerfilFundoQuery
	{
		public EntidadePerfilFundoQuery()
		{

		}		
		
		public EntidadePerfilFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EntidadePerfilFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EntidadePerfilFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EntidadePerfilFundoMetadata.ColumnNames.IdEntidade, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EntidadePerfilFundoMetadata.PropertyNames.IdEntidade;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EntidadePerfilFundoMetadata.ColumnNames.Codigo, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = EntidadePerfilFundoMetadata.PropertyNames.Codigo;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EntidadePerfilFundoMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = EntidadePerfilFundoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EntidadePerfilFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEntidade = "IdEntidade";
			 public const string Codigo = "Codigo";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEntidade = "IdEntidade";
			 public const string Codigo = "Codigo";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EntidadePerfilFundoMetadata))
			{
				if(EntidadePerfilFundoMetadata.mapDelegates == null)
				{
					EntidadePerfilFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EntidadePerfilFundoMetadata.meta == null)
				{
					EntidadePerfilFundoMetadata.meta = new EntidadePerfilFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEntidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Codigo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "EntidadePerfilFundo";
				meta.Destination = "EntidadePerfilFundo";
				
				meta.spInsert = "proc_EntidadePerfilFundoInsert";				
				meta.spUpdate = "proc_EntidadePerfilFundoUpdate";		
				meta.spDelete = "proc_EntidadePerfilFundoDelete";
				meta.spLoadAll = "proc_EntidadePerfilFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_EntidadePerfilFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EntidadePerfilFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
