/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/03/2016 18:55:42
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;
using Financial.Captacao;
using Financial.InvestidorCotista;
using Financial.Enquadra;
using Financial.CRM;
using Financial.Investidor;
using Financial.RendaFixa;
using Financial.Swap;


namespace Financial.Fundo
{

	[Serializable]
	abstract public class esCarteiraCollection : esEntityCollection
	{
		public esCarteiraCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CarteiraCollection";
		}

		#region Query Logic
		protected void InitQuery(esCarteiraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCarteiraQuery);
		}
		#endregion
		
		virtual public Carteira DetachEntity(Carteira entity)
		{
			return base.DetachEntity(entity) as Carteira;
		}
		
		virtual public Carteira AttachEntity(Carteira entity)
		{
			return base.AttachEntity(entity) as Carteira;
		}
		
		virtual public void Combine(CarteiraCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Carteira this[int index]
		{
			get
			{
				return base[index] as Carteira;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Carteira);
		}
	}



	[Serializable]
	abstract public class esCarteira : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCarteiraQuery GetDynamicQuery()
		{
			return null;
		}

		public esCarteira()
		{

		}

		public esCarteira(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCarteira)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira);
		}

        /// <summary>
        /// Loads an entity by primary key
        /// </summary>
        /// <remarks>
        /// EntitySpaces requires primary keys be defined on all tables.
        /// If a table does not have a primary key set,
        /// this method will not compile.
        /// Does not support sqlAcessType. It only works with DynamicQuery
        /// </remarks>
        /// <param name="fieldsToReturn">Fields desired</param>
        public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCarteira)
        {
            esQueryItem[] fields = fieldsToReturn.ToArray();
            esCarteiraQuery query = this.GetDynamicQuery();
            query
                .Select(fields)
                .Where(query.IdCarteira == idCarteira);

            return query.Load();
        }

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCarteira)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCarteira)
		{
			esCarteiraQuery query = this.GetDynamicQuery();
			query.Where(query.IdCarteira == idCarteira);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCarteira)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCarteira",idCarteira);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "Nome": this.str.Nome = (string)value; break;							
						case "Apelido": this.str.Apelido = (string)value; break;							
						case "TipoCota": this.str.TipoCota = (string)value; break;							
						case "TipoCarteira": this.str.TipoCarteira = (string)value; break;							
						case "StatusAtivo": this.str.StatusAtivo = (string)value; break;							
						case "IdIndiceBenchmark": this.str.IdIndiceBenchmark = (string)value; break;							
						case "CasasDecimaisCota": this.str.CasasDecimaisCota = (string)value; break;							
						case "CasasDecimaisQuantidade": this.str.CasasDecimaisQuantidade = (string)value; break;							
						case "TruncaCota": this.str.TruncaCota = (string)value; break;							
						case "TruncaQuantidade": this.str.TruncaQuantidade = (string)value; break;							
						case "TruncaFinanceiro": this.str.TruncaFinanceiro = (string)value; break;							
						case "CotaInicial": this.str.CotaInicial = (string)value; break;							
						case "ValorMinimoAplicacao": this.str.ValorMinimoAplicacao = (string)value; break;							
						case "ValorMinimoResgate": this.str.ValorMinimoResgate = (string)value; break;							
						case "ValorMinimoSaldo": this.str.ValorMinimoSaldo = (string)value; break;							
						case "ValorMaximoAplicacao": this.str.ValorMaximoAplicacao = (string)value; break;							
						case "ValorMinimoInicial": this.str.ValorMinimoInicial = (string)value; break;							
						case "TipoRentabilidade": this.str.TipoRentabilidade = (string)value; break;							
						case "TipoCusto": this.str.TipoCusto = (string)value; break;							
						case "DiasCotizacaoAplicacao": this.str.DiasCotizacaoAplicacao = (string)value; break;							
						case "DiasCotizacaoResgate": this.str.DiasCotizacaoResgate = (string)value; break;							
						case "DiasLiquidacaoAplicacao": this.str.DiasLiquidacaoAplicacao = (string)value; break;							
						case "DiasLiquidacaoResgate": this.str.DiasLiquidacaoResgate = (string)value; break;							
						case "TipoTributacao": this.str.TipoTributacao = (string)value; break;							
						case "CalculaIOF": this.str.CalculaIOF = (string)value; break;							
						case "DiasAniversario": this.str.DiasAniversario = (string)value; break;							
						case "TipoAniversario": this.str.TipoAniversario = (string)value; break;							
						case "HorarioInicio": this.str.HorarioInicio = (string)value; break;							
						case "HorarioFim": this.str.HorarioFim = (string)value; break;							
						case "HorarioLimiteCotizacao": this.str.HorarioLimiteCotizacao = (string)value; break;							
						case "IdCategoria": this.str.IdCategoria = (string)value; break;							
						case "IdSubCategoria": this.str.IdSubCategoria = (string)value; break;							
						case "IdAgenteAdministrador": this.str.IdAgenteAdministrador = (string)value; break;							
						case "IdAgenteGestor": this.str.IdAgenteGestor = (string)value; break;							
						case "CobraTaxaFiscalizacaoCVM": this.str.CobraTaxaFiscalizacaoCVM = (string)value; break;							
						case "TipoCVM": this.str.TipoCVM = (string)value; break;							
						case "DataInicioCota": this.str.DataInicioCota = (string)value; break;							
						case "CalculaEnquadra": this.str.CalculaEnquadra = (string)value; break;							
						case "ContagemDiasConversaoResgate": this.str.ContagemDiasConversaoResgate = (string)value; break;							
						case "ProjecaoIRResgate": this.str.ProjecaoIRResgate = (string)value; break;							
						case "PublicoAlvo": this.str.PublicoAlvo = (string)value; break;							
						case "Objetivo": this.str.Objetivo = (string)value; break;							
						case "IdAgenteCustodiante": this.str.IdAgenteCustodiante = (string)value; break;							
						case "TextoLivre1": this.str.TextoLivre1 = (string)value; break;							
						case "TextoLivre2": this.str.TextoLivre2 = (string)value; break;							
						case "TextoLivre3": this.str.TextoLivre3 = (string)value; break;							
						case "TextoLivre4": this.str.TextoLivre4 = (string)value; break;							
						case "TipoVisaoFundo": this.str.TipoVisaoFundo = (string)value; break;							
						case "DistribuicaoDividendo": this.str.DistribuicaoDividendo = (string)value; break;							
						case "IdEstrategia": this.str.IdEstrategia = (string)value; break;							
						case "CodigoAnbid": this.str.CodigoAnbid = (string)value; break;							
						case "ContagemPrazoIOF": this.str.ContagemPrazoIOF = (string)value; break;							
						case "PrioridadeOperacao": this.str.PrioridadeOperacao = (string)value; break;							
						case "CodigoCDA": this.str.CodigoCDA = (string)value; break;							
						case "CompensacaoPrejuizo": this.str.CompensacaoPrejuizo = (string)value; break;							
						case "DiasConfidencialidade": this.str.DiasConfidencialidade = (string)value; break;							
						case "CodigoFDIC": this.str.CodigoFDIC = (string)value; break;							
						case "TipoCalculoRetorno": this.str.TipoCalculoRetorno = (string)value; break;							
						case "IdGrauRisco": this.str.IdGrauRisco = (string)value; break;							
						case "CodigoIsin": this.str.CodigoIsin = (string)value; break;							
						case "CodigoCetip": this.str.CodigoCetip = (string)value; break;							
						case "CodigoBloomberg": this.str.CodigoBloomberg = (string)value; break;							
						case "HorarioInicioResgate": this.str.HorarioInicioResgate = (string)value; break;							
						case "HorarioFimResgate": this.str.HorarioFimResgate = (string)value; break;							
						case "IdGrupoEconomico": this.str.IdGrupoEconomico = (string)value; break;							
						case "FundoExclusivo": this.str.FundoExclusivo = (string)value; break;							
						case "OrdemRelatorio": this.str.OrdemRelatorio = (string)value; break;							
						case "LiberaAplicacao": this.str.LiberaAplicacao = (string)value; break;							
						case "LiberaResgate": this.str.LiberaResgate = (string)value; break;							
						case "PerfilRisco": this.str.PerfilRisco = (string)value; break;							
						case "CalculaMTM": this.str.CalculaMTM = (string)value; break;							
						case "CalculaPrazoMedio": this.str.CalculaPrazoMedio = (string)value; break;							
						case "DiasAposResgateIR": this.str.DiasAposResgateIR = (string)value; break;							
						case "ProjecaoIRComeCotas": this.str.ProjecaoIRComeCotas = (string)value; break;							
						case "DiasAposComeCotasIR": this.str.DiasAposComeCotasIR = (string)value; break;							
						case "RealizaCompensacaoDePrejuizo": this.str.RealizaCompensacaoDePrejuizo = (string)value; break;							
						case "BuscaCotaAnterior": this.str.BuscaCotaAnterior = (string)value; break;							
						case "NumeroDiasBuscaCota": this.str.NumeroDiasBuscaCota = (string)value; break;							
						case "BandaVariacao": this.str.BandaVariacao = (string)value; break;							
						case "ExcecaoRegraTxAdm": this.str.ExcecaoRegraTxAdm = (string)value; break;							
						case "RebateImpactaPL": this.str.RebateImpactaPL = (string)value; break;							
						case "TipoFundo": this.str.TipoFundo = (string)value; break;							
						case "Fie": this.str.Fie = (string)value; break;							
						case "DataInicioContIOF": this.str.DataInicioContIOF = (string)value; break;							
						case "DataFimContIOF": this.str.DataFimContIOF = (string)value; break;							
						case "ContaPrzIOFVirtual": this.str.ContaPrzIOFVirtual = (string)value; break;							
						case "ProjecaoIOFResgate": this.str.ProjecaoIOFResgate = (string)value; break;							
						case "DiasAposResgateIOF": this.str.DiasAposResgateIOF = (string)value; break;							
						case "CodigoConsolidacaoExterno": this.str.CodigoConsolidacaoExterno = (string)value; break;							
						case "CodigoBDS": this.str.CodigoBDS = (string)value; break;							
						case "CodigoSTI": this.str.CodigoSTI = (string)value; break;							
						case "TipoVisualizacaoResgCotista": this.str.TipoVisualizacaoResgCotista = (string)value; break;							
						case "ApenasInvestidorProfissional": this.str.ApenasInvestidorProfissional = (string)value; break;							
						case "ApenasInvestidorQualificado": this.str.ApenasInvestidorQualificado = (string)value; break;							
						case "RealizaOfertaSubscricao": this.str.RealizaOfertaSubscricao = (string)value; break;							
						case "Rendimento": this.str.Rendimento = (string)value; break;							
						case "IdGrupoPerfilMTM": this.str.IdGrupoPerfilMTM = (string)value; break;							
						case "ProcAutomatico": this.str.ProcAutomatico = (string)value; break;							
						case "ExplodeCotasDeFundos": this.str.ExplodeCotasDeFundos = (string)value; break;							
						case "ControladoriaAtivo": this.str.ControladoriaAtivo = (string)value; break;							
						case "ControladoriaPassivo": this.str.ControladoriaPassivo = (string)value; break;							
						case "MesmoConglomerado": this.str.MesmoConglomerado = (string)value; break;							
						case "CategoriaAnbima": this.str.CategoriaAnbima = (string)value; break;							
						case "Contratante": this.str.Contratante = (string)value; break;							
						case "Corretora": this.str.Corretora = (string)value; break;							
						case "ExportaGalgo": this.str.ExportaGalgo = (string)value; break;							
						case "PossuiResgateAutomatico": this.str.PossuiResgateAutomatico = (string)value; break;							
						case "InfluenciaGestorLocalCvm": this.str.InfluenciaGestorLocalCvm = (string)value; break;							
						case "InvestimentoColetivoCvm": this.str.InvestimentoColetivoCvm = (string)value; break;							
						case "ComeCotasEntreRegatesConversao": this.str.ComeCotasEntreRegatesConversao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "TipoCota":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCota = (System.Byte?)value;
							break;
						
						case "TipoCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCarteira = (System.Byte?)value;
							break;
						
						case "StatusAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.StatusAtivo = (System.Byte?)value;
							break;
						
						case "IdIndiceBenchmark":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndiceBenchmark = (System.Int16?)value;
							break;
						
						case "CasasDecimaisCota":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.CasasDecimaisCota = (System.Byte?)value;
							break;
						
						case "CasasDecimaisQuantidade":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.CasasDecimaisQuantidade = (System.Byte?)value;
							break;
						
						case "CotaInicial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaInicial = (System.Decimal?)value;
							break;
						
						case "ValorMinimoAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMinimoAplicacao = (System.Decimal?)value;
							break;
						
						case "ValorMinimoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMinimoResgate = (System.Decimal?)value;
							break;
						
						case "ValorMinimoSaldo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMinimoSaldo = (System.Decimal?)value;
							break;
						
						case "ValorMaximoAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMaximoAplicacao = (System.Decimal?)value;
							break;
						
						case "ValorMinimoInicial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMinimoInicial = (System.Decimal?)value;
							break;
						
						case "TipoRentabilidade":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoRentabilidade = (System.Byte?)value;
							break;
						
						case "TipoCusto":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCusto = (System.Byte?)value;
							break;
						
						case "DiasCotizacaoAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.DiasCotizacaoAplicacao = (System.Byte?)value;
							break;
						
						case "DiasCotizacaoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.DiasCotizacaoResgate = (System.Byte?)value;
							break;
						
						case "DiasLiquidacaoAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.DiasLiquidacaoAplicacao = (System.Byte?)value;
							break;
						
						case "DiasLiquidacaoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.DiasLiquidacaoResgate = (System.Byte?)value;
							break;
						
						case "TipoTributacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoTributacao = (System.Byte?)value;
							break;
						
						case "DiasAniversario":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.DiasAniversario = (System.Int16?)value;
							break;
						
						case "TipoAniversario":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoAniversario = (System.Byte?)value;
							break;
						
						case "HorarioInicio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.HorarioInicio = (System.DateTime?)value;
							break;
						
						case "HorarioFim":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.HorarioFim = (System.DateTime?)value;
							break;
						
						case "HorarioLimiteCotizacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.HorarioLimiteCotizacao = (System.DateTime?)value;
							break;
						
						case "IdCategoria":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCategoria = (System.Int32?)value;
							break;
						
						case "IdSubCategoria":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSubCategoria = (System.Int32?)value;
							break;
						
						case "IdAgenteAdministrador":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteAdministrador = (System.Int32?)value;
							break;
						
						case "IdAgenteGestor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteGestor = (System.Int32?)value;
							break;
						
						case "TipoCVM":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoCVM = (System.Int32?)value;
							break;
						
						case "DataInicioCota":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicioCota = (System.DateTime?)value;
							break;
						
						case "ContagemDiasConversaoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.ContagemDiasConversaoResgate = (System.Int32?)value;
							break;
						
						case "ProjecaoIRResgate":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ProjecaoIRResgate = (System.Byte?)value;
							break;
						
						case "IdAgenteCustodiante":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteCustodiante = (System.Int32?)value;
							break;
						
						case "TipoVisaoFundo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoVisaoFundo = (System.Byte?)value;
							break;
						
						case "DistribuicaoDividendo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.DistribuicaoDividendo = (System.Byte?)value;
							break;
						
						case "IdEstrategia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEstrategia = (System.Int32?)value;
							break;
						
						case "ContagemPrazoIOF":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContagemPrazoIOF = (System.Byte?)value;
							break;
						
						case "PrioridadeOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.PrioridadeOperacao = (System.Byte?)value;
							break;
						
						case "CodigoCDA":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoCDA = (System.Int32?)value;
							break;
						
						case "CompensacaoPrejuizo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.CompensacaoPrejuizo = (System.Byte?)value;
							break;
						
						case "DiasConfidencialidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DiasConfidencialidade = (System.Int32?)value;
							break;
						
						case "CodigoFDIC":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoFDIC = (System.Int32?)value;
							break;
						
						case "TipoCalculoRetorno":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCalculoRetorno = (System.Byte?)value;
							break;
						
						case "IdGrauRisco":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrauRisco = (System.Int32?)value;
							break;
						
						case "HorarioInicioResgate":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.HorarioInicioResgate = (System.DateTime?)value;
							break;
						
						case "HorarioFimResgate":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.HorarioFimResgate = (System.DateTime?)value;
							break;
						
						case "IdGrupoEconomico":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupoEconomico = (System.Int32?)value;
							break;
						
						case "OrdemRelatorio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.OrdemRelatorio = (System.Int32?)value;
							break;
						
						case "PerfilRisco":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PerfilRisco = (System.Int32?)value;
							break;
						
						case "CalculaMTM":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CalculaMTM = (System.Int32?)value;
							break;
						
						case "DiasAposResgateIR":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DiasAposResgateIR = (System.Int32?)value;
							break;
						
						case "ProjecaoIRComeCotas":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ProjecaoIRComeCotas = (System.Byte?)value;
							break;
						
						case "DiasAposComeCotasIR":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DiasAposComeCotasIR = (System.Int32?)value;
							break;
						
						case "NumeroDiasBuscaCota":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroDiasBuscaCota = (System.Int32?)value;
							break;
						
						case "BandaVariacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.BandaVariacao = (System.Decimal?)value;
							break;
						
						case "TipoFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoFundo = (System.Int32?)value;
							break;
						
						case "DataInicioContIOF":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.DataInicioContIOF = (System.Int16?)value;
							break;
						
						case "DataFimContIOF":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.DataFimContIOF = (System.Int16?)value;
							break;
						
						case "ContaPrzIOFVirtual":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.ContaPrzIOFVirtual = (System.Int16?)value;
							break;
						
						case "ProjecaoIOFResgate":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ProjecaoIOFResgate = (System.Byte?)value;
							break;
						
						case "DiasAposResgateIOF":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DiasAposResgateIOF = (System.Int32?)value;
							break;
						
						case "CodigoBDS":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoBDS = (System.Int32?)value;
							break;
						
						case "Rendimento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Rendimento = (System.Int32?)value;
							break;
						
						case "IdGrupoPerfilMTM":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupoPerfilMTM = (System.Int32?)value;
							break;
						
						case "CategoriaAnbima":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CategoriaAnbima = (System.Int32?)value;
							break;
						
						case "Contratante":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Contratante = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Carteira.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.Nome
		/// </summary>
		virtual public System.String Nome
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.Nome);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.Nome, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.Apelido
		/// </summary>
		virtual public System.String Apelido
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.Apelido);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.Apelido, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TipoCota
		/// </summary>
		virtual public System.Byte? TipoCota
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.TipoCota);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.TipoCota, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TipoCarteira
		/// </summary>
		virtual public System.Byte? TipoCarteira
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.TipoCarteira);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.TipoCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.StatusAtivo
		/// </summary>
		virtual public System.Byte? StatusAtivo
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.StatusAtivo);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.StatusAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.IdIndiceBenchmark
		/// </summary>
		virtual public System.Int16? IdIndiceBenchmark
		{
			get
			{
				return base.GetSystemInt16(CarteiraMetadata.ColumnNames.IdIndiceBenchmark);
			}
			
			set
			{
				if(base.SetSystemInt16(CarteiraMetadata.ColumnNames.IdIndiceBenchmark, value))
				{
					this._UpToIndiceByIdIndiceBenchmark = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CasasDecimaisCota
		/// </summary>
		virtual public System.Byte? CasasDecimaisCota
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.CasasDecimaisCota);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.CasasDecimaisCota, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CasasDecimaisQuantidade
		/// </summary>
		virtual public System.Byte? CasasDecimaisQuantidade
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.CasasDecimaisQuantidade);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.CasasDecimaisQuantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TruncaCota
		/// </summary>
		virtual public System.String TruncaCota
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.TruncaCota);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.TruncaCota, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TruncaQuantidade
		/// </summary>
		virtual public System.String TruncaQuantidade
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.TruncaQuantidade);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.TruncaQuantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TruncaFinanceiro
		/// </summary>
		virtual public System.String TruncaFinanceiro
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.TruncaFinanceiro);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.TruncaFinanceiro, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CotaInicial
		/// </summary>
		virtual public System.Decimal? CotaInicial
		{
			get
			{
				return base.GetSystemDecimal(CarteiraMetadata.ColumnNames.CotaInicial);
			}
			
			set
			{
				base.SetSystemDecimal(CarteiraMetadata.ColumnNames.CotaInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ValorMinimoAplicacao
		/// </summary>
		virtual public System.Decimal? ValorMinimoAplicacao
		{
			get
			{
				return base.GetSystemDecimal(CarteiraMetadata.ColumnNames.ValorMinimoAplicacao);
			}
			
			set
			{
				base.SetSystemDecimal(CarteiraMetadata.ColumnNames.ValorMinimoAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ValorMinimoResgate
		/// </summary>
		virtual public System.Decimal? ValorMinimoResgate
		{
			get
			{
				return base.GetSystemDecimal(CarteiraMetadata.ColumnNames.ValorMinimoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(CarteiraMetadata.ColumnNames.ValorMinimoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ValorMinimoSaldo
		/// </summary>
		virtual public System.Decimal? ValorMinimoSaldo
		{
			get
			{
				return base.GetSystemDecimal(CarteiraMetadata.ColumnNames.ValorMinimoSaldo);
			}
			
			set
			{
				base.SetSystemDecimal(CarteiraMetadata.ColumnNames.ValorMinimoSaldo, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ValorMaximoAplicacao
		/// </summary>
		virtual public System.Decimal? ValorMaximoAplicacao
		{
			get
			{
				return base.GetSystemDecimal(CarteiraMetadata.ColumnNames.ValorMaximoAplicacao);
			}
			
			set
			{
				base.SetSystemDecimal(CarteiraMetadata.ColumnNames.ValorMaximoAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ValorMinimoInicial
		/// </summary>
		virtual public System.Decimal? ValorMinimoInicial
		{
			get
			{
				return base.GetSystemDecimal(CarteiraMetadata.ColumnNames.ValorMinimoInicial);
			}
			
			set
			{
				base.SetSystemDecimal(CarteiraMetadata.ColumnNames.ValorMinimoInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TipoRentabilidade
		/// </summary>
		virtual public System.Byte? TipoRentabilidade
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.TipoRentabilidade);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.TipoRentabilidade, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TipoCusto
		/// </summary>
		virtual public System.Byte? TipoCusto
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.TipoCusto);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.TipoCusto, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.DiasCotizacaoAplicacao
		/// </summary>
		virtual public System.Byte? DiasCotizacaoAplicacao
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.DiasCotizacaoAplicacao);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.DiasCotizacaoAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.DiasCotizacaoResgate
		/// </summary>
		virtual public System.Byte? DiasCotizacaoResgate
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.DiasCotizacaoResgate);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.DiasCotizacaoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.DiasLiquidacaoAplicacao
		/// </summary>
		virtual public System.Byte? DiasLiquidacaoAplicacao
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.DiasLiquidacaoAplicacao);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.DiasLiquidacaoAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.DiasLiquidacaoResgate
		/// </summary>
		virtual public System.Byte? DiasLiquidacaoResgate
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.DiasLiquidacaoResgate);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.DiasLiquidacaoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TipoTributacao
		/// </summary>
		virtual public System.Byte? TipoTributacao
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.TipoTributacao);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.TipoTributacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CalculaIOF
		/// </summary>
		virtual public System.String CalculaIOF
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.CalculaIOF);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.CalculaIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.DiasAniversario
		/// </summary>
		virtual public System.Int16? DiasAniversario
		{
			get
			{
				return base.GetSystemInt16(CarteiraMetadata.ColumnNames.DiasAniversario);
			}
			
			set
			{
				base.SetSystemInt16(CarteiraMetadata.ColumnNames.DiasAniversario, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TipoAniversario
		/// </summary>
		virtual public System.Byte? TipoAniversario
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.TipoAniversario);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.TipoAniversario, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.HorarioInicio
		/// </summary>
		virtual public System.DateTime? HorarioInicio
		{
			get
			{
				return base.GetSystemDateTime(CarteiraMetadata.ColumnNames.HorarioInicio);
			}
			
			set
			{
				base.SetSystemDateTime(CarteiraMetadata.ColumnNames.HorarioInicio, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.HorarioFim
		/// </summary>
		virtual public System.DateTime? HorarioFim
		{
			get
			{
				return base.GetSystemDateTime(CarteiraMetadata.ColumnNames.HorarioFim);
			}
			
			set
			{
				base.SetSystemDateTime(CarteiraMetadata.ColumnNames.HorarioFim, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.HorarioLimiteCotizacao
		/// </summary>
		virtual public System.DateTime? HorarioLimiteCotizacao
		{
			get
			{
				return base.GetSystemDateTime(CarteiraMetadata.ColumnNames.HorarioLimiteCotizacao);
			}
			
			set
			{
				base.SetSystemDateTime(CarteiraMetadata.ColumnNames.HorarioLimiteCotizacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.IdCategoria
		/// </summary>
		virtual public System.Int32? IdCategoria
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.IdCategoria);
			}
			
			set
			{
				if(base.SetSystemInt32(CarteiraMetadata.ColumnNames.IdCategoria, value))
				{
					this._UpToCategoriaFundoByIdCategoria = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Carteira.IdSubCategoria
		/// </summary>
		virtual public System.Int32? IdSubCategoria
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.IdSubCategoria);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.IdSubCategoria, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.IdAgenteAdministrador
		/// </summary>
		virtual public System.Int32? IdAgenteAdministrador
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.IdAgenteAdministrador);
			}
			
			set
			{
				if(base.SetSystemInt32(CarteiraMetadata.ColumnNames.IdAgenteAdministrador, value))
				{
					this._UpToAgenteMercadoByIdAgenteAdministrador = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Carteira.IdAgenteGestor
		/// </summary>
		virtual public System.Int32? IdAgenteGestor
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.IdAgenteGestor);
			}
			
			set
			{
				if(base.SetSystemInt32(CarteiraMetadata.ColumnNames.IdAgenteGestor, value))
				{
					this._UpToAgenteMercadoByIdAgenteGestor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CobraTaxaFiscalizacaoCVM
		/// </summary>
		virtual public System.String CobraTaxaFiscalizacaoCVM
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.CobraTaxaFiscalizacaoCVM);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.CobraTaxaFiscalizacaoCVM, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TipoCVM
		/// </summary>
		virtual public System.Int32? TipoCVM
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.TipoCVM);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.TipoCVM, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.DataInicioCota
		/// </summary>
		virtual public System.DateTime? DataInicioCota
		{
			get
			{
				return base.GetSystemDateTime(CarteiraMetadata.ColumnNames.DataInicioCota);
			}
			
			set
			{
				base.SetSystemDateTime(CarteiraMetadata.ColumnNames.DataInicioCota, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CalculaEnquadra
		/// </summary>
		virtual public System.String CalculaEnquadra
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.CalculaEnquadra);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.CalculaEnquadra, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ContagemDiasConversaoResgate
		/// </summary>
		virtual public System.Int32? ContagemDiasConversaoResgate
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.ContagemDiasConversaoResgate);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.ContagemDiasConversaoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ProjecaoIRResgate
		/// </summary>
		virtual public System.Byte? ProjecaoIRResgate
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.ProjecaoIRResgate);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.ProjecaoIRResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.PublicoAlvo
		/// </summary>
		virtual public System.String PublicoAlvo
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.PublicoAlvo);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.PublicoAlvo, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.Objetivo
		/// </summary>
		virtual public System.String Objetivo
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.Objetivo);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.Objetivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.IdAgenteCustodiante
		/// </summary>
		virtual public System.Int32? IdAgenteCustodiante
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.IdAgenteCustodiante);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.IdAgenteCustodiante, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TextoLivre1
		/// </summary>
		virtual public System.String TextoLivre1
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.TextoLivre1);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.TextoLivre1, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TextoLivre2
		/// </summary>
		virtual public System.String TextoLivre2
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.TextoLivre2);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.TextoLivre2, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TextoLivre3
		/// </summary>
		virtual public System.String TextoLivre3
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.TextoLivre3);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.TextoLivre3, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TextoLivre4
		/// </summary>
		virtual public System.String TextoLivre4
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.TextoLivre4);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.TextoLivre4, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TipoVisaoFundo
		/// </summary>
		virtual public System.Byte? TipoVisaoFundo
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.TipoVisaoFundo);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.TipoVisaoFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.DistribuicaoDividendo
		/// </summary>
		virtual public System.Byte? DistribuicaoDividendo
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.DistribuicaoDividendo);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.DistribuicaoDividendo, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.IdEstrategia
		/// </summary>
		virtual public System.Int32? IdEstrategia
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.IdEstrategia);
			}
			
			set
			{
				if(base.SetSystemInt32(CarteiraMetadata.ColumnNames.IdEstrategia, value))
				{
					this._UpToEstrategiaByIdEstrategia = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CodigoAnbid
		/// </summary>
		virtual public System.String CodigoAnbid
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.CodigoAnbid);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.CodigoAnbid, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ContagemPrazoIOF
		/// </summary>
		virtual public System.Byte? ContagemPrazoIOF
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.ContagemPrazoIOF);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.ContagemPrazoIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.PrioridadeOperacao
		/// </summary>
		virtual public System.Byte? PrioridadeOperacao
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.PrioridadeOperacao);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.PrioridadeOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CodigoCDA
		/// </summary>
		virtual public System.Int32? CodigoCDA
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.CodigoCDA);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.CodigoCDA, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CompensacaoPrejuizo
		/// </summary>
		virtual public System.Byte? CompensacaoPrejuizo
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.CompensacaoPrejuizo);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.CompensacaoPrejuizo, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.DiasConfidencialidade
		/// </summary>
		virtual public System.Int32? DiasConfidencialidade
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.DiasConfidencialidade);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.DiasConfidencialidade, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CodigoFDIC
		/// </summary>
		virtual public System.Int32? CodigoFDIC
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.CodigoFDIC);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.CodigoFDIC, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TipoCalculoRetorno
		/// </summary>
		virtual public System.Byte? TipoCalculoRetorno
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.TipoCalculoRetorno);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.TipoCalculoRetorno, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.IdGrauRisco
		/// </summary>
		virtual public System.Int32? IdGrauRisco
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.IdGrauRisco);
			}
			
			set
			{
				if(base.SetSystemInt32(CarteiraMetadata.ColumnNames.IdGrauRisco, value))
				{
					this._UpToGrauRiscoByIdGrauRisco = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CodigoIsin
		/// </summary>
		virtual public System.String CodigoIsin
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.CodigoIsin);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.CodigoIsin, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CodigoCetip
		/// </summary>
		virtual public System.String CodigoCetip
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.CodigoCetip);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.CodigoCetip, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CodigoBloomberg
		/// </summary>
		virtual public System.String CodigoBloomberg
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.CodigoBloomberg);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.CodigoBloomberg, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.HorarioInicioResgate
		/// </summary>
		virtual public System.DateTime? HorarioInicioResgate
		{
			get
			{
				return base.GetSystemDateTime(CarteiraMetadata.ColumnNames.HorarioInicioResgate);
			}
			
			set
			{
				base.SetSystemDateTime(CarteiraMetadata.ColumnNames.HorarioInicioResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.HorarioFimResgate
		/// </summary>
		virtual public System.DateTime? HorarioFimResgate
		{
			get
			{
				return base.GetSystemDateTime(CarteiraMetadata.ColumnNames.HorarioFimResgate);
			}
			
			set
			{
				base.SetSystemDateTime(CarteiraMetadata.ColumnNames.HorarioFimResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.IdGrupoEconomico
		/// </summary>
		virtual public System.Int32? IdGrupoEconomico
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.IdGrupoEconomico);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.IdGrupoEconomico, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.FundoExclusivo
		/// </summary>
		virtual public System.String FundoExclusivo
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.FundoExclusivo);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.FundoExclusivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.OrdemRelatorio
		/// </summary>
		virtual public System.Int32? OrdemRelatorio
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.OrdemRelatorio);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.OrdemRelatorio, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.LiberaAplicacao
		/// </summary>
		virtual public System.String LiberaAplicacao
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.LiberaAplicacao);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.LiberaAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.LiberaResgate
		/// </summary>
		virtual public System.String LiberaResgate
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.LiberaResgate);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.LiberaResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.PerfilRisco
		/// </summary>
		virtual public System.Int32? PerfilRisco
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.PerfilRisco);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.PerfilRisco, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CalculaMTM
		/// </summary>
		virtual public System.Int32? CalculaMTM
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.CalculaMTM);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.CalculaMTM, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CalculaPrazoMedio
		/// </summary>
		virtual public System.String CalculaPrazoMedio
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.CalculaPrazoMedio);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.CalculaPrazoMedio, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.DiasAposResgateIR
		/// </summary>
		virtual public System.Int32? DiasAposResgateIR
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.DiasAposResgateIR);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.DiasAposResgateIR, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ProjecaoIRComeCotas
		/// </summary>
		virtual public System.Byte? ProjecaoIRComeCotas
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.ProjecaoIRComeCotas);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.ProjecaoIRComeCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.DiasAposComeCotasIR
		/// </summary>
		virtual public System.Int32? DiasAposComeCotasIR
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.DiasAposComeCotasIR);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.DiasAposComeCotasIR, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.RealizaCompensacaoDePrejuizo
		/// </summary>
		virtual public System.String RealizaCompensacaoDePrejuizo
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.RealizaCompensacaoDePrejuizo);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.RealizaCompensacaoDePrejuizo, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.BuscaCotaAnterior
		/// </summary>
		virtual public System.String BuscaCotaAnterior
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.BuscaCotaAnterior);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.BuscaCotaAnterior, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.NumeroDiasBuscaCota
		/// </summary>
		virtual public System.Int32? NumeroDiasBuscaCota
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.NumeroDiasBuscaCota);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.NumeroDiasBuscaCota, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.BandaVariacao
		/// </summary>
		virtual public System.Decimal? BandaVariacao
		{
			get
			{
				return base.GetSystemDecimal(CarteiraMetadata.ColumnNames.BandaVariacao);
			}
			
			set
			{
				base.SetSystemDecimal(CarteiraMetadata.ColumnNames.BandaVariacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ExcecaoRegraTxAdm
		/// </summary>
		virtual public System.String ExcecaoRegraTxAdm
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.ExcecaoRegraTxAdm);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.ExcecaoRegraTxAdm, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.RebateImpactaPL
		/// </summary>
		virtual public System.String RebateImpactaPL
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.RebateImpactaPL);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.RebateImpactaPL, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TipoFundo
		/// </summary>
		virtual public System.Int32? TipoFundo
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.TipoFundo);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.TipoFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.FIE
		/// </summary>
		virtual public System.String Fie
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.Fie);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.Fie, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.DataInicioContIOF
		/// </summary>
		virtual public System.Int16? DataInicioContIOF
		{
			get
			{
				return base.GetSystemInt16(CarteiraMetadata.ColumnNames.DataInicioContIOF);
			}
			
			set
			{
				base.SetSystemInt16(CarteiraMetadata.ColumnNames.DataInicioContIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.DataFimContIOF
		/// </summary>
		virtual public System.Int16? DataFimContIOF
		{
			get
			{
				return base.GetSystemInt16(CarteiraMetadata.ColumnNames.DataFimContIOF);
			}
			
			set
			{
				base.SetSystemInt16(CarteiraMetadata.ColumnNames.DataFimContIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ContaPrzIOFVirtual
		/// </summary>
		virtual public System.Int16? ContaPrzIOFVirtual
		{
			get
			{
				return base.GetSystemInt16(CarteiraMetadata.ColumnNames.ContaPrzIOFVirtual);
			}
			
			set
			{
				base.SetSystemInt16(CarteiraMetadata.ColumnNames.ContaPrzIOFVirtual, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ProjecaoIOFResgate
		/// </summary>
		virtual public System.Byte? ProjecaoIOFResgate
		{
			get
			{
				return base.GetSystemByte(CarteiraMetadata.ColumnNames.ProjecaoIOFResgate);
			}
			
			set
			{
				base.SetSystemByte(CarteiraMetadata.ColumnNames.ProjecaoIOFResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.DiasAposResgateIOF
		/// </summary>
		virtual public System.Int32? DiasAposResgateIOF
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.DiasAposResgateIOF);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.DiasAposResgateIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CodigoConsolidacaoExterno
		/// </summary>
		virtual public System.String CodigoConsolidacaoExterno
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.CodigoConsolidacaoExterno);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.CodigoConsolidacaoExterno, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CodigoBDS
		/// </summary>
		virtual public System.Int32? CodigoBDS
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.CodigoBDS);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.CodigoBDS, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CodigoSTI
		/// </summary>
		virtual public System.String CodigoSTI
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.CodigoSTI);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.CodigoSTI, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.TipoVisualizacaoResgCotista
		/// </summary>
		virtual public System.String TipoVisualizacaoResgCotista
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.TipoVisualizacaoResgCotista);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.TipoVisualizacaoResgCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ApenasInvestidorProfissional
		/// </summary>
		virtual public System.String ApenasInvestidorProfissional
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.ApenasInvestidorProfissional);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.ApenasInvestidorProfissional, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ApenasInvestidorQualificado
		/// </summary>
		virtual public System.String ApenasInvestidorQualificado
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.ApenasInvestidorQualificado);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.ApenasInvestidorQualificado, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.RealizaOfertaSubscricao
		/// </summary>
		virtual public System.String RealizaOfertaSubscricao
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.RealizaOfertaSubscricao);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.RealizaOfertaSubscricao, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.Rendimento
		/// </summary>
		virtual public System.Int32? Rendimento
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.Rendimento);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.Rendimento, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.IdGrupoPerfilMTM
		/// </summary>
		virtual public System.Int32? IdGrupoPerfilMTM
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.IdGrupoPerfilMTM);
			}
			
			set
			{
				if(base.SetSystemInt32(CarteiraMetadata.ColumnNames.IdGrupoPerfilMTM, value))
				{
					this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ProcAutomatico
		/// </summary>
		virtual public System.String ProcAutomatico
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.ProcAutomatico);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.ProcAutomatico, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ExplodeCotasDeFundos
		/// </summary>
		virtual public System.String ExplodeCotasDeFundos
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.ExplodeCotasDeFundos);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.ExplodeCotasDeFundos, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ControladoriaAtivo
		/// </summary>
		virtual public System.String ControladoriaAtivo
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.ControladoriaAtivo);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.ControladoriaAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ControladoriaPassivo
		/// </summary>
		virtual public System.String ControladoriaPassivo
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.ControladoriaPassivo);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.ControladoriaPassivo, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.MesmoConglomerado
		/// </summary>
		virtual public System.String MesmoConglomerado
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.MesmoConglomerado);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.MesmoConglomerado, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.CategoriaAnbima
		/// </summary>
		virtual public System.Int32? CategoriaAnbima
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.CategoriaAnbima);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraMetadata.ColumnNames.CategoriaAnbima, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.Contratante
		/// </summary>
		virtual public System.Int32? Contratante
		{
			get
			{
				return base.GetSystemInt32(CarteiraMetadata.ColumnNames.Contratante);
			}
			
			set
			{
				if(base.SetSystemInt32(CarteiraMetadata.ColumnNames.Contratante, value))
				{
					this._UpToAgenteMercadoByContratante = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to Carteira.Corretora
		/// </summary>
		virtual public System.String Corretora
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.Corretora);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.Corretora, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ExportaGalgo
		/// </summary>
		virtual public System.String ExportaGalgo
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.ExportaGalgo);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.ExportaGalgo, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.PossuiResgateAutomatico
		/// </summary>
		virtual public System.String PossuiResgateAutomatico
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.PossuiResgateAutomatico);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.PossuiResgateAutomatico, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.InfluenciaGestorLocalCvm
		/// </summary>
		virtual public System.String InfluenciaGestorLocalCvm
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.InfluenciaGestorLocalCvm);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.InfluenciaGestorLocalCvm, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.InvestimentoColetivoCvm
		/// </summary>
		virtual public System.String InvestimentoColetivoCvm
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.InvestimentoColetivoCvm);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.InvestimentoColetivoCvm, value);
			}
		}
		
		/// <summary>
		/// Maps to Carteira.ComeCotasEntreRegatesConversao
		/// </summary>
		virtual public System.String ComeCotasEntreRegatesConversao
		{
			get
			{
				return base.GetSystemString(CarteiraMetadata.ColumnNames.ComeCotasEntreRegatesConversao);
			}
			
			set
			{
				base.SetSystemString(CarteiraMetadata.ColumnNames.ComeCotasEntreRegatesConversao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteAdministrador;
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteGestor;
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByContratante;
		[CLSCompliant(false)]
		internal protected CategoriaFundo _UpToCategoriaFundoByIdCategoria;
		[CLSCompliant(false)]
		internal protected Estrategia _UpToEstrategiaByIdEstrategia;
		[CLSCompliant(false)]
		internal protected GrauRisco _UpToGrauRiscoByIdGrauRisco;
		[CLSCompliant(false)]
		internal protected GrupoPerfilMTM _UpToGrupoPerfilMTMByIdGrupoPerfilMTM;
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndiceBenchmark;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCarteira entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String Nome
			{
				get
				{
					System.String data = entity.Nome;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Nome = null;
					else entity.Nome = Convert.ToString(value);
				}
			}
				
			public System.String Apelido
			{
				get
				{
					System.String data = entity.Apelido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Apelido = null;
					else entity.Apelido = Convert.ToString(value);
				}
			}
				
			public System.String TipoCota
			{
				get
				{
					System.Byte? data = entity.TipoCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCota = null;
					else entity.TipoCota = Convert.ToByte(value);
				}
			}
				
			public System.String TipoCarteira
			{
				get
				{
					System.Byte? data = entity.TipoCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCarteira = null;
					else entity.TipoCarteira = Convert.ToByte(value);
				}
			}
				
			public System.String StatusAtivo
			{
				get
				{
					System.Byte? data = entity.StatusAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.StatusAtivo = null;
					else entity.StatusAtivo = Convert.ToByte(value);
				}
			}
				
			public System.String IdIndiceBenchmark
			{
				get
				{
					System.Int16? data = entity.IdIndiceBenchmark;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndiceBenchmark = null;
					else entity.IdIndiceBenchmark = Convert.ToInt16(value);
				}
			}
				
			public System.String CasasDecimaisCota
			{
				get
				{
					System.Byte? data = entity.CasasDecimaisCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CasasDecimaisCota = null;
					else entity.CasasDecimaisCota = Convert.ToByte(value);
				}
			}
				
			public System.String CasasDecimaisQuantidade
			{
				get
				{
					System.Byte? data = entity.CasasDecimaisQuantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CasasDecimaisQuantidade = null;
					else entity.CasasDecimaisQuantidade = Convert.ToByte(value);
				}
			}
				
			public System.String TruncaCota
			{
				get
				{
					System.String data = entity.TruncaCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TruncaCota = null;
					else entity.TruncaCota = Convert.ToString(value);
				}
			}
				
			public System.String TruncaQuantidade
			{
				get
				{
					System.String data = entity.TruncaQuantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TruncaQuantidade = null;
					else entity.TruncaQuantidade = Convert.ToString(value);
				}
			}
				
			public System.String TruncaFinanceiro
			{
				get
				{
					System.String data = entity.TruncaFinanceiro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TruncaFinanceiro = null;
					else entity.TruncaFinanceiro = Convert.ToString(value);
				}
			}
				
			public System.String CotaInicial
			{
				get
				{
					System.Decimal? data = entity.CotaInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaInicial = null;
					else entity.CotaInicial = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorMinimoAplicacao
			{
				get
				{
					System.Decimal? data = entity.ValorMinimoAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMinimoAplicacao = null;
					else entity.ValorMinimoAplicacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorMinimoResgate
			{
				get
				{
					System.Decimal? data = entity.ValorMinimoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMinimoResgate = null;
					else entity.ValorMinimoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorMinimoSaldo
			{
				get
				{
					System.Decimal? data = entity.ValorMinimoSaldo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMinimoSaldo = null;
					else entity.ValorMinimoSaldo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorMaximoAplicacao
			{
				get
				{
					System.Decimal? data = entity.ValorMaximoAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMaximoAplicacao = null;
					else entity.ValorMaximoAplicacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorMinimoInicial
			{
				get
				{
					System.Decimal? data = entity.ValorMinimoInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMinimoInicial = null;
					else entity.ValorMinimoInicial = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoRentabilidade
			{
				get
				{
					System.Byte? data = entity.TipoRentabilidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoRentabilidade = null;
					else entity.TipoRentabilidade = Convert.ToByte(value);
				}
			}
				
			public System.String TipoCusto
			{
				get
				{
					System.Byte? data = entity.TipoCusto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCusto = null;
					else entity.TipoCusto = Convert.ToByte(value);
				}
			}
				
			public System.String DiasCotizacaoAplicacao
			{
				get
				{
					System.Byte? data = entity.DiasCotizacaoAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasCotizacaoAplicacao = null;
					else entity.DiasCotizacaoAplicacao = Convert.ToByte(value);
				}
			}
				
			public System.String DiasCotizacaoResgate
			{
				get
				{
					System.Byte? data = entity.DiasCotizacaoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasCotizacaoResgate = null;
					else entity.DiasCotizacaoResgate = Convert.ToByte(value);
				}
			}
				
			public System.String DiasLiquidacaoAplicacao
			{
				get
				{
					System.Byte? data = entity.DiasLiquidacaoAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasLiquidacaoAplicacao = null;
					else entity.DiasLiquidacaoAplicacao = Convert.ToByte(value);
				}
			}
				
			public System.String DiasLiquidacaoResgate
			{
				get
				{
					System.Byte? data = entity.DiasLiquidacaoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasLiquidacaoResgate = null;
					else entity.DiasLiquidacaoResgate = Convert.ToByte(value);
				}
			}
				
			public System.String TipoTributacao
			{
				get
				{
					System.Byte? data = entity.TipoTributacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoTributacao = null;
					else entity.TipoTributacao = Convert.ToByte(value);
				}
			}
				
			public System.String CalculaIOF
			{
				get
				{
					System.String data = entity.CalculaIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CalculaIOF = null;
					else entity.CalculaIOF = Convert.ToString(value);
				}
			}
				
			public System.String DiasAniversario
			{
				get
				{
					System.Int16? data = entity.DiasAniversario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasAniversario = null;
					else entity.DiasAniversario = Convert.ToInt16(value);
				}
			}
				
			public System.String TipoAniversario
			{
				get
				{
					System.Byte? data = entity.TipoAniversario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoAniversario = null;
					else entity.TipoAniversario = Convert.ToByte(value);
				}
			}
				
			public System.String HorarioInicio
			{
				get
				{
					System.DateTime? data = entity.HorarioInicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.HorarioInicio = null;
					else entity.HorarioInicio = Convert.ToDateTime(value);
				}
			}
				
			public System.String HorarioFim
			{
				get
				{
					System.DateTime? data = entity.HorarioFim;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.HorarioFim = null;
					else entity.HorarioFim = Convert.ToDateTime(value);
				}
			}
				
			public System.String HorarioLimiteCotizacao
			{
				get
				{
					System.DateTime? data = entity.HorarioLimiteCotizacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.HorarioLimiteCotizacao = null;
					else entity.HorarioLimiteCotizacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCategoria
			{
				get
				{
					System.Int32? data = entity.IdCategoria;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCategoria = null;
					else entity.IdCategoria = Convert.ToInt32(value);
				}
			}
				
			public System.String IdSubCategoria
			{
				get
				{
					System.Int32? data = entity.IdSubCategoria;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSubCategoria = null;
					else entity.IdSubCategoria = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteAdministrador
			{
				get
				{
					System.Int32? data = entity.IdAgenteAdministrador;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteAdministrador = null;
					else entity.IdAgenteAdministrador = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteGestor
			{
				get
				{
					System.Int32? data = entity.IdAgenteGestor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteGestor = null;
					else entity.IdAgenteGestor = Convert.ToInt32(value);
				}
			}
				
			public System.String CobraTaxaFiscalizacaoCVM
			{
				get
				{
					System.String data = entity.CobraTaxaFiscalizacaoCVM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CobraTaxaFiscalizacaoCVM = null;
					else entity.CobraTaxaFiscalizacaoCVM = Convert.ToString(value);
				}
			}
				
			public System.String TipoCVM
			{
				get
				{
					System.Int32? data = entity.TipoCVM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCVM = null;
					else entity.TipoCVM = Convert.ToInt32(value);
				}
			}
				
			public System.String DataInicioCota
			{
				get
				{
					System.DateTime? data = entity.DataInicioCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicioCota = null;
					else entity.DataInicioCota = Convert.ToDateTime(value);
				}
			}
				
			public System.String CalculaEnquadra
			{
				get
				{
					System.String data = entity.CalculaEnquadra;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CalculaEnquadra = null;
					else entity.CalculaEnquadra = Convert.ToString(value);
				}
			}
				
			public System.String ContagemDiasConversaoResgate
			{
				get
				{
					System.Int32? data = entity.ContagemDiasConversaoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContagemDiasConversaoResgate = null;
					else entity.ContagemDiasConversaoResgate = Convert.ToInt32(value);
				}
			}
				
			public System.String ProjecaoIRResgate
			{
				get
				{
					System.Byte? data = entity.ProjecaoIRResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ProjecaoIRResgate = null;
					else entity.ProjecaoIRResgate = Convert.ToByte(value);
				}
			}
				
			public System.String PublicoAlvo
			{
				get
				{
					System.String data = entity.PublicoAlvo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PublicoAlvo = null;
					else entity.PublicoAlvo = Convert.ToString(value);
				}
			}
				
			public System.String Objetivo
			{
				get
				{
					System.String data = entity.Objetivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Objetivo = null;
					else entity.Objetivo = Convert.ToString(value);
				}
			}
				
			public System.String IdAgenteCustodiante
			{
				get
				{
					System.Int32? data = entity.IdAgenteCustodiante;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteCustodiante = null;
					else entity.IdAgenteCustodiante = Convert.ToInt32(value);
				}
			}
				
			public System.String TextoLivre1
			{
				get
				{
					System.String data = entity.TextoLivre1;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TextoLivre1 = null;
					else entity.TextoLivre1 = Convert.ToString(value);
				}
			}
				
			public System.String TextoLivre2
			{
				get
				{
					System.String data = entity.TextoLivre2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TextoLivre2 = null;
					else entity.TextoLivre2 = Convert.ToString(value);
				}
			}
				
			public System.String TextoLivre3
			{
				get
				{
					System.String data = entity.TextoLivre3;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TextoLivre3 = null;
					else entity.TextoLivre3 = Convert.ToString(value);
				}
			}
				
			public System.String TextoLivre4
			{
				get
				{
					System.String data = entity.TextoLivre4;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TextoLivre4 = null;
					else entity.TextoLivre4 = Convert.ToString(value);
				}
			}
				
			public System.String TipoVisaoFundo
			{
				get
				{
					System.Byte? data = entity.TipoVisaoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoVisaoFundo = null;
					else entity.TipoVisaoFundo = Convert.ToByte(value);
				}
			}
				
			public System.String DistribuicaoDividendo
			{
				get
				{
					System.Byte? data = entity.DistribuicaoDividendo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DistribuicaoDividendo = null;
					else entity.DistribuicaoDividendo = Convert.ToByte(value);
				}
			}
				
			public System.String IdEstrategia
			{
				get
				{
					System.Int32? data = entity.IdEstrategia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEstrategia = null;
					else entity.IdEstrategia = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoAnbid
			{
				get
				{
					System.String data = entity.CodigoAnbid;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoAnbid = null;
					else entity.CodigoAnbid = Convert.ToString(value);
				}
			}
				
			public System.String ContagemPrazoIOF
			{
				get
				{
					System.Byte? data = entity.ContagemPrazoIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContagemPrazoIOF = null;
					else entity.ContagemPrazoIOF = Convert.ToByte(value);
				}
			}
				
			public System.String PrioridadeOperacao
			{
				get
				{
					System.Byte? data = entity.PrioridadeOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrioridadeOperacao = null;
					else entity.PrioridadeOperacao = Convert.ToByte(value);
				}
			}
				
			public System.String CodigoCDA
			{
				get
				{
					System.Int32? data = entity.CodigoCDA;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCDA = null;
					else entity.CodigoCDA = Convert.ToInt32(value);
				}
			}
				
			public System.String CompensacaoPrejuizo
			{
				get
				{
					System.Byte? data = entity.CompensacaoPrejuizo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CompensacaoPrejuizo = null;
					else entity.CompensacaoPrejuizo = Convert.ToByte(value);
				}
			}
				
			public System.String DiasConfidencialidade
			{
				get
				{
					System.Int32? data = entity.DiasConfidencialidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasConfidencialidade = null;
					else entity.DiasConfidencialidade = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoFDIC
			{
				get
				{
					System.Int32? data = entity.CodigoFDIC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoFDIC = null;
					else entity.CodigoFDIC = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoCalculoRetorno
			{
				get
				{
					System.Byte? data = entity.TipoCalculoRetorno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCalculoRetorno = null;
					else entity.TipoCalculoRetorno = Convert.ToByte(value);
				}
			}
				
			public System.String IdGrauRisco
			{
				get
				{
					System.Int32? data = entity.IdGrauRisco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrauRisco = null;
					else entity.IdGrauRisco = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoIsin
			{
				get
				{
					System.String data = entity.CodigoIsin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoIsin = null;
					else entity.CodigoIsin = Convert.ToString(value);
				}
			}
				
			public System.String CodigoCetip
			{
				get
				{
					System.String data = entity.CodigoCetip;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCetip = null;
					else entity.CodigoCetip = Convert.ToString(value);
				}
			}
				
			public System.String CodigoBloomberg
			{
				get
				{
					System.String data = entity.CodigoBloomberg;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoBloomberg = null;
					else entity.CodigoBloomberg = Convert.ToString(value);
				}
			}
				
			public System.String HorarioInicioResgate
			{
				get
				{
					System.DateTime? data = entity.HorarioInicioResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.HorarioInicioResgate = null;
					else entity.HorarioInicioResgate = Convert.ToDateTime(value);
				}
			}
				
			public System.String HorarioFimResgate
			{
				get
				{
					System.DateTime? data = entity.HorarioFimResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.HorarioFimResgate = null;
					else entity.HorarioFimResgate = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdGrupoEconomico
			{
				get
				{
					System.Int32? data = entity.IdGrupoEconomico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupoEconomico = null;
					else entity.IdGrupoEconomico = Convert.ToInt32(value);
				}
			}
				
			public System.String FundoExclusivo
			{
				get
				{
					System.String data = entity.FundoExclusivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FundoExclusivo = null;
					else entity.FundoExclusivo = Convert.ToString(value);
				}
			}
				
			public System.String OrdemRelatorio
			{
				get
				{
					System.Int32? data = entity.OrdemRelatorio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OrdemRelatorio = null;
					else entity.OrdemRelatorio = Convert.ToInt32(value);
				}
			}
				
			public System.String LiberaAplicacao
			{
				get
				{
					System.String data = entity.LiberaAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LiberaAplicacao = null;
					else entity.LiberaAplicacao = Convert.ToString(value);
				}
			}
				
			public System.String LiberaResgate
			{
				get
				{
					System.String data = entity.LiberaResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LiberaResgate = null;
					else entity.LiberaResgate = Convert.ToString(value);
				}
			}
				
			public System.String PerfilRisco
			{
				get
				{
					System.Int32? data = entity.PerfilRisco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerfilRisco = null;
					else entity.PerfilRisco = Convert.ToInt32(value);
				}
			}
				
			public System.String CalculaMTM
			{
				get
				{
					System.Int32? data = entity.CalculaMTM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CalculaMTM = null;
					else entity.CalculaMTM = Convert.ToInt32(value);
				}
			}
				
			public System.String CalculaPrazoMedio
			{
				get
				{
					System.String data = entity.CalculaPrazoMedio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CalculaPrazoMedio = null;
					else entity.CalculaPrazoMedio = Convert.ToString(value);
				}
			}
				
			public System.String DiasAposResgateIR
			{
				get
				{
					System.Int32? data = entity.DiasAposResgateIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasAposResgateIR = null;
					else entity.DiasAposResgateIR = Convert.ToInt32(value);
				}
			}
				
			public System.String ProjecaoIRComeCotas
			{
				get
				{
					System.Byte? data = entity.ProjecaoIRComeCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ProjecaoIRComeCotas = null;
					else entity.ProjecaoIRComeCotas = Convert.ToByte(value);
				}
			}
				
			public System.String DiasAposComeCotasIR
			{
				get
				{
					System.Int32? data = entity.DiasAposComeCotasIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasAposComeCotasIR = null;
					else entity.DiasAposComeCotasIR = Convert.ToInt32(value);
				}
			}
				
			public System.String RealizaCompensacaoDePrejuizo
			{
				get
				{
					System.String data = entity.RealizaCompensacaoDePrejuizo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RealizaCompensacaoDePrejuizo = null;
					else entity.RealizaCompensacaoDePrejuizo = Convert.ToString(value);
				}
			}
				
			public System.String BuscaCotaAnterior
			{
				get
				{
					System.String data = entity.BuscaCotaAnterior;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BuscaCotaAnterior = null;
					else entity.BuscaCotaAnterior = Convert.ToString(value);
				}
			}
				
			public System.String NumeroDiasBuscaCota
			{
				get
				{
					System.Int32? data = entity.NumeroDiasBuscaCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroDiasBuscaCota = null;
					else entity.NumeroDiasBuscaCota = Convert.ToInt32(value);
				}
			}
				
			public System.String BandaVariacao
			{
				get
				{
					System.Decimal? data = entity.BandaVariacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BandaVariacao = null;
					else entity.BandaVariacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ExcecaoRegraTxAdm
			{
				get
				{
					System.String data = entity.ExcecaoRegraTxAdm;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExcecaoRegraTxAdm = null;
					else entity.ExcecaoRegraTxAdm = Convert.ToString(value);
				}
			}
				
			public System.String RebateImpactaPL
			{
				get
				{
					System.String data = entity.RebateImpactaPL;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RebateImpactaPL = null;
					else entity.RebateImpactaPL = Convert.ToString(value);
				}
			}
				
			public System.String TipoFundo
			{
				get
				{
					System.Int32? data = entity.TipoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoFundo = null;
					else entity.TipoFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String Fie
			{
				get
				{
					System.String data = entity.Fie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fie = null;
					else entity.Fie = Convert.ToString(value);
				}
			}
				
			public System.String DataInicioContIOF
			{
				get
				{
					System.Int16? data = entity.DataInicioContIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicioContIOF = null;
					else entity.DataInicioContIOF = Convert.ToInt16(value);
				}
			}
				
			public System.String DataFimContIOF
			{
				get
				{
					System.Int16? data = entity.DataFimContIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFimContIOF = null;
					else entity.DataFimContIOF = Convert.ToInt16(value);
				}
			}
				
			public System.String ContaPrzIOFVirtual
			{
				get
				{
					System.Int16? data = entity.ContaPrzIOFVirtual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContaPrzIOFVirtual = null;
					else entity.ContaPrzIOFVirtual = Convert.ToInt16(value);
				}
			}
				
			public System.String ProjecaoIOFResgate
			{
				get
				{
					System.Byte? data = entity.ProjecaoIOFResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ProjecaoIOFResgate = null;
					else entity.ProjecaoIOFResgate = Convert.ToByte(value);
				}
			}
				
			public System.String DiasAposResgateIOF
			{
				get
				{
					System.Int32? data = entity.DiasAposResgateIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiasAposResgateIOF = null;
					else entity.DiasAposResgateIOF = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoConsolidacaoExterno
			{
				get
				{
					System.String data = entity.CodigoConsolidacaoExterno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoConsolidacaoExterno = null;
					else entity.CodigoConsolidacaoExterno = Convert.ToString(value);
				}
			}
				
			public System.String CodigoBDS
			{
				get
				{
					System.Int32? data = entity.CodigoBDS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoBDS = null;
					else entity.CodigoBDS = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoSTI
			{
				get
				{
					System.String data = entity.CodigoSTI;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoSTI = null;
					else entity.CodigoSTI = Convert.ToString(value);
				}
			}
				
			public System.String TipoVisualizacaoResgCotista
			{
				get
				{
					System.String data = entity.TipoVisualizacaoResgCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoVisualizacaoResgCotista = null;
					else entity.TipoVisualizacaoResgCotista = Convert.ToString(value);
				}
			}
				
			public System.String ApenasInvestidorProfissional
			{
				get
				{
					System.String data = entity.ApenasInvestidorProfissional;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ApenasInvestidorProfissional = null;
					else entity.ApenasInvestidorProfissional = Convert.ToString(value);
				}
			}
				
			public System.String ApenasInvestidorQualificado
			{
				get
				{
					System.String data = entity.ApenasInvestidorQualificado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ApenasInvestidorQualificado = null;
					else entity.ApenasInvestidorQualificado = Convert.ToString(value);
				}
			}
				
			public System.String RealizaOfertaSubscricao
			{
				get
				{
					System.String data = entity.RealizaOfertaSubscricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RealizaOfertaSubscricao = null;
					else entity.RealizaOfertaSubscricao = Convert.ToString(value);
				}
			}
				
			public System.String Rendimento
			{
				get
				{
					System.Int32? data = entity.Rendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Rendimento = null;
					else entity.Rendimento = Convert.ToInt32(value);
				}
			}
				
			public System.String IdGrupoPerfilMTM
			{
				get
				{
					System.Int32? data = entity.IdGrupoPerfilMTM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupoPerfilMTM = null;
					else entity.IdGrupoPerfilMTM = Convert.ToInt32(value);
				}
			}
				
			public System.String ProcAutomatico
			{
				get
				{
					System.String data = entity.ProcAutomatico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ProcAutomatico = null;
					else entity.ProcAutomatico = Convert.ToString(value);
				}
			}
				
			public System.String ExplodeCotasDeFundos
			{
				get
				{
					System.String data = entity.ExplodeCotasDeFundos;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExplodeCotasDeFundos = null;
					else entity.ExplodeCotasDeFundos = Convert.ToString(value);
				}
			}
				
			public System.String ControladoriaAtivo
			{
				get
				{
					System.String data = entity.ControladoriaAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ControladoriaAtivo = null;
					else entity.ControladoriaAtivo = Convert.ToString(value);
				}
			}
				
			public System.String ControladoriaPassivo
			{
				get
				{
					System.String data = entity.ControladoriaPassivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ControladoriaPassivo = null;
					else entity.ControladoriaPassivo = Convert.ToString(value);
				}
			}
				
			public System.String MesmoConglomerado
			{
				get
				{
					System.String data = entity.MesmoConglomerado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MesmoConglomerado = null;
					else entity.MesmoConglomerado = Convert.ToString(value);
				}
			}
				
			public System.String CategoriaAnbima
			{
				get
				{
					System.Int32? data = entity.CategoriaAnbima;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CategoriaAnbima = null;
					else entity.CategoriaAnbima = Convert.ToInt32(value);
				}
			}
				
			public System.String Contratante
			{
				get
				{
					System.Int32? data = entity.Contratante;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Contratante = null;
					else entity.Contratante = Convert.ToInt32(value);
				}
			}
				
			public System.String Corretora
			{
				get
				{
					System.String data = entity.Corretora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Corretora = null;
					else entity.Corretora = Convert.ToString(value);
				}
			}
				
			public System.String ExportaGalgo
			{
				get
				{
					System.String data = entity.ExportaGalgo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExportaGalgo = null;
					else entity.ExportaGalgo = Convert.ToString(value);
				}
			}
				
			public System.String PossuiResgateAutomatico
			{
				get
				{
					System.String data = entity.PossuiResgateAutomatico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PossuiResgateAutomatico = null;
					else entity.PossuiResgateAutomatico = Convert.ToString(value);
				}
			}
				
			public System.String InfluenciaGestorLocalCvm
			{
				get
				{
					System.String data = entity.InfluenciaGestorLocalCvm;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InfluenciaGestorLocalCvm = null;
					else entity.InfluenciaGestorLocalCvm = Convert.ToString(value);
				}
			}
				
			public System.String InvestimentoColetivoCvm
			{
				get
				{
					System.String data = entity.InvestimentoColetivoCvm;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.InvestimentoColetivoCvm = null;
					else entity.InvestimentoColetivoCvm = Convert.ToString(value);
				}
			}
				
			public System.String ComeCotasEntreRegatesConversao
			{
				get
				{
					System.String data = entity.ComeCotasEntreRegatesConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ComeCotasEntreRegatesConversao = null;
					else entity.ComeCotasEntreRegatesConversao = Convert.ToString(value);
				}
			}
			

			private esCarteira entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCarteiraQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCarteira can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Carteira : esCarteira
	{

				
		#region AgendaComeCotasCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__AgendaCom__IdCar__20ECC9AD
		/// </summary>

		[XmlIgnore]
		public AgendaComeCotasCollection AgendaComeCotasCollectionByIdCarteira
		{
			get
			{
				if(this._AgendaComeCotasCollectionByIdCarteira == null)
				{
					this._AgendaComeCotasCollectionByIdCarteira = new AgendaComeCotasCollection();
					this._AgendaComeCotasCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AgendaComeCotasCollectionByIdCarteira", this._AgendaComeCotasCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._AgendaComeCotasCollectionByIdCarteira.Query.Where(this._AgendaComeCotasCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._AgendaComeCotasCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._AgendaComeCotasCollectionByIdCarteira.fks.Add(AgendaComeCotasMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._AgendaComeCotasCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AgendaComeCotasCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("AgendaComeCotasCollectionByIdCarteira"); 
					this._AgendaComeCotasCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private AgendaComeCotasCollection _AgendaComeCotasCollectionByIdCarteira;
		#endregion

				
		#region AgendaFundoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_AgendaFundo_FK1
		/// </summary>

		[XmlIgnore]
		public AgendaFundoCollection AgendaFundoCollectionByIdCarteira
		{
			get
			{
				if(this._AgendaFundoCollectionByIdCarteira == null)
				{
					this._AgendaFundoCollectionByIdCarteira = new AgendaFundoCollection();
					this._AgendaFundoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AgendaFundoCollectionByIdCarteira", this._AgendaFundoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._AgendaFundoCollectionByIdCarteira.Query.Where(this._AgendaFundoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._AgendaFundoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._AgendaFundoCollectionByIdCarteira.fks.Add(AgendaFundoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._AgendaFundoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AgendaFundoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("AgendaFundoCollectionByIdCarteira"); 
					this._AgendaFundoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private AgendaFundoCollection _AgendaFundoCollectionByIdCarteira;
		#endregion

				
		#region AliquotaEspecificaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AliquotaEspecifica_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public AliquotaEspecificaCollection AliquotaEspecificaCollectionByIdCarteira
		{
			get
			{
				if(this._AliquotaEspecificaCollectionByIdCarteira == null)
				{
					this._AliquotaEspecificaCollectionByIdCarteira = new AliquotaEspecificaCollection();
					this._AliquotaEspecificaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AliquotaEspecificaCollectionByIdCarteira", this._AliquotaEspecificaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._AliquotaEspecificaCollectionByIdCarteira.Query.Where(this._AliquotaEspecificaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._AliquotaEspecificaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._AliquotaEspecificaCollectionByIdCarteira.fks.Add(AliquotaEspecificaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._AliquotaEspecificaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AliquotaEspecificaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("AliquotaEspecificaCollectionByIdCarteira"); 
					this._AliquotaEspecificaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private AliquotaEspecificaCollection _AliquotaEspecificaCollectionByIdCarteira;
		#endregion

				
		#region CalculoAdministracaoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_CalculoAdministracao_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoAdministracaoCollection CalculoAdministracaoCollectionByIdCarteira
		{
			get
			{
				if(this._CalculoAdministracaoCollectionByIdCarteira == null)
				{
					this._CalculoAdministracaoCollectionByIdCarteira = new CalculoAdministracaoCollection();
					this._CalculoAdministracaoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoAdministracaoCollectionByIdCarteira", this._CalculoAdministracaoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._CalculoAdministracaoCollectionByIdCarteira.Query.Where(this._CalculoAdministracaoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._CalculoAdministracaoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoAdministracaoCollectionByIdCarteira.fks.Add(CalculoAdministracaoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._CalculoAdministracaoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoAdministracaoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("CalculoAdministracaoCollectionByIdCarteira"); 
					this._CalculoAdministracaoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private CalculoAdministracaoCollection _CalculoAdministracaoCollectionByIdCarteira;
		#endregion

				
		#region CalculoAdministracaoAssociadaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_CalculoAdministracaoAssociada_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoAdministracaoAssociadaCollection CalculoAdministracaoAssociadaCollectionByIdCarteira
		{
			get
			{
				if(this._CalculoAdministracaoAssociadaCollectionByIdCarteira == null)
				{
					this._CalculoAdministracaoAssociadaCollectionByIdCarteira = new CalculoAdministracaoAssociadaCollection();
					this._CalculoAdministracaoAssociadaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoAdministracaoAssociadaCollectionByIdCarteira", this._CalculoAdministracaoAssociadaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._CalculoAdministracaoAssociadaCollectionByIdCarteira.Query.Where(this._CalculoAdministracaoAssociadaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._CalculoAdministracaoAssociadaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoAdministracaoAssociadaCollectionByIdCarteira.fks.Add(CalculoAdministracaoAssociadaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._CalculoAdministracaoAssociadaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoAdministracaoAssociadaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("CalculoAdministracaoAssociadaCollectionByIdCarteira"); 
					this._CalculoAdministracaoAssociadaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private CalculoAdministracaoAssociadaCollection _CalculoAdministracaoAssociadaCollectionByIdCarteira;
		#endregion

				
		#region CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_CalculoAdministracaoAssociadaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoAdministracaoAssociadaHistoricoCollection CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira
		{
			get
			{
				if(this._CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira == null)
				{
					this._CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira = new CalculoAdministracaoAssociadaHistoricoCollection();
					this._CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira", this._CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira.Query.Where(this._CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira.fks.Add(CalculoAdministracaoAssociadaHistoricoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira"); 
					this._CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private CalculoAdministracaoAssociadaHistoricoCollection _CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira;
		#endregion

				
		#region CalculoAdministracaoHistoricoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_CalculoAdministracaoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoAdministracaoHistoricoCollection CalculoAdministracaoHistoricoCollectionByIdCarteira
		{
			get
			{
				if(this._CalculoAdministracaoHistoricoCollectionByIdCarteira == null)
				{
					this._CalculoAdministracaoHistoricoCollectionByIdCarteira = new CalculoAdministracaoHistoricoCollection();
					this._CalculoAdministracaoHistoricoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoAdministracaoHistoricoCollectionByIdCarteira", this._CalculoAdministracaoHistoricoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._CalculoAdministracaoHistoricoCollectionByIdCarteira.Query.Where(this._CalculoAdministracaoHistoricoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._CalculoAdministracaoHistoricoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoAdministracaoHistoricoCollectionByIdCarteira.fks.Add(CalculoAdministracaoHistoricoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._CalculoAdministracaoHistoricoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoAdministracaoHistoricoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("CalculoAdministracaoHistoricoCollectionByIdCarteira"); 
					this._CalculoAdministracaoHistoricoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private CalculoAdministracaoHistoricoCollection _CalculoAdministracaoHistoricoCollectionByIdCarteira;
		#endregion

				
		#region CalculoPerformanceCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_CalculoPerformance_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoPerformanceCollection CalculoPerformanceCollectionByIdCarteira
		{
			get
			{
				if(this._CalculoPerformanceCollectionByIdCarteira == null)
				{
					this._CalculoPerformanceCollectionByIdCarteira = new CalculoPerformanceCollection();
					this._CalculoPerformanceCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoPerformanceCollectionByIdCarteira", this._CalculoPerformanceCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._CalculoPerformanceCollectionByIdCarteira.Query.Where(this._CalculoPerformanceCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._CalculoPerformanceCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoPerformanceCollectionByIdCarteira.fks.Add(CalculoPerformanceMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._CalculoPerformanceCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoPerformanceCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("CalculoPerformanceCollectionByIdCarteira"); 
					this._CalculoPerformanceCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private CalculoPerformanceCollection _CalculoPerformanceCollectionByIdCarteira;
		#endregion

				
		#region CalculoPerformanceHistoricoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_CalculoPerformanceHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoPerformanceHistoricoCollection CalculoPerformanceHistoricoCollectionByIdCarteira
		{
			get
			{
				if(this._CalculoPerformanceHistoricoCollectionByIdCarteira == null)
				{
					this._CalculoPerformanceHistoricoCollectionByIdCarteira = new CalculoPerformanceHistoricoCollection();
					this._CalculoPerformanceHistoricoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoPerformanceHistoricoCollectionByIdCarteira", this._CalculoPerformanceHistoricoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._CalculoPerformanceHistoricoCollectionByIdCarteira.Query.Where(this._CalculoPerformanceHistoricoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._CalculoPerformanceHistoricoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoPerformanceHistoricoCollectionByIdCarteira.fks.Add(CalculoPerformanceHistoricoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._CalculoPerformanceHistoricoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoPerformanceHistoricoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("CalculoPerformanceHistoricoCollectionByIdCarteira"); 
					this._CalculoPerformanceHistoricoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private CalculoPerformanceHistoricoCollection _CalculoPerformanceHistoricoCollectionByIdCarteira;
		#endregion

				
		#region CalculoProvisaoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_CalculoProvisao_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoProvisaoCollection CalculoProvisaoCollectionByIdCarteira
		{
			get
			{
				if(this._CalculoProvisaoCollectionByIdCarteira == null)
				{
					this._CalculoProvisaoCollectionByIdCarteira = new CalculoProvisaoCollection();
					this._CalculoProvisaoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoProvisaoCollectionByIdCarteira", this._CalculoProvisaoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._CalculoProvisaoCollectionByIdCarteira.Query.Where(this._CalculoProvisaoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._CalculoProvisaoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoProvisaoCollectionByIdCarteira.fks.Add(CalculoProvisaoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._CalculoProvisaoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoProvisaoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("CalculoProvisaoCollectionByIdCarteira"); 
					this._CalculoProvisaoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private CalculoProvisaoCollection _CalculoProvisaoCollectionByIdCarteira;
		#endregion

				
		#region CalculoProvisaoHistoricoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_CalculoProvisaoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoProvisaoHistoricoCollection CalculoProvisaoHistoricoCollectionByIdCarteira
		{
			get
			{
				if(this._CalculoProvisaoHistoricoCollectionByIdCarteira == null)
				{
					this._CalculoProvisaoHistoricoCollectionByIdCarteira = new CalculoProvisaoHistoricoCollection();
					this._CalculoProvisaoHistoricoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoProvisaoHistoricoCollectionByIdCarteira", this._CalculoProvisaoHistoricoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._CalculoProvisaoHistoricoCollectionByIdCarteira.Query.Where(this._CalculoProvisaoHistoricoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._CalculoProvisaoHistoricoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoProvisaoHistoricoCollectionByIdCarteira.fks.Add(CalculoProvisaoHistoricoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._CalculoProvisaoHistoricoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoProvisaoHistoricoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("CalculoProvisaoHistoricoCollectionByIdCarteira"); 
					this._CalculoProvisaoHistoricoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private CalculoProvisaoHistoricoCollection _CalculoProvisaoHistoricoCollectionByIdCarteira;
		#endregion

				
		#region CalculoRebateCarteiraCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_CalculoRebateCarteira_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoRebateCarteiraCollection CalculoRebateCarteiraCollectionByIdCarteira
		{
			get
			{
				if(this._CalculoRebateCarteiraCollectionByIdCarteira == null)
				{
					this._CalculoRebateCarteiraCollectionByIdCarteira = new CalculoRebateCarteiraCollection();
					this._CalculoRebateCarteiraCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoRebateCarteiraCollectionByIdCarteira", this._CalculoRebateCarteiraCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._CalculoRebateCarteiraCollectionByIdCarteira.Query.Where(this._CalculoRebateCarteiraCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._CalculoRebateCarteiraCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoRebateCarteiraCollectionByIdCarteira.fks.Add(CalculoRebateCarteiraMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._CalculoRebateCarteiraCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoRebateCarteiraCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("CalculoRebateCarteiraCollectionByIdCarteira"); 
					this._CalculoRebateCarteiraCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private CalculoRebateCarteiraCollection _CalculoRebateCarteiraCollectionByIdCarteira;
		#endregion

				
		#region CalculoRebateCotistaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_CalculoRebateCotista_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoRebateCotistaCollection CalculoRebateCotistaCollectionByIdCarteira
		{
			get
			{
				if(this._CalculoRebateCotistaCollectionByIdCarteira == null)
				{
					this._CalculoRebateCotistaCollectionByIdCarteira = new CalculoRebateCotistaCollection();
					this._CalculoRebateCotistaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoRebateCotistaCollectionByIdCarteira", this._CalculoRebateCotistaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._CalculoRebateCotistaCollectionByIdCarteira.Query.Where(this._CalculoRebateCotistaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._CalculoRebateCotistaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoRebateCotistaCollectionByIdCarteira.fks.Add(CalculoRebateCotistaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._CalculoRebateCotistaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoRebateCotistaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("CalculoRebateCotistaCollectionByIdCarteira"); 
					this._CalculoRebateCotistaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private CalculoRebateCotistaCollection _CalculoRebateCotistaCollectionByIdCarteira;
		#endregion

				
		#region CalculoRebateImpactaPLCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - CalculoRebateImpactaPL_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoRebateImpactaPLCollection CalculoRebateImpactaPLCollectionByIdCarteira
		{
			get
			{
				if(this._CalculoRebateImpactaPLCollectionByIdCarteira == null)
				{
					this._CalculoRebateImpactaPLCollectionByIdCarteira = new CalculoRebateImpactaPLCollection();
					this._CalculoRebateImpactaPLCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoRebateImpactaPLCollectionByIdCarteira", this._CalculoRebateImpactaPLCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._CalculoRebateImpactaPLCollectionByIdCarteira.Query.Where(this._CalculoRebateImpactaPLCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._CalculoRebateImpactaPLCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoRebateImpactaPLCollectionByIdCarteira.fks.Add(CalculoRebateImpactaPLMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._CalculoRebateImpactaPLCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoRebateImpactaPLCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("CalculoRebateImpactaPLCollectionByIdCarteira"); 
					this._CalculoRebateImpactaPLCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private CalculoRebateImpactaPLCollection _CalculoRebateImpactaPLCollectionByIdCarteira;
		#endregion

				
		#region CarteiraComplementoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - CarteiraComplemento_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public CarteiraComplementoCollection CarteiraComplementoCollectionByIdCarteira
		{
			get
			{
				if(this._CarteiraComplementoCollectionByIdCarteira == null)
				{
					this._CarteiraComplementoCollectionByIdCarteira = new CarteiraComplementoCollection();
					this._CarteiraComplementoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CarteiraComplementoCollectionByIdCarteira", this._CarteiraComplementoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._CarteiraComplementoCollectionByIdCarteira.Query.Where(this._CarteiraComplementoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._CarteiraComplementoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._CarteiraComplementoCollectionByIdCarteira.fks.Add(CarteiraComplementoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._CarteiraComplementoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CarteiraComplementoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("CarteiraComplementoCollectionByIdCarteira"); 
					this._CarteiraComplementoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private CarteiraComplementoCollection _CarteiraComplementoCollectionByIdCarteira;
		#endregion

				
		#region CarteiraComplementoCollectionByIdCarteiraFundoPai - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - CarteiraComplemento_Carteira_FK2
		/// </summary>

		[XmlIgnore]
		public CarteiraComplementoCollection CarteiraComplementoCollectionByIdCarteiraFundoPai
		{
			get
			{
				if(this._CarteiraComplementoCollectionByIdCarteiraFundoPai == null)
				{
					this._CarteiraComplementoCollectionByIdCarteiraFundoPai = new CarteiraComplementoCollection();
					this._CarteiraComplementoCollectionByIdCarteiraFundoPai.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CarteiraComplementoCollectionByIdCarteiraFundoPai", this._CarteiraComplementoCollectionByIdCarteiraFundoPai);
				
					if(this.IdCarteira != null)
					{
						this._CarteiraComplementoCollectionByIdCarteiraFundoPai.Query.Where(this._CarteiraComplementoCollectionByIdCarteiraFundoPai.Query.IdCarteiraFundoPai == this.IdCarteira);
						this._CarteiraComplementoCollectionByIdCarteiraFundoPai.Query.Load();

						// Auto-hookup Foreign Keys
						this._CarteiraComplementoCollectionByIdCarteiraFundoPai.fks.Add(CarteiraComplementoMetadata.ColumnNames.IdCarteiraFundoPai, this.IdCarteira);
					}
				}

				return this._CarteiraComplementoCollectionByIdCarteiraFundoPai;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CarteiraComplementoCollectionByIdCarteiraFundoPai != null) 
				{ 
					this.RemovePostSave("CarteiraComplementoCollectionByIdCarteiraFundoPai"); 
					this._CarteiraComplementoCollectionByIdCarteiraFundoPai = null;
					
				} 
			} 			
		}

		private CarteiraComplementoCollection _CarteiraComplementoCollectionByIdCarteiraFundoPai;
		#endregion

				
		#region CarteiraMaeCollectionByIdCarteiraMae - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - CarteiraMae_Mae_FK
		/// </summary>

		[XmlIgnore]
		public CarteiraMaeCollection CarteiraMaeCollectionByIdCarteiraMae
		{
			get
			{
				if(this._CarteiraMaeCollectionByIdCarteiraMae == null)
				{
					this._CarteiraMaeCollectionByIdCarteiraMae = new CarteiraMaeCollection();
					this._CarteiraMaeCollectionByIdCarteiraMae.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CarteiraMaeCollectionByIdCarteiraMae", this._CarteiraMaeCollectionByIdCarteiraMae);
				
					if(this.IdCarteira != null)
					{
						this._CarteiraMaeCollectionByIdCarteiraMae.Query.Where(this._CarteiraMaeCollectionByIdCarteiraMae.Query.IdCarteiraMae == this.IdCarteira);
						this._CarteiraMaeCollectionByIdCarteiraMae.Query.Load();

						// Auto-hookup Foreign Keys
						this._CarteiraMaeCollectionByIdCarteiraMae.fks.Add(CarteiraMaeMetadata.ColumnNames.IdCarteiraMae, this.IdCarteira);
					}
				}

				return this._CarteiraMaeCollectionByIdCarteiraMae;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CarteiraMaeCollectionByIdCarteiraMae != null) 
				{ 
					this.RemovePostSave("CarteiraMaeCollectionByIdCarteiraMae"); 
					this._CarteiraMaeCollectionByIdCarteiraMae = null;
					
				} 
			} 			
		}

		private CarteiraMaeCollection _CarteiraMaeCollectionByIdCarteiraMae;
		#endregion

				
		#region CarteiraMaeCollectionByIdCarteiraFilha - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - CarteiraMae_Filha_FK
		/// </summary>

		[XmlIgnore]
		public CarteiraMaeCollection CarteiraMaeCollectionByIdCarteiraFilha
		{
			get
			{
				if(this._CarteiraMaeCollectionByIdCarteiraFilha == null)
				{
					this._CarteiraMaeCollectionByIdCarteiraFilha = new CarteiraMaeCollection();
					this._CarteiraMaeCollectionByIdCarteiraFilha.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CarteiraMaeCollectionByIdCarteiraFilha", this._CarteiraMaeCollectionByIdCarteiraFilha);
				
					if(this.IdCarteira != null)
					{
						this._CarteiraMaeCollectionByIdCarteiraFilha.Query.Where(this._CarteiraMaeCollectionByIdCarteiraFilha.Query.IdCarteiraFilha == this.IdCarteira);
						this._CarteiraMaeCollectionByIdCarteiraFilha.Query.Load();

						// Auto-hookup Foreign Keys
						this._CarteiraMaeCollectionByIdCarteiraFilha.fks.Add(CarteiraMaeMetadata.ColumnNames.IdCarteiraFilha, this.IdCarteira);
					}
				}

				return this._CarteiraMaeCollectionByIdCarteiraFilha;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CarteiraMaeCollectionByIdCarteiraFilha != null) 
				{ 
					this.RemovePostSave("CarteiraMaeCollectionByIdCarteiraFilha"); 
					this._CarteiraMaeCollectionByIdCarteiraFilha = null;
					
				} 
			} 			
		}

		private CarteiraMaeCollection _CarteiraMaeCollectionByIdCarteiraFilha;
		#endregion

				
		#region ClassesOffShore - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Classes_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public ClassesOffShore ClassesOffShore
		{
			get
			{
				if(this._ClassesOffShore == null)
				{
					this._ClassesOffShore = new ClassesOffShore();
					this._ClassesOffShore.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("ClassesOffShore", this._ClassesOffShore);
				
					if(this.IdCarteira != null)
					{
						this._ClassesOffShore.Query.Where(this._ClassesOffShore.Query.IdClassesOffShore == this.IdCarteira);
						this._ClassesOffShore.Query.Load();
					}
				}

				return this._ClassesOffShore;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ClassesOffShore != null) 
				{ 
					this.RemovePostOneSave("ClassesOffShore"); 
					this._ClassesOffShore = null;
					
				} 
			}          			
		}

		private ClassesOffShore _ClassesOffShore;
		#endregion

				
		#region CotistaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Cotista_IdCarteira_FK1
		/// </summary>

		[XmlIgnore]
		public CotistaCollection CotistaCollectionByIdCarteira
		{
			get
			{
				if(this._CotistaCollectionByIdCarteira == null)
				{
					this._CotistaCollectionByIdCarteira = new CotistaCollection();
					this._CotistaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CotistaCollectionByIdCarteira", this._CotistaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._CotistaCollectionByIdCarteira.Query.Where(this._CotistaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._CotistaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._CotistaCollectionByIdCarteira.fks.Add(CotistaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._CotistaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CotistaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("CotistaCollectionByIdCarteira"); 
					this._CotistaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private CotistaCollection _CotistaCollectionByIdCarteira;
		#endregion

				
		#region DesenquadramentoTributarioCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__Desenquad__Fundo__08211BE3
		/// </summary>

		[XmlIgnore]
		public DesenquadramentoTributarioCollection DesenquadramentoTributarioCollectionByIdCarteira
		{
			get
			{
				if(this._DesenquadramentoTributarioCollectionByIdCarteira == null)
				{
					this._DesenquadramentoTributarioCollectionByIdCarteira = new DesenquadramentoTributarioCollection();
					this._DesenquadramentoTributarioCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("DesenquadramentoTributarioCollectionByIdCarteira", this._DesenquadramentoTributarioCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._DesenquadramentoTributarioCollectionByIdCarteira.Query.Where(this._DesenquadramentoTributarioCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._DesenquadramentoTributarioCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._DesenquadramentoTributarioCollectionByIdCarteira.fks.Add(DesenquadramentoTributarioMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._DesenquadramentoTributarioCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._DesenquadramentoTributarioCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("DesenquadramentoTributarioCollectionByIdCarteira"); 
					this._DesenquadramentoTributarioCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private DesenquadramentoTributarioCollection _DesenquadramentoTributarioCollectionByIdCarteira;
		#endregion

				
		#region DetalheResgateCotistaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_DetalheResgateCotista_FK1
		/// </summary>

		[XmlIgnore]
		public DetalheResgateCotistaCollection DetalheResgateCotistaCollectionByIdCarteira
		{
			get
			{
				if(this._DetalheResgateCotistaCollectionByIdCarteira == null)
				{
					this._DetalheResgateCotistaCollectionByIdCarteira = new DetalheResgateCotistaCollection();
					this._DetalheResgateCotistaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("DetalheResgateCotistaCollectionByIdCarteira", this._DetalheResgateCotistaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._DetalheResgateCotistaCollectionByIdCarteira.Query.Where(this._DetalheResgateCotistaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._DetalheResgateCotistaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._DetalheResgateCotistaCollectionByIdCarteira.fks.Add(DetalheResgateCotistaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._DetalheResgateCotistaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._DetalheResgateCotistaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("DetalheResgateCotistaCollectionByIdCarteira"); 
					this._DetalheResgateCotistaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private DetalheResgateCotistaCollection _DetalheResgateCotistaCollectionByIdCarteira;
		#endregion

				
		#region DetalheResgateFundoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_DetalheResgateFundo_FK1
		/// </summary>

		[XmlIgnore]
		public DetalheResgateFundoCollection DetalheResgateFundoCollectionByIdCarteira
		{
			get
			{
				if(this._DetalheResgateFundoCollectionByIdCarteira == null)
				{
					this._DetalheResgateFundoCollectionByIdCarteira = new DetalheResgateFundoCollection();
					this._DetalheResgateFundoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("DetalheResgateFundoCollectionByIdCarteira", this._DetalheResgateFundoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._DetalheResgateFundoCollectionByIdCarteira.Query.Where(this._DetalheResgateFundoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._DetalheResgateFundoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._DetalheResgateFundoCollectionByIdCarteira.fks.Add(DetalheResgateFundoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._DetalheResgateFundoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._DetalheResgateFundoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("DetalheResgateFundoCollectionByIdCarteira"); 
					this._DetalheResgateFundoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private DetalheResgateFundoCollection _DetalheResgateFundoCollectionByIdCarteira;
		#endregion

				
		#region EnquadraItemFundoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_EnquadraItemFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemFundoCollection EnquadraItemFundoCollectionByIdCarteira
		{
			get
			{
				if(this._EnquadraItemFundoCollectionByIdCarteira == null)
				{
					this._EnquadraItemFundoCollectionByIdCarteira = new EnquadraItemFundoCollection();
					this._EnquadraItemFundoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraItemFundoCollectionByIdCarteira", this._EnquadraItemFundoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._EnquadraItemFundoCollectionByIdCarteira.Query.Where(this._EnquadraItemFundoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._EnquadraItemFundoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraItemFundoCollectionByIdCarteira.fks.Add(EnquadraItemFundoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._EnquadraItemFundoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemFundoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("EnquadraItemFundoCollectionByIdCarteira"); 
					this._EnquadraItemFundoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private EnquadraItemFundoCollection _EnquadraItemFundoCollectionByIdCarteira;
		#endregion

				
		#region EnquadraRegraCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_EnquadraRegra_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraRegraCollection EnquadraRegraCollectionByIdCarteira
		{
			get
			{
				if(this._EnquadraRegraCollectionByIdCarteira == null)
				{
					this._EnquadraRegraCollectionByIdCarteira = new EnquadraRegraCollection();
					this._EnquadraRegraCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraRegraCollectionByIdCarteira", this._EnquadraRegraCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._EnquadraRegraCollectionByIdCarteira.Query.Where(this._EnquadraRegraCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._EnquadraRegraCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraRegraCollectionByIdCarteira.fks.Add(EnquadraRegraMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._EnquadraRegraCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraRegraCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("EnquadraRegraCollectionByIdCarteira"); 
					this._EnquadraRegraCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private EnquadraRegraCollection _EnquadraRegraCollectionByIdCarteira;
		#endregion

				
		#region EnquadraResultadoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_EnquadraResultado_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraResultadoCollection EnquadraResultadoCollectionByIdCarteira
		{
			get
			{
				if(this._EnquadraResultadoCollectionByIdCarteira == null)
				{
					this._EnquadraResultadoCollectionByIdCarteira = new EnquadraResultadoCollection();
					this._EnquadraResultadoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraResultadoCollectionByIdCarteira", this._EnquadraResultadoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._EnquadraResultadoCollectionByIdCarteira.Query.Where(this._EnquadraResultadoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._EnquadraResultadoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraResultadoCollectionByIdCarteira.fks.Add(EnquadraResultadoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._EnquadraResultadoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraResultadoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("EnquadraResultadoCollectionByIdCarteira"); 
					this._EnquadraResultadoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private EnquadraResultadoCollection _EnquadraResultadoCollectionByIdCarteira;
		#endregion

				
		#region EnquadraResultadoDetalheCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_EnquadraResultadoDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraResultadoDetalheCollection EnquadraResultadoDetalheCollectionByIdCarteira
		{
			get
			{
				if(this._EnquadraResultadoDetalheCollectionByIdCarteira == null)
				{
					this._EnquadraResultadoDetalheCollectionByIdCarteira = new EnquadraResultadoDetalheCollection();
					this._EnquadraResultadoDetalheCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraResultadoDetalheCollectionByIdCarteira", this._EnquadraResultadoDetalheCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._EnquadraResultadoDetalheCollectionByIdCarteira.Query.Where(this._EnquadraResultadoDetalheCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._EnquadraResultadoDetalheCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraResultadoDetalheCollectionByIdCarteira.fks.Add(EnquadraResultadoDetalheMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._EnquadraResultadoDetalheCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraResultadoDetalheCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("EnquadraResultadoDetalheCollectionByIdCarteira"); 
					this._EnquadraResultadoDetalheCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private EnquadraResultadoDetalheCollection _EnquadraResultadoDetalheCollectionByIdCarteira;
		#endregion

				
		#region EventoFundoCollectionByIdCarteiraDestino - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EventoFundo_Carteira_FK2
		/// </summary>

		[XmlIgnore]
		public EventoFundoCollection EventoFundoCollectionByIdCarteiraDestino
		{
			get
			{
				if(this._EventoFundoCollectionByIdCarteiraDestino == null)
				{
					this._EventoFundoCollectionByIdCarteiraDestino = new EventoFundoCollection();
					this._EventoFundoCollectionByIdCarteiraDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoFundoCollectionByIdCarteiraDestino", this._EventoFundoCollectionByIdCarteiraDestino);
				
					if(this.IdCarteira != null)
					{
						this._EventoFundoCollectionByIdCarteiraDestino.Query.Where(this._EventoFundoCollectionByIdCarteiraDestino.Query.IdCarteiraDestino == this.IdCarteira);
						this._EventoFundoCollectionByIdCarteiraDestino.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoFundoCollectionByIdCarteiraDestino.fks.Add(EventoFundoMetadata.ColumnNames.IdCarteiraDestino, this.IdCarteira);
					}
				}

				return this._EventoFundoCollectionByIdCarteiraDestino;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoFundoCollectionByIdCarteiraDestino != null) 
				{ 
					this.RemovePostSave("EventoFundoCollectionByIdCarteiraDestino"); 
					this._EventoFundoCollectionByIdCarteiraDestino = null;
					
				} 
			} 			
		}

		private EventoFundoCollection _EventoFundoCollectionByIdCarteiraDestino;
		#endregion

				
		#region EventoFundoCollectionByIdCarteiraOrigem - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EventoFundo_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundoCollection EventoFundoCollectionByIdCarteiraOrigem
		{
			get
			{
				if(this._EventoFundoCollectionByIdCarteiraOrigem == null)
				{
					this._EventoFundoCollectionByIdCarteiraOrigem = new EventoFundoCollection();
					this._EventoFundoCollectionByIdCarteiraOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoFundoCollectionByIdCarteiraOrigem", this._EventoFundoCollectionByIdCarteiraOrigem);
				
					if(this.IdCarteira != null)
					{
						this._EventoFundoCollectionByIdCarteiraOrigem.Query.Where(this._EventoFundoCollectionByIdCarteiraOrigem.Query.IdCarteiraOrigem == this.IdCarteira);
						this._EventoFundoCollectionByIdCarteiraOrigem.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoFundoCollectionByIdCarteiraOrigem.fks.Add(EventoFundoMetadata.ColumnNames.IdCarteiraOrigem, this.IdCarteira);
					}
				}

				return this._EventoFundoCollectionByIdCarteiraOrigem;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoFundoCollectionByIdCarteiraOrigem != null) 
				{ 
					this.RemovePostSave("EventoFundoCollectionByIdCarteiraOrigem"); 
					this._EventoFundoCollectionByIdCarteiraOrigem = null;
					
				} 
			} 			
		}

		private EventoFundoCollection _EventoFundoCollectionByIdCarteiraOrigem;
		#endregion

				
		#region ExcecoesTributacaoIRCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__ExcecoesT__IdCar__1B88F612
		/// </summary>

		[XmlIgnore]
		public ExcecoesTributacaoIRCollection ExcecoesTributacaoIRCollectionByIdCarteira
		{
			get
			{
				if(this._ExcecoesTributacaoIRCollectionByIdCarteira == null)
				{
					this._ExcecoesTributacaoIRCollectionByIdCarteira = new ExcecoesTributacaoIRCollection();
					this._ExcecoesTributacaoIRCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ExcecoesTributacaoIRCollectionByIdCarteira", this._ExcecoesTributacaoIRCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._ExcecoesTributacaoIRCollectionByIdCarteira.Query.Where(this._ExcecoesTributacaoIRCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._ExcecoesTributacaoIRCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._ExcecoesTributacaoIRCollectionByIdCarteira.fks.Add(ExcecoesTributacaoIRMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._ExcecoesTributacaoIRCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ExcecoesTributacaoIRCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("ExcecoesTributacaoIRCollectionByIdCarteira"); 
					this._ExcecoesTributacaoIRCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private ExcecoesTributacaoIRCollection _ExcecoesTributacaoIRCollectionByIdCarteira;
		#endregion

				
		#region FundoInvestimentoFormaCondominioCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__FundoInve__Fundo__0544AF38
		/// </summary>

		[XmlIgnore]
		public FundoInvestimentoFormaCondominioCollection FundoInvestimentoFormaCondominioCollectionByIdCarteira
		{
			get
			{
				if(this._FundoInvestimentoFormaCondominioCollectionByIdCarteira == null)
				{
					this._FundoInvestimentoFormaCondominioCollectionByIdCarteira = new FundoInvestimentoFormaCondominioCollection();
					this._FundoInvestimentoFormaCondominioCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("FundoInvestimentoFormaCondominioCollectionByIdCarteira", this._FundoInvestimentoFormaCondominioCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._FundoInvestimentoFormaCondominioCollectionByIdCarteira.Query.Where(this._FundoInvestimentoFormaCondominioCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._FundoInvestimentoFormaCondominioCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._FundoInvestimentoFormaCondominioCollectionByIdCarteira.fks.Add(FundoInvestimentoFormaCondominioMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._FundoInvestimentoFormaCondominioCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._FundoInvestimentoFormaCondominioCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("FundoInvestimentoFormaCondominioCollectionByIdCarteira"); 
					this._FundoInvestimentoFormaCondominioCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private FundoInvestimentoFormaCondominioCollection _FundoInvestimentoFormaCondominioCollectionByIdCarteira;
		#endregion

				
		#region HistoricoCotaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_HistoricoCota_FK1
		/// </summary>

		[XmlIgnore]
		public HistoricoCotaCollection HistoricoCotaCollectionByIdCarteira
		{
			get
			{
				if(this._HistoricoCotaCollectionByIdCarteira == null)
				{
					this._HistoricoCotaCollectionByIdCarteira = new HistoricoCotaCollection();
					this._HistoricoCotaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("HistoricoCotaCollectionByIdCarteira", this._HistoricoCotaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._HistoricoCotaCollectionByIdCarteira.Query.Where(this._HistoricoCotaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._HistoricoCotaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._HistoricoCotaCollectionByIdCarteira.fks.Add(HistoricoCotaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._HistoricoCotaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._HistoricoCotaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("HistoricoCotaCollectionByIdCarteira"); 
					this._HistoricoCotaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private HistoricoCotaCollection _HistoricoCotaCollectionByIdCarteira;
		#endregion

				
		#region HistoricoCotaGalgoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_HistoricoCotaGalgo_FK1
		/// </summary>

		[XmlIgnore]
		public HistoricoCotaGalgoCollection HistoricoCotaGalgoCollectionByIdCarteira
		{
			get
			{
				if(this._HistoricoCotaGalgoCollectionByIdCarteira == null)
				{
					this._HistoricoCotaGalgoCollectionByIdCarteira = new HistoricoCotaGalgoCollection();
					this._HistoricoCotaGalgoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("HistoricoCotaGalgoCollectionByIdCarteira", this._HistoricoCotaGalgoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._HistoricoCotaGalgoCollectionByIdCarteira.Query.Where(this._HistoricoCotaGalgoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._HistoricoCotaGalgoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._HistoricoCotaGalgoCollectionByIdCarteira.fks.Add(HistoricoCotaGalgoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._HistoricoCotaGalgoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._HistoricoCotaGalgoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("HistoricoCotaGalgoCollectionByIdCarteira"); 
					this._HistoricoCotaGalgoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private HistoricoCotaGalgoCollection _HistoricoCotaGalgoCollectionByIdCarteira;
		#endregion

				
		#region InformacoesComplementaresFundoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_InformacoesComplementaresFundo_Carteira
		/// </summary>

		[XmlIgnore]
		public InformacoesComplementaresFundoCollection InformacoesComplementaresFundoCollectionByIdCarteira
		{
			get
			{
				if(this._InformacoesComplementaresFundoCollectionByIdCarteira == null)
				{
					this._InformacoesComplementaresFundoCollectionByIdCarteira = new InformacoesComplementaresFundoCollection();
					this._InformacoesComplementaresFundoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("InformacoesComplementaresFundoCollectionByIdCarteira", this._InformacoesComplementaresFundoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._InformacoesComplementaresFundoCollectionByIdCarteira.Query.Where(this._InformacoesComplementaresFundoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._InformacoesComplementaresFundoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._InformacoesComplementaresFundoCollectionByIdCarteira.fks.Add(InformacoesComplementaresFundoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._InformacoesComplementaresFundoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._InformacoesComplementaresFundoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("InformacoesComplementaresFundoCollectionByIdCarteira"); 
					this._InformacoesComplementaresFundoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private InformacoesComplementaresFundoCollection _InformacoesComplementaresFundoCollectionByIdCarteira;
		#endregion

				
		#region IRProventosCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - IRProventos_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public IRProventosCollection IRProventosCollectionByIdCarteira
		{
			get
			{
				if(this._IRProventosCollectionByIdCarteira == null)
				{
					this._IRProventosCollectionByIdCarteira = new IRProventosCollection();
					this._IRProventosCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("IRProventosCollectionByIdCarteira", this._IRProventosCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._IRProventosCollectionByIdCarteira.Query.Where(this._IRProventosCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._IRProventosCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._IRProventosCollectionByIdCarteira.fks.Add(IRProventosMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._IRProventosCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._IRProventosCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("IRProventosCollectionByIdCarteira"); 
					this._IRProventosCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private IRProventosCollection _IRProventosCollectionByIdCarteira;
		#endregion

				
		#region LaminaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_Lamina_Carteira
		/// </summary>

		[XmlIgnore]
		public LaminaCollection LaminaCollectionByIdCarteira
		{
			get
			{
				if(this._LaminaCollectionByIdCarteira == null)
				{
					this._LaminaCollectionByIdCarteira = new LaminaCollection();
					this._LaminaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LaminaCollectionByIdCarteira", this._LaminaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._LaminaCollectionByIdCarteira.Query.Where(this._LaminaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._LaminaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._LaminaCollectionByIdCarteira.fks.Add(LaminaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._LaminaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LaminaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("LaminaCollectionByIdCarteira"); 
					this._LaminaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private LaminaCollection _LaminaCollectionByIdCarteira;
		#endregion

		#region UpToIndiceCollection - Many To Many
		/// <summary>
		/// Many to Many
		/// Foreign Key Name - Carteira_ListaBenchmark_FK1
		/// </summary>

		[XmlIgnore]
		public IndiceCollection UpToIndiceCollection
		{
			get
			{
				if(this._UpToIndiceCollection == null)
				{
					this._UpToIndiceCollection = new IndiceCollection();
					this._UpToIndiceCollection.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("UpToIndiceCollection", this._UpToIndiceCollection);
					this._UpToIndiceCollection.ManyToManyCarteiraCollection(this.IdCarteira);
				}

				return this._UpToIndiceCollection;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._UpToIndiceCollection != null) 
				{ 
					this.RemovePostSave("UpToIndiceCollection"); 
					this._UpToIndiceCollection = null;
					
				} 
			}  			
		}

		/// <summary>
		/// Many to Many Associate
		/// Foreign Key Name - Carteira_ListaBenchmark_FK1
		/// </summary>
		public void AssociateIndiceCollection(Indice entity)
		{
			if (this._ListaBenchmarkCollection == null)
			{
				this._ListaBenchmarkCollection = new ListaBenchmarkCollection();
				this._ListaBenchmarkCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("ListaBenchmarkCollection", this._ListaBenchmarkCollection);
			}

			ListaBenchmark obj = this._ListaBenchmarkCollection.AddNew();
			obj.IdCarteira = this.IdCarteira;
			obj.IdIndice = entity.IdIndice;
		}

		/// <summary>
		/// Many to Many Dissociate
		/// Foreign Key Name - Carteira_ListaBenchmark_FK1
		/// </summary>
		public void DissociateIndiceCollection(Indice entity)
		{
			if (this._ListaBenchmarkCollection == null)
			{
				this._ListaBenchmarkCollection = new ListaBenchmarkCollection();
				this._ListaBenchmarkCollection.es.Connection.Name = this.es.Connection.Name;
				this.SetPostSave("ListaBenchmarkCollection", this._ListaBenchmarkCollection);
			}

			ListaBenchmark obj = this._ListaBenchmarkCollection.AddNew();
			obj.IdCarteira = this.IdCarteira;
			obj.IdIndice = entity.IdIndice;
			obj.AcceptChanges();
			obj.MarkAsDeleted();
		}

		private IndiceCollection _UpToIndiceCollection;
		private ListaBenchmarkCollection _ListaBenchmarkCollection;
		#endregion

				
		#region ListaBenchmarkCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_ListaBenchmark_FK1
		/// </summary>

		[XmlIgnore]
		public ListaBenchmarkCollection ListaBenchmarkCollectionByIdCarteira
		{
			get
			{
				if(this._ListaBenchmarkCollectionByIdCarteira == null)
				{
					this._ListaBenchmarkCollectionByIdCarteira = new ListaBenchmarkCollection();
					this._ListaBenchmarkCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ListaBenchmarkCollectionByIdCarteira", this._ListaBenchmarkCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._ListaBenchmarkCollectionByIdCarteira.Query.Where(this._ListaBenchmarkCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._ListaBenchmarkCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._ListaBenchmarkCollectionByIdCarteira.fks.Add(ListaBenchmarkMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._ListaBenchmarkCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ListaBenchmarkCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("ListaBenchmarkCollectionByIdCarteira"); 
					this._ListaBenchmarkCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private ListaBenchmarkCollection _ListaBenchmarkCollectionByIdCarteira;
		#endregion

				
		#region OfertaDistribuicaoCotasCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_OfertaDistribuicaoCotas_FK1
		/// </summary>

		[XmlIgnore]
		public OfertaDistribuicaoCotasCollection OfertaDistribuicaoCotasCollectionByIdCarteira
		{
			get
			{
				if(this._OfertaDistribuicaoCotasCollectionByIdCarteira == null)
				{
					this._OfertaDistribuicaoCotasCollectionByIdCarteira = new OfertaDistribuicaoCotasCollection();
					this._OfertaDistribuicaoCotasCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OfertaDistribuicaoCotasCollectionByIdCarteira", this._OfertaDistribuicaoCotasCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._OfertaDistribuicaoCotasCollectionByIdCarteira.Query.Where(this._OfertaDistribuicaoCotasCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._OfertaDistribuicaoCotasCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._OfertaDistribuicaoCotasCollectionByIdCarteira.fks.Add(OfertaDistribuicaoCotasMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._OfertaDistribuicaoCotasCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OfertaDistribuicaoCotasCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("OfertaDistribuicaoCotasCollectionByIdCarteira"); 
					this._OfertaDistribuicaoCotasCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private OfertaDistribuicaoCotasCollection _OfertaDistribuicaoCotasCollectionByIdCarteira;
		#endregion

				
		#region OperacaoCotistaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_OperacaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoCotistaCollection OperacaoCotistaCollectionByIdCarteira
		{
			get
			{
				if(this._OperacaoCotistaCollectionByIdCarteira == null)
				{
					this._OperacaoCotistaCollectionByIdCarteira = new OperacaoCotistaCollection();
					this._OperacaoCotistaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoCotistaCollectionByIdCarteira", this._OperacaoCotistaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._OperacaoCotistaCollectionByIdCarteira.Query.Where(this._OperacaoCotistaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._OperacaoCotistaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoCotistaCollectionByIdCarteira.fks.Add(OperacaoCotistaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._OperacaoCotistaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoCotistaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("OperacaoCotistaCollectionByIdCarteira"); 
					this._OperacaoCotistaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private OperacaoCotistaCollection _OperacaoCotistaCollectionByIdCarteira;
		#endregion

				
		#region OperacaoFundoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_OperacaoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoFundoCollection OperacaoFundoCollectionByIdCarteira
		{
			get
			{
				if(this._OperacaoFundoCollectionByIdCarteira == null)
				{
					this._OperacaoFundoCollectionByIdCarteira = new OperacaoFundoCollection();
					this._OperacaoFundoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoFundoCollectionByIdCarteira", this._OperacaoFundoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._OperacaoFundoCollectionByIdCarteira.Query.Where(this._OperacaoFundoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._OperacaoFundoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoFundoCollectionByIdCarteira.fks.Add(OperacaoFundoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._OperacaoFundoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoFundoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("OperacaoFundoCollectionByIdCarteira"); 
					this._OperacaoFundoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private OperacaoFundoCollection _OperacaoFundoCollectionByIdCarteira;
		#endregion

				
		#region OperacaoRendaFixaCollectionByIdCarteiraContraparte - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__OperacaoR__IdCar__41C3AD93
		/// </summary>

		[XmlIgnore]
		public OperacaoRendaFixaCollection OperacaoRendaFixaCollectionByIdCarteiraContraparte
		{
			get
			{
				if(this._OperacaoRendaFixaCollectionByIdCarteiraContraparte == null)
				{
					this._OperacaoRendaFixaCollectionByIdCarteiraContraparte = new OperacaoRendaFixaCollection();
					this._OperacaoRendaFixaCollectionByIdCarteiraContraparte.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoRendaFixaCollectionByIdCarteiraContraparte", this._OperacaoRendaFixaCollectionByIdCarteiraContraparte);
				
					if(this.IdCarteira != null)
					{
						this._OperacaoRendaFixaCollectionByIdCarteiraContraparte.Query.Where(this._OperacaoRendaFixaCollectionByIdCarteiraContraparte.Query.IdCarteiraContraparte == this.IdCarteira);
						this._OperacaoRendaFixaCollectionByIdCarteiraContraparte.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoRendaFixaCollectionByIdCarteiraContraparte.fks.Add(OperacaoRendaFixaMetadata.ColumnNames.IdCarteiraContraparte, this.IdCarteira);
					}
				}

				return this._OperacaoRendaFixaCollectionByIdCarteiraContraparte;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoRendaFixaCollectionByIdCarteiraContraparte != null) 
				{ 
					this.RemovePostSave("OperacaoRendaFixaCollectionByIdCarteiraContraparte"); 
					this._OperacaoRendaFixaCollectionByIdCarteiraContraparte = null;
					
				} 
			} 			
		}

		private OperacaoRendaFixaCollection _OperacaoRendaFixaCollectionByIdCarteiraContraparte;
		#endregion

				
		#region OperacaoSwapCollectionByIdAtivoCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - OperacaoSwap_Carteira_FK
		/// </summary>

		[XmlIgnore]
		public OperacaoSwapCollection OperacaoSwapCollectionByIdAtivoCarteira
		{
			get
			{
				if(this._OperacaoSwapCollectionByIdAtivoCarteira == null)
				{
					this._OperacaoSwapCollectionByIdAtivoCarteira = new OperacaoSwapCollection();
					this._OperacaoSwapCollectionByIdAtivoCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoSwapCollectionByIdAtivoCarteira", this._OperacaoSwapCollectionByIdAtivoCarteira);
				
					if(this.IdCarteira != null)
					{
						this._OperacaoSwapCollectionByIdAtivoCarteira.Query.Where(this._OperacaoSwapCollectionByIdAtivoCarteira.Query.IdAtivoCarteira == this.IdCarteira);
						this._OperacaoSwapCollectionByIdAtivoCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoSwapCollectionByIdAtivoCarteira.fks.Add(OperacaoSwapMetadata.ColumnNames.IdAtivoCarteira, this.IdCarteira);
					}
				}

				return this._OperacaoSwapCollectionByIdAtivoCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoSwapCollectionByIdAtivoCarteira != null) 
				{ 
					this.RemovePostSave("OperacaoSwapCollectionByIdAtivoCarteira"); 
					this._OperacaoSwapCollectionByIdAtivoCarteira = null;
					
				} 
			} 			
		}

		private OperacaoSwapCollection _OperacaoSwapCollectionByIdAtivoCarteira;
		#endregion

				
		#region OperacaoSwapCollectionByIdAtivoCarteiraContraParte - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - OperacaoSwap_CarteiraCP_FK
		/// </summary>

		[XmlIgnore]
		public OperacaoSwapCollection OperacaoSwapCollectionByIdAtivoCarteiraContraParte
		{
			get
			{
				if(this._OperacaoSwapCollectionByIdAtivoCarteiraContraParte == null)
				{
					this._OperacaoSwapCollectionByIdAtivoCarteiraContraParte = new OperacaoSwapCollection();
					this._OperacaoSwapCollectionByIdAtivoCarteiraContraParte.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoSwapCollectionByIdAtivoCarteiraContraParte", this._OperacaoSwapCollectionByIdAtivoCarteiraContraParte);
				
					if(this.IdCarteira != null)
					{
						this._OperacaoSwapCollectionByIdAtivoCarteiraContraParte.Query.Where(this._OperacaoSwapCollectionByIdAtivoCarteiraContraParte.Query.IdAtivoCarteiraContraParte == this.IdCarteira);
						this._OperacaoSwapCollectionByIdAtivoCarteiraContraParte.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoSwapCollectionByIdAtivoCarteiraContraParte.fks.Add(OperacaoSwapMetadata.ColumnNames.IdAtivoCarteiraContraParte, this.IdCarteira);
					}
				}

				return this._OperacaoSwapCollectionByIdAtivoCarteiraContraParte;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoSwapCollectionByIdAtivoCarteiraContraParte != null) 
				{ 
					this.RemovePostSave("OperacaoSwapCollectionByIdAtivoCarteiraContraParte"); 
					this._OperacaoSwapCollectionByIdAtivoCarteiraContraParte = null;
					
				} 
			} 			
		}

		private OperacaoSwapCollection _OperacaoSwapCollectionByIdAtivoCarteiraContraParte;
		#endregion

				
		#region OrdemCotistaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_OrdemCotista_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemCotistaCollection OrdemCotistaCollectionByIdCarteira
		{
			get
			{
				if(this._OrdemCotistaCollectionByIdCarteira == null)
				{
					this._OrdemCotistaCollectionByIdCarteira = new OrdemCotistaCollection();
					this._OrdemCotistaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemCotistaCollectionByIdCarteira", this._OrdemCotistaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._OrdemCotistaCollectionByIdCarteira.Query.Where(this._OrdemCotistaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._OrdemCotistaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemCotistaCollectionByIdCarteira.fks.Add(OrdemCotistaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._OrdemCotistaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemCotistaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("OrdemCotistaCollectionByIdCarteira"); 
					this._OrdemCotistaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private OrdemCotistaCollection _OrdemCotistaCollectionByIdCarteira;
		#endregion

				
		#region OrdemFundoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_OrdemFundo_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemFundoCollection OrdemFundoCollectionByIdCarteira
		{
			get
			{
				if(this._OrdemFundoCollectionByIdCarteira == null)
				{
					this._OrdemFundoCollectionByIdCarteira = new OrdemFundoCollection();
					this._OrdemFundoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemFundoCollectionByIdCarteira", this._OrdemFundoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._OrdemFundoCollectionByIdCarteira.Query.Where(this._OrdemFundoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._OrdemFundoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemFundoCollectionByIdCarteira.fks.Add(OrdemFundoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._OrdemFundoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemFundoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("OrdemFundoCollectionByIdCarteira"); 
					this._OrdemFundoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private OrdemFundoCollection _OrdemFundoCollectionByIdCarteira;
		#endregion

				
		#region OrdemRendaFixaCollectionByIdCarteiraContraParte - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__OrdemRend__IdCar__61674175
		/// </summary>

		[XmlIgnore]
		public OrdemRendaFixaCollection OrdemRendaFixaCollectionByIdCarteiraContraParte
		{
			get
			{
				if(this._OrdemRendaFixaCollectionByIdCarteiraContraParte == null)
				{
					this._OrdemRendaFixaCollectionByIdCarteiraContraParte = new OrdemRendaFixaCollection();
					this._OrdemRendaFixaCollectionByIdCarteiraContraParte.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemRendaFixaCollectionByIdCarteiraContraParte", this._OrdemRendaFixaCollectionByIdCarteiraContraParte);
				
					if(this.IdCarteira != null)
					{
						this._OrdemRendaFixaCollectionByIdCarteiraContraParte.Query.Where(this._OrdemRendaFixaCollectionByIdCarteiraContraParte.Query.IdCarteiraContraParte == this.IdCarteira);
						this._OrdemRendaFixaCollectionByIdCarteiraContraParte.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemRendaFixaCollectionByIdCarteiraContraParte.fks.Add(OrdemRendaFixaMetadata.ColumnNames.IdCarteiraContraParte, this.IdCarteira);
					}
				}

				return this._OrdemRendaFixaCollectionByIdCarteiraContraParte;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemRendaFixaCollectionByIdCarteiraContraParte != null) 
				{ 
					this.RemovePostSave("OrdemRendaFixaCollectionByIdCarteiraContraParte"); 
					this._OrdemRendaFixaCollectionByIdCarteiraContraParte = null;
					
				} 
			} 			
		}

		private OrdemRendaFixaCollection _OrdemRendaFixaCollectionByIdCarteiraContraParte;
		#endregion

				
		#region PatrimonioFundoEmissaoSerieCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PatrimonioFundoEmissaoSerie_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public PatrimonioFundoEmissaoSerieCollection PatrimonioFundoEmissaoSerieCollectionByIdCarteira
		{
			get
			{
				if(this._PatrimonioFundoEmissaoSerieCollectionByIdCarteira == null)
				{
					this._PatrimonioFundoEmissaoSerieCollectionByIdCarteira = new PatrimonioFundoEmissaoSerieCollection();
					this._PatrimonioFundoEmissaoSerieCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PatrimonioFundoEmissaoSerieCollectionByIdCarteira", this._PatrimonioFundoEmissaoSerieCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PatrimonioFundoEmissaoSerieCollectionByIdCarteira.Query.Where(this._PatrimonioFundoEmissaoSerieCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._PatrimonioFundoEmissaoSerieCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PatrimonioFundoEmissaoSerieCollectionByIdCarteira.fks.Add(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._PatrimonioFundoEmissaoSerieCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PatrimonioFundoEmissaoSerieCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("PatrimonioFundoEmissaoSerieCollectionByIdCarteira"); 
					this._PatrimonioFundoEmissaoSerieCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private PatrimonioFundoEmissaoSerieCollection _PatrimonioFundoEmissaoSerieCollectionByIdCarteira;
		#endregion

				
		#region PerfilClassificacaoFundoCarteiraCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PerfilClassificacaoFundoCarteira_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilClassificacaoFundoCarteiraCollection PerfilClassificacaoFundoCarteiraCollectionByIdCarteira
		{
			get
			{
				if(this._PerfilClassificacaoFundoCarteiraCollectionByIdCarteira == null)
				{
					this._PerfilClassificacaoFundoCarteiraCollectionByIdCarteira = new PerfilClassificacaoFundoCarteiraCollection();
					this._PerfilClassificacaoFundoCarteiraCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PerfilClassificacaoFundoCarteiraCollectionByIdCarteira", this._PerfilClassificacaoFundoCarteiraCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PerfilClassificacaoFundoCarteiraCollectionByIdCarteira.Query.Where(this._PerfilClassificacaoFundoCarteiraCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._PerfilClassificacaoFundoCarteiraCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PerfilClassificacaoFundoCarteiraCollectionByIdCarteira.fks.Add(PerfilClassificacaoFundoCarteiraMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._PerfilClassificacaoFundoCarteiraCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PerfilClassificacaoFundoCarteiraCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("PerfilClassificacaoFundoCarteiraCollectionByIdCarteira"); 
					this._PerfilClassificacaoFundoCarteiraCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private PerfilClassificacaoFundoCarteiraCollection _PerfilClassificacaoFundoCarteiraCollectionByIdCarteira;
		#endregion

				
		#region PerfilClienteCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__PerfilCli__IdCar__157B1701
		/// </summary>

		[XmlIgnore]
		public PerfilClienteCollection PerfilClienteCollectionByIdCarteira
		{
			get
			{
				if(this._PerfilClienteCollectionByIdCarteira == null)
				{
					this._PerfilClienteCollectionByIdCarteira = new PerfilClienteCollection();
					this._PerfilClienteCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PerfilClienteCollectionByIdCarteira", this._PerfilClienteCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PerfilClienteCollectionByIdCarteira.Query.Where(this._PerfilClienteCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._PerfilClienteCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PerfilClienteCollectionByIdCarteira.fks.Add(PerfilClienteMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._PerfilClienteCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PerfilClienteCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("PerfilClienteCollectionByIdCarteira"); 
					this._PerfilClienteCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private PerfilClienteCollection _PerfilClienteCollectionByIdCarteira;
		#endregion

				
		#region PortfolioPadraoCollectionByIdCarteiraDespesasReceitas - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_CarteiraDespesasReceitas_Carteira
		/// </summary>

		[XmlIgnore]
		public PortfolioPadraoCollection PortfolioPadraoCollectionByIdCarteiraDespesasReceitas
		{
			get
			{
				if(this._PortfolioPadraoCollectionByIdCarteiraDespesasReceitas == null)
				{
					this._PortfolioPadraoCollectionByIdCarteiraDespesasReceitas = new PortfolioPadraoCollection();
					this._PortfolioPadraoCollectionByIdCarteiraDespesasReceitas.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PortfolioPadraoCollectionByIdCarteiraDespesasReceitas", this._PortfolioPadraoCollectionByIdCarteiraDespesasReceitas);
				
					if(this.IdCarteira != null)
					{
						this._PortfolioPadraoCollectionByIdCarteiraDespesasReceitas.Query.Where(this._PortfolioPadraoCollectionByIdCarteiraDespesasReceitas.Query.IdCarteiraDespesasReceitas == this.IdCarteira);
						this._PortfolioPadraoCollectionByIdCarteiraDespesasReceitas.Query.Load();

						// Auto-hookup Foreign Keys
						this._PortfolioPadraoCollectionByIdCarteiraDespesasReceitas.fks.Add(PortfolioPadraoMetadata.ColumnNames.IdCarteiraDespesasReceitas, this.IdCarteira);
					}
				}

				return this._PortfolioPadraoCollectionByIdCarteiraDespesasReceitas;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PortfolioPadraoCollectionByIdCarteiraDespesasReceitas != null) 
				{ 
					this.RemovePostSave("PortfolioPadraoCollectionByIdCarteiraDespesasReceitas"); 
					this._PortfolioPadraoCollectionByIdCarteiraDespesasReceitas = null;
					
				} 
			} 			
		}

		private PortfolioPadraoCollection _PortfolioPadraoCollectionByIdCarteiraDespesasReceitas;
		#endregion

				
		#region PortfolioPadraoCollectionByIdCarteiraTaxaGestao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_CarteiraTaxaGestao_Carteira
		/// </summary>

		[XmlIgnore]
		public PortfolioPadraoCollection PortfolioPadraoCollectionByIdCarteiraTaxaGestao
		{
			get
			{
				if(this._PortfolioPadraoCollectionByIdCarteiraTaxaGestao == null)
				{
					this._PortfolioPadraoCollectionByIdCarteiraTaxaGestao = new PortfolioPadraoCollection();
					this._PortfolioPadraoCollectionByIdCarteiraTaxaGestao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PortfolioPadraoCollectionByIdCarteiraTaxaGestao", this._PortfolioPadraoCollectionByIdCarteiraTaxaGestao);
				
					if(this.IdCarteira != null)
					{
						this._PortfolioPadraoCollectionByIdCarteiraTaxaGestao.Query.Where(this._PortfolioPadraoCollectionByIdCarteiraTaxaGestao.Query.IdCarteiraTaxaGestao == this.IdCarteira);
						this._PortfolioPadraoCollectionByIdCarteiraTaxaGestao.Query.Load();

						// Auto-hookup Foreign Keys
						this._PortfolioPadraoCollectionByIdCarteiraTaxaGestao.fks.Add(PortfolioPadraoMetadata.ColumnNames.IdCarteiraTaxaGestao, this.IdCarteira);
					}
				}

				return this._PortfolioPadraoCollectionByIdCarteiraTaxaGestao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PortfolioPadraoCollectionByIdCarteiraTaxaGestao != null) 
				{ 
					this.RemovePostSave("PortfolioPadraoCollectionByIdCarteiraTaxaGestao"); 
					this._PortfolioPadraoCollectionByIdCarteiraTaxaGestao = null;
					
				} 
			} 			
		}

		private PortfolioPadraoCollection _PortfolioPadraoCollectionByIdCarteiraTaxaGestao;
		#endregion

				
		#region PortfolioPadraoCollectionByIdCarteiraTaxaPerformance - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_CarteiraTaxaPerformance_Carteira
		/// </summary>

		[XmlIgnore]
		public PortfolioPadraoCollection PortfolioPadraoCollectionByIdCarteiraTaxaPerformance
		{
			get
			{
				if(this._PortfolioPadraoCollectionByIdCarteiraTaxaPerformance == null)
				{
					this._PortfolioPadraoCollectionByIdCarteiraTaxaPerformance = new PortfolioPadraoCollection();
					this._PortfolioPadraoCollectionByIdCarteiraTaxaPerformance.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PortfolioPadraoCollectionByIdCarteiraTaxaPerformance", this._PortfolioPadraoCollectionByIdCarteiraTaxaPerformance);
				
					if(this.IdCarteira != null)
					{
						this._PortfolioPadraoCollectionByIdCarteiraTaxaPerformance.Query.Where(this._PortfolioPadraoCollectionByIdCarteiraTaxaPerformance.Query.IdCarteiraTaxaPerformance == this.IdCarteira);
						this._PortfolioPadraoCollectionByIdCarteiraTaxaPerformance.Query.Load();

						// Auto-hookup Foreign Keys
						this._PortfolioPadraoCollectionByIdCarteiraTaxaPerformance.fks.Add(PortfolioPadraoMetadata.ColumnNames.IdCarteiraTaxaPerformance, this.IdCarteira);
					}
				}

				return this._PortfolioPadraoCollectionByIdCarteiraTaxaPerformance;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PortfolioPadraoCollectionByIdCarteiraTaxaPerformance != null) 
				{ 
					this.RemovePostSave("PortfolioPadraoCollectionByIdCarteiraTaxaPerformance"); 
					this._PortfolioPadraoCollectionByIdCarteiraTaxaPerformance = null;
					
				} 
			} 			
		}

		private PortfolioPadraoCollection _PortfolioPadraoCollectionByIdCarteiraTaxaPerformance;
		#endregion

				
		#region PosicaoCotistaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_PosicaoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoCotistaCollection PosicaoCotistaCollectionByIdCarteira
		{
			get
			{
				if(this._PosicaoCotistaCollectionByIdCarteira == null)
				{
					this._PosicaoCotistaCollectionByIdCarteira = new PosicaoCotistaCollection();
					this._PosicaoCotistaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoCotistaCollectionByIdCarteira", this._PosicaoCotistaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PosicaoCotistaCollectionByIdCarteira.Query.Where(this._PosicaoCotistaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._PosicaoCotistaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoCotistaCollectionByIdCarteira.fks.Add(PosicaoCotistaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._PosicaoCotistaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoCotistaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("PosicaoCotistaCollectionByIdCarteira"); 
					this._PosicaoCotistaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private PosicaoCotistaCollection _PosicaoCotistaCollectionByIdCarteira;
		#endregion

				
		#region PosicaoCotistaAberturaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_PosicaoCotistaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoCotistaAberturaCollection PosicaoCotistaAberturaCollectionByIdCarteira
		{
			get
			{
				if(this._PosicaoCotistaAberturaCollectionByIdCarteira == null)
				{
					this._PosicaoCotistaAberturaCollectionByIdCarteira = new PosicaoCotistaAberturaCollection();
					this._PosicaoCotistaAberturaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoCotistaAberturaCollectionByIdCarteira", this._PosicaoCotistaAberturaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PosicaoCotistaAberturaCollectionByIdCarteira.Query.Where(this._PosicaoCotistaAberturaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._PosicaoCotistaAberturaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoCotistaAberturaCollectionByIdCarteira.fks.Add(PosicaoCotistaAberturaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._PosicaoCotistaAberturaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoCotistaAberturaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("PosicaoCotistaAberturaCollectionByIdCarteira"); 
					this._PosicaoCotistaAberturaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private PosicaoCotistaAberturaCollection _PosicaoCotistaAberturaCollectionByIdCarteira;
		#endregion

				
		#region PosicaoCotistaAlteradaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_PosicaoCotistaAlterada_Carteira
		/// </summary>

		[XmlIgnore]
		public PosicaoCotistaAlteradaCollection PosicaoCotistaAlteradaCollectionByIdCarteira
		{
			get
			{
				if(this._PosicaoCotistaAlteradaCollectionByIdCarteira == null)
				{
					this._PosicaoCotistaAlteradaCollectionByIdCarteira = new PosicaoCotistaAlteradaCollection();
					this._PosicaoCotistaAlteradaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoCotistaAlteradaCollectionByIdCarteira", this._PosicaoCotistaAlteradaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PosicaoCotistaAlteradaCollectionByIdCarteira.Query.Where(this._PosicaoCotistaAlteradaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._PosicaoCotistaAlteradaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoCotistaAlteradaCollectionByIdCarteira.fks.Add(PosicaoCotistaAlteradaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._PosicaoCotistaAlteradaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoCotistaAlteradaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("PosicaoCotistaAlteradaCollectionByIdCarteira"); 
					this._PosicaoCotistaAlteradaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private PosicaoCotistaAlteradaCollection _PosicaoCotistaAlteradaCollectionByIdCarteira;
		#endregion

				
		#region PosicaoCotistaHistoricoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_PosicaoCotistaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoCotistaHistoricoCollection PosicaoCotistaHistoricoCollectionByIdCarteira
		{
			get
			{
				if(this._PosicaoCotistaHistoricoCollectionByIdCarteira == null)
				{
					this._PosicaoCotistaHistoricoCollectionByIdCarteira = new PosicaoCotistaHistoricoCollection();
					this._PosicaoCotistaHistoricoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoCotistaHistoricoCollectionByIdCarteira", this._PosicaoCotistaHistoricoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PosicaoCotistaHistoricoCollectionByIdCarteira.Query.Where(this._PosicaoCotistaHistoricoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._PosicaoCotistaHistoricoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoCotistaHistoricoCollectionByIdCarteira.fks.Add(PosicaoCotistaHistoricoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._PosicaoCotistaHistoricoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoCotistaHistoricoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("PosicaoCotistaHistoricoCollectionByIdCarteira"); 
					this._PosicaoCotistaHistoricoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private PosicaoCotistaHistoricoCollection _PosicaoCotistaHistoricoCollectionByIdCarteira;
		#endregion

				
		#region PosicaoFundoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_PosicaoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoFundoCollection PosicaoFundoCollectionByIdCarteira
		{
			get
			{
				if(this._PosicaoFundoCollectionByIdCarteira == null)
				{
					this._PosicaoFundoCollectionByIdCarteira = new PosicaoFundoCollection();
					this._PosicaoFundoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoFundoCollectionByIdCarteira", this._PosicaoFundoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PosicaoFundoCollectionByIdCarteira.Query.Where(this._PosicaoFundoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._PosicaoFundoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoFundoCollectionByIdCarteira.fks.Add(PosicaoFundoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._PosicaoFundoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoFundoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("PosicaoFundoCollectionByIdCarteira"); 
					this._PosicaoFundoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private PosicaoFundoCollection _PosicaoFundoCollectionByIdCarteira;
		#endregion

				
		#region PosicaoFundoAberturaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_PosicaoFundoAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoFundoAberturaCollection PosicaoFundoAberturaCollectionByIdCarteira
		{
			get
			{
				if(this._PosicaoFundoAberturaCollectionByIdCarteira == null)
				{
					this._PosicaoFundoAberturaCollectionByIdCarteira = new PosicaoFundoAberturaCollection();
					this._PosicaoFundoAberturaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoFundoAberturaCollectionByIdCarteira", this._PosicaoFundoAberturaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PosicaoFundoAberturaCollectionByIdCarteira.Query.Where(this._PosicaoFundoAberturaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._PosicaoFundoAberturaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoFundoAberturaCollectionByIdCarteira.fks.Add(PosicaoFundoAberturaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._PosicaoFundoAberturaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoFundoAberturaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("PosicaoFundoAberturaCollectionByIdCarteira"); 
					this._PosicaoFundoAberturaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private PosicaoFundoAberturaCollection _PosicaoFundoAberturaCollectionByIdCarteira;
		#endregion

				
		#region PosicaoFundoHistoricoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_PosicaoFundoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoFundoHistoricoCollection PosicaoFundoHistoricoCollectionByIdCarteira
		{
			get
			{
				if(this._PosicaoFundoHistoricoCollectionByIdCarteira == null)
				{
					this._PosicaoFundoHistoricoCollectionByIdCarteira = new PosicaoFundoHistoricoCollection();
					this._PosicaoFundoHistoricoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoFundoHistoricoCollectionByIdCarteira", this._PosicaoFundoHistoricoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PosicaoFundoHistoricoCollectionByIdCarteira.Query.Where(this._PosicaoFundoHistoricoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._PosicaoFundoHistoricoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoFundoHistoricoCollectionByIdCarteira.fks.Add(PosicaoFundoHistoricoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._PosicaoFundoHistoricoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoFundoHistoricoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("PosicaoFundoHistoricoCollectionByIdCarteira"); 
					this._PosicaoFundoHistoricoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private PosicaoFundoHistoricoCollection _PosicaoFundoHistoricoCollectionByIdCarteira;
		#endregion

				
		#region PosicaoSwapCollectionByIdAtivoCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PosicaoSwap_Carteira_FK
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapCollection PosicaoSwapCollectionByIdAtivoCarteira
		{
			get
			{
				if(this._PosicaoSwapCollectionByIdAtivoCarteira == null)
				{
					this._PosicaoSwapCollectionByIdAtivoCarteira = new PosicaoSwapCollection();
					this._PosicaoSwapCollectionByIdAtivoCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapCollectionByIdAtivoCarteira", this._PosicaoSwapCollectionByIdAtivoCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PosicaoSwapCollectionByIdAtivoCarteira.Query.Where(this._PosicaoSwapCollectionByIdAtivoCarteira.Query.IdAtivoCarteira == this.IdCarteira);
						this._PosicaoSwapCollectionByIdAtivoCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapCollectionByIdAtivoCarteira.fks.Add(PosicaoSwapMetadata.ColumnNames.IdAtivoCarteira, this.IdCarteira);
					}
				}

				return this._PosicaoSwapCollectionByIdAtivoCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapCollectionByIdAtivoCarteira != null) 
				{ 
					this.RemovePostSave("PosicaoSwapCollectionByIdAtivoCarteira"); 
					this._PosicaoSwapCollectionByIdAtivoCarteira = null;
					
				} 
			} 			
		}

		private PosicaoSwapCollection _PosicaoSwapCollectionByIdAtivoCarteira;
		#endregion

				
		#region PosicaoSwapCollectionByIdAtivoCarteiraContraParte - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PosicaoSwap_CarteiraCP_FK
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapCollection PosicaoSwapCollectionByIdAtivoCarteiraContraParte
		{
			get
			{
				if(this._PosicaoSwapCollectionByIdAtivoCarteiraContraParte == null)
				{
					this._PosicaoSwapCollectionByIdAtivoCarteiraContraParte = new PosicaoSwapCollection();
					this._PosicaoSwapCollectionByIdAtivoCarteiraContraParte.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapCollectionByIdAtivoCarteiraContraParte", this._PosicaoSwapCollectionByIdAtivoCarteiraContraParte);
				
					if(this.IdCarteira != null)
					{
						this._PosicaoSwapCollectionByIdAtivoCarteiraContraParte.Query.Where(this._PosicaoSwapCollectionByIdAtivoCarteiraContraParte.Query.IdAtivoCarteiraContraParte == this.IdCarteira);
						this._PosicaoSwapCollectionByIdAtivoCarteiraContraParte.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapCollectionByIdAtivoCarteiraContraParte.fks.Add(PosicaoSwapMetadata.ColumnNames.IdAtivoCarteiraContraParte, this.IdCarteira);
					}
				}

				return this._PosicaoSwapCollectionByIdAtivoCarteiraContraParte;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapCollectionByIdAtivoCarteiraContraParte != null) 
				{ 
					this.RemovePostSave("PosicaoSwapCollectionByIdAtivoCarteiraContraParte"); 
					this._PosicaoSwapCollectionByIdAtivoCarteiraContraParte = null;
					
				} 
			} 			
		}

		private PosicaoSwapCollection _PosicaoSwapCollectionByIdAtivoCarteiraContraParte;
		#endregion

				
		#region PosicaoSwapAberturaCollectionByIdAtivoCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PosicaoSwapAber_Carteira_FK
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapAberturaCollection PosicaoSwapAberturaCollectionByIdAtivoCarteira
		{
			get
			{
				if(this._PosicaoSwapAberturaCollectionByIdAtivoCarteira == null)
				{
					this._PosicaoSwapAberturaCollectionByIdAtivoCarteira = new PosicaoSwapAberturaCollection();
					this._PosicaoSwapAberturaCollectionByIdAtivoCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapAberturaCollectionByIdAtivoCarteira", this._PosicaoSwapAberturaCollectionByIdAtivoCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PosicaoSwapAberturaCollectionByIdAtivoCarteira.Query.Where(this._PosicaoSwapAberturaCollectionByIdAtivoCarteira.Query.IdAtivoCarteira == this.IdCarteira);
						this._PosicaoSwapAberturaCollectionByIdAtivoCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapAberturaCollectionByIdAtivoCarteira.fks.Add(PosicaoSwapAberturaMetadata.ColumnNames.IdAtivoCarteira, this.IdCarteira);
					}
				}

				return this._PosicaoSwapAberturaCollectionByIdAtivoCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapAberturaCollectionByIdAtivoCarteira != null) 
				{ 
					this.RemovePostSave("PosicaoSwapAberturaCollectionByIdAtivoCarteira"); 
					this._PosicaoSwapAberturaCollectionByIdAtivoCarteira = null;
					
				} 
			} 			
		}

		private PosicaoSwapAberturaCollection _PosicaoSwapAberturaCollectionByIdAtivoCarteira;
		#endregion

				
		#region PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PosicaoSwapAber_CarteiraCP_FK
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapAberturaCollection PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte
		{
			get
			{
				if(this._PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte == null)
				{
					this._PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte = new PosicaoSwapAberturaCollection();
					this._PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte", this._PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte);
				
					if(this.IdCarteira != null)
					{
						this._PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte.Query.Where(this._PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte.Query.IdAtivoCarteiraContraParte == this.IdCarteira);
						this._PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte.fks.Add(PosicaoSwapAberturaMetadata.ColumnNames.IdAtivoCarteiraContraParte, this.IdCarteira);
					}
				}

				return this._PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte != null) 
				{ 
					this.RemovePostSave("PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte"); 
					this._PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte = null;
					
				} 
			} 			
		}

		private PosicaoSwapAberturaCollection _PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte;
		#endregion

				
		#region PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PosicaoSwapHist_CarteiraCP_FK
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapHistoricoCollection PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte
		{
			get
			{
				if(this._PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte == null)
				{
					this._PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte = new PosicaoSwapHistoricoCollection();
					this._PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte", this._PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte);
				
					if(this.IdCarteira != null)
					{
						this._PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte.Query.Where(this._PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte.Query.IdAtivoCarteiraContraParte == this.IdCarteira);
						this._PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte.fks.Add(PosicaoSwapHistoricoMetadata.ColumnNames.IdAtivoCarteiraContraParte, this.IdCarteira);
					}
				}

				return this._PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte != null) 
				{ 
					this.RemovePostSave("PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte"); 
					this._PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte = null;
					
				} 
			} 			
		}

		private PosicaoSwapHistoricoCollection _PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte;
		#endregion

				
		#region PosicaoSwapHistoricoCollectionByIdAtivoCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PosicaoSwapHist_Carteira_FK
		/// </summary>

		[XmlIgnore]
		public PosicaoSwapHistoricoCollection PosicaoSwapHistoricoCollectionByIdAtivoCarteira
		{
			get
			{
				if(this._PosicaoSwapHistoricoCollectionByIdAtivoCarteira == null)
				{
					this._PosicaoSwapHistoricoCollectionByIdAtivoCarteira = new PosicaoSwapHistoricoCollection();
					this._PosicaoSwapHistoricoCollectionByIdAtivoCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoSwapHistoricoCollectionByIdAtivoCarteira", this._PosicaoSwapHistoricoCollectionByIdAtivoCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PosicaoSwapHistoricoCollectionByIdAtivoCarteira.Query.Where(this._PosicaoSwapHistoricoCollectionByIdAtivoCarteira.Query.IdAtivoCarteira == this.IdCarteira);
						this._PosicaoSwapHistoricoCollectionByIdAtivoCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoSwapHistoricoCollectionByIdAtivoCarteira.fks.Add(PosicaoSwapHistoricoMetadata.ColumnNames.IdAtivoCarteira, this.IdCarteira);
					}
				}

				return this._PosicaoSwapHistoricoCollectionByIdAtivoCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoSwapHistoricoCollectionByIdAtivoCarteira != null) 
				{ 
					this.RemovePostSave("PosicaoSwapHistoricoCollectionByIdAtivoCarteira"); 
					this._PosicaoSwapHistoricoCollectionByIdAtivoCarteira = null;
					
				} 
			} 			
		}

		private PosicaoSwapHistoricoCollection _PosicaoSwapHistoricoCollectionByIdAtivoCarteira;
		#endregion

				
		#region PrejuizoCotistaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_PrejuizoCotista_FK1
		/// </summary>

		[XmlIgnore]
		public PrejuizoCotistaCollection PrejuizoCotistaCollectionByIdCarteira
		{
			get
			{
				if(this._PrejuizoCotistaCollectionByIdCarteira == null)
				{
					this._PrejuizoCotistaCollectionByIdCarteira = new PrejuizoCotistaCollection();
					this._PrejuizoCotistaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PrejuizoCotistaCollectionByIdCarteira", this._PrejuizoCotistaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PrejuizoCotistaCollectionByIdCarteira.Query.Where(this._PrejuizoCotistaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._PrejuizoCotistaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PrejuizoCotistaCollectionByIdCarteira.fks.Add(PrejuizoCotistaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._PrejuizoCotistaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PrejuizoCotistaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("PrejuizoCotistaCollectionByIdCarteira"); 
					this._PrejuizoCotistaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private PrejuizoCotistaCollection _PrejuizoCotistaCollectionByIdCarteira;
		#endregion

				
		#region PrejuizoCotistaHistoricoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_PrejuizoCotistaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PrejuizoCotistaHistoricoCollection PrejuizoCotistaHistoricoCollectionByIdCarteira
		{
			get
			{
				if(this._PrejuizoCotistaHistoricoCollectionByIdCarteira == null)
				{
					this._PrejuizoCotistaHistoricoCollectionByIdCarteira = new PrejuizoCotistaHistoricoCollection();
					this._PrejuizoCotistaHistoricoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PrejuizoCotistaHistoricoCollectionByIdCarteira", this._PrejuizoCotistaHistoricoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PrejuizoCotistaHistoricoCollectionByIdCarteira.Query.Where(this._PrejuizoCotistaHistoricoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._PrejuizoCotistaHistoricoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PrejuizoCotistaHistoricoCollectionByIdCarteira.fks.Add(PrejuizoCotistaHistoricoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._PrejuizoCotistaHistoricoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PrejuizoCotistaHistoricoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("PrejuizoCotistaHistoricoCollectionByIdCarteira"); 
					this._PrejuizoCotistaHistoricoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private PrejuizoCotistaHistoricoCollection _PrejuizoCotistaHistoricoCollectionByIdCarteira;
		#endregion

				
		#region PrejuizoCotistaUsadoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_PrejuizoCotistaUsado_Carteira
		/// </summary>

		[XmlIgnore]
		public PrejuizoCotistaUsadoCollection PrejuizoCotistaUsadoCollectionByIdCarteira
		{
			get
			{
				if(this._PrejuizoCotistaUsadoCollectionByIdCarteira == null)
				{
					this._PrejuizoCotistaUsadoCollectionByIdCarteira = new PrejuizoCotistaUsadoCollection();
					this._PrejuizoCotistaUsadoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PrejuizoCotistaUsadoCollectionByIdCarteira", this._PrejuizoCotistaUsadoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PrejuizoCotistaUsadoCollectionByIdCarteira.Query.Where(this._PrejuizoCotistaUsadoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._PrejuizoCotistaUsadoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PrejuizoCotistaUsadoCollectionByIdCarteira.fks.Add(PrejuizoCotistaUsadoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._PrejuizoCotistaUsadoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PrejuizoCotistaUsadoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("PrejuizoCotistaUsadoCollectionByIdCarteira"); 
					this._PrejuizoCotistaUsadoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private PrejuizoCotistaUsadoCollection _PrejuizoCotistaUsadoCollectionByIdCarteira;
		#endregion

				
		#region PrejuizoFundoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_PrejuizoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public PrejuizoFundoCollection PrejuizoFundoCollectionByIdCarteira
		{
			get
			{
				if(this._PrejuizoFundoCollectionByIdCarteira == null)
				{
					this._PrejuizoFundoCollectionByIdCarteira = new PrejuizoFundoCollection();
					this._PrejuizoFundoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PrejuizoFundoCollectionByIdCarteira", this._PrejuizoFundoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PrejuizoFundoCollectionByIdCarteira.Query.Where(this._PrejuizoFundoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._PrejuizoFundoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PrejuizoFundoCollectionByIdCarteira.fks.Add(PrejuizoFundoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._PrejuizoFundoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PrejuizoFundoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("PrejuizoFundoCollectionByIdCarteira"); 
					this._PrejuizoFundoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private PrejuizoFundoCollection _PrejuizoFundoCollectionByIdCarteira;
		#endregion

				
		#region PrejuizoFundoHistoricoCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_PrejuizoFundoHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PrejuizoFundoHistoricoCollection PrejuizoFundoHistoricoCollectionByIdCarteira
		{
			get
			{
				if(this._PrejuizoFundoHistoricoCollectionByIdCarteira == null)
				{
					this._PrejuizoFundoHistoricoCollectionByIdCarteira = new PrejuizoFundoHistoricoCollection();
					this._PrejuizoFundoHistoricoCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PrejuizoFundoHistoricoCollectionByIdCarteira", this._PrejuizoFundoHistoricoCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._PrejuizoFundoHistoricoCollectionByIdCarteira.Query.Where(this._PrejuizoFundoHistoricoCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._PrejuizoFundoHistoricoCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._PrejuizoFundoHistoricoCollectionByIdCarteira.fks.Add(PrejuizoFundoHistoricoMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._PrejuizoFundoHistoricoCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PrejuizoFundoHistoricoCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("PrejuizoFundoHistoricoCollectionByIdCarteira"); 
					this._PrejuizoFundoHistoricoCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private PrejuizoFundoHistoricoCollection _PrejuizoFundoHistoricoCollectionByIdCarteira;
		#endregion

				
		#region TabelaPerfilMensalCVMCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_TabelaPerfilMensalCVM_Carteira
		/// </summary>

		[XmlIgnore]
		public TabelaPerfilMensalCVMCollection TabelaPerfilMensalCVMCollectionByIdCarteira
		{
			get
			{
				if(this._TabelaPerfilMensalCVMCollectionByIdCarteira == null)
				{
					this._TabelaPerfilMensalCVMCollectionByIdCarteira = new TabelaPerfilMensalCVMCollection();
					this._TabelaPerfilMensalCVMCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaPerfilMensalCVMCollectionByIdCarteira", this._TabelaPerfilMensalCVMCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._TabelaPerfilMensalCVMCollectionByIdCarteira.Query.Where(this._TabelaPerfilMensalCVMCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._TabelaPerfilMensalCVMCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaPerfilMensalCVMCollectionByIdCarteira.fks.Add(TabelaPerfilMensalCVMMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._TabelaPerfilMensalCVMCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaPerfilMensalCVMCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("TabelaPerfilMensalCVMCollectionByIdCarteira"); 
					this._TabelaPerfilMensalCVMCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private TabelaPerfilMensalCVMCollection _TabelaPerfilMensalCVMCollectionByIdCarteira;
		#endregion

				
		#region TabelaRebateCarteiraCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_TabelaRebateCarteira_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaRebateCarteiraCollection TabelaRebateCarteiraCollectionByIdCarteira
		{
			get
			{
				if(this._TabelaRebateCarteiraCollectionByIdCarteira == null)
				{
					this._TabelaRebateCarteiraCollectionByIdCarteira = new TabelaRebateCarteiraCollection();
					this._TabelaRebateCarteiraCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaRebateCarteiraCollectionByIdCarteira", this._TabelaRebateCarteiraCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._TabelaRebateCarteiraCollectionByIdCarteira.Query.Where(this._TabelaRebateCarteiraCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._TabelaRebateCarteiraCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaRebateCarteiraCollectionByIdCarteira.fks.Add(TabelaRebateCarteiraMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._TabelaRebateCarteiraCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaRebateCarteiraCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("TabelaRebateCarteiraCollectionByIdCarteira"); 
					this._TabelaRebateCarteiraCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private TabelaRebateCarteiraCollection _TabelaRebateCarteiraCollectionByIdCarteira;
		#endregion

				
		#region TabelaRebateDistribuidorCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_TabelaRebateDistribuidor_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaRebateDistribuidorCollection TabelaRebateDistribuidorCollectionByIdCarteira
		{
			get
			{
				if(this._TabelaRebateDistribuidorCollectionByIdCarteira == null)
				{
					this._TabelaRebateDistribuidorCollectionByIdCarteira = new TabelaRebateDistribuidorCollection();
					this._TabelaRebateDistribuidorCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaRebateDistribuidorCollectionByIdCarteira", this._TabelaRebateDistribuidorCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._TabelaRebateDistribuidorCollectionByIdCarteira.Query.Where(this._TabelaRebateDistribuidorCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._TabelaRebateDistribuidorCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaRebateDistribuidorCollectionByIdCarteira.fks.Add(TabelaRebateDistribuidorMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._TabelaRebateDistribuidorCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaRebateDistribuidorCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("TabelaRebateDistribuidorCollectionByIdCarteira"); 
					this._TabelaRebateDistribuidorCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private TabelaRebateDistribuidorCollection _TabelaRebateDistribuidorCollectionByIdCarteira;
		#endregion

				
		#region TemplateCarteira - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Carteira_TemplateCarteira_FK1
		/// </summary>

		[XmlIgnore]
		public TemplateCarteira TemplateCarteira
		{
			get
			{
				if(this._TemplateCarteira == null)
				{
					this._TemplateCarteira = new TemplateCarteira();
					this._TemplateCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("TemplateCarteira", this._TemplateCarteira);
				
					if(this.IdCarteira != null)
					{
						this._TemplateCarteira.Query.Where(this._TemplateCarteira.Query.IdCarteira == this.IdCarteira);
						this._TemplateCarteira.Query.Load();
					}
				}

				return this._TemplateCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TemplateCarteira != null) 
				{ 
					this.RemovePostOneSave("TemplateCarteira"); 
					this._TemplateCarteira = null;
					
				} 
			}          			
		}

		private TemplateCarteira _TemplateCarteira;
		#endregion

				
		#region TraderCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Trader_Carteira_FK
		/// </summary>

		[XmlIgnore]
		public TraderCollection TraderCollectionByIdCarteira
		{
			get
			{
				if(this._TraderCollectionByIdCarteira == null)
				{
					this._TraderCollectionByIdCarteira = new TraderCollection();
					this._TraderCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TraderCollectionByIdCarteira", this._TraderCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._TraderCollectionByIdCarteira.Query.Where(this._TraderCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._TraderCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._TraderCollectionByIdCarteira.fks.Add(TraderMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._TraderCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TraderCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("TraderCollectionByIdCarteira"); 
					this._TraderCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private TraderCollection _TraderCollectionByIdCarteira;
		#endregion

				
		#region TransferenciaCotaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_TransferenciaCota_FK1
		/// </summary>

		[XmlIgnore]
		public TransferenciaCotaCollection TransferenciaCotaCollectionByIdCarteira
		{
			get
			{
				if(this._TransferenciaCotaCollectionByIdCarteira == null)
				{
					this._TransferenciaCotaCollectionByIdCarteira = new TransferenciaCotaCollection();
					this._TransferenciaCotaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaCotaCollectionByIdCarteira", this._TransferenciaCotaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._TransferenciaCotaCollectionByIdCarteira.Query.Where(this._TransferenciaCotaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._TransferenciaCotaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaCotaCollectionByIdCarteira.fks.Add(TransferenciaCotaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._TransferenciaCotaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaCotaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("TransferenciaCotaCollectionByIdCarteira"); 
					this._TransferenciaCotaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private TransferenciaCotaCollection _TransferenciaCotaCollectionByIdCarteira;
		#endregion

				
		#region TransferenciaEntreCotistaCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TransEntreCotista_Carteira_FK
		/// </summary>

		[XmlIgnore]
		public TransferenciaEntreCotistaCollection TransferenciaEntreCotistaCollectionByIdCarteira
		{
			get
			{
				if(this._TransferenciaEntreCotistaCollectionByIdCarteira == null)
				{
					this._TransferenciaEntreCotistaCollectionByIdCarteira = new TransferenciaEntreCotistaCollection();
					this._TransferenciaEntreCotistaCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaEntreCotistaCollectionByIdCarteira", this._TransferenciaEntreCotistaCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._TransferenciaEntreCotistaCollectionByIdCarteira.Query.Where(this._TransferenciaEntreCotistaCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._TransferenciaEntreCotistaCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaEntreCotistaCollectionByIdCarteira.fks.Add(TransferenciaEntreCotistaMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._TransferenciaEntreCotistaCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaEntreCotistaCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("TransferenciaEntreCotistaCollectionByIdCarteira"); 
					this._TransferenciaEntreCotistaCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private TransferenciaEntreCotistaCollection _TransferenciaEntreCotistaCollectionByIdCarteira;
		#endregion

				
		#region TravamentoCotasCollectionByIdCarteira - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_TravamentoCotas_FK1
		/// </summary>

		[XmlIgnore]
		public TravamentoCotasCollection TravamentoCotasCollectionByIdCarteira
		{
			get
			{
				if(this._TravamentoCotasCollectionByIdCarteira == null)
				{
					this._TravamentoCotasCollectionByIdCarteira = new TravamentoCotasCollection();
					this._TravamentoCotasCollectionByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TravamentoCotasCollectionByIdCarteira", this._TravamentoCotasCollectionByIdCarteira);
				
					if(this.IdCarteira != null)
					{
						this._TravamentoCotasCollectionByIdCarteira.Query.Where(this._TravamentoCotasCollectionByIdCarteira.Query.IdCarteira == this.IdCarteira);
						this._TravamentoCotasCollectionByIdCarteira.Query.Load();

						// Auto-hookup Foreign Keys
						this._TravamentoCotasCollectionByIdCarteira.fks.Add(TravamentoCotasMetadata.ColumnNames.IdCarteira, this.IdCarteira);
					}
				}

				return this._TravamentoCotasCollectionByIdCarteira;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TravamentoCotasCollectionByIdCarteira != null) 
				{ 
					this.RemovePostSave("TravamentoCotasCollectionByIdCarteira"); 
					this._TravamentoCotasCollectionByIdCarteira = null;
					
				} 
			} 			
		}

		private TravamentoCotasCollection _TravamentoCotasCollectionByIdCarteira;
		#endregion

				
		#region UpToAgenteMercadoByIdAgenteAdministrador - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteAdministrador
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteAdministrador == null
					&& IdAgenteAdministrador != null					)
				{
					this._UpToAgenteMercadoByIdAgenteAdministrador = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteAdministrador.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteAdministrador", this._UpToAgenteMercadoByIdAgenteAdministrador);
					this._UpToAgenteMercadoByIdAgenteAdministrador.Query.Where(this._UpToAgenteMercadoByIdAgenteAdministrador.Query.IdAgente == this.IdAgenteAdministrador);
					this._UpToAgenteMercadoByIdAgenteAdministrador.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteAdministrador;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteAdministrador");
				

				if(value == null)
				{
					this.IdAgenteAdministrador = null;
					this._UpToAgenteMercadoByIdAgenteAdministrador = null;
				}
				else
				{
					this.IdAgenteAdministrador = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteAdministrador = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteAdministrador", this._UpToAgenteMercadoByIdAgenteAdministrador);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAgenteMercadoByIdAgenteGestor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_Carteira_FK2
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteGestor
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteGestor == null
					&& IdAgenteGestor != null					)
				{
					this._UpToAgenteMercadoByIdAgenteGestor = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteGestor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteGestor", this._UpToAgenteMercadoByIdAgenteGestor);
					this._UpToAgenteMercadoByIdAgenteGestor.Query.Where(this._UpToAgenteMercadoByIdAgenteGestor.Query.IdAgente == this.IdAgenteGestor);
					this._UpToAgenteMercadoByIdAgenteGestor.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteGestor;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteGestor");
				

				if(value == null)
				{
					this.IdAgenteGestor = null;
					this._UpToAgenteMercadoByIdAgenteGestor = null;
				}
				else
				{
					this.IdAgenteGestor = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteGestor = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteGestor", this._UpToAgenteMercadoByIdAgenteGestor);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAgenteMercadoByContratante - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__Carteira__Contra__6537D259
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByContratante
		{
			get
			{
				if(this._UpToAgenteMercadoByContratante == null
					&& Contratante != null					)
				{
					this._UpToAgenteMercadoByContratante = new AgenteMercado();
					this._UpToAgenteMercadoByContratante.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByContratante", this._UpToAgenteMercadoByContratante);
					this._UpToAgenteMercadoByContratante.Query.Where(this._UpToAgenteMercadoByContratante.Query.IdAgente == this.Contratante);
					this._UpToAgenteMercadoByContratante.Query.Load();
				}

				return this._UpToAgenteMercadoByContratante;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByContratante");
				

				if(value == null)
				{
					this.Contratante = null;
					this._UpToAgenteMercadoByContratante = null;
				}
				else
				{
					this.Contratante = value.IdAgente;
					this._UpToAgenteMercadoByContratante = value;
					this.SetPreSave("UpToAgenteMercadoByContratante", this._UpToAgenteMercadoByContratante);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCategoriaFundoByIdCategoria - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - CategoriaFundo_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public CategoriaFundo UpToCategoriaFundoByIdCategoria
		{
			get
			{
				if(this._UpToCategoriaFundoByIdCategoria == null
					&& IdCategoria != null					)
				{
					this._UpToCategoriaFundoByIdCategoria = new CategoriaFundo();
					this._UpToCategoriaFundoByIdCategoria.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCategoriaFundoByIdCategoria", this._UpToCategoriaFundoByIdCategoria);
					this._UpToCategoriaFundoByIdCategoria.Query.Where(this._UpToCategoriaFundoByIdCategoria.Query.IdCategoria == this.IdCategoria);
					this._UpToCategoriaFundoByIdCategoria.Query.Load();
				}

				return this._UpToCategoriaFundoByIdCategoria;
			}
			
			set
			{
				this.RemovePreSave("UpToCategoriaFundoByIdCategoria");
				

				if(value == null)
				{
					this.IdCategoria = null;
					this._UpToCategoriaFundoByIdCategoria = null;
				}
				else
				{
					this.IdCategoria = value.IdCategoria;
					this._UpToCategoriaFundoByIdCategoria = value;
					this.SetPreSave("UpToCategoriaFundoByIdCategoria", this._UpToCategoriaFundoByIdCategoria);
				}
				
			}
		}
		#endregion
		

		#region UpToCliente - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Cliente_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToCliente
		{
			get
			{
				if(this._UpToCliente == null
					&& IdCarteira != null					)
				{
					this._UpToCliente = new Cliente();
					this._UpToCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCliente", this._UpToCliente);
					this._UpToCliente.Query.Where(this._UpToCliente.Query.IdCliente == this.IdCarteira);
					this._UpToCliente.Query.Load();
				}

				return this._UpToCliente;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToCliente");

				if(value == null)
				{
					this._UpToCliente = null;
				}
				else
				{
					this._UpToCliente = value;
					this.SetPreSave("UpToCliente", this._UpToCliente);
				}
				
				
			} 
		}

		private Cliente _UpToCliente;
		#endregion

				
		#region UpToEstrategiaByIdEstrategia - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Estrategia_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public Estrategia UpToEstrategiaByIdEstrategia
		{
			get
			{
				if(this._UpToEstrategiaByIdEstrategia == null
					&& IdEstrategia != null					)
				{
					this._UpToEstrategiaByIdEstrategia = new Estrategia();
					this._UpToEstrategiaByIdEstrategia.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEstrategiaByIdEstrategia", this._UpToEstrategiaByIdEstrategia);
					this._UpToEstrategiaByIdEstrategia.Query.Where(this._UpToEstrategiaByIdEstrategia.Query.IdEstrategia == this.IdEstrategia);
					this._UpToEstrategiaByIdEstrategia.Query.Load();
				}

				return this._UpToEstrategiaByIdEstrategia;
			}
			
			set
			{
				this.RemovePreSave("UpToEstrategiaByIdEstrategia");
				

				if(value == null)
				{
					this.IdEstrategia = null;
					this._UpToEstrategiaByIdEstrategia = null;
				}
				else
				{
					this.IdEstrategia = value.IdEstrategia;
					this._UpToEstrategiaByIdEstrategia = value;
					this.SetPreSave("UpToEstrategiaByIdEstrategia", this._UpToEstrategiaByIdEstrategia);
				}
				
			}
		}
		#endregion
		

				
		#region UpToGrauRiscoByIdGrauRisco - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - GrauRisco_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public GrauRisco UpToGrauRiscoByIdGrauRisco
		{
			get
			{
				if(this._UpToGrauRiscoByIdGrauRisco == null
					&& IdGrauRisco != null					)
				{
					this._UpToGrauRiscoByIdGrauRisco = new GrauRisco();
					this._UpToGrauRiscoByIdGrauRisco.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToGrauRiscoByIdGrauRisco", this._UpToGrauRiscoByIdGrauRisco);
					this._UpToGrauRiscoByIdGrauRisco.Query.Where(this._UpToGrauRiscoByIdGrauRisco.Query.IdGrauRisco == this.IdGrauRisco);
					this._UpToGrauRiscoByIdGrauRisco.Query.Load();
				}

				return this._UpToGrauRiscoByIdGrauRisco;
			}
			
			set
			{
				this.RemovePreSave("UpToGrauRiscoByIdGrauRisco");
				

				if(value == null)
				{
					this.IdGrauRisco = null;
					this._UpToGrauRiscoByIdGrauRisco = null;
				}
				else
				{
					this.IdGrauRisco = value.IdGrauRisco;
					this._UpToGrauRiscoByIdGrauRisco = value;
					this.SetPreSave("UpToGrauRiscoByIdGrauRisco", this._UpToGrauRiscoByIdGrauRisco);
				}
				
			}
		}
		#endregion
		

				
		#region UpToGrupoPerfilMTMByIdGrupoPerfilMTM - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_GrupoPerfil_FK
		/// </summary>

		[XmlIgnore]
		public GrupoPerfilMTM UpToGrupoPerfilMTMByIdGrupoPerfilMTM
		{
			get
			{
				if(this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM == null
					&& IdGrupoPerfilMTM != null					)
				{
					this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM = new GrupoPerfilMTM();
					this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToGrupoPerfilMTMByIdGrupoPerfilMTM", this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM);
					this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM.Query.Where(this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM.Query.IdGrupoPerfilMTM == this.IdGrupoPerfilMTM);
					this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM.Query.Load();
				}

				return this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM;
			}
			
			set
			{
				this.RemovePreSave("UpToGrupoPerfilMTMByIdGrupoPerfilMTM");
				

				if(value == null)
				{
					this.IdGrupoPerfilMTM = null;
					this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM = null;
				}
				else
				{
					this.IdGrupoPerfilMTM = value.IdGrupoPerfilMTM;
					this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM = value;
					this.SetPreSave("UpToGrupoPerfilMTMByIdGrupoPerfilMTM", this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM);
				}
				
			}
		}
		#endregion
		

				
		#region UpToIndiceByIdIndiceBenchmark - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Indice_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndiceBenchmark
		{
			get
			{
				if(this._UpToIndiceByIdIndiceBenchmark == null
					&& IdIndiceBenchmark != null					)
				{
					this._UpToIndiceByIdIndiceBenchmark = new Indice();
					this._UpToIndiceByIdIndiceBenchmark.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndiceBenchmark", this._UpToIndiceByIdIndiceBenchmark);
					this._UpToIndiceByIdIndiceBenchmark.Query.Where(this._UpToIndiceByIdIndiceBenchmark.Query.IdIndice == this.IdIndiceBenchmark);
					this._UpToIndiceByIdIndiceBenchmark.Query.Load();
				}

				return this._UpToIndiceByIdIndiceBenchmark;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndiceBenchmark");
				

				if(value == null)
				{
					this.IdIndiceBenchmark = null;
					this._UpToIndiceByIdIndiceBenchmark = null;
				}
				else
				{
					this.IdIndiceBenchmark = value.IdIndice;
					this._UpToIndiceByIdIndiceBenchmark = value;
					this.SetPreSave("UpToIndiceByIdIndiceBenchmark", this._UpToIndiceByIdIndiceBenchmark);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "AgendaComeCotasCollectionByIdCarteira", typeof(AgendaComeCotasCollection), new AgendaComeCotas()));
			props.Add(new esPropertyDescriptor(this, "AgendaFundoCollectionByIdCarteira", typeof(AgendaFundoCollection), new AgendaFundo()));
			props.Add(new esPropertyDescriptor(this, "AliquotaEspecificaCollectionByIdCarteira", typeof(AliquotaEspecificaCollection), new AliquotaEspecifica()));
			props.Add(new esPropertyDescriptor(this, "CalculoAdministracaoCollectionByIdCarteira", typeof(CalculoAdministracaoCollection), new CalculoAdministracao()));
			props.Add(new esPropertyDescriptor(this, "CalculoAdministracaoAssociadaCollectionByIdCarteira", typeof(CalculoAdministracaoAssociadaCollection), new CalculoAdministracaoAssociada()));
			props.Add(new esPropertyDescriptor(this, "CalculoAdministracaoAssociadaHistoricoCollectionByIdCarteira", typeof(CalculoAdministracaoAssociadaHistoricoCollection), new CalculoAdministracaoAssociadaHistorico()));
			props.Add(new esPropertyDescriptor(this, "CalculoAdministracaoHistoricoCollectionByIdCarteira", typeof(CalculoAdministracaoHistoricoCollection), new CalculoAdministracaoHistorico()));
			props.Add(new esPropertyDescriptor(this, "CalculoPerformanceCollectionByIdCarteira", typeof(CalculoPerformanceCollection), new CalculoPerformance()));
			props.Add(new esPropertyDescriptor(this, "CalculoPerformanceHistoricoCollectionByIdCarteira", typeof(CalculoPerformanceHistoricoCollection), new CalculoPerformanceHistorico()));
			props.Add(new esPropertyDescriptor(this, "CalculoProvisaoCollectionByIdCarteira", typeof(CalculoProvisaoCollection), new CalculoProvisao()));
			props.Add(new esPropertyDescriptor(this, "CalculoProvisaoHistoricoCollectionByIdCarteira", typeof(CalculoProvisaoHistoricoCollection), new CalculoProvisaoHistorico()));
			props.Add(new esPropertyDescriptor(this, "CalculoRebateCarteiraCollectionByIdCarteira", typeof(CalculoRebateCarteiraCollection), new CalculoRebateCarteira()));
			props.Add(new esPropertyDescriptor(this, "CalculoRebateCotistaCollectionByIdCarteira", typeof(CalculoRebateCotistaCollection), new CalculoRebateCotista()));
			props.Add(new esPropertyDescriptor(this, "CalculoRebateImpactaPLCollectionByIdCarteira", typeof(CalculoRebateImpactaPLCollection), new CalculoRebateImpactaPL()));
			props.Add(new esPropertyDescriptor(this, "CarteiraComplementoCollectionByIdCarteira", typeof(CarteiraComplementoCollection), new CarteiraComplemento()));
			props.Add(new esPropertyDescriptor(this, "CarteiraComplementoCollectionByIdCarteiraFundoPai", typeof(CarteiraComplementoCollection), new CarteiraComplemento()));
			props.Add(new esPropertyDescriptor(this, "CarteiraMaeCollectionByIdCarteiraMae", typeof(CarteiraMaeCollection), new CarteiraMae()));
			props.Add(new esPropertyDescriptor(this, "CarteiraMaeCollectionByIdCarteiraFilha", typeof(CarteiraMaeCollection), new CarteiraMae()));
			props.Add(new esPropertyDescriptor(this, "CotistaCollectionByIdCarteira", typeof(CotistaCollection), new Cotista()));
			props.Add(new esPropertyDescriptor(this, "DesenquadramentoTributarioCollectionByIdCarteira", typeof(DesenquadramentoTributarioCollection), new DesenquadramentoTributario()));
			props.Add(new esPropertyDescriptor(this, "DetalheResgateCotistaCollectionByIdCarteira", typeof(DetalheResgateCotistaCollection), new DetalheResgateCotista()));
			props.Add(new esPropertyDescriptor(this, "DetalheResgateFundoCollectionByIdCarteira", typeof(DetalheResgateFundoCollection), new DetalheResgateFundo()));
			props.Add(new esPropertyDescriptor(this, "EnquadraItemFundoCollectionByIdCarteira", typeof(EnquadraItemFundoCollection), new EnquadraItemFundo()));
			props.Add(new esPropertyDescriptor(this, "EnquadraRegraCollectionByIdCarteira", typeof(EnquadraRegraCollection), new EnquadraRegra()));
			props.Add(new esPropertyDescriptor(this, "EnquadraResultadoCollectionByIdCarteira", typeof(EnquadraResultadoCollection), new EnquadraResultado()));
			props.Add(new esPropertyDescriptor(this, "EnquadraResultadoDetalheCollectionByIdCarteira", typeof(EnquadraResultadoDetalheCollection), new EnquadraResultadoDetalhe()));
			props.Add(new esPropertyDescriptor(this, "EventoFundoCollectionByIdCarteiraDestino", typeof(EventoFundoCollection), new EventoFundo()));
			props.Add(new esPropertyDescriptor(this, "EventoFundoCollectionByIdCarteiraOrigem", typeof(EventoFundoCollection), new EventoFundo()));
			props.Add(new esPropertyDescriptor(this, "ExcecoesTributacaoIRCollectionByIdCarteira", typeof(ExcecoesTributacaoIRCollection), new ExcecoesTributacaoIR()));
			props.Add(new esPropertyDescriptor(this, "FundoInvestimentoFormaCondominioCollectionByIdCarteira", typeof(FundoInvestimentoFormaCondominioCollection), new FundoInvestimentoFormaCondominio()));
			props.Add(new esPropertyDescriptor(this, "HistoricoCotaCollectionByIdCarteira", typeof(HistoricoCotaCollection), new HistoricoCota()));
			props.Add(new esPropertyDescriptor(this, "HistoricoCotaGalgoCollectionByIdCarteira", typeof(HistoricoCotaGalgoCollection), new HistoricoCotaGalgo()));
			props.Add(new esPropertyDescriptor(this, "InformacoesComplementaresFundoCollectionByIdCarteira", typeof(InformacoesComplementaresFundoCollection), new InformacoesComplementaresFundo()));
			props.Add(new esPropertyDescriptor(this, "IRProventosCollectionByIdCarteira", typeof(IRProventosCollection), new IRProventos()));
			props.Add(new esPropertyDescriptor(this, "LaminaCollectionByIdCarteira", typeof(LaminaCollection), new Lamina()));
			props.Add(new esPropertyDescriptor(this, "ListaBenchmarkCollectionByIdCarteira", typeof(ListaBenchmarkCollection), new ListaBenchmark()));
			props.Add(new esPropertyDescriptor(this, "OfertaDistribuicaoCotasCollectionByIdCarteira", typeof(OfertaDistribuicaoCotasCollection), new OfertaDistribuicaoCotas()));
			props.Add(new esPropertyDescriptor(this, "OperacaoCotistaCollectionByIdCarteira", typeof(OperacaoCotistaCollection), new OperacaoCotista()));
			props.Add(new esPropertyDescriptor(this, "OperacaoFundoCollectionByIdCarteira", typeof(OperacaoFundoCollection), new OperacaoFundo()));
			props.Add(new esPropertyDescriptor(this, "OperacaoRendaFixaCollectionByIdCarteiraContraparte", typeof(OperacaoRendaFixaCollection), new OperacaoRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "OperacaoSwapCollectionByIdAtivoCarteira", typeof(OperacaoSwapCollection), new OperacaoSwap()));
			props.Add(new esPropertyDescriptor(this, "OperacaoSwapCollectionByIdAtivoCarteiraContraParte", typeof(OperacaoSwapCollection), new OperacaoSwap()));
			props.Add(new esPropertyDescriptor(this, "OrdemCotistaCollectionByIdCarteira", typeof(OrdemCotistaCollection), new OrdemCotista()));
			props.Add(new esPropertyDescriptor(this, "OrdemFundoCollectionByIdCarteira", typeof(OrdemFundoCollection), new OrdemFundo()));
			props.Add(new esPropertyDescriptor(this, "OrdemRendaFixaCollectionByIdCarteiraContraParte", typeof(OrdemRendaFixaCollection), new OrdemRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "PatrimonioFundoEmissaoSerieCollectionByIdCarteira", typeof(PatrimonioFundoEmissaoSerieCollection), new PatrimonioFundoEmissaoSerie()));
			props.Add(new esPropertyDescriptor(this, "PerfilClassificacaoFundoCarteiraCollectionByIdCarteira", typeof(PerfilClassificacaoFundoCarteiraCollection), new PerfilClassificacaoFundoCarteira()));
			props.Add(new esPropertyDescriptor(this, "PerfilClienteCollectionByIdCarteira", typeof(PerfilClienteCollection), new PerfilCliente()));
			props.Add(new esPropertyDescriptor(this, "PortfolioPadraoCollectionByIdCarteiraDespesasReceitas", typeof(PortfolioPadraoCollection), new PortfolioPadrao()));
			props.Add(new esPropertyDescriptor(this, "PortfolioPadraoCollectionByIdCarteiraTaxaGestao", typeof(PortfolioPadraoCollection), new PortfolioPadrao()));
			props.Add(new esPropertyDescriptor(this, "PortfolioPadraoCollectionByIdCarteiraTaxaPerformance", typeof(PortfolioPadraoCollection), new PortfolioPadrao()));
			props.Add(new esPropertyDescriptor(this, "PosicaoCotistaCollectionByIdCarteira", typeof(PosicaoCotistaCollection), new PosicaoCotista()));
			props.Add(new esPropertyDescriptor(this, "PosicaoCotistaAberturaCollectionByIdCarteira", typeof(PosicaoCotistaAberturaCollection), new PosicaoCotistaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoCotistaAlteradaCollectionByIdCarteira", typeof(PosicaoCotistaAlteradaCollection), new PosicaoCotistaAlterada()));
			props.Add(new esPropertyDescriptor(this, "PosicaoCotistaHistoricoCollectionByIdCarteira", typeof(PosicaoCotistaHistoricoCollection), new PosicaoCotistaHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoFundoCollectionByIdCarteira", typeof(PosicaoFundoCollection), new PosicaoFundo()));
			props.Add(new esPropertyDescriptor(this, "PosicaoFundoAberturaCollectionByIdCarteira", typeof(PosicaoFundoAberturaCollection), new PosicaoFundoAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoFundoHistoricoCollectionByIdCarteira", typeof(PosicaoFundoHistoricoCollection), new PosicaoFundoHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapCollectionByIdAtivoCarteira", typeof(PosicaoSwapCollection), new PosicaoSwap()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapCollectionByIdAtivoCarteiraContraParte", typeof(PosicaoSwapCollection), new PosicaoSwap()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapAberturaCollectionByIdAtivoCarteira", typeof(PosicaoSwapAberturaCollection), new PosicaoSwapAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapAberturaCollectionByIdAtivoCarteiraContraParte", typeof(PosicaoSwapAberturaCollection), new PosicaoSwapAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapHistoricoCollectionByIdAtivoCarteiraContraParte", typeof(PosicaoSwapHistoricoCollection), new PosicaoSwapHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoSwapHistoricoCollectionByIdAtivoCarteira", typeof(PosicaoSwapHistoricoCollection), new PosicaoSwapHistorico()));
			props.Add(new esPropertyDescriptor(this, "PrejuizoCotistaCollectionByIdCarteira", typeof(PrejuizoCotistaCollection), new PrejuizoCotista()));
			props.Add(new esPropertyDescriptor(this, "PrejuizoCotistaHistoricoCollectionByIdCarteira", typeof(PrejuizoCotistaHistoricoCollection), new PrejuizoCotistaHistorico()));
			props.Add(new esPropertyDescriptor(this, "PrejuizoCotistaUsadoCollectionByIdCarteira", typeof(PrejuizoCotistaUsadoCollection), new PrejuizoCotistaUsado()));
			props.Add(new esPropertyDescriptor(this, "PrejuizoFundoCollectionByIdCarteira", typeof(PrejuizoFundoCollection), new PrejuizoFundo()));
			props.Add(new esPropertyDescriptor(this, "PrejuizoFundoHistoricoCollectionByIdCarteira", typeof(PrejuizoFundoHistoricoCollection), new PrejuizoFundoHistorico()));
			props.Add(new esPropertyDescriptor(this, "TabelaPerfilMensalCVMCollectionByIdCarteira", typeof(TabelaPerfilMensalCVMCollection), new TabelaPerfilMensalCVM()));
			props.Add(new esPropertyDescriptor(this, "TabelaRebateCarteiraCollectionByIdCarteira", typeof(TabelaRebateCarteiraCollection), new TabelaRebateCarteira()));
			props.Add(new esPropertyDescriptor(this, "TabelaRebateDistribuidorCollectionByIdCarteira", typeof(TabelaRebateDistribuidorCollection), new TabelaRebateDistribuidor()));
			props.Add(new esPropertyDescriptor(this, "TraderCollectionByIdCarteira", typeof(TraderCollection), new Trader()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaCotaCollectionByIdCarteira", typeof(TransferenciaCotaCollection), new TransferenciaCota()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaEntreCotistaCollectionByIdCarteira", typeof(TransferenciaEntreCotistaCollection), new TransferenciaEntreCotista()));
			props.Add(new esPropertyDescriptor(this, "TravamentoCotasCollectionByIdCarteira", typeof(TravamentoCotasCollection), new TravamentoCotas()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteAdministrador != null)
			{
				this.IdAgenteAdministrador = this._UpToAgenteMercadoByIdAgenteAdministrador.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteGestor != null)
			{
				this.IdAgenteGestor = this._UpToAgenteMercadoByIdAgenteGestor.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByContratante != null)
			{
				this.Contratante = this._UpToAgenteMercadoByContratante.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToCategoriaFundoByIdCategoria != null)
			{
				this.IdCategoria = this._UpToCategoriaFundoByIdCategoria.IdCategoria;
			}
			if(!this.es.IsDeleted && this._UpToEstrategiaByIdEstrategia != null)
			{
				this.IdEstrategia = this._UpToEstrategiaByIdEstrategia.IdEstrategia;
			}
			if(!this.es.IsDeleted && this._UpToGrauRiscoByIdGrauRisco != null)
			{
				this.IdGrauRisco = this._UpToGrauRiscoByIdGrauRisco.IdGrauRisco;
			}
			if(!this.es.IsDeleted && this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM != null)
			{
				this.IdGrupoPerfilMTM = this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM.IdGrupoPerfilMTM;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}
	
	public partial class CarteiraCollection : esCarteiraCollection
	{
		#region ManyToManyIndiceCollection
		/// <summary>
		/// Used internally for constructing a Many to Many JOIN
		/// </summary>
		public bool ManyToManyIndiceCollection(System.Int16? IdIndice)
		{
			esParameters parms = new esParameters();
			parms.Add("IdIndice", IdIndice);
	
			return base.Load( esQueryType.ManyToMany, 
				"Carteira,ListaBenchmark|IdCarteira,IdCarteira|IdIndice",	parms);
		}
		#endregion
	}




	[Serializable]
	abstract public class esCarteiraQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CarteiraMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Nome
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.Nome, esSystemType.String);
			}
		} 
		
		public esQueryItem Apelido
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.Apelido, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoCota
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TipoCota, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoCarteira
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TipoCarteira, esSystemType.Byte);
			}
		} 
		
		public esQueryItem StatusAtivo
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.StatusAtivo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdIndiceBenchmark
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.IdIndiceBenchmark, esSystemType.Int16);
			}
		} 
		
		public esQueryItem CasasDecimaisCota
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CasasDecimaisCota, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CasasDecimaisQuantidade
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CasasDecimaisQuantidade, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TruncaCota
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TruncaCota, esSystemType.String);
			}
		} 
		
		public esQueryItem TruncaQuantidade
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TruncaQuantidade, esSystemType.String);
			}
		} 
		
		public esQueryItem TruncaFinanceiro
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TruncaFinanceiro, esSystemType.String);
			}
		} 
		
		public esQueryItem CotaInicial
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CotaInicial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorMinimoAplicacao
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ValorMinimoAplicacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorMinimoResgate
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ValorMinimoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorMinimoSaldo
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ValorMinimoSaldo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorMaximoAplicacao
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ValorMaximoAplicacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorMinimoInicial
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ValorMinimoInicial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoRentabilidade
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TipoRentabilidade, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoCusto
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TipoCusto, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DiasCotizacaoAplicacao
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.DiasCotizacaoAplicacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DiasCotizacaoResgate
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.DiasCotizacaoResgate, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DiasLiquidacaoAplicacao
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.DiasLiquidacaoAplicacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DiasLiquidacaoResgate
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.DiasLiquidacaoResgate, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoTributacao
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TipoTributacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CalculaIOF
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CalculaIOF, esSystemType.String);
			}
		} 
		
		public esQueryItem DiasAniversario
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.DiasAniversario, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TipoAniversario
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TipoAniversario, esSystemType.Byte);
			}
		} 
		
		public esQueryItem HorarioInicio
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.HorarioInicio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem HorarioFim
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.HorarioFim, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem HorarioLimiteCotizacao
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.HorarioLimiteCotizacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCategoria
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.IdCategoria, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdSubCategoria
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.IdSubCategoria, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteAdministrador
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.IdAgenteAdministrador, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteGestor
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.IdAgenteGestor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CobraTaxaFiscalizacaoCVM
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CobraTaxaFiscalizacaoCVM, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoCVM
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TipoCVM, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataInicioCota
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.DataInicioCota, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CalculaEnquadra
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CalculaEnquadra, esSystemType.String);
			}
		} 
		
		public esQueryItem ContagemDiasConversaoResgate
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ContagemDiasConversaoResgate, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ProjecaoIRResgate
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ProjecaoIRResgate, esSystemType.Byte);
			}
		} 
		
		public esQueryItem PublicoAlvo
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.PublicoAlvo, esSystemType.String);
			}
		} 
		
		public esQueryItem Objetivo
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.Objetivo, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAgenteCustodiante
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.IdAgenteCustodiante, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TextoLivre1
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TextoLivre1, esSystemType.String);
			}
		} 
		
		public esQueryItem TextoLivre2
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TextoLivre2, esSystemType.String);
			}
		} 
		
		public esQueryItem TextoLivre3
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TextoLivre3, esSystemType.String);
			}
		} 
		
		public esQueryItem TextoLivre4
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TextoLivre4, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoVisaoFundo
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TipoVisaoFundo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DistribuicaoDividendo
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.DistribuicaoDividendo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdEstrategia
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.IdEstrategia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoAnbid
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CodigoAnbid, esSystemType.String);
			}
		} 
		
		public esQueryItem ContagemPrazoIOF
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ContagemPrazoIOF, esSystemType.Byte);
			}
		} 
		
		public esQueryItem PrioridadeOperacao
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.PrioridadeOperacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CodigoCDA
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CodigoCDA, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CompensacaoPrejuizo
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CompensacaoPrejuizo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DiasConfidencialidade
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.DiasConfidencialidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoFDIC
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CodigoFDIC, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoCalculoRetorno
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TipoCalculoRetorno, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdGrauRisco
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.IdGrauRisco, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoIsin
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CodigoIsin, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoCetip
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CodigoCetip, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoBloomberg
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CodigoBloomberg, esSystemType.String);
			}
		} 
		
		public esQueryItem HorarioInicioResgate
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.HorarioInicioResgate, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem HorarioFimResgate
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.HorarioFimResgate, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdGrupoEconomico
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.IdGrupoEconomico, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FundoExclusivo
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.FundoExclusivo, esSystemType.String);
			}
		} 
		
		public esQueryItem OrdemRelatorio
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.OrdemRelatorio, esSystemType.Int32);
			}
		} 
		
		public esQueryItem LiberaAplicacao
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.LiberaAplicacao, esSystemType.String);
			}
		} 
		
		public esQueryItem LiberaResgate
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.LiberaResgate, esSystemType.String);
			}
		} 
		
		public esQueryItem PerfilRisco
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.PerfilRisco, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CalculaMTM
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CalculaMTM, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CalculaPrazoMedio
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CalculaPrazoMedio, esSystemType.String);
			}
		} 
		
		public esQueryItem DiasAposResgateIR
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.DiasAposResgateIR, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ProjecaoIRComeCotas
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ProjecaoIRComeCotas, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DiasAposComeCotasIR
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.DiasAposComeCotasIR, esSystemType.Int32);
			}
		} 
		
		public esQueryItem RealizaCompensacaoDePrejuizo
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.RealizaCompensacaoDePrejuizo, esSystemType.String);
			}
		} 
		
		public esQueryItem BuscaCotaAnterior
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.BuscaCotaAnterior, esSystemType.String);
			}
		} 
		
		public esQueryItem NumeroDiasBuscaCota
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.NumeroDiasBuscaCota, esSystemType.Int32);
			}
		} 
		
		public esQueryItem BandaVariacao
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.BandaVariacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ExcecaoRegraTxAdm
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ExcecaoRegraTxAdm, esSystemType.String);
			}
		} 
		
		public esQueryItem RebateImpactaPL
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.RebateImpactaPL, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoFundo
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TipoFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Fie
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.Fie, esSystemType.String);
			}
		} 
		
		public esQueryItem DataInicioContIOF
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.DataInicioContIOF, esSystemType.Int16);
			}
		} 
		
		public esQueryItem DataFimContIOF
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.DataFimContIOF, esSystemType.Int16);
			}
		} 
		
		public esQueryItem ContaPrzIOFVirtual
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ContaPrzIOFVirtual, esSystemType.Int16);
			}
		} 
		
		public esQueryItem ProjecaoIOFResgate
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ProjecaoIOFResgate, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DiasAposResgateIOF
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.DiasAposResgateIOF, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoConsolidacaoExterno
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CodigoConsolidacaoExterno, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoBDS
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CodigoBDS, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoSTI
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CodigoSTI, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoVisualizacaoResgCotista
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.TipoVisualizacaoResgCotista, esSystemType.String);
			}
		} 
		
		public esQueryItem ApenasInvestidorProfissional
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ApenasInvestidorProfissional, esSystemType.String);
			}
		} 
		
		public esQueryItem ApenasInvestidorQualificado
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ApenasInvestidorQualificado, esSystemType.String);
			}
		} 
		
		public esQueryItem RealizaOfertaSubscricao
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.RealizaOfertaSubscricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Rendimento
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.Rendimento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdGrupoPerfilMTM
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.IdGrupoPerfilMTM, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ProcAutomatico
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ProcAutomatico, esSystemType.String);
			}
		} 
		
		public esQueryItem ExplodeCotasDeFundos
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ExplodeCotasDeFundos, esSystemType.String);
			}
		} 
		
		public esQueryItem ControladoriaAtivo
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ControladoriaAtivo, esSystemType.String);
			}
		} 
		
		public esQueryItem ControladoriaPassivo
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ControladoriaPassivo, esSystemType.String);
			}
		} 
		
		public esQueryItem MesmoConglomerado
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.MesmoConglomerado, esSystemType.String);
			}
		} 
		
		public esQueryItem CategoriaAnbima
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.CategoriaAnbima, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Contratante
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.Contratante, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Corretora
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.Corretora, esSystemType.String);
			}
		} 
		
		public esQueryItem ExportaGalgo
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ExportaGalgo, esSystemType.String);
			}
		} 
		
		public esQueryItem PossuiResgateAutomatico
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.PossuiResgateAutomatico, esSystemType.String);
			}
		} 
		
		public esQueryItem InfluenciaGestorLocalCvm
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.InfluenciaGestorLocalCvm, esSystemType.String);
			}
		} 
		
		public esQueryItem InvestimentoColetivoCvm
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.InvestimentoColetivoCvm, esSystemType.String);
			}
		} 
		
		public esQueryItem ComeCotasEntreRegatesConversao
		{
			get
			{
				return new esQueryItem(this, CarteiraMetadata.ColumnNames.ComeCotasEntreRegatesConversao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CarteiraCollection")]
	public partial class CarteiraCollection : esCarteiraCollection, IEnumerable<Carteira>
	{
		public CarteiraCollection()
		{

		}
		
		public static implicit operator List<Carteira>(CarteiraCollection coll)
		{
			List<Carteira> list = new List<Carteira>();
			
			foreach (Carteira emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CarteiraMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CarteiraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Carteira(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Carteira();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CarteiraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CarteiraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CarteiraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Carteira AddNew()
		{
			Carteira entity = base.AddNewEntity() as Carteira;
			
			return entity;
		}

		public Carteira FindByPrimaryKey(System.Int32 idCarteira)
		{
			return base.FindByPrimaryKey(idCarteira) as Carteira;
		}


		#region IEnumerable<Carteira> Members

		IEnumerator<Carteira> IEnumerable<Carteira>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Carteira;
			}
		}

		#endregion
		
		private CarteiraQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Carteira' table
	/// </summary>

	[Serializable]
	public partial class Carteira : esCarteira
	{
		public Carteira()
		{

		}
	
		public Carteira(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CarteiraMetadata.Meta();
			}
		}
		
		
		
		override protected esCarteiraQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CarteiraQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CarteiraQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CarteiraQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CarteiraQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CarteiraQuery query;
	}



	[Serializable]
	public partial class CarteiraQuery : esCarteiraQuery
	{
		public CarteiraQuery()
		{

		}		
		
		public CarteiraQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CarteiraMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CarteiraMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.IdCarteira, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.Nome, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.Nome;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.Apelido, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.Apelido;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TipoCota, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.TipoCota;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TipoCarteira, 4, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.TipoCarteira;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.StatusAtivo, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.StatusAtivo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.IdIndiceBenchmark, 6, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = CarteiraMetadata.PropertyNames.IdIndiceBenchmark;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CasasDecimaisCota, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.CasasDecimaisCota;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CasasDecimaisQuantidade, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.CasasDecimaisQuantidade;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TruncaCota, 9, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.TruncaCota;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TruncaQuantidade, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.TruncaQuantidade;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TruncaFinanceiro, 11, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.TruncaFinanceiro;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CotaInicial, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CarteiraMetadata.PropertyNames.CotaInicial;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ValorMinimoAplicacao, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CarteiraMetadata.PropertyNames.ValorMinimoAplicacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ValorMinimoResgate, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CarteiraMetadata.PropertyNames.ValorMinimoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ValorMinimoSaldo, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CarteiraMetadata.PropertyNames.ValorMinimoSaldo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ValorMaximoAplicacao, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CarteiraMetadata.PropertyNames.ValorMaximoAplicacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ValorMinimoInicial, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CarteiraMetadata.PropertyNames.ValorMinimoInicial;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TipoRentabilidade, 18, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.TipoRentabilidade;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TipoCusto, 19, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.TipoCusto;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.DiasCotizacaoAplicacao, 20, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.DiasCotizacaoAplicacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.DiasCotizacaoResgate, 21, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.DiasCotizacaoResgate;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.DiasLiquidacaoAplicacao, 22, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.DiasLiquidacaoAplicacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.DiasLiquidacaoResgate, 23, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.DiasLiquidacaoResgate;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TipoTributacao, 24, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.TipoTributacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CalculaIOF, 25, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.CalculaIOF;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.DiasAniversario, 26, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = CarteiraMetadata.PropertyNames.DiasAniversario;	
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TipoAniversario, 27, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.TipoAniversario;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.HorarioInicio, 28, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CarteiraMetadata.PropertyNames.HorarioInicio;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.HorarioFim, 29, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CarteiraMetadata.PropertyNames.HorarioFim;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.HorarioLimiteCotizacao, 30, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CarteiraMetadata.PropertyNames.HorarioLimiteCotizacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.IdCategoria, 31, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.IdCategoria;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.IdSubCategoria, 32, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.IdSubCategoria;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.IdAgenteAdministrador, 33, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.IdAgenteAdministrador;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.IdAgenteGestor, 34, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.IdAgenteGestor;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CobraTaxaFiscalizacaoCVM, 35, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.CobraTaxaFiscalizacaoCVM;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TipoCVM, 36, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.TipoCVM;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.DataInicioCota, 37, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CarteiraMetadata.PropertyNames.DataInicioCota;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CalculaEnquadra, 38, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.CalculaEnquadra;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ContagemDiasConversaoResgate, 39, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.ContagemDiasConversaoResgate;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ProjecaoIRResgate, 40, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.ProjecaoIRResgate;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.PublicoAlvo, 41, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.PublicoAlvo;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.Objetivo, 42, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.Objetivo;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.IdAgenteCustodiante, 43, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.IdAgenteCustodiante;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TextoLivre1, 44, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.TextoLivre1;
			c.CharacterMaxLength = 1000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TextoLivre2, 45, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.TextoLivre2;
			c.CharacterMaxLength = 1000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TextoLivre3, 46, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.TextoLivre3;
			c.CharacterMaxLength = 1000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TextoLivre4, 47, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.TextoLivre4;
			c.CharacterMaxLength = 1000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TipoVisaoFundo, 48, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.TipoVisaoFundo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.DistribuicaoDividendo, 49, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.DistribuicaoDividendo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.IdEstrategia, 50, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.IdEstrategia;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CodigoAnbid, 51, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.CodigoAnbid;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ContagemPrazoIOF, 52, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.ContagemPrazoIOF;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.PrioridadeOperacao, 53, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.PrioridadeOperacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CodigoCDA, 54, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.CodigoCDA;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CompensacaoPrejuizo, 55, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.CompensacaoPrejuizo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.DiasConfidencialidade, 56, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.DiasConfidencialidade;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CodigoFDIC, 57, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.CodigoFDIC;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TipoCalculoRetorno, 58, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.TipoCalculoRetorno;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.IdGrauRisco, 59, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.IdGrauRisco;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CodigoIsin, 60, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.CodigoIsin;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CodigoCetip, 61, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.CodigoCetip;
			c.CharacterMaxLength = 14;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CodigoBloomberg, 62, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.CodigoBloomberg;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.HorarioInicioResgate, 63, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CarteiraMetadata.PropertyNames.HorarioInicioResgate;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.HorarioFimResgate, 64, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CarteiraMetadata.PropertyNames.HorarioFimResgate;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.IdGrupoEconomico, 65, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.IdGrupoEconomico;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.FundoExclusivo, 66, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.FundoExclusivo;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.OrdemRelatorio, 67, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.OrdemRelatorio;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.LiberaAplicacao, 68, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.LiberaAplicacao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('S')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.LiberaResgate, 69, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.LiberaResgate;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('S')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.PerfilRisco, 70, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.PerfilRisco;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CalculaMTM, 71, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.CalculaMTM;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CalculaPrazoMedio, 72, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.CalculaPrazoMedio;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.DiasAposResgateIR, 73, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.DiasAposResgateIR;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ProjecaoIRComeCotas, 74, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.ProjecaoIRComeCotas;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"('2')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.DiasAposComeCotasIR, 75, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.DiasAposComeCotasIR;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.RealizaCompensacaoDePrejuizo, 76, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.RealizaCompensacaoDePrejuizo;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.BuscaCotaAnterior, 77, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.BuscaCotaAnterior;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.NumeroDiasBuscaCota, 78, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.NumeroDiasBuscaCota;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.BandaVariacao, 79, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CarteiraMetadata.PropertyNames.BandaVariacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0.00))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ExcecaoRegraTxAdm, 80, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.ExcecaoRegraTxAdm;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.RebateImpactaPL, 81, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.RebateImpactaPL;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TipoFundo, 82, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.TipoFundo;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((1))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.Fie, 83, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.Fie;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.DataInicioContIOF, 84, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = CarteiraMetadata.PropertyNames.DataInicioContIOF;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.DataFimContIOF, 85, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = CarteiraMetadata.PropertyNames.DataFimContIOF;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ContaPrzIOFVirtual, 86, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = CarteiraMetadata.PropertyNames.ContaPrzIOFVirtual;	
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ProjecaoIOFResgate, 87, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = CarteiraMetadata.PropertyNames.ProjecaoIOFResgate;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"('2')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.DiasAposResgateIOF, 88, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.DiasAposResgateIOF;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CodigoConsolidacaoExterno, 89, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.CodigoConsolidacaoExterno;
			c.CharacterMaxLength = 60;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CodigoBDS, 90, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.CodigoBDS;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CodigoSTI, 91, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.CodigoSTI;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.TipoVisualizacaoResgCotista, 92, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.TipoVisualizacaoResgCotista;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('C')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ApenasInvestidorProfissional, 93, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.ApenasInvestidorProfissional;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ApenasInvestidorQualificado, 94, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.ApenasInvestidorQualificado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.RealizaOfertaSubscricao, 95, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.RealizaOfertaSubscricao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.Rendimento, 96, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.Rendimento;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((1))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.IdGrupoPerfilMTM, 97, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.IdGrupoPerfilMTM;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ProcAutomatico, 98, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.ProcAutomatico;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ExplodeCotasDeFundos, 99, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.ExplodeCotasDeFundos;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ControladoriaAtivo, 100, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.ControladoriaAtivo;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ControladoriaPassivo, 101, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.ControladoriaPassivo;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.MesmoConglomerado, 102, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.MesmoConglomerado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.CategoriaAnbima, 103, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.CategoriaAnbima;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.Contratante, 104, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraMetadata.PropertyNames.Contratante;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.Corretora, 105, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.Corretora;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ExportaGalgo, 106, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.ExportaGalgo;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.PossuiResgateAutomatico, 107, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.PossuiResgateAutomatico;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.InfluenciaGestorLocalCvm, 108, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.InfluenciaGestorLocalCvm;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.InvestimentoColetivoCvm, 109, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.InvestimentoColetivoCvm;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraMetadata.ColumnNames.ComeCotasEntreRegatesConversao, 110, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraMetadata.PropertyNames.ComeCotasEntreRegatesConversao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('S')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CarteiraMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string Nome = "Nome";
			 public const string Apelido = "Apelido";
			 public const string TipoCota = "TipoCota";
			 public const string TipoCarteira = "TipoCarteira";
			 public const string StatusAtivo = "StatusAtivo";
			 public const string IdIndiceBenchmark = "IdIndiceBenchmark";
			 public const string CasasDecimaisCota = "CasasDecimaisCota";
			 public const string CasasDecimaisQuantidade = "CasasDecimaisQuantidade";
			 public const string TruncaCota = "TruncaCota";
			 public const string TruncaQuantidade = "TruncaQuantidade";
			 public const string TruncaFinanceiro = "TruncaFinanceiro";
			 public const string CotaInicial = "CotaInicial";
			 public const string ValorMinimoAplicacao = "ValorMinimoAplicacao";
			 public const string ValorMinimoResgate = "ValorMinimoResgate";
			 public const string ValorMinimoSaldo = "ValorMinimoSaldo";
			 public const string ValorMaximoAplicacao = "ValorMaximoAplicacao";
			 public const string ValorMinimoInicial = "ValorMinimoInicial";
			 public const string TipoRentabilidade = "TipoRentabilidade";
			 public const string TipoCusto = "TipoCusto";
			 public const string DiasCotizacaoAplicacao = "DiasCotizacaoAplicacao";
			 public const string DiasCotizacaoResgate = "DiasCotizacaoResgate";
			 public const string DiasLiquidacaoAplicacao = "DiasLiquidacaoAplicacao";
			 public const string DiasLiquidacaoResgate = "DiasLiquidacaoResgate";
			 public const string TipoTributacao = "TipoTributacao";
			 public const string CalculaIOF = "CalculaIOF";
			 public const string DiasAniversario = "DiasAniversario";
			 public const string TipoAniversario = "TipoAniversario";
			 public const string HorarioInicio = "HorarioInicio";
			 public const string HorarioFim = "HorarioFim";
			 public const string HorarioLimiteCotizacao = "HorarioLimiteCotizacao";
			 public const string IdCategoria = "IdCategoria";
			 public const string IdSubCategoria = "IdSubCategoria";
			 public const string IdAgenteAdministrador = "IdAgenteAdministrador";
			 public const string IdAgenteGestor = "IdAgenteGestor";
			 public const string CobraTaxaFiscalizacaoCVM = "CobraTaxaFiscalizacaoCVM";
			 public const string TipoCVM = "TipoCVM";
			 public const string DataInicioCota = "DataInicioCota";
			 public const string CalculaEnquadra = "CalculaEnquadra";
			 public const string ContagemDiasConversaoResgate = "ContagemDiasConversaoResgate";
			 public const string ProjecaoIRResgate = "ProjecaoIRResgate";
			 public const string PublicoAlvo = "PublicoAlvo";
			 public const string Objetivo = "Objetivo";
			 public const string IdAgenteCustodiante = "IdAgenteCustodiante";
			 public const string TextoLivre1 = "TextoLivre1";
			 public const string TextoLivre2 = "TextoLivre2";
			 public const string TextoLivre3 = "TextoLivre3";
			 public const string TextoLivre4 = "TextoLivre4";
			 public const string TipoVisaoFundo = "TipoVisaoFundo";
			 public const string DistribuicaoDividendo = "DistribuicaoDividendo";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string CodigoAnbid = "CodigoAnbid";
			 public const string ContagemPrazoIOF = "ContagemPrazoIOF";
			 public const string PrioridadeOperacao = "PrioridadeOperacao";
			 public const string CodigoCDA = "CodigoCDA";
			 public const string CompensacaoPrejuizo = "CompensacaoPrejuizo";
			 public const string DiasConfidencialidade = "DiasConfidencialidade";
			 public const string CodigoFDIC = "CodigoFDIC";
			 public const string TipoCalculoRetorno = "TipoCalculoRetorno";
			 public const string IdGrauRisco = "IdGrauRisco";
			 public const string CodigoIsin = "CodigoIsin";
			 public const string CodigoCetip = "CodigoCetip";
			 public const string CodigoBloomberg = "CodigoBloomberg";
			 public const string HorarioInicioResgate = "HorarioInicioResgate";
			 public const string HorarioFimResgate = "HorarioFimResgate";
			 public const string IdGrupoEconomico = "IdGrupoEconomico";
			 public const string FundoExclusivo = "FundoExclusivo";
			 public const string OrdemRelatorio = "OrdemRelatorio";
			 public const string LiberaAplicacao = "LiberaAplicacao";
			 public const string LiberaResgate = "LiberaResgate";
			 public const string PerfilRisco = "PerfilRisco";
			 public const string CalculaMTM = "CalculaMTM";
			 public const string CalculaPrazoMedio = "CalculaPrazoMedio";
			 public const string DiasAposResgateIR = "DiasAposResgateIR";
			 public const string ProjecaoIRComeCotas = "ProjecaoIRComeCotas";
			 public const string DiasAposComeCotasIR = "DiasAposComeCotasIR";
			 public const string RealizaCompensacaoDePrejuizo = "RealizaCompensacaoDePrejuizo";
			 public const string BuscaCotaAnterior = "BuscaCotaAnterior";
			 public const string NumeroDiasBuscaCota = "NumeroDiasBuscaCota";
			 public const string BandaVariacao = "BandaVariacao";
			 public const string ExcecaoRegraTxAdm = "ExcecaoRegraTxAdm";
			 public const string RebateImpactaPL = "RebateImpactaPL";
			 public const string TipoFundo = "TipoFundo";
			 public const string Fie = "FIE";
			 public const string DataInicioContIOF = "DataInicioContIOF";
			 public const string DataFimContIOF = "DataFimContIOF";
			 public const string ContaPrzIOFVirtual = "ContaPrzIOFVirtual";
			 public const string ProjecaoIOFResgate = "ProjecaoIOFResgate";
			 public const string DiasAposResgateIOF = "DiasAposResgateIOF";
			 public const string CodigoConsolidacaoExterno = "CodigoConsolidacaoExterno";
			 public const string CodigoBDS = "CodigoBDS";
			 public const string CodigoSTI = "CodigoSTI";
			 public const string TipoVisualizacaoResgCotista = "TipoVisualizacaoResgCotista";
			 public const string ApenasInvestidorProfissional = "ApenasInvestidorProfissional";
			 public const string ApenasInvestidorQualificado = "ApenasInvestidorQualificado";
			 public const string RealizaOfertaSubscricao = "RealizaOfertaSubscricao";
			 public const string Rendimento = "Rendimento";
			 public const string IdGrupoPerfilMTM = "IdGrupoPerfilMTM";
			 public const string ProcAutomatico = "ProcAutomatico";
			 public const string ExplodeCotasDeFundos = "ExplodeCotasDeFundos";
			 public const string ControladoriaAtivo = "ControladoriaAtivo";
			 public const string ControladoriaPassivo = "ControladoriaPassivo";
			 public const string MesmoConglomerado = "MesmoConglomerado";
			 public const string CategoriaAnbima = "CategoriaAnbima";
			 public const string Contratante = "Contratante";
			 public const string Corretora = "Corretora";
			 public const string ExportaGalgo = "ExportaGalgo";
			 public const string PossuiResgateAutomatico = "PossuiResgateAutomatico";
			 public const string InfluenciaGestorLocalCvm = "InfluenciaGestorLocalCvm";
			 public const string InvestimentoColetivoCvm = "InvestimentoColetivoCvm";
			 public const string ComeCotasEntreRegatesConversao = "ComeCotasEntreRegatesConversao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string Nome = "Nome";
			 public const string Apelido = "Apelido";
			 public const string TipoCota = "TipoCota";
			 public const string TipoCarteira = "TipoCarteira";
			 public const string StatusAtivo = "StatusAtivo";
			 public const string IdIndiceBenchmark = "IdIndiceBenchmark";
			 public const string CasasDecimaisCota = "CasasDecimaisCota";
			 public const string CasasDecimaisQuantidade = "CasasDecimaisQuantidade";
			 public const string TruncaCota = "TruncaCota";
			 public const string TruncaQuantidade = "TruncaQuantidade";
			 public const string TruncaFinanceiro = "TruncaFinanceiro";
			 public const string CotaInicial = "CotaInicial";
			 public const string ValorMinimoAplicacao = "ValorMinimoAplicacao";
			 public const string ValorMinimoResgate = "ValorMinimoResgate";
			 public const string ValorMinimoSaldo = "ValorMinimoSaldo";
			 public const string ValorMaximoAplicacao = "ValorMaximoAplicacao";
			 public const string ValorMinimoInicial = "ValorMinimoInicial";
			 public const string TipoRentabilidade = "TipoRentabilidade";
			 public const string TipoCusto = "TipoCusto";
			 public const string DiasCotizacaoAplicacao = "DiasCotizacaoAplicacao";
			 public const string DiasCotizacaoResgate = "DiasCotizacaoResgate";
			 public const string DiasLiquidacaoAplicacao = "DiasLiquidacaoAplicacao";
			 public const string DiasLiquidacaoResgate = "DiasLiquidacaoResgate";
			 public const string TipoTributacao = "TipoTributacao";
			 public const string CalculaIOF = "CalculaIOF";
			 public const string DiasAniversario = "DiasAniversario";
			 public const string TipoAniversario = "TipoAniversario";
			 public const string HorarioInicio = "HorarioInicio";
			 public const string HorarioFim = "HorarioFim";
			 public const string HorarioLimiteCotizacao = "HorarioLimiteCotizacao";
			 public const string IdCategoria = "IdCategoria";
			 public const string IdSubCategoria = "IdSubCategoria";
			 public const string IdAgenteAdministrador = "IdAgenteAdministrador";
			 public const string IdAgenteGestor = "IdAgenteGestor";
			 public const string CobraTaxaFiscalizacaoCVM = "CobraTaxaFiscalizacaoCVM";
			 public const string TipoCVM = "TipoCVM";
			 public const string DataInicioCota = "DataInicioCota";
			 public const string CalculaEnquadra = "CalculaEnquadra";
			 public const string ContagemDiasConversaoResgate = "ContagemDiasConversaoResgate";
			 public const string ProjecaoIRResgate = "ProjecaoIRResgate";
			 public const string PublicoAlvo = "PublicoAlvo";
			 public const string Objetivo = "Objetivo";
			 public const string IdAgenteCustodiante = "IdAgenteCustodiante";
			 public const string TextoLivre1 = "TextoLivre1";
			 public const string TextoLivre2 = "TextoLivre2";
			 public const string TextoLivre3 = "TextoLivre3";
			 public const string TextoLivre4 = "TextoLivre4";
			 public const string TipoVisaoFundo = "TipoVisaoFundo";
			 public const string DistribuicaoDividendo = "DistribuicaoDividendo";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string CodigoAnbid = "CodigoAnbid";
			 public const string ContagemPrazoIOF = "ContagemPrazoIOF";
			 public const string PrioridadeOperacao = "PrioridadeOperacao";
			 public const string CodigoCDA = "CodigoCDA";
			 public const string CompensacaoPrejuizo = "CompensacaoPrejuizo";
			 public const string DiasConfidencialidade = "DiasConfidencialidade";
			 public const string CodigoFDIC = "CodigoFDIC";
			 public const string TipoCalculoRetorno = "TipoCalculoRetorno";
			 public const string IdGrauRisco = "IdGrauRisco";
			 public const string CodigoIsin = "CodigoIsin";
			 public const string CodigoCetip = "CodigoCetip";
			 public const string CodigoBloomberg = "CodigoBloomberg";
			 public const string HorarioInicioResgate = "HorarioInicioResgate";
			 public const string HorarioFimResgate = "HorarioFimResgate";
			 public const string IdGrupoEconomico = "IdGrupoEconomico";
			 public const string FundoExclusivo = "FundoExclusivo";
			 public const string OrdemRelatorio = "OrdemRelatorio";
			 public const string LiberaAplicacao = "LiberaAplicacao";
			 public const string LiberaResgate = "LiberaResgate";
			 public const string PerfilRisco = "PerfilRisco";
			 public const string CalculaMTM = "CalculaMTM";
			 public const string CalculaPrazoMedio = "CalculaPrazoMedio";
			 public const string DiasAposResgateIR = "DiasAposResgateIR";
			 public const string ProjecaoIRComeCotas = "ProjecaoIRComeCotas";
			 public const string DiasAposComeCotasIR = "DiasAposComeCotasIR";
			 public const string RealizaCompensacaoDePrejuizo = "RealizaCompensacaoDePrejuizo";
			 public const string BuscaCotaAnterior = "BuscaCotaAnterior";
			 public const string NumeroDiasBuscaCota = "NumeroDiasBuscaCota";
			 public const string BandaVariacao = "BandaVariacao";
			 public const string ExcecaoRegraTxAdm = "ExcecaoRegraTxAdm";
			 public const string RebateImpactaPL = "RebateImpactaPL";
			 public const string TipoFundo = "TipoFundo";
			 public const string Fie = "Fie";
			 public const string DataInicioContIOF = "DataInicioContIOF";
			 public const string DataFimContIOF = "DataFimContIOF";
			 public const string ContaPrzIOFVirtual = "ContaPrzIOFVirtual";
			 public const string ProjecaoIOFResgate = "ProjecaoIOFResgate";
			 public const string DiasAposResgateIOF = "DiasAposResgateIOF";
			 public const string CodigoConsolidacaoExterno = "CodigoConsolidacaoExterno";
			 public const string CodigoBDS = "CodigoBDS";
			 public const string CodigoSTI = "CodigoSTI";
			 public const string TipoVisualizacaoResgCotista = "TipoVisualizacaoResgCotista";
			 public const string ApenasInvestidorProfissional = "ApenasInvestidorProfissional";
			 public const string ApenasInvestidorQualificado = "ApenasInvestidorQualificado";
			 public const string RealizaOfertaSubscricao = "RealizaOfertaSubscricao";
			 public const string Rendimento = "Rendimento";
			 public const string IdGrupoPerfilMTM = "IdGrupoPerfilMTM";
			 public const string ProcAutomatico = "ProcAutomatico";
			 public const string ExplodeCotasDeFundos = "ExplodeCotasDeFundos";
			 public const string ControladoriaAtivo = "ControladoriaAtivo";
			 public const string ControladoriaPassivo = "ControladoriaPassivo";
			 public const string MesmoConglomerado = "MesmoConglomerado";
			 public const string CategoriaAnbima = "CategoriaAnbima";
			 public const string Contratante = "Contratante";
			 public const string Corretora = "Corretora";
			 public const string ExportaGalgo = "ExportaGalgo";
			 public const string PossuiResgateAutomatico = "PossuiResgateAutomatico";
			 public const string InfluenciaGestorLocalCvm = "InfluenciaGestorLocalCvm";
			 public const string InvestimentoColetivoCvm = "InvestimentoColetivoCvm";
			 public const string ComeCotasEntreRegatesConversao = "ComeCotasEntreRegatesConversao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CarteiraMetadata))
			{
				if(CarteiraMetadata.mapDelegates == null)
				{
					CarteiraMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CarteiraMetadata.meta == null)
				{
					CarteiraMetadata.meta = new CarteiraMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Nome", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Apelido", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoCota", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoCarteira", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("StatusAtivo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdIndiceBenchmark", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("CasasDecimaisCota", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CasasDecimaisQuantidade", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TruncaCota", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TruncaQuantidade", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TruncaFinanceiro", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("CotaInicial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorMinimoAplicacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorMinimoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorMinimoSaldo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorMaximoAplicacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorMinimoInicial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoRentabilidade", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoCusto", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DiasCotizacaoAplicacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DiasCotizacaoResgate", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DiasLiquidacaoAplicacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DiasLiquidacaoResgate", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoTributacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CalculaIOF", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DiasAniversario", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TipoAniversario", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("HorarioInicio", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("HorarioFim", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("HorarioLimiteCotizacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCategoria", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdSubCategoria", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteAdministrador", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteGestor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CobraTaxaFiscalizacaoCVM", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TipoCVM", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataInicioCota", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CalculaEnquadra", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ContagemDiasConversaoResgate", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ProjecaoIRResgate", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PublicoAlvo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Objetivo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAgenteCustodiante", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TextoLivre1", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TextoLivre2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TextoLivre3", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TextoLivre4", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoVisaoFundo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DistribuicaoDividendo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdEstrategia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoAnbid", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ContagemPrazoIOF", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PrioridadeOperacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CodigoCDA", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CompensacaoPrejuizo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DiasConfidencialidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoFDIC", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoCalculoRetorno", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdGrauRisco", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoIsin", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoCetip", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoBloomberg", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("HorarioInicioResgate", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("HorarioFimResgate", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdGrupoEconomico", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FundoExclusivo", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("OrdemRelatorio", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("LiberaAplicacao", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("LiberaResgate", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("PerfilRisco", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CalculaMTM", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CalculaPrazoMedio", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DiasAposResgateIR", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ProjecaoIRComeCotas", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DiasAposComeCotasIR", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RealizaCompensacaoDePrejuizo", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("BuscaCotaAnterior", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("NumeroDiasBuscaCota", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("BandaVariacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ExcecaoRegraTxAdm", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("RebateImpactaPL", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TipoFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FIE", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DataInicioContIOF", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("DataFimContIOF", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ContaPrzIOFVirtual", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("ProjecaoIOFResgate", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DiasAposResgateIOF", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoConsolidacaoExterno", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoBDS", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoSTI", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoVisualizacaoResgCotista", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ApenasInvestidorProfissional", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ApenasInvestidorQualificado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("RealizaOfertaSubscricao", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Rendimento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdGrupoPerfilMTM", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ProcAutomatico", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ExplodeCotasDeFundos", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ControladoriaAtivo", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ControladoriaPassivo", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("MesmoConglomerado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("CategoriaAnbima", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Contratante", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Corretora", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ExportaGalgo", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("PossuiResgateAutomatico", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("InfluenciaGestorLocalCvm", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("InvestimentoColetivoCvm", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ComeCotasEntreRegatesConversao", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "Carteira";
				meta.Destination = "Carteira";
				
				meta.spInsert = "proc_CarteiraInsert";				
				meta.spUpdate = "proc_CarteiraUpdate";		
				meta.spDelete = "proc_CarteiraDelete";
				meta.spLoadAll = "proc_CarteiraLoadAll";
				meta.spLoadByPrimaryKey = "proc_CarteiraLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CarteiraMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
