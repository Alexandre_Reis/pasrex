/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 01/02/2016 19:02:04
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esPreProcessamentoHistoricoCotaCollection : esEntityCollection
	{
		public esPreProcessamentoHistoricoCotaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PreProcessamentoHistoricoCotaCollection";
		}

		#region Query Logic
		protected void InitQuery(esPreProcessamentoHistoricoCotaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPreProcessamentoHistoricoCotaQuery);
		}
		#endregion
		
		virtual public PreProcessamentoHistoricoCota DetachEntity(PreProcessamentoHistoricoCota entity)
		{
			return base.DetachEntity(entity) as PreProcessamentoHistoricoCota;
		}
		
		virtual public PreProcessamentoHistoricoCota AttachEntity(PreProcessamentoHistoricoCota entity)
		{
			return base.AttachEntity(entity) as PreProcessamentoHistoricoCota;
		}
		
		virtual public void Combine(PreProcessamentoHistoricoCotaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PreProcessamentoHistoricoCota this[int index]
		{
			get
			{
				return base[index] as PreProcessamentoHistoricoCota;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PreProcessamentoHistoricoCota);
		}
	}



	[Serializable]
	abstract public class esPreProcessamentoHistoricoCota : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPreProcessamentoHistoricoCotaQuery GetDynamicQuery()
		{
			return null;
		}

		public esPreProcessamentoHistoricoCota()
		{

		}

		public esPreProcessamentoHistoricoCota(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPreProcessamentoHistoricoCota)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPreProcessamentoHistoricoCota);
			else
				return LoadByPrimaryKeyStoredProcedure(idPreProcessamentoHistoricoCota);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPreProcessamentoHistoricoCota)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPreProcessamentoHistoricoCota);
			else
				return LoadByPrimaryKeyStoredProcedure(idPreProcessamentoHistoricoCota);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPreProcessamentoHistoricoCota)
		{
			esPreProcessamentoHistoricoCotaQuery query = this.GetDynamicQuery();
			query.Where(query.IdPreProcessamentoHistoricoCota == idPreProcessamentoHistoricoCota);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPreProcessamentoHistoricoCota)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPreProcessamentoHistoricoCota",idPreProcessamentoHistoricoCota);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPreProcessamentoHistoricoCota": this.str.IdPreProcessamentoHistoricoCota = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Chave": this.str.Chave = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPreProcessamentoHistoricoCota":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPreProcessamentoHistoricoCota = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Chave":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Chave = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PreProcessamentoHistoricoCota.IdPreProcessamentoHistoricoCota
		/// </summary>
		virtual public System.Int32? IdPreProcessamentoHistoricoCota
		{
			get
			{
				return base.GetSystemInt32(PreProcessamentoHistoricoCotaMetadata.ColumnNames.IdPreProcessamentoHistoricoCota);
			}
			
			set
			{
				base.SetSystemInt32(PreProcessamentoHistoricoCotaMetadata.ColumnNames.IdPreProcessamentoHistoricoCota, value);
			}
		}
		
		/// <summary>
		/// Maps to PreProcessamentoHistoricoCota.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(PreProcessamentoHistoricoCotaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(PreProcessamentoHistoricoCotaMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to PreProcessamentoHistoricoCota.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(PreProcessamentoHistoricoCotaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(PreProcessamentoHistoricoCotaMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to PreProcessamentoHistoricoCota.Chave
		/// </summary>
		virtual public System.Int32? Chave
		{
			get
			{
				return base.GetSystemInt32(PreProcessamentoHistoricoCotaMetadata.ColumnNames.Chave);
			}
			
			set
			{
				base.SetSystemInt32(PreProcessamentoHistoricoCotaMetadata.ColumnNames.Chave, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPreProcessamentoHistoricoCota entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPreProcessamentoHistoricoCota
			{
				get
				{
					System.Int32? data = entity.IdPreProcessamentoHistoricoCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPreProcessamentoHistoricoCota = null;
					else entity.IdPreProcessamentoHistoricoCota = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Chave
			{
				get
				{
					System.Int32? data = entity.Chave;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Chave = null;
					else entity.Chave = Convert.ToInt32(value);
				}
			}
			

			private esPreProcessamentoHistoricoCota entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPreProcessamentoHistoricoCotaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPreProcessamentoHistoricoCota can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PreProcessamentoHistoricoCota : esPreProcessamentoHistoricoCota
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPreProcessamentoHistoricoCotaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PreProcessamentoHistoricoCotaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPreProcessamentoHistoricoCota
		{
			get
			{
				return new esQueryItem(this, PreProcessamentoHistoricoCotaMetadata.ColumnNames.IdPreProcessamentoHistoricoCota, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, PreProcessamentoHistoricoCotaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, PreProcessamentoHistoricoCotaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Chave
		{
			get
			{
				return new esQueryItem(this, PreProcessamentoHistoricoCotaMetadata.ColumnNames.Chave, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PreProcessamentoHistoricoCotaCollection")]
	public partial class PreProcessamentoHistoricoCotaCollection : esPreProcessamentoHistoricoCotaCollection, IEnumerable<PreProcessamentoHistoricoCota>
	{
		public PreProcessamentoHistoricoCotaCollection()
		{

		}
		
		public static implicit operator List<PreProcessamentoHistoricoCota>(PreProcessamentoHistoricoCotaCollection coll)
		{
			List<PreProcessamentoHistoricoCota> list = new List<PreProcessamentoHistoricoCota>();
			
			foreach (PreProcessamentoHistoricoCota emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PreProcessamentoHistoricoCotaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PreProcessamentoHistoricoCotaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PreProcessamentoHistoricoCota(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PreProcessamentoHistoricoCota();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PreProcessamentoHistoricoCotaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PreProcessamentoHistoricoCotaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PreProcessamentoHistoricoCotaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PreProcessamentoHistoricoCota AddNew()
		{
			PreProcessamentoHistoricoCota entity = base.AddNewEntity() as PreProcessamentoHistoricoCota;
			
			return entity;
		}

		public PreProcessamentoHistoricoCota FindByPrimaryKey(System.Int32 idPreProcessamentoHistoricoCota)
		{
			return base.FindByPrimaryKey(idPreProcessamentoHistoricoCota) as PreProcessamentoHistoricoCota;
		}


		#region IEnumerable<PreProcessamentoHistoricoCota> Members

		IEnumerator<PreProcessamentoHistoricoCota> IEnumerable<PreProcessamentoHistoricoCota>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PreProcessamentoHistoricoCota;
			}
		}

		#endregion
		
		private PreProcessamentoHistoricoCotaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PreProcessamentoHistoricoCota' table
	/// </summary>

	[Serializable]
	public partial class PreProcessamentoHistoricoCota : esPreProcessamentoHistoricoCota
	{
		public PreProcessamentoHistoricoCota()
		{

		}
	
		public PreProcessamentoHistoricoCota(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PreProcessamentoHistoricoCotaMetadata.Meta();
			}
		}
		
		
		
		override protected esPreProcessamentoHistoricoCotaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PreProcessamentoHistoricoCotaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PreProcessamentoHistoricoCotaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PreProcessamentoHistoricoCotaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PreProcessamentoHistoricoCotaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PreProcessamentoHistoricoCotaQuery query;
	}



	[Serializable]
	public partial class PreProcessamentoHistoricoCotaQuery : esPreProcessamentoHistoricoCotaQuery
	{
		public PreProcessamentoHistoricoCotaQuery()
		{

		}		
		
		public PreProcessamentoHistoricoCotaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PreProcessamentoHistoricoCotaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PreProcessamentoHistoricoCotaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PreProcessamentoHistoricoCotaMetadata.ColumnNames.IdPreProcessamentoHistoricoCota, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PreProcessamentoHistoricoCotaMetadata.PropertyNames.IdPreProcessamentoHistoricoCota;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PreProcessamentoHistoricoCotaMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PreProcessamentoHistoricoCotaMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PreProcessamentoHistoricoCotaMetadata.ColumnNames.Data, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PreProcessamentoHistoricoCotaMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PreProcessamentoHistoricoCotaMetadata.ColumnNames.Chave, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PreProcessamentoHistoricoCotaMetadata.PropertyNames.Chave;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PreProcessamentoHistoricoCotaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPreProcessamentoHistoricoCota = "IdPreProcessamentoHistoricoCota";
			 public const string IdCarteira = "IdCarteira";
			 public const string Data = "Data";
			 public const string Chave = "Chave";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPreProcessamentoHistoricoCota = "IdPreProcessamentoHistoricoCota";
			 public const string IdCarteira = "IdCarteira";
			 public const string Data = "Data";
			 public const string Chave = "Chave";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PreProcessamentoHistoricoCotaMetadata))
			{
				if(PreProcessamentoHistoricoCotaMetadata.mapDelegates == null)
				{
					PreProcessamentoHistoricoCotaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PreProcessamentoHistoricoCotaMetadata.meta == null)
				{
					PreProcessamentoHistoricoCotaMetadata.meta = new PreProcessamentoHistoricoCotaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPreProcessamentoHistoricoCota", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Chave", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "PreProcessamentoHistoricoCota";
				meta.Destination = "PreProcessamentoHistoricoCota";
				
				meta.spInsert = "proc_PreProcessamentoHistoricoCotaInsert";				
				meta.spUpdate = "proc_PreProcessamentoHistoricoCotaUpdate";		
				meta.spDelete = "proc_PreProcessamentoHistoricoCotaDelete";
				meta.spLoadAll = "proc_PreProcessamentoHistoricoCotaLoadAll";
				meta.spLoadByPrimaryKey = "proc_PreProcessamentoHistoricoCotaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PreProcessamentoHistoricoCotaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
