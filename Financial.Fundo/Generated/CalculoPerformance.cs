/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:10 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		









		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esCalculoPerformanceCollection : esEntityCollection
	{
		public esCalculoPerformanceCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CalculoPerformanceCollection";
		}

		#region Query Logic
		protected void InitQuery(esCalculoPerformanceQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCalculoPerformanceQuery);
		}
		#endregion
		
		virtual public CalculoPerformance DetachEntity(CalculoPerformance entity)
		{
			return base.DetachEntity(entity) as CalculoPerformance;
		}
		
		virtual public CalculoPerformance AttachEntity(CalculoPerformance entity)
		{
			return base.AttachEntity(entity) as CalculoPerformance;
		}
		
		virtual public void Combine(CalculoPerformanceCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CalculoPerformance this[int index]
		{
			get
			{
				return base[index] as CalculoPerformance;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CalculoPerformance);
		}
	}



	[Serializable]
	abstract public class esCalculoPerformance : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCalculoPerformanceQuery GetDynamicQuery()
		{
			return null;
		}

		public esCalculoPerformance()
		{

		}

		public esCalculoPerformance(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabela)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTabela)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCalculoPerformanceQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTabela == idTabela);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabela)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabela)
		{
			esCalculoPerformanceQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabela == idTabela);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabela)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabela",idTabela);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "ValorAcumulado": this.str.ValorAcumulado = (string)value; break;							
						case "DataFimApropriacao": this.str.DataFimApropriacao = (string)value; break;							
						case "DataPagamento": this.str.DataPagamento = (string)value; break;							
						case "ValorCPMFAcumulado": this.str.ValorCPMFAcumulado = (string)value; break;							
						case "ValorDia": this.str.ValorDia = (string)value; break;
						case "CotaAtual": this.str.CotaAtual = (string)value; break;							
						case "CotaBase": this.str.CotaBase = (string)value; break;							
						case "CotaCorrigida": this.str.CotaCorrigida = (string)value; break;							
						case "FatorIndice": this.str.FatorIndice = (string)value; break;							
						case "FatorJuros": this.str.FatorJuros = (string)value; break;							
						case "QuantidadeCotas": this.str.QuantidadeCotas = (string)value; break;							
						case "Rendimento": this.str.Rendimento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "ValorAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAcumulado = (System.Decimal?)value;
							break;
						
						case "DataFimApropriacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFimApropriacao = (System.DateTime?)value;
							break;
						
						case "DataPagamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPagamento = (System.DateTime?)value;
							break;
						
						case "ValorCPMFAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMFAcumulado = (System.Decimal?)value;
							break;
						
						case "ValorDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorDia = (System.Decimal?)value;
							break;
							
						case "CotaAtual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaAtual = (System.Decimal?)value;
							break;
						
						case "CotaBase":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaBase = (System.Decimal?)value;
							break;
						
						case "CotaCorrigida":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaCorrigida = (System.Decimal?)value;
							break;
						
						case "FatorIndice":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.FatorIndice = (System.Decimal?)value;
							break;
						
						case "FatorJuros":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.FatorJuros = (System.Decimal?)value;
							break;
						
						case "QuantidadeCotas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeCotas = (System.Decimal?)value;
							break;
						
						case "Rendimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Rendimento = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CalculoPerformance.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(CalculoPerformanceMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				base.SetSystemInt32(CalculoPerformanceMetadata.ColumnNames.IdTabela, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoPerformance.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(CalculoPerformanceMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(CalculoPerformanceMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CalculoPerformance.ValorAcumulado
		/// </summary>
		virtual public System.Decimal? ValorAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.ValorAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.ValorAcumulado, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoPerformance.DataFimApropriacao
		/// </summary>
		virtual public System.DateTime? DataFimApropriacao
		{
			get
			{
				return base.GetSystemDateTime(CalculoPerformanceMetadata.ColumnNames.DataFimApropriacao);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoPerformanceMetadata.ColumnNames.DataFimApropriacao, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoPerformance.DataPagamento
		/// </summary>
		virtual public System.DateTime? DataPagamento
		{
			get
			{
				return base.GetSystemDateTime(CalculoPerformanceMetadata.ColumnNames.DataPagamento);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoPerformanceMetadata.ColumnNames.DataPagamento, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoPerformance.ValorCPMFAcumulado
		/// </summary>
		virtual public System.Decimal? ValorCPMFAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.ValorCPMFAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.ValorCPMFAcumulado, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoPerformance.ValorDia
		/// </summary>
		virtual public System.Decimal? ValorDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.ValorDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.ValorDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoPerformance.CotaAtual
		/// </summary>
		virtual public System.Decimal? CotaAtual
		{
			get
			{
				return base.GetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.CotaAtual);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.CotaAtual, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoPerformance.CotaBase
		/// </summary>
		virtual public System.Decimal? CotaBase
		{
			get
			{
				return base.GetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.CotaBase);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.CotaBase, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoPerformance.CotaCorrigida
		/// </summary>
		virtual public System.Decimal? CotaCorrigida
		{
			get
			{
				return base.GetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.CotaCorrigida);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.CotaCorrigida, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoPerformance.FatorIndice
		/// </summary>
		virtual public System.Decimal? FatorIndice
		{
			get
			{
				return base.GetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.FatorIndice);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.FatorIndice, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoPerformance.FatorJuros
		/// </summary>
		virtual public System.Decimal? FatorJuros
		{
			get
			{
				return base.GetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.FatorJuros);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.FatorJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoPerformance.QuantidadeCotas
		/// </summary>
		virtual public System.Decimal? QuantidadeCotas
		{
			get
			{
				return base.GetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.QuantidadeCotas);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.QuantidadeCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoPerformance.Rendimento
		/// </summary>
		virtual public System.Decimal? Rendimento
		{
			get
			{
				return base.GetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.Rendimento);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoPerformanceMetadata.ColumnNames.Rendimento, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCalculoPerformance entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorAcumulado
			{
				get
				{
					System.Decimal? data = entity.ValorAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAcumulado = null;
					else entity.ValorAcumulado = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataFimApropriacao
			{
				get
				{
					System.DateTime? data = entity.DataFimApropriacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFimApropriacao = null;
					else entity.DataFimApropriacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataPagamento
			{
				get
				{
					System.DateTime? data = entity.DataPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPagamento = null;
					else entity.DataPagamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorCPMFAcumulado
			{
				get
				{
					System.Decimal? data = entity.ValorCPMFAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMFAcumulado = null;
					else entity.ValorCPMFAcumulado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorDia
			{
				get
				{
					System.Decimal? data = entity.ValorDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDia = null;
					else entity.ValorDia = Convert.ToDecimal(value);
				}
			}
			
			public System.String CotaAtual
			{
				get
				{
					System.Decimal? data = entity.CotaAtual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaAtual = null;
					else entity.CotaAtual = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaBase
			{
				get
				{
					System.Decimal? data = entity.CotaBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaBase = null;
					else entity.CotaBase = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaCorrigida
			{
				get
				{
					System.Decimal? data = entity.CotaCorrigida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaCorrigida = null;
					else entity.CotaCorrigida = Convert.ToDecimal(value);
				}
			}
				
			public System.String FatorIndice
			{
				get
				{
					System.Decimal? data = entity.FatorIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FatorIndice = null;
					else entity.FatorIndice = Convert.ToDecimal(value);
				}
			}
				
			public System.String FatorJuros
			{
				get
				{
					System.Decimal? data = entity.FatorJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FatorJuros = null;
					else entity.FatorJuros = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeCotas
			{
				get
				{
					System.Decimal? data = entity.QuantidadeCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeCotas = null;
					else entity.QuantidadeCotas = Convert.ToDecimal(value);
				}
			}
				
			public System.String Rendimento
			{
				get
				{
					System.Decimal? data = entity.Rendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Rendimento = null;
					else entity.Rendimento = Convert.ToDecimal(value);
				}
			}
			

			private esCalculoPerformance entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCalculoPerformanceQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCalculoPerformance can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CalculoPerformance : esCalculoPerformance
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_CalculoPerformance_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		#region UpToTabelaTaxaPerformance - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - TabelaTaxaPerformance_CalculoPerformance_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaPerformance UpToTabelaTaxaPerformance
		{
			get
			{
				if(this._UpToTabelaTaxaPerformance == null
					&& IdTabela != null					)
				{
					this._UpToTabelaTaxaPerformance = new TabelaTaxaPerformance();
					this._UpToTabelaTaxaPerformance.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTabelaTaxaPerformance", this._UpToTabelaTaxaPerformance);
					this._UpToTabelaTaxaPerformance.Query.Where(this._UpToTabelaTaxaPerformance.Query.IdTabela == this.IdTabela);
					this._UpToTabelaTaxaPerformance.Query.Load();
				}

				return this._UpToTabelaTaxaPerformance;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToTabelaTaxaPerformance");

				if(value == null)
				{
					this._UpToTabelaTaxaPerformance = null;
				}
				else
				{
					this._UpToTabelaTaxaPerformance = value;
					this.SetPreSave("UpToTabelaTaxaPerformance", this._UpToTabelaTaxaPerformance);
				}
				
				
			} 
		}

		private TabelaTaxaPerformance _UpToTabelaTaxaPerformance;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTabelaTaxaPerformance != null)
			{
				this.IdTabela = this._UpToTabelaTaxaPerformance.IdTabela;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCalculoPerformanceQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CalculoPerformanceMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, CalculoPerformanceMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, CalculoPerformanceMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoPerformanceMetadata.ColumnNames.ValorAcumulado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataFimApropriacao
		{
			get
			{
				return new esQueryItem(this, CalculoPerformanceMetadata.ColumnNames.DataFimApropriacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataPagamento
		{
			get
			{
				return new esQueryItem(this, CalculoPerformanceMetadata.ColumnNames.DataPagamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorCPMFAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoPerformanceMetadata.ColumnNames.ValorCPMFAcumulado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorDia
		{
			get
			{
				return new esQueryItem(this, CalculoPerformanceMetadata.ColumnNames.ValorDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaAtual
		{
			get
			{
				return new esQueryItem(this, CalculoPerformanceMetadata.ColumnNames.CotaAtual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaBase
		{
			get
			{
				return new esQueryItem(this, CalculoPerformanceMetadata.ColumnNames.CotaBase, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaCorrigida
		{
			get
			{
				return new esQueryItem(this, CalculoPerformanceMetadata.ColumnNames.CotaCorrigida, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem FatorIndice
		{
			get
			{
				return new esQueryItem(this, CalculoPerformanceMetadata.ColumnNames.FatorIndice, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem FatorJuros
		{
			get
			{
				return new esQueryItem(this, CalculoPerformanceMetadata.ColumnNames.FatorJuros, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeCotas
		{
			get
			{
				return new esQueryItem(this, CalculoPerformanceMetadata.ColumnNames.QuantidadeCotas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Rendimento
		{
			get
			{
				return new esQueryItem(this, CalculoPerformanceMetadata.ColumnNames.Rendimento, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CalculoPerformanceCollection")]
	public partial class CalculoPerformanceCollection : esCalculoPerformanceCollection, IEnumerable<CalculoPerformance>
	{
		public CalculoPerformanceCollection()
		{

		}
		
		public static implicit operator List<CalculoPerformance>(CalculoPerformanceCollection coll)
		{
			List<CalculoPerformance> list = new List<CalculoPerformance>();
			
			foreach (CalculoPerformance emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CalculoPerformanceMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoPerformanceQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CalculoPerformance(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CalculoPerformance();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CalculoPerformanceQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoPerformanceQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CalculoPerformanceQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CalculoPerformance AddNew()
		{
			CalculoPerformance entity = base.AddNewEntity() as CalculoPerformance;
			
			return entity;
		}

		public CalculoPerformance FindByPrimaryKey(System.Int32 idTabela)
		{
			return base.FindByPrimaryKey(idTabela) as CalculoPerformance;
		}


		#region IEnumerable<CalculoPerformance> Members

		IEnumerator<CalculoPerformance> IEnumerable<CalculoPerformance>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CalculoPerformance;
			}
		}

		#endregion
		
		private CalculoPerformanceQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CalculoPerformance' table
	/// </summary>

	[Serializable]
	public partial class CalculoPerformance : esCalculoPerformance
	{
		public CalculoPerformance()
		{

		}
	
		public CalculoPerformance(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CalculoPerformanceMetadata.Meta();
			}
		}
		
		
		
		override protected esCalculoPerformanceQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoPerformanceQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CalculoPerformanceQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoPerformanceQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CalculoPerformanceQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CalculoPerformanceQuery query;
	}



	[Serializable]
	public partial class CalculoPerformanceQuery : esCalculoPerformanceQuery
	{
		public CalculoPerformanceQuery()
		{

		}		
		
		public CalculoPerformanceQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CalculoPerformanceMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CalculoPerformanceMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CalculoPerformanceMetadata.ColumnNames.IdTabela, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoPerformanceMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoPerformanceMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoPerformanceMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoPerformanceMetadata.ColumnNames.ValorAcumulado, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoPerformanceMetadata.PropertyNames.ValorAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoPerformanceMetadata.ColumnNames.DataFimApropriacao, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoPerformanceMetadata.PropertyNames.DataFimApropriacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoPerformanceMetadata.ColumnNames.DataPagamento, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoPerformanceMetadata.PropertyNames.DataPagamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoPerformanceMetadata.ColumnNames.ValorCPMFAcumulado, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoPerformanceMetadata.PropertyNames.ValorCPMFAcumulado;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoPerformanceMetadata.ColumnNames.ValorDia, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoPerformanceMetadata.PropertyNames.ValorDia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoPerformanceMetadata.ColumnNames.CotaAtual, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoPerformanceMetadata.PropertyNames.CotaAtual;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoPerformanceMetadata.ColumnNames.CotaBase, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoPerformanceMetadata.PropertyNames.CotaBase;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoPerformanceMetadata.ColumnNames.CotaCorrigida, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoPerformanceMetadata.PropertyNames.CotaCorrigida;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoPerformanceMetadata.ColumnNames.FatorIndice, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoPerformanceMetadata.PropertyNames.FatorIndice;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoPerformanceMetadata.ColumnNames.FatorJuros, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoPerformanceMetadata.PropertyNames.FatorJuros;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoPerformanceMetadata.ColumnNames.QuantidadeCotas, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoPerformanceMetadata.PropertyNames.QuantidadeCotas;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoPerformanceMetadata.ColumnNames.Rendimento, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoPerformanceMetadata.PropertyNames.Rendimento;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CalculoPerformanceMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorAcumulado = "ValorAcumulado";
			 public const string DataFimApropriacao = "DataFimApropriacao";
			 public const string DataPagamento = "DataPagamento";
			 public const string ValorCPMFAcumulado = "ValorCPMFAcumulado";
			 public const string ValorDia = "ValorDia";
			 public const string CotaAtual = "CotaAtual";
			 public const string CotaBase = "CotaBase";
			 public const string CotaCorrigida = "CotaCorrigida";
			 public const string FatorIndice = "FatorIndice";
			 public const string FatorJuros = "FatorJuros";
			 public const string QuantidadeCotas = "QuantidadeCotas";
			 public const string Rendimento = "Rendimento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorAcumulado = "ValorAcumulado";
			 public const string DataFimApropriacao = "DataFimApropriacao";
			 public const string DataPagamento = "DataPagamento";
			 public const string ValorCPMFAcumulado = "ValorCPMFAcumulado";
			 public const string ValorDia = "ValorDia";
			 public const string CotaAtual = "CotaAtual";
			 public const string CotaBase = "CotaBase";
			 public const string CotaCorrigida = "CotaCorrigida";
			 public const string FatorIndice = "FatorIndice";
			 public const string FatorJuros = "FatorJuros";
			 public const string QuantidadeCotas = "QuantidadeCotas";
			 public const string Rendimento = "Rendimento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CalculoPerformanceMetadata))
			{
				if(CalculoPerformanceMetadata.mapDelegates == null)
				{
					CalculoPerformanceMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CalculoPerformanceMetadata.meta == null)
				{
					CalculoPerformanceMetadata.meta = new CalculoPerformanceMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorAcumulado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataFimApropriacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataPagamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorCPMFAcumulado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaAtual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaBase", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaCorrigida", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("FatorIndice", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("FatorJuros", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeCotas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Rendimento", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CalculoPerformance";
				meta.Destination = "CalculoPerformance";
				
				meta.spInsert = "proc_CalculoPerformanceInsert";				
				meta.spUpdate = "proc_CalculoPerformanceUpdate";		
				meta.spDelete = "proc_CalculoPerformanceDelete";
				meta.spLoadAll = "proc_CalculoPerformanceLoadAll";
				meta.spLoadByPrimaryKey = "proc_CalculoPerformanceLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CalculoPerformanceMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
