/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 13/07/2015 12:40:35
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esEventoFundoBolsaCollection : esEntityCollection
	{
		public esEventoFundoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EventoFundoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esEventoFundoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEventoFundoBolsaQuery);
		}
		#endregion
		
		virtual public EventoFundoBolsa DetachEntity(EventoFundoBolsa entity)
		{
			return base.DetachEntity(entity) as EventoFundoBolsa;
		}
		
		virtual public EventoFundoBolsa AttachEntity(EventoFundoBolsa entity)
		{
			return base.AttachEntity(entity) as EventoFundoBolsa;
		}
		
		virtual public void Combine(EventoFundoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EventoFundoBolsa this[int index]
		{
			get
			{
				return base[index] as EventoFundoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EventoFundoBolsa);
		}
	}



	[Serializable]
	abstract public class esEventoFundoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEventoFundoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esEventoFundoBolsa()
		{

		}

		public esEventoFundoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String cdAtivoBolsa, System.Int32 idAgente, System.Int32 idEventoFundo, System.Int32 tipoMercado)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivoBolsa, idAgente, idEventoFundo, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivoBolsa, idAgente, idEventoFundo, tipoMercado);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String cdAtivoBolsa, System.Int32 idAgente, System.Int32 idEventoFundo, System.Int32 tipoMercado)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivoBolsa, idAgente, idEventoFundo, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivoBolsa, idAgente, idEventoFundo, tipoMercado);
		}

		private bool LoadByPrimaryKeyDynamic(System.String cdAtivoBolsa, System.Int32 idAgente, System.Int32 idEventoFundo, System.Int32 tipoMercado)
		{
			esEventoFundoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.CdAtivoBolsa == cdAtivoBolsa, query.IdAgente == idAgente, query.IdEventoFundo == idEventoFundo, query.TipoMercado == tipoMercado);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String cdAtivoBolsa, System.Int32 idAgente, System.Int32 idEventoFundo, System.Int32 tipoMercado)
		{
			esParameters parms = new esParameters();
			parms.Add("CdAtivoBolsa",cdAtivoBolsa);			parms.Add("IdAgente",idAgente);			parms.Add("IdEventoFundo",idEventoFundo);			parms.Add("TipoMercado",tipoMercado);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEventoFundo": this.str.IdEventoFundo = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "Processar": this.str.Processar = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEventoFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoFundo = (System.Int32?)value;
							break;
						
						case "TipoMercado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoMercado = (System.Int32?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "Processar":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Processar = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EventoFundoBolsa.IdEventoFundo
		/// </summary>
		virtual public System.Int32? IdEventoFundo
		{
			get
			{
				return base.GetSystemInt32(EventoFundoBolsaMetadata.ColumnNames.IdEventoFundo);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoFundoBolsaMetadata.ColumnNames.IdEventoFundo, value))
				{
					this._UpToEventoFundoByIdEventoFundo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoBolsa.TipoMercado
		/// </summary>
		virtual public System.Int32? TipoMercado
		{
			get
			{
				return base.GetSystemInt32(EventoFundoBolsaMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoBolsaMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(EventoFundoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				base.SetSystemString(EventoFundoBolsaMetadata.ColumnNames.CdAtivoBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoBolsa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(EventoFundoBolsaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(EventoFundoBolsaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoBolsa.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(EventoFundoBolsaMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoBolsaMetadata.ColumnNames.IdAgente, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundoBolsa.Processar
		/// </summary>
		virtual public System.Byte? Processar
		{
			get
			{
				return base.GetSystemByte(EventoFundoBolsaMetadata.ColumnNames.Processar);
			}
			
			set
			{
				base.SetSystemByte(EventoFundoBolsaMetadata.ColumnNames.Processar, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected EventoFundo _UpToEventoFundoByIdEventoFundo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEventoFundoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEventoFundo
			{
				get
				{
					System.Int32? data = entity.IdEventoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoFundo = null;
					else entity.IdEventoFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.Int32? data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String Processar
			{
				get
				{
					System.Byte? data = entity.Processar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Processar = null;
					else entity.Processar = Convert.ToByte(value);
				}
			}
			

			private esEventoFundoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEventoFundoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEventoFundoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EventoFundoBolsa : esEventoFundoBolsa
	{

				
		#region UpToEventoFundoByIdEventoFundo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EventoFundoBolsa_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundo UpToEventoFundoByIdEventoFundo
		{
			get
			{
				if(this._UpToEventoFundoByIdEventoFundo == null
					&& IdEventoFundo != null					)
				{
					this._UpToEventoFundoByIdEventoFundo = new EventoFundo();
					this._UpToEventoFundoByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Where(this._UpToEventoFundoByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
					this._UpToEventoFundoByIdEventoFundo.Query.Load();
				}

				return this._UpToEventoFundoByIdEventoFundo;
			}
			
			set
			{
				this.RemovePreSave("UpToEventoFundoByIdEventoFundo");
				

				if(value == null)
				{
					this.IdEventoFundo = null;
					this._UpToEventoFundoByIdEventoFundo = null;
				}
				else
				{
					this.IdEventoFundo = value.IdEventoFundo;
					this._UpToEventoFundoByIdEventoFundo = value;
					this.SetPreSave("UpToEventoFundoByIdEventoFundo", this._UpToEventoFundoByIdEventoFundo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEventoFundoByIdEventoFundo != null)
			{
				this.IdEventoFundo = this._UpToEventoFundoByIdEventoFundo.IdEventoFundo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEventoFundoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEventoFundo
		{
			get
			{
				return new esQueryItem(this, EventoFundoBolsaMetadata.ColumnNames.IdEventoFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, EventoFundoBolsaMetadata.ColumnNames.TipoMercado, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, EventoFundoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, EventoFundoBolsaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, EventoFundoBolsaMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Processar
		{
			get
			{
				return new esQueryItem(this, EventoFundoBolsaMetadata.ColumnNames.Processar, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EventoFundoBolsaCollection")]
	public partial class EventoFundoBolsaCollection : esEventoFundoBolsaCollection, IEnumerable<EventoFundoBolsa>
	{
		public EventoFundoBolsaCollection()
		{

		}
		
		public static implicit operator List<EventoFundoBolsa>(EventoFundoBolsaCollection coll)
		{
			List<EventoFundoBolsa> list = new List<EventoFundoBolsa>();
			
			foreach (EventoFundoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EventoFundoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EventoFundoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EventoFundoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EventoFundoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EventoFundoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EventoFundoBolsa AddNew()
		{
			EventoFundoBolsa entity = base.AddNewEntity() as EventoFundoBolsa;
			
			return entity;
		}

		public EventoFundoBolsa FindByPrimaryKey(System.String cdAtivoBolsa, System.Int32 idAgente, System.Int32 idEventoFundo, System.Int32 tipoMercado)
		{
			return base.FindByPrimaryKey(cdAtivoBolsa, idAgente, idEventoFundo, tipoMercado) as EventoFundoBolsa;
		}


		#region IEnumerable<EventoFundoBolsa> Members

		IEnumerator<EventoFundoBolsa> IEnumerable<EventoFundoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EventoFundoBolsa;
			}
		}

		#endregion
		
		private EventoFundoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EventoFundoBolsa' table
	/// </summary>

	[Serializable]
	public partial class EventoFundoBolsa : esEventoFundoBolsa
	{
		public EventoFundoBolsa()
		{

		}
	
		public EventoFundoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esEventoFundoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EventoFundoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EventoFundoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EventoFundoBolsaQuery query;
	}



	[Serializable]
	public partial class EventoFundoBolsaQuery : esEventoFundoBolsaQuery
	{
		public EventoFundoBolsaQuery()
		{

		}		
		
		public EventoFundoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EventoFundoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EventoFundoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EventoFundoBolsaMetadata.ColumnNames.IdEventoFundo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoBolsaMetadata.PropertyNames.IdEventoFundo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoBolsaMetadata.ColumnNames.TipoMercado, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoBolsaMetadata.PropertyNames.TipoMercado;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoBolsaMetadata.ColumnNames.CdAtivoBolsa, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = EventoFundoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoBolsaMetadata.ColumnNames.Quantidade, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EventoFundoBolsaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoBolsaMetadata.ColumnNames.IdAgente, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoBolsaMetadata.PropertyNames.IdAgente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoBolsaMetadata.ColumnNames.Processar, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EventoFundoBolsaMetadata.PropertyNames.Processar;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EventoFundoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string TipoMercado = "TipoMercado";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Quantidade = "Quantidade";
			 public const string IdAgente = "IdAgente";
			 public const string Processar = "Processar";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string TipoMercado = "TipoMercado";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Quantidade = "Quantidade";
			 public const string IdAgente = "IdAgente";
			 public const string Processar = "Processar";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EventoFundoBolsaMetadata))
			{
				if(EventoFundoBolsaMetadata.mapDelegates == null)
				{
					EventoFundoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EventoFundoBolsaMetadata.meta == null)
				{
					EventoFundoBolsaMetadata.meta = new EventoFundoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEventoFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Processar", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "EventoFundoBolsa";
				meta.Destination = "EventoFundoBolsa";
				
				meta.spInsert = "proc_EventoFundoBolsaInsert";				
				meta.spUpdate = "proc_EventoFundoBolsaUpdate";		
				meta.spDelete = "proc_EventoFundoBolsaDelete";
				meta.spLoadAll = "proc_EventoFundoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_EventoFundoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EventoFundoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
