/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:18 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				




using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Swap;
using Financial.Common;









	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esEstrategiaCollection : esEntityCollection
	{
		public esEstrategiaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EstrategiaCollection";
		}

		#region Query Logic
		protected void InitQuery(esEstrategiaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEstrategiaQuery);
		}
		#endregion
		
		virtual public Estrategia DetachEntity(Estrategia entity)
		{
			return base.DetachEntity(entity) as Estrategia;
		}
		
		virtual public Estrategia AttachEntity(Estrategia entity)
		{
			return base.AttachEntity(entity) as Estrategia;
		}
		
		virtual public void Combine(EstrategiaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Estrategia this[int index]
		{
			get
			{
				return base[index] as Estrategia;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Estrategia);
		}
	}



	[Serializable]
	abstract public class esEstrategia : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEstrategiaQuery GetDynamicQuery()
		{
			return null;
		}

		public esEstrategia()
		{

		}

		public esEstrategia(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEstrategia)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEstrategia);
			else
				return LoadByPrimaryKeyStoredProcedure(idEstrategia);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idEstrategia)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEstrategiaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdEstrategia == idEstrategia);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEstrategia)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEstrategia);
			else
				return LoadByPrimaryKeyStoredProcedure(idEstrategia);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEstrategia)
		{
			esEstrategiaQuery query = this.GetDynamicQuery();
			query.Where(query.IdEstrategia == idEstrategia);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEstrategia)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEstrategia",idEstrategia);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEstrategia": this.str.IdEstrategia = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "IdIndiceBenchmark": this.str.IdIndiceBenchmark = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEstrategia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEstrategia = (System.Int32?)value;
							break;
						
						case "IdIndiceBenchmark":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndiceBenchmark = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Estrategia.IdEstrategia
		/// </summary>
		virtual public System.Int32? IdEstrategia
		{
			get
			{
				return base.GetSystemInt32(EstrategiaMetadata.ColumnNames.IdEstrategia);
			}
			
			set
			{
				base.SetSystemInt32(EstrategiaMetadata.ColumnNames.IdEstrategia, value);
			}
		}
		
		/// <summary>
		/// Maps to Estrategia.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(EstrategiaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(EstrategiaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to Estrategia.IdIndiceBenchmark
		/// </summary>
		virtual public System.Int16? IdIndiceBenchmark
		{
			get
			{
				return base.GetSystemInt16(EstrategiaMetadata.ColumnNames.IdIndiceBenchmark);
			}
			
			set
			{
				if(base.SetSystemInt16(EstrategiaMetadata.ColumnNames.IdIndiceBenchmark, value))
				{
					this._UpToIndiceByIdIndiceBenchmark = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndiceBenchmark;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEstrategia entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEstrategia
			{
				get
				{
					System.Int32? data = entity.IdEstrategia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEstrategia = null;
					else entity.IdEstrategia = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String IdIndiceBenchmark
			{
				get
				{
					System.Int16? data = entity.IdIndiceBenchmark;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndiceBenchmark = null;
					else entity.IdIndiceBenchmark = Convert.ToInt16(value);
				}
			}
			

			private esEstrategia entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEstrategiaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEstrategia can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Estrategia : esEstrategia
	{

				
		#region AtivoBMFCollectionByIdEstrategia - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Estrategia_AtivoBMF_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBMFCollection AtivoBMFCollectionByIdEstrategia
		{
			get
			{
				if(this._AtivoBMFCollectionByIdEstrategia == null)
				{
					this._AtivoBMFCollectionByIdEstrategia = new AtivoBMFCollection();
					this._AtivoBMFCollectionByIdEstrategia.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AtivoBMFCollectionByIdEstrategia", this._AtivoBMFCollectionByIdEstrategia);
				
					if(this.IdEstrategia != null)
					{
						this._AtivoBMFCollectionByIdEstrategia.Query.Where(this._AtivoBMFCollectionByIdEstrategia.Query.IdEstrategia == this.IdEstrategia);
						this._AtivoBMFCollectionByIdEstrategia.Query.Load();

						// Auto-hookup Foreign Keys
						this._AtivoBMFCollectionByIdEstrategia.fks.Add(AtivoBMFMetadata.ColumnNames.IdEstrategia, this.IdEstrategia);
					}
				}

				return this._AtivoBMFCollectionByIdEstrategia;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AtivoBMFCollectionByIdEstrategia != null) 
				{ 
					this.RemovePostSave("AtivoBMFCollectionByIdEstrategia"); 
					this._AtivoBMFCollectionByIdEstrategia = null;
					
				} 
			} 			
		}

		private AtivoBMFCollection _AtivoBMFCollectionByIdEstrategia;
		#endregion

				
		#region AtivoBolsaCollectionByIdEstrategia - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Estrategia_AtivoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsaCollection AtivoBolsaCollectionByIdEstrategia
		{
			get
			{
				if(this._AtivoBolsaCollectionByIdEstrategia == null)
				{
					this._AtivoBolsaCollectionByIdEstrategia = new AtivoBolsaCollection();
					this._AtivoBolsaCollectionByIdEstrategia.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AtivoBolsaCollectionByIdEstrategia", this._AtivoBolsaCollectionByIdEstrategia);
				
					if(this.IdEstrategia != null)
					{
						this._AtivoBolsaCollectionByIdEstrategia.Query.Where(this._AtivoBolsaCollectionByIdEstrategia.Query.IdEstrategia == this.IdEstrategia);
						this._AtivoBolsaCollectionByIdEstrategia.Query.Load();

						// Auto-hookup Foreign Keys
						this._AtivoBolsaCollectionByIdEstrategia.fks.Add(AtivoBolsaMetadata.ColumnNames.IdEstrategia, this.IdEstrategia);
					}
				}

				return this._AtivoBolsaCollectionByIdEstrategia;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AtivoBolsaCollectionByIdEstrategia != null) 
				{ 
					this.RemovePostSave("AtivoBolsaCollectionByIdEstrategia"); 
					this._AtivoBolsaCollectionByIdEstrategia = null;
					
				} 
			} 			
		}

		private AtivoBolsaCollection _AtivoBolsaCollectionByIdEstrategia;
		#endregion

				
		#region CarteiraCollectionByIdEstrategia - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Estrategia_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public CarteiraCollection CarteiraCollectionByIdEstrategia
		{
			get
			{
				if(this._CarteiraCollectionByIdEstrategia == null)
				{
					this._CarteiraCollectionByIdEstrategia = new CarteiraCollection();
					this._CarteiraCollectionByIdEstrategia.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CarteiraCollectionByIdEstrategia", this._CarteiraCollectionByIdEstrategia);
				
					if(this.IdEstrategia != null)
					{
						this._CarteiraCollectionByIdEstrategia.Query.Where(this._CarteiraCollectionByIdEstrategia.Query.IdEstrategia == this.IdEstrategia);
						this._CarteiraCollectionByIdEstrategia.Query.Load();

						// Auto-hookup Foreign Keys
						this._CarteiraCollectionByIdEstrategia.fks.Add(CarteiraMetadata.ColumnNames.IdEstrategia, this.IdEstrategia);
					}
				}

				return this._CarteiraCollectionByIdEstrategia;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CarteiraCollectionByIdEstrategia != null) 
				{ 
					this.RemovePostSave("CarteiraCollectionByIdEstrategia"); 
					this._CarteiraCollectionByIdEstrategia = null;
					
				} 
			} 			
		}

		private CarteiraCollection _CarteiraCollectionByIdEstrategia;
		#endregion

				
		#region TituloRendaFixaCollectionByIdEstrategia - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Estrategia_TituloRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixaCollection TituloRendaFixaCollectionByIdEstrategia
		{
			get
			{
				if(this._TituloRendaFixaCollectionByIdEstrategia == null)
				{
					this._TituloRendaFixaCollectionByIdEstrategia = new TituloRendaFixaCollection();
					this._TituloRendaFixaCollectionByIdEstrategia.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TituloRendaFixaCollectionByIdEstrategia", this._TituloRendaFixaCollectionByIdEstrategia);
				
					if(this.IdEstrategia != null)
					{
						this._TituloRendaFixaCollectionByIdEstrategia.Query.Where(this._TituloRendaFixaCollectionByIdEstrategia.Query.IdEstrategia == this.IdEstrategia);
						this._TituloRendaFixaCollectionByIdEstrategia.Query.Load();

						// Auto-hookup Foreign Keys
						this._TituloRendaFixaCollectionByIdEstrategia.fks.Add(TituloRendaFixaMetadata.ColumnNames.IdEstrategia, this.IdEstrategia);
					}
				}

				return this._TituloRendaFixaCollectionByIdEstrategia;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TituloRendaFixaCollectionByIdEstrategia != null) 
				{ 
					this.RemovePostSave("TituloRendaFixaCollectionByIdEstrategia"); 
					this._TituloRendaFixaCollectionByIdEstrategia = null;
					
				} 
			} 			
		}

		private TituloRendaFixaCollection _TituloRendaFixaCollectionByIdEstrategia;
		#endregion

				
		#region UpToIndiceByIdIndiceBenchmark - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_Estrategia_Indice
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndiceBenchmark
		{
			get
			{
				if(this._UpToIndiceByIdIndiceBenchmark == null
					&& IdIndiceBenchmark != null					)
				{
					this._UpToIndiceByIdIndiceBenchmark = new Indice();
					this._UpToIndiceByIdIndiceBenchmark.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndiceBenchmark", this._UpToIndiceByIdIndiceBenchmark);
					this._UpToIndiceByIdIndiceBenchmark.Query.Where(this._UpToIndiceByIdIndiceBenchmark.Query.IdIndice == this.IdIndiceBenchmark);
					this._UpToIndiceByIdIndiceBenchmark.Query.Load();
				}

				return this._UpToIndiceByIdIndiceBenchmark;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndiceBenchmark");
				

				if(value == null)
				{
					this.IdIndiceBenchmark = null;
					this._UpToIndiceByIdIndiceBenchmark = null;
				}
				else
				{
					this.IdIndiceBenchmark = value.IdIndice;
					this._UpToIndiceByIdIndiceBenchmark = value;
					this.SetPreSave("UpToIndiceByIdIndiceBenchmark", this._UpToIndiceByIdIndiceBenchmark);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "AtivoBMFCollectionByIdEstrategia", typeof(AtivoBMFCollection), new AtivoBMF()));
			props.Add(new esPropertyDescriptor(this, "AtivoBolsaCollectionByIdEstrategia", typeof(AtivoBolsaCollection), new AtivoBolsa()));
			props.Add(new esPropertyDescriptor(this, "CarteiraCollectionByIdEstrategia", typeof(CarteiraCollection), new Carteira()));
			props.Add(new esPropertyDescriptor(this, "TituloRendaFixaCollectionByIdEstrategia", typeof(TituloRendaFixaCollection), new TituloRendaFixa()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._AtivoBMFCollectionByIdEstrategia != null)
			{
				foreach(AtivoBMF obj in this._AtivoBMFCollectionByIdEstrategia)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEstrategia = this.IdEstrategia;
					}
				}
			}
			if(this._AtivoBolsaCollectionByIdEstrategia != null)
			{
				foreach(AtivoBolsa obj in this._AtivoBolsaCollectionByIdEstrategia)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEstrategia = this.IdEstrategia;
					}
				}
			}
			if(this._CarteiraCollectionByIdEstrategia != null)
			{
				foreach(Carteira obj in this._CarteiraCollectionByIdEstrategia)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEstrategia = this.IdEstrategia;
					}
				}
			}
			if(this._TituloRendaFixaCollectionByIdEstrategia != null)
			{
				foreach(TituloRendaFixa obj in this._TituloRendaFixaCollectionByIdEstrategia)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEstrategia = this.IdEstrategia;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEstrategiaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EstrategiaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEstrategia
		{
			get
			{
				return new esQueryItem(this, EstrategiaMetadata.ColumnNames.IdEstrategia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, EstrategiaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdIndiceBenchmark
		{
			get
			{
				return new esQueryItem(this, EstrategiaMetadata.ColumnNames.IdIndiceBenchmark, esSystemType.Int16);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EstrategiaCollection")]
	public partial class EstrategiaCollection : esEstrategiaCollection, IEnumerable<Estrategia>
	{
		public EstrategiaCollection()
		{

		}
		
		public static implicit operator List<Estrategia>(EstrategiaCollection coll)
		{
			List<Estrategia> list = new List<Estrategia>();
			
			foreach (Estrategia emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EstrategiaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EstrategiaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Estrategia(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Estrategia();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EstrategiaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EstrategiaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EstrategiaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Estrategia AddNew()
		{
			Estrategia entity = base.AddNewEntity() as Estrategia;
			
			return entity;
		}

		public Estrategia FindByPrimaryKey(System.Int32 idEstrategia)
		{
			return base.FindByPrimaryKey(idEstrategia) as Estrategia;
		}


		#region IEnumerable<Estrategia> Members

		IEnumerator<Estrategia> IEnumerable<Estrategia>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Estrategia;
			}
		}

		#endregion
		
		private EstrategiaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Estrategia' table
	/// </summary>

	[Serializable]
	public partial class Estrategia : esEstrategia
	{
		public Estrategia()
		{

		}
	
		public Estrategia(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EstrategiaMetadata.Meta();
			}
		}
		
		
		
		override protected esEstrategiaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EstrategiaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EstrategiaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EstrategiaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EstrategiaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EstrategiaQuery query;
	}



	[Serializable]
	public partial class EstrategiaQuery : esEstrategiaQuery
	{
		public EstrategiaQuery()
		{

		}		
		
		public EstrategiaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EstrategiaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EstrategiaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EstrategiaMetadata.ColumnNames.IdEstrategia, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EstrategiaMetadata.PropertyNames.IdEstrategia;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EstrategiaMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = EstrategiaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EstrategiaMetadata.ColumnNames.IdIndiceBenchmark, 2, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = EstrategiaMetadata.PropertyNames.IdIndiceBenchmark;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EstrategiaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEstrategia = "IdEstrategia";
			 public const string Descricao = "Descricao";
			 public const string IdIndiceBenchmark = "IdIndiceBenchmark";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEstrategia = "IdEstrategia";
			 public const string Descricao = "Descricao";
			 public const string IdIndiceBenchmark = "IdIndiceBenchmark";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EstrategiaMetadata))
			{
				if(EstrategiaMetadata.mapDelegates == null)
				{
					EstrategiaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EstrategiaMetadata.meta == null)
				{
					EstrategiaMetadata.meta = new EstrategiaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEstrategia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdIndiceBenchmark", new esTypeMap("smallint", "System.Int16"));			
				
				
				
				meta.Source = "Estrategia";
				meta.Destination = "Estrategia";
				
				meta.spInsert = "proc_EstrategiaInsert";				
				meta.spUpdate = "proc_EstrategiaUpdate";		
				meta.spDelete = "proc_EstrategiaDelete";
				meta.spLoadAll = "proc_EstrategiaLoadAll";
				meta.spLoadByPrimaryKey = "proc_EstrategiaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EstrategiaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
