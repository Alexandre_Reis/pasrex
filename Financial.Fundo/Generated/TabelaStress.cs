/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:22 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTabelaStressCollection : esEntityCollection
	{
		public esTabelaStressCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaStressCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaStressQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaStressQuery);
		}
		#endregion
		
		virtual public TabelaStress DetachEntity(TabelaStress entity)
		{
			return base.DetachEntity(entity) as TabelaStress;
		}
		
		virtual public TabelaStress AttachEntity(TabelaStress entity)
		{
			return base.AttachEntity(entity) as TabelaStress;
		}
		
		virtual public void Combine(TabelaStressCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaStress this[int index]
		{
			get
			{
				return base[index] as TabelaStress;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaStress);
		}
	}



	[Serializable]
	abstract public class esTabelaStress : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaStressQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaStress()
		{

		}

		public esTabelaStress(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String idAtivo, System.DateTime dataReferencia, System.Byte tipoAtivo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAtivo, dataReferencia, tipoAtivo);
			else
				return LoadByPrimaryKeyStoredProcedure(idAtivo, dataReferencia, tipoAtivo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.String idAtivo, System.DateTime dataReferencia, System.Byte tipoAtivo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaStressQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdAtivo == idAtivo, query.DataReferencia == dataReferencia, query.TipoAtivo == tipoAtivo);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String idAtivo, System.DateTime dataReferencia, System.Byte tipoAtivo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAtivo, dataReferencia, tipoAtivo);
			else
				return LoadByPrimaryKeyStoredProcedure(idAtivo, dataReferencia, tipoAtivo);
		}

		private bool LoadByPrimaryKeyDynamic(System.String idAtivo, System.DateTime dataReferencia, System.Byte tipoAtivo)
		{
			esTabelaStressQuery query = this.GetDynamicQuery();
			query.Where(query.IdAtivo == idAtivo, query.DataReferencia == dataReferencia, query.TipoAtivo == tipoAtivo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String idAtivo, System.DateTime dataReferencia, System.Byte tipoAtivo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdAtivo",idAtivo);			parms.Add("DataReferencia",dataReferencia);			parms.Add("TipoAtivo",tipoAtivo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdAtivo": this.str.IdAtivo = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "TipoAtivo": this.str.TipoAtivo = (string)value; break;							
						case "Stress": this.str.Stress = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "TipoAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoAtivo = (System.Byte?)value;
							break;
						
						case "Stress":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Stress = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaStress.IdAtivo
		/// </summary>
		virtual public System.String IdAtivo
		{
			get
			{
				return base.GetSystemString(TabelaStressMetadata.ColumnNames.IdAtivo);
			}
			
			set
			{
				base.SetSystemString(TabelaStressMetadata.ColumnNames.IdAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaStress.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaStressMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaStressMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaStress.TipoAtivo
		/// </summary>
		virtual public System.Byte? TipoAtivo
		{
			get
			{
				return base.GetSystemByte(TabelaStressMetadata.ColumnNames.TipoAtivo);
			}
			
			set
			{
				base.SetSystemByte(TabelaStressMetadata.ColumnNames.TipoAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaStress.Stress
		/// </summary>
		virtual public System.Decimal? Stress
		{
			get
			{
				return base.GetSystemDecimal(TabelaStressMetadata.ColumnNames.Stress);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaStressMetadata.ColumnNames.Stress, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaStress entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdAtivo
			{
				get
				{
					System.String data = entity.IdAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAtivo = null;
					else entity.IdAtivo = Convert.ToString(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoAtivo
			{
				get
				{
					System.Byte? data = entity.TipoAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoAtivo = null;
					else entity.TipoAtivo = Convert.ToByte(value);
				}
			}
				
			public System.String Stress
			{
				get
				{
					System.Decimal? data = entity.Stress;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Stress = null;
					else entity.Stress = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaStress entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaStressQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaStress can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaStress : esTabelaStress
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaStressQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaStressMetadata.Meta();
			}
		}	
		

		public esQueryItem IdAtivo
		{
			get
			{
				return new esQueryItem(this, TabelaStressMetadata.ColumnNames.IdAtivo, esSystemType.String);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaStressMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoAtivo
		{
			get
			{
				return new esQueryItem(this, TabelaStressMetadata.ColumnNames.TipoAtivo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Stress
		{
			get
			{
				return new esQueryItem(this, TabelaStressMetadata.ColumnNames.Stress, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaStressCollection")]
	public partial class TabelaStressCollection : esTabelaStressCollection, IEnumerable<TabelaStress>
	{
		public TabelaStressCollection()
		{

		}
		
		public static implicit operator List<TabelaStress>(TabelaStressCollection coll)
		{
			List<TabelaStress> list = new List<TabelaStress>();
			
			foreach (TabelaStress emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaStressMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaStressQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaStress(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaStress();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaStressQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaStressQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaStressQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaStress AddNew()
		{
			TabelaStress entity = base.AddNewEntity() as TabelaStress;
			
			return entity;
		}

		public TabelaStress FindByPrimaryKey(System.String idAtivo, System.DateTime dataReferencia, System.Byte tipoAtivo)
		{
			return base.FindByPrimaryKey(idAtivo, dataReferencia, tipoAtivo) as TabelaStress;
		}


		#region IEnumerable<TabelaStress> Members

		IEnumerator<TabelaStress> IEnumerable<TabelaStress>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaStress;
			}
		}

		#endregion
		
		private TabelaStressQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaStress' table
	/// </summary>

	[Serializable]
	public partial class TabelaStress : esTabelaStress
	{
		public TabelaStress()
		{

		}
	
		public TabelaStress(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaStressMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaStressQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaStressQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaStressQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaStressQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaStressQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaStressQuery query;
	}



	[Serializable]
	public partial class TabelaStressQuery : esTabelaStressQuery
	{
		public TabelaStressQuery()
		{

		}		
		
		public TabelaStressQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaStressMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaStressMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaStressMetadata.ColumnNames.IdAtivo, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaStressMetadata.PropertyNames.IdAtivo;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaStressMetadata.ColumnNames.DataReferencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaStressMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaStressMetadata.ColumnNames.TipoAtivo, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaStressMetadata.PropertyNames.TipoAtivo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaStressMetadata.ColumnNames.Stress, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaStressMetadata.PropertyNames.Stress;	
			c.NumericPrecision = 12;
			c.NumericScale = 6;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaStressMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdAtivo = "IdAtivo";
			 public const string DataReferencia = "DataReferencia";
			 public const string TipoAtivo = "TipoAtivo";
			 public const string Stress = "Stress";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdAtivo = "IdAtivo";
			 public const string DataReferencia = "DataReferencia";
			 public const string TipoAtivo = "TipoAtivo";
			 public const string Stress = "Stress";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaStressMetadata))
			{
				if(TabelaStressMetadata.mapDelegates == null)
				{
					TabelaStressMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaStressMetadata.meta == null)
				{
					TabelaStressMetadata.meta = new TabelaStressMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdAtivo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoAtivo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Stress", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaStress";
				meta.Destination = "TabelaStress";
				
				meta.spInsert = "proc_TabelaStressInsert";				
				meta.spUpdate = "proc_TabelaStressUpdate";		
				meta.spDelete = "proc_TabelaStressDelete";
				meta.spLoadAll = "proc_TabelaStressLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaStressLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaStressMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
