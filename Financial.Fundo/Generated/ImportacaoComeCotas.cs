/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 11/06/2015 15:19:49
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esImportacaoComeCotasCollection : esEntityCollection
	{
		public esImportacaoComeCotasCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ImportacaoComeCotasCollection";
		}

		#region Query Logic
		protected void InitQuery(esImportacaoComeCotasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esImportacaoComeCotasQuery);
		}
		#endregion
		
		virtual public ImportacaoComeCotas DetachEntity(ImportacaoComeCotas entity)
		{
			return base.DetachEntity(entity) as ImportacaoComeCotas;
		}
		
		virtual public ImportacaoComeCotas AttachEntity(ImportacaoComeCotas entity)
		{
			return base.AttachEntity(entity) as ImportacaoComeCotas;
		}
		
		virtual public void Combine(ImportacaoComeCotasCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ImportacaoComeCotas this[int index]
		{
			get
			{
				return base[index] as ImportacaoComeCotas;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ImportacaoComeCotas);
		}
	}



	[Serializable]
	abstract public class esImportacaoComeCotas : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esImportacaoComeCotasQuery GetDynamicQuery()
		{
			return null;
		}

		public esImportacaoComeCotas()
		{

		}

		public esImportacaoComeCotas(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao, System.Int32 tipoPosicao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, tipoPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, tipoPosicao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao, System.Int32 tipoPosicao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, tipoPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, tipoPosicao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao, System.Int32 tipoPosicao)
		{
			esImportacaoComeCotasQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao, query.TipoPosicao == tipoPosicao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao, System.Int32 tipoPosicao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);			parms.Add("TipoPosicao",tipoPosicao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "IdPosicaoComeCotas": this.str.IdPosicaoComeCotas = (string)value; break;							
						case "TipoPosicao": this.str.TipoPosicao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "IdPosicaoComeCotas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoComeCotas = (System.String)value;
							break;
						
						case "TipoPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoPosicao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ImportacaoComeCotas.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(ImportacaoComeCotasMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(ImportacaoComeCotasMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to ImportacaoComeCotas.IdPosicaoComeCotas
		/// </summary>
		virtual public System.String IdPosicaoComeCotas
		{
			get
			{
				return base.GetSystemString(ImportacaoComeCotasMetadata.ColumnNames.IdPosicaoComeCotas);
			}
			
			set
			{
				base.SetSystemString(ImportacaoComeCotasMetadata.ColumnNames.IdPosicaoComeCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to ImportacaoComeCotas.TipoPosicao
		/// </summary>
		virtual public System.Int32? TipoPosicao
		{
			get
			{
				return base.GetSystemInt32(ImportacaoComeCotasMetadata.ColumnNames.TipoPosicao);
			}
			
			set
			{
				base.SetSystemInt32(ImportacaoComeCotasMetadata.ColumnNames.TipoPosicao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esImportacaoComeCotas entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicaoComeCotas
			{
				get
				{
					System.String data = entity.IdPosicaoComeCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoComeCotas = null;
					else entity.IdPosicaoComeCotas = Convert.ToString(value);
				}
			}
				
			public System.String TipoPosicao
			{
				get
				{
					System.Int32? data = entity.TipoPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPosicao = null;
					else entity.TipoPosicao = Convert.ToInt32(value);
				}
			}
			

			private esImportacaoComeCotas entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esImportacaoComeCotasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esImportacaoComeCotas can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ImportacaoComeCotas : esImportacaoComeCotas
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esImportacaoComeCotasQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ImportacaoComeCotasMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, ImportacaoComeCotasMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicaoComeCotas
		{
			get
			{
				return new esQueryItem(this, ImportacaoComeCotasMetadata.ColumnNames.IdPosicaoComeCotas, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoPosicao
		{
			get
			{
				return new esQueryItem(this, ImportacaoComeCotasMetadata.ColumnNames.TipoPosicao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ImportacaoComeCotasCollection")]
	public partial class ImportacaoComeCotasCollection : esImportacaoComeCotasCollection, IEnumerable<ImportacaoComeCotas>
	{
		public ImportacaoComeCotasCollection()
		{

		}
		
		public static implicit operator List<ImportacaoComeCotas>(ImportacaoComeCotasCollection coll)
		{
			List<ImportacaoComeCotas> list = new List<ImportacaoComeCotas>();
			
			foreach (ImportacaoComeCotas emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ImportacaoComeCotasMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ImportacaoComeCotasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ImportacaoComeCotas(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ImportacaoComeCotas();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ImportacaoComeCotasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ImportacaoComeCotasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ImportacaoComeCotasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ImportacaoComeCotas AddNew()
		{
			ImportacaoComeCotas entity = base.AddNewEntity() as ImportacaoComeCotas;
			
			return entity;
		}

		public ImportacaoComeCotas FindByPrimaryKey(System.Int32 idPosicao, System.Int32 tipoPosicao)
		{
			return base.FindByPrimaryKey(idPosicao, tipoPosicao) as ImportacaoComeCotas;
		}


		#region IEnumerable<ImportacaoComeCotas> Members

		IEnumerator<ImportacaoComeCotas> IEnumerable<ImportacaoComeCotas>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ImportacaoComeCotas;
			}
		}

		#endregion
		
		private ImportacaoComeCotasQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ImportacaoComeCotas' table
	/// </summary>

	[Serializable]
	public partial class ImportacaoComeCotas : esImportacaoComeCotas
	{
		public ImportacaoComeCotas()
		{

		}
	
		public ImportacaoComeCotas(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ImportacaoComeCotasMetadata.Meta();
			}
		}
		
		
		
		override protected esImportacaoComeCotasQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ImportacaoComeCotasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ImportacaoComeCotasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ImportacaoComeCotasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ImportacaoComeCotasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ImportacaoComeCotasQuery query;
	}



	[Serializable]
	public partial class ImportacaoComeCotasQuery : esImportacaoComeCotasQuery
	{
		public ImportacaoComeCotasQuery()
		{

		}		
		
		public ImportacaoComeCotasQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ImportacaoComeCotasMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ImportacaoComeCotasMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ImportacaoComeCotasMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ImportacaoComeCotasMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ImportacaoComeCotasMetadata.ColumnNames.IdPosicaoComeCotas, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ImportacaoComeCotasMetadata.PropertyNames.IdPosicaoComeCotas;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ImportacaoComeCotasMetadata.ColumnNames.TipoPosicao, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ImportacaoComeCotasMetadata.PropertyNames.TipoPosicao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ImportacaoComeCotasMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdPosicaoComeCotas = "IdPosicaoComeCotas";
			 public const string TipoPosicao = "TipoPosicao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdPosicaoComeCotas = "IdPosicaoComeCotas";
			 public const string TipoPosicao = "TipoPosicao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ImportacaoComeCotasMetadata))
			{
				if(ImportacaoComeCotasMetadata.mapDelegates == null)
				{
					ImportacaoComeCotasMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ImportacaoComeCotasMetadata.meta == null)
				{
					ImportacaoComeCotasMetadata.meta = new ImportacaoComeCotasMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicaoComeCotas", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoPosicao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "ImportacaoComeCotas";
				meta.Destination = "ImportacaoComeCotas";
				
				meta.spInsert = "proc_ImportacaoComeCotasInsert";				
				meta.spUpdate = "proc_ImportacaoComeCotasUpdate";		
				meta.spDelete = "proc_ImportacaoComeCotasDelete";
				meta.spLoadAll = "proc_ImportacaoComeCotasLoadAll";
				meta.spLoadByPrimaryKey = "proc_ImportacaoComeCotasLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ImportacaoComeCotasMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
