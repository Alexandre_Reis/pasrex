/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/08/2015 12:40:30
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esAgendaComeCotasCollection : esEntityCollection
	{
		public esAgendaComeCotasCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AgendaComeCotasCollection";
		}

		#region Query Logic
		protected void InitQuery(esAgendaComeCotasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAgendaComeCotasQuery);
		}
		#endregion
		
		virtual public AgendaComeCotas DetachEntity(AgendaComeCotas entity)
		{
			return base.DetachEntity(entity) as AgendaComeCotas;
		}
		
		virtual public AgendaComeCotas AttachEntity(AgendaComeCotas entity)
		{
			return base.AttachEntity(entity) as AgendaComeCotas;
		}
		
		virtual public void Combine(AgendaComeCotasCollection collection)
		{
			base.Combine(collection);
		}
		
		new public AgendaComeCotas this[int index]
		{
			get
			{
				return base[index] as AgendaComeCotas;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(AgendaComeCotas);
		}
	}



	[Serializable]
	abstract public class esAgendaComeCotas : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAgendaComeCotasQuery GetDynamicQuery()
		{
			return null;
		}

		public esAgendaComeCotas()
		{

		}

		public esAgendaComeCotas(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idAgendamentoComeCotas)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgendamentoComeCotas);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgendamentoComeCotas);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idAgendamentoComeCotas)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgendamentoComeCotas);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgendamentoComeCotas);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idAgendamentoComeCotas)
		{
			esAgendaComeCotasQuery query = this.GetDynamicQuery();
			query.Where(query.IdAgendamentoComeCotas == idAgendamentoComeCotas);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idAgendamentoComeCotas)
		{
			esParameters parms = new esParameters();
			parms.Add("IdAgendamentoComeCotas",idAgendamentoComeCotas);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdAgendamentoComeCotas": this.str.IdAgendamentoComeCotas = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataLancamento": this.str.DataLancamento = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "TipoEvento": this.str.TipoEvento = (string)value; break;							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "DataUltimoIR": this.str.DataUltimoIR = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeAntesCortes": this.str.QuantidadeAntesCortes = (string)value; break;							
						case "ValorCotaAplic": this.str.ValorCotaAplic = (string)value; break;							
						case "ValorCotaUltimoPagamentoIR": this.str.ValorCotaUltimoPagamentoIR = (string)value; break;							
						case "ValorCota": this.str.ValorCota = (string)value; break;							
						case "AliquotaCC": this.str.AliquotaCC = (string)value; break;							
						case "RendimentoBrutoDesdeAplicacao": this.str.RendimentoBrutoDesdeAplicacao = (string)value; break;							
						case "RendimentoDesdeUltimoPagamentoIR": this.str.RendimentoDesdeUltimoPagamentoIR = (string)value; break;							
						case "NumDiasCorridosDesdeAquisicao": this.str.NumDiasCorridosDesdeAquisicao = (string)value; break;							
						case "PrazoIOF": this.str.PrazoIOF = (string)value; break;							
						case "AliquotaIOF": this.str.AliquotaIOF = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "ValorIOFVirtual": this.str.ValorIOFVirtual = (string)value; break;							
						case "PrejuizoUsado": this.str.PrejuizoUsado = (string)value; break;							
						case "RendimentoCompensado": this.str.RendimentoCompensado = (string)value; break;							
						case "ValorIRAgendado": this.str.ValorIRAgendado = (string)value; break;							
						case "ValorIRPago": this.str.ValorIRPago = (string)value; break;							
						case "Residuo15": this.str.Residuo15 = (string)value; break;							
						case "Residuo175": this.str.Residuo175 = (string)value; break;							
						case "Residuo20": this.str.Residuo20 = (string)value; break;							
						case "Residuo225": this.str.Residuo225 = (string)value; break;							
						case "QuantidadeComida": this.str.QuantidadeComida = (string)value; break;							
						case "QuantidadeFinal": this.str.QuantidadeFinal = (string)value; break;							
						case "ExecucaoRecolhimento": this.str.ExecucaoRecolhimento = (string)value; break;							
						case "TipoAliquotaIR": this.str.TipoAliquotaIR = (string)value; break;							
						case "AliquotaIR": this.str.AliquotaIR = (string)value; break;							
						case "TipoPosicao": this.str.TipoPosicao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdAgendamentoComeCotas":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgendamentoComeCotas = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataLancamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLancamento = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "TipoEvento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoEvento = (System.Int32?)value;
							break;
						
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "DataUltimoIR":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimoIR = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "QuantidadeAntesCortes":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeAntesCortes = (System.Decimal?)value;
							break;
						
						case "ValorCotaAplic":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCotaAplic = (System.Decimal?)value;
							break;
						
						case "ValorCotaUltimoPagamentoIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCotaUltimoPagamentoIR = (System.Decimal?)value;
							break;
						
						case "ValorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCota = (System.Decimal?)value;
							break;
						
						case "AliquotaCC":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AliquotaCC = (System.Decimal?)value;
							break;
						
						case "RendimentoBrutoDesdeAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoBrutoDesdeAplicacao = (System.Decimal?)value;
							break;
						
						case "RendimentoDesdeUltimoPagamentoIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoDesdeUltimoPagamentoIR = (System.Decimal?)value;
							break;
						
						case "NumDiasCorridosDesdeAquisicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumDiasCorridosDesdeAquisicao = (System.Int32?)value;
							break;
						
						case "PrazoIOF":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PrazoIOF = (System.Int32?)value;
							break;
						
						case "AliquotaIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AliquotaIOF = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "ValorIOFVirtual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOFVirtual = (System.Decimal?)value;
							break;
						
						case "PrejuizoUsado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoUsado = (System.Decimal?)value;
							break;
						
						case "RendimentoCompensado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoCompensado = (System.Decimal?)value;
							break;
						
						case "ValorIRAgendado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIRAgendado = (System.Decimal?)value;
							break;
						
						case "ValorIRPago":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIRPago = (System.Decimal?)value;
							break;
						
						case "Residuo15":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Residuo15 = (System.Decimal?)value;
							break;
						
						case "Residuo175":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Residuo175 = (System.Decimal?)value;
							break;
						
						case "Residuo20":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Residuo20 = (System.Decimal?)value;
							break;
						
						case "Residuo225":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Residuo225 = (System.Decimal?)value;
							break;
						
						case "QuantidadeComida":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeComida = (System.Decimal?)value;
							break;
						
						case "QuantidadeFinal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeFinal = (System.Decimal?)value;
							break;
						
						case "ExecucaoRecolhimento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.ExecucaoRecolhimento = (System.Int32?)value;
							break;
						
						case "TipoAliquotaIR":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoAliquotaIR = (System.Int32?)value;
							break;
						
						case "AliquotaIR":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.AliquotaIR = (System.Int32?)value;
							break;
						
						case "TipoPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoPosicao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to AgendaComeCotas.IdAgendamentoComeCotas
		/// </summary>
		virtual public System.Int32? IdAgendamentoComeCotas
		{
			get
			{
				return base.GetSystemInt32(AgendaComeCotasMetadata.ColumnNames.IdAgendamentoComeCotas);
			}
			
			set
			{
				base.SetSystemInt32(AgendaComeCotasMetadata.ColumnNames.IdAgendamentoComeCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(AgendaComeCotasMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(AgendaComeCotasMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.DataLancamento
		/// </summary>
		virtual public System.DateTime? DataLancamento
		{
			get
			{
				return base.GetSystemDateTime(AgendaComeCotasMetadata.ColumnNames.DataLancamento);
			}
			
			set
			{
				base.SetSystemDateTime(AgendaComeCotasMetadata.ColumnNames.DataLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(AgendaComeCotasMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(AgendaComeCotasMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.TipoEvento
		/// </summary>
		virtual public System.Int32? TipoEvento
		{
			get
			{
				return base.GetSystemInt32(AgendaComeCotasMetadata.ColumnNames.TipoEvento);
			}
			
			set
			{
				base.SetSystemInt32(AgendaComeCotasMetadata.ColumnNames.TipoEvento, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(AgendaComeCotasMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(AgendaComeCotasMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.DataUltimoIR
		/// </summary>
		virtual public System.DateTime? DataUltimoIR
		{
			get
			{
				return base.GetSystemDateTime(AgendaComeCotasMetadata.ColumnNames.DataUltimoIR);
			}
			
			set
			{
				base.SetSystemDateTime(AgendaComeCotasMetadata.ColumnNames.DataUltimoIR, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.QuantidadeAntesCortes
		/// </summary>
		virtual public System.Decimal? QuantidadeAntesCortes
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.QuantidadeAntesCortes);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.QuantidadeAntesCortes, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.ValorCotaAplic
		/// </summary>
		virtual public System.Decimal? ValorCotaAplic
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.ValorCotaAplic);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.ValorCotaAplic, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.ValorCotaUltimoPagamentoIR
		/// </summary>
		virtual public System.Decimal? ValorCotaUltimoPagamentoIR
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.ValorCotaUltimoPagamentoIR);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.ValorCotaUltimoPagamentoIR, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.ValorCota
		/// </summary>
		virtual public System.Decimal? ValorCota
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.ValorCota);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.ValorCota, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.AliquotaCC
		/// </summary>
		virtual public System.Decimal? AliquotaCC
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.AliquotaCC);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.AliquotaCC, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.RendimentoBrutoDesdeAplicacao
		/// </summary>
		virtual public System.Decimal? RendimentoBrutoDesdeAplicacao
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.RendimentoBrutoDesdeAplicacao);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.RendimentoBrutoDesdeAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.RendimentoDesdeUltimoPagamentoIR
		/// </summary>
		virtual public System.Decimal? RendimentoDesdeUltimoPagamentoIR
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.RendimentoDesdeUltimoPagamentoIR);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.RendimentoDesdeUltimoPagamentoIR, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.NumDiasCorridosDesdeAquisicao
		/// </summary>
		virtual public System.Int32? NumDiasCorridosDesdeAquisicao
		{
			get
			{
				return base.GetSystemInt32(AgendaComeCotasMetadata.ColumnNames.NumDiasCorridosDesdeAquisicao);
			}
			
			set
			{
				base.SetSystemInt32(AgendaComeCotasMetadata.ColumnNames.NumDiasCorridosDesdeAquisicao, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.PrazoIOF
		/// </summary>
		virtual public System.Int32? PrazoIOF
		{
			get
			{
				return base.GetSystemInt32(AgendaComeCotasMetadata.ColumnNames.PrazoIOF);
			}
			
			set
			{
				base.SetSystemInt32(AgendaComeCotasMetadata.ColumnNames.PrazoIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.AliquotaIOF
		/// </summary>
		virtual public System.Decimal? AliquotaIOF
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.AliquotaIOF);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.AliquotaIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.ValorIOFVirtual
		/// </summary>
		virtual public System.Decimal? ValorIOFVirtual
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.ValorIOFVirtual);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.ValorIOFVirtual, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.PrejuizoUsado
		/// </summary>
		virtual public System.Decimal? PrejuizoUsado
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.PrejuizoUsado);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.PrejuizoUsado, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.RendimentoCompensado
		/// </summary>
		virtual public System.Decimal? RendimentoCompensado
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.RendimentoCompensado);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.RendimentoCompensado, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.ValorIRAgendado
		/// </summary>
		virtual public System.Decimal? ValorIRAgendado
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.ValorIRAgendado);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.ValorIRAgendado, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.ValorIRPago
		/// </summary>
		virtual public System.Decimal? ValorIRPago
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.ValorIRPago);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.ValorIRPago, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.Residuo15
		/// </summary>
		virtual public System.Decimal? Residuo15
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.Residuo15);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.Residuo15, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.Residuo175
		/// </summary>
		virtual public System.Decimal? Residuo175
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.Residuo175);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.Residuo175, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.Residuo20
		/// </summary>
		virtual public System.Decimal? Residuo20
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.Residuo20);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.Residuo20, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.Residuo225
		/// </summary>
		virtual public System.Decimal? Residuo225
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.Residuo225);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.Residuo225, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.QuantidadeComida
		/// </summary>
		virtual public System.Decimal? QuantidadeComida
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.QuantidadeComida);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.QuantidadeComida, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.QuantidadeFinal
		/// </summary>
		virtual public System.Decimal? QuantidadeFinal
		{
			get
			{
				return base.GetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.QuantidadeFinal);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaComeCotasMetadata.ColumnNames.QuantidadeFinal, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.ExecucaoRecolhimento
		/// </summary>
		virtual public System.Int32? ExecucaoRecolhimento
		{
			get
			{
				return base.GetSystemInt32(AgendaComeCotasMetadata.ColumnNames.ExecucaoRecolhimento);
			}
			
			set
			{
				base.SetSystemInt32(AgendaComeCotasMetadata.ColumnNames.ExecucaoRecolhimento, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.TipoAliquotaIR
		/// </summary>
		virtual public System.Int32? TipoAliquotaIR
		{
			get
			{
				return base.GetSystemInt32(AgendaComeCotasMetadata.ColumnNames.TipoAliquotaIR);
			}
			
			set
			{
				base.SetSystemInt32(AgendaComeCotasMetadata.ColumnNames.TipoAliquotaIR, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.AliquotaIR
		/// </summary>
		virtual public System.Int32? AliquotaIR
		{
			get
			{
				return base.GetSystemInt32(AgendaComeCotasMetadata.ColumnNames.AliquotaIR);
			}
			
			set
			{
				base.SetSystemInt32(AgendaComeCotasMetadata.ColumnNames.AliquotaIR, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaComeCotas.TipoPosicao
		/// </summary>
		virtual public System.Int32? TipoPosicao
		{
			get
			{
				return base.GetSystemInt32(AgendaComeCotasMetadata.ColumnNames.TipoPosicao);
			}
			
			set
			{
				base.SetSystemInt32(AgendaComeCotasMetadata.ColumnNames.TipoPosicao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAgendaComeCotas entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdAgendamentoComeCotas
			{
				get
				{
					System.Int32? data = entity.IdAgendamentoComeCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgendamentoComeCotas = null;
					else entity.IdAgendamentoComeCotas = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataLancamento
			{
				get
				{
					System.DateTime? data = entity.DataLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLancamento = null;
					else entity.DataLancamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoEvento
			{
				get
				{
					System.Int32? data = entity.TipoEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoEvento = null;
					else entity.TipoEvento = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataUltimoIR
			{
				get
				{
					System.DateTime? data = entity.DataUltimoIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimoIR = null;
					else entity.DataUltimoIR = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeAntesCortes
			{
				get
				{
					System.Decimal? data = entity.QuantidadeAntesCortes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeAntesCortes = null;
					else entity.QuantidadeAntesCortes = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCotaAplic
			{
				get
				{
					System.Decimal? data = entity.ValorCotaAplic;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCotaAplic = null;
					else entity.ValorCotaAplic = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCotaUltimoPagamentoIR
			{
				get
				{
					System.Decimal? data = entity.ValorCotaUltimoPagamentoIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCotaUltimoPagamentoIR = null;
					else entity.ValorCotaUltimoPagamentoIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCota
			{
				get
				{
					System.Decimal? data = entity.ValorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCota = null;
					else entity.ValorCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String AliquotaCC
			{
				get
				{
					System.Decimal? data = entity.AliquotaCC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AliquotaCC = null;
					else entity.AliquotaCC = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoBrutoDesdeAplicacao
			{
				get
				{
					System.Decimal? data = entity.RendimentoBrutoDesdeAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoBrutoDesdeAplicacao = null;
					else entity.RendimentoBrutoDesdeAplicacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoDesdeUltimoPagamentoIR
			{
				get
				{
					System.Decimal? data = entity.RendimentoDesdeUltimoPagamentoIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoDesdeUltimoPagamentoIR = null;
					else entity.RendimentoDesdeUltimoPagamentoIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumDiasCorridosDesdeAquisicao
			{
				get
				{
					System.Int32? data = entity.NumDiasCorridosDesdeAquisicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumDiasCorridosDesdeAquisicao = null;
					else entity.NumDiasCorridosDesdeAquisicao = Convert.ToInt32(value);
				}
			}
				
			public System.String PrazoIOF
			{
				get
				{
					System.Int32? data = entity.PrazoIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoIOF = null;
					else entity.PrazoIOF = Convert.ToInt32(value);
				}
			}
				
			public System.String AliquotaIOF
			{
				get
				{
					System.Decimal? data = entity.AliquotaIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AliquotaIOF = null;
					else entity.AliquotaIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOFVirtual
			{
				get
				{
					System.Decimal? data = entity.ValorIOFVirtual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOFVirtual = null;
					else entity.ValorIOFVirtual = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrejuizoUsado
			{
				get
				{
					System.Decimal? data = entity.PrejuizoUsado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoUsado = null;
					else entity.PrejuizoUsado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoCompensado
			{
				get
				{
					System.Decimal? data = entity.RendimentoCompensado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoCompensado = null;
					else entity.RendimentoCompensado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIRAgendado
			{
				get
				{
					System.Decimal? data = entity.ValorIRAgendado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIRAgendado = null;
					else entity.ValorIRAgendado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIRPago
			{
				get
				{
					System.Decimal? data = entity.ValorIRPago;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIRPago = null;
					else entity.ValorIRPago = Convert.ToDecimal(value);
				}
			}
				
			public System.String Residuo15
			{
				get
				{
					System.Decimal? data = entity.Residuo15;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Residuo15 = null;
					else entity.Residuo15 = Convert.ToDecimal(value);
				}
			}
				
			public System.String Residuo175
			{
				get
				{
					System.Decimal? data = entity.Residuo175;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Residuo175 = null;
					else entity.Residuo175 = Convert.ToDecimal(value);
				}
			}
				
			public System.String Residuo20
			{
				get
				{
					System.Decimal? data = entity.Residuo20;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Residuo20 = null;
					else entity.Residuo20 = Convert.ToDecimal(value);
				}
			}
				
			public System.String Residuo225
			{
				get
				{
					System.Decimal? data = entity.Residuo225;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Residuo225 = null;
					else entity.Residuo225 = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeComida
			{
				get
				{
					System.Decimal? data = entity.QuantidadeComida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeComida = null;
					else entity.QuantidadeComida = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeFinal
			{
				get
				{
					System.Decimal? data = entity.QuantidadeFinal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeFinal = null;
					else entity.QuantidadeFinal = Convert.ToDecimal(value);
				}
			}
				
			public System.String ExecucaoRecolhimento
			{
				get
				{
					System.Int32? data = entity.ExecucaoRecolhimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExecucaoRecolhimento = null;
					else entity.ExecucaoRecolhimento = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoAliquotaIR
			{
				get
				{
					System.Int32? data = entity.TipoAliquotaIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoAliquotaIR = null;
					else entity.TipoAliquotaIR = Convert.ToInt32(value);
				}
			}
				
			public System.String AliquotaIR
			{
				get
				{
					System.Int32? data = entity.AliquotaIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AliquotaIR = null;
					else entity.AliquotaIR = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoPosicao
			{
				get
				{
					System.Int32? data = entity.TipoPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPosicao = null;
					else entity.TipoPosicao = Convert.ToInt32(value);
				}
			}
			

			private esAgendaComeCotas entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAgendaComeCotasQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAgendaComeCotas can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class AgendaComeCotas : esAgendaComeCotas
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__AgendaCom__IdCar__26068485
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAgendaComeCotasQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AgendaComeCotasMetadata.Meta();
			}
		}	
		

		public esQueryItem IdAgendamentoComeCotas
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.IdAgendamentoComeCotas, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataLancamento
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.DataLancamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoEvento
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.TipoEvento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataUltimoIR
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.DataUltimoIR, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeAntesCortes
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.QuantidadeAntesCortes, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCotaAplic
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.ValorCotaAplic, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCotaUltimoPagamentoIR
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.ValorCotaUltimoPagamentoIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCota
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.ValorCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AliquotaCC
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.AliquotaCC, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoBrutoDesdeAplicacao
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.RendimentoBrutoDesdeAplicacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoDesdeUltimoPagamentoIR
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.RendimentoDesdeUltimoPagamentoIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumDiasCorridosDesdeAquisicao
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.NumDiasCorridosDesdeAquisicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PrazoIOF
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.PrazoIOF, esSystemType.Int32);
			}
		} 
		
		public esQueryItem AliquotaIOF
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.AliquotaIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOFVirtual
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.ValorIOFVirtual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrejuizoUsado
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.PrejuizoUsado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoCompensado
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.RendimentoCompensado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIRAgendado
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.ValorIRAgendado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIRPago
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.ValorIRPago, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Residuo15
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.Residuo15, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Residuo175
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.Residuo175, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Residuo20
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.Residuo20, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Residuo225
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.Residuo225, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeComida
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.QuantidadeComida, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeFinal
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.QuantidadeFinal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ExecucaoRecolhimento
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.ExecucaoRecolhimento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoAliquotaIR
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.TipoAliquotaIR, esSystemType.Int32);
			}
		} 
		
		public esQueryItem AliquotaIR
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.AliquotaIR, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoPosicao
		{
			get
			{
				return new esQueryItem(this, AgendaComeCotasMetadata.ColumnNames.TipoPosicao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("AgendaComeCotasCollection")]
	public partial class AgendaComeCotasCollection : esAgendaComeCotasCollection, IEnumerable<AgendaComeCotas>
	{
		public AgendaComeCotasCollection()
		{

		}
		
		public static implicit operator List<AgendaComeCotas>(AgendaComeCotasCollection coll)
		{
			List<AgendaComeCotas> list = new List<AgendaComeCotas>();
			
			foreach (AgendaComeCotas emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AgendaComeCotasMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgendaComeCotasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new AgendaComeCotas(row);
		}

		override protected esEntity CreateEntity()
		{
			return new AgendaComeCotas();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AgendaComeCotasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgendaComeCotasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AgendaComeCotasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public AgendaComeCotas AddNew()
		{
			AgendaComeCotas entity = base.AddNewEntity() as AgendaComeCotas;
			
			return entity;
		}

		public AgendaComeCotas FindByPrimaryKey(System.Int32 idAgendamentoComeCotas)
		{
			return base.FindByPrimaryKey(idAgendamentoComeCotas) as AgendaComeCotas;
		}


		#region IEnumerable<AgendaComeCotas> Members

		IEnumerator<AgendaComeCotas> IEnumerable<AgendaComeCotas>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as AgendaComeCotas;
			}
		}

		#endregion
		
		private AgendaComeCotasQuery query;
	}


	/// <summary>
	/// Encapsulates the 'AgendaComeCotas' table
	/// </summary>

	[Serializable]
	public partial class AgendaComeCotas : esAgendaComeCotas
	{
		public AgendaComeCotas()
		{

		}
	
		public AgendaComeCotas(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AgendaComeCotasMetadata.Meta();
			}
		}
		
		
		
		override protected esAgendaComeCotasQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgendaComeCotasQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AgendaComeCotasQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgendaComeCotasQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AgendaComeCotasQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AgendaComeCotasQuery query;
	}



	[Serializable]
	public partial class AgendaComeCotasQuery : esAgendaComeCotasQuery
	{
		public AgendaComeCotasQuery()
		{

		}		
		
		public AgendaComeCotasQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AgendaComeCotasMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AgendaComeCotasMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.IdAgendamentoComeCotas, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.IdAgendamentoComeCotas;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.DataLancamento, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.DataLancamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.DataVencimento, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.TipoEvento, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.TipoEvento;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.IdPosicao, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.IdPosicao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.DataUltimoIR, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.DataUltimoIR;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.Quantidade, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.QuantidadeAntesCortes, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.QuantidadeAntesCortes;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.ValorCotaAplic, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.ValorCotaAplic;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.ValorCotaUltimoPagamentoIR, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.ValorCotaUltimoPagamentoIR;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.ValorCota, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.ValorCota;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.AliquotaCC, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.AliquotaCC;	
			c.NumericPrecision = 5;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.RendimentoBrutoDesdeAplicacao, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.RendimentoBrutoDesdeAplicacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.RendimentoDesdeUltimoPagamentoIR, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.RendimentoDesdeUltimoPagamentoIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.NumDiasCorridosDesdeAquisicao, 15, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.NumDiasCorridosDesdeAquisicao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.PrazoIOF, 16, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.PrazoIOF;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.AliquotaIOF, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.AliquotaIOF;	
			c.NumericPrecision = 5;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.ValorIOF, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.ValorIOFVirtual, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.ValorIOFVirtual;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.PrejuizoUsado, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.PrejuizoUsado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.RendimentoCompensado, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.RendimentoCompensado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.ValorIRAgendado, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.ValorIRAgendado;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.ValorIRPago, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.ValorIRPago;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.Residuo15, 24, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.Residuo15;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.Residuo175, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.Residuo175;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.Residuo20, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.Residuo20;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.Residuo225, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.Residuo225;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.QuantidadeComida, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.QuantidadeComida;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.QuantidadeFinal, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.QuantidadeFinal;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.ExecucaoRecolhimento, 30, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.ExecucaoRecolhimento;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.TipoAliquotaIR, 31, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.TipoAliquotaIR;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.AliquotaIR, 32, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.AliquotaIR;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaComeCotasMetadata.ColumnNames.TipoPosicao, 33, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaComeCotasMetadata.PropertyNames.TipoPosicao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public AgendaComeCotasMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdAgendamentoComeCotas = "IdAgendamentoComeCotas";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataVencimento = "DataVencimento";
			 public const string TipoEvento = "TipoEvento";
			 public const string IdPosicao = "IdPosicao";
			 public const string DataUltimoIR = "DataUltimoIR";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeAntesCortes = "QuantidadeAntesCortes";
			 public const string ValorCotaAplic = "ValorCotaAplic";
			 public const string ValorCotaUltimoPagamentoIR = "ValorCotaUltimoPagamentoIR";
			 public const string ValorCota = "ValorCota";
			 public const string AliquotaCC = "AliquotaCC";
			 public const string RendimentoBrutoDesdeAplicacao = "RendimentoBrutoDesdeAplicacao";
			 public const string RendimentoDesdeUltimoPagamentoIR = "RendimentoDesdeUltimoPagamentoIR";
			 public const string NumDiasCorridosDesdeAquisicao = "NumDiasCorridosDesdeAquisicao";
			 public const string PrazoIOF = "PrazoIOF";
			 public const string AliquotaIOF = "AliquotaIOF";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorIOFVirtual = "ValorIOFVirtual";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoCompensado = "RendimentoCompensado";
			 public const string ValorIRAgendado = "ValorIRAgendado";
			 public const string ValorIRPago = "ValorIRPago";
			 public const string Residuo15 = "Residuo15";
			 public const string Residuo175 = "Residuo175";
			 public const string Residuo20 = "Residuo20";
			 public const string Residuo225 = "Residuo225";
			 public const string QuantidadeComida = "QuantidadeComida";
			 public const string QuantidadeFinal = "QuantidadeFinal";
			 public const string ExecucaoRecolhimento = "ExecucaoRecolhimento";
			 public const string TipoAliquotaIR = "TipoAliquotaIR";
			 public const string AliquotaIR = "AliquotaIR";
			 public const string TipoPosicao = "TipoPosicao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdAgendamentoComeCotas = "IdAgendamentoComeCotas";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataVencimento = "DataVencimento";
			 public const string TipoEvento = "TipoEvento";
			 public const string IdPosicao = "IdPosicao";
			 public const string DataUltimoIR = "DataUltimoIR";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeAntesCortes = "QuantidadeAntesCortes";
			 public const string ValorCotaAplic = "ValorCotaAplic";
			 public const string ValorCotaUltimoPagamentoIR = "ValorCotaUltimoPagamentoIR";
			 public const string ValorCota = "ValorCota";
			 public const string AliquotaCC = "AliquotaCC";
			 public const string RendimentoBrutoDesdeAplicacao = "RendimentoBrutoDesdeAplicacao";
			 public const string RendimentoDesdeUltimoPagamentoIR = "RendimentoDesdeUltimoPagamentoIR";
			 public const string NumDiasCorridosDesdeAquisicao = "NumDiasCorridosDesdeAquisicao";
			 public const string PrazoIOF = "PrazoIOF";
			 public const string AliquotaIOF = "AliquotaIOF";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorIOFVirtual = "ValorIOFVirtual";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoCompensado = "RendimentoCompensado";
			 public const string ValorIRAgendado = "ValorIRAgendado";
			 public const string ValorIRPago = "ValorIRPago";
			 public const string Residuo15 = "Residuo15";
			 public const string Residuo175 = "Residuo175";
			 public const string Residuo20 = "Residuo20";
			 public const string Residuo225 = "Residuo225";
			 public const string QuantidadeComida = "QuantidadeComida";
			 public const string QuantidadeFinal = "QuantidadeFinal";
			 public const string ExecucaoRecolhimento = "ExecucaoRecolhimento";
			 public const string TipoAliquotaIR = "TipoAliquotaIR";
			 public const string AliquotaIR = "AliquotaIR";
			 public const string TipoPosicao = "TipoPosicao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AgendaComeCotasMetadata))
			{
				if(AgendaComeCotasMetadata.mapDelegates == null)
				{
					AgendaComeCotasMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AgendaComeCotasMetadata.meta == null)
				{
					AgendaComeCotasMetadata.meta = new AgendaComeCotasMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdAgendamentoComeCotas", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataLancamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoEvento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataUltimoIR", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeAntesCortes", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCotaAplic", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCotaUltimoPagamentoIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AliquotaCC", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoBrutoDesdeAplicacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoDesdeUltimoPagamentoIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("NumDiasCorridosDesdeAquisicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PrazoIOF", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AliquotaIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOFVirtual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrejuizoUsado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoCompensado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIRAgendado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIRPago", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Residuo15", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Residuo175", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Residuo20", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Residuo225", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeComida", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeFinal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ExecucaoRecolhimento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoAliquotaIR", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AliquotaIR", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoPosicao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "AgendaComeCotas";
				meta.Destination = "AgendaComeCotas";
				
				meta.spInsert = "proc_AgendaComeCotasInsert";				
				meta.spUpdate = "proc_AgendaComeCotasUpdate";		
				meta.spDelete = "proc_AgendaComeCotasDelete";
				meta.spLoadAll = "proc_AgendaComeCotasLoadAll";
				meta.spLoadByPrimaryKey = "proc_AgendaComeCotasLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AgendaComeCotasMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
