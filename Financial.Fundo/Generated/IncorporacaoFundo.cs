/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:18 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esIncorporacaoFundoCollection : esEntityCollection
	{
		public esIncorporacaoFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "IncorporacaoFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esIncorporacaoFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esIncorporacaoFundoQuery);
		}
		#endregion
		
		virtual public IncorporacaoFundo DetachEntity(IncorporacaoFundo entity)
		{
			return base.DetachEntity(entity) as IncorporacaoFundo;
		}
		
		virtual public IncorporacaoFundo AttachEntity(IncorporacaoFundo entity)
		{
			return base.AttachEntity(entity) as IncorporacaoFundo;
		}
		
		virtual public void Combine(IncorporacaoFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public IncorporacaoFundo this[int index]
		{
			get
			{
				return base[index] as IncorporacaoFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(IncorporacaoFundo);
		}
	}



	[Serializable]
	abstract public class esIncorporacaoFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esIncorporacaoFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esIncorporacaoFundo()
		{

		}

		public esIncorporacaoFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCarteiraOrigem, System.Int32 idCarteiraDestino, System.DateTime data)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteiraOrigem, idCarteiraDestino, data);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteiraOrigem, idCarteiraDestino, data);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCarteiraOrigem, System.Int32 idCarteiraDestino, System.DateTime data)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esIncorporacaoFundoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCarteiraOrigem == idCarteiraOrigem, query.IdCarteiraDestino == idCarteiraDestino, query.Data == data);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCarteiraOrigem, System.Int32 idCarteiraDestino, System.DateTime data)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteiraOrigem, idCarteiraDestino, data);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteiraOrigem, idCarteiraDestino, data);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCarteiraOrigem, System.Int32 idCarteiraDestino, System.DateTime data)
		{
			esIncorporacaoFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdCarteiraOrigem == idCarteiraOrigem, query.IdCarteiraDestino == idCarteiraDestino, query.Data == data);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCarteiraOrigem, System.Int32 idCarteiraDestino, System.DateTime data)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCarteiraOrigem",idCarteiraOrigem);			parms.Add("IdCarteiraDestino",idCarteiraDestino);			parms.Add("Data",data);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteiraOrigem": this.str.IdCarteiraOrigem = (string)value; break;							
						case "IdCarteiraDestino": this.str.IdCarteiraDestino = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteiraOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraOrigem = (System.Int32?)value;
							break;
						
						case "IdCarteiraDestino":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraDestino = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to IncorporacaoFundo.IdCarteiraOrigem
		/// </summary>
		virtual public System.Int32? IdCarteiraOrigem
		{
			get
			{
				return base.GetSystemInt32(IncorporacaoFundoMetadata.ColumnNames.IdCarteiraOrigem);
			}
			
			set
			{
				base.SetSystemInt32(IncorporacaoFundoMetadata.ColumnNames.IdCarteiraOrigem, value);
			}
		}
		
		/// <summary>
		/// Maps to IncorporacaoFundo.IdCarteiraDestino
		/// </summary>
		virtual public System.Int32? IdCarteiraDestino
		{
			get
			{
				return base.GetSystemInt32(IncorporacaoFundoMetadata.ColumnNames.IdCarteiraDestino);
			}
			
			set
			{
				base.SetSystemInt32(IncorporacaoFundoMetadata.ColumnNames.IdCarteiraDestino, value);
			}
		}
		
		/// <summary>
		/// Maps to IncorporacaoFundo.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(IncorporacaoFundoMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(IncorporacaoFundoMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to IncorporacaoFundo.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(IncorporacaoFundoMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(IncorporacaoFundoMetadata.ColumnNames.Taxa, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esIncorporacaoFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteiraOrigem
			{
				get
				{
					System.Int32? data = entity.IdCarteiraOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraOrigem = null;
					else entity.IdCarteiraOrigem = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteiraDestino
			{
				get
				{
					System.Int32? data = entity.IdCarteiraDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraDestino = null;
					else entity.IdCarteiraDestino = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
			

			private esIncorporacaoFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esIncorporacaoFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esIncorporacaoFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class IncorporacaoFundo : esIncorporacaoFundo
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esIncorporacaoFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return IncorporacaoFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteiraOrigem
		{
			get
			{
				return new esQueryItem(this, IncorporacaoFundoMetadata.ColumnNames.IdCarteiraOrigem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteiraDestino
		{
			get
			{
				return new esQueryItem(this, IncorporacaoFundoMetadata.ColumnNames.IdCarteiraDestino, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, IncorporacaoFundoMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, IncorporacaoFundoMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("IncorporacaoFundoCollection")]
	public partial class IncorporacaoFundoCollection : esIncorporacaoFundoCollection, IEnumerable<IncorporacaoFundo>
	{
		public IncorporacaoFundoCollection()
		{

		}
		
		public static implicit operator List<IncorporacaoFundo>(IncorporacaoFundoCollection coll)
		{
			List<IncorporacaoFundo> list = new List<IncorporacaoFundo>();
			
			foreach (IncorporacaoFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  IncorporacaoFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IncorporacaoFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new IncorporacaoFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new IncorporacaoFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public IncorporacaoFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IncorporacaoFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(IncorporacaoFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public IncorporacaoFundo AddNew()
		{
			IncorporacaoFundo entity = base.AddNewEntity() as IncorporacaoFundo;
			
			return entity;
		}

		public IncorporacaoFundo FindByPrimaryKey(System.Int32 idCarteiraOrigem, System.Int32 idCarteiraDestino, System.DateTime data)
		{
			return base.FindByPrimaryKey(idCarteiraOrigem, idCarteiraDestino, data) as IncorporacaoFundo;
		}


		#region IEnumerable<IncorporacaoFundo> Members

		IEnumerator<IncorporacaoFundo> IEnumerable<IncorporacaoFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as IncorporacaoFundo;
			}
		}

		#endregion
		
		private IncorporacaoFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'IncorporacaoFundo' table
	/// </summary>

	[Serializable]
	public partial class IncorporacaoFundo : esIncorporacaoFundo
	{
		public IncorporacaoFundo()
		{

		}
	
		public IncorporacaoFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return IncorporacaoFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esIncorporacaoFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IncorporacaoFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public IncorporacaoFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IncorporacaoFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(IncorporacaoFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private IncorporacaoFundoQuery query;
	}



	[Serializable]
	public partial class IncorporacaoFundoQuery : esIncorporacaoFundoQuery
	{
		public IncorporacaoFundoQuery()
		{

		}		
		
		public IncorporacaoFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class IncorporacaoFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected IncorporacaoFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(IncorporacaoFundoMetadata.ColumnNames.IdCarteiraOrigem, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IncorporacaoFundoMetadata.PropertyNames.IdCarteiraOrigem;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IncorporacaoFundoMetadata.ColumnNames.IdCarteiraDestino, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IncorporacaoFundoMetadata.PropertyNames.IdCarteiraDestino;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IncorporacaoFundoMetadata.ColumnNames.Data, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = IncorporacaoFundoMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IncorporacaoFundoMetadata.ColumnNames.Taxa, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IncorporacaoFundoMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 25;
			c.NumericScale = 18;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public IncorporacaoFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteiraOrigem = "IdCarteiraOrigem";
			 public const string IdCarteiraDestino = "IdCarteiraDestino";
			 public const string Data = "Data";
			 public const string Taxa = "Taxa";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteiraOrigem = "IdCarteiraOrigem";
			 public const string IdCarteiraDestino = "IdCarteiraDestino";
			 public const string Data = "Data";
			 public const string Taxa = "Taxa";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(IncorporacaoFundoMetadata))
			{
				if(IncorporacaoFundoMetadata.mapDelegates == null)
				{
					IncorporacaoFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (IncorporacaoFundoMetadata.meta == null)
				{
					IncorporacaoFundoMetadata.meta = new IncorporacaoFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteiraOrigem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteiraDestino", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "IncorporacaoFundo";
				meta.Destination = "IncorporacaoFundo";
				
				meta.spInsert = "proc_IncorporacaoFundoInsert";				
				meta.spUpdate = "proc_IncorporacaoFundoUpdate";		
				meta.spDelete = "proc_IncorporacaoFundoDelete";
				meta.spLoadAll = "proc_IncorporacaoFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_IncorporacaoFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private IncorporacaoFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
