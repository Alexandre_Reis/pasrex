/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 31/07/2015 16:30:52
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esPatrimonioFundoEmissaoSerieCollection : esEntityCollection
	{
		public esPatrimonioFundoEmissaoSerieCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PatrimonioFundoEmissaoSerieCollection";
		}

		#region Query Logic
		protected void InitQuery(esPatrimonioFundoEmissaoSerieQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPatrimonioFundoEmissaoSerieQuery);
		}
		#endregion
		
		virtual public PatrimonioFundoEmissaoSerie DetachEntity(PatrimonioFundoEmissaoSerie entity)
		{
			return base.DetachEntity(entity) as PatrimonioFundoEmissaoSerie;
		}
		
		virtual public PatrimonioFundoEmissaoSerie AttachEntity(PatrimonioFundoEmissaoSerie entity)
		{
			return base.AttachEntity(entity) as PatrimonioFundoEmissaoSerie;
		}
		
		virtual public void Combine(PatrimonioFundoEmissaoSerieCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PatrimonioFundoEmissaoSerie this[int index]
		{
			get
			{
				return base[index] as PatrimonioFundoEmissaoSerie;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PatrimonioFundoEmissaoSerie);
		}
	}



	[Serializable]
	abstract public class esPatrimonioFundoEmissaoSerie : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPatrimonioFundoEmissaoSerieQuery GetDynamicQuery()
		{
			return null;
		}

		public esPatrimonioFundoEmissaoSerie()
		{

		}

		public esPatrimonioFundoEmissaoSerie(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime competencia, System.Int32 idCarteira)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(competencia, idCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(competencia, idCarteira);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime competencia, System.Int32 idCarteira)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(competencia, idCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(competencia, idCarteira);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime competencia, System.Int32 idCarteira)
		{
			esPatrimonioFundoEmissaoSerieQuery query = this.GetDynamicQuery();
			query.Where(query.Competencia == competencia, query.IdCarteira == idCarteira);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime competencia, System.Int32 idCarteira)
		{
			esParameters parms = new esParameters();
			parms.Add("Competencia",competencia);			parms.Add("IdCarteira",idCarteira);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "Competencia": this.str.Competencia = (string)value; break;							
						case "DataBaseCota": this.str.DataBaseCota = (string)value; break;							
						case "DataCotaEx": this.str.DataCotaEx = (string)value; break;							
						case "PatrimonioLiquido": this.str.PatrimonioLiquido = (string)value; break;							
						case "QuantidadeCotas": this.str.QuantidadeCotas = (string)value; break;							
						case "VlCotaPatrimonial": this.str.VlCotaPatrimonial = (string)value; break;							
						case "VlRendimentoDistribuido": this.str.VlRendimentoDistribuido = (string)value; break;							
						case "VlRendimentoPorCota": this.str.VlRendimentoPorCota = (string)value; break;							
						case "VlAmortizacao": this.str.VlAmortizacao = (string)value; break;							
						case "VlAmortizacaoPorCota": this.str.VlAmortizacaoPorCota = (string)value; break;							
						case "VlDividendoDistribuido": this.str.VlDividendoDistribuido = (string)value; break;							
						case "VlDividendoPorCota": this.str.VlDividendoPorCota = (string)value; break;							
						case "CotaRendimento": this.str.CotaRendimento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "Competencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Competencia = (System.DateTime?)value;
							break;
						
						case "DataBaseCota":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataBaseCota = (System.DateTime?)value;
							break;
						
						case "DataCotaEx":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataCotaEx = (System.DateTime?)value;
							break;
						
						case "PatrimonioLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PatrimonioLiquido = (System.Decimal?)value;
							break;
						
						case "QuantidadeCotas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeCotas = (System.Decimal?)value;
							break;
						
						case "VlCotaPatrimonial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlCotaPatrimonial = (System.Decimal?)value;
							break;
						
						case "VlRendimentoDistribuido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlRendimentoDistribuido = (System.Decimal?)value;
							break;
						
						case "VlRendimentoPorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlRendimentoPorCota = (System.Decimal?)value;
							break;
						
						case "VlAmortizacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlAmortizacao = (System.Decimal?)value;
							break;
						
						case "VlAmortizacaoPorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlAmortizacaoPorCota = (System.Decimal?)value;
							break;
						
						case "VlDividendoDistribuido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlDividendoDistribuido = (System.Decimal?)value;
							break;
						
						case "VlDividendoPorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VlDividendoPorCota = (System.Decimal?)value;
							break;
						
						case "CotaRendimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaRendimento = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PatrimonioFundoEmissaoSerie.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PatrimonioFundoEmissaoSerie.Competencia
		/// </summary>
		virtual public System.DateTime? Competencia
		{
			get
			{
				return base.GetSystemDateTime(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.Competencia);
			}
			
			set
			{
				base.SetSystemDateTime(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.Competencia, value);
			}
		}
		
		/// <summary>
		/// Maps to PatrimonioFundoEmissaoSerie.DataBaseCota
		/// </summary>
		virtual public System.DateTime? DataBaseCota
		{
			get
			{
				return base.GetSystemDateTime(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.DataBaseCota);
			}
			
			set
			{
				base.SetSystemDateTime(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.DataBaseCota, value);
			}
		}
		
		/// <summary>
		/// Maps to PatrimonioFundoEmissaoSerie.DataCotaEx
		/// </summary>
		virtual public System.DateTime? DataCotaEx
		{
			get
			{
				return base.GetSystemDateTime(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.DataCotaEx);
			}
			
			set
			{
				base.SetSystemDateTime(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.DataCotaEx, value);
			}
		}
		
		/// <summary>
		/// Maps to PatrimonioFundoEmissaoSerie.PatrimonioLiquido
		/// </summary>
		virtual public System.Decimal? PatrimonioLiquido
		{
			get
			{
				return base.GetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.PatrimonioLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.PatrimonioLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to PatrimonioFundoEmissaoSerie.QuantidadeCotas
		/// </summary>
		virtual public System.Decimal? QuantidadeCotas
		{
			get
			{
				return base.GetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.QuantidadeCotas);
			}
			
			set
			{
				base.SetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.QuantidadeCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to PatrimonioFundoEmissaoSerie.VlCotaPatrimonial
		/// </summary>
		virtual public System.Decimal? VlCotaPatrimonial
		{
			get
			{
				return base.GetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlCotaPatrimonial);
			}
			
			set
			{
				base.SetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlCotaPatrimonial, value);
			}
		}
		
		/// <summary>
		/// Maps to PatrimonioFundoEmissaoSerie.VlRendimentoDistribuido
		/// </summary>
		virtual public System.Decimal? VlRendimentoDistribuido
		{
			get
			{
				return base.GetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlRendimentoDistribuido);
			}
			
			set
			{
				base.SetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlRendimentoDistribuido, value);
			}
		}
		
		/// <summary>
		/// Maps to PatrimonioFundoEmissaoSerie.VlRendimentoPorCota
		/// </summary>
		virtual public System.Decimal? VlRendimentoPorCota
		{
			get
			{
				return base.GetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlRendimentoPorCota);
			}
			
			set
			{
				base.SetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlRendimentoPorCota, value);
			}
		}
		
		/// <summary>
		/// Maps to PatrimonioFundoEmissaoSerie.VlAmortizacao
		/// </summary>
		virtual public System.Decimal? VlAmortizacao
		{
			get
			{
				return base.GetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlAmortizacao);
			}
			
			set
			{
				base.SetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlAmortizacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PatrimonioFundoEmissaoSerie.VlAmortizacaoPorCota
		/// </summary>
		virtual public System.Decimal? VlAmortizacaoPorCota
		{
			get
			{
				return base.GetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlAmortizacaoPorCota);
			}
			
			set
			{
				base.SetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlAmortizacaoPorCota, value);
			}
		}
		
		/// <summary>
		/// Maps to PatrimonioFundoEmissaoSerie.VlDividendoDistribuido
		/// </summary>
		virtual public System.Decimal? VlDividendoDistribuido
		{
			get
			{
				return base.GetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlDividendoDistribuido);
			}
			
			set
			{
				base.SetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlDividendoDistribuido, value);
			}
		}
		
		/// <summary>
		/// Maps to PatrimonioFundoEmissaoSerie.VlDividendoPorCota
		/// </summary>
		virtual public System.Decimal? VlDividendoPorCota
		{
			get
			{
				return base.GetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlDividendoPorCota);
			}
			
			set
			{
				base.SetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlDividendoPorCota, value);
			}
		}
		
		/// <summary>
		/// Maps to PatrimonioFundoEmissaoSerie.CotaRendimento
		/// </summary>
		virtual public System.Decimal? CotaRendimento
		{
			get
			{
				return base.GetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.CotaRendimento);
			}
			
			set
			{
				base.SetSystemDecimal(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.CotaRendimento, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPatrimonioFundoEmissaoSerie entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String Competencia
			{
				get
				{
					System.DateTime? data = entity.Competencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Competencia = null;
					else entity.Competencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataBaseCota
			{
				get
				{
					System.DateTime? data = entity.DataBaseCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataBaseCota = null;
					else entity.DataBaseCota = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataCotaEx
			{
				get
				{
					System.DateTime? data = entity.DataCotaEx;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataCotaEx = null;
					else entity.DataCotaEx = Convert.ToDateTime(value);
				}
			}
				
			public System.String PatrimonioLiquido
			{
				get
				{
					System.Decimal? data = entity.PatrimonioLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PatrimonioLiquido = null;
					else entity.PatrimonioLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeCotas
			{
				get
				{
					System.Decimal? data = entity.QuantidadeCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeCotas = null;
					else entity.QuantidadeCotas = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlCotaPatrimonial
			{
				get
				{
					System.Decimal? data = entity.VlCotaPatrimonial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlCotaPatrimonial = null;
					else entity.VlCotaPatrimonial = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlRendimentoDistribuido
			{
				get
				{
					System.Decimal? data = entity.VlRendimentoDistribuido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlRendimentoDistribuido = null;
					else entity.VlRendimentoDistribuido = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlRendimentoPorCota
			{
				get
				{
					System.Decimal? data = entity.VlRendimentoPorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlRendimentoPorCota = null;
					else entity.VlRendimentoPorCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlAmortizacao
			{
				get
				{
					System.Decimal? data = entity.VlAmortizacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlAmortizacao = null;
					else entity.VlAmortizacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlAmortizacaoPorCota
			{
				get
				{
					System.Decimal? data = entity.VlAmortizacaoPorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlAmortizacaoPorCota = null;
					else entity.VlAmortizacaoPorCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlDividendoDistribuido
			{
				get
				{
					System.Decimal? data = entity.VlDividendoDistribuido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlDividendoDistribuido = null;
					else entity.VlDividendoDistribuido = Convert.ToDecimal(value);
				}
			}
				
			public System.String VlDividendoPorCota
			{
				get
				{
					System.Decimal? data = entity.VlDividendoPorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VlDividendoPorCota = null;
					else entity.VlDividendoPorCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaRendimento
			{
				get
				{
					System.Decimal? data = entity.CotaRendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaRendimento = null;
					else entity.CotaRendimento = Convert.ToDecimal(value);
				}
			}
			

			private esPatrimonioFundoEmissaoSerie entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPatrimonioFundoEmissaoSerieQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPatrimonioFundoEmissaoSerie can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PatrimonioFundoEmissaoSerie : esPatrimonioFundoEmissaoSerie
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PatrimonioFundoEmissaoSerie_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPatrimonioFundoEmissaoSerieQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PatrimonioFundoEmissaoSerieMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, PatrimonioFundoEmissaoSerieMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Competencia
		{
			get
			{
				return new esQueryItem(this, PatrimonioFundoEmissaoSerieMetadata.ColumnNames.Competencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataBaseCota
		{
			get
			{
				return new esQueryItem(this, PatrimonioFundoEmissaoSerieMetadata.ColumnNames.DataBaseCota, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataCotaEx
		{
			get
			{
				return new esQueryItem(this, PatrimonioFundoEmissaoSerieMetadata.ColumnNames.DataCotaEx, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PatrimonioLiquido
		{
			get
			{
				return new esQueryItem(this, PatrimonioFundoEmissaoSerieMetadata.ColumnNames.PatrimonioLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeCotas
		{
			get
			{
				return new esQueryItem(this, PatrimonioFundoEmissaoSerieMetadata.ColumnNames.QuantidadeCotas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlCotaPatrimonial
		{
			get
			{
				return new esQueryItem(this, PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlCotaPatrimonial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlRendimentoDistribuido
		{
			get
			{
				return new esQueryItem(this, PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlRendimentoDistribuido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlRendimentoPorCota
		{
			get
			{
				return new esQueryItem(this, PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlRendimentoPorCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlAmortizacao
		{
			get
			{
				return new esQueryItem(this, PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlAmortizacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlAmortizacaoPorCota
		{
			get
			{
				return new esQueryItem(this, PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlAmortizacaoPorCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlDividendoDistribuido
		{
			get
			{
				return new esQueryItem(this, PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlDividendoDistribuido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VlDividendoPorCota
		{
			get
			{
				return new esQueryItem(this, PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlDividendoPorCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaRendimento
		{
			get
			{
				return new esQueryItem(this, PatrimonioFundoEmissaoSerieMetadata.ColumnNames.CotaRendimento, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PatrimonioFundoEmissaoSerieCollection")]
	public partial class PatrimonioFundoEmissaoSerieCollection : esPatrimonioFundoEmissaoSerieCollection, IEnumerable<PatrimonioFundoEmissaoSerie>
	{
		public PatrimonioFundoEmissaoSerieCollection()
		{

		}
		
		public static implicit operator List<PatrimonioFundoEmissaoSerie>(PatrimonioFundoEmissaoSerieCollection coll)
		{
			List<PatrimonioFundoEmissaoSerie> list = new List<PatrimonioFundoEmissaoSerie>();
			
			foreach (PatrimonioFundoEmissaoSerie emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PatrimonioFundoEmissaoSerieMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PatrimonioFundoEmissaoSerieQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PatrimonioFundoEmissaoSerie(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PatrimonioFundoEmissaoSerie();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PatrimonioFundoEmissaoSerieQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PatrimonioFundoEmissaoSerieQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PatrimonioFundoEmissaoSerieQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PatrimonioFundoEmissaoSerie AddNew()
		{
			PatrimonioFundoEmissaoSerie entity = base.AddNewEntity() as PatrimonioFundoEmissaoSerie;
			
			return entity;
		}

		public PatrimonioFundoEmissaoSerie FindByPrimaryKey(System.DateTime competencia, System.Int32 idCarteira)
		{
			return base.FindByPrimaryKey(competencia, idCarteira) as PatrimonioFundoEmissaoSerie;
		}


		#region IEnumerable<PatrimonioFundoEmissaoSerie> Members

		IEnumerator<PatrimonioFundoEmissaoSerie> IEnumerable<PatrimonioFundoEmissaoSerie>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PatrimonioFundoEmissaoSerie;
			}
		}

		#endregion
		
		private PatrimonioFundoEmissaoSerieQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PatrimonioFundoEmissaoSerie' table
	/// </summary>

	[Serializable]
	public partial class PatrimonioFundoEmissaoSerie : esPatrimonioFundoEmissaoSerie
	{
		public PatrimonioFundoEmissaoSerie()
		{

		}
	
		public PatrimonioFundoEmissaoSerie(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PatrimonioFundoEmissaoSerieMetadata.Meta();
			}
		}
		
		
		
		override protected esPatrimonioFundoEmissaoSerieQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PatrimonioFundoEmissaoSerieQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PatrimonioFundoEmissaoSerieQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PatrimonioFundoEmissaoSerieQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PatrimonioFundoEmissaoSerieQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PatrimonioFundoEmissaoSerieQuery query;
	}



	[Serializable]
	public partial class PatrimonioFundoEmissaoSerieQuery : esPatrimonioFundoEmissaoSerieQuery
	{
		public PatrimonioFundoEmissaoSerieQuery()
		{

		}		
		
		public PatrimonioFundoEmissaoSerieQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PatrimonioFundoEmissaoSerieMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PatrimonioFundoEmissaoSerieMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.IdCarteira, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PatrimonioFundoEmissaoSerieMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.Competencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PatrimonioFundoEmissaoSerieMetadata.PropertyNames.Competencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.DataBaseCota, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PatrimonioFundoEmissaoSerieMetadata.PropertyNames.DataBaseCota;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.DataCotaEx, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PatrimonioFundoEmissaoSerieMetadata.PropertyNames.DataCotaEx;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.PatrimonioLiquido, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PatrimonioFundoEmissaoSerieMetadata.PropertyNames.PatrimonioLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.QuantidadeCotas, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PatrimonioFundoEmissaoSerieMetadata.PropertyNames.QuantidadeCotas;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlCotaPatrimonial, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PatrimonioFundoEmissaoSerieMetadata.PropertyNames.VlCotaPatrimonial;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlRendimentoDistribuido, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PatrimonioFundoEmissaoSerieMetadata.PropertyNames.VlRendimentoDistribuido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlRendimentoPorCota, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PatrimonioFundoEmissaoSerieMetadata.PropertyNames.VlRendimentoPorCota;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlAmortizacao, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PatrimonioFundoEmissaoSerieMetadata.PropertyNames.VlAmortizacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlAmortizacaoPorCota, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PatrimonioFundoEmissaoSerieMetadata.PropertyNames.VlAmortizacaoPorCota;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlDividendoDistribuido, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PatrimonioFundoEmissaoSerieMetadata.PropertyNames.VlDividendoDistribuido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.VlDividendoPorCota, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PatrimonioFundoEmissaoSerieMetadata.PropertyNames.VlDividendoPorCota;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PatrimonioFundoEmissaoSerieMetadata.ColumnNames.CotaRendimento, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PatrimonioFundoEmissaoSerieMetadata.PropertyNames.CotaRendimento;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PatrimonioFundoEmissaoSerieMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string Competencia = "Competencia";
			 public const string DataBaseCota = "DataBaseCota";
			 public const string DataCotaEx = "DataCotaEx";
			 public const string PatrimonioLiquido = "PatrimonioLiquido";
			 public const string QuantidadeCotas = "QuantidadeCotas";
			 public const string VlCotaPatrimonial = "VlCotaPatrimonial";
			 public const string VlRendimentoDistribuido = "VlRendimentoDistribuido";
			 public const string VlRendimentoPorCota = "VlRendimentoPorCota";
			 public const string VlAmortizacao = "VlAmortizacao";
			 public const string VlAmortizacaoPorCota = "VlAmortizacaoPorCota";
			 public const string VlDividendoDistribuido = "VlDividendoDistribuido";
			 public const string VlDividendoPorCota = "VlDividendoPorCota";
			 public const string CotaRendimento = "CotaRendimento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string Competencia = "Competencia";
			 public const string DataBaseCota = "DataBaseCota";
			 public const string DataCotaEx = "DataCotaEx";
			 public const string PatrimonioLiquido = "PatrimonioLiquido";
			 public const string QuantidadeCotas = "QuantidadeCotas";
			 public const string VlCotaPatrimonial = "VlCotaPatrimonial";
			 public const string VlRendimentoDistribuido = "VlRendimentoDistribuido";
			 public const string VlRendimentoPorCota = "VlRendimentoPorCota";
			 public const string VlAmortizacao = "VlAmortizacao";
			 public const string VlAmortizacaoPorCota = "VlAmortizacaoPorCota";
			 public const string VlDividendoDistribuido = "VlDividendoDistribuido";
			 public const string VlDividendoPorCota = "VlDividendoPorCota";
			 public const string CotaRendimento = "CotaRendimento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PatrimonioFundoEmissaoSerieMetadata))
			{
				if(PatrimonioFundoEmissaoSerieMetadata.mapDelegates == null)
				{
					PatrimonioFundoEmissaoSerieMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PatrimonioFundoEmissaoSerieMetadata.meta == null)
				{
					PatrimonioFundoEmissaoSerieMetadata.meta = new PatrimonioFundoEmissaoSerieMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Competencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataBaseCota", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataCotaEx", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PatrimonioLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeCotas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VlCotaPatrimonial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VlRendimentoDistribuido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VlRendimentoPorCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VlAmortizacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VlAmortizacaoPorCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VlDividendoDistribuido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VlDividendoPorCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaRendimento", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "PatrimonioFundoEmissaoSerie";
				meta.Destination = "PatrimonioFundoEmissaoSerie";
				
				meta.spInsert = "proc_PatrimonioFundoEmissaoSerieInsert";				
				meta.spUpdate = "proc_PatrimonioFundoEmissaoSerieUpdate";		
				meta.spDelete = "proc_PatrimonioFundoEmissaoSerieDelete";
				meta.spLoadAll = "proc_PatrimonioFundoEmissaoSerieLoadAll";
				meta.spLoadByPrimaryKey = "proc_PatrimonioFundoEmissaoSerieLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PatrimonioFundoEmissaoSerieMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
