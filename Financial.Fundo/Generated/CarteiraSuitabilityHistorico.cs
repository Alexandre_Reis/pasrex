/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/09/2015 14:48:41
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esCarteiraSuitabilityHistoricoCollection : esEntityCollection
	{
		public esCarteiraSuitabilityHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CarteiraSuitabilityHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esCarteiraSuitabilityHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCarteiraSuitabilityHistoricoQuery);
		}
		#endregion
		
		virtual public CarteiraSuitabilityHistorico DetachEntity(CarteiraSuitabilityHistorico entity)
		{
			return base.DetachEntity(entity) as CarteiraSuitabilityHistorico;
		}
		
		virtual public CarteiraSuitabilityHistorico AttachEntity(CarteiraSuitabilityHistorico entity)
		{
			return base.AttachEntity(entity) as CarteiraSuitabilityHistorico;
		}
		
		virtual public void Combine(CarteiraSuitabilityHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CarteiraSuitabilityHistorico this[int index]
		{
			get
			{
				return base[index] as CarteiraSuitabilityHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CarteiraSuitabilityHistorico);
		}
	}



	[Serializable]
	abstract public class esCarteiraSuitabilityHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCarteiraSuitabilityHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esCarteiraSuitabilityHistorico()
		{

		}

		public esCarteiraSuitabilityHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idCarteira)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idCarteira);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idCarteira)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idCarteira);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idCarteira)
		{
			esCarteiraSuitabilityHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdCarteira == idCarteira);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idCarteira)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdCarteira",idCarteira);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdPerfilRisco": this.str.IdPerfilRisco = (string)value; break;							
						case "PerfilRisco": this.str.PerfilRisco = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdPerfilRisco":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilRisco = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CarteiraSuitabilityHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(CarteiraSuitabilityHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(CarteiraSuitabilityHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraSuitabilityHistorico.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(CarteiraSuitabilityHistoricoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraSuitabilityHistoricoMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraSuitabilityHistorico.IdPerfilRisco
		/// </summary>
		virtual public System.Int32? IdPerfilRisco
		{
			get
			{
				return base.GetSystemInt32(CarteiraSuitabilityHistoricoMetadata.ColumnNames.IdPerfilRisco);
			}
			
			set
			{
				base.SetSystemInt32(CarteiraSuitabilityHistoricoMetadata.ColumnNames.IdPerfilRisco, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraSuitabilityHistorico.PerfilRisco
		/// </summary>
		virtual public System.String PerfilRisco
		{
			get
			{
				return base.GetSystemString(CarteiraSuitabilityHistoricoMetadata.ColumnNames.PerfilRisco);
			}
			
			set
			{
				base.SetSystemString(CarteiraSuitabilityHistoricoMetadata.ColumnNames.PerfilRisco, value);
			}
		}
		
		/// <summary>
		/// Maps to CarteiraSuitabilityHistorico.Tipo
		/// </summary>
		virtual public System.String Tipo
		{
			get
			{
				return base.GetSystemString(CarteiraSuitabilityHistoricoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemString(CarteiraSuitabilityHistoricoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCarteiraSuitabilityHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPerfilRisco
			{
				get
				{
					System.Int32? data = entity.IdPerfilRisco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilRisco = null;
					else entity.IdPerfilRisco = Convert.ToInt32(value);
				}
			}
				
			public System.String PerfilRisco
			{
				get
				{
					System.String data = entity.PerfilRisco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PerfilRisco = null;
					else entity.PerfilRisco = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.String data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToString(value);
				}
			}
			

			private esCarteiraSuitabilityHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCarteiraSuitabilityHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCarteiraSuitabilityHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CarteiraSuitabilityHistorico : esCarteiraSuitabilityHistorico
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCarteiraSuitabilityHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CarteiraSuitabilityHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, CarteiraSuitabilityHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, CarteiraSuitabilityHistoricoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPerfilRisco
		{
			get
			{
				return new esQueryItem(this, CarteiraSuitabilityHistoricoMetadata.ColumnNames.IdPerfilRisco, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PerfilRisco
		{
			get
			{
				return new esQueryItem(this, CarteiraSuitabilityHistoricoMetadata.ColumnNames.PerfilRisco, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, CarteiraSuitabilityHistoricoMetadata.ColumnNames.Tipo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CarteiraSuitabilityHistoricoCollection")]
	public partial class CarteiraSuitabilityHistoricoCollection : esCarteiraSuitabilityHistoricoCollection, IEnumerable<CarteiraSuitabilityHistorico>
	{
		public CarteiraSuitabilityHistoricoCollection()
		{

		}
		
		public static implicit operator List<CarteiraSuitabilityHistorico>(CarteiraSuitabilityHistoricoCollection coll)
		{
			List<CarteiraSuitabilityHistorico> list = new List<CarteiraSuitabilityHistorico>();
			
			foreach (CarteiraSuitabilityHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CarteiraSuitabilityHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CarteiraSuitabilityHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CarteiraSuitabilityHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CarteiraSuitabilityHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CarteiraSuitabilityHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CarteiraSuitabilityHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CarteiraSuitabilityHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CarteiraSuitabilityHistorico AddNew()
		{
			CarteiraSuitabilityHistorico entity = base.AddNewEntity() as CarteiraSuitabilityHistorico;
			
			return entity;
		}

		public CarteiraSuitabilityHistorico FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idCarteira)
		{
			return base.FindByPrimaryKey(dataHistorico, idCarteira) as CarteiraSuitabilityHistorico;
		}


		#region IEnumerable<CarteiraSuitabilityHistorico> Members

		IEnumerator<CarteiraSuitabilityHistorico> IEnumerable<CarteiraSuitabilityHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CarteiraSuitabilityHistorico;
			}
		}

		#endregion
		
		private CarteiraSuitabilityHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CarteiraSuitabilityHistorico' table
	/// </summary>

	[Serializable]
	public partial class CarteiraSuitabilityHistorico : esCarteiraSuitabilityHistorico
	{
		public CarteiraSuitabilityHistorico()
		{

		}
	
		public CarteiraSuitabilityHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CarteiraSuitabilityHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esCarteiraSuitabilityHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CarteiraSuitabilityHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CarteiraSuitabilityHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CarteiraSuitabilityHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CarteiraSuitabilityHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CarteiraSuitabilityHistoricoQuery query;
	}



	[Serializable]
	public partial class CarteiraSuitabilityHistoricoQuery : esCarteiraSuitabilityHistoricoQuery
	{
		public CarteiraSuitabilityHistoricoQuery()
		{

		}		
		
		public CarteiraSuitabilityHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CarteiraSuitabilityHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CarteiraSuitabilityHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CarteiraSuitabilityHistoricoMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CarteiraSuitabilityHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraSuitabilityHistoricoMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraSuitabilityHistoricoMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraSuitabilityHistoricoMetadata.ColumnNames.IdPerfilRisco, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CarteiraSuitabilityHistoricoMetadata.PropertyNames.IdPerfilRisco;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraSuitabilityHistoricoMetadata.ColumnNames.PerfilRisco, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraSuitabilityHistoricoMetadata.PropertyNames.PerfilRisco;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CarteiraSuitabilityHistoricoMetadata.ColumnNames.Tipo, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = CarteiraSuitabilityHistoricoMetadata.PropertyNames.Tipo;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CarteiraSuitabilityHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdPerfilRisco = "IdPerfilRisco";
			 public const string PerfilRisco = "PerfilRisco";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdPerfilRisco = "IdPerfilRisco";
			 public const string PerfilRisco = "PerfilRisco";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CarteiraSuitabilityHistoricoMetadata))
			{
				if(CarteiraSuitabilityHistoricoMetadata.mapDelegates == null)
				{
					CarteiraSuitabilityHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CarteiraSuitabilityHistoricoMetadata.meta == null)
				{
					CarteiraSuitabilityHistoricoMetadata.meta = new CarteiraSuitabilityHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPerfilRisco", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PerfilRisco", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "CarteiraSuitabilityHistorico";
				meta.Destination = "CarteiraSuitabilityHistorico";
				
				meta.spInsert = "proc_CarteiraSuitabilityHistoricoInsert";				
				meta.spUpdate = "proc_CarteiraSuitabilityHistoricoUpdate";		
				meta.spDelete = "proc_CarteiraSuitabilityHistoricoDelete";
				meta.spLoadAll = "proc_CarteiraSuitabilityHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_CarteiraSuitabilityHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CarteiraSuitabilityHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
