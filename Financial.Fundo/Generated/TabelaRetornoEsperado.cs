/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:22 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTabelaRetornoEsperadoCollection : esEntityCollection
	{
		public esTabelaRetornoEsperadoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaRetornoEsperadoCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaRetornoEsperadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaRetornoEsperadoQuery);
		}
		#endregion
		
		virtual public TabelaRetornoEsperado DetachEntity(TabelaRetornoEsperado entity)
		{
			return base.DetachEntity(entity) as TabelaRetornoEsperado;
		}
		
		virtual public TabelaRetornoEsperado AttachEntity(TabelaRetornoEsperado entity)
		{
			return base.AttachEntity(entity) as TabelaRetornoEsperado;
		}
		
		virtual public void Combine(TabelaRetornoEsperadoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaRetornoEsperado this[int index]
		{
			get
			{
				return base[index] as TabelaRetornoEsperado;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaRetornoEsperado);
		}
	}



	[Serializable]
	abstract public class esTabelaRetornoEsperado : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaRetornoEsperadoQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaRetornoEsperado()
		{

		}

		public esTabelaRetornoEsperado(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String idAtivo, System.DateTime dataReferencia, System.Byte tipoAtivo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAtivo, dataReferencia, tipoAtivo);
			else
				return LoadByPrimaryKeyStoredProcedure(idAtivo, dataReferencia, tipoAtivo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.String idAtivo, System.DateTime dataReferencia, System.Byte tipoAtivo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaRetornoEsperadoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdAtivo == idAtivo, query.DataReferencia == dataReferencia, query.TipoAtivo == tipoAtivo);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String idAtivo, System.DateTime dataReferencia, System.Byte tipoAtivo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAtivo, dataReferencia, tipoAtivo);
			else
				return LoadByPrimaryKeyStoredProcedure(idAtivo, dataReferencia, tipoAtivo);
		}

		private bool LoadByPrimaryKeyDynamic(System.String idAtivo, System.DateTime dataReferencia, System.Byte tipoAtivo)
		{
			esTabelaRetornoEsperadoQuery query = this.GetDynamicQuery();
			query.Where(query.IdAtivo == idAtivo, query.DataReferencia == dataReferencia, query.TipoAtivo == tipoAtivo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String idAtivo, System.DateTime dataReferencia, System.Byte tipoAtivo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdAtivo",idAtivo);			parms.Add("DataReferencia",dataReferencia);			parms.Add("TipoAtivo",tipoAtivo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdAtivo": this.str.IdAtivo = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "TipoAtivo": this.str.TipoAtivo = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "TipoAtivo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoAtivo = (System.Byte?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaRetornoEsperado.IdAtivo
		/// </summary>
		virtual public System.String IdAtivo
		{
			get
			{
				return base.GetSystemString(TabelaRetornoEsperadoMetadata.ColumnNames.IdAtivo);
			}
			
			set
			{
				base.SetSystemString(TabelaRetornoEsperadoMetadata.ColumnNames.IdAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaRetornoEsperado.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaRetornoEsperadoMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaRetornoEsperadoMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaRetornoEsperado.TipoAtivo
		/// </summary>
		virtual public System.Byte? TipoAtivo
		{
			get
			{
				return base.GetSystemByte(TabelaRetornoEsperadoMetadata.ColumnNames.TipoAtivo);
			}
			
			set
			{
				base.SetSystemByte(TabelaRetornoEsperadoMetadata.ColumnNames.TipoAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaRetornoEsperado.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(TabelaRetornoEsperadoMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaRetornoEsperadoMetadata.ColumnNames.Valor, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaRetornoEsperado entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdAtivo
			{
				get
				{
					System.String data = entity.IdAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAtivo = null;
					else entity.IdAtivo = Convert.ToString(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoAtivo
			{
				get
				{
					System.Byte? data = entity.TipoAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoAtivo = null;
					else entity.TipoAtivo = Convert.ToByte(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaRetornoEsperado entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaRetornoEsperadoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaRetornoEsperado can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaRetornoEsperado : esTabelaRetornoEsperado
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaRetornoEsperadoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaRetornoEsperadoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdAtivo
		{
			get
			{
				return new esQueryItem(this, TabelaRetornoEsperadoMetadata.ColumnNames.IdAtivo, esSystemType.String);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaRetornoEsperadoMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoAtivo
		{
			get
			{
				return new esQueryItem(this, TabelaRetornoEsperadoMetadata.ColumnNames.TipoAtivo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, TabelaRetornoEsperadoMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaRetornoEsperadoCollection")]
	public partial class TabelaRetornoEsperadoCollection : esTabelaRetornoEsperadoCollection, IEnumerable<TabelaRetornoEsperado>
	{
		public TabelaRetornoEsperadoCollection()
		{

		}
		
		public static implicit operator List<TabelaRetornoEsperado>(TabelaRetornoEsperadoCollection coll)
		{
			List<TabelaRetornoEsperado> list = new List<TabelaRetornoEsperado>();
			
			foreach (TabelaRetornoEsperado emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaRetornoEsperadoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaRetornoEsperadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaRetornoEsperado(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaRetornoEsperado();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaRetornoEsperadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaRetornoEsperadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaRetornoEsperadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaRetornoEsperado AddNew()
		{
			TabelaRetornoEsperado entity = base.AddNewEntity() as TabelaRetornoEsperado;
			
			return entity;
		}

		public TabelaRetornoEsperado FindByPrimaryKey(System.String idAtivo, System.DateTime dataReferencia, System.Byte tipoAtivo)
		{
			return base.FindByPrimaryKey(idAtivo, dataReferencia, tipoAtivo) as TabelaRetornoEsperado;
		}


		#region IEnumerable<TabelaRetornoEsperado> Members

		IEnumerator<TabelaRetornoEsperado> IEnumerable<TabelaRetornoEsperado>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaRetornoEsperado;
			}
		}

		#endregion
		
		private TabelaRetornoEsperadoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaRetornoEsperado' table
	/// </summary>

	[Serializable]
	public partial class TabelaRetornoEsperado : esTabelaRetornoEsperado
	{
		public TabelaRetornoEsperado()
		{

		}
	
		public TabelaRetornoEsperado(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaRetornoEsperadoMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaRetornoEsperadoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaRetornoEsperadoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaRetornoEsperadoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaRetornoEsperadoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaRetornoEsperadoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaRetornoEsperadoQuery query;
	}



	[Serializable]
	public partial class TabelaRetornoEsperadoQuery : esTabelaRetornoEsperadoQuery
	{
		public TabelaRetornoEsperadoQuery()
		{

		}		
		
		public TabelaRetornoEsperadoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaRetornoEsperadoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaRetornoEsperadoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaRetornoEsperadoMetadata.ColumnNames.IdAtivo, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaRetornoEsperadoMetadata.PropertyNames.IdAtivo;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRetornoEsperadoMetadata.ColumnNames.DataReferencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaRetornoEsperadoMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRetornoEsperadoMetadata.ColumnNames.TipoAtivo, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaRetornoEsperadoMetadata.PropertyNames.TipoAtivo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaRetornoEsperadoMetadata.ColumnNames.Valor, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaRetornoEsperadoMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 12;
			c.NumericScale = 6;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaRetornoEsperadoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdAtivo = "IdAtivo";
			 public const string DataReferencia = "DataReferencia";
			 public const string TipoAtivo = "TipoAtivo";
			 public const string Valor = "Valor";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdAtivo = "IdAtivo";
			 public const string DataReferencia = "DataReferencia";
			 public const string TipoAtivo = "TipoAtivo";
			 public const string Valor = "Valor";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaRetornoEsperadoMetadata))
			{
				if(TabelaRetornoEsperadoMetadata.mapDelegates == null)
				{
					TabelaRetornoEsperadoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaRetornoEsperadoMetadata.meta == null)
				{
					TabelaRetornoEsperadoMetadata.meta = new TabelaRetornoEsperadoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdAtivo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoAtivo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaRetornoEsperado";
				meta.Destination = "TabelaRetornoEsperado";
				
				meta.spInsert = "proc_TabelaRetornoEsperadoInsert";				
				meta.spUpdate = "proc_TabelaRetornoEsperadoUpdate";		
				meta.spDelete = "proc_TabelaRetornoEsperadoDelete";
				meta.spLoadAll = "proc_TabelaRetornoEsperadoLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaRetornoEsperadoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaRetornoEsperadoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
