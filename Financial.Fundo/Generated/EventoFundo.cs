/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/12/2014 15:53:45
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esEventoFundoCollection : esEntityCollection
	{
		public esEventoFundoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EventoFundoCollection";
		}

		#region Query Logic
		protected void InitQuery(esEventoFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEventoFundoQuery);
		}
		#endregion
		
		virtual public EventoFundo DetachEntity(EventoFundo entity)
		{
			return base.DetachEntity(entity) as EventoFundo;
		}
		
		virtual public EventoFundo AttachEntity(EventoFundo entity)
		{
			return base.AttachEntity(entity) as EventoFundo;
		}
		
		virtual public void Combine(EventoFundoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EventoFundo this[int index]
		{
			get
			{
				return base[index] as EventoFundo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EventoFundo);
		}
	}



	[Serializable]
	abstract public class esEventoFundo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEventoFundoQuery GetDynamicQuery()
		{
			return null;
		}

		public esEventoFundo()
		{

		}

		public esEventoFundo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEventoFundo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEventoFundo);
			else
				return LoadByPrimaryKeyStoredProcedure(idEventoFundo);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEventoFundo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEventoFundo);
			else
				return LoadByPrimaryKeyStoredProcedure(idEventoFundo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEventoFundo)
		{
			esEventoFundoQuery query = this.GetDynamicQuery();
			query.Where(query.IdEventoFundo == idEventoFundo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEventoFundo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEventoFundo",idEventoFundo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEventoFundo": this.str.IdEventoFundo = (string)value; break;							
						case "IdCarteiraOrigem": this.str.IdCarteiraOrigem = (string)value; break;							
						case "IdCarteiraDestino": this.str.IdCarteiraDestino = (string)value; break;							
						case "TipoEvento": this.str.TipoEvento = (string)value; break;							
						case "Percentual": this.str.Percentual = (string)value; break;							
						case "DataPosicao": this.str.DataPosicao = (string)value; break;							
						case "Status": this.str.Status = (string)value; break;							
						case "ExecucaoRecolhimento": this.str.ExecucaoRecolhimento = (string)value; break;							
						case "AliquotaIR": this.str.AliquotaIR = (string)value; break;							
						case "DataRecolhimento": this.str.DataRecolhimento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEventoFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoFundo = (System.Int32?)value;
							break;
						
						case "IdCarteiraOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraOrigem = (System.Int32?)value;
							break;
						
						case "IdCarteiraDestino":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraDestino = (System.Int32?)value;
							break;
						
						case "TipoEvento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoEvento = (System.Int32?)value;
							break;
						
						case "Percentual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Percentual = (System.Decimal?)value;
							break;
						
						case "DataPosicao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPosicao = (System.DateTime?)value;
							break;
						
						case "ExecucaoRecolhimento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.ExecucaoRecolhimento = (System.Int32?)value;
							break;
						
						case "AliquotaIR":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.AliquotaIR = (System.Int32?)value;
							break;
						
						case "DataRecolhimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRecolhimento = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EventoFundo.IdEventoFundo
		/// </summary>
		virtual public System.Int32? IdEventoFundo
		{
			get
			{
				return base.GetSystemInt32(EventoFundoMetadata.ColumnNames.IdEventoFundo);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoMetadata.ColumnNames.IdEventoFundo, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundo.IdCarteiraOrigem
		/// </summary>
		virtual public System.Int32? IdCarteiraOrigem
		{
			get
			{
				return base.GetSystemInt32(EventoFundoMetadata.ColumnNames.IdCarteiraOrigem);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoFundoMetadata.ColumnNames.IdCarteiraOrigem, value))
				{
					this._UpToCarteiraByIdCarteiraOrigem = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoFundo.IdCarteiraDestino
		/// </summary>
		virtual public System.Int32? IdCarteiraDestino
		{
			get
			{
				return base.GetSystemInt32(EventoFundoMetadata.ColumnNames.IdCarteiraDestino);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoFundoMetadata.ColumnNames.IdCarteiraDestino, value))
				{
					this._UpToCarteiraByIdCarteiraDestino = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoFundo.TipoEvento
		/// </summary>
		virtual public System.Int32? TipoEvento
		{
			get
			{
				return base.GetSystemInt32(EventoFundoMetadata.ColumnNames.TipoEvento);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoMetadata.ColumnNames.TipoEvento, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundo.Percentual
		/// </summary>
		virtual public System.Decimal? Percentual
		{
			get
			{
				return base.GetSystemDecimal(EventoFundoMetadata.ColumnNames.Percentual);
			}
			
			set
			{
				base.SetSystemDecimal(EventoFundoMetadata.ColumnNames.Percentual, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundo.DataPosicao
		/// </summary>
		virtual public System.DateTime? DataPosicao
		{
			get
			{
				return base.GetSystemDateTime(EventoFundoMetadata.ColumnNames.DataPosicao);
			}
			
			set
			{
				base.SetSystemDateTime(EventoFundoMetadata.ColumnNames.DataPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundo.Status
		/// </summary>
		virtual public System.String Status
		{
			get
			{
				return base.GetSystemString(EventoFundoMetadata.ColumnNames.Status);
			}
			
			set
			{
				base.SetSystemString(EventoFundoMetadata.ColumnNames.Status, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundo.ExecucaoRecolhimento
		/// </summary>
		virtual public System.Int32? ExecucaoRecolhimento
		{
			get
			{
				return base.GetSystemInt32(EventoFundoMetadata.ColumnNames.ExecucaoRecolhimento);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoMetadata.ColumnNames.ExecucaoRecolhimento, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundo.AliquotaIR
		/// </summary>
		virtual public System.Int32? AliquotaIR
		{
			get
			{
				return base.GetSystemInt32(EventoFundoMetadata.ColumnNames.AliquotaIR);
			}
			
			set
			{
				base.SetSystemInt32(EventoFundoMetadata.ColumnNames.AliquotaIR, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFundo.DataRecolhimento
		/// </summary>
		virtual public System.DateTime? DataRecolhimento
		{
			get
			{
				return base.GetSystemDateTime(EventoFundoMetadata.ColumnNames.DataRecolhimento);
			}
			
			set
			{
				base.SetSystemDateTime(EventoFundoMetadata.ColumnNames.DataRecolhimento, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteiraOrigem;
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteiraDestino;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEventoFundo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEventoFundo
			{
				get
				{
					System.Int32? data = entity.IdEventoFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoFundo = null;
					else entity.IdEventoFundo = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteiraOrigem
			{
				get
				{
					System.Int32? data = entity.IdCarteiraOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraOrigem = null;
					else entity.IdCarteiraOrigem = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteiraDestino
			{
				get
				{
					System.Int32? data = entity.IdCarteiraDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraDestino = null;
					else entity.IdCarteiraDestino = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoEvento
			{
				get
				{
					System.Int32? data = entity.TipoEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoEvento = null;
					else entity.TipoEvento = Convert.ToInt32(value);
				}
			}
				
			public System.String Percentual
			{
				get
				{
					System.Decimal? data = entity.Percentual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Percentual = null;
					else entity.Percentual = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataPosicao
			{
				get
				{
					System.DateTime? data = entity.DataPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPosicao = null;
					else entity.DataPosicao = Convert.ToDateTime(value);
				}
			}
				
			public System.String Status
			{
				get
				{
					System.String data = entity.Status;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Status = null;
					else entity.Status = Convert.ToString(value);
				}
			}
				
			public System.String ExecucaoRecolhimento
			{
				get
				{
					System.Int32? data = entity.ExecucaoRecolhimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExecucaoRecolhimento = null;
					else entity.ExecucaoRecolhimento = Convert.ToInt32(value);
				}
			}
				
			public System.String AliquotaIR
			{
				get
				{
					System.Int32? data = entity.AliquotaIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AliquotaIR = null;
					else entity.AliquotaIR = Convert.ToInt32(value);
				}
			}
				
			public System.String DataRecolhimento
			{
				get
				{
					System.DateTime? data = entity.DataRecolhimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRecolhimento = null;
					else entity.DataRecolhimento = Convert.ToDateTime(value);
				}
			}
			

			private esEventoFundo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEventoFundoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEventoFundo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EventoFundo : esEventoFundo
	{

				
		#region EventoFundoBMFCollectionByIdEventoFundo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EventoFundoBMF_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundoBMFCollection EventoFundoBMFCollectionByIdEventoFundo
		{
			get
			{
				if(this._EventoFundoBMFCollectionByIdEventoFundo == null)
				{
					this._EventoFundoBMFCollectionByIdEventoFundo = new EventoFundoBMFCollection();
					this._EventoFundoBMFCollectionByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoFundoBMFCollectionByIdEventoFundo", this._EventoFundoBMFCollectionByIdEventoFundo);
				
					if(this.IdEventoFundo != null)
					{
						this._EventoFundoBMFCollectionByIdEventoFundo.Query.Where(this._EventoFundoBMFCollectionByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
						this._EventoFundoBMFCollectionByIdEventoFundo.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoFundoBMFCollectionByIdEventoFundo.fks.Add(EventoFundoBMFMetadata.ColumnNames.IdEventoFundo, this.IdEventoFundo);
					}
				}

				return this._EventoFundoBMFCollectionByIdEventoFundo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoFundoBMFCollectionByIdEventoFundo != null) 
				{ 
					this.RemovePostSave("EventoFundoBMFCollectionByIdEventoFundo"); 
					this._EventoFundoBMFCollectionByIdEventoFundo = null;
					
				} 
			} 			
		}

		private EventoFundoBMFCollection _EventoFundoBMFCollectionByIdEventoFundo;
		#endregion

				
		#region EventoFundoBolsaCollectionByIdEventoFundo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EventoFundoBolsa_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundoBolsaCollection EventoFundoBolsaCollectionByIdEventoFundo
		{
			get
			{
				if(this._EventoFundoBolsaCollectionByIdEventoFundo == null)
				{
					this._EventoFundoBolsaCollectionByIdEventoFundo = new EventoFundoBolsaCollection();
					this._EventoFundoBolsaCollectionByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoFundoBolsaCollectionByIdEventoFundo", this._EventoFundoBolsaCollectionByIdEventoFundo);
				
					if(this.IdEventoFundo != null)
					{
						this._EventoFundoBolsaCollectionByIdEventoFundo.Query.Where(this._EventoFundoBolsaCollectionByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
						this._EventoFundoBolsaCollectionByIdEventoFundo.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoFundoBolsaCollectionByIdEventoFundo.fks.Add(EventoFundoBolsaMetadata.ColumnNames.IdEventoFundo, this.IdEventoFundo);
					}
				}

				return this._EventoFundoBolsaCollectionByIdEventoFundo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoFundoBolsaCollectionByIdEventoFundo != null) 
				{ 
					this.RemovePostSave("EventoFundoBolsaCollectionByIdEventoFundo"); 
					this._EventoFundoBolsaCollectionByIdEventoFundo = null;
					
				} 
			} 			
		}

		private EventoFundoBolsaCollection _EventoFundoBolsaCollectionByIdEventoFundo;
		#endregion

				
		#region EventoFundoCaixaCollectionByIdEventoFundo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EventoFundoCaixa_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundoCaixaCollection EventoFundoCaixaCollectionByIdEventoFundo
		{
			get
			{
				if(this._EventoFundoCaixaCollectionByIdEventoFundo == null)
				{
					this._EventoFundoCaixaCollectionByIdEventoFundo = new EventoFundoCaixaCollection();
					this._EventoFundoCaixaCollectionByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoFundoCaixaCollectionByIdEventoFundo", this._EventoFundoCaixaCollectionByIdEventoFundo);
				
					if(this.IdEventoFundo != null)
					{
						this._EventoFundoCaixaCollectionByIdEventoFundo.Query.Where(this._EventoFundoCaixaCollectionByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
						this._EventoFundoCaixaCollectionByIdEventoFundo.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoFundoCaixaCollectionByIdEventoFundo.fks.Add(EventoFundoCaixaMetadata.ColumnNames.IdEventoFundo, this.IdEventoFundo);
					}
				}

				return this._EventoFundoCaixaCollectionByIdEventoFundo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoFundoCaixaCollectionByIdEventoFundo != null) 
				{ 
					this.RemovePostSave("EventoFundoCaixaCollectionByIdEventoFundo"); 
					this._EventoFundoCaixaCollectionByIdEventoFundo = null;
					
				} 
			} 			
		}

		private EventoFundoCaixaCollection _EventoFundoCaixaCollectionByIdEventoFundo;
		#endregion

				
		#region EventoFundoCautelaCollectionByIdEventoFundo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EventoFundoCautela_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundoCautelaCollection EventoFundoCautelaCollectionByIdEventoFundo
		{
			get
			{
				if(this._EventoFundoCautelaCollectionByIdEventoFundo == null)
				{
					this._EventoFundoCautelaCollectionByIdEventoFundo = new EventoFundoCautelaCollection();
					this._EventoFundoCautelaCollectionByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoFundoCautelaCollectionByIdEventoFundo", this._EventoFundoCautelaCollectionByIdEventoFundo);
				
					if(this.IdEventoFundo != null)
					{
						this._EventoFundoCautelaCollectionByIdEventoFundo.Query.Where(this._EventoFundoCautelaCollectionByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
						this._EventoFundoCautelaCollectionByIdEventoFundo.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoFundoCautelaCollectionByIdEventoFundo.fks.Add(EventoFundoCautelaMetadata.ColumnNames.IdEventoFundo, this.IdEventoFundo);
					}
				}

				return this._EventoFundoCautelaCollectionByIdEventoFundo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoFundoCautelaCollectionByIdEventoFundo != null) 
				{ 
					this.RemovePostSave("EventoFundoCautelaCollectionByIdEventoFundo"); 
					this._EventoFundoCautelaCollectionByIdEventoFundo = null;
					
				} 
			} 			
		}

		private EventoFundoCautelaCollection _EventoFundoCautelaCollectionByIdEventoFundo;
		#endregion

				
		#region EventoFundoFundoCollectionByIdEventoFundo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EventoFundoFundo_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundoFundoCollection EventoFundoFundoCollectionByIdEventoFundo
		{
			get
			{
				if(this._EventoFundoFundoCollectionByIdEventoFundo == null)
				{
					this._EventoFundoFundoCollectionByIdEventoFundo = new EventoFundoFundoCollection();
					this._EventoFundoFundoCollectionByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoFundoFundoCollectionByIdEventoFundo", this._EventoFundoFundoCollectionByIdEventoFundo);
				
					if(this.IdEventoFundo != null)
					{
						this._EventoFundoFundoCollectionByIdEventoFundo.Query.Where(this._EventoFundoFundoCollectionByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
						this._EventoFundoFundoCollectionByIdEventoFundo.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoFundoFundoCollectionByIdEventoFundo.fks.Add(EventoFundoFundoMetadata.ColumnNames.IdEventoFundo, this.IdEventoFundo);
					}
				}

				return this._EventoFundoFundoCollectionByIdEventoFundo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoFundoFundoCollectionByIdEventoFundo != null) 
				{ 
					this.RemovePostSave("EventoFundoFundoCollectionByIdEventoFundo"); 
					this._EventoFundoFundoCollectionByIdEventoFundo = null;
					
				} 
			} 			
		}

		private EventoFundoFundoCollection _EventoFundoFundoCollectionByIdEventoFundo;
		#endregion

				
		#region EventoFundoLiquidacaoCollectionByIdEventoFundo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EventoFundoLiquidacao_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundoLiquidacaoCollection EventoFundoLiquidacaoCollectionByIdEventoFundo
		{
			get
			{
				if(this._EventoFundoLiquidacaoCollectionByIdEventoFundo == null)
				{
					this._EventoFundoLiquidacaoCollectionByIdEventoFundo = new EventoFundoLiquidacaoCollection();
					this._EventoFundoLiquidacaoCollectionByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoFundoLiquidacaoCollectionByIdEventoFundo", this._EventoFundoLiquidacaoCollectionByIdEventoFundo);
				
					if(this.IdEventoFundo != null)
					{
						this._EventoFundoLiquidacaoCollectionByIdEventoFundo.Query.Where(this._EventoFundoLiquidacaoCollectionByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
						this._EventoFundoLiquidacaoCollectionByIdEventoFundo.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoFundoLiquidacaoCollectionByIdEventoFundo.fks.Add(EventoFundoLiquidacaoMetadata.ColumnNames.IdEventoFundo, this.IdEventoFundo);
					}
				}

				return this._EventoFundoLiquidacaoCollectionByIdEventoFundo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoFundoLiquidacaoCollectionByIdEventoFundo != null) 
				{ 
					this.RemovePostSave("EventoFundoLiquidacaoCollectionByIdEventoFundo"); 
					this._EventoFundoLiquidacaoCollectionByIdEventoFundo = null;
					
				} 
			} 			
		}

		private EventoFundoLiquidacaoCollection _EventoFundoLiquidacaoCollectionByIdEventoFundo;
		#endregion

				
		#region EventoFundoMovimentoAtivosCollectionByIdEventoFundo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EventoFundoMovimentoAtivos_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundoMovimentoAtivosCollection EventoFundoMovimentoAtivosCollectionByIdEventoFundo
		{
			get
			{
				if(this._EventoFundoMovimentoAtivosCollectionByIdEventoFundo == null)
				{
					this._EventoFundoMovimentoAtivosCollectionByIdEventoFundo = new EventoFundoMovimentoAtivosCollection();
					this._EventoFundoMovimentoAtivosCollectionByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoFundoMovimentoAtivosCollectionByIdEventoFundo", this._EventoFundoMovimentoAtivosCollectionByIdEventoFundo);
				
					if(this.IdEventoFundo != null)
					{
						this._EventoFundoMovimentoAtivosCollectionByIdEventoFundo.Query.Where(this._EventoFundoMovimentoAtivosCollectionByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
						this._EventoFundoMovimentoAtivosCollectionByIdEventoFundo.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoFundoMovimentoAtivosCollectionByIdEventoFundo.fks.Add(EventoFundoMovimentoAtivosMetadata.ColumnNames.IdEventoFundo, this.IdEventoFundo);
					}
				}

				return this._EventoFundoMovimentoAtivosCollectionByIdEventoFundo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoFundoMovimentoAtivosCollectionByIdEventoFundo != null) 
				{ 
					this.RemovePostSave("EventoFundoMovimentoAtivosCollectionByIdEventoFundo"); 
					this._EventoFundoMovimentoAtivosCollectionByIdEventoFundo = null;
					
				} 
			} 			
		}

		private EventoFundoMovimentoAtivosCollection _EventoFundoMovimentoAtivosCollectionByIdEventoFundo;
		#endregion

				
		#region EventoFundoMovimentoCautelasCollectionByIdEventoFundo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EventoFundoMovimentoCautelas_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundoMovimentoCautelasCollection EventoFundoMovimentoCautelasCollectionByIdEventoFundo
		{
			get
			{
				if(this._EventoFundoMovimentoCautelasCollectionByIdEventoFundo == null)
				{
					this._EventoFundoMovimentoCautelasCollectionByIdEventoFundo = new EventoFundoMovimentoCautelasCollection();
					this._EventoFundoMovimentoCautelasCollectionByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoFundoMovimentoCautelasCollectionByIdEventoFundo", this._EventoFundoMovimentoCautelasCollectionByIdEventoFundo);
				
					if(this.IdEventoFundo != null)
					{
						this._EventoFundoMovimentoCautelasCollectionByIdEventoFundo.Query.Where(this._EventoFundoMovimentoCautelasCollectionByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
						this._EventoFundoMovimentoCautelasCollectionByIdEventoFundo.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoFundoMovimentoCautelasCollectionByIdEventoFundo.fks.Add(EventoFundoMovimentoCautelasMetadata.ColumnNames.IdEventoFundo, this.IdEventoFundo);
					}
				}

				return this._EventoFundoMovimentoCautelasCollectionByIdEventoFundo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoFundoMovimentoCautelasCollectionByIdEventoFundo != null) 
				{ 
					this.RemovePostSave("EventoFundoMovimentoCautelasCollectionByIdEventoFundo"); 
					this._EventoFundoMovimentoCautelasCollectionByIdEventoFundo = null;
					
				} 
			} 			
		}

		private EventoFundoMovimentoCautelasCollection _EventoFundoMovimentoCautelasCollectionByIdEventoFundo;
		#endregion

				
		#region EventoFundoRendaFixaCollectionByIdEventoFundo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EventoFundoRendaFixa_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundoRendaFixaCollection EventoFundoRendaFixaCollectionByIdEventoFundo
		{
			get
			{
				if(this._EventoFundoRendaFixaCollectionByIdEventoFundo == null)
				{
					this._EventoFundoRendaFixaCollectionByIdEventoFundo = new EventoFundoRendaFixaCollection();
					this._EventoFundoRendaFixaCollectionByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoFundoRendaFixaCollectionByIdEventoFundo", this._EventoFundoRendaFixaCollectionByIdEventoFundo);
				
					if(this.IdEventoFundo != null)
					{
						this._EventoFundoRendaFixaCollectionByIdEventoFundo.Query.Where(this._EventoFundoRendaFixaCollectionByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
						this._EventoFundoRendaFixaCollectionByIdEventoFundo.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoFundoRendaFixaCollectionByIdEventoFundo.fks.Add(EventoFundoRendaFixaMetadata.ColumnNames.IdEventoFundo, this.IdEventoFundo);
					}
				}

				return this._EventoFundoRendaFixaCollectionByIdEventoFundo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoFundoRendaFixaCollectionByIdEventoFundo != null) 
				{ 
					this.RemovePostSave("EventoFundoRendaFixaCollectionByIdEventoFundo"); 
					this._EventoFundoRendaFixaCollectionByIdEventoFundo = null;
					
				} 
			} 			
		}

		private EventoFundoRendaFixaCollection _EventoFundoRendaFixaCollectionByIdEventoFundo;
		#endregion

				
		#region EventoFundoSwapCollectionByIdEventoFundo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - EventoFundoSwap_EventoFundo_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFundoSwapCollection EventoFundoSwapCollectionByIdEventoFundo
		{
			get
			{
				if(this._EventoFundoSwapCollectionByIdEventoFundo == null)
				{
					this._EventoFundoSwapCollectionByIdEventoFundo = new EventoFundoSwapCollection();
					this._EventoFundoSwapCollectionByIdEventoFundo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoFundoSwapCollectionByIdEventoFundo", this._EventoFundoSwapCollectionByIdEventoFundo);
				
					if(this.IdEventoFundo != null)
					{
						this._EventoFundoSwapCollectionByIdEventoFundo.Query.Where(this._EventoFundoSwapCollectionByIdEventoFundo.Query.IdEventoFundo == this.IdEventoFundo);
						this._EventoFundoSwapCollectionByIdEventoFundo.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoFundoSwapCollectionByIdEventoFundo.fks.Add(EventoFundoSwapMetadata.ColumnNames.IdEventoFundo, this.IdEventoFundo);
					}
				}

				return this._EventoFundoSwapCollectionByIdEventoFundo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoFundoSwapCollectionByIdEventoFundo != null) 
				{ 
					this.RemovePostSave("EventoFundoSwapCollectionByIdEventoFundo"); 
					this._EventoFundoSwapCollectionByIdEventoFundo = null;
					
				} 
			} 			
		}

		private EventoFundoSwapCollection _EventoFundoSwapCollectionByIdEventoFundo;
		#endregion

				
		#region UpToCarteiraByIdCarteiraOrigem - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EventoFundo_Carteira_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteiraOrigem
		{
			get
			{
				if(this._UpToCarteiraByIdCarteiraOrigem == null
					&& IdCarteiraOrigem != null					)
				{
					this._UpToCarteiraByIdCarteiraOrigem = new Carteira();
					this._UpToCarteiraByIdCarteiraOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteiraOrigem", this._UpToCarteiraByIdCarteiraOrigem);
					this._UpToCarteiraByIdCarteiraOrigem.Query.Where(this._UpToCarteiraByIdCarteiraOrigem.Query.IdCarteira == this.IdCarteiraOrigem);
					this._UpToCarteiraByIdCarteiraOrigem.Query.Load();
				}

				return this._UpToCarteiraByIdCarteiraOrigem;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteiraOrigem");
				

				if(value == null)
				{
					this.IdCarteiraOrigem = null;
					this._UpToCarteiraByIdCarteiraOrigem = null;
				}
				else
				{
					this.IdCarteiraOrigem = value.IdCarteira;
					this._UpToCarteiraByIdCarteiraOrigem = value;
					this.SetPreSave("UpToCarteiraByIdCarteiraOrigem", this._UpToCarteiraByIdCarteiraOrigem);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCarteiraByIdCarteiraDestino - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - EventoFundo_Carteira_FK2
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteiraDestino
		{
			get
			{
				if(this._UpToCarteiraByIdCarteiraDestino == null
					&& IdCarteiraDestino != null					)
				{
					this._UpToCarteiraByIdCarteiraDestino = new Carteira();
					this._UpToCarteiraByIdCarteiraDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteiraDestino", this._UpToCarteiraByIdCarteiraDestino);
					this._UpToCarteiraByIdCarteiraDestino.Query.Where(this._UpToCarteiraByIdCarteiraDestino.Query.IdCarteira == this.IdCarteiraDestino);
					this._UpToCarteiraByIdCarteiraDestino.Query.Load();
				}

				return this._UpToCarteiraByIdCarteiraDestino;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteiraDestino");
				

				if(value == null)
				{
					this.IdCarteiraDestino = null;
					this._UpToCarteiraByIdCarteiraDestino = null;
				}
				else
				{
					this.IdCarteiraDestino = value.IdCarteira;
					this._UpToCarteiraByIdCarteiraDestino = value;
					this.SetPreSave("UpToCarteiraByIdCarteiraDestino", this._UpToCarteiraByIdCarteiraDestino);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "EventoFundoBMFCollectionByIdEventoFundo", typeof(EventoFundoBMFCollection), new EventoFundoBMF()));
			props.Add(new esPropertyDescriptor(this, "EventoFundoBolsaCollectionByIdEventoFundo", typeof(EventoFundoBolsaCollection), new EventoFundoBolsa()));
			props.Add(new esPropertyDescriptor(this, "EventoFundoCaixaCollectionByIdEventoFundo", typeof(EventoFundoCaixaCollection), new EventoFundoCaixa()));
			props.Add(new esPropertyDescriptor(this, "EventoFundoCautelaCollectionByIdEventoFundo", typeof(EventoFundoCautelaCollection), new EventoFundoCautela()));
			props.Add(new esPropertyDescriptor(this, "EventoFundoFundoCollectionByIdEventoFundo", typeof(EventoFundoFundoCollection), new EventoFundoFundo()));
			props.Add(new esPropertyDescriptor(this, "EventoFundoLiquidacaoCollectionByIdEventoFundo", typeof(EventoFundoLiquidacaoCollection), new EventoFundoLiquidacao()));
			props.Add(new esPropertyDescriptor(this, "EventoFundoMovimentoAtivosCollectionByIdEventoFundo", typeof(EventoFundoMovimentoAtivosCollection), new EventoFundoMovimentoAtivos()));
			props.Add(new esPropertyDescriptor(this, "EventoFundoMovimentoCautelasCollectionByIdEventoFundo", typeof(EventoFundoMovimentoCautelasCollection), new EventoFundoMovimentoCautelas()));
			props.Add(new esPropertyDescriptor(this, "EventoFundoRendaFixaCollectionByIdEventoFundo", typeof(EventoFundoRendaFixaCollection), new EventoFundoRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "EventoFundoSwapCollectionByIdEventoFundo", typeof(EventoFundoSwapCollection), new EventoFundoSwap()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._EventoFundoBMFCollectionByIdEventoFundo != null)
			{
				foreach(EventoFundoBMF obj in this._EventoFundoBMFCollectionByIdEventoFundo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoFundo = this.IdEventoFundo;
					}
				}
			}
			if(this._EventoFundoBolsaCollectionByIdEventoFundo != null)
			{
				foreach(EventoFundoBolsa obj in this._EventoFundoBolsaCollectionByIdEventoFundo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoFundo = this.IdEventoFundo;
					}
				}
			}
			if(this._EventoFundoCaixaCollectionByIdEventoFundo != null)
			{
				foreach(EventoFundoCaixa obj in this._EventoFundoCaixaCollectionByIdEventoFundo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoFundo = this.IdEventoFundo;
					}
				}
			}
			if(this._EventoFundoCautelaCollectionByIdEventoFundo != null)
			{
				foreach(EventoFundoCautela obj in this._EventoFundoCautelaCollectionByIdEventoFundo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoFundo = this.IdEventoFundo;
					}
				}
			}
			if(this._EventoFundoFundoCollectionByIdEventoFundo != null)
			{
				foreach(EventoFundoFundo obj in this._EventoFundoFundoCollectionByIdEventoFundo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoFundo = this.IdEventoFundo;
					}
				}
			}
			if(this._EventoFundoLiquidacaoCollectionByIdEventoFundo != null)
			{
				foreach(EventoFundoLiquidacao obj in this._EventoFundoLiquidacaoCollectionByIdEventoFundo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoFundo = this.IdEventoFundo;
					}
				}
			}
			if(this._EventoFundoMovimentoAtivosCollectionByIdEventoFundo != null)
			{
				foreach(EventoFundoMovimentoAtivos obj in this._EventoFundoMovimentoAtivosCollectionByIdEventoFundo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoFundo = this.IdEventoFundo;
					}
				}
			}
			if(this._EventoFundoMovimentoCautelasCollectionByIdEventoFundo != null)
			{
				foreach(EventoFundoMovimentoCautelas obj in this._EventoFundoMovimentoCautelasCollectionByIdEventoFundo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoFundo = this.IdEventoFundo;
					}
				}
			}
			if(this._EventoFundoRendaFixaCollectionByIdEventoFundo != null)
			{
				foreach(EventoFundoRendaFixa obj in this._EventoFundoRendaFixaCollectionByIdEventoFundo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoFundo = this.IdEventoFundo;
					}
				}
			}
			if(this._EventoFundoSwapCollectionByIdEventoFundo != null)
			{
				foreach(EventoFundoSwap obj in this._EventoFundoSwapCollectionByIdEventoFundo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdEventoFundo = this.IdEventoFundo;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEventoFundoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEventoFundo
		{
			get
			{
				return new esQueryItem(this, EventoFundoMetadata.ColumnNames.IdEventoFundo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteiraOrigem
		{
			get
			{
				return new esQueryItem(this, EventoFundoMetadata.ColumnNames.IdCarteiraOrigem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteiraDestino
		{
			get
			{
				return new esQueryItem(this, EventoFundoMetadata.ColumnNames.IdCarteiraDestino, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoEvento
		{
			get
			{
				return new esQueryItem(this, EventoFundoMetadata.ColumnNames.TipoEvento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Percentual
		{
			get
			{
				return new esQueryItem(this, EventoFundoMetadata.ColumnNames.Percentual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataPosicao
		{
			get
			{
				return new esQueryItem(this, EventoFundoMetadata.ColumnNames.DataPosicao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Status
		{
			get
			{
				return new esQueryItem(this, EventoFundoMetadata.ColumnNames.Status, esSystemType.String);
			}
		} 
		
		public esQueryItem ExecucaoRecolhimento
		{
			get
			{
				return new esQueryItem(this, EventoFundoMetadata.ColumnNames.ExecucaoRecolhimento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem AliquotaIR
		{
			get
			{
				return new esQueryItem(this, EventoFundoMetadata.ColumnNames.AliquotaIR, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataRecolhimento
		{
			get
			{
				return new esQueryItem(this, EventoFundoMetadata.ColumnNames.DataRecolhimento, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EventoFundoCollection")]
	public partial class EventoFundoCollection : esEventoFundoCollection, IEnumerable<EventoFundo>
	{
		public EventoFundoCollection()
		{

		}
		
		public static implicit operator List<EventoFundo>(EventoFundoCollection coll)
		{
			List<EventoFundo> list = new List<EventoFundo>();
			
			foreach (EventoFundo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EventoFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EventoFundo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EventoFundo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EventoFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EventoFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EventoFundo AddNew()
		{
			EventoFundo entity = base.AddNewEntity() as EventoFundo;
			
			return entity;
		}

		public EventoFundo FindByPrimaryKey(System.Int32 idEventoFundo)
		{
			return base.FindByPrimaryKey(idEventoFundo) as EventoFundo;
		}


		#region IEnumerable<EventoFundo> Members

		IEnumerator<EventoFundo> IEnumerable<EventoFundo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EventoFundo;
			}
		}

		#endregion
		
		private EventoFundoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EventoFundo' table
	/// </summary>

	[Serializable]
	public partial class EventoFundo : esEventoFundo
	{
		public EventoFundo()
		{

		}
	
		public EventoFundo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EventoFundoMetadata.Meta();
			}
		}
		
		
		
		override protected esEventoFundoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFundoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EventoFundoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFundoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EventoFundoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EventoFundoQuery query;
	}



	[Serializable]
	public partial class EventoFundoQuery : esEventoFundoQuery
	{
		public EventoFundoQuery()
		{

		}		
		
		public EventoFundoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EventoFundoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EventoFundoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EventoFundoMetadata.ColumnNames.IdEventoFundo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoMetadata.PropertyNames.IdEventoFundo;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoMetadata.ColumnNames.IdCarteiraOrigem, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoMetadata.PropertyNames.IdCarteiraOrigem;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoMetadata.ColumnNames.IdCarteiraDestino, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoMetadata.PropertyNames.IdCarteiraDestino;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoMetadata.ColumnNames.TipoEvento, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoMetadata.PropertyNames.TipoEvento;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoMetadata.ColumnNames.Percentual, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EventoFundoMetadata.PropertyNames.Percentual;	
			c.NumericPrecision = 5;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoMetadata.ColumnNames.DataPosicao, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EventoFundoMetadata.PropertyNames.DataPosicao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoMetadata.ColumnNames.Status, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = EventoFundoMetadata.PropertyNames.Status;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoMetadata.ColumnNames.ExecucaoRecolhimento, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoMetadata.PropertyNames.ExecucaoRecolhimento;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoMetadata.ColumnNames.AliquotaIR, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFundoMetadata.PropertyNames.AliquotaIR;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFundoMetadata.ColumnNames.DataRecolhimento, 9, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EventoFundoMetadata.PropertyNames.DataRecolhimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EventoFundoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string IdCarteiraOrigem = "IdCarteiraOrigem";
			 public const string IdCarteiraDestino = "IdCarteiraDestino";
			 public const string TipoEvento = "TipoEvento";
			 public const string Percentual = "Percentual";
			 public const string DataPosicao = "DataPosicao";
			 public const string Status = "Status";
			 public const string ExecucaoRecolhimento = "ExecucaoRecolhimento";
			 public const string AliquotaIR = "AliquotaIR";
			 public const string DataRecolhimento = "DataRecolhimento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEventoFundo = "IdEventoFundo";
			 public const string IdCarteiraOrigem = "IdCarteiraOrigem";
			 public const string IdCarteiraDestino = "IdCarteiraDestino";
			 public const string TipoEvento = "TipoEvento";
			 public const string Percentual = "Percentual";
			 public const string DataPosicao = "DataPosicao";
			 public const string Status = "Status";
			 public const string ExecucaoRecolhimento = "ExecucaoRecolhimento";
			 public const string AliquotaIR = "AliquotaIR";
			 public const string DataRecolhimento = "DataRecolhimento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EventoFundoMetadata))
			{
				if(EventoFundoMetadata.mapDelegates == null)
				{
					EventoFundoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EventoFundoMetadata.meta == null)
				{
					EventoFundoMetadata.meta = new EventoFundoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEventoFundo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteiraOrigem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteiraDestino", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoEvento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Percentual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataPosicao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Status", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("ExecucaoRecolhimento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AliquotaIR", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataRecolhimento", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "EventoFundo";
				meta.Destination = "EventoFundo";
				
				meta.spInsert = "proc_EventoFundoInsert";				
				meta.spUpdate = "proc_EventoFundoUpdate";		
				meta.spDelete = "proc_EventoFundoDelete";
				meta.spLoadAll = "proc_EventoFundoLoadAll";
				meta.spLoadByPrimaryKey = "proc_EventoFundoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EventoFundoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
