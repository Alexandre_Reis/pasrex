/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 19/04/2016 17:07:29
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;

namespace Financial.Fundo
{

	[Serializable]
	abstract public class esOperacaoFundoAuxCollection : esEntityCollection
	{
		public esOperacaoFundoAuxCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OperacaoFundoAuxCollection";
		}

		#region Query Logic
		protected void InitQuery(esOperacaoFundoAuxQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOperacaoFundoAuxQuery);
		}
		#endregion
		
		virtual public OperacaoFundoAux DetachEntity(OperacaoFundoAux entity)
		{
			return base.DetachEntity(entity) as OperacaoFundoAux;
		}
		
		virtual public OperacaoFundoAux AttachEntity(OperacaoFundoAux entity)
		{
			return base.AttachEntity(entity) as OperacaoFundoAux;
		}
		
		virtual public void Combine(OperacaoFundoAuxCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OperacaoFundoAux this[int index]
		{
			get
			{
				return base[index] as OperacaoFundoAux;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OperacaoFundoAux);
		}
	}



	[Serializable]
	abstract public class esOperacaoFundoAux : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOperacaoFundoAuxQuery GetDynamicQuery()
		{
			return null;
		}

		public esOperacaoFundoAux()
		{

		}

		public esOperacaoFundoAux(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacaoFundoAux)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacaoFundoAux);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacaoFundoAux);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacaoFundoAux)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacaoFundoAux);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacaoFundoAux);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacaoFundoAux)
		{
			esOperacaoFundoAuxQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacaoFundoAux == idOperacaoFundoAux);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacaoFundoAux)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacaoFundoAux",idOperacaoFundoAux);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacaoFundoAux": this.str.IdOperacaoFundoAux = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "DataConversao": this.str.DataConversao = (string)value; break;							
						case "DataLiquidacao": this.str.DataLiquidacao = (string)value; break;							
						case "DataAgendamento": this.str.DataAgendamento = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "TipoResgate": this.str.TipoResgate = (string)value; break;							
						case "IdPosicaoResgatada": this.str.IdPosicaoResgatada = (string)value; break;							
						case "IdFormaLiquidacao": this.str.IdFormaLiquidacao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "CotaOperacao": this.str.CotaOperacao = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "ValorCPMF": this.str.ValorCPMF = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "PrejuizoUsado": this.str.PrejuizoUsado = (string)value; break;							
						case "RendimentoResgate": this.str.RendimentoResgate = (string)value; break;							
						case "VariacaoResgate": this.str.VariacaoResgate = (string)value; break;							
						case "IdConta": this.str.IdConta = (string)value; break;							
						case "Observacao": this.str.Observacao = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "IdAgenda": this.str.IdAgenda = (string)value; break;							
						case "CotaInformada": this.str.CotaInformada = (string)value; break;							
						case "IdOperacaoResgatada": this.str.IdOperacaoResgatada = (string)value; break;							
						case "RegistroEditado": this.str.RegistroEditado = (string)value; break;							
						case "ValoresColados": this.str.ValoresColados = (string)value; break;							
						case "IdLocalNegociacao": this.str.IdLocalNegociacao = (string)value; break;							
						case "DataRegistro": this.str.DataRegistro = (string)value; break;							
						case "DepositoRetirada": this.str.DepositoRetirada = (string)value; break;							
						case "FieModalidade": this.str.FieModalidade = (string)value; break;							
						case "FieTabelaIr": this.str.FieTabelaIr = (string)value; break;							
						case "DataAplicCautelaResgatada": this.str.DataAplicCautelaResgatada = (string)value; break;							
						case "UserTimeStamp": this.str.UserTimeStamp = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "IdCategoriaMovimentacao": this.str.IdCategoriaMovimentacao = (string)value; break;							
						case "ExclusaoLogica": this.str.ExclusaoLogica = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacaoFundoAux":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoFundoAux = (System.Int32?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "DataConversao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataConversao = (System.DateTime?)value;
							break;
						
						case "DataLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiquidacao = (System.DateTime?)value;
							break;
						
						case "DataAgendamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAgendamento = (System.DateTime?)value;
							break;
						
						case "TipoOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoOperacao = (System.Byte?)value;
							break;
						
						case "TipoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoResgate = (System.Byte?)value;
							break;
						
						case "IdPosicaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoResgatada = (System.Int32?)value;
							break;
						
						case "IdFormaLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdFormaLiquidacao = (System.Byte?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "CotaOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaOperacao = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "ValorCPMF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMF = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "PrejuizoUsado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrejuizoUsado = (System.Decimal?)value;
							break;
						
						case "RendimentoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoResgate = (System.Decimal?)value;
							break;
						
						case "VariacaoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.VariacaoResgate = (System.Decimal?)value;
							break;
						
						case "IdConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConta = (System.Int32?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "IdAgenda":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenda = (System.Int32?)value;
							break;
						
						case "CotaInformada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CotaInformada = (System.Decimal?)value;
							break;
						
						case "IdOperacaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoResgatada = (System.Int32?)value;
							break;
						
						case "IdLocalNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalNegociacao = (System.Int32?)value;
							break;
						
						case "DataRegistro":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRegistro = (System.DateTime?)value;
							break;
						
						case "DepositoRetirada":
						
							if (value == null || value.GetType().ToString() == "System.Boolean")
								this.DepositoRetirada = (System.Boolean?)value;
							break;
						
						case "FieModalidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FieModalidade = (System.Int32?)value;
							break;
						
						case "FieTabelaIr":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.FieTabelaIr = (System.Int32?)value;
							break;
						
						case "DataAplicCautelaResgatada":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAplicCautelaResgatada = (System.DateTime?)value;
							break;
						
						case "UserTimeStamp":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.UserTimeStamp = (System.DateTime?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "IdCategoriaMovimentacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCategoriaMovimentacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OperacaoFundoAux.IdOperacaoFundoAux
		/// </summary>
		virtual public System.Int32? IdOperacaoFundoAux
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdOperacaoFundoAux);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdOperacaoFundoAux, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoFundoAuxMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoFundoAuxMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.DataConversao
		/// </summary>
		virtual public System.DateTime? DataConversao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoFundoAuxMetadata.ColumnNames.DataConversao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoFundoAuxMetadata.ColumnNames.DataConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.DataLiquidacao
		/// </summary>
		virtual public System.DateTime? DataLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoFundoAuxMetadata.ColumnNames.DataLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoFundoAuxMetadata.ColumnNames.DataLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.DataAgendamento
		/// </summary>
		virtual public System.DateTime? DataAgendamento
		{
			get
			{
				return base.GetSystemDateTime(OperacaoFundoAuxMetadata.ColumnNames.DataAgendamento);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoFundoAuxMetadata.ColumnNames.DataAgendamento, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.TipoOperacao
		/// </summary>
		virtual public System.Byte? TipoOperacao
		{
			get
			{
				return base.GetSystemByte(OperacaoFundoAuxMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemByte(OperacaoFundoAuxMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.TipoResgate
		/// </summary>
		virtual public System.Byte? TipoResgate
		{
			get
			{
				return base.GetSystemByte(OperacaoFundoAuxMetadata.ColumnNames.TipoResgate);
			}
			
			set
			{
				base.SetSystemByte(OperacaoFundoAuxMetadata.ColumnNames.TipoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.IdPosicaoResgatada
		/// </summary>
		virtual public System.Int32? IdPosicaoResgatada
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdPosicaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdPosicaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.IdFormaLiquidacao
		/// </summary>
		virtual public System.Byte? IdFormaLiquidacao
		{
			get
			{
				return base.GetSystemByte(OperacaoFundoAuxMetadata.ColumnNames.IdFormaLiquidacao);
			}
			
			set
			{
				base.SetSystemByte(OperacaoFundoAuxMetadata.ColumnNames.IdFormaLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.CotaOperacao
		/// </summary>
		virtual public System.Decimal? CotaOperacao
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.CotaOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.CotaOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.ValorCPMF
		/// </summary>
		virtual public System.Decimal? ValorCPMF
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.ValorCPMF);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.ValorCPMF, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.PrejuizoUsado
		/// </summary>
		virtual public System.Decimal? PrejuizoUsado
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.PrejuizoUsado);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.PrejuizoUsado, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.RendimentoResgate
		/// </summary>
		virtual public System.Decimal? RendimentoResgate
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.RendimentoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.RendimentoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.VariacaoResgate
		/// </summary>
		virtual public System.Decimal? VariacaoResgate
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.VariacaoResgate);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.VariacaoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.IdConta
		/// </summary>
		virtual public System.Int32? IdConta
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdConta);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdConta, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.Observacao
		/// </summary>
		virtual public System.String Observacao
		{
			get
			{
				return base.GetSystemString(OperacaoFundoAuxMetadata.ColumnNames.Observacao);
			}
			
			set
			{
				base.SetSystemString(OperacaoFundoAuxMetadata.ColumnNames.Observacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(OperacaoFundoAuxMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(OperacaoFundoAuxMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.IdAgenda
		/// </summary>
		virtual public System.Int32? IdAgenda
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdAgenda);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdAgenda, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.CotaInformada
		/// </summary>
		virtual public System.Decimal? CotaInformada
		{
			get
			{
				return base.GetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.CotaInformada);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoFundoAuxMetadata.ColumnNames.CotaInformada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.IdOperacaoResgatada
		/// </summary>
		virtual public System.Int32? IdOperacaoResgatada
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdOperacaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdOperacaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.RegistroEditado
		/// </summary>
		virtual public System.String RegistroEditado
		{
			get
			{
				return base.GetSystemString(OperacaoFundoAuxMetadata.ColumnNames.RegistroEditado);
			}
			
			set
			{
				base.SetSystemString(OperacaoFundoAuxMetadata.ColumnNames.RegistroEditado, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.ValoresColados
		/// </summary>
		virtual public System.String ValoresColados
		{
			get
			{
				return base.GetSystemString(OperacaoFundoAuxMetadata.ColumnNames.ValoresColados);
			}
			
			set
			{
				base.SetSystemString(OperacaoFundoAuxMetadata.ColumnNames.ValoresColados, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.IdLocalNegociacao
		/// </summary>
		virtual public System.Int32? IdLocalNegociacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdLocalNegociacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdLocalNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.DataRegistro
		/// </summary>
		virtual public System.DateTime? DataRegistro
		{
			get
			{
				return base.GetSystemDateTime(OperacaoFundoAuxMetadata.ColumnNames.DataRegistro);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoFundoAuxMetadata.ColumnNames.DataRegistro, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.DepositoRetirada
		/// </summary>
		virtual public System.Boolean? DepositoRetirada
		{
			get
			{
				return base.GetSystemBoolean(OperacaoFundoAuxMetadata.ColumnNames.DepositoRetirada);
			}
			
			set
			{
				base.SetSystemBoolean(OperacaoFundoAuxMetadata.ColumnNames.DepositoRetirada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.FieModalidade
		/// </summary>
		virtual public System.Int32? FieModalidade
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.FieModalidade);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.FieModalidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.FieTabelaIr
		/// </summary>
		virtual public System.Int32? FieTabelaIr
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.FieTabelaIr);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.FieTabelaIr, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.DataAplicCautelaResgatada
		/// </summary>
		virtual public System.DateTime? DataAplicCautelaResgatada
		{
			get
			{
				return base.GetSystemDateTime(OperacaoFundoAuxMetadata.ColumnNames.DataAplicCautelaResgatada);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoFundoAuxMetadata.ColumnNames.DataAplicCautelaResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.UserTimeStamp
		/// </summary>
		virtual public System.DateTime? UserTimeStamp
		{
			get
			{
				return base.GetSystemDateTime(OperacaoFundoAuxMetadata.ColumnNames.UserTimeStamp);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoFundoAuxMetadata.ColumnNames.UserTimeStamp, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.IdCategoriaMovimentacao
		/// </summary>
		virtual public System.Int32? IdCategoriaMovimentacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdCategoriaMovimentacao);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoFundoAuxMetadata.ColumnNames.IdCategoriaMovimentacao, value))
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoFundoAux.ExclusaoLogica
		/// </summary>
		virtual public System.String ExclusaoLogica
		{
			get
			{
				return base.GetSystemString(OperacaoFundoAuxMetadata.ColumnNames.ExclusaoLogica);
			}
			
			set
			{
				base.SetSystemString(OperacaoFundoAuxMetadata.ColumnNames.ExclusaoLogica, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected CategoriaMovimentacao _UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOperacaoFundoAux entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacaoFundoAux
			{
				get
				{
					System.Int32? data = entity.IdOperacaoFundoAux;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoFundoAux = null;
					else entity.IdOperacaoFundoAux = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataConversao
			{
				get
				{
					System.DateTime? data = entity.DataConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataConversao = null;
					else entity.DataConversao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DataLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiquidacao = null;
					else entity.DataLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataAgendamento
			{
				get
				{
					System.DateTime? data = entity.DataAgendamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAgendamento = null;
					else entity.DataAgendamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.Byte? data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToByte(value);
				}
			}
				
			public System.String TipoResgate
			{
				get
				{
					System.Byte? data = entity.TipoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoResgate = null;
					else entity.TipoResgate = Convert.ToByte(value);
				}
			}
				
			public System.String IdPosicaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdPosicaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoResgatada = null;
					else entity.IdPosicaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdFormaLiquidacao
			{
				get
				{
					System.Byte? data = entity.IdFormaLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdFormaLiquidacao = null;
					else entity.IdFormaLiquidacao = Convert.ToByte(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String CotaOperacao
			{
				get
				{
					System.Decimal? data = entity.CotaOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaOperacao = null;
					else entity.CotaOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCPMF
			{
				get
				{
					System.Decimal? data = entity.ValorCPMF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMF = null;
					else entity.ValorCPMF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrejuizoUsado
			{
				get
				{
					System.Decimal? data = entity.PrejuizoUsado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrejuizoUsado = null;
					else entity.PrejuizoUsado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoResgate
			{
				get
				{
					System.Decimal? data = entity.RendimentoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoResgate = null;
					else entity.RendimentoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String VariacaoResgate
			{
				get
				{
					System.Decimal? data = entity.VariacaoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.VariacaoResgate = null;
					else entity.VariacaoResgate = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdConta
			{
				get
				{
					System.Int32? data = entity.IdConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConta = null;
					else entity.IdConta = Convert.ToInt32(value);
				}
			}
				
			public System.String Observacao
			{
				get
				{
					System.String data = entity.Observacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Observacao = null;
					else entity.Observacao = Convert.ToString(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String IdAgenda
			{
				get
				{
					System.Int32? data = entity.IdAgenda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenda = null;
					else entity.IdAgenda = Convert.ToInt32(value);
				}
			}
				
			public System.String CotaInformada
			{
				get
				{
					System.Decimal? data = entity.CotaInformada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CotaInformada = null;
					else entity.CotaInformada = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdOperacaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdOperacaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoResgatada = null;
					else entity.IdOperacaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String RegistroEditado
			{
				get
				{
					System.String data = entity.RegistroEditado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RegistroEditado = null;
					else entity.RegistroEditado = Convert.ToString(value);
				}
			}
				
			public System.String ValoresColados
			{
				get
				{
					System.String data = entity.ValoresColados;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValoresColados = null;
					else entity.ValoresColados = Convert.ToString(value);
				}
			}
				
			public System.String IdLocalNegociacao
			{
				get
				{
					System.Int32? data = entity.IdLocalNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalNegociacao = null;
					else entity.IdLocalNegociacao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataRegistro
			{
				get
				{
					System.DateTime? data = entity.DataRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRegistro = null;
					else entity.DataRegistro = Convert.ToDateTime(value);
				}
			}
				
			public System.String DepositoRetirada
			{
				get
				{
					System.Boolean? data = entity.DepositoRetirada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DepositoRetirada = null;
					else entity.DepositoRetirada = Convert.ToBoolean(value);
				}
			}
				
			public System.String FieModalidade
			{
				get
				{
					System.Int32? data = entity.FieModalidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FieModalidade = null;
					else entity.FieModalidade = Convert.ToInt32(value);
				}
			}
				
			public System.String FieTabelaIr
			{
				get
				{
					System.Int32? data = entity.FieTabelaIr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FieTabelaIr = null;
					else entity.FieTabelaIr = Convert.ToInt32(value);
				}
			}
				
			public System.String DataAplicCautelaResgatada
			{
				get
				{
					System.DateTime? data = entity.DataAplicCautelaResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAplicCautelaResgatada = null;
					else entity.DataAplicCautelaResgatada = Convert.ToDateTime(value);
				}
			}
				
			public System.String UserTimeStamp
			{
				get
				{
					System.DateTime? data = entity.UserTimeStamp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UserTimeStamp = null;
					else entity.UserTimeStamp = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCategoriaMovimentacao
			{
				get
				{
					System.Int32? data = entity.IdCategoriaMovimentacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCategoriaMovimentacao = null;
					else entity.IdCategoriaMovimentacao = Convert.ToInt32(value);
				}
			}
				
			public System.String ExclusaoLogica
			{
				get
				{
					System.String data = entity.ExclusaoLogica;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExclusaoLogica = null;
					else entity.ExclusaoLogica = Convert.ToString(value);
				}
			}
			

			private esOperacaoFundoAux entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOperacaoFundoAuxQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOperacaoFundoAux can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OperacaoFundoAux : esOperacaoFundoAux
	{

				
		#region UpToCategoriaMovimentacaoByIdCategoriaMovimentacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OperacaoFundoAux_CatMovimentacao_FK1
		/// </summary>

		[XmlIgnore]
		public CategoriaMovimentacao UpToCategoriaMovimentacaoByIdCategoriaMovimentacao
		{
			get
			{
				if(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao == null
					&& IdCategoriaMovimentacao != null					)
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = new CategoriaMovimentacao();
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Where(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.IdCategoriaMovimentacao == this.IdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Load();
				}

				return this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
			}
			
			set
			{
				this.RemovePreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao");
				

				if(value == null)
				{
					this.IdCategoriaMovimentacao = null;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
				else
				{
					this.IdCategoriaMovimentacao = value.IdCategoriaMovimentacao;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = value;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OpFundoAux_Trader_FK
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao != null)
			{
				this.IdCategoriaMovimentacao = this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.IdCategoriaMovimentacao;
			}
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOperacaoFundoAuxQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoFundoAuxMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacaoFundoAux
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.IdOperacaoFundoAux, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataConversao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.DataConversao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataLiquidacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.DataLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataAgendamento
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.DataAgendamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.TipoOperacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoResgate
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.TipoResgate, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdPosicaoResgatada
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.IdPosicaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdFormaLiquidacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.IdFormaLiquidacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CotaOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.CotaOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCPMF
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.ValorCPMF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrejuizoUsado
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.PrejuizoUsado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoResgate
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.RendimentoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem VariacaoResgate
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.VariacaoResgate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdConta
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.IdConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Observacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.Observacao, esSystemType.String);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdAgenda
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.IdAgenda, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CotaInformada
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.CotaInformada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdOperacaoResgatada
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.IdOperacaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem RegistroEditado
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.RegistroEditado, esSystemType.String);
			}
		} 
		
		public esQueryItem ValoresColados
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.ValoresColados, esSystemType.String);
			}
		} 
		
		public esQueryItem IdLocalNegociacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.IdLocalNegociacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataRegistro
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.DataRegistro, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DepositoRetirada
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.DepositoRetirada, esSystemType.Boolean);
			}
		} 
		
		public esQueryItem FieModalidade
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.FieModalidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem FieTabelaIr
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.FieTabelaIr, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataAplicCautelaResgatada
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.DataAplicCautelaResgatada, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem UserTimeStamp
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.UserTimeStamp, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCategoriaMovimentacao
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.IdCategoriaMovimentacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ExclusaoLogica
		{
			get
			{
				return new esQueryItem(this, OperacaoFundoAuxMetadata.ColumnNames.ExclusaoLogica, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OperacaoFundoAuxCollection")]
	public partial class OperacaoFundoAuxCollection : esOperacaoFundoAuxCollection, IEnumerable<OperacaoFundoAux>
	{
		public OperacaoFundoAuxCollection()
		{

		}
		
		public static implicit operator List<OperacaoFundoAux>(OperacaoFundoAuxCollection coll)
		{
			List<OperacaoFundoAux> list = new List<OperacaoFundoAux>();
			
			foreach (OperacaoFundoAux emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OperacaoFundoAuxMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoFundoAuxQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OperacaoFundoAux(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OperacaoFundoAux();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OperacaoFundoAuxQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoFundoAuxQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OperacaoFundoAuxQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OperacaoFundoAux AddNew()
		{
			OperacaoFundoAux entity = base.AddNewEntity() as OperacaoFundoAux;
			
			return entity;
		}

		public OperacaoFundoAux FindByPrimaryKey(System.Int32 idOperacaoFundoAux)
		{
			return base.FindByPrimaryKey(idOperacaoFundoAux) as OperacaoFundoAux;
		}


		#region IEnumerable<OperacaoFundoAux> Members

		IEnumerator<OperacaoFundoAux> IEnumerable<OperacaoFundoAux>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OperacaoFundoAux;
			}
		}

		#endregion
		
		private OperacaoFundoAuxQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OperacaoFundoAux' table
	/// </summary>

	[Serializable]
	public partial class OperacaoFundoAux : esOperacaoFundoAux
	{
		public OperacaoFundoAux()
		{

		}
	
		public OperacaoFundoAux(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoFundoAuxMetadata.Meta();
			}
		}
		
		
		
		override protected esOperacaoFundoAuxQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoFundoAuxQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OperacaoFundoAuxQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoFundoAuxQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OperacaoFundoAuxQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OperacaoFundoAuxQuery query;
	}



	[Serializable]
	public partial class OperacaoFundoAuxQuery : esOperacaoFundoAuxQuery
	{
		public OperacaoFundoAuxQuery()
		{

		}		
		
		public OperacaoFundoAuxQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OperacaoFundoAuxMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OperacaoFundoAuxMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.IdOperacaoFundoAux, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.IdOperacaoFundoAux;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.IdOperacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.IdCarteira, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.DataOperacao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.DataConversao, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.DataConversao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.DataLiquidacao, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.DataLiquidacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.DataAgendamento, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.DataAgendamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.TipoOperacao, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.TipoOperacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.TipoResgate, 9, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.TipoResgate;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.IdPosicaoResgatada, 10, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.IdPosicaoResgatada;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.IdFormaLiquidacao, 11, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.IdFormaLiquidacao;	
			c.NumericPrecision = 3;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.Quantidade, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.CotaOperacao, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.CotaOperacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.ValorBruto, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.ValorLiquido, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.ValorIR, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.ValorIOF, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.ValorCPMF, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.ValorCPMF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.ValorPerformance, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.PrejuizoUsado, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.PrejuizoUsado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.RendimentoResgate, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.RendimentoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.VariacaoResgate, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.VariacaoResgate;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.IdConta, 23, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.IdConta;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.Observacao, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.Observacao;
			c.CharacterMaxLength = 400;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.Fonte, 25, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.IdAgenda, 26, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.IdAgenda;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.CotaInformada, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.CotaInformada;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.IdOperacaoResgatada, 28, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.IdOperacaoResgatada;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.RegistroEditado, 29, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.RegistroEditado;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.ValoresColados, 30, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.ValoresColados;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.IdLocalNegociacao, 31, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.IdLocalNegociacao;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('1')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.DataRegistro, 32, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.DataRegistro;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.DepositoRetirada, 33, typeof(System.Boolean), esSystemType.Boolean);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.DepositoRetirada;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.FieModalidade, 34, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.FieModalidade;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.FieTabelaIr, 35, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.FieTabelaIr;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.DataAplicCautelaResgatada, 36, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.DataAplicCautelaResgatada;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.UserTimeStamp, 37, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.UserTimeStamp;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.IdTrader, 38, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.IdCategoriaMovimentacao, 39, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.IdCategoriaMovimentacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoFundoAuxMetadata.ColumnNames.ExclusaoLogica, 40, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoFundoAuxMetadata.PropertyNames.ExclusaoLogica;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OperacaoFundoAuxMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacaoFundoAux = "IdOperacaoFundoAux";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataConversao = "DataConversao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string DataAgendamento = "DataAgendamento";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string TipoResgate = "TipoResgate";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string IdFormaLiquidacao = "IdFormaLiquidacao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaOperacao = "CotaOperacao";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string IdConta = "IdConta";
			 public const string Observacao = "Observacao";
			 public const string Fonte = "Fonte";
			 public const string IdAgenda = "IdAgenda";
			 public const string CotaInformada = "CotaInformada";
			 public const string IdOperacaoResgatada = "IdOperacaoResgatada";
			 public const string RegistroEditado = "RegistroEditado";
			 public const string ValoresColados = "ValoresColados";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string DataRegistro = "DataRegistro";
			 public const string DepositoRetirada = "DepositoRetirada";
			 public const string FieModalidade = "FieModalidade";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string DataAplicCautelaResgatada = "DataAplicCautelaResgatada";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string IdTrader = "IdTrader";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
			 public const string ExclusaoLogica = "ExclusaoLogica";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacaoFundoAux = "IdOperacaoFundoAux";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdCarteira = "IdCarteira";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataConversao = "DataConversao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string DataAgendamento = "DataAgendamento";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string TipoResgate = "TipoResgate";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string IdFormaLiquidacao = "IdFormaLiquidacao";
			 public const string Quantidade = "Quantidade";
			 public const string CotaOperacao = "CotaOperacao";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorCPMF = "ValorCPMF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string PrejuizoUsado = "PrejuizoUsado";
			 public const string RendimentoResgate = "RendimentoResgate";
			 public const string VariacaoResgate = "VariacaoResgate";
			 public const string IdConta = "IdConta";
			 public const string Observacao = "Observacao";
			 public const string Fonte = "Fonte";
			 public const string IdAgenda = "IdAgenda";
			 public const string CotaInformada = "CotaInformada";
			 public const string IdOperacaoResgatada = "IdOperacaoResgatada";
			 public const string RegistroEditado = "RegistroEditado";
			 public const string ValoresColados = "ValoresColados";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string DataRegistro = "DataRegistro";
			 public const string DepositoRetirada = "DepositoRetirada";
			 public const string FieModalidade = "FieModalidade";
			 public const string FieTabelaIr = "FieTabelaIr";
			 public const string DataAplicCautelaResgatada = "DataAplicCautelaResgatada";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string IdTrader = "IdTrader";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
			 public const string ExclusaoLogica = "ExclusaoLogica";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OperacaoFundoAuxMetadata))
			{
				if(OperacaoFundoAuxMetadata.mapDelegates == null)
				{
					OperacaoFundoAuxMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OperacaoFundoAuxMetadata.meta == null)
				{
					OperacaoFundoAuxMetadata.meta = new OperacaoFundoAuxMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacaoFundoAux", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataConversao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataLiquidacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataAgendamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoResgate", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdPosicaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdFormaLiquidacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CotaOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCPMF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrejuizoUsado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("VariacaoResgate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Observacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdAgenda", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CotaInformada", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdOperacaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RegistroEditado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ValoresColados", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdLocalNegociacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataRegistro", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DepositoRetirada", new esTypeMap("bit", "System.Boolean"));
				meta.AddTypeMap("FieModalidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("FieTabelaIr", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataAplicCautelaResgatada", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("UserTimeStamp", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCategoriaMovimentacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ExclusaoLogica", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "OperacaoFundoAux";
				meta.Destination = "OperacaoFundoAux";
				
				meta.spInsert = "proc_OperacaoFundoAuxInsert";				
				meta.spUpdate = "proc_OperacaoFundoAuxUpdate";		
				meta.spDelete = "proc_OperacaoFundoAuxDelete";
				meta.spLoadAll = "proc_OperacaoFundoAuxLoadAll";
				meta.spLoadByPrimaryKey = "proc_OperacaoFundoAuxLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OperacaoFundoAuxMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
