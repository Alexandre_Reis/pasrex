/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:21 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTabelaFundoCategoriaCollection : esEntityCollection
	{
		public esTabelaFundoCategoriaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaFundoCategoriaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaFundoCategoriaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaFundoCategoriaQuery);
		}
		#endregion
		
		virtual public TabelaFundoCategoria DetachEntity(TabelaFundoCategoria entity)
		{
			return base.DetachEntity(entity) as TabelaFundoCategoria;
		}
		
		virtual public TabelaFundoCategoria AttachEntity(TabelaFundoCategoria entity)
		{
			return base.AttachEntity(entity) as TabelaFundoCategoria;
		}
		
		virtual public void Combine(TabelaFundoCategoriaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaFundoCategoria this[int index]
		{
			get
			{
				return base[index] as TabelaFundoCategoria;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaFundoCategoria);
		}
	}



	[Serializable]
	abstract public class esTabelaFundoCategoria : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaFundoCategoriaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaFundoCategoria()
		{

		}

		public esTabelaFundoCategoria(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCarteira, System.Int32 idLista)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira, idLista);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira, idLista);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCarteira, System.Int32 idLista)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaFundoCategoriaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCarteira == idCarteira, query.IdLista == idLista);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCarteira, System.Int32 idLista)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteira, idLista);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteira, idLista);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCarteira, System.Int32 idLista)
		{
			esTabelaFundoCategoriaQuery query = this.GetDynamicQuery();
			query.Where(query.IdCarteira == idCarteira, query.IdLista == idLista);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCarteira, System.Int32 idLista)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCarteira",idCarteira);			parms.Add("IdLista",idLista);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdLista": this.str.IdLista = (string)value; break;							
						case "IdCategoriaFundo": this.str.IdCategoriaFundo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "IdLista":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLista = (System.Int32?)value;
							break;
						
						case "IdCategoriaFundo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCategoriaFundo = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaFundoCategoria.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(TabelaFundoCategoriaMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(TabelaFundoCategoriaMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaFundoCategoria.IdLista
		/// </summary>
		virtual public System.Int32? IdLista
		{
			get
			{
				return base.GetSystemInt32(TabelaFundoCategoriaMetadata.ColumnNames.IdLista);
			}
			
			set
			{
				base.SetSystemInt32(TabelaFundoCategoriaMetadata.ColumnNames.IdLista, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaFundoCategoria.IdCategoriaFundo
		/// </summary>
		virtual public System.Int32? IdCategoriaFundo
		{
			get
			{
				return base.GetSystemInt32(TabelaFundoCategoriaMetadata.ColumnNames.IdCategoriaFundo);
			}
			
			set
			{
				base.SetSystemInt32(TabelaFundoCategoriaMetadata.ColumnNames.IdCategoriaFundo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaFundoCategoria entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String IdLista
			{
				get
				{
					System.Int32? data = entity.IdLista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLista = null;
					else entity.IdLista = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCategoriaFundo
			{
				get
				{
					System.Int32? data = entity.IdCategoriaFundo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCategoriaFundo = null;
					else entity.IdCategoriaFundo = Convert.ToInt32(value);
				}
			}
			

			private esTabelaFundoCategoria entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaFundoCategoriaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaFundoCategoria can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaFundoCategoria : esTabelaFundoCategoria
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaFundoCategoriaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaFundoCategoriaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TabelaFundoCategoriaMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdLista
		{
			get
			{
				return new esQueryItem(this, TabelaFundoCategoriaMetadata.ColumnNames.IdLista, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCategoriaFundo
		{
			get
			{
				return new esQueryItem(this, TabelaFundoCategoriaMetadata.ColumnNames.IdCategoriaFundo, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaFundoCategoriaCollection")]
	public partial class TabelaFundoCategoriaCollection : esTabelaFundoCategoriaCollection, IEnumerable<TabelaFundoCategoria>
	{
		public TabelaFundoCategoriaCollection()
		{

		}
		
		public static implicit operator List<TabelaFundoCategoria>(TabelaFundoCategoriaCollection coll)
		{
			List<TabelaFundoCategoria> list = new List<TabelaFundoCategoria>();
			
			foreach (TabelaFundoCategoria emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaFundoCategoriaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaFundoCategoriaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaFundoCategoria(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaFundoCategoria();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaFundoCategoriaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaFundoCategoriaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaFundoCategoriaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaFundoCategoria AddNew()
		{
			TabelaFundoCategoria entity = base.AddNewEntity() as TabelaFundoCategoria;
			
			return entity;
		}

		public TabelaFundoCategoria FindByPrimaryKey(System.Int32 idCarteira, System.Int32 idLista)
		{
			return base.FindByPrimaryKey(idCarteira, idLista) as TabelaFundoCategoria;
		}


		#region IEnumerable<TabelaFundoCategoria> Members

		IEnumerator<TabelaFundoCategoria> IEnumerable<TabelaFundoCategoria>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaFundoCategoria;
			}
		}

		#endregion
		
		private TabelaFundoCategoriaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaFundoCategoria' table
	/// </summary>

	[Serializable]
	public partial class TabelaFundoCategoria : esTabelaFundoCategoria
	{
		public TabelaFundoCategoria()
		{

		}
	
		public TabelaFundoCategoria(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaFundoCategoriaMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaFundoCategoriaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaFundoCategoriaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaFundoCategoriaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaFundoCategoriaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaFundoCategoriaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaFundoCategoriaQuery query;
	}



	[Serializable]
	public partial class TabelaFundoCategoriaQuery : esTabelaFundoCategoriaQuery
	{
		public TabelaFundoCategoriaQuery()
		{

		}		
		
		public TabelaFundoCategoriaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaFundoCategoriaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaFundoCategoriaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaFundoCategoriaMetadata.ColumnNames.IdCarteira, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaFundoCategoriaMetadata.PropertyNames.IdCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaFundoCategoriaMetadata.ColumnNames.IdLista, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaFundoCategoriaMetadata.PropertyNames.IdLista;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaFundoCategoriaMetadata.ColumnNames.IdCategoriaFundo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaFundoCategoriaMetadata.PropertyNames.IdCategoriaFundo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaFundoCategoriaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string IdLista = "IdLista";
			 public const string IdCategoriaFundo = "IdCategoriaFundo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteira = "IdCarteira";
			 public const string IdLista = "IdLista";
			 public const string IdCategoriaFundo = "IdCategoriaFundo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaFundoCategoriaMetadata))
			{
				if(TabelaFundoCategoriaMetadata.mapDelegates == null)
				{
					TabelaFundoCategoriaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaFundoCategoriaMetadata.meta == null)
				{
					TabelaFundoCategoriaMetadata.meta = new TabelaFundoCategoriaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdLista", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCategoriaFundo", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "TabelaFundoCategoria";
				meta.Destination = "TabelaFundoCategoria";
				
				meta.spInsert = "proc_TabelaFundoCategoriaInsert";				
				meta.spUpdate = "proc_TabelaFundoCategoriaUpdate";		
				meta.spDelete = "proc_TabelaFundoCategoriaDelete";
				meta.spLoadAll = "proc_TabelaFundoCategoriaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaFundoCategoriaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaFundoCategoriaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
