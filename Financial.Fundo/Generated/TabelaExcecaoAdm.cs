/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 11/11/2014 09:54:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTabelaExcecaoAdmCollection : esEntityCollection
	{
		public esTabelaExcecaoAdmCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaExcecaoAdmCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaExcecaoAdmQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaExcecaoAdmQuery);
		}
		#endregion
		
		virtual public TabelaExcecaoAdm DetachEntity(TabelaExcecaoAdm entity)
		{
			return base.DetachEntity(entity) as TabelaExcecaoAdm;
		}
		
		virtual public TabelaExcecaoAdm AttachEntity(TabelaExcecaoAdm entity)
		{
			return base.AttachEntity(entity) as TabelaExcecaoAdm;
		}
		
		virtual public void Combine(TabelaExcecaoAdmCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaExcecaoAdm this[int index]
		{
			get
			{
				return base[index] as TabelaExcecaoAdm;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaExcecaoAdm);
		}
	}



	[Serializable]
	abstract public class esTabelaExcecaoAdm : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaExcecaoAdmQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaExcecaoAdm()
		{

		}

		public esTabelaExcecaoAdm(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabelaExcecao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabelaExcecao);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabelaExcecao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabelaExcecao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabelaExcecao);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabelaExcecao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabelaExcecao)
		{
			esTabelaExcecaoAdmQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabelaExcecao == idTabelaExcecao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabelaExcecao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabelaExcecao",idTabelaExcecao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabelaExcecao": this.str.IdTabelaExcecao = (string)value; break;							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "TipoGrupo": this.str.TipoGrupo = (string)value; break;							
						case "CodigoAtivo": this.str.CodigoAtivo = (string)value; break;							
						case "Excecoes": this.str.Excecoes = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabelaExcecao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabelaExcecao = (System.Int32?)value;
							break;
						
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "TipoGrupo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoGrupo = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaExcecaoAdm.IdTabelaExcecao
		/// </summary>
		virtual public System.Int32? IdTabelaExcecao
		{
			get
			{
				return base.GetSystemInt32(TabelaExcecaoAdmMetadata.ColumnNames.IdTabelaExcecao);
			}
			
			set
			{
				base.SetSystemInt32(TabelaExcecaoAdmMetadata.ColumnNames.IdTabelaExcecao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaExcecaoAdm.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(TabelaExcecaoAdmMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaExcecaoAdmMetadata.ColumnNames.IdTabela, value))
				{
					this._UpToTabelaTaxaAdministracaoByIdTabela = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaExcecaoAdm.TipoGrupo
		/// </summary>
		virtual public System.Int32? TipoGrupo
		{
			get
			{
				return base.GetSystemInt32(TabelaExcecaoAdmMetadata.ColumnNames.TipoGrupo);
			}
			
			set
			{
				base.SetSystemInt32(TabelaExcecaoAdmMetadata.ColumnNames.TipoGrupo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaExcecaoAdm.CodigoAtivo
		/// </summary>
		virtual public System.String CodigoAtivo
		{
			get
			{
				return base.GetSystemString(TabelaExcecaoAdmMetadata.ColumnNames.CodigoAtivo);
			}
			
			set
			{
				base.SetSystemString(TabelaExcecaoAdmMetadata.ColumnNames.CodigoAtivo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaExcecaoAdm.Excecoes
		/// </summary>
		virtual public System.String Excecoes
		{
			get
			{
				return base.GetSystemString(TabelaExcecaoAdmMetadata.ColumnNames.Excecoes);
			}
			
			set
			{
				base.SetSystemString(TabelaExcecaoAdmMetadata.ColumnNames.Excecoes, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected TabelaTaxaAdministracao _UpToTabelaTaxaAdministracaoByIdTabela;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaExcecaoAdm entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabelaExcecao
			{
				get
				{
					System.Int32? data = entity.IdTabelaExcecao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabelaExcecao = null;
					else entity.IdTabelaExcecao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoGrupo
			{
				get
				{
					System.Int32? data = entity.TipoGrupo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoGrupo = null;
					else entity.TipoGrupo = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoAtivo
			{
				get
				{
					System.String data = entity.CodigoAtivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoAtivo = null;
					else entity.CodigoAtivo = Convert.ToString(value);
				}
			}
				
			public System.String Excecoes
			{
				get
				{
					System.String data = entity.Excecoes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Excecoes = null;
					else entity.Excecoes = Convert.ToString(value);
				}
			}
			

			private esTabelaExcecaoAdm entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaExcecaoAdmQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaExcecaoAdm can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaExcecaoAdm : esTabelaExcecaoAdm
	{

				
		#region UpToTabelaTaxaAdministracaoByIdTabela - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TabelaExcecaoAdm_TabelaTaxaAdministracao_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaAdministracao UpToTabelaTaxaAdministracaoByIdTabela
		{
			get
			{
				if(this._UpToTabelaTaxaAdministracaoByIdTabela == null
					&& IdTabela != null					)
				{
					this._UpToTabelaTaxaAdministracaoByIdTabela = new TabelaTaxaAdministracao();
					this._UpToTabelaTaxaAdministracaoByIdTabela.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTabelaTaxaAdministracaoByIdTabela", this._UpToTabelaTaxaAdministracaoByIdTabela);
					this._UpToTabelaTaxaAdministracaoByIdTabela.Query.Where(this._UpToTabelaTaxaAdministracaoByIdTabela.Query.IdTabela == this.IdTabela);
					this._UpToTabelaTaxaAdministracaoByIdTabela.Query.Load();
				}

				return this._UpToTabelaTaxaAdministracaoByIdTabela;
			}
			
			set
			{
				this.RemovePreSave("UpToTabelaTaxaAdministracaoByIdTabela");
				

				if(value == null)
				{
					this.IdTabela = null;
					this._UpToTabelaTaxaAdministracaoByIdTabela = null;
				}
				else
				{
					this.IdTabela = value.IdTabela;
					this._UpToTabelaTaxaAdministracaoByIdTabela = value;
					this.SetPreSave("UpToTabelaTaxaAdministracaoByIdTabela", this._UpToTabelaTaxaAdministracaoByIdTabela);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTabelaTaxaAdministracaoByIdTabela != null)
			{
				this.IdTabela = this._UpToTabelaTaxaAdministracaoByIdTabela.IdTabela;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaExcecaoAdmQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaExcecaoAdmMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabelaExcecao
		{
			get
			{
				return new esQueryItem(this, TabelaExcecaoAdmMetadata.ColumnNames.IdTabelaExcecao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, TabelaExcecaoAdmMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoGrupo
		{
			get
			{
				return new esQueryItem(this, TabelaExcecaoAdmMetadata.ColumnNames.TipoGrupo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoAtivo
		{
			get
			{
				return new esQueryItem(this, TabelaExcecaoAdmMetadata.ColumnNames.CodigoAtivo, esSystemType.String);
			}
		} 
		
		public esQueryItem Excecoes
		{
			get
			{
				return new esQueryItem(this, TabelaExcecaoAdmMetadata.ColumnNames.Excecoes, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaExcecaoAdmCollection")]
	public partial class TabelaExcecaoAdmCollection : esTabelaExcecaoAdmCollection, IEnumerable<TabelaExcecaoAdm>
	{
		public TabelaExcecaoAdmCollection()
		{

		}
		
		public static implicit operator List<TabelaExcecaoAdm>(TabelaExcecaoAdmCollection coll)
		{
			List<TabelaExcecaoAdm> list = new List<TabelaExcecaoAdm>();
			
			foreach (TabelaExcecaoAdm emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaExcecaoAdmMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaExcecaoAdmQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaExcecaoAdm(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaExcecaoAdm();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaExcecaoAdmQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaExcecaoAdmQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaExcecaoAdmQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaExcecaoAdm AddNew()
		{
			TabelaExcecaoAdm entity = base.AddNewEntity() as TabelaExcecaoAdm;
			
			return entity;
		}

		public TabelaExcecaoAdm FindByPrimaryKey(System.Int32 idTabelaExcecao)
		{
			return base.FindByPrimaryKey(idTabelaExcecao) as TabelaExcecaoAdm;
		}


		#region IEnumerable<TabelaExcecaoAdm> Members

		IEnumerator<TabelaExcecaoAdm> IEnumerable<TabelaExcecaoAdm>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaExcecaoAdm;
			}
		}

		#endregion
		
		private TabelaExcecaoAdmQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaExcecaoAdm' table
	/// </summary>

	[Serializable]
	public partial class TabelaExcecaoAdm : esTabelaExcecaoAdm
	{
		public TabelaExcecaoAdm()
		{

		}
	
		public TabelaExcecaoAdm(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaExcecaoAdmMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaExcecaoAdmQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaExcecaoAdmQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaExcecaoAdmQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaExcecaoAdmQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaExcecaoAdmQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaExcecaoAdmQuery query;
	}



	[Serializable]
	public partial class TabelaExcecaoAdmQuery : esTabelaExcecaoAdmQuery
	{
		public TabelaExcecaoAdmQuery()
		{

		}		
		
		public TabelaExcecaoAdmQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaExcecaoAdmMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaExcecaoAdmMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaExcecaoAdmMetadata.ColumnNames.IdTabelaExcecao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaExcecaoAdmMetadata.PropertyNames.IdTabelaExcecao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaExcecaoAdmMetadata.ColumnNames.IdTabela, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaExcecaoAdmMetadata.PropertyNames.IdTabela;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaExcecaoAdmMetadata.ColumnNames.TipoGrupo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaExcecaoAdmMetadata.PropertyNames.TipoGrupo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaExcecaoAdmMetadata.ColumnNames.CodigoAtivo, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaExcecaoAdmMetadata.PropertyNames.CodigoAtivo;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaExcecaoAdmMetadata.ColumnNames.Excecoes, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaExcecaoAdmMetadata.PropertyNames.Excecoes;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaExcecaoAdmMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabelaExcecao = "IdTabelaExcecao";
			 public const string IdTabela = "IdTabela";
			 public const string TipoGrupo = "TipoGrupo";
			 public const string CodigoAtivo = "CodigoAtivo";
			 public const string Excecoes = "Excecoes";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabelaExcecao = "IdTabelaExcecao";
			 public const string IdTabela = "IdTabela";
			 public const string TipoGrupo = "TipoGrupo";
			 public const string CodigoAtivo = "CodigoAtivo";
			 public const string Excecoes = "Excecoes";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaExcecaoAdmMetadata))
			{
				if(TabelaExcecaoAdmMetadata.mapDelegates == null)
				{
					TabelaExcecaoAdmMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaExcecaoAdmMetadata.meta == null)
				{
					TabelaExcecaoAdmMetadata.meta = new TabelaExcecaoAdmMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabelaExcecao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoGrupo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoAtivo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Excecoes", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "TabelaExcecaoAdm";
				meta.Destination = "TabelaExcecaoAdm";
				
				meta.spInsert = "proc_TabelaExcecaoAdmInsert";				
				meta.spUpdate = "proc_TabelaExcecaoAdmUpdate";		
				meta.spDelete = "proc_TabelaExcecaoAdmDelete";
				meta.spLoadAll = "proc_TabelaExcecaoAdmLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaExcecaoAdmLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaExcecaoAdmMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
