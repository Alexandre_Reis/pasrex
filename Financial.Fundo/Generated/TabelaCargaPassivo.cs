/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:20 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTabelaCargaPassivoCollection : esEntityCollection
	{
		public esTabelaCargaPassivoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaCargaPassivoCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaCargaPassivoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaCargaPassivoQuery);
		}
		#endregion
		
		virtual public TabelaCargaPassivo DetachEntity(TabelaCargaPassivo entity)
		{
			return base.DetachEntity(entity) as TabelaCargaPassivo;
		}
		
		virtual public TabelaCargaPassivo AttachEntity(TabelaCargaPassivo entity)
		{
			return base.AttachEntity(entity) as TabelaCargaPassivo;
		}
		
		virtual public void Combine(TabelaCargaPassivoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaCargaPassivo this[int index]
		{
			get
			{
				return base[index] as TabelaCargaPassivo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaCargaPassivo);
		}
	}



	[Serializable]
	abstract public class esTabelaCargaPassivo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaCargaPassivoQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaCargaPassivo()
		{

		}

		public esTabelaCargaPassivo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabela)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTabela)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaCargaPassivoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTabela == idTabela);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabela)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabela)
		{
			esTabelaCargaPassivoQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabela == idTabela);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabela)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabela",idTabela);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "IdCotista": this.str.IdCotista = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;							
						case "IdNotaOperacao": this.str.IdNotaOperacao = (string)value; break;							
						case "IdNotaResgatada": this.str.IdNotaResgatada = (string)value; break;							
						case "DataAplicacao": this.str.DataAplicacao = (string)value; break;							
						case "ValorCota": this.str.ValorCota = (string)value; break;							
						case "QuantidadeCotas": this.str.QuantidadeCotas = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "ValorPerformance": this.str.ValorPerformance = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Tipo = (System.Byte?)value;
							break;
						
						case "IdNotaOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdNotaOperacao = (System.Int32?)value;
							break;
						
						case "IdNotaResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdNotaResgatada = (System.Int32?)value;
							break;
						
						case "DataAplicacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAplicacao = (System.DateTime?)value;
							break;
						
						case "ValorCota":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCota = (System.Decimal?)value;
							break;
						
						case "QuantidadeCotas":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeCotas = (System.Decimal?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "ValorPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPerformance = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaCargaPassivo.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(TabelaCargaPassivoMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				base.SetSystemInt32(TabelaCargaPassivoMetadata.ColumnNames.IdTabela, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCargaPassivo.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(TabelaCargaPassivoMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaCargaPassivoMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCargaPassivo.IdCarteira
		/// </summary>
		virtual public System.String IdCarteira
		{
			get
			{
				return base.GetSystemString(TabelaCargaPassivoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemString(TabelaCargaPassivoMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCargaPassivo.IdCotista
		/// </summary>
		virtual public System.String IdCotista
		{
			get
			{
				return base.GetSystemString(TabelaCargaPassivoMetadata.ColumnNames.IdCotista);
			}
			
			set
			{
				base.SetSystemString(TabelaCargaPassivoMetadata.ColumnNames.IdCotista, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCargaPassivo.Tipo
		/// </summary>
		virtual public System.Byte? Tipo
		{
			get
			{
				return base.GetSystemByte(TabelaCargaPassivoMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemByte(TabelaCargaPassivoMetadata.ColumnNames.Tipo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCargaPassivo.IdNotaOperacao
		/// </summary>
		virtual public System.Int32? IdNotaOperacao
		{
			get
			{
				return base.GetSystemInt32(TabelaCargaPassivoMetadata.ColumnNames.IdNotaOperacao);
			}
			
			set
			{
				base.SetSystemInt32(TabelaCargaPassivoMetadata.ColumnNames.IdNotaOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCargaPassivo.IdNotaResgatada
		/// </summary>
		virtual public System.Int32? IdNotaResgatada
		{
			get
			{
				return base.GetSystemInt32(TabelaCargaPassivoMetadata.ColumnNames.IdNotaResgatada);
			}
			
			set
			{
				base.SetSystemInt32(TabelaCargaPassivoMetadata.ColumnNames.IdNotaResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCargaPassivo.DataAplicacao
		/// </summary>
		virtual public System.DateTime? DataAplicacao
		{
			get
			{
				return base.GetSystemDateTime(TabelaCargaPassivoMetadata.ColumnNames.DataAplicacao);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaCargaPassivoMetadata.ColumnNames.DataAplicacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCargaPassivo.ValorCota
		/// </summary>
		virtual public System.Decimal? ValorCota
		{
			get
			{
				return base.GetSystemDecimal(TabelaCargaPassivoMetadata.ColumnNames.ValorCota);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCargaPassivoMetadata.ColumnNames.ValorCota, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCargaPassivo.QuantidadeCotas
		/// </summary>
		virtual public System.Decimal? QuantidadeCotas
		{
			get
			{
				return base.GetSystemDecimal(TabelaCargaPassivoMetadata.ColumnNames.QuantidadeCotas);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCargaPassivoMetadata.ColumnNames.QuantidadeCotas, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCargaPassivo.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(TabelaCargaPassivoMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCargaPassivoMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCargaPassivo.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(TabelaCargaPassivoMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCargaPassivoMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCargaPassivo.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(TabelaCargaPassivoMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCargaPassivoMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCargaPassivo.ValorPerformance
		/// </summary>
		virtual public System.Decimal? ValorPerformance
		{
			get
			{
				return base.GetSystemDecimal(TabelaCargaPassivoMetadata.ColumnNames.ValorPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCargaPassivoMetadata.ColumnNames.ValorPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCargaPassivo.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(TabelaCargaPassivoMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCargaPassivoMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaCargaPassivo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.String data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToString(value);
				}
			}
				
			public System.String IdCotista
			{
				get
				{
					System.String data = entity.IdCotista;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCotista = null;
					else entity.IdCotista = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Byte? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToByte(value);
				}
			}
				
			public System.String IdNotaOperacao
			{
				get
				{
					System.Int32? data = entity.IdNotaOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdNotaOperacao = null;
					else entity.IdNotaOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdNotaResgatada
			{
				get
				{
					System.Int32? data = entity.IdNotaResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdNotaResgatada = null;
					else entity.IdNotaResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String DataAplicacao
			{
				get
				{
					System.DateTime? data = entity.DataAplicacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAplicacao = null;
					else entity.DataAplicacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorCota
			{
				get
				{
					System.Decimal? data = entity.ValorCota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCota = null;
					else entity.ValorCota = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeCotas
			{
				get
				{
					System.Decimal? data = entity.QuantidadeCotas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeCotas = null;
					else entity.QuantidadeCotas = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPerformance
			{
				get
				{
					System.Decimal? data = entity.ValorPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPerformance = null;
					else entity.ValorPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaCargaPassivo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaCargaPassivoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaCargaPassivo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaCargaPassivo : esTabelaCargaPassivo
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaCargaPassivoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCargaPassivoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, TabelaCargaPassivoMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, TabelaCargaPassivoMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TabelaCargaPassivoMetadata.ColumnNames.IdCarteira, esSystemType.String);
			}
		} 
		
		public esQueryItem IdCotista
		{
			get
			{
				return new esQueryItem(this, TabelaCargaPassivoMetadata.ColumnNames.IdCotista, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, TabelaCargaPassivoMetadata.ColumnNames.Tipo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdNotaOperacao
		{
			get
			{
				return new esQueryItem(this, TabelaCargaPassivoMetadata.ColumnNames.IdNotaOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdNotaResgatada
		{
			get
			{
				return new esQueryItem(this, TabelaCargaPassivoMetadata.ColumnNames.IdNotaResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataAplicacao
		{
			get
			{
				return new esQueryItem(this, TabelaCargaPassivoMetadata.ColumnNames.DataAplicacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorCota
		{
			get
			{
				return new esQueryItem(this, TabelaCargaPassivoMetadata.ColumnNames.ValorCota, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeCotas
		{
			get
			{
				return new esQueryItem(this, TabelaCargaPassivoMetadata.ColumnNames.QuantidadeCotas, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, TabelaCargaPassivoMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, TabelaCargaPassivoMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, TabelaCargaPassivoMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPerformance
		{
			get
			{
				return new esQueryItem(this, TabelaCargaPassivoMetadata.ColumnNames.ValorPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, TabelaCargaPassivoMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaCargaPassivoCollection")]
	public partial class TabelaCargaPassivoCollection : esTabelaCargaPassivoCollection, IEnumerable<TabelaCargaPassivo>
	{
		public TabelaCargaPassivoCollection()
		{

		}
		
		public static implicit operator List<TabelaCargaPassivo>(TabelaCargaPassivoCollection coll)
		{
			List<TabelaCargaPassivo> list = new List<TabelaCargaPassivo>();
			
			foreach (TabelaCargaPassivo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaCargaPassivoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCargaPassivoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaCargaPassivo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaCargaPassivo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaCargaPassivoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCargaPassivoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaCargaPassivoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaCargaPassivo AddNew()
		{
			TabelaCargaPassivo entity = base.AddNewEntity() as TabelaCargaPassivo;
			
			return entity;
		}

		public TabelaCargaPassivo FindByPrimaryKey(System.Int32 idTabela)
		{
			return base.FindByPrimaryKey(idTabela) as TabelaCargaPassivo;
		}


		#region IEnumerable<TabelaCargaPassivo> Members

		IEnumerator<TabelaCargaPassivo> IEnumerable<TabelaCargaPassivo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaCargaPassivo;
			}
		}

		#endregion
		
		private TabelaCargaPassivoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaCargaPassivo' table
	/// </summary>

	[Serializable]
	public partial class TabelaCargaPassivo : esTabelaCargaPassivo
	{
		public TabelaCargaPassivo()
		{

		}
	
		public TabelaCargaPassivo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCargaPassivoMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaCargaPassivoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCargaPassivoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaCargaPassivoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCargaPassivoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaCargaPassivoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaCargaPassivoQuery query;
	}



	[Serializable]
	public partial class TabelaCargaPassivoQuery : esTabelaCargaPassivoQuery
	{
		public TabelaCargaPassivoQuery()
		{

		}		
		
		public TabelaCargaPassivoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaCargaPassivoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaCargaPassivoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaCargaPassivoMetadata.ColumnNames.IdTabela, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCargaPassivoMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCargaPassivoMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaCargaPassivoMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCargaPassivoMetadata.ColumnNames.IdCarteira, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaCargaPassivoMetadata.PropertyNames.IdCarteira;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCargaPassivoMetadata.ColumnNames.IdCotista, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaCargaPassivoMetadata.PropertyNames.IdCotista;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCargaPassivoMetadata.ColumnNames.Tipo, 4, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaCargaPassivoMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCargaPassivoMetadata.ColumnNames.IdNotaOperacao, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCargaPassivoMetadata.PropertyNames.IdNotaOperacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCargaPassivoMetadata.ColumnNames.IdNotaResgatada, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCargaPassivoMetadata.PropertyNames.IdNotaResgatada;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCargaPassivoMetadata.ColumnNames.DataAplicacao, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaCargaPassivoMetadata.PropertyNames.DataAplicacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCargaPassivoMetadata.ColumnNames.ValorCota, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCargaPassivoMetadata.PropertyNames.ValorCota;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCargaPassivoMetadata.ColumnNames.QuantidadeCotas, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCargaPassivoMetadata.PropertyNames.QuantidadeCotas;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCargaPassivoMetadata.ColumnNames.ValorBruto, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCargaPassivoMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCargaPassivoMetadata.ColumnNames.ValorIR, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCargaPassivoMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCargaPassivoMetadata.ColumnNames.ValorIOF, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCargaPassivoMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCargaPassivoMetadata.ColumnNames.ValorPerformance, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCargaPassivoMetadata.PropertyNames.ValorPerformance;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCargaPassivoMetadata.ColumnNames.ValorLiquido, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCargaPassivoMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaCargaPassivoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string Data = "Data";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCotista = "IdCotista";
			 public const string Tipo = "Tipo";
			 public const string IdNotaOperacao = "IdNotaOperacao";
			 public const string IdNotaResgatada = "IdNotaResgatada";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string ValorCota = "ValorCota";
			 public const string QuantidadeCotas = "QuantidadeCotas";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string ValorLiquido = "ValorLiquido";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string Data = "Data";
			 public const string IdCarteira = "IdCarteira";
			 public const string IdCotista = "IdCotista";
			 public const string Tipo = "Tipo";
			 public const string IdNotaOperacao = "IdNotaOperacao";
			 public const string IdNotaResgatada = "IdNotaResgatada";
			 public const string DataAplicacao = "DataAplicacao";
			 public const string ValorCota = "ValorCota";
			 public const string QuantidadeCotas = "QuantidadeCotas";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string ValorPerformance = "ValorPerformance";
			 public const string ValorLiquido = "ValorLiquido";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaCargaPassivoMetadata))
			{
				if(TabelaCargaPassivoMetadata.mapDelegates == null)
				{
					TabelaCargaPassivoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaCargaPassivoMetadata.meta == null)
				{
					TabelaCargaPassivoMetadata.meta = new TabelaCargaPassivoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdCotista", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdNotaOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdNotaResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataAplicacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorCota", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeCotas", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaCargaPassivo";
				meta.Destination = "TabelaCargaPassivo";
				
				meta.spInsert = "proc_TabelaCargaPassivoInsert";				
				meta.spUpdate = "proc_TabelaCargaPassivoUpdate";		
				meta.spDelete = "proc_TabelaCargaPassivoDelete";
				meta.spLoadAll = "proc_TabelaCargaPassivoLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaCargaPassivoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaCargaPassivoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
